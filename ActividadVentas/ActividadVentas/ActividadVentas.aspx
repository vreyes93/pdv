﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ActividadVentas.aspx.cs" Inherits="ActividadVentas.ActividadVentas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            text-align: center;
            text-decoration: underline;
        }
        .style2
        {
            border-style: solid;
            border-width: 2px;
        }
        .style3
        {
            color: #000000;
        }
        .style4
        {
            text-decoration: underline;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function AbrirVentana(url) 
        {
            var prop = "";
            prop = "toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=0,width=100height=100";
            window.open(url, "", prop);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table align="center" id="tablaTitulos" runat="server">
        <tr>
            <td class="style4">
                <strong style="text-align: center; font-weight: bold;">REPORTE DE ACTIVIDAD DE VENTAS</strong></td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:LinkButton ID="lbInfo" runat="server" Font-Underline="True" 
                    ForeColor="#696984" onclick="lbInfo_Click" Font-Bold="True" 
                    Font-Size="Small" ToolTip="Regresa la página a su vista normal"></asp:LinkButton>
            </td>
        </tr>
    </table>
    <table align="center" id="tablaFechas" runat="server">
        <tr>
            <td>
                <asp:Label ID="lbFechaInicial" runat="server" Text="Fecha Inicial:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="FechaInicial" runat="server" Width="135px" TabIndex="10" ToolTip="Ingrese la fecha inicial en formato dd/MM/yyyy" 
                    ViewStateMode="Enabled"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="lbFechaFinal" runat="server" Text="Fecha Final:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="FechaFinal" runat="server" Width="135px" TabIndex="20" 
                    MaxLength="10" 
                    ToolTip="Ingrese la fecha final en formato dd/MM/yyyy"></asp:TextBox>
            </td>
            <td>
                <asp:LinkButton ID="lbCalendarioInicio" runat="server" 
                    onclick="lbCalendarioInicio_Click" TabIndex="30" Width="107px" 
                    Visible="False">Abrir Calendario</asp:LinkButton>
                <asp:LinkButton ID="lbCalendarioFinal" runat="server" 
                    onclick="lbCalendarioFinal_Click" TabIndex="40" Width="107px" 
                    Visible="False">Abrir Calendario</asp:LinkButton>
            </td>
            <td>
                <asp:Calendar ID="CalendarioFinal" runat="server" BackColor="White" 
                    BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" 
                    DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="6pt" 
                    ForeColor="#003399" Height="150px" Visible="False" Width="160px" 
                    onselectionchanged="CalendarioFinal_SelectionChanged" TabIndex="50">
                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                    <OtherMonthDayStyle Font-Bold="False" ForeColor="#999999" />
                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" 
                        Font-Bold="True" Font-Size="8pt" ForeColor="#CCCCFF" Height="22px" />
                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                    <WeekendDayStyle BackColor="#CCCCFF" />
                </asp:Calendar>
                <asp:Calendar ID="CalendarioInicial" runat="server" BackColor="White" 
                    BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" 
                    DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="6pt" 
                    ForeColor="#003399" Height="150px" Visible="False" Width="160px" 
                    onselectionchanged="CalendarioInicial_SelectionChanged" TabIndex="60">
                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                    <OtherMonthDayStyle Font-Bold="False" ForeColor="#999999" />
                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" 
                        Font-Bold="True" Font-Size="8pt" ForeColor="#CCCCFF" Height="22px" />
                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                    <WeekendDayStyle BackColor="#CCCCFF" />
                </asp:Calendar>
            </td>
            <td>
                <asp:CheckBox ID="cbTodosArticulos" runat="server" 
                    Text="Ver todos" TabIndex="70" 
                    ToolTip="Mostrará todos los artículos aunque no se hayan vendido en el rango de fechas seleccionado" 
                    Width="85px" />
            </td>
            <td>
                <asp:CheckBox ID="cbComparar" runat="server" Text="Comparar año" 
                    ToolTip="Al marcar o desmarcar esta casilla debe hacer clic en &quot;Generar Reporte&quot;" 
                    TabIndex="80" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbProveedor" runat="server" Text="Proveedor:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="proveedor" runat="server" Width="135px" TabIndex="90" 
                    MaxLength="10" ToolTip="Código del Proveedor a Buscar"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="lbArticulo" runat="server" Text="Artículo:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="articulo" runat="server" Width="135px" TabIndex="100" 
                    MaxLength="10" ToolTip="Código del Artículo a Buscar"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:LinkButton ID="lbBuscarArticulo" runat="server" 
                    onclick="lbBuscarClasificacion_Click" TabIndex="110" 
                    ToolTip="Busca la clasificación de los artículos">Buscar Clasificación</asp:LinkButton>
                <asp:TextBox ID="nombreArticulo" runat="server" TabIndex="120" 
                    MaxLength="10" ToolTip="Nombre del Artículo a Buscar" Visible="False" 
                    Width="10px"></asp:TextBox>
                <asp:TextBox ID="nombreProveedor" runat="server" Width="252px" TabIndex="130" 
                    MaxLength="10" ToolTip="Nombre del Proveedor a Buscar" Visible="False"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btnGenerar" runat="server" Text="Generar Reporte" 
                    onclick="btnGenerar_Click" TabIndex="140" />
            </td>
            <td>
                <asp:Button ID="btnExcel" runat="server" Text="Excel" 
                    ToolTip="Exportar a EXCEL" onclick="btnExcel_Click" TabIndex="150" />
                &nbsp;
                <asp:Button ID="btnImprimir" runat="server" onclick="btnImprimir_Click" 
                    Text="Imprimir" 
                    ToolTip="Prepara la página para impresión y para regresar a la vista normal haga clic sobre las fechas." 
                    TabIndex="160" />&nbsp;
                <asp:Button ID="btnSalir" runat="server" Text="Salir" onclick="btnSalir_Click" 
                    TabIndex="170" />
            </td>
        </tr>
        </table>
            <table id="tablaBusquedaProveedor" runat="server" align="center" class="style2">
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2">
                <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                <asp:Label ID="Label2" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                Proveedor:</td>
            <td>
                <asp:TextBox ID="proveedorBuscar" runat="server" TabIndex="90"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                Nombre:</td>
            <td>
                <asp:TextBox ID="nombreProveedorBuscar" runat="server" TabIndex="100" 
                    MaxLength="100"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                <asp:Button ID="btnBuscarProveedor" runat="server" Text="Buscar Proveedor" 
                    ToolTip="Busca proveedores según el criterio de búsqueda ingresado" 
                    onclick="btnBuscarProveedor_Click" TabIndex="110" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td class="style1" colspan="2">
                <asp:GridView ID="gridProveedores" runat="server" CellPadding="4" 
                    ForeColor="#333333" GridLines="None" TabIndex="120" 
                    AutoGenerateColumns="False" onrowdatabound="gridProveedores_RowDataBound" 
                    onselectedindexchanged="gridProveedores_SelectedIndexChanged" 
                    style="text-align: left">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1Det" runat="server" CausesValidation="False" 
                                    CommandName="Select" 
                                    Text="Seleccionar"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Proveedor" HeaderText="Proveedor" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre del Proveedor" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                <asp:LinkButton ID="lbCerrarBusquedaProveedor" runat="server" 
                    ToolTip="Cierra la ventana de búsqueda" onclick="lbCerrarBusquedaProveedor_Click" 
                    TabIndex="130">Cerrar Búsqueda</asp:LinkButton>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <table id="tablaBusquedaClasificacion" runat="server" align="center" 
        class="style2">
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2">
                <asp:Label ID="Label3" runat="server" ForeColor="Red"></asp:Label>
                <asp:Label ID="Label4" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                Clasificación:</td>
            <td>
                <asp:TextBox ID="clasificacionBuscar" runat="server" TabIndex="180"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                Nombre:</td>
            <td>
                <asp:TextBox ID="nombreClasificacionBuscar" runat="server" TabIndex="190" 
                    MaxLength="100"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                <asp:Button ID="btnBuscarClasificacion" runat="server" Text="Buscar" 
                    ToolTip="Busca clasificaciones de artículos según el criterio de búsqueda ingresado" 
                    onclick="btnBuscarClasificacion_Click" TabIndex="200" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td class="style1" colspan="2">
                <asp:GridView ID="gridClasificaciones" runat="server" CellPadding="4" 
                    ForeColor="#333333" GridLines="None" TabIndex="210" 
                    AutoGenerateColumns="False" onrowdatabound="gridClasificaciones_RowDataBound" 
                    onselectedindexchanged="gridClasificaciones_SelectedIndexChanged" 
                    style="text-align: left">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1Det" runat="server" CausesValidation="False" 
                                    CommandName="Select" 
                                    Text="Seleccionar"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Clasificacion" HeaderText="Clasificación" />
                        <asp:BoundField DataField="Descripcion" 
                            HeaderText="Nombre de la Clasificación" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                <asp:LinkButton ID="lbCerrarBusquedaArticulo" runat="server" 
                    ToolTip="Cierra la ventana de búsqueda" onclick="lbCerrarBusquedaArticulo_Click" 
                    TabIndex="220">Cerrar Búsqueda</asp:LinkButton>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <table align="center" id="tablaBotones" runat="server">
        <tr>
            <td style="text-align: center">
                <asp:Label ID="Label5" runat="server" ForeColor="Red"></asp:Label>
                <asp:CheckBox ID="cbTodosProveedores" runat="server" 
                    Text="Ver todos los proveedores" TabIndex="70" Visible="False" />
                <asp:LinkButton ID="lbBuscarProveedor" runat="server" 
                    onclick="lbBuscarProveedor_Click" TabIndex="80" Visible="False">Buscar Proveedor</asp:LinkButton>
                </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Label ID="Label6" runat="server"></asp:Label>
            </td>
        </tr>
        </table>
    <table align="center">
        <tr>
            <td>
            <div style="overflow:hidden; width:1300px; height:60px;" id="divVentasCabecera" runat="server">
                <asp:GridView ID="gridVentasCabecera" runat="server" CellPadding="4" 
                    ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" 
                    onrowdatabound="gridVentas_RowDataBound" TabIndex="260" 
                    onsorting="gridVentas_Sorting" AllowSorting="True">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Proveedor" Visible="false" AccessibleHeaderText="Proveedor">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Proveedor") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Proveedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="CodigoProveedor" HeaderText="Código Proveedor" 
                            Visible="False">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NombreProveedor" HeaderText="Nombre del Proveedor" 
                            Visible="False">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Articulo" HeaderText="Artículo">
                        <HeaderStyle HorizontalAlign="Center" Width="70px" />
                        <ItemStyle HorizontalAlign="Center" Width="70px" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NombreArticulo" HeaderText="Nombre del Artículo">
                        <HeaderStyle HorizontalAlign="Left" Width="450px" />
                        <ItemStyle HorizontalAlign="Left" Width="450px" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Existencias a la Fecha" AccessibleHeaderText="ExistenciasFecha">
                            <ItemTemplate>
                                <asp:Label ID="Label12" runat="server" Text='<%# Bind("ExistenciasFecha", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox16" runat="server" Text='<%# Bind("ExistenciasFecha", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="60px" />
                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="Compras" HeaderText="Compras">
                            <ItemTemplate>
                                <asp:Label ID="Label13" runat="server" Text='<%# Bind("Compras", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox17" runat="server" Text='<%# Bind("Compras", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="40px" />
                            <ItemStyle HorizontalAlign="Center" Width="40px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="UltimaCompra" HeaderText="Última Compra">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Ventas" AccessibleHeaderText="Ventas">
                        <ItemTemplate>
                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("Ventas", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                        </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("Ventas", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Width="40px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" Width="40px"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ventas Anterior" AccessibleHeaderText="VentasAnterior" Visible="true">
                        <ItemTemplate>
                            <asp:Label ID="lbVentasAnterior" runat="server" Text='<%# Bind("VentasAnterior", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                        </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVentasAnterior" runat="server" Text='<%# Bind("VentasAnterior", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Width="40px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" Width="40px" BackColor="#dceafd"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Costo Venta Unitario" AccessibleHeaderText="CostoVentaUnitario">
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text='<%# Bind("CostoVentaUnitario", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox12" runat="server" 
                                    Text='<%# Bind("CostoVentaUnitario", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="40px" HorizontalAlign="Right" />
                            <ItemStyle Width="40px" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Costo Venta Unitario Anterior" AccessibleHeaderText="CostoVentaUnitarioAnterior">
                            <ItemTemplate>
                                <asp:Label ID="Label9" runat="server" 
                                    Text='<%# Bind("CostoVentaUnitarioAnterior", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox13" runat="server" 
                                    Text='<%# Bind("CostoVentaUnitarioAnterior", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="40px" HorizontalAlign="Right" />
                            <ItemStyle BackColor="#DCEAFD" Width="40px" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor Venta Unitario" AccessibleHeaderText="ValorVentaUnitario">
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" 
                                    Text='<%# Bind("ValorVentaUnitario", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox14" runat="server" 
                                    Text='<%# Bind("ValorVentaUnitario", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="100px" HorizontalAlign="Right" />
                            <ItemStyle Width="100px" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor Venta Unitario Anterior" AccessibleHeaderText="ValorVentaUnitarioAnterior">
                            <ItemTemplate>
                                <asp:Label ID="lbCostoVentasAnterior" runat="server" 
                                    Text='<%# Bind("ValorVentaUnitarioAnterior", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox15" runat="server" 
                                    Text='<%# Bind("ValorVentaUnitarioAnterior", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="100px" HorizontalAlign="Right" />
                            <ItemStyle BackColor="#DCEAFD" Width="100px" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Costo Venta" AccessibleHeaderText="CostoVentas">
                            <ItemTemplate>
                                <asp:Label ID="Label10" runat="server" Text='<%# Bind("CostoVentas", "{0:####,###,###,###,##.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("CostoVentas", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="40px" HorizontalAlign="Right" />
                            <ItemStyle Width="40px" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Costo Venta Anterior" AccessibleHeaderText="CostoVentasAnterior" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="Label11" runat="server" 
                                    Text='<%# Bind("CostoVentasAnterior", "{0:####,###,###,###,##.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCostoVentasAnterior" runat="server" Text='<%# Bind("CostoVentasAnterior", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="40px" HorizontalAlign="Right" />
                            <ItemStyle Width="40px" BackColor="#dceafd" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor Ventas" AccessibleHeaderText="ValorVentas">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("ValorVentas", "{0:####,###,###,###,##.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("ValorVentas", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="100px" />
                            <ItemStyle HorizontalAlign="Right" Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor Ventas Anterior" AccessibleHeaderText="ValorVentasAnterior" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="lbValorVentasAnterior" runat="server" Text='<%# Bind("ValorVentasAnterior", "{0:####,###,###,###,##.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtValorVentasAnterior" runat="server" Text='<%# Bind("ValorVentasAnterior", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="100px" />
                            <ItemStyle HorizontalAlign="Right" Width="100px" BackColor="#dceafd" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Margen Ventas Monto" AccessibleHeaderText="MargenVentasMonto" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("MargenVentasMonto", "{0:####,###,###,###,##.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("MargenVentasMonto", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="110px" />
                            <ItemStyle HorizontalAlign="Right" Width="110px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Margen Monto Anterior" AccessibleHeaderText="MargenVentasMontoAnterior" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="lbMargenVentasMontoAnterior" runat="server" Text='<%# Bind("MargenVentasMontoAnterior", "{0:####,###,###,###,##.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMargenVentasMontoAnterior" runat="server" Text='<%# Bind("MargenVentasMontoAnterior", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="110px" />
                            <ItemStyle HorizontalAlign="Right" Width="110px" BackColor="#dceafd" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Margen %" AccessibleHeaderText="MargenVentasPorcentaje" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Bind("MargenVentasPorcentaje", "{0:#.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("MargenVentasPorcentaje") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="60px" />
                            <ItemStyle HorizontalAlign="Right" Width="60px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Margen % Ant" AccessibleHeaderText="MargenVentasPorcentajeAnterior" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="lbMargenVentasPorcentajeAnterior" runat="server" Text='<%# Bind("MargenVentasPorcentajeAnterior", "{0:#.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMargenVentasPorcentajeAnterior" runat="server" Text='<%# Bind("MargenVentasPorcentajeAnterior") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="60px" />
                            <ItemStyle HorizontalAlign="Right" Width="60px" BackColor="#dceafd" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Margen Precio" AccessibleHeaderText="MargenPrecio" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Bind("MargenPrecio", "{0:#.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("MargenPrecio") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="40px" />
                            <ItemStyle HorizontalAlign="Right" Width="40px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Margen Anterior" AccessibleHeaderText="MargenPrecioAnterior" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="lbMargenPrecioAnterior" runat="server" Text='<%# Bind("MargenPrecioAnterior", "{0:#.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMargenPrecioAnterior" runat="server" Text='<%# Bind("MargenPrecioAnterior") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="40px" />
                            <ItemStyle HorizontalAlign="Right" Width="40px" BackColor="#dceafd" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Entradas Positivas" Visible="False" AccessibleHeaderText="EntradasPositivas">
                            <ItemTemplate>
                                <asp:Label ID="lbEntradasPositivas" runat="server" Text='<%# Bind("EntradasPositivas") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox8" runat="server" 
                                    Text='<%# Bind("EntradasPositivas") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Entradas Negativas" Visible="False" AccessibleHeaderText="EntradasNegativas">
                            <ItemTemplate>
                                <asp:Label ID="lbEntradasNegativas" runat="server" Text='<%# Bind("EntradasNegativas") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox7" runat="server" 
                                    Text='<%# Bind("EntradasNegativas") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Salidas Positivas" Visible="False" AccessibleHeaderText="SalidasPositivas">
                            <ItemTemplate>
                                <asp:Label ID="lbSalidasPositivas" runat="server" Text='<%# Bind("SalidasPositivas") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox9" runat="server" 
                                    Text='<%# Bind("SalidasPositivas") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Salidas Negativas" Visible="False" AccessibleHeaderText="SalidasNegativas">
                            <ItemTemplate>
                                <asp:Label ID="lbSalidasNegativas" runat="server" Text='<%# Bind("SalidasNegativas") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox10" runat="server" 
                                    Text='<%# Bind("SalidasNegativas") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </div>
            <div style="overflow:auto; width:1317px; height:640px" id="divVentas" runat="server">
                <asp:GridView ID="gridVentas" runat="server" CellPadding="4" 
                    ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" 
                    onrowdatabound="gridVentas_RowDataBound" TabIndex="260" 
                    onsorting="gridVentas_Sorting" AllowSorting="True" ShowHeader="false">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Proveedor" Visible="false" AccessibleHeaderText="Proveedor">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Proveedor") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Proveedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="CodigoProveedor" HeaderText="Código Proveedor" 
                            Visible="False">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NombreProveedor" HeaderText="Nombre del Proveedor" 
                            Visible="False">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Articulo" HeaderText="Artículo">
                        <HeaderStyle HorizontalAlign="Center" Width="70px" />
                        <ItemStyle HorizontalAlign="Center" Width="70px" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NombreArticulo" HeaderText="Nombre del Artículo">
                        <HeaderStyle HorizontalAlign="Left" Width="450px" />
                        <ItemStyle HorizontalAlign="Left" Width="450px" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Existencias a la Fecha" AccessibleHeaderText="ExistenciasFecha">
                            <ItemTemplate>
                                <asp:Label ID="Label12" runat="server" Text='<%# Bind("ExistenciasFecha", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox16" runat="server" Text='<%# Bind("ExistenciasFecha", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="60px" />
                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="Compras" HeaderText="Compras">
                            <ItemTemplate>
                                <asp:Label ID="Label13" runat="server" Text='<%# Bind("Compras", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox17" runat="server" Text='<%# Bind("Compras", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="40px" />
                            <ItemStyle HorizontalAlign="Center" Width="40px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="UltimaCompra" HeaderText="Última Compra">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Ventas" AccessibleHeaderText="Ventas">
                        <ItemTemplate>
                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("Ventas", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                        </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("Ventas", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Width="40px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" Width="40px"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ventas Anterior" AccessibleHeaderText="VentasAnterior" Visible="true">
                        <ItemTemplate>
                            <asp:Label ID="lbVentasAnterior" runat="server" Text='<%# Bind("VentasAnterior", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                        </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVentasAnterior" runat="server" Text='<%# Bind("VentasAnterior", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Width="40px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" Width="40px" BackColor="#dceafd"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Costo Venta Unitario" AccessibleHeaderText="CostoVentaUnitario">
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text='<%# Bind("CostoVentaUnitario", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox12" runat="server" 
                                    Text='<%# Bind("CostoVentaUnitario", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="40px" HorizontalAlign="Right" />
                            <ItemStyle Width="40px" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Costo Venta Unitario Anterior" AccessibleHeaderText="CostoVentaUnitarioAnterior">
                            <ItemTemplate>
                                <asp:Label ID="Label9" runat="server" 
                                    Text='<%# Bind("CostoVentaUnitarioAnterior", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox13" runat="server" 
                                    Text='<%# Bind("CostoVentaUnitarioAnterior", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="40px" HorizontalAlign="Right" />
                            <ItemStyle BackColor="#DCEAFD" Width="40px" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor Venta Unitario" AccessibleHeaderText="ValorVentaUnitario">
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" 
                                    Text='<%# Bind("ValorVentaUnitario", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox14" runat="server" 
                                    Text='<%# Bind("ValorVentaUnitario", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="100px" HorizontalAlign="Right" />
                            <ItemStyle Width="100px" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor Venta Unitario Anterior" AccessibleHeaderText="ValorVentaUnitarioAnterior">
                            <ItemTemplate>
                                <asp:Label ID="lbCostoVentasAnterior" runat="server" 
                                    Text='<%# Bind("ValorVentaUnitarioAnterior", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox15" runat="server" 
                                    Text='<%# Bind("ValorVentaUnitarioAnterior", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="100px" HorizontalAlign="Right" />
                            <ItemStyle BackColor="#DCEAFD" Width="100px" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Costo Venta" AccessibleHeaderText="CostoVentas">
                            <ItemTemplate>
                                <asp:Label ID="Label10" runat="server" Text='<%# Bind("CostoVentas", "{0:####,###,###,###,##.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("CostoVentas", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="40px" HorizontalAlign="Right" />
                            <ItemStyle Width="40px" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Costo Venta Anterior" AccessibleHeaderText="CostoVentasAnterior" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="Label11" runat="server" 
                                    Text='<%# Bind("CostoVentasAnterior", "{0:####,###,###,###,##.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCostoVentasAnterior" runat="server" Text='<%# Bind("CostoVentasAnterior", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="40px" HorizontalAlign="Right" />
                            <ItemStyle Width="40px" BackColor="#dceafd" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor Ventas" AccessibleHeaderText="ValorVentas">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("ValorVentas", "{0:####,###,###,###,##.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("ValorVentas", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="100px" />
                            <ItemStyle HorizontalAlign="Right" Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor Ventas Anterior" AccessibleHeaderText="ValorVentasAnterior" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="lbValorVentasAnterior" runat="server" Text='<%# Bind("ValorVentasAnterior", "{0:####,###,###,###,##.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtValorVentasAnterior" runat="server" Text='<%# Bind("ValorVentasAnterior", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="100px" />
                            <ItemStyle HorizontalAlign="Right" Width="100px" BackColor="#dceafd" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Margen Ventas Monto" AccessibleHeaderText="MargenVentasMonto" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("MargenVentasMonto", "{0:####,###,###,###,##.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("MargenVentasMonto", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="110px" />
                            <ItemStyle HorizontalAlign="Right" Width="110px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Margen Monto Anterior" AccessibleHeaderText="MargenVentasMontoAnterior" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="lbMargenVentasMontoAnterior" runat="server" Text='<%# Bind("MargenVentasMontoAnterior", "{0:####,###,###,###,##.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMargenVentasMontoAnterior" runat="server" Text='<%# Bind("MargenVentasMontoAnterior", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="110px" />
                            <ItemStyle HorizontalAlign="Right" Width="110px" BackColor="#dceafd" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Margen %" AccessibleHeaderText="MargenVentasPorcentaje" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Bind("MargenVentasPorcentaje", "{0:#.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("MargenVentasPorcentaje") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="60px" />
                            <ItemStyle HorizontalAlign="Right" Width="60px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Margen % Ant" AccessibleHeaderText="MargenVentasPorcentajeAnterior" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="lbMargenVentasPorcentajeAnterior" runat="server" Text='<%# Bind("MargenVentasPorcentajeAnterior", "{0:#.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMargenVentasPorcentajeAnterior" runat="server" Text='<%# Bind("MargenVentasPorcentajeAnterior") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="60px" />
                            <ItemStyle HorizontalAlign="Right" Width="60px" BackColor="#dceafd" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Margen Precio" AccessibleHeaderText="MargenPrecio" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Bind("MargenPrecio", "{0:#.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("MargenPrecio") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="40px" />
                            <ItemStyle HorizontalAlign="Right" Width="40px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Margen Anterior" AccessibleHeaderText="MargenPrecioAnterior" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="lbMargenPrecioAnterior" runat="server" Text='<%# Bind("MargenPrecioAnterior", "{0:#.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMargenPrecioAnterior" runat="server" Text='<%# Bind("MargenPrecioAnterior") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="40px" />
                            <ItemStyle HorizontalAlign="Right" Width="40px" BackColor="#dceafd" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Entradas Positivas" Visible="False" AccessibleHeaderText="EntradasPositivas">
                            <ItemTemplate>
                                <asp:Label ID="lbEntradasPositivas" runat="server" Text='<%# Bind("EntradasPositivas") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox8" runat="server" 
                                    Text='<%# Bind("EntradasPositivas") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Entradas Negativas" Visible="False" AccessibleHeaderText="EntradasNegativas">
                            <ItemTemplate>
                                <asp:Label ID="lbEntradasNegativas" runat="server" Text='<%# Bind("EntradasNegativas") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox7" runat="server" 
                                    Text='<%# Bind("EntradasNegativas") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Salidas Positivas" Visible="False" AccessibleHeaderText="SalidasPositivas">
                            <ItemTemplate>
                                <asp:Label ID="lbSalidasPositivas" runat="server" Text='<%# Bind("SalidasPositivas") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox9" runat="server" 
                                    Text='<%# Bind("SalidasPositivas") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Salidas Negativas" Visible="False" AccessibleHeaderText="SalidasNegativas">
                            <ItemTemplate>
                                <asp:Label ID="lbSalidasNegativas" runat="server" Text='<%# Bind("SalidasNegativas") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox10" runat="server" 
                                    Text='<%# Bind("SalidasNegativas") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </div>
                <asp:GridView ID="gridVentasImprimir" runat="server" CellPadding="4" 
                    ForeColor="#333333" AutoGenerateColumns="False" 
                    onrowdatabound="gridVentas_RowDataBound" TabIndex="260" 
                    onsorting="gridVentas_Sorting" AllowSorting="True" ShowHeader="true">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Proveedor" Visible="false" AccessibleHeaderText="Proveedor">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Proveedor") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Proveedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="CodigoProveedor" HeaderText="Código Proveedor" 
                            Visible="False">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NombreProveedor" HeaderText="Nombre del Proveedor" 
                            Visible="False">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Articulo" HeaderText="Artículo">
                        <HeaderStyle HorizontalAlign="Center" Width="70px" />
                        <ItemStyle HorizontalAlign="Center" Width="70px" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NombreArticulo" HeaderText="Nombre del Artículo">
                        <HeaderStyle HorizontalAlign="Left" Width="450px" />
                        <ItemStyle HorizontalAlign="Left" Width="450px" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Existencias a la Fecha" AccessibleHeaderText="ExistenciasFecha">
                            <ItemTemplate>
                                <asp:Label ID="Label12" runat="server" Text='<%# Bind("ExistenciasFecha", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox16" runat="server" Text='<%# Bind("ExistenciasFecha", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="60px" />
                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="Compras" HeaderText="Compras">
                            <ItemTemplate>
                                <asp:Label ID="Label13" runat="server" Text='<%# Bind("Compras", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox17" runat="server" Text='<%# Bind("Compras", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="40px" />
                            <ItemStyle HorizontalAlign="Center" Width="40px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="UltimaCompra" HeaderText="Última Compra">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Ventas" AccessibleHeaderText="Ventas">
                        <ItemTemplate>
                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("Ventas", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                        </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("Ventas", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Width="40px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" Width="40px"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ventas Anterior" AccessibleHeaderText="VentasAnterior" Visible="true">
                        <ItemTemplate>
                            <asp:Label ID="lbVentasAnterior" runat="server" Text='<%# Bind("VentasAnterior", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                        </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVentasAnterior" runat="server" Text='<%# Bind("VentasAnterior", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Width="40px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" Width="40px" BackColor="#dceafd"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Costo Venta Unitario" AccessibleHeaderText="CostoVentaUnitario">
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text='<%# Bind("CostoVentaUnitario", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox12" runat="server" 
                                    Text='<%# Bind("CostoVentaUnitario", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="40px" HorizontalAlign="Right" />
                            <ItemStyle Width="40px" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Costo Venta Unitario Anterior" AccessibleHeaderText="CostoVentaUnitarioAnterior">
                            <ItemTemplate>
                                <asp:Label ID="Label9" runat="server" 
                                    Text='<%# Bind("CostoVentaUnitarioAnterior", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox13" runat="server" 
                                    Text='<%# Bind("CostoVentaUnitarioAnterior", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="40px" HorizontalAlign="Right" />
                            <ItemStyle BackColor="#DCEAFD" Width="40px" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor Venta Unitario" AccessibleHeaderText="ValorVentaUnitario">
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" 
                                    Text='<%# Bind("ValorVentaUnitario", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox14" runat="server" 
                                    Text='<%# Bind("ValorVentaUnitario", "{0:###,###,###,###,###.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="100px" HorizontalAlign="Right" />
                            <ItemStyle Width="100px" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor Venta Unitario Anterior" AccessibleHeaderText="ValorVentaUnitarioAnterior">
                            <ItemTemplate>
                                <asp:Label ID="lbCostoVentasAnterior" runat="server" 
                                    Text='<%# Bind("ValorVentaUnitarioAnterior", "{0:###,###,###,###,###.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox15" runat="server" 
                                    Text='<%# Bind("ValorVentaUnitarioAnterior", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="100px" HorizontalAlign="Right" />
                            <ItemStyle BackColor="#DCEAFD" Width="100px" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Costo Venta" AccessibleHeaderText="CostoVentas">
                            <ItemTemplate>
                                <asp:Label ID="Label10" runat="server" Text='<%# Bind("CostoVentas", "{0:####,###,###,###,##.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("CostoVentas", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="40px" HorizontalAlign="Right" />
                            <ItemStyle Width="40px" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Costo Venta Anterior" AccessibleHeaderText="CostoVentasAnterior" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="Label11" runat="server" 
                                    Text='<%# Bind("CostoVentasAnterior", "{0:####,###,###,###,##.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCostoVentasAnterior" runat="server" Text='<%# Bind("CostoVentasAnterior", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="40px" HorizontalAlign="Right" />
                            <ItemStyle Width="40px" BackColor="#dceafd" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor Ventas" AccessibleHeaderText="ValorVentas">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("ValorVentas", "{0:####,###,###,###,##.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("ValorVentas", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="100px" />
                            <ItemStyle HorizontalAlign="Right" Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor Ventas Anterior" AccessibleHeaderText="ValorVentasAnterior" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="lbValorVentasAnterior" runat="server" Text='<%# Bind("ValorVentasAnterior", "{0:####,###,###,###,##.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtValorVentasAnterior" runat="server" Text='<%# Bind("ValorVentasAnterior", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="100px" />
                            <ItemStyle HorizontalAlign="Right" Width="100px" BackColor="#dceafd" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Margen Ventas Monto" AccessibleHeaderText="MargenVentasMonto" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("MargenVentasMonto", "{0:####,###,###,###,##.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("MargenVentasMonto", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="110px" />
                            <ItemStyle HorizontalAlign="Right" Width="110px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Margen Monto Anterior" AccessibleHeaderText="MargenVentasMontoAnterior" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="lbMargenVentasMontoAnterior" runat="server" Text='<%# Bind("MargenVentasMontoAnterior", "{0:####,###,###,###,##.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMargenVentasMontoAnterior" runat="server" Text='<%# Bind("MargenVentasMontoAnterior", "{0:####,###,###,###,##.##}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="110px" />
                            <ItemStyle HorizontalAlign="Right" Width="110px" BackColor="#dceafd" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Margen %" AccessibleHeaderText="MargenVentasPorcentaje" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Bind("MargenVentasPorcentaje", "{0:#.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("MargenVentasPorcentaje") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="60px" />
                            <ItemStyle HorizontalAlign="Right" Width="60px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Margen % Ant" AccessibleHeaderText="MargenVentasPorcentajeAnterior" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="lbMargenVentasPorcentajeAnterior" runat="server" Text='<%# Bind("MargenVentasPorcentajeAnterior", "{0:#.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMargenVentasPorcentajeAnterior" runat="server" Text='<%# Bind("MargenVentasPorcentajeAnterior") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="60px" />
                            <ItemStyle HorizontalAlign="Right" Width="60px" BackColor="#dceafd" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Margen Precio" AccessibleHeaderText="MargenPrecio" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Bind("MargenPrecio", "{0:#.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("MargenPrecio") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="40px" />
                            <ItemStyle HorizontalAlign="Right" Width="40px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Margen Anterior" AccessibleHeaderText="MargenPrecioAnterior" Visible="true">
                            <ItemTemplate>
                                <asp:Label ID="lbMargenPrecioAnterior" runat="server" Text='<%# Bind("MargenPrecioAnterior", "{0:#.##}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMargenPrecioAnterior" runat="server" Text='<%# Bind("MargenPrecioAnterior") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="40px" />
                            <ItemStyle HorizontalAlign="Right" Width="40px" BackColor="#dceafd" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Entradas Positivas" Visible="False" AccessibleHeaderText="EntradasPositivas">
                            <ItemTemplate>
                                <asp:Label ID="lbEntradasPositivas" runat="server" Text='<%# Bind("EntradasPositivas") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox8" runat="server" 
                                    Text='<%# Bind("EntradasPositivas") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Entradas Negativas" Visible="False" AccessibleHeaderText="EntradasNegativas">
                            <ItemTemplate>
                                <asp:Label ID="lbEntradasNegativas" runat="server" Text='<%# Bind("EntradasNegativas") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox7" runat="server" 
                                    Text='<%# Bind("EntradasNegativas") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Salidas Positivas" Visible="False" AccessibleHeaderText="SalidasPositivas">
                            <ItemTemplate>
                                <asp:Label ID="lbSalidasPositivas" runat="server" Text='<%# Bind("SalidasPositivas") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox9" runat="server" 
                                    Text='<%# Bind("SalidasPositivas") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Salidas Negativas" Visible="False" AccessibleHeaderText="SalidasNegativas">
                            <ItemTemplate>
                                <asp:Label ID="lbSalidasNegativas" runat="server" Text='<%# Bind("SalidasNegativas") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox10" runat="server" 
                                    Text='<%# Bind("SalidasNegativas") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="Black" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="Black" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    </asp:Content>
