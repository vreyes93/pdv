﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data;

namespace ActividadVentas
{
    public partial class ActividadVentas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (ViewState["FechaInicial"] != null)
            //{
            //    FechaInicial.Attributes.Add("Value", ViewState["FechaInicial"].ToString());
            //    FechaFinal.Attributes.Add("Value", ViewState["FechaFinal"].ToString());
            //}

            if (!IsPostBack)
            {
                Label1.Text = "";
                Label2.Text = "";
                Label3.Text = "";
                Label4.Text = "";
                Label5.Text = "";
                Label6.Text = "";

                proveedor.Text = "";
                nombreProveedor.Text = "";

                articulo.Text = "";
                nombreArticulo.Text = "";

                cbTodosProveedores.Checked = true;
                cbTodosArticulos.Checked = false;

                tablaBusquedaClasificacion.Visible = false;
                tablaBusquedaProveedor.Visible = false;

                FechaInicial.Focus();
                ViewState["url"] = string.Format("http://sql.fiesta.local/services/wsActividadVentas.asmx", Request.Url.Host);

                if (Request.QueryString["d1"] != null) GenerarActividadVentas();
            }
            //else
            //{
            //    if (ViewState["FechaInicial"] != null)
            //    {
            //        FechaInicial.Attributes["Value"] = ViewState["FechaInicial"].ToString();
            //        FechaFinal.Attributes["Value"] = ViewState["FechaFinal"].ToString();
            //    }
            //}
        }
           
        protected void btnGenerar_Click(object sender, EventArgs e)
        {
            DateTime mFechaInicial, mFechaFinal;

            if (FechaInicial.Text.Trim().Length == 0)
            {
                Label6.Text = "";
                Label5.Text = "Debe ingresar la fecha inicial.";
                FechaInicial.Focus();
                return;
            }

            if (FechaFinal.Text.Trim().Length == 0)
            {
                Label6.Text = "";
                Label5.Text = "Debe ingresar la fecha final.";
                FechaFinal.Focus();
                return;
            }

            FechaInicial.Text = FechaInicial.Text.Replace("-", "/");
            FechaFinal.Text = FechaFinal.Text.Replace("-", "/");

            try
            {
                mFechaInicial = Convert.ToDateTime(FechaInicial.Text);
            }
            catch
            {
                Label6.Text = "";
                Label5.Text = "La fecha inicial ingresada es inválida.";
                FechaInicial.Focus();
                return;
            }

            try
            {
                mFechaFinal = Convert.ToDateTime(FechaFinal.Text);
            }
            catch
            {
                Label6.Text = "";
                Label5.Text = "La fecha final ingresada es inválida.";
                FechaFinal.Focus();
                return;
            }

            if (mFechaInicial > mFechaFinal)
            {
                Label6.Text = "";
                Label5.Text = "La fecha inicial no puede ser mayor a la fecha final.";
                FechaInicial.Focus();
                return;
            }

            GenerarActividadVentas();  
        }

        void GenerarActividadVentas()
        {
            string mFechaInicial = FechaInicial.Text.Replace("-", "/");
            string mFechaFinal = FechaFinal.Text.Replace("-", "/");

            char[] Separador = { '/' };
            string uno = FechaInicial.Text;
            string dos = FechaFinal.Text;
            string[] stringFechaInicial = mFechaInicial.Split(Separador);
            string[] stringFechaFinal = mFechaFinal.Split(Separador);

            DateTime mInicial = DateTime.Now;
            DateTime mFinal = DateTime.Now;

            if (Request.QueryString["d1"] == null)
            {
                try
                {
                    if (stringFechaInicial[0].Length == 4)
                    {
                        mInicial = new DateTime(Convert.ToInt32(stringFechaInicial[0]), Convert.ToInt32(stringFechaInicial[1]), Convert.ToInt32(stringFechaInicial[2]));
                    }
                    else
                    {
                        mInicial = new DateTime(Convert.ToInt32(stringFechaInicial[2]), Convert.ToInt32(stringFechaInicial[1]), Convert.ToInt32(stringFechaInicial[0]));
                    }
                }
                catch
                {
                    Label5.Text = "Debe ingresar una fecha inicial válida.";
                    return;
                }

                try
                {
                    if (stringFechaFinal[0].Length == 4)
                    {
                        mFinal = new DateTime(Convert.ToInt32(stringFechaFinal[0]), Convert.ToInt32(stringFechaFinal[1]), Convert.ToInt32(stringFechaFinal[2]));
                    }
                    else
                    {
                        mFinal = new DateTime(Convert.ToInt32(stringFechaFinal[2]), Convert.ToInt32(stringFechaFinal[1]), Convert.ToInt32(stringFechaFinal[0]));
                    }
                }
                catch
                {
                    Label5.Text = "Debe ingresar una fecha final válida.";
                    return;
                }

                if (mInicial > mFinal)
                {
                    Label5.Text = "La fecha inicial no puede ser mayor a la fecha final.";
                    return;
                }
            }

            Label5.Text = "";
            string mArticulo = articulo.Text; string mProveedor = proveedor.Text; string mNombreArticulo = nombreArticulo.Text; string mNombreProveedor = nombreProveedor.Text; string mTipo = "N";

            if (cbTodosArticulos.Checked) mTipo = "T";
            if (nombreArticulo.Text.Trim() == "") mNombreArticulo = "%";
            if (nombreProveedor.Text.Trim() == "") mNombreProveedor = "%";

            if (Request.QueryString["d1"] == null)
            {
                if (articulo.Text.Trim() == "") mArticulo = "%";
                if (proveedor.Text.Trim() == "") mProveedor = "%";
            }
            else
            {
                mArticulo = Request.QueryString["art"];
                mProveedor = Request.QueryString["prov"];

                if (Request.QueryString["art"] == "XXX") mArticulo = "%";
                if (Request.QueryString["prov"] == "XXX") mProveedor = "%";
            }

            if (cbComparar.Checked)
            {
                divVentas.Style.Add("width", "1867px");
                divVentasCabecera.Style.Add("height", "80px");
                divVentasCabecera.Style.Add("width", "1850px");

                gridVentas.Columns[9].Visible = true;
                gridVentas.Columns[11].Visible = true;
                gridVentas.Columns[13].Visible = true;
                gridVentas.Columns[15].Visible = true;
                gridVentas.Columns[17].Visible = true;
                gridVentas.Columns[19].Visible = true;
                gridVentas.Columns[21].Visible = true;
                gridVentas.Columns[23].Visible = true;

                gridVentasCabecera.Columns[9].Visible = true;
                gridVentasCabecera.Columns[11].Visible = true;
                gridVentasCabecera.Columns[13].Visible = true;
                gridVentasCabecera.Columns[15].Visible = true;
                gridVentasCabecera.Columns[17].Visible = true;
                gridVentasCabecera.Columns[19].Visible = true;
                gridVentasCabecera.Columns[21].Visible = true;
                gridVentasCabecera.Columns[23].Visible = true;

                gridVentasImprimir.Columns[9].Visible = true;
                gridVentasImprimir.Columns[11].Visible = true;
                gridVentasImprimir.Columns[13].Visible = true;
                gridVentasImprimir.Columns[15].Visible = true;
                gridVentasImprimir.Columns[17].Visible = true;
                gridVentasImprimir.Columns[19].Visible = true;
                gridVentasImprimir.Columns[21].Visible = true;
                gridVentasImprimir.Columns[23].Visible = true;
            }
            else
            {
                divVentas.Style.Add("width", "1379px");
                divVentasCabecera.Style.Add("height", "60px");
                divVentasCabecera.Style.Add("width", "1362px");

                gridVentas.Columns[9].Visible = false;
                gridVentas.Columns[11].Visible = false;
                gridVentas.Columns[13].Visible = false;
                gridVentas.Columns[15].Visible = false;
                gridVentas.Columns[17].Visible = false;
                gridVentas.Columns[19].Visible = false;
                gridVentas.Columns[21].Visible = false;
                gridVentas.Columns[23].Visible = false;

                gridVentasCabecera.Columns[9].Visible = false;
                gridVentasCabecera.Columns[11].Visible = false;
                gridVentasCabecera.Columns[13].Visible = false;
                gridVentasCabecera.Columns[15].Visible = false;
                gridVentasCabecera.Columns[17].Visible = false;
                gridVentasCabecera.Columns[19].Visible = false;
                gridVentasCabecera.Columns[21].Visible = false;
                gridVentasCabecera.Columns[23].Visible = false;

                gridVentasImprimir.Columns[9].Visible = false;
                gridVentasImprimir.Columns[11].Visible = false;
                gridVentasImprimir.Columns[13].Visible = false;
                gridVentasImprimir.Columns[15].Visible = false;
                gridVentasImprimir.Columns[17].Visible = false;
                gridVentasImprimir.Columns[19].Visible = false;
                gridVentasImprimir.Columns[21].Visible = false;
                gridVentasImprimir.Columns[23].Visible = false;
            }

            if (Request.QueryString["d1"] == null)
            {
                gridVentasImprimir.Visible = false;
            }
            else
            {
                divVentas.Visible = false;
                divVentasCabecera.Visible = false;
                gridVentasImprimir.Visible = true;

                tablaFechas.Visible = false;
                tablaBotones.Visible = false;

                string mDia1 = Request.QueryString["d1"];
                string mMes1 = Request.QueryString["m1"];
                string mAnio1 = Request.QueryString["a1"];
                string mDia2 = Request.QueryString["d2"];
                string mMes2 = Request.QueryString["m2"];
                string mAnio2 = Request.QueryString["a2"];

                mInicial = new DateTime(Convert.ToInt32(mAnio1), Convert.ToInt32(mMes1), Convert.ToInt32(mDia1));
                mFinal = new DateTime(Convert.ToInt32(mAnio2), Convert.ToInt32(mMes2), Convert.ToInt32(mDia2));

                Label5.Text = "";
                Label6.Text = "";
            }
            
            lbInfo.Text = string.Format("Del: {0}   Al: {1}", mInicial.ToShortDateString(), mFinal.ToShortDateString());

            ViewState["FechaInicial"] = mInicial;
            ViewState["FechaFinal"] = mFinal;
            ViewState["Articulo"] = mArticulo;
            ViewState["Proveedor"] = mProveedor;

            uno = FechaInicial.Text;
            dos = FechaFinal.Text;
            
            var ws = new wsActividadVentas.wsActividadVentas();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);
            
            ws.Timeout = 99999999;

            var q = ws.ReporteActividadVentas(mInicial, mFinal, mProveedor, mArticulo, mNombreProveedor, mNombreArticulo, mTipo);
            for (int ii = 0; ii < q.Count(); ii++)
            {
                q[ii].ExistenciasFecha = q[ii].EntradasPositivas - q[ii].EntradasNegativas - q[ii].SalidasPositivas + q[ii].SalidasNegativas;
            }

            //q.OrderBy(x => x.Proveedor);

            DataSet ds = new DataSet();
            ds.Tables.Add(new DataTable("ventas"));

            ds.Tables["ventas"].Columns.Add(new DataColumn("Proveedor", typeof(System.String)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("CodigoProveedor", typeof(System.String)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("NombreProveedor", typeof(System.String)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("Articulo", typeof(System.String)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("NombreArticulo", typeof(System.String)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("UltimaCompra", typeof(System.String)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("Compras", typeof(System.Int32)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("ExistenciasFecha", typeof(System.Int32)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("Ventas", typeof(System.Int32)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("VentasAnterior", typeof(System.Int32)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("CostoVentaUnitario", typeof(System.Decimal)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("CostoVentaUnitarioAnterior", typeof(System.Decimal)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("CostoVentas", typeof(System.Decimal)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("CostoVentasAnterior", typeof(System.Decimal)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("ValorVentaUnitario", typeof(System.Decimal)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("ValorVentaUnitarioAnterior", typeof(System.Decimal)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("ValorVentas", typeof(System.Decimal)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("ValorVentasAnterior", typeof(System.Decimal)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("MargenVentasMonto", typeof(System.Decimal)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("MargenVentasMontoAnterior", typeof(System.Decimal)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("MargenVentasPorcentaje", typeof(System.Decimal)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("MargenVentasPorcentajeAnterior", typeof(System.Decimal)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("MargenPrecio", typeof(System.Decimal)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("MargenPrecioAnterior", typeof(System.Decimal)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("EntradasPositivas", typeof(System.Int32)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("EntradasNegativas", typeof(System.Int32)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("SalidasPositivas", typeof(System.Int32)));
            ds.Tables["ventas"].Columns.Add(new DataColumn("SalidasNegativas", typeof(System.Int32)));

            DataTable dtVentas = new DataTable();

            dtVentas.Columns.Add(new DataColumn("Proveedor", typeof(System.String)));
            dtVentas.Columns.Add(new DataColumn("CodigoProveedor", typeof(System.String)));
            dtVentas.Columns.Add(new DataColumn("NombreProveedor", typeof(System.String)));
            dtVentas.Columns.Add(new DataColumn("Articulo", typeof(System.String)));
            dtVentas.Columns.Add(new DataColumn("NombreArticulo", typeof(System.String)));
            dtVentas.Columns.Add(new DataColumn("UltimaCompra", typeof(System.String)));
            dtVentas.Columns.Add(new DataColumn("Compras", typeof(System.Int32)));
            dtVentas.Columns.Add(new DataColumn("ExistenciasFecha", typeof(System.Int32)));
            dtVentas.Columns.Add(new DataColumn("Ventas", typeof(System.Int32)));
            dtVentas.Columns.Add(new DataColumn("VentasAnterior", typeof(System.Int32)));
            dtVentas.Columns.Add(new DataColumn("CostoVentaUnitario", typeof(System.Decimal)));
            dtVentas.Columns.Add(new DataColumn("CostoVentaUnitarioAnterior", typeof(System.Decimal)));
            dtVentas.Columns.Add(new DataColumn("CostoVentas", typeof(System.Decimal)));
            dtVentas.Columns.Add(new DataColumn("CostoVentasAnterior", typeof(System.Decimal)));
            dtVentas.Columns.Add(new DataColumn("ValorVentaUnitario", typeof(System.Decimal)));
            dtVentas.Columns.Add(new DataColumn("ValorVentaUnitarioAnterior", typeof(System.Decimal)));
            dtVentas.Columns.Add(new DataColumn("ValorVentas", typeof(System.Decimal)));
            dtVentas.Columns.Add(new DataColumn("ValorVentasAnterior", typeof(System.Decimal)));
            dtVentas.Columns.Add(new DataColumn("MargenVentasMonto", typeof(System.Decimal)));
            dtVentas.Columns.Add(new DataColumn("MargenVentasMontoAnterior", typeof(System.Decimal)));
            dtVentas.Columns.Add(new DataColumn("MargenVentasPorcentaje", typeof(System.Decimal)));
            dtVentas.Columns.Add(new DataColumn("MargenVentasPorcentajeAnterior", typeof(System.Decimal)));
            dtVentas.Columns.Add(new DataColumn("MargenPrecio", typeof(System.Decimal)));
            dtVentas.Columns.Add(new DataColumn("MargenPrecioAnterior", typeof(System.Decimal)));
            dtVentas.Columns.Add(new DataColumn("EntradasPositivas", typeof(System.Int32)));
            dtVentas.Columns.Add(new DataColumn("EntradasNegativas", typeof(System.Int32)));
            dtVentas.Columns.Add(new DataColumn("SalidasPositivas", typeof(System.Int32)));
            dtVentas.Columns.Add(new DataColumn("SalidasNegativas", typeof(System.Int32)));

            for (int ii = 0; ii < q.Count(); ii++)
            {
                DataRow row = dtVentas.NewRow();

                row["Proveedor"] = q[ii].Proveedor;
                row["CodigoProveedor"] = q[ii].NombreProveedor;
                row["NombreProveedor"] = q[ii].CodigoProveedor;
                row["Articulo"] = q[ii].Articulo;
                row["NombreArticulo"] = q[ii].NombreArticulo;
                row["UltimaCompra"] = q[ii].UltimaCompra;
                row["Compras"] = q[ii].Compras;
                row["ExistenciasFecha"] = q[ii].ExistenciasFecha;
                row["Ventas"] = q[ii].Ventas;
                row["VentasAnterior"] = q[ii].VentasAnterior;
                row["CostoVentaUnitario"] = q[ii].CostoVentaUnitario;
                row["CostoVentaUnitarioAnterior"] = q[ii].CostoVentaUnitarioAnterior == null ? 0 : q[ii].CostoVentaUnitarioAnterior;
                row["CostoVentas"] = q[ii].CostoVentas;
                row["CostoVentasAnterior"] = q[ii].CostoVentasAnterior == null ? 0 : q[ii].CostoVentasAnterior;
                row["ValorVentaUnitario"] = q[ii].ValorVentaUnitario;
                row["ValorVentaUnitarioAnterior"] = q[ii].ValorVentaUnitarioAnterior == null ? 0 : q[ii].ValorVentaUnitarioAnterior;
                row["ValorVentas"] = q[ii].ValorVentas;
                row["ValorVentasAnterior"] = q[ii].ValorVentasAnterior == null ? 0 : q[ii].ValorVentasAnterior;
                row["MargenVentasMonto"] = q[ii].MargenVentasMonto;
                row["MargenVentasMontoAnterior"] = q[ii].MargenVentasMontoAnterior == null ? 0 : q[ii].MargenVentasMontoAnterior;
                row["MargenVentasPorcentaje"] = q[ii].MargenVentasPorcentaje;
                row["MargenVentasPorcentajeAnterior"] = q[ii].MargenVentasPorcentajeAnterior == null ? 0 : q[ii].MargenVentasPorcentajeAnterior;
                row["MargenPrecio"] = q[ii].MargenPrecio;
                row["MargenPrecioAnterior"] = q[ii].MargenPrecioAnterior == null ? 0 : q[ii].MargenPrecioAnterior;
                row["EntradasPositivas"] = q[ii].EntradasPositivas;
                row["EntradasNegativas"] = q[ii].EntradasNegativas;
                row["SalidasPositivas"] = q[ii].SalidasPositivas;
                row["SalidasNegativas"] = q[ii].SalidasNegativas;

                dtVentas.Rows.Add(row);
            }

            DataRow[] rows = dtVentas.Select("", "Proveedor");

            for (int ii = 0; ii < rows.Length; ii++)
            {
                DataRow row = ds.Tables["ventas"].NewRow();

                for (int jj = 0; jj < ds.Tables["ventas"].Columns.Count; jj++)
                {
                    row[jj] = rows[ii][jj];
                }

                ds.Tables["ventas"].Rows.Add(row);
            }

            ViewState["ventas"] = ds;
            FormatearGrids();

            CalendarioInicial.Visible = false;
            CalendarioFinal.Visible = false;

            tablaBusquedaClasificacion.Visible = false;
            tablaBusquedaProveedor.Visible = false;


            if (Request.QueryString["d1"] != null)
            {
                if (Request.QueryString["tipo"] == "EXCEL") ExportarExcel("Ventas", gridVentasImprimir);
            }
            
        }

        void FormatearGrids()
        {
            var ventas = (DataSet)ViewState["ventas"];

            gridVentas.DataSource = ventas;
            gridVentas.DataBind();

            gridVentasCabecera.DataSource = ventas;
            gridVentasCabecera.DataBind();

            gridVentasImprimir.DataSource = ventas;
            gridVentasImprimir.DataBind();

            GridViewHelper helper = new GridViewHelper(this.gridVentas);

            helper.RegisterGroup("Proveedor", true, true);
            helper.GroupHeader += new GroupEvent(helper_GroupHeader);

            helper.RegisterSummary("Ventas", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
            helper.RegisterSummary("CostoVentas", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
            helper.RegisterSummary("ValorVentas", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
            helper.RegisterSummary("MargenVentasMonto", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
            helper.RegisterSummary("Articulo", "{0:###,###,###,###,###.##}", SummaryOperation.Count, "Proveedor");
            helper.RegisterSummary("ExistenciasFecha", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
            helper.RegisterSummary("Compras", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");

            helper.RegisterSummary("CostoVentaUnitario", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
            helper.RegisterSummary("ValorVentaUnitario", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");

            if (cbComparar.Checked)
            {
                helper.RegisterSummary("VentasAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
                helper.RegisterSummary("CostoVentasAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
                helper.RegisterSummary("ValorVentasAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
                helper.RegisterSummary("MargenVentasMontoAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");

                helper.RegisterSummary("CostoVentaUnitarioAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
                helper.RegisterSummary("ValorVentaUnitarioAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
            }

            helper.GroupSummary += new GroupEvent(helper_Bug);

            helper.RegisterSummary("Ventas", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
            helper.RegisterSummary("CostoVentas", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
            helper.RegisterSummary("ValorVentas", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
            helper.RegisterSummary("MargenVentasMonto", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
            helper.RegisterSummary("Articulo", "{0:###,###,###,###,###.##}", SummaryOperation.Count);
            helper.RegisterSummary("ExistenciasFecha", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
            helper.RegisterSummary("Compras", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

            helper.RegisterSummary("CostoVentaUnitario", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
            helper.RegisterSummary("ValorVentaUnitario", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

            if (cbComparar.Checked)
            {
                helper.RegisterSummary("VentasAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
                helper.RegisterSummary("CostoVentasAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
                helper.RegisterSummary("ValorVentasAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
                helper.RegisterSummary("MargenVentasMontoAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helper.RegisterSummary("CostoVentaUnitarioAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
                helper.RegisterSummary("ValorVentaUnitarioAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
            }

            helper.GeneralSummary += new FooterEvent(helper_GeneralSummary);

            helper.ApplyGroupSort();
            gridVentas.DataBind();

            //Grid para impresión
            GridViewHelper helperImprimir = new GridViewHelper(this.gridVentasImprimir);

            helperImprimir.RegisterGroup("Proveedor", true, true);
            helperImprimir.GroupHeader += new GroupEvent(helper_GroupHeader);

            helperImprimir.RegisterSummary("Ventas", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
            helperImprimir.RegisterSummary("CostoVentas", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
            helperImprimir.RegisterSummary("ValorVentas", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
            helperImprimir.RegisterSummary("MargenVentasMonto", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
            helperImprimir.RegisterSummary("Articulo", "{0:###,###,###,###,###.##}", SummaryOperation.Count, "Proveedor");
            helperImprimir.RegisterSummary("ExistenciasFecha", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
            helperImprimir.RegisterSummary("Compras", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");

            helperImprimir.RegisterSummary("CostoVentaUnitario", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
            helperImprimir.RegisterSummary("ValorVentaUnitario", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");

            if (cbComparar.Checked)
            {
                helperImprimir.RegisterSummary("VentasAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
                helperImprimir.RegisterSummary("CostoVentasAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
                helperImprimir.RegisterSummary("ValorVentasAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
                helperImprimir.RegisterSummary("MargenVentasMontoAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");

                helperImprimir.RegisterSummary("CostoVentaUnitarioAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
                helperImprimir.RegisterSummary("ValorVentaUnitarioAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum, "Proveedor");
            }

            helperImprimir.GroupSummary += new GroupEvent(helper_Bug);

            helperImprimir.RegisterSummary("Ventas", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
            helperImprimir.RegisterSummary("CostoVentas", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
            helperImprimir.RegisterSummary("ValorVentas", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
            helperImprimir.RegisterSummary("MargenVentasMonto", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
            helperImprimir.RegisterSummary("Articulo", "{0:###,###,###,###,###.##}", SummaryOperation.Count);
            helperImprimir.RegisterSummary("ExistenciasFecha", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
            helperImprimir.RegisterSummary("Compras", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

            helperImprimir.RegisterSummary("CostoVentaUnitario", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
            helperImprimir.RegisterSummary("ValorVentaUnitario", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

            if (cbComparar.Checked)
            {
                helperImprimir.RegisterSummary("VentasAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
                helperImprimir.RegisterSummary("CostoVentasAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
                helperImprimir.RegisterSummary("ValorVentasAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
                helperImprimir.RegisterSummary("MargenVentasMontoAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperImprimir.RegisterSummary("CostoVentaUnitarioAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
                helperImprimir.RegisterSummary("ValorVentaUnitarioAnterior", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);
            }

            helperImprimir.GeneralSummary += new FooterEvent(helper_GeneralSummary);

            helperImprimir.ApplyGroupSort();
            gridVentasImprimir.DataBind();
            gridVentasImprimir.Sort("Proveedor", SortDirection.Ascending);

            SetToolTips();
            gridVentas.Sort("Proveedor", SortDirection.Ascending);
        }

        protected void gridVentas_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            
        }
          
        private void helper_Bug(string groupName, object[] values, GridViewRow row)
        {
            if (groupName == null) return;

            row.Cells[1].Text = "TOTAL PROVEEDOR:";
            row.Cells[1].HorizontalAlign = HorizontalAlign.Right;

            row.Font.Bold = true;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#F5F6CE");
        }

        private void helper_GeneralSummary(GridViewRow row)
        {
            row.Cells[1].Text = "TOTAL GENERAL:";
            row.Cells[1].HorizontalAlign = HorizontalAlign.Right;

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
  
            if (cbComparar.Checked)
            {
                row.Cells[4].ForeColor = System.Drawing.Color.Black;
                row.Cells[6].ForeColor = System.Drawing.Color.Black;
                row.Cells[8].ForeColor = System.Drawing.Color.Black;
                row.Cells[10].ForeColor = System.Drawing.Color.Black;
                row.Cells[12].ForeColor = System.Drawing.Color.Black;
                row.Cells[14].ForeColor = System.Drawing.Color.Black;
                row.Cells[16].ForeColor = System.Drawing.Color.Black;
            }
        }
          
        private void helper_GroupHeader(string groupName, object[] values, GridViewRow row)
        {
            if (groupName == "Proveedor")
            {
                row.Cells[0].Font.Bold = true;
                row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                row.BackColor = System.Drawing.ColorTranslator.FromHtml("#B3D5F7");
                row.Cells[0].Text = string.Format("Proveedor:   {0}", values[0].ToString());
            }
        }

        void SetToolTips()
        {
            char[] Separador = { '/' };
            string[] stringFechaInicial = FechaInicial.Text.Split(Separador);
            string[] stringFechaFinal = FechaFinal.Text.Split(Separador);

            DateTime mInicial;
            DateTime mFinal;
            
            try
            {
                if (stringFechaInicial[0].Length == 4)
                {
                    mInicial = new DateTime(Convert.ToInt32(stringFechaInicial[0]), Convert.ToInt32(stringFechaInicial[1]), Convert.ToInt32(stringFechaInicial[2]));
                }
                else
                {
                    mInicial = new DateTime(Convert.ToInt32(stringFechaInicial[2]), Convert.ToInt32(stringFechaInicial[1]), Convert.ToInt32(stringFechaInicial[0]));
                }
            }
            catch
            {
                Label5.Text = "Debe ingresar una fecha inicial válida.";
                return;
            }

            try
            {
                if (stringFechaFinal[0].Length == 4)
                {
                    mFinal = new DateTime(Convert.ToInt32(stringFechaFinal[0]), Convert.ToInt32(stringFechaFinal[1]), Convert.ToInt32(stringFechaFinal[2]));
                }
                else
                {
                    mFinal = new DateTime(Convert.ToInt32(stringFechaFinal[2]), Convert.ToInt32(stringFechaFinal[1]), Convert.ToInt32(stringFechaFinal[0]));
                }
            }
            catch
            {
                Label5.Text = "Debe ingresar una fecha final válida.";
                return;
            }

            mInicial = mInicial.AddYears(-1);
            mFinal = mFinal.AddYears(-1);

            for (int ii = 0; ii < gridVentas.Rows.Count; ii++)
            {
                Label lbProveedorGrid = (Label)gridVentas.Rows[ii].FindControl("Label1");
                //Label lbEntradasPositivas = (Label)gridVentas.Rows[ii].FindControl("lbEntradasPositivas");
                //Label lbEntradasNegativas = (Label)gridVentas.Rows[ii].FindControl("lbEntradasNegativas");
                //Label lbSalidasPositivas = (Label)gridVentas.Rows[ii].FindControl("lbSalidasPositivas");
                //Label lbSalidasNegativas = (Label)gridVentas.Rows[ii].FindControl("lbSalidasNegativas");

                //gridVentas.Rows[ii].Cells[5].Text = Convert.ToString(Convert.ToInt32(lbEntradasPositivas.Text) - Convert.ToInt32(lbEntradasNegativas.Text) - Convert.ToInt32(lbSalidasPositivas.Text) + Convert.ToInt32(lbSalidasNegativas.Text));

                gridVentas.Rows[ii].Cells[3].ToolTip = string.Format("Código del artículo{0}Proveedor:  {1}", Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[4].ToolTip = string.Format("Nombre del artículo  {0}{1}Proveedor: {2}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[5].ToolTip = string.Format("Existencias a la fecha inicial del artículo {0}{1}Del: {2}  Al: {3}{4}Proveedor:  {5}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, FechaInicial.Text, FechaFinal.Text, Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[6].ToolTip = string.Format("Compras del artículo  {0}{1}Del: {2}  Al: {3}{4}Proveedor:  {5}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, FechaInicial.Text, FechaFinal.Text, Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[7].ToolTip = string.Format("Fecha de última compra del artículo  {0}{1}Del: {2}  Al: {3}{4}Proveedor:  {5}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, FechaInicial.Text, FechaFinal.Text, Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[8].ToolTip = string.Format("Ventas del artículo  {0}{1}Del: {2}  Al: {3}{4}Proveedor:  {5}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, FechaInicial.Text, FechaFinal.Text, Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[9].ToolTip = string.Format("Ventas del artículo  {0}{1}Del: {2}  Al: {3}{4}Proveedor:  {5}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, mInicial.ToShortDateString(), mFinal.ToShortDateString(), Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[10].ToolTip = string.Format("Costo de venta unitario del artículo  {0}{1}Del: {2}  Al: {3}{4}Proveedor:  {5}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, FechaInicial.Text, FechaFinal.Text, Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[11].ToolTip = string.Format("Costo de venta unitario del artículo  {0}{1}Del: {2}  Al: {3}{4}Proveedor:  {5}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, mInicial.ToShortDateString(), mFinal.ToShortDateString(), Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[12].ToolTip = string.Format("Valor de venta unitario del artículo  {0}{1}Del: {2}  Al: {3}{4}Proveedor:  {5}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, FechaInicial.Text, FechaFinal.Text, Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[13].ToolTip = string.Format("Valor de venta unitario del artículo  {0}{1}Del: {2}  Al: {3}{4}Proveedor:  {5}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, mInicial.ToShortDateString(), mFinal.ToShortDateString(), Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[14].ToolTip = string.Format("Costo de ventas del artículo  {0}{1}Del: {2}  Al: {3}{4}Proveedor:  {5}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, FechaInicial.Text, FechaFinal.Text, Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[15].ToolTip = string.Format("Costo de ventas del artículo  {0}{1}Del: {2}  Al: {3}{4}Proveedor:  {5}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, mInicial.ToShortDateString(), mFinal.ToShortDateString(), Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[16].ToolTip = string.Format("Valor de ventas del artículo  {0}{1}Del: {2}  Al: {3}{4}Proveedor:  {5}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, FechaInicial.Text, FechaFinal.Text, Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[17].ToolTip = string.Format("Valor de ventas del artículo  {0}{1}Del: {2}  Al: {3}{4}Proveedor:  {5}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, mInicial.ToShortDateString(), mFinal.ToShortDateString(), Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[18].ToolTip = string.Format("Margen de ventas en monto del artículo  {0}{1}Del: {2}  Al: {3}{4}Proveedor:  {5}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, FechaInicial.Text, FechaFinal.Text, Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[19].ToolTip = string.Format("Margen de ventas en monto del artículo  {0}{1}Del: {2}  Al: {3}{4}Proveedor:  {5}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, mInicial.ToShortDateString(), mFinal.ToShortDateString(), Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[20].ToolTip = string.Format("Margen de ventas en % del artículo  {0}{1}Del: {2}  Al: {3}{4}Proveedor:  {5}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, FechaInicial.Text, FechaFinal.Text, Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[21].ToolTip = string.Format("Margen de ventas en % del artículo  {0}{1}Del: {2}  Al: {3}{4}Proveedor:  {5}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, mInicial.ToShortDateString(), mFinal.ToShortDateString(), Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[22].ToolTip = string.Format("Margen de precio del artículo  {0}{1}Del: {2}  Al: {3}{4}Proveedor:  {5}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, FechaInicial.Text, FechaFinal.Text, Environment.NewLine, lbProveedorGrid.Text);
                gridVentas.Rows[ii].Cells[23].ToolTip = string.Format("Margen de precio del artículo  {0}{1}Del: {2}  Al: {3}{4}Proveedor:  {5}", gridVentas.Rows[ii].Cells[3].Text, Environment.NewLine, mInicial.ToShortDateString(), mFinal.ToShortDateString(), Environment.NewLine, lbProveedorGrid.Text);
            }
        }
          
        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void gridVentas_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void lbBuscarProveedor_Click(object sender, EventArgs e)
        {

        }

        protected void btnBuscarProveedor_Click(object sender, EventArgs e)
        {

        }

        protected void gridProveedores_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridProveedores_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void lbCerrarBusquedaProveedor_Click(object sender, EventArgs e)
        {
            tablaBusquedaProveedor.Visible = false;
        }

        protected void lbBuscarClasificacion_Click(object sender, EventArgs e)
        {
            Label3.Text = "";
            Label4.Text = "";
            clasificacionBuscar.Text = "";
            nombreClasificacionBuscar.Text = "";
            tablaBusquedaClasificacion.Visible = true;
            nombreClasificacionBuscar.Focus();
        }
                  
        protected void btnBuscarClasificacion_Click(object sender, EventArgs e)
        {
            Label3.Text = "";
            Label4.Text = "";

            string mClasificacion = clasificacionBuscar.Text;
            string mDescripcion = nombreClasificacionBuscar.Text;
              
            var ws = new wsActividadVentas.wsActividadVentas();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);
            
            var q = ws.ListarClasificacionesCriterio(mClasificacion, mDescripcion);
            var clasificaciones = q.ToArray();

            gridClasificaciones.DataSource = clasificaciones;
            gridClasificaciones.DataBind();

            if (q.Length == 0)
            {
                Label3.Text = "No se encontraron clasificaciones con el criterio de búsqueda ingresado.";
            }
            else
            {
                Label4.Text = string.Format("Se encontraron {0} clasificaciones con el criterio de búsqueda ingresado.", q.Length);
            }
        }

        protected void gridClasificaciones_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridClasificaciones_SelectedIndexChanged(object sender, EventArgs e)
        {
            articulo.Text = gridClasificaciones.SelectedRow.Cells[1].Text;

            Label3.Text = "";
            Label4.Text = "";

            btnGenerar.Focus();
            tablaBusquedaClasificacion.Visible = false;
        }

        protected void lbCerrarBusquedaArticulo_Click(object sender, EventArgs e)
        {
            tablaBusquedaClasificacion.Visible = false;
        }

        protected void lbCalendarioInicio_Click(object sender, EventArgs e)
        {
            if (CalendarioInicial.Visible)
            {
                CalendarioInicial.Visible = false;
                lbCalendarioInicio.Text = "Abrir Calendario";
            }
            else
            {
                CalendarioInicial.Visible = true;
                lbCalendarioInicio.Text = "Cerrar Calendario";
            }
        }

        protected void CalendarioInicial_SelectionChanged(object sender, EventArgs e)
        {
            CalendarioInicial.Visible = false;
            lbCalendarioInicio.Text = "Abrir Calendario";
            FechaInicial.Text = CalendarioInicial.SelectedDate.ToShortDateString();
        }

        protected void lbCalendarioFinal_Click(object sender, EventArgs e)
        {
            if (CalendarioFinal.Visible)
            {
                CalendarioFinal.Visible = false;
                lbCalendarioFinal.Text = "Abrir Calendario";
            }
            else
            {
                CalendarioFinal.Visible = true;
                lbCalendarioFinal.Text = "Cerrar Calendario";
            }
        }

        protected void CalendarioFinal_SelectionChanged(object sender, EventArgs e)
        {
            CalendarioFinal.Visible = false;
            lbCalendarioFinal.Text = "Abrir Calendario";
            FechaFinal.Text = CalendarioFinal.SelectedDate.ToShortDateString();
        }

        protected void gridVentas_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void btnImprimir_Click(object sender, EventArgs e)
        {
            CargarPagina("IMP");
            FormatearGrids();
        }

        void CargarPagina(string tipo)
        {
            DateTime mInicial = Convert.ToDateTime(ViewState["FechaInicial"]);
            DateTime mFinal = Convert.ToDateTime(ViewState["FechaFinal"]);
            string mArticulo = Convert.ToString(ViewState["Articulo"]);
            string mProveedor = Convert.ToString(ViewState["Proveedor"]);

            FechaInicial.Attributes.Add("Value", ViewState["FechaInicial"].ToString());
            FechaFinal.Attributes.Add("Value", ViewState["FechaFinal"].ToString());

            if (mArticulo.Trim().Length == 0) mArticulo = "XXX";
            if (mProveedor.Trim().Length == 0) mProveedor = "XXX";

            string mUrl = string.Format("ActividadVentas.aspx?d1={0}&m1={1}&a1={2}&d2={3}&m2={4}&a2={5}&art={6}&prov={7}&tipo={8}", mInicial.Day.ToString(), mInicial.Month.ToString(), mInicial.Year.ToString(), mFinal.Day.ToString(), mFinal.Month.ToString(), mFinal.Year.ToString(), mArticulo, mProveedor, tipo);
            Page.ClientScript.RegisterStartupScript(GetType(), "script", string.Format("AbrirVentana('{0}');", mUrl), true);
        }

        protected void lbInfo_Click(object sender, EventArgs e)
        {
            Label5.Text = "";
            Label6.Text = "";
            lbInfo.Text = "";

            tablaFechas.Visible = true;
            tablaBotones.Visible = true;

            divVentas.Visible = true;
            divVentasCabecera.Visible = true;
            gridVentasImprimir.Visible = false;
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            CargarPagina("EXCEL");
            FormatearGrids();
        }

        private void ExportarExcel(string nameReport, GridView wControl)
        {
            HttpResponse response = Response;
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            Page pageToRender = new Page();
            HtmlForm form = new HtmlForm();
            form.Controls.Add(wControl);
            pageToRender.Controls.Add(form);
            response.Clear();
            response.Buffer = true;
            response.ContentType = "application/vnd.ms-excel";
            response.AddHeader("Content-Disposition", "attachment;filename=" + nameReport);
            response.Charset = "UTF-8";
            response.ContentEncoding = Encoding.Default;
            pageToRender.RenderControl(htw);
            response.Write(sw.ToString());
            response.End();
        }

    }
}