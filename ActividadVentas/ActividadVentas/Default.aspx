﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="ActividadVentas._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .style1
        {
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

    <table align="center">
        <tr>
            <td class="style1" style="text-align: center">
                <strong>MENU SISTEMA ACTIVIDAD DE VENTAS</strong></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnActividadVentas" runat="server" 
                    onclick="btnActividadVentas_Click" Text="Reporte de Actividad de Ventas" 
                    Width="274px" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnRegresar" runat="server" 
                    onclick="btnRegresar_Click" Text="Regresar" width="274px" />
            </td>
        </tr>
    </table>

</asp:Content>
