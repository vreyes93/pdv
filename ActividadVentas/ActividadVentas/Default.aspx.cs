﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ActividadVentas
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnActividadVentas_Click(object sender, EventArgs e)
        {
            Response.Redirect("ActividadVentas.aspx");
        }
          
        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("http://{0}/FiestaNet/Default.aspx", Request.Url.Host));
        }
    }
}
