﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArticuloFactura
{
    public partial class Form1 : Form
    {
        string vNombre = "Artículo Factura";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.AcceptButton = btnGrabar;
        }
          
        private void btnGrabar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtFactura.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Debe ingresar un número de factura.", vNombre, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtFactura.Focus();
                    return;
                }
                if (txtArticulo.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Debe ingresar el artículo anterior.", vNombre, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtArticulo.Focus();
                    return;
                }
                if (txtArticuloNuevo.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Debe ingresar el artículo nuevo.", vNombre, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtArticuloNuevo.Focus();
                    return;
                }
                
                string mMensaje = "";
                wsUtilitarios.wsUtilitarios ws = new wsUtilitarios.wsUtilitarios();
                ws.Url = "http://sql.fiesta.local/services/wsUtilitarios.asmx";
                
                if (!ws.ArticuloFactura(txtFactura.Text.Trim(), txtArticulo.Text.Trim(), txtArticuloNuevo.Text.Trim(), ref mMensaje))
                {
                    MessageBox.Show(mMensaje, vNombre, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                
                MessageBox.Show(mMensaje, vNombre, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtFactura.Text = "";
                txtArticulo.Text = "";
                txtArticuloNuevo.Text = "";
                txtFactura.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                MessageBox.Show(string.Format("Error al grabar. {0}{1}{0}{2}", System.Environment.NewLine, ex.Message, m), vNombre, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            txtFactura.Text = "";
            txtArticulo.Text = "";
            txtArticuloNuevo.Text = "";
            txtFactura.Focus();
        }

    }
}
