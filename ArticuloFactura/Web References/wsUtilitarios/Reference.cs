﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace ArticuloFactura.wsUtilitarios {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="wsUtilitariosSoap", Namespace="http://tempuri.org/")]
    public partial class wsUtilitarios : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback LocalizacionFacturaOperationCompleted;
        
        private System.Threading.SendOrPostCallback ArticuloFacturaOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public wsUtilitarios() {
            this.Url = global::ArticuloFactura.Properties.Settings.Default.ArticuloFactura_wsUtilitarios_wsUtilitarios;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event LocalizacionFacturaCompletedEventHandler LocalizacionFacturaCompleted;
        
        /// <remarks/>
        public event ArticuloFacturaCompletedEventHandler ArticuloFacturaCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/LocalizacionFactura", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public bool LocalizacionFactura(string factura, ref string mensaje) {
            object[] results = this.Invoke("LocalizacionFactura", new object[] {
                        factura,
                        mensaje});
            mensaje = ((string)(results[1]));
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public void LocalizacionFacturaAsync(string factura, string mensaje) {
            this.LocalizacionFacturaAsync(factura, mensaje, null);
        }
        
        /// <remarks/>
        public void LocalizacionFacturaAsync(string factura, string mensaje, object userState) {
            if ((this.LocalizacionFacturaOperationCompleted == null)) {
                this.LocalizacionFacturaOperationCompleted = new System.Threading.SendOrPostCallback(this.OnLocalizacionFacturaOperationCompleted);
            }
            this.InvokeAsync("LocalizacionFactura", new object[] {
                        factura,
                        mensaje}, this.LocalizacionFacturaOperationCompleted, userState);
        }
        
        private void OnLocalizacionFacturaOperationCompleted(object arg) {
            if ((this.LocalizacionFacturaCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.LocalizacionFacturaCompleted(this, new LocalizacionFacturaCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/ArticuloFactura", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public bool ArticuloFactura(string factura, string articulo, string articuloNuevo, ref string mensaje) {
            object[] results = this.Invoke("ArticuloFactura", new object[] {
                        factura,
                        articulo,
                        articuloNuevo,
                        mensaje});
            mensaje = ((string)(results[1]));
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public void ArticuloFacturaAsync(string factura, string articulo, string articuloNuevo, string mensaje) {
            this.ArticuloFacturaAsync(factura, articulo, articuloNuevo, mensaje, null);
        }
        
        /// <remarks/>
        public void ArticuloFacturaAsync(string factura, string articulo, string articuloNuevo, string mensaje, object userState) {
            if ((this.ArticuloFacturaOperationCompleted == null)) {
                this.ArticuloFacturaOperationCompleted = new System.Threading.SendOrPostCallback(this.OnArticuloFacturaOperationCompleted);
            }
            this.InvokeAsync("ArticuloFactura", new object[] {
                        factura,
                        articulo,
                        articuloNuevo,
                        mensaje}, this.ArticuloFacturaOperationCompleted, userState);
        }
        
        private void OnArticuloFacturaOperationCompleted(object arg) {
            if ((this.ArticuloFacturaCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.ArticuloFacturaCompleted(this, new ArticuloFacturaCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    public delegate void LocalizacionFacturaCompletedEventHandler(object sender, LocalizacionFacturaCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class LocalizacionFacturaCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal LocalizacionFacturaCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
        
        /// <remarks/>
        public string mensaje {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[1]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    public delegate void ArticuloFacturaCompletedEventHandler(object sender, ArticuloFacturaCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class ArticuloFacturaCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal ArticuloFacturaCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
        
        /// <remarks/>
        public string mensaje {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[1]));
            }
        }
    }
}

#pragma warning restore 1591