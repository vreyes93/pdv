﻿namespace Asistencia
{
    partial class frmAsistencia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAsistencia));
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbFecha = new System.Windows.Forms.Label();
            this.lbEmpleado = new System.Windows.Forms.Label();
            this.lkEstatus = new System.Windows.Forms.LinkLabel();
            this.lbEstatus = new System.Windows.Forms.Label();
            this.lkGrabarHuella = new System.Windows.Forms.LinkLabel();
            this.pbHuella2 = new System.Windows.Forms.PictureBox();
            this.pbHuella = new System.Windows.Forms.PictureBox();
            this.lbInfo = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHuella2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHuella)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(500, 500);
            this.label1.TabIndex = 4;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(10, 11);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(188, 52);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(472, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "X";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbFecha);
            this.groupBox1.Controls.Add(this.lbEmpleado);
            this.groupBox1.Controls.Add(this.lkEstatus);
            this.groupBox1.Controls.Add(this.lbEstatus);
            this.groupBox1.Controls.Add(this.lkGrabarHuella);
            this.groupBox1.Controls.Add(this.pbHuella2);
            this.groupBox1.Controls.Add(this.pbHuella);
            this.groupBox1.Controls.Add(this.lbInfo);
            this.groupBox1.Location = new System.Drawing.Point(27, 103);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(436, 385);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            // 
            // lbFecha
            // 
            this.lbFecha.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFecha.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(149)))), ((int)(((byte)(90)))));
            this.lbFecha.Location = new System.Drawing.Point(6, 356);
            this.lbFecha.Name = "lbFecha";
            this.lbFecha.Size = new System.Drawing.Size(332, 22);
            this.lbFecha.TabIndex = 15;
            this.lbFecha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbEmpleado
            // 
            this.lbEmpleado.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEmpleado.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(149)))), ((int)(((byte)(90)))));
            this.lbEmpleado.Location = new System.Drawing.Point(8, 328);
            this.lbEmpleado.Name = "lbEmpleado";
            this.lbEmpleado.Size = new System.Drawing.Size(421, 28);
            this.lbEmpleado.TabIndex = 14;
            this.lbEmpleado.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lkEstatus
            // 
            this.lkEstatus.AutoSize = true;
            this.lkEstatus.Location = new System.Drawing.Point(267, 26);
            this.lkEstatus.Name = "lkEstatus";
            this.lkEstatus.Size = new System.Drawing.Size(105, 16);
            this.lkEstatus.TabIndex = 12;
            this.lkEstatus.TabStop = true;
            this.lkEstatus.Text = "Finalizar Captura";
            this.lkEstatus.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lkEstatus_LinkClicked);
            // 
            // lbEstatus
            // 
            this.lbEstatus.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEstatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(149)))), ((int)(((byte)(90)))));
            this.lbEstatus.Location = new System.Drawing.Point(57, 20);
            this.lbEstatus.Name = "lbEstatus";
            this.lbEstatus.Size = new System.Drawing.Size(204, 28);
            this.lbEstatus.TabIndex = 13;
            this.lbEstatus.Text = "Captura de huella iniciada ...";
            this.lbEstatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lkGrabarHuella
            // 
            this.lkGrabarHuella.AutoSize = true;
            this.lkGrabarHuella.Location = new System.Drawing.Point(344, 362);
            this.lkGrabarHuella.Name = "lkGrabarHuella";
            this.lkGrabarHuella.Size = new System.Drawing.Size(86, 16);
            this.lkGrabarHuella.TabIndex = 11;
            this.lkGrabarHuella.TabStop = true;
            this.lkGrabarHuella.Text = "Grabar Huella";
            this.lkGrabarHuella.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lkGrabarHuella_LinkClicked);
            // 
            // pbHuella2
            // 
            this.pbHuella2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbHuella2.Location = new System.Drawing.Point(128, 58);
            this.pbHuella2.Name = "pbHuella2";
            this.pbHuella2.Size = new System.Drawing.Size(180, 227);
            this.pbHuella2.TabIndex = 10;
            this.pbHuella2.TabStop = false;
            // 
            // pbHuella
            // 
            this.pbHuella.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbHuella.Location = new System.Drawing.Point(128, 58);
            this.pbHuella.Name = "pbHuella";
            this.pbHuella.Size = new System.Drawing.Size(180, 227);
            this.pbHuella.TabIndex = 8;
            this.pbHuella.TabStop = false;
            // 
            // lbInfo
            // 
            this.lbInfo.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(149)))), ((int)(((byte)(90)))));
            this.lbInfo.Location = new System.Drawing.Point(9, 291);
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.Size = new System.Drawing.Size(421, 28);
            this.lbInfo.TabIndex = 7;
            this.lbInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(149)))), ((int)(((byte)(90)))));
            this.label3.Location = new System.Drawing.Point(30, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(441, 25);
            this.label3.TabIndex = 10;
            this.label3.Text = "Asistencia de Personal";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmAsistencia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(500, 500);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAsistencia";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Asistencia de Personal";
            this.Load += new System.EventHandler(this.frmAsistencia_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHuella2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHuella)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbInfo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pbHuella;
        private System.Windows.Forms.LinkLabel lkGrabarHuella;
        private System.Windows.Forms.PictureBox pbHuella2;
        private System.Windows.Forms.LinkLabel lkEstatus;
        private System.Windows.Forms.Label lbEstatus;
        private System.Windows.Forms.Label lbEmpleado;
        private System.Windows.Forms.Label lbFecha;
    }
}