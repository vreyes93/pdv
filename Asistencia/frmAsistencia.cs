﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Threading;
using System.IO;

namespace Asistencia
{
    delegate void Functionz();
    public partial class frmAsistencia : Form, DPFP.Capture.EventHandler
    {
        public int PROBABILITY_ONE = 0x7FFFFFFF;
        bool registrationInProgress = false;
        int fingerCount = 0;
        //byte[] huella = null;
        System.Drawing.Graphics graphics;
        System.Drawing.Font font;
        DPFP.Capture.ReadersCollection readers;
        DPFP.Capture.ReaderDescription readerDescription;
        DPFP.Capture.Capture capturer;
        DPFP.Template template;
        DPFP.FeatureSet[] regFeatures;
        DPFP.FeatureSet verFeatures;
        DPFP.Processing.Enrollment createRegTemplate;
        DPFP.Verification.Verification verify;
        DPFP.Capture.SampleConversion converter;
        bool status = false;

        public frmAsistencia()
        {
            graphics = this.CreateGraphics();
            font = new Font("Times New Roman", 12, FontStyle.Bold, GraphicsUnit.Pixel);
            DPFP.Capture.ReadersCollection coll = new DPFP.Capture.ReadersCollection();
            
            InitializeComponent();

            regFeatures = new DPFP.FeatureSet[4];
            for (int i = 0; i < 4; i++)
                regFeatures[i] = new DPFP.FeatureSet();

            verFeatures = new DPFP.FeatureSet();
            createRegTemplate = new DPFP.Processing.Enrollment();

            readers = new DPFP.Capture.ReadersCollection();

            for (int i = 0; i < readers.Count; i++)
            {
                readerDescription = readers[i];
                if ((readerDescription.Vendor == "Digital Persona, Inc.") || (readerDescription.Vendor == "DigitalPersona, Inc."))
                {
                    try
                    {
                        capturer = new DPFP.Capture.Capture(readerDescription.SerialNumber, DPFP.Capture.Priority.Normal);//CREAMOS UNA OPERACION DE CAPTURAS.
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    capturer.EventHandler = this;							//AQUI CAPTURAMOS LOS EVENTOS.

                    converter = new DPFP.Capture.SampleConversion();
                    try
                    {
                        verify = new DPFP.Verification.Verification();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Ex: " + ex.ToString());
                    }
                    break;
                }
            }
        }

        private void frmAsistencia_Load(object sender, EventArgs e)
        {
            global gl = new global();

            iniciarCaptura();
        }

        void iniciarCaptura()
        {
            registrationInProgress = true;
            fingerCount = 0;
            createRegTemplate.Clear();
            if (capturer != null)
            {
                try
                {
                    capturer.StartCapture();

                    status = true;
                    lkEstatus.Text = "Finalizar Captura";
                    lbEstatus.Text = "Captura de huella iniciada ...";
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("Error al iniciar la captura de la huella. {0}{1}{2}", System.Environment.NewLine, System.Environment.NewLine, ex.Message));
                }
            }
        }
        
        void finalizarCaptura()
        {
            capturer.StopCapture();

            status = false;
            lbInfo.Text = "";
            pbHuella2.Visible = true;
            lkEstatus.Text = "Iniciar Captura";
            lbEstatus.Text = "Debe iniciar la captura";
        }

        public void OnComplete(object obj, string info, DPFP.Sample sample)
        {
            this.Invoke(new Functionz(delegate()
            {
                lbInfo.Text = "Captura completa, remueva su dedo";
                lbEmpleado.Text = "";
                lbFecha.Text = "";
            }));

            this.Invoke(new Functionz(delegate()
            {
                Bitmap tempRef = null;
                converter.ConvertToPicture(sample, ref tempRef);
                System.Drawing.Image img = tempRef;
                //AQUI MOSTRAMOS LA HUELLA CAPTURADA EN EL PICTUREBOX Y LA REDIMENSIONAMOS AL TAMAÑO DEL PICTUREBOX
                Bitmap bmp = new Bitmap(converter.ConvertToPicture(sample, ref tempRef), pbHuella.Size);
                String pxFormat = bmp.PixelFormat.ToString();
                Point txtLoc = new Point(pbHuella.Width / 2 - 20, 0);
                graphics = Graphics.FromImage(bmp);
                //AHORA CUANDO EL LECTOR YA TENGA CAPTURADA UNA HUELLA COMIENZA TODO EL PROCESO
                if (registrationInProgress)
                {
                    try
                    {
                        //CAPTURAMOS 1 EXTRACCION DE LA HUELLA PARA PODER CREAR UNA PLANTILLA OPTIMA QUE SERA COMPARADA
                        regFeatures[fingerCount] = ExtractFeatures(sample, DPFP.Processing.DataPurpose.Enrollment);
                        if (regFeatures[fingerCount] != null)
                        {
                            string b64 = Convert.ToBase64String(regFeatures[fingerCount].Bytes);
                            regFeatures[fingerCount].DeSerialize(Convert.FromBase64String(b64));

                            if (regFeatures[fingerCount] == null)
                            {
                                txtLoc.X = pbHuella.Width / 2 - 26;
                                graphics.DrawString("Presión equivocada", font, Brushes.Cyan, txtLoc);
                                return;
                            }
                            ++fingerCount;

                            createRegTemplate.AddFeatures(regFeatures[fingerCount - 1]);

                            graphics = Graphics.FromImage(bmp);
                            if (fingerCount < 4)
                                graphics.DrawString("" + fingerCount + " De 4", font, Brushes.Black, txtLoc);

                            if (createRegTemplate.TemplateStatus == DPFP.Processing.Enrollment.Status.Failed)
                            {
                                capturer.StopCapture();
                                fingerCount = 0;
                                MessageBox.Show("Registro fallido, \nAsegurese de haber colocado el mismo dedo.");
                            }
                            else
                                if (createRegTemplate.TemplateStatus == DPFP.Processing.Enrollment.Status.Ready)
                                {
                                    string mensaje = ""; string empleado = ""; string nombre = ""; DateTime FechaHora = new DateTime(1980, 1, 1, 1, 0, 0); string tipo = "";

                                    MemoryStream x = new MemoryStream();
                                    MemoryStream mem = new MemoryStream();
                                    template = createRegTemplate.Template;
                                    template.Serialize(mem);
                                    verFeatures = ExtractFeatures(sample, DPFP.Processing.DataPurpose.Verification);
                                    mensaje = comparar(verFeatures, ref empleado, ref nombre, ref FechaHora, ref tipo);
                                    if (mensaje == "Ya existe un empleado con la huella capturada.")
                                    {
                                        lbEmpleado.Text = string.Format("{0}   -   {1}", empleado, nombre);
                                        lbFecha.Text = string.Format("{0}   -   {1}", tipo, FechaHora.ToString());

                                        //MessageBox.Show(mensaje, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                        //capturer.StopCapture();
                                        //this.Close();
                                    }
                                    else
                                    {
                                        MessageBox.Show("No se encontró su huella en la base de datos.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        //MessageBox.Show("Se Procedera a guardar la huella digital", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        //huella = mem.GetBuffer();
                                        ////AQUI PROCEDEMOS A GUARDAR LA HUELLA EN LA DB
                                        //guardarhuellaenDB();
                                        //capturer.StopCapture();
                                        //this.Close();
                                    }

                                    finalizarCaptura();
                                    iniciarCaptura();
                                    lbInfo.Text = "Lector de huellas conectado, coloque su dedo";

                                }
                        }

                    }
                    catch (DPFP.Error.SDKException ex)
                    {

                        MessageBox.Show(ex.Message);
                    }

                }
                else
                {
                    DPFP.Verification.Verification.Result rslt = new DPFP.Verification.Verification.Result();
                    verFeatures = ExtractFeatures(sample, DPFP.Processing.DataPurpose.Verification);
                    verify.Verify(verFeatures, template, ref rslt);

                    txtLoc.X = pbHuella.Width / 2 - 38;
                    if (rslt.Verified == true)
                        graphics.DrawString("Igual!!!!", font, Brushes.LightGreen, txtLoc);
                    else graphics.DrawString("No Igual!!!", font, Brushes.Red, txtLoc);
                }
                pbHuella.Image = bmp;
            }));
        }

        void limpiarControles()
        {
            lkEstatus.Visible = false;
            lbEstatus.Text = "Seleccione un empleado";

            lbInfo.Text = "";
            lbEmpleado.Text = "";
            lbFecha.Text = "";

            finalizarCaptura();
        }
        
        private string comparar(DPFP.FeatureSet features, ref string empleado, ref string nombre, ref DateTime FechaHora, ref string tipo)
        {
            string mensaje = "";
            global gl = new global();
            
            try
            {
                var ws = new wsAsistencia.wsAsistencia();
                //ws.Url = gl.WebServer;

                DataTable dtHuellas = new DataTable();
                dtHuellas = ws.DevuelveHuellas();

                byte[] tx = null;
                int cont = 0;
                DPFP.Verification.Verification.Result resulta = new DPFP.Verification.Verification.Result();

                for (int ii = 0; ii < dtHuellas.Rows.Count; ii++)
                {
                    tx = (byte[])dtHuellas.Rows[ii]["HUELLA"];
                    DPFP.Template templates = new DPFP.Template();
                    templates.DeSerialize((byte[])tx);
                    verify.Verify(features, templates, ref resulta);

                    if (resulta.Verified)
                    {
                        mensaje = "Ya existe un empleado con la huella capturada.";

                        cont++;
                        FechaHora = (DateTime)dtHuellas.Rows[ii]["FECHA"];
                        empleado = dtHuellas.Rows[ii]["EMPLEADO"].ToString();
                        string mEmpresa = dtHuellas.Rows[ii]["EMPRESA"].ToString();
                        nombre = ws.DevuelveNombreEmpleado(empleado, mEmpresa);
                    
                        string mMensaje = "";
                        tipo = ws.DevuelveTipo(empleado, mEmpresa);

                        if (!ws.GrabarAsistencia(ref mMensaje, empleado, mEmpresa))
                        {
                            MessageBox.Show(string.Format("Error al grabar asistencia.{0}{1}", System.Environment.NewLine, mMensaje), "Asistencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return "Error";
                        }

                        break;
                    }
                }
            }
            catch
            {
                MessageBox.Show("La VPN no está conectada, por favor verifique.", "Asistencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
            return mensaje;
        }

        public void OnFingerGone(object Capture, string ReaderSerialNumber)
        {
            this.Invoke(new Functionz(delegate()
            {
                lbInfo.Text = "Dedo removido, coloquelo nuevamente";
                if (pbHuella2.Visible) lbInfo.Text = "Lector de huellas conectado, coloque su dedo";
            }));
        }

        public void OnFingerTouch(object Capture, string ReaderSerialNumber)
        {

            this.Invoke(new Functionz(delegate()
            {
                pbHuella2.Visible = false;
                lbInfo.Text = "Capturando huella";
            }));
        }

        public void OnReaderConnect(object Capture, string ReaderSerialNumber)
        {
            this.Invoke(new Functionz(delegate()
            {
                lbInfo.Text = "Lector de huellas conectado, coloque su dedo";
            }));
        }

        public void OnReaderDisconnect(object Capture, string ReaderSerialNumber)
        {

            this.Invoke(new Functionz(delegate()
            {

                lbInfo.Text = "Lector de huellas desconectado"; //MessageBox.Show("Recuento de lecturas: " + readers.Count);
            }));

        }

        public void OnSampleQuality(object Capture, string ReaderSerialNumber, DPFP.Capture.CaptureFeedback CaptureFeedback)
        {
            MessageBox.Show("Muestra de calidad!!!! " + CaptureFeedback.ToString());
        }

        protected DPFP.FeatureSet ExtractFeatures(DPFP.Sample Sample, DPFP.Processing.DataPurpose Purpose)
        {
            DPFP.Processing.FeatureExtraction Extractor = new DPFP.Processing.FeatureExtraction();	// Create a feature extractor
            DPFP.Capture.CaptureFeedback feedback = DPFP.Capture.CaptureFeedback.None;
            DPFP.FeatureSet features = new DPFP.FeatureSet();
            try
            {
                Extractor.CreateFeatureSet(Sample, Purpose, ref feedback, ref features);
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message);
            }
            if (feedback == DPFP.Capture.CaptureFeedback.Good)
                return features;
            else
                return null;
        }

        private void GuardarHuella_FormClosing(object sender, FormClosingEventArgs e)
        {
            finalizarCaptura();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lkGrabarHuella_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmPassword frmPass = new frmPassword();
            if (!frmPass.showPassword()) return;

            finalizarCaptura();
            frmHuella frmGrabarHuella = new frmHuella();
            frmGrabarHuella.ShowDialog();
            frmGrabarHuella.Dispose();
            iniciarCaptura();
        }

        private void lkEstatus_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (status)
            {
                finalizarCaptura();
            }
            else
            {
                iniciarCaptura();
            }
        }

    }
}
