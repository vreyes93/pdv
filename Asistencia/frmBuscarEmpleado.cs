﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Asistencia
{
    public partial class frmBuscarEmpleado : Form
    {
        public frmBuscarEmpleado()
        {
            InitializeComponent();
        }

        private void frmBuscarEmpleado_Load(object sender, EventArgs e)
        {
            this.CancelButton = btnSalir;
        }

        private void frmBuscarEmpleado_Shown(object sender, EventArgs e)
        {
            lbInfo2.Text = "";
            nombreEmpleado.Focus();
        }

        private void nombreEmpleado_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) buscarEmpleado();
        }

        void buscarEmpleado()
        {
            try
            {
                global gl = new global();
                var ws = new wsAsistencia.wsAsistencia();
                //ws.Url = gl.WebServer;

                DataTable dtEmpleados = new DataTable();
                dtEmpleados = ws.DevuelveEmpleados(nombreEmpleado.Text.Trim());

                empleados.DataSource = dtEmpleados;
                
                if (dtEmpleados.Rows.Count == 0)
                {
                    lbInfo2.Text = string.Format("No se encontraron registros con el criterio ingresado.");
                }
                else
                {
                    empleados.Focus();
                    lbInfo2.Text = string.Format("Se encontraron {0} empleados con el criterio ingresado.", dtEmpleados.Rows.Count.ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Se produjo el siguiente error al buscar el empleado.{0}{1}{2}", System.Environment.NewLine, System.Environment.NewLine, ex.Message));
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnBuscarEmpleado_Click(object sender, EventArgs e)
        {
            buscarEmpleado();
        }

        private void empleados_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            seleccionarEmpleado();
        }

        private void empleados_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter) seleccionarEmpleado();
        }

        void seleccionarEmpleado()
        {
            this.DialogResult = DialogResult.OK;
        }

        public bool showBuscarEmpleado()
        {
            if (this.ShowDialog() == DialogResult.OK)
            {
                this.AccessibleDescription = empleados.CurrentRow.Cells["Empleado"].Value.ToString();
                this.AccessibleName = empleados.CurrentRow.Cells["Nombre"].Value.ToString();
                this.Tag = empleados.CurrentRow.Cells["Empresa"].Value.ToString();
                return true;
            }
            else
            {
                return false;
            }
        }

        private void empleados_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            try
            {
                empleados.Columns["Nombre"].Width = 300;
            }
            catch
            {
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void empleados_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

        }

    }
}
