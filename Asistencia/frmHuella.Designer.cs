﻿namespace Asistencia
{
    partial class frmHuella
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHuella));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pbHuella2 = new System.Windows.Forms.PictureBox();
            this.lkEstatus = new System.Windows.Forms.LinkLabel();
            this.lbEstatus = new System.Windows.Forms.Label();
            this.btnBuscarEmpleado = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.nombreEmpleado = new System.Windows.Forms.TextBox();
            this.empleado = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pbHuella = new System.Windows.Forms.PictureBox();
            this.lbInfo = new System.Windows.Forms.Label();
            this.empresa = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHuella2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHuella)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(500, 500);
            this.label1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(472, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "X";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(10, 11);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(188, 52);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(149)))), ((int)(((byte)(90)))));
            this.label3.Location = new System.Drawing.Point(8, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(481, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Grabación de Huella Digital";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pbHuella2);
            this.groupBox1.Controls.Add(this.lkEstatus);
            this.groupBox1.Controls.Add(this.lbEstatus);
            this.groupBox1.Controls.Add(this.btnBuscarEmpleado);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.nombreEmpleado);
            this.groupBox1.Controls.Add(this.empleado);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.pbHuella);
            this.groupBox1.Controls.Add(this.lbInfo);
            this.groupBox1.Controls.Add(this.empresa);
            this.groupBox1.Location = new System.Drawing.Point(27, 103);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(436, 385);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // pbHuella2
            // 
            this.pbHuella2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbHuella2.Location = new System.Drawing.Point(128, 114);
            this.pbHuella2.Name = "pbHuella2";
            this.pbHuella2.Size = new System.Drawing.Size(180, 227);
            this.pbHuella2.TabIndex = 9;
            this.pbHuella2.TabStop = false;
            // 
            // lkEstatus
            // 
            this.lkEstatus.AutoSize = true;
            this.lkEstatus.Location = new System.Drawing.Point(272, 85);
            this.lkEstatus.Name = "lkEstatus";
            this.lkEstatus.Size = new System.Drawing.Size(105, 16);
            this.lkEstatus.TabIndex = 1;
            this.lkEstatus.TabStop = true;
            this.lkEstatus.Text = "Finalizar Captura";
            this.lkEstatus.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lkEstatus_LinkClicked);
            // 
            // lbEstatus
            // 
            this.lbEstatus.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEstatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(149)))), ((int)(((byte)(90)))));
            this.lbEstatus.Location = new System.Drawing.Point(62, 79);
            this.lbEstatus.Name = "lbEstatus";
            this.lbEstatus.Size = new System.Drawing.Size(204, 28);
            this.lbEstatus.TabIndex = 4;
            this.lbEstatus.Text = "Captura de huella iniciada ...";
            this.lbEstatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnBuscarEmpleado
            // 
            this.btnBuscarEmpleado.Location = new System.Drawing.Point(177, 22);
            this.btnBuscarEmpleado.Name = "btnBuscarEmpleado";
            this.btnBuscarEmpleado.Size = new System.Drawing.Size(131, 23);
            this.btnBuscarEmpleado.TabIndex = 0;
            this.btnBuscarEmpleado.Text = "Buscar Empleado";
            this.btnBuscarEmpleado.UseVisualStyleBackColor = true;
            this.btnBuscarEmpleado.Click += new System.EventHandler(this.btnBuscarEmpleado_Click);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(149)))), ((int)(((byte)(90)))));
            this.label5.Location = new System.Drawing.Point(6, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 28);
            this.label5.TabIndex = 7;
            this.label5.Text = "Nombre:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nombreEmpleado
            // 
            this.nombreEmpleado.Enabled = false;
            this.nombreEmpleado.Location = new System.Drawing.Point(91, 51);
            this.nombreEmpleado.Name = "nombreEmpleado";
            this.nombreEmpleado.Size = new System.Drawing.Size(321, 23);
            this.nombreEmpleado.TabIndex = 3;
            // 
            // empleado
            // 
            this.empleado.Enabled = false;
            this.empleado.Location = new System.Drawing.Point(91, 22);
            this.empleado.Name = "empleado";
            this.empleado.Size = new System.Drawing.Size(80, 23);
            this.empleado.TabIndex = 2;
            this.empleado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(149)))), ((int)(((byte)(90)))));
            this.label4.Location = new System.Drawing.Point(6, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 28);
            this.label4.TabIndex = 6;
            this.label4.Text = "Empleado:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pbHuella
            // 
            this.pbHuella.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbHuella.Location = new System.Drawing.Point(128, 114);
            this.pbHuella.Name = "pbHuella";
            this.pbHuella.Size = new System.Drawing.Size(180, 227);
            this.pbHuella.TabIndex = 8;
            this.pbHuella.TabStop = false;
            // 
            // lbInfo
            // 
            this.lbInfo.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(149)))), ((int)(((byte)(90)))));
            this.lbInfo.Location = new System.Drawing.Point(9, 346);
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.Size = new System.Drawing.Size(421, 28);
            this.lbInfo.TabIndex = 5;
            this.lbInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // empresa
            // 
            this.empresa.Enabled = false;
            this.empresa.Location = new System.Drawing.Point(178, 181);
            this.empresa.Name = "empresa";
            this.empresa.ReadOnly = true;
            this.empresa.Size = new System.Drawing.Size(80, 23);
            this.empresa.TabIndex = 0;
            this.empresa.TabStop = false;
            this.empresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.empresa.Visible = false;
            // 
            // frmHuella
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(500, 500);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmHuella";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Grabación de Huella";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmHuella_FormClosing);
            this.Load += new System.EventHandler(this.frmHuella_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHuella2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHuella)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pbHuella;
        private System.Windows.Forms.Label lbInfo;
        private System.Windows.Forms.Button btnBuscarEmpleado;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox nombreEmpleado;
        private System.Windows.Forms.TextBox empleado;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.LinkLabel lkEstatus;
        private System.Windows.Forms.Label lbEstatus;
        private System.Windows.Forms.PictureBox pbHuella2;
        private System.Windows.Forms.TextBox empresa;
    }
}