﻿namespace Asistencia
{
    partial class frmPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPassword));
            this.label1 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lkAceptar = new System.Windows.Forms.LinkLabel();
            this.lkCancelar = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(543, 150);
            this.label1.TabIndex = 40;
            // 
            // txtPassword
            // 
            this.txtPassword.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.txtPassword.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(27, 40);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '•';
            this.txtPassword.Size = new System.Drawing.Size(494, 52);
            this.txtPassword.TabIndex = 0;
            this.txtPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(224, 16);
            this.label2.TabIndex = 30;
            this.label2.Text = "Ingrese la contraseña para continuar:";
            // 
            // lkAceptar
            // 
            this.lkAceptar.AutoSize = true;
            this.lkAceptar.Location = new System.Drawing.Point(184, 110);
            this.lkAceptar.Name = "lkAceptar";
            this.lkAceptar.Size = new System.Drawing.Size(52, 16);
            this.lkAceptar.TabIndex = 10;
            this.lkAceptar.TabStop = true;
            this.lkAceptar.Text = "Aceptar";
            this.lkAceptar.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lkAceptar_LinkClicked);
            // 
            // lkCancelar
            // 
            this.lkCancelar.AutoSize = true;
            this.lkCancelar.Location = new System.Drawing.Point(295, 110);
            this.lkCancelar.Name = "lkCancelar";
            this.lkCancelar.Size = new System.Drawing.Size(58, 16);
            this.lkCancelar.TabIndex = 20;
            this.lkCancelar.TabStop = true;
            this.lkCancelar.Text = "Cancelar";
            this.lkCancelar.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lkCancelar_LinkClicked);
            // 
            // frmPassword
            // 
            this.AcceptButton = this.lkAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.CancelButton = this.lkCancelar;
            this.ClientSize = new System.Drawing.Size(543, 150);
            this.Controls.Add(this.lkCancelar);
            this.Controls.Add(this.lkAceptar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPassword";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Contraseña";
            this.Load += new System.EventHandler(this.frmPassword_Load);
            this.Shown += new System.EventHandler(this.frmPassword_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.LinkLabel lkAceptar;
        private System.Windows.Forms.LinkLabel lkCancelar;
    }
}