﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Asistencia
{
    public partial class frmPassword : Form
    {
        public frmPassword()
        {
            InitializeComponent();
        }

        private void frmPassword_Load(object sender, EventArgs e)
        {

        }

        private void frmPassword_Shown(object sender, EventArgs e)
        {
            txtPassword.Focus();
        }

        private void lkAceptar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string mensaje = "";
            global gl = new global();

            try
            {
                if (txtPassword.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Debe ingresar la contraseña.", "Asistencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                var ws = new wsAsistencia.wsAsistencia();
                //ws.Url = gl.WebServer;

                if (ws.PermiteCambiarHuella(txtPassword.Text.Trim().ToLower(), ref mensaje))
                {
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                }
                else
                {
                    MessageBox.Show(mensaje, "Asistencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al cargar {0} {1}", ex.Message, m), "Asistencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            }

        }
        
        private void lkCancelar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        public bool showPassword()
        {
            try
            {
                if (this.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al cargar {0} {1}", ex.Message, m), "Asistencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

    }
}
