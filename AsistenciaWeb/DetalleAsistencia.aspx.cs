﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace AsistenciaWeb
{
    public partial class DetalleAsistencia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                cargarDetalleEmpleado();
            }
        }

        void cargarDetalleEmpleado()
        {
            string mEmpleado = Request.QueryString["em"];
            string mCodigoEmpresa = Request.QueryString["emp"];
            string mDia1 = Request.QueryString["d1"];
            string mMes1 = Request.QueryString["m1"];
            string mAnio1 = Request.QueryString["a1"];
            string mDia2 = Request.QueryString["d2"];
            string mMes2 = Request.QueryString["m2"];
            string mAnio2 = Request.QueryString["a2"];
            string mNombreEmpleado = ""; string mCentroCosto = ""; string mEmpresa = "prodmult"; string mNombreEmpresa = "Productos Múltiples";
            
            DateTime mFechaInicial = new DateTime(Convert.ToInt32(mAnio1), Convert.ToInt32(mMes1), Convert.ToInt32(mDia1));
            DateTime mFechaFinal = new DateTime(Convert.ToInt32(mAnio2), Convert.ToInt32(mMes2), Convert.ToInt32(mDia2));

            if (Convert.ToInt32(mDia1) < 10) mDia1 = string.Format("0{0}", mDia1);
            if (Convert.ToInt32(mDia2) < 10) mDia2 = string.Format("0{0}", mDia2);
            if (Convert.ToInt32(mMes1) < 10) mMes1 = string.Format("0{0}", mMes1);
            if (Convert.ToInt32(mMes2) < 10) mMes2 = string.Format("0{0}", mMes2);

            if (mCodigoEmpresa == "2")
            {
                mEmpresa = "armados";
                mNombreEmpresa = "Armados Industriales";
            }
            if (mCodigoEmpresa == "3")
            {
                mEmpresa = "crediplus";
                mNombreEmpresa = "Comercial ATID, S.A.";
            }

            string mAmbiente = Request.QueryString["amb"];

            string mServidor = "(local)";
            string mBase = "EXACTUSERP";

            if (mAmbiente == "PRO")
            {
                mBase = "EXACTUSERP";
                mServidor = "sql.fiesta.local";
            }

            if (mAmbiente == "PRU")
            {
                mBase = "PRUEBAS";
                mServidor = "sql.fiesta.local";
            }

            SqlCommand mCommand = new SqlCommand(); SqlDataReader mLector; DataSet dsInfo = new DataSet();
            SqlConnection mConexion = new SqlConnection(string.Format("Data Source = {0}; Initial Catalog = {1};User id = mf.fiestanet;password = 54321", mServidor, mBase));
            
            SqlDataAdapter daAsistencia = new SqlDataAdapter(string.Format("DECLARE @FechaEvaluar AS DATETIME = @FechaInicial BEGIN TRY CREATE TABLE #temp( fecha datetime ) END TRY BEGIN CATCH /****** Nothing ******/ END CATCH DELETE FROM #temp " +
            "WHILE @FechaEvaluar <= @FechaFinal BEGIN INSERT INTO #temp values (@FechaEvaluar) SET @FechaEvaluar = DATEADD(DAY, 1, @FechaEvaluar) CONTINUE END SELECT t.fecha AS Fecha, " +
            "(SELECT TOP 1 COALESCE(b.FECHAHORA, 'No Marcó') FROM prodmult.MF_EmpleadoHuellaAsistencia b WHERE b.EMPLEADO = a.EMPLEADO AND b.FECHA = t.fecha AND b.TIPO = 'E' ORDER BY FECHAHORA) AS Entrada, " +
            "(SELECT TOP 1 COALESCE(b.FECHAHORA, 'No Marcó') FROM prodmult.MF_EmpleadoHuellaAsistencia b WHERE b.EMPLEADO = a.EMPLEADO AND b.FECHA = t.fecha AND b.TIPO = 'S' ORDER BY FECHAHORA DESC) AS Salida, " +
            "a.EMPLEADO AS Empleado, (SELECT b.NOMBRE_PILA FROM {0}.EMPLEADO b WHERE b.EMPLEADO = a.EMPLEADO) AS Nombre, '{1}' AS Empresa, " +
            "(SELECT b.ESTADO_EMPLEADO FROM {0}.EMPLEADO b WHERE b.EMPLEADO = a.EMPLEADO) AS Estado, '' AS Detalle, " +
            "(SELECT TOP 1 COALESCE(b.FECHAHORA, 'No Marcó') FROM prodmult.MF_EmpleadoHuellaAsistencia b WHERE b.EMPLEADO = a.EMPLEADO AND b.FECHA = t.fecha AND b.TIPO = 'E' ORDER BY FECHAHORA) AS HoraEntrada, " +
            "(SELECT TOP 1 COALESCE(b.FECHAHORA, 'No Marcó') FROM prodmult.MF_EmpleadoHuellaAsistencia b WHERE b.EMPLEADO = a.EMPLEADO AND b.FECHA = t.fecha AND b.TIPO = 'S' ORDER BY FECHAHORA DESC) AS HoraSalida " +
            "FROM prodmult.MF_EmpleadoHuella a, #temp t " +
            "WHERE a.EMPRESA = '{0}' AND DATEPART(WEEKDAY, t.Fecha) > (CASE (SELECT b.HORARIO FROM {0}.EMPLEADO b WHERE b.EMPLEADO = a.EMPLEADO) WHEN 'OFIC' THEN 1 ELSE 0 END) AND a.EMPLEADO = '{2}' ORDER BY Fecha, Entrada, Nombre", mEmpresa, mNombreEmpresa, mEmpleado), mConexion);

            daAsistencia.SelectCommand.Parameters.Add("@FechaInicial", SqlDbType.DateTime).Value = mFechaInicial;
            daAsistencia.SelectCommand.Parameters.Add("@FechaFinal", SqlDbType.DateTime).Value = mFechaFinal;

            dsInfo.Tables.Add(new DataTable("detalle"));
            daAsistencia.Fill(dsInfo, "detalle");

            //dsInfo.Tables["detalle"].Columns["Detalle"].MaxLength = 250;
            dsInfo.Tables["detalle"].Columns["Detalle"].ReadOnly = false;
            dsInfo.Tables["detalle"].Columns["Detalle"].DataType = typeof(System.String);

            mConexion.Open();
            mCommand.Connection = mConexion;

            mCommand.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = DateTime.Now.Date;

            mCommand.CommandText = string.Format("SELECT a.NOMBRE_PILA, b.DESCRIPCION FROM {0}.EMPLEADO a JOIN {0}.CENTRO_COSTO b ON a.CENTRO_COSTO = b.CENTRO_COSTO WHERE a.EMPLEADO = '{1}'", mEmpresa, mEmpleado);
            mLector = mCommand.ExecuteReader();

            while(mLector.Read())
            {
                mNombreEmpleado = Convert.ToString(mLector.GetValue(0));
                mCentroCosto = Convert.ToString(mLector.GetValue(1));
            }

            mLector.Close();

            for (int ii = 0; ii < dsInfo.Tables["detalle"].Rows.Count; ii++)
            {
                Int32 mDia; string mSeparador = "";
                DateTime mFecha = Convert.ToDateTime(dsInfo.Tables["detalle"].Rows[ii]["Fecha"]);
                mCommand.Parameters["@Fecha"].Value = mFecha;

                switch (mFecha.DayOfWeek)
                {
                    case DayOfWeek.Saturday:
                        mDia = 0;
                        break;
                    case DayOfWeek.Sunday:
                        mDia = 1;
                        break;
                    case DayOfWeek.Monday:
                        mDia = 2;
                        break;
                    case DayOfWeek.Tuesday:
                        mDia = 3;
                        break;
                    case DayOfWeek.Wednesday:
                        mDia = 4;
                        break;
                    case DayOfWeek.Thursday:
                        mDia = 5;
                        break;
                    default:
                        mDia = 6;
                        break;
                }
                
                mCommand.CommandText = string.Format("SELECT c.HORA_INICIO FROM {0}.EMPLEADO a JOIN {0}.HORARIO b ON a.HORARIO = b.HORARIO JOIN {0}.HORARIO_CONCEPTO c ON b.HORARIO = c.HORARIO WHERE a.EMPLEADO = '{1}' AND c.DIA = {2}", mEmpresa, mEmpleado, mDia);
                DateTime mHoraEntrada = Convert.ToDateTime(mCommand.ExecuteScalar());

                if (Convert.ToString(dsInfo.Tables["detalle"].Rows[ii]["Entrada"]).Trim().Length > 0)
                {
                    DateTime mHoraEntro = Convert.ToDateTime(dsInfo.Tables["detalle"].Rows[ii]["Entrada"]);
                    DateTime mHoraGracia = new DateTime(mHoraEntro.Year, mHoraEntro.Month, mHoraEntro.Day, mHoraEntrada.Hour, mHoraEntrada.Minute + 5, 0);

                    if (mHoraEntro >= mHoraGracia)
                    {
                        mCommand.CommandText = string.Format("SELECT COUNT(*) FROM {0}.EMPLEADO_ACC_PER WHERE EMPLEADO = '{1}' AND @FECHA BETWEEN FECHA_RIGE AND FECHA_VENCE AND TIPO_ACCION = 'TARD' ", mEmpresa, mEmpleado);
                        int mCuantasTarde = Convert.ToInt32(mCommand.ExecuteScalar());

                        if (mCuantasTarde > 0)
                        {
                            mCommand.CommandText = string.Format("SELECT NOTAS FROM {0}.EMPLEADO_ACC_PER WHERE EMPLEADO = '{1}' AND @FECHA BETWEEN FECHA_RIGE AND FECHA_VENCE AND TIPO_ACCION = 'TARD' ", mEmpresa, mEmpleado);
                            string mNotasTarde = mCommand.ExecuteScalar() == null ? "" : Convert.ToString(mCommand.ExecuteScalar());

                            mSeparador = string.Format("{0} y ", mNotasTarde);
                            dsInfo.Tables["detalle"].Rows[ii]["Detalle"] = mNotasTarde;
                        }
                        else
                        {
                            mSeparador = "Entró Tarde y ";
                            dsInfo.Tables["detalle"].Rows[ii]["Detalle"] = "Entró Tarde";
                        }
                    }
                }

                mCommand.CommandText = string.Format("SELECT c.HORA_FINAL FROM {0}.EMPLEADO a JOIN {0}.HORARIO b ON a.HORARIO = b.HORARIO JOIN {0}.HORARIO_CONCEPTO c ON b.HORARIO = c.HORARIO WHERE a.EMPLEADO = '{1}' AND c.DIA = {2}", mEmpresa, mEmpleado, mDia);
                DateTime mHoraSalida = new DateTime(mFecha.Year, mFecha.Month, mFecha.Day, Convert.ToDateTime(mCommand.ExecuteScalar()).Hour, Convert.ToDateTime(mCommand.ExecuteScalar()).Minute, 0);

                if (Convert.ToString(dsInfo.Tables["detalle"].Rows[ii]["Salida"]).Trim().Length > 0)
                {
                    DateTime mHoraSalio = Convert.ToDateTime(dsInfo.Tables["detalle"].Rows[ii]["Salida"]);

                    if (mHoraSalio < mHoraSalida)
                    {
                        mCommand.CommandText = string.Format("SELECT COUNT(*) FROM {0}.EMPLEADO_ACC_PER WHERE EMPLEADO = '{1}' AND @FECHA BETWEEN FECHA_RIGE AND FECHA_VENCE AND TIPO_ACCION = 'ANTS' ", mEmpresa, mEmpleado);
                        int mCuantasTarde = Convert.ToInt32(mCommand.ExecuteScalar());

                        if (mCuantasTarde > 0)
                        {
                            mCommand.CommandText = string.Format("SELECT NOTAS FROM {0}.EMPLEADO_ACC_PER WHERE EMPLEADO = '{1}' AND @FECHA BETWEEN FECHA_RIGE AND FECHA_VENCE AND TIPO_ACCION = 'ANTS' ", mEmpresa, mEmpleado);
                            string mNotasTarde = mCommand.ExecuteScalar() == null ? "" : Convert.ToString(mCommand.ExecuteScalar());

                            mSeparador = string.Format("{0} y ", mNotasTarde);
                            dsInfo.Tables["detalle"].Rows[ii]["Detalle"] = dsInfo.Tables["detalle"].Rows[ii]["Detalle"].ToString().Trim().Length == 0 ? mNotasTarde : string.Format("{0} y {1}", dsInfo.Tables["detalle"].Rows[ii]["Detalle"].ToString(), mNotasTarde);
                        }
                        else
                        {
                            mSeparador = "Salió Antes y ";
                            dsInfo.Tables["detalle"].Rows[ii]["Detalle"] = dsInfo.Tables["detalle"].Rows[ii]["Detalle"].ToString().Trim().Length == 0 ? "Salió Antes" : string.Format("{0} y Salió Antes", dsInfo.Tables["detalle"].Rows[ii]["Detalle"].ToString());
                        }
                    }
                }
  
                dsInfo.Tables["detalle"].Rows[ii]["HoraEntrada"] = new DateTime(mFecha.Year, mFecha.Month, mFecha.Day, mHoraEntrada.Hour, mHoraEntrada.Minute, 0);
                dsInfo.Tables["detalle"].Rows[ii]["HoraSalida"] = new DateTime(mFecha.Year, mFecha.Month, mFecha.Day, mHoraSalida.Hour, mHoraSalida.Minute, 0);

                if (Convert.ToString(dsInfo.Tables["detalle"].Rows[ii]["Entrada"]).Trim().Length == 0) dsInfo.Tables["detalle"].Rows[ii]["Detalle"] = string.Format("{0}No marcó Entrada", mSeparador);
                if (Convert.ToString(dsInfo.Tables["detalle"].Rows[ii]["Salida"]).Trim().Length == 0) dsInfo.Tables["detalle"].Rows[ii]["Detalle"] = string.Format("{0}No marcó Salida", mSeparador);

                if (Convert.ToString(dsInfo.Tables["detalle"].Rows[ii]["Entrada"]).Trim().Length == 0 && Convert.ToString(dsInfo.Tables["detalle"].Rows[ii]["Salida"]).Trim().Length == 0)
                {
                    mCommand.CommandText = string.Format("SELECT COUNT(*) FROM prodmult.DIAS_FERIADOS WHERE DIA = {0} AND MES = {1}", mFecha.Day, mFecha.Month);
                    if (Convert.ToInt32(mCommand.ExecuteScalar()) == 0)
                    {
                        dsInfo.Tables["detalle"].Rows[ii]["Detalle"] = "I n a s i s t e n c i a";
                    }
                    else
                    {
                        mCommand.CommandText = string.Format("SELECT DESCRIPCION FROM prodmult.DIAS_FERIADOS WHERE DIA = {0} AND MES = {1}", mFecha.Day, mFecha.Month);
                        dsInfo.Tables["detalle"].Rows[ii]["Detalle"] = mCommand.ExecuteScalar().ToString();
                    }
                }

                mCommand.CommandText = string.Format("SELECT COUNT(*) FROM {0}.EMPLEADO_ACC_PER WHERE EMPLEADO = '{1}' AND @FECHA BETWEEN FECHA_RIGE AND FECHA_VENCE AND TIPO_ACCION IN ( 'VACS', 'PERC', 'VACA', 'VACG', 'ENFR', 'PERM', 'VAAD', 'VADG', 'INC1', 'INC3', 'INC4', 'MATD', 'DICV', 'DESC', 'SUSP', 'PERS', 'AUSI', 'FUER' ) ", mEmpresa, mEmpleado);
                int mCuantos = Convert.ToInt32(mCommand.ExecuteScalar());

                if (mCuantos > 0)
                {
                    mCommand.CommandText = string.Format("SELECT b.DESCRIPCION FROM {0}.EMPLEADO_ACC_PER a JOIN {0}.TIPO_ACCION b ON a.TIPO_ACCION = b.TIPO_ACCION WHERE EMPLEADO = '{1}' AND @FECHA BETWEEN a.FECHA_RIGE AND a.FECHA_VENCE AND a.TIPO_ACCION IN ( 'VACS', 'PERC', 'VACA', 'VACG', 'ENFR', 'PERM', 'VAAD', 'VADG', 'INC1', 'INC3', 'INC4', 'MATD', 'DICV', 'DESC', 'SUSP', 'PERS', 'AUSI', 'FUER' ) ", mEmpresa, mEmpleado);
                    dsInfo.Tables["detalle"].Rows[ii]["Detalle"] = Convert.ToString(mCommand.ExecuteScalar());

                    mCommand.CommandText = string.Format("SELECT COUNT(*) FROM {0}.EMPLEADO_ACC_PER WHERE EMPLEADO = '{1}' AND @FECHA BETWEEN FECHA_RIGE AND FECHA_VENCE AND TIPO_ACCION = 'FUER' ", mEmpresa, mEmpleado);
                    mCuantos = Convert.ToInt32(mCommand.ExecuteScalar());

                    if (mCuantos > 0)
                    {
                        mCommand.CommandText = string.Format("SELECT NOTAS FROM {0}.EMPLEADO_ACC_PER WHERE EMPLEADO = '{1}' AND @FECHA BETWEEN FECHA_RIGE AND FECHA_VENCE AND TIPO_ACCION = 'FUER' ", mEmpresa, mEmpleado);
                        dsInfo.Tables["detalle"].Rows[ii]["Detalle"] = string.Format("{0} - {1}", dsInfo.Tables["detalle"].Rows[ii]["Detalle"], Convert.ToString(mCommand.ExecuteScalar()));
                    }
                }


            }
            
            mConexion.Close();

            gvEmpleado.DataSource = dsInfo;
            gvEmpleado.DataMember = "detalle";
            gvEmpleado.DataBind();

            for (int ii = 0; ii < gvEmpleado.Rows.Count; ii++)
            {
                Label lbHoraEntrada = (Label)gvEmpleado.Rows[ii].FindControl("lbHoraEntrada");
                Label lbHoraSalida = (Label)gvEmpleado.Rows[ii].FindControl("lbHoraSalida");

                string mDia = "";
                DateTime mHoraEntrada = Convert.ToDateTime(lbHoraEntrada.Text);
                DateTime mHoraSalida = Convert.ToDateTime(lbHoraSalida.Text);
                DateTime mFecha = Convert.ToDateTime(gvEmpleado.Rows[ii].Cells[0].Text);

                if (mFecha.DayOfWeek == DayOfWeek.Monday) mDia = "Lunes";
                if (mFecha.DayOfWeek == DayOfWeek.Tuesday) mDia = "Martes";
                if (mFecha.DayOfWeek == DayOfWeek.Wednesday) mDia = "Miércoles";
                if (mFecha.DayOfWeek == DayOfWeek.Thursday) mDia = "Jueves";
                if (mFecha.DayOfWeek == DayOfWeek.Friday) mDia = "Viernes";
                if (mFecha.DayOfWeek == DayOfWeek.Saturday) mDia = "Sábados";
                if (mFecha.DayOfWeek == DayOfWeek.Sunday) mDia = "Domingos";
  
                string mToolTip = string.Format("{3}{1}Hora de Entrada:  {0}{1}Hora de Salida:  {2}", mHoraEntrada.ToShortTimeString(), Environment.NewLine, mHoraSalida.ToShortTimeString(), mDia);
                  
                gvEmpleado.Rows[ii].Cells[1].ToolTip = mToolTip;
                gvEmpleado.Rows[ii].Cells[2].ToolTip = mToolTip;
            }

            InfoEmpleado.InnerText = string.Format("  Asistencia del Empleado:  {0}  -  {1}  ({2})  -  Del: {3} Al: {4}", mEmpleado, mNombreEmpleado, mCentroCosto, mFechaInicial.ToShortDateString(), mFechaFinal.ToShortDateString());
        }
        
        protected void gvEmpleado_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }

            if (e.Row.Cells[7].Text.Contains("No marc&#243; Entrada") || e.Row.Cells[7].Text.Contains("No marc&#243; Salida") || e.Row.Cells[7].Text.Contains("Sali&#243; Antes"))
            {
                e.Row.ForeColor = System.Drawing.Color.DarkOrange;
                e.Row.Font.Bold = true;
            }
            if (e.Row.Cells[7].Text.Contains("I n a s i s t e n c i a") || e.Row.Cells[7].Text.Contains("Entr&#243; Tarde"))
            {
                e.Row.ForeColor = System.Drawing.Color.Red;
                e.Row.Font.Bold = true;
            }
        }

    }
}