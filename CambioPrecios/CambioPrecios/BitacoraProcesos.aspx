﻿<%@ Page Title="Bitácora de Procesos" Language="C#" MasterPageFile="~/SimpleMaster.Master" AutoEventWireup="true" CodeBehind="BitacoraProcesos.aspx.cs" Inherits="CambioPrecios.BitacoraProcesos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="menu" runat="server">
    <style type="text/css">
        .style1
        {
            border-style: solid;
            border-width: 2px;
        }
        .style2
        {
            text-decoration: underline;
        }
        .style3
        {
            height: 11px;
            text-align: left;
        }
        #divContenido
        {
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="divContenido">
<br />
    <table align="center" class="style1">
        <tr>
            <td colspan="4" style="text-align: center">
                <span class="style2"><strong>BITACORA DE PROCESOS AUTOMATICOS</strong></span>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: left">
                <asp:Label ID="Label8" runat="server" ForeColor="Red" style="text-align: left"></asp:Label>
                </td>
        </tr>
        <tr>
            <td>
                Fecha Inicial:</td>
            <td style="text-align: left">
                <asp:TextBox ID="FechaInicial" runat="server" MaxLength="10"></asp:TextBox>
            </td>
            <td>
                Fecha Final:</td>
            <td style="text-align: left">
                <asp:TextBox ID="FechaFinal" runat="server" MaxLength="10"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style3">
            </td>
            <td class="style3">
                <asp:LinkButton ID="lbCalendarioInicio" runat="server" 
                    onclick="lbCalendarioInicio_Click" TabIndex="110">Abrir Calendario</asp:LinkButton>
            &nbsp;&nbsp;
                <asp:LinkButton ID="lbHoyInicio" runat="server" onclick="lbHoyInicio_Click">Hoy</asp:LinkButton>
            </td>
            <td class="style3">
            </td>
            <td class="style3">
                <asp:LinkButton ID="lbCalendarioFinal" runat="server" 
                    onclick="lbCalendarioFinal_Click" TabIndex="140">Abrir Calendario</asp:LinkButton>
            &nbsp;&nbsp;
                <asp:LinkButton ID="lbHoyFin" runat="server" onclick="lbHoyFin_Click">Hoy</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Calendar ID="CalendarioInicial" runat="server" BackColor="White" 
                    BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" 
                    DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="6pt" 
                    ForeColor="#003399" Height="150px" Visible="False" Width="160px" 
                    onselectionchanged="CalendarioInicial_SelectionChanged" TabIndex="120">
                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                    <OtherMonthDayStyle Font-Bold="False" ForeColor="#999999" />
                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" 
                        Font-Bold="True" Font-Size="8pt" ForeColor="#CCCCFF" Height="22px" />
                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                    <WeekendDayStyle BackColor="#CCCCFF" />
                </asp:Calendar>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Calendar ID="CalendarioFinal" runat="server" BackColor="White" 
                    BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" 
                    DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="6pt" 
                    ForeColor="#003399" Height="150px" Visible="False" Width="160px" 
                    onselectionchanged="CalendarioFinal_SelectionChanged" TabIndex="150">
                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                    <OtherMonthDayStyle Font-Bold="False" ForeColor="#999999" />
                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" 
                        Font-Bold="True" Font-Size="8pt" ForeColor="#CCCCFF" Height="22px" />
                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                    <WeekendDayStyle BackColor="#CCCCFF" />
                </asp:Calendar>
            </td>
        </tr>
        <tr>
            <td style="text-align: left">
                Tipo:</td>
            <td>
                <asp:DropDownList ID="Tipo" runat="server">
                    <asp:ListItem Value="Actualización de Precios">Actualización de Precios</asp:ListItem>
                    <asp:ListItem Value="Inicio de Ofertas">Inicio de Ofertas</asp:ListItem>
                    <asp:ListItem Value="Fin de Ofertas">Fin de Ofertas</asp:ListItem>
                    <asp:ListItem Value="T">Todos</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                Resultado:</td>
            <td style="text-align: left">
                <asp:DropDownList ID="Resultado" runat="server">
                    <asp:ListItem Value="Error">Error</asp:ListItem>
                    <asp:ListItem Value="Exito">Exito</asp:ListItem>
                    <asp:ListItem Value="T">Todos</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center">
                <asp:Button ID="btnBuscar" runat="server" Text="Buscar" 
                    onclick="btnBuscar_Click" />
                <asp:Button ID="btnSalir" runat="server" onclick="btnSalir_Click" 
                    Text="Salir" />
            </td>
        </tr>
    </table>
    <br />
    <table align="center">
        <tr>
            <td>
                <asp:GridView ID="grid" runat="server" CellPadding="4" ForeColor="#333333" 
                    GridLines="None" AllowPaging="True" 
                    onpageindexchanging="grid_PageIndexChanging" onrowdatabound="grid_RowDataBound">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</div>
</asp:Content>
