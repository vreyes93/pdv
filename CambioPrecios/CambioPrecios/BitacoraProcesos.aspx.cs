﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CambioPrecios
{
    public partial class BitacoraProcesos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FechaInicial.Focus();
                ViewState["url"] = string.Format("http://integra/services/wsCambioPrecios.asmx", Request.Url.Host);
            }
        }

        protected void lbCalendarioInicio_Click(object sender, EventArgs e)
        {
            if (CalendarioInicial.Visible)
            {
                CalendarioInicial.Visible = false;
                lbCalendarioInicio.Text = "Abrir Calendario";
            }
            else
            {
                CalendarioInicial.Visible = true;
                lbCalendarioInicio.Text = "Cerrar Calendario";
            }
        }

        protected void lbCalendarioFinal_Click(object sender, EventArgs e)
        {
            if (CalendarioFinal.Visible)
            {
                CalendarioFinal.Visible = false;
                lbCalendarioFinal.Text = "Abrir Calendario";
            }
            else
            {
                CalendarioFinal.Visible = true;
                lbCalendarioFinal.Text = "Cerrar Calendario";
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            DateTime mFechaInicial = DateTime.Now; DateTime mFechaFinal = DateTime.Now;

            if (FechaInicial.Text.Trim().Length > 0)
            {
                FechaInicial.Text = FechaInicial.Text.Replace("-", "/");

                try
                {
                    mFechaInicial = Convert.ToDateTime(FechaInicial.Text);
                }
                catch
                {
                    Label8.Text = "La fecha inicial ingresada es inválida.";
                    FechaInicial.Focus();
                    return;
                }
            }

            if (FechaFinal.Text.Trim().Length > 0)
            {
                FechaFinal.Text = FechaFinal.Text.Replace("-", "/");

                try
                {
                    mFechaFinal = Convert.ToDateTime(FechaFinal.Text);
                }
                catch
                {
                    Label8.Text = "La fecha final ingresada es inválida.";
                    FechaFinal.Focus();
                    return;
                }
            }

            if (FechaInicial.Text.Trim().Length > 0 && FechaFinal.Text.Trim().Length > 0)
            {
                if (mFechaInicial > mFechaFinal)
                {
                    Label8.Text = "La fecha inicial no puede ser mayor a la fecha final.";
                    FechaInicial.Focus();
                    return;
                }
            }

            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);
            var ds = ws.ListarBitacoraProcesosCriterio(FechaInicial.Text, FechaFinal.Text, Tipo.SelectedValue, Resultado.SelectedValue);
            
            ViewState["ds"] = ds;
            grid.DataSource = ds;
            grid.DataBind();
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void CalendarioInicial_SelectionChanged(object sender, EventArgs e)
        {
            CalendarioInicial.Visible = false;
            lbCalendarioInicio.Text = "Abrir Calendario";
            FechaInicial.Text = CalendarioInicial.SelectedDate.ToShortDateString();
        }

        protected void CalendarioFinal_SelectionChanged(object sender, EventArgs e)
        {
            CalendarioFinal.Visible = false;
            lbCalendarioFinal.Text = "Abrir Calendario";
            FechaFinal.Text = CalendarioFinal.SelectedDate.ToShortDateString();
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }

            foreach (TableCell cel in e.Row.Cells)
            {
                cel.Attributes.Add("style", "text-align: left");
            }
        }

        protected void grid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            var ds = ViewState["ds"];

            grid.DataSource = ds;
            grid.PageIndex = e.NewPageIndex;
            grid.DataBind();
        }

        protected void lbHoyInicio_Click(object sender, EventArgs e)
        {
            FechaInicial.Text = DateTime.Now.ToShortDateString();
        }

        protected void lbHoyFin_Click(object sender, EventArgs e)
        {
            FechaFinal.Text = DateTime.Now.ToShortDateString();
        }

    }
}