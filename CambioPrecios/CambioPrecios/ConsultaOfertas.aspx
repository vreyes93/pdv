﻿<%@ Page Title="Consulta General de Ofertas" Language="C#" MasterPageFile="~/SimpleMaster.Master" AutoEventWireup="true" CodeBehind="ConsultaOfertas.aspx.cs" Inherits="CambioPrecios.ConsultaOfertas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="menu" runat="server">
    <style type="text/css">
        .style1
        {
            border-style: solid;
            border-width: 2px;
        }
        .style2
        {
            border-style: solid;
            border-width: 2px;
        }
        .style3
        {
            text-align: center;
            text-decoration: underline;
        }
        .style4
        {
            text-align: center;
        }
        </style>    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="divContenido">
<br />
    <table align="center" class="style1">
        <tr>
            <td class="style3" colspan="4">
                <strong>CONSULTA GENERAL DE OFERTAS</strong></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="Label8" runat="server" ForeColor="Red"></asp:Label>
                </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Fecha Inicial:</td>
            <td>
                <asp:TextBox ID="FechaInicial" runat="server" MaxLength="10"></asp:TextBox>
            </td>
            <td>
                Fecha Final:</td>
            <td>
                <asp:TextBox ID="FechaFinal" runat="server" MaxLength="10"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:LinkButton ID="lbCalendarioInicio" runat="server" 
                    onclick="lbCalendarioInicio_Click" TabIndex="110">Abrir Calendario</asp:LinkButton>
            </td>
            <td>
            </td>
            <td>
                <asp:LinkButton ID="lbCalendarioFinal" runat="server" 
                    onclick="lbCalendarioFinal_Click" TabIndex="140">Abrir Calendario</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Calendar ID="CalendarioInicial" runat="server" BackColor="White" 
                    BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" 
                    DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="6pt" 
                    ForeColor="#003399" Height="150px" Visible="False" Width="160px" 
                    onselectionchanged="CalendarioInicial_SelectionChanged" TabIndex="120">
                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                    <OtherMonthDayStyle Font-Bold="False" ForeColor="#999999" />
                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" 
                        Font-Bold="True" Font-Size="8pt" ForeColor="#CCCCFF" Height="22px" />
                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                    <WeekendDayStyle BackColor="#CCCCFF" />
                </asp:Calendar>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Calendar ID="CalendarioFinal" runat="server" BackColor="White" 
                    BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" 
                    DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="6pt" 
                    ForeColor="#003399" Height="150px" Visible="False" Width="160px" 
                    onselectionchanged="CalendarioFinal_SelectionChanged" TabIndex="150">
                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                    <OtherMonthDayStyle Font-Bold="False" ForeColor="#999999" />
                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" 
                        Font-Bold="True" Font-Size="8pt" ForeColor="#CCCCFF" Height="22px" />
                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                    <WeekendDayStyle BackColor="#CCCCFF" />
                </asp:Calendar>
            </td>
        </tr>
        <tr>
            <td>
                Tipo:</td>
            <td>
                <asp:DropDownList ID="Tipo" runat="server">
                    <asp:ListItem Value="A">Ofertas por Artículo</asp:ListItem>
                    <asp:ListItem Value="N">Ofertas por Nivel de Precio</asp:ListItem>
                    <asp:ListItem Value="B">Ofertas por Artículo + Nivel de Precio</asp:ListItem>
                    <asp:ListItem Value="T">Todas</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                Estatus:</td>
            <td>
                <asp:DropDownList ID="Estatus" runat="server">
                    <asp:ListItem Value="P">Pendientes</asp:ListItem>
                    <asp:ListItem Value="V">Vigentes</asp:ListItem>
                    <asp:ListItem Value="C">Canceladas</asp:ListItem>
                    <asp:ListItem Value="F">Finalizadas</asp:ListItem>
                    <asp:ListItem Value="T">Todas</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Artículo:</td>
            <td>
                <asp:TextBox ID="articulo" runat="server" AutoPostBack="True" 
                    ontextchanged="articulo_TextChanged"></asp:TextBox>
            </td>
            <td>
                Nivel de Precio:</td>
            <td>
                <asp:TextBox ID="nivel" runat="server" AutoPostBack="True" 
                    ontextchanged="nivel_TextChanged" TabIndex="7"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:LinkButton ID="lbBuscarArticulo" runat="server" 
                    onclick="lbBuscarArticulo_Click" TabIndex="10">Buscar Artículos</asp:LinkButton>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:LinkButton ID="lbBuscarNivel" runat="server" 
                    onclick="lbBuscarNivel_Click" TabIndex="15">Buscar Nivel</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <table id="tablaBusqueda" runat="server" align="center" class="style2">
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2">
                <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                <asp:Label ID="Label5" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                Artículo:</td>
            <td>
                <asp:TextBox ID="codigoArticulo" runat="server" TabIndex="20"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                Nombre:</td>
            <td>
                <asp:TextBox ID="descripcionArticulo" runat="server" TabIndex="30"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                <asp:Button ID="btnBuscar" runat="server" Text="Buscar" 
                    ToolTip="Busca artículos según el criterio de búsqueda ingresado" 
                    onclick="btnBuscar_Click" TabIndex="40" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td class="style4" colspan="2">
                <asp:GridView ID="gridArticulos" runat="server" CellPadding="4" 
                    ForeColor="#333333" GridLines="None" 
                    onselectedindexchanged="gridArticulos_SelectedIndexChanged" TabIndex="50">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                                    CommandName="Select" 
                                    Text="Seleccionar"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                <asp:LinkButton ID="lbCerrarBusqueda" runat="server" 
                    ToolTip="Cierra la ventana de búsqueda" onclick="lbCerrarBusqueda_Click" 
                    TabIndex="60">Cerrar Búsqueda</asp:LinkButton>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>

                </td>
            <td>
                &nbsp;</td>
            <td>
                <table id="tablaBusquedaNivel" runat="server" align="center" class="style2">
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2">
                <asp:Label ID="Label6" runat="server" ForeColor="Red"></asp:Label>
                <asp:Label ID="Label7" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                Nivel a Buscar:</td>
            <td>
                <asp:TextBox ID="codigoNivel" runat="server" TabIndex="62"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                Descripción:</td>
            <td>
                <asp:TextBox ID="descripcionNivel" runat="server" TabIndex="64"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                <asp:Button ID="btnBuscarNivel" runat="server" Text="Buscar" 
                    ToolTip="Busca niveles según el criterio de búsqueda ingresado" 
                    onclick="btnBuscarNivel_Click" TabIndex="66" />
                <asp:Button ID="btnNivelesTodos" runat="server" onclick="btnNivelesTodos_Click" 
                    Text="Listar todos los niveles" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td class="style4" colspan="2">
                <asp:GridView ID="gridNiveles" runat="server" CellPadding="4" 
                    ForeColor="#333333" GridLines="None" 
                    onselectedindexchanged="gridNiveles_SelectedIndexChanged" TabIndex="68">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:CommandField SelectText="Seleccionar" ShowSelectButton="True" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                <asp:LinkButton ID="lbCerrarBusquedaNivel" runat="server" 
                    ToolTip="Cierra la ventana de búsqueda" onclick="lbCerrarBusquedaNivel_Click" 
                    TabIndex="69">Cerrar Búsqueda</asp:LinkButton>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>

                </td>
        </tr>
        <tr>
            <td>
                Nombre Artículo:</td>
            <td colspan="3">
                <asp:TextBox ID="DescripcionArticuloBuscar" runat="server" Width="526px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Descripción Oferta:</td>
            <td colspan="3">
                <asp:TextBox ID="Descripcion" runat="server" Width="526px" MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbMotivoCancelacion" runat="server" Text="Motivo Cancelación:" 
                    Visible="False"></asp:Label>
            </td>
            <td colspan="3">
                <asp:TextBox ID="MotivoCancelacion" runat="server" width="534px" 
                    Visible="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center">
                <asp:Button ID="btnBuscarOfertas" runat="server" 
                    onclick="btnBuscarOfertas_Click" Text="Buscar Ofertas" 
                    ToolTip="Busca Ofertas con los criterios de búsqueda definidos" />
                <asp:Button ID="btnTodas" runat="server" onclick="btnTodas_Click" 
                    Text="Listar Todas" 
                    ToolTip="Lista todas las ofertas existentes en el sistema" />
                <asp:Button ID="btnSalir" runat="server" onclick="btnSalir_Click" 
                    Text="Salir" />
            </td>
        </tr>
    </table>
                <asp:Label ID="Label9" runat="server" ForeColor="Red"></asp:Label>
                <asp:Label ID="Label10" runat="server"></asp:Label>
    <br />
    <table align="center">
        <tr>
            <td>
            <div id="divGrid" runat="server" style="overflow:auto; width:945px; height:510px" >
                <asp:GridView ID="grid" runat="server" CellPadding="4" ForeColor="#333333" 
                    GridLines="None" AllowPaging="True" 
                    onpageindexchanging="grid_PageIndexChanging" AutoGenerateColumns="False" 
                    onrowdatabound="grid_RowDataBound" 
                    onselectedindexchanged="grid_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField ShowHeader="False" Visible="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbCancelarOferta" runat="server" CausesValidation="False" 
                                    CommandName="Select" Text="Cancelar" 
                                    ToolTip="Cancela la Oferta Seleccionada" onclick="lbCancelarOferta_Click" 
                                    onclientclick="javascript:return confirm(&quot;Desea cancelar la oferta?&quot;)"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tipo">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Tipo") %>' Width="110px"></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Tipo") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Artículo">
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("Articulo") %>' 
                                    Width="80px"></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Articulo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nombre del Artículo">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("NombreArticulo") %>' 
                                    Width="175px"></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("NombreArticulo") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="NivelPrecio" HeaderText="Nivel de Precio" />
                        <asp:BoundField DataField="FechaInicial" HeaderText="Fecha Inicial" 
                            DataFormatString="{0:d}" />
                        <asp:BoundField DataField="FechaFinal" HeaderText="Fecha Final" 
                            DataFormatString="{0:d}" />
                        <asp:TemplateField HeaderText="Precio Original">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("PrecioOriginal") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" style="text-align: right" 
                                    Text='<%# Bind("PrecioOriginal") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Precio Oferta">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("PrecioOferta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Text='<%# Bind("PrecioOferta") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" />
                        <asp:TemplateField HeaderText="Descripción de la Oferta">
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("DescripcionOferta") %>' 
                                    Width="160px"></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox4" runat="server" 
                                    Text='<%# Bind("DescripcionOferta") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="UsuarioCreo" HeaderText="Usuario Creó" />
                        <asp:TemplateField HeaderText="Fecha Creada">
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Bind("FechaCreada") %>' 
                                    Width="155px"></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("FechaCreada") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="UsuarioCancela" HeaderText="Usuario Cancela" />
                        <asp:TemplateField HeaderText="Fecha Cancela">
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text='<%# Bind("FechaCancela") %>' 
                                    Width="155px"></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("FechaCancela") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Motivo Cancela">
                            <ItemTemplate>
                                <asp:Label ID="Label9" runat="server" Text='<%# Bind("MotivoCancela") %>' 
                                    Width="220px"></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("MotivoCancela") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </div>
            </td>
        </tr>
    </table>
</div>
</asp:Content>
