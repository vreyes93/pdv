﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CambioPrecios
{
    public partial class ConsultaOfertas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FechaInicial.Focus();
                tablaBusqueda.Visible = false;
                tablaBusquedaNivel.Visible = false;
                ViewState["url"] = string.Format("http://integra/services/wsCambioPrecios.asmx", Request.Url.Host);
            }
        }
  
        protected void lbCalendarioInicio_Click(object sender, EventArgs e)
        {
            if (CalendarioInicial.Visible)
            {
                CalendarioInicial.Visible = false;
                lbCalendarioInicio.Text = "Abrir Calendario";
            }
            else
            {
                CalendarioInicial.Visible = true;
                lbCalendarioInicio.Text = "Cerrar Calendario";
            }
        }

        protected void lbCalendarioFinal_Click(object sender, EventArgs e)
        {
            if (CalendarioFinal.Visible)
            {
                CalendarioFinal.Visible = false;
                lbCalendarioFinal.Text = "Abrir Calendario";
            }
            else
            {
                CalendarioFinal.Visible = true;
                lbCalendarioFinal.Text = "Cerrar Calendario";
            }
        }

        protected void CalendarioInicial_SelectionChanged(object sender, EventArgs e)
        {
            CalendarioInicial.Visible = false;
            lbCalendarioInicio.Text = "Abrir Calendario";
            FechaInicial.Text = CalendarioInicial.SelectedDate.ToShortDateString();
        }

        protected void CalendarioFinal_SelectionChanged(object sender, EventArgs e)
        {
            CalendarioFinal.Visible = false;
            lbCalendarioFinal.Text = "Abrir Calendario";
            FechaFinal.Text = CalendarioFinal.SelectedDate.ToShortDateString();
        }

        protected void lbBuscarArticulo_Click(object sender, EventArgs e)
        {
            Label1.Text = "";
            Label5.Text = "";
            codigoArticulo.Text = "";
            descripcionArticulo.Text = "";
            tablaBusqueda.Visible = true;
            descripcionArticulo.Focus();
        }

        protected void lbBuscarNivel_Click(object sender, EventArgs e)
        {
            Label6.Text = "";
            Label7.Text = "";
            codigoNivel.Text = "";
            descripcionNivel.Text = "";
            tablaBusquedaNivel.Visible = true;
            codigoNivel.Focus();
        }

        protected void gridArticulos_SelectedIndexChanged(object sender, EventArgs e)
        {
            articulo.Text = gridArticulos.SelectedRow.Cells[1].Text;

            Label1.Text = "";
            Label5.Text = "";

            nivel.Focus();
            tablaBusqueda.Visible = false;
        }

        protected void articulo_TextChanged(object sender, EventArgs e)
        {
            if (articulo.Text.Trim().Length == 0)
            {
                Label1.Text = "";
                Label5.Text = "";
                return;
            }

            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarArticulosCriterio("C", articulo.Text, "");
            var articulos = q.ToArray();

            gridArticulos.DataSource = articulos;
            gridArticulos.DataBind();

            if (gridArticulos.Rows.Count == 0)
            {
                articulo.Text = "";
                Label1.Text = "El código de artículo ingresado no existe.";
                articulo.Focus();
                return;
            }

            Label1.Text = "";
            Label5.Text = "";

            nivel.Focus();
            tablaBusqueda.Visible = false;
        }

        protected void nivel_TextChanged(object sender, EventArgs e)
        {
            if (nivel.Text.Trim().Length == 0)
            {
                Label6.Text = "";
                Label7.Text = "";
                return;
            }

            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarCoeficientesCriterio("N", nivel.Text, "");
            var nivels = q.ToArray();

            gridNiveles.DataSource = nivels;
            gridNiveles.DataBind();

            if (gridNiveles.Rows.Count == 0)
            {
                nivel.Text = "";
                Label6.Text = "El código de nivel ingresado no existe.";
                nivel.Focus();
                return;
            }

            Label6.Text = "";
            Label7.Text = "";

            tablaBusquedaNivel.Visible = false;
        }

        protected void btnBuscarOfertas_Click(object sender, EventArgs e)
        {
            DateTime mFechaInicial = DateTime.Now; DateTime mFechaFinal = DateTime.Now;
            string mArticulo, mNivelPrecio, mNombreArticulo, mDescripcionOferta;

            if (FechaInicial.Text.Trim().Length > 0)
            {
                FechaInicial.Text = FechaInicial.Text.Replace("-", "/");

                try
                {
                    mFechaInicial = Convert.ToDateTime(FechaInicial.Text);
                }
                catch
                {
                    Label8.Text = "La fecha inicial ingresada es inválida.";
                    FechaInicial.Focus();
                    return;
                }
            }

            if (FechaFinal.Text.Trim().Length > 0)
            {
                FechaFinal.Text = FechaFinal.Text.Replace("-", "/");

                try
                {
                    mFechaFinal = Convert.ToDateTime(FechaFinal.Text);
                }
                catch
                {
                    Label8.Text = "La fecha final ingresada es inválida.";
                    FechaFinal.Focus();
                    return;
                }
            }

            if (FechaInicial.Text.Trim().Length > 0 && FechaFinal.Text.Trim().Length > 0)
            {
                if (mFechaInicial > mFechaFinal)
                {
                    Label8.Text = "La fecha inicial no puede ser mayor a la fecha final.";
                    FechaInicial.Focus();
                    return;
                }
            }

            mArticulo = "%";
            if (articulo.Text.Trim().Length > 0) mArticulo = articulo.Text;

            mNivelPrecio = "%";
            if (nivel.Text.Trim().Length > 0) mNivelPrecio = nivel.Text;

            mNombreArticulo = "%";
            if (DescripcionArticuloBuscar.Text.Trim().Length > 0) mNombreArticulo = DescripcionArticuloBuscar.Text;

            mDescripcionOferta = "%";
            if (Descripcion.Text.Trim().Length > 0) mDescripcionOferta = Descripcion.Text;


            Label8.Text = "";

            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var ds = ws.ListarOfertas(FechaInicial.Text, FechaFinal.Text, Tipo.SelectedValue, Estatus.SelectedValue, mArticulo, mNivelPrecio, mNombreArticulo, mDescripcionOferta);

            ViewState["ds"] = ds;
            grid.DataSource = ds;
            grid.DataBind();

            PuedeCancelar();
        }

        protected void btnTodas_Click(object sender, EventArgs e)
        {
            Label8.Text = "";

            FechaInicial.Text = "";
            FechaFinal.Text = "";
            Tipo.SelectedValue = "T";
            Estatus.SelectedValue = "T";
            articulo.Text = "";
            nivel.Text = "";
            DescripcionArticuloBuscar.Text = "";
            Descripcion.Text = "";

            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var ds = ws.ListarOfertas("", "", "T", "T", "%", "%", "%", "%");

            ViewState["ds"] = ds;
            grid.DataSource = ds;
            grid.DataBind();

            PuedeCancelar();
        }

        void PuedeCancelar()
        {
            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            if (ws.PuedeCancelarOfertas(System.Environment.UserName))
            {
                grid.Columns[0].Visible = true;
                MotivoCancelacion.Visible = true;
                lbMotivoCancelacion.Visible = true;
                Label10.Text = "Para cancelar una oferta, escriba la razón en 'Motivo Cancelación' y luego haga clic en el link 'Cancelar' de la oferta en el listado.";
            }
            else
            {
                Label10.Text = "";
                grid.Columns[0].Visible = false;
                MotivoCancelacion.Visible = false;
                lbMotivoCancelacion.Visible = false;
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            string mTipo = "";

            Label1.Text = "";
            Label5.Text = "";

            if (codigoArticulo.Text.Trim().Length == 0 && descripcionArticulo.Text.Trim().Length == 0)
            {
                Label1.Text = "Debe ingresar un criterio de búsqueda ya sea el código, la descripción o ambos.";
                codigoArticulo.Focus();
                return;
            }

            if (codigoArticulo.Text.Trim().Length > 0) mTipo = "C";
            if (descripcionArticulo.Text.Trim().Length > 0) mTipo = "D";
            if (codigoArticulo.Text.Trim().Length > 0 && descripcionArticulo.Text.Trim().Length > 0) mTipo = "A";

            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarArticulosCriterio(mTipo, codigoArticulo.Text, descripcionArticulo.Text);
            var articulos = q.ToArray();

            gridArticulos.DataSource = articulos;
            gridArticulos.DataBind();

            descripcionArticulo.Focus();
            if (q.Length == 0)
            {
                Label1.Text = "No se encontraron artículos con el criterio de búsqueda ingresado.";
            }
            else
            {
                Label5.Text = string.Format("Se encontraron {0} artículos con el criterio de búsqueda ingresado.", q.Length);
            }
        }

        protected void btnBuscarNivel_Click(object sender, EventArgs e)
        {
            string mTipo = "";

            Label6.Text = "";
            Label7.Text = "";

            if (codigoNivel.Text.Trim().Length == 0 && descripcionNivel.Text.Trim().Length == 0)
            {
                Label6.Text = "Debe ingresar un criterio de búsqueda ya sea el código, la descripción o ambos.";
                codigoNivel.Focus();
                return;
            }

            if (codigoNivel.Text.Trim().Length > 0) mTipo = "C";
            if (descripcionNivel.Text.Trim().Length > 0) mTipo = "D";
            if (codigoNivel.Text.Trim().Length > 0 && descripcionNivel.Text.Trim().Length > 0) mTipo = "A";

            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarCoeficientesCriterio(mTipo, codigoNivel.Text, descripcionNivel.Text);
            var niveles = q.ToArray();

            gridNiveles.DataSource = niveles;
            gridNiveles.DataBind();

            codigoNivel.Focus();
            if (q.Length == 0)
            {
                Label6.Text = "No se encontraron niveles con el criterio de búsqueda ingresado.";
            }
            else
            {
                Label7.Text = string.Format("Se encontraron {0} niveles con el criterio de búsqueda ingresado.", q.Length);
            }
        }

        protected void btnNivelesTodos_Click(object sender, EventArgs e)
        {
            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarCoeficientes();
            var coeficientes = q.ToArray();

            gridNiveles.DataSource = coeficientes;
            gridNiveles.DataBind();
        }

        protected void lbCerrarBusqueda_Click(object sender, EventArgs e)
        {
            tablaBusqueda.Visible = false;
        }

        protected void lbCerrarBusquedaNivel_Click(object sender, EventArgs e)
        {
            tablaBusquedaNivel.Visible = false;
        }

        protected void gridNiveles_SelectedIndexChanged(object sender, EventArgs e)
        {
            nivel.Text = gridNiveles.SelectedRow.Cells[1].Text;

            Label6.Text = "";
            Label7.Text = "";

            tablaBusquedaNivel.Visible = false;
        }

        protected void grid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            var ds = ViewState["ds"];

            grid.DataSource = ds;
            grid.PageIndex = e.NewPageIndex;
            grid.DataBind();
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }

            foreach (TableCell cel in e.Row.Cells)
            {
                cel.Attributes.Add("style", "text-align: left");
            }
        }

        protected void lbCancelarOferta_Click(object sender, EventArgs e)
        {
            ViewState["Boton"] = "CancelarOferta";
        }

        protected void grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "CancelarOferta":
                    if (grid.SelectedRow.Cells[9].Text == "Vigente" || grid.SelectedRow.Cells[9].Text == "Pendiente")
                    {
                        if (MotivoCancelacion.Text.Trim().Length == 0)
                        {
                            MotivoCancelacion.Focus();
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('Debe ingresar el motivo de la cancelación de la oferta.');", true);
                            return;
                        }
                        if (MotivoCancelacion.Text.Trim().Length <= 20)
                        {
                            MotivoCancelacion.Focus();
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('Debe ser más explícito al ingresar el motivo de la cancelación de la oferta.');", true);
                            return;
                        }

                        string mMensaje = "";
                        var ws = new wsCambioPrecios.wsCambioPrecios();
                        if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                        Label _Tipo = ((Label)grid.SelectedRow.FindControl("Label1"));
                        Label _Articulo = ((Label)grid.SelectedRow.FindControl("Label2"));

                        if (ws.CancelaOfertas(_Tipo.Text, _Articulo.Text, grid.SelectedRow.Cells[4].Text, Convert.ToDateTime(grid.SelectedRow.Cells[5].Text), MotivoCancelacion.Text, System.Environment.UserName, ref mMensaje))
                        {
                            Label9.Text = "";
                            Label10.Text = mMensaje;
                            MotivoCancelacion.Text = "";
                        }
                        else
                        {
                            Label10.Text = "";
                            Label9.Text = mMensaje;
                        }
                    }
                    else
                    {
                        Label10.Text = "";
                        Label9.Text = string.Format("La Oferta seleccionada no se puede cancelar ya que se encuentra {0}.", grid.SelectedRow.Cells[9].Text);
                    }

                    break;
            }
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

    }
}