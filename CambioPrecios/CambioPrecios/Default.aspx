﻿<%@ Page
    Language="C#"
    AutoEventWireup="true"
    CodeBehind="Default.aspx.cs"
    MasterPageFile="~/SimpleMaster.master"
    Inherits="FiestaNET._Default" %>

<asp:Content
    ID="Content1"
    ContentPlaceHolderID="content"
    runat="server">
    <h1>
        Menú
        Principal</h1>
    <asp:SiteMapDataSource
        ID="srcSiteMap"
        ShowStartingNode="False"
        runat="server" />
    <fieldset>
        <asp:TreeView
            ID="TreeView1"
            DataSourceID="srcSiteMap"
            ImageSet="XPFileExplorer"
            NodeIndent="50"
            ShowLines="true"
            runat="server">
        </asp:TreeView>
    </fieldset>
</asp:Content>
