﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CambioPrecios
{
    public partial class OfertaArticulo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Label1.Text = "";
                Label2.Text = "";
                Label5.Text = "";
                articulo.Focus();
                tablaBusqueda.Visible = false;
                ViewState["url"] = string.Format("http://integra/services/wsCambioPrecios.asmx", Request.Url.Host);


            }
        }
        protected void lbBuscarArticulo_Click(object sender, EventArgs e)
        {
            Label1.Text = "";
            Label5.Text = "";
            codigoArticulo.Text = "";
            descripcionArticulo.Text = "";
            tablaBusqueda.Visible = true;
            descripcionArticulo.Focus();
        }

        protected void lbCerrarBusqueda_Click(object sender, EventArgs e)
        {
            tablaBusqueda.Visible = false;
        }

        protected void lbCalendarioInicio_Click(object sender, EventArgs e)
        {
            if (CalendarioInicial.Visible)
            {
                CalendarioInicial.Visible = false;
                lbCalendarioInicio.Text = "Abrir Calendario";
            }
            else
            {
                CalendarioInicial.Visible = true;
                lbCalendarioInicio.Text = "Cerrar Calendario";
            }
        }

        protected void lbCalendarioFinal_Click(object sender, EventArgs e)
        {
            if (CalendarioFinal.Visible)
            {
                CalendarioFinal.Visible = false;
                lbCalendarioFinal.Text = "Abrir Calendario";
            }
            else
            {
                CalendarioFinal.Visible = true;
                lbCalendarioFinal.Text = "Cerrar Calendario";
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            string mTipo = "";

            Label1.Text = "";
            Label5.Text = "";

            if (codigoArticulo.Text.Trim().Length == 0 && descripcionArticulo.Text.Trim().Length == 0) 
            {
                Label1.Text = "Debe ingresar un criterio de búsqueda ya sea el código, la descripción o ambos.";
                codigoArticulo.Focus();
                return;
            }

            if (codigoArticulo.Text.Trim().Length > 0) mTipo = "C";
            if (descripcionArticulo.Text.Trim().Length > 0) mTipo = "D";
            if (codigoArticulo.Text.Trim().Length > 0 && descripcionArticulo.Text.Trim().Length > 0) mTipo = "A";

            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarArticulosCriterio(mTipo, codigoArticulo.Text, descripcionArticulo.Text);
            var articulos = q.ToArray();

            gridArticulos.DataSource = articulos;
            gridArticulos.DataBind();

            descripcionArticulo.Focus();
            if (q.Length == 0)
            {
                Label1.Text = "No se encontraron artículos con el criterio de búsqueda ingresado.";
            }
            else
            {
                Label5.Text = string.Format("Se encontraron {0} artículos con el criterio de búsqueda ingresado.", q.Length);
            }
        }

        protected void gridArticulos_SelectedIndexChanged(object sender, EventArgs e)
        {
            articulo.Text = gridArticulos.SelectedRow.Cells[1].Text;
            descripcion.Text = gridArticulos.SelectedRow.Cells[2].Text;
            PrecioOriginal.Text = String.Format("{0:0,0.000000}", gridArticulos.SelectedRow.Cells[3].Text);
            PrecioOriginalConIVA.Text = String.Format("{0:0,0.00}", (Convert.ToDouble(gridArticulos.SelectedRow.Cells[3].Text) * 1.12));

            if (descripcion.Text == "&nbsp;") descripcion.Text = "";

            descripcion.Text = descripcion.Text.Replace("&#39;", "'");
            descripcion.Text = descripcion.Text.Replace("&#225;", "á");
            descripcion.Text = descripcion.Text.Replace("&#233;", "é");
            descripcion.Text = descripcion.Text.Replace("&#237;", "í");
            descripcion.Text = descripcion.Text.Replace("&#243;", "ó");
            descripcion.Text = descripcion.Text.Replace("&#250;", "ú");

            Label1.Text = "";
            Label2.Text = "";
            Label5.Text = "";
              
            PrecioOferta.Focus();
            tablaBusqueda.Visible = false;
        }

        protected void articulo_TextChanged(object sender, EventArgs e)
        {
            if (articulo.Text.Trim().Length == 0)
            {
                Label1.Text = "";
                Label5.Text = "";
                descripcion.Text = "";
                PrecioOriginal.Text = "";
                PrecioOriginalConIVA.Text = "";
                return;
            }

            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarArticulosCriterio("C", articulo.Text, "");
            var articulos = q.ToArray();

            gridArticulos.DataSource = articulos;
            gridArticulos.DataBind();

            if (gridArticulos.Rows.Count == 0)
            {
                articulo.Text = "";
                descripcion.Text = "";
                PrecioOriginal.Text = "";
                PrecioOriginalConIVA.Text = "";
                PrecioOferta.Text = "";
                PrecioOfertaConIVA.Text = "";
                Label1.Text = "El código de artículo ingresado no existe.";
                articulo.Focus();
                return;
            }

            descripcion.Text = gridArticulos.Rows[0].Cells[2].Text;
            PrecioOriginal.Text = String.Format("{0:0,0.00000000}", gridArticulos.Rows[0].Cells[3].Text);
            PrecioOriginalConIVA.Text = String.Format("{0:0,0.00}", (Convert.ToDouble(gridArticulos.Rows[0].Cells[3].Text) * 1.12));

            PrecioOferta.Text = "";
            PrecioOfertaConIVA.Text = "";

            Label1.Text = "";
            Label2.Text = "";
            Label5.Text = "";

            PrecioOferta.Focus();
            tablaBusqueda.Visible = false;
        }

        protected void CalendarioInicial_SelectionChanged(object sender, EventArgs e)
        {
            CalendarioInicial.Visible = false;
            lbCalendarioInicio.Text = "Abrir Calendario";
            FechaInicial.Text = CalendarioInicial.SelectedDate.ToShortDateString();
        }

        protected void CalendarioFinal_SelectionChanged(object sender, EventArgs e)
        {
            CalendarioFinal.Visible = false;
            lbCalendarioFinal.Text = "Abrir Calendario";
            FechaFinal.Text = CalendarioFinal.SelectedDate.ToShortDateString();
        }

        protected void btnGrabar_Click(object sender, EventArgs e)
        {
            decimal mPrecioOriginal, mPrecioOferta; DateTime mFechaInicial, mFechaFinal;

            if (articulo.Text.Trim().Length == 0)
            {
                Label4.Text = "";
                Label3.Text = "Debe ingresar un artículo a ofertar.";
                articulo.Focus();
                return;
            }

            mPrecioOriginal = Convert.ToDecimal(PrecioOriginal.Text);

            if (PrecioOferta.Text.Trim().Length == 0)
            {
                Label4.Text = "";
                Label3.Text = "Debe ingresar el precio a ofertar.";
                PrecioOferta.Focus();
                return;
            }

            try
            {
                mPrecioOferta = Convert.ToDecimal(PrecioOferta.Text);
            }
            catch
            {
                Label4.Text = "";
                Label3.Text = "El precio de oferta ingresado es inválido.";
                PrecioOferta.Focus();
                return;
            }

            if (mPrecioOferta <= 0)
            {
                Label4.Text = "";
                Label3.Text = "El precio de oferta ingresado es inválido.";
                PrecioOferta.Focus();
                return;
            }

            if (mPrecioOriginal == mPrecioOferta)
            {
                Label4.Text = "";
                Label3.Text = "El precio de la oferta es igual al precio original.";
                PrecioOferta.Focus();
                return;
            }

            if (descripcionOferta.Text.Trim().Length == 0)
            {
                Label4.Text = "";
                Label3.Text = "Debe ingresar una descripción para esta oferta.";
                descripcionOferta.Focus();
                return;
            }

            if (descripcionOferta.Text.Trim().Length <= 10)
            {
                Label4.Text = "";
                Label3.Text = "Debe ser más explícito al ingresar la descripción para esta oferta.";
                descripcionOferta.Focus();
                return;
            }

            if (FechaInicial.Text.Trim().Length == 0)
            {
                Label4.Text = "";
                Label3.Text = "Debe ingresar la fecha inicial de la oferta.";
                FechaInicial.Focus();
                return;
            }

            if (FechaFinal.Text.Trim().Length == 0)
            {
                Label4.Text = "";
                Label3.Text = "Debe ingresar la fecha final de la oferta.";
                FechaFinal.Focus();
                return;
            }

            FechaInicial.Text = FechaInicial.Text.Replace("-", "/");
            FechaFinal.Text = FechaFinal.Text.Replace("-", "/");

            try
            {
                mFechaInicial = Convert.ToDateTime(FechaInicial.Text);
            }
            catch
            {
                Label4.Text = "";
                Label3.Text = "La fecha inicial ingresada es inválida.";
                FechaInicial.Focus();
                return;
            }

            if (mFechaInicial <= DateTime.Now.Date)
            {
                Label4.Text = "";
                Label3.Text = "La fecha inicial ingresada debe ser mayor al día de hoy.";
                FechaInicial.Focus();
                return;
            }

            try
            {
                mFechaFinal = Convert.ToDateTime(FechaFinal.Text);
            }
            catch
            {
                Label4.Text = "";
                Label3.Text = "La fecha final ingresada es inválida.";
                FechaFinal.Focus();
                return;
            }

            if (mFechaFinal <= DateTime.Now.Date)
            {
                Label4.Text = "";
                Label3.Text = "La fecha final ingresada debe ser mayor al día de hoy.";
                FechaFinal.Focus();
                return;
            }

            if (mFechaInicial > mFechaFinal)
            {
                Label4.Text = "";
                Label3.Text = "La fecha inicial no puede ser mayor a la fecha final.";
                FechaInicial.Focus();
                return;
            }
              
            char[] Separador = { '/' };
            string[] stringFechaInicial = FechaInicial.Text.Split(Separador);
            string[] stringFechaFinal = FechaFinal.Text.Split(Separador);

            DateTime mInicial = new DateTime(Convert.ToInt32(stringFechaInicial[2]), Convert.ToInt32(stringFechaInicial[1]), Convert.ToInt32(stringFechaInicial[0]));
            DateTime mFinal = new DateTime(Convert.ToInt32(stringFechaFinal[2]), Convert.ToInt32(stringFechaFinal[1]), Convert.ToInt32(stringFechaFinal[0]));

            var ws = new wsCambioPrecios.wsCambioPrecios(); string mMensaje = "";
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            if (!ws.GrabarOfertaArticulo(ref mMensaje, articulo.Text, mInicial, mFinal, Convert.ToDecimal(PrecioOriginal.Text), Convert.ToDecimal(PrecioOferta.Text), descripcionOferta.Text))
            {
                Label1.Text = "";
                Label2.Text = "";
                Label4.Text = "";
                Label5.Text = "";
                Label3.Text = mMensaje;
                return;
            }

            Label1.Text = "";
            Label2.Text = "";
            Label3.Text = "";
            Label5.Text = "";
            Label4.Text = mMensaje;

            articulo.Text = "";
            descripcion.Text = "";
            PrecioOriginal.Text = "";
            PrecioOriginalConIVA.Text = "";
            PrecioOferta.Text = "";
            PrecioOfertaConIVA.Text = "";
            pctjDescuento.Text = "";
            descripcionOferta.Text = "";
            FechaInicial.Text = "";
            FechaFinal.Text = "";

            CalendarioInicial.Visible = false;
            CalendarioFinal.Visible = false;

            lbCalendarioInicio.Text = "Abrir Calendario";
            lbCalendarioFinal.Text = "Abrir Calendario";

            articulo.Focus();
            tablaBusqueda.Visible = false;
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void gridArticulos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }

            foreach (TableCell cel in e.Row.Cells)
            {
                cel.Attributes.Add("style", "text-align: left");
            }
        }

        protected void btnDescuento_Click(object sender, EventArgs e)
        {
            PrecioOferta.Text = Convert.ToString(Convert.ToDouble(PrecioOriginal.Text) * (1 - (Convert.ToDouble(pctjDescuento.Text)) / 100));
            PrecioOfertaConIVA.Text = String.Format("{0:0,0.00}", (Convert.ToDouble(PrecioOferta.Text) * 1.12));
        }

        protected void PrecioOferta_TextChanged(object sender, EventArgs e)
        {
            PrecioOfertaConIVA.Text = String.Format("{0:0,0.00}", (Convert.ToDouble(PrecioOfertaConIVA.Text) * 1.12));
        }

        protected void PrecioOfertaConIVA_TextChanged(object sender, EventArgs e)
        {
            PrecioOferta.Text = Convert.ToString(Convert.ToDouble(PrecioOfertaConIVA.Text) / 1.12);
        }

    }
}
