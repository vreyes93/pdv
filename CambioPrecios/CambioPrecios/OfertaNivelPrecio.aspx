﻿<%@ Page Title="Ofertas por Nivel de Precio" Language="C#" MasterPageFile="~/SimpleMaster.Master" AutoEventWireup="true" CodeBehind="OfertaNivelPrecio.aspx.cs" Inherits="CambioPrecios.OfertaNivelPrecio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="menu" runat="server">
    <style type="text/css">
        .style1
        {
            text-align: center;
        }
        .style2
        {
            border-style: solid;
            border-width: 2px;
        }
        .style3
        {
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="divContenido">
    <table align="center">
        <tr>
            <td class="style3" colspan="3" style="text-align: center">
                <strong>PUBLICACION DE OFERTAS POR NIVEL DE PRECIO</strong></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2">
                <asp:Label ID="Label2" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Nivel:</td>
            <td>
                <asp:TextBox ID="nivel" runat="server" Width="120px" AutoPostBack="True" 
                    ontextchanged="nivel_TextChanged"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="descripcion" runat="server" Width="340px" 
                    BackColor="AliceBlue" Font-Bold="True" ReadOnly="True" TabIndex="5" 
                    ToolTip="Nothing"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:LinkButton ID="lbBuscarNivel" runat="server" 
                    onclick="lbBuscarNivel_Click" TabIndex="10">Buscar Nivel</asp:LinkButton>
            </td>
        </tr>
        </table>
    <table id="tablaBusqueda" runat="server" align="center" class="style2">
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2">
                <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                <asp:Label ID="Label5" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                Nivel a Buscar:</td>
            <td>
                <asp:TextBox ID="codigoNivel" runat="server" TabIndex="20"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                Descripción:</td>
            <td>
                <asp:TextBox ID="descripcionNivel" runat="server" TabIndex="30"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                <asp:Button ID="btnBuscar" runat="server" Text="Buscar" 
                    ToolTip="Busca niveles según el criterio de búsqueda ingresado" 
                    onclick="btnBuscar_Click" TabIndex="40" />
                <asp:Button ID="btnNivelesTodos" runat="server" onclick="btnNivelesTodos_Click" 
                    Text="Listar todos los niveles" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td class="style1" colspan="2">
                <asp:GridView ID="gridNiveles" runat="server" CellPadding="4" 
                    ForeColor="#333333" GridLines="None" 
                    onselectedindexchanged="gridNiveles_SelectedIndexChanged" TabIndex="50" 
                    onrowdatabound="gridNiveles_RowDataBound">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:CommandField SelectText="Seleccionar" ShowSelectButton="True" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                <asp:LinkButton ID="lbCerrarBusqueda" runat="server" 
                    ToolTip="Cierra la ventana de búsqueda" onclick="lbCerrarBusqueda_Click" 
                    TabIndex="60">Cerrar Búsqueda</asp:LinkButton>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <table align="center">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Coeficiente Original:</td>
            <td>
                <asp:TextBox ID="CoeficienteOriginal" runat="server" Width="110px" 
                    BackColor="AliceBlue" Font-Bold="True" ReadOnly="True" TabIndex="70"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Coeficiente Oferta:</td>
            <td>
                <asp:TextBox ID="CoeficienteOferta" runat="server" Width="110px" TabIndex="80"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Descripción:</td>
            <td colspan="3">
                <asp:TextBox ID="descripcionOferta" runat="server" Width="385px" TabIndex="90" 
                    MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Inicio (dd/MM/yyyy):</td>
            <td>
                <asp:TextBox ID="FechaInicial" runat="server" Width="110px" TabIndex="100" 
                    MaxLength="10"></asp:TextBox>
            </td>
            <td style="text-align: left">
                <asp:LinkButton ID="lbCalendarioInicio" runat="server" 
                    onclick="lbCalendarioInicio_Click" TabIndex="110">Abrir Calendario</asp:LinkButton>
            </td>
            <td>
                <asp:Calendar ID="CalendarioInicial" runat="server" BackColor="White" 
                    BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" 
                    DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="6pt" 
                    ForeColor="#003399" Height="150px" Visible="False" Width="160px" 
                    onselectionchanged="CalendarioInicial_SelectionChanged" TabIndex="120">
                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                    <OtherMonthDayStyle Font-Bold="False" ForeColor="#999999" />
                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" 
                        Font-Bold="True" Font-Size="8pt" ForeColor="#CCCCFF" Height="22px" />
                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                    <WeekendDayStyle BackColor="#CCCCFF" />
                </asp:Calendar>
            </td>
        </tr>
        <tr>
            <td>
                Fin (dd/MM/yyyy):</td>
            <td>
                <asp:TextBox ID="FechaFinal" runat="server" Width="110px" TabIndex="130" 
                    MaxLength="10"></asp:TextBox>
            </td>
            <td style="text-align: left">
                <asp:LinkButton ID="lbCalendarioFinal" runat="server" 
                    onclick="lbCalendarioFinal_Click" TabIndex="140">Abrir Calendario</asp:LinkButton>
            </td>
            <td>
                <asp:Calendar ID="CalendarioFinal" runat="server" BackColor="White" 
                    BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" 
                    DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="6pt" 
                    ForeColor="#003399" Height="150px" Visible="False" Width="160px" 
                    onselectionchanged="CalendarioFinal_SelectionChanged" TabIndex="150">
                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                    <OtherMonthDayStyle Font-Bold="False" ForeColor="#999999" />
                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" 
                        Font-Bold="True" Font-Size="8pt" ForeColor="#CCCCFF" Height="22px" />
                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                    <WeekendDayStyle BackColor="#CCCCFF" />
                </asp:Calendar>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center">
                <asp:Button ID="btnGrabar" runat="server" onclick="btnGrabar_Click" 
                    TabIndex="160" Text="Grabar Oferta del Nivel" UseSubmitBehavior="False" />
                <asp:Button ID="btnSalir" runat="server" onclick="btnSalir_Click" 
                    Text="Salir" />
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center">
                <asp:Label ID="Label3" runat="server" ForeColor="Red"></asp:Label>
                <asp:Label ID="Label4" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</div>
</asp:Content>
