﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CambioPrecios
{
    public partial class OfertaNivelPrecio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Label1.Text = "";
                Label2.Text = "";
                Label5.Text = "";
                nivel.Focus();
                tablaBusqueda.Visible = false;
                ViewState["url"] = string.Format("http://integra/services/wsCambioPrecios.asmx", Request.Url.Host);
            }
        }

        protected void lbBuscarNivel_Click(object sender, EventArgs e)
        {
            Label1.Text = "";
            Label5.Text = "";
            codigoNivel.Text = "";
            descripcionNivel.Text = "";
            tablaBusqueda.Visible = true;
            codigoNivel.Focus();
        }

        protected void lbCerrarBusqueda_Click(object sender, EventArgs e)
        {
            tablaBusqueda.Visible = false;
        }

        protected void lbCalendarioInicio_Click(object sender, EventArgs e)
        {
            if (CalendarioInicial.Visible)
            {
                CalendarioInicial.Visible = false;
                lbCalendarioInicio.Text = "Abrir Calendario";
            }
            else
            {
                CalendarioInicial.Visible = true;
                lbCalendarioInicio.Text = "Cerrar Calendario";
            }
        }

        protected void lbCalendarioFinal_Click(object sender, EventArgs e)
        {
            if (CalendarioFinal.Visible)
            {
                CalendarioFinal.Visible = false;
                lbCalendarioFinal.Text = "Abrir Calendario";
            }
            else
            {
                CalendarioFinal.Visible = true;
                lbCalendarioFinal.Text = "Cerrar Calendario";
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            string mTipo = "";

            Label1.Text = "";
            Label5.Text = "";

            if (codigoNivel.Text.Trim().Length == 0 && descripcionNivel.Text.Trim().Length == 0)
            {
                Label1.Text = "Debe ingresar un criterio de búsqueda ya sea el código, la descripción o ambos.";
                codigoNivel.Focus();
                return;
            }

            if (codigoNivel.Text.Trim().Length > 0) mTipo = "C";
            if (descripcionNivel.Text.Trim().Length > 0) mTipo = "D";
            if (codigoNivel.Text.Trim().Length > 0 && descripcionNivel.Text.Trim().Length > 0) mTipo = "A";

            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarCoeficientesCriterio(mTipo, codigoNivel.Text, descripcionNivel.Text);
            var niveles = q.ToArray();

            gridNiveles.DataSource = niveles;
            gridNiveles.DataBind();

            codigoNivel.Focus();
            if (q.Length == 0)
            {
                Label1.Text = "No se encontraron niveles con el criterio de búsqueda ingresado.";
            }
            else
            {
                Label5.Text = string.Format("Se encontraron {0} niveles con el criterio de búsqueda ingresado.", q.Length);
            }
        }

        protected void gridNiveles_SelectedIndexChanged(object sender, EventArgs e)
        {
            nivel.Text = gridNiveles.SelectedRow.Cells[1].Text;
            descripcion.Text = gridNiveles.SelectedRow.Cells[3].Text;
            CoeficienteOriginal.Text = gridNiveles.SelectedRow.Cells[2].Text;

            if (descripcion.Text == "&nbsp;") descripcion.Text = "";

            descripcion.Text = descripcion.Text.Replace("&#39;", "'");
            descripcion.Text = descripcion.Text.Replace("&#225;", "á");
            descripcion.Text = descripcion.Text.Replace("&#233;", "é");
            descripcion.Text = descripcion.Text.Replace("&#237;", "í");
            descripcion.Text = descripcion.Text.Replace("&#243;", "ó");
            descripcion.Text = descripcion.Text.Replace("&#250;", "ú");

            Label1.Text = "";
            Label2.Text = "";
            Label5.Text = "";

            CoeficienteOferta.Focus();
            tablaBusqueda.Visible = false;
        }

        protected void nivel_TextChanged(object sender, EventArgs e)
        {
            if (nivel.Text.Trim().Length == 0)
            {
                Label1.Text = "";
                Label5.Text = "";
                descripcion.Text = "";
                CoeficienteOriginal.Text = "";
                return;
            }
              
            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarCoeficientesCriterio("N", nivel.Text, "");
            var nivels = q.ToArray();

            gridNiveles.DataSource = nivels;
            gridNiveles.DataBind();

            if (gridNiveles.Rows.Count == 0)
            {
                nivel.Text = "";
                descripcion.Text = "";
                CoeficienteOriginal.Text = "";
                Label1.Text = "El código de nivel ingresado no existe.";
                nivel.Focus();
                return;
            }

            descripcion.Text = gridNiveles.Rows[0].Cells[3].Text;
            CoeficienteOriginal.Text = gridNiveles.Rows[0].Cells[2].Text;
  
            if (descripcion.Text == "&nbsp;") descripcion.Text = "";

            descripcion.Text = descripcion.Text.Replace("&#39;", "'");
            descripcion.Text = descripcion.Text.Replace("&#225;", "á");
            descripcion.Text = descripcion.Text.Replace("&#233;", "é");
            descripcion.Text = descripcion.Text.Replace("&#237;", "í");
            descripcion.Text = descripcion.Text.Replace("&#243;", "ó");
            descripcion.Text = descripcion.Text.Replace("&#250;", "ú");

            Label1.Text = "";
            Label2.Text = "";
            Label5.Text = "";

            CoeficienteOferta.Focus();
            tablaBusqueda.Visible = false;
        }

        protected void CalendarioInicial_SelectionChanged(object sender, EventArgs e)
        {
            CalendarioInicial.Visible = false;
            lbCalendarioInicio.Text = "Abrir Calendario";
            FechaInicial.Text = CalendarioInicial.SelectedDate.ToShortDateString();
        }

        protected void CalendarioFinal_SelectionChanged(object sender, EventArgs e)
        {
            CalendarioFinal.Visible = false;
            lbCalendarioFinal.Text = "Abrir Calendario";
            FechaFinal.Text = CalendarioFinal.SelectedDate.ToShortDateString();
        }

        protected void btnGrabar_Click(object sender, EventArgs e)
        {
            decimal mCoeficienteOriginal, mCoeficienteOferta; DateTime mFechaInicial, mFechaFinal;

            if (nivel.Text.Trim().Length == 0)
            {
                Label4.Text = "";
                Label3.Text = "Debe ingresar un nivel a ofertar.";
                nivel.Focus();
                return;
            }

            mCoeficienteOriginal = Convert.ToDecimal(CoeficienteOriginal.Text);

            if (CoeficienteOferta.Text.Trim().Length == 0)
            {
                Label4.Text = "";
                Label3.Text = "Debe ingresar el coeficiente a ofertar.";
                CoeficienteOferta.Focus();
                return;
            }

            try
            {
                mCoeficienteOferta = Convert.ToDecimal(CoeficienteOferta.Text);
            }
            catch
            {
                Label4.Text = "";
                Label3.Text = "El coeficiente de oferta ingresado es inválido.";
                CoeficienteOferta.Focus();
                return;
            }

            if (mCoeficienteOferta <= 0)
            {
                Label4.Text = "";
                Label3.Text = "El coeficiente de oferta ingresado es inválido.";
                CoeficienteOferta.Focus();
                return;
            }

            if (mCoeficienteOriginal == mCoeficienteOferta)
            {
                Label4.Text = "";
                Label3.Text = "El coeficiente de la oferta es igual al coeficiente original.";
                CoeficienteOferta.Focus();
                return;
            }

            if (descripcionOferta.Text.Trim().Length == 0)
            {
                Label4.Text = "";
                Label3.Text = "Debe ingresar una descripción para esta oferta.";
                descripcionOferta.Focus();
                return;
            }

            if (descripcionOferta.Text.Trim().Length <= 10)
            {
                Label4.Text = "";
                Label3.Text = "Debe ser más explícito al ingresar la descripción para esta oferta.";
                descripcionOferta.Focus();
                return;
            }

            if (FechaInicial.Text.Trim().Length == 0)
            {
                Label4.Text = "";
                Label3.Text = "Debe ingresar la fecha inicial de la oferta.";
                FechaInicial.Focus();
                return;
            }

            if (FechaFinal.Text.Trim().Length == 0)
            {
                Label4.Text = "";
                Label3.Text = "Debe ingresar la fecha final de la oferta.";
                FechaFinal.Focus();
                return;
            }

            FechaInicial.Text = FechaInicial.Text.Replace("-", "/");
            FechaFinal.Text = FechaFinal.Text.Replace("-", "/");

            try
            {
                mFechaInicial = Convert.ToDateTime(FechaInicial.Text);
            }
            catch
            {
                Label4.Text = "";
                Label3.Text = "La fecha inicial ingresada es inválida.";
                FechaInicial.Focus();
                return;
            }

            if (mFechaInicial <= DateTime.Now.Date)
            {
                Label4.Text = "";
                Label3.Text = "La fecha inicial ingresada debe ser mayor al día de hoy.";
                FechaInicial.Focus();
                return;
            }

            try
            {
                mFechaFinal = Convert.ToDateTime(FechaFinal.Text);
            }
            catch
            {
                Label4.Text = "";
                Label3.Text = "La fecha final ingresada es inválida.";
                FechaFinal.Focus();
                return;
            }

            if (mFechaFinal <= DateTime.Now.Date)
            {
                Label4.Text = "";
                Label3.Text = "La fecha final ingresada debe ser mayor al día de hoy.";
                FechaFinal.Focus();
                return;
            }

            if (mFechaInicial > mFechaFinal)
            {
                Label4.Text = "";
                Label3.Text = "La fecha inicial no puede ser mayor a la fecha final.";
                FechaInicial.Focus();
                return;
            }

            char[] Separador = { '/' };
            string[] stringFechaInicial = FechaInicial.Text.Split(Separador);
            string[] stringFechaFinal = FechaFinal.Text.Split(Separador);

            DateTime mInicial = new DateTime(Convert.ToInt32(stringFechaInicial[2]), Convert.ToInt32(stringFechaInicial[1]), Convert.ToInt32(stringFechaInicial[0]));
            DateTime mFinal = new DateTime(Convert.ToInt32(stringFechaFinal[2]), Convert.ToInt32(stringFechaFinal[1]), Convert.ToInt32(stringFechaFinal[0]));

            var ws = new wsCambioPrecios.wsCambioPrecios(); string mMensaje = "";
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            if (!ws.GrabarOfertaNivel(ref mMensaje, nivel.Text, mInicial, mFinal, Convert.ToDecimal(CoeficienteOriginal.Text), Convert.ToDecimal(CoeficienteOferta.Text), descripcionOferta.Text))
            {
                Label1.Text = "";
                Label2.Text = "";
                Label4.Text = "";
                Label5.Text = "";
                Label3.Text = mMensaje;
                return;
            }

            Label1.Text = "";
            Label2.Text = "";
            Label3.Text = "";
            Label5.Text = "";
            Label4.Text = mMensaje;

            nivel.Text = "";
            descripcion.Text = "";
            CoeficienteOriginal.Text = "";
            CoeficienteOferta.Text = "";
            descripcionOferta.Text = "";
            FechaInicial.Text = "";
            FechaFinal.Text = "";

            CalendarioInicial.Visible = false;
            CalendarioFinal.Visible = false;

            lbCalendarioInicio.Text = "Abrir Calendario";
            lbCalendarioFinal.Text = "Abrir Calendario";

            nivel.Focus();
            tablaBusqueda.Visible = false;
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void gridNiveles_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }

            foreach (TableCell cel in e.Row.Cells)
            {
                cel.Attributes.Add("style", "text-align: left");
            }
        }

        protected void btnNivelesTodos_Click(object sender, EventArgs e)
        {
            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarCoeficientes();
            var coeficientes = q.ToArray();

            gridNiveles.DataSource = coeficientes;
            gridNiveles.DataBind();
        }

    }
}
