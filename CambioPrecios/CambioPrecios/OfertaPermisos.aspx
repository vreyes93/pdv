﻿<%@ Page Title="Permisos a Ofertas" Language="C#" MasterPageFile="~/SimpleMaster.Master" AutoEventWireup="true" CodeBehind="OfertaPermisos.aspx.cs" Inherits="CambioPrecios.OfertaPermisos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="menu" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="divContenido">
        <table align="center">
            <tr>
                <td 
                    style="text-align: center; font-weight: 700; text-decoration: underline">
                    ASIGNAR PERMISOS PARA CANCELAR OFERTAS</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label3" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Button ID="btnGrabar" runat="server" Text="Grabar" 
                        onclick="btnGrabar_Click" />
                    <asp:Button ID="btnSalir" runat="server" Text="Salir" 
                        onclick="btnSalir_Click" />
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:CheckBoxList ID="usuarios" runat="server" Height="26px" Width="250px">
                    </asp:CheckBoxList>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Button ID="btnGrabar0" runat="server" Text="Grabar" 
                        onclick="btnGrabar_Click" />
                    <asp:Button ID="btnSalir0" runat="server" Text="Salir" 
                        onclick="btnSalir_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label4" runat="server"></asp:Label>
                </td>
            </tr>
            </table>
    </div>
</asp:Content>
