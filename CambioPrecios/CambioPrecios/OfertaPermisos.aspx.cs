﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;

namespace CambioPrecios
{
    public partial class OfertaPermisos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["url"] = string.Format("http://integra/services/wsCambioPrecios.asmx", Request.Url.Host);
                  
                DirectoryContext dc = new DirectoryContext(DirectoryContextType.Domain, Environment.UserDomainName);
                Domain domain = Domain.GetDomain(dc);
                DirectoryEntry de = domain.GetDirectoryEntry();
                    
                LlenarUsuarios(de);
            }
        }
          
        private void LlenarUsuarios(DirectoryEntry de)
        {
            DirectorySearcher deSearch = new DirectorySearcher(de);

            deSearch.Filter = "(&(objectClass=user)(objectCategory=person))";
            SearchResultCollection results = deSearch.FindAll();

            System.Data.DataTable dt = new System.Data.DataTable("usuarios");
            dt.Columns.Add(new System.Data.DataColumn("usuario", typeof(System.String)));

            foreach (SearchResult srUser in results)
            {
                DirectoryEntry deUser = srUser.GetDirectoryEntry();

                int flag = (int)deUser.Properties["userAccountControl"].Value;

                bool mActivo = false;
                if (!Convert.ToBoolean(flag & 0x0002)) mActivo = true;

                if (deUser.Properties["sAMAccountName"].Value.ToString().Contains(".") && mActivo && !deUser.Properties["sAMAccountName"].Value.ToString().Contains("admin"))
                {
                    System.Data.DataRow mRow = dt.NewRow();
                    mRow["usuario"] = deUser.Properties["sAMAccountName"].Value.ToString();
                    dt.Rows.Add(mRow);
                }
            }

            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarUsuariosCancelan();
            var UsuariosCancelan = q.ToArray();
            
            System.Data.DataRow[] mRows = dt.Select("", "usuario");

            for (int ii = 0; ii <= (mRows.Length - 1); ii++)
            {
                usuarios.Items.Add(mRows[ii]["usuario"].ToString());

                for (int jj = 0; jj < UsuariosCancelan.Length; jj++)
                {
                    if (mRows[ii]["usuario"].ToString() == UsuariosCancelan[jj].usuario) usuarios.Items[ii].Selected = true;
                }
            }

            string mTexto = string.Format("Se listan {0} usuarios, habiendo {1} con pemisos de cancelar ofertas.", usuarios.Items.Count.ToString(), UsuariosCancelan.Length.ToString());
            Label3.Text = mTexto;
            Label4.Text = mTexto;
        }

        protected void btnGrabar_Click(object sender, EventArgs e)
        {
            Label1.Text = "";
            Label2.Text = "";
            Label3.Text = "";
            Label4.Text = "";

            bool mMarcados = false; string[] usuariosPermisos = new string[1]; int jj = 0;

            for (int ii = 0; ii < usuarios.Items.Count; ii++)
            {
                if (usuarios.Items[ii].Selected)
                {
                    jj++;
                    mMarcados = true;

                    if (jj > 1) Array.Resize(ref usuariosPermisos, jj);
                    usuariosPermisos[jj - 1] = usuarios.Items[ii].Text;
                }
            }

            if (!mMarcados)
            {
                string mTexto = "Debe seleccionar al menos un usuario que pueda cancelar ofertas.";
                Label1.Text = mTexto;
                Label2.Text = mTexto;
                return;
            }

            var ws = new wsCambioPrecios.wsCambioPrecios(); string mMensaje = "";
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            if (!ws.GrabarUsuariosCancelanOfertas(ref mMensaje, usuariosPermisos))
            {
                Label1.Text = "";
                Label3.Text = "";
                Label4.Text = "";
                Label2.Text = mMensaje;
                return;
            }

            Label1.Text = "";
            Label2.Text = "";
            Label3.Text = mMensaje;
            Label4.Text = mMensaje;
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

    }
}