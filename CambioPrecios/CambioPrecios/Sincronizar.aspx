﻿<%@ Page Title="Sincronizar y Modificar de Niveles de Precio" Language="C#" MasterPageFile="~/SimpleMaster.Master" AutoEventWireup="true" CodeBehind="Sincronizar.aspx.cs" Inherits="CambioPrecios.Sincronizar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="menu" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="divContenido">
        <table align="center">
            <tr>
                <td colspan="2" 
                    style="text-align: center; font-weight: 700; text-decoration: underline">
                    SINCRONIZAR Y MODIFICAR NIVELES DE PRECIO</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    Nivel de Precio:</td>
                <td>
                    <asp:TextBox ID="Nivel" runat="server" ReadOnly="True"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Coeficiente a Multiplicar:</td>
                <td>
                    <asp:TextBox ID="Coeficiente" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Descripción:</td>
                <td>
                    <asp:TextBox ID="Descripcion" runat="server" Width="295px" MaxLength="50"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <asp:Button ID="btnGrabar" runat="server" Text="Grabar" 
                        onclick="btnGrabar_Click" />
                    <asp:Button ID="btnSalir" runat="server" Text="Salir" 
                        onclick="btnSalir_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="BuscarNivel" runat="server"></asp:TextBox>
                    <asp:Button ID="btnBuscar" runat="server" onclick="btnBuscar_Click" 
                        Text="Buscar Nivel de Precio" />
                    <asp:Button ID="btnActualizar" runat="server" onclick="btnActualizar_Click" 
                        Text="Actualizar Niveles de Precio" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <asp:GridView ID="grid" runat="server" AllowPaging="True" CellPadding="4" 
                        ForeColor="#333333" GridLines="None" 
                        onpageindexchanging="grid_PageIndexChanging" PageSize="20" 
                        onselectedindexchanged="grid_SelectedIndexChanged" 
                        onrowdatabound="grid_RowDataBound">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <Columns>
                            <asp:CommandField SelectText="Seleccionar" ShowSelectButton="True" />
                        </Columns>
                        <EditRowStyle BackColor="#999999" />
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
