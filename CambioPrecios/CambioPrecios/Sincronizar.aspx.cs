﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CambioPrecios
{
    public partial class Sincronizar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ViewState["url"] = string.Format("http://integra/services/wsCambioPrecios.asmx", Request.Url.Host);
                sincronizarTablas();
                Coeficiente.Focus();
            }
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            sincronizarTablas();
        }

        void sincronizarTablas()
        {
            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            ws.Sincronizar();

            var q = ws.ListarCoeficientes();
            var coeficientes = q.ToArray();

            grid.DataSource = coeficientes;

            ViewState["ds"] = coeficientes;

            grid.DataBind();

            Nivel.Text = "";
            Coeficiente.Text = "";
            Descripcion.Text = "";
            BuscarNivel.Text = "";

            Label2.Text = "";
            Label1.Text = string.Format("Se encontraron {0} Niveles de Precios.", coeficientes.Length);
        }


        protected void grid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            var ds = (wsCambioPrecios.NivelPrecioCoeficiente[])ViewState["ds"];

            grid.DataSource = ds;
            grid.PageIndex = e.NewPageIndex;
            grid.DataBind();
        }

        protected void grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            Nivel.Text = grid.SelectedRow.Cells[1].Text;
            Coeficiente.Text = grid.SelectedRow.Cells[2].Text;
            Descripcion.Text = grid.SelectedRow.Cells[3].Text;

            if (Descripcion.Text == "&nbsp;") Descripcion.Text = "";

            Descripcion.Text = Descripcion.Text.Replace("&#39;", "'");
            Descripcion.Text = Descripcion.Text.Replace("&#225;", "á");
            Descripcion.Text = Descripcion.Text.Replace("&#233;", "é");
            Descripcion.Text = Descripcion.Text.Replace("&#237;", "í");
            Descripcion.Text = Descripcion.Text.Replace("&#243;", "ó");
            Descripcion.Text = Descripcion.Text.Replace("&#250;", "ú");

            Coeficiente.Focus();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var datos = ws.ListarCoeficientesPorNivel(BuscarNivel.Text);

            ViewState["ds"] = datos;

            grid.DataSource = datos;
            grid.DataBind();

            Label2.Text = "";
            Label1.Text = string.Format("Se encontraron {0} Niveles de Precios con el criterio de búsqueda ingresado.", datos.Length);
        }

        protected void btnGrabar_Click(object sender, EventArgs e)
        {
            try
            {
                Decimal ii;
                try
                {
                    ii = Convert.ToDecimal(Coeficiente.Text);
                }
                catch
                {
                    Label1.Text = "";
                    Label2.Text = "El coeficiente ingresado es inválido.";
                    Coeficiente.Focus();
                    return;
                }

                if (ii <= 0)
                {
                    Label1.Text = "";
                    Label2.Text = "El coeficiente ingresado debe ser mayor que cero.";
                    Coeficiente.Focus();
                    return;
                }

                var ws = new wsCambioPrecios.wsCambioPrecios(); string mMensaje = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.GrabarCoeficientes(Nivel.Text, Coeficiente.Text, Descripcion.Text, ref mMensaje))
                {
                    Label1.Text = "";
                    Label2.Text = mMensaje;
                    return;
                }

                sincronizarTablas();

                Label2.Text = "";
                Label1.Text = mMensaje;
            }
            catch (Exception ex)
            {
                Label1.Text = "";
                Label2.Text = string.Format("Se produjo el siguiente error al grabar: {0}", ex.Message);
            }
        }
          
        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }

            foreach (TableCell cel in e.Row.Cells)
            {
                cel.Attributes.Add("style", "text-align: left");
            }
        }

    }
}