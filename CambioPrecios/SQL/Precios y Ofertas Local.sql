USE desarrollo
GO

CREATE TYPE FlagNyType FROM tinyint NOT NULL
GO
CREATE TYPE CurrentDateType FROM datetime NOT NULL
GO
CREATE TYPE RowPointerType FROM uniqueidentifier NOT NULL
GO
CREATE TYPE UsernameType FROM varchar(30) NOT NULL
GO

CREATE TABLE ARTICULO (
	ARTICULO			VARCHAR(20)				NOT NULL,
	DESCRIPCION			VARCHAR(254)			NOT NULL,
	PRECIO_BASE_LOCAL	DECIMAL(28,8)			NOT NULL,
	PRECIO_BASE_DOLAR	DECIMAL(28,8)			NOT NULL,
	NoteExistsFlag		FlagNyType				NOT NULL,
	RecordDate			CurrentDateType			NOT NULL,
	RowPointer			RowPointerType			NOT NULL,
	CreatedBy			UsernameType			NOT NULL,
	UpdatedBy			UsernameType			NOT NULL,
	CreateDate			CurrentDateType			NOT NULL
)
go

ALTER TABLE ARTICULO ADD PRIMARY KEY ( ARTICULO )
GO

CREATE TABLE ARTICULO_PRECIO (
	NIVEL_PRECIO		VARCHAR(12)				NOT NULL,
	MONEDA				VARCHAR(1)				NOT NULL,
	VERSION				INT						NOT NULL,
	ARTICULO			VARCHAR(20)				NOT NULL,
	VERSION_ARTICULO	INT						NOT NULL,
	PRECIO				DECIMAL(28,8)			NOT NULL,
	ESQUEMA_TRABAJO		VARCHAR(1)				NOT NULL,
	MARGEN_MULR			DECIMAL(28,8)			NOT NULL,
	MARGEN_UTILIDAD		DECIMAL(28,8)			NOT NULL,
	FECHA_INICIO		DATETIME				NOT NULL,
	FECHA_FIN			DATETIME				NOT NULL,
	FECHA_ULT_MODIF		DATETIME				NOT NULL,
	USUARIO_ULT_MODIF	VARCHAR(10)				NOT NULL,
	NoteExistsFlag		FlagNyType				NOT NULL,
	RecordDate			CurrentDateType			NOT NULL,
	RowPointer			RowPointerType			NOT NULL,
	CreatedBy			UsernameType			NOT NULL,
	UpdatedBy			UsernameType			NOT NULL,
	CreateDate			CurrentDateType			NOT NULL
)
GO

ALTER TABLE ARTICULO_PRECIO ADD PRIMARY KEY ( NIVEL_PRECIO, MONEDA, VERSION, ARTICULO, VERSION_ARTICULO )
GO

CREATE TABLE NIVEL_PRECIO (
	NIVEL_PRECIO		VARCHAR(12)				NOT NULL,
	MONEDA				VARCHAR(1)				NOT NULL,
	CONDICION_PAGO		VARCHAR(4)				NULL,
	ESQUEMA_TRABAJO		VARCHAR(1)				NOT NULL,
	DESCUENTOS			VARCHAR(1)				NOT NULL,
	SUGERIR_DESCUENTO	VARCHAR(1)				NOT NULL,
	NoteExistsFlag		FlagNyType				NOT NULL,
	RecordDate			CurrentDateType			NOT NULL,
	RowPointer			RowPointerType			NOT NULL,
	CreatedBy			UsernameType			NOT NULL,
	UpdatedBy			UsernameType			NOT NULL,
	CreateDate			CurrentDateType			NOT NULL
)
GO

ALTER TABLE NIVEL_PRECIO ADD PRIMARY KEY ( NIVEL_PRECIO, MONEDA )
GO

CREATE TABLE MF_NIVEL_PRECIO_COEFICIENTE (
	NIVEL_PRECIO		VARCHAR(12)				NOT NULL,
	MONEDA				VARCHAR(1)				NOT NULL,
	COEFICIENTE_MULR	DECIMAL(18,6)			NOT NULL,
	DESCRIPCION			VARCHAR(50)				NULL
)
GO

ALTER TABLE MF_NIVEL_PRECIO_COEFICIENTE ADD PRIMARY KEY ( NIVEL_PRECIO )
GO

CREATE TABLE MF_ArticuloPrecioOferta (
	Articulo			VARCHAR(20)				NOT NULL,
	FechaOfertaDesde	DATETIME				NOT NULL,
	FechaOfertaHasta	DATETIME				NOT NULL,
	PrecioOriginal		DECIMAL(28,8)			NOT NULL,
	PrecioOferta		DECIMAL(28,8)			NOT NULL,
	Estatus				CHAR(1)					NOT NULL,	--P: Pendiente, V: Vigente, T: Terminada
	Descripcion			VARCHAR(50)				NULL,
	RecordDate			CurrentDateType			NOT NULL,
	CreatedBy			UsernameType			NOT NULL,
	UpdatedBy			UsernameType			NOT NULL,
	CreateDate			CurrentDateType			NOT NULL
)
GO

ALTER TABLE MF_ArticuloPrecioOferta ADD PRIMARY KEY ( Articulo, FechaOfertaDesde )
GO

CREATE TABLE MF_ArticuloNivelPrecioOferta (
	NivelPrecio			VARCHAR(12)				NOT NULL,
	Moneda				VARCHAR(1)				NOT NULL,
	Version				INT						NOT NULL,
	Articulo			VARCHAR(20)				NOT NULL,
	VersionArticulo		INT						NOT NULL,
	FechaOfertaDesde	DATETIME				NOT NULL,
	FechaOfertaHasta	DATETIME				NOT NULL,
	PrecioOriginal		DECIMAL(28,8)			NOT NULL,
	PrecioOferta		DECIMAL(28,8)			NOT NULL,
	Estatus				CHAR(1)					NOT NULL,	--P: Pendiente, V: Vigente, T: Terminada
	Descripcion			VARCHAR(50)				NULL,
	RecordDate			CurrentDateType			NOT NULL,
	CreatedBy			UsernameType			NOT NULL,
	UpdatedBy			UsernameType			NOT NULL,
	CreateDate			CurrentDateType			NOT NULL
)
GO

ALTER TABLE MF_ArticuloNivelPrecioOferta ADD PRIMARY KEY ( NivelPrecio, Moneda, Version, Articulo, VersionArticulo, FechaOfertaDesde )
GO

CREATE TABLE MF_NivelPrecioCoeficienteOferta (
	NivelPrecio					VARCHAR(12)				NOT NULL,
	Moneda						VARCHAR(1)				NOT NULL,
	FechaOfertaDesde			DATETIME				NOT NULL,
	FechaOfertaHasta			DATETIME				NOT NULL,
	CoeficienteMulrOriginal		DECIMAL(18,6)			NOT NULL,
	CoeficienteMulrOferta		DECIMAL(18,6)			NOT NULL,
	Estatus						CHAR(1)					NOT NULL,	--P: Pendiente, V: Vigente, T: Terminada
	Descripcion					VARCHAR(50)				NULL,
	RecordDate					CurrentDateType			NOT NULL,
	CreatedBy					UsernameType			NOT NULL,
	UpdatedBy					UsernameType			NOT NULL,
	CreateDate					CurrentDateType			NOT NULL
)
GO

ALTER TABLE MF_NivelPrecioCoeficienteOferta ADD PRIMARY KEY ( NivelPrecio, Moneda, FechaOfertaDesde )
GO


ALTER TABLE MF_ArticuloNivelPrecioOferta  WITH CHECK ADD CONSTRAINT FK_MF_ArticuloNivelPrecioOferta_ARTICULO FOREIGN KEY(Articulo)
REFERENCES ARTICULO (ARTICULO)
GO
ALTER TABLE MF_ArticuloNivelPrecioOferta CHECK CONSTRAINT FK_MF_ArticuloNivelPrecioOferta_ARTICULO
GO


ALTER TABLE MF_ArticuloNivelPrecioOferta  WITH CHECK ADD CONSTRAINT FK_MF_ArticuloNivelPrecioOferta_ARTICULO_PRECIO FOREIGN KEY(NivelPrecio, Moneda, Version, Articulo, VersionArticulo)
REFERENCES ARTICULO_PRECIO (NIVEL_PRECIO, MONEDA, VERSION, ARTICULO, VERSION_ARTICULO)
GO
ALTER TABLE MF_ArticuloNivelPrecioOferta CHECK CONSTRAINT FK_MF_ArticuloNivelPrecioOferta_ARTICULO_PRECIO
GO


ALTER TABLE MF_ArticuloNivelPrecioOferta  WITH CHECK ADD CONSTRAINT FK_MF_ArticuloNivelPrecioOferta_NIVEL_PRECIO FOREIGN KEY(NivelPrecio, Moneda)
REFERENCES NIVEL_PRECIO (NIVEL_PRECIO, MONEDA)
GO
ALTER TABLE MF_ArticuloNivelPrecioOferta CHECK CONSTRAINT FK_MF_ArticuloNivelPrecioOferta_NIVEL_PRECIO
GO


ALTER TABLE MF_ArticuloPrecioOferta  WITH CHECK ADD CONSTRAINT FK_MF_ArticuloPrecioOferta_ARTICULO FOREIGN KEY(Articulo)
REFERENCES ARTICULO (ARTICULO)
GO
ALTER TABLE MF_ArticuloPrecioOferta CHECK CONSTRAINT FK_MF_ArticuloPrecioOferta_ARTICULO
GO


ALTER TABLE MF_NIVEL_PRECIO_COEFICIENTE  WITH CHECK ADD CONSTRAINT FK_MF_NIVEL_PRECIO_COEFICIENTE_NIVEL_PRECIO FOREIGN KEY(NIVEL_PRECIO, MONEDA)
REFERENCES NIVEL_PRECIO (NIVEL_PRECIO, MONEDA)
GO
ALTER TABLE MF_NIVEL_PRECIO_COEFICIENTE CHECK CONSTRAINT FK_MF_NIVEL_PRECIO_COEFICIENTE_NIVEL_PRECIO
GO


ALTER TABLE MF_NivelPrecioCoeficienteOferta  WITH CHECK ADD CONSTRAINT FK_MF_NivelPrecioCoeficienteOferta_MF_NIVEL_PRECIO_COEFICIENTE FOREIGN KEY(NivelPrecio, Moneda)
REFERENCES MF_NIVEL_PRECIO_COEFICIENTE (NIVEL_PRECIO, MONEDA)
GO

ALTER TABLE MF_NivelPrecioCoeficienteOferta CHECK CONSTRAINT FK_MF_NivelPrecioCoeficienteOferta_MF_NIVEL_PRECIO_COEFICIENTE
GO

CREATE TABLE prodmult.MF_UsuariosCancelanOfertas (
	usuario						VARCHAR(30)		NOT NULL
)
GO

ALTER TABLE prodmult.MF_UsuariosCancelanOfertas ADD PRIMARY KEY ( usuario )
GO



-- Esta tabla contendr� la bit�cora del resultado de los procesos que se ejecutar�n en las noches y madrugadas.
-- Aqu� se podr� consultar si hubo alg�n error.
CREATE TABLE prodmult.MF_BitacoraPreciosOfertas (
	IdBitacora					INT	IDENTITY(1,1)		NOT NULL,
	Tipo						CHAR(1)					NOT NULL,	--P: Actualizaci�n de Precios; I: Inicio de Ofertas; F: Fin de Ofertas; 
	Resultado					CHAR(1)					NOT NULL,	--S: Success; F: Failure; 
	Descripcion					VARCHAR(1000)			NOT NULL,
	RecordDate					CurrentDateType			NOT NULL,
	CreatedBy					UsernameType			NOT NULL
)
GO

ALTER TABLE prodmult.MF_BitacoraPreciosOfertas ADD PRIMARY KEY ( IdBitacora )
GO



ALTER TABLE prodmult.MF_ArticuloPrecioOferta ADD MotivoCancela VARCHAR(200) NULL
go
ALTER TABLE prodmult.MF_ArticuloPrecioOferta ADD UsuarioCancela UsernameType NULL
go
ALTER TABLE prodmult.MF_ArticuloPrecioOferta ALTER COLUMN FechaCancela CurrentDateType NULL
go


ALTER TABLE prodmult.MF_ArticuloNivelPrecioOferta ADD MotivoCancela VARCHAR(200) NULL
go
ALTER TABLE prodmult.MF_ArticuloNivelPrecioOferta ADD UsuarioCancela UsernameType NULL
go
ALTER TABLE prodmult.MF_ArticuloNivelPrecioOferta ALTER COLUMN FechaCancela CurrentDateType NULL
go


ALTER TABLE prodmult.MF_NivelPrecioCoeficienteOferta ADD MotivoCancela VARCHAR(200) NULL
go
ALTER TABLE prodmult.MF_NivelPrecioCoeficienteOferta ADD UsuarioCancela UsernameType NULL
go
ALTER TABLE prodmult.MF_NivelPrecioCoeficienteOferta ALTER COLUMN FechaCancela CurrentDateType NULL
go

-- Esta tabla contendr� las ofertas por Nivel de Precio en su detalle de articulos.
CREATE TABLE prodmult.MF_NivelPrecioCoeficienteOfertaDet (
	NivelPrecio			VARCHAR(12)				NOT NULL,
	FechaOfertaDesde	DATETIME				NOT NULL,
	Articulo			VARCHAR(20)				NOT NULL,
	PrecioOriginal		DECIMAL(28,8)			NOT NULL
)
GO

ALTER TABLE prodmult.MF_NivelPrecioCoeficienteOfertaDet ADD PRIMARY KEY ( NivelPrecio, FechaOfertaDesde, Articulo )
GO

ALTER TABLE prodmult.MF_NIVEL_PRECIO_COEFICIENTE ADD MODIFICADO CHAR(1) NULL
go
UPDATE prodmult.MF_NIVEL_PRECIO_COEFICIENTE SET MODIFICADO = 'N'
go

