USE EXACTUSERP
GO

-- Elimino esta tabla para poder crearla de nuevo con la estructura correcta.
DROP TABLE prodmult.MF_ARTICULO_PRECIO_OFERTA
GO

-- Esta tabla contendr� las ofertas por Art�culo.
CREATE TABLE prodmult.MF_ArticuloPrecioOferta (
	Articulo			VARCHAR(20)				NOT NULL,
	FechaOfertaDesde	DATETIME				NOT NULL,
	FechaOfertaHasta	DATETIME				NOT NULL,
	PrecioOriginal		DECIMAL(28,8)			NOT NULL,
	PrecioOferta		DECIMAL(28,8)			NOT NULL,
	Estatus				CHAR(1)					NOT NULL,	--P: Pendiente, V: Vigente, T: Terminada
	Descripcion			VARCHAR(100)				NULL,
	RecordDate			CurrentDateType			NOT NULL,
	CreatedBy			UsernameType			NOT NULL,
	UpdatedBy			UsernameType				NULL,
	CreateDate			CurrentDateType			NOT NULL,
	MotivoCancela		VARCHAR(200)				NULL,
	UsuarioCancela		UsernameType				NULL,
	FechaCancela		CurrentDateType				NULL
)
GO

ALTER TABLE prodmult.MF_ArticuloPrecioOferta ADD PRIMARY KEY ( Articulo, FechaOfertaDesde )
GO

-- Esta tabla contendr� las ofertas por Art�culo + Nivel de Precio.
CREATE TABLE prodmult.MF_ArticuloNivelPrecioOferta (
	NivelPrecio			VARCHAR(12)				NOT NULL,
	Articulo			VARCHAR(20)				NOT NULL,
	FechaOfertaDesde	DATETIME				NOT NULL,
	FechaOfertaHasta	DATETIME				NOT NULL,
	PrecioOriginal		DECIMAL(28,8)			NOT NULL,
	PrecioOferta		DECIMAL(28,8)			NOT NULL,
	Estatus				CHAR(1)					NOT NULL,	--P: Pendiente, V: Vigente, T: Terminada
	Descripcion			VARCHAR(100)				NULL,
	RecordDate			CurrentDateType			NOT NULL,
	CreatedBy			UsernameType			NOT NULL,
	UpdatedBy			UsernameType				NULL,
	CreateDate			CurrentDateType			NOT NULL,
	MotivoCancela		VARCHAR(200)				NULL,
	UsuarioCancela		UsernameType				NULL,
	FechaCancela		CurrentDateType				NULL
)
GO

ALTER TABLE prodmult.MF_ArticuloNivelPrecioOferta ADD PRIMARY KEY ( NivelPrecio, Articulo, FechaOfertaDesde )
GO

-- Esta tabla contendr� las ofertas por Nivel de Precio.
CREATE TABLE prodmult.MF_NivelPrecioCoeficienteOferta (
	NivelPrecio					VARCHAR(12)				NOT NULL,
	FechaOfertaDesde			DATETIME				NOT NULL,
	FechaOfertaHasta			DATETIME				NOT NULL,
	CoeficienteMulrOriginal		DECIMAL(18,6)			NOT NULL,
	CoeficienteMulrOferta		DECIMAL(18,6)			NOT NULL,
	Estatus						CHAR(1)					NOT NULL,	--P: Pendiente, V: Vigente, T: Terminada
	Descripcion					VARCHAR(100)				NULL,
	RecordDate					CurrentDateType			NOT NULL,
	CreatedBy					UsernameType			NOT NULL,
	UpdatedBy					UsernameType				NULL,
	CreateDate					CurrentDateType			NOT NULL,
	MotivoCancela				VARCHAR(200)				NULL,
	UsuarioCancela				UsernameType				NULL,
	FechaCancela				CurrentDateType				NULL
)
GO

ALTER TABLE prodmult.MF_NivelPrecioCoeficienteOferta ADD PRIMARY KEY ( NivelPrecio, FechaOfertaDesde )
GO

-- Esta tabla contendr� las ofertas por Nivel de Precio en su detalle de articulos.
CREATE TABLE prodmult.MF_NivelPrecioCoeficienteOfertaDet (
	Tipo				CHAR(1)					NOT NULL,
	NivelPrecio			VARCHAR(12)				NOT NULL,
	FechaOfertaDesde	DATETIME				NOT NULL,
	Articulo			VARCHAR(20)				NOT NULL,
	PrecioOriginal		DECIMAL(28,8)			NOT NULL
)
GO

ALTER TABLE prodmult.MF_NivelPrecioCoeficienteOfertaDet ADD PRIMARY KEY ( Tipo, NivelPrecio, FechaOfertaDesde, Articulo )
GO

CREATE TABLE prodmult.MF_NIVEL_PRECIO_COEFICIENTE (
	NIVEL_PRECIO		VARCHAR(12)				NOT NULL,
	MONEDA				VARCHAR(1)				NOT NULL,
	COEFICIENTE_MULR	DECIMAL(18,6)			NOT NULL,
	DESCRIPCION			VARCHAR(50)				NULL,
	MODIFICADO			CHAR(1)					NOT NULL	DEFAULT('N')
)
GO

ALTER TABLE prodmult.MF_NIVEL_PRECIO_COEFICIENTE ADD PRIMARY KEY ( NIVEL_PRECIO )
GO

ALTER TABLE prodmult.MF_NivelPrecioCoeficienteOferta  WITH CHECK ADD CONSTRAINT FK_MF_NivelPrecioCoeficienteOferta_MF_NIVEL_PRECIO_COEFICIENTE FOREIGN KEY(NivelPrecio)
REFERENCES prodmult.MF_NIVEL_PRECIO_COEFICIENTE (NIVEL_PRECIO)
GO
ALTER TABLE prodmult.MF_NivelPrecioCoeficienteOferta CHECK CONSTRAINT FK_MF_NivelPrecioCoeficienteOferta_MF_NIVEL_PRECIO_COEFICIENTE
GO


-- Esta tabla contendr� los usuarios que tienen permisos para cancelar ofertas.
CREATE TABLE prodmult.MF_UsuariosCancelanOfertas (
	usuario						VARCHAR(30)		NOT NULL
)
GO

ALTER TABLE prodmult.MF_UsuariosCancelanOfertas ADD PRIMARY KEY ( usuario )
GO


-- Esta tabla contendr� la bit�cora del resultado de los procesos que se ejecutar�n en las noches y madrugadas.
-- Aqu� se podr� consultar si hubo alg�n error.
CREATE TABLE prodmult.MF_BitacoraPreciosOfertas (
	IdBitacora					INT	IDENTITY(1,1)		NOT NULL,
	Tipo						CHAR(1)					NOT NULL,	--P: Actualizaci�n de Precios; I: Inicio de Ofertas; F: Fin de Ofertas; 
	Resultado					CHAR(1)					NOT NULL,	--S: Success; F: Failure; 
	Descripcion					VARCHAR(1000)			NOT NULL,
	RecordDate					CurrentDateType			NOT NULL,
	CreatedBy					UsernameType			NOT NULL
)
GO

ALTER TABLE prodmult.MF_BitacoraPreciosOfertas ADD PRIMARY KEY ( IdBitacora )
GO

