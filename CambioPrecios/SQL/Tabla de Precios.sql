USE master
go

CREATE DATABASE [precios] ON  PRIMARY 
( NAME = N'precios', FILENAME = N'C:\DB\precios.mdf' , SIZE = 3072KB , FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'precios_log', FILENAME = N'C:\DB\precios_log.ldf' , SIZE = 1024KB , FILEGROWTH = 10%)
GO
ALTER DATABASE [precios] SET COMPATIBILITY_LEVEL = 100
GO
ALTER DATABASE [precios] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [precios] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [precios] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [precios] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [precios] SET ARITHABORT OFF 
GO
ALTER DATABASE [precios] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [precios] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [precios] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [precios] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [precios] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [precios] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [precios] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [precios] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [precios] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [precios] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [precios] SET  DISABLE_BROKER 
GO
ALTER DATABASE [precios] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [precios] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [precios] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [precios] SET  READ_WRITE 
GO
ALTER DATABASE [precios] SET RECOVERY FULL 
GO
ALTER DATABASE [precios] SET  MULTI_USER 
GO
ALTER DATABASE [precios] SET PAGE_VERIFY CHECKSUM  
GO

USE [precios]
GO
IF NOT EXISTS (SELECT name FROM sys.filegroups WHERE is_default=1 AND name = N'PRIMARY') ALTER DATABASE [precios] MODIFY FILEGROUP [PRIMARY] DEFAULT
GO

CREATE TYPE FlagNyType FROM tinyint NOT NULL
GO
CREATE TYPE CurrentDateType FROM datetime NOT NULL
GO
CREATE TYPE RowPointerType FROM uniqueidentifier NOT NULL
GO
CREATE TYPE UsernameType FROM varchar(30) NOT NULL
GO

CREATE SCHEMA [prodmult] AUTHORIZATION [db_owner]
GO

/****** Object:  Table [prodmult].[MF_NIVEL_PRECIO_COEFICIENTE]    Script Date: 03/02/2012 09:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [prodmult].[MF_NIVEL_PRECIO_COEFICIENTE](
	[NIVEL_PRECIO] [varchar](12) NOT NULL,
	[MONEDA] [varchar](1) NOT NULL,
	[COEFICIENTE_MULR] [decimal](18, 6) NOT NULL,
	[DESCRIPCION] [varchar](50) NULL,
 CONSTRAINT [PK_MF_NIVEL_PRECIO_COEFICIENTE] PRIMARY KEY CLUSTERED 
(
	[NIVEL_PRECIO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [prodmult].[MF_ArticuloPrecioOferta]    Script Date: 03/02/2012 09:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [prodmult].[MF_ArticuloPrecioOferta](
	[Articulo] [varchar](20) NOT NULL,
	[FechaOfertaDesde] [datetime] NOT NULL,
	[FechaOfertaHasta] [datetime] NOT NULL,
	[PrecioOriginal] [decimal](28, 8) NOT NULL,
	[PrecioOferta] [decimal](28, 8) NOT NULL,
	[Estatus] [char](1) NOT NULL,
	[Descripcion] [varchar](50) NULL,
	[RecordDate] [dbo].[CurrentDateType] NOT NULL,
	[CreatedBy] [dbo].[UsernameType] NOT NULL,
	[UpdatedBy] [dbo].[UsernameType] NOT NULL,
	[CreateDate] [dbo].[CurrentDateType] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Articulo] ASC,
	[FechaOfertaDesde] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [prodmult].[MF_ArticuloNivelPrecioOferta]    Script Date: 03/02/2012 09:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [prodmult].[MF_ArticuloNivelPrecioOferta](
	[NivelPrecio] [varchar](12) NOT NULL,
	[Articulo] [varchar](20) NOT NULL,
	[FechaOfertaDesde] [datetime] NOT NULL,
	[FechaOfertaHasta] [datetime] NOT NULL,
	[PrecioOriginal] [decimal](28, 8) NOT NULL,
	[PrecioOferta] [decimal](28, 8) NOT NULL,
	[Estatus] [char](1) NOT NULL,
	[Descripcion] [varchar](50) NULL,
	[RecordDate] [dbo].[CurrentDateType] NOT NULL,
	[CreatedBy] [dbo].[UsernameType] NOT NULL,
	[UpdatedBy] [dbo].[UsernameType] NOT NULL,
	[CreateDate] [dbo].[CurrentDateType] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[NivelPrecio] ASC,
	[Articulo] ASC,
	[FechaOfertaDesde] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [prodmult].[NIVEL_PRECIO]    Script Date: 03/02/2012 09:52:07 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [prodmult].[NIVEL_PRECIO](
	[NIVEL_PRECIO] [varchar](12) NOT NULL,
	[MONEDA] [varchar](1) NOT NULL,
	[CONDICION_PAGO] [varchar](4) NULL,
	[ESQUEMA_TRABAJO] [varchar](1) NOT NULL,
	[DESCUENTOS] [varchar](1) NOT NULL,
	[SUGERIR_DESCUENTO] [varchar](1) NOT NULL,
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL,
	[RecordDate] [dbo].[CurrentDateType] NOT NULL,
	[RowPointer] [dbo].[RowPointerType] NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [prodmult].[NIVEL_PRECIO] ADD [CreatedBy] [dbo].[UsernameType] NOT NULL
ALTER TABLE [prodmult].[NIVEL_PRECIO] ADD [UpdatedBy] [dbo].[UsernameType] NOT NULL
ALTER TABLE [prodmult].[NIVEL_PRECIO] ADD [CreateDate] [dbo].[CurrentDateType] NOT NULL
ALTER TABLE [prodmult].[NIVEL_PRECIO] ADD  CONSTRAINT [NIVEL_PRECIOPK] PRIMARY KEY NONCLUSTERED 
(
	[NIVEL_PRECIO] ASC,
	[MONEDA] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
ALTER TABLE [prodmult].[NIVEL_PRECIO] ADD  CONSTRAINT [NIVEL_PRECIORPIx] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [prodmult].[ARTICULO]    Script Date: 03/02/2012 09:52:07 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [prodmult].[ARTICULO](
	[ARTICULO] [varchar](20) NOT NULL,
	[PLANTILLA_SERIE] [varchar](4) NULL,
	[DESCRIPCION] [varchar](254) NOT NULL,
	[CLASIFICACION_1] [varchar](12) NULL,
	[CLASIFICACION_2] [varchar](12) NULL,
	[CLASIFICACION_3] [varchar](12) NULL,
	[CLASIFICACION_4] [varchar](12) NULL,
	[CLASIFICACION_5] [varchar](12) NULL,
	[CLASIFICACION_6] [varchar](12) NULL,
	[FACTOR_CONVER_1] [decimal](28, 8) NULL,
	[FACTOR_CONVER_2] [decimal](28, 8) NULL,
	[FACTOR_CONVER_3] [decimal](28, 8) NULL,
	[FACTOR_CONVER_4] [decimal](28, 8) NULL,
	[FACTOR_CONVER_5] [decimal](28, 8) NULL,
	[FACTOR_CONVER_6] [decimal](28, 8) NULL,
	[TIPO] [varchar](1) NOT NULL,
	[ORIGEN_CORP] [varchar](1) NOT NULL,
	[PESO_NETO] [decimal](28, 8) NOT NULL,
	[PESO_BRUTO] [decimal](28, 8) NOT NULL,
	[VOLUMEN] [decimal](28, 8) NOT NULL,
	[BULTOS] [smallint] NOT NULL,
	[ARTICULO_CUENTA] [varchar](4) NOT NULL,
	[IMPUESTO] [varchar](4) NOT NULL,
	[FACTOR_EMPAQUE] [decimal](28, 8) NOT NULL,
	[FACTOR_VENTA] [decimal](28, 8) NOT NULL,
	[EXISTENCIA_MINIMA] [decimal](28, 8) NOT NULL,
	[EXISTENCIA_MAXIMA] [decimal](28, 8) NOT NULL,
	[PUNTO_DE_REORDEN] [decimal](28, 8) NOT NULL,
	[COSTO_FISCAL] [varchar](1) NOT NULL,
	[COSTO_COMPARATIVO] [varchar](1) NOT NULL,
	[COSTO_PROM_LOC] [decimal](28, 8) NOT NULL,
	[COSTO_PROM_DOL] [decimal](28, 8) NOT NULL,
	[COSTO_STD_LOC] [decimal](28, 8) NOT NULL,
	[COSTO_STD_DOL] [decimal](28, 8) NOT NULL,
	[COSTO_ULT_LOC] [decimal](28, 8) NOT NULL,
	[COSTO_ULT_DOL] [decimal](28, 8) NOT NULL,
	[PRECIO_BASE_LOCAL] [decimal](28, 8) NOT NULL,
	[PRECIO_BASE_DOLAR] [decimal](28, 8) NOT NULL,
	[ULTIMA_SALIDA] [datetime] NOT NULL,
	[ULTIMO_MOVIMIENTO] [datetime] NOT NULL,
	[ULTIMO_INGRESO] [datetime] NOT NULL,
	[ULTIMO_INVENTARIO] [datetime] NOT NULL,
	[CLASE_ABC] [varchar](1) NOT NULL,
	[FRECUENCIA_CONTEO] [smallint] NOT NULL,
	[CODIGO_BARRAS_VENT] [varchar](20) NULL,
	[CODIGO_BARRAS_INVT] [varchar](20) NULL,
	[ACTIVO] [varchar](1) NOT NULL,
	[USA_LOTES] [varchar](1) NOT NULL,
	[OBLIGA_CUARENTENA] [varchar](1) NOT NULL,
	[MIN_VIDA_COMPRA] [smallint] NOT NULL,
	[MIN_VIDA_CONSUMO] [smallint] NOT NULL,
	[MIN_VIDA_VENTA] [smallint] NOT NULL,
	[VIDA_UTIL_PROM] [smallint] NOT NULL,
	[DIAS_CUARENTENA] [smallint] NOT NULL,
	[PROVEEDOR] [varchar](20) NULL,
	[ARTICULO_DEL_PROV] [varchar](20) NULL,
	[ORDEN_MINIMA] [decimal](28, 8) NOT NULL,
	[PLAZO_REABAST] [smallint] NOT NULL,
	[LOTE_MULTIPLO] [decimal](28, 8) NOT NULL,
	[NOTAS] [text] NULL,
	[UTILIZADO_MANUFACT] [varchar](1) NOT NULL,
	[USUARIO_CREACION] [varchar](10) NULL,
	[FCH_HORA_CREACION] [datetime] NULL,
	[USUARIO_ULT_MODIF] [varchar](10) NULL,
	[FCH_HORA_ULT_MODIF] [datetime] NULL,
	[USA_NUMEROS_SERIE] [varchar](1) NOT NULL,
	[MODALIDAD_INV_FIS] [varchar](1) NULL,
	[TIPO_COD_BARRA_DET] [varchar](1) NULL,
	[TIPO_COD_BARRA_ALM] [varchar](1) NULL,
	[USA_REGLAS_LOCALES] [varchar](1) NULL,
	[UNIDAD_ALMACEN] [varchar](6) NOT NULL,
	[UNIDAD_EMPAQUE] [varchar](6) NOT NULL,
	[UNIDAD_VENTA] [varchar](6) NOT NULL,
	[PERECEDERO] [varchar](1) NOT NULL,
	[GTIN] [varchar](13) NULL,
	[MANUFACTURADOR] [varchar](35) NULL,
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL,
	[RecordDate] [dbo].[CurrentDateType] NOT NULL,
	[RowPointer] [dbo].[RowPointerType] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [prodmult].[ARTICULO] ADD [CreatedBy] [dbo].[UsernameType] NOT NULL
ALTER TABLE [prodmult].[ARTICULO] ADD [UpdatedBy] [dbo].[UsernameType] NOT NULL
ALTER TABLE [prodmult].[ARTICULO] ADD [CreateDate] [dbo].[CurrentDateType] NOT NULL
ALTER TABLE [prodmult].[ARTICULO] ADD [CODIGO_RETENCION] [varchar](4) NULL
ALTER TABLE [prodmult].[ARTICULO] ADD [RETENCION_VENTA] [varchar](4) NULL
ALTER TABLE [prodmult].[ARTICULO] ADD [RETENCION_COMPRA] [varchar](4) NULL
ALTER TABLE [prodmult].[ARTICULO] ADD  CONSTRAINT [ARTICULOPK] PRIMARY KEY NONCLUSTERED 
(
	[ARTICULO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
ALTER TABLE [prodmult].[ARTICULO] ADD  CONSTRAINT [ARTICULORPIx] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [prodmult].[ARTICULO_PRECIO]    Script Date: 03/02/2012 09:52:07 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [prodmult].[ARTICULO_PRECIO](
	[NIVEL_PRECIO] [varchar](12) NOT NULL,
	[MONEDA] [varchar](1) NOT NULL,
	[VERSION] [int] NOT NULL,
	[ARTICULO] [varchar](20) NOT NULL,
	[VERSION_ARTICULO] [int] NOT NULL,
	[PRECIO] [decimal](28, 8) NOT NULL,
	[ESQUEMA_TRABAJO] [varchar](1) NOT NULL,
	[MARGEN_MULR] [decimal](28, 8) NULL,
	[MARGEN_UTILIDAD] [decimal](28, 8) NOT NULL,
	[FECHA_INICIO] [datetime] NOT NULL,
	[FECHA_FIN] [datetime] NOT NULL,
	[FECHA_ULT_MODIF] [datetime] NULL,
	[USUARIO_ULT_MODIF] [varchar](10) NULL,
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL,
	[RecordDate] [dbo].[CurrentDateType] NOT NULL,
	[RowPointer] [dbo].[RowPointerType] NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [prodmult].[ARTICULO_PRECIO] ADD [CreatedBy] [dbo].[UsernameType] NOT NULL
ALTER TABLE [prodmult].[ARTICULO_PRECIO] ADD [UpdatedBy] [dbo].[UsernameType] NOT NULL
ALTER TABLE [prodmult].[ARTICULO_PRECIO] ADD [CreateDate] [dbo].[CurrentDateType] NOT NULL
ALTER TABLE [prodmult].[ARTICULO_PRECIO] ADD  CONSTRAINT [XPKARTICULO_PRECIO] PRIMARY KEY NONCLUSTERED 
(
	[NIVEL_PRECIO] ASC,
	[MONEDA] ASC,
	[VERSION] ASC,
	[ARTICULO] ASC,
	[VERSION_ARTICULO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
ALTER TABLE [prodmult].[ARTICULO_PRECIO] ADD  CONSTRAINT [ARTICULO_PRECIORPIx] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Default [DF__ARTICULO__NoteEx__72D1C499]    Script Date: 03/02/2012 09:52:07 ******/
ALTER TABLE [prodmult].[ARTICULO] ADD  DEFAULT ((0)) FOR [NoteExistsFlag]
GO
/****** Object:  Default [DF__ARTICULO__Record__73C5E8D2]    Script Date: 03/02/2012 09:52:07 ******/
ALTER TABLE [prodmult].[ARTICULO] ADD  DEFAULT (getdate()) FOR [RecordDate]
GO
/****** Object:  Default [DF__ARTICULO__RowPoi__74BA0D0B]    Script Date: 03/02/2012 09:52:07 ******/
ALTER TABLE [prodmult].[ARTICULO] ADD  DEFAULT (newid()) FOR [RowPointer]
GO
/****** Object:  Default [DF__ARTICULO__Create__75AE3144]    Script Date: 03/02/2012 09:52:07 ******/
ALTER TABLE [prodmult].[ARTICULO] ADD  DEFAULT (suser_sname()) FOR [CreatedBy]
GO
/****** Object:  Default [DF__ARTICULO__Update__76A2557D]    Script Date: 03/02/2012 09:52:07 ******/
ALTER TABLE [prodmult].[ARTICULO] ADD  DEFAULT (suser_sname()) FOR [UpdatedBy]
GO
/****** Object:  Default [DF__ARTICULO__Create__779679B6]    Script Date: 03/02/2012 09:52:07 ******/
ALTER TABLE [prodmult].[ARTICULO] ADD  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF__ARTICULO___NoteE__377BF4A1]    Script Date: 03/02/2012 09:52:07 ******/
ALTER TABLE [prodmult].[ARTICULO_PRECIO] ADD  DEFAULT ((0)) FOR [NoteExistsFlag]
GO
/****** Object:  Default [DF__ARTICULO___Recor__387018DA]    Script Date: 03/02/2012 09:52:07 ******/
ALTER TABLE [prodmult].[ARTICULO_PRECIO] ADD  DEFAULT (getdate()) FOR [RecordDate]
GO
/****** Object:  Default [DF__ARTICULO___RowPo__39643D13]    Script Date: 03/02/2012 09:52:07 ******/
ALTER TABLE [prodmult].[ARTICULO_PRECIO] ADD  DEFAULT (newid()) FOR [RowPointer]
GO
/****** Object:  Default [DF__ARTICULO___Creat__3A58614C]    Script Date: 03/02/2012 09:52:07 ******/
ALTER TABLE [prodmult].[ARTICULO_PRECIO] ADD  DEFAULT (suser_sname()) FOR [CreatedBy]
GO
/****** Object:  Default [DF__ARTICULO___Updat__3B4C8585]    Script Date: 03/02/2012 09:52:07 ******/
ALTER TABLE [prodmult].[ARTICULO_PRECIO] ADD  DEFAULT (suser_sname()) FOR [UpdatedBy]
GO
/****** Object:  Default [DF__ARTICULO___Creat__3C40A9BE]    Script Date: 03/02/2012 09:52:07 ******/
ALTER TABLE [prodmult].[ARTICULO_PRECIO] ADD  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_MF_NIVEL_PRECIO_COEFICIENTE_MONEDA]    Script Date: 03/02/2012 09:52:07 ******/
ALTER TABLE [prodmult].[MF_NIVEL_PRECIO_COEFICIENTE] ADD  CONSTRAINT [DF_MF_NIVEL_PRECIO_COEFICIENTE_MONEDA]  DEFAULT ('L') FOR [MONEDA]
GO
/****** Object:  Default [DF__NIVEL_PRE__NoteE__27D06814]    Script Date: 03/02/2012 09:52:07 ******/
ALTER TABLE [prodmult].[NIVEL_PRECIO] ADD  DEFAULT ((0)) FOR [NoteExistsFlag]
GO
/****** Object:  Default [DF__NIVEL_PRE__Recor__28C48C4D]    Script Date: 03/02/2012 09:52:07 ******/
ALTER TABLE [prodmult].[NIVEL_PRECIO] ADD  DEFAULT (getdate()) FOR [RecordDate]
GO
/****** Object:  Default [DF__NIVEL_PRE__RowPo__29B8B086]    Script Date: 03/02/2012 09:52:07 ******/
ALTER TABLE [prodmult].[NIVEL_PRECIO] ADD  DEFAULT (newid()) FOR [RowPointer]
GO
/****** Object:  Default [DF__NIVEL_PRE__Creat__2AACD4BF]    Script Date: 03/02/2012 09:52:07 ******/
ALTER TABLE [prodmult].[NIVEL_PRECIO] ADD  DEFAULT (suser_sname()) FOR [CreatedBy]
GO
/****** Object:  Default [DF__NIVEL_PRE__Updat__2BA0F8F8]    Script Date: 03/02/2012 09:52:07 ******/
ALTER TABLE [prodmult].[NIVEL_PRECIO] ADD  DEFAULT (suser_sname()) FOR [UpdatedBy]
GO
/****** Object:  Default [DF__NIVEL_PRE__Creat__2C951D31]    Script Date: 03/02/2012 09:52:07 ******/
ALTER TABLE [prodmult].[NIVEL_PRECIO] ADD  DEFAULT (getdate()) FOR [CreateDate]
GO
