CREATE TABLE MF_NIVEL_PRECIO (
	usuario			VARCHAR(30)		NOT NULL	DEFAULT(suser_name()),
	fechaRegistro	DATETIME		NOT NULL	DEFAULT(getDate()),
	pcRegistro		VARCHAR(50)		NOT NULL	DEFAULT(host_name())
)
go

SELECT top 50 articulo, nivel_precio, precio, * FROM prodmult.ARTICULO_PRECIO
where articulo = '511317-017'
ORDER BY nivel_precio

use fiestanet
go
select * from tienda

--Timestamp

SELECT PRECIO_BASE_LOCAL, * FROM prodmult.ARTICULO
ORDER BY ARTICULO

SELECT ARTICULO AS Articulo, DESCRIPCION AS Descripción, PRECIO_BASE_LOCAL AS PrecioLista FROM prodmult.ARTICULO ORDER BY ARTICULO

INSERT INTO prodmult.MF_ArticuloPrecioOferta ( Articulo, FechaOfertaDesde, FechaOfertaHasta, PrecioOriginal, PrecioOferta, Estatus, Descripcion, RecordDate, CreatedBy )
VALUES ( '12345', '2008/01/01', '2008/01/01', 1, 2, 'P', 'Descripcion varios', GETDATE(), SUSER_NAME() )

SELECT COUNT(*) FROM prodmult.MF_ArticuloPrecioOferta WHERE Estatus = 'P' AND Articulo = '135105-045' AND (('2012/03/07' BETWEEN FechaOfertaDesde AND FechaOfertaHasta) OR ('2012/03/15' BETWEEN FechaOfertaDesde AND FechaOfertaHasta))
 
SELECT * FROM prodmult.MF_ArticuloPrecioOferta

SELECT COUNT(*) FROM prodmult.MF_ArticuloPrecioOferta

SELECT * FROM prodmult.NIVEL_PRECIO
ORDER BY NIVEL_PRECIO

SELECT * FROM prodmult.MF_NivelPrecioCoeficienteOferta

SELECT * FROM prodmult.MF_ArticuloNivelPrecioOferta
SELECT * FROM prodmult.MF_UsuariosCancelanOfertas

INSERT INTO prodmult.MF_BitacoraPreciosOfertas ( Tipo, Resultado, Descripcion, RecordDate, CreatedBy )
VALUES ( 'P', 'S', 'Hubo un error ahi.', '2012/01/01', 'rop' )

SELECT * FROM prodmult.ARTICULO_PRECIO 
WHERE ARTICULO = '131002-001'
AND NIVEL_PRECIO = 'BI-Facil-03'
--Error al actualizar los precios. - The transaction associated with the current connection has completed but has not been disposed.  The transaction must be disposed before the connection can be used to execute SQL statements. - Art 010310-000; aquiii Niv ValoresIn03M; ii 63; jj 57
021001-000  PlanFacMC04M ii84 jj40
SELECT * FROM prodmult.MF_BitacoraPreciosOfertas
where RecordDate >= '2012/03/14' AND RecordDate <= '2012/03/15'

UPDATE a SET a.PRECIO = (SELECT c.PRECIO FROM prodmult.ARTICULO_PRECIO c WHERE c.ARTICULO = '021001-000' AND c.VERSION = 1 AND c.NIVEL_PRECIO = ' PRECIOLISTA') * b.COEFICIENTE_MULR
FROM prodmult.ARTICULO_PRECIO a JOIN prodmult.MF_NIVEL_PRECIO_COEFICIENTE b ON a.NIVEL_PRECIO = b.NIVEL_PRECIO
WHERE a.ARTICULO = '021001-000' AND a.NIVEL_PRECIO <> ' PRECIOLISTA'

SELECT * FROM prodmult.ARTICULO ORDER BY ARTICULO

SELECT ARTICULO, NIVEL_PRECIO, PRECIO FROM prodmult.ARTICULO_PRECIO WHERE VERSION = 1 ORDER BY ARTICULO, NIVEL_PRECIO

SELECT COEFICIENTE_MULR FROM prodmult.MF_NIVEL_PRECIO_COEFICIENTE WHERE NIVEL_PRECIO = '123456'

SELECT * FROM prodmult.ARTICULO a JOIN prodmult.ARTICULO_PRECIO b ON a.ARTICULO = b.ARTICULO
WHERE b.NIVEL_PRECIO = ' PRECIOLISTA' AND a.PRECIO_BASE_LOCAL <> b.PRECIO
--5409.82140000

SELECT * FROM prodmult.MF_ArticuloPrecioOferta

SELECT * FROM prodmult.MF_NivelPrecioCoeficienteOferta

SELECT * FROM prodmult.MF_ArticuloNivelPrecioOferta

SELECT 'Por Artículo' AS Tipo, prodmult.MF_ArticuloPrecioOferta.Articulo AS Articulo, prodmult.ARTICULO.DESCRIPCION AS NombreArticulo, '' AS NivelPrecio, prodmult.MF_ArticuloPrecioOferta.FechaOfertaDesde AS FechaInicial, prodmult.MF_ArticuloPrecioOferta.FechaOfertaHasta AS FechaFinal, 
prodmult.MF_ArticuloPrecioOferta.PrecioOriginal AS PrecioOriginal, prodmult.MF_ArticuloPrecioOferta.PrecioOferta AS PrecioOferta, 
(CASE prodmult.MF_ArticuloPrecioOferta.Estatus WHEN 'P' THEN 'Pendiente' WHEN 'V' THEN 'Vigente' WHEN 'C' THEN 'Canceladas' ELSE 'Terminadas' END) AS Estatus, 
prodmult.MF_ArticuloPrecioOferta.Descripcion AS DescripcionOferta, prodmult.MF_ArticuloPrecioOferta.CreatedBy AS UsuarioCreo, prodmult.MF_ArticuloPrecioOferta.RecordDate AS FechaCreada, 'A' AS Tip 
FROM prodmult.MF_ArticuloPrecioOferta JOIN prodmult.ARTICULO ON prodmult.MF_ArticuloPrecioOferta.Articulo = prodmult.ARTICULO.ARTICULO 
UNION 
SELECT 'Por Nivel de Precio' AS Tipo, '' AS Articulo, '' AS NombreArticulo, prodmult.MF_NivelPrecioCoeficienteOferta.NivelPrecio AS NivelPrecio, prodmult.MF_NivelPrecioCoeficienteOferta.FechaOfertaDesde AS FechaInicial, prodmult.MF_NivelPrecioCoeficienteOferta.FechaOfertaHasta AS FechaFinal, 
prodmult.MF_NivelPrecioCoeficienteOferta.CoeficienteMulrOriginal AS PrecioOriginal, prodmult.MF_NivelPrecioCoeficienteOferta.CoeficienteMulrOferta AS PrecioOferta, 
(CASE prodmult.MF_NivelPrecioCoeficienteOferta.Estatus WHEN 'P' THEN 'Pendiente' WHEN 'V' THEN 'Vigente' WHEN 'C' THEN 'Canceladas' ELSE 'Terminadas' END) AS Estatus, 
prodmult.MF_NivelPrecioCoeficienteOferta.Descripcion AS DescripcionOferta, prodmult.MF_NivelPrecioCoeficienteOferta.CreatedBy AS UsuarioCreo, prodmult.MF_NivelPrecioCoeficienteOferta.RecordDate AS FechaCreada, 'N' AS Tip 
FROM prodmult.MF_NivelPrecioCoeficienteOferta 
UNION 
SELECT 'Por Artículo + Nivel de Precio' AS Tipo, prodmult.MF_ArticuloNivelPrecioOferta.Articulo AS Articulo, prodmult.ARTICULO.DESCRIPCION AS NombreArticulo, 
NivelPrecio, FechaOfertaDesde AS FechaInicial, FechaOfertaHasta AS FechaFinal, prodmult.MF_ArticuloNivelPrecioOferta.PrecioOriginal AS PrecioOriginal, 
prodmult.MF_ArticuloNivelPrecioOferta.PrecioOferta AS PrecioOferta, (CASE prodmult.MF_ArticuloNivelPrecioOferta.Estatus WHEN 'P' THEN 'Pendiente' WHEN 'V' THEN 'Vigente' WHEN 'C' THEN 'Canceladas' ELSE 'Terminadas' END) AS Estatus, 
prodmult.MF_ArticuloNivelPrecioOferta.Descripcion AS DescripcionOferta, prodmult.MF_ArticuloNivelPrecioOferta.CreatedBy AS UsuarioCreo, prodmult.MF_ArticuloNivelPrecioOferta.RecordDate AS FechaCreada, 'P' AS Tip 
FROM prodmult.MF_ArticuloNivelPrecioOferta JOIN prodmult.ARTICULO ON prodmult.MF_ArticuloNivelPrecioOferta.Articulo = prodmult.ARTICULO.ARTICULO 
ORDER BY FechaInicial DESC 


DECLARE	@FechaInicial AS DATETIME
DECLARE	@FechaFinal AS DATETIME
DECLARE	@Tipo AS VARCHAR(30)
DECLARE	@Estatus AS CHAR(1)
DECLARE	@Articulo AS VARCHAR(20)
DECLARE	@NivelPrecio AS VARCHAR(12)
DECLARE	@NombreArticulo AS VARCHAR(254)
DECLARE	@Descripcion AS VARCHAR(50)

SET @FechaInicial = '2012/03/01'
SET @FechaFinal = '3099/03/31'
SET @Tipo = 'T'
SET @Estatus = 'T'
SET @Articulo = '%'
SET @NivelPrecio = '%'
SET @NombreArticulo = '%'
SET @Descripcion = '%'

EXEC spDevuelveOfertas @FechaInicial, @FechaFinal, @Tipo, @Estatus, @Articulo, @NivelPrecio, @NombreArticulo, @Descripcion

SELECT (CASE Tipo WHEN 'P' THEN 'Actualización de Precios' WHEN 'I' THEN 'Inicio de Ofertas' ELSE 'Fin de Ofertas' END) AS Tipo,  
(CASE Resultado WHEN 'S' THEN 'Exito' ELSE 'Error' END) AS Resultado, Descripcion, RecordDate AS FechaRegistro, CreatedBy AS UsuarioCreo 
FROM prodmult.MF_BitacoraPreciosOfertas ORDER BY RecordDate DESC 

SELECT * FROM prodmult.ARTICULO
--where ARTICULO = '141113-004' or ARTICULO = '141113-018'
--where ARTICULO = '111101-001'
--where ARTICULO = '114105-001'
--where ARTICULO = '116101-001'
where ARTICULO = '122003-025'
SELECT TOP 1 MARGEN_MULR FROM prodmult.ARTICULO_PRECIO WHERE NIVEL_PRECIO = 'CrediExTa18M'
SELECT * FROM prodmult.ARTICULO_PRECIO 
--where ARTICULO = '141113-004' or ARTICULO = '141113-018'
--where ARTICULO = '111101-001'
--where ARTICULO = '141061-002'
--where ARTICULO = '141071-011'
--where NIVEL_PRECIO = 'CrediExTa18M'
--where NIVEL_PRECIO = 'Bi-Facil-24M'
--where NIVEL_PRECIO = 'Bi-Facil-18M'
--where NIVEL_PRECIO = 'BI-Facil-12M' AND ARTICULO = '141071-011'
--where ARTICULO = '114105-001'
--where ARTICULO = '116101-001'
--where ARTICULO = '122003-025'
--where ARTICULO = '122003-025' AND NIVEL_PRECIO = 'InterCons24M'
order by ARTICULO, NIVEL_PRECIO

EXEC spIniciaOfertas
EXEC spFinalizaOfertas

select * from prodmult.MF_NivelPrecioCoeficienteOfertaDet
SELECT Estatus FROM prodmult.MF_ArticuloPrecioOferta WHERE Articulo = '123' AND FechaOfertaDesde = '2012/03/19'
SELECT PRECIO FROM prodmult.ARTICULO_PRECIO WHERE ARTICULO = '020003-000' AND NIVEL_PRECIO = ' PRECIOLISTA' AND VERSION = 1

select * from prodmult.ARTICULO_PRECIO where precio is null

DECLARE @dia AS INT
DECLARE @mes AS INT
DECLARE @anio AS INT

SET @dia = (SELECT DAY(getDate()))
SET @mes = (SELECT MONTH(getDate()))
SET @anio = (SELECT YEAR(getDate()))

	UPDATE prodmult.MF_NivelPrecioCoeficienteOferta SET Estatus = 'F'
	WHERE Estatus = 'V' AND DAY(FechaOfertaHasta) = @dia AND MONTH(FechaOfertaHasta) = @mes 
	AND YEAR(FechaOfertaHasta) = @anio

SELECT NivelPrecio, CoeficienteMulrOriginal FROM prodmult.MF_NivelPrecioCoeficienteOferta 
WHERE Estatus = 'V' AND DAY(FechaOfertaHasta) = @dia AND MONTH(FechaOfertaHasta) = @mes 
AND YEAR(FechaOfertaHasta) = @anio ORDER BY NivelPrecio

SELECT a.ARTICULO, a.DESCRIPCION, a.PRECIO_BASE_LOCAL, (SELECT PRECIO FROM prodmult.ARTICULO_PRECIO b WHERE b.ARTICULO = a.ARTICULO AND b.NIVEL_PRECIO = ' PRECIOLISTA' AND VERSION = 1) AS PRECIOLISTA FROM prodmult.ARTICULO a
WHERE (SELECT PRECIO FROM prodmult.ARTICULO_PRECIO b WHERE b.ARTICULO = a.ARTICULO AND b.NIVEL_PRECIO = ' PRECIOLISTA' AND VERSION = 1) <> A.PRECIO_BASE_LOCAL
ORDER BY a.ARTICULO


SELECT * FROM prodmult.ARTICULO
--where ARTICULO = '141113-004' or ARTICULO = '141113-018'
--where ARTICULO = '111101-001' --Usa Precio Lista Version 2
--where ARTICULO = '141061-002'
--where ARTICULO = '141071-011'
where articulo = '111102-048'

--SELECT PRECIO FROM prodmult.ARTICULO_PRECIO WHERE NIVEL_PRECIO = 'CrediExTa18M' AND ARTICULO = '111101-001' AND VERSION = 1

SELECT * FROM prodmult.ARTICULO_PRECIO 
--where ARTICULO = '141113-004' or ARTICULO = '141113-018'
--where ARTICULO = '111101-001'
--where ARTICULO = '141061-002'
--where ARTICULO = '141071-011'
--where ARTICULO = '141071-011'
where articulo = '111102-048'
--where NIVEL_PRECIO = 'CrediExTa18M'
--where NIVEL_PRECIO = 'BI-Facil-12M' AND ARTICULO = '141071-011'
order by ARTICULO, NIVEL_PRECIO
