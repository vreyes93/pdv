USE EXACTUSERP
GO

CREATE PROCEDURE spCambiaPrecios
AS
BEGIN

DECLARE @NivelPrecio AS VARCHAR(12)
DECLARE @Coeficiente AS DECIMAL(18,6)

BEGIN TRANSACTION
BEGIN TRY

--Cursor de Coeficientes de los niveles de precio
DECLARE #CursorCoeficientes CURSOR LOCAL FOR
SELECT NIVEL_PRECIO, COEFICIENTE_MULR FROM prodmult.MF_NIVEL_PRECIO_COEFICIENTE WHERE MODIFICADO = 'S' ORDER BY NIVEL_PRECIO 

OPEN #CursorCoeficientes
FETCH NEXT FROM #CursorCoeficientes INTO @NivelPrecio, @Coeficiente
	
WHILE @@FETCH_STATUS = 0
BEGIN
	UPDATE a SET a.PRECIO = b.PRECIO_BASE_LOCAL * @Coeficiente, a.MARGEN_MULR = @Coeficiente 
	FROM prodmult.ARTICULO_PRECIO a JOIN prodmult.ARTICULO b ON a.ARTICULO = b.ARTICULO 
	WHERE a.NIVEL_PRECIO = @NivelPrecio 
	
	--Fetch Cursor de Coeficientes de los niveles de precio
	FETCH NEXT FROM #CursorCoeficientes INTO @NivelPrecio, @Coeficiente
END
CLOSE #CursorCoeficientes
DEALLOCATE #CursorCoeficientes

UPDATE prodmult.MF_NIVEL_PRECIO_COEFICIENTE SET MODIFICADO = 'N'

INSERT INTO prodmult.MF_BitacoraPreciosOfertas ( Tipo, Resultado, Descripcion, RecordDate, CreatedBy )
VALUES ( 'P', 'S', 'Los precios fueron actualizados exitosamente.', GETDATE(), SUSER_NAME())

END TRY

BEGIN CATCH
	IF @@TRANCOUNT > 0 
	BEGIN
		ROLLBACK TRANSACTION
		INSERT INTO prodmult.MF_BitacoraPreciosOfertas ( Tipo, Resultado, Descripcion, RecordDate, CreatedBy )
		VALUES ( 'P', 'F', 'Se produjo error al actualizar los precos.  L�nea ' + CONVERT(VARCHAR(4), ERROR_LINE()) + '   ' + ERROR_MESSAGE(), GETDATE(), SUSER_NAME())
	END
END CATCH

IF @@TRANCOUNT > 0 COMMIT TRANSACTION 

END
GO
