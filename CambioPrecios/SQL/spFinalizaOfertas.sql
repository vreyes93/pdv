USE EXACTUSERP
GO

BEGIN TRY
	DROP PROCEDURE spFinalizaOfertas
END TRY
BEGIN CATCH
	--Nothing
END CATCH
GO

CREATE PROCEDURE spFinalizaOfertas
AS
BEGIN

DECLARE @Articulo AS VARCHAR(20)
DECLARE @PrecioOriginal AS DECIMAL(28,8)
DECLARE @PrecioLista AS DECIMAL(28,8)
DECLARE @NivelPrecio AS VARCHAR(20)
DECLARE @CoeficienteOriginal AS DECIMAL(18,6)
DECLARE @dia AS INT
DECLARE @mes AS INT
DECLARE @anio AS INT
DECLARE @OfertasArticulo AS INT
DECLARE @OfertasNivel AS INT
DECLARE @OfertasArticuloNivel AS INT
DECLARE @FechaInicial AS DATETIME
DECLARE @IVA AS DECIMAL(28,8)
DECLARE @Estatus AS CHAR(1)
DECLARE @Condicional AS CHAR(1)

BEGIN TRANSACTION
BEGIN TRY

SET @dia = (SELECT DAY(getDate()))
SET @mes = (SELECT MONTH(getDate()))
SET @anio = (SELECT YEAR(getDate()))

SET @OfertasArticulo = 0
SET @OfertasNivel = 0
SET @OfertasArticuloNivel = 0

SET @IVA = (SELECT 1 + (IMPUESTO1 / 100) FROM prodmult.IMPUESTO WHERE IMPUESTO = 'IVA')

--Cursor de Ofertas por Art�culos
DECLARE #CursorOfertasArticulos CURSOR LOCAL FOR
SELECT Articulo, PrecioOriginal, FechaOfertaDesde, Estatus, Condicional FROM prodmult.MF_ArticuloPrecioOferta 
WHERE (Estatus = 'V' OR Estatus = 'C') AND DAY(FechaOfertaHasta) = @dia AND MONTH(FechaOfertaHasta) = @mes 
AND YEAR(FechaOfertaHasta) = @anio ORDER BY Articulo 

OPEN #CursorOfertasArticulos
FETCH NEXT FROM #CursorOfertasArticulos INTO @Articulo, @PrecioOriginal, @FechaInicial, @Estatus, @Condicional

WHILE @@FETCH_STATUS = 0
BEGIN
	IF (@Condicional = 'N')
	BEGIN
		UPDATE prodmult.ARTICULO SET PRECIO_BASE_LOCAL = @PrecioOriginal WHERE ARTICULO = @Articulo
		
		UPDATE a SET a.PRECIO = (CASE SUBSTRING(b.PAGOS, 1, 2) WHEN '12' THEN @PrecioOriginal ELSE ((((@PrecioOriginal * @IVA) * ((100 - c.pctjRestar) / 100)) * b.FACTOR) / @IVA) END),
		a.MARGEN_MULR = @PrecioOriginal / ( CASE (CASE SUBSTRING(b.PAGOS, 1, 2) WHEN '12' THEN @PrecioOriginal ELSE ((((@PrecioOriginal * @IVA) * ((100 - c.pctjRestar) / 100)) * b.FACTOR) / @IVA) END) WHEN 0 THEN @PrecioOriginal ELSE (CASE SUBSTRING(b.PAGOS, 1, 2) WHEN '12' THEN @PrecioOriginal ELSE ((((@PrecioOriginal * @IVA) * ((100 - c.pctjRestar) / 100)) * b.FACTOR) / @IVA) END) END)
		FROM prodmult.ARTICULO_PRECIO a JOIN prodmult.MF_NIVEL_PRECIO_COEFICIENTE b ON a.NIVEL_PRECIO = b.NIVEL_PRECIO JOIN prodmult.MF_Financiera c ON b.FINANCIERA = c.Financiera
		WHERE a.ARTICULO = @Articulo
	END
	
	UPDATE prodmult.MF_ArticuloPrecioOferta SET Estatus = (CASE @Estatus WHEN 'C' THEN 'C' ELSE 'F' END)
	WHERE (Estatus = 'V' OR Estatus = 'C') AND DAY(FechaOfertaHasta) = @dia AND MONTH(FechaOfertaHasta) = @mes 
	AND YEAR(FechaOfertaHasta) = @anio AND ARTICULO = @Articulo

	UPDATE prodmult.MF_ArticuloPrecioOfertaCondicion SET Estatus = (CASE @Estatus WHEN 'C' THEN 'C' ELSE 'F' END)
	WHERE (Estatus = 'V' OR Estatus = 'C') AND DAY(FechaOfertaHasta) = @dia AND MONTH(FechaOfertaHasta) = @mes 
	AND YEAR(FechaOfertaHasta) = @anio AND ARTICULO = @Articulo

	INSERT INTO prodmult.MF_BitacoraPreciosOfertas ( Tipo, Resultado, Descripcion, RecordDate, CreatedBy )
	VALUES ( 'F', 'S', 'La Oferta del Art�culo ' + @Articulo + ' finaliz� exitosamente.', GETDATE(), SUSER_NAME())
	
	--Fetch Cursor Ofertas por Art�culos
	FETCH NEXT FROM #CursorOfertasArticulos INTO @Articulo, @PrecioOriginal, @FechaInicial, @Estatus, @Condicional
END
CLOSE #CursorOfertasArticulos
DEALLOCATE #CursorOfertasArticulos

--Fin de Ofertas por Articulo

--Cursor de Ofertas por Nivel
DECLARE #CursorOfertasNivel CURSOR LOCAL FOR
SELECT NivelPrecio, CoeficienteMulrOriginal, FechaOfertaDesde FROM prodmult.MF_NivelPrecioCoeficienteOferta 
WHERE Estatus = 'V' AND DAY(FechaOfertaHasta) = @dia AND MONTH(FechaOfertaHasta) = @mes 
AND YEAR(FechaOfertaHasta) = @anio ORDER BY NivelPrecio

OPEN #CursorOfertasNivel
FETCH NEXT FROM #CursorOfertasNivel INTO @NivelPrecio, @CoeficienteOriginal, @FechaInicial

WHILE @@FETCH_STATUS = 0
BEGIN
	--SET @OfertasNivel = @OfertasNivel + 1
	
	--UPDATE prodmult.MF_NIVEL_PRECIO_COEFICIENTE SET COEFICIENTE_MULR = @CoeficienteOriginal
	--WHERE NIVEL_PRECIO = @NivelPrecio

	--UPDATE a SET a.PRECIO = b.PrecioOriginal
	--FROM prodmult.ARTICULO_PRECIO a JOIN prodmult.MF_NivelPrecioCoeficienteOfertaDet b ON a.NIVEL_PRECIO = b.NivelPrecio AND a.ARTICULO = b.Articulo
	--WHERE a.NIVEL_PRECIO = @NivelPrecio AND b.FechaOfertaDesde = @FechaInicial AND b.Tipo = 'N'

	--Fetch Cursor Ofertas por Nivel
	FETCH NEXT FROM #CursorOfertasNivel INTO @NivelPrecio, @CoeficienteOriginal, @FechaInicial
END
CLOSE #CursorOfertasNivel
DEALLOCATE #CursorOfertasNivel


--Cursor de Ofertas por Articulo+Nivel
DECLARE #CursorOfertasAticuloNivel CURSOR LOCAL FOR
SELECT a.NivelPrecio, a.Articulo, b.PRECIO_BASE_LOCAL, a.Estatus FROM prodmult.MF_ArticuloNivelPrecioOferta a JOIN prodmult.ARTICULO b ON a.Articulo = b.ARTICULO
WHERE (Estatus = 'V' OR Estatus = 'C') AND DAY(FechaOfertaHasta) = @dia AND MONTH(FechaOfertaHasta) = @mes 
AND YEAR(FechaOfertaHasta) = @anio ORDER BY NivelPrecio

OPEN #CursorOfertasAticuloNivel
FETCH NEXT FROM #CursorOfertasAticuloNivel INTO @NivelPrecio, @Articulo, @PrecioOriginal, @Estatus

WHILE @@FETCH_STATUS = 0
BEGIN
	UPDATE a SET a.PRECIO = (CASE SUBSTRING(b.PAGOS, 1, 2) WHEN '12' THEN @PrecioOriginal ELSE ((((@PrecioOriginal * @IVA) * ((100 - c.pctjRestar) / 100)) * b.FACTOR) / @IVA) END),
	a.MARGEN_MULR = @PrecioOriginal / ( CASE (CASE SUBSTRING(b.PAGOS, 1, 2) WHEN '12' THEN @PrecioOriginal ELSE ((((@PrecioOriginal * @IVA) * ((100 - c.pctjRestar) / 100)) * b.FACTOR) / @IVA) END) WHEN 0 THEN @PrecioOriginal ELSE (CASE SUBSTRING(b.PAGOS, 1, 2) WHEN '12' THEN @PrecioOriginal ELSE ((((@PrecioOriginal * @IVA) * ((100 - c.pctjRestar) / 100)) * b.FACTOR) / @IVA) END) END)
	FROM prodmult.ARTICULO_PRECIO a JOIN prodmult.MF_NIVEL_PRECIO_COEFICIENTE b ON a.NIVEL_PRECIO = b.NIVEL_PRECIO JOIN prodmult.MF_Financiera c ON b.FINANCIERA = c.Financiera
	WHERE a.ARTICULO = @Articulo
	
	UPDATE prodmult.MF_ArticuloNivelPrecioOferta SET Estatus = (CASE @Estatus WHEN 'C' THEN 'C' ELSE 'F' END)
	WHERE (Estatus = 'V' OR Estatus = 'C') AND DAY(FechaOfertaHasta) = @dia AND MONTH(FechaOfertaHasta) = @mes 
	AND YEAR(FechaOfertaHasta) = @anio AND ARTICULO = @Articulo
	
	INSERT INTO prodmult.MF_BitacoraPreciosOfertas ( Tipo, Resultado, Descripcion, RecordDate, CreatedBy )
	VALUES ( 'F', 'S', 'La Oferta del Art�culo ' + @Articulo + ' y Nivel ' + @NivelPrecio + ' finaliz� exitosamente.', GETDATE(), SUSER_NAME())
	
	--Fetch Cursor Ofertas por Articulo+Nivel
	FETCH NEXT FROM #CursorOfertasAticuloNivel INTO @NivelPrecio, @Articulo, @PrecioOriginal, @Estatus
END
CLOSE #CursorOfertasAticuloNivel
DEALLOCATE #CursorOfertasAticuloNivel

--Fin de Ofertas por Articulo+Nivel

END TRY

BEGIN CATCH
	IF @@TRANCOUNT > 0 
	BEGIN
		ROLLBACK TRANSACTION
		INSERT INTO prodmult.MF_BitacoraPreciosOfertas ( Tipo, Resultado, Descripcion, RecordDate, CreatedBy )
		VALUES ( 'F', 'F', 'Se produjo error al finalizar la oferta.  Art�culo ' + @Articulo + '.  L�nea ' + CONVERT(VARCHAR(4), ERROR_LINE()) + '   ' + ERROR_MESSAGE(), GETDATE(), SUSER_NAME())
	END
END CATCH

IF @@TRANCOUNT > 0 COMMIT TRANSACTION 

END
GO
