USE EXACTUSERP
GO

BEGIN TRY
	drop PROCEDURE spIniciaOfertas
END TRY
BEGIN CATCH
	--Nothing
END CATCH
GO

CREATE PROCEDURE spIniciaOfertas
AS
BEGIN

DECLARE @Articulo AS VARCHAR(20)
DECLARE @PrecioOferta AS DECIMAL(28,8)
DECLARE @PrecioLista AS DECIMAL(28,8)
DECLARE @NivelPrecio AS VARCHAR(20)
DECLARE @CoeficienteOferta AS DECIMAL(18,6)
DECLARE @dia AS INT
DECLARE @mes AS INT
DECLARE @anio AS INT
DECLARE @OfertasArticulo AS INT
DECLARE @OfertasNivel AS INT
DECLARE @OfertasArticuloNivel AS INT
DECLARE @FechaOfertaDesde AS DATETIME
DECLARE @PrecioOriginal AS DECIMAL(28,8)
DECLARE @PrecioListaTemp AS DECIMAL(28,8)
DECLARE @PrecioBase AS DECIMAL(28,8)
DECLARE @IVA AS DECIMAL(28,8)
DECLARE @PctjDescuento AS DECIMAL(28,8)
DECLARE @Factor AS DECIMAL(28,8)
DECLARE @Condicional AS CHAR(1)
DECLARE @Pagos AS VARCHAR(100)

BEGIN TRANSACTION
BEGIN TRY

--DECLARE @dia AS INT
--DECLARE @mes AS INT
--DECLARE @anio AS INT

SET @dia = (SELECT DAY(getDate()))
SET @mes = (SELECT MONTH(getDate()))
SET @anio = (SELECT YEAR(getDate()))

--SELECT a.NivelPrecio, a.Articulo, c.PRECIO_BASE_LOCAL, a.pctjDescuento, b.FACTOR
--FROM prodmult.MF_ArticuloNivelPrecioOferta a JOIN prodmult.MF_NIVEL_PRECIO_COEFICIENTE b ON a.NivelPrecio = b.NIVEL_PRECIO
--JOIN prodmult.ARTICULO c ON a.Articulo = c.ARTICULO
--WHERE Estatus = 'P' AND DAY(FechaOfertaDesde) = @dia AND MONTH(FechaOfertaDesde) = @mes 

SET @OfertasArticulo = 0
SET @OfertasNivel = 0
SET @OfertasArticuloNivel = 0

SET @IVA = (SELECT 1 + (IMPUESTO1 / 100) FROM prodmult.IMPUESTO WHERE IMPUESTO = 'IVA')

--Cursor de Ofertas por Art�culos
DECLARE #CursorOfertasArticulos CURSOR LOCAL FOR
SELECT Articulo, PrecioOferta, FechaOfertaDesde, Condicional FROM prodmult.MF_ArticuloPrecioOferta 
WHERE Estatus = 'P' AND DAY(FechaOfertaDesde) = @dia AND MONTH(FechaOfertaDesde) = @mes 
AND YEAR(FechaOfertaDesde) = @anio ORDER BY Articulo 

OPEN #CursorOfertasArticulos
FETCH NEXT FROM #CursorOfertasArticulos INTO @Articulo, @PrecioOferta, @FechaOfertaDesde, @Condicional

WHILE @@FETCH_STATUS = 0
BEGIN
	IF (@Condicional = 'N')
	BEGIN
		UPDATE prodmult.ARTICULO SET PRECIO_BASE_LOCAL = @PrecioOferta WHERE ARTICULO = @Articulo
				
		UPDATE a SET a.PRECIO = (CASE SUBSTRING(b.PAGOS, 1, 2) WHEN '12' THEN @PrecioOferta ELSE ((((@PrecioOferta * @IVA) * ((100 - c.pctjRestar) / 100)) * b.FACTOR) / @IVA) END),
		a.MARGEN_MULR = @PrecioOferta / ( CASE (CASE SUBSTRING(b.PAGOS, 1, 2) WHEN '12' THEN @PrecioOferta ELSE ((((@PrecioOferta * @IVA) * ((100 - c.pctjRestar) / 100)) * b.FACTOR) / @IVA) END) WHEN 0 THEN @PrecioOferta ELSE (CASE SUBSTRING(b.PAGOS, 1, 2) WHEN '12' THEN @PrecioOferta ELSE ((((@PrecioOferta * @IVA) * ((100 - c.pctjRestar) / 100)) * b.FACTOR) / @IVA) END) END)
		FROM prodmult.ARTICULO_PRECIO a JOIN prodmult.MF_NIVEL_PRECIO_COEFICIENTE b ON a.NIVEL_PRECIO = b.NIVEL_PRECIO JOIN prodmult.MF_Financiera c ON b.FINANCIERA = c.Financiera
		WHERE a.ARTICULO = @Articulo
	END
		
	UPDATE prodmult.MF_ArticuloPrecioOferta SET Estatus = 'V'
	WHERE Estatus = 'P' AND DAY(FechaOfertaDesde) = @dia AND MONTH(FechaOfertaDesde) = @mes 
	AND YEAR(FechaOfertaDesde) = @anio AND Articulo = @Articulo

	UPDATE prodmult.MF_ArticuloPrecioOfertaCondicion SET Estatus = 'V'
	WHERE Estatus = 'P' AND DAY(FechaOfertaDesde) = @dia AND MONTH(FechaOfertaDesde) = @mes 
	AND YEAR(FechaOfertaDesde) = @anio AND Articulo = @Articulo

	INSERT INTO prodmult.MF_BitacoraPreciosOfertas ( Tipo, Resultado, Descripcion, RecordDate, CreatedBy )
	VALUES ( 'I', 'S', 'La Oferta del Art�culo ' + @Articulo + ' fue iniciada exitosamente.', GETDATE(), SUSER_NAME())
	
	--Fetch Cursor Ofertas por Art�culos
	FETCH NEXT FROM #CursorOfertasArticulos INTO @Articulo, @PrecioOferta, @FechaOfertaDesde, @Condicional
END
CLOSE #CursorOfertasArticulos
DEALLOCATE #CursorOfertasArticulos

--Fin de Ofertas por Articulo

--Cursor de Ofertas por Nivel
DECLARE #CursorOfertasNivel CURSOR LOCAL FOR
SELECT NivelPrecio, CoeficienteMulrOferta, FechaOfertaDesde FROM prodmult.MF_NivelPrecioCoeficienteOferta 
WHERE Estatus = 'P' AND DAY(FechaOfertaDesde) = @dia AND MONTH(FechaOfertaDesde) = @mes 
AND YEAR(FechaOfertaDesde) = @anio ORDER BY NivelPrecio

OPEN #CursorOfertasNivel
FETCH NEXT FROM #CursorOfertasNivel INTO @NivelPrecio, @CoeficienteOferta, @FechaOfertaDesde

WHILE @@FETCH_STATUS = 0
BEGIN	
	--UPDATE prodmult.MF_NIVEL_PRECIO_COEFICIENTE SET COEFICIENTE_MULR = @CoeficienteOferta
	--WHERE NIVEL_PRECIO = @NivelPrecio

	--INSERT INTO prodmult.MF_NivelPrecioCoeficienteOfertaDet ( Tipo, NivelPrecio, FechaOfertaDesde, Articulo, PrecioOriginal )
	--SELECT 'N', @NivelPrecio, @FechaOfertaDesde, ARTICULO, PRECIO FROM prodmult.ARTICULO_PRECIO WHERE NIVEL_PRECIO = @NivelPrecio ORDER BY NIVEL_PRECIO
	
	--UPDATE a SET a.PRECIO = b.PRECIO_BASE_LOCAL * @CoeficienteOferta
	--FROM prodmult.ARTICULO_PRECIO a JOIN prodmult.ARTICULO b ON a.ARTICULO = b.ARTICULO
	--WHERE a.NIVEL_PRECIO = @NivelPrecio		
	
	--SET @OfertasNivel = @OfertasNivel + 1
	
	--Fetch Cursor Ofertas por Nivel
	FETCH NEXT FROM #CursorOfertasNivel INTO @NivelPrecio, @CoeficienteOferta, @FechaOfertaDesde
END
CLOSE #CursorOfertasNivel
DEALLOCATE #CursorOfertasNivel

--Fin de Ofertas por Nivel

--Cursor de Ofertas por Articulo+Nivel
DECLARE #CursorOfertasAticuloNivel CURSOR LOCAL FOR
SELECT a.NivelPrecio, a.Articulo, c.PRECIO_BASE_LOCAL, a.pctjDescuento, b.FACTOR, b.PAGOS
FROM prodmult.MF_ArticuloNivelPrecioOferta a JOIN prodmult.MF_NIVEL_PRECIO_COEFICIENTE b ON a.NivelPrecio = b.NIVEL_PRECIO
JOIN prodmult.ARTICULO c ON a.Articulo = c.ARTICULO
WHERE Estatus = 'P' AND DAY(FechaOfertaDesde) = @dia AND MONTH(FechaOfertaDesde) = @mes 
AND YEAR(FechaOfertaDesde) = @anio ORDER BY NivelPrecio

OPEN #CursorOfertasAticuloNivel
FETCH NEXT FROM #CursorOfertasAticuloNivel INTO @NivelPrecio, @Articulo, @PrecioOriginal, @PctjDescuento, @Factor, @Pagos

WHILE @@FETCH_STATUS = 0
BEGIN
	IF (@Pagos = '12 PAGOS') SET @Factor = 1

	UPDATE a SET a.PRECIO = ((((@PrecioOriginal * @IVA) * ((100 - @PctjDescuento) / 100)) * @Factor) / @IVA),
	a.MARGEN_MULR = @PrecioOriginal / ((((@PrecioOriginal * @IVA) * ((100 - @PctjDescuento) / 100)) * @Factor) / @IVA) 
	FROM prodmult.ARTICULO_PRECIO a JOIN prodmult.MF_NIVEL_PRECIO_COEFICIENTE b ON a.NIVEL_PRECIO = b.NIVEL_PADRE
	WHERE a.ARTICULO = @Articulo AND a.NIVEL_PRECIO = @NivelPrecio
	
	UPDATE prodmult.MF_ArticuloNivelPrecioOferta SET Estatus = 'V'
	WHERE Estatus = 'P' AND DAY(FechaOfertaDesde) = @dia AND MONTH(FechaOfertaDesde) = @mes 
	AND YEAR(FechaOfertaDesde) = @anio AND Articulo = @Articulo AND NivelPrecio = @NivelPrecio

	INSERT INTO prodmult.MF_BitacoraPreciosOfertas ( Tipo, Resultado, Descripcion, RecordDate, CreatedBy )
	VALUES ( 'I', 'S', 'La Oferta del Art�culo ' + @Articulo + ' y Nivel ' + @NivelPrecio + ' fue iniciada exitosamente.', GETDATE(), SUSER_NAME())
	
	--Fetch Cursor Ofertas por Articulo+Nivel
	FETCH NEXT FROM #CursorOfertasAticuloNivel INTO @NivelPrecio, @Articulo, @PrecioOriginal, @PctjDescuento, @Factor, @Pagos
END
CLOSE #CursorOfertasAticuloNivel
DEALLOCATE #CursorOfertasAticuloNivel

END TRY

BEGIN CATCH
	IF @@TRANCOUNT > 0 
	BEGIN
		ROLLBACK TRANSACTION
		INSERT INTO prodmult.MF_BitacoraPreciosOfertas ( Tipo, Resultado, Descripcion, RecordDate, CreatedBy )
		VALUES ( 'I', 'F', 'Se produjo error al iniciar la oferta.  Articulo ' + @Articulo + '.  L�nea ' + CONVERT(VARCHAR(4), ERROR_LINE()) + '   ' + ERROR_MESSAGE(), GETDATE(), SUSER_NAME())
	END
END CATCH

IF @@TRANCOUNT > 0 COMMIT TRANSACTION 

END
GO
