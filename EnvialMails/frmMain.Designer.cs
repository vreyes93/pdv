﻿namespace EnvialMails
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.btnVales200 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnVales200
            // 
            this.btnVales200.Location = new System.Drawing.Point(60, 50);
            this.btnVales200.Name = "btnVales200";
            this.btnVales200.Size = new System.Drawing.Size(75, 23);
            this.btnVales200.TabIndex = 0;
            this.btnVales200.Text = "Enviar Vales de Q200.00";
            this.btnVales200.UseVisualStyleBackColor = true;
            this.btnVales200.Click += new System.EventHandler(this.btnVales200_Click);
            // 
            // frmMain
            // 
            this.AccessibleDescription = "xxx";
            this.AccessibleName = "xxx";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 346);
            this.Controls.Add(this.btnVales200);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "xxx";
            this.Text = "Envío de correos";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnVales200;
    }
}