﻿namespace EnvialMails
{
    partial class frmVale200
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pb = new System.Windows.Forms.PictureBox();
            this.lbVale = new System.Windows.Forms.Label();
            this.lbFecha = new System.Windows.Forms.Label();
            this.lbCliente = new System.Windows.Forms.Label();
            this.pbEnviar = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEnviar)).BeginInit();
            this.SuspendLayout();
            // 
            // pb
            // 
            this.pb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pb.Image = global::EnvialMails.Properties.Resources.ValeMF;
            this.pb.Location = new System.Drawing.Point(0, 0);
            this.pb.Name = "pb";
            this.pb.Size = new System.Drawing.Size(632, 739);
            this.pb.TabIndex = 0;
            this.pb.TabStop = false;
            // 
            // lbVale
            // 
            this.lbVale.Font = new System.Drawing.Font("Bookman Old Style", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVale.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(150)))));
            this.lbVale.Location = new System.Drawing.Point(446, 37);
            this.lbVale.Name = "lbVale";
            this.lbVale.Size = new System.Drawing.Size(158, 23);
            this.lbVale.TabIndex = 1;
            this.lbVale.Text = "Vale No. 000001";
            this.lbVale.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbFecha
            // 
            this.lbFecha.Font = new System.Drawing.Font("Bookman Old Style", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFecha.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(150)))));
            this.lbFecha.Location = new System.Drawing.Point(332, 56);
            this.lbFecha.Name = "lbFecha";
            this.lbFecha.Size = new System.Drawing.Size(272, 23);
            this.lbFecha.TabIndex = 2;
            this.lbFecha.Text = "Válido hasta el 11/12/2013";
            this.lbFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbCliente
            // 
            this.lbCliente.BackColor = System.Drawing.Color.Transparent;
            this.lbCliente.Font = new System.Drawing.Font("Bookman Old Style", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCliente.ForeColor = System.Drawing.Color.Black;
            this.lbCliente.Location = new System.Drawing.Point(30, 129);
            this.lbCliente.Name = "lbCliente";
            this.lbCliente.Size = new System.Drawing.Size(431, 23);
            this.lbCliente.TabIndex = 3;
            this.lbCliente.Text = "Estimada Tzipi Altalef,";
            this.lbCliente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pbEnviar
            // 
            this.pbEnviar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbEnviar.Location = new System.Drawing.Point(0, 0);
            this.pbEnviar.Name = "pbEnviar";
            this.pbEnviar.Size = new System.Drawing.Size(632, 739);
            this.pbEnviar.TabIndex = 4;
            this.pbEnviar.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmVale200
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(632, 739);
            this.Controls.Add(this.lbCliente);
            this.Controls.Add(this.lbFecha);
            this.Controls.Add(this.lbVale);
            this.Controls.Add(this.pb);
            this.Controls.Add(this.pbEnviar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmVale200";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmVale200_Load);
            this.Shown += new System.EventHandler(this.frmVale200_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEnviar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pb;
        private System.Windows.Forms.Label lbVale;
        private System.Windows.Forms.Label lbFecha;
        private System.Windows.Forms.Label lbCliente;
        private System.Windows.Forms.PictureBox pbEnviar;
        private System.Windows.Forms.Timer timer1;
    }
}