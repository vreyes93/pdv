﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using System.IO;
using System.Data.SqlClient;

namespace EnvialMails
{
    public partial class frmVale200 : Form
    {
        public frmVale200()
        {
            InitializeComponent();
        }

        private void frmVale200_Load(object sender, EventArgs e)
        {

        }

        private void frmVale200_Shown(object sender, EventArgs e)
        {
            EnviarMail();
            //timer1.Start();
            //this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        void EnviarMail()
        {
            try
            {
                DataTable dt = new DataTable("clientes"); SqlCommand mCommand = new SqlCommand(); SqlTransaction mTransaction = null;
                SqlConnection mConexion = new SqlConnection(string.Format("Data Source = integra; Initial Catalog = EXACTUSERP;User id = mf.fiestanet;password = 54321"));

                //SqlDataAdapter da = new SqlDataAdapter("SELECT a.CLIENTE AS Cliente, a.NOMBRE AS Nombre, a.E_MAIL AS Email, b.GENERO AS Genero FROM prodmult.CLIENTE a JOIN prodmult.MF_Cliente b ON a.CLIENTE = b.CLIENTE WHERE a.CLIENTE IN ( '0036617', '0013826', '0022525', '0000905' ) ORDER BY a.CLIENTE", mConexion);
                //SqlDataAdapter da = new SqlDataAdapter("SELECT a.CLIENTE AS Cliente, a.NOMBRE AS Nombre, c.Correo AS Email, b.GENERO AS Genero FROM prodmult.CLIENTE a JOIN prodmult.MF_Cliente b ON a.CLIENTE = b.CLIENTE JOIN prodmult.mf_clienteCallCenter c ON a.CLIENTE = c.Cliente WHERE c.Correo LIKE '%@%'", mConexion);
                //SqlDataAdapter da = new SqlDataAdapter("SELECT a.CLIENTE AS Cliente, a.NOMBRE AS Nombre, c.Correo AS Email, b.GENERO AS Genero FROM prodmult.CLIENTE a JOIN prodmult.MF_Cliente b ON a.CLIENTE = b.CLIENTE JOIN prodmult.mf_clienteCallCenter c ON a.CLIENTE = c.Cliente WHERE c.Correo IS NULL", mConexion);
                SqlDataAdapter da = new SqlDataAdapter("SELECT a.CLIENTE AS Cliente, a.NOMBRE AS Nombre, c.Correo AS Email, b.GENERO AS Genero FROM prodmult.CLIENTE a JOIN prodmult.MF_Cliente b ON a.CLIENTE = b.CLIENTE JOIN prodmult.mf_clienteCallCenter c ON a.CLIENTE = c.Cliente", mConexion);

                da.Fill(dt);
                DateTime mFechaVence = DateTime.Now.Date.AddMonths(3);

                mConexion.Open();
                mTransaction = mConexion.BeginTransaction();

                mCommand.Connection = mConexion;
                mCommand.Transaction = mTransaction;

                mCommand.Parameters.Add("@fecha", System.Data.SqlDbType.DateTime).Value = DateTime.Now.Date;
                mCommand.Parameters.Add("@fechaVence", System.Data.SqlDbType.DateTime).Value = mFechaVence;
                
                mCommand.CommandText = "SELECT COALESCE(MAX(VALE), 0) FROM prodmult.MF_ClienteVale200";
                int mVale = Convert.ToInt32(mCommand.ExecuteScalar());

                for (int ii = 0; ii < dt.Rows.Count; ii++)
                {
                    mVale = mVale + 1;
                    string mCeros;

                    switch (mVale.ToString().Length)
                    {
                        case 1:
                            mCeros = "0000";
                            break;
                        case 2:
                            mCeros = "000";
                            break;
                        case 3:
                            mCeros = "00";
                            break;
                        case 4:
                            mCeros = "0";
                            break;
                        default:
                            mCeros = "";
                            break;
                    }

                    //lbCliente.Text = string.Format("{0} {1}", dt.Rows[ii]["Genero"].ToString() == "H" ? "Estimado" : "Estimada",  dt.Rows[ii]["Nombre"].ToString());
                    lbCliente.Text = string.Format("Estimado(a) {0}", dt.Rows[ii]["Nombre"].ToString());
                    lbVale.Text = string.Format("Vale No. {0}{1}", mCeros, mVale.ToString());
                    lbFecha.Text = string.Format("Válido hasta el {0}", mFechaVence.ToShortDateString());

                    //WaitSeconds(2);

                    //SmtpClient smtp = new SmtpClient();
                    //MailMessage mail = new MailMessage();

                    //mail.Subject = "Vale de descuento";

                    //mail.From = new MailAddress("servicioalcliente@mueblesfiesta.com");
                    //mail.ReplyToList.Add("servicioalcliente@mueblesfiesta.com");

                    //bool mMailValido;
                    string mEnviadoViaMail = "N";

                    //try
                    //{
                    //    mail.To.Add(dt.Rows[ii]["Email"].ToString());
                    //    mMailValido = true;
                    //}
                    //catch
                    //{
                    //    mMailValido = false;
                    //}

                    //ScreenCapture sc = new ScreenCapture();
                    //Image imagen = sc.CaptureWindow(this.Handle);

                    //pbEnviar.Image = imagen;

                    //string mArchivo = string.Format(@"C:\ImagenesVales\Vale{0}{1}.jpg", mCeros, mVale.ToString());
                    //if (File.Exists(mArchivo)) File.Delete(mArchivo);

                    //MemoryStream mstr = new MemoryStream();
                    //pbEnviar.Image.Save(mArchivo);

                    //if (mMailValido)
                    //{
                    //    mail.IsBodyHtml = true;
                    //    StringBuilder mBody = new StringBuilder();

                    //    mBody.Append("<a>Puede contactarnos escribiendo a <a HREF='mailto:servicioalcliente@mueblesfiesta.com?subject=Recibí%20Vale%20de%20Descuento'>servicioalcliente@mueblesfiesta.com</a> o llamando al 6644-3700.</a>");

                    //    if (dt.Rows[ii]["Email"].ToString().ToLower().Contains("hotmail"))
                    //    {
                    //        mBody.Append("<p></p><a>Favor ver el documento adjunto</a>");
                    //        mail.Body = mBody.ToString();
                    //        mail.Attachments.Add(new Attachment(mArchivo));
                    //    }
                    //    else
                    //    {
                    //        mBody.Append("<div dir='ltr'><div class='ValeMF' style='font-family:comic sans ms,sans-serif;color:#3333ff'><img src='cid:Haga_Clic_Abajo_En_Descargar_Mensaje_Completo' alt='Haga clic en mostrar imágenes o bien en Descargar mensaje completo' width='632' height='739'><br clear='all'></div><div><br></div>");

                    //        AlternateView html = AlternateView.CreateAlternateViewFromString(mBody.ToString(), Encoding.UTF8, MediaTypeNames.Text.Html);
                    //        LinkedResource Vale = new LinkedResource(mArchivo);

                    //        Vale.ContentId = "Haga_Clic_Abajo_En_Descargar_Mensaje_Completo";
                    //        html.LinkedResources.Add(Vale);

                    //        mail.AlternateViews.Add(html);
                    //    }

                    //smtp.Host = WebConfigurationManager.AppSettings["MailHost"];
                    //smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort"]);

                    //    smtp.EnableSsl = true;
                    //    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                    //    try
                    //    {
                    //        smtp.Credentials = new System.Net.NetworkCredential("noresponder@productosmultiples.com", "Estrella01");
                    //        smtp.Send(mail);
                    //        mEnviadoViaMail = "S";
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        mCommand.CommandText = string.Format("INSERT INTO prodmult.MF_ClienteCallCenterErrorEnviar ( Cliente, Correo, Error ) VALUES ( {0}, '{1}', '{2}' )", dt.Rows[ii]["Cliente"].ToString(), dt.Rows[ii]["Email"].ToString(), string.Format("{0} {1}", ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")), ex.Message));
                    //        mCommand.ExecuteNonQuery();
                    //    }
                    //}

                    mCommand.CommandText = string.Format("INSERT INTO prodmult.MF_ClienteVale200 ( VALE, CLIENTE, FECHA, FECHA_VENCE, ENVIADO_VIA_MAIL ) VALUES ( {0}, '{1}', @fecha, @fechaVence, '{2}' )", mVale.ToString(), dt.Rows[ii]["Cliente"].ToString(), mEnviadoViaMail);
                    mCommand.ExecuteNonQuery();
                }

                mTransaction.Commit();
                mConexion.Close();

                MessageBox.Show("Los correos para el vale de Q200 de descuento fueron enviados exitosamente.", "Enviar Vales", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Error al enviar el correo.{0}{1}{0}{2}", System.Environment.NewLine, ex.Message, ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"))), "Enviar Vales", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private static void WaitSeconds(double nSecs)
        {
            // Esperar los segundos indicados

            // Crear la cadena para convertir en TimeSpan
            string s = "0.00:00:" + nSecs.ToString().Replace(",", ".");
            TimeSpan ts = TimeSpan.Parse(s);

            // Añadirle la diferencia a la hora actual
            DateTime t1 = DateTime.Now.Add(ts);

            // Esta asignación solo es necesaria
            // si la comprobación se hace al principio del bucle
            DateTime t2 = DateTime.Now;

            // Mientras no haya pasado el tiempo indicado
            while (t2 < t1)
            {
                // Un respiro para el sitema
                System.Windows.Forms.Application.DoEvents();
                // Asignar la hora actual
                t2 = DateTime.Now;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //EnviarMail();
            //timer1.Stop();
        }

    }
}
