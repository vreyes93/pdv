﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using System.Net;
using System.Net.Mail;

namespace EnviarDatosMayoreo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                //ws.Url = "http://sql.fiesta.local/services/wsPuntoVenta.asmx";

                ws.Timeout = 999999999;

                DataSet ds = new DataSet(); string mMensaje = ""; DateTime fecha = DateTime.Now.Date;
                ds = ws.AntiguedadSaldosMayoreo(ref mMensaje, fecha, true);

                string mFecha = string.Format("{0}{1}{2}{3}{4}", fecha.Year.ToString(), fecha.Month < 10 ? "0" : "", fecha.Month.ToString(), fecha.Day < 10 ? "0" : "", fecha.Day.ToString());
                string mFecha2 = string.Format("{3}{4}/{1}{2}/{0}", fecha.Year.ToString(), fecha.Month < 10 ? "0" : "", fecha.Month.ToString(), fecha.Day < 10 ? "0" : "", fecha.Day.ToString());

                string mNombreDocumento = string.Format(@"C:\Exactus\mayoreo\AntiguedadMayoreo_{0}.pdf", mFecha);
                string mNombreDocumentoXLS = mNombreDocumento.Replace("pdf", "xls");

                if (File.Exists(mNombreDocumento)) File.Delete(mNombreDocumento);
                if (File.Exists(mNombreDocumentoXLS)) File.Delete(mNombreDocumentoXLS);

                rptAntiguedadMayoreo mReporte = new rptAntiguedadMayoreo();
                rptAntiguedadMayoreoIntegracion mReporte2 = new rptAntiguedadMayoreoIntegracion();

                mReporte.SetDataSource(ds);
                mReporte.SetParameterValue("subtitulo", string.Format("AL: {0}", mFecha2));
                mReporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, mNombreDocumento);

                mReporte2.SetDataSource(ds);
                mReporte2.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, mNombreDocumentoXLS);
                
                SmtpClient smtp = new SmtpClient();
                MailMessage mail = new MailMessage();

                mail.Subject = string.Format("Antigüedad de Mayoreo al {0}", mFecha2);
                mail.Body = string.Format("Adjunto encontrará la antigüedad de Mayoreo correspondiente al {0}.", mFecha2);

                for (int ii = 0; ii < ds.Tables["destinatarios"].Rows.Count; ii++)
                {
                    mail.To.Add(ds.Tables["destinatarios"].Rows[ii]["email"].ToString());
                }

                mail.IsBodyHtml = true;
                mail.From = new MailAddress("puntodeventa@productosmultiples.com", "Antigüedad saldos mayoreo");
                //mail.From = new MailAddress("puntodeventa@mueblesfiesta.com", mNombreVendedor);
                mail.Attachments.Add(new Attachment(mNombreDocumento));
                mail.Attachments.Add(new Attachment(mNombreDocumentoXLS));

                smtp.Host = "smtp.googlemail.com";
                //smtp.Host = "merkava.mueblesfiesta.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                
                try
                {
                    smtp.Credentials = new System.Net.NetworkCredential("puntodeventa@productosmultiples.com", "Estrella01");
                    //smtp.Credentials = new System.Net.NetworkCredential("puntodeventa", "jh$%Pjma");
                    smtp.Send(mail);
                }
                catch
                {
                    //Nothing
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error {0} {1}.", ex.Message, m));
            }

            Application.Exit();
        }
    }
}
