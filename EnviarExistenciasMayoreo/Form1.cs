﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EnviarExistenciasMayoreo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                string mNombreDocumento = string.Format("ExistenciasMayoreoAl_{0}{1}{2}", DateTime.Now.Day.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Year.ToString());
                
                if (!Directory.Exists(@"C:\reportes\")) Directory.CreateDirectory(@"C:\reportes\");
                if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                SqlConnection mConexion = new SqlConnection(string.Format("Data Source = (local); Initial Catalog = EXACTUSERP;User id = {0};password = {1}", "REPORTEADOR", "RPTcry98"));
                SqlDataAdapter daExistencias = new SqlDataAdapter("prodmult.spExistenciasMayoreo", mConexion);

                daExistencias.SelectCommand.CommandType = CommandType.StoredProcedure;
                daExistencias.SelectCommand.Parameters.Add("@fecha", SqlDbType.DateTime).Value = DateTime.Now.Date;

                dsInfo ds = new dsInfo();
                daExistencias.Fill(ds.ExistenciasMayoreo);

                rptExistenciasMayoreo mReporte = new rptExistenciasMayoreo();

                mReporte.SetDataSource(ds);
                mReporte.SetParameterValue("Fecha", DateTime.Now.Date);

                mReporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                MessageBox.Show(string.Format("Error {0} {1}", ex.Message, m));
            }
        }
    }
}
