﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EnviarReporteRecibos
{
    public partial class frmEnviarRecibos : Form
    {
        public frmEnviarRecibos()
        {
            InitializeComponent();
        }

        private void frmEnviarRecibos_Load(object sender, EventArgs e)
        {

        }

        private void frmEnviarRecibos_Shown(object sender, EventArgs e)
        {
            try
            {
                string mURL = "http://sql.fiesta.local/services/wsPuntoVenta.asmx";
                
                var ws = new wsPuntoVenta.wsPuntoVenta();
                ws.Url = mURL;

                DateTime mFechaInicial = DateTime.Now.Date;
                DateTime mFechaFinal = DateTime.Now.Date;

                mFechaInicial = mFechaInicial.AddDays(-1);
                mFechaFinal = mFechaFinal.AddDays(-1);
                
                //mFechaInicial = new DateTime(2014, 9, 15);
                //mFechaFinal = new DateTime(2014, 10, 10);

                ws.EnviarReporteRecibos(mFechaInicial, mFechaFinal);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                MessageBox.Show(string.Format("Error: {0} - {1}", ex.Message, m), "Recibos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Application.Exit();
        }
    }
}
