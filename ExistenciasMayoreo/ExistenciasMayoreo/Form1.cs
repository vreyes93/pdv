using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;

namespace ExistenciasMayoreo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                string mNombreDocumento = string.Format("ExistenciasMayoreoAl_{0}{1}{2}.pdf", DateTime.Now.Day.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Year.ToString());
                string mNombreDocumento2 = string.Format("ExistenciasMayoreoFiestaAl_{0}{1}{2}.pdf", DateTime.Now.Day.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Year.ToString());

                if (!Directory.Exists(@"C:\reportes\")) Directory.CreateDirectory(@"C:\reportes\");
                if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                SqlConnection mConexion = new SqlConnection(string.Format("Data Source = sql.fiesta.local; Initial catalog = EXACTUSERP;User id = {0};password = {1}", "REPORTEADOR", "RPTcry98"));
                SqlDataAdapter daExistencias = new SqlDataAdapter("prodmult.spExistenciasMayoreo", mConexion);
                SqlDataAdapter daDestinatarios = new SqlDataAdapter("SELECT mail FROM prodmult.MF_EnviarExistenciasMayoreo", mConexion);
                SqlDataAdapter daDestinatarios2 = new SqlDataAdapter("SELECT mail FROM prodmult.MF_EnviarExistenciasMayoreo2", mConexion);

                daExistencias.SelectCommand.CommandType = CommandType.StoredProcedure;
                daExistencias.SelectCommand.Parameters.Add("@fecha", SqlDbType.DateTime).Value = DateTime.Now.Date;

                dsInfo ds = new dsInfo();
                daExistencias.Fill(ds.ExistenciasMayoreo);

                rptExistenciasMayoreo mReporte = new rptExistenciasMayoreo();
                rptExistenciasMayoreo mReporte2 = new rptExistenciasMayoreo();

                try
                {
                    mReporte.SetDataSource(ds);
                }
                catch (Exception ex)
                {
                    string m = "";
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                    }
                    catch
                    {
                        try
                        {
                            m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                        }
                        catch
                        {
                            //Nothing
                        }
                    }
                    MessageBox.Show(string.Format("Error {0} {1}", ex.Message, m));
                }

                mReporte.SetParameterValue("Fecha", DateTime.Now.Date);
                mReporte.SetParameterValue("Grupo", "Mayoreo");
                mReporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                mReporte2.SetDataSource(ds);
                mReporte2.SetParameterValue("Fecha", DateTime.Now.Date);
                mReporte2.SetParameterValue("Grupo", "Fiesta");
                mReporte2.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento2);

                DataTable dt = new DataTable("destinatarios");
                DataTable dt2 = new DataTable("destinatarios2");

                daDestinatarios.Fill(dt);
                daDestinatarios2.Fill(dt2);

                if (dt.Rows.Count > 0)
                {
                    SmtpClient smtp = new SmtpClient();
                    MailMessage mail = new MailMessage();

                    mail.From = new MailAddress("puntodeventa@productosmultiples.com");
                    mail.Subject = string.Format("Existencias Mayoreo al {0}", DateTime.Now.Date.ToShortDateString());
                    mail.Body = string.Format("Adjunto podrán encontrar las existencias de mayoreo al {0}", DateTime.Now.Date.ToShortDateString());
                    mail.Attachments.Add(new Attachment(@"C:\reportes\" + mNombreDocumento));
                    mail.ReplyTo = new MailAddress("mayoreo@productosmultiples.com", "Mayoreo");
                    mail.From = new MailAddress("mayoreo@productosmultiples.com", "Mayoreo");

                    smtp.Host = "smtp.googlemail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                    for (int ii = 0; ii < dt.Rows.Count; ii++)
                    {
                        mail.To.Add(dt.Rows[ii][0].ToString());
                    }

                    try
                    {
                        smtp.Credentials = new System.Net.NetworkCredential("puntodeventa@productosmultiples.com", "Ffq!b!Q445u{N6!v");
                        smtp.Send(mail);
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                if (dt2.Rows.Count > 0)
                {
                    SmtpClient smtp = new SmtpClient();
                    MailMessage mail = new MailMessage();

                    mail.From = new MailAddress("puntodeventa@productosmultiples.com");
                    mail.Subject = string.Format("Existencias Mayoreo y Fiesta al {0}", DateTime.Now.Date.ToShortDateString());
                    mail.Body = string.Format("Adjunto podrán encontrar las existencias de mayoreo y fiesta al {0}", DateTime.Now.Date.ToShortDateString());
                    mail.Attachments.Add(new Attachment(@"C:\reportes\" + mNombreDocumento2));
                    mail.ReplyTo = new MailAddress("mayoreo@productosmultiples.com", "Mayoreo");
                    mail.From = new MailAddress("mayoreo@productosmultiples.com", "Mayoreo");

                    smtp.Host = "smtp.googlemail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                    for (int ii = 0; ii < dt2.Rows.Count; ii++)
                    {
                        mail.To.Add(dt2.Rows[ii][0].ToString());
                    }

                    try
                    {
                        smtp.Credentials = new System.Net.NetworkCredential("puntodeventa@productosmultiples.com", "Ffq!b!Q445u{N6!v");
                        smtp.Send(mail);
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                Application.Exit();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                MessageBox.Show(string.Format("Error {0} {1}", ex.Message, m));
            }
        }
    }
}
