using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;

namespace ExistenciasMayoreoSuenia
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                string mNombreDocumento = string.Format("ExistenciasSueniaAl_{0}{1}{2}.pdf", DateTime.Now.Day.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Year.ToString());
        
                if (!Directory.Exists(@"C:\reportes\")) Directory.CreateDirectory(@"C:\reportes\");
                if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                SqlConnection mConexion = new SqlConnection(string.Format("Data Source = sql.fiesta.local; Initial catalog = EXACTUSERP;User id = {0};password = {1}", "REPORTEADOR", "RPTcry98"));
                SqlDataAdapter daExistencias = new SqlDataAdapter("prodmult.spExistenciasMayoreoSuenia", mConexion);
                SqlDataAdapter daDestinatarios = new SqlDataAdapter("SELECT DESTINATARIO FROM prodmult.MF_DestinatariosSuenia", mConexion);

                daExistencias.SelectCommand.CommandType = CommandType.StoredProcedure;
                daExistencias.SelectCommand.Parameters.Add("@fecha", SqlDbType.DateTime).Value = DateTime.Now.Date;
				
                dsInfo ds = new dsInfo();
                daExistencias.Fill(ds.ExistenciasMayoreo);

                rptExistenciasMayoreo mReporte = new rptExistenciasMayoreo();

                mReporte.SetDataSource(ds);
                mReporte.SetParameterValue("Fecha", DateTime.Now.Date);
                mReporte.SetParameterValue("Grupo", "Mayoreo");
                mReporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                DataTable dt = new DataTable("destinatarios");
                
                daDestinatarios.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    SmtpClient smtp = new SmtpClient();
                    MailMessage mail = new MailMessage();
                    
                    mail.From = new MailAddress("puntodeventa@productosmultiples.com");
                    mail.Subject = string.Format("Existencias Sueña al {0}", DateTime.Now.Date.ToShortDateString());
                    mail.Body = string.Format("Adjunto podrán encontrar las existencias de Sueña al {0}", DateTime.Now.Date.ToShortDateString());
                    mail.Attachments.Add(new Attachment(@"C:\reportes\" + mNombreDocumento));
                    mail.ReplyTo = new MailAddress("mayoreo@productosmultiples.com", "Sueña");
                    mail.From = new MailAddress("mayoreo@productosmultiples.com", "Sueña");

                    smtp.Host = "smtp.googlemail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    
                    for (int ii = 0; ii < dt.Rows.Count; ii++)
                    {
                        mail.To.Add(dt.Rows[ii][0].ToString());
                    }
                    
                    mail.Bcc.Add("rolando.pineda@mueblesfiesta.com");

                    try
                    {
                        smtp.Credentials = new System.Net.NetworkCredential("puntodeventa@productosmultiples.com", "Ffq!b!Q445u{N6!v");
                        smtp.Send(mail);
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                Application.Exit();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                MessageBox.Show(string.Format("Error {0} {1}", ex.Message, m));
            }
        }
    }
}
