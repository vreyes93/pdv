﻿using System.Web.Http;
using Newtonsoft.Json.Serialization;

namespace FiestaNETRestServices
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApiWithId",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });

            config.Routes.MapHttpRoute(
                 name: "DefaultApiWithAction",
                 routeTemplate: "api/{controller}/{action}/{codigoFactura}",
                 defaults: new { codigoFactura = RouteParameter.Optional });

            config.Routes.MapHttpRoute(
                 name: "Default2ApiWithAction",
                 routeTemplate: "api/{controller}/{action}/{tienda}/{atributo}",
                 defaults: new { tienda = RouteParameter.Optional, atributo = RouteParameter.Optional });

            config.Routes.MapHttpRoute(
                             name: "config",
                             routeTemplate: "api/{controller}/{action}/{usuario}",
                             defaults: new { usuario = RouteParameter.Optional });

            config.Routes.MapHttpRoute(
               name: "validar",
               routeTemplate: "api/{controller}/{action}/{solicitud}/{cobrador}",
               defaults: new { solicitud = RouteParameter.Optional, cobrador = RouteParameter.Optional });

            config.Routes.MapHttpRoute(
              name: "ActualizarInventario",
              routeTemplate: "api/{controller}/{action}/{sku}",
              defaults: new { sku = RouteParameter.Optional });

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new System.Net.Http.Headers.MediaTypeHeaderValue("text/html"));
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}
