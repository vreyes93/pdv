﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using FiestaNETRestServices.Utils;
using FiestaNETRestServices.Models;
using MF.DAL.Exactus.Crediplus;

namespace FiestaNETRestServices.Content.Abstract
{
    public class MFApiController : ApiController
    {
        /// <summary>
        /// Variable que nos sirve imprimir la trazabilidad del sistema en un archivo de texto plano.
        /// </summary>
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Clase que tiene varios métodos y funciones utilitarias.
        /// </summary>
        public Utilitarios Utilitario = new Utilitarios();

        /// <summary>
        /// Variable que almacena la respuesta del servicio REST.
        /// </summary>
        //public Respuesta Respuesta = new Respuesta();

        /// <summary>
        /// Variable que almacena la conexión a la base de datos.
        /// </summary>
        public EXACTUSERPEntities db = new EXACTUSERPEntities();
        public EXACTUSERPEntities dbExactus = new EXACTUSERPEntities();

        /// <summary>
        /// Variable que almacena la conexion a la base de datos de crediplus.
        /// </summary>
        public CrediplusContext dbCrediplus = new CrediplusContext();

        //public string ObtenerAmbiente()
        //{
        //    //EXACTUSERPEntities db = new EXACTUSERPEntities();
        //    string mAmbiente = "DES";

        //    if (dbExactus.Connection.ConnectionString.Contains("sql.fiesta.local")) mAmbiente = "PRO";
        //    if (dbExactus.Connection.ConnectionString.Contains("PRUEBAS")) mAmbiente = "PRU";

        //    return mAmbiente;

        //}
    }
}
