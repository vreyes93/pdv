﻿using FiestaNETRestServices.Models;
using MF_Clases;
using MF.DAL.Exactus.Crediplus;


namespace FiestaNETRestServices.Content.Abstract
{
    public class MFDAL
    {
        /// <summary>
        /// Variable que nos sirve imprimir la trazabilidad del sistema en un archivo de texto plano.
        /// </summary>
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Clase que tiene varios métodos y funciones utilitarias.
        /// </summary>
        public Utils.Utilitarios Utilitario = new Utils.Utilitarios();

        /// <summary>
        /// Variable que almacena la respuesta del servicio REST.
        /// </summary>
        public Respuesta Respuesta = new Respuesta();
        
        /// <summary>
        /// Variable que almacena la conexión a la base de datos.
        /// </summary>
        public EXACTUSERPEntities dbExactus = new EXACTUSERPEntities();

        /// <summary>
        /// Variable que almacena la conexion a la base de datos de crediplus.
        /// </summary>
        public CrediplusContext dbCrediplus = new CrediplusContext();
    }
}