﻿using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using FiestaNETRestServices.Models.ATID;
using FiestaNETRestServices.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.ATID
{
    public class CatalogosController : ApiController
    {
        /// <summary>
        /// Esta función tiene como propósito obtener el listado de los tipos de servicios 
        /// para debuguear se llama asi en MV5: http://localhost:53874/api/Armados/TipoServicio
        /// para debuguear en MV4: http://localhost:53874/api/TipoServicio
        /// </summary>
        /// <param name="mUsuario">.</param>
        /// <returns>Retorna un IHttpActionResult Ok((List<FriedmanResumenTrimestralDto>) o BadRequest(Mensaje de respuesta con el mensaje de error.), . </returns>
        //-----------------------------------------------------------------------------------------------------------------------
        #region Get
        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                //http://cpweb1.cp.local/ATIDServices
                //Utilitarios mUtilitarios = new Utilitarios();
                try
                {
                    WebRequest request = WebRequest.Create(string.Format("{0}/api/{1}", Utilitarios.GetATIDURLServices(), "Pais"));
                    request.Method = "GET";
                    request.ContentType = "application/json";

                    var response = (HttpWebResponse)request.GetResponse();
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        return BadRequest("");

                    }
                    else
                    {
                        List<Pais> mListadoPaises = new List<Pais>();
                        mListadoPaises = JsonConvert.DeserializeObject<List<Pais>>(responseString);

                        foreach (var itemPais in mListadoPaises)
                        {
                            var mPais = itemPais;
                            Departamento[] mListadoDepartamentos  = mPais.Departamentos;

                            DALComunes mComunes = new DALComunes();
                            mComunes.SincronizaCatalogoDeptoMunicipioCanton(mListadoDepartamentos);

                        }
                        //return Ok();
                    }


                    request = WebRequest.Create(string.Format("{0}/api/{1}", Utilitarios.GetATIDURLServices(), "Profesion"));
                    request.Method = "GET";
                    request.ContentType = "application/json";

                    response = (HttpWebResponse)request.GetResponse();
                    responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        return BadRequest("");

                    }
                    else
                    {
                        List<Profesion> mListadoProfesion = new List<Profesion>();
                        mListadoProfesion = JsonConvert.DeserializeObject<List<Profesion>>(responseString);

                        DALComunes mComunes = new DALComunes();
                        mComunes.SincronizaCatalogoProfesion(mListadoProfesion);

                    }

                    request = WebRequest.Create(string.Format("{0}/api/{1}", Utilitarios.GetATIDURLServices(), "Ocupacion"));
                    request.Method = "GET";
                    request.ContentType = "application/json";

                    response = (HttpWebResponse)request.GetResponse();
                    responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        return BadRequest("");

                    }
                    else
                    {
                        List<Ocupacion> mListadoOcupacion = new List<Ocupacion>();
                        mListadoOcupacion = JsonConvert.DeserializeObject<List<Ocupacion>>(responseString);

                        DALComunes mComunes = new DALComunes();
                        mComunes.SincronizaCatalogoOcupacion(mListadoOcupacion);


                        return Ok();
                    }
                }
                catch
                {
                    return BadRequest("");
                }

            }
            catch
            {
                return BadRequest("Excepcion, favor de comunicarse con Informática.");
            }
        }
        #endregion
    }
}
