﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;
using WebGrease.Css.Extensions;
using MF_Clases.Restful;
using RestSharp;
using MF_Clases.Zoho;
using System.Security.Cryptography.X509Certificates;
using RestSharp.Extensions;

namespace FiestaNETRestServices.Controllers.Armados
{
    public class SolicitudDesarmadoController : MFApiController
    {
        // GET: api/SolicitudDesarmado
        public string Get()
        {
            return "Método no disponible";
        }

        // GET: api/SolicitudDesarmado/5
        public Desarmado Get(string id)
        {
            Desarmado mRetorna = new Desarmado();

            try
            {
                if (id.Substring(0, 1) == "P")
                {
                    var q = (from p in db.MF_PedidoArmado where p.PEDIDO == id select p).First();

                    mRetorna.Tienda = q.COBRADOR;
                    mRetorna.Vendedor = q.VENDEDOR;
                    mRetorna.NombreVendedor = (from v in db.VENDEDOR where v.VENDEDOR1 == q.VENDEDOR select v).First().NOMBRE;
                    mRetorna.FechaDesarmado = q.FECHA_ARMADO;
                    mRetorna.Observaciones = q.OBSERVACIONES;
                    mRetorna.NoRequisicion = q.NO_REQUISICION;
                    mRetorna.Mensaje = string.Format("Requisición No. {0}", q.NO_REQUISICION);
                    mRetorna.YaEnviada = true;
                    mRetorna.Pedido = id;
                    mRetorna.Tipo = q.TIPO;
                    mRetorna.Cliente = q.CLIENTE;
                    if (q.RecordDate.Year == q.CreateDate.Year && q.RecordDate.Month == q.CreateDate.Month && q.RecordDate.Day == q.CreateDate.Day && q.RecordDate.Hour == q.CreateDate.Hour && q.RecordDate.Minute == q.CreateDate.Minute) mRetorna.YaEnviada = false;
                }
                else
                {
                    int mDesarmado = Convert.ToInt32(id);
                    var q = from d in db.MF_DesarmadoTienda where d.DESARMADO == mDesarmado select d;

                    if (q.Count() == 0)
                    {
                        mRetorna.Mensaje = string.Format("Error, el desarmado {0} no existe", id);
                    }
                    else
                    {
                        foreach (var item in q)
                        {
                            var qCliente = (from f in db.FACTURA join c in db.CLIENTE on f.CLIENTE equals c.CLIENTE1 where f.FACTURA1 == item.FACTURA select c).First();

                            mRetorna.Tienda = item.COBRADOR;
                            mRetorna.Vendedor = item.VENDEDOR;
                            mRetorna.NombreVendedor = (from v in db.VENDEDOR where v.VENDEDOR1 == item.VENDEDOR select v).First().NOMBRE;
                            mRetorna.FechaDesarmado = item.FECHA_DESARMADO;
                            mRetorna.Observaciones = item.OBSERVACIONES;
                            mRetorna.NoDesarmado = mDesarmado;
                            mRetorna.Mensaje = "";
                            mRetorna.Pedido = "";
                            mRetorna.Tipo = "D";
                            mRetorna.Cliente = qCliente.CLIENTE1;
                            mRetorna.NombreCliente = qCliente.NOMBRE;
                            mRetorna.Estado = item.ESTADO;
                            mRetorna.Factura = item.FACTURA;
                            mRetorna.UsuarioAutoriza = item.USUARIO_AUTORIZA;
                            mRetorna.ObservacionesAutoriza = item.OBSERVACIONES_AUTORIZA;
                        }

                        var qDetalle = from d in db.MF_DesarmadoTiendaArticulo where d.DESARMADO == mDesarmado select d;

                        foreach (var item in qDetalle)
                        {
                            Articulos mItem = new Articulos();
                            Int32 mExistencias = 0; string mExistenciasBodegas = "";
                            Existencias(item.ARTICULO, ref mExistencias, ref mExistenciasBodegas);

                            mItem.Articulo = item.ARTICULO;
                            mItem.Descripcion = (from a in db.ARTICULO where a.ARTICULO1 == item.ARTICULO select a).First().DESCRIPCION;
                            mItem.Existencias = mExistencias;
                            mItem.Ubicaciones = mExistenciasBodegas;
                            mRetorna.Articulo.Add(mItem);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                mRetorna.Mensaje = string.Format("{0} {1}", CatchClass.ExMessage(ex, "SolicitudDesarmadoController", "Get"), "");
            }

            return mRetorna;
        }

        void Existencias(string articulo, ref int existencias, ref string existenciasBodegas)
        {
            Int32 mExistencias = 0;

            var qExistencias = from e in db.EXISTENCIA_BODEGA where e.ARTICULO == articulo && e.CANT_DISPONIBLE > 0 orderby e.BODEGA select e;
            if (qExistencias.Count() > 0) mExistencias = (Int32)(qExistencias.Sum(x => x.CANT_DISPONIBLE));

            string mExistenciasBodegas = ""; string mSeparador = "";
            foreach (var itemExistencias in qExistencias)
            {
                mExistenciasBodegas = string.Format("{0}{1}{2}={3}", mExistenciasBodegas, mSeparador, itemExistencias.BODEGA, Convert.ToInt32(itemExistencias.CANT_DISPONIBLE).ToString().Replace(".00", ""));
                mSeparador = "; ";
            }
            if (mExistenciasBodegas.Trim().Length == 0) mExistenciasBodegas = "No hay existencias";

            existencias = mExistencias;
            existenciasBodegas = mExistenciasBodegas;
        }

        // POST: api/SolicitudDesarmado
        public string Post([FromBody]Desarmado desarmado)
        {
            string mRetorna = "";
            db.CommandTimeout = 999999999;
            
            string mAmbiente = WebConfigurationManager.AppSettings["Ambiente"];

            try
            {
                if (desarmado.Tipo == "S" && desarmado.Cliente.Trim().Length == 0)
                    return "Error, Debe ingresar el cliente";
                if ((desarmado.Tipo == "A" || desarmado.Tipo == "T") && desarmado.Cliente.Trim().Length > 0)
                    return "Error, Los Armados y servicios en tienda no aplica definir cliente";

                if (desarmado.Cliente.Trim().Length > 0)
                {
                    try
                    {
                        int mCodigoCliente = Convert.ToInt32(desarmado.Cliente);
                    }
                    catch
                    {
                        return "Error, debe ingresar un código de cliente válido";
                    }
                }

                if (desarmado.Factura.Trim().Length == 0)
                    return "Error, debe ingresar la factura";

                desarmado.Factura = desarmado.Factura.Trim().ToUpper();
                var qFactura = from f in db.FACTURA where f.FACTURA1 == desarmado.Factura select f;

                if (qFactura.Count() == 0)
                    return "Error, la factura ingresada no existe";
                if (qFactura.First().ANULADA == "S")
                    return "Error, la factura ingresada está anulada";
                if (desarmado.Articulo.Count() == 0)
                    return "Error, debe enviar al menos un artículo a desarmar";

                string msjValidaciónTiendaProducto = "";
                //validar si ya se envió la solcitud de desarmado en tienda
                (from h in db.MF_DesarmadoTienda
                 join d in db.MF_DesarmadoTiendaArticulo
                   on h.DESARMADO equals d.DESARMADO
                 where h.FACTURA == desarmado.Factura
                    && h.ESTADO != "R"
                    && h.COBRADOR == desarmado.Tienda
                 select d).ToList().ForEach(x =>
                 {
                     var solicitud = desarmado.Articulo.Where(y => y.Articulo == x.ARTICULO);
                     if (solicitud.Count() > 0)
                     {
                         string descripcion = solicitud.FirstOrDefault().Descripcion;
                         string tienda = desarmado.Tienda;
                         msjValidaciónTiendaProducto = "error: La solicitud de desarmado para '" + descripcion + "' en tienda '" + tienda + "' ya fue solicitado para esta factura.";
                     }
                 });
                if (msjValidaciónTiendaProducto.Length > 0)
                    return msjValidaciónTiendaProducto;

                string mUsuario = desarmado.Usuario;
                string mCliente = qFactura.First().CLIENTE;
                var qFacturaLinea = from f in db.FACTURA_LINEA where f.FACTURA == desarmado.Factura select f;
                var qCliente = (from c in db.CLIENTE where c.CLIENTE1 == mCliente select c).First();
                var qPedidLinea = from p in db.MF_Pedido_Linea where p.FACTURA == desarmado.Factura select p;

                //Verificación de existencias y armado
                var groupedResult = desarmado.Articulo.GroupBy(x => x.Articulo)
                    .Select(g => new { Articulo = g.Key, Count = g.Count() }).ToList();
                foreach (var item in groupedResult)
                {
                    //validar existencias en el caso de las tiendas outlet
                    bool blValidarExistenciasArt = DALValidar.ValidarExistencias(item.Articulo);

                    string descripcion = desarmado.Articulo.FirstOrDefault(x => x.Articulo == item.Articulo).Descripcion,
                            articulo = item.Articulo;
                    int cantidad = item.Count;

                    var qArticulo = (from a in db.ARTICULO where a.ARTICULO1 == articulo select a).First();

                    if (qArticulo.TIPO == "K")
                    {
                        return string.Format("Error, el artículo {0} es un combo, por favor ingrese el artículo que dentro del combo requiere armado", articulo);
                    }
                    else
                    {
                        var queryFactura = db.FACTURA_LINEA
                                           .Where(x => x.FACTURA == desarmado.Factura && x.ARTICULO == articulo)
                                           .GroupBy(x => new { x.BODEGA, x.ARTICULO })
                                           .Select(g => new { g.Key.BODEGA, g.Key.ARTICULO, CANTIDAD = g.Sum(x => x.CANTIDAD) })
                                           ;
                        var queryPedido = db.MF_DesarmadoTienda
                                            .Join(db.MF_DesarmadoTiendaArticulo, h => h.DESARMADO, d => d.DESARMADO, (h, d) => new { h, d })
                                            .Where(k => k.h.ESTADO != "R" && k.h.FACTURA == desarmado.Factura && k.d.ARTICULO == articulo)
                                            .GroupBy(x => new { x.h.COBRADOR, x.d.ARTICULO })
                                            .Select(g => new { BODEGA = g.Key.COBRADOR, g.Key.ARTICULO, CANTIDAD = g.Count() })
                                            ;
                        
                        decimal cantidadFactura = queryFactura.Count() == 0? 0 : queryFactura.Sum(x=>x.CANTIDAD);
                        int cantidadPedido = queryPedido.Count() == 0 ? 0 : queryPedido.Sum(x => x.CANTIDAD);

                        var qExistencia = db.EXISTENCIA_BODEGA.FirstOrDefault(x => x.ARTICULO == articulo && x.BODEGA == desarmado.Tienda);
                        decimal existencia = 0;
                        if (qExistencia!=null)
                             existencia = qExistencia.CANT_DISPONIBLE;
                        else
                            return string.Format("Error, no hay existencias  del artículo {0} en {1}", articulo,desarmado.Tienda);

                        //no tomar en cuenta las tiendas outlet para la validación de las existencias
                        if (existencia == 0 && blValidarExistenciasArt)
                            return string.Format("Error, el artículo {0} no tiene existencias en {1}", articulo, desarmado.Tienda);
                        if (cantidad > existencia && blValidarExistenciasArt)
                            return string.Format("Error, {0} cuenta con {1} {3}, y se solicitan {2}. No hay existencia para cubrir la solicitud.", desarmado.Tienda, (int)existencia, cantidad, descripcion);
                        if (!DALValidar.RequiereArmado(articulo))
                            return string.Format("Error, el artículo {0} no maneja armado", articulo);
                        if (cantidadFactura == 0)
                            return string.Format("Error, el artículo {0} no pertenece a la factura {1}", articulo, desarmado.Factura);
                        if (cantidadPedido >= cantidadFactura)
                            return string.Format("Error: El artículo '{0}' excede la cantidad detallada en la factura '{1}'", descripcion, desarmado.Factura);
                    }
                }

                int mDesarmado = 0;
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    var q = from d in db.MF_DesarmadoTienda select d;
                    if (q.Count() > 0) mDesarmado = q.Max(x => x.DESARMADO);

                    mDesarmado++;
                    MF_DesarmadoTienda iDesarmado = new MF_DesarmadoTienda
                    {
                        DESARMADO = mDesarmado,
                        COBRADOR = desarmado.Tienda,
                        VENDEDOR = desarmado.Vendedor,
                        FECHA_DESARMADO = desarmado.FechaDesarmado,
                        OBSERVACIONES = desarmado.Observaciones,
                        ESTADO = "P",
                        FACTURA = desarmado.Factura,
                        RecordDate = DateTime.Now,
                        CreatedBy = string.Format("FA/{0}", mUsuario),
                        UpdatedBy = string.Format("FA/{0}", mUsuario),
                        CreateDate = DateTime.Now
                    };

                    db.MF_DesarmadoTienda.AddObject(iDesarmado);

                    int mLinea = 0;
                    foreach (var item in desarmado.Articulo)
                    {
                        mLinea++;
                        MF_DesarmadoTiendaArticulo iArticulo = new MF_DesarmadoTiendaArticulo
                        {
                            DESARMADO = mDesarmado,
                            LINEA = mLinea,
                            ARTICULO = item.Articulo,
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("FA/{0}", mUsuario),
                            UpdatedBy = string.Format("FA/{0}", mUsuario),
                            CreateDate = DateTime.Now
                        };

                        db.MF_DesarmadoTiendaArticulo.AddObject(iArticulo);
                    }

                    db.SaveChanges();
                    transactionScope.Complete();
                }

                var qVendedor = (from v in db.MF_Vendedor where v.VENDEDOR == desarmado.Vendedor select v).First();
                string mNombreVendedor = string.Format("{0} {1}", qVendedor.NOMBRE, qVendedor.APELLIDO);

                List<string> mDestinatarios = new List<string>();
                var qDestinatarios = from c in db.MF_Catalogo where c.CODIGO_TABLA == Const.CATALOGO.DESTINATARIOS_ADICIONALES_AUTORIZACION_DESARMADO select c;

                var qTienda = (from c in db.MF_Cobrador join u in db.USUARIO on c.CODIGO_SUPERVISOR equals u.CELULAR where c.COBRADOR == desarmado.Tienda select new { Usuario = u.USUARIO1, Email = c.SUPERVISOR }).First();
                mDestinatarios.Add(string.Format("{0},{1}", qTienda.Usuario, qTienda.Email));

                foreach (var item in qDestinatarios)
                {
                    mDestinatarios.Add(string.Format("{0},{1}", item.CODIGO_CAT, item.NOMBRE_CAT));
                }

                foreach (string item in mDestinatarios)
                {
                    string[] MailDestinatario = item.Split(new string[] { "," }, StringSplitOptions.None);

                    string mAutorizar = string.Format("{2}/solicitudDesarmado.aspx?proc={0}&sol=T&g={1}&amb={3}", mDesarmado, MailDestinatario[0], General.URLPrincipal,General.Ambiente);
                    string mRechazar = string.Format("{2}/solicitudDesarmado.aspx?proc={0}&sol=B&g={1}&amb={3}", mDesarmado, MailDestinatario[0], General.URLPrincipal, General.Ambiente);
                    List<string> mCorreo = new List<string>();
                    mCorreo.Add(MailDestinatario[1]);

                    #region "Cuerpo Correo"
                    StringBuilder mBody = new StringBuilder();

                    mBody.Append("<!DOCTYPE html><html lang='en-us'>");
                    mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } </style></head>");
                    mBody.Append("<body>");
                    mBody.Append("<table align='left'>");
                    mBody.Append(string.Format("<tr><td><b>Desarmado:</b></td><td><b>{0}</b></td></tr>", mDesarmado));
                    mBody.Append(string.Format("<tr><td>Vendedor:</td><td>{0} - {1}</td></tr>", mNombreVendedor, qVendedor.COBRADOR));
                    mBody.Append(string.Format("<tr><td>Fecha:</td><td>{0}{1}/{2}{3}/{4}</td></tr>", desarmado.FechaDesarmado.Day < 10 ? "0" : "", desarmado.FechaDesarmado.Day.ToString(), desarmado.FechaDesarmado.Month < 10 ? "0" : "", desarmado.FechaDesarmado.Month.ToString(), desarmado.FechaDesarmado.Year.ToString()));
                    mBody.Append("<tr><td colspan='2'>&nbsp;</td></tr>");
                    mBody.Append(string.Format("<tr><td>Factura:</td><td>{0}</td></tr>", desarmado.Factura));
                    mBody.Append(string.Format("<tr><td>Cliente:</td><td>{0} - {1}</td></tr>", qCliente.CLIENTE1, qCliente.NOMBRE));

                    foreach (var linea in desarmado.Articulo)
                    {
                        Int32 mExistencias = 0; string mExistenciasBodegas = ""; string mGEL = "No";
                        string mNombreArticulo = (from a in db.ARTICULO where a.ARTICULO1 == linea.Articulo select a).First().DESCRIPCION;

                        if (qPedidLinea.Where(x => x.ARTICULO == linea.Articulo).Count() > 0)
                        {
                            if (qPedidLinea.Where(x => x.ARTICULO == linea.Articulo).First().GEL == "S") mGEL = "Sí";
                        }

                        Existencias(linea.Articulo, ref mExistencias, ref mExistenciasBodegas);

                        mBody.Append("<tr><td colspan='2'>&nbsp;</td></tr>");
                        mBody.Append(string.Format("<tr><td>Art&#237;culo:</td><td>{0}</td></tr>", linea.Articulo));
                        mBody.Append(string.Format("<tr><td>Descripci&#243;n:</td><td>{0}</td></tr>", mNombreArticulo));
                        mBody.Append(string.Format("<tr><td>GEL:</td><td>{0}</td></tr>", mGEL));
                        mBody.Append(string.Format("<tr><td>Existencias:</td><td>{0}</td></tr>", mExistencias));
                        mBody.Append(string.Format("<tr><td>Ubicaciones:</td><td>{0}</td></tr>", mExistenciasBodegas));
                    }

                    mBody.Append("<tr><td colspan='2'>&nbsp;</td></tr>");
                    mBody.Append(string.Format("<tr><td>Notas:</td><td>{0}</td></tr>", desarmado.Observaciones));
                    mBody.Append("<tr><td colspan='2' style='text-align: center'>");
                    mBody.Append(string.Format("<a href='{0}' tabindex='0'>Autorizar</a>", mAutorizar));
                    mBody.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                    mBody.Append(string.Format("<a href='{0}' tabindex='0'>Rechazar</a>", mRechazar));
                    mBody.Append("</td></tr>");
                    mBody.Append("</table>");
                    mBody.Append("<p>&nbsp;</p>");
                    mBody.Append("</body>");
                    mBody.Append("</html>");
                    #endregion

                    try
                    {
                        //INTENTO 1
                        Utilitario.EnviarEmail(qVendedor.EMAIL, mCorreo, string.Format("Solicitud de desarmado en {0}", desarmado.Tienda), mBody, mNombreVendedor);
                        log.Info("Envio de solicitud de desarmado en " + desarmado.Tienda + " para " + item+" en el intento (1)");
                    }
                    catch(Exception ex)
                    {
                        log.Error("Error en el intento 1 al enviar el desarmado de tienda para "+item+" error: "+ex.Message);
                        //INTENTO 2
                        try
                        {
                            Utilitario.EnviarEmail(qVendedor.EMAIL, mCorreo, string.Format("Solicitud de desarmado en {0}", desarmado.Tienda), mBody, mNombreVendedor);
                            log.Info("Envio de solicitud de desarmado en " + desarmado.Tienda + " para " + item + " en el intento (2)");
                        }
                        catch(Exception exi)
                        {
                            log.Error("Error en el intento 2 al enviar el desarmado de tienda para " + item + " error: " + ex.Message);
                            Utilitario.EnviarEmail(qVendedor.EMAIL, mCorreo, string.Format("Solicitud de desarmado en {0}", desarmado.Tienda), mBody, mNombreVendedor);
                            log.Info("Envio de solicitud de desarmado en " + desarmado.Tienda + " para " + item + " en el intento (3)");
                        }
                    }

                }
                //notificación al vendedor

                StringBuilder mCuerpoCorreo = new StringBuilder();
                mCuerpoCorreo.Append(string.Format("<html><body><br/>Estimado {0}: <br/> En este momento se ha enviado la solicitud de desarmado en {1} No.{2} a su supervisor.<br/><br/> <b> Por favor proporcione este número para cualquier asistencia que necesite.<b><br/> saludos,", mNombreVendedor,desarmado.Tienda, mDesarmado));
                mCuerpoCorreo.Append("</body></html>");
                Utilitario.EnviarEmail(qVendedor.EMAIL, qVendedor.EMAIL, string.Format("Solicitud de desarmado en {0}", desarmado.Tienda), mCuerpoCorreo, "MUEBLES FIESTA - NOTIFICACIONES");
                mRetorna = "Solicitud de autorización de desarmado enviada exitosamente";

            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0} {1}", CatchClass.ExMessage(ex, "SolicitudDesarmadoController", "Post"), "");
            }

            return mRetorna;
        }

        DateTime FechaArmado(DateTime FechaEntrega)
        {
            DateTime mFechaArmado = FechaEntrega.AddDays(2);
            if (mFechaArmado.DayOfWeek == DayOfWeek.Sunday && (mFechaArmado.Month == 1 || mFechaArmado.Month == 3 || mFechaArmado.Month == 4 || mFechaArmado.Month == 6 || mFechaArmado.Month == 8 || mFechaArmado.Month == 9 || mFechaArmado.Month == 10 || mFechaArmado.Month == 11)) mFechaArmado = mFechaArmado.AddDays(1);
            return mFechaArmado;
        }

        // PUT: api/SolicitudDesarmado/5
        public string Put([FromBody]Desarmado desarmado)
        {
            string mRetorna = "";
            Utils.Utilitarios ut = new Utils.Utilitarios();
            try
            {
                //Validando que el artículo exista para armado y que la categorización producto-tienda coincida
                if (desarmado.Tipo == "R")
                {
                    string msjValida = "";
                    desarmado.Articulo.ForEach(x =>
                    {
                        //Validando bandera de armado
                        if (!DALValidar.RequiereArmado(x.Articulo)) 
                            msjValida = string.Format("Error, el artículo {0} no maneja armado", x.Articulo);

                        //Validando categorización
                        if (!DALValidar.CompatibilidadArmadoEntreTiendaProducto(desarmado.Tienda, x.Articulo))
                        {
                            string errorMsj = "Error: El producto '{0}' no puede ser armado en la tienda '{1}' debido a la categoría. Para mayor información comuníquese con el departamento de importaciones.";
                            msjValida = string.Format(errorMsj, x.Descripcion, desarmado.Tienda);
                        }
                    });
                    if (msjValida.Length > 0) 
                        return msjValida;
                }
                string Ticketzoho = string.Empty;
                string mVendedor = desarmado.Vendedor; 
                int mRequisicion = 1;
                string mCliente = desarmado.Cliente; 
                string mCobrador = desarmado.Tienda;
                string mNombreVendedor = "";
                string mApellidosVendedor = string.Empty;
                string mNombresVendedor = string.Empty;
                string mMailVendedor = ""; 
                string mFactura = ""; 
                string mDescripcionServicio = "";
                string mMailArmados = ""; 
                string mMailArmados1 = ""; 
                string mMailArmados2 = ""; 
                string mMailArmados3 = "";
                string mNombreCliente = "";
                string mTelefonoResidencia = ""; 
                string mTelefonoTrabajo = ""; 
                string mTelefonoCelular = "";
                string mDireccionEntrega = "";
                string mDireccionEntregaCliente = string.Empty;
                string mColoniaEntrega = ""; 
                string mZonaEntrega = ""; 
                string mDepartamentoEntrega = ""; 
                string mMunicipioEntrega = ""; 
                string mIndicacionesLlegar = "";
                string mUsuarioAutoriza = "";
                bool mDePedido = false;
                string mPedido = desarmado.Pedido;
                if (mPedido.Trim().Length > 0) 
                    mDePedido = true;


                DALComunes mComunes = new DALComunes();
                List<int> mCatalogos = new List<int>();
                mCatalogos.Add(Const.CATALOGO.TIPO_DE_SERVICIO_ARMADO_PEDIDOS);
                mCatalogos.Add(Const.CATALOGO.TIPO_DE_SERVICIO_ARMADO_TIENDA);
                Respuesta mRespuesta = mComunes.ObtenerCatalogo(mCatalogos);

                List<Catalogo> mTipoServicioArmado = new List<Catalogo>();
                #region "validaciones"
                if (mRespuesta.Exito)
                {
                    mTipoServicioArmado = (List<Catalogo>)mRespuesta.Objeto;

                    try
                    {
                        mDescripcionServicio = (from TS in mTipoServicioArmado
                                                where TS.Codigo == desarmado.Tipo
                                                select TS.Descripcion
                                                ).FirstOrDefault().ToString();
                    }
                    catch
                    {
                        mDescripcionServicio = "Servicio al Cliente";
                    }
                }


                if (desarmado.Tipo == "S" && desarmado.Cliente.Trim().Length == 0) 
                    return "Error, Debe ingresar el cliente";
               

                if (desarmado.Cliente.Trim().Length > 0)
                {
                    try
                    {
                        int mCodigoCliente = Convert.ToInt32(desarmado.Cliente);
                    }
                    catch
                    {
                        return "Error, debe ingresar un código de cliente válido";
                    }

                    if ((from c in db.CLIENTE where c.CLIENTE1 == desarmado.Cliente select c).Count() == 0) 
                        return ("Error, cliente inválido");
                }

                if (mDePedido)
                {
                    int mCuantosArmado = (from p in db.MF_Pedido_Linea where p.PEDIDO == mPedido && p.REQUISICION_ARMADO == "S" select p).Count();
                    if (mCuantosArmado == 0) 
                        return "Error en este pedido no hay artículos marcados para armado o servicio";

                    DateTime mFechaMinimaArmado = FechaArmado(Convert.ToDateTime((from p in db.MF_Pedido where p.PEDIDO == mPedido select p).First().FECHA_ENTREGA));

                    if (desarmado.FechaDesarmado < mFechaMinimaArmado)
                        return string.Format("Error, la fecha mínima para el armado es el {0}{1}/{2}{3}/{4}", mFechaMinimaArmado.Day < 10 ? "0" : "", mFechaMinimaArmado.Day.ToString(), mFechaMinimaArmado.Month < 10 ? "0" : "", mFechaMinimaArmado.Month.ToString(), mFechaMinimaArmado.Year.ToString());
                }

                if (string.IsNullOrEmpty(mFactura))
                {
                    mFactura = string.IsNullOrEmpty(desarmado.Factura) ? "" : desarmado.Factura;
                    //validando la factura en caso la ingresaran en el pdv
                    if (mFactura.Length > 0)
                    {
                        var existe = db.FACTURA.Where(x => x.FACTURA1 == mFactura).Count() > 0;
                        if (!existe)
                            return "La factura ingresda no existe, por favor verifique";
                    }

                }


                if (desarmado.Tipo == "D") 
                    mUsuarioAutoriza = (from u in db.USUARIO where u.USUARIO1 == desarmado.UsuarioAutoriza select u).First().NOMBRE;
                #endregion

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    string mEnTienda = "S";
                    if (mCliente.Length == 0) mEnTienda = "N";

                    var qConfigura = from c in db.MF_Configura select c;
                    foreach (var item in qConfigura)
                    {
                        mMailArmados1 = item.mailArmados1;
                        mMailArmados2 = item.mailArmados2;
                        mMailArmados3 = item.mailArmados3;
                    }

                    mMailArmados = mMailArmados1;
                    if ((from c in db.MF_Cobrador where c.COBRADOR == mCobrador select c).First().ES_CIUDAD == "N") 
                        mMailArmados = mMailArmados2;

                    var qVendedor = db.MF_Vendedor.Where(x => x.VENDEDOR == mVendedor).FirstOrDefault();
                        mNombreVendedor = string.Format("{0} {1}", qVendedor.NOMBRE, qVendedor.APELLIDO);
                    mNombresVendedor = qVendedor.NOMBRE;
                    mApellidosVendedor = qVendedor.APELLIDO;
                        mMailVendedor = qVendedor.EMAIL;
                    

                    var qDireccion =
                        (from c in db.CLIENTE
                        join d in db.DETALLE_DIRECCION on c.DETALLE_DIRECCION equals d.DETALLE_DIRECCION1
                        where c.CLIENTE1 == mCliente
                        select d).FirstOrDefault();

                    if (qDireccion != null)
                    { 
                        mDireccionEntrega = string.Format("{0} {1} {2}", qDireccion.CAMPO_1 == (string)null ? "" : qDireccion.CAMPO_1.ToUpper(), qDireccion.CAMPO_2 == (string)null ? "" : qDireccion.CAMPO_2.ToUpper(), qDireccion.CAMPO_3 == (string)null ? "" : qDireccion.CAMPO_3.ToUpper());
                        mColoniaEntrega = qDireccion.CAMPO_5 == (string)null ? "" : qDireccion.CAMPO_5.ToUpper();
                        mZonaEntrega = qDireccion.CAMPO_4 == (string)null ? "" : qDireccion.CAMPO_4.ToUpper();
                        mDepartamentoEntrega = qDireccion.CAMPO_6 == (string)null ? "" : qDireccion.CAMPO_6.ToUpper();
                        mMunicipioEntrega = qDireccion.CAMPO_7 == (string)null ? "" : qDireccion.CAMPO_7.ToUpper();
                        mIndicacionesLlegar = qDireccion.CAMPO_10 == (string)null ? "" : qDireccion.CAMPO_10.ToUpper();
                        mDireccionEntregaCliente = $"{mDireccionEntrega} {mColoniaEntrega} {mZonaEntrega} {mMunicipioEntrega},{mDepartamentoEntrega}";
                    }
                    var qCliente = db.CLIENTE.Where(x => x.CLIENTE1 == mCliente).FirstOrDefault();
                        
                    if(qCliente!=null)
                    {
                        mNombreCliente = qCliente.NOMBRE;
                        mTelefonoResidencia = qCliente.TELEFONO1;
                        mTelefonoTrabajo = qCliente.TELEFONO2;
                        mTelefonoCelular = qCliente.FAX;
                    }

                int mCuantos = 0;
                    if ((from p in db.MF_PedidoArmado select p).Count() > 0) 
                        mRequisicion = (from p in db.MF_PedidoArmado select p.NO_REQUISICION).Max() + 1;

                    if (!mDePedido)
                    {
                        mPedido = string.Format("P{0}", mRequisicion.ToString());
                        desarmado.Usuario = (from u in db.USUARIO where u.CELULAR == mVendedor select u).First().USUARIO1;
                    }
                    else
                    {
                        mFactura = (from f in db.FACTURA where f.PEDIDO == mPedido select f).First().FACTURA1;
                        mCuantos = (from p in db.MF_PedidoArmado where p.PEDIDO == mPedido select p).Count();
                    }

                    
                    if (string.IsNullOrEmpty(mCliente))
                    {
                        mCliente = string.IsNullOrEmpty(desarmado.Cliente) ? "" : desarmado.Cliente;
                    }

                    if (mCuantos == 0)
                    {
                        MF_PedidoArmado iArmado = new MF_PedidoArmado()
                        {
                            PEDIDO = mPedido,
                            FACTURA = mFactura,
                            CLIENTE = mCliente,
                            TIPO = desarmado.Tipo,
                            FECHA_ARMADO = desarmado.FechaDesarmado,
                            OBSERVACIONES = desarmado.Observaciones,
                            NO_REQUISICION = mRequisicion,
                            ENVIADA = "S",
                            COBRADOR = mCobrador,
                            VENDEDOR = mVendedor,
                            EN_TIENDA = mEnTienda,
                            GRABADA = "N",
                            MODIFICADA = "N",
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("FA/{0}", desarmado.Usuario),
                            UpdatedBy = string.Format("FA/{0}", desarmado.Usuario),
                            CreateDate = DateTime.Now
                        };

                        db.MF_PedidoArmado.AddObject(iArmado);
                    }
                    else
                    {
                        var qReq = from p in db.MF_PedidoArmado where p.PEDIDO == mPedido select p;
                        mRequisicion = qReq.First().NO_REQUISICION;
                        #region "Correo"
                        StringBuilder mBody = new StringBuilder();

                        mBody.Append("<!DOCTYPE html><html lang='en-us'>");
                        mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } .style1 { color: #8C4510; background-color: #FFF7E7; font-size: xx-small; text-align: left; height:35px; } .style2 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; height:35px; } .style3 { color: #8C4510; background-color: #FFF7E7; font-size: x-small; text-align: left; height:35px; } .style4 { border: 1px solid #8C4510; } .style5 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; text-align: left; height:35px; } </style></head>");
                        mBody.Append("<body>");
                        mBody.Append("<table align='center'>");

                        mBody.Append(string.Format("<tr><td class='style5'><b>No. de requisici&#243;n:&nbsp;&nbsp;</b></td><td class='style3'><b>&nbsp;&nbsp;{0}&nbsp;&nbsp;</b></td></tr>", mRequisicion));
                        mBody.Append(string.Format("<tr><td class='style5'>Tienda:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td></tr>", mCobrador));
                        mBody.Append(string.Format("<tr><td class='style5'>Nuevo tipo:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td></tr>", desarmado.Tipo == "A" ? "Armado" : "Desarmado y Armado por cobro"));
                        mBody.Append(string.Format("<tr><td class='style5'>Nueva fecha de armado:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}{1}/{2}{3}/{4}&nbsp;&nbsp;</td></tr>", desarmado.FechaDesarmado.Day < 10 ? "0" : "", desarmado.FechaDesarmado.Day.ToString(), desarmado.FechaDesarmado.Month < 10 ? "0" : "", desarmado.FechaDesarmado.Month.ToString(), desarmado.FechaDesarmado.Year.ToString()));
                        mBody.Append(string.Format("<tr><td class='style5'>Nuevas observaciones:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td></tr>", desarmado.Observaciones));
                        mBody.Append(string.Format("<tr><td class='style5'>Usuario modific&#243;:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td></tr>", mNombreVendedor));

                        mBody.Append("</table>");
                        mBody.Append("</br>");
                        mBody.Append("</br>");
                        mBody.Append("<table align='center'>");

                        foreach (var item in qReq)
                        {
                            mBody.Append(string.Format("<tr><td class='style5'><b>No. de requisici&#243;n:&nbsp;&nbsp;</b></td><td class='style3'><b>&nbsp;&nbsp;{0}&nbsp;&nbsp;</b></td></tr>", item.NO_REQUISICION));
                            mBody.Append(string.Format("<tr><td class='style5'>Tienda:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td></tr>", item.COBRADOR));
                            mBody.Append(string.Format("<tr><td class='style5'>Tipo anterior:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td></tr>", item.TIPO == "A" ? "Armado" : "Desarmado y Armado por cobro"));
                            mBody.Append(string.Format("<tr><td class='style5'>Fecha de armado anterior:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}{1}/{2}{3}/{4}&nbsp;&nbsp;</td></tr>", item.FECHA_ARMADO.Day < 10 ? "0" : "", item.FECHA_ARMADO.Day.ToString(), item.FECHA_ARMADO.Month < 10 ? "0" : "", item.FECHA_ARMADO.Month.ToString(), item.FECHA_ARMADO.Year.ToString()));
                            mBody.Append(string.Format("<tr><td class='style5'>Observaciones anteriores:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td></tr>", item.OBSERVACIONES));
                        }

                        mBody.Append("</table>");

                        mBody.Append("</body>");
                        mBody.Append("</html>");

                        
                                List<string>  mCuentaCorreo = new List<string>();

                                if (mMailArmados1.Contains("@")) mCuentaCorreo.Add(mMailArmados1);
                                if (mMailArmados2.Contains("@")) mCuentaCorreo.Add(mMailArmados2);
                                if (mMailArmados3.Contains("@")) mCuentaCorreo.Add(mMailArmados3);
                                
                                string mAsunto = string.Format("Se modificó la requisición {0} en {1}", mRequisicion, mCobrador);
                                
                                ut.EnviarEmail(mMailVendedor, mCuentaCorreo, mAsunto, mBody, mNombreVendedor);

                        #endregion
                        foreach (var item in qReq)
                        {
                            item.TIPO = desarmado.Tipo;
                            item.FECHA_ARMADO = desarmado.FechaDesarmado;
                            item.OBSERVACIONES = string.Format("{0} / {1}", item.OBSERVACIONES, desarmado.Observaciones);
                            item.MODIFICADA = "S";
                            item.RecordDate = DateTime.Now;
                            item.UpdatedBy = string.Format("FA/{0}", desarmado.Usuario);
                        }

                    }

                    if (!mDePedido)
                    {
                        int mLinea = 0;
                        foreach (var item in desarmado.Articulo)
                        {
                            mLinea++;
                            MF_PedidoArmadoLinea iArmadoLinea = new MF_PedidoArmadoLinea()
                            {
                                PEDIDO = string.Format("P{0}", mRequisicion.ToString()),
                                LINEA = mLinea,
                                NO_REQUISICION = mRequisicion,
                                ARTICULO = item.Articulo,
                                RecordDate = DateTime.Now,
                                CreatedBy = string.Format("FA/{0}", desarmado.Usuario),
                                UpdatedBy = string.Format("FA/{0}", desarmado.Usuario),
                                CreateDate = DateTime.Now
                            };

                            db.MF_PedidoArmadoLinea.AddObject(iArmadoLinea);
                        }

                        if (desarmado.Tipo == "D")
                        {
                            var qDesarmado = from d in db.MF_DesarmadoTienda where d.DESARMADO == desarmado.NoDesarmado select d;
                            foreach (var item in qDesarmado)
                            {
                                item.ESTADO = "A";
                                item.USUARIO_AUTORIZA = desarmado.UsuarioAutoriza;
                                item.FECHA_HORA_AUTORIZA = DateTime.Now;
                                item.OBSERVACIONES_AUTORIZA = desarmado.ObservacionesAutoriza;
                            }
                        }
                    }

                    db.SaveChanges();
                    transactionScope.Complete();

                    mRetorna = string.Format("Req. No. {0} grabada exitosamente", mRequisicion.ToString());
                    if (desarmado.Tipo == "D") 
                        mRetorna = string.Format("Desarmado No. {0} autorizado exitosamente, se notificó al vendedor", desarmado.NoDesarmado);
                }


                #region "Zoho Desk"
                #region "informacion campos personalizados"
                List<Product> productos = new List<Product>();
                bool garantiaEspecial = false;
                string NombreTransportista = string.Empty;
                string Despacho = string.Empty;
                DateTime dtFechaFactura=new DateTime(1900,1,1);
                DateTime dtFechaEntrega = new DateTime(1900, 1, 1);
                //Productos
                desarmado.Articulo.ForEach(a =>
                {
                    productos.Add(new Product {
                         productCode=a.Articulo,
                          productName= $"{a.Descripcion}",
                           unitPrice=a.PrecioLista
                    });

                });
                if (mFactura.Length > 0)
                {
                    desarmado.Articulo.ForEach(x => {
                        var qPedidoLinea = db.MF_Pedido_Linea.Join(db.MF_Pedido, pl => pl.PEDIDO, p => p.PEDIDO, (pl, p) => new { pl, p }).Where
                            (y => y.p.FACTURA == mFactura && y.pl.ARTICULO == x.Articulo);
                        if (qPedidoLinea.Select(s => s.pl.GEL.Equals("S")).Count() > 0)
                            garantiaEspecial = true;
                        }
                    );
                    var resDespacho = from f in db.FACTURA.Where(x =>x.FACTURA1 == mFactura)
                                 join des in db.MF_Despacho on f.FACTURA1 equals des.FACTURA into xjoin
                                 from m in xjoin.DefaultIfEmpty()
                                 join trans in db.MF_Transportista on m.TRANSPORTISTA equals trans.TRANSPORTISTA into xjoin1
                                 from t in xjoin1.DefaultIfEmpty()
                                 select new
                                 {
                                     DESPACHO =m.DESPACHO,
                                     FECHA_FACTURA=f.FECHA,
                                     FECHA_ENTREGA=f.FECHA_ENTREGA,
                                     PILOTO=  t.PILOTO,
                                     VEHICULO=t.VEHICULO
                                     
                                 };
                    if (resDespacho.Count() > 0)
                    {
                        NombreTransportista= $"{resDespacho.FirstOrDefault().PILOTO} - {resDespacho.FirstOrDefault().VEHICULO}";
                        Despacho = resDespacho.FirstOrDefault().DESPACHO;
                        dtFechaEntrega = resDespacho.FirstOrDefault().FECHA_ENTREGA;
                        dtFechaFactura = resDespacho.FirstOrDefault().FECHA_FACTURA;
                    }
                    
                }
                #endregion
                var qZohoTickets = db.MF_Catalogo.Where(x => x.CODIGO_TABLA == 72).Select(x => x.CODIGO_CAT);
                if (qZohoTickets.Contains(desarmado.Tipo))
                {
                    Dictionary<string, string> header = new Dictionary<string, string>();
                    header.Add("AppKey", General.AppkeyAutenticacion.ToString());
                    header.Add("Token", WebConfigurationManager.AppSettings["CookieName"].ToString());
                    Api api = new Api(WebConfigurationManager.AppSettings[$"UrlNetunimAPI{General.Ambiente}"].ToString());
                    string endpoint = "tickets";
                    Contacto co = new Contacto
                    {
                        email = mMailVendedor,
                        firstName = mNombresVendedor,
                        lastName = mApellidosVendedor,
                        phone=WebConfigurationManager.AppSettings["ZohoContactPhone"].ToString()
                    };

                    CamposPersonalizados campos = new CamposPersonalizados
                    {
                        cf_numero_de_factura = mFactura,
                        cf_descripcion = desarmado.Observaciones,
                        cf_solicitud = desarmado.Observaciones,
                        cf_usuario = desarmado.Usuario,
                        cf_nombre_del_comprador = mNombreCliente,
                        cf_direccion_de_entrega = mDireccionEntregaCliente,
                        cf_origen = "Fiesta",
                        cf_nombre_del_distribuidor= "Muebles Fiesta",
                        cf_tipo_de_garantia = garantiaEspecial ? "Especial Limitada" : "Normal",
                        cf_nombre_transportista = NombreTransportista,
                        cf_numero_de_envio = Despacho,
                        cf_fecha = dtFechaFactura.Year.ToString() + "-" + dtFechaFactura.Month.ToString("00") + "-" + dtFechaFactura.Day.ToString("00")+ "T10:00:00.000Z",
                        cf_fecha_de_entrega= dtFechaEntrega.Year.ToString() + "-" + dtFechaEntrega.Month.ToString("00") + "-" + dtFechaEntrega.Day.ToString("00"),

                    };
                    Ticket ticket1 = new Ticket
                    {
                        cf = campos,
                        contact = co,
                        departmentId = General.ZohoServicesDeptId,
                        email = mMailVendedor,
                        status = "Open",
                        statusType = "Open",
                        subject = $"Solcitud de " + mNombreVendedor
                    };
                    TicketRequest infoTicket = new TicketRequest
                    {
                         product=productos[0],
                         ticket=ticket1
                    };
                    IRestResponse response = api.Process(Method.POST, endpoint, infoTicket, header, null);
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        throw new Exception("Problema al generar el ticket en Zoho Desktop " + response.Content);
                    }
                    else
                        Ticketzoho=  response.Content.Replace("\"", string.Empty);
                }
                #endregion

                if (!mDePedido)
                {
                    string mCuerpo = ""; 
                    string mInfoArmado = ""; 
                    string mArticulos = "";
                    List<string> mCuentaCorreo = new List<string>();
                    string mAsunto = string.Empty;
                    if (desarmado.Tipo != "D")
                        mRetorna = string.Format("{0}, enviada a oficinas y a su correo", mRetorna);

                    mAsunto = string.Format("Requisición de Armado No. {0} - Tienda: {1}", mRequisicion.ToString(), mCobrador);
                    mCuerpo = string.Format("A continuación encontrarán la Requisición de Armado No. {0} {2} en {1} para su revisión.", mRequisicion.ToString(), mCobrador, desarmado.Tipo == "D" ? "<span class='verde'><strong>AUTORIZADA</strong></span>" : "generada");
                    mInfoArmado = string.Format("<BR><b>Tipo:</b> {0}<BR><b>Fecha de Armado:</b> {1}/{2}/{3}", mDescripcionServicio, desarmado.FechaDesarmado.Day, desarmado.FechaDesarmado.Month, desarmado.FechaDesarmado.Year);
                    
                    if (desarmado.Factura.Trim().Length > 0) 
                    mInfoArmado = string.Format("{0}<BR><b>Factura:</b> {1}", mInfoArmado, desarmado.Factura);
                    mInfoArmado += Ticketzoho.Length > 0 ? $"<br/><b>Zoho Desk Ticket:  </b>{Ticketzoho}":string.Empty;

                            if (mCliente.Length > 0)
                            {
                                mInfoArmado = string.Format("{11}<BR><BR><b>Cliente:</b> {0} - {1}<BR><b>Direccion</b>: {2} Zona {3} Colonia {4}<BR><b>Departamento:</b> {5}<BR><b>Municipio:</b> {6}<BR><b>Indicaciones para llegar:</b> {7}<BR><b>Télefono Casa:</b> {8}<BR><b>Teléfono Oficina:</b> {9}<BR><b>Celular:</b> {10}",
                                    mCliente, mNombreCliente, mDireccionEntrega, mZonaEntrega, mColoniaEntrega, mDepartamentoEntrega, mMunicipioEntrega, mIndicacionesLlegar, mTelefonoResidencia, mTelefonoTrabajo, mTelefonoCelular, mInfoArmado);

                                if ((from c in db.MF_Cobrador where c.DEPARTAMENTO == mDepartamentoEntrega select c).Count() == 0) mDepartamentoEntrega = "GUATEMALA";

                                mMailArmados = mMailArmados1;
                                if ((from c in db.MF_Cobrador where c.DEPARTAMENTO == mDepartamentoEntrega select c).First().ES_CIUDAD == "N") mMailArmados = mMailArmados2;
                            }

                            foreach (var item in desarmado.Articulo)
                            {
                                mArticulos = string.Format("{0}<BR>{1} - {2}", mArticulos, item.Articulo, item.Descripcion);
                            }

                            if (mMailVendedor.Contains("@")) 
                                mCuentaCorreo.Add(mMailVendedor);

                           

                            StringBuilder mBody = new StringBuilder();
                            mBody.Append("<!DOCTYPE html><html lang='en-us'>");
                            mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } .verde { color: #24E711; }</style></head>");
                            mBody.Append("<body>");
                            mBody.Append(string.Format("<p>Estimado(s),</p>"));
                            mBody.Append(string.Format("<p>{0}</p>", mCuerpo));
                            mBody.Append(mInfoArmado);
                            mBody.Append(string.Format("<BR><BR><b>Artículos:</b><BR>{0}", mArticulos));
                            mBody.Append(string.Format("<p><b>Observaciones: </b>{0}</p>", desarmado.Observaciones));
                            mBody.Append("<p>Atentamente,</p>");
                            mBody.Append(string.Format("<p>{0}<BR><b>Muebles Fiesta - {1}</b></p>", desarmado.Tipo == "D" ? mUsuarioAutoriza : mNombreVendedor, mCobrador));
                            mBody.Append("</body>");
                            mBody.Append("</html>");
                            
                            ut.EnviarEmail(mMailVendedor,mCuentaCorreo,mAsunto,mBody, desarmado.Tipo == "D" ? mUsuarioAutoriza : mNombreVendedor);
                            
                }

               
            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0} {1}", CatchClass.ExMessage(ex, "SolicitudDesarmadoController", "Put"), "");
            }

            return mRetorna;
        }

        // DELETE: api/SolicitudDesarmado/5
        public string Delete([FromBody]Desarmado desarmado)
        {
            string mRetorna = "";

            try
            {
                string mUsuarioRechaza = (from u in db.USUARIO where u.USUARIO1 == desarmado.UsuarioAutoriza select u).First().NOMBRE;

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    var qDesarmado = from d in db.MF_DesarmadoTienda where d.DESARMADO == desarmado.NoDesarmado select d;
                    foreach (var item in qDesarmado)
                    {
                        item.ESTADO = "R";
                        item.USUARIO_AUTORIZA = desarmado.UsuarioAutoriza;
                        item.FECHA_HORA_AUTORIZA = DateTime.Now;
                        item.OBSERVACIONES_AUTORIZA = desarmado.ObservacionesAutoriza;
                    }

                    db.SaveChanges();
                    transactionScope.Complete();
                }

                string mCuerpo = ""; string mInfoArmado = ""; string mArticulos = ""; string mMailVendedor = ""; string mNombreVendedor = "";
                mRetorna = string.Format("Desarmado No. {0} rechazado exitosamente, se notificó al vendedor", desarmado.NoDesarmado.ToString());

                var qCliente = (from f in db.FACTURA join c in db.CLIENTE on f.CLIENTE equals c.CLIENTE1 where f.FACTURA1 == desarmado.Factura select c).First();

                using (SmtpClient smtp = new SmtpClient())
                {
                    using (MailMessage mail = new MailMessage())
                    {
                        mail.Subject = string.Format("Rechazo de desarmado No. {0} - Tienda: {1}", desarmado.NoDesarmado.ToString(), desarmado.Tienda);
                        mCuerpo = string.Format("Se le informa que su solicitud de desarmado No. {0} fue <span class='rojo'><strong>RECHAZADA</strong></span>.", desarmado.NoDesarmado.ToString());
                        mInfoArmado = string.Format("<BR><b>Tipo:</b> {0}<BR><b>Fecha de Armado:</b> {1}/{2}/{3}", desarmado.Tipo == "A" ? "Armado en tienda" : desarmado.Tipo == "D" ? "Desarmado en tienda" : desarmado.Tipo == "S" ? "Servicio a cliente" : "Servicio en tienda", desarmado.FechaDesarmado.Day, desarmado.FechaDesarmado.Month, desarmado.FechaDesarmado.Year);
                        if (desarmado.Factura.Trim().Length > 0) mInfoArmado = string.Format("{0}<BR><b>Factura:</b> {1}<BR><b>Cliente:</b> {2} - {3}", mInfoArmado, desarmado.Factura, qCliente.CLIENTE1, qCliente.NOMBRE);

                        foreach (var item in desarmado.Articulo)
                        {
                            mArticulos = string.Format("{0}<BR>{1} - {2}", mArticulos, item.Articulo, item.Descripcion);
                        }

                        var qVendedor = from v in db.MF_Vendedor where v.VENDEDOR == desarmado.Vendedor select v;
                        foreach (var item in qVendedor)
                        {
                            mNombreVendedor = string.Format("{0} {1}", item.NOMBRE, item.APELLIDO);
                            mMailVendedor = item.EMAIL;
                        }

                        if (mMailVendedor.Contains("@")) mail.To.Add(mMailVendedor);

                        mail.IsBodyHtml = true;
                        mail.ReplyToList.Add(mMailVendedor);
                        mail.From = new MailAddress(mMailVendedor, mUsuarioRechaza);

                        StringBuilder mBody = new StringBuilder();

                        mBody.Append("<!DOCTYPE html><html lang='en-us'>");
                        mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } .rojo { color: #FF0000; }</style></head>");
                        mBody.Append("<body>");
                        mBody.Append(string.Format("<p>Estimado(s),</p>"));
                        mBody.Append(string.Format("<p>{0}</p>", mCuerpo));
                        mBody.Append(mInfoArmado);
                        mBody.Append(string.Format("<BR><BR><b>Artículos:</b><BR>{0}", mArticulos));
                        mBody.Append(string.Format("<p><b>Observaciones: </b>{0}</p>", desarmado.ObservacionesAutoriza));
                        mBody.Append("<p>Atentamente,</p>");
                        mBody.Append(string.Format("<p>{0}<BR><b>Muebles Fiesta - {1}</b></p>", mUsuarioRechaza, desarmado.Tienda));
                        mBody.Append("</body>");
                        mBody.Append("</html>");

                        mail.Body = mBody.ToString();

                        smtp.Host = WebConfigurationManager.AppSettings["MailHost2"];
                        smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort2"]);
                        smtp.EnableSsl = true;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                        try
                        {
                            smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail2"], WebConfigurationManager.AppSettings["PwdMail2"]);
                            smtp.Send(mail);
                        }
                        catch (Exception ex2)
                        {
                            string mm = "";
                            try
                            {
                                mm = ex2.StackTrace.Substring(ex2.StackTrace.IndexOf("línea"));
                            }
                            catch
                            {
                                try
                                {
                                    mm = ex2.StackTrace.Substring(ex2.StackTrace.IndexOf("line"));
                                }
                                catch
                                {
                                    //Nothing
                                }
                            }

                            mRetorna = string.Format("Error, La requsición fue rechazada pero hubo error al enviar {0} {1}", ex2.Message, mm);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0} {1}", CatchClass.ExMessage(ex, "SolicitudDesarmadoController", "Delete"), "");
            }

            return mRetorna;
        }

    }
}
