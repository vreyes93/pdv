﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;

namespace FiestaNETRestServices.Controllers.Bancos
{
    public class ConfirmaBoletaController : MFApiController
    {
        // GET api/confirmaboleta
        public string GetConfirmaBoleta()
        {
            return "Este servicio no retorna ningún valor";
        }

        // GET api/confirmaboleta/5
        public string GetConfirmaBoleta(int id)
        {
            return "Este servicio no retorna ningún valor";
        }

        // POST api/confirmaboleta
        public string PostConfirmaBoleta([FromBody]BoletaConfirmar boleta)
        {
            string mRetorna = "";
            db.CommandTimeout = 999999999;

            try
            {
                var qVendedor = (from v in db.MF_Vendedor where v.VENDEDOR == boleta.Vendedor select v).First();
                var qDestinatarios = from c in db.MF_Catalogo where c.CODIGO_TABLA == 15 orderby c.ORDEN select c;
                string mJefe = (from c in db.MF_Cobrador where c.COBRADOR == qVendedor.COBRADOR select c).First().JEFE;
                
                StringBuilder mBody = new StringBuilder();
                
                mBody.Append("<!DOCTYPE html><html lang='en-us'>");
                mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } .style1 { color: #8C4510; background-color: #FFF7E7; font-size: xx-small; text-align: left; height:35px; } .style2 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; height:35px; } .style3 { color: #8C4510; background-color: #FFF7E7; font-size: x-small; text-align: left; height:35px; } .style4 { border: 1px solid #8C4510; } .style5 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; text-align: left; height:35px; } </style></head>");
                mBody.Append("<body>");
                mBody.Append("Estimados:<br/><br/>Por este medio se les solicita confirmar datos del siguiente depósito:");
                mBody.Append("<br />");
                mBody.Append("<br />");
                mBody.Append("<table align='center'>");

                mBody.Append(string.Format("<tr><td class='style5'><b>No. de boleta:&nbsp;&nbsp;</b></td><td class='style3'><b>&nbsp;&nbsp;{0}&nbsp;&nbsp;</b></td></tr>", boleta.Boleta));
                mBody.Append(string.Format("<tr><td class='style5'>Cuenta:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td></tr>", boleta.Banco));
                mBody.Append(string.Format("<tr><td class='style5'>Fecha:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}{1}/{2}{3}/{4}&nbsp;&nbsp;</td></tr>", boleta.Fecha.Day < 10 ? "0" : "", boleta.Fecha.Day.ToString(), boleta.Fecha.Month < 10 ? "0" : "", boleta.Fecha.Month.ToString(), boleta.Fecha.Year.ToString()));
                mBody.Append(string.Format("<tr><td class='style5'>Monto:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td></tr>", string.Format("{0:0,0.00}", boleta.Monto)));

                mBody.Append("</table>");

                mBody.Append("</body>");
                mBody.Append("</html>");

                string mRemitente = qVendedor.EMAIL;
                string mDisplayName = string.Format("{0} {1}", qVendedor.NOMBRE, qVendedor.APELLIDO);
                string mAsunto = string.Format("Confirmación boleta {0}", boleta.Boleta.ToString());

                List<string> mDestinatarios = new List<string>();
                foreach (var item in qDestinatarios)
                {
                    mDestinatarios.Add(item.NOMBRE_CAT);
                }

                mDestinatarios.Add(mJefe);
                Utilitario.EnviarEmail(mRemitente, mDestinatarios, mAsunto, mBody, mDisplayName);

                mRetorna = string.Format("Se solicitó confirmación de boleta {0}, espere respuesta a su correo", boleta.Boleta);
            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0} {1}", CatchClass.ExMessage(ex, "ConfirmaBoletaController", "PostConfirmaBoleta"), "");
            }

            return mRetorna;
        }

    }
}
