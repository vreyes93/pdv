﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;

namespace FiestaNETRestServices.Controllers.Bodega
{
    public class AjustesCierreController : MFApiController
    {
        // GET: api/AjustesCierre
        public Respuesta Get()
        {
            return new Respuesta(false, "Debe enviar el año y el mes");
        }

        // GET: api/AjustesCierre/5
        public List<AjustesInventario> Get(string anio, string mes)
        {
            List<AjustesInventario> mRetorna = new List<AjustesInventario>();

            if (anio == null) return mRetorna;
            if (anio.Trim().Length == 0) return mRetorna;

            int mMes = Convert.ToInt32(mes);
            int mAnio = Convert.ToInt32(anio);

            var q =
                (from i in db.MF_Inventario
                 join b in db.BODEGA on new { Bodega = i.BODEGA } equals new { Bodega = b.BODEGA1 }
                 where i.ANIO == mAnio && i.MES == mMes
                 orderby i.BODEGA
                 select new AjustesInventario()
                 {
                     Bodega = i.BODEGA,
                     Nombre = b.NOMBRE,
                     Estado = i.CERRADO == "S" ? "Cerrada" : ""
                 });

            return q.ToList();
        }

        // POST: api/AjustesCierre
        public string Post(string bodega, string anio, string mes)
        {
            string mRetorna = "";
            db.CommandTimeout = 999999999;

            try
            {
                if (bodega == null)
                {
                    mRetorna = "Error, debe enviar la bodega";
                    return mRetorna;
                }

                if (bodega.Trim().Length == 0)
                {
                    mRetorna = "Error, debe enviar la bodega";
                    return mRetorna;
                }

                int mMes = Convert.ToInt32(mes);
                int mAnio = Convert.ToInt32(anio);
                string mNombreMes = Utilitario.Mes(new DateTime(mAnio, mMes, 1));

                var qInv = from i in db.MF_Inventario where i.BODEGA == bodega && i.ANIO == mAnio && i.MES == mMes select i;
                if (qInv.Count() == 0)
                {
                    mRetorna = string.Format("Error, no se encontró información de {0} {1} {2}", bodega, mNombreMes, anio);
                    return mRetorna;
                }

                if (qInv.First().CERRADO == "N")
                {
                    mRetorna = string.Format("Error, {0} {1} {2} NO está cerrado", mNombreMes, anio, bodega);
                    return mRetorna;
                }

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    var qInventario = from i in db.MF_Inventario where i.BODEGA == bodega && i.ANIO == mAnio && i.MES == mMes select i;

                    foreach (var item in qInventario)
                    {
                        item.CERRADO = "N";
                    }

                    db.SaveChanges();
                    transactionScope.Complete();
                }

                mRetorna = string.Format("{0} abierta nuevamente", bodega);
            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0} {1}", CatchClass.ExMessage(ex, "AjustesCierreController", "Post"), "");
            }

            return mRetorna;
        }

        // PUT: api/AjustesCierre/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/AjustesCierre/5
        public void Delete(int id)
        {
        }
    }
}
