﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;

namespace FiestaNETRestServices.Controllers.Bodega
{
    public class ArticuloController : MFApiController
    {
        // GET api/articulo
        public Respuesta GetArticulo()
        {
            return new Respuesta(false, "Debe ingresar un criterio de búsqueda");
        }

        // GET api/articulo/5
        public List<Articulos> GetArticulo(string id)
        {
            List<Articulos> mArticulos = new List<Articulos>();

            if (id == null) return mArticulos;
            if (id.Trim().Length <= 2) return mArticulos;

            id = id.ToUpper();
            bool mCodigo = true;

            string mCriterio = id.Substring(0, 2);
            string mSegundoCaracter = id.Substring(1, 1);

            int mNumero = 0;

            try
            {
                mNumero = Convert.ToInt32(mCriterio);
            }
            catch
            {
                mCodigo = false;
            }

            try
            {
                mNumero = Convert.ToInt32(mSegundoCaracter);
                mCodigo = true;
            }
            catch
            {
                mCodigo = false;
            }
            
            decimal mIVA = 1 + ((from im in db.IMPUESTO where im.IMPUESTO1 == "IVA" select im).First().IMPUESTO11 / 100);

            var q = from a in db.ARTICULO
                    where a.ACTIVO == "S"
                    orderby a.ARTICULO1
                    select new Articulos()
                    {
                        Articulo = a.ARTICULO1,
                        Descripcion = a.DESCRIPCION,
                        PrecioBase = a.PRECIO_BASE_LOCAL,
                        PrecioLista = a.PRECIO_BASE_LOCAL * mIVA,
                        Tooltip = ""
                    };

            if (mCodigo)
            {
                q = q.Where(x => x.Articulo.Equals(id));
            }
            else
            {
                q = q.Where(x => x.Descripcion.Contains(id));
            }

            foreach (var item in q)
            {
                string mTooltip = "";
                var qExistencias = from e in db.EXISTENCIA_LOTE where e.CANT_DISPONIBLE > 0 && e.ARTICULO == item.Articulo orderby e.BODEGA, e.LOCALIZACION select e;

                int ii = 0;
                int mExistencias = 0;

                foreach (var itemE in qExistencias)
                {
                    string mSeparador = "";
                    if (ii == 0) mSeparador = System.Environment.NewLine;

                    mExistencias += Convert.ToInt32(itemE.CANT_DISPONIBLE);
                    mTooltip = string.Format("{0}{1}{2} ({3}) = {4}", mTooltip, mSeparador, itemE.BODEGA, itemE.LOCALIZACION, itemE.CANT_DISPONIBLE.ToString().Replace(".00000000", ""));
                }

                if (mTooltip.Trim().Length == 0) mTooltip = string.Format("No hay existencias del artículo {0}", item.Articulo);

                Articulos mArticulo = new Articulos();
                mArticulo.Articulo = item.Articulo;
                mArticulo.Descripcion = item.Descripcion;
                mArticulo.PrecioBase = item.PrecioBase;
                mArticulo.PrecioLista = Math.Round(item.PrecioLista, 2, MidpointRounding.AwayFromZero);
                mArticulo.Tooltip = mTooltip;
                mArticulos.Add(mArticulo);
            }

            return mArticulos;
        }

    }
}
