﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Text;
using FiestaNETRestServices.Utils;

namespace FiestaNETRestServices.Controllers.Bodega
{
    public class CierreInventarioController : MFApiController
    {
        // GET: api/CierreInventario
        public Respuesta Get()
        {
            return new Respuesta(false, "Debe enviar la bodega");
        }

        /// <summary>
        /// Este método recibe bodega, anio y mes procesar el cierre
        /// </summary>
        /// <param name="tipo">"P" es preliminar, "D" descarga el PDF preliminar, "C" es el cierre definitivo, "R" es re-envio del cierre</param>
        // GET: api/CierreInventario/5
        public Clases.TomaInventario Get(string bodega, string anio, string mes, string usuario, string tipo)
        {
            Clases.TomaInventario mRetorna = new Clases.TomaInventario();

            try
            {
                if (bodega == null)
                {
                    mRetorna.Mensaje = "Error, debe enviar la bodega";
                    return mRetorna;
                }

                if (bodega.Trim().Length == 0)
                {
                    mRetorna.Mensaje = "Error, debe enviar la bodega";
                    return mRetorna;
                }

                int mMes = Convert.ToInt32(mes);
                int mAnio = Convert.ToInt32(anio);
                string mNombreMes = Utilitario.Mes(new DateTime(mAnio, mMes, 1));

                DateTime mFecha = new DateTime(mAnio, mMes, 1);
                mFecha = mFecha.AddMonths(1).AddDays(-1);
                mRetorna.Fecha = mFecha;

                bool mCerrar = false;
                if (tipo == "C") mCerrar = true;

                var qInv = from i in db.MF_Inventario where i.BODEGA == bodega && i.ANIO == mAnio && i.MES == mMes select i;
                if (qInv.Count() > 0)
                {
                    if (qInv.First().CERRADO == "S" && tipo == "P")
                    {
                        mRetorna.Mensaje = string.Format("Error, {0} {1} {2} ya está cerrado", mNombreMes, anio, bodega);
                        return mRetorna;
                    }

                    if (qInv.First().CERRADO == "S" && tipo == "C") mCerrar = false;
                    if (qInv.First().CERRADO == "N" && tipo == "R") mRetorna.Mensaje = string.Format("Erro, no está cerrado {0} {1} {2}", bodega, mNombreMes, anio);
                }

                if (qInv.Count() == 0 && tipo == "C")
                {
                    mRetorna.Mensaje = string.Format("Error, no se encontró información de {0} {1} {2}", bodega, mNombreMes, anio);
                    return mRetorna;
                }
                if (qInv.Count() == 0 && tipo == "R")
                {
                    mRetorna.Mensaje = string.Format("Error, no se encontró el cierre de {0} {1} {2}", bodega, mNombreMes, anio);
                    return mRetorna;
                }
                if (qInv.First().CERRADO == "N" && tipo == "R")
                {
                    mRetorna.Mensaje = string.Format("Error, no se encontró el cierre de {0} {1} {2}", bodega, mNombreMes, anio);
                    return mRetorna;
                }

                var qArticulos =
                    (from a in db.ARTICULO
                     join i in db.MF_InventarioDetalle on a.ARTICULO1 equals i.ARTICULO
                     where i.BODEGA == bodega && i.ANIO == mAnio && i.MES == mMes
                     select new ArticuloInventario()
                     {
                         Clasificacion = a.CLASIFICACION_3,
                         Articulo = a.ARTICULO1,
                         Descripcion = a.DESCRIPCION,
                         UltimaCompra = a.ULTIMO_INGRESO,
                         Blanco1 = i.BLANCO1,
                         Existencias = i.EXISTENCIA_TOTAL,
                         Blanco2 = "",
                         ExistenciaFisica = i.EXISTENCIA_FISICA,
                         Conteo = i.CONTEO == "0" ? "" : i.CONTEO,
                         Observaciones = i.OBSERVACIONES,
                         Tipo = bodega == "F01" ?   //Camas
                                                    a.ARTICULO1.Substring(0, 3) == "021"
                                                    || a.ARTICULO1.Substring(0, 3) == "081"
                                                    || a.ARTICULO1.Substring(0, 2) == "09"
                                                    || a.ARTICULO1.Substring(0, 3) == "110"
                                                    || a.ARTICULO1.Substring(0, 3) == "140"
                                                    || a.ARTICULO1.Substring(0, 3) == "141"
                                                    || a.ARTICULO1.Substring(0, 3) == "142"
                                                    || a.ARTICULO1.Substring(0, 3) == "143"
                                                    || a.ARTICULO1.Substring(0, 3) == "144"
                                                    || a.ARTICULO1.Substring(0, 3) == "145"
                                                    || a.ARTICULO1.Substring(0, 4) == "M141"
                                                    || a.ARTICULO1.Substring(0, 4) == "M142"
                                                        ? "CAMAS"
                                                        : a.ARTICULO1.Substring(0, 1) == "0" || a.ARTICULO1.Substring(0, 1) == "A"
                                                        ? "DECORACIÓN"
                                                        : "RESTO DE ARTÍCULOS" : "",
                         Diferencia = i.DIFERENCIA
                     }).Distinct().OrderBy(x => x.Articulo);

                if (mCerrar)
                {
                    using (TransactionScope transactionScope = new TransactionScope())
                    {
                        var qInventario = from i in db.MF_Inventario where i.BODEGA == bodega && i.ANIO == mAnio && i.MES == mMes select i;

                        foreach (var item in qInventario)
                        {
                            item.CERRADO = "S";
                            item.USUARIO_CERRO = string.Format("FA/{0}", usuario);
                            item.FECHA_CERRADO = DateTime.Now.Date;
                            item.FECHA_HORA_CERRADO = DateTime.Now;
                        }

                        db.SaveChanges();
                        transactionScope.Complete();
                    }
                }

                List<ArticuloInventario> data = qArticulos.ToList();
                //UtilitariosBodega func = new TomaInventarioController();
                

                mRetorna.Articulos = data;
                mRetorna.ArticulosCodigo = UtilitariosBodega.OrderData(data, "DESCRIPCION");
                mRetorna.ArticulosClasificacion = UtilitariosBodega.OrderData(data, "CLASIFICACION");
                mRetorna.Diferencias = mRetorna.Articulos.Where(x => x.Diferencia != 0).ToList();
                mRetorna.Mensaje = string.Format("Preliminar {0} {1} {2} generado exitosamente", bodega, mNombreMes, anio);
                if (mRetorna.Diferencias.Count() == 0) mRetorna.Mensaje = string.Format("No se encontraron diferencias para {0} en {1} {2}", bodega, mNombreMes, anio);

                if (tipo == "C" || tipo == "R")
                {
                    StringBuilder mBody = new StringBuilder();

                    mBody.Append("<!DOCTYPE html><html lang='en-us'>");
                    mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } .style1 { color: #8C4510; background-color: #FFF7E7; font-size: xx-small; text-align: left; height:35px; } .style2 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; height:35px; } .style3 { color: #8C4510; background-color: #FFF7E7; font-size: x-small; text-align: center; height:35px; } .style4 { border: 1px solid #8C4510; } .style5 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; text-align: center; height:35px; } </style></head>");
                    mBody.Append("<body>");
                    mBody.Append("<table align='center' class='style4'>");

                    mBody.Append("<tr><td class='style5'>Art&#237;culo</td>");
                    mBody.Append("<td class='style2'>&nbsp;Descripci&#243;n</td>");
                    mBody.Append("<td class='style5'>&nbsp;Diferencias&nbsp;</td>");
                    mBody.Append("<td class='style5'>&nbsp;Exist. Sistema&nbsp;</td>");
                    mBody.Append("<td class='style5'>&nbsp;Conteo físico&nbsp;</td>");
                    mBody.Append("<td class='style2'>&nbsp;Observaciones</td></tr>");

                    mRetorna.Diferencias.ForEach(x => {
                        mBody.Append(string.Format("<tr><td class='style3'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td>", x.Articulo));
                        mBody.Append(string.Format("<td class='style1'>&nbsp;{0}&nbsp;&nbsp;</td>", x.Descripcion));
                        mBody.Append(string.Format("<td class='style3'>{0}</td>", x.Blanco1));
                        mBody.Append(string.Format("<td class='style3'>{0}</td>", x.Existencias));
                        mBody.Append(string.Format("<td class='style3'>{0}</td>", x.ExistenciaFisica));
                        mBody.Append(string.Format("<td class='style1'>&nbsp;{0}&nbsp;&nbsp;</td></tr>", x.Observaciones));
                    });

                    mBody.Append("</table>");
                    mBody.Append("<p>&nbsp;</p>");
                    if (mRetorna.Diferencias.Count() == 0) mBody.Append(string.Format("<p>NOTA: No se encontraron diferencias en {0}</p>", bodega));
                    mBody.Append("</body>");
                    mBody.Append("</html>");

                    mRetorna.Html = mBody.ToString();
                    mRetorna.Asunto = string.Format("Inventario {0} - {1} {2}", bodega, Utilitario.Mes(new DateTime(mAnio, mMes, 1)), anio);

                    List<string> email = General.Ambiente == "PRO" ? db.MF_Gerente.Where(x => x.ENVIAR_INVENTARIO == "S").Select(x => x.EMAIL).ToList() : db.MF_Catalogo.Where(x => x.CODIGO_TABLA == 63 && x.ACTIVO == "S").Select(x => x.NOMBRE_CAT).ToList();
                    email.ForEach(x =>
                    {
                        mRetorna.Destinatarios.Add(x);
                    });

                    mRetorna.Mensaje = string.Format("Cierre {0} {1} {2} generado exitosamente", bodega, mNombreMes, anio);
                    if (tipo == "R") mRetorna.Mensaje = string.Format("Re-envío exitoso cierre {0} {1} {2}", bodega, mNombreMes, anio);

                    var qUsuario = (from u in db.USUARIO join v in db.MF_Vendedor on u.CELULAR equals v.VENDEDOR select v).First();
                    mRetorna.CorreoUsuario = General.Ambiente == "PRO" ? qUsuario.EMAIL : email[0];

                    if (!mCerrar && tipo == "C")
                    {
                        mRetorna.ConsultaCierre = true;
                        mRetorna.Mensaje = string.Format("El cierre de {0} {1} {2} quedó de la siguiente manera:", bodega, mNombreMes, anio);
                        if (mRetorna.Diferencias.Count() == 0) mRetorna.Mensaje = string.Format("{0} Sin diferencias", mRetorna.Mensaje);
                    }
                }

            }
            catch (Exception ex)
            {
                mRetorna.Mensaje = string.Format("{0} {1}", CatchClass.ExMessage(ex, "CierreInventarioController", "Get"), "");
            }

            return mRetorna;
        }

        // POST: api/CierreInventario
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/CierreInventario/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/CierreInventario/5
        public void Delete(int id)
        {
        }
    }
}
