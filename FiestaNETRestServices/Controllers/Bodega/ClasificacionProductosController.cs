﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Models;
using MF_Clases.Comun;
using System;
using System.Linq;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Bodega
{
    public class ClasificacionProductosController : MFApiController
    {
        public IHttpActionResult Get()
        {
            using (EXACTUSERPEntities db = new EXACTUSERPEntities())
            {
                try
                {
                    return Ok(db.MF_Clasificacion
                        .OrderBy(x => x.ORDEN)
                        .Select(x => new Clasificacion()
                        {
                            Id = x.MF_CLASIFICACION1,
                            Name = x.DESCRIPCION,
                            Order = x.ORDEN
                        })
                        .ToList());
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                }
            }
        }
    }
}
