﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.EntityClient;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;
using Newtonsoft.Json;
using RestSharp;

namespace FiestaNETRestServices.Controllers.Bodega
{
    public class DespachoMayoreoController : MFApiController
    {
        // GET: api/DespachoMayoreo
        public List<DespachosMayoreo> Get()
        {
            List<DespachosMayoreo> mDespachos = new List<DespachosMayoreo>();

            try
            {
                DateTime mFechaFinal = DateTime.Now.Date;
                DateTime mFechaInicial = new DateTime(mFechaFinal.Year, mFechaFinal.Month, 1);

                List<string> mClientes = new List<string> { "MA0008", "MA0007", "MA0011", "MA0012", "MA0005", "MA0016", "MA0017" };

                var qCamas = from c in db.MF_CamasDespachoMayoreo orderby c.MARCA, c.ORDEN select c;
                string mCodigoImperial = "1"; string mCodigoSemi = "2"; string mCodigoMatri = "3"; string mCodigoQueen = "4"; string mCodigoKing = "5";

                foreach (var cama in qCamas)
                {
                    string mImperial = string.Format("{0}{1}", cama.COLCHON, mCodigoImperial);
                    string mImperialSet = string.Format("{0}{1}", cama.CODIGO_SET, mCodigoImperial);

                    string mSemi = string.Format("{0}{1}", cama.COLCHON, mCodigoSemi);
                    string mSemiSet = string.Format("{0}{1}", cama.CODIGO_SET, mCodigoSemi);

                    string mMatri = string.Format("{0}{1}", cama.COLCHON, mCodigoMatri);
                    string mMatriSet = string.Format("{0}{1}", cama.CODIGO_SET, mCodigoMatri);

                    string mQueen = string.Format("{0}{1}", cama.COLCHON, mCodigoQueen);
                    string mQueenSet = string.Format("{0}{1}", cama.CODIGO_SET, mCodigoQueen);

                    string mKing = string.Format("{0}{1}", cama.COLCHON, mCodigoKing);
                    string mKingSet = string.Format("{0}{1}", cama.CODIGO_SET, mCodigoKing);

                    //Set
                    DespachosMayoreo mDespacho = new DespachosMayoreo();
                    mDespacho.Despachos = "DEL DIA";
                    mDespacho.Marca = cama.MARCA;
                    mDespacho.Tipo = "SETS";
                    mDespacho.Modelo = cama.MODELO;

                    mDespacho.MA0008_Imperial = Despachos("MA0008", mImperialSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0008_Semi = Despachos("MA0008", mSemiSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0008_Matri = Despachos("MA0008", mMatriSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0008_Queen = Despachos("MA0008", mQueenSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0008_King = Despachos("MA0008", mKingSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0008_Total = mDespacho.MA0008_Imperial + mDespacho.MA0008_Semi + mDespacho.MA0008_Matri + mDespacho.MA0008_Queen + mDespacho.MA0008_King;

                    mDespacho.MA0007_Imperial = Despachos("MA0007", mImperialSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0007_Semi = Despachos("MA0007", mSemiSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0007_Matri = Despachos("MA0007", mMatriSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0007_Queen = Despachos("MA0007", mQueenSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0007_King = Despachos("MA0007", mKingSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0007_Total = mDespacho.MA0007_Imperial + mDespacho.MA0007_Semi + mDespacho.MA0007_Matri + mDespacho.MA0007_Queen + mDespacho.MA0007_King;

                    mDespacho.MA0011_Imperial = Despachos("MA0011", mImperialSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0011_Semi = Despachos("MA0011", mSemiSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0011_Matri = Despachos("MA0011", mMatriSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0011_Queen = Despachos("MA0011", mQueenSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0011_King = Despachos("MA0011", mKingSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0011_Total = mDespacho.MA0011_Imperial + mDespacho.MA0011_Semi + mDespacho.MA0011_Matri + mDespacho.MA0011_Queen + mDespacho.MA0011_King;

                    mDespacho.MA0012_Imperial = Despachos("MA0012", mImperialSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0012_Semi = Despachos("MA0012", mSemiSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0012_Matri = Despachos("MA0012", mMatriSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0012_Queen = Despachos("MA0012", mQueenSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0012_King = Despachos("MA0012", mKingSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0012_Total = mDespacho.MA0012_Imperial + mDespacho.MA0012_Semi + mDespacho.MA0012_Matri + mDespacho.MA0012_Queen + mDespacho.MA0012_King;

                    mDespacho.MA0005_Imperial = Despachos("MA0005", mImperialSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0005_Semi = Despachos("MA0005", mSemiSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0005_Matri = Despachos("MA0005", mMatriSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0005_Queen = Despachos("MA0005", mQueenSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0005_King = Despachos("MA0005", mKingSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0005_Total = mDespacho.MA0005_Imperial + mDespacho.MA0005_Semi + mDespacho.MA0005_Matri + mDespacho.MA0005_Queen + mDespacho.MA0005_King;

                    mDespacho.MA0016_Imperial = Despachos("MA0016", mImperialSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0016_Semi = Despachos("MA0016", mSemiSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0016_Matri = Despachos("MA0016", mMatriSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0016_Queen = Despachos("MA0016", mQueenSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0016_King = Despachos("MA0016", mKingSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0016_Total = mDespacho.MA0016_Imperial + mDespacho.MA0016_Semi + mDespacho.MA0016_Matri + mDespacho.MA0016_Queen + mDespacho.MA0016_King;

                    mDespacho.MA0017_Imperial = Despachos("MA0017", mImperialSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0017_Semi = Despachos("MA0017", mSemiSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0017_Matri = Despachos("MA0017", mMatriSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0017_Queen = Despachos("MA0017", mQueenSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0017_King = Despachos("MA0017", mKingSet, mFechaFinal, mFechaFinal);
                    mDespacho.MA0017_Total = mDespacho.MA0017_Imperial + mDespacho.MA0017_Semi + mDespacho.MA0017_Matri + mDespacho.MA0017_Queen + mDespacho.MA0017_King;

                    mDespacho.Total = mDespacho.MA0008_Total + mDespacho.MA0007_Total + mDespacho.MA0011_Total + mDespacho.MA0012_Total + mDespacho.MA0005_Total + mDespacho.MA0016_Total + mDespacho.MA0017_Total;
                    mDespacho.pctjTotal = 0;
                    mDespacho.porcentaje = 0;

                    mDespachos.Add(mDespacho);


                    //Colchón
                    DespachosMayoreo mDespachoColchon = new DespachosMayoreo();
                    mDespachoColchon.Despachos = "DEL DIA";
                    mDespachoColchon.Marca = cama.MARCA;
                    mDespachoColchon.Tipo = "COLCHONES";
                    mDespachoColchon.Modelo = cama.MODELO;

                    mDespachoColchon.MA0008_Imperial = Despachos("MA0008", mImperial, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0008_Semi = Despachos("MA0008", mSemi, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0008_Matri = Despachos("MA0008", mMatri, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0008_Queen = Despachos("MA0008", mQueen, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0008_King = Despachos("MA0008", mKing, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0008_Total = mDespachoColchon.MA0008_Imperial + mDespachoColchon.MA0008_Semi + mDespachoColchon.MA0008_Matri + mDespachoColchon.MA0008_Queen + mDespachoColchon.MA0008_King;

                    mDespachoColchon.MA0007_Imperial = Despachos("MA0007", mImperial, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0007_Semi = Despachos("MA0007", mSemi, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0007_Matri = Despachos("MA0007", mMatri, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0007_Queen = Despachos("MA0007", mQueen, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0007_King = Despachos("MA0007", mKing, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0007_Total = mDespachoColchon.MA0007_Imperial + mDespachoColchon.MA0007_Semi + mDespachoColchon.MA0007_Matri + mDespachoColchon.MA0007_Queen + mDespachoColchon.MA0007_King;

                    mDespachoColchon.MA0011_Imperial = Despachos("MA0011", mImperial, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0011_Semi = Despachos("MA0011", mSemi, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0011_Matri = Despachos("MA0011", mMatri, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0011_Queen = Despachos("MA0011", mQueen, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0011_King = Despachos("MA0011", mKing, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0011_Total = mDespachoColchon.MA0011_Imperial + mDespachoColchon.MA0011_Semi + mDespachoColchon.MA0011_Matri + mDespachoColchon.MA0011_Queen + mDespachoColchon.MA0011_King;

                    mDespachoColchon.MA0012_Imperial = Despachos("MA0012", mImperial, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0012_Semi = Despachos("MA0012", mSemi, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0012_Matri = Despachos("MA0012", mMatri, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0012_Queen = Despachos("MA0012", mQueen, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0012_King = Despachos("MA0012", mKing, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0012_Total = mDespachoColchon.MA0012_Imperial + mDespachoColchon.MA0012_Semi + mDespachoColchon.MA0012_Matri + mDespachoColchon.MA0012_Queen + mDespachoColchon.MA0012_King;

                    mDespachoColchon.MA0005_Imperial = Despachos("MA0005", mImperial, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0005_Semi = Despachos("MA0005", mSemi, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0005_Matri = Despachos("MA0005", mMatri, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0005_Queen = Despachos("MA0005", mQueen, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0005_King = Despachos("MA0005", mKing, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0005_Total = mDespachoColchon.MA0005_Imperial + mDespachoColchon.MA0005_Semi + mDespachoColchon.MA0005_Matri + mDespachoColchon.MA0005_Queen + mDespachoColchon.MA0005_King;

                    mDespachoColchon.MA0016_Imperial = Despachos("MA0016", mImperial, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0016_Semi = Despachos("MA0016", mSemi, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0016_Matri = Despachos("MA0016", mMatri, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0016_Queen = Despachos("MA0016", mQueen, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0016_King = Despachos("MA0016", mKing, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0016_Total = mDespachoColchon.MA0016_Imperial + mDespachoColchon.MA0016_Semi + mDespachoColchon.MA0016_Matri + mDespachoColchon.MA0016_Queen + mDespachoColchon.MA0016_King;

                    mDespachoColchon.MA0017_Imperial = Despachos("MA0017", mImperial, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0017_Semi = Despachos("MA0017", mSemi, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0017_Matri = Despachos("MA0017", mMatri, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0017_Queen = Despachos("MA0017", mQueen, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0017_King = Despachos("MA0017", mKing, mFechaFinal, mFechaFinal);
                    mDespachoColchon.MA0017_Total = mDespachoColchon.MA0017_Imperial + mDespachoColchon.MA0017_Semi + mDespachoColchon.MA0017_Matri + mDespachoColchon.MA0017_Queen + mDespachoColchon.MA0017_King;

                    mDespachoColchon.Total = mDespachoColchon.MA0008_Total + mDespachoColchon.MA0007_Total + mDespachoColchon.MA0011_Total + mDespachoColchon.MA0012_Total + mDespachoColchon.MA0005_Total + mDespachoColchon.MA0016_Total + mDespachoColchon.MA0017_Total;
                    mDespachoColchon.pctjTotal = 0;
                    mDespachoColchon.porcentaje = 0;

                    mDespachos.Add(mDespachoColchon);


                    //Set Acumulado
                    DespachosMayoreo mDespachoAcum = new DespachosMayoreo();
                    mDespachoAcum.Despachos = "ACUMULADO";
                    mDespachoAcum.Marca = cama.MARCA;
                    mDespachoAcum.Tipo = "SETS";
                    mDespachoAcum.Modelo = cama.MODELO;

                    mDespachoAcum.MA0008_Imperial = Despachos("MA0008", mImperialSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0008_Semi = Despachos("MA0008", mSemiSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0008_Matri = Despachos("MA0008", mMatriSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0008_Queen = Despachos("MA0008", mQueenSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0008_King = Despachos("MA0008", mKingSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0008_Total = mDespachoAcum.MA0008_Imperial + mDespachoAcum.MA0008_Semi + mDespachoAcum.MA0008_Matri + mDespachoAcum.MA0008_Queen + mDespachoAcum.MA0008_King;

                    mDespachoAcum.MA0007_Imperial = Despachos("MA0007", mImperialSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0007_Semi = Despachos("MA0007", mSemiSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0007_Matri = Despachos("MA0007", mMatriSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0007_Queen = Despachos("MA0007", mQueenSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0007_King = Despachos("MA0007", mKingSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0007_Total = mDespachoAcum.MA0007_Imperial + mDespachoAcum.MA0007_Semi + mDespachoAcum.MA0007_Matri + mDespachoAcum.MA0007_Queen + mDespachoAcum.MA0007_King;

                    mDespachoAcum.MA0011_Imperial = Despachos("MA0011", mImperialSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0011_Semi = Despachos("MA0011", mSemiSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0011_Matri = Despachos("MA0011", mMatriSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0011_Queen = Despachos("MA0011", mQueenSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0011_King = Despachos("MA0011", mKingSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0011_Total = mDespachoAcum.MA0011_Imperial + mDespachoAcum.MA0011_Semi + mDespachoAcum.MA0011_Matri + mDespachoAcum.MA0011_Queen + mDespachoAcum.MA0011_King;

                    mDespachoAcum.MA0012_Imperial = Despachos("MA0012", mImperialSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0012_Semi = Despachos("MA0012", mSemiSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0012_Matri = Despachos("MA0012", mMatriSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0012_Queen = Despachos("MA0012", mQueenSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0012_King = Despachos("MA0012", mKingSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0012_Total = mDespachoAcum.MA0012_Imperial + mDespachoAcum.MA0012_Semi + mDespachoAcum.MA0012_Matri + mDespachoAcum.MA0012_Queen + mDespachoAcum.MA0012_King;

                    mDespachoAcum.MA0005_Imperial = Despachos("MA0005", mImperialSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0005_Semi = Despachos("MA0005", mSemiSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0005_Matri = Despachos("MA0005", mMatriSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0005_Queen = Despachos("MA0005", mQueenSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0005_King = Despachos("MA0005", mKingSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0005_Total = mDespachoAcum.MA0005_Imperial + mDespachoAcum.MA0005_Semi + mDespachoAcum.MA0005_Matri + mDespachoAcum.MA0005_Queen + mDespachoAcum.MA0005_King;

                    mDespachoAcum.MA0016_Imperial = Despachos("MA0016", mImperialSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0016_Semi = Despachos("MA0016", mSemiSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0016_Matri = Despachos("MA0016", mMatriSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0016_Queen = Despachos("MA0016", mQueenSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0016_King = Despachos("MA0016", mKingSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0016_Total = mDespachoAcum.MA0016_Imperial + mDespachoAcum.MA0016_Semi + mDespachoAcum.MA0016_Matri + mDespachoAcum.MA0016_Queen + mDespachoAcum.MA0016_King;

                    mDespachoAcum.MA0017_Imperial = Despachos("MA0017", mImperialSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0017_Semi = Despachos("MA0017", mSemiSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0017_Matri = Despachos("MA0017", mMatriSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0017_Queen = Despachos("MA0017", mQueenSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0017_King = Despachos("MA0017", mKingSet, mFechaInicial, mFechaFinal);
                    mDespachoAcum.MA0017_Total = mDespachoAcum.MA0017_Imperial + mDespachoAcum.MA0017_Semi + mDespachoAcum.MA0017_Matri + mDespachoAcum.MA0017_Queen + mDespachoAcum.MA0017_King;

                    mDespachoAcum.Total = mDespachoAcum.MA0008_Total + mDespachoAcum.MA0007_Total + mDespachoAcum.MA0011_Total + mDespachoAcum.MA0012_Total + mDespachoAcum.MA0005_Total + mDespachoAcum.MA0016_Total + mDespachoAcum.MA0017_Total;
                    mDespachoAcum.pctjTotal = 0;
                    mDespachoAcum.porcentaje = 0;

                    mDespachos.Add(mDespachoAcum);


                    //Colchón Acumulado
                    DespachosMayoreo mDespachoColchonAcum = new DespachosMayoreo();
                    mDespachoColchonAcum.Despachos = "ACUMULADO";
                    mDespachoColchonAcum.Marca = cama.MARCA;
                    mDespachoColchonAcum.Tipo = "COLCHONES";
                    mDespachoColchonAcum.Modelo = cama.MODELO;

                    mDespachoColchonAcum.MA0008_Imperial = Despachos("MA0008", mImperial, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0008_Semi = Despachos("MA0008", mSemi, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0008_Matri = Despachos("MA0008", mMatri, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0008_Queen = Despachos("MA0008", mQueen, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0008_King = Despachos("MA0008", mKing, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0008_Total = mDespachoColchonAcum.MA0008_Imperial + mDespachoColchonAcum.MA0008_Semi + mDespachoColchonAcum.MA0008_Matri + mDespachoColchonAcum.MA0008_Queen + mDespachoColchonAcum.MA0008_King;

                    mDespachoColchonAcum.MA0007_Imperial = Despachos("MA0007", mImperial, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0007_Semi = Despachos("MA0007", mSemi, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0007_Matri = Despachos("MA0007", mMatri, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0007_Queen = Despachos("MA0007", mQueen, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0007_King = Despachos("MA0007", mKing, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0007_Total = mDespachoColchonAcum.MA0007_Imperial + mDespachoColchonAcum.MA0007_Semi + mDespachoColchonAcum.MA0007_Matri + mDespachoColchonAcum.MA0007_Queen + mDespachoColchonAcum.MA0007_King;

                    mDespachoColchonAcum.MA0011_Imperial = Despachos("MA0011", mImperial, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0011_Semi = Despachos("MA0011", mSemi, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0011_Matri = Despachos("MA0011", mMatri, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0011_Queen = Despachos("MA0011", mQueen, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0011_King = Despachos("MA0011", mKing, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0011_Total = mDespachoColchonAcum.MA0011_Imperial + mDespachoColchonAcum.MA0011_Semi + mDespachoColchonAcum.MA0011_Matri + mDespachoColchonAcum.MA0011_Queen + mDespachoColchonAcum.MA0011_King;

                    mDespachoColchonAcum.MA0012_Imperial = Despachos("MA0012", mImperial, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0012_Semi = Despachos("MA0012", mSemi, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0012_Matri = Despachos("MA0012", mMatri, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0012_Queen = Despachos("MA0012", mQueen, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0012_King = Despachos("MA0012", mKing, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0012_Total = mDespachoColchonAcum.MA0012_Imperial + mDespachoColchonAcum.MA0012_Semi + mDespachoColchonAcum.MA0012_Matri + mDespachoColchonAcum.MA0012_Queen + mDespachoColchonAcum.MA0012_King;

                    mDespachoColchonAcum.MA0005_Imperial = Despachos("MA0005", mImperial, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0005_Semi = Despachos("MA0005", mSemi, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0005_Matri = Despachos("MA0005", mMatri, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0005_Queen = Despachos("MA0005", mQueen, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0005_King = Despachos("MA0005", mKing, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0005_Total = mDespachoColchonAcum.MA0005_Imperial + mDespachoColchonAcum.MA0005_Semi + mDespachoColchonAcum.MA0005_Matri + mDespachoColchonAcum.MA0005_Queen + mDespachoColchonAcum.MA0005_King;

                    mDespachoColchonAcum.MA0016_Imperial = Despachos("MA0016", mImperial, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0016_Semi = Despachos("MA0016", mSemi, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0016_Matri = Despachos("MA0016", mMatri, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0016_Queen = Despachos("MA0016", mQueen, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0016_King = Despachos("MA0016", mKing, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0016_Total = mDespachoColchonAcum.MA0016_Imperial + mDespachoColchonAcum.MA0016_Semi + mDespachoColchonAcum.MA0016_Matri + mDespachoColchonAcum.MA0016_Queen + mDespachoColchonAcum.MA0016_King;

                    mDespachoColchonAcum.MA0017_Imperial = Despachos("MA0017", mImperial, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0017_Semi = Despachos("MA0017", mSemi, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0017_Matri = Despachos("MA0017", mMatri, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0017_Queen = Despachos("MA0017", mQueen, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0017_King = Despachos("MA0017", mKing, mFechaInicial, mFechaFinal);
                    mDespachoColchonAcum.MA0017_Total = mDespachoColchonAcum.MA0017_Imperial + mDespachoColchonAcum.MA0017_Semi + mDespachoColchonAcum.MA0017_Matri + mDespachoColchonAcum.MA0017_Queen + mDespachoColchonAcum.MA0017_King;

                    mDespachoColchonAcum.Total = mDespachoColchonAcum.MA0008_Total + mDespachoColchonAcum.MA0007_Total + mDespachoColchonAcum.MA0011_Total + mDespachoColchonAcum.MA0012_Total + mDespachoColchonAcum.MA0005_Total + mDespachoColchonAcum.MA0016_Total + mDespachoColchonAcum.MA0017_Total;
                    mDespachoColchonAcum.pctjTotal = 0;
                    mDespachoColchonAcum.porcentaje = 0;

                    mDespachos.Add(mDespachoColchonAcum);
                }


                int mGranTotal = mDespachos.Where(x => x.Despachos == "ACUMULADO").Sum(x => x.Total);
                var qDespachoTipo = (from d in mDespachos select new { Despachos = d.Despachos, Marca = d.Marca, Tipo = d.Tipo }).Distinct().ToList();
                mGranTotal = mGranTotal * 1;

                foreach (var item in qDespachoTipo)
                {
                    var qTipo = from d in mDespachos where d.Despachos == item.Despachos && d.Marca == item.Marca && d.Tipo == item.Tipo select d;
                    int mTotal = qTipo.Sum(x => x.Total); int mSubTotal = 0;

                    if (mTotal > 0)
                    {
                        foreach (var tipo in qTipo)
                        {
                            tipo.pctjTotal = Convert.ToInt32(((decimal)tipo.Total / (decimal)mTotal) * 100);
                            mSubTotal = mSubTotal + tipo.pctjTotal;
                        }
                    }

                    DespachosMayoreo mDespacho = new DespachosMayoreo();
                    mDespacho.Despachos = item.Despachos;
                    mDespacho.Marca = item.Marca;
                    mDespacho.Tipo = item.Tipo;
                    mDespacho.Modelo = "SUBTOTAL";

                    mDespacho.MA0008_Imperial = qTipo.Sum(x => x.MA0008_Imperial);
                    mDespacho.MA0008_Semi = qTipo.Sum(x => x.MA0008_Semi);
                    mDespacho.MA0008_Matri = qTipo.Sum(x => x.MA0008_Matri);
                    mDespacho.MA0008_Queen = qTipo.Sum(x => x.MA0008_Queen);
                    mDespacho.MA0008_King = qTipo.Sum(x => x.MA0008_King);
                    mDespacho.MA0008_Total = qTipo.Sum(x => x.MA0008_Total);

                    mDespacho.MA0007_Imperial = qTipo.Sum(x => x.MA0007_Imperial);
                    mDespacho.MA0007_Semi = qTipo.Sum(x => x.MA0007_Semi);
                    mDespacho.MA0007_Matri = qTipo.Sum(x => x.MA0007_Matri);
                    mDespacho.MA0007_Queen = qTipo.Sum(x => x.MA0007_Queen);
                    mDespacho.MA0007_King = qTipo.Sum(x => x.MA0007_King);
                    mDespacho.MA0007_Total = qTipo.Sum(x => x.MA0007_Total);

                    mDespacho.MA0011_Imperial = qTipo.Sum(x => x.MA0011_Imperial);
                    mDespacho.MA0011_Semi = qTipo.Sum(x => x.MA0011_Semi);
                    mDespacho.MA0011_Matri = qTipo.Sum(x => x.MA0011_Matri);
                    mDespacho.MA0011_Queen = qTipo.Sum(x => x.MA0011_Queen);
                    mDespacho.MA0011_King = qTipo.Sum(x => x.MA0011_King);
                    mDespacho.MA0011_Total = qTipo.Sum(x => x.MA0011_Total);

                    mDespacho.MA0012_Imperial = qTipo.Sum(x => x.MA0012_Imperial);
                    mDespacho.MA0012_Semi = qTipo.Sum(x => x.MA0012_Semi);
                    mDespacho.MA0012_Matri = qTipo.Sum(x => x.MA0012_Matri);
                    mDespacho.MA0012_Queen = qTipo.Sum(x => x.MA0012_Queen);
                    mDespacho.MA0012_King = qTipo.Sum(x => x.MA0012_King);
                    mDespacho.MA0012_Total = qTipo.Sum(x => x.MA0012_Total);

                    mDespacho.MA0005_Imperial = qTipo.Sum(x => x.MA0005_Imperial);
                    mDespacho.MA0005_Semi = qTipo.Sum(x => x.MA0005_Semi);
                    mDespacho.MA0005_Matri = qTipo.Sum(x => x.MA0005_Matri);
                    mDespacho.MA0005_Queen = qTipo.Sum(x => x.MA0005_Queen);
                    mDespacho.MA0005_King = qTipo.Sum(x => x.MA0005_King);
                    mDespacho.MA0005_Total = qTipo.Sum(x => x.MA0005_Total);

                    mDespacho.MA0016_Imperial = qTipo.Sum(x => x.MA0016_Imperial);
                    mDespacho.MA0016_Semi = qTipo.Sum(x => x.MA0016_Semi);
                    mDespacho.MA0016_Matri = qTipo.Sum(x => x.MA0016_Matri);
                    mDespacho.MA0016_Queen = qTipo.Sum(x => x.MA0016_Queen);
                    mDespacho.MA0016_King = qTipo.Sum(x => x.MA0016_King);
                    mDespacho.MA0016_Total = qTipo.Sum(x => x.MA0016_Total);

                    mDespacho.MA0017_Imperial = qTipo.Sum(x => x.MA0017_Imperial);
                    mDespacho.MA0017_Semi = qTipo.Sum(x => x.MA0017_Semi);
                    mDespacho.MA0017_Matri = qTipo.Sum(x => x.MA0017_Matri);
                    mDespacho.MA0017_Queen = qTipo.Sum(x => x.MA0017_Queen);
                    mDespacho.MA0017_King = qTipo.Sum(x => x.MA0017_King);
                    mDespacho.MA0017_Total = qTipo.Sum(x => x.MA0017_Total);

                    mDespacho.Total = mDespacho.MA0008_Total + mDespacho.MA0007_Total + mDespacho.MA0011_Total + mDespacho.MA0012_Total + mDespacho.MA0005_Total + mDespacho.MA0016_Total + mDespacho.MA0017_Total;
                    mDespacho.pctjTotal = qTipo.Sum(x => x.pctjTotal);

                    if (item.Despachos == "ACUMULADO")
                    {
                        mDespacho.porcentaje = Convert.ToInt32(((decimal)mDespacho.Total / (decimal)mGranTotal) * 100);
                    }
                    else
                    {
                        mDespacho.porcentaje = 0;
                    }

                    mDespachos.Add(mDespacho);
                }
            }
            catch (Exception ex)
            {
                DespachosMayoreo mDespacho = new DespachosMayoreo();
                mDespacho.Despachos = string.Format("{0}", CatchClass.ExMessage(ex, "DespachoMayoreoController", "Get"));
                mDespachos.Add(mDespacho);
            }

            return mDespachos.OrderByDescending(x => x.Despachos).ThenByDescending(x => x.Tipo).ThenBy(x => x.Marca).ToList();
        }

        int Despachos(string cliente, string articulo, DateTime inicio, DateTime fin)
        {
            string mArticuloM = string.Format("M{0}", articulo);

            var q = (from d in db.DESPACHO_DETALLE
                     join de in db.DESPACHO on d.DESPACHO equals de.DESPACHO1
                     where de.CLIENTE == cliente && de.ESTADO == "D" && de.FECHA >= inicio && de.FECHA <= fin && (d.ARTICULO.Substring(0, 6) == articulo || d.ARTICULO.Substring(0, 7) == mArticuloM)
                     select d);

            if (q.Count() == 0)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(q.Sum(x => x.CANTIDAD));
            }
        }

    }
}
