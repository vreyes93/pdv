﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using FiestaNETRestServices.Content.Abstract;
using Newtonsoft.Json;
using MF_Clases;

namespace FiestaNETRestServices.Controllers.Bodega
{
    public class DiasDespachoController : MFApiController
    {
        // GET: api/DiasDespacho
        public Clases.DiasDespachos Get()
        {
            return Dias();
        }

        // GET: api/DiasDespacho/5
        public Clases.DiasDespachos Get(int id)
        {
            return Dias();
        }

        public string Get(string fecha, string cliente)
        {
            string mRetorna = "OK!";

            try
            {
                string mSeAtiendeDepartamento = ""; string mSeAtiendeMunicipio = "";
                string mDepartamento = ""; string mMunicipio = ""; string mDia = "";
                string[] mInfo = fecha.Split(new string[] { "-" }, StringSplitOptions.None);
                DateTime mFecha = new DateTime(Convert.ToInt32(mInfo[0]), Convert.ToInt32(mInfo[1]), Convert.ToInt32(mInfo[2]));

                var qDireccion = from d in db.DETALLE_DIRECCION join c in db.CLIENTE on d.DETALLE_DIRECCION1 equals c.DETALLE_DIRECCION where c.CLIENTE1 == cliente select d;
                foreach (var item in qDireccion)
                {
                    mDepartamento = item.CAMPO_6.ToUpper();
                    mMunicipio = item.CAMPO_7.ToUpper();
                }

                switch (mFecha.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        mDia = "lunes";
                        mSeAtiendeDepartamento = (from d in db.MF_DiasDespachoRegion where d.TIPO == "D" && d.DEPARTAMENTO == mDepartamento select d).First().LUNES;
                        mSeAtiendeMunicipio = (from d in db.MF_DiasDespachoRegion where d.TIPO == "M" && d.MUNICIPIO == mMunicipio select d).First().LUNES;

                        break;
                    case DayOfWeek.Tuesday:
                        mDia = "martes";
                        mSeAtiendeDepartamento = (from d in db.MF_DiasDespachoRegion where d.TIPO == "D" && d.DEPARTAMENTO == mDepartamento select d).First().MARTES;
                        mSeAtiendeMunicipio = (from d in db.MF_DiasDespachoRegion where d.TIPO == "M" && d.MUNICIPIO == mMunicipio select d).First().MARTES;

                        break;
                    case DayOfWeek.Wednesday:
                        mDia = "miércoles";
                        mSeAtiendeDepartamento = (from d in db.MF_DiasDespachoRegion where d.TIPO == "D" && d.DEPARTAMENTO == mDepartamento select d).First().MIERCOLES;
                        mSeAtiendeMunicipio = (from d in db.MF_DiasDespachoRegion where d.TIPO == "M" && d.MUNICIPIO == mMunicipio select d).First().MIERCOLES;

                        break;
                    case DayOfWeek.Thursday:
                        mDia = "jueves";
                        mSeAtiendeDepartamento = (from d in db.MF_DiasDespachoRegion where d.TIPO == "D" && d.DEPARTAMENTO == mDepartamento select d).First().JUEVES;
                        mSeAtiendeMunicipio = (from d in db.MF_DiasDespachoRegion where d.TIPO == "M" && d.MUNICIPIO == mMunicipio select d).First().JUEVES;

                        break;
                    case DayOfWeek.Friday:
                        mDia = "viernes";
                        mSeAtiendeDepartamento = (from d in db.MF_DiasDespachoRegion where d.TIPO == "D" && d.DEPARTAMENTO == mDepartamento select d).First().VIERNES;
                        mSeAtiendeMunicipio = (from d in db.MF_DiasDespachoRegion where d.TIPO == "M" && d.MUNICIPIO == mMunicipio select d).First().VIERNES;

                        break;
                    case DayOfWeek.Saturday:
                        mDia = "sábado";
                        mSeAtiendeDepartamento = (from d in db.MF_DiasDespachoRegion where d.TIPO == "D" && d.DEPARTAMENTO == mDepartamento select d).First().SABADO;
                        mSeAtiendeMunicipio = (from d in db.MF_DiasDespachoRegion where d.TIPO == "M" && d.MUNICIPIO == mMunicipio select d).First().SABADO;

                        break;
                    default:
                        mDia = "domingo";
                        mSeAtiendeDepartamento = (from d in db.MF_DiasDespachoRegion where d.TIPO == "D" && d.DEPARTAMENTO == mDepartamento select d).First().DOMINGO;
                        mSeAtiendeMunicipio = (from d in db.MF_DiasDespachoRegion where d.TIPO == "M" && d.MUNICIPIO == mMunicipio select d).First().DOMINGO;

                        break;
                }
                
                string mDiasDepartamento = "";
                var qDepartamento = (from d in db.MF_DiasDespachoRegion where d.TIPO == "D" && d.DEPARTAMENTO == mDepartamento select d).First();
                if (qDepartamento.LUNES == "S") mDiasDepartamento = string.Format("{0}{1}lunes", mDiasDepartamento, mDiasDepartamento.Trim().Length == 0 ? "" : ", ");
                if (qDepartamento.MARTES == "S") mDiasDepartamento = string.Format("{0}{1}martes", mDiasDepartamento, mDiasDepartamento.Trim().Length == 0 ? "" : ", ");
                if (qDepartamento.MIERCOLES == "S") mDiasDepartamento = string.Format("{0}{1}miércoles", mDiasDepartamento, mDiasDepartamento.Trim().Length == 0 ? "" : ", ");
                if (qDepartamento.JUEVES == "S") mDiasDepartamento = string.Format("{0}{1}jueves", mDiasDepartamento, mDiasDepartamento.Trim().Length == 0 ? "" : ", ");
                if (qDepartamento.VIERNES == "S") mDiasDepartamento = string.Format("{0}{1}viernes", mDiasDepartamento, mDiasDepartamento.Trim().Length == 0 ? "" : ", ");
                if (qDepartamento.SABADO == "S") mDiasDepartamento = string.Format("{0}{1}sábado", mDiasDepartamento, mDiasDepartamento.Trim().Length == 0 ? "" : ", ");
                if (qDepartamento.DOMINGO == "S") mDiasDepartamento = string.Format("{0}{1}domingo", mDiasDepartamento, mDiasDepartamento.Trim().Length == 0 ? "" : ", ");
                
                string mDiasMunicipio = "";
                var qMunicipio = (from d in db.MF_DiasDespachoRegion where d.TIPO == "M" && d.MUNICIPIO == mMunicipio select d).First();
                if (qMunicipio.LUNES == "S") mDiasMunicipio = string.Format("{0}{1}lunes", mDiasMunicipio, mDiasMunicipio.Trim().Length == 0 ? "" : ", ");
                if (qMunicipio.MARTES == "S") mDiasMunicipio = string.Format("{0}{1}martes", mDiasMunicipio, mDiasMunicipio.Trim().Length == 0 ? "" : ", ");
                if (qMunicipio.MIERCOLES == "S") mDiasMunicipio = string.Format("{0}{1}miércoles", mDiasMunicipio, mDiasMunicipio.Trim().Length == 0 ? "" : ", ");
                if (qMunicipio.JUEVES == "S") mDiasMunicipio = string.Format("{0}{1}jueves", mDiasMunicipio, mDiasMunicipio.Trim().Length == 0 ? "" : ", ");
                if (qMunicipio.VIERNES == "S") mDiasMunicipio = string.Format("{0}{1}viernes", mDiasMunicipio, mDiasMunicipio.Trim().Length == 0 ? "" : ", ");
                if (qMunicipio.SABADO == "S") mDiasMunicipio = string.Format("{0}{1}sábado", mDiasMunicipio, mDiasMunicipio.Trim().Length == 0 ? "" : ", ");
                if (qMunicipio.DOMINGO == "S") mDiasMunicipio = string.Format("{0}{1}domingo", mDiasMunicipio, mDiasMunicipio.Trim().Length == 0 ? "" : ", ");

                if (mSeAtiendeDepartamento == "N" && General.Ambiente.Equals("PRO"))
                    mRetorna = string.Format("Error, los {0} no se atiende {1}, consulte con su departamento de bodega o escoga otra fecha, {1} se visita los dias {2}", mDia, mDepartamento, mDiasDepartamento);
                if (mSeAtiendeMunicipio == "N" && General.Ambiente.Equals("PRO"))
                    mRetorna = string.Format("Error, los {0} no se atiende {1}, consulte con su departamento de bodega o escoga otra fecha, {1} se visita los dias {2}", mDia, mMunicipio, mDiasMunicipio);
            }
            catch (Exception ex)
            {
                mRetorna = CatchClass.ExMessage(ex, "DiasDespachoController", "Get");
            }

            return mRetorna;
        }

        Clases.DiasDespachos Dias()
        {
            Clases.DiasDespachos mDias = new Clases.DiasDespachos();

            var q = from d in db.MF_DiasDespachoRegion
                    orderby d.DEPARTAMENTO
                    where d.TIPO == "D"
                    select new Clases.DiasDepartamentos()
                    {
                        Dia = d.DIA_DESPACHO,
                        Tipo = d.TIPO,
                        Departamento = d.DEPARTAMENTO,
                        Lunes = d.LUNES == "S" ? true : false,
                        Martes = d.MARTES == "S" ? true : false,
                        Miercoles = d.MIERCOLES == "S" ? true : false,
                        Jueves = d.JUEVES == "S" ? true : false,
                        Viernes = d.VIERNES == "S" ? true : false,
                        Sabado = d.SABADO == "S" ? true : false,
                        Domingo = d.DOMINGO == "S" ? true : false,
                        Tooltip = d.TOOLTIP
                    };

            var qDet = from d in db.MF_DiasDespachoRegion
                    orderby d.DEPARTAMENTO, d.MUNICIPIO
                    where d.TIPO == "M"
                    select new Clases.DiasMunicipios()
                    {
                        Dia = d.DIA_DESPACHO,
                        Tipo = d.TIPO,
                        Departamento = d.DEPARTAMENTO,
                        Municipio = d.MUNICIPIO,
                        Lunes = d.LUNES == "S" ? true : false,
                        Martes = d.MARTES == "S" ? true : false,
                        Miercoles = d.MIERCOLES == "S" ? true : false,
                        Jueves = d.JUEVES == "S" ? true : false,
                        Viernes = d.VIERNES == "S" ? true : false,
                        Sabado = d.SABADO == "S" ? true : false,
                        Domingo = d.DOMINGO == "S" ? true : false,
                        Tooltip = d.TOOLTIP
                    };


            mDias.Departamentos = q.ToList();
            mDias.Municipios = qDet.ToList();

            return mDias;
        }

        // POST: api/DiasDespacho
        public string Post([FromBody]Clases.DiasDespachos dias)
        {
            string mRetorna = "";

            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    string mNombreUsuaio = (from u in db.USUARIO where u.USUARIO1 == dias.Usuario select u).First().NOMBRE;
                    string mTooltip = string.Format("Registro modificado por {0} el {1} a las {2}", mNombreUsuaio, Utilitario.FormatoDDMMYYYY(DateTime.Now.Date), DateTime.Now.ToShortTimeString());

                    foreach (var item in dias.Departamentos)
                    {
                        var q = from d in db.MF_DiasDespachoRegion where d.DIA_DESPACHO == item.Dia select d;
                        foreach (var dia in q)
                        {
                            bool mCambio = false;

                            if (dia.LUNES != (item.Lunes ? "S" : "N")) mCambio = true;
                            if (dia.MARTES != (item.Martes ? "S" : "N")) mCambio = true;
                            if (dia.MIERCOLES != (item.Miercoles ? "S" : "N")) mCambio = true;
                            if (dia.JUEVES != (item.Jueves ? "S" : "N")) mCambio = true;
                            if (dia.VIERNES != (item.Viernes ? "S" : "N")) mCambio = true;
                            if (dia.SABADO != (item.Sabado ? "S" : "N")) mCambio = true;
                            if (dia.DOMINGO != (item.Domingo ? "S" : "N")) mCambio = true;

                            dia.LUNES = item.Lunes ? "S" : "N";
                            dia.MARTES = item.Martes ? "S" : "N";
                            dia.MIERCOLES = item.Miercoles ? "S" : "N";
                            dia.JUEVES = item.Jueves ? "S" : "N";
                            dia.VIERNES = item.Viernes ? "S" : "N";
                            dia.SABADO = item.Sabado ? "S" : "N";
                            dia.DOMINGO = item.Domingo ? "S" : "N";

                            if (mCambio)
                            {
                                dia.TOOLTIP = mTooltip;
                                dia.RecordDate = DateTime.Now;
                                dia.UpdatedBy = dias.Usuario;
                            }
                        }
                    }

                    foreach (var item in dias.Municipios)
                    {
                        var q = from d in db.MF_DiasDespachoRegion where d.DIA_DESPACHO == item.Dia select d;
                        foreach (var dia in q)
                        {
                            bool mCambio = false;

                            if (dia.LUNES != (item.Lunes ? "S" : "N")) mCambio = true;
                            if (dia.MARTES != (item.Martes ? "S" : "N")) mCambio = true;
                            if (dia.MIERCOLES != (item.Miercoles ? "S" : "N")) mCambio = true;
                            if (dia.JUEVES != (item.Jueves ? "S" : "N")) mCambio = true;
                            if (dia.VIERNES != (item.Viernes ? "S" : "N")) mCambio = true;
                            if (dia.SABADO != (item.Sabado ? "S" : "N")) mCambio = true;
                            if (dia.DOMINGO != (item.Domingo ? "S" : "N")) mCambio = true;

                            dia.LUNES = item.Lunes ? "S" : "N";
                            dia.MARTES = item.Martes ? "S" : "N";
                            dia.MIERCOLES = item.Miercoles ? "S" : "N";
                            dia.JUEVES = item.Jueves ? "S" : "N";
                            dia.VIERNES = item.Viernes ? "S" : "N";
                            dia.SABADO = item.Sabado ? "S" : "N";
                            dia.DOMINGO = item.Domingo ? "S" : "N";

                            if (mCambio)
                            {
                                dia.TOOLTIP = mTooltip;
                                dia.RecordDate = DateTime.Now;
                                dia.UpdatedBy = dias.Usuario;
                            }
                        }
                    }

                    db.SaveChanges();
                    transactionScope.Complete();
                }

                mRetorna = "Los días fueron grabados exitosamente";
            }
            catch (Exception ex)
            {
                mRetorna = CatchClass.ExMessage(ex, "DiasDespachoController", "Post");
            }

            return mRetorna;
        }

    }
}
