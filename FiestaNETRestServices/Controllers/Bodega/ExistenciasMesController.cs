﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;

namespace FiestaNETRestServices.Controllers.Bodega
{
    public class ExistenciasMesController : MFApiController
    {
        // GET: api/ExistenciasMes
        public Respuesta Get()
        {
            return new Respuesta(false, "Debe enviar la bodega, año y mes");
        }

        // GET: api/ExistenciasMes/5
        public string Get(string bodega, string anio, string mes)
        {
            string mRetorna = "";
            db.CommandTimeout = 999999999;

            try
            {
                int mMes = Convert.ToInt32(mes);
                int mAnio = Convert.ToInt32(anio);

                DateTime mFecha = new DateTime(mAnio, mMes, 1, 23, 59, 59);
                mFecha = mFecha.AddMonths(1).AddDays(-1);
                string mNombreMes = Utilitario.Mes(new DateTime(mAnio, mMes, 1));

                var qInv = from i in db.MF_Inventario where i.BODEGA == bodega && i.ANIO == mAnio && i.MES == mMes select i;
                if (qInv.Count() > 0)
                {
                    if (qInv.First().CERRADO == "S") return string.Format("Error, {0} {1} para {2} ya está cerrado", mNombreMes, anio, bodega);
                }
                
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    var qInventario = from i in db.MF_InventarioDetalle where i.BODEGA == bodega && i.ANIO == mAnio && i.MES == mMes select i;

                    foreach (var item in qInventario)
                    {
                        var q = from t in db.TRANSACCION_INV
                                where t.FECHA <= mFecha && t.BODEGA == bodega && t.ARTICULO == item.ARTICULO && (t.NATURALEZA == "E" || t.NATURALEZA == "S")
                                group t by t.ARTICULO into grp
                                select new
                                {
                                    Existencia = grp.Sum(x => x.NATURALEZA == "E" ? x.CANTIDAD > 0 ? x.CANTIDAD : -1 * x.CANTIDAD : x.CANTIDAD > 0 ? -1 * x.CANTIDAD : x.CANTIDAD)
                                };

                        int mExistencias = 0;
                        if (q.Count() > 0) 
                            mExistencias = Convert.ToInt32(q.First().Existencia);

                        //Se verifica la existencia de códigos M para F01
                        //comentar este bloque si se necesita (desagrupar mayoreo-fiesta)
                        if (bodega == "F01")
                        {
                            string mArticuloM = string.Format("M{0}", item.ARTICULO);
                            var qM = from t in db.TRANSACCION_INV
                                     where t.FECHA <= mFecha && t.BODEGA == bodega && t.ARTICULO == mArticuloM && (t.NATURALEZA == "E" || t.NATURALEZA == "S")
                                     group t by t.ARTICULO into grp
                                     select new
                                     {
                                         Existencia = grp.Sum(x => x.NATURALEZA == "E" ? x.CANTIDAD > 0 ? x.CANTIDAD : -1 * x.CANTIDAD : x.CANTIDAD > 0 ? -1 * x.CANTIDAD : x.CANTIDAD)
                                     };
                            if (qM.Count() > 0)
                                mExistencias = mExistencias + Convert.ToInt32(qM.First().Existencia);
                        }

                        int mDiferencia = mExistencias - item.EXISTENCIA_FISICA;

                        string mResultado = "";
                        if (mDiferencia > 0) mResultado = string.Format("F{0}", mDiferencia.ToString());
                        if (mDiferencia < 0) mResultado = string.Format("S{0}", (-1 * mDiferencia).ToString());

                        item.BLANCO1 = mResultado;
                        item.DIFERENCIA = mDiferencia;
                        item.EXISTENCIA_TOTAL = mExistencias;
                    }

                    db.SaveChanges();
                    transactionScope.Complete();
                }

                mRetorna = string.Format("Existencias de {0} actualizadas exitosamente", bodega);
            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0} {1}", CatchClass.ExMessage(ex, "ExistenciasMesController", "Get"), "");
            }

            return mRetorna;
        }

        // POST: api/ExistenciasMes
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/ExistenciasMes/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ExistenciasMes/5
        public void Delete(int id)
        {
        }
    }
}
