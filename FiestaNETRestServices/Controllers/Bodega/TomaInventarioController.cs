﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Models;
using MF_Clases.Comun;
using Newtonsoft.Json;
using FiestaNETRestServices.Utils;
using System.Data.Common.CommandTrees.ExpressionBuilder;
using System.Data.Objects.SqlClient;

namespace FiestaNETRestServices.Controllers.Bodega
{
    public class TomaInventarioController : MFApiController
    {
        // GET: api/TomaInventario
        public Respuesta Get()
        {
            log.Info("Servicio GET");
            return new Respuesta(false, "Debe enviar la bodega");
        }
        /// <summary>
        /// Mapeo de bodegas Crediplus <==> MF
        /// </summary>
        /// <param name="bodegaMF"></param>
        /// <returns></returns>
        private string obtenerBodegaCP(string bodegaMF)
        {
            return db.MF_Catalogo.Where(x => x.CODIGO_TABLA == 70 && x.CODIGO_CAT == bodegaMF).Select(x => x.NOMBRE_CAT).FirstOrDefault();
        }
        // GET: api/TomaInventario/5
        public List<ArticuloInventario> Get(string bodega, string usuario, string orderBy = null, string clasificacion = null)
        {
            log.Info("Servicio GET string");

            string bodegaCrediplus = "";


            List<ArticuloInventario> mRetorna = new List<ArticuloInventario>();

            bool mMostrarExistencias = false;
            bool gotOrder = (!string.IsNullOrEmpty(orderBy) && !string.IsNullOrWhiteSpace(orderBy));
            if (bodega == null) return mRetorna;
            if (bodega.Trim().Length == 0) return mRetorna;

            bodegaCrediplus = obtenerBodegaCP(bodega);

            if ((from c in db.MF_Catalogo where c.NOMBRE_CAT == usuario && c.CODIGO_TABLA == Const.CATALOGO.EXISTENCIAS_TOMA_INVENTARIO select c).Count() > 0) mMostrarExistencias = true;

            var qArticulos =
                (from a in db.ARTICULO
                 join ex in db.EXISTENCIA_BODEGA on new { Articulo = a.ARTICULO1 } equals new { Articulo = ex.ARTICULO }
                 where a.ACTIVO == "S" && a.TIPO == "T" && ex.BODEGA == bodega
                 select new ArticuloInventario()
                 {
                     Clasificacion = a.CLASIFICACION_3,
                     Articulo = a.ARTICULO1,
                     Descripcion = a.DESCRIPCION,
                     UltimaCompra = a.ULTIMO_INGRESO,
                     Blanco1 = "",
                     Existencias = mMostrarExistencias ? (Int32)ex.CANT_DISPONIBLE : 0,
                     Blanco2 = "",
                     ExistenciaFisica = 0,
                     Conteo = "",
                     Observaciones = "",
                     Tipo = bodega == "F01" ?   //Camas
                                                ((a.ARTICULO1.Substring(0, 3) == "141" ||
                                                  a.ARTICULO1.Substring(0, 3) == "142" ||
                                                  a.ARTICULO1.Substring(0, 4) == "M141" ||
                                                  a.ARTICULO1.Substring(0, 4) == "M142")
                                                    ? ("CAMAS")
                                                    : (
                                                        //Articulos de decoración
                                                        ((a.ARTICULO1.Substring(0, 1) == "0" ||
                                                          a.ARTICULO1.Substring(0, 1) == "A" ||
                                                          a.ARTICULO1.Substring(0, 3) == "021" ||
                                                          a.ARTICULO1.Substring(0, 3) == "081" ||
                                                          a.ARTICULO1.Substring(0, 2) == "09" ||
                                                          a.ARTICULO1.Substring(0, 3) == "110" ||
                                                          a.ARTICULO1.Substring(0, 3) == "140" ||
                                                          a.ARTICULO1.Substring(0, 3) == "143" ||
                                                          a.ARTICULO1.Substring(0, 3) == "144" ||
                                                          a.ARTICULO1.Substring(0, 3) == "145")
                                                            ? ("DECORACIÓN")
                                                            //Por defecto, resto de articulos
                                                            : "RESTO DE ARTÍCULOS")
                                                        )
                                                 )
                                           : ""
                 }).Distinct().OrderBy(x => x.Articulo);

            #region "Crediplus"
            var qArticulosCrediplus =
                (from a in dbCrediplus.ARTICULO
                 join ex in dbCrediplus.EXISTENCIA_BODEGA on new { Articulo = a.ARTICULO1 } equals new { Articulo = ex.ARTICULO }
                 where a.ACTIVO == "S" && a.TIPO == "T" && ex.BODEGA == bodegaCrediplus
                 select new ArticuloInventario()
                 {
                     Clasificacion = "-1",
                     Articulo = a.ARTICULO1,
                     Descripcion = a.DESCRIPCION,
                     UltimaCompra = a.ULTIMO_INGRESO,
                     Blanco1 = "",
                     Existencias = mMostrarExistencias ? (Int32)ex.CANT_DISPONIBLE : 0,
                     Blanco2 = "",
                     ExistenciaFisica = 0,
                     Conteo = "",
                     Observaciones = "",
                     Tipo = "OUTLET-CREDIPLUS"
                 }).Distinct().OrderBy(x => x.Articulo);
            #endregion
            if (bodega == "F01")
            {
                List<ArticuloInventario> mInventario = new List<ArticuloInventario>();

                foreach (var item in qArticulos)
                {

                    bool mInsertar = true;
                    int mExistencias = item.Existencias;

                    string mArticuloFiesta = item.Articulo.Replace("M", "");
                    string mArticuloMayoreo = string.Format("M{0}", item.Articulo);

                    //comentar estas 4 lineas si se quiere desagrupar (desagrupar mayoreo-fiesta)
                    if (qArticulos.Count(x => x.Articulo == mArticuloFiesta) > 0 && item.Articulo.Substring(0, 1) == "M")
                        mInsertar = false;
                    if (qArticulos.Count(x => x.Articulo == mArticuloMayoreo) > 0)
                        mExistencias = mExistencias + qArticulos.Where(x => x.Articulo == mArticuloMayoreo).First().Existencias;

                    if (mInsertar)
                    {
                        ArticuloInventario mItemInventario = new ArticuloInventario();
                        mItemInventario.Clasificacion = item.Clasificacion;
                        mItemInventario.Articulo = item.Articulo;
                        mItemInventario.Descripcion = item.Descripcion;
                        mItemInventario.UltimaCompra = item.UltimaCompra;
                        mItemInventario.Blanco1 = item.Blanco1;
                        mItemInventario.Existencias = mExistencias;
                        mItemInventario.Blanco2 = item.Blanco2;
                        mItemInventario.ExistenciaFisica = item.ExistenciaFisica;
                        mItemInventario.Conteo = item.Conteo;
                        mItemInventario.Observaciones = item.Observaciones;
                        mItemInventario.Tipo = item.Tipo;
                        mInventario.Add(mItemInventario);
                    }
                }

                foreach (var item in qArticulosCrediplus)
                {
                    bool mInsertar = true;
                    int mExistencias = item.Existencias;

                    string mArticuloFiesta = item.Articulo.Replace("M", "");
                    string mArticuloMayoreo = string.Format("M{0}", item.Articulo);

                    if (qArticulosCrediplus.Count(x => x.Articulo == mArticuloFiesta) > 0 && item.Articulo.Substring(0, 1) == "M") mInsertar = false;
                    if (qArticulosCrediplus.Count(x => x.Articulo == mArticuloMayoreo) > 0) mExistencias = mExistencias + qArticulosCrediplus.Where(x => x.Articulo == mArticuloMayoreo).First().Existencias;

                    if (mInsertar)
                    {
                        ArticuloInventario mItemInventario = new ArticuloInventario();
                        mItemInventario.Clasificacion = item.Clasificacion;
                        mItemInventario.Articulo = item.Articulo;
                        mItemInventario.Descripcion = item.Descripcion;
                        mItemInventario.UltimaCompra = item.UltimaCompra;
                        mItemInventario.Blanco1 = item.Blanco1;
                        mItemInventario.Existencias = mExistencias;
                        mItemInventario.Blanco2 = item.Blanco2;
                        mItemInventario.ExistenciaFisica = item.ExistenciaFisica;
                        mItemInventario.Conteo = item.Conteo;
                        mItemInventario.Observaciones = item.Observaciones;
                        mItemInventario.Tipo = item.Tipo;
                        mInventario.Add(mItemInventario);
                    }
                }

                mInventario.Where(x => x.Clasificacion == "-1").ToList().ForEach(x =>
                {
                    x.AgrupacionEspecial = General.TextoAgrupacionEspecial;
                });

                if (!string.IsNullOrEmpty(clasificacion) && !string.IsNullOrWhiteSpace(clasificacion))
                    mInventario = UtilitariosBodega.FilterData(mInventario, JsonConvert.DeserializeObject<List<int>>(clasificacion));

                return gotOrder ? UtilitariosBodega.OrderData(mInventario, orderBy) : mInventario;
            }
            else
            {
                List<ArticuloInventario> resultData = qArticulos.ToList().Union(qArticulosCrediplus.ToList()).ToList();

                resultData.Where(x => x.Clasificacion == "-1").ToList().ForEach(x =>
                {
                    x.AgrupacionEspecial = $"{General.TextoAgrupacionEspecial} al {DateTime.Today.ToString("dd/MM/yyyy")}";
                });

                if (!string.IsNullOrEmpty(clasificacion) && !string.IsNullOrWhiteSpace(clasificacion))
                    resultData = UtilitariosBodega.FilterData(resultData, JsonConvert.DeserializeObject<List<int>>(clasificacion));

                return gotOrder ? UtilitariosBodega.OrderData(resultData, orderBy) : resultData;
            }
        }


        // POST: api/TomaInventario
        public string Post([FromBody] Clases.TomaInventario inventario)
        {
            log.Info("Servicio POST");

            string mRetorna = "";
            int mLineaArchivo = 1;
            var mErrores = new List<string>();
            db.CommandTimeout = 999999999;

            try
            {
                log.Info("Verificacion XLS");
                if (inventario.Articulos.Count() == 0 && inventario.Tipo == "XLS") return "Error, debe ingresar por lo menos un artículo";

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    log.Info("Verificacion transaction");
                    Int32 mMes = inventario.Mes;
                    Int32 mAnio = inventario.Anio;

                    string mTipo = inventario.Tipo;
                    string mBodega = inventario.Bodega;
                    string mUsuario = inventario.Usuario;

                    log.Info("Tipo: " + mTipo + " Bodega: " + mBodega + " Usuario: " + mUsuario + " Mes: " + mMes + " Anio: " + mAnio);
                    var qBodega = from b in db.BODEGA where b.BODEGA1 == mBodega select b;
                    if (qBodega.Count() == 0) return string.Format("La bodega {0} no existe", mBodega);

                    log.Info("Verificacion mf_inventario");
                    var q = from i in db.MF_Inventario where i.BODEGA == mBodega && i.ANIO == mAnio && i.MES == mMes select i;

                    if (q.Count() > 0)
                    {
                        if (q.First().CERRADO == "S") return string.Format("Error, el mes indicado para {0} ya está cerrado", mBodega);
                    }

                    log.Info("Verificacion mf_inventario");
                    if (q.Count() == 0)
                    {
                        MF_Inventario iInventario = new MF_Inventario()
                        {
                            BODEGA = mBodega,
                            ANIO = mAnio,
                            MES = mMes,
                            FECHA_INICIO_TOMA = DateTime.Now.Date,
                            OBSERVACIONES_TIENDA = "",
                            OBSERVACIONES_OFICINA = "",
                            CERRADO = "N",
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("FA/{0}", mUsuario),
                            UpdatedBy = string.Format("FA/{0}", mUsuario),
                            CreateDate = DateTime.Now
                        };

                        db.MF_Inventario.AddObject(iInventario);
                    }

                    //Si es este caso, es grabación del PDF de la tienda, solo hacemos INSERT a la tabla MF_Inventario
                    if (mTipo == "PDF")
                    {
                        if (q.Count() > 0)
                        {
                            foreach (var item in q)
                            {
                                item.CreatedBy = string.Format("FA/{0}", mUsuario);
                                item.CreateDate = DateTime.Now;
                            }
                        }

                        mRetorna = "PDF cargado exitosamente";
                    }
                    else
                    {
                        //Si se trata de una carga desde EXCEL
                        DateTime mFecha = new DateTime(mAnio, mMes, 1);
                        mFecha = mFecha.AddMonths(1);
                        mFecha = new DateTime(mFecha.Year, mFecha.Month, 1);
                        mFecha = mFecha.AddDays(-1);

                        List<MF_InventarioDetalle> mBorrar = db.MF_InventarioDetalle.Where(x => x.BODEGA == mBodega && x.ANIO == mAnio && x.MES == mMes).ToList();
                        foreach (MF_InventarioDetalle item in mBorrar) db.MF_InventarioDetalle.DeleteObject(item);
                        foreach (var item in inventario.Articulos)
                        {
                            mLineaArchivo++;
                            string mArticulo = item.Articulo;
                            DateTime mUltimaCompra = new DateTime(1980, 1, 1);
                            string mArticuloActivo = "";

                            try
                            {
                                var mArticuloObj = (from ar in db.ARTICULO where ar.ARTICULO1 == mArticulo select ar).Single();

                                mUltimaCompra = mArticuloObj.ULTIMO_INGRESO;
                                mArticuloActivo = mArticuloObj.ACTIVO;


                            }
                            catch
                            {
                                mErrores.Add(string.Format("Línea {0}: código {1} no existe ", mLineaArchivo.ToString(), mArticulo));
                                //Nothing
                            }

                            int mExistenciaFisica = 0;
                            string mConteo = item.Conteo.Trim().Replace(" ", "")
                                                .Replace("&", "+").Replace("q", "+")
                                                .Replace("Q", "+").Replace("w", "+")
                                                .Replace("W", "+").Replace("e", "+")
                                                .Replace("E", "+").Replace("r", "+")
                                                .Replace("R", "+").Replace("t", "+")
                                                .Replace("T", "+").Replace("y", "+")
                                                .Replace("Y", "+").Replace("u", "+")
                                                .Replace("U", "+").Replace("i", "+")
                                                .Replace("I", "+").Replace("o", "+")
                                                .Replace("O", "+").Replace("p", "+")
                                                .Replace("P", "+").Replace("F", "");

                            if (mConteo.Length > 0)
                            {
                                char[] Suma = { '+' };
                                string[] mSumando = mConteo.Split(Suma);

                                for (int jj = 0; jj < mSumando.Length; jj++)
                                {
                                    int mNumero = 0;

                                    try
                                    {
                                        mNumero = Convert.ToInt32(mSumando[jj]);
                                    }
                                    catch
                                    {
                                        mNumero = 0;
                                    }

                                    mExistenciaFisica += mNumero;
                                }
                            }

                            MF_InventarioDetalle iInventarioDetalle = new MF_InventarioDetalle()
                            {
                                BODEGA = mBodega,
                                ANIO = mAnio,
                                MES = mMes,
                                ARTICULO = mArticulo,
                                ULTIMA_COMPRA = mUltimaCompra,
                                BLANCO1 = "",
                                EXISTENCIA_TOTAL = 0,
                                EXISTENCIA_FISICA = mExistenciaFisica,
                                DIFERENCIA = mExistenciaFisica * -1,
                                CONTEO = item.Conteo.Replace("F", ""),
                                OBSERVACIONES = item.Observaciones,
                                RecordDate = DateTime.Now,
                                CreatedBy = string.Format("FA/{0}", mUsuario),
                                UpdatedBy = string.Format("FA/{0}", mUsuario),
                                CreateDate = DateTime.Now
                            };

                            db.MF_InventarioDetalle.AddObject(iInventarioDetalle);
                        }

                    }
                    db.SaveChanges();

                    #region GuardarInformacionFaltante
                    List<string> listArticulos = db.MF_InventarioDetalle
                        .Where(x => x.BODEGA == mBodega && x.ANIO == mAnio && x.MES == mMes)
                        .Select(x => x.ARTICULO).ToList();

                    List<ArticuloInventario> data = db.ARTICULO
                        .Join(db.EXISTENCIA_BODEGA, a => a.ARTICULO1, eb => eb.ARTICULO, (a, eb) => new { a, eb })
                        .Where(x =>
                            x.eb.BODEGA == mBodega
                            && x.eb.CANT_DISPONIBLE > 0
                            && x.a.TIPO != "K"
                            && !x.a.ARTICULO1.StartsWith("M141")
                            && !x.a.ARTICULO1.StartsWith("M142")
                            && !x.a.ARTICULO1.StartsWith("M149")
                            && !listArticulos.Contains(x.eb.ARTICULO))
                        //para desagrupar mf y mayoreo utilizar esta linea en lugar de la del Contains (desagrupar mayoreo-fiesta)
                        // && (!listArticulos.Any(y => y == x.eb.ARTICULO && y.Length == x.eb.ARTICULO.Length))) // que no esté dentro de la lista de artículos
                        .Select(x => new ArticuloInventario()
                        {
                            Clasificacion = x.a.CLASIFICACION_3,
                            Articulo = x.a.ARTICULO1,
                            Descripcion = x.a.DESCRIPCION,
                            UltimaCompra = x.a.ULTIMO_INGRESO,
                            Existencias = (int)x.eb.CANT_DISPONIBLE,
                            Blanco2 = "",
                            ExistenciaFisica = 0,
                            Conteo = "0",
                            Observaciones = "",
                            Tipo = mBodega == "F01"
                                ? //Camas
                                x.a.ARTICULO1.Substring(0, 3) == "021"
                                || x.a.ARTICULO1.Substring(0, 3) == "081"
                                || x.a.ARTICULO1.Substring(0, 2) == "09"
                                || x.a.ARTICULO1.Substring(0, 3) == "110"
                                || x.a.ARTICULO1.Substring(0, 3) == "140"
                                || x.a.ARTICULO1.Substring(0, 3) == "141"
                                || x.a.ARTICULO1.Substring(0, 3) == "142"
                                || x.a.ARTICULO1.Substring(0, 3) == "143"
                                || x.a.ARTICULO1.Substring(0, 3) == "144"
                                || x.a.ARTICULO1.Substring(0, 3) == "145"
                                || x.a.ARTICULO1.Substring(0, 4) == "M141"
                                || x.a.ARTICULO1.Substring(0, 4) == "M142"
                                    ? "CAMAS"
                                    : x.a.ARTICULO1.Substring(0, 1) == "0" || x.a.ARTICULO1.Substring(0, 1) == "A"
                                        ? "DECORACIÓN"
                                        : "RESTO DE ARTÍCULOS"
                                : "",
                            Diferencia = (int)x.eb.CANT_DISPONIBLE
                        }).ToList();

                    data.ForEach(x =>
                    {
                        db.MF_InventarioDetalle.AddObject(new MF_InventarioDetalle()
                        {
                            BODEGA = mBodega,
                            ANIO = mAnio,
                            MES = mMes,
                            ARTICULO = x.Articulo,
                            ULTIMA_COMPRA = x.UltimaCompra,
                            BLANCO1 = "F" + x.Existencias,
                            EXISTENCIA_TOTAL = x.Existencias,
                            EXISTENCIA_FISICA = x.ExistenciaFisica,
                            DIFERENCIA = x.Diferencia,
                            CONTEO = x.Conteo,
                            OBSERVACIONES = x.Observaciones,
                            RecordDate = DateTime.Now,
                            CreatedBy = $"FA/{mUsuario}",
                            UpdatedBy = $"FA/{mUsuario}",
                            CreateDate = DateTime.Now
                        });
                    });
                    db.SaveChanges();
                    #endregion

                    transactionScope.Complete();
                    if (mErrores.Count() > 0)
                    {
                        mRetorna = string.Format("Inventario registrado exitosamente, favor tomar nota de las siguientes inconsistencias: {0}", string.Join(",", mErrores.ToArray()));
                    }
                    else
                    {
                        mRetorna = string.Format("Inventario registrado exitosamente");
                    }
                    log.Info(string.Format("Bodega: {0} Año: {1} Mes: {2} - {3}", mBodega, mAnio, mMes, mRetorna));
                }
            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0} {1}", CatchClass.ExMessage(ex, "TomaInventarioController", "Post"), "");
            }

            return mRetorna;
        }

        // PUT: api/TomaInventario/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/TomaInventario/5
        public void Delete(int id)
        {
        }
    }
}
