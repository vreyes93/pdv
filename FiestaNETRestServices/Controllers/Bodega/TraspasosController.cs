﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;
using Newtonsoft.Json;

namespace FiestaNETRestServices.Controllers.Bodega
{
    public class TraspasosController : MFApiController
    {
        // GET api/traspasos
        public Respuesta GetTraspasos()
        {
            return new Respuesta(false, "Debe ingresar un número de traspaso");
        }

        // GET api/traspasos/5
        public Clases.TraspasoImpresion GetTraspasos(string id)
        {
            Clases.TraspasoImpresion mRetorna = new Clases.TraspasoImpresion();

            if (id == null) return mRetorna;

            id = id.Trim().ToUpper();
            if (id.Trim().Length == 0) return mRetorna;

            var qTraspaso = from a in db.AUDIT_TRANS_INV where a.APLICACION == id select a;
            var qConsecutivoCliente = from c in db.MF_TraspasoCliente select c;
            var qArticulosMayoreo = from a in db.MF_ArticulosMayoreo select a;

            foreach (var item in qTraspaso)
            {
                int jj = 0;
                string mBodegaOrigen = ""; string mArticuloAnterior = "";
                var qArticulo = from t in db.TRANSACCION_INV where t.AUDIT_TRANS_INV == item.AUDIT_TRANS_INV1 orderby t.CONSECUTIVO select t;

                foreach (var articulo in qArticulo)
                {
                    if ((articulo.CONSECUTIVO % 2) == 0)
                    {
                        jj++;
                        string mDescripcion = (from a in db.ARTICULO where a.ARTICULO1 == articulo.ARTICULO select a).First().DESCRIPCION;
                        string mNombreUsuario = (from u in db.USUARIO where u.USUARIO1 == item.USUARIO select u).First().NOMBRE;
                        string mNombreRecibe = ""; string mMayoreo = "N";

                        try
                        {
                            mNombreRecibe = (from c in db.MF_Cobrador join v in db.VENDEDOR on c.CODIGO_JEFE equals v.VENDEDOR1 where c.COBRADOR == articulo.BODEGA select v).First().NOMBRE;
                        }
                        catch
                        {
                            mNombreRecibe = mNombreUsuario;

                            try
                            {
                                mNombreUsuario = (from c in db.MF_Cobrador join v in db.VENDEDOR on c.CODIGO_JEFE equals v.VENDEDOR1 where c.COBRADOR == mBodegaOrigen select v).First().NOMBRE;
                            }
                            catch
                            {
                                mNombreUsuario = "";
                            }
                        }

                        string mSKU = "";
                        foreach (var itemConsecutivo in qConsecutivoCliente)
                        {
                            if (itemConsecutivo.CONSECUTIVO == item.CONSECUTIVO)
                            {
                                mMayoreo = "S";
                                mNombreRecibe = "";
                                mNombreUsuario = (from u in db.USUARIO where u.USUARIO1 == item.USUARIO select u).First().NOMBRE;

                                if (itemConsecutivo.SKU_EN_TRASPASOS == "S")
                                {
                                    if (qArticulosMayoreo.Where(x => x.CLIENTE == itemConsecutivo.CLIENTE && x.ARTICULO == articulo.ARTICULO).Count() > 0)
                                        mSKU = qArticulosMayoreo.Where(x => x.CLIENTE == itemConsecutivo.CLIENTE && x.ARTICULO == articulo.ARTICULO).First().SKU;
                                }
                            }
                        }

                        Traspasos mArticulo = new Traspasos();
                        mArticulo.Traspaso = item.APLICACION;
                        mArticulo.Fecha = new DateTime(item.FECHA_HORA.Year, item.FECHA_HORA.Month, item.FECHA_HORA.Day);
                        mArticulo.Linea = jj;
                        mArticulo.Articulo = articulo.ARTICULO;
                        mArticulo.Descripcion = mDescripcion;
                        mArticulo.Localizacion = articulo.LOCALIZACION;
                        mArticulo.BodegaOrigen = mBodegaOrigen;
                        mArticulo.BodegaDestino = articulo.BODEGA;
                        mArticulo.Cantidad = Convert.ToInt32(articulo.CANTIDAD);
                        mArticulo.Usuario = item.USUARIO;
                        mArticulo.NombreUsuario = mNombreUsuario;
                        mArticulo.CreateDate = Convert.ToDateTime(item.CreateDate);
                        mArticulo.NombreRecibe = mNombreRecibe;
                        mArticulo.Consecutivo = item.CONSECUTIVO;
                        mArticulo.Tooltip = "";
                        mArticulo.Mayoreo = mMayoreo;
                        mArticulo.SKU = mSKU;
                        mRetorna.Traspaso.Add(mArticulo);

                        if (articulo.ARTICULO.Substring(0, 3) == "142" && !string.IsNullOrEmpty(mArticuloAnterior)
                                && (mArticuloAnterior.Substring(0, 3) == "141" || mArticuloAnterior.Substring(0, 4) == "M141")) mRetorna.Traspaso[jj - 2].SKU = "";
                        mArticuloAnterior = articulo.ARTICULO;
                    }
                    else
                    {
                        mBodegaOrigen = articulo.BODEGA;
                    }
                }
            }

            return mRetorna;
        }

        // POST api/traspasos
        public string PostTraspasos([FromBody]List<Traspasos> traspaso)
        {
            int mLogEtapa = 0;
            int mLogAuditTransINV = 0;
            string mRetorna = "";
            string mConsecutivo = "";
            db.CommandTimeout = 999999999;

            try
            {
                if (traspaso.Count() == 0) return "Error, debe ingresar por lo menos un artículo";

                string mUsuario = traspaso[0].Usuario;
                var qConsecutivo = from u in db.MF_UsuarioDespachoTraspaso where u.USUARIO == mUsuario select u;

                DataTable dtBodegas = new DataTable("Bodegas");
                dtBodegas.Columns.Add(new DataColumn("Bodega", typeof(System.String)));
                dtBodegas.Columns.Add(new DataColumn("Consecutivo", typeof(System.String)));

                var qConsecutivoClientes = from c in db.MF_TraspasoCliente select c;

                foreach (var item in qConsecutivoClientes)
                {
                    string[] mBodegasCliente = item.BODEGAS.Split(new string[] { "," }, StringSplitOptions.None);

                    for (int ii = 0; ii < mBodegasCliente.Length; ii++)
                    {
                        DataRow mRow = dtBodegas.NewRow();
                        mRow["Bodega"] = mBodegasCliente[ii];
                        mRow["Consecutivo"] = item.CONSECUTIVO;
                        dtBodegas.Rows.Add(mRow);
                    }
                }

                if (qConsecutivo.Count() == 0) return "Error, usted no tiene acceso a traspasos, notifique a IT";

                foreach (var item in traspaso)
                {
                    try
                    {
                        DALValidar.CompatibilidadTraspasoTiendaProducto(item.BodegaOrigen, item.BodegaDestino, item.Articulo, item.Descripcion);
                    }
                    catch (Exception ex)
                    {
                        log.Error("Error al chequear la CompatibilidadTraspasoTiendaProducto | " + JsonConvert.SerializeObject(ex));
                        return ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                    }

                    if (item.Traspaso.Trim().Length > 0) return "Error, no es posible grabar un traspaso seleccionado";

                    int mCantidad = 0;
                    try
                    {
                        mCantidad = Convert.ToInt32(item.Cantidad);
                    }
                    catch
                    {
                        mCantidad = 0;
                    }

                    if (mCantidad <= 0) return string.Format("Error, la cantidad del artículo {0} es inválida", item.Articulo);
                }

                var qDistinct = traspaso.Select(x => new { x.Articulo, x.BodegaOrigen }).Distinct();
                var qDistinctLocalizacion = traspaso.Select(x => new { x.Articulo, x.BodegaOrigen, x.Localizacion }).Distinct();

                var qDistinctDestino = traspaso.Select(x => new { x.Articulo, x.BodegaDestino }).Distinct();
                var qDistinctDestinoLocalizacion = traspaso.Select(x => new { x.Articulo, x.BodegaDestino, x.Localizacion }).Distinct();

                foreach (var item in qDistinctLocalizacion)
                {
                    var qArticulo = from a in db.ARTICULO where a.ARTICULO1 == item.Articulo select a;
                    if (qArticulo.Count() == 0) return string.Format("Error, el artículo {0} no existe", item.Articulo);
                    if (qArticulo.First().ACTIVO == "N") return string.Format("Error, el artículo {0} no está inactivo", item.Articulo);

                    int mExistencia = 0;
                    int mCantidad = (from t in traspaso where t.Articulo.Equals(item.Articulo) && t.BodegaOrigen.Equals(item.BodegaOrigen) && t.Localizacion.Equals(item.Localizacion) select t.Cantidad).Sum();
                    var qExistencia = from e in db.EXISTENCIA_LOTE where e.ARTICULO == item.Articulo && e.BODEGA == item.BodegaOrigen && e.LOCALIZACION == item.Localizacion select e;

                    if (qExistencia.Count() > 0) mExistencia = Convert.ToInt32(qExistencia.First().CANT_DISPONIBLE);
                    if (mExistencia < mCantidad) return string.Format("Error, requiere {0} unidades en {1} del {2} en {3} y hay {4} disponibles", mCantidad, item.Localizacion, item.Articulo, item.BodegaOrigen, mExistencia);
                }

                mConsecutivo = qConsecutivo.First().CONSECUTIVO_TRASPASO;

                for (int ii = 0; ii < dtBodegas.Rows.Count; ii++)
                {
                    if (Convert.ToString(dtBodegas.Rows[ii]["Bodega"]) == traspaso[0].BodegaDestino)
                        mConsecutivo = Convert.ToString(dtBodegas.Rows[ii]["Consecutivo"]);

                    if (traspaso[0].BodegaDestino == "F01")
                    {
                        if (Convert.ToString(dtBodegas.Rows[ii]["Bodega"]) == traspaso[0].BodegaOrigen)
                            mConsecutivo = Convert.ToString(dtBodegas.Rows[ii]["Consecutivo"]);
                    }
                }

                string mTraspaso = (from c in db.CONSECUTIVO_CI where c.CONSECUTIVO == mConsecutivo select c).First().SIGUIENTE_CONSEC;

                Int32 mAuditTransINV = 0;
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    AUDIT_TRANS_INV iAudit_Trans_INV = new AUDIT_TRANS_INV
                    {
                        CONSECUTIVO = mConsecutivo,
                        USUARIO = mUsuario,
                        FECHA_HORA = DateTime.Now,
                        MODULO_ORIGEN = "CI",
                        APLICACION = mTraspaso,
                        REFERENCIA = "",
                        NoteExistsFlag = 0,
                        RowPointer = Guid.NewGuid(),
                        RecordDate = DateTime.Now,
                        CreatedBy = string.Format("FA/{0}", mUsuario),
                        UpdatedBy = string.Format("FA/{0}", mUsuario),
                        CreateDate = DateTime.Now
                    };

                    db.AUDIT_TRANS_INV.AddObject(iAudit_Trans_INV);
                    mLogEtapa++;//1
                    db.SaveChanges();
                    transactionScope.Complete();
                    mAuditTransINV = iAudit_Trans_INV.AUDIT_TRANS_INV1;
                    mLogAuditTransINV = mAuditTransINV;
                }
                mLogEtapa++;//2
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    foreach (var item in qDistinct)
                    {
                        var qBodegaOrigen = from e in db.EXISTENCIA_BODEGA where e.BODEGA == item.BodegaOrigen && e.ARTICULO == item.Articulo select e;
                        foreach (var itemBodega in qBodegaOrigen)
                        {
                            int mCantidad = (from t in traspaso where t.Articulo.Equals(item.Articulo) && t.BodegaOrigen.Equals(item.BodegaOrigen) select t.Cantidad).Sum();
                            itemBodega.CANT_DISPONIBLE = itemBodega.CANT_DISPONIBLE - mCantidad;
                        }
                    }

                    foreach (var item in qDistinctLocalizacion)
                    {
                        var qLoteOrigen = from e in db.EXISTENCIA_LOTE where e.BODEGA == item.BodegaOrigen && e.ARTICULO == item.Articulo && e.LOCALIZACION == item.Localizacion select e;
                        foreach (var itemLote in qLoteOrigen)
                        {
                            int mCantidad = (from t in traspaso where t.Articulo.Equals(item.Articulo) && t.BodegaOrigen.Equals(item.BodegaOrigen) && t.Localizacion.Equals(item.Localizacion) select t.Cantidad).Sum();
                            itemLote.CANT_DISPONIBLE = itemLote.CANT_DISPONIBLE - mCantidad;
                        }
                    }
                    mLogEtapa++;//3
                    foreach (var item in qDistinctDestino)
                    {
                        var qArticulo = (from a in db.ARTICULO where a.ARTICULO1 == item.Articulo select a).First();

                        decimal mCostoTotFisc = qArticulo.COSTO_PROM_LOC;
                        decimal mCostoTotFiscDol = qArticulo.COSTO_PROM_DOL;
                        int mCantidad = (from t in traspaso where t.Articulo.Equals(item.Articulo) && t.BodegaDestino.Equals(item.BodegaDestino) select t.Cantidad).Sum();

                        var qBodegaDestino = from e in db.EXISTENCIA_BODEGA where e.BODEGA == item.BodegaDestino && e.ARTICULO == item.Articulo select e;
                        if (qBodegaDestino.Count() == 0)
                        {
                            EXISTENCIA_BODEGA iExistenciaBodega = new EXISTENCIA_BODEGA
                            {
                                ARTICULO = item.Articulo,
                                BODEGA = item.BodegaDestino,
                                EXISTENCIA_MINIMA = 0,
                                EXISTENCIA_MAXIMA = 0,
                                PUNTO_DE_REORDEN = 0,
                                CANT_DISPONIBLE = mCantidad,
                                CANT_RESERVADA = 0,
                                CANT_NO_APROBADA = 0,
                                CANT_VENCIDA = 0,
                                CANT_TRANSITO = 0,
                                CANT_PRODUCCION = 0,
                                CANT_PEDIDA = 0,
                                CANT_REMITIDA = 0,
                                CONGELADO = "N",
                                BLOQUEA_TRANS = "N",
                                NoteExistsFlag = 0,
                                RowPointer = Guid.NewGuid(),
                                RecordDate = DateTime.Now,
                                CreatedBy = string.Format("{0}", mUsuario),
                                UpdatedBy = string.Format("{0}", mUsuario),
                                CreateDate = DateTime.Now,
                                COSTO_UNT_PROMEDIO_LOC = mCostoTotFisc,
                                COSTO_UNT_PROMEDIO_DOL = mCostoTotFiscDol,
                                COSTO_UNT_ESTANDAR_LOC = 0,
                                COSTO_UNT_ESTANDAR_DOL = 0,
                                COSTO_PROM_COMPARATIVO_LOC = 0,
                                COSTO_PROM_COMPARATIVO_DOLAR = 0
                            };

                            db.EXISTENCIA_BODEGA.AddObject(iExistenciaBodega);
                        }
                        else
                        {
                            foreach (var itemBodega in qBodegaDestino)
                            {
                                itemBodega.CANT_DISPONIBLE = itemBodega.CANT_DISPONIBLE + mCantidad;
                            }
                        }
                    }
                    mLogEtapa++;//4
                    foreach (var item in qDistinctDestinoLocalizacion)
                    {
                        var qArticulo = (from a in db.ARTICULO where a.ARTICULO1 == item.Articulo select a).First();

                        decimal mCostoTotFisc = qArticulo.COSTO_PROM_LOC;
                        decimal mCostoTotFiscDol = qArticulo.COSTO_PROM_DOL;
                        int mCantidad = (from t in traspaso where t.Articulo.Equals(item.Articulo) && t.BodegaDestino.Equals(item.BodegaDestino) && t.Localizacion.Equals(item.Localizacion) select t.Cantidad).Sum();

                        var qLoteDestino = from e in db.EXISTENCIA_LOTE where e.BODEGA == item.BodegaDestino && e.ARTICULO == item.Articulo && e.LOCALIZACION == item.Localizacion select e;
                        if (qLoteDestino.Count() == 0)
                        {
                            EXISTENCIA_LOTE iExistenciaLote = new EXISTENCIA_LOTE
                            {
                                BODEGA = item.BodegaDestino,
                                ARTICULO = item.Articulo,
                                LOCALIZACION = item.Localizacion,
                                LOTE = "ND",
                                CANT_DISPONIBLE = mCantidad,
                                CANT_RESERVADA = 0,
                                CANT_NO_APROBADA = 0,
                                CANT_VENCIDA = 0,
                                CANT_REMITIDA = 0,
                                NoteExistsFlag = 0,
                                RowPointer = Guid.NewGuid(),
                                RecordDate = DateTime.Now,
                                CreatedBy = string.Format("{0}", mUsuario),
                                UpdatedBy = string.Format("{0}", mUsuario),
                                CreateDate = DateTime.Now,
                                COSTO_UNT_PROMEDIO_LOC = mCostoTotFisc,
                                COSTO_UNT_PROMEDIO_DOL = mCostoTotFiscDol,
                                COSTO_UNT_ESTANDAR_LOC = 0,
                                COSTO_UNT_ESTANDAR_DOL = 0
                            };

                            db.EXISTENCIA_LOTE.AddObject(iExistenciaLote);
                        }
                        else
                        {
                            foreach (var itemLote in qLoteDestino)
                            {
                                itemLote.CANT_DISPONIBLE = itemLote.CANT_DISPONIBLE + mCantidad;
                            }
                        }
                    }
                    mLogEtapa++;//5
                    int jj = 0;

                    foreach (var item in traspaso)
                    {
                        var qArticulo = (from a in db.ARTICULO where a.ARTICULO1 == item.Articulo select a).First();

                        decimal mCostoTotFisc = qArticulo.COSTO_PROM_LOC;
                        decimal mCostoTotFiscDol = qArticulo.COSTO_PROM_DOL;
                        decimal mCostoTotComp = qArticulo.COSTO_ULT_LOC;
                        decimal mCostoTotCompDol = qArticulo.COSTO_ULT_DOL;

                        jj++;
                        TRANSACCION_INV iTransaccionINV = new TRANSACCION_INV
                        {
                            AUDIT_TRANS_INV = mAuditTransINV,
                            CONSECUTIVO = jj,
                            FECHA_HORA_TRANSAC = DateTime.Now,
                            AJUSTE_CONFIG = "~TT~",
                            ARTICULO = item.Articulo,
                            BODEGA = item.BodegaOrigen,
                            LOCALIZACION = item.Localizacion,
                            TIPO = "T",
                            SUBTIPO = "D",
                            SUBSUBTIPO = "",
                            NATURALEZA = "S",
                            CANTIDAD = item.Cantidad * -1,
                            COSTO_TOT_FISC_LOC = mCostoTotFisc,
                            COSTO_TOT_FISC_DOL = mCostoTotFiscDol,
                            COSTO_TOT_COMP_LOC = mCostoTotComp,
                            COSTO_TOT_COMP_DOL = mCostoTotCompDol,
                            PRECIO_TOTAL_LOCAL = 0,
                            PRECIO_TOTAL_DOLAR = 0,
                            CONTABILIZADA = "N",
                            FECHA = DateTime.Now.Date,
                            NoteExistsFlag = 0,
                            RowPointer = Guid.NewGuid(),
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("{0}", mUsuario),
                            UpdatedBy = string.Format("{0}", mUsuario),
                            CreateDate = DateTime.Now
                        };

                        db.TRANSACCION_INV.AddObject(iTransaccionINV);

                        jj++;
                        TRANSACCION_INV iTransaccionINV2 = new TRANSACCION_INV
                        {
                            AUDIT_TRANS_INV = mAuditTransINV,
                            CONSECUTIVO = jj,
                            FECHA_HORA_TRANSAC = DateTime.Now,
                            AJUSTE_CONFIG = "~TT~",
                            ARTICULO = item.Articulo,
                            BODEGA = item.BodegaDestino,
                            LOCALIZACION = item.Localizacion,
                            TIPO = "T",
                            SUBTIPO = "D",
                            SUBSUBTIPO = "",
                            NATURALEZA = "E",
                            CANTIDAD = item.Cantidad,
                            COSTO_TOT_FISC_LOC = mCostoTotFisc,
                            COSTO_TOT_FISC_DOL = mCostoTotFiscDol,
                            COSTO_TOT_COMP_LOC = mCostoTotComp,
                            COSTO_TOT_COMP_DOL = mCostoTotCompDol,
                            PRECIO_TOTAL_LOCAL = 0,
                            PRECIO_TOTAL_DOLAR = 0,
                            CONTABILIZADA = "N",
                            FECHA = DateTime.Now.Date,
                            NoteExistsFlag = 0,
                            RowPointer = Guid.NewGuid(),
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("{0}", mUsuario),
                            UpdatedBy = string.Format("{0}", mUsuario),
                            CreateDate = DateTime.Now
                        };

                        db.TRANSACCION_INV.AddObject(iTransaccionINV2);
                    }
                    mLogEtapa++;//6
                    string mCeros = ""; string mBodega = ""; string mReemplazar = ""; Int32 mTraspasoInt = 0; Int32 mLongitud = 0;

                    if (mConsecutivo.Substring(0, 2) == "TP")
                    {
                        mBodega = mTraspaso.Substring(2, 2);
                        mReemplazar = string.Format("TP{0}-", mBodega);
                        mTraspasoInt = Convert.ToInt32(mTraspaso.Replace(mReemplazar, "")) + 1;
                        mLongitud = mTraspasoInt.ToString().Length;
                    }
                    else
                    {
                        string mMascara = (from c in db.CONSECUTIVO_CI where c.CONSECUTIVO == mConsecutivo select c).First().MASCARA;
                        string mNumeros = mMascara.Replace("N", "");

                        for (int ii = 0; ii < mTraspaso.Length; ii++)
                        {
                            string mCaracter = mTraspaso.Substring(ii, 1);
                            try
                            {
                                int m = Convert.ToInt32(mCaracter);
                            }
                            catch
                            {
                                mReemplazar = string.Format("{0}{1}", mReemplazar, mCaracter);
                            }
                        }

                        mTraspasoInt = Convert.ToInt32(mTraspaso.Replace(mReemplazar, "")) + 1;
                        mLongitud = 13 - mNumeros.Length - mTraspasoInt.ToString().Length;
                    }

                    switch (mLongitud)
                    {
                        case 1:
                            mCeros = "000000";
                            break;
                        case 2:
                            mCeros = "00000";
                            break;
                        case 3:
                            mCeros = "0000";
                            break;
                        case 4:
                            mCeros = "000";
                            break;
                        case 5:
                            mCeros = "00";
                            break;
                        case 6:
                            mCeros = "0";
                            break;
                        default:
                            mCeros = "";
                            break;
                    }

                    string mSiguienteTraspaso = string.Format("TP{0}-{1}{2}", mBodega, mCeros, mTraspasoInt.ToString());
                    if (mConsecutivo.Substring(0, 2) != "TP") mSiguienteTraspaso = string.Format("{0}{1}{2}", mReemplazar, mCeros, mTraspasoInt.ToString());

                    var qConsecutivoI = (from c in db.CONSECUTIVO_CI where c.CONSECUTIVO == mConsecutivo select c);
                    foreach (var item in qConsecutivoI)
                    {
                        item.SIGUIENTE_CONSEC = mSiguienteTraspaso;
                    }
                    mLogEtapa++;//7
                    db.SaveChanges();
                    transactionScope.Complete();
                }
                mLogEtapa++;//8
                mRetorna = string.Format("Se generó exitosamente el traspaso {0}", mTraspaso);
            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0} {1}", CatchClass.ExMessage(ex, "TraspasosController", "PostTraspasos"), "");
                log.Error(string.Format("{0} - Etapa:{1} - AutidTransINV: {2} - {3}", "BODEGA", mLogEtapa, mLogAuditTransINV, mRetorna));

                if (!RollbackExcepcionEnTraspaso(mConsecutivo, mLogAuditTransINV))
                {
                    log.Error("Error en primer rollback");
                    if (!RollbackExcepcionEnTraspaso(mConsecutivo, mLogAuditTransINV))
                    {
                        log.Error("Error en segundo rollback");
                        if (!RollbackExcepcionEnTraspaso(mConsecutivo, mLogAuditTransINV))
                        {
                            log.Error("Error en tercer rollback");
                            log.Error(string.Format("{0} - {1}", "BODEGA", "Error al hacer rollback"));
                        }
                    }
                }
            }

            return mRetorna;
        }

        private bool RollbackExcepcionEnTraspaso(string pConsecutivo,int pAuditTransINV)
        {
            EXACTUSERPEntities dbExactus = new EXACTUSERPEntities();
            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {

                    DateTime mFecha = DateTime.Now.AddDays(-3);

                    List<AUDIT_TRANS_INV> mBorrar = dbExactus.AUDIT_TRANS_INV.Where(
                                                x => x.FECHA_HORA > mFecha
                                                && x.CONSECUTIVO == pConsecutivo//Const.CONSECUTIVO.TRASPASOS
                                                && x.AUDIT_TRANS_INV1 == pAuditTransINV
                                                && !dbExactus.TRANSACCION_INV.Any(ven => ven.AUDIT_TRANS_INV == x.AUDIT_TRANS_INV1)
                                                ).ToList();
                    foreach (AUDIT_TRANS_INV item in mBorrar) dbExactus.AUDIT_TRANS_INV.DeleteObject(item);

                    dbExactus.SaveChanges();
                    transactionScope.Complete();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        // PUT api/traspasos/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/traspasos/5
        public void Delete(int id)
        {
        }
    }
}
