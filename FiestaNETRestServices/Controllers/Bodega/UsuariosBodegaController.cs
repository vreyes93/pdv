﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using FiestaNETRestServices.Content.Abstract;
using Newtonsoft.Json;
using MF_Clases;

namespace FiestaNETRestServices.Controllers.Bodega
{
    public class UsuariosBodegaController : MFApiController
    {

        // GET api/usuariosbodega/5
        public Clases.RetornaUsuarioBodega GetUsuariosBodega(string id = "")
        {
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            db.CommandTimeout = 999999999;
            Clases.RetornaUsuarioBodega mRetorna = new Clases.RetornaUsuarioBodega();

            try
            {
                var q = from ub in db.MF_UsuarioBodega
                        join u in db.USUARIO on ub.USUARIO equals u.USUARIO1
                        join v in db.VENDEDOR on u.CELULAR equals v.VENDEDOR1
                        where v.ACTIVO == "S"
                        orderby ub.USUARIO, ub.BODEGA
                        select new
                        {
                            Usuario = ub.USUARIO,
                            Nombre = v.NOMBRE,
                            Bodega = ub.BODEGA
                        };

                if (id.Trim().Length > 0) q = q.Where(x => x.Usuario.Equals(id));

                foreach (var item in q)
                {
                    Clases.UsuarioBodega mItem = new Clases.UsuarioBodega();
                    mItem.ID = string.Format("{0},{1}", item.Usuario, item.Bodega);
                    mItem.Usuario = item.Usuario;
                    mItem.Nombre = item.Nombre;
                    mItem.Bodega = item.Bodega;
                    mRetorna.UsuarioBodega.Add(mItem);
                }

                var qBodegas = from c in db.MF_Cobrador where c.ACTIVO == "S"
                        orderby c.COBRADOR
                        select new 
                        {
                            Bodega = c.COBRADOR,
                            Nombre = c.NOMBRE
                        };

                foreach (var item in qBodegas)
                {
                    Clases.Bodegas mItem = new Clases.Bodegas();
                    mItem.Bodega = item.Bodega;
                    mItem.Nombre = item.Nombre;
                    mRetorna.Bodegas.Add(mItem);
                }

                var qUsuarios = from u in db.USUARIO 
                        join v in db.VENDEDOR on u.CELULAR equals v.VENDEDOR1
                        where v.ACTIVO == "S"
                        orderby v.NOMBRE
                        select new
                        {
                            Usuario = u.USUARIO1,
                            Nombre = v.NOMBRE
                        };

                foreach (var item in qUsuarios)
                {
                    Clases.Usuarios mItem = new Clases.Usuarios();
                    mItem.Usuario = item.Usuario;
                    mItem.Nombre = item.Nombre;
                    mRetorna.Usuarios.Add(mItem);
                }

                bool mExito = true;
                string mMensaje = "Usuarios consultados exitosamente";
                if (id.Trim().Length > 0) mMensaje = "Usuario consultado exitosamente";

                if (q.Count() == 0)
                {
                    mExito = false;
                    mMensaje = "No se encontraron usuarios asignados a bodegas";
                    if (id.Trim().Length > 0) mMensaje = string.Format("No se encontró al usuario {0}", id);
                }

                Clases.RetornaExito mItemExito = new Clases.RetornaExito();
                mItemExito.exito = mExito;
                mItemExito.mensaje = mMensaje;
                mRetorna.Exito.Add(mItemExito);
            }
            catch (Exception ex)
            {
                string mMensajeError = string.Format("{0} {1}", CatchClass.ExMessage(ex, "UsuariosBodegaController", "GetUsuariosBodega"), "");

                Clases.RetornaExito mExito = new Clases.RetornaExito();
                mExito.exito = false;
                mExito.mensaje = mMensajeError;
                mRetorna.Exito.Add(mExito);
            }

            return mRetorna;
        }

        // POST api/usuariosbodega
        public string PostUsuarioBodega([FromBody]string id)
        {
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            db.CommandTimeout = 999999999;
            string mRetorna = "";

            try
            {
                string[] mKey = id.Split(new string[] { "-" }, StringSplitOptions.None);
                string mUsuarioBodega = mKey[0];
                string mBodega = mKey[1];
                string mUsuario = mKey[2];

                if ((from u in db.MF_UsuarioBodega where u.USUARIO == mUsuarioBodega && u.BODEGA == mBodega select u).Count() > 0)
                    return string.Format("Error, el usuario ya existe en {0}", mBodega);
                
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    MF_UsuarioBodega iUsuario = new MF_UsuarioBodega
                    {
                        USUARIO = mUsuarioBodega,
                        BODEGA = mBodega,
                        RecordDate = DateTime.Now,
                        CreatedBy = string.Format("FA/{0}", mUsuario),
                        UpdatedBy = string.Format("FA/{0}", mUsuario),
                        CreateDate = DateTime.Now
                    };
                    db.MF_UsuarioBodega.AddObject(iUsuario);

                    db.SaveChanges();
                    transactionScope.Complete();
                }

                mRetorna = "El usuario fue grabado exitosamente";
            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0} {1}", CatchClass.ExMessage(ex, "UsuariosBodegaController", "PostUsuarioBodega"), "");
            }

            return mRetorna;
        }

        public string DeleteUsuarioBodega(string id)
        {
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            db.CommandTimeout = 999999999;
            string mRetorna = "";

            try
            {
                string[] mKey = id.Split(new string[] { "," }, StringSplitOptions.None);
                string mUsuario = mKey[0];
                string mBodega = mKey[1];

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    List<MF_UsuarioBodega> mBorrar = db.MF_UsuarioBodega.Where(x => x.USUARIO == mUsuario && x.BODEGA == mBodega).ToList();
                    foreach (MF_UsuarioBodega item in mBorrar) db.MF_UsuarioBodega.DeleteObject(item);

                    db.SaveChanges();
                    transactionScope.Complete();
                }

                mRetorna = "Registro eliminado exitosamente";
            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0} {1}", CatchClass.ExMessage(ex, "UsuarioBodegaController", "DeleteUsuarioBodega"), "");
            }

            return mRetorna;
        }

    }
}
