﻿using FiestaNETRestServices.DAL;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.BuroInterno
{
    public class CalificacionClienteController : ApiController
    {

        /// <summary>
        /// Esta función tiene como propósito obtener la calificacion del cliente en Muebles Fiesta
        /// para debuguear se llama asi en MV5: http://localhost:53874/api/BuroInterno/CalificacionCliente
        /// para debuguear en MV4: http://localhost:53874/api/CalificacionCliente
        /// </summary>
        /// <param name="mUsuario">.</param>
        /// <returns>Retorna un IHttpActionResult Ok(BuroInterno) o BadRequest(Mensaje de respuesta con el mensaje de error.), . </returns>
        //-----------------------------------------------------------------------------------------------------------------------
        #region Get
        [Route("~/GetCalificacionCliente?Documento={Documento}&Usuario={Usuario}")]
        [HttpGet]
        public IHttpActionResult GetCalificacionCliente(string Documento, string Usuario)
        {
            BuroCalificacion mRespuesta = new BuroCalificacion();
            try
            {
                DALCliente mCliente = new DALCliente();

                Respuesta mRespuestaDAL = mCliente.ObtenerCalificacionClienteByDPI(Documento);
                if (mRespuestaDAL.Exito)
                    return Ok((BuroCalificacion)mRespuestaDAL.Objeto);

                return BadRequest(mRespuestaDAL.Mensaje);
            }
            catch
            {
                return BadRequest("Excepcion, favor de comunicarse con Informática.");
            }

        }
        #endregion
    }
}
