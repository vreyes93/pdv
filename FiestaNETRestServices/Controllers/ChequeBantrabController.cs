﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers
{
    public class ChequeBantrabController : MFApiController
    {
        
        public HttpResponseMessage GetCheque(string id)
        {

            var response = new HttpResponseMessage();


            StringBuilder mensaje = new StringBuilder();
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            log.Info("Iniciando consulta de cheques.");
            DateTime mFechaConsulta;

            //Utils.Utilitarios mUtilitarios = new Utils.Utilitarios();

            mensaje.Append(Utilitario.CorreoInicioFormatoRojo());

            if (!Utilitario.FechaValida(id))
            {
                response.Content = new StringContent("<br>Favor de validar el formato de la fecha <b>YYYY-MM-DD</b>. Ejemplo: <b>2019-03-21</b></br>");
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                return response;
            }
            mFechaConsulta = Utilitario.Fecha(id);
            var respuesta = from mb in db.MOV_BANCOS
                            where mb.CUENTA_BANCO == "10-9004645-2"
                            && mb.TIPO_DOCUMENTO == "CHQ"
                            && mb.FECHA == mFechaConsulta
                            //&& mb.NUMERO == id. 
                            select new { CHEQUE = mb.NUMERO, MONTO = mb.MONTO, ANOMBREDE = mb.PAGADERO_A, FECHA = mb.FECHA };

            if (respuesta.Count() > 0)
            {
                mensaje.Append("<table align='center' class='style4'>");
                mensaje.Append("<tr>");
                mensaje.Append(Utilitario.FormatoTituloIzquierdo("CHEQUE"));
                mensaje.Append(Utilitario.FormatoTituloIzquierdo("MONTO"));
                mensaje.Append(Utilitario.FormatoTituloIzquierdo("FECHA"));
                mensaje.Append(Utilitario.FormatoTituloIzquierdo("A NOMBRE DE"));

                mensaje.Append("</tr>");
                foreach (var item in respuesta)
                {
                    mensaje.Append("<tr>");
                    mensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.CHEQUE.ToString()));
                    mensaje.Append(Utilitario.FormatoCuerpoDerecha(Utilitario.FormatoNumeroDecimal(item.MONTO)));
                    mensaje.Append(Utilitario.FormatoCuerpoCentro(Utilitario.FormatoDDMMYYYY(item.FECHA)));
                    mensaje.Append(Utilitario.FormatoCuerpoIzquierda(HttpUtility.HtmlEncode(item.ANOMBREDE.ToString())));
                    mensaje.Append("</tr>");
                }
                mensaje.Append("</table><br/>");

            }
            else
                mensaje.Append("No existen datos de Cheque");

            mensaje.Append(Utilitario.CorreoFin());
            response.Content = new StringContent(mensaje.ToString());
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }
    }
}
