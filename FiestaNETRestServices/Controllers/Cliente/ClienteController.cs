﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.DAL;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using ActionNameAttribute = System.Web.Mvc.ActionNameAttribute;
using HttpGetAttribute = System.Web.Mvc.HttpGetAttribute;

namespace FiestaNETRestServices.Controllers.Cliente
{
    [RoutePrefix("api/Cliente")]
    public class ClienteController : MFApiController
    {
        
        
        [HttpGet]
        public HttpResponseMessage Get(string id)
        {
            // DALCliente cliente = new DALCliente();
            // string HTMLresult = string.Empty;
            //HTMLresult = "<HTML>SIN RESULTADOS</HTML>";

            // return Ok<string>(HTMLresult);
            StringBuilder mensaje = new StringBuilder();
            var response = new HttpResponseMessage();
            DALCliente cliente = new DALCliente();
            string HTMLresult = string.Empty;
            var resp = cliente.ObtenerDireccionEntrega(id, MF_Clases.Utils.CONSTANTES.FINANCIERA.CREDIPLUS);
            if (resp.Exito)
            {
                dtoDireccionCreditos direccion = ((dtoDireccionCreditos)resp.Objeto);
                HTMLresult = Utilitario.CorreoInicioFormatoAzulYGris()+"<BR/><TABLE><TR>"+ Utilitario.FormatoTituloCentro("CREDITO")+Utilitario.FormatoTituloCentro("CLIENTE")+Utilitario.FormatoTituloCentro("DIRECCION ENTREGA")+Utilitario.FormatoTituloCentro("INDICACIONES")+Utilitario.FormatoTituloCentro("ESTADO")+"</TR>";
                HTMLresult += "<TR>"+Utilitario.FormatoCuerpoCentro(id) + Utilitario.FormatoCuerpoCentro(direccion.CLIENTE) + Utilitario.FormatoCuerpoCentro(direccion.DIRECCION)+ Utilitario.FormatoCuerpoCentro(HttpUtility.HtmlEncode(direccion.INDICACIONES))+ Utilitario.FormatoCuerpoCentro(direccion.ESTADO);

                HTMLresult += "</TABLE>";
                if (direccion.ARTICULOS.Count() > 0)
                {
                    HTMLresult += "<h4><br/>ARTICULOS:</h4><TABLE><TR>" + Utilitario.FormatoTituloCentro("ARTICULO") + Utilitario.FormatoTituloCentro(HttpUtility.HtmlEncode("DESCRIPCIÓN")) + Utilitario.FormatoTituloCentro("CANTIDAD") + Utilitario.FormatoTituloCentro("MONTO TOTAL") + "</TR>";
                    foreach (var item in direccion.ARTICULOS)
                    {
                        HTMLresult += "<TR>" + Utilitario.FormatoCuerpoCentro(item.Articulo) + Utilitario.FormatoCuerpoCentro(HttpUtility.HtmlEncode(item.Descripcion)) + Utilitario.FormatoCuerpoCentro(item.cantidad.ToString("##")) + Utilitario.FormatoCuerpoCentro(Utilitario.FormatoNumeroDecimal(item.Precio)) + "</TR>";
                    }
                    HTMLresult += "</TABLE>";
                }

                
            }
            else
                HTMLresult = "<HTML>SIN RESULTADOS</HTML>";

            mensaje.Append(HTMLresult);
            response.Content = new StringContent(mensaje.ToString());
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
           
        }


        

    }
}
