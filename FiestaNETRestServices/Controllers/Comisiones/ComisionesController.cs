﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web.Http;
using FiestaNETRestServices.Models;
using MF_Clases;
using System.Data.Objects;
using MF.Comun.Dto.Comision;
using System.Transactions;
using FiestaNETRestServices.Controllers.Tools;

namespace FiestaNETRestServices.Controllers.Comisiones
{
    public class ComisionesController : ApiController
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET api/comisiones
        public Clases.RetornaComisiones GetComisiones(string anio1, string mes1, string dia1, string anio2, string mes2, string dia2, string vendedor, string tienda)
        {
            string mInfo = "";
            Clases.RetornaComisiones mRetorna = new Clases.RetornaComisiones();

            try
            {
                log.Info("Iniciando proceso de comisiones");

                DateTime mFechaInicial; DateTime mFechaFinal;

                try
                {
                    mFechaInicial = new DateTime(Convert.ToInt32(anio1), Convert.ToInt32(mes1), Convert.ToInt32(dia1));
                }
                catch
                {
                    Clases.Info mItemInfo = new Clases.Info();
                    mItemInfo.Exito = false;
                    mItemInfo.Mensaje = "La fecha inicial es inválida.";
                    mRetorna.Info.Add(mItemInfo);
                    return mRetorna;
                }

                try
                {
                    mFechaFinal = new DateTime(Convert.ToInt32(anio2), Convert.ToInt32(mes2), Convert.ToInt32(dia2));
                }
                catch
                {
                    Clases.Info mItemInfo = new Clases.Info();
                    mItemInfo.Exito = false;
                    mItemInfo.Mensaje = "La fecha final es inválida.";
                    mRetorna.Info.Add(mItemInfo);
                    return mRetorna;
                }

                if (tienda == null) tienda = "";
                if (vendedor == null) vendedor = "";

                EXACTUSERPEntities db = new EXACTUSERPEntities();
                db.CommandTimeout = 999999999;
                DateTime mFechaPendientes = new DateTime(2016, 12, 1);
                DateTime mFechaInicioValidacion = new DateTime(2019, 09, 01);



                if ((from c in db.MF_Comision where c.FECHA_INICIAL == mFechaInicial && c.FECHA_FINAL == mFechaFinal select c).Count() > 0)
                {
                    int jj = 0;
                    string mMensaje = "";

                    var qComision = from c in db.MF_Comision where c.FECHA_INICIAL == mFechaInicial && c.FECHA_FINAL == mFechaFinal orderby c.COBRADOR, c.VENDEDOR select c;

                    if (tienda.Trim().Length > 0) qComision = (System.Linq.IOrderedQueryable<FiestaNETRestServices.Models.MF_Comision>)qComision.Where(x => x.COBRADOR.Equals(tienda));
                    if (vendedor.Trim().Length > 0) qComision = (System.Linq.IOrderedQueryable<FiestaNETRestServices.Models.MF_Comision>)qComision.Where(x => x.COBRADOR.Equals(vendedor));

                    foreach (var item in qComision)
                    {
                        Clases.Comisiones mItemComision = new Clases.Comisiones();
                        mItemComision.NombreVendedor = (from v in db.VENDEDOR where v.VENDEDOR1 == item.VENDEDOR select v).First().NOMBRE;
                        mItemComision.Valor = item.VALOR;
                        mItemComision.MontoComisionable = item.MONTO_COMISIONABLE;
                        mItemComision.ValorNeto = item.VALOR_NETO;
                        mItemComision.Comision = item.COMISION;
                        mItemComision.Porcentaje = item.PORCENTAJE;
                        mItemComision.Vendedor = item.VENDEDOR;
                        mItemComision.Tienda = item.COBRADOR;
                        mItemComision.Titulo = "Comisiones";
                        mItemComision.Tooltip = item.TOOLTIP;
                        mRetorna.Comisiones.Add(mItemComision);

                        jj += 1;
                        if (jj == 1)
                        {
                            string mHora = item.CreateDate.ToShortTimeString();
                            string mFecha = string.Format("{0}{1}/{2}{3}/{4}", item.CreateDate.Day < 10 ? "0" : "", item.CreateDate.Day, item.CreateDate.Month < 10 ? "0" : "", item.CreateDate.Month, item.CreateDate.Year);

                            mMensaje = string.Format("Comisiones grabadas por {0} el {1} a las {2}.", (from u in db.USUARIO where u.USUARIO1 == item.CreatedBy.Replace("FA/", "") select u).First().NOMBRE, mFecha, mHora);
                        }
                    }

                    var qComisionTienda = from c in db.MF_ComisionTienda where c.FECHA_INICIAL == mFechaInicial && c.FECHA_FINAL == mFechaFinal orderby c.COBRADOR select c;

                    if (tienda.Trim().Length > 0) qComisionTienda = (System.Linq.IOrderedQueryable<FiestaNETRestServices.Models.MF_ComisionTienda>)qComisionTienda.Where(x => x.COBRADOR.Equals(tienda));

                    foreach (var item in qComisionTienda)
                    {
                        if (vendedor.Trim().Length == 0)
                        {
                            Clases.Tiendas mItemTienda = new Clases.Tiendas();
                            mItemTienda.Tienda = item.COBRADOR;
                            mItemTienda.Valor = item.VALOR;
                            mItemTienda.MontoComisionable = item.MONTO_COMISIONABLE;
                            mItemTienda.ValorNeto = item.VALOR_NETO;
                            mItemTienda.Comision = item.COMISION;
                            mItemTienda.Titulo = "Tiendas";
                            mRetorna.Tiendas.Add(mItemTienda);
                        }
                    }

                    var qComisionFactura = from c in db.MF_ComisionFactura
                                           join N in db.MF_NIVEL_PRECIO_COEFICIENTE on c.NIVEL_PRECIO equals N.NIVEL_PRECIO
                                           join F in db.MF_Financiera on N.FINANCIERA equals F.Financiera
                                           where c.FECHA_INICIAL == mFechaInicial && c.FECHA_FINAL == mFechaFinal
                                           && ((c.VENDEDOR == vendedor) || (vendedor.Trim().Length == 0))
                                           orderby c.COBRADOR, c.VENDEDOR, c.FACTURA
                                           select new
                                           {
                                               c.FECHA_INICIAL,
                                               c.FECHA_FINAL,
                                               c.COBRADOR,
                                               VENDEDOR = c.VENDEDOR,
                                               c.FACTURA,
                                               c.VALOR,
                                               c.MONTO_COMISIONABLE,
                                               c.VALOR_NETO,
                                               c.COMISION,
                                               c.PORCENTAJE,
                                               c.FECHA_FACTURA,
                                               c.FECHA_ENTREGA,
                                               c.RecordDate,
                                               c.CreatedBy,
                                               c.CreateDate,
                                               c.NIVEL_PRECIO,
                                               c.PORCENTAJE_TARJETA,
                                               c.ENVIO,
                                               FormaDePago = F.Nombre
                                           };

                    //if (vendedor.Trim().Length > 0) //qComisionFactura = (System.Linq.IOrderedQueryable<FiestaNETRestServices.Models.MF_ComisionFactura>)qComisionFactura.Where(x => x.VENDEDOR.Equals(vendedor)
                    foreach (var item in qComisionFactura)
                    {
                        bool mInsertarFactura = true;

                        if (tienda.Trim().Length > 0)
                        {
                            mInsertarFactura = false;
                            if (item.COBRADOR == tienda) mInsertarFactura = true;
                        }

                        if (mInsertarFactura)
                        {
                            Clases.Facturas mItemFactura = new Clases.Facturas();
                            mItemFactura.Factura = item.FACTURA;
                            mItemFactura.Valor = item.VALOR;
                            mItemFactura.MontoComisionable = item.MONTO_COMISIONABLE;
                            mItemFactura.ValorNeto = item.VALOR_NETO;
                            mItemFactura.Comision = item.COMISION;
                            mItemFactura.Porcentaje = item.PORCENTAJE;
                            mItemFactura.FechaFactura = item.FECHA_FACTURA;
                            //mItemFactura.FechaEntrega = (DateTime)item.FECHA_ENTREGA;
                            mItemFactura.CodigoVendedor = item.VENDEDOR;
                            mItemFactura.NombreVendedor = (from v in db.VENDEDOR where v.VENDEDOR1 == item.VENDEDOR select v).First().NOMBRE;
                            mItemFactura.Tienda = item.COBRADOR;
                            mItemFactura.Titulo = "Facturas";
                            mItemFactura.NivelPrecio = item.NIVEL_PRECIO;
                            mItemFactura.FormaDePago = item.FormaDePago.Equals("Efectivo/Cheque") ? "Contado" : item.FormaDePago;
                            mItemFactura.PorcentajeTarjeta = (decimal)item.PORCENTAJE_TARJETA;
                            mItemFactura.Envio = item.ENVIO;
                            mRetorna.Facturas.Add(mItemFactura);
                        }
                    }

                    var qComisionExcepcion = from c in db.MF_ComisionExcepcionCalculo where c.FECHA_INICIAL == mFechaInicial && c.FECHA_FINAL == mFechaFinal orderby c.COBRADOR, c.VENDEDOR, c.FACTURA select c;

                    if (vendedor.Trim().Length > 0) qComisionExcepcion = (System.Linq.IOrderedQueryable<FiestaNETRestServices.Models.MF_ComisionExcepcionCalculo>)qComisionExcepcion.Where(x => x.VENDEDOR.Equals(vendedor));

                    foreach (var item in qComisionExcepcion)
                    {
                        bool mInsertarFactura = true;

                        if (tienda.Trim().Length > 0)
                        {
                            mInsertarFactura = false;
                            if (item.COBRADOR == tienda) mInsertarFactura = true;
                        }

                        if (mInsertarFactura)
                        {
                            Clases.Excepciones mItemExcepcion = new Clases.Excepciones();
                            mItemExcepcion.Factura = item.FACTURA;
                            mItemExcepcion.Valor = item.VALOR;
                            mItemExcepcion.MontoComisionable = item.MONTO_COMISIONABLE;
                            mItemExcepcion.ValorNeto = item.VALOR_NETO;
                            mItemExcepcion.Comision = 0;
                            mItemExcepcion.Porcentaje = 0;
                            mItemExcepcion.FechaFactura = item.FECHA_FACTURA;
                            mItemExcepcion.Vendedor = item.VENDEDOR;
                            mItemExcepcion.NombreVendedor = (from v in db.VENDEDOR where v.VENDEDOR1 == item.VENDEDOR select v).First().NOMBRE;
                            mItemExcepcion.Tienda = item.COBRADOR;
                            mItemExcepcion.Tipo = item.TIPO;
                            mItemExcepcion.Titulo = "Excepciones";
                            mRetorna.Excepciones.Add(mItemExcepcion);
                        }
                    }

                    var qComisionAnulada = from c in db.MF_ComisionAnulada where c.FECHA_INICIAL == mFechaInicial && c.FECHA_FINAL == mFechaFinal orderby c.COBRADOR, c.VENDEDOR, c.FACTURA select c;

                    if (vendedor.Trim().Length > 0) qComisionAnulada = (System.Linq.IOrderedQueryable<FiestaNETRestServices.Models.MF_ComisionAnulada>)qComisionAnulada.Where(x => x.VENDEDOR.Equals(vendedor));

                    foreach (var item in qComisionAnulada)
                    {
                        bool mInsertarFactura = true;

                        if (tienda.Trim().Length > 0)
                        {
                            mInsertarFactura = false;
                            if (item.COBRADOR == tienda) mInsertarFactura = true;
                        }

                        if (mInsertarFactura)
                        {
                            Clases.Anuladas mItemAnulada = new Clases.Anuladas();
                            mItemAnulada.Factura = item.FACTURA;
                            mItemAnulada.Valor = 0;
                            mItemAnulada.MontoComisionable = 0;
                            mItemAnulada.ValorNeto = 0;
                            mItemAnulada.Comision = 0;
                            mItemAnulada.Porcentaje = 0;
                            mItemAnulada.FechaFactura = item.FECHA_FACTURA;
                            mItemAnulada.Vendedor = item.VENDEDOR;
                            mItemAnulada.NombreVendedor = (from v in db.VENDEDOR where v.VENDEDOR1 == item.VENDEDOR select v).First().NOMBRE;
                            mItemAnulada.Tienda = item.COBRADOR;
                            mItemAnulada.Titulo = "Anuladas";
                            mRetorna.Anuladas.Add(mItemAnulada);
                        }
                    }

                    var qComisionPendiente = from c in db.MF_ComisionPendiente where c.FECHA_INICIAL == mFechaInicial && c.FECHA_FINAL == mFechaFinal orderby c.COBRADOR, c.VENDEDOR, c.FACTURA select c;

                    if (vendedor.Trim().Length > 0) qComisionPendiente = (System.Linq.IOrderedQueryable<FiestaNETRestServices.Models.MF_ComisionPendiente>)qComisionPendiente.Where(x => x.VENDEDOR.Equals(vendedor));

                    foreach (var item in qComisionPendiente)
                    {
                        bool mInsertarFactura = true;

                        if (tienda.Trim().Length > 0)
                        {
                            mInsertarFactura = false;
                            if (item.COBRADOR == tienda) mInsertarFactura = true;
                        }

                        if (mInsertarFactura)
                        {
                            string mTipo = "Contado"; string mEstado = "";
                            string mTipoFactura = (from f in db.MF_Factura where f.FACTURA == item.FACTURA select f).First().TIPO;
                            if (mTipoFactura == "CR") mTipo = "Crédito";

                            Clases.Pendientes mItemPendiente = new Clases.Pendientes();
                            mItemPendiente.Factura = item.FACTURA;
                            mItemPendiente.Tipo = mTipo;
                            mItemPendiente.Estado = mEstado;
                            mItemPendiente.Valor = item.VALOR;
                            mItemPendiente.MontoComisionable = item.MONTO_COMISIONABLE;
                            mItemPendiente.ValorNeto = item.VALOR_NETO;
                            mItemPendiente.Comision = 0;
                            mItemPendiente.Porcentaje = 0;
                            mItemPendiente.FechaFactura = item.FECHA_FACTURA;
                            mItemPendiente.FechaEntrega = "";
                            if (item.ESTA_DESPACHADO == "S") mItemPendiente.FechaEntrega = string.Format("{0}{1}/{2}{3}/{4}", Convert.ToDateTime(item.FECHA_ENTREGA).Day < 10 ? "0" : "", Convert.ToDateTime(item.FECHA_ENTREGA).Day.ToString(), Convert.ToDateTime(item.FECHA_ENTREGA).Month < 10 ? "0" : "", Convert.ToDateTime(item.FECHA_ENTREGA).Month.ToString(), Convert.ToDateTime(item.FECHA_ENTREGA).Year.ToString());
                            mItemPendiente.EstaDespachado = item.ESTA_DESPACHADO;
                            mItemPendiente.Vendedor = item.VENDEDOR;
                            mItemPendiente.NombreVendedor = (from v in db.VENDEDOR where v.VENDEDOR1 == item.VENDEDOR select v).First().NOMBRE;
                            mItemPendiente.Tienda = item.COBRADOR;
                            mItemPendiente.Titulo = "Pendientes";
                            mRetorna.Pendientes.Add(mItemPendiente);
                        }
                    }

                    Clases.Info mItemInfo = new Clases.Info();
                    mItemInfo.Exito = true;
                    mItemInfo.Mensaje = mMensaje;
                    mItemInfo.FechaInicial = mFechaInicial;
                    mItemInfo.FechaFinal = mFechaFinal;
                    mItemInfo.YaGrabadas = true;
                    mRetorna.Info.Add(mItemInfo);

                    return mRetorna;
                }

                List<string> mProtectores = new List<string> { "110032-000", "110033-000" };

                var qComisiones = from f in db.FACTURA
                                  join v in db.VENDEDOR on f.VENDEDOR equals v.VENDEDOR1
                                  join mf in db.MF_Factura on f.FACTURA1 equals mf.FACTURA
                                  join n in db.MF_NIVEL_PRECIO_COEFICIENTE on mf.NIVEL_PRECIO equals n.NIVEL_PRECIO
                                  join mfi in db.MF_Financiera on mf.FINANCIERA equals mfi.Financiera
                                  join mc in db.MF_Catalogo on Const.CONFIGURACION_PDV_MODULOS equals mc.CODIGO_TABLA
                                  join j in db.VENDEDOR on mf.JEFE equals j.VENDEDOR1 into jefes
                                  from j in jefes.DefaultIfEmpty()
                                  join s in db.VENDEDOR on mf.SUPERVISOR equals s.VENDEDOR1 into supervisores
                                  from s in supervisores.DefaultIfEmpty()
                                      //join desp in db.MF_Despacho on f.FACTURA1 equals desp.DESPACHO into left
                                      //from resDesp in left.DefaultIfEmpty()
                                  where f.FECHA >= mFechaInicioValidacion
                                        && f.FECHA <= mFechaFinal
                                        && f.ANULADA == "N"
                                        && mf.SE_ANULARA == "N"
                                        // && v.ACTIVO == "S"
                                        && !db.MF_ComisionFactura.Any(mcf => mcf.FACTURA == mf.FACTURA)
                                        && !db.MF_ComisionFacturaPagar.Any(mcf => mcf.FACTURA == mf.FACTURA)
                                        //Con esta sentencia habilitamos o no la regla de una factura comisionable
                                        //Una factura comisionable debe estar desembolsada por la financiera.
                                        //de lo contrario no se toma como comisionable.
                                        && mc.CODIGO_CAT == Const.MODULO.HABILITAR_REGLA_COMISIONES_FINANCIERAS
                                        && (mc.NOMBRE_CAT == "N"
                                            || (mc.NOMBRE_CAT == "S"
                                                    && (mfi.Financiera != Const.FINANCIERA.INTERCONSUMO
                                                            || (mfi.Financiera == Const.FINANCIERA.INTERCONSUMO
                                                                && db.MF_Factura_Estado.Any(mfe => mfe.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                                && mfe.FACTURA == mf.FACTURA
                                                                                && mfe.FECHA < EntityFunctions.AddDays(mFechaFinal, mc.ORDEN ?? 3)
                                                                    )
                                                                )
                                                        )
                                                )
                                        )
                                        //Agregamos excepciones para comisiones, listado de vendedores
                                        //que no deben ser facturas comisionables.
                                        && !db.MF_Catalogo.Any(ven => ven.CODIGO_TABLA == Const.CATALOGO.COMISIONES_EXCEPCIONES_VENDEDORES
                                                                && f.VENDEDOR == ven.CODIGO_CAT
                                                            )
                                        //verificamos que no tenga una devolución
                                        && !db.FACTURA.Any(au => au.TIPO_DOCUMENTO == "D" && au.FACTURA_ORIGINAL == f.FACTURA1)
                                        && v.VENDEDOR1 != "0107"
                                  //Verificamos que no exista despachos incompletos
                                  //y que discrimine los articulos promocionales que empiezan cero "0"
                                  //&& !db.FACTURA_LINEA.Any(fl =>
                                  //                    fl.FACTURA == f.FACTURA1
                                  //                    && fl.TIPO_DOCUMENTO == f.TIPO_DOCUMENTO
                                  //                    && fl.ARTICULO.Substring(0, 1) != "0"
                                  //                    && fl.ARTICULO != "110004-000"
                                  //                    && fl.ARTICULO != "110005-000"
                                  //                    && fl.ARTICULO != "110003-000"
                                  //                    && fl.ARTICULO != "110012-000"
                                  //                    && fl.ARTICULO != "110013-000"
                                  //                    && fl.ARTICULO != "110014-000"
                                  //                    && fl.ARTICULO != "110015-000"
                                  //                    && fl.ARTICULO != "110001-000"
                                  //                    && fl.ARTICULO != "110030-000"
                                  //                    && fl.ARTICULO != "110032-000"
                                  //                    && fl.ARTICULO != "110033-000"
                                  //                    && fl.ARTICULO != "110034-000"
                                  //                    && fl.ARTICULO != "110035-000"
                                  //                    && fl.ARTICULO != "110036-000"
                                  //                    && fl.CANTIDAD > fl.CANT_DESPACHADA
                                  //                    )
                                  //&& d.ACEPTADO_CLIENTE == "S"
                                  orderby f.COBRADOR, f.VENDEDOR, f.FACTURA1

                                  select new
                                  {
                                      Factura = f.FACTURA1,
                                      Valor = f.TOTAL_FACTURA,
                                      FechaFactura = f.FECHA,
                                      FechaEntrega = string.Empty,//resDesp.FECHA_ENTREGA,
                                      CodigoVendedor = f.VENDEDOR,
                                      NombreVendedor = v.NOMBRE,
                                      CodigoJefe = mf.JEFE,
                                      NombreJefe = j.NOMBRE,
                                      CodigoSupervisor = mf.SUPERVISOR,
                                      NombreSupervisor = s.NOMBRE,
                                      Tienda = f.COBRADOR,
                                      Iva = mf.PCTJ_IVA,
                                      NivelPrecio = mf.NIVEL_PRECIO,
                                      Financiera = mf.FINANCIERA,
                                      Comision = n.COMISION,
                                      FormaDePago = mfi.Nombre,
                                      Envio = string.Empty//resDesp.DESPACHO

                                  };

                if (tienda.Trim().Length > 0) qComisiones = qComisiones.Where(x => x.Tienda.Equals(tienda));
                if (vendedor.Trim().Length > 0) qComisiones = qComisiones.Where(x => x.CodigoVendedor.Equals(vendedor));

                foreach (var item in qComisiones)
                {
                    decimal mIva = 1 + (item.Iva / 100); decimal mMontoComisionable = 0;
                    var qComisionExcepcion = from e in db.MF_ComisionExcepcion where e.FACTURA == item.Factura select e;

                    if (qComisionExcepcion.Count() == 0)
                    {
                        mMontoComisionable = Math.Round(Convert.ToDecimal(item.Valor * item.Comision), 2, MidpointRounding.AwayFromZero);

                        if (item.Valor < 300) mMontoComisionable = item.Valor;
                        if (mMontoComisionable > item.Valor) mMontoComisionable = item.Valor;
                    }
                    else
                    {
                        if (qComisionExcepcion.First().TIPO == "N")
                        {
                            mMontoComisionable = 0;
                        }
                        else
                        {
                            mMontoComisionable = qComisionExcepcion.First().MONTO_COMISION;
                        }
                    }

                    decimal mValorNeto = Math.Round(mMontoComisionable / mIva, 2, MidpointRounding.AwayFromZero);

                    bool mInsertar = true;
                    if (mRetorna.Facturas.Count() > 0)
                    {
                        if (mRetorna.Facturas.Where(x => x.Factura.Equals(item.Factura)).Count() > 0) mInsertar = false;
                    }

                    if (mInsertar)
                    {
                        Clases.Facturas mItemFactura = new Clases.Facturas();
                        mItemFactura.Factura = item.Factura;
                        mItemFactura.Valor = item.Valor;
                        mItemFactura.MontoComisionable = mMontoComisionable;
                        mItemFactura.ValorNeto = mValorNeto;
                        mItemFactura.Comision = 0;
                        mItemFactura.Porcentaje = 0;
                        mItemFactura.FechaFactura = item.FechaFactura;
                        mItemFactura.CodigoVendedor = item.CodigoVendedor;
                        mItemFactura.NombreVendedor = item.NombreVendedor;
                        mItemFactura.CodigoJefe = item.CodigoJefe;
                        mItemFactura.NombreJefe = item.NombreJefe;
                        mItemFactura.CodigoSupervisor = item.CodigoSupervisor;
                        mItemFactura.NombreSupervisor = item.NombreSupervisor;
                        mItemFactura.Tienda = item.Tienda;
                        mItemFactura.Titulo = "Facturas";
                        mItemFactura.NivelPrecio = item.NivelPrecio;
                        mItemFactura.FormaDePago = item.FormaDePago.Equals("Efectivo/Cheque") ? "Contado" : item.FormaDePago;
                        mItemFactura.PorcentajeTarjeta = (decimal)item.Comision;
                        mItemFactura.Envio = item.Envio;
                        mRetorna.Facturas.Add(mItemFactura);
                    }
                }

                var qComisionesAdicionales = from f in db.MF_ComisionFacturaPagar
                                             join fa in db.FACTURA on f.FACTURA equals fa.FACTURA1
                                             join mf in db.MF_Factura on f.FACTURA equals mf.FACTURA
                                             join v in db.VENDEDOR on f.VENDEDOR equals v.VENDEDOR1
                                             join j in db.VENDEDOR on mf.JEFE equals j.VENDEDOR1 into jefes
                                             from j in jefes.DefaultIfEmpty()
                                             join s in db.VENDEDOR on mf.SUPERVISOR equals s.VENDEDOR1 into supervisores
                                             from s in supervisores.DefaultIfEmpty()
                                             where f.FECHA_INICIAL >= mFechaInicial && f.FECHA_FINAL <= mFechaFinal && fa.ANULADA == "N" && v.ACTIVO == "S"
                                             orderby f.COBRADOR, f.VENDEDOR, f.FACTURA
                                             select new
                                             {
                                                 Factura = f.FACTURA,
                                                 Valor = fa.TOTAL_FACTURA,
                                                 FechaFactura = fa.FECHA,
                                                 FechaEntrega = fa.FECHA,
                                                 CodigoVendedor = f.VENDEDOR,
                                                 NombreVendedor = v.NOMBRE,
                                                 CodigoJefe = mf.JEFE,
                                                 NombreJefe = j.NOMBRE,
                                                 CodigoSupervisor = mf.SUPERVISOR,
                                                 NombreSupervisor = s.NOMBRE,
                                                 Tienda = f.COBRADOR,
                                                 Iva = fa.TOTAL_FACTURA / fa.TOTAL_MERCADERIA,
                                                 NivelPrecio = fa.NIVEL_PRECIO,
                                                 Financiera = 1,
                                                 Comision = 1,
                                                 Envio = "",
                                                 MontoComisionable = f.MONTO_COMISIONABLE
                                             };

                if (tienda.Trim().Length > 0) qComisionesAdicionales = qComisionesAdicionales.Where(x => x.Tienda.Equals(tienda));
                if (vendedor.Trim().Length > 0) qComisionesAdicionales = qComisionesAdicionales.Where(x => x.CodigoVendedor.Equals(vendedor));

                foreach (var item in qComisionesAdicionales)
                {
                    Clases.Facturas mItemFactura = new Clases.Facturas();
                    mItemFactura.Factura = item.Factura;
                    mItemFactura.Valor = item.Valor;
                    mItemFactura.MontoComisionable = item.MontoComisionable;
                    mItemFactura.ValorNeto = Math.Round(item.MontoComisionable / Math.Round(item.Iva, 2, MidpointRounding.AwayFromZero), 2, MidpointRounding.AwayFromZero);
                    mItemFactura.Comision = 0;
                    mItemFactura.Porcentaje = 0;
                    mItemFactura.FechaFactura = item.FechaFactura;
                    mItemFactura.FechaEntrega = item.FechaEntrega;
                    mItemFactura.CodigoVendedor = item.CodigoVendedor;
                    mItemFactura.NombreVendedor = item.NombreVendedor;
                    mItemFactura.CodigoJefe = item.CodigoJefe;
                    mItemFactura.NombreJefe = item.NombreJefe;
                    mItemFactura.CodigoSupervisor = item.CodigoSupervisor;
                    mItemFactura.NombreSupervisor = item.NombreSupervisor;
                    mItemFactura.Tienda = item.Tienda;
                    mItemFactura.Titulo = "Facturas";
                    mItemFactura.NivelPrecio = item.NivelPrecio;
                    mItemFactura.PorcentajeTarjeta = (decimal)item.Comision;
                    mItemFactura.Envio = item.Envio;
                    mRetorna.Facturas.Add(mItemFactura);
                }

                if (mRetorna.Facturas.Count() == 0)
                {
                    Clases.Info mItemInfo = new Clases.Info();
                    mItemInfo.Exito = false;
                    mItemInfo.Mensaje = "No se encontraron facturas.";
                    mRetorna.Info.Add(mItemInfo);
                }

                List<string> mVendedores = mRetorna.Facturas.Select(x => x.CodigoVendedor).Distinct().ToList();

                foreach (var itemVendedor in mVendedores)
                {

                    string mVendedor = itemVendedor;
                    List<Clases.Facturas> mFacturasVendedor = mRetorna.Facturas.Where(x => x.CodigoVendedor.Equals(mVendedor)).ToList();
                    if (mFacturasVendedor.Count() > 0)
                    {
                        mInfo = string.Format("Vendedor = {0}, NombreVendedor = {1}, Factura = {2}", mVendedor, mFacturasVendedor[0].NombreVendedor, mFacturasVendedor[0].Factura);
                        var qVendedor = (from v in db.MF_Vendedor where v.VENDEDOR == mVendedor select v);
                        if (qVendedor.Count() > 0)
                        {
                            string mTienda = qVendedor.FirstOrDefault().COBRADOR;
                            string mNombreVendedor = mFacturasVendedor[0].NombreVendedor;

                            decimal mValor = Convert.ToDecimal(mRetorna.Facturas.Where(x => x.CodigoVendedor.Equals(mVendedor)).Sum(x => x.Valor)); //ds.Tables["facturas"].Compute("SUM(Valor)", string.Format("Vendedor = '{0}'", mVendedor)));
                            decimal mMontoComisionable = Convert.ToDecimal(mRetorna.Facturas.Where(x => x.CodigoVendedor.Equals(mVendedor)).Sum(x => x.MontoComisionable)); //Convert.ToDecimal(ds.Tables["facturas"].Compute("SUM(MontoComisionable)", string.Format("Vendedor = '{0}'", mVendedor)));
                            decimal mValorNeto = Convert.ToDecimal(mRetorna.Facturas.Where(x => x.CodigoVendedor.Equals(mVendedor)).Sum(x => x.ValorNeto)); //Convert.ToDecimal(ds.Tables["facturas"].Compute("SUM(ValorNeto)", string.Format("Vendedor = '{0}'", mVendedor)));

                            decimal mPorcentaje = 0;
                            decimal mComision = 0;

                            mPorcentaje = db.MF_RangoComisionCobrador.Where(x => x.COBRADOR == mTienda && mValorNeto >= x.INICIO && mValorNeto <= x.FIN).Any() ?
                                            (decimal)db.MF_RangoComisionCobrador.Where(x => x.COBRADOR == mTienda && mValorNeto >= x.INICIO && mValorNeto <= x.FIN).FirstOrDefault().PORCENTAJE :
                                            (from r in db.MF_RangoComision where mValorNeto >= r.INICIO && mValorNeto <= r.FIN select r).First().PORCENTAJE;


                            mComision = Math.Round((mPorcentaje * mValorNeto) / 100, 2, MidpointRounding.AwayFromZero);

                            string mTooltip = ""; string mTiendasComision = ""; string mTiendaAnterior = "";
                            for (int ii = 0; ii < mFacturasVendedor.Count(); ii++)
                            {
                                if (ii == 0)
                                {
                                    mTiendasComision = mFacturasVendedor[ii].Tienda.ToString();
                                }
                                else
                                {
                                    if (mTiendaAnterior != mFacturasVendedor[ii].Tienda.ToString()) mTiendasComision = string.Format("{0},{1}", mTiendasComision, mFacturasVendedor[ii].Tienda.ToString());
                                }

                                mTiendaAnterior = mFacturasVendedor[ii].Tienda.ToString();
                            }
                            if (mTiendasComision.Length > 3)
                            {
                                mTooltip = string.Format("{0} generó comisión en: {1}.", mNombreVendedor, mTiendasComision);
                                if (mComision == 0) mTooltip = string.Format("{0} facturó sin comisionar en: {1}.", mNombreVendedor, mTiendasComision);
                            }

                            Clases.Comisiones mItemComision = new Clases.Comisiones();
                            mItemComision.NombreVendedor = mNombreVendedor;
                            mItemComision.Valor = mValor;
                            mItemComision.MontoComisionable = mMontoComisionable;
                            mItemComision.ValorNeto = mValorNeto;
                            mItemComision.Comision = mComision;
                            mItemComision.Porcentaje = mPorcentaje;
                            mItemComision.Vendedor = mVendedor;
                            mItemComision.Tienda = mTienda;
                            mItemComision.Titulo = "Comisiones";
                            mItemComision.Tooltip = mTooltip;
                            mRetorna.Comisiones.Add(mItemComision);
                        }
                    }

                }

                foreach (var itemFactura in mRetorna.Facturas)
                {
                    if (mRetorna.Comisiones.Where(x => x.Vendedor.Equals(itemFactura.CodigoVendedor)).Any())
                    {
                        decimal mPorcentaje = mRetorna.Comisiones.Where(x => x.Vendedor.Equals(itemFactura.CodigoVendedor)).FirstOrDefault().Porcentaje;
                        decimal mComision = Math.Round((mPorcentaje * itemFactura.ValorNeto) / 100, 2, MidpointRounding.AwayFromZero);

                        itemFactura.Comision = mComision;
                        itemFactura.Porcentaje = mPorcentaje;
                    }
                }

                List<string> dbTiendas = db.MF_Cobrador
                    .Where(x => x.ACTIVO == "S" && x.JEFE != "" && x.CODIGO_JEFE != "").Select(x => x.COBRADOR)
                    .ToList();
                List<string> mTiendas = mRetorna.Facturas.OrderBy(x => dbTiendas.Contains(x.Tienda)).Select(x => x.Tienda).Distinct().ToList();

                foreach (var itemTienda in mTiendas)
                {
                    string mTienda = itemTienda;

                    Clases.Tiendas mItemTienda = new Clases.Tiendas();
                    mItemTienda.Tienda = mTienda;
                    mItemTienda.Valor = Convert.ToDecimal(mRetorna.Facturas.Where(x => x.Tienda.Equals(mTienda)).Sum(x => x.Valor));  //Convert.ToDecimal(ds.Tables["facturas"].Compute("SUM(Valor)", string.Format("Tienda = '{0}'", mTienda)));
                    mItemTienda.MontoComisionable = Convert.ToDecimal(mRetorna.Facturas.Where(x => x.Tienda.Equals(mTienda)).Sum(x => x.MontoComisionable));  //Convert.ToDecimal(ds.Tables["facturas"].Compute("SUM(MontoComisionable)", string.Format("Tienda = '{0}'", mTienda)));
                    mItemTienda.ValorNeto = Convert.ToDecimal(mRetorna.Facturas.Where(x => x.Tienda.Equals(mTienda)).Sum(x => x.ValorNeto));  //Convert.ToDecimal(ds.Tables["facturas"].Compute("SUM(ValorNeto)", string.Format("Tienda = '{0}'", mTienda)));
                    mItemTienda.Comision = Convert.ToDecimal(mRetorna.Facturas.Where(x => x.Tienda.Equals(mTienda)).Sum(x => x.Comision));  //Convert.ToDecimal(ds.Tables["facturas"].Compute("SUM(Comision)", string.Format("Tienda = '{0}'", mTienda)));
                    mItemTienda.Titulo = "Tiendas";
                    mRetorna.Tiendas.Add(mItemTienda);
                }

                //Facturas pendientes
                var qPendientes = (from f in db.FACTURA
                                   join v in db.VENDEDOR on f.VENDEDOR equals v.VENDEDOR1
                                   join mf in db.MF_Factura on f.FACTURA1 equals mf.FACTURA
                                   join n in db.MF_NIVEL_PRECIO_COEFICIENTE on mf.NIVEL_PRECIO equals n.NIVEL_PRECIO
                                   join mfi in db.MF_Financiera on mf.FINANCIERA equals mfi.Financiera
                                   join mc in db.MF_Catalogo on Const.CONFIGURACION_PDV_MODULOS equals mc.CODIGO_TABLA
                                   where f.FECHA <= mFechaFinal
                                        && f.ANULADA == "N"
                                        && f.TIPO_DOCUMENTO == "F"
                                        && v.ACTIVO == "S"
                                        && !db.MF_ComisionFactura.Any(mcf => mcf.FACTURA == mf.FACTURA)
                                        && !db.MF_ComisionFacturaPagar.Any(mcf => mcf.FACTURA == mf.FACTURA)
                                        && mc.CODIGO_CAT == Const.MODULO.HABILITAR_REGLA_COMISIONES_FINANCIERAS
                                        && (f.ESTA_DESPACHADO == "N"
                                                || (mc.NOMBRE_CAT == "S"
                                                        && mfi.Financiera == Const.FINANCIERA.INTERCONSUMO
                                                        && f.FECHA > new DateTime(2018, 3, 1)
                                                        && (!db.MF_Factura_Estado.Any(mfeNoDesembolsado => mfeNoDesembolsado.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                    && mfeNoDesembolsado.FACTURA == mf.FACTURA
                                                                )
                                                                || db.MF_Factura_Estado.Any(mfeDesmbolso => mfeDesmbolso.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                        && mfeDesmbolso.FACTURA == mf.FACTURA
                                                                        && mfeDesmbolso.FECHA >= EntityFunctions.AddDays(mFechaFinal, mc.ORDEN ?? 3)
                                                                        )
                                                            )
                                                    )
                                            )
                                        && f.COBRADOR != "F01"
                                        && f.COBRADOR != "ND"
                                        && f.VENDEDOR != "0002"
                                        && f.VENDEDOR != "ND"
                                        && f.VENDEDOR != "0107"
                                        && f.VENDEDOR != "0003"
                                        && f.VENDEDOR != "0001"
                                        && f.VENDEDOR != "000"
                                   orderby f.COBRADOR, f.VENDEDOR, f.FACTURA1
                                   select new { Factura = f.FACTURA1, Valor = f.TOTAL_FACTURA, FechaFactura = f.FECHA, FechaEntrega = f.FECHA, Vendedor = f.VENDEDOR, NombreVendedor = v.NOMBRE, Tienda = f.COBRADOR, EstaDespachado = f.ESTA_DESPACHADO, Iva = mf.PCTJ_IVA, Comision = n.COMISION, Tipo = mf.TIPO }).Union(
                                  from d in db.MF_Despacho
                                  join f in db.FACTURA on d.FACTURA equals f.FACTURA1
                                  join v in db.VENDEDOR on f.VENDEDOR equals v.VENDEDOR1
                                  join mf in db.MF_Factura on d.FACTURA equals mf.FACTURA
                                  join n in db.MF_NIVEL_PRECIO_COEFICIENTE on mf.NIVEL_PRECIO equals n.NIVEL_PRECIO
                                  join mfi in db.MF_Financiera on mf.FINANCIERA equals mfi.Financiera
                                  join mc in db.MF_Catalogo on Const.CONFIGURACION_PDV_MODULOS equals mc.CODIGO_TABLA
                                  where f.FECHA <= mFechaFinal
                                        && f.ANULADA == "N"
                                        && f.TIPO_DOCUMENTO == "F"
                                        && v.ACTIVO == "S"
                                        && !db.MF_ComisionFactura.Any(mcf => mcf.FACTURA == mf.FACTURA)
                                        && !db.MF_ComisionFacturaPagar.Any(mcf => mcf.FACTURA == mf.FACTURA)
                                        && mc.CODIGO_CAT == Const.MODULO.HABILITAR_REGLA_COMISIONES_FINANCIERAS
                                        && ((f.ESTA_DESPACHADO == "S"
                                                    && d.FECHA_ENTREGA > mFechaFinal
                                                )
                                                || (mc.NOMBRE_CAT == "S"
                                                        && mfi.Financiera == Const.FINANCIERA.INTERCONSUMO
                                                        && f.FECHA > new DateTime(2018, 3, 1)
                                                        && (!db.MF_Factura_Estado.Any(mfeNoDesembolsado => mfeNoDesembolsado.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                    && mfeNoDesembolsado.FACTURA == mf.FACTURA
                                                                )
                                                                || db.MF_Factura_Estado.Any(mfeDesmbolso => mfeDesmbolso.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                        && mfeDesmbolso.FACTURA == mf.FACTURA
                                                                        && mfeDesmbolso.FECHA >= EntityFunctions.AddDays(mFechaFinal, mc.ORDEN ?? 3)
                                                                        )
                                                            )
                                                    )
                                            )
                                        && f.COBRADOR != "F01"
                                        && f.COBRADOR != "ND"
                                        && f.VENDEDOR != "0002"
                                        && f.VENDEDOR != "ND"
                                        && f.VENDEDOR != "0107"
                                        && f.VENDEDOR != "0003"
                                        && f.VENDEDOR != "0001"
                                        && f.VENDEDOR != "000"
                                  orderby f.COBRADOR, f.VENDEDOR, f.FACTURA1
                                  select new { Factura = f.FACTURA1, Valor = f.TOTAL_FACTURA, FechaFactura = f.FECHA, FechaEntrega = d.FECHA_ENTREGA, Vendedor = f.VENDEDOR, NombreVendedor = v.NOMBRE, Tienda = f.COBRADOR, EstaDespachado = f.ESTA_DESPACHADO, Iva = mf.PCTJ_IVA, Comision = n.COMISION, Tipo = mf.TIPO })
                    ;

                if (tienda.Trim().Length > 0) qPendientes = qPendientes.Where(x => x.Tienda.Equals(tienda) && x.FechaFactura >= mFechaPendientes).OrderBy(x => x.Tienda).ThenBy(x => x.Vendedor).ThenBy(x => x.Factura);
                if (vendedor.Trim().Length > 0) qPendientes = qPendientes.Where(x => x.Vendedor.Equals(vendedor) && x.FechaFactura >= mFechaPendientes).OrderBy(x => x.Tienda).ThenBy(x => x.Vendedor).ThenBy(x => x.Factura);

                foreach (var item in qPendientes)
                {
                    decimal mIva = 1 + (item.Iva / 100); decimal mMontoComisionable = 0;
                    var qComisionExcepcion = from e in db.MF_ComisionExcepcion where e.FACTURA == item.Factura select e;

                    if (qComisionExcepcion.Count() == 0)
                    {
                        mMontoComisionable = Math.Round(Convert.ToDecimal(item.Valor * item.Comision), 2, MidpointRounding.AwayFromZero);

                        if (item.Valor < 300) mMontoComisionable = item.Valor;
                        if (mMontoComisionable > item.Valor) mMontoComisionable = item.Valor;
                    }
                    else
                    {
                        if (qComisionExcepcion.First().TIPO == "N")
                        {
                            mMontoComisionable = 0;
                        }
                        else
                        {
                            mMontoComisionable = qComisionExcepcion.First().MONTO_COMISION;
                        }
                    }

                    decimal mValorNeto = Math.Round(mMontoComisionable / mIva, 2, MidpointRounding.AwayFromZero);

                    bool mInsertar = true;
                    if (mRetorna.Pendientes.Where(x => x.Factura.Equals(item.Factura)).Count() > 0) mInsertar = false;
                    if (mRetorna.Facturas.Where(x => x.Factura.Equals(item.Factura)).Count() > 0) mInsertar = false;

                    if (mInsertar)
                    {
                        if (item.Factura == "6829C527-46023690")
                        {
                            int jm = 0;
                            jm++;
                        }

                        string mFechaEntrega = "";
                        var qDespachoFactura = from d in db.MF_Despacho where d.FACTURA == item.Factura select d;
                        if (qDespachoFactura.Count() > 0)
                        {
                            DateTime mFechaEntregaDate = qDespachoFactura.OrderByDescending(x => x.FECHA_ENTREGA).First().FECHA_ENTREGA;
                            mFechaEntrega = string.Format("{0}{1}/{2}{3}/{4}", mFechaEntregaDate.Day < 10 ? "0" : "", mFechaEntregaDate.Day.ToString(), mFechaEntregaDate.Month < 10 ? "0" : "", mFechaEntregaDate.Month.ToString(), mFechaEntregaDate.Year.ToString());
                        }

                        string mTipo = "Contado";
                        if (item.Tipo == "CR") mTipo = "Crédito";

                        string mEstado = "";
                        if (item.Tipo == "CR")
                        {
                            mEstado = "No desembolsado";
                            if ((from f in db.MF_Factura_Estado where f.FACTURA == item.Factura && f.ESTADO == "D" select f).Count() > 0) mEstado = "Desembolsado";
                        }

                        Clases.Pendientes mItemPendiente = new Clases.Pendientes();
                        mItemPendiente.Factura = item.Factura;
                        mItemPendiente.Tipo = mTipo;
                        mItemPendiente.Estado = mEstado;
                        mItemPendiente.Valor = item.Valor;
                        mItemPendiente.MontoComisionable = mMontoComisionable;
                        mItemPendiente.ValorNeto = mValorNeto;
                        mItemPendiente.Comision = 0;
                        mItemPendiente.Porcentaje = 0;
                        mItemPendiente.FechaFactura = item.FechaFactura;
                        mItemPendiente.FechaEntrega = mFechaEntrega;
                        //mItemPendiente.FechaEntrega = "";
                        //if (item.EstaDespachado == "S") mItemPendiente.FechaEntrega = string.Format("{0}{1}/{2}{3}/{4}", item.FechaEntrega.Day < 10 ? "0" : "", item.FechaEntrega.Day.ToString(), item.FechaEntrega.Month < 10 ? "0" : "", item.FechaEntrega.Month.ToString(), item.FechaEntrega.Year.ToString());
                        mItemPendiente.EstaDespachado = item.EstaDespachado;
                        mItemPendiente.Vendedor = item.Vendedor;
                        mItemPendiente.NombreVendedor = item.NombreVendedor;
                        mItemPendiente.Tienda = item.Tienda;
                        mItemPendiente.Titulo = "Pendientes";
                        mRetorna.Pendientes.Add(mItemPendiente);
                    }
                }


                //Excepciones
                var qExcepciones = from e in db.MF_ComisionExcepcion
                                   join f in db.FACTURA on e.FACTURA equals f.FACTURA1
                                   join v in db.VENDEDOR on f.VENDEDOR equals v.VENDEDOR1
                                   join mf in db.MF_Factura on f.FACTURA1 equals mf.FACTURA
                                   where f.ANULADA == "N" && f.FECHA >= mFechaInicial && f.FECHA <= mFechaFinal && v.ACTIVO == "S"
                                   orderby f.COBRADOR, v.VENDEDOR1, f.FECHA
                                   select new { Factura = f.FACTURA1, Valor = f.TOTAL_FACTURA, MontoComisionable = e.MONTO_COMISION, ValorNeto = f.TOTAL_MERCADERIA, FechaFactura = f.FECHA, Vendedor = f.VENDEDOR, NombreVendedor = v.NOMBRE, Tienda = f.COBRADOR, Pedido = f.PEDIDO, Tipo = e.TIPO, Iva = mf.PCTJ_IVA };

                if (tienda.Trim().Length > 0) qExcepciones = qExcepciones.Where(x => x.Tienda.Equals(tienda)).OrderBy(p => p.Tienda).ThenBy(p => p.Vendedor).ThenBy(p => p.FechaFactura);
                if (vendedor.Trim().Length > 0) qExcepciones = qExcepciones.Where(x => x.Vendedor.Equals(vendedor)).OrderBy(p => p.Tienda).ThenBy(p => p.Vendedor).ThenBy(p => p.FechaFactura);

                foreach (var item in qExcepciones)
                {
                    bool mInsertar = true;
                    if (mRetorna.Excepciones.Where(x => x.Factura.Equals(item.Factura)).Count() > 0) mInsertar = false;

                    if (mInsertar)
                    {
                        decimal mMontoComisionable = 0;
                        string mTipo = "No pagar comisión";

                        if (item.Tipo == "M")
                        {
                            mTipo = "Monto comisionable modificado";
                            mMontoComisionable = Convert.ToDecimal(item.MontoComisionable);
                        }

                        decimal mIva = 1 + (item.Iva / 100);

                        Clases.Excepciones mItemExcepcion = new Clases.Excepciones();
                        mItemExcepcion.Factura = item.Factura;
                        mItemExcepcion.Valor = item.Valor;
                        mItemExcepcion.MontoComisionable = mMontoComisionable;
                        mItemExcepcion.ValorNeto = Math.Round(mMontoComisionable / mIva, 2, MidpointRounding.AwayFromZero);
                        mItemExcepcion.Comision = 0;
                        mItemExcepcion.Porcentaje = 0;
                        mItemExcepcion.FechaFactura = item.FechaFactura;
                        mItemExcepcion.Vendedor = item.Vendedor;
                        mItemExcepcion.NombreVendedor = item.NombreVendedor;
                        mItemExcepcion.Tienda = item.Tienda;
                        mItemExcepcion.Tipo = mTipo;
                        mItemExcepcion.Titulo = "Excepciones";
                        mRetorna.Excepciones.Add(mItemExcepcion);
                    }
                }


                //Anuladas
                var qAnuladas = from f in db.FACTURA
                                join v in db.VENDEDOR on f.VENDEDOR equals v.VENDEDOR1
                                where f.FECHA >= mFechaInicial && f.FECHA <= mFechaFinal && f.ANULADA == "S" && f.COBRADOR != "F01" && v.ACTIVO == "S"
                                orderby f.COBRADOR, v.VENDEDOR1, f.FECHA
                                select new { Factura = f.FACTURA1, Valor = f.TOTAL_FACTURA, MontoComisionable = f.TOTAL_FACTURA, ValorNeto = f.TOTAL_MERCADERIA, FechaFactura = f.FECHA, Vendedor = f.VENDEDOR, NombreVendedor = v.NOMBRE, Tienda = f.COBRADOR, Pedido = f.PEDIDO };

                if (tienda.Trim().Length > 0) qAnuladas = qAnuladas.Where(x => x.Tienda.Equals(tienda)).OrderBy(p => p.Tienda).ThenBy(p => p.Vendedor).ThenBy(p => p.FechaFactura);
                if (vendedor.Trim().Length > 0) qAnuladas = qAnuladas.Where(x => x.Vendedor.Equals(vendedor)).OrderBy(p => p.Tienda).ThenBy(p => p.Vendedor).ThenBy(p => p.FechaFactura);

                foreach (var item in qAnuladas)
                {
                    bool mInsertar = true;
                    if (mRetorna.Anuladas.Where(x => x.Factura.Equals(item.Factura)).Count() > 0) mInsertar = false;

                    if (mInsertar)
                    {
                        Clases.Anuladas mItemAnulada = new Clases.Anuladas();
                        mItemAnulada.Factura = item.Factura;
                        mItemAnulada.Valor = 0;
                        mItemAnulada.MontoComisionable = 0;
                        mItemAnulada.ValorNeto = 0;
                        mItemAnulada.Comision = 0;
                        mItemAnulada.Porcentaje = 0;
                        mItemAnulada.FechaFactura = item.FechaFactura;
                        mItemAnulada.Vendedor = item.Vendedor;
                        mItemAnulada.NombreVendedor = item.NombreVendedor;
                        mItemAnulada.Tienda = item.Tienda;
                        mItemAnulada.Titulo = "Anuladas";
                        mRetorna.Anuladas.Add(mItemAnulada);
                    }
                }

                Clases.Info mItemInfo2 = new Clases.Info();
                mItemInfo2.Exito = true;
                mItemInfo2.FechaInicial = mFechaInicial;
                mItemInfo2.FechaFinal = mFechaFinal;
                mItemInfo2.Mensaje = "Comisiones generadas exitosamente.";
                mItemInfo2.YaGrabadas = false;
                mRetorna.Info.Add(mItemInfo2);
            }
            catch (Exception ex)
            {
                string mMensajeError = string.Format("{0} {1}", CatchClass.ExMessage(ex, "ComisionesController", "GetComisiones"), mInfo);

                Clases.Info mItemInfo = new Clases.Info();
                mItemInfo.Exito = false;
                mItemInfo.Mensaje = mMensajeError;
                mItemInfo.YaGrabadas = false;
                mRetorna.Info.Add(mItemInfo);

                return mRetorna;
            }

            return mRetorna;
        }

        [HttpGet]
        [Route("Evaluar")]
        public IHttpActionResult EvalExecution(string anio1, string mes1, string dia1, string anio2, string mes2, string dia2)
        {
            try
            {
                DateTime mFechaInicial; DateTime mFechaFinal;

                if (!DateTime.TryParse($"{anio1}-{mes1}-{dia1}", out mFechaInicial))
                    throw new Exception("La fecha inicial es inválida.");

                if (!DateTime.TryParse($"{anio2}-{mes2}-{dia2}", out mFechaFinal))
                    throw new Exception("La fecha final es inválida.");

                using (EXACTUSERPEntities db = new EXACTUSERPEntities())
                {
                    return Ok((from c in db.MF_Comision where c.FECHA_INICIAL == mFechaInicial && c.FECHA_FINAL == mFechaFinal select c).Count() > 0);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
        }


        [HttpDelete]
        [Route("Eliminar")]
        public IHttpActionResult BorrarComisiones(int mes, int anio)
        {
            DateTime today = DateTime.Today;
            DateTime fechaInicial = Convert.ToDateTime($"{anio}-{mes}-1");
            DateTime fechaFinal = Convert.ToDateTime($"{anio}-{mes}-{DateTime.DaysInMonth(anio, mes)}");

            using (EXACTUSERPEntities db = new EXACTUSERPEntities())
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    try
                    {
                        NOMINA_HISTORICO nomina = db.NOMINA_HISTORICO.FirstOrDefault(x => x.NOMINA == General.ComisionesNomina && x.FECHA_INICIO == fechaFinal && x.FECHA_FIN == fechaFinal);
                        if (nomina != null && nomina.FECHA_APROBACION != null)
                            throw new Exception("No se puede borrar las comisiones del mes seleccionado, debido a que la nómina ya fue aprobada.");


                        if (today.Day > 10 && today.Month > mes)
                            throw new Exception("No se puede borrar las comisiones del mes seleccionado, debido a que ya se encuentra cerrado.");

                        if (nomina != null)
                        {
                            MF_SincronizacionComision header = db.MF_SincronizacionComision
                            .FirstOrDefault(x => x.FechaInicio == fechaInicial && x.FechaFin == fechaFinal
                            && x.CodigoNomina == General.ComisionesNomina && x.Nomina == nomina.NUMERO_NOMINA
                            && x.CodigoConcepto == General.ComisionesConcepto);

                            if (header != null)
                            {
                                db.MF_SincronizacionComisionDetalle.Where(x => x.Sincronizacion == header.Sincronizacion)
                                    .ToList().ForEach(x =>
                                    {
                                        db.MF_SincronizacionComisionDetalle.DeleteObject(x);
                                    });

                                db.MF_SincronizacionComision.DeleteObject(header);
                                db.SaveChanges();
                            }
                        }

                        db.MF_Comision.Where(x => x.FECHA_INICIAL == fechaInicial && x.FECHA_FINAL == fechaFinal).ToList().ForEach(x =>
                        {
                            db.MF_Comision.DeleteObject(x);
                            db.SaveChanges();
                        });

                        db.MF_ComisionTienda.Where(x => x.FECHA_INICIAL == fechaInicial && x.FECHA_FINAL == fechaFinal).ToList().ForEach(x =>
                        {
                            db.MF_ComisionTienda.DeleteObject(x);
                            db.SaveChanges();
                        });

                        db.MF_ComisionFactura.Where(x => x.FECHA_INICIAL == fechaInicial && x.FECHA_FINAL == fechaFinal).ToList().ForEach(x =>
                        {
                            db.MF_ComisionFactura.DeleteObject(x);
                            db.SaveChanges();
                        });

                        db.MF_ComisionExcepcionCalculo.Where(x => x.FECHA_INICIAL == fechaInicial && x.FECHA_FINAL == fechaFinal).ToList().ForEach(x =>
                        {
                            db.MF_ComisionExcepcionCalculo.DeleteObject(x);
                            db.SaveChanges();
                        });

                        db.MF_ComisionAnulada.Where(x => x.FECHA_INICIAL == fechaInicial && x.FECHA_FINAL == fechaFinal).ToList().ForEach(x =>
                        {
                            db.MF_ComisionAnulada.DeleteObject(x);
                            db.SaveChanges();
                        });

                        db.MF_ComisionPendiente.Where(x => x.FECHA_INICIAL == fechaInicial && x.FECHA_FINAL == fechaFinal).ToList().ForEach(x =>
                        {
                            db.MF_ComisionPendiente.DeleteObject(x);
                            db.SaveChanges();
                        });

                        db.MF_ComisionJefeSupervisor.Where(x => x.FECHA_INICIAL == fechaInicial && x.FECHA_FINAL == fechaFinal).ToList().ForEach(x =>
                        {
                            db.MF_ComisionJefeSupervisor.DeleteObject(x);
                            db.SaveChanges();
                        });

                        db.MF_ComisionDesgloseJefeSupervisor.Where(x => x.FechaInicial == fechaInicial && x.FechaFinal == fechaFinal).ToList().ForEach(
                            x =>
                            {
                                db.MF_ComisionDesgloseJefeSupervisor.DeleteObject(x);
                                db.SaveChanges();
                            });

                        db.MF_ComisionDetalleJefe.Where(x => x.FechaInicial == fechaInicial && x.FechaFinal == fechaFinal).ToList().ForEach(
                            x =>
                            {
                                db.MF_ComisionDetalleJefe.DeleteObject(x);
                                db.SaveChanges();
                            });

                        db.MF_ComisionResumenJefe.Where(x => x.FechaInicial == fechaInicial && x.FechaFinal == fechaFinal).ToList().ForEach(
                            x =>
                            {
                                db.MF_ComisionResumenJefe.DeleteObject(x);
                                db.SaveChanges();
                            });

                        db.MF_ComisionDetalleSupervisor.Where(x => x.FechaInicial == fechaInicial && x.FechaFinal == fechaFinal).ToList().ForEach(
                            x =>
                            {
                                db.MF_ComisionDetalleSupervisor.DeleteObject(x);
                                db.SaveChanges();
                            });

                        db.MF_ComisionJefeSupervisorResumen.Where(x => x.FECHA_INICIAL == fechaInicial && x.FECHA_FINAL == fechaFinal).ToList().ForEach(x =>
                        {
                            db.MF_ComisionJefeSupervisorResumen.DeleteObject(x);
                            db.SaveChanges();
                        });

                        transaction.Complete();
                        return Ok(true);
                    }
                    catch (Exception ex)
                    {
                        transaction.Dispose();
                        return BadRequest(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                    }
                }
            }
        }

        [HttpPut]
        [Route("EvaluarBoton")]
        public IHttpActionResult EvaluarBoton(int mes, int anio, string boton)
        {
            DateTime today = DateTime.Today;
            DateTime fechaInicial = Convert.ToDateTime($"{anio}-{mes}-1");
            DateTime fechaFinal = Convert.ToDateTime($"{anio}-{mes}-{DateTime.DaysInMonth(anio, mes)}");

            try
            {
                using (EXACTUSERPEntities db = new EXACTUSERPEntities())
                {
                    bool result = false;

                    switch (boton)
                    {
                        case "BorrarComision":
                            NOMINA_HISTORICO nomina = db.NOMINA_HISTORICO.FirstOrDefault(x => x.NOMINA == General.ComisionesNomina && x.FECHA_INICIO == fechaFinal && x.FECHA_FIN == fechaFinal);
                            result = db.MF_Comision.Count(x => x.FECHA_INICIAL == fechaInicial && x.FECHA_FINAL == fechaFinal) > 0;
                            result = ((nomina != null && nomina.FECHA_APROBACION != null) || (today.Day > 10 && today.Month > mes)) ? false : result;
                            break;
                        case "EnviarResumen":
                            result = db.MF_SincronizacionComision.Count(x => x.FechaInicio == fechaInicial && x.FechaFin == fechaFinal) > 0;
                            break;
                    }

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
        }
    }
}
