﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web.Http;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;
using MF_Clases;

namespace FiestaNETRestServices.Controllers.Comisiones
{
    public class ComisionesJefesSupervisoresController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // POST api/comisionesjefessupervisores
        public string PostComisionesJefesSupervisores([FromBody] Clases.RetornaComisionesJefesSupervisores comisiones)
        {
            string mRetorna = "";

            try
            {
                string mUsuario = comisiones.ComisionesJefesSupervisores[0].Usuario;
                DateTime mFechaInicial = comisiones.ComisionesJefesSupervisores[0].FechaInicial; DateTime mFechaFinal = comisiones.ComisionesJefesSupervisores[0].FechaFinal;

                EXACTUSERPEntities db = new EXACTUSERPEntities();
                db.CommandTimeout = 999999999;

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    List<MF_ComisionJefeSupervisor> mComisionBorrar = db.MF_ComisionJefeSupervisor.Where(x => x.FECHA_INICIAL == mFechaInicial && x.FECHA_FINAL == mFechaFinal).ToList();
                    foreach (MF_ComisionJefeSupervisor item in mComisionBorrar) db.MF_ComisionJefeSupervisor.DeleteObject(item);

                    List<MF_ComisionJefeSupervisorResumen> mResumenBorrar = db.MF_ComisionJefeSupervisorResumen.Where(x => x.FECHA_INICIAL == mFechaInicial && x.FECHA_FINAL == mFechaFinal).ToList();
                    foreach (MF_ComisionJefeSupervisorResumen item in mResumenBorrar) db.MF_ComisionJefeSupervisorResumen.DeleteObject(item);

                    foreach (var item in comisiones.ComisionesJefesSupervisores)
                    {
                        MF_ComisionJefeSupervisor iComision = new MF_ComisionJefeSupervisor
                        {
                            FECHA_INICIAL = mFechaInicial,
                            FECHA_FINAL = mFechaFinal,
                            COBRADOR = item.Tienda,
                            SUPERVISOR = System.Net.WebUtility.HtmlDecode(item.Supervisor),
                            CATEGORIA = item.Categoria,
                            UBICACION = item.Ubicacion,
                            JEFE = item.Jefe,
                            CODIGO_JEFE = item.CodigoJefe,
                            CODIGO_SUPERVISOR = item.CodigoSupervisor,
                            VENTAS_SIN_IVA = item.VentasSinIVA,
                            CATEGORIA_SUPERVISOR = item.Categoria_Supervisor,
                            COMISION = item.Comision,
                            VENTAS_TIENDA_SIN_IVA = item.VentasTiendaSinIVA,
                            MINIMO = item.Minimo,
                            META = item.Meta,
                            RESULTADO_TIENDA = item.ResultadoTienda,
                            VENTA_COMISIONAR_JEFE = item.VentaComisionarJefe,
                            PCTJ_JEFE = item.PctjJefe,
                            COMISION_JEFE = item.ComisionJefe,
                            COMISION_JEFE_TOTAL = item.ComisionJefeTotal,
                            PCTJ_SUPERVISOR = item.PctjSupervisor,
                            COMISION_SUPERVISOR = item.ComisionSupervisor,
                            PCTJ_SUPERVISOR2 = item.PctjSupervisor2,
                            COMISION_SUPERVISOR2 = item.ComisionSupervisor2,
                            TITULO = item.Titulo,
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("FA/{0}", mUsuario),
                            UpdatedBy = string.Format("FA/{0}", mUsuario),
                            CreateDate = DateTime.Now
                        };
                        string json = JsonConvert.SerializeObject(iComision);
                        log.Debug("MF_ComisionJefeSupervisor: " + json);

                        db.MF_ComisionJefeSupervisor.AddObject(iComision);
                    }

                    //foreach (var item in comisiones.SupervisoresResumen)
                    //{
                    //    string mRubro = "";

                    //    try
                    //    {
                    //        mRubro = item.Rubro;
                    //        if (mRubro == null) mRubro = "";
                    //    }
                    //    catch
                    //    {
                    //        mRubro = "";
                    //    }

                    //    MF_ComisionJefeSupervisorResumen iResumen = new MF_ComisionJefeSupervisorResumen
                    //    {
                    //        FECHA_INICIAL = mFechaInicial,
                    //        FECHA_FINAL = mFechaFinal,
                    //        CODIGO_SUPERVISOR = item.Codigo_Supervisor,
                    //        SUPERVISOR = System.Net.WebUtility.HtmlDecode(item.Supervisor),
                    //        COMISION = item.Comision,
                    //        INFO1 = item.Info1,
                    //        INFO2 = item.Info2,
                    //        RUBRO = mRubro,
                    //        PORCENTAJE = item.Porcentaje,
                    //        RecordDate = DateTime.Now,
                    //        CreatedBy = string.Format("FA/{0}", mUsuario),
                    //        UpdatedBy = string.Format("FA/{0}", mUsuario),
                    //        CreateDate = DateTime.Now
                    //    };

                    //    string json = JsonConvert.SerializeObject(iResumen);
                    //    log.Debug("MF_ComisionJefeSupervisorResumen: " + json);


                    //    db.MF_ComisionJefeSupervisorResumen.AddObject(iResumen);
                    //}

                    comisiones.JefeSupervisorDetalle
                        .Select(x => new MF_ComisionDesgloseJefeSupervisor()
                        {
                            FechaInicial = mFechaInicial,
                            FechaFinal = mFechaFinal,
                            CodigoSupervisor = x.CodigoSupervisor,
                            NombreSupervisor = x.NombreSupervisor,
                            Categoria = x.Categoria,
                            Tienda = x.Tienda,
                            Ubicacion = x.Ubicacion,
                            CodigoJefe = x.CodigoJefe,
                            NombreJefe = x.NombreJefe,
                            VentasSinIva = x.VentasSinIVA,
                            Comision = x.Comision,
                            VentasTiendaSinIva = x.VentasTiendaSinIVA,
                            Minimo = x.Minimo,
                            Meta = x.Meta,
                            ResultadoTienda = x.ResultadoTienda,
                            VentaComisionarJefe = x.VentaComisionarJefe,
                            PctDividido = x.PctDividido,
                            PctTiendaJefe = x.PctTiendaJefe,
                            ComisionJefe = x.ComisionJefe,
                            ComisionJefeTotal = x.ComisionJefeTotal,
                            PctTiendaS1 = x.PctTiendaS1,
                            ComisionS1 = x.ComisionS1,
                            PctTiendaS2 = x.PctTiendaS2,
                            ComisionS2 = x.ComisionS2,
                            CreatedBy = $"FA/{mUsuario}",
                            UpdatedBy = $"FA/{mUsuario}",
                            CreateDate = DateTime.Now,
                            RecordDate = DateTime.Now
                        }).ToList().ForEach(x => { db.MF_ComisionDesgloseJefeSupervisor.AddObject(x); });

                    comisiones.DetalleComisionesJefe
                        .Select(x => new MF_ComisionDetalleJefe()
                        {
                            FechaInicial = mFechaInicial,
                            FechaFinal = mFechaFinal,
                            Categoria = x.Categoria,
                            Tienda = x.Tienda,
                            Ubicacion = x.Ubicacion,
                            CodigoJefe = x.CodigoJefe,
                            NombreJefe = x.NombreJefe,
                            PctDividido = x.PctDividido,
                            PctTiendaJefe = x.PctTiendaJefe,
                            ComisionJefe = x.ComisionJefe,
                            ComisionJefeTotal = x.ComisionJefeTotal,
                            CreatedBy = $"FA/{mUsuario}",
                            UpdatedBy = $"FA/{mUsuario}",
                            CreateDate = DateTime.Now,
                            RecordDate = DateTime.Now
                        }).ToList().ForEach(x => { db.MF_ComisionDetalleJefe.AddObject(x); });

                    comisiones.ResumenJefe
                        .Select(x => new MF_ComisionResumenJefe()
                        {
                            FechaInicial = mFechaInicial,
                            FechaFinal = mFechaFinal,
                            CodigoJefe = x.CodigoJefe,
                            NombreJefe = x.NombreJefe,
                            Comision = x.Comision,
                            CreatedBy = $"FA/{mUsuario}",
                            UpdatedBy = $"FA/{mUsuario}",
                            CreateDate = DateTime.Now,
                            RecordDate = DateTime.Now
                        }).ToList().ForEach(x => { db.MF_ComisionResumenJefe.AddObject(x); });

                    comisiones.DetalleComisionesSupervisor
                        .Select(x => new MF_ComisionDetalleSupervisor()
                        {
                            FechaInicial = mFechaInicial,
                            FechaFinal = mFechaFinal,
                            Categoria = x.Categoria,
                            Tienda = x.Tienda,
                            Ubicacion = x.Ubicacion,
                            CodigoSupervisor = x.CodigoSupervisor,
                            NombreSupervisor = x.NombreSupervisor,
                            PctDividido = x.PctDividido,
                            PctTiendaS1 = x.PctTiendaS1,
                            ComisionS1 = x.ComisionS1,
                            PctTiendaS2 = x.PctTiendaS2,
                            ComisionS2 = x.ComisionS2,
                            CreatedBy = $"FA/{mUsuario}",
                            UpdatedBy = $"FA/{mUsuario}",
                            CreateDate = DateTime.Now,
                            RecordDate = DateTime.Now
                        }).ToList().ForEach(x => { db.MF_ComisionDetalleSupervisor.AddObject(x); });

                    comisiones.ResumenSupervisor
                        .Select(x => new MF_ComisionJefeSupervisorResumen()
                        {
                            FECHA_INICIAL = mFechaInicial,
                            FECHA_FINAL = mFechaFinal,
                            CODIGO_SUPERVISOR = x.CodigoSupervisor,
                            SUPERVISOR = x.NombreSupervisor,
                            COMISION = x.Comision,
                            INFO1 = "",
                            INFO2 = "",
                            RUBRO = x.Titulo ?? "",
                            CreatedBy = $"FA/{mUsuario}",
                            UpdatedBy = $"FA/{mUsuario}",
                            CreateDate = DateTime.Now,
                            RecordDate = DateTime.Now
                        }).ToList().ForEach(x => { db.MF_ComisionJefeSupervisorResumen.AddObject(x); });

                    db.SaveChanges();
                    transactionScope.Complete();
                }

                mRetorna = "Comisiones de jefes grabadas exitosamente";
            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0} {1}", CatchClass.ExMessage(ex, "ComisionesJefesSupervisoresController", "PostComisionesJefesSupervisores"), "");
            }

            return mRetorna;
        }

        // GET api/comisionesjefessupervisores/5
        public Clases.RetornaComisionesJefesSupervisores GetComisionesJefesSupervisores(string anio1, string mes1, string dia1, string anio2, string mes2, string dia2, string usuario, string tienda = "")
        {
            string mInfo = "";
            Clases.RetornaComisionesJefesSupervisores mRetorna = new Clases.RetornaComisionesJefesSupervisores();

            try
            {
                DateTime mFechaInicial; DateTime mFechaFinal;
                string mTitulo = "Jefes y Supervisores";
                try
                {
                    mFechaInicial = new DateTime(Convert.ToInt32(anio1), Convert.ToInt32(mes1), Convert.ToInt32(dia1));
                }
                catch
                {
                    Clases.Info mItemInfo = new Clases.Info();
                    mItemInfo.Exito = false;
                    mItemInfo.Mensaje = "La fecha inicial es inválida.";
                    mRetorna.Info.Add(mItemInfo);
                    return mRetorna;
                }

                try
                {
                    mFechaFinal = new DateTime(Convert.ToInt32(anio2), Convert.ToInt32(mes2), Convert.ToInt32(dia2));
                }
                catch
                {
                    Clases.Info mItemInfo = new Clases.Info();
                    mItemInfo.Exito = false;
                    mItemInfo.Mensaje = "La fecha final es inválida.";
                    mRetorna.Info.Add(mItemInfo);
                    return mRetorna;
                }

                if (tienda == null) tienda = "";

                //Periodo extraordinario diciembre 2017
                DateTime mFechaExtra = new DateTime(2017, 12, 25);
                if (mFechaFinal == mFechaExtra)
                {
                    mFechaFinal = new DateTime(2017, 12, 31);
                    anio2 = mFechaFinal.Year.ToString();
                    mes2 = mFechaFinal.Month.ToString();
                    dia2 = mFechaFinal.Day.ToString();
                }

                EXACTUSERPEntities db = new EXACTUSERPEntities();
                db.CommandTimeout = 999999999;

                var qCategoriaSup = db.MF_Catalogo.Where(x => x.CODIGO_TABLA == MF_Clases.Utils.CONSTANTES.CATALOGOS.CATEGORIAS_SUPERVISORES);
                if ((from c in db.MF_ComisionJefeSupervisor where c.FECHA_INICIAL == mFechaInicial && c.FECHA_FINAL == mFechaFinal select c).Count() > 0)
                {
                    var qComisiones = from c in db.MF_ComisionJefeSupervisor where c.FECHA_INICIAL == mFechaInicial && c.FECHA_FINAL == mFechaFinal select c;
                    foreach (var item in qComisiones)
                    {
                        Clases.ComisionesJefesSupervisores mItemComision = new Clases.ComisionesJefesSupervisores();
                        mItemComision.FechaInicial = item.FECHA_INICIAL;
                        mItemComision.FechaFinal = item.FECHA_FINAL;
                        mItemComision.Supervisor = item.SUPERVISOR;
                        mItemComision.Categoria = item.CATEGORIA;
                        mItemComision.Tienda = item.COBRADOR;
                        mItemComision.Ubicacion = item.UBICACION;
                        mItemComision.Jefe = item.JEFE;
                        mItemComision.CodigoJefe = item.CODIGO_JEFE;
                        mItemComision.CodigoSupervisor = item.CODIGO_SUPERVISOR;
                        mItemComision.VentasSinIVA = item.VENTAS_SIN_IVA;
                        mItemComision.Comision = item.COMISION;
                        mItemComision.VentasTiendaSinIVA = item.VENTAS_TIENDA_SIN_IVA;
                        mItemComision.Minimo = item.MINIMO;
                        mItemComision.Meta = item.META;
                        mItemComision.ResultadoTienda = item.RESULTADO_TIENDA;
                        mItemComision.VentaComisionarJefe = item.VENTA_COMISIONAR_JEFE;
                        mItemComision.PctjJefe = item.PCTJ_JEFE;
                        mItemComision.ComisionJefe = item.COMISION_JEFE;
                        mItemComision.ComisionJefeTotal = item.COMISION_JEFE_TOTAL;
                        mItemComision.PctjSupervisor = (decimal)(qCategoriaSup.Where(x => x.CODIGO_CAT == item.CODIGO_SUPERVISOR).FirstOrDefault().NOMBRE_CAT == MF_Clases.Utils.CONSTANTES.CATEGORIA_SUPERVISORES.SupervisorTipo1 ? item.PCTJ_SUPERVISOR : item.PCTJ_SUPERVISOR2);
                        mItemComision.ComisionSupervisor = (decimal)(qCategoriaSup.Where(x => x.CODIGO_CAT == item.CODIGO_SUPERVISOR).FirstOrDefault().NOMBRE_CAT == MF_Clases.Utils.CONSTANTES.CATEGORIA_SUPERVISORES.SupervisorTipo1 ? item.COMISION_SUPERVISOR : item.COMISION_SUPERVISOR2);
                        //mItemComision.PctjSupervisorTipo1 = item.PCTJ_SUPERVISOR1;
                        //mItemComision.ComisionSupervisorTipo1 = item.COMISION_SUPERVISOR1;
                        //mItemComision.PctjSupervisorTipo2 = item.PCTJ_SUPERVISOR2;
                        //mItemComision.ComisionSupervisorTipo2 = item.COMISION_SUPERVISOR2;
                        mItemComision.Titulo = item.TITULO;
                        mItemComision.Usuario = item.CreatedBy.Replace("FA/", "");
                        mRetorna.ComisionesJefesSupervisores.Add(mItemComision);
                    }

                    var qResumen = from r in db.MF_ComisionJefeSupervisorResumen where r.FECHA_INICIAL == mFechaInicial && r.FECHA_FINAL == mFechaFinal select r;
                    foreach (var item in qResumen)
                    {
                        Clases.SupervisoresResumen mItemResumen = new Clases.SupervisoresResumen();
                        mItemResumen.FechaInicial = item.FECHA_INICIAL;
                        mItemResumen.FechaFinal = item.FECHA_FINAL;
                        mItemResumen.Usuario = item.CreatedBy.Replace("FA/", "");
                        mItemResumen.Info1 = item.INFO1;
                        mItemResumen.Info2 = item.INFO2;
                        mItemResumen.Supervisor = item.SUPERVISOR;
                        mItemResumen.Comision = item.COMISION;
                        mItemResumen.Rubro = item.RUBRO;
                        mItemResumen.Porcentaje = item.PORCENTAJE;
                        mItemResumen.Codigo_Supervisor = item.CODIGO_SUPERVISOR;
                        mRetorna.SupervisoresResumen.Add(mItemResumen);
                    }

                    //desglose de comisiones por jefe y supervisor
                    #region DesgloseComisiones
                    mRetorna.JefeSupervisorDetalle = db.MF_ComisionDesgloseJefeSupervisor
                        .Where(x => x.FechaInicial == mFechaInicial && x.FechaFinal == mFechaFinal)
                        .Select(x => new Clases.ComisionesJefesSupervisoresDetalle()
                        {
                            CodigoSupervisor = x.CodigoSupervisor,
                            NombreSupervisor = x.NombreSupervisor,
                            Categoria = x.Categoria,
                            Tienda = x.Tienda,
                            Ubicacion = x.Ubicacion,
                            CodigoJefe = x.CodigoJefe,
                            NombreJefe = x.NombreJefe,
                            VentasSinIVA = x.VentasSinIva,
                            Comision = x.Comision,
                            VentasTiendaSinIVA = x.VentasTiendaSinIva,
                            Minimo = x.Minimo,
                            Meta = x.Meta,
                            ResultadoTienda = x.ResultadoTienda,
                            VentaComisionarJefe = x.VentaComisionarJefe,
                            PctDividido = x.PctDividido,
                            PctTiendaJefe = x.PctTiendaJefe,
                            ComisionJefe = x.ComisionJefe,
                            ComisionJefeTotal = x.ComisionJefeTotal,
                            PctTiendaS1 = x.PctTiendaS1,
                            ComisionS1 = x.ComisionS1,
                            PctTiendaS2 = x.PctTiendaS2 ?? 0,
                            ComisionS2 = x.ComisionS2 ?? 0
                        }).ToList();
                    #endregion

                    //detalle comisiones jefe
                    #region DetalleComisionJefe
                    mRetorna.DetalleComisionesJefe = db.MF_ComisionDetalleJefe
                        .Where(x => x.FechaInicial == mFechaInicial && x.FechaFinal == mFechaFinal)
                        .Select(x => new Clases.DetalleComisionJefe()
                        {
                            Categoria = x.Categoria,
                            Tienda = x.Tienda,
                            Ubicacion = x.Ubicacion,
                            CodigoJefe = x.CodigoJefe,
                            NombreJefe = x.NombreJefe,
                            PctDividido = x.PctDividido,
                            PctTiendaJefe = x.PctTiendaJefe,
                            ComisionJefe = x.ComisionJefe,
                            ComisionJefeTotal = x.ComisionJefeTotal
                        }).ToList();
                    #endregion

                    //resumen comisiones jefe
                    #region ResumenComisionJefe
                    mRetorna.ResumenJefe = db.MF_ComisionResumenJefe
                        .Where(x => x.FechaInicial == mFechaInicial && x.FechaFinal == mFechaFinal)
                        .Select(x => new Clases.ResumenComisionJefe()
                        {
                            CodigoJefe = x.CodigoJefe,
                            NombreJefe = x.NombreJefe,
                            Comision = x.Comision
                        }).ToList();
                    #endregion

                    //detalle comisiones supervisor
                    #region DetalleComisionSupervisor
                    mRetorna.DetalleComisionesSupervisor = db.MF_ComisionDetalleSupervisor
                        .Where(x => x.FechaInicial == mFechaInicial && x.FechaFinal == mFechaFinal)
                        .Select(x => new Clases.DetalleComisionSupervisor()
                        {
                            Categoria = x.Categoria,
                            Tienda = x.Tienda,
                            Ubicacion = x.Ubicacion,
                            CodigoSupervisor = x.CodigoSupervisor,
                            NombreSupervisor = x.NombreSupervisor,
                            PctDividido = x.PctDividido,
                            PctTiendaS1 = x.PctTiendaS1,
                            ComisionS1 = x.ComisionS1,
                            PctTiendaS2 = x.PctTiendaS2 ?? 0,
                            ComisionS2 = x.ComisionS2 ?? 0
                        }).ToList();
                    #endregion

                    //resumen comisiones supervisor
                    #region ResumenComisionSupervisor
                    mRetorna.ResumenSupervisor = db.MF_ComisionJefeSupervisorResumen
                        .Where(x => x.FECHA_INICIAL == mFechaInicial && x.FECHA_FINAL == mFechaFinal)
                        .Select(x => new Clases.ResumenComisionSupervisor()
                        {
                            CodigoSupervisor = x.CODIGO_SUPERVISOR,
                            NombreSupervisor = x.SUPERVISOR,
                            Titulo = x.RUBRO,
                            Porcentaje = x.PORCENTAJE,
                            Comision = x.COMISION
                        }).ToList();
                    #endregion

                    Clases.Info mItemInfo2 = new Clases.Info();
                    mItemInfo2.Exito = true;
                    mItemInfo2.FechaInicial = mFechaInicial;
                    mItemInfo2.FechaFinal = mFechaFinal;
                    mItemInfo2.Mensaje = "Comisiones consultadas exitosamente.";
                    mItemInfo2.YaGrabadas = true;
                    mRetorna.Info.Add(mItemInfo2);
                }
                else
                {
                    var qRangos = from r in db.MF_RangoComisionJefeSupervisor select r;
                    var qMetas = from m in db.MF_MetaPeriodoTienda where m.FECHA_INICIAL == mFechaInicial && m.FECHA_FINAL == mFechaFinal select m;

                    var qComisiones = from c in db.MF_Cobrador
                                      join v in db.MF_Vendedor on c.CODIGO_SUPERVISOR equals v.VENDEDOR
                                      join ve in db.VENDEDOR on c.CODIGO_JEFE equals ve.VENDEDOR1
                                      where c.ACTIVO == "S" && c.COBRADOR != "F01"
                                      orderby c.COBRADOR
                                      select new
                                      {
                                          Supervisor = v.NOMBRE,
                                          Categoria = c.CATEGORIA,
                                          Tienda = c.COBRADOR,
                                          Ubicacion = c.NOMBRE,
                                          Jefe = ve.NOMBRE,
                                          CodigoJefe = c.CODIGO_JEFE,
                                          CodigoSupervisor = c.CODIGO_SUPERVISOR
                                      };

                    if (tienda.Trim().Length > 0)
                        qComisiones = qComisiones.Where(x => x.Tienda.Equals(tienda));

                    foreach (var item in qComisiones)
                    {
                        ComisionesController RestComisiones = new ComisionesController();

                        Clases.RetornaComisiones mComisionesTienda = new Clases.RetornaComisiones();
                        mComisionesTienda = RestComisiones.GetComisiones(anio1, mes1, dia1, anio2, mes2, dia2, "", item.Tienda);

                        Clases.RetornaComisiones mComisionesJefe = new Clases.RetornaComisiones();
                        mComisionesJefe = RestComisiones.GetComisiones(anio1, mes1, dia1, anio2, mes2, dia2, item.CodigoJefe, "");

                        decimal mVentasTiendaSinIVA = 0;
                        decimal mVentasJefeSinIVA = 0;
                        decimal mComisionJefeVendedor = 0;

                        if (mComisionesTienda.Tiendas.Count() > 0) mVentasTiendaSinIVA = mComisionesTienda.Tiendas[0].ValorNeto;

                        if (mComisionesJefe.Comisiones.Where(x => x.Vendedor.Equals(item.CodigoJefe)).Count() > 0)
                        {
                            mVentasJefeSinIVA = mComisionesJefe.Comisiones.Where(x => x.Vendedor.Equals(item.CodigoJefe)).First().ValorNeto;
                            mComisionJefeVendedor = mComisionesJefe.Comisiones.Where(x => x.Vendedor.Equals(item.CodigoJefe)).First().Comision;
                        }

                        decimal mVentasComisionarJefe = 0;

                        //Esto es si la tienda no llega a la comisión que tiene el Jefe, ya que el Jefe puede ser que haya comisionado en otra tienda, 
                        //esto es para que no de negativo.
                        if (mVentasTiendaSinIVA > mVentasJefeSinIVA)
                        {
                            mVentasComisionarJefe = mVentasTiendaSinIVA - mVentasJefeSinIVA;
                        }

                        string mResultadoTienda = "ROJO";
                        decimal mPctjJefe = 0;
                        decimal mComisionJefe = 0;
                        //CATEGORIA 1 DE SUPERVISORES
                        decimal mPctjSuprvsrTipo1 = 0;
                        decimal mComisionSuprvsrTipo1 = 0;
                        //CATEGORIA 2 DE SUPERVISORES /SUP SUP
                        decimal mPctjSuprvsrTipo2 = 0;
                        decimal mComisionSuprvsrTipo2 = 0;

                        decimal mMinimo = 0;
                        decimal mMeta = 0;

                        if (qMetas.Where(x => x.COBRADOR == item.Tienda).Count() > 0)
                        {
                            mMeta = qMetas.Where(x => x.COBRADOR == item.Tienda).First().META;
                            mMinimo = qMetas.Where(x => x.COBRADOR == item.Tienda).First().MINIMO;
                        }

                        if (mVentasTiendaSinIVA >= mMinimo)
                            mResultadoTienda = "NEGRO";
                        if (mVentasTiendaSinIVA >= mMeta)
                            mResultadoTienda = "ESTRELLA";

                        switch (mResultadoTienda)
                        {
                            case "ROJO":
                                mPctjJefe = qRangos.Where(x => x.TIPO.Equals("J") && x.CATEGORIA.Equals(item.Categoria)).First().ROJO;
                                mPctjSuprvsrTipo1 = qRangos.Where(x => x.TIPO.Equals("S1") && x.CATEGORIA.Equals(item.Categoria)).First().ROJO;
                                mPctjSuprvsrTipo2 = qRangos.Where(x => x.TIPO.Equals("S2") && x.CATEGORIA.Equals(item.Categoria)).First().ROJO;
                                break;
                            case "NEGRO":
                                mPctjJefe = qRangos.Where(x => x.TIPO.Equals("J") && x.CATEGORIA.Equals(item.Categoria)).First().NEGRO;
                                mPctjSuprvsrTipo1 = qRangos.Where(x => x.TIPO.Equals("S1") && x.CATEGORIA.Equals(item.Categoria)).First().NEGRO;
                                mPctjSuprvsrTipo2 = qRangos.Where(x => x.TIPO.Equals("S2") && x.CATEGORIA.Equals(item.Categoria)).First().NEGRO;
                                break;
                            default:
                                mPctjJefe = qRangos.Where(x => x.TIPO.Equals("J") && x.CATEGORIA.Equals(item.Categoria)).First().ESTRELLA;
                                mPctjSuprvsrTipo1 = qRangos.Where(x => x.TIPO.Equals("S1") && x.CATEGORIA.Equals(item.Categoria)).First().ESTRELLA;
                                mPctjSuprvsrTipo2 = qRangos.Where(x => x.TIPO.Equals("S2") && x.CATEGORIA.Equals(item.Categoria)).First().ESTRELLA;
                                break;
                        }


                        mComisionJefe = mVentasComisionarJefe * (mPctjJefe / 100);
                        mComisionSuprvsrTipo1 = mVentasTiendaSinIVA * (mPctjSuprvsrTipo1 / 100);
                        mComisionSuprvsrTipo2 = mVentasTiendaSinIVA * (mPctjSuprvsrTipo2 / 100);



                        if (mVentasTiendaSinIVA == 0) mResultadoTienda = "";

                        Clases.ComisionesJefesSupervisores mItemComision = new Clases.ComisionesJefesSupervisores();
                        mItemComision.FechaInicial = mFechaInicial;
                        mItemComision.FechaFinal = mFechaFinal;
                        mItemComision.Supervisor = item.Supervisor;
                        mItemComision.Categoria = item.Categoria;
                        mItemComision.Tienda = item.Tienda;
                        mItemComision.Ubicacion = item.Ubicacion;
                        mItemComision.Jefe = item.Jefe;
                        mItemComision.CodigoJefe = item.CodigoJefe;
                        mItemComision.CodigoSupervisor = item.CodigoSupervisor;
                        mItemComision.VentasSinIVA = mVentasJefeSinIVA;
                        mItemComision.Comision = mComisionJefeVendedor;
                        mItemComision.VentasTiendaSinIVA = mVentasTiendaSinIVA;
                        mItemComision.Minimo = mMinimo;
                        mItemComision.Meta = mMeta;
                        mItemComision.ResultadoTienda = mResultadoTienda;
                        mItemComision.VentaComisionarJefe = mVentasComisionarJefe;
                        mItemComision.PctjJefe = mPctjJefe;
                        mItemComision.ComisionJefe = mComisionJefe;
                        mItemComision.ComisionJefeTotal = mComisionJefeVendedor + mComisionJefe;
                        mItemComision.PctjSupervisor = mPctjSuprvsrTipo1;
                        mItemComision.ComisionSupervisor = mComisionSuprvsrTipo1;
                        mItemComision.PctjSupervisor2 = mPctjSuprvsrTipo2;
                        mItemComision.ComisionSupervisor2 = mComisionSuprvsrTipo2;
                        mItemComision.Titulo = mTitulo;
                        mItemComision.Usuario = usuario;
                        mItemComision.Agrupado = "N";
                        mRetorna.ComisionesJefesSupervisores.Add(mItemComision);
                    }

                    var qSupervisores2 = db.MF_Catalogo.Where(x => x.NOMBRE_CAT == MF_Clases.Utils.CONSTANTES.CATEGORIA_SUPERVISORES.SupervisorTipo2 && x.CODIGO_TABLA == MF_Clases.Utils.CONSTANTES.CATALOGOS.CATEGORIAS_SUPERVISORES).Select(x => x.CODIGO_CAT);
                    foreach (var sup in qSupervisores2)
                    {
                        string NombreSupervisor = db.MF_Vendedor.Where(x => x.VENDEDOR == sup).Select(x => x.NOMBRE_COMPLETO).FirstOrDefault();

                        Clases.ComisionesJefesSupervisores mItemComision = new Clases.ComisionesJefesSupervisores();
                        mItemComision.FechaInicial = mFechaInicial;
                        mItemComision.FechaFinal = mFechaFinal;
                        mItemComision.Supervisor = NombreSupervisor;
                        mItemComision.Categoria_Supervisor = MF_Clases.Utils.CONSTANTES.CATEGORIA_SUPERVISORES.SupervisorTipo2;
                        mItemComision.Tienda = "";
                        mItemComision.Categoria = "";
                        mItemComision.Ubicacion = "";
                        mItemComision.Jefe = "";
                        mItemComision.CodigoJefe = "";
                        mItemComision.CodigoSupervisor = sup;
                        mItemComision.VentasSinIVA = 0;
                        mItemComision.Comision = 0;
                        mItemComision.VentasTiendaSinIVA = 0;
                        mItemComision.Minimo = 0;
                        mItemComision.Meta = 0;
                        mItemComision.ResultadoTienda = "NEGRO";
                        mItemComision.VentaComisionarJefe = 0;
                        mItemComision.PctjJefe = 0;
                        mItemComision.ComisionJefe = 0;
                        mItemComision.ComisionJefeTotal = 0;
                        mItemComision.PctjSupervisor = (decimal)(mRetorna.ComisionesJefesSupervisores.Sum(x => x.ComisionSupervisor2) / mRetorna.ComisionesJefesSupervisores.Sum(x => x.VentasTiendaSinIVA)) * 100;
                        mItemComision.ComisionSupervisor = mRetorna.ComisionesJefesSupervisores.Sum(x => x.ComisionSupervisor2) + mRetorna.ComisionesJefesSupervisores.Where(x => x.Categoria_Supervisor == MF_Clases.Utils.CONSTANTES.CATEGORIA_SUPERVISORES.SupervisorTipo1 && x.CodigoSupervisor == sup).Sum(x => x.ComisionSupervisor);
                        mItemComision.Titulo = mTitulo;
                        mItemComision.Usuario = usuario;
                        mItemComision.Agrupado = "S";
                        mRetorna.ComisionesJefesSupervisores.Add(mItemComision);
                    }

                    decimal mTotalVentasTiendaSinIVA = mRetorna.ComisionesJefesSupervisores.Sum(x => x.VentasTiendaSinIVA);
                    decimal mTotalComisionJefe = mRetorna.ComisionesJefesSupervisores.Sum(x => x.ComisionJefeTotal);
                    decimal mTotalComisionSupervisor = mRetorna.ComisionesJefesSupervisores.Sum(x => x.ComisionSupervisor);
                    //decimal mTotalComisionSuprvsrTipo1 = mRetorna.ComisionesJefesSupervisores.Sum(x => x.ComisionSupervisorTipo1);
                    decimal? mTotalComisionSuprvsrTipo2 = mRetorna.ComisionesJefesSupervisores.Sum(x => x.ComisionSupervisor2);
                    List<string> mSupervisores = mRetorna.ComisionesJefesSupervisores.Where(x => x.Titulo.Equals(mTitulo)).Select(x => x.CodigoSupervisor).Distinct().ToList();

                    int ii = 0;
                    foreach (string sup in mSupervisores)
                    {
                        var catSupervisores = db.MF_Catalogo.Where(x => x.CODIGO_TABLA == MF_Clases.Utils.CONSTANTES.CATALOGOS.CATEGORIAS_SUPERVISORES && x.CODIGO_CAT == sup).FirstOrDefault().NOMBRE_CAT;
                        string NombreSupervisor = db.MF_Vendedor.Where(x => x.VENDEDOR == sup).Select(x => x.NOMBRE_COMPLETO).FirstOrDefault();
                        Clases.SupervisoresResumen mItemResumen = new Clases.SupervisoresResumen();
                        mItemResumen.FechaInicial = mFechaInicial;
                        mItemResumen.FechaFinal = mFechaFinal;
                        mItemResumen.Usuario = usuario;
                        mItemResumen.Info1 = "";
                        mItemResumen.Info2 = "";
                        mItemResumen.Categoria = catSupervisores;
                        mItemResumen.Supervisor = NombreSupervisor;
                        mItemResumen.Codigo_Supervisor = sup;
                        mItemResumen.Comision = mRetorna.ComisionesJefesSupervisores.Where(x => x.CodigoSupervisor.Equals(sup)).Sum(x => x.ComisionSupervisor);


                        if (ii == 0)
                        {
                            mItemResumen.Rubro = "% Jefes sobre venta sin IVA";
                            mItemResumen.Porcentaje = (mTotalComisionJefe / mTotalVentasTiendaSinIVA) * 100;
                        }
                        if (ii == 1)
                        {
                            mItemResumen.Rubro = "% Supervisores sobre venta sin IVA";
                            //mItemResumen.Porcentaje =( (catSupervisores == MF_Clases.Utils.CONSTANTES.CATEGORIA_SUPERVISORES.SupervisorTipo1 ?  mTotalComisionSuprvsrTipo1 : mTotalComisionSuprvsrTipo1 ) / mTotalVentasTiendaSinIVA) * 100;
                            mItemResumen.Porcentaje = (mTotalComisionSupervisor / mTotalVentasTiendaSinIVA) * 100;
                        }
                        if (ii == 2)
                        {
                            mItemResumen.Rubro = "% Supervisores de Supervisores sobre venta sin IVA";
                            mItemResumen.Porcentaje = (decimal)(mTotalComisionSuprvsrTipo2 / mTotalVentasTiendaSinIVA) * 100;
                        }

                        ii++;
                        mRetorna.SupervisoresResumen.Add(mItemResumen);
                    }

                    Clases.Info mItemInfo2 = new Clases.Info();
                    mItemInfo2.Exito = true;
                    mItemInfo2.FechaInicial = mFechaInicial;
                    mItemInfo2.FechaFinal = mFechaFinal;
                    mItemInfo2.Mensaje = "Comisiones generadas exitosamente.";
                    mItemInfo2.YaGrabadas = false;
                    mRetorna.Info.Add(mItemInfo2);

                    //obteniendo tuplas de jefes y supervisores por tienda
                    #region TuplasJefesSupervisores
                    Clases.RetornaComisiones comisiones = new ComisionesController().GetComisiones(anio1, mes1, dia1, anio2, mes2, dia2, "", "");

                    List<Clases.MontosJefesSupervisores> jefesupervisor = comisiones.Facturas
                        .Join(db.MF_Cobrador, f => f.Tienda, mc => mc.COBRADOR, (f, mc) => new { f, mc })
                        .GroupBy(x => new
                        {
                            x.f.Tienda,
                            CodigoJefe = x.f.CodigoJefe ?? x.mc.CODIGO_JEFE,
                            CodigoSupervisor = x.f.CodigoSupervisor ?? x.mc.CODIGO_SUPERVISOR
                        })
                        .Select(x => new Clases.MontosJefesSupervisores()
                        {
                            Tienda = x.Key.Tienda,
                            CodigoJefe = x.Key.CodigoJefe,
                            CodigoSupervisor = x.Key.CodigoSupervisor,
                            ValorFactura = x.Sum(y => y.f.Valor),
                            MontoComisionable = x.Sum(y => y.f.MontoComisionable),
                            ValorNeto = x.Sum(y => y.f.ValorNeto)
                        })
                        .OrderBy(x => x.Tienda)
                        .ToList();
                    #endregion

                    //desglose de comisiones por jefe y supervisor
                    #region DesgloseComisiones
                    mRetorna.JefeSupervisorDetalle = jefesupervisor
                        .Join(mRetorna.ComisionesJefesSupervisores, jsr => jsr.Tienda, js => js.Tienda,
                            (jsr, js) => new { jsr, js })
                        .GroupJoin(db.VENDEDOR, q1 => q1.jsr.CodigoSupervisor, s => s.VENDEDOR1,
                            (q1, s) => new { q1, s = s.DefaultIfEmpty() })
                        .GroupJoin(db.VENDEDOR, q2 => q2.q1.jsr.CodigoJefe, j => j.VENDEDOR1,
                            (q2, j) => new { q2, j = j.DefaultIfEmpty() })
                        .Select(x => new Clases.ComisionesJefesSupervisoresDetalle()
                        {
                            CodigoSupervisor = x.q2.q1.jsr.CodigoSupervisor ?? x.q2.q1.js.CodigoSupervisor,
                            NombreSupervisor = x.q2.s.FirstOrDefault() == null ? x.q2.q1.js.Supervisor : x.q2.s.FirstOrDefault()?.NOMBRE,
                            Categoria = x.q2.q1.js.Categoria,
                            Tienda = x.q2.q1.js.Tienda,
                            Ubicacion = x.q2.q1.js.Ubicacion,
                            CodigoJefe = x.q2.q1.jsr.CodigoJefe ?? x.q2.q1.js.CodigoJefe,
                            NombreJefe = x.j.FirstOrDefault() == null ? x.q2.q1.js.Jefe : x.j.FirstOrDefault()?.NOMBRE,
                            VentasSinIVA = Math.Round(x.q2.q1.js.VentasSinIVA * Math.Round(x.q2.q1.jsr.ValorNeto / x.q2.q1.js.VentasTiendaSinIVA, 2, MidpointRounding.AwayFromZero), 2, MidpointRounding.AwayFromZero),
                            Comision = Math.Round(x.q2.q1.js.Comision * Math.Round(x.q2.q1.jsr.ValorNeto / x.q2.q1.js.VentasTiendaSinIVA, 2, MidpointRounding.AwayFromZero), 2, MidpointRounding.AwayFromZero),
                            VentasTiendaSinIVA = Math.Round(x.q2.q1.js.VentasTiendaSinIVA * Math.Round(x.q2.q1.jsr.ValorNeto / x.q2.q1.js.VentasTiendaSinIVA, 2, MidpointRounding.AwayFromZero), 2, MidpointRounding.AwayFromZero),
                            Minimo = x.q2.q1.js.Minimo,
                            Meta = x.q2.q1.js.Meta,
                            ResultadoTienda = x.q2.q1.js.ResultadoTienda,
                            VentaComisionarJefe = Math.Round(x.q2.q1.js.VentaComisionarJefe * Math.Round(x.q2.q1.jsr.ValorNeto / x.q2.q1.js.VentasTiendaSinIVA, 2, MidpointRounding.AwayFromZero), 2, MidpointRounding.AwayFromZero),
                            PctDividido = Math.Round(x.q2.q1.jsr.ValorNeto / x.q2.q1.js.VentasTiendaSinIVA, 2, MidpointRounding.AwayFromZero),
                            PctTiendaJefe = x.q2.q1.js.PctjJefe,
                            ComisionJefe = Math.Round(x.q2.q1.js.ComisionJefe * Math.Round(x.q2.q1.jsr.ValorNeto / x.q2.q1.js.VentasTiendaSinIVA, 2, MidpointRounding.AwayFromZero), 2, MidpointRounding.AwayFromZero),
                            ComisionJefeTotal = Math.Round(x.q2.q1.js.ComisionJefeTotal * Math.Round(x.q2.q1.jsr.ValorNeto / x.q2.q1.js.VentasTiendaSinIVA, 2, MidpointRounding.AwayFromZero), 2, MidpointRounding.AwayFromZero),
                            PctTiendaS1 = x.q2.q1.js.PctjSupervisor,
                            ComisionS1 = Math.Round(x.q2.q1.js.ComisionSupervisor * Math.Round(x.q2.q1.jsr.ValorNeto / x.q2.q1.js.VentasTiendaSinIVA, 2, MidpointRounding.AwayFromZero), 2, MidpointRounding.AwayFromZero),
                            PctTiendaS2 = x.q2.q1.js.PctjSupervisor2,
                            ComisionS2 = Math.Round(x.q2.q1.js.ComisionSupervisor2 * Math.Round(x.q2.q1.jsr.ValorNeto / x.q2.q1.js.VentasTiendaSinIVA, 2, MidpointRounding.AwayFromZero), 2, MidpointRounding.AwayFromZero),
                        }).ToList();
                    #endregion

                    //detalle comisiones jefe
                    #region DetalleComisionJefe
                    mRetorna.DetalleComisionesJefe = mRetorna.JefeSupervisorDetalle
                        .GroupBy(x => new { x.Categoria, x.Tienda, x.Ubicacion, x.CodigoJefe, x.NombreJefe })
                        .Select(x => new Clases.DetalleComisionJefe()
                        {
                            Categoria = x.Key.Categoria,
                            Tienda = x.Key.Tienda,
                            Ubicacion = x.Key.Ubicacion,
                            CodigoJefe = x.Key.CodigoJefe,
                            NombreJefe = x.Key.NombreJefe,
                            PctDividido = x.Sum(y => y.PctDividido),
                            PctTiendaJefe = x.Sum(y => y.PctTiendaJefe),
                            ComisionJefe = x.Sum(y => y.ComisionJefe),
                            ComisionJefeTotal = x.Sum(y => y.ComisionJefeTotal)
                        })
                        .OrderBy(x => x.Tienda)
                        .ToList();
                    #endregion

                    //resumen comisiones jefe
                    #region ResumenComisionJefe
                    mRetorna.ResumenJefe = mRetorna.DetalleComisionesJefe
                        .GroupBy(x => new { x.CodigoJefe, x.NombreJefe })
                        .Select(x => new Clases.ResumenComisionJefe()
                        {
                            CodigoJefe = x.Key.CodigoJefe,
                            NombreJefe = x.Key.NombreJefe,
                            Comision = x.Sum(y => y.ComisionJefeTotal)
                        })
                        .ToList();
                    #endregion

                    //detalle comisiones supervisor
                    #region DetalleComisionSupervisor
                    mRetorna.DetalleComisionesSupervisor = mRetorna.JefeSupervisorDetalle
                        .GroupBy(x => new { x.Categoria, x.Tienda, x.Ubicacion, x.CodigoSupervisor, x.NombreSupervisor })
                        .Select(x => new Clases.DetalleComisionSupervisor()
                        {
                            Categoria = x.Key.Categoria,
                            Tienda = x.Key.Tienda,
                            Ubicacion = x.Key.Ubicacion,
                            CodigoSupervisor = x.Key.CodigoSupervisor,
                            NombreSupervisor = x.Key.NombreSupervisor,
                            PctDividido = x.Sum(y => y.PctDividido),
                            PctTiendaS1 = x.Sum(y => y.PctTiendaS1),
                            ComisionS1 = x.Sum(y => y.ComisionS1),
                            PctTiendaS2 = x.Sum(y => y.PctTiendaS2),
                            ComisionS2 = x.Sum(y => y.ComisionS2)
                        })
                        .OrderBy(x => x.Tienda)
                        .ToList();
                    #endregion

                    //resumen comisiones supervisor
                    #region ResumenComisionSupervisor
                    mRetorna.ResumenSupervisor = mRetorna.DetalleComisionesSupervisor
                        .GroupBy(x => new { x.CodigoSupervisor, x.NombreSupervisor })
                        .Select(x => new Clases.ResumenComisionSupervisor()
                        {
                            CodigoSupervisor = x.Key.CodigoSupervisor,
                            NombreSupervisor = x.Key.NombreSupervisor,
                            Comision = x.Sum(y => y.ComisionS1)
                        })
                        .ToList();
                    #endregion

                    //cálculo supervisor 2
                    #region CalculoSupervisor2
                    Clases.ResumenComisionSupervisor supervisor2 = db.MF_Catalogo
                        .Join(db.VENDEDOR, mc => mc.CODIGO_CAT, v => v.VENDEDOR1, (mc, v) => new { mc, v })
                        .Where(x => x.mc.CODIGO_TABLA == 47 && x.mc.NOMBRE_CAT == "S2")
                        .Select(x => new Clases.ResumenComisionSupervisor()
                        {
                            CodigoSupervisor = x.v.VENDEDOR1,
                            NombreSupervisor = x.v.NOMBRE,
                            Comision = 0,
                            Titulo = null,
                            Porcentaje = 0
                        })
                        .FirstOrDefault();
                    supervisor2.Comision = mRetorna.DetalleComisionesSupervisor.Sum(x => x.ComisionS2);
                    mRetorna.ResumenSupervisor.Add(supervisor2);

                    mRetorna.ResumenJefe = mRetorna.ResumenJefe.OrderBy(x => x.NombreJefe).ToList();
                    mRetorna.ResumenSupervisor = mRetorna.ResumenSupervisor.OrderBy(x => x.NombreSupervisor).ToList();

                    decimal ventasTiendasSinIva = mRetorna.JefeSupervisorDetalle.Sum(x => x.VentasTiendaSinIVA),
                        comisionJefe = mRetorna.JefeSupervisorDetalle.Sum(x => x.ComisionJefeTotal),
                        comisionS1 = mRetorna.JefeSupervisorDetalle.Sum(x => x.ComisionS1),
                        comisionS2 = mRetorna.JefeSupervisorDetalle.Sum(x => x.ComisionS2);

                    decimal pctJefe = Math.Round((comisionJefe / ventasTiendasSinIva) * 100, 2, MidpointRounding.AwayFromZero),
                        pctS1 = Math.Round(((comisionS1 + comisionS2) / ventasTiendasSinIva) * 100, 2, MidpointRounding.AwayFromZero),
                        pctS2 = Math.Round((comisionS2 / ventasTiendasSinIva) * 100, 2, MidpointRounding.AwayFromZero);

                    mRetorna.ResumenSupervisor[0].Titulo = "% Jefes sobre venta sin IVA";
                    mRetorna.ResumenSupervisor[0].Porcentaje = pctJefe;
                    mRetorna.ResumenSupervisor[1].Titulo = "% Supervisores sobre venta sin IVA";
                    mRetorna.ResumenSupervisor[1].Porcentaje = pctS1;
                    mRetorna.ResumenSupervisor[2].Titulo = "% Supervisores de Supervisores sobre venta sin IVA";
                    mRetorna.ResumenSupervisor[2].Porcentaje = pctS2;
                    #endregion
                }


                //Revisión de un jefe en varias tiendas
                List<Clases.ComisionesJefesSupervisores> mJefes = new List<Clases.ComisionesJefesSupervisores>();
                mJefes = mRetorna.ComisionesJefesSupervisores;
                var qJefes = from j in mJefes orderby j.VentasTiendaSinIVA descending select j;

                foreach (var item in qJefes)
                {
                    if (mRetorna.ComisionesJefesSupervisores.Where(x => x.CodigoJefe == item.CodigoJefe && x.Agrupado == "N").Count() > 1)
                    {

                        var qJefe = from j in mRetorna.ComisionesJefesSupervisores where j.CodigoJefe == item.CodigoJefe select j;
                        foreach (var jefe in qJefe)
                        {
                            jefe.Agrupado = "S";
                        }

                        foreach (var jefe in mRetorna.ComisionesJefesSupervisores)
                        {
                            if (jefe.CodigoJefe == item.CodigoJefe && jefe.Tienda != item.Tienda)
                            {
                                jefe.VentasSinIVA = 0;
                                jefe.Comision = 0;
                                jefe.ComisionJefeTotal = jefe.ComisionJefe;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                string mMensajeError = string.Format("{0} {1}", CatchClass.ExMessage(ex, "ComisionesJefesSupervisoresController", "GetComisionesJefesSupervisores"), mInfo);

                Clases.Info mItemInfo = new Clases.Info();
                mItemInfo.Exito = false;
                mItemInfo.Mensaje = mMensajeError;
                mItemInfo.YaGrabadas = false;
                mRetorna.Info.Add(mItemInfo);

                return mRetorna;
            }

            return mRetorna;
        }

    }
}
