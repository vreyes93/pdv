﻿using System;
using System.Linq;
using System.Web.Http;
using FiestaNETRestServices.Models;
using MF_Clases;

namespace FiestaNETRestServices.Controllers.Comisiones
{
    public class FechasComisionesController : ApiController
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET api/fechascomisiones
        public FechaComisionesRespuesta GetFechas()
        {
            FechaComisionesRespuesta mRetorna = new FechaComisionesRespuesta();

            EXACTUSERPEntities db = new EXACTUSERPEntities();
            db.CommandTimeout = 999999999;

            try
            {
                log.Info("Inicio de validación de fechas");
                DateTime mFechaInicial = DateTime.Now;
                DateTime mFechaFinal = DateTime.Now;
                DateTime fechatemp = DateTime.Today;

                Int16 mRestarInicial = -1;
                Int16 mRestarFinal = 0;

                if (fechatemp.Day < 10)
                {
                    mRestarInicial = -2;
                    mRestarFinal = -1;
                    fechatemp = fechatemp.AddDays(-fechatemp.Day);
                }
                
                //El calculo de las fechas se deja igual para fechas anteriores al ano 2018 y se realiza el calculo para las fechas posteriores al 2017
                if (fechatemp.Year < 2018)
                {

                    mFechaInicial = DateTime.Now.Date.AddMonths(mRestarInicial);
                    mFechaFinal = DateTime.Now.Date.AddMonths(mRestarFinal);
                    mFechaInicial = new DateTime(mFechaInicial.Year, mFechaInicial.Month, 26);
                    mFechaFinal = new DateTime(mFechaFinal.Year, mFechaFinal.Month, 25);
                    if ((from c in db.MF_Comision where c.FECHA_INICIAL == mFechaInicial && c.FECHA_FINAL == mFechaFinal select c).Count() > 0)
                    {
                        mFechaInicial = mFechaFinal.AddDays(1);
                        mFechaFinal = mFechaFinal.AddMonths(1);
                        mFechaFinal = new DateTime(mFechaFinal.Year, mFechaFinal.Month, 25);
                    }
                }
                else
                {
                    mFechaInicial = new DateTime(fechatemp.Year, fechatemp.Month, 1);
                    mFechaFinal = mFechaInicial.AddMonths(1);
                    mFechaFinal = new DateTime(mFechaFinal.Year, mFechaFinal.Month, 1).AddDays(-1);
                }

               
                mRetorna.FechaInicial = mFechaInicial;
                mRetorna.FechaFinal = mFechaFinal;

                string mMes = "Enero";
                if (mRetorna.FechaFinal.Month == 2) mMes = "Febrero";
                if (mRetorna.FechaFinal.Month == 3) mMes = "Marzo";
                if (mRetorna.FechaFinal.Month == 4) mMes = "Abril";
                if (mRetorna.FechaFinal.Month == 5) mMes = "Mayo";
                if (mRetorna.FechaFinal.Month == 6) mMes = "Junio";
                if (mRetorna.FechaFinal.Month == 7) mMes = "Julio";
                if (mRetorna.FechaFinal.Month == 8) mMes = "Agosto";
                if (mRetorna.FechaFinal.Month == 9) mMes = "Septiembre";
                if (mRetorna.FechaFinal.Month == 10) mMes = "Octubre";
                if (mRetorna.FechaFinal.Month == 11) mMes = "Noviembre";
                if (mRetorna.FechaFinal.Month == 12) mMes = "Diciembre";

                mRetorna.exito = true;
                mRetorna.mensaje = string.Format("Comisiones correspondientes al mes de {0} {1}.", mMes, mRetorna.FechaFinal.Year.ToString());
                log.Info("Fechas de comisiones generadas exitosamente.");
            }
            catch (Exception ex)
            {
                mRetorna.exito = false;
                mRetorna.mensaje = CatchClass.ExMessage(ex, "FechasComisionesController", "GetFechas");
            }

            return mRetorna;
        }
    }
}
