﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Transactions;
using System.Web.Http;
using FiestaNETRestServices.Models;
using MF_Clases;

namespace FiestaNETRestServices.Controllers.Comisiones
{
    public class GrabarComisionesController : ApiController
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public class Retorna
        {
            public bool exito { get; set; }
            public string mensaje { get; set; }
        }

        // POST api/grabarcomisiones
        public Retorna PostComisiones([FromBody]Clases.RetornaComisiones comisiones)
        {
            Retorna mRetorna = new Retorna();
            string mUsuario = comisiones.Info[0].Usuario;
            DateTime mFechaInicial = comisiones.Info[0].FechaInicial; DateTime mFechaFinal = comisiones.Info[0].FechaFinal;

            using (EXACTUSERPEntities db = new EXACTUSERPEntities())
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    try
                    {
                        db.CommandTimeout = 999999999;

                        List<MF_Comision> mComisionBorrar = db.MF_Comision.Where(x => x.FECHA_INICIAL == mFechaInicial && x.FECHA_FINAL == mFechaFinal).ToList();
                        foreach (MF_Comision item in mComisionBorrar) db.MF_Comision.DeleteObject(item);

                        List<MF_ComisionTienda> mTiendaBorrar = db.MF_ComisionTienda.Where(x => x.FECHA_INICIAL == mFechaInicial && x.FECHA_FINAL == mFechaFinal).ToList();
                        foreach (MF_ComisionTienda item in mTiendaBorrar) db.MF_ComisionTienda.DeleteObject(item);

                        List<MF_ComisionFactura> mFacturaBorrar = db.MF_ComisionFactura.Where(x => x.FECHA_INICIAL == mFechaInicial && x.FECHA_FINAL == mFechaFinal).ToList();
                        foreach (MF_ComisionFactura item in mFacturaBorrar) db.MF_ComisionFactura.DeleteObject(item);

                        List<MF_ComisionExcepcionCalculo> mExcepcionBorrar = db.MF_ComisionExcepcionCalculo.Where(x => x.FECHA_INICIAL == mFechaInicial && x.FECHA_FINAL == mFechaFinal).ToList();
                        foreach (MF_ComisionExcepcionCalculo item in mExcepcionBorrar) db.MF_ComisionExcepcionCalculo.DeleteObject(item);

                        List<MF_ComisionAnulada> mAnuladaBorrar = db.MF_ComisionAnulada.Where(x => x.FECHA_INICIAL == mFechaInicial && x.FECHA_FINAL == mFechaFinal).ToList();
                        foreach (MF_ComisionAnulada item in mAnuladaBorrar) db.MF_ComisionAnulada.DeleteObject(item);

                        List<MF_ComisionPendiente> mPendienteBorrar = db.MF_ComisionPendiente.Where(x => x.FECHA_INICIAL == mFechaInicial && x.FECHA_FINAL == mFechaFinal).ToList();
                        foreach (MF_ComisionPendiente item in mPendienteBorrar) db.MF_ComisionPendiente.DeleteObject(item);
                        db.SaveChanges();

                        foreach (var item in comisiones.Comisiones)
                        {
                            MF_Comision iComision = new MF_Comision
                            {
                                FECHA_INICIAL = mFechaInicial,
                                FECHA_FINAL = mFechaFinal,
                                COBRADOR = item.Tienda,
                                VENDEDOR = item.Vendedor,
                                VALOR = item.Valor,
                                MONTO_COMISIONABLE = item.MontoComisionable,
                                VALOR_NETO = item.ValorNeto,
                                COMISION = item.Comision,
                                PORCENTAJE = item.Porcentaje,
                                RecordDate = DateTime.Now,
                                CreatedBy = string.Format("FA/{0}", mUsuario),
                                UpdatedBy = string.Format("FA/{0}", mUsuario),
                                CreateDate = DateTime.Now,
                                TOOLTIP = item.Tooltip
                            };
                            db.MF_Comision.AddObject(iComision);
                        }
                        db.SaveChanges();

                        foreach (var item in comisiones.Tiendas)
                        {
                            MF_ComisionTienda iComisionTienda = new MF_ComisionTienda
                            {
                                FECHA_INICIAL = mFechaInicial,
                                FECHA_FINAL = mFechaFinal,
                                COBRADOR = item.Tienda,
                                VALOR = item.Valor,
                                MONTO_COMISIONABLE = item.MontoComisionable,
                                VALOR_NETO = item.ValorNeto,
                                COMISION = item.Comision,
                                RecordDate = DateTime.Now,
                                CreatedBy = string.Format("FA/{0}", mUsuario),
                                UpdatedBy = string.Format("FA/{0}", mUsuario),
                                CreateDate = DateTime.Now,
                            };
                            db.MF_ComisionTienda.AddObject(iComisionTienda);
                        }
                        db.SaveChanges();

                        foreach (var item in comisiones.Facturas)
                        {
                            MF_ComisionFactura iComisionFactura = new MF_ComisionFactura
                            {
                                FECHA_INICIAL = mFechaInicial,
                                FECHA_FINAL = mFechaFinal,
                                COBRADOR = item.Tienda,
                                VENDEDOR = item.CodigoVendedor,
                                FACTURA = item.Factura,
                                VALOR = item.Valor,
                                MONTO_COMISIONABLE = item.MontoComisionable,
                                VALOR_NETO = item.ValorNeto,
                                COMISION = item.Comision,
                                PORCENTAJE = item.Porcentaje,
                                FECHA_FACTURA = item.FechaFactura,
                                FECHA_ENTREGA = null,
                                NIVEL_PRECIO = item.NivelPrecio,
                                PORCENTAJE_TARJETA = item.Comision,
                                ENVIO = item.Envio,
                                RecordDate = DateTime.Now,
                                CreatedBy = string.Format("FA/{0}", mUsuario),
                                UpdatedBy = string.Format("FA/{0}", mUsuario),
                                CreateDate = DateTime.Now,
                            };
                            db.MF_ComisionFactura.AddObject(iComisionFactura);
                        }
                        db.SaveChanges();

                        foreach (var item in comisiones.Anuladas)
                        {
                            MF_ComisionAnulada iComisionAnulada = new MF_ComisionAnulada
                            {
                                FECHA_INICIAL = mFechaInicial,
                                FECHA_FINAL = mFechaFinal,
                                COBRADOR = item.Tienda,
                                VENDEDOR = item.Vendedor,
                                FACTURA = item.Factura,
                                VALOR = item.Valor,
                                FECHA_FACTURA = item.FechaFactura,
                                RecordDate = DateTime.Now,
                                CreatedBy = string.Format("FA/{0}", mUsuario),
                                UpdatedBy = string.Format("FA/{0}", mUsuario),
                                CreateDate = DateTime.Now,
                            };
                            db.MF_ComisionAnulada.AddObject(iComisionAnulada);
                        }
                        db.SaveChanges();

                        foreach (var item in comisiones.Excepciones)
                        {
                            DateTime mFechaEntrega = DateTime.Now; bool mFechaEntregaValida = true;
                            try
                            {
                                mFechaEntrega = Convert.ToDateTime(item.FechaEntrega);
                                if (mFechaEntrega.Year == 1) mFechaEntregaValida = false;
                            }
                            catch
                            {
                                mFechaEntregaValida = false;
                            }

                            if (mFechaEntregaValida)
                            {
                                MF_ComisionExcepcionCalculo iComisionExcepcionCalculo = new MF_ComisionExcepcionCalculo
                                {
                                    FECHA_INICIAL = mFechaInicial,
                                    FECHA_FINAL = mFechaFinal,
                                    COBRADOR = item.Tienda,
                                    VENDEDOR = item.Vendedor,
                                    FACTURA = item.Factura,
                                    VALOR = item.Valor,
                                    MONTO_COMISIONABLE = item.MontoComisionable,
                                    VALOR_NETO = item.ValorNeto,
                                    TIPO = item.Tipo,
                                    FECHA_FACTURA = item.FechaFactura,
                                    FECHA_ENTREGA = mFechaEntrega,
                                    RecordDate = DateTime.Now,
                                    CreatedBy = string.Format("FA/{0}", mUsuario),
                                    UpdatedBy = string.Format("FA/{0}", mUsuario),
                                    CreateDate = DateTime.Now,
                                };
                                db.MF_ComisionExcepcionCalculo.AddObject(iComisionExcepcionCalculo);
                            }
                            else
                            {
                                MF_ComisionExcepcionCalculo iComisionExcepcionCalculo = new MF_ComisionExcepcionCalculo
                                {
                                    FECHA_INICIAL = mFechaInicial,
                                    FECHA_FINAL = mFechaFinal,
                                    COBRADOR = item.Tienda,
                                    VENDEDOR = item.Vendedor,
                                    FACTURA = item.Factura,
                                    VALOR = item.Valor,
                                    MONTO_COMISIONABLE = item.MontoComisionable,
                                    VALOR_NETO = item.ValorNeto,
                                    TIPO = item.Tipo,
                                    FECHA_FACTURA = item.FechaFactura,
                                    RecordDate = DateTime.Now,
                                    CreatedBy = string.Format("FA/{0}", mUsuario),
                                    UpdatedBy = string.Format("FA/{0}", mUsuario),
                                    CreateDate = DateTime.Now,
                                };
                                db.MF_ComisionExcepcionCalculo.AddObject(iComisionExcepcionCalculo);
                            }
                        }
                        db.SaveChanges();

                        foreach (var item in comisiones.Pendientes)
                        {
                            DateTime mFechaEntrega = DateTime.Now; bool mFechaEntregaValida = true;
                            try
                            {
                                mFechaEntrega = Convert.ToDateTime(item.FechaEntrega);
                                if (mFechaEntrega.Year == 1) mFechaEntregaValida = false;
                            }
                            catch
                            {
                                mFechaEntregaValida = false;
                            }

                            if (mFechaEntregaValida)
                            {
                                MF_ComisionPendiente iComisionPendiente = new MF_ComisionPendiente
                                {
                                    FECHA_INICIAL = mFechaInicial,
                                    FECHA_FINAL = mFechaFinal,
                                    COBRADOR = item.Tienda,
                                    VENDEDOR = item.Vendedor,
                                    FACTURA = item.Factura,
                                    VALOR = item.Valor,
                                    MONTO_COMISIONABLE = item.MontoComisionable,
                                    VALOR_NETO = item.ValorNeto,
                                    FECHA_FACTURA = item.FechaFactura,
                                    FECHA_ENTREGA = mFechaEntrega,
                                    ESTA_DESPACHADO = item.EstaDespachado,
                                    RecordDate = DateTime.Now,
                                    CreatedBy = string.Format("FA/{0}", mUsuario),
                                    UpdatedBy = string.Format("FA/{0}", mUsuario),
                                    CreateDate = DateTime.Now,
                                };
                                db.MF_ComisionPendiente.AddObject(iComisionPendiente);
                            }
                            else
                            {
                                MF_ComisionPendiente iComisionPendiente = new MF_ComisionPendiente
                                {
                                    FECHA_INICIAL = mFechaInicial,
                                    FECHA_FINAL = mFechaFinal,
                                    COBRADOR = item.Tienda,
                                    VENDEDOR = item.Vendedor,
                                    FACTURA = item.Factura,
                                    VALOR = item.Valor,
                                    MONTO_COMISIONABLE = item.MontoComisionable,
                                    VALOR_NETO = item.ValorNeto,
                                    FECHA_FACTURA = item.FechaFactura,
                                    RecordDate = DateTime.Now,
                                    CreatedBy = string.Format("FA/{0}", mUsuario),
                                    UpdatedBy = string.Format("FA/{0}", mUsuario),
                                    CreateDate = DateTime.Now,
                                };
                                db.MF_ComisionPendiente.AddObject(iComisionPendiente);
                            }
                        }

                        db.SaveChanges();
                        transactionScope.Complete();
                        mRetorna.exito = true;
                        mRetorna.mensaje = "Comisiones grabadas exitosamente.";
                    }
                    catch (Exception ex)
                    {
                        transactionScope.Dispose();
                        mRetorna.exito = false;
                        mRetorna.mensaje = CatchClass.ExMessage(ex, "GrabarComisionesController", "PostComisiones");
                    }

                    return mRetorna;
                }
            }
        }
    }
}
