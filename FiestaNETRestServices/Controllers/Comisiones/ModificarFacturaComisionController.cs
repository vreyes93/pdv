﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;
using MF_Clases;


namespace FiestaNETRestServices.Controllers.Comisiones
{
    public class ModificarFacturaComisionController : ApiController
    {
        // POST api/modificarfacturacomision
        public Clases.RetornaExito PostModificarFactura([FromBody]Clases.FacturaModificaComision factura)
        {
            Clases.RetornaExito mRetorna = new Clases.RetornaExito();
            EXACTUSERPEntities db = new EXACTUSERPEntities();

            mRetorna.exito = false;
            db.CommandTimeout = 999999999;

            try
            {
                var qFactura = from f in db.FACTURA where f.FACTURA1 == factura.Factura select f;
                var qFacturaComision = from f in db.MF_ComisionFactura where f.FACTURA == factura.Factura select f;

                if (qFactura.Count() == 0)
                {
                    mRetorna.mensaje = "La factura ingresada no existe.";
                    return mRetorna;
                }

                if (qFactura.First().ANULADA == "S")
                {
                    mRetorna.mensaje = "La factura está anulada.";
                    return mRetorna;
                }

                string mCobrador = qFactura.First().COBRADOR;
                string mVendedor = qFactura.First().VENDEDOR;
                Int32 mCuantos = (from f in db.MF_ComisionExcepcion where f.FACTURA == factura.Factura select f).Count();

                if (factura.Tipo == "E" && mCuantos == 0)
                {
                    mRetorna.mensaje = "La factura no tiene excepción guardada";
                    return mRetorna;
                }

                if (qFacturaComision.Count() > 0)
                {
                    string mFechaComision = string.Format("{0}{1}/{2}{3}/{4}", qFacturaComision.First().FECHA_FINAL.Day < 10 ? "0" : "", qFacturaComision.First().FECHA_FINAL.Day.ToString(), qFacturaComision.First().FECHA_FINAL.Month < 10 ? "0" : "", qFacturaComision.First().FECHA_FINAL.Month.ToString(), qFacturaComision.First().FECHA_FINAL.Year.ToString());
                    mRetorna.mensaje = string.Format("La factura {0} ya fue pagada el {1}.", factura.Factura, mFechaComision);
                    return mRetorna;
                }

                if (factura.Tipo == "P")
                {
                    Int16 mInicial = -1;
                    Int16 mFinal = 0;

                    if (DateTime.Now.Date.Day >= 26 && DateTime.Now.Day <= 31)
                    {
                        mInicial = 0;
                        mFinal = 1;
                    }

                    DateTime mFechaInicial = DateTime.Now.Date.AddMonths(mInicial);
                    DateTime mFechaFinal = DateTime.Now.Date.AddMonths(mFinal);

                    mFechaInicial = new DateTime(mFechaInicial.Year, mFechaInicial.Month, 26);
                    mFechaFinal = new DateTime(mFechaFinal.Year, mFechaFinal.Month, 25);

                    var qComisiones = from d in db.MF_Despacho
                                      join de in db.DESPACHO on d.DESPACHO equals de.DESPACHO1
                                      join f in db.FACTURA on d.FACTURA equals f.FACTURA1
                                      join v in db.VENDEDOR on f.VENDEDOR equals v.VENDEDOR1
                                      join mf in db.MF_Factura on d.FACTURA equals mf.FACTURA
                                      join n in db.MF_NIVEL_PRECIO_COEFICIENTE on mf.NIVEL_PRECIO equals n.NIVEL_PRECIO
                                      where d.FECHA_ENTREGA >= mFechaInicial && d.FECHA_ENTREGA <= mFechaFinal && de.ESTADO == "D" && f.ANULADA == "N" && f.COBRADOR != "F01" && d.ACEPTADO_CLIENTE == "S" && f.FACTURA1 == factura.Factura
                                      select f;

                    if (qComisiones.Count() > 0)
                    {
                        string mFechaComision = string.Format("{0}{1}/{2}{3}/{4}", mFechaFinal.Day < 10 ? "0" : "", mFechaFinal.Day.ToString(), mFechaFinal.Month < 10 ? "0" : "", mFechaFinal.Month.ToString(), mFechaFinal.Year.ToString());
                        mRetorna.mensaje = string.Format("La factura {0} califica para ser pagada el {1}.", factura.Factura, mFechaComision);
                        return mRetorna;
                    }

                    if (factura.MontoComision < 0)
                    {
                        mRetorna.mensaje = "El monto comisionable ingresado es inválido.";
                        return mRetorna;
                    }

                    if (factura.MontoComision > qFactura.First().TOTAL_FACTURA)
                    {
                        mRetorna.mensaje = "El monto comisionable no puede ser mayor al número de la factura.";
                        return mRetorna;
                    }
                }

                if (factura.Tipo == "C")
                {
                    if ((from c in db.MF_ComisionFacturaPagar where c.FACTURA == factura.Factura select c).Count() == 0)
                    {
                        mRetorna.mensaje = "La factura no se encuentra en comisiones.";
                        return mRetorna;
                    }
                }

                DateTime mFechaFinalComision = DateTime.Now.Date;
                DateTime mFechaInicialComision = DateTime.Now.Date;

                if (factura.Tipo == "P")
                {
                    mFechaFinalComision = new DateTime(Convert.ToInt32(factura.Anio), Convert.ToInt32(factura.Mes), 25);
                    mFechaInicialComision = mFechaFinalComision.AddMonths(-1);
                    mFechaInicialComision = mFechaInicialComision = new DateTime(mFechaInicialComision.Year, mFechaInicialComision.Month, 26);

                    if ((from c in db.MF_ComisionFactura where c.FECHA_INICIAL == mFechaInicialComision && c.FECHA_FINAL == mFechaFinalComision select c).Count() > 0)
                    {
                        mRetorna.mensaje = "Ese mes de comisiones ya está cerrado.";
                        return mRetorna;
                    }
                }

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    if (factura.Tipo == "P" || factura.Tipo == "C")
                    {
                        List<MF_ComisionFacturaPagar> mBorrar = db.MF_ComisionFacturaPagar.Where(x => x.FACTURA == factura.Factura).ToList();
                        foreach (MF_ComisionFacturaPagar item in mBorrar) db.MF_ComisionFacturaPagar.DeleteObject(item);

                        if (factura.Tipo == "P")
                        {
                            MF_ComisionFacturaPagar iComisionFacturaPagar = new MF_ComisionFacturaPagar
                            {
                                FECHA_INICIAL = mFechaInicialComision,
                                FECHA_FINAL = mFechaFinalComision,
                                COBRADOR = qFactura.First().COBRADOR,
                                VENDEDOR = qFactura.First().VENDEDOR,
                                FACTURA = factura.Factura,
                                VALOR = qFactura.First().TOTAL_FACTURA,
                                MONTO_COMISIONABLE = factura.MontoComision,
                                FECHA_FACTURA = qFactura.First().FECHA,
                                RecordDate = DateTime.Now,
                                CreatedBy = string.Format("FA/{0}", factura.Usuario),
                                UpdatedBy = string.Format("FA/{0}", factura.Usuario),
                                CreateDate = DateTime.Now,
                            };
                            db.MF_ComisionFacturaPagar.AddObject(iComisionFacturaPagar);

                            mRetorna.mensaje = string.Format("Factura {0} grabada para comisión exitosamente.", factura.Factura);
                        }
                        else
                        {
                            mRetorna.mensaje = string.Format("Factura {0} eliminada exitosamente.", factura.Factura);
                        }
                    }
                    else
                    {
                        List<MF_ComisionExcepcion> mBorrar = db.MF_ComisionExcepcion.Where(x => x.FACTURA == factura.Factura).ToList();
                        foreach (MF_ComisionExcepcion item in mBorrar) db.MF_ComisionExcepcion.DeleteObject(item);

                        if (factura.Tipo != "E")
                        {
                            MF_ComisionExcepcion iComision = new MF_ComisionExcepcion
                            {
                                FACTURA = factura.Factura,
                                TIPO = factura.Tipo,
                                MONTO_COMISION = factura.MontoComision,
                                COBRADOR = mCobrador,
                                VENDEDOR = mVendedor,
                                RecordDate = DateTime.Now,
                                CreatedBy = string.Format("FA/{0}", factura.Usuario),
                                UpdatedBy = string.Format("FA/{0}", factura.Usuario),
                                CreateDate = DateTime.Now
                            };

                            db.MF_ComisionExcepcion.AddObject(iComision);
                        }
                        
                        mRetorna.mensaje = string.Format("La excepción fue aplicada exitosamente.");
                        if (mCuantos > 0) mRetorna.mensaje = "Se eliminó la excepción anterior y se aplicó esta última.";
                        if (factura.Tipo == "E") mRetorna.mensaje = "La excepción fue eliminada exitosamente.";
                    }

                    db.SaveChanges();
                    transactionScope.Complete();
                }

                mRetorna.exito = true;
            }
            catch (Exception ex)
            {
                mRetorna.mensaje = CatchClass.ExMessage(ex, "ModificarFacturaComisionController", "PostModificarFactura");
            }

            return mRetorna;
        }
    }
}
