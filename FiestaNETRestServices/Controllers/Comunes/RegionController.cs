﻿using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Comunes
{
    //[RoutePrefix("api/Armados/Region")]
    public class RegionController : ApiController
    {

        /// <summary>
        /// Esta función tiene como propósito obtener el listado de los tipos de servicios 
        /// para debuguear se llama asi en MV5: http://localhost:53874/api/Armados/TipoServicio
        /// para debuguear en MV4: http://localhost:53874/api/TipoServicio
        /// </summary>
        /// <param name="mUsuario">.</param>
        /// <returns>Retorna un IHttpActionResult Ok((List<FriedmanResumenTrimestralDto>) o BadRequest(Mensaje de respuesta con el mensaje de error.), . </returns>
        //-----------------------------------------------------------------------------------------------------------------------
        #region Get
        [HttpGet]
        public IHttpActionResult Get(byte id)
        {
            try
            {
                DALComunes mComunes = new DALComunes();
                Respuesta mRespuesta = mComunes.ObtenerRegion(id);
                if (mRespuesta.Exito)
                    return Ok(mRespuesta.Objeto);

                return BadRequest(mRespuesta.Mensaje);

            }
            catch
            {
                return BadRequest("Excepcion, favor de comunicarse con Informática.");
            }
        }
        #endregion

    }
}
