﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;
using System.Xml;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;
using MF_Clases;
using System.Web.Configuration;

namespace FiestaNETRestServices.Controllers
{
    public class ConsultaInterconsumoController : ApiController
    {
        
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public class Retorna
        {
            public bool exito { get; set; }
            public string mensaje { get; set; }
            public string solicitud { get; set; }
            public string autorizacion { get; set; }
            public string resultadoSolicitud { get; set; }
            public string quienAutorizo { get; set; }
            public string mensaje2 { get; set; }
            public string nombre { get; set; }
        }

        // GET api/consultainterconsumo
        public Retorna GetString(string documento)
        {
            Retorna retorna = new Retorna();
            EXACTUSERPEntities db = new EXACTUSERPEntities();

            retorna.exito = true;
            string mInfoError = string.Format(" - Pedido: {0}", documento);
            
            try
            {
                log.Info("Inicio de validaciones.");

                string tipo = "C";
                if (documento.Substring(0, 1) == "P") tipo = "P";

                if (documento == "P000000")
                {
                    retorna.exito = false;
                    retorna.mensaje = "Debe seleccionar un pedido.";
                    return retorna;
                }

                if (documento == "000000")
                {
                    retorna.exito = false;
                    retorna.mensaje = "Debe seleccionar una cotización.";
                    return retorna;
                }

                if (documento == "0")
                {
                    retorna.exito = false;
                    retorna.mensaje = "Debe enviar la solicitud a Interconsumo antes de consultarla.";
                    return retorna;
                }

                Int32 mCotizacion = 0;
                if (tipo == "C") mCotizacion = Convert.ToInt32(documento);

                string mCliente = ""; string mSolicitud = ""; string mTienda = ""; string mAutorizacion = ""; decimal mFacturar = 0; decimal mEnganche = 0;

                if (tipo == "C")
                {
                    if ((from c in db.MF_Cotizacion where c.COTIZACION == mCotizacion select c).Count() == 0)
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("La cotización {0} no existe.", documento);
                        return retorna;
                    }

                    var qCotizacion = (from c in db.MF_Cotizacion where c.COTIZACION == mCotizacion select c).First();

                    if (qCotizacion.FINANCIERA != 7)
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("La cotización {0} no pertenece a Interconsumo.", documento);
                        return retorna;
                    }

                    try
                    {
                        mCliente = qCotizacion.CLIENTE;
                    }
                    catch
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("La cotización {0} debe tener cliente asignado.", documento);
                        return retorna;
                    }

                    if (mCliente == null)
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("La cotización {0} debe tener cliente asignado.", documento);
                        return retorna;
                    }

                    if (mCliente.Trim().Length == 0)
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("La cotización {0} debe tener cliente asignado.", documento);
                        return retorna;
                    }

                    if (qCotizacion.GRABADA_EN_LINEA == null)
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("La cotización {0} no fue grabada en línea.", documento);
                        return retorna;
                    }

                    if (qCotizacion.GRABADA_EN_LINEA == "N")
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("La cotización {0} no fue grabada en línea.", documento);
                        return retorna;
                    }

                    Int32 mDiasGracia = Convert.ToInt32((from c in db.MF_Configura select c).First().diasGraciaCotizacion);
                    DateTime mFechaVence = qCotizacion.FECHA_VENCE;

                    mFechaVence = mFechaVence.AddDays(mDiasGracia);

                    if (DateTime.Now.Date > mFechaVence)
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("Esta cotización ya está vencida y se terminaron los {0} dias de gracia autorizados, no es posible utilizarla.", mDiasGracia);
                        return retorna;
                    }

                    mFacturar = Convert.ToDecimal(qCotizacion.TOTAL_FACTURAR);
                    mTienda = qCotizacion.COBRADOR;
                    mSolicitud = qCotizacion.SOLICITUD.ToString();

                    mInfoError = string.Format(" - Cotización: {0}", documento);
                    retorna.mensaje = string.Format("La cotización {0} no tiene solicitud de crédito asignada.", documento);

                    mEnganche = Convert.ToDecimal(qCotizacion.ENGANCHE);
                }
                else
                {

                    if ((from p in db.MF_Pedido where p.PEDIDO == documento select p).Count() == 0)
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("El pedido {0} no existe.", documento);
                        return retorna;
                    }

                    var qPedido = (from p in db.MF_Pedido where p.PEDIDO == documento select p).First();

                    if (qPedido.FINANCIERA != 7)
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("El pedido {0} no pertenece a Interconsumo.", documento);
                        return retorna;
                    }
                    
                    if (qPedido.GRABADA_EN_LINEA == null)
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("El pedido {0} no fue grabado en línea.", documento);
                        return retorna;
                    }
                    
                    if (qPedido.GRABADA_EN_LINEA == "N")
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("El pedido {0} no fue grabado en línea.", documento);
                        return retorna;
                    }

                    if ((from f in db.FACTURA where f.PEDIDO == documento && f.ANULADA == "N" select f).Count() > 0)
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("El pedido {0} ya se encuentra facturado.", documento);
                        return retorna;
                    }

                    var qPedidoEx = (from p in db.PEDIDO where p.PEDIDO1 == documento select p).First();

                    mCliente = qPedidoEx.CLIENTE;
                    mFacturar = qPedidoEx.TOTAL_A_FACTURAR;
                    mTienda = qPedidoEx.COBRADOR;
                    mSolicitud = qPedido.SOLICITUD.ToString();
                    retorna.mensaje = string.Format("El pedido {0} no tiene solicitud de crédito asignada.", documento);

                    mEnganche = Convert.ToDecimal(qPedido.ENGANCHE);
                }

                if (mSolicitud == null) mSolicitud = "";
                if (mSolicitud.Trim().Length == 0) return retorna;
                retorna.mensaje = "";

                log.Info("Generando XML para consultar en Interconsumo.");

                
                

                string xmlData = "<solicitud>";
                xmlData += "<autenticacion>";
                xmlData += "<aplicacion>Muebles Fiesta</aplicacion>";
                xmlData += string.Format("<usuario><![CDATA[{0}]]></usuario>", WebConfigurationManager.AppSettings["UsrInterconsumo"]);
                xmlData += string.Format("<clave><![CDATA[{0}]]></clave>", WebConfigurationManager.AppSettings["PwdInterconsumo"] );
                xmlData += "</autenticacion>";
                xmlData += string.Format("<referencia>{0}</referencia>", mSolicitud);
                xmlData += string.Format("<sucursal>{0}</sucursal>", mTienda);
                xmlData += "<idcaja>1</idcaja>";
                xmlData += "</solicitud>";

                log.Info("Consumiendo WS de consulta de Interconsumo.");
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                FiestaNETRestServices.srvIntegracionInterConsumo.srvIntegracionInteconsumoPuente srvIntegra = new FiestaNETRestServices.srvIntegracionInterConsumo.srvIntegracionInteconsumoPuente();
                InterEncript.Encriptacion dllEncriptacion = new InterEncript.Encriptacion();

                string resultado = dllEncriptacion.Proceso(xmlData, "solicitud", "1", "xml");
                string respuesta = srvIntegra.EntradaXmlConsultaSolicitud(resultado);
                resultado = dllEncriptacion.Proceso(respuesta.Trim(), "", "2", "1");
                
                XmlDocument xmlRespuesta = new XmlDocument();

                xmlRespuesta.PreserveWhitespace = true;
                xmlRespuesta.LoadXml(resultado);

                log.Info("Leyendo respuesta de Interconsumo.");

                string mNombre = ""; decimal mValorSolicitud = 0; string mDescripcionError = ""; string mEstado = ""; string mPrimerNombre = ""; string mSegundoNombre = ""; string mTercerNombre = ""; string mPrimerApellido = ""; string mSegundoApellido = ""; string mApellidoCasada = "";

                Int32 mPlazo = 0; decimal mCuota = 0; decimal mUltimaCuota = 0;

                string mCodigoRespuesta = xmlRespuesta.SelectNodes("respuesta/codigo").Item(0).InnerText;

                try
                {
                    mNombre = xmlRespuesta.SelectNodes("respuesta/nombre").Item(0).InnerText;
                }
                catch
                {
                    mNombre = "";
                }

                try
                {
                    mDescripcionError = xmlRespuesta.SelectNodes("respuesta/descripcion").Item(0).InnerText;
                }
                catch
                {
                    mDescripcionError = "";
                }

                try
                {
                    mValorSolicitud = Convert.ToDecimal(xmlRespuesta.SelectNodes("respuesta/valor").Item(0).InnerText);
                }
                catch
                {
                    mValorSolicitud = 0;
                }

                try
                {
                    mAutorizacion = xmlRespuesta.SelectNodes("respuesta/autorizacion").Item(0).InnerText;
                }
                catch
                {
                    mAutorizacion = "";
                }

                try
                {
                    mEstado = xmlRespuesta.SelectNodes("respuesta/estado").Item(0).InnerText;
                }
                catch
                {
                    mEstado = "";
                }

                try
                {
                    mPrimerNombre = xmlRespuesta.SelectNodes("respuesta/primer_nombre").Item(0).InnerText;
                }
                catch
                {
                    mPrimerNombre = "";
                }

                try
                {
                    mSegundoNombre = xmlRespuesta.SelectNodes("respuesta/segundo_nombre").Item(0).InnerText;
                }
                catch
                {
                    mSegundoNombre = "";
                }

                try
                {
                    mTercerNombre = xmlRespuesta.SelectNodes("respuesta/tercer_nombre").Item(0).InnerText;
                }
                catch
                {
                    mTercerNombre = "";
                }

                try
                {
                    mPrimerApellido = xmlRespuesta.SelectNodes("respuesta/primer_apellido").Item(0).InnerText;
                }
                catch
                {
                    mPrimerApellido = "";
                }

                try
                {
                    mSegundoApellido = xmlRespuesta.SelectNodes("respuesta/segundo_apellido").Item(0).InnerText;
                }
                catch
                {
                    mSegundoApellido = "";
                }

                try
                {
                    mApellidoCasada = xmlRespuesta.SelectNodes("respuesta/apellido_casada").Item(0).InnerText;
                }
                catch
                {
                    mApellidoCasada = "";
                }



                try
                {
                    mPlazo = Convert.ToInt32(xmlRespuesta.SelectNodes("respuesta/plazo").Item(0).InnerText);
                }
                catch
                {
                    mPlazo = 0;
                }


                try
                {
                    mCuota = Convert.ToDecimal(xmlRespuesta.SelectNodes("respuesta/cuota").Item(0).InnerText);
                }
                catch
                {
                    mCuota = 0;
                }


                try
                {
                    mUltimaCuota = Convert.ToDecimal(xmlRespuesta.SelectNodes("respuesta/apellido_casada").Item(0).InnerText);
                }
                catch
                {
                    mUltimaCuota = 0;
                }




                string mRequisitos = "";
                try
                {
                    mRequisitos = xmlRespuesta.SelectNodes("respuesta/requisitos").Item(0).InnerText;
                }
                catch
                {
                    mRequisitos = "";
                }


                retorna.mensaje = "";
                retorna.autorizacion = mAutorizacion;
                retorna.quienAutorizo = "Autorización en línea";
                retorna.mensaje2 = string.Format("La solicitud fue APROBADA, proceda a facturar. {0}", mRequisitos);


                if (mAutorizacion.Trim().Length == 0)
                {
                    using (TransactionScope transactionScope = new TransactionScope())
                    {
                        if (tipo == "C")
                        {
                            var qUpdateCotizacion = from c in db.MF_Cotizacion where c.COTIZACION == mCotizacion select c;
                            foreach (var item in qUpdateCotizacion)
                            {
                                item.XML_CONSULTA = xmlData;
                                item.XML_CONSULTA_RESPUESTA = resultado;
                            }
                        }
                        else
                        {
                            var qUpdateMFPedido = from p in db.MF_Pedido where p.PEDIDO == documento select p;
                            foreach (var item in qUpdateMFPedido)
                            {
                                item.XML_CONSULTA = xmlData;
                                item.XML_CONSULTA_RESPUESTA = resultado;
                            }
                        }

                        db.SaveChanges();
                        transactionScope.Complete();
                    }
                }
                
                if (mCodigoRespuesta == "000")
                {
                    retorna.mensaje = "SOLICITUD APROBADA";
                    if (mAutorizacion.Trim().Length == 0)
                    {
                        if (mEstado.Trim().Length == 0)
                        {
                            retorna.exito = false;
                            retorna.mensaje = string.Format("La solicitud {0} no devolvió el número de autorización, puede ser porque no ha capturado el DPI, la huella y la fotografía del cliente en el portal. {1}", mSolicitud, mRequisitos);
                        }
                        else
                        {
                            retorna.exito = false;
                            retorna.mensaje = string.Format("La solicitud {0} se encuentra en estado {1}. {2}", mSolicitud, mEstado, mRequisitos);
                        }
                    }
                }
                else
                {
                    retorna.exito = false;
                    retorna.mensaje2 = "";
                    retorna.mensaje = string.Format("Error {0} {1} en la solicitud {2}, envíe este error por soporte.", mCodigoRespuesta, mDescripcionError, mSolicitud);
                    
                    if (mCodigoRespuesta == "092") retorna.mensaje = string.Format("La solicitud {0} no existe en Interconsumo.", mSolicitud);
                    if (mCodigoRespuesta == "094") retorna.mensaje = string.Format("La solicitud {0} no pertenece a Muebles Fiesta.", mSolicitud);
                    if (mCodigoRespuesta == "095") retorna.mensaje = string.Format("La solicitud {0} está PreAutorizada, Diferida o Rechazada. {1}", mSolicitud, mRequisitos);
                    if (mCodigoRespuesta == "096") retorna.mensaje = string.Format("La solicitud {0} está Autorizada, pero se ingresó hace más de 30 días. {1}", mSolicitud, mRequisitos);
                }

                if (Math.Round(mFacturar, 2, MidpointRounding.AwayFromZero) != Math.Round(mValorSolicitud + mEnganche, 2, MidpointRounding.AwayFromZero) && mValorSolicitud > 0)
                {
                    string mMensajeMonto = string.Format("La solicitud {0} fue aprobada por {1} y usted desea facturar {2}, no es posible continuar. {3} {4}", mSolicitud, String.Format("{0:0,0.00}", mValorSolicitud), String.Format("{0:0,0.00}", mFacturar), mValorSolicitud, mFacturar);

                    retorna.exito = false;
                    retorna.mensaje = mMensajeMonto;
                    log.Info(mMensajeMonto);
                }

                if (retorna.mensaje.Trim().Length > 0 && retorna.mensaje != "SOLICITUD APROBADA") return retorna;

                log.Info("Solicitud aprobada, se procede a grabar los datos en la cotización y pedido.");

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    if (tipo == "C")
                    {
                        var qUpdateCotizacion = from c in db.MF_Cotizacion where c.COTIZACION == mCotizacion select c;
                        foreach (var item in qUpdateCotizacion)
                        {
                            item.XML_CONSULTA = xmlData;
                            item.XML_CONSULTA_RESPUESTA = resultado;
                            item.AUTORIZACION_EN_LINEA = "S";
                            item.NOMBRE_AUTORIZACION = retorna.quienAutorizo;
                            item.AUTORIZACION_INTERCONSUMO = retorna.autorizacion;
                        }
                    }
                    else
                    {
                        var qUpdateMFPedido = from p in db.MF_Pedido where p.PEDIDO == documento select p;
                        foreach (var item in qUpdateMFPedido)
                        {
                            item.XML_CONSULTA = xmlData;
                            item.XML_CONSULTA_RESPUESTA = resultado;
                            item.AUTORIZACION_EN_LINEA = "S";
                            item.NOMBRE_AUTORIZACION = retorna.quienAutorizo;
                            item.AUTORIZACION = retorna.autorizacion;
                        }
                    }

                    if (mPrimerNombre.Trim().Length > 0)
                    {
                        mNombre = string.Format("{0}{1}{2}{3}{4}, {5}{6}{7}{8}{9}", mPrimerApellido, mSegundoApellido.Trim().Length == 0 ? "" : " ", mSegundoApellido, mApellidoCasada.Trim().Length == 0 ? "" : " ", mApellidoCasada, mPrimerNombre, mSegundoNombre.Trim().Length == 0 ? "" : " ", mSegundoNombre, mTercerNombre.Trim().Length == 0 ? "" : " ", mTercerNombre).ToUpper();
                        retorna.nombre = mNombre;

                        var qUpdateMF_Cliente = from c in db.MF_Cliente where c.CLIENTE == mCliente select c;
                        foreach (var item in qUpdateMF_Cliente)
                        {
                            item.PRIMER_NOMBRE = mPrimerNombre;
                            item.SEGUNDO_NOMBRE = mSegundoNombre;
                            item.TERCER_NOMBRE = mTercerNombre;
                            item.PRIMER_APELLIDO = mPrimerApellido;
                            item.SEGUNDO_APELLIDO = mSegundoApellido;
                            item.APELLIDO_CASADA = mApellidoCasada;
                            item.NOMBRE_FACTURA = mNombre;
                        }

                        var qUpdateCliente = from c in db.CLIENTE where c.CLIENTE1 == mCliente select c;
                        foreach (var item in qUpdateCliente)
                        {
                            item.NOMBRE = mNombre;
                        }
                    }

                    db.SaveChanges();
                    transactionScope.Complete();
                }

                log.Info("Datos en cotización y pedido grabados exitosamente.");
            }
            catch (Exception ex)
            {
                string mMensajeError = CatchClass.ExMessage(ex, "ConsultaInterconsumoController", "GetString");

                retorna.exito = false;
                retorna.mensaje = mMensajeError;
                log.Error(mMensajeError);
            }

            return retorna;
        }
    }
}
