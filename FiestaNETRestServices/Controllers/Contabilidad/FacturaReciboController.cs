﻿using FiestaNETRestServices.Content.Abstract;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Contabilidad
{
    [RoutePrefix("api/FacturaRecibo")]
    public class FacturaReciboController : MFApiController
    {
        

        [HttpGet]
        [Route("~/FacturaRecibo/Saldos/{id}")]
        public List<Clases.DocumentoConSaldo> Saldos(string id)
        {
            //id=cliente
                DAL.DALRecibo recibo = new DAL.DALRecibo();
                var recibos = recibo.obtenerRecibosconSaldo(id);
            return recibos;
        }

        
    }
}
