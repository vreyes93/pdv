﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.EntityClient;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Http;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.DAL;
using System.IO;
using System.Web.Configuration;
using Newtonsoft.Json;
using MF_Clases.Comun;
using System.Text;
using MF_Clases.Utils;

namespace FiestaNETRestServices.Controllers.Contabilidad
{
    [RoutePrefix("api/FacturasAtid")]
    public class FacturasAtidController : MFApiController
    {

        private DALCrediplus DalCP = new DALCrediplus();


        // GET: api/FacturasAtid
        public string Get()
        {
            return CargaFacturas("");
        }

        // GET: api/FacturasAtid/5
        public string Get(string id)
        {
            return CargaFacturas(id);
        }

        public string Get(string inicio, string fin)
        {
            return CargaFacturas("F", inicio, fin);
        }

        public string Get(string inicio, string fin, string soloTraslado)
        {
            return CargaFacturas("F", inicio, fin, soloTraslado);
        }

        [HttpPost]
        public IHttpActionResult Post(string id)
        {
            try
            {
                if (!DalCP.ValidarVisibilidadTienda(id))
                {
                    return Ok(new { process = "FacturaMF->CP", factura = id, hasError = true, comment = "Esta factura no se encuentra configurada para trabajar con Crediplus" });
                }
                else
                {
                    log.Info(JsonConvert.SerializeObject(new { process = "FacturaMF->CP", factura = id, hasError = false, comment = "Iniciando proceso" }));

                    if (DalCP.ValidarFacturaEnCrediplus(id))
                        throw new Exception("Esta factura ya fue operada en Crediplus.");
                    string TiendaFactura = DalCP.TiendaFacturacionOutlet(id);
                    string mensaje = "La factura <strong>" + id + "</strong>, ha sido emitida en la tienda <strong>"+ TiendaFactura + "</strong> de  Muebles Fiesta, ";
                    List<ArticuloCrediplus> articulos = DalCP.ObtenerProductosCrediplus(id);

                    if (articulos.Count > 0)
                    {
                        List<ArticuloCrediplus> artNoExiste = articulos.Where(x => x.Existe == false).ToList(),
                        artInactivo = articulos.Where(x => x.Activo == false).ToList(),
                        artSinExistencia = articulos.Where(x => x.HayExistencia == false).ToList();

                        if (artNoExiste.Count > 0)
                        {
                            mensaje = mensaje + "pero no se generó la factura de Crediplus porque los siguientes artículos no se encuentran registrados:";
                            NotificarFactura(artNoExiste, mensaje);
                            var resultObj = new { Mensaje = mensaje, Data = artNoExiste, Factura = id };

                            log.Info(JsonConvert.SerializeObject(new { process = "FacturaMF->CP", factura = id, hasError = true, comment = resultObj }));
                            return Ok(JsonConvert.SerializeObject(resultObj));
                        }

                        if (artInactivo.Count > 0)
                        {
                            mensaje = mensaje + "pero no se generó la factura de Crediplus porque los siguientes artículos se encuentran inactivos:";
                            NotificarFactura(artInactivo, mensaje);
                            var resultObj = new { Mensaje = mensaje, Data = artInactivo, Factura = id };

                            log.Info(JsonConvert.SerializeObject(new { process = "FacturaMF->CP", factura = id, hasError = true, comment = resultObj }));
                            return Ok(JsonConvert.SerializeObject(resultObj));
                        }

                        if (artSinExistencia.Count > 0)
                        {
                            mensaje = mensaje + "pero no se generó la factura de Crediplus porque los siguientes artículos no cuentan con existencia:";
                            NotificarFactura(artSinExistencia, mensaje);
                            var resultObj = new { Mensaje = mensaje, Data = artSinExistencia, Factura = id };

                            log.Info(JsonConvert.SerializeObject(new { process = "FacturaMF->CP", factura = id, hasError = true, comment = resultObj }));
                            return Ok(JsonConvert.SerializeObject(resultObj));
                        }
                        string fact = string.Empty;
                        //////
                        ////----FACTURAR///////////
                        ///
                        UtilitariosAtid crediplus = new UtilitariosAtid();
                        Clases.RespuestaGuateFacturas mRespuesta = new RespuestaGuateFacturas();
                        var resp = crediplus.Facturar(id);

                        ////FACTURAR
                        ///
                        if (resp.Exito)
                        {
                            mRespuesta = (RespuestaGuateFacturas)resp.Objeto;
                            mensaje = mensaje + " la factura <strong>" + mRespuesta.facturaElectronica + "</strong> de Crediplus, fué generada con los siguientes productos:";
                            NotificarFactura(articulos, mensaje);
                        }
                        else
                        {
                            NotificarFactura(null, mensaje + resp.Mensaje, true);
                        }
                        return Ok(articulos);
                    }
                    else
                        return Ok("No es necesario facturar de parte de CREDIPLUS");                    
                }
            }
            catch (Exception ex)
            {
                var error = new { process = "FacturaMF->CP", factura = id, hasError = true, comment = "Excepción", errorMessage = ex.Message, innerMessage = ex.InnerException == null ? null : ex.InnerException.Message };
                string msjError = JsonConvert.SerializeObject(error);
                log.Info(msjError);
                NotificarFactura(null, msjError, true);

                return Ok(msjError);
            }
            finally
            {
                log.Info(JsonConvert.SerializeObject(new { process = "FacturaMF->CP", factura = id, hasError = false, comment = "Finalizando proceso." }));
            }


        }

        public IHttpActionResult Post([FromBody]FacturacionArticulos datos)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                string json = JsonConvert.SerializeObject(datos);
                log.Debug("Iniciando la facturación de articulos para Crediplus: " + json);
                DAL.DALCliente cliente = new DAL.DALCliente();
                MF.Comun.Dto.Cliente cli = new MF.Comun.Dto.Cliente
                {
                    Nombres = datos.NombreCliente,
                    Apellidos = datos.ApellidosCliente,
                    Email = datos.Email,
                    Departamento = CONSTANTES.CREDIPLUS.DEPARTAMENTO_DEFAULT,
                    Municipio = CONSTANTES.CREDIPLUS.MUNICIPIO_DEFAULT,
                    Indicaciones = datos.DirCliente,
                    FormaDePago = datos.FormaDePago,
                    NIT = datos.NitCliente,
                    Casa = string.Empty,
                    Calle_avenida = string.Empty,
                    Colonia = string.Empty,
                    Apartamento = string.Empty,
                    Identificacion = CONSTANTES.CREDIPLUS.IDENT_DEFAULT,
                    Zona = CONSTANTES.CREDIPLUS.ZONA_DEFAULT,
                    Genero = string.Empty
                 
                };
                 respuesta = cliente.ObtenerCodigoCliente(cli,"crediplus");
                if (respuesta.Exito)
                {
                    string cCLIENTE = respuesta.Objeto.ToString();
                    char[] aCli = cCLIENTE.ToArray();
                    //crear pedido 
                    datos.cliente = new string(aCli);

                    UtilitariosAtid utilitarios = new UtilitariosAtid();
                    var resp = utilitarios.FacturarArticulos(datos);
                    return Ok(resp);
                }

            } catch (Exception Ex)
            {
                log.Error("No se pudo obtener la facturacion de artículos de crédiplus "+ Ex.Message);
                respuesta.Exito = false;
                respuesta.Mensaje = Ex.Message+" "+Ex.InnerException ;
            }
            return Ok(respuesta);
        }
        public string Get(string inicio, string fin, bool soloFirma)
        {
            string mRetorna = "OK"; string mMensaje = "";

            EntityConnection ec = (EntityConnection)db.Connection;
            SqlConnection sc = (SqlConnection)ec.StoreConnection;

            SqlConnection mConexion = new SqlConnection(sc.ConnectionString);
            SqlCommand mCommand = new SqlCommand(); SqlTransaction mTransaction = null;

            try
            {
                string[] mArregloInicial = inicio.Split(new string[] { "-" }, StringSplitOptions.None);
                string[] mArregloFinal = fin.Split(new string[] { "-" }, StringSplitOptions.None);

                UtilitariosAtid mUtilitariosAtid = new UtilitariosAtid();
                DateTime mFechaInicial = new DateTime(Convert.ToInt32(mArregloInicial[0]), Convert.ToInt32(mArregloInicial[1]), Convert.ToInt32(mArregloInicial[2]));
                DateTime mFechaFinal = new DateTime(Convert.ToInt32(mArregloFinal[0]), Convert.ToInt32(mArregloFinal[1]), Convert.ToInt32(mArregloFinal[2]));

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter("SELECT a.FACTURA, a.CLIENTE, a.TOTAL_MERCADERIA, a.TOTAL_IMPUESTO1, a.TOTAL_FACTURA, a.FECHA, b.NOMBRE " +
                    "FROM crediplus.FACTURA a JOIN crediplus.CLIENTE b ON a.CLIENTE = b.CLIENTE WHERE a.FECHA BETWEEN @FechaInicial AND @FechaFinal AND A.ANULADA = 'N' ORDER BY A.FECHA", mConexion);

                da.SelectCommand.Parameters.Add("@FechaInicial", SqlDbType.DateTime).Value = mFechaInicial;
                da.SelectCommand.Parameters.Add("@FechaFinal", SqlDbType.DateTime).Value = mFechaFinal;

                da.Fill(dt);

                mConexion.Open();
                mTransaction = mConexion.BeginTransaction();

                mCommand.Connection = mConexion;
                mCommand.Transaction = mTransaction;

                mCommand.Parameters.Add("@Cliente", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@DireccionDevuelveGFace", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@NombreDevuelveGFace", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@error", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@XMLDocumento", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@XMLReversion", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@XMLRespuesta", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = DateTime.Now.Date;

                int mCuantasFacturas = 0; int mCuantasFirmadas = 0;

                foreach (DataRow row in dt.Rows)
                {
                    bool mFirmar = true; bool mUpdate = false;
                    string mFactura = Convert.ToString(row["FACTURA"]);
                    decimal mValor = Convert.ToDecimal(row["TOTAL_MERCADERIA"]);
                    decimal mIva = Convert.ToDecimal(row["TOTAL_IMPUESTO1"]);
                    decimal mTotal = Convert.ToDecimal(row["TOTAL_FACTURA"]);
                    string mCredito = "";

                    mCommand.CommandText = $"SELECT COUNT(FACTURA) FROM crediplus.CP_Factura WHERE FACTURA = '{mFactura}'";
                    if (Convert.ToInt32(mCommand.ExecuteScalar()) > 0)
                    {
                        mCommand.CommandText = $"SELECT FIRMADA FROM crediplus.CP_Factura WHERE FACTURA = '{mFactura}'";
                        string mFirmada = Convert.ToString(mCommand.ExecuteScalar());

                        if (mFirmada == "S") mFirmar = false;
                        if (mFirmada == "N") mUpdate = true;
                    }

                    if (mFirmar)
                    {
                        mCuantasFacturas++;
                        string mXML = mUtilitariosAtid.xmlCrediplus(mFactura, true, "N");
                        string mXMLReversion = mUtilitariosAtid.xmlCrediplus(mFactura, true, "S");

                        Clases.RespuestaGuateFacturas mRespuesta = new RespuestaGuateFacturas();
                        mRespuesta = mUtilitariosAtid.firmarFactura(mXML, MF_Clases.Utils.CONSTANTES.GUATEFAC.MAQUINA_CREDIPLUS.FACTURACION_PHOENIX);

                        mCommand.Parameters["@Cliente"].Value = Convert.ToString(row["CLIENTE"]);
                        mCommand.Parameters["@Nombre"].Value = Convert.ToString(row["NOMBRE"]);
                        mCommand.Parameters["@Fecha"].Value = Convert.ToDateTime(row["FECHA"]);
                        mCommand.Parameters["@DireccionDevuelveGFace"].Value = mRespuesta.direccion;
                        mCommand.Parameters["@NombreDevuelveGFace"].Value = mRespuesta.nombre;
                        mCommand.Parameters["@error"].Value = mRespuesta.error;
                        mCommand.Parameters["@XMLDocumento"].Value = mXML.Length <= 4999 ? mXML : mXML.Substring(0, 4999);
                        mCommand.Parameters["@XMLReversion"].Value = mXMLReversion.Length <= 4999 ? mXMLReversion : mXMLReversion.Substring(0, 4999);
                        mCommand.Parameters["@XMLRespuesta"].Value = mRespuesta.xml.Length <= 3999 ? mRespuesta.xml : mRespuesta.xml.Substring(0, 3999);

                        if (mUpdate)
                        {
                            mCommand.CommandText = "UPDATE crediplus.CP_Factura SET FACTURA_ELECTRONICA = '" + mRespuesta.facturaElectronica + "', FIRMADA = '" + mRespuesta.firmada + "', FIRMA = '" + mRespuesta.firma + "', " +
                                "SERIE = '" + mRespuesta.serie + "', NUMERO = '" + mRespuesta.numero + "', IDENTIFICADOR = '" + mRespuesta.identificador + "', MAQUINA = '" + mRespuesta.maquina + "', NOMBRE = @NombreDevuelveGFace, " +
                                "DIRECCION = @DireccionDevuelveGFace, TELEFONO = '" + mRespuesta.telefono + "', ERROR = @Error, XML_DOCUMENTO = @XMLDocumento, XML_REVERSION = @XMLReversion, XML_RESPUESTA = @XMLRespuesta, FECHA_REGISTRO = GETDATE() " +
                                "WHERE FACTURA = '" + mFactura + "'";
                            mCommand.ExecuteNonQuery();
                        }
                        else
                        {
                            mCommand.CommandText = "INSERT INTO crediplus.CP_Factura ( FACTURA, FACTURA_ELECTRONICA, CLIENTE, FECHA, FIRMADA, FIRMA, SERIE, NUMERO, " +
                                "IDENTIFICADOR, MAQUINA, NOMBRE, DIRECCION, TELEFONO, ERROR, XML_DOCUMENTO, XML_REVERSION, XML_RESPUESTA, " +
                                "FECHA_REGISTRO, ID_CREDIPLUS, CLIENTE_CREDIPLUS, CREDITO_CREDIPLUS, NOMBRE_CREDIPLUS, VALOR_CREDIPLUS, IVA_CREDIPLUS, TOTAL_CREDIPLUS, FECHA_REGISTRO_CREDIPLUS ) " +
                                "VALUES ( '" + mFactura + "', '" + mRespuesta.facturaElectronica + "', @Cliente, @Fecha, '" + mRespuesta.firmada + "', '" + mRespuesta.firma + "', '" + mRespuesta.serie + "', '" + mRespuesta.numero + "', " +
                                "'" + mRespuesta.identificador + "', '" + mRespuesta.maquina + "', @NombreDevuelveGFace, @DireccionDevuelveGFace, '" + mRespuesta.telefono + "', @error, @XMLDocumento, @XMLReversion, @XMLRespuesta, " +
                                "GETDATE(), 0, @Cliente, '" + mCredito + "', @Nombre, " + mValor.ToString() + ", " + mIva.ToString() + ", " + mTotal.ToString() + ", @Fecha ) ";
                            mCommand.ExecuteNonQuery();
                        }

                        if (mRespuesta.firmada == "S")
                            mCuantasFirmadas++;
                    }
                }

                mTransaction.Commit();
                mConexion.Close();

                mRetorna = string.Format("Se encontraron {0} facturas y se firmaron {1}", mCuantasFacturas, mCuantasFirmadas);
            }
            catch (Exception ex)
            {
                if (mConexion.State == ConnectionState.Open)
                {
                    mTransaction.Rollback();
                    mConexion.Close();
                }

                mRetorna = string.Format("{0} {1} {2}", CatchClass.ExMessage(ex, "FacturasAtidController", "GET"), mMensaje, mCommand.CommandText);
                EnviarError(mRetorna);
                log.Error(mRetorna);
            }

            return mRetorna;
        }

        string CargaFacturas(string tipo, string inicio = "", string fin = "", string soloTraslado = "N")
        {
            string mRetorna = "OK"; string mMensaje = "";

            EntityConnection ec = (EntityConnection)db.Connection;
            SqlConnection sc = (SqlConnection)ec.StoreConnection;

            SqlConnection mConexion = new SqlConnection(sc.ConnectionString);
            SqlCommand mCommand = new SqlCommand(); SqlTransaction mTransaction = null;

            try
            {
                string mFechaInicial = ""; string mFechaFinal = "";

                if (tipo == "")
                {
                    DateTime mFinal = DateTime.Now.Date;
                    DateTime mInicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

                    if (DateTime.Now.Day <= 3)
                    {
                        mInicio = DateTime.Now.AddMonths(-1);
                        mInicio = new DateTime(mInicio.Year, mInicio.Month, 1);

                        mFinal = mInicio.AddMonths(1);
                        mFinal = new DateTime(mFinal.Year, mFinal.Month, 1);
                        mFinal = mFinal.AddDays(-1);
                    }

                    mFechaInicial = string.Format("{0}-{1}{2}-01", mInicio.Year.ToString(), mInicio.Month < 10 ? "0" : "", mInicio.Month.ToString());
                    mFechaFinal = string.Format("{0}-{1}{2}-{3}{4}", mFinal.Year.ToString(), mFinal.Month < 10 ? "0" : "", mFinal.Month.ToString(), mFinal.Day < 10 ? "0" : "", mFinal.Day.ToString());
                }
                else
                {
                    if (tipo == "F")
                    {
                        mFechaInicial = inicio;
                        mFechaFinal = fin;
                    }
                    else
                    {
                        int mAnio = DateTime.Now.Year; int mMes = 0;
                        string[] mArreglo = tipo.Split(new string[] { "-" }, StringSplitOptions.None);

                        if (mArreglo.Length == 1)
                        {
                            mMes = Convert.ToInt32(mArreglo[0]);
                        }
                        else
                        {
                            mMes = Convert.ToInt32(mArreglo[1]);
                            mAnio = Convert.ToInt32(mArreglo[0]);
                        }

                        DateTime mFechaInicialDT = new DateTime(mAnio, mMes, 1);
                        DateTime mFechaFinalDT = mFechaInicialDT.AddMonths(1);

                        mFechaFinalDT = new DateTime(mFechaFinalDT.Year, mFechaFinalDT.Month, 1);
                        mFechaFinalDT = mFechaFinalDT.AddDays(-1);

                        mFechaFinal = string.Format("{0}-{1}{2}-{3}{4}", mFechaFinalDT.Year.ToString(), mFechaFinalDT.Month < 10 ? "0" : "", mFechaFinalDT.Month.ToString(), mFechaFinalDT.Day < 10 ? "0" : "", mFechaFinalDT.Day.ToString());
                        mFechaInicial = string.Format("{0}-{1}{2}-{3}{4}", mFechaInicialDT.Year.ToString(), mFechaInicialDT.Month < 10 ? "0" : "", mFechaInicialDT.Month.ToString(), mFechaInicialDT.Day < 10 ? "0" : "", mFechaInicialDT.Day.ToString());
                    }
                }

                List<Clases.FacturasAtid> mFacturas = new List<Clases.FacturasAtid>();

                string mUrlCrediPlus = WebConfigurationManager.AppSettings["DefaultATIDServicesURL"];
                WebRequest request = WebRequest.Create(string.Format("{0}api/FacturaCredito/{1}/{2}", mUrlCrediPlus, mFechaInicial, mFechaFinal));

                request.Method = "GET";
                request.ContentType = "application/json";

                try
                {
                    var response = (HttpWebResponse)request.GetResponse();
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    mFacturas = JsonConvert.DeserializeObject<List<Clases.FacturasAtid>>(responseString);
                }
                catch (Exception ex1)
                {
                    return "Error, no se encontraron facturas." + ex1.Message;
                }

                if (mFacturas.Count() == 0) return "Error, no se encontraron facturas.";
                var qFacturasOrdenadas = from f in mFacturas orderby f.fechaRegistro select f;

                mConexion.Open();
                mTransaction = mConexion.BeginTransaction();

                mCommand.Connection = mConexion;
                mCommand.Transaction = mTransaction;

                mCommand.CommandText = "SELECT NOMBRE_CAT FROM crediplus.CP_Catalogo WHERE CODIGO_TABLA = 1 AND CODIGO_CAT = 'SOLO_UNA_FACTURA'";
                string mSoloUnaFactura = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.Parameters.Add("@Direccion", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@Descripcion", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@FechaAnulacion", SqlDbType.DateTime).Value = DateTime.Now.Date;
                mCommand.Parameters.Add("@FechaNacimiento", SqlDbType.DateTime).Value = DateTime.Now.Date;
                mCommand.Parameters.Add("@FechaOperacion", SqlDbType.DateTime).Value = DateTime.Now.Date;
                mCommand.Parameters.Add("@FechaRegistro", SqlDbType.DateTime).Value = DateTime.Now.Date;

                //if (mSoloUnaFactura == "S") qFacturasOrdenadas = qFacturasOrdenadas.Take(1).OrderBy(x => x.fechaRegistro);

                int mCuantasTrasladadas = 0;
                foreach (var item in qFacturasOrdenadas)
                {
                    bool mEntra = true;
                    mCommand.CommandText = $"SELECT COUNT(ID) FROM crediplus.CP_Factura_Phoenix WHERE ID = {item.id}";
                    if (Convert.ToInt32(mCommand.ExecuteScalar()) > 0)
                        mEntra = false;

                    if (mSoloUnaFactura == "S" && mCuantasTrasladadas >= 1)
                        mEntra = false;

                    if (mEntra)
                    {
                        mCuantasTrasladadas++;
                        string mFechaAnulacion = "NULL";

                        mCommand.Parameters["@Direccion"].Value = item.direccion;
                        mCommand.Parameters["@FechaNacimiento"].Value = item.fechaNacimiento;
                        mCommand.Parameters["@FechaOperacion"].Value = item.fechaOperacion;
                        mCommand.Parameters["@FechaRegistro"].Value = item.fechaRegistro;

                        if (item.anulada == "S")
                        {
                            try
                            {
                                DateTime mFechaAnulacionDateTime = Convert.ToDateTime(item.fechaAnulacion);
                                if (mFechaAnulacionDateTime.Year > 1)
                                {
                                    mCommand.Parameters["@FechaAnulacion"].Value = new DateTime(mFechaAnulacionDateTime.Year, mFechaAnulacionDateTime.Month, mFechaAnulacionDateTime.Day);
                                    mFechaAnulacion = "@FechaAnulacion";
                                }
                            }
                            catch
                            {
                                mFechaAnulacion = "NULL";
                            }
                        }

                        string mEnExactus = "N"; string mFacturaExactus = "NULL";
                        if (soloTraslado == "S") mEnExactus = "S";

                        mCommand.CommandText = $"SELECT COUNT(ID) FROM crediplus.CP_Factura_Phoenix WHERE ID = {item.id.ToString()}";
                        if (Convert.ToInt32(mCommand.ExecuteScalar()) == 0)
                        {
                            mCommand.CommandText = "INSERT INTO crediplus.CP_Factura_Phoenix ( ID, CLIENTE, DPI, NIT, CREDITO, FECHA_NACIMIENTO, SEXO, ESTADO_CIVIL, E_MAIL,  " +
                            "NOMBRE_CLIENTE, PRIMER_NOMBRE, SEGUNDO_NOMBRE, TERCER_NOMBRE, PRIMER_APELLIDO, SEGUNDO_APELLIDO, APELLIDO_CASADA, " +
                            "DIRECCION, MUNICIPIO, DEPARTAMENTO, FECHA_OPERACION, FECHA_REGISTRO, ANULADA, FECHA_ANULACION, EN_EXACTUS, FACTURA_EXACTUS ) " +
                            "VALUES ( " + item.id.ToString() + ", '" + item.cliente + "', '" + item.dpi + "', '" + item.nit + "', '" + item.credito + "', @FechaNacimiento, '" + item.sexo + "', '" + item.estadoCivil + "', '" + item.eMail + "', '" +
                            item.nombrecliente + "', '" + item.primerNombre + "', '" + item.segundoNombre + "', '" + item.tercerNombre + "', '" + item.primerApellido + "', '" + item.segundoApellido + "', '" + item.apellidoCasada + "', " +
                            "@Direccion, '" + item.municipio + "', '" + item.departamento + "', @FechaOperacion, @FechaRegistro, '" + item.anulada + "', " + mFechaAnulacion + ", '" + mEnExactus + "', " + mFacturaExactus + " ) ";
                            mCommand.ExecuteNonQuery();

                            foreach (var linea in item.detalleFactura)
                            {
                                mCommand.Parameters["@Descripcion"].Value = linea.descripcion;

                                mCommand.CommandText = "INSERT INTO crediplus.CP_Factura_Phoenix_Linea ( ID, LINEA, ID_FACTURA, PRODUCTO, DESCRIPCION, VALOR, IVA, TOTAL ) " +
                                    "VALUES ( " + linea.id.ToString() + ", " + linea.linea.ToString() + ", " + linea.idFactura + ", '" + linea.producto + "', @Descripcion, " + linea.valor.ToString() + ", " + linea.iva.ToString() + ", " + linea.total.ToString() + " ) ";
                                mCommand.ExecuteNonQuery();
                            }
                        }
                    }
                }

                mTransaction.Commit();
                mConexion.Close();

                if (soloTraslado == "S") return string.Format("Se trasladaron exitosamente {0} facturas", mCuantasTrasladadas.ToString());

                DataTable dt = new DataTable(); DataTable dtLinea = new DataTable(); DataTable dtLineasCliente = new DataTable(); DataTable dtFacturasCliente = new DataTable();

                dtFacturasCliente.Columns.Add(new DataColumn("CLIENTE", typeof(System.String)));
                dtFacturasCliente.Columns.Add(new DataColumn("FACTURA", typeof(System.String)));

                SqlDataAdapter da = new SqlDataAdapter("SELECT ID, EN_EXACTUS, FACTURA_EXACTUS, CLIENTE, DPI, NIT, CREDITO, FECHA_NACIMIENTO, SEXO, ESTADO_CIVIL, E_MAIL, NOMBRE_CLIENTE, PRIMER_NOMBRE, SEGUNDO_NOMBRE, " +
                    "TERCER_NOMBRE, PRIMER_APELLIDO, SEGUNDO_APELLIDO, APELLIDO_CASADA, DIRECCION, MUNICIPIO, DEPARTAMENTO, FECHA_OPERACION, FECHA_REGISTRO, ANULADA, FECHA_ANULACION, FECHA_HORA " +
                    "FROM crediplus.CP_Factura_Phoenix WHERE EN_EXACTUS = 'N' AND ANULADA = 'N' ORDER BY CLIENTE", mConexion);

                SqlDataAdapter daLinea = new SqlDataAdapter("SELECT a.ID, a.LINEA, a.ID_FACTURA, a.PRODUCTO, a.DESCRIPCION, a.VALOR, a.IVA, a.TOTAL, a.FECHA_HORA, b.CLIENTE " +
                    "FROM CREDIPLUS.CP_Factura_Phoenix_Linea a JOIN CREDIPLUS.CP_Factura_Phoenix b ON a.ID_FACTURA = b.ID " +
                    "WHERE b.EN_EXACTUS = 'N' AND b.ANULADA = 'N' ORDER BY b.CLIENTE, a.ID_FACTURA, b.ID", mConexion);

                da.Fill(dt);
                daLinea.Fill(dtLinea);
                daLinea.FillSchema(dtLineasCliente, SchemaType.Mapped);

                DataTable dtClientesDistinct = new DataTable();
                DataView dvClientes = dt.DefaultView;

                dtClientesDistinct = dvClientes.ToTable(true, "CLIENTE");
                dtClientesDistinct.Columns.Add(new DataColumn("FECHA", typeof(System.DateTime)));

                mConexion.Open();
                mTransaction = mConexion.BeginTransaction();

                mCommand.Connection = mConexion;
                mCommand.Transaction = mTransaction;

                foreach (DataRow row in dtClientesDistinct.Rows)
                {
                    mCommand.CommandText = $"SELECT MAX(FECHA_REGISTRO) FROM crediplus.CP_Factura_Phoenix WHERE EN_EXACTUS = 'N' AND ANULADA = 'N' AND CLIENTE = '{Convert.ToString(row["CLIENTE"])}'";
                    row["FECHA"] = Convert.ToDateTime(mCommand.ExecuteScalar());
                }

                DataTable dtClientes = new DataTable();
                dtClientes.Columns.Add(new DataColumn("CLIENTE", typeof(System.String)));
                dtClientes.Columns.Add(new DataColumn("FECHA", typeof(System.DateTime)));

                DataRow[] mRowsOrdenadasPorFecha = dtClientesDistinct.Select("", "FECHA");
                for (int ii = 0; ii < mRowsOrdenadasPorFecha.Length; ii++)
                {
                    DataRow mRow = dtClientes.NewRow();
                    mRow["CLIENTE"] = mRowsOrdenadasPorFecha[ii]["CLIENTE"];
                    mRow["FECHA"] = mRowsOrdenadasPorFecha[ii]["FECHA"];
                    dtClientes.Rows.Add(mRow);
                }

                mCommand.CommandText = "SELECT NOMBRE_CAT FROM crediplus.CP_Catalogo WHERE CODIGO_TABLA = 1 AND CODIGO_CAT = 'FACTURA_ELECTRONICA'";
                string mFacElectronica = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = "SELECT TOP 1 MONTO FROM prodmult.TIPO_CAMBIO_HIST WHERE TIPO_CAMBIO = 'REFR' ORDER BY FECHA DESC";
                decimal mTipoCambio = Convert.ToDecimal(mCommand.ExecuteScalar());

                mCommand.CommandText = "SELECT VALOR_CONSECUTIVO FROM crediplus.CONSECUTIVO_FA WHERE CODIGO_CONSECUTIVO = '02'";
                Int64 mNumeroPedido = Convert.ToInt64(mCommand.ExecuteScalar().ToString().Replace("P", ""));

                string mFactura = "";

                mCommand.CommandText = "SELECT E_MAIL FROM crediplus.COBRADOR WHERE COBRADOR = 'ND'";
                string[] mConsecutivos = mCommand.ExecuteScalar().ToString().Split(new string[] { "," }, StringSplitOptions.None);

                if (mFacElectronica == "N")
                {
                    if (mConsecutivos.Count() == 0)
                    {
                        mTransaction.Rollback();
                        mConexion.Close();

                        mRetorna = "Error, no se encontró el consecutivo de facturas para crediplus";

                        EnviarError(mRetorna);
                        log.Error(mRetorna);
                        return mRetorna;
                    }
                }

                string[] mConsecutivoFABK;
                string mConsecutivo = mConsecutivos[0]; string mConsecutivoBK = ""; string mUltimoValorBk = ""; int mDisponibles1 = 0; int mDisponibles2 = 0; int mDisponibles = 0; int mNumeroFacturaBk = 0; string mValorActualBK = "";

                mCommand.CommandText = $"SELECT VALOR_MAXIMO FROM crediplus.CONSECUTIVO_FA WHERE CODIGO_CONSECUTIVO = '{mConsecutivo}'";
                string mUltimoValor = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = $"SELECT VALOR_CONSECUTIVO FROM crediplus.CONSECUTIVO_FA WHERE CODIGO_CONSECUTIVO = '{mConsecutivo}'";
                string mValorActual = Convert.ToString(mCommand.ExecuteScalar());

                string[] mConsecutivoFA = mValorActual.Split(new string[] { "-" }, StringSplitOptions.None);
                Int32 mNumeroFactura = Convert.ToInt32(mConsecutivoFA[1]);

                string[] mConsecutivoUltimoFA = mUltimoValor.Split(new string[] { "-" }, StringSplitOptions.None);
                Int32 mNumeroFacturaUltimo = Convert.ToInt32(mConsecutivoUltimoFA[1]);

                mDisponibles1 = mNumeroFacturaUltimo - mNumeroFactura;

                try
                {
                    mConsecutivoBK = mConsecutivos[1];

                    mCommand.CommandText = $"SELECT VALOR_MAXIMO FROM crediplus.CONSECUTIVO_FA WHERE CODIGO_CONSECUTIVO = '{mConsecutivoBK}'";
                    mUltimoValorBk = Convert.ToString(mCommand.ExecuteScalar());

                    mCommand.CommandText = $"SELECT VALOR_CONSECUTIVO FROM crediplus.CONSECUTIVO_FA WHERE CODIGO_CONSECUTIVO = '{mConsecutivoBK}'";
                    mValorActualBK = Convert.ToString(mCommand.ExecuteScalar());

                    mConsecutivoFABK = mValorActualBK.Split(new string[] { "-" }, StringSplitOptions.None);
                    mNumeroFacturaBk = Convert.ToInt32(mConsecutivoFABK[1]);

                    string[] mConsecutivoUltimoFABk = mUltimoValorBk.Split(new string[] { "-" }, StringSplitOptions.None);
                    Int32 mNumeroFacturaUltimoBk = Convert.ToInt32(mConsecutivoUltimoFABk[1]);

                    mDisponibles2 = mNumeroFacturaUltimoBk - mNumeroFacturaBk;
                    mNumeroFacturaBk = mNumeroFacturaBk - 1;
                }
                catch
                {
                    mConsecutivoBK = "";
                }

                mDisponibles = mDisponibles1 + mDisponibles2;

                if (mFacElectronica == "N")
                {
                    if (mUltimoValor == mValorActual)
                    {
                        mTransaction.Rollback();
                        mConexion.Close();

                        mRetorna = string.Format("Error, no se encontraron facturas disponibles para crediplus, ultimo valor: {0}, valor actual: {1}", mUltimoValor, mValorActual);

                        EnviarError(mRetorna);
                        log.Error(mRetorna);
                        return mRetorna;
                    }

                    if (mDisponibles < dtClientes.Rows.Count)
                    {
                        mTransaction.Rollback();
                        mConexion.Close();

                        mRetorna = string.Format("Error, se tienen {0} facturas disponibles para crediplus, y se necesitan {1}, se requieren {2} facturas para continuar", mDisponibles, dtClientes.Rows.Count, (dtClientes.Rows.Count - mDisponibles));

                        EnviarError(mRetorna);
                        log.Error(mRetorna);
                        return mRetorna;
                    }
                }

                string mCeros = ""; int mFolioInicialBK = 0;

                mCommand.CommandText = $"SELECT b.FOLIO_INICIAL FROM crediplus.CONSECUTIVO_FA a JOIN crediplus.RESOLUCION_DOC_ELECTRONICO b ON a.RESOLUCION = b.RESOLUCION WHERE a.CODIGO_CONSECUTIVO = '{mConsecutivo}'";
                int mFolioInicial = Convert.ToInt32(mCommand.ExecuteScalar());

                try
                {
                    mCommand.CommandText = $"SELECT b.FOLIO_INICIAL FROM crediplus.CONSECUTIVO_FA a JOIN crediplus.RESOLUCION_DOC_ELECTRONICO b ON a.RESOLUCION = b.RESOLUCION WHERE a.CODIGO_CONSECUTIVO = '{mConsecutivoBK}'";
                    mFolioInicialBK = Convert.ToInt32(mCommand.ExecuteScalar());
                }
                catch
                {
                    mFolioInicialBK = 0;
                }

                if (mFolioInicial == mNumeroFactura) mNumeroFactura = mNumeroFactura - 1;

                mCommand.Parameters.Add("@Cliente", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@Departamento", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@Municipio", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@Nit", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@DireccionCompleta", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@DireccionDevuelveGFace", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@NombreDevuelveGFace", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@error", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@XMLDocumento", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@XMLReversion", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@XMLRespuesta", SqlDbType.VarChar).Value = "";

                mCommand.Parameters.Add("@Hoy", SqlDbType.DateTime).Value = DateTime.Now.Date;
                mCommand.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = DateTime.Now.Date;
                mCommand.Parameters.Add("@FechaUltima", SqlDbType.DateTime).Value = new DateTime(1980, 1, 1);

                string mUltimaFactura = "";
                int mCuantasFacturas = 0; int mFirmadas = 0;
                UtilitariosAtid mUtilitariosAtid = new UtilitariosAtid();

                foreach (DataRow row in dtClientes.Rows)
                {
                    mCuantasFacturas++;
                    string mCliente = Convert.ToString(row["CLIENTE"]);

                    DataRow[] mRowCliente = dt.Select("CLIENTE = '" + mCliente + "'", "FECHA_REGISTRO DESC");
                    string mNit = Convert.ToString(mRowCliente[0]["NIT"]).Replace(" ", "");
                    string mNombreCliente = Convert.ToString(mRowCliente[0]["NOMBRE_CLIENTE"]);
                    string mDireccion = Convert.ToString(mRowCliente[0]["DIRECCION"]);
                    string mDepartamento = Convert.ToString(mRowCliente[0]["DEPARTAMENTO"]);
                    string mMunicipio = Convert.ToString(mRowCliente[0]["MUNICIPIO"]);
                    string mCredito = Convert.ToString(mRowCliente[0]["CREDITO"]);
                    string mEmail = Convert.ToString(mRowCliente[0]["E_MAIL"]);
                    DateTime mFechaRegistro = Convert.ToDateTime(row["FECHA"]);

                    decimal mValor = Convert.ToDecimal(dtLinea.Compute("SUM(VALOR)", "CLIENTE = '" + mCliente + "'"));
                    decimal mIva = Convert.ToDecimal(dtLinea.Compute("SUM(IVA)", "CLIENTE = '" + mCliente + "'"));
                    decimal mTotal = Convert.ToDecimal(dtLinea.Compute("SUM(TOTAL)", "CLIENTE = '" + mCliente + "'"));
                    decimal mTotalDolar = Math.Round((mTotal / mTipoCambio), 2, MidpointRounding.AwayFromZero);
                    string mReferencia = Convert.ToString(dtLinea.Compute("MAX(ID_FACTURA)", "CLIENTE = '" + mCliente + "'"));

                    mCommand.Parameters["@Nit"].Value = mNit.Replace(" ", "");
                    if (mNit == "" || mNit == "C/F" || mNit == "C F") mCommand.Parameters["@Nit"].Value = "CF";

                    mCommand.Parameters["@Cliente"].Value = mCliente;
                    mCommand.Parameters["@Nombre"].Value = mNombreCliente.ToUpper();
                    mCommand.Parameters["@Direccion"].Value = mDireccion.ToUpper();
                    mCommand.Parameters["@Departamento"].Value = mDepartamento.ToUpper();
                    mCommand.Parameters["@Municipio"].Value = mMunicipio.ToUpper();
                    mCommand.Parameters["@Fecha"].Value = mFechaRegistro;
                    mCommand.Parameters["@DireccionCompleta"].Value = string.Format("{0}, {1}, {2}", mDireccion.ToUpper(), mMunicipio.ToUpper(), mDepartamento.ToUpper());
                    mCommand.Parameters["@Descripcion"].Value = "";

                    mCommand.CommandText = "SELECT COUNT(NIT) FROM crediplus.NIT WHERE NIT = @Nit";
                    int mCuantos = Convert.ToInt32(mCommand.ExecuteScalar());

                    if (mCuantos == 0)
                    {
                        mCommand.CommandText = "INSERT INTO crediplus.NIT ( NIT, RAZON_SOCIAL, TIPO, NoteExistsFlag, ACTIVO ) " +
                            "VALUES ( @Nit, @Nombre, 'ND', 0, 'S' ) ";
                        mCommand.ExecuteNonQuery();
                    }

                    mCommand.CommandText = "SELECT COUNT(CLIENTE) FROM crediplus.CLIENTE WHERE CLIENTE = @Cliente";
                    mCuantos = Convert.ToInt32(mCommand.ExecuteScalar());

                    if (mCuantos == 0)
                    {
                        mCommand.CommandText = "SELECT COALESCE(MAX(DETALLE_DIRECCION), 0) + 1 FROM crediplus.DETALLE_DIRECCION";
                        int mDetalleDireccion = Convert.ToInt32(mCommand.ExecuteScalar());

                        mCommand.CommandText = "INSERT INTO crediplus.DETALLE_DIRECCION ( DETALLE_DIRECCION, DIRECCION, CAMPO_1, CAMPO_6, CAMPO_7 ) " +
                            "VALUES ( " + mDetalleDireccion.ToString() + ", 'DETALLADA', @Direccion, @Departamento, @Municipio ) ";
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = "INSERT INTO crediplus.CLIENTE ( CLIENTE, NOMBRE, DETALLE_DIRECCION, ALIAS, CONTACTO, CARGO, DIRECCION, DIR_EMB_DEFAULT, TELEFONO1, TELEFONO2, FAX, CONTRIBUYENTE, FECHA_INGRESO, " +
                                "MULTIMONEDA, MONEDA, SALDO, SALDO_LOCAL, SALDO_DOLAR, SALDO_CREDITO, SALDO_NOCARGOS, LIMITE_CREDITO, EXCEDER_LIMITE, TASA_INTERES, TASA_INTERES_MORA, " +
                                "FECHA_ULT_MORA, FECHA_ULT_MOV, CONDICION_PAGO, NIVEL_PRECIO, DESCUENTO, MONEDA_NIVEL, ACEPTA_BACKORDER, PAIS, ZONA, RUTA, VENDEDOR, COBRADOR, " +
                                "ACEPTA_FRACCIONES, ACTIVO, EXENTO_IMPUESTOS, EXENCION_IMP1, EXENCION_IMP2, COBRO_JUDICIAL, CATEGORIA_CLIENTE, CLASE_ABC, DIAS_ABASTECIMIEN, USA_TARJETA, TIPO_TARJETA, E_MAIL, REQUIERE_OC, ES_CORPORACION, " +
                                "REGISTRARDOCSACORP, USAR_DIREMB_CORP, APLICAC_ABIERTAS, VERIF_LIMCRED_CORP, USAR_DESC_CORP, DOC_A_GENERAR, TIENE_CONVENIO, NOTAS, DIAS_PROMED_ATRASO, ASOCOBLIGCONTFACT, USAR_PRECIOS_CORP, " +
                                "USAR_EXENCIMP_CORP, AJUSTE_FECHA_COBRO, CLASE_DOCUMENTO, LOCAL, TIPO_CONTRIBUYENTE, ACEPTA_DOC_ELECTRONICO, CONFIRMA_DOC_ELECTRONICO, EMAIL_DOC_ELECTRONICO, ACEPTA_DOC_EDI, NOTIFICAR_ERROR_EDI, " +
                                "MOROSO, MODIF_NOMB_EN_FAC, SALDO_TRANS, SALDO_TRANS_LOCAL, SALDO_TRANS_DOLAR, PERMITE_DOC_GP, PARTICIPA_FLUJOCAJA, CURP, USUARIO_CREACION, FECHA_HORA_CREACION, USUARIO_ULT_MOD, FCH_HORA_ULT_MOD, " +
                                "EMAIL_DOC_ELECTRONICO_COPIA, DETALLAR_KITS, NoteExistsFlag ) " +
                                "VALUES ( @Cliente, @Nombre, " + mDetalleDireccion.ToString() + ", @Nombre, '', '', @DireccionCompleta, 1, '', '', '', @Nit, @Hoy, " +
                                "'S', 'GTQ', " + mTotal + ", " + mTotal + ", " + mTotalDolar + ", 0, 0, 0, 'S', 0, 0, " +
                                "@FechaUltima, @Hoy, 0, 'ContadoEfect', 0, 'L', 'S', 'GUA', '0101', 'ND', 'ND', 'ND', " +
                                "'N', 'S', 'N', 0, 0, 'N', 'ND', 'A', 15, 'S', 'ND', '" + mEmail + "', 'N', 'S', " +
                                "'N', 'N', 'N', 'N', 'N', 'F', 'N', '', 0, 'S', 'N', " +
                                "'N', 'P', 'N', 'L', 'F', 'N', 'N', '', 'N', 'N', " +
                                "'N', 'N', 0, 0, 0, 'N', 'N', '', 'sa', GETDATE(), 'sa', GETDATE(), " +
                                "'', 'N', 0 ) ";
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = "INSERT INTO crediplus.CLIENTE_VENDEDOR ( CLIENTE, VENDEDOR, NoteExistsFlag ) VALUES ( @Cliente, 'ND', 0 )";
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = "INSERT INTO crediplus.DIRECC_EMBARQUE ( CLIENTE, DIRECCION, DETALLE_DIRECCION, DESCRIPCION ) " +
                            "VALUES ( @Cliente, '1', " + mDetalleDireccion.ToString() + ", @DireccionCompleta ) ";
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = "UPDATE crediplus.CONSECUTIVO SET ULTIMO_VALOR = @Cliente WHERE CONSECUTIVO = 'CLIENTES'";
                        mCommand.ExecuteNonQuery();
                    }
                    else
                    {
                        mCommand.CommandText = "SELECT DETALLE_DIRECCION FROM crediplus.CLIENTE WHERE CLIENTE = @Cliente";
                        int mDetalleDireccion = Convert.ToInt32(mCommand.ExecuteScalar());

                        mCommand.CommandText = "UPDATE crediplus.DETALLE_DIRECCION SET CAMPO_1 = @Direccion, CAMPO_6 = @Departamento, CAMPO_7 = @Municipio WHERE DETALLE_DIRECCION = " + mDetalleDireccion.ToString() + " AND DIRECCION = 'DETALLADA'";
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = "UPDATE crediplus.CLIENTE SET SALDO = SALDO + " + mTotal.ToString() + ", SALDO_LOCAL = SALDO_LOCAL + " + mTotal.ToString() + ", SALDO_DOLAR = SALDO_DOLAR + " + mTotalDolar.ToString() + ", DIRECCION = @DireccionCompleta WHERE CLIENTE = @Cliente";
                        mCommand.ExecuteNonQuery();
                    }

                    mNumeroPedido = mNumeroPedido + 1;
                    string mPedido = Convert.ToString(mNumeroPedido);

                    switch (mPedido.Length)
                    {
                        case 1:
                            mPedido = string.Format("P000000{0}", mPedido);
                            break;
                        case 2:
                            mPedido = string.Format("P00000{0}", mPedido);
                            break;
                        case 3:
                            mPedido = string.Format("P0000{0}", mPedido);
                            break;
                        case 4:
                            mPedido = string.Format("P000{0}", mPedido);
                            break;
                        case 5:
                            mPedido = string.Format("P00{0}", mPedido);
                            break;
                        case 6:
                            mPedido = string.Format("P0{0}", mPedido);
                            break;
                        case 7:
                            mPedido = string.Format("P{0}", mPedido);
                            break;
                    }

                    mCommand.CommandText = "INSERT INTO crediplus.PEDIDO ( PEDIDO, ESTADO, FECHA_PEDIDO, FECHA_PROMETIDA, FECHA_PROX_EMBARQU, FECHA_ULT_EMBARQUE, FECHA_ULT_CANCELAC, FECHA_ORDEN, TARJETA_CREDITO, EMBARCAR_A, DIREC_EMBARQUE, " +
                        "DIRECCION_FACTURA, OBSERVACIONES, TOTAL_MERCADERIA, MONTO_ANTICIPO, MONTO_FLETE, MONTO_SEGURO, MONTO_DOCUMENTACIO, TIPO_DESCUENTO1, TIPO_DESCUENTO2, MONTO_DESCUENTO1, MONTO_DESCUENTO2, PORC_DESCUENTO1, " +
                        "PORC_DESCUENTO2, TOTAL_IMPUESTO1, TOTAL_IMPUESTO2, TOTAL_A_FACTURAR, PORC_COMI_VENDEDOR, PORC_COMI_COBRADOR, TOTAL_CANCELADO, TOTAL_UNIDADES, IMPRESO, FECHA_HORA, DESCUENTO_VOLUMEN, TIPO_PEDIDO, MONEDA_PEDIDO, " +
                        "VERSION_NP, AUTORIZADO, DOC_A_GENERAR, CLASE_PEDIDO, MONEDA, NIVEL_PRECIO, COBRADOR, RUTA, USUARIO, CONDICION_PAGO, BODEGA, ZONA, VENDEDOR, CLIENTE, CLIENTE_DIRECCION, CLIENTE_CORPORAC, CLIENTE_ORIGEN, " +
                        "PAIS, SUBTIPO_DOC_CXC, TIPO_DOC_CXC, BACKORDER, PORC_INTCTE, DESCUENTO_CASCADA, FIJAR_TIPO_CAMBIO, ORIGEN_PEDIDO, DESC_DIREC_EMBARQUE, BASE_IMPUESTO1, BASE_IMPUESTO2, NOMBRE_CLIENTE, FECHA_PROYECTADA, " +
                        "TIPO_DOCUMENTO, TASA_IMPOSITIVA_PORC, TASA_CREE1_PORC, TASA_CREE2_PORC, TASA_GAN_OCASIONAL_PORC, CONTRATO_REVENTA ) " +
                        "VALUES ( '" + mPedido + "', 'F', @Fecha, @Fecha, @Fecha, @Fecha, @Fecha, @Fecha, '', @Cliente, 1, " +
                        "@DireccionCompleta, '', " + mValor + ", 0, 0, 0, 0, 'P', 'P', 0, 0, 0, " +
                        "0, " + mIva + ", 0, " + mTotal + ", 0, 0, 0, 0, 'N', GETDATE(), 0, 'N', 'L', " +
                        "1, 'N', 'F', 'N', 'L', 'CONTADOEFECT', 'ND', 'ND', 'sa', 0, 'F01', '0101', 'ND', @Cliente, @Cliente, @Cliente, @Cliente, " +
                        "'GUA', 0, 'FAC', 'C', 0 , 'S', 'N', 'F', @DireccionCompleta, " + mValor + ", 0, @Nombre, @Hoy, " +
                        "'P', 0, 0, 0, 0, 'N' ) ";
                    mCommand.ExecuteNonQuery();

                    mCommand.CommandText = $"UPDATE crediplus.CONSECUTIVO_FA SET VALOR_CONSECUTIVO = '{mPedido}' WHERE CODIGO_CONSECUTIVO = '02'";
                    mCommand.ExecuteNonQuery();

                    mNumeroFactura++;
                    int mNumeroFacturaLength = Convert.ToString(mNumeroFactura).Length;
                    int mConsecutivoFALength = mConsecutivoFA[1].Length;

                    mCeros = "";
                    for (int ii = Convert.ToString(mNumeroFactura).Length; ii < mConsecutivoFA[1].Length; ii++)
                    {
                        mCeros = string.Format("0{0}", mCeros);
                    }

                    mFactura = string.Format("{0}-{1}{2}", mConsecutivoFA[0], mCeros, Convert.ToString(mNumeroFactura));

                    mCommand.CommandText = "INSERT INTO crediplus.AUDIT_TRANS_INV ( USUARIO, FECHA_HORA, MODULO_ORIGEN, APLICACION, REFERENCIA ) " +
                        "VALUES ( 'sa', GETDATE(), 'FA', 'FAC#" + mFactura + "', 'Cliente: " + mCliente + "' ) ";
                    mCommand.ExecuteNonQuery();

                    mCommand.CommandText = "SELECT @@IDENTITY";
                    Int64 mAudit = Convert.ToInt64(mCommand.ExecuteScalar());

                    bool mGrabarFactura = true;
                    if (mFacElectronica == "S")
                    {
                        string mXML = mUtilitariosAtid.xmlCrediplus(mCliente, false, "N");
                        string mXMLReversion = mUtilitariosAtid.xmlCrediplus(mCliente, false, "S");

                        log.Info(string.Format("Firmando factura crediplus - Maquina: {0} XML: {1}", MF_Clases.Utils.CONSTANTES.GUATEFAC.MAQUINA_CREDIPLUS.FACTURACION_PHOENIX, mXML));

                        Clases.RespuestaGuateFacturas mRespuesta = new RespuestaGuateFacturas();
                        mRespuesta = mUtilitariosAtid.firmarFactura(mXML, MF_Clases.Utils.CONSTANTES.GUATEFAC.MAQUINA_CREDIPLUS.FACTURACION_PHOENIX);

                        log.Info(string.Format("Respuesta firma crediplus, Firmada: {0}, Firma: {1}, Factura: {2}, XmlRespuesta: {3}", mRespuesta.firmada, mRespuesta.firma, mRespuesta.factura, mRespuesta.xml));

                        mGrabarFactura = false;
                        if (mRespuesta.firmada == "S")
                        {
                            mFirmadas++;
                            mGrabarFactura = true;
                            mFactura = mRespuesta.factura;
                        }

                        mCommand.Parameters["@DireccionDevuelveGFace"].Value = mRespuesta.direccion;
                        mCommand.Parameters["@error"].Value = mRespuesta.error;
                        mCommand.Parameters["@XMLDocumento"].Value = mXML.Length <= 4999 ? mXML : mXML.Substring(0, 4999);
                        mCommand.Parameters["@NombreDevuelveGFace"].Value = mRespuesta.nombre;

                        mCommand.Parameters["@XMLReversion"].Value = mXMLReversion.Length <= 4999 ? mXMLReversion : mXMLReversion.Substring(0, 4999);
                        mCommand.Parameters["@XMLRespuesta"].Value = mRespuesta.xml.Length <= 3999 ? mRespuesta.xml : mRespuesta.xml.Substring(0, 3999);
                        mMensaje = string.Format(" {0} # {1} # {2} # {3} # {4} ", mMensaje, mRespuesta.direccion, mRespuesta.error, "", "", "");

                        mCommand.CommandText = $"SELECT COUNT(FACTURA) FROM crediplus.CP_Factura WHERE FACTURA = '{mFactura}'";
                        if (Convert.ToInt32(mCommand.ExecuteScalar()) == 0)
                        {
                            mCommand.CommandText = "INSERT INTO crediplus.CP_Factura ( FACTURA, FACTURA_ELECTRONICA, CLIENTE, FECHA, FIRMADA, FIRMA, SERIE, NUMERO, " +
                                "IDENTIFICADOR, MAQUINA, NOMBRE, DIRECCION, TELEFONO, ERROR, XML_DOCUMENTO, XML_REVERSION, XML_RESPUESTA, " +
                                "FECHA_REGISTRO, ID_CREDIPLUS, CLIENTE_CREDIPLUS, CREDITO_CREDIPLUS, NOMBRE_CREDIPLUS, VALOR_CREDIPLUS, IVA_CREDIPLUS, TOTAL_CREDIPLUS, FECHA_REGISTRO_CREDIPLUS ) " +
                                "VALUES ( '" + mFactura + "', '" + mRespuesta.facturaElectronica + "', @Cliente, @Fecha, '" + mRespuesta.firmada + "', '" + mRespuesta.firma + "', '" + mRespuesta.serie + "', '" + mRespuesta.numero + "', " +
                                "'" + mRespuesta.identificador + "', '" + mRespuesta.maquina + "', @NombreDevuelveGFace, @DireccionDevuelveGFace, '" + mRespuesta.telefono + "', @error, @XMLDocumento, @XMLReversion, @XMLRespuesta, " +
                                "GETDATE(), 0, '" + mCliente + "', '" + mCredito + "', @Nombre, " + mValor.ToString() + ", " + mIva.ToString() + ", " + mTotal.ToString() + ", @Fecha ) ";
                            mCommand.ExecuteNonQuery();
                        }

                        if (mRespuesta.firmada == "N")
                        {
                            foreach (DataRow rowC in dtFacturasCliente.Rows)
                            {
                                string mClienteRow = Convert.ToString(rowC["CLIENTE"]);
                                string mFacturaExactusRow = Convert.ToString(rowC["FACTURA"]);

                                mCommand.CommandText = $"UPDATE crediplus.CP_Factura_Phoenix SET EN_EXACTUS = 'S', FACTURA_EXACTUS = '{mFacturaExactusRow}' WHERE CLIENTE = '{mClienteRow}' AND EN_EXACTUS = 'N'";
                                mCommand.ExecuteNonQuery();
                            }

                            mTransaction.Commit();
                            mConexion.Close();

                            mRetorna = string.Format("Se cargaron exitosamente {0} facturas, se firmaron {1}, la últma factura firmada fue la {2}, en el cliente {3} se presentó el error {4}", mCuantasFacturas, mFirmadas, mUltimaFactura, mCliente, mRespuesta.error);
                            EnviarError(mRetorna);
                            log.Error(mRetorna);
                            return mRetorna;
                        }
                    }

                    mCommand.CommandText = $"SELECT COUNT(FACTURA) FROM crediplus.FACTURA WHERE FACTURA = '{mFactura}'";
                    if (Convert.ToInt32(mCommand.ExecuteScalar()) > 0) mGrabarFactura = false;

                    if (mGrabarFactura)
                    {
                        mCommand.CommandText = "INSERT INTO crediplus.FACTURA ( TIPO_DOCUMENTO, FACTURA, AUDIT_TRANS_INV, ESTA_DESPACHADO, EN_INVESTIGACION, TRANS_ADICIONALES, ESTADO_REMISION, DESCUENTO_VOLUMEN, MONEDA_FACTURA, FECHA_DESPACHO, " +
                            "CLASE_DOCUMENTO, FECHA_RECIBIDO, PEDIDO, COMISION_COBRADOR, TARJETA_CREDITO, TOTAL_VOLUMEN, TOTAL_PESO, MONTO_COBRADO, TOTAL_IMPUESTO1, FECHA, FECHA_ENTREGA, TOTAL_IMPUESTO2, PORC_DESCUENTO2, MONTO_FLETE, MONTO_SEGURO, " +
                            "MONTO_DOCUMENTACIO, TIPO_DESCUENTO1, TIPO_DESCUENTO2, MONTO_DESCUENTO1, MONTO_DESCUENTO2, PORC_DESCUENTO1, TOTAL_FACTURA, FECHA_PEDIDO, FECHA_HORA_ANULA, FECHA_ORDEN, TOTAL_MERCADERIA, COMISION_VENDEDOR, FECHA_HORA, " +
                            "TOTAL_UNIDADES, NUMERO_PAGINAS, TIPO_CAMBIO, ANULADA, MODULO, CARGADO_CG, CARGADO_CXC, EMBARCAR_A, DIREC_EMBARQUE, DIRECCION_FACTURA, MULTIPLICADOR_EV, OBSERVACIONES, VERSION_NP, MONEDA, NIVEL_PRECIO, COBRADOR, RUTA, " +
                            "USUARIO, USUARIO_ANULA, CONDICION_PAGO, ZONA, VENDEDOR, CLIENTE_DIRECCION, CLIENTE_CORPORAC, CLIENTE_ORIGEN, CLIENTE, PAIS, SUBTIPO_DOC_CXC, TIPO_DOC_CXC, MONTO_ANTICIPO, TOTAL_PESO_NETO, FECHA_RIGE, PORC_INTCTE, " +
                            "USA_DESPACHOS, COBRADA, DESCUENTO_CASCADA, DIRECCION_EMBARQUE, CONSECUTIVO, REIMPRESO, BASE_IMPUESTO1, BASE_IMPUESTO2, NOMBRE_CLIENTE, NOMBREMAQUINA, SERIE_RESOLUCION, CONSEC_RESOLUCION, GENERA_DOC_FE, " +
                            "TASA_IMPOSITIVA_PORC, TASA_CREE1_PORC, TASA_CREE2_PORC, TASA_GAN_OCASIONAL_PORC, COMENTARIO_CXC ) " +
                            "VALUES ( 'F', '" + mFactura + "', " + mAudit + ", 'N', 'N', 'N', 'N', 0, 'L', @Fecha, " +
                            "'N', @Fecha, '" + mPedido + "', 0, '', 1, 1, 0, " + mIva.ToString() + ", @Fecha, @Fecha, 0, 0, 0, 0, " +
                            "0, 'P', 'P', 0, 0, 0, " + mTotal.ToString() + ", @Fecha, NULL, @Fecha, " + mValor.ToString() + ", 0, GETDATE(), " +
                            "1, 1, " + mTipoCambio.ToString() + ", 'N', 'FA', 'N', 'S', @Cliente, 1, @DireccionCompleta, 1, '', 1, 'L', 'CONTADOEFECT', 'ND', 'ND', " +
                            "'sa', NULL, 0, 001010, 'ND', @Cliente, @Cliente, @Cliente, @Cliente, 'GUA', 0, 'FAC', 0, 1, @Fecha, 0, " +
                            "'N', 'S', 'S', @DireccionCompleta, '" + mConsecutivo + "', 0, " + mValor.ToString() + ", 0, @Nombre, '', '" + mConsecutivoFA[0] + "', " + Convert.ToString(mNumeroFactura) + ", 'N', " +
                            "0, 0, 0, 0, '" + mReferencia + "' ) ";
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = "INSERT INTO crediplus.DOCUMENTOS_CC ( DOCUMENTO, TIPO, APLICACION, FECHA_DOCUMENTO, FECHA, MONTO, SALDO, MONTO_LOCAL, SALDO_LOCAL, MONTO_DOLAR, " +
                            "SALDO_DOLAR, MONTO_CLIENTE, SALDO_CLIENTE, TIPO_CAMBIO_MONEDA, TIPO_CAMBIO_DOLAR, TIPO_CAMBIO_CLIENT, TIPO_CAMB_ACT_LOC, TIPO_CAMB_ACT_DOL, TIPO_CAMB_ACT_CLI, SUBTOTAL, DESCUENTO, IMPUESTO1, IMPUESTO2, RUBRO1, RUBRO2, " +
                            "MONTO_RETENCION, SALDO_RETENCION, DEPENDIENTE, FECHA_ULT_CREDITO, CARGADO_DE_FACT, APROBADO, ASIENTO_PENDIENTE, FECHA_ULT_MOD, NOTAS, CLASE_DOCUMENTO, FECHA_VENCE, NUM_PARCIALIDADES, COBRADOR, USUARIO_ULT_MOD, CONDICION_PAGO, " +
                            "MONEDA, VENDEDOR, CLIENTE_REPORTE, CLIENTE_ORIGEN, CLIENTE, SUBTIPO, PORC_INTCTE, FECHA_ANUL, AUD_USUARIO_ANUL, AUD_FECHA_ANUL, USUARIO_APROBACION, FECHA_APROBACION, ANULADO, PAIS, BASE_IMPUESTO1, BASE_IMPUESTO2, " +
                            "DEPENDIENTE_GP, SALDO_TRANS, SALDO_TRANS_LOCAL, SALDO_TRANS_DOLAR, SALDO_TRANS_CLI, FACTURADO, GENERA_DOC_FE ) " +
                            "VALUES ( '" + mFactura + "', 'FAC', 'Cargado de factura: " + mFactura + "', @Fecha, @Fecha, " + mTotal.ToString() + ", " + mTotal.ToString() + ", " + mTotal.ToString() + ", " + mTotal.ToString() + ", " + mTotalDolar + ", " +
                            mTotalDolar + ", " + mTotal + ", " + mTotal + ", 1, " + mTipoCambio.ToString() + ", 1, 1, " + mTipoCambio.ToString() + ", 1, " + mValor.ToString() + ", 0, " + mIva.ToString() + ", 0, 0, 0, " +
                            "0, 0, 'S', '1980-01-01', 'S', 'S', 'S', GETDATE(), '', 'N', @Fecha, 0, 'ND', 'sa', 0, " +
                            "'GTQ', 'ND', @Cliente, @Cliente, @Cliente, 0, 0, NULL, NULL, NULL, 'sa', @Fecha, 'N', 'GUA', " + mValor.ToString() + ", 0, " +
                            "'N', 0, 0, 0, 0, 'N', 'N' ) ";
                        mCommand.ExecuteNonQuery();

                        dtLineasCliente.Clear();
                        DataRow[] mRowsFacturas = dtLinea.Select("CLIENTE = '" + mCliente + "'", "CLIENTE, ID_FACTURA, ID");

                        for (int ii = 0; ii < mRowsFacturas.Length; ii++)
                        {
                            DataRow mRowFactura = dtLineasCliente.NewRow();
                            for (int jj = 0; jj < dtLineasCliente.Columns.Count; jj++)
                            {
                                mRowFactura[jj] = mRowsFacturas[ii][jj];
                            }
                            dtLineasCliente.Rows.Add(mRowFactura);
                        }

                        DataTable dtProductos = new DataTable();
                        DataView dvProductos = dtLineasCliente.DefaultView;

                        dtProductos = dvProductos.ToTable(true, "DESCRIPCION");

                        int mLinea = 0;
                        foreach (DataRow producto in dtProductos.Rows)
                        {
                            mLinea++;
                            string mArticulo = "00000002"; string mDescripcion = Convert.ToString(producto["DESCRIPCION"]);

                            try
                            {
                                int kk = mCredito.Length;
                                mCredito = string.Format(" Préstamo No. {0}", mCredito);
                            }
                            catch
                            {
                                mCredito = "";
                            }

                            mCommand.Parameters["@Descripcion"].Value = string.Format("{0}{1}", mDescripcion, mCredito);
                            if (mDescripcion.ToLower().Contains("inter")) mArticulo = "00000001";
                            if (mDescripcion.ToLower().Contains("varios")) mArticulo = "00000003";

                            decimal mValorProducto = Convert.ToDecimal(dtLineasCliente.Compute("SUM(VALOR)", "DESCRIPCION = '" + mDescripcion + "'"));
                            decimal mIvaProducto = Convert.ToDecimal(dtLineasCliente.Compute("SUM(IVA)", "DESCRIPCION = '" + mDescripcion + "'"));

                            mCommand.CommandText = "INSERT INTO crediplus.PEDIDO_LINEA ( PEDIDO, PEDIDO_LINEA, BODEGA, ARTICULO, ESTADO, FECHA_ENTREGA, LINEA_USUARIO, PRECIO_UNITARIO, CANTIDAD_PEDIDA, CANTIDAD_A_FACTURA, " +
                                "CANTIDAD_FACTURADA, CANTIDAD_RESERVADA, CANTIDAD_BONIFICAD, CANTIDAD_CANCELADA, TIPO_DESCUENTO, MONTO_DESCUENTO, PORC_DESCUENTO, DESCRIPCION, COMENTARIO, FECHA_PROMETIDA ) " +
                                "VALUES ( '" + mPedido + "', " + mLinea.ToString() + ", 'F01'," + "'" + mArticulo + "', 'F', @Fecha, " + (mLinea - 1).ToString() + ", " + mValorProducto.ToString() + ", 1, 0, " +
                                "1, 0, 0, 0, 'P', 0, 0, @Descripcion, '', @Fecha )";
                            mCommand.ExecuteNonQuery();

                            mCommand.CommandText = "INSERT INTO crediplus.FACTURA_LINEA ( FACTURA, TIPO_DOCUMENTO, LINEA, BODEGA, COSTO_TOTAL_DOLAR, PEDIDO, ARTICULO, ANULADA, FECHA_FACTURA, CANTIDAD, PRECIO_UNITARIO, TOTAL_IMPUESTO1, TOTAL_IMPUESTO2, " +
                                "DESC_TOT_LINEA, DESC_TOT_GENERAL, COSTO_TOTAL, PRECIO_TOTAL, DESCRIPCION, COMENTARIO, CANTIDAD_DEVUELT, DESCUENTO_VOLUMEN, TIPO_LINEA, CANTIDAD_ACEPTADA, CANT_NO_ENTREGADA, COSTO_TOTAL_LOCAL, PEDIDO_LINEA, " +
                                "MULTIPLICADOR_EV, CANT_DESPACHADA, COSTO_ESTIM_LOCAL, COSTO_ESTIM_DOLAR, CANT_ANUL_PORDESPA, MONTO_RETENCION, BASE_IMPUESTO1, BASE_IMPUESTO2, COSTO_TOTAL_COMP, COSTO_TOTAL_COMP_LOCAL, " +
                                "COSTO_TOTAL_COMP_DOLAR, COSTO_ESTIM_COMP_LOCAL, COSTO_ESTIM_COMP_DOLAR, CANT_DEV_PROCESO ) " +
                                "VALUES ( '" + mFactura + "', 'F', " + (mLinea - 1).ToString() + ", 'F01', 0, '" + mPedido + "', '" + mArticulo + "', 'N', @Fecha, 1, " + mValorProducto.ToString() + ", " + mIvaProducto.ToString() + ", 0, " +
                                "0, 0, 0, " + mValorProducto.ToString() + ", @Descripcion, '', 0, 0, 'N', 0, 0, 0, " + mLinea.ToString() + ", " +
                                "1, 0, 0, 0, 0, 0, " + mValorProducto.ToString() + ", 0, 0, 0, " +
                                "0, 0, 0, 0 ) ";
                            mCommand.ExecuteNonQuery();
                        }

                        DataRow mRowFacturaCliente = dtFacturasCliente.NewRow();
                        mRowFacturaCliente["CLIENTE"] = mCliente;
                        mRowFacturaCliente["FACTURA"] = mFactura;
                        dtFacturasCliente.Rows.Add(mRowFacturaCliente);

                        mCommand.CommandText = "SELECT COUNT(CLIENTE) FROM crediplus.SALDO_CLIENTE WHERE CLIENTE = @Cliente";
                        if (Convert.ToInt32(mCommand.ExecuteScalar()) == 0)
                        {
                            mCommand.CommandText = "INSERT INTO crediplus.SALDO_CLIENTE ( CLIENTE, MONEDA, SALDO, SALDO_CORPORACION, SALDO_SUCURSALES, FECHA_ULT_MOV ) " +
                                "VALUES ( @Cliente, 'GTQ', " + mTotal.ToString() + ", 0, 0, @Hoy ) ";
                            mCommand.ExecuteNonQuery();
                        }
                        else
                        {
                            mCommand.CommandText = "UPDATE crediplus.SALDO_CLIENTE SET SALDO = SALDO + " + mTotal.ToString() + " WHERE CLIENTE = @Cliente";
                            mCommand.ExecuteNonQuery();
                        }

                        if (mFacElectronica == "N")
                        {
                            mCommand.CommandText = $"UPDATE crediplus.CONSECUTIVO_FA SET VALOR_CONSECUTIVO = '{mFactura}' WHERE CODIGO_CONSECUTIVO = '{mConsecutivo}'";
                            mCommand.ExecuteNonQuery();

                            if (mFactura == mUltimoValor)
                            {
                                mNumeroFactura = mNumeroFacturaBk;
                                mConsecutivo = mConsecutivoBK;

                                mCommand.CommandText = $"UPDATE crediplus.COBRADOR SET E_MAIL = '{mConsecutivo},' WHERE COBRADOR = 'ND'";
                                mCommand.ExecuteNonQuery();

                                EnviarError("Se utilizó el consecutivo de BackUP de crediplus, es urgente agregar un consecutivo de BackUP nuevo.", "BK");
                                
                            }
                        }
                    }

                    mUltimaFactura = mFactura;
                }

                foreach (DataRow row in dtFacturasCliente.Rows)
                {
                    string mCliente = Convert.ToString(row["CLIENTE"]);
                    string mFacturaExactus = Convert.ToString(row["FACTURA"]);

                    mCommand.CommandText = $"UPDATE crediplus.CP_Factura_Phoenix SET EN_EXACTUS = 'S', FACTURA_EXACTUS = '{mFacturaExactus}' WHERE CLIENTE = '{mCliente}' AND EN_EXACTUS = 'N'";
                    mCommand.ExecuteNonQuery();
                }

                if ((mDisponibles - mCuantasFacturas) <= 25) EnviarError("Se le informa que solamente quedan menos de 25 facturas disponibles en crediplus", "25");

                mTransaction.Commit();
                mConexion.Close();

                if (mCuantasFacturas == 0)
                {
                    mRetorna = "Error, no se encontraron facturas para cargar";
                    log.Debug(mRetorna);
                }
                else
                {
                    mRetorna = string.Format("Se cargaron exitosamente {0} facturas", mCuantasFacturas);
                    if (mFacElectronica == "S") mRetorna = string.Format("{0} y se firmaron {1}", mRetorna, mFirmadas);

                    log.Debug(mRetorna);
                }

            }
            catch (Exception ex)
            {
                if (mConexion.State == ConnectionState.Open)
                {
                    mTransaction.Rollback();
                    mConexion.Close();
                }

                mRetorna = string.Format("{0} {1} {2}", CatchClass.ExMessage(ex, "FacturasAtidController", "GET"), mMensaje, mCommand.CommandText);
                EnviarError(mRetorna);
                log.Error(mRetorna);
            }

            return mRetorna;
        }

        private void EnviarError(string mensaje, string conta = "")
        {
            using (SmtpClient smtp = new SmtpClient())
            {
                using (MailMessage mail = new MailMessage())
                {
                    string mAsunto = "Error al cargar facturas ATID";

                    string mNombreUsuario = "Punto de Venta";
                    string mCuerpo = mensaje;
                    string mMailTo = WebConfigurationManager.AppSettings["ITMailNotification"].ToString();
                    string mMailUsuario = "puntodeventa@productosmultiples.com";

                    if (conta == "")
                    {
                        mail.To.Add(mMailTo);
                    }
                    else
                    {
                        var q = from c in db.MF_Catalogo where c.CODIGO_TABLA == 28 select c;
                        foreach (var item in q)
                        {
                            mail.To.Add(item.NOMBRE_CAT);
                        }

                        mAsunto = "Facturas disponibles de crediplus";
                        if (conta == "BK") mAsunto = "Consecutivo de BackUP de crediplus";
                    }

                    mail.Subject = mAsunto;

                    mail.ReplyToList.Add(mMailUsuario);
                    mail.From = new MailAddress(mMailUsuario, mNombreUsuario);
                    mail.Body = mCuerpo;

                    smtp.Host = WebConfigurationManager.AppSettings["MailHost2"];
                    smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort2"]);
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                    try
                    {
                        smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail2"], WebConfigurationManager.AppSettings["PwdMail2"]);
                        smtp.Send(mail);
                    }
                    catch
                    {
                        //Nothing
                    }
                }
            }
        }


        private void NotificarFactura(List<ArticuloCrediplus> articulos, string mensaje, bool hayError = false)
        {
            articulos = articulos == null ? new List<ArticuloCrediplus>() : articulos;
            string remitente = "facturas@mueblesfiesta.com";
            string asunto = "Proceso facturación Crediplus -> Muebles fiesta";
            string displayName = "Facturación Crediplus";

            if (General.Ambiente != "PRO")
            {
                displayName = displayName + " [Ambiente de pruebas]";
                displayName = displayName + " [Ambiente de pruebas]";
            }

            List<string> destinatarios = hayError ? DalCP.DestinatariosInformatica() : DalCP.ObtenerDestinatariosFacturacionCrediplus();
            StringBuilder body = mensajeNotificacion(articulos, mensaje);

            Utilitario.EnviarEmail(remitente, destinatarios, asunto, body, displayName, null, null, null);
        }

        private StringBuilder mensajeNotificacion(List<ArticuloCrediplus> articulos, string mensaje)
        {
            articulos = articulos == null ? new List<ArticuloCrediplus>() : articulos;
            string htmlProductos = "", htmlTabla = "";

            if (articulos.Count > 0)
            {
                articulos.ForEach(x => {
                    htmlProductos = htmlProductos +
                        " <tr>" +
                            " <td style='border-right: solid 1px;padding: 10px 30px; text-align: center; font-size: 14px;'>" + x.Cantidad + "</td>" +
                            " <td style='border-right: solid 1px;padding: 10px 30px; font-size: 14px;'>" + x.CodigoMF + "</td>" +
                            " <td style='border-right: solid 1px;padding: 10px 30px; font-size: 14px;'>" + x.CodigoCP + "</td>" +
                            " <td style='border-right: solid 1px;padding: 10px 30px; font-size: 14px;'>" + x.Descripcion + "</td>" +
                            " <td style='border-right: none;padding: 10px 30px; font-size: 14px;'>" + string.Format("{0:#.00}", x.PrecioMF) + "</td>" +
                        " </tr>";
                });

                htmlTabla = " <p>" +
                                        " <table style='border: solid 1px;border-spacing: 0;border-collapse: 0;font-size: 16px;'>" +
                                            " <tbody>" +
                                                " <tr>" +
                                                    " <th style='border-bottom: solid 1px;border-right: solid 1px;padding: 10px 30px;'>Cantidad</th>" +
                                                    " <th style='border-bottom: solid 1px;border-right: solid 1px;padding: 10px 30px;'>Código MF</th>" +
                                                    " <th style='border-bottom: solid 1px;border-right: solid 1px;padding: 10px 30px;'>Código CP</th>" +
                                                    " <th style='border-bottom: solid 1px;border-right: solid 1px;padding: 10px 30px;'>Artículo</th>" +
                                                    " <th style='border-bottom: solid 1px;border-right: none;padding: 10px 30px;'>Precio</th>" +
                                                " </tr>" +
                                                htmlProductos +
                                            " </tbody>" +
                                        " </table>" +
                                    " </p>";
            }

            string result = "<!doctype html>" +
                        " <html lang='es'>" +
                            " <head>" +
                                " <meta charset='utf-8'>" +
                            " </head>" +
                            " <body>" +
                                " <div style='font-family: arial;font-size: 18px;'>" +
                                     " <p>" + mensaje + "</p>" +
                                    htmlTabla +
                                " </div>" +
                            " </body>" +
                        " </html>";

            return new StringBuilder().Append(result);

        }
    }
}
