﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;

namespace FiestaNETRestServices.Controllers.Contabilidad
{
    public class LibroComprasController : MFApiController
    {

        // GET: api/LibroCompras
        public string Get()
        {
            return "Método temporalmente deshabilitado";
        }

        // GET: api/LibroCompras/5
        public string Get(int id)
        {
            return "Método temporalmente deshabilitado";
        }

        // POST: api/LibroCompras
        public string Post([FromBody]Clases.LibroComprasMod libro)
        {
            string mRetorna = "";
            db.CommandTimeout = 999999999;
            
            try
            {
                if ((from c in db.MF_Catalogo where c.CODIGO_TABLA == 19 && c.NOMBRE_CAT == libro.Usuario select c).Count() == 0) return "Error, Usted no tiene acceso a modificar el libro de compras";

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    //Eliminación de documentos de caja chica y cuentas por pagar
                    foreach (var item in libro.Borrar)
                    {
                        if (item.Modulo == "CH")
                        {
                            List<DOCS_SOPORTE> mDocSoporte = db.DOCS_SOPORTE.Where(x => x.VALE == item.Vale && x.DOC_SOPORTE == item.Doc).ToList();
                            foreach (DOCS_SOPORTE itemBorrar in mDocSoporte) db.DOCS_SOPORTE.DeleteObject(itemBorrar);
                        }
                        else
                        {
                            var qProveedor = from p in db.PROVEEDOR where p.PROVEEDOR1 == item.Proveedor select p;
                            var qDocumento = (from d in db.DOCUMENTOS_CP where d.PROVEEDOR == item.Proveedor && d.DOCUMENTO == item.Doc select d).First();
                            var qAuxiliar = from a in db.AUXILIAR_CP where a.PROVEEDOR == item.Proveedor && a.CREDITO == item.Doc select a;

                            decimal mSaldo = qProveedor.First().SALDO;
                            decimal mSaldoDolar = qProveedor.First().SALDO_DOLAR;
                            decimal mMonto = qDocumento.MONTO;
                            decimal mMontoDolar = qDocumento.MONTO_DOLAR;

                            if (qAuxiliar.Count() > 0)
                            {
                                string mDocAuxiliar = qAuxiliar.First().DEBITO;
                                var qDocumentoAux = from d in db.DOCUMENTOS_CP where d.PROVEEDOR == item.Proveedor && d.DOCUMENTO == mDocAuxiliar select d;

                                foreach (var aux in qDocumentoAux)
                                {
                                    aux.SALDO = aux.SALDO + mMonto;
                                    aux.SALDO_LOCAL = aux.SALDO_LOCAL + mMonto;
                                    aux.SALDO_DOLAR = aux.SALDO_DOLAR + mMontoDolar;
                                }
                            }
                            
                            foreach (var prov in qProveedor)
                            {
                                prov.SALDO = prov.SALDO + mMonto;
                                prov.SALDO_LOCAL = prov.SALDO_LOCAL + mMonto;
                                prov.SALDO_DOLAR = prov.SALDO_LOCAL + mMontoDolar;
                            }

                            List<AUXILIAR_CP> mAuxiliar = db.AUXILIAR_CP.Where(x => x.PROVEEDOR == item.Proveedor && x.CREDITO == item.Doc).ToList();
                            foreach (AUXILIAR_CP itemBorrar in mAuxiliar) db.AUXILIAR_CP.DeleteObject(itemBorrar);
                        }
                    }
                    
                    //Cambio de bien a servicio y viceversa en documentos de caja chica y cuentas por pagar
                    foreach (var item in libro.BienServicio)
                    {
                        Int16 mNuevoSubTipo = 0;
                        if (item.BienServicio.Contains("Bienes")) mNuevoSubTipo = 4;

                        if (item.Modulo == "CH")
                        {
                            var qCH = from d in db.DOCS_SOPORTE where d.VALE == item.Vale && d.DOC_SOPORTE == item.Doc select d;
                            foreach (var ch in qCH)
                            {
                                ch.SUBTIPO = mNuevoSubTipo;
                            }
                        }
                        else
                        {
                            var qCP = from d in db.DOCUMENTOS_CP where d.PROVEEDOR == item.Proveedor && d.DOCUMENTO == item.Doc select d;
                            foreach (var cp in qCP)
                            {
                                cp.SUBTIPO = mNuevoSubTipo;
                            }
                        }
                    }
                    
                    decimal mIva = 1 + ((from im in db.IMPUESTO where im.IMPUESTO1 == "IVA" select im).First().IMPUESTO11 / 100);

                    //Cambio de factura de pequeño contribuyente a factura normal o viceversa a documentos de caja chica y cuentas por pagar
                    foreach (var item in libro.PC)
                    {
                        if (item.Modulo == "CH")
                        {
                            var qCH = from d in db.DOCS_SOPORTE where d.VALE == item.Vale && d.DOC_SOPORTE == item.Doc select d;
                            foreach (var ch in qCH)
                            {
                                if (item.Documento == "FPC")
                                {
                                    ch.IMPUESTO1 = ch.MONTO - (ch.MONTO / mIva);
                                    ch.BASE_IMPUESTO1 = ch.MONTO - (ch.MONTO / mIva);
                                }
                                else
                                {
                                    ch.IMPUESTO1 = 0;
                                    ch.BASE_IMPUESTO1 = 0;
                                }
                            }
                        }
                        else
                        {
                            var qCP = from d in db.DOCUMENTOS_CP where d.PROVEEDOR == item.Proveedor && d.DOCUMENTO == item.Doc select d;
                            foreach (var cp in qCP)
                            {
                                if (item.Documento == "FPC")
                                {
                                    cp.IMPUESTO1 = cp.MONTO - (cp.MONTO / mIva);
                                    cp.BASE_IMPUESTO1 = cp.MONTO - (cp.MONTO / mIva);
                                }
                                else
                                {
                                    cp.IMPUESTO1 = 0;
                                    cp.BASE_IMPUESTO1 = 0;
                                }
                            }
                        }
                    }

                    //Cambio de factura especial a normal o viceversa
                    foreach (var item in libro.FE)
                    {
                        if (item.Modulo == "CH")
                        {
                            var qCH = from d in db.DOCS_SOPORTE where d.VALE == item.Vale && d.DOC_SOPORTE == item.Doc select d;
                            foreach (var ch in qCH)
                            {
                                if (item.Documento == "FE")
                                {
                                    ch.DOC_SOPORTE = string.Format("A-{0}", Convert.ToString(item.Numero));
                                }
                                else
                                {
                                    ch.DOC_SOPORTE = string.Format("A2-{0}", Convert.ToString(item.Numero));
                                }
                            }
                        }
                        else
                        {
                            return "Error, no es posible manipular facturas especiales en cuentas por pagar";
                        }
                    }

                    //Modificación de serie, número, fecha y NIT
                    foreach (var item in libro.Editar)
                    {
                        if (item.Modulo == "CH")
                        {
                            var qCH = from d in db.DOCS_SOPORTE where d.VALE == item.Vale && d.DOC_SOPORTE == item.Doc select d;
                            foreach (var ch in qCH)
                            {
                                int mExisteNIT = (from n in db.NIT where n.NIT1 == item.NIT select n).Count();

                                if (mExisteNIT == 0)
                                {
                                    NIT iNit = new NIT
                                    {
                                        NIT1 = item.NIT,
                                        RAZON_SOCIAL = item.Nombre,
                                        ALIAS = item.Nombre,
                                        NOTAS = "",
                                        TIPO = "ND",
                                        ORIGEN = "O",
                                        ACTIVO = "S",
                                        NoteExistsFlag = 0,
                                        RecordDate = DateTime.Now,
                                        RowPointer = Guid.NewGuid(),
                                        CreatedBy = string.Format("FA/{0}", libro.Usuario),
                                        UpdatedBy = string.Format("FA/{0}", libro.Usuario),
                                        CreateDate = DateTime.Now
                                    };

                                    db.NIT.AddObject(iNit);
                                }


                                ch.DOC_SOPORTE = string.Format("{0}-{1}", item.Serie, Convert.ToString(item.Numero));
                                ch.FECHA = item.Fecha;
                                ch.NIT = item.NIT;
                            }
                        }
                        else
                        {
                            return "Error, no es posible modificar los registros de cuentas por pagar, se le sugiere que elimine el docuento";
                        }
                    }

                    db.SaveChanges();
                    transactionScope.Complete();
                }

                mRetorna = "Libro de compras modificado exitosamente";
            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0} {1}", CatchClass.ExMessage(ex, "LibroComprasController", "PostLibroCompras"), "");
            }

            return mRetorna;
        }

        // PUT: api/LibroCompras/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/LibroCompras/5
        public void Delete(int id)
        {
        }
    }
}
