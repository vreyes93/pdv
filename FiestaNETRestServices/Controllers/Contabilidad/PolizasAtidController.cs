﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.EntityClient;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;
using Newtonsoft.Json;
using RestSharp;

namespace FiestaNETRestServices.Controllers.Contabilidad
{
    [RoutePrefix("api/PolizasAtid")]
    public class PolizasAtidController : MFApiController
    {
        public string Get(string id)
        {
            return CargaPolizas(id);
        }
        
        // GET: api/PolizasAtid
        public string Get()
        {
            return CargaPolizas("");
        }

        public string Get(string inicio, string fin)
        {
            return CargaPolizas("F", inicio, fin);
        }

        public string Get(string inicio, string fin, string borrar)
        {
            return CargaPolizas("F", inicio, fin, borrar);
        }

        string CargaPolizas(string tipo, string inicio = "", string fin = "", string borrar = "N")
        {
            string mRetorna = "OK"; string mMensaje = ""; string mMensaje2 = "";

            EntityConnection ec = (EntityConnection)db.Connection;
            SqlConnection sc = (SqlConnection)ec.StoreConnection;

            SqlConnection mConexion = new SqlConnection(sc.ConnectionString);
            SqlCommand mCommand = new SqlCommand(); SqlTransaction mTransaction = null;

            try
            {
                DateTime mInicio = DateTime.Now.Date; DateTime mFin = DateTime.Now.Date;
                string mUsuario = "sa"; string mFechaInicial = ""; string mFechaFinal = "";

                if (tipo == "")
                {
                    mFechaFinal = string.Format("{0}-{1}{2}-{3}{4}", DateTime.Now.Year.ToString(), DateTime.Now.Month < 10 ? "0" : "", DateTime.Now.Month.ToString(), DateTime.Now.Day < 10 ? "0" : "", DateTime.Now.Day.ToString());

                    mInicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

                    int DiaInicioCargaFacturas = int.Parse(WebConfigurationManager.AppSettings["DiaMinimoCargaFactura"]);
                    if (DateTime.Now.Day <= DiaInicioCargaFacturas)
                    {
                        mInicio = DateTime.Now.AddMonths(-1);
                        mInicio = new DateTime(mInicio.Year, mInicio.Month, 1);
                    }
                    mFechaInicial = string.Format("{0}-{1}{2}-01", mInicio.Year.ToString(), mInicio.Month < 10 ? "0" : "", mInicio.Month.ToString());
                }
                else
                {
                    if (tipo == "F")
                    {
                        mFechaInicial = inicio;
                        mFechaFinal = fin;

                        string[] mArregloInicio = inicio.Split(new string[] { "-" }, StringSplitOptions.None);
                        string[] mArregloFin = fin.Split(new string[] { "-" }, StringSplitOptions.None);

                        mInicio = new DateTime(Convert.ToInt32(mArregloInicio[0]), Convert.ToInt32(mArregloInicio[1]), Convert.ToInt32(mArregloInicio[2]));
                        mFin = new DateTime(Convert.ToInt32(mArregloFin[0]), Convert.ToInt32(mArregloFin[1]), Convert.ToInt32(mArregloFin[2]));
                    }
                    else
                    {
                        int mAnio = DateTime.Now.Year; int mMes = 0;
                        string[] mArreglo = tipo.Split(new string[] { "-" }, StringSplitOptions.None);

                        if (mArreglo.Length == 1)
                        {
                            mMes = Convert.ToInt32(mArreglo[0]);
                        }
                        else
                        {
                            mMes = Convert.ToInt32(mArreglo[1]);
                            mAnio = Convert.ToInt32(mArreglo[0]);
                        }

                        DateTime mFechaInicialDT = new DateTime(mAnio, mMes, 1);
                        DateTime mFechaFinalDT = mFechaInicialDT.AddMonths(1);

                        mFechaFinalDT = new DateTime(mFechaFinalDT.Year, mFechaFinalDT.Month, 1);
                        mFechaFinalDT = mFechaFinalDT.AddDays(-1);

                        mFechaFinal = string.Format("{0}-{1}{2}-{3}{4}", mFechaFinalDT.Year.ToString(), mFechaFinalDT.Month < 10 ? "0" : "", mFechaFinalDT.Month.ToString(), mFechaFinalDT.Day < 10 ? "0" : "", mFechaFinalDT.Day.ToString());
                        mFechaInicial = string.Format("{0}-{1}{2}-{3}{4}", mFechaInicialDT.Year.ToString(), mFechaInicialDT.Month < 10 ? "0" : "", mFechaInicialDT.Month.ToString(), mFechaInicialDT.Day < 10 ? "0" : "", mFechaInicialDT.Day.ToString());
                    }
                }

                string mUrlCrediPlus = WebConfigurationManager.AppSettings["DefaultATIDServicesURL"];
                WebRequest request = WebRequest.Create(string.Format("{0}api/VistaContabilidad/{1}/{2}", mUrlCrediPlus, mFechaInicial, mFechaFinal));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                //List<Clases.PolizasAtid> mPolizas = new List<Clases.PolizasAtid>();
                //mPolizas = JsonConvert.DeserializeObject<List<Clases.PolizasAtid>>(responseString);

                Clases.ContabilidadAtid mContabilidad = new ContabilidadAtid();
                mContabilidad = JsonConvert.DeserializeObject<Clases.ContabilidadAtid>(responseString);

                List<Clases.PolizasAtid> mPolizas = new List<Clases.PolizasAtid>();

                foreach (var item in mContabilidad.contabilidad)
                {
                    PolizasAtid mItem = new PolizasAtid();
                    mItem.Poliza = item.Poliza;
                    mItem.FechaOperacion = item.FechaOperacion;
                    mItem.NumeroCredito = item.NumeroCredito;
                    mItem.CuentaContable = item.CuentaContable;
                    mItem.NombreCuentaContable = item.NombreCuentaContable;
                    mItem.Descripcion = item.Descripcion;
                    mItem.MovimientoTransaccion = item.MovimientoTransaccion;
                    mItem.Valor = item.Valor;
                    mItem.AgenciaTransaccion = item.AgenciaTransaccion;
                    mItem.TipoTransaccion = item.TipoTransaccion;
                    mPolizas.Add(mItem);
                }

                mConexion.Open();
                mTransaction = mConexion.BeginTransaction();

                mCommand.Connection = mConexion;
                mCommand.Transaction = mTransaction;

                mCommand.Parameters.Add("@Notas", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@Hoy", SqlDbType.DateTime).Value = DateTime.Now.Date;
                mCommand.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = DateTime.Now.Date;
                mCommand.Parameters.Add("@FechaInicio", SqlDbType.DateTime).Value = mInicio;
                mCommand.Parameters.Add("@FechaFin", SqlDbType.DateTime).Value = mFin;

                mCommand.CommandText = "SELECT ULTIMO_ASIENTO FROM crediplus.PAQUETE WHERE PAQUETE = 'FA'";
                string mAsientoFA = mCommand.ExecuteScalar().ToString();

                mCommand.CommandText = "SELECT ULTIMO_ASIENTO FROM crediplus.PAQUETE WHERE PAQUETE = 'CG'";
                string mAsientoCG = mCommand.ExecuteScalar().ToString();

                mCommand.CommandText = "SELECT ULTIMO_ASIENTO FROM crediplus.PAQUETE WHERE PAQUETE = 'CB'";
                string mAsientoCB = mCommand.ExecuteScalar().ToString();

                mCommand.CommandText = "SELECT ULTIMO_ASIENTO FROM prodmult.PAQUETE WHERE PAQUETE = 'CB'";
                string mAsientoCB_PM = mCommand.ExecuteScalar().ToString();

                Int32 mAsientoIntFA = Convert.ToInt32(mAsientoFA.Replace("FA", ""));
                Int32 mAsientoIntCG = Convert.ToInt32(mAsientoCG.Replace("CG", ""));
                Int32 mAsientoIntCB = Convert.ToInt32(mAsientoCB.Replace("CB", ""));
                Int32 mAsientoIntCB_PM = Convert.ToInt32(mAsientoCB_PM.Replace("CB", ""));

                List<string> mNumerosPolizas = new List<string>();
                mNumerosPolizas = (from p in mPolizas select p.Poliza).Distinct().ToList();

                mCommand.CommandText = "SELECT FECHA_TRABAJO_INI FROM crediplus.GLOBALES_AS";
                DateTime mFechaTrabajoIni = Convert.ToDateTime(mCommand.ExecuteScalar());

                mCommand.CommandText = "SELECT FECHA_TRABAJO_FIN FROM crediplus.GLOBALES_AS";
                DateTime mFechaTrabajoFin = Convert.ToDateTime(mCommand.ExecuteScalar());

                mCommand.CommandText = "SELECT NOMBRE_CAT FROM crediplus.CP_Catalogo WHERE CODIGO_TABLA = 1 AND CODIGO_CAT = 'CARGAR_BANCOS_PM'";
                bool mCargarPM = Convert.ToString(mCommand.ExecuteScalar()) == "S" ? true : false;

                mCommand.CommandText = "SELECT ULTIMO_VALOR FROM crediplus.CONSECUTIVO WHERE CONSECUTIVO = 'TFBAM'";
                string mConsecutivoBAM = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = "SELECT ULTIMO_VALOR FROM crediplus.CONSECUTIVO WHERE CONSECUTIVO = 'TFBI'";
                string mConsecutivoBI = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = "SELECT ULTIMO_VALOR FROM crediplus.CONSECUTIVO WHERE CONSECUTIVO = 'TFG&T'";
                string mConsecutivoGYT = Convert.ToString(mCommand.ExecuteScalar());

                //Borro las pólizas si así corresponde
                if (borrar == "S")
                {
                    mCommand.CommandText = "DELETE FROM crediplus.DIARIO WHERE ASIENTO IN ( SELECT ASIENTO FROM CREDIPLUS.ASIENTO_DE_DIARIO WHERE FECHA BETWEEN @FechaInicio AND @FechaFin AND CreatedBy = 'mf.fiestanet' AND PAQUETE = 'CG')";
                    mCommand.ExecuteNonQuery();

                    mCommand.CommandText = "DELETE FROM crediplus.ASIENTO_DE_DIARIO WHERE FECHA BETWEEN @FechaInicio AND @FechaFin AND CreatedBy = 'mf.fiestanet' AND PAQUETE = 'CG' ";
                    mCommand.ExecuteNonQuery();
                }

                foreach (string pol in mNumerosPolizas)
                {
                    string mNotas = string.Format("Póliza CREDIPLUS No. {0}", pol);

                    mCommand.Parameters["@Notas"].Value = mNotas;
                    mCommand.CommandText = "SELECT COUNT(ASIENTO) FROM crediplus.DIARIO WHERE FUENTE = '" + pol + "'";
                    int mDiario = Convert.ToInt32(mCommand.ExecuteScalar());

                    mCommand.CommandText = "SELECT COUNT(ASIENTO) FROM crediplus.MAYOR WHERE FUENTE = '" + pol + "'";
                    int mMayor = Convert.ToInt32(mCommand.ExecuteScalar());

                    if (mDiario == 0 && mMayor == 0)
                    {
                        var qPoliza = (from p in mPolizas where p.Poliza == pol select p).First();

                        if (qPoliza.FechaOperacion >= mFechaTrabajoIni && qPoliza.FechaOperacion <= mFechaTrabajoFin)
                        {
                            //Ok!
                        }
                        else
                        {
                            var qUpdatePol = from p in mPolizas where p.Poliza == pol select p;
                            foreach (var item in qUpdatePol)
                            {
                                item.Poliza = "N";
                            }
                        }
                    }
                    else
                    {
                        var qUpdatePol = from p in mPolizas where p.Poliza == pol select p;
                        foreach (var item in qUpdatePol)
                        {
                            item.Poliza = "N";
                        }
                    }
                }


                //Elimino las notas de debito
                var qUpdatePolND = from p in mPolizas select p;
                foreach (var item in qUpdatePolND)
                {
                    if (item.TipoTransaccion == "NotaDebito") item.Poliza = "N";
                }
                

                string mErrorCuentas = "";
                var qPolizas = from p in mPolizas select p;

                foreach (var item in qPolizas)
                {
                    string mCuentaAtid = item.CuentaContable;
                    string mCuentaContable = string.Format("{0}-{1}-{2}-0{3}-00{4}", mCuentaAtid.Substring(0, 1), mCuentaAtid.Substring(1, 1), Convert.ToInt32(mCuentaAtid.Substring(2, 2)).ToString(), mCuentaAtid.Substring(4, 2), mCuentaAtid.Substring(6, 2));

                    if (mCuentaContable == "4-0-0-001-0001") mCuentaContable = "4-1-1-001-0001";
                    if (mCuentaContable == "4-0-0-001-0002") mCuentaContable = "4-1-1-001-0002";
                    if (mCuentaContable == "6-2-25-001-0001") mCuentaContable = "6-2-2-020-0001";
                    if (mCuentaContable == "4-0-1-001-0004") mCuentaContable = "4-1-2-001-0004";

                    mCommand.CommandText = "SELECT COUNT(CUENTA_CONTABLE) FROM crediplus.CUENTA_CONTABLE WHERE CUENTA_CONTABLE = '" + mCuentaContable + "'";
                    if (Convert.ToInt32(mCommand.ExecuteScalar()) == 0)
                    {
                        mErrorCuentas = string.Format("{0} {1}-{2} {3}; ", mErrorCuentas, mCuentaContable, item.NombreCuentaContable, item.Poliza);
                    }
                    else
                    {
                        item.CuentaContable = mCuentaContable;
                    }
                }

                if (mErrorCuentas.Trim().Length > 0)
                {
                    mTransaction.Rollback();
                    mConexion.Close();

                    string mMensajeError = string.Format("Error, no se encontraron las siguientes cuentas: {0}", mErrorCuentas);
                    EnviarError(mMensajeError);

                    return mMensajeError;
                }

                int mCuantasPolizas = 0; int mCuantosDepositos = 0; int mCuantosDesembolsos = 0;
                List<string> mPolizasValidas = new List<string>();
                mPolizasValidas = (from p in mPolizas where p.Poliza != "N" select p.Poliza).Distinct().ToList();

                mCommand.CommandText = "SELECT TOP 1 MONTO FROM prodmult.TIPO_CAMBIO_HIST WHERE TIPO_CAMBIO = 'REFR' ORDER BY FECHA DESC";
                decimal mTipoCambio = Convert.ToDecimal(mCommand.ExecuteScalar());

                int mAsientoInt = 0; int ii = 0; string mAsiento = "";

                foreach (string pol in mPolizasValidas)
                {
                    string mModulo = ""; string mTipoPoliza = "";
                    string mNotas = string.Format("Póliza CREDIPLUS No. {0}", pol);

                    mAsientoInt = 0;
                    mCuantasPolizas++;

                    List<Clases.PolizasAtid> mPoliza = new List<PolizasAtid>();
                    mPoliza = (from p in mPolizas where p.Poliza == pol select p).ToList();

                    switch (mPoliza[0].TipoTransaccion)
                    {
                        case "Desembolso":
                        case "Orden de Pago":
                        case "NotaDebito":
                            mModulo = "CB";
                            mAsientoIntCB++;
                            mAsientoInt = mAsientoIntCB;

                            mTipoPoliza = "DEP";
                            if (mPoliza[0].TipoTransaccion == "Orden de Pago") mTipoPoliza = "PROV";

                            break;
                        case "Nota":
                            mModulo = "FA";
                            mAsientoIntFA++;
                            mTipoPoliza = "NDE";
                            mAsientoInt = mAsientoIntFA;

                            break;
                        default:
                            mModulo = "CG";
                            mAsientoIntCG++;
                            mTipoPoliza = "AJU";
                            mAsientoInt = mAsientoIntCG;

                            break;
                    }

                    mAsiento = string.Format("{0}{1}{2}", mModulo, Ceros(mAsientoInt), mAsientoInt.ToString());

                    decimal mCargos = (from p in mPoliza where p.MovimientoTransaccion == "CARGO" select p.Valor).Sum(); //Débitos
                    decimal mAbonos = (from p in mPoliza where p.MovimientoTransaccion == "ABONO" select p.Valor).Sum(); //Créditos

                    decimal mCargosDolar = Math.Round((mCargos / mTipoCambio), 2, MidpointRounding.AwayFromZero);
                    decimal mAbonosDolar = Math.Round((mAbonos / mTipoCambio), 2, MidpointRounding.AwayFromZero);

                    mCommand.Parameters["@Fecha"].Value = mPoliza[0].FechaOperacion;

                    mCommand.CommandText = "INSERT INTO crediplus.ASIENTO_DE_DIARIO ( ASIENTO, PAQUETE, TIPO_ASIENTO, FECHA, CONTABILIDAD, ORIGEN, CLASE_ASIENTO, TOTAL_DEBITO_LOC, TOTAL_DEBITO_DOL, TOTAL_CREDITO_LOC, TOTAL_CREDITO_DOL, " +
                                            "ULTIMO_USUARIO, FECHA_ULT_MODIF, MARCADO, NOTAS, TOTAL_CONTROL_LOC, TOTAL_CONTROL_DOL, USUARIO_CREACION, FECHA_CREACION, " +
                                            "NoteExistsFlag, RecordDate, RowPointer, CreatedBy, UpdatedBy, CreateDate ) " +
                                            "VALUES ( '" + mAsiento + "', '" + mModulo + "', '" + mTipoPoliza + "', @Fecha , 'A', '" + mModulo + "', 'N', " + mCargos.ToString() + ", " + mCargosDolar.ToString() + ", " + mAbonos.ToString() + ", " + mAbonosDolar.ToString() + ", " +
                                            "'" + mUsuario + "', @Hoy, 'N', '" + mNotas + "', " + mCargos.ToString() + ", " + mCargosDolar.ToString() + ", '" + mUsuario + "', @Hoy, " +
                                            "0, GETDATE(), prodmult.fcNewID(), 'CG/" + mUsuario + "', 'CG/" + mUsuario + "', GETDATE() ) ";
                    mCommand.ExecuteNonQuery();

                    ii = 0;
                    foreach (var item in mPoliza)
                    {
                        decimal mCargo = 0; decimal mAbono = 0;
                        decimal mCargoDolar = 0; decimal mAbonoDolar = 0;

                        

                        if (item.MovimientoTransaccion == "CARGO")
                        {
                            mCargo = item.Valor;
                            mCargoDolar = Math.Round((mCargo / mTipoCambio), 2, MidpointRounding.AwayFromZero);
                        }
                        else
                        {
                            mAbono = item.Valor;
                            mAbonoDolar = Math.Round((mAbono / mTipoCambio), 2, MidpointRounding.AwayFromZero);
                        }

                        ii++;
                        string mReferencia = string.Format("{0} Crédito No. {1}", item.Descripcion, item.NumeroCredito);

                        mMensaje = string.Format("{0} - {1} {2}", mAsiento, item.CuentaContable, item.Poliza);
                        mCommand.CommandText = "INSERT INTO crediplus.DIARIO ( ASIENTO, CONSECUTIVO, NIT, CENTRO_COSTO, CUENTA_CONTABLE, FUENTE, REFERENCIA, DEBITO_LOCAL, DEBITO_DOLAR, CREDITO_LOCAL, CREDITO_DOLAR, " +
                                                "TIPO_CAMBIO, NoteExistsFlag, RecordDate, RowPointer, CreatedBy, UpdatedBy, CreateDate ) " +
                                                "VALUES ( '" + mAsiento + "', " + ii.ToString() + ", 'ND', '0-000', '" + item.CuentaContable + "', '" + item.Poliza + "', '" + mReferencia + "', " 
                                                + (item.MovimientoTransaccion=="CARGO" ? mCargo.ToString() : Const._NULL_)
                                                //+ mCargo.ToString() 
                                                + ", "
                                                + (item.MovimientoTransaccion == "CARGO" ? mCargoDolar.ToString() : Const._NULL_)
                                                //+ mCargoDolar.ToString() 
                                                + ", "
                                                + (item.MovimientoTransaccion == "ABONO" ? mAbono.ToString() : Const._NULL_)
                                                //+ mAbono.ToString() 
                                                + ", "
                                                + (item.MovimientoTransaccion == "ABONO" ? mAbonoDolar.ToString() : Const._NULL_)
                                                //+ mAbonoDolar.ToString() 
                                                + ", " + mTipoCambio.ToString() + ", 0, GETDATE(), prodmult.fcNewID(), 'CG/" + mUsuario + "', 'CG/" + mUsuario + "', GETDATE() ) ";
                        mCommand.ExecuteNonQuery();
                    }

                    mCommand.CommandText = "UPDATE crediplus.PAQUETE SET ULTIMO_ASIENTO = '" + mAsiento + "' WHERE PAQUETE = '" + mModulo + "'";
                    mCommand.ExecuteNonQuery();
                }

                mCommand.Parameters.Add("@FechaIngreso", SqlDbType.DateTime).Value = DateTime.Now.Date;
                mCommand.Parameters.Add("@FechaAplicacion", SqlDbType.DateTime).Value = DateTime.Now.Date;
                mCommand.Parameters.Add("@FechaDesembolso", SqlDbType.DateTime).Value = DateTime.Now.Date;
                mCommand.Parameters.Add("@Guid", SqlDbType.UniqueIdentifier).Value = Guid.NewGuid();


                //Depósitos
                foreach (var item in mContabilidad.depositos)
                {
                    mCommand.CommandText = "SELECT COUNT(ID) FROM crediplus.CP_Depositos WHERE ID = " + item.id.ToString();
                    if (Convert.ToInt32(mCommand.ExecuteScalar()) == 0)
                    {
                        mCuantosDepositos++;
                        mAsientoIntCB++;
                        mAsientoInt = mAsientoIntCB;
                        
                        mAsiento = string.Format("{0}{1}{2}", "CB", Ceros(mAsientoInt), mAsientoInt.ToString());
                        decimal mValorDolar = Math.Round((item.valor / mTipoCambio), 2, MidpointRounding.AwayFromZero);
                        
                        mCommand.Parameters["@FechaIngreso"].Value = item.fechaIngreso.Date;
                        mCommand.Parameters["@FechaAplicacion"].Value = item.fechaAplicacion.Date;

                        mCommand.CommandText = "INSERT INTO crediplus.CP_Depositos ( ID, EN_EXACTUS, FECHA_INGRESO, FECHA_APLICACION, CUENTA_EMISION, NUMERO_BOLETA, DESCRIPCION, DESCRIPCION_TRANSACCION, VALOR, FECHA_HORA ) " +
                            "VALUES ( " + item.id.ToString() + ", 'S', @FechaIngreso, @FechaAplicacion, '" + item.cuentaEmision + "', '" + item.numeroBoleta + "', '" + item.descripcion + "', '" + item.descripcionTransaccion + "', " + item.valor.ToString() + ", GETDATE() )";
                        mCommand.ExecuteNonQuery();

                        mMensaje2 = mCommand.CommandText;

                        string mReferencia = "";
                        if (item.descripcion.Trim().Length > 0) mReferencia = string.Format("Depósito de crédito No. {0}", item.descripcion);

                        mCommand.CommandText = "SELECT prodmult.fcNewID()";
                        mCommand.Parameters["@Guid"].Value = mCommand.ExecuteScalar();

                        if (item.cuentaEmision == "66-27808-9") item.cuentaEmision = "066-0027808-9";
                        if (item.cuentaEmision == "30-4022953-6 ") item.cuentaEmision = "30-4022953-6";

                        if (item.numeroBoleta.Trim().Length == 0) item.numeroBoleta = item.id.ToString();
                        if (mReferencia.Trim().Length == 0) mReferencia = string.Format("Depósito con ID No. {0}", item.id.ToString());

                        string mNumeroBoleta = string.Format("{0}{1}", item.id.ToString(), item.numeroBoleta);
                        
                        mCommand.CommandText = "INSERT INTO crediplus.MOV_BANCOS ( CUENTA_BANCO, TIPO_DOCUMENTO, NUMERO, SUBTIPO, FECHA, REFERENCIA, MONTO, DETALLE, CONFIRMADO, ORIGEN, ASIENTO, ANULADO, FCH_HORA_CREACION, " +
                            "USUARIO_CREACION, ESTADO, IMPRESO, CLASE_DIF, ACLARADA_DIF, CLASE_DOCUMENTO, MODO_REGISTRO, LIQUIDADO, FCH_HORA_MODIFIC, USUARIO_MODIFIC, TIPO_CAMBIO_LOCAL, TIPO_CAMBIO_DOLAR, APROBADO, " +
                            "USUARIO_APROBACION, FECHA_APROBACION, TIPO_ASIENTO, PAQUETE, DEPENDIENTE_GP, FECHA_CONTABLE, IMPUESTO1, IMP1_NODES, NoteExistsFlag, RecordDate, RowPointer, CreatedBy, UpdatedBy, CreateDate ) " +
                            "VALUES ( '" + item.cuentaEmision + "', 'DEP', " + mNumeroBoleta + ", 0, @FechaAplicacion, '" + mReferencia + "', " + item.valor.ToString() + ", '', 'S', 'CB', '" + mAsiento + "', 'N', GETDATE(), " +
                            "'" + mUsuario + "', 'N', 'N', 'X', 'X', 'N', 'M', 'N', GETDATE(), '" + mUsuario + "', 1, " + mTipoCambio.ToString() + ", 'S', " +
                            "'" + mUsuario + "', GETDATE(), 'BCO', 'CB', 'N', @FechaAplicacion, 0, 0, 0, GETDATE(), @Guid, '" + mUsuario + "', '" + mUsuario + "', GETDATE() ) ";
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = "INSERT INTO crediplus.ASIENTO_DE_DIARIO ( ASIENTO, PAQUETE, TIPO_ASIENTO, FECHA, CONTABILIDAD, ORIGEN, CLASE_ASIENTO, TOTAL_DEBITO_LOC, TOTAL_DEBITO_DOL, TOTAL_CREDITO_LOC, TOTAL_CREDITO_DOL, " +
                                                "ULTIMO_USUARIO, FECHA_ULT_MODIF, MARCADO, NOTAS, TOTAL_CONTROL_LOC, TOTAL_CONTROL_DOL, USUARIO_CREACION, FECHA_CREACION, " +
                                                "NoteExistsFlag, RecordDate, RowPointer, CreatedBy, UpdatedBy, CreateDate ) " +
                                                "VALUES ( '" + mAsiento + "', 'CB', 'BCO', @FechaAplicacion , 'A', 'CB', 'N', " + item.valor.ToString() + ", " + mValorDolar.ToString() + ", " + item.valor.ToString() + ", " + mValorDolar.ToString() + ", " +
                                                "'" + mUsuario + "', @Hoy, 'N', '" + mReferencia + "', " + item.valor.ToString() + ", " + item.valor.ToString() + ", '" + mUsuario + "', @Hoy, " +
                                                "0, GETDATE(), prodmult.fcNewID(), '" + mUsuario + "', '" + mUsuario + "', GETDATE() ) ";
                        mCommand.ExecuteNonQuery();

                        string mFuente = string.Format("DEP{0}", item.numeroBoleta);
                        decimal mCargo = item.valor; decimal mAbono = 0; decimal mAbonoDolar = 0;
                        decimal mCargoDolar = Math.Round((item.valor / mTipoCambio), 2, MidpointRounding.AwayFromZero);

                        mCommand.CommandText = $"SELECT CTA_CONTABLE FROM crediplus.CUENTA_BANCARIA WHERE CUENTA_BANCO = '{item.cuentaEmision}'";
                        string mCuentaContable = Convert.ToString(mCommand.ExecuteScalar());

                        mCommand.CommandText = "INSERT INTO crediplus.DIARIO ( ASIENTO, CONSECUTIVO, NIT, CENTRO_COSTO, CUENTA_CONTABLE, FUENTE, REFERENCIA, DEBITO_LOCAL, DEBITO_DOLAR, CREDITO_LOCAL, CREDITO_DOLAR, " +
                                                "TIPO_CAMBIO, NoteExistsFlag, RecordDate, RowPointer, CreatedBy, UpdatedBy, CreateDate ) " +
                                                "VALUES ( '" + mAsiento + "', 1, 'ND', '0-000', '" + mCuentaContable + "', '" + mFuente + "', '" + mReferencia + "', " + mCargo.ToString() + ", " + mCargoDolar.ToString() 
                                                + ", " 
                                                + Const._NULL_
                                                //+ mAbono.ToString() 
                                                + ", "
                                                + Const._NULL_
                                                //+ mAbonoDolar.ToString() 
                                                + ", " + mTipoCambio.ToString() + ", 0, GETDATE(), prodmult.fcNewID(), 'CG/" + mUsuario + "', '" + mUsuario + "', GETDATE() ) ";
                        mCommand.ExecuteNonQuery();

                        mAbono = mCargo;
                        mAbonoDolar = mCargoDolar;
                        mCargo = 0;
                        mCargoDolar = 0;
                        mCuentaContable = "1-1-1-001-0001";

                        mCommand.CommandText = "INSERT INTO crediplus.DIARIO ( ASIENTO, CONSECUTIVO, NIT, CENTRO_COSTO, CUENTA_CONTABLE, FUENTE, REFERENCIA, DEBITO_LOCAL, DEBITO_DOLAR, CREDITO_LOCAL, CREDITO_DOLAR, " +
                                                "TIPO_CAMBIO, NoteExistsFlag, RecordDate, RowPointer, CreatedBy, UpdatedBy, CreateDate ) " +
                                                "VALUES ( '" + mAsiento + "', 2, 'ND', '0-000', '" + mCuentaContable + "', '" + mFuente + "', '" + mReferencia 
                                                + "', " 
                                                + Const._NULL_
                                                //+ mCargo.ToString() 
                                                + ", "
                                                + Const._NULL_
                                                //+ mCargoDolar.ToString() 
                                                + ", " +
                                                mAbono.ToString() + ", " + mAbonoDolar.ToString() + ", " + mTipoCambio.ToString() + ", 0, GETDATE(), prodmult.fcNewID(), 'CG/" + mUsuario + "', '" + mUsuario + "', GETDATE() ) ";
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = "INSERT INTO crediplus.CG_AUX ( GUID_ORIGEN, TABLA_ORIGEN, ASIENTO, LINEA, COMENTARIO, NoteExistsFlag, RecordDate, RowPointer, CreatedBy, UpdatedBy, CreateDate ) " +
                            "VALUES ( @Guid, 'MOV_BANCOS', '" + mAsiento + "', 1, 'GUID - Origen', 0, GETDATE(), prodmult.fcNewID(), '" + mUsuario + "', '" + mUsuario + "', GETDATE() ) ";
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = "UPDATE crediplus.PAQUETE SET ULTIMO_ASIENTO = '" + mAsiento + "' WHERE PAQUETE = 'CB'";
                        mCommand.ExecuteNonQuery();

                        item.descripcion = "S";
                    }
                }

                var qCuentasDepositos = mContabilidad.depositos.Select(x => x.cuentaEmision).Distinct();
                foreach (var cuenta in qCuentasDepositos)
                {
                    decimal mValor = mContabilidad.depositos.Where(x => x.cuentaEmision == cuenta && x.descripcion == "S").Sum(x => x.valor);

                    mCommand.CommandText = "UPDATE crediplus.CUENTA_BANCARIA SET SALDO = ROUND(SALDO + " + mValor.ToString() + ", 2), POSICION_DE_CAJA = ROUND(POSICION_DE_CAJA + " + mValor.ToString() + ", 2), FECHA_ULT_MOV = @Hoy " +
                        "WHERE CUENTA_BANCO = '" + cuenta + "'";
                    mCommand.ExecuteNonQuery();
                }


                //Desembolsos
                foreach (var item in mContabilidad.desembolsos)
                {
                    if (item.id == 10834)
                    {
                        int kk = 0;
                        kk++;
                    }

                    mCommand.CommandText = "SELECT COUNT(ID) FROM crediplus.CP_Desembolsos WHERE ID = " + item.id.ToString();
                    if (Convert.ToInt32(mCommand.ExecuteScalar()) == 0)
                    {
                        mCuantosDesembolsos++;
                        mAsientoIntCB++;
                        mAsientoInt = mAsientoIntCB;

                        mAsiento = string.Format("{0}{1}{2}", "CB", Ceros(mAsientoInt), mAsientoInt.ToString());
                        decimal mValorDolar = Math.Round((item.valorDesembolso / mTipoCambio), 2, MidpointRounding.AwayFromZero);

                        mCommand.Parameters["@FechaIngreso"].Value = item.fechaDesembolso.Date;
                        mMensaje2 = string.Format("{0} * {1}", mCommand.CommandText, item.fechaDesembolso.Date.ToShortDateString());

                        mCommand.CommandText = "INSERT INTO crediplus.CP_Desembolsos ( ID, EN_EXACTUS, FECHA_DESEMBOLSO, CUENTA_EMISION, CUENTA_DESTINO, NUMERO_TRANSFERENCIA, VALOR_DESEMBOLSO, FECHA_HORA ) " +
                            "VALUES ( " + item.id.ToString() + ", 'S', @FechaIngreso, '" + item.cuentaEmision + "', '" + item.cuentaDestino + "', '" + item.numeroTransferencia + "', " + item.valorDesembolso.ToString() + ", GETDATE() )";
                        mCommand.ExecuteNonQuery();
                        
                        string mReferencia = ""; int mNumeroTrans = item.id;
                        if (item.numeroTransferencia.Trim().Length > 0)
                        {
                            string[] mInfo = item.numeroTransferencia.Split(new string[] { " " }, StringSplitOptions.None);

                            if (mInfo.Length > 0)
                            {
                                try
                                {
                                    mReferencia = string.Format("T/D {0}", mInfo[1]);
                                    try
                                    {
                                        mNumeroTrans = Convert.ToInt32(mInfo[1].Replace("-", ""));
                                    }
                                    catch {
                                        mNumeroTrans = item.id;
                                    }
                                }
                                catch
                                {
                                    mReferencia = string.Format("T/D {0}", mInfo[0]);

                                    try
                                    {
                                        mNumeroTrans = Convert.ToInt32(mInfo[0].Replace("-", ""));
                                    }
                                    catch
                                    {
                                        mNumeroTrans = item.id;
                                    }
                                }
                            }
                        }

                        //Cambio el # de transferencia por el consecutivo de cada cuenta
                        if (item.cuentaEmision == "66-27808-9") item.cuentaEmision = "066-0027808-9";
                        if (item.cuentaEmision == "30-4022953-6 ") item.cuentaEmision = "30-4022953-6";

                        if (item.cuentaEmision == "30-4022953-6") mNumeroTrans = NumeroTransferencia(ref mConsecutivoBAM); //Banco Agromercantil
                        if (item.cuentaEmision == "214-009860-5") mNumeroTrans = NumeroTransferencia(ref mConsecutivoBI); //Banco Industrial
                        if (item.cuentaEmision == "066-0027808-9" || item.cuentaEmision == "66-27808-9") mNumeroTrans = NumeroTransferencia(ref mConsecutivoGYT); //Banco G&T

                        mCommand.CommandText = "SELECT prodmult.fcNewID()";
                        mCommand.Parameters["@Guid"].Value = mCommand.ExecuteScalar();

                        mCommand.CommandText = "INSERT INTO crediplus.MOV_BANCOS ( CUENTA_BANCO, TIPO_DOCUMENTO, NUMERO, SUBTIPO, FECHA, REFERENCIA, MONTO, DETALLE, CONFIRMADO, ORIGEN, ASIENTO, ANULADO, FCH_HORA_CREACION, " +
                            "USUARIO_CREACION, ESTADO, IMPRESO, CLASE_DIF, ACLARADA_DIF, CLASE_DOCUMENTO, MODO_REGISTRO, LIQUIDADO, FCH_HORA_MODIFIC, USUARIO_MODIFIC, TIPO_CAMBIO_LOCAL, TIPO_CAMBIO_DOLAR, APROBADO, " +
                            "USUARIO_APROBACION, FECHA_APROBACION, TIPO_ASIENTO, PAQUETE, DEPENDIENTE_GP, FECHA_CONTABLE, IMPUESTO1, IMP1_NODES, NoteExistsFlag, RecordDate, RowPointer, CreatedBy, UpdatedBy, CreateDate ) " +
                            "VALUES ( '" + item.cuentaEmision + "', 'T/D', " + mNumeroTrans.ToString() + ", 0, @FechaIngreso, '" + mReferencia + "', " + item.valorDesembolso.ToString() + ", '', 'S', 'CB', '" + mAsiento + "', 'N', GETDATE(), " +
                            "'" + mUsuario + "', 'N', 'N', 'X', 'X', 'N', 'M', 'N', GETDATE(), '" + mUsuario + "', 1, " + mTipoCambio.ToString() + ", 'S', " +
                            "'" + mUsuario + "', GETDATE(), 'BCO', 'CB', 'N', @FechaIngreso, 0, 0, 0, GETDATE(), @Guid, '" + mUsuario + "', '" + mUsuario + "', GETDATE() ) ";
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = "INSERT INTO crediplus.ASIENTO_DE_DIARIO ( ASIENTO, PAQUETE, TIPO_ASIENTO, FECHA, CONTABILIDAD, ORIGEN, CLASE_ASIENTO, TOTAL_DEBITO_LOC, TOTAL_DEBITO_DOL, TOTAL_CREDITO_LOC, TOTAL_CREDITO_DOL, " +
                                                "ULTIMO_USUARIO, FECHA_ULT_MODIF, MARCADO, NOTAS, TOTAL_CONTROL_LOC, TOTAL_CONTROL_DOL, USUARIO_CREACION, FECHA_CREACION, " +
                                                "NoteExistsFlag, RecordDate, RowPointer, CreatedBy, UpdatedBy, CreateDate ) " +
                                                "VALUES ( '" + mAsiento + "', 'CB', 'BCO', @FechaIngreso , 'A', 'CB', 'N', " + item.valorDesembolso.ToString() + ", " + mValorDolar.ToString() + ", " + item.valorDesembolso.ToString() + ", " + mValorDolar.ToString() + ", " +
                                                "'" + mUsuario + "', @Hoy, 'N', '" + mReferencia + "', " + item.valorDesembolso.ToString() + ", " + item.valorDesembolso.ToString() + ", '" + mUsuario + "', @Hoy, " +
                                                "0, GETDATE(), prodmult.fcNewID(), '" + mUsuario + "', '" + mUsuario + "', GETDATE() ) ";
                        mCommand.ExecuteNonQuery();

                        string mFuente = mReferencia;
                        decimal mCargo = item.valorDesembolso; decimal mAbono = 0; decimal mAbonoDolar = 0;
                        decimal mCargoDolar = Math.Round((item.valorDesembolso / mTipoCambio), 2, MidpointRounding.AwayFromZero);
                        string mCuentaContable = "2-1-3-001-0001";
                        if (item.cuentaDestino == "02-0003419-5") mCuentaContable = "2-1-3-001-0001";
                        if (item.cuentaDestino == "901915389") mCuentaContable = "2-1-3-001-0002";
                        if (item.cuentaDestino == "3040211333") mCuentaContable = "2-1-3-001-0003";

                        mCommand.CommandText = "INSERT INTO crediplus.DIARIO ( ASIENTO, CONSECUTIVO, NIT, CENTRO_COSTO, CUENTA_CONTABLE, FUENTE, REFERENCIA, DEBITO_LOCAL, DEBITO_DOLAR, CREDITO_LOCAL, CREDITO_DOLAR, " +
                                                "TIPO_CAMBIO, NoteExistsFlag, RecordDate, RowPointer, CreatedBy, UpdatedBy, CreateDate ) " +
                                                "VALUES ( '" + mAsiento + "', 1, 'ND', '0-000', '" + mCuentaContable + "', '" + mFuente + "', '" + mReferencia 
                                                + "', " 
                                                + mCargo.ToString() 
                                                + ", " 
                                                + mCargoDolar.ToString() 
                                                + ", "
                                                + Const._NULL_
                                                //+ mAbono.ToString() 
                                                + ", "
                                                + Const._NULL_
                                                //+ mAbonoDolar.ToString() 
                                                + ", " + mTipoCambio.ToString() + ", 0, GETDATE(), prodmult.fcNewID(), 'CG/" + mUsuario + "', '" + mUsuario + "', GETDATE() ) ";
                        mCommand.ExecuteNonQuery();

                        mAbono = mCargo;
                        mAbonoDolar = mCargoDolar;
                        mCargo = 0;
                        mCargoDolar = 0;

                        mCommand.CommandText = $"SELECT CTA_CONTABLE FROM crediplus.CUENTA_BANCARIA WHERE CUENTA_BANCO = '{item.cuentaEmision}'";
                        mCuentaContable = Convert.ToString(mCommand.ExecuteScalar());


                        mCommand.CommandText = "INSERT INTO crediplus.DIARIO ( ASIENTO, CONSECUTIVO, NIT, CENTRO_COSTO, CUENTA_CONTABLE, FUENTE, REFERENCIA, DEBITO_LOCAL, DEBITO_DOLAR, CREDITO_LOCAL, CREDITO_DOLAR, " +
                                                "TIPO_CAMBIO, NoteExistsFlag, RecordDate, RowPointer, CreatedBy, UpdatedBy, CreateDate ) " +
                                                "VALUES ( '" + mAsiento + "', 2, 'ND', '0-000', '" + mCuentaContable + "', '" + mFuente + "', '" + mReferencia 
                                                + "', "
                                                + Const._NULL_
                                                //+ mCargo.ToString() 
                                                + ", "
                                                + Const._NULL_
                                                //+ mCargoDolar.ToString() 
                                                + ", " 
                                                + mAbono.ToString() 
                                                + ", " 
                                                + mAbonoDolar.ToString() 
                                                + ", " 
                                                + mTipoCambio.ToString() + ", 0, GETDATE(), prodmult.fcNewID(), 'CG/" + mUsuario + "', 'CG/" + mUsuario + "', GETDATE() ) ";
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = "INSERT INTO crediplus.CG_AUX ( GUID_ORIGEN, TABLA_ORIGEN, ASIENTO, LINEA, COMENTARIO, NoteExistsFlag, RecordDate, RowPointer, CreatedBy, UpdatedBy, CreateDate ) " +
                            "VALUES ( @Guid, 'MOV_BANCOS', '" + mAsiento + "', 1, 'GUID - Origen', 0, GETDATE(), prodmult.fcNewID(), '" + mUsuario + "', '" + mUsuario + "', GETDATE() ) ";
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = "UPDATE crediplus.PAQUETE SET ULTIMO_ASIENTO = '" + mAsiento + "' WHERE PAQUETE = 'CB'";
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = $"UPDATE crediplus.CONSECUTIVO SET ULTIMO_VALOR = '{mConsecutivoBAM}' WHERE CONSECUTIVO = 'TFBAM'";
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = $"UPDATE crediplus.CONSECUTIVO SET ULTIMO_VALOR = '{mConsecutivoBI}' WHERE CONSECUTIVO = 'TFBI'";
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = $"UPDATE crediplus.CONSECUTIVO SET ULTIMO_VALOR = '{mConsecutivoGYT}' WHERE CONSECUTIVO = 'TFG&T'";
                        mCommand.ExecuteNonQuery();


                        //Ingreso a Productos Múltiples
                        if (mCargarPM && item.cuentaDestino == "02-0003419-5")
                        {
                            mAsientoIntCB_PM++;
                            string mAsiento_PM = string.Format("{0}{1}{2}", "CB", Ceros(mAsientoIntCB_PM), mAsientoIntCB_PM.ToString());

                            mReferencia = mReferencia.Replace("T/D", "T/C");

                            mCommand.CommandText = "SELECT prodmult.fcNewID()";
                            mCommand.Parameters["@Guid"].Value = mCommand.ExecuteScalar();
                            
                            mCommand.CommandText = "INSERT INTO prodmult.MOV_BANCOS ( CUENTA_BANCO, TIPO_DOCUMENTO, NUMERO, SUBTIPO, FECHA, REFERENCIA, MONTO, DETALLE, CONFIRMADO, ORIGEN, ASIENTO, ANULADO, FCH_HORA_CREACION, " +
                                "USUARIO_CREACION, ESTADO, IMPRESO, CLASE_DIF, ACLARADA_DIF, CLASE_DOCUMENTO, MODO_REGISTRO, LIQUIDADO, FCH_HORA_MODIFIC, USUARIO_MODIFIC, TIPO_CAMBIO_LOCAL, TIPO_CAMBIO_DOLAR, APROBADO, " +
                                "USUARIO_APROBACION, FECHA_APROBACION, TIPO_ASIENTO, PAQUETE, DEPENDIENTE_GP, FECHA_CONTABLE, IMPUESTO1, IMP1_NODES, NoteExistsFlag, RecordDate, RowPointer, CreatedBy, UpdatedBy, CreateDate ) " +
                                "VALUES ( '" + item.cuentaDestino + "', 'T/C', " + mNumeroTrans.ToString() + ", 0, @FechaIngreso, '" + mReferencia + "', " + item.valorDesembolso.ToString() + ", '', 'S', 'CB', '" + mAsiento_PM + "', 'N', GETDATE(), " +
                                "'" + mUsuario + "', 'N', 'N', 'X', 'X', 'N', 'M', 'N', GETDATE(), '" + mUsuario + "', 1, " + mTipoCambio.ToString() + ", 'S', " +
                                "'" + mUsuario + "', GETDATE(), 'BCO', 'CB', 'N', @FechaIngreso, 0, 0, 0, GETDATE(), @Guid, '" + mUsuario + "', '" + mUsuario + "', GETDATE() ) ";
                            mCommand.ExecuteNonQuery();

                            mCommand.CommandText = "INSERT INTO prodmult.ASIENTO_DE_DIARIO ( ASIENTO, PAQUETE, TIPO_ASIENTO, FECHA, CONTABILIDAD, ORIGEN, CLASE_ASIENTO, TOTAL_DEBITO_LOC, TOTAL_DEBITO_DOL, TOTAL_CREDITO_LOC, TOTAL_CREDITO_DOL, " +
                                                    "ULTIMO_USUARIO, FECHA_ULT_MODIF, MARCADO, NOTAS, TOTAL_CONTROL_LOC, TOTAL_CONTROL_DOL, USUARIO_CREACION, FECHA_CREACION, " +
                                                    "NoteExistsFlag, RecordDate, RowPointer, CreatedBy, UpdatedBy, CreateDate ) " +
                                                    "VALUES ( '" + mAsiento_PM + "', 'CB', 'BCO', @FechaIngreso , 'A', 'CB', 'N', " + item.valorDesembolso.ToString() + ", " + mValorDolar.ToString() + ", " + item.valorDesembolso.ToString() + ", " + mValorDolar.ToString() + ", " +
                                                    "'" + mUsuario + "', @Hoy, 'N', '" + mReferencia + "', " + item.valorDesembolso.ToString() + ", " + item.valorDesembolso.ToString() + ", '" + mUsuario + "', @Hoy, " +
                                                    "0, GETDATE(), prodmult.fcNewID(), '" + mUsuario + "', '" + mUsuario + "', GETDATE() ) ";
                            mCommand.ExecuteNonQuery();

                            mFuente = mReferencia;
                            mCargo = item.valorDesembolso;
                            mAbono = 0;
                            mAbonoDolar = 0;
                            mCargoDolar = Math.Round((item.valorDesembolso / mTipoCambio), 2, MidpointRounding.AwayFromZero);

                            mCommand.CommandText = $"SELECT CTA_CONTABLE FROM prodmult.CUENTA_BANCARIA WHERE CUENTA_BANCO = '{item.cuentaDestino}'";
                            mCuentaContable = Convert.ToString(mCommand.ExecuteScalar());

                            mCommand.CommandText = "INSERT INTO prodmult.DIARIO ( ASIENTO, CONSECUTIVO, NIT, CENTRO_COSTO, CUENTA_CONTABLE, FUENTE, REFERENCIA, DEBITO_LOCAL, DEBITO_DOLAR, CREDITO_LOCAL, CREDITO_DOLAR, " +
                                                    "TIPO_CAMBIO, NoteExistsFlag, RecordDate, RowPointer, CreatedBy, UpdatedBy, CreateDate, PROYECTO, FASE ) " +
                                                    "VALUES ( '" + mAsiento_PM + "', 1, 'ND', '0-000', '" + mCuentaContable + "', '" + mFuente + "', '" + mReferencia + "', " + mCargo.ToString() + ", " + mCargoDolar.ToString() + ", " +
                                                    mAbono.ToString() + ", " + mAbonoDolar.ToString() + ", " + mTipoCambio.ToString() + ", 0, GETDATE(), prodmult.fcNewID(), 'CG/" + mUsuario + "', '" + mUsuario + "', GETDATE(), NULL, NULL ) ";
                            mCommand.ExecuteNonQuery();

                            mAbono = mCargo;
                            mAbonoDolar = mCargoDolar;
                            mCargo = 0;
                            mCargoDolar = 0;
                            mCuentaContable = "1-1-1-001-0001";

                            mCommand.CommandText = "INSERT INTO prodmult.DIARIO ( ASIENTO, CONSECUTIVO, NIT, CENTRO_COSTO, CUENTA_CONTABLE, FUENTE, REFERENCIA, DEBITO_LOCAL, DEBITO_DOLAR, CREDITO_LOCAL, CREDITO_DOLAR, " +
                                                    "TIPO_CAMBIO, NoteExistsFlag, RecordDate, RowPointer, CreatedBy, UpdatedBy, CreateDate, PROYECTO, FASE ) " +
                                                    "VALUES ( '" + mAsiento_PM + "', 2, 'ND', '0-000', '" + mCuentaContable + "', '" + mFuente + "', '" + mReferencia + "', " + mCargo.ToString() + ", " + mCargoDolar.ToString() + ", " +
                                                    mAbono.ToString() + ", " + mAbonoDolar.ToString() + ", " + mTipoCambio.ToString() + ", 0, GETDATE(), prodmult.fcNewID(), 'CG/" + mUsuario + "', '" + mUsuario + "', GETDATE(), NULL, NULL ) ";
                            mCommand.ExecuteNonQuery();

                            mCommand.CommandText = "INSERT INTO prodmult.CG_AUX ( GUID_ORIGEN, TABLA_ORIGEN, ASIENTO, LINEA, COMENTARIO, NoteExistsFlag, RecordDate, RowPointer, CreatedBy, UpdatedBy, CreateDate ) " +
                                "VALUES ( @Guid, 'MOV_BANCOS', '" + mAsiento_PM + "', 1, 'GUID - Origen', 0, GETDATE(), prodmult.fcNewID(), '" + mUsuario + "', '" + mUsuario + "', GETDATE() ) ";
                            mCommand.ExecuteNonQuery();

                            mCommand.CommandText = "UPDATE prodmult.PAQUETE SET ULTIMO_ASIENTO = '" + mAsiento_PM + "' WHERE PAQUETE = 'CB'";
                            mCommand.ExecuteNonQuery();
                        }

                        item.numeroTransferencia = "S";
                    }
                }

                var qCuentasDesembolsos = mContabilidad.desembolsos.Select(x => x.cuentaEmision).Distinct();
                foreach (var cuenta in qCuentasDesembolsos)
                {
                    decimal mValor = mContabilidad.desembolsos.Where(x => x.cuentaEmision == cuenta && x.numeroTransferencia == "S").Sum(x => x.valorDesembolso);

                    mCommand.CommandText = "UPDATE crediplus.CUENTA_BANCARIA SET SALDO = ROUND(SALDO - " + mValor.ToString() + ", 2), POSICION_DE_CAJA = ROUND(POSICION_DE_CAJA - " + mValor.ToString() + ", 2), FECHA_ULT_MOV = @Hoy " +
                        "WHERE CUENTA_BANCO = '" + cuenta + "'";
                    mCommand.ExecuteNonQuery();
                }

                //Ingreso a Productos Múltiples
                if (mCargarPM)
                {
                    var qCuentasDesembolsosPM = mContabilidad.desembolsos.Select(x => x.cuentaDestino).Distinct();
                    foreach (var cuenta in qCuentasDesembolsosPM)
                    {
                        decimal mValor = mContabilidad.desembolsos.Where(x => x.cuentaDestino == cuenta && x.numeroTransferencia == "S").Sum(x => x.valorDesembolso);

                        mCommand.CommandText = "UPDATE prodmult.CUENTA_BANCARIA SET SALDO = ROUND(SALDO + " + mValor.ToString() + ", 2), POSICION_DE_CAJA = ROUND(POSICION_DE_CAJA + " + mValor.ToString() + ", 2), FECHA_ULT_MOV = @Hoy " +
                            "WHERE CUENTA_BANCO = '" + cuenta + "'";
                        mCommand.ExecuteNonQuery();
                    }
                }

                mTransaction.Commit();
                mConexion.Close();

                if (mCuantasPolizas == 0 && mCuantosDepositos == 0 && mCuantosDesembolsos == 0)
                {
                    mRetorna = "Error, no se encontraron pólizas, depósitos ni desembolsos para cargar";
                }
                else
                {
                    mRetorna = string.Format("Se cargaron exitosamente {0} pólizas, {1} depósitos y {2} desembolsos", mCuantasPolizas, mCuantosDepositos, mCuantosDesembolsos);
                }

            }
            catch (Exception ex)
            {
                if (mConexion.State == ConnectionState.Open)
                {
                    mTransaction.Rollback();
                    mConexion.Close();
                }

                mRetorna = string.Format("{0} {1} {2} -- {3}", CatchClass.ExMessage(ex, "PolizasAtidController", "GET"), mMensaje, mCommand.CommandText, mMensaje2);
                EnviarError(mRetorna);
            }

            return mRetorna;
        }

        string Ceros(int asiento)
        {
            string mCeros = "";

            switch (asiento.ToString().Length)
            {
                case 1:
                    mCeros = "00000";
                    break;
                case 2:
                    mCeros = "0000";
                    break;
                case 3:
                    mCeros = "000";
                    break;
                case 4:
                    mCeros = "00";
                    break;
                case 5:
                    mCeros = "0";
                    break;
                default:
                    mCeros = "";
                    break;
            }

            return mCeros;
        }
        
        private void EnviarError(string mensaje)
        {
            using (SmtpClient smtp = new SmtpClient())
            {
                using (MailMessage mail = new MailMessage())
                {
                    string mAsunto = "Error al cargar pólizas ATID";

                    string mNombreUsuario = "Punto de Venta";
                    string mCuerpo = mensaje;
                    string mMailTo = WebConfigurationManager.AppSettings["ITMailNotification"].ToString();
                    string mMailUsuario = "puntodeventa@productosmultiples.com";

                    mail.Subject = mAsunto;
                    mail.To.Add(mMailTo);

                    mail.ReplyToList.Add(mMailUsuario);
                    mail.From = new MailAddress(mMailUsuario, mNombreUsuario);
                    mail.Body = mCuerpo;

                    smtp.Host = WebConfigurationManager.AppSettings["MailHost"];
                    smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort"]);
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                    try
                    {
                        smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail"], WebConfigurationManager.AppSettings["PwdMail"]);
                        smtp.Send(mail);
                    }
                    catch
                    {
                        //Nothing
                    }
                }
            }
        }

        int NumeroTransferencia(ref string consecutivo)
        {
            string mCeros = "";
            int mConsecutivo = Convert.ToInt32(consecutivo) + 1;

            switch (mConsecutivo.ToString().Length)
            {
                case 1:
                    mCeros = "000000";
                    break;
                case 2:
                    mCeros = "00000";
                    break;
                case 3:
                    mCeros = "0000";
                    break;
                case 4:
                    mCeros = "000";
                    break;
                case 5:
                    mCeros = "00";
                    break;
                case 6:
                    mCeros = "0";
                    break;
                default:
                    mCeros = "";
                    break;
            }

            consecutivo = string.Format("{0}{1}", mCeros, mConsecutivo.ToString());
            return mConsecutivo;
        }

    }
}
