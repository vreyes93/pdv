﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.EntityClient;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;
using Newtonsoft.Json;
using RestSharp;

namespace FiestaNETRestServices.Controllers.Contabilidad
{
    [RoutePrefix("api/ReFacturacionAtid")]
    public class ReFacturacionAtidController : MFApiController
    {
        // GET: api/ReFacturacionAtid
        public string Get()
        {
            return "Error, Debe enviar el número de factura a re-facturar";
        }

        // GET: api/ReFacturacionAtid/5
        public string Get(string id)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            string mFacElectronica = "";
            string mAmbiente = WebConfigurationManager.AppSettings["Ambiente"];
            string mUrl = ""; string mUsuario = ""; string mPassword = "";
            string mRetorna = "OK"; string mMensaje = ""; bool mSeAnuloElectronica = false;

            EntityConnection ec = (EntityConnection)db.Connection;
            SqlConnection sc = (SqlConnection)ec.StoreConnection;

            SqlConnection mConexion = new SqlConnection(sc.ConnectionString);
            SqlCommand mCommand = new SqlCommand(); SqlTransaction mTransaction = null;

            try
            {
                mConexion.Open();
                mTransaction = mConexion.BeginTransaction();

                mCommand.Connection = mConexion;
                mCommand.Transaction = mTransaction;

                string mFactura = "";

                mCommand.CommandText = "SELECT NOMBRE_CAT FROM crediplus.CP_Catalogo WHERE CODIGO_TABLA = 1 AND CODIGO_CAT = 'URL_GFACE'";
                string mUrlProduccion = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = "SELECT NOMBRE_CAT FROM crediplus.CP_Catalogo WHERE CODIGO_TABLA = 1 AND CODIGO_CAT = 'USUARIO_GFACE'";
                string mUsuarioProduccion = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = "SELECT NOMBRE_CAT FROM crediplus.CP_Catalogo WHERE CODIGO_TABLA = 1 AND CODIGO_CAT = 'PASSWORD_GFACE'";
                string mPasswordProduccion = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = "SELECT NOMBRE_CAT FROM crediplus.CP_Catalogo WHERE CODIGO_TABLA = 1 AND CODIGO_CAT = 'URL_GFACE_PRUEBAS'";
                string mUrlPruebas = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = "SELECT NOMBRE_CAT FROM crediplus.CP_Catalogo WHERE CODIGO_TABLA = 1 AND CODIGO_CAT = 'USUARIO_GFACE_PRUEBAS'";
                string mUsuarioPruebas = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = "SELECT NOMBRE_CAT FROM crediplus.CP_Catalogo WHERE CODIGO_TABLA = 1 AND CODIGO_CAT = 'PASSWORD_GFACE_PRUEBAS'";
                string mPasswordPruebas = Convert.ToString(mCommand.ExecuteScalar());

                mUrl = mUrlProduccion;
                mUsuario = mUsuarioProduccion;
                mPassword = mPasswordProduccion;

                if (mAmbiente == "DES" || mAmbiente == "PRU")
                {
                    mUrl = mUrlPruebas;
                    mUsuario = mUsuarioPruebas;
                    mPassword = mPasswordPruebas;
                }

                mCommand.CommandText = "SELECT NOMBRE_CAT FROM crediplus.CP_Catalogo WHERE CODIGO_TABLA = 1 AND CODIGO_CAT = 'FACTURA_ELECTRONICA'";
                mFacElectronica = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = "SELECT E_MAIL FROM crediplus.COBRADOR WHERE COBRADOR = 'ND'";
                string[] mConsecutivos = mCommand.ExecuteScalar().ToString().Split(new string[] { "," }, StringSplitOptions.None);

                if (mFacElectronica == "N")
                {
                    if (mConsecutivos.Count() == 0)
                    {
                        mTransaction.Rollback();
                        mConexion.Close();

                        mRetorna = "Error, no se encontró el consecutivo de facturas para crediplus";
                        return mRetorna;
                    }
                }

                string[] mConsecutivoFABK;
                string mConsecutivo = mConsecutivos[0]; string mConsecutivoBK = ""; string mUltimoValorBk = ""; int mDisponibles1 = 0; int mDisponibles2 = 0; int mDisponibles = 0; int mNumeroFacturaBk = 0; string mValorActualBK = "";

                mCommand.CommandText = $"SELECT VALOR_MAXIMO FROM crediplus.CONSECUTIVO_FA WHERE CODIGO_CONSECUTIVO = '{mConsecutivo}'";
                string mUltimoValor = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = $"SELECT VALOR_CONSECUTIVO FROM crediplus.CONSECUTIVO_FA WHERE CODIGO_CONSECUTIVO = '{mConsecutivo}'";
                string mValorActual = Convert.ToString(mCommand.ExecuteScalar());

                string[] mConsecutivoFA = mValorActual.Split(new string[] { "-" }, StringSplitOptions.None);
                Int32 mNumeroFactura = Convert.ToInt32(mConsecutivoFA[1]);

                string[] mConsecutivoUltimoFA = mUltimoValor.Split(new string[] { "-" }, StringSplitOptions.None);
                Int32 mNumeroFacturaUltimo = Convert.ToInt32(mConsecutivoUltimoFA[1]);

                mDisponibles1 = mNumeroFacturaUltimo - mNumeroFactura;

                try
                {
                    mConsecutivoBK = mConsecutivos[1];

                    mCommand.CommandText = $"SELECT VALOR_MAXIMO FROM crediplus.CONSECUTIVO_FA WHERE CODIGO_CONSECUTIVO = '{mConsecutivoBK}'";
                    mUltimoValorBk = Convert.ToString(mCommand.ExecuteScalar());

                    mCommand.CommandText = $"SELECT VALOR_CONSECUTIVO FROM crediplus.CONSECUTIVO_FA WHERE CODIGO_CONSECUTIVO = '{mConsecutivoBK}'";
                    mValorActualBK = Convert.ToString(mCommand.ExecuteScalar());

                    mConsecutivoFABK = mValorActualBK.Split(new string[] { "-" }, StringSplitOptions.None);
                    mNumeroFacturaBk = Convert.ToInt32(mConsecutivoFABK[1]);

                    string[] mConsecutivoUltimoFABk = mUltimoValorBk.Split(new string[] { "-" }, StringSplitOptions.None);
                    Int32 mNumeroFacturaUltimoBk = Convert.ToInt32(mConsecutivoUltimoFABk[1]);

                    mDisponibles2 = mNumeroFacturaUltimoBk - mNumeroFacturaBk;
                    mNumeroFacturaBk = mNumeroFacturaBk - 1;
                }
                catch
                {
                    mConsecutivoBK = "";
                }

                mDisponibles = mDisponibles1 + mDisponibles2;

                if (mFacElectronica == "N")
                {
                    if (mUltimoValor == mValorActual)
                    {
                        mTransaction.Rollback();
                        mConexion.Close();

                        mRetorna = string.Format("Error, no se encontraron facturas disponibles para crediplus, ultimo valor: {0}, valor actual: {1}", mUltimoValor, mValorActual);
                        return mRetorna;
                    }

                    if (mDisponibles == 0)
                    {
                        mTransaction.Rollback();
                        mConexion.Close();

                        mRetorna = string.Format("Error, se tienen {0} facturas disponibles para crediplus, y se necesita 1", mDisponibles);
                        return mRetorna;
                    }
                }

                string mCeros = ""; int mFolioInicialBK = 0;

                mCommand.CommandText = $"SELECT b.FOLIO_INICIAL FROM crediplus.CONSECUTIVO_FA a JOIN crediplus.RESOLUCION_DOC_ELECTRONICO b ON a.RESOLUCION = b.RESOLUCION WHERE a.CODIGO_CONSECUTIVO = '{mConsecutivo}'";
                int mFolioInicial = Convert.ToInt32(mCommand.ExecuteScalar());

                try
                {
                    mCommand.CommandText = $"SELECT b.FOLIO_INICIAL FROM crediplus.CONSECUTIVO_FA a JOIN crediplus.RESOLUCION_DOC_ELECTRONICO b ON a.RESOLUCION = b.RESOLUCION WHERE a.CODIGO_CONSECUTIVO = '{mConsecutivoBK}'";
                    mFolioInicialBK = Convert.ToInt32(mCommand.ExecuteScalar());
                }
                catch
                {
                    mFolioInicialBK = 0;
                }

                if (mFolioInicial == mNumeroFactura) mNumeroFactura = mNumeroFactura - 1;

                mNumeroFactura++;
                int mNumeroFacturaLength = Convert.ToString(mNumeroFactura).Length;
                int mConsecutivoFALength = mConsecutivoFA[1].Length;

                mCeros = "";
                for (int ii = Convert.ToString(mNumeroFactura).Length; ii < mConsecutivoFA[1].Length; ii++)
                {
                    mCeros = string.Format("0{0}", mCeros);
                }

                mFactura = string.Format("{0}-{1}{2}", mConsecutivoFA[0], mCeros, Convert.ToString(mNumeroFactura));

                DateTime mFechaUnoMes = new DateTime(DateTime.Now.Date.Year, DateTime.Now.Date.Month, 1);

                mCommand.Parameters.Add("@XML", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@Error", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@Cliente", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@Factura", SqlDbType.VarChar).Value = mFactura;
                mCommand.Parameters.Add("@FacturaAnular", SqlDbType.VarChar).Value = id;
                mCommand.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = DateTime.Now.Date;
                mCommand.Parameters.Add("@FechaUnoMes", SqlDbType.DateTime).Value = mFechaUnoMes;
                mCommand.Parameters.Add("@Consecutivo", SqlDbType.VarChar).Value = mConsecutivo;
                mCommand.Parameters.Add("@Audit", SqlDbType.Int).Value = 0;
                mCommand.Parameters.Add("@Observaciones", SqlDbType.VarChar).Value = string.Format("Refacturación desde el PDV, viene de factura {0}", id);

                mCommand.Parameters.Add("@DireccionDevuelveGFace", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@NombreDevuelveGFace", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@XMLDocumento", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@XMLReversion", SqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@XMLRespuesta", SqlDbType.VarChar).Value = "";

                mCommand.CommandText = "SELECT COUNT(FACTURA) FROM crediplus.FACTURA WHERE FACTURA = @FacturaAnular";
                if (Convert.ToInt32(mCommand.ExecuteScalar()) == 0)
                {
                    mTransaction.Rollback();
                    mConexion.Close();

                    mRetorna = string.Format("Error, la factura {0} no existe", id);
                    return mRetorna;
                }

                mCommand.CommandText = "SELECT ANULADA FROM crediplus.FACTURA WHERE FACTURA = @FacturaAnular";
                if (Convert.ToString(mCommand.ExecuteScalar()) == "S")
                {
                    mTransaction.Rollback();
                    mConexion.Close();

                    return string.Format("Error, la factura {0} ya está anulada", id);
                }

                mCommand.CommandText = "SELECT CLIENTE FROM crediplus.FACTURA WHERE FACTURA = @FacturaAnular";
                mCommand.Parameters["@Cliente"].Value = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = "SELECT FECHA FROM crediplus.FACTURA WHERE FACTURA = @FacturaAnular";
                DateTime mFecha = Convert.ToDateTime(mCommand.ExecuteScalar());

                if (mFecha.Year == 2018 && mFecha.Month == 11) mCommand.Parameters["@Fecha"].Value = new DateTime(2018, 11, 30);
                
                DateTime mFechaUnMes = DateTime.Now.Date.AddMonths(-1);
                mFechaUnMes = new DateTime(mFechaUnMes.Year, mFechaUnMes.Month, 1);

                if (mFecha < mFechaUnMes)
                {
                    mTransaction.Rollback();
                    mConexion.Close();

                    return string.Format("Error, la factura {0} tiene fecha de hace dos meses ({1}), no puede ser anulada", id, Utilitario.FormatoDDMMYYYY(mFecha));
                }

                if (mFecha.Month == mFechaUnMes.Month)
                {
                    mCommand.CommandText = "SELECT COUNT(FACTURA) FROM crediplus.FACTURA WHERE FECHA >= @FechaUnoMes";
                    if (Convert.ToInt32(mCommand.ExecuteScalar()) > 0)
                    {
                        mTransaction.Rollback();
                        mConexion.Close();

                        return string.Format("Error, la factura {0} tiene fecha del mes anterior ({1}) y ya hay facturas emitidas en el mes en curso, no es posible anularla", id, Utilitario.FormatoDDMMYYYY(mFecha));
                    }

                    mFechaUnMes = mFechaUnoMes.AddDays(-1);
                    mCommand.Parameters["@Fecha"].Value = mFechaUnMes;
                }

                if (mFacElectronica == "S")
                {
                    mCommand.CommandText = $"SELECT COUNT(FACTURA) FROM crediplus.CP_Factura WHERE FACTURA = '{id}'";
                    if (Convert.ToInt32(mCommand.ExecuteScalar()) > 0)
                    {
                        mCommand.CommandText = $"SELECT FIRMADA FROM crediplus.CP_Factura WHERE FACTURA = '{id}'";
                        string mFirmada = Convert.ToString(mCommand.ExecuteScalar());

                        mCommand.CommandText = $"SELECT ANULADA_ELECTRONICA FROM crediplus.CP_Factura WHERE FACTURA = '{id}'";
                        string mAnuladaElectronica = Convert.ToString(mCommand.ExecuteScalar());

                        if (mFirmada == "S" && mAnuladaElectronica == "N")
                        {
                            mCommand.CommandText = $"SELECT SERIE FROM crediplus.CP_Factura WHERE FACTURA = '{id}'";
                            string mSerie = Convert.ToString(mCommand.ExecuteScalar());

                            mCommand.CommandText = $"SELECT NUMERO FROM crediplus.CP_Factura WHERE FACTURA = '{id}'";
                            string mPreImpreso = Convert.ToString(mCommand.ExecuteScalar());

                            mCommand.CommandText = $"SELECT b.CONTRIBUYENTE FROM crediplus.CP_Factura a JOIN crediplus.CLIENTE b ON a.CLIENTE = b.CLIENTE WHERE FACTURA = '{id}'";
                            string mNit = Convert.ToString(mCommand.ExecuteScalar());

                            mCommand.CommandText = $"SELECT FECHA FROM crediplus.FACTURA WHERE FACTURA = '{id}'";
                            DateTime mFechaFactura = Convert.ToDateTime(mCommand.ExecuteScalar());

                            string mFechaAnulacion = string.Format("{0}{1}{2}{3}{4}", mFechaFactura.Year.ToString(), mFechaFactura.Month < 10 ? "0" : "", mFechaFactura.Month.ToString(), mFechaFactura.Day < 10 ? "0" : "", mFechaFactura.Day.ToString());

                            guateFacturas.Guatefac ws = new guateFacturas.Guatefac();
                            ws.Url = mUrl;
                            /**********************************************************************************/
                            //-------------------------------------------------------------------------------
                            System.Net.CredentialCache myCredentials = new System.Net.CredentialCache();
                            NetworkCredential netCred = new NetworkCredential(WebConfigurationManager.AppSettings["UsrGuateFacturasBasicAuth"], WebConfigurationManager.AppSettings["PwdGuateFacturasBasicAuth"]);
                            myCredentials.Add(new Uri(ws.Url), "Basic", netCred);
                            ws.Credentials = myCredentials;
                            //-------------------------------------------------------------------------------

                            string mXmlAnulacion = ws.anulaDocumento(mUsuario, mPassword, "101550790", mSerie, mPreImpreso, mNit, mFechaAnulacion, "Factura no procede");

                            string mResultado = "";
                            XmlDocument xmlRespuesta = new XmlDocument();

                            xmlRespuesta.PreserveWhitespace = true;
                            xmlRespuesta.LoadXml(mXmlAnulacion);

                            mCommand.Parameters["@XML"].Value = mXmlAnulacion;

                            try
                            {
                                mResultado = xmlRespuesta.SelectNodes("RESULTADO/ESTADO").Item(0).InnerText;
                            }
                            catch
                            {
                                mResultado = "";
                            }

                            if (mResultado.Trim().Length == 0)
                            {
                                string mError = "";

                                try
                                {
                                    mError = xmlRespuesta.SelectNodes("RESULTADO/ERROR").Item(0).InnerText;
                                }
                                catch
                                {
                                    mError = "";
                                }

                                mCommand.Parameters["@Error"].Value = mError;
                                
                                mCommand.CommandText = $"UPDATE crediplus.CP_Factura SET ERROR = @Error, XML_RESPUESTA_ANULACION = @XML, FECHA_HORA_ANULACION = GETDATE() WHERE FACTURA = '{id}'";
                                mCommand.ExecuteNonQuery();

                                mTransaction.Commit();
                                mConexion.Close();

                                return string.Format("Error, la factura {0} no pudo ser anulada en el GFACE por la siguiente razón: {1}", id, mError);
                            }

                            mCommand.CommandText = $"UPDATE crediplus.CP_Factura SET ANULADA_ELECTRONICA = 'S', XML_RESPUESTA_ANULACION = @XML, FECHA_HORA_ANULACION = GETDATE() WHERE FACTURA = '{id}'";
                            mCommand.ExecuteNonQuery();

                            mSeAnuloElectronica = true;
                        }
                    }

                    mTransaction.Commit();
                    mConexion.Close();

                    mConexion.Open();
                    mTransaction = mConexion.BeginTransaction();

                    mCommand.Connection = mConexion;
                    mCommand.Transaction = mTransaction;

                    UtilitariosAtid mUtilitariosAtid = new UtilitariosAtid();
                    string mXML = mUtilitariosAtid.xmlCrediplus(id, true, "N", true);
                    string mXMLReversion = mUtilitariosAtid.xmlCrediplus(id, true, "S", true);

                    Clases.RespuestaGuateFacturas mRespuesta = new RespuestaGuateFacturas();
                    mRespuesta = mUtilitariosAtid.firmarFactura(mXML, MF_Clases.Utils.CONSTANTES.GUATEFAC.MAQUINA_CREDIPLUS.FACTURACION_PHOENIX);

                    mFactura = mRespuesta.factura;

                    mCommand.CommandText = $"SELECT CLIENTE FROM crediplus.FACTURA WHERE FACTURA = '{id}'";
                    string mCliente = Convert.ToString(mCommand.ExecuteScalar());

                    mCommand.CommandText = $"SELECT b.NOMBRE FROM crediplus.FACTURA a JOIN crediplus.CLIENTE b ON a.CLIENTE = b.CLIENTE WHERE a.FACTURA = '{id}'";
                    string mNombre = Convert.ToString(mCommand.ExecuteScalar());

                    string mCredito = "";
                    try
                    {
                        mCommand.CommandText = $"SELECT CREDITO FROM crediplus.CP_Factura_Phoenix WHERE FACTURA_EXACTUS = '{id}'";
                        mCredito = Convert.ToString(mCommand.ExecuteScalar());
                    }
                    catch
                    {
                        mCredito = "";
                    }

                    mCommand.CommandText = "SELECT MAX(FECHA) FROM crediplus.CP_Factura";
                    mCommand.Parameters["@Fecha"].Value = Convert.ToDateTime(mCommand.ExecuteScalar());

                    mCommand.CommandText = $"SELECT TOTAL_MERCADERIA FROM crediplus.FACTURA WHERE FACTURA = '{id}'";
                    decimal mValor = Convert.ToDecimal(mCommand.ExecuteScalar());

                    mCommand.CommandText = $"SELECT TOTAL_IMPUESTO1 FROM crediplus.FACTURA WHERE FACTURA = '{id}'";
                    decimal mIva = Convert.ToDecimal(mCommand.ExecuteScalar());

                    mCommand.CommandText = $"SELECT TOTAL_FACTURA FROM crediplus.FACTURA WHERE FACTURA = '{id}'";
                    decimal mTotal = Convert.ToDecimal(mCommand.ExecuteScalar());

                    mCommand.Parameters["@Cliente"].Value = mCliente;
                    mCommand.Parameters["@Nombre"].Value = mNombre;
                    mCommand.Parameters["@DireccionDevuelveGFace"].Value = mRespuesta.direccion;
                    mCommand.Parameters["@NombreDevuelveGFace"].Value = mRespuesta.nombre;

                    
                    mCommand.Parameters["@error"].Value = mRespuesta.error;
                    mCommand.Parameters["@XMLDocumento"].Value = mXML;
                    mCommand.Parameters["@XMLReversion"].Value = mXMLReversion;
                    mCommand.Parameters["@XMLRespuesta"].Value = mRespuesta.xml;

                    mCommand.CommandText = "DELETE FROM crediplus.CP_Factura WHERE FACTURA = ''";
                    mCommand.ExecuteNonQuery();

                    mCommand.CommandText = "INSERT INTO crediplus.CP_Factura ( FACTURA, FACTURA_ELECTRONICA, CLIENTE, FECHA, FIRMADA, FIRMA, SERIE, NUMERO, " +
                        "IDENTIFICADOR, MAQUINA, NOMBRE, DIRECCION, TELEFONO, ERROR, XML_DOCUMENTO, XML_REVERSION, XML_RESPUESTA, " +
                        "FECHA_REGISTRO, ID_CREDIPLUS, CLIENTE_CREDIPLUS, CREDITO_CREDIPLUS, NOMBRE_CREDIPLUS, VALOR_CREDIPLUS, IVA_CREDIPLUS, TOTAL_CREDIPLUS, FECHA_REGISTRO_CREDIPLUS ) " +
                        "VALUES ( '" + mFactura + "', '" + mRespuesta.facturaElectronica + "', @Cliente, @Fecha, '" + mRespuesta.firmada + "', '" + mRespuesta.firma + "', '" + mRespuesta.serie + "', '" + mRespuesta.numero + "', " +
                        "'" + mRespuesta.identificador + "', '" + mRespuesta.maquina + "', @NombreDevuelveGFace, @DireccionDevuelveGFace, '" + mRespuesta.telefono + "', @error, @XMLDocumento, @XMLReversion, @XMLRespuesta, " +
                        "GETDATE(), 0, @Cliente, '" + mCredito + "', @Nombre, " + mValor.ToString() + ", " + mIva.ToString() + ", " + mTotal.ToString() + ", @Fecha ) ";
                    mCommand.ExecuteNonQuery();

                    if (mRespuesta.firmada == "N")
                    {
                        mTransaction.Commit();
                        mConexion.Close();

                        if (mSeAnuloElectronica) mMensaje = string.Format("{0} ***NOTA: La factura fue anulada por el GFACE***", mMensaje);
                        return string.Format("Error, la nueva factura no pudo ser firmada por el GFACE por la siguiente razón: {0} {1}", mRespuesta.error, mMensaje);
                    }
                }


                mCommand.CommandText = "SELECT CARGADO_CG FROM crediplus.FACTURA WHERE FACTURA = @FacturaAnular";
                string mCargadoCG = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = "INSERT INTO crediplus.AUDIT_TRANS_INV ( USUARIO, FECHA_HORA, MODULO_ORIGEN, APLICACION, REFERENCIA ) " +
                    "VALUES ( 'sa', GETDATE(), 'FA', 'FAC#" + mFactura + "', 'Cliente: @Cliente' ) ";
                mCommand.ExecuteNonQuery();

                mCommand.CommandText = "SELECT @@IDENTITY";
                mCommand.Parameters["@Audit"].Value = Convert.ToInt64(mCommand.ExecuteScalar());

                mCommand.CommandText = "INSERT INTO crediplus.FACTURA ( TIPO_DOCUMENTO, FACTURA, AUDIT_TRANS_INV, ESTA_DESPACHADO, EN_INVESTIGACION, TRANS_ADICIONALES, ESTADO_REMISION, DESCUENTO_VOLUMEN, MONEDA_FACTURA, FECHA_DESPACHO, " +
                    "CLASE_DOCUMENTO, FECHA_RECIBIDO, PEDIDO, COMISION_COBRADOR, TARJETA_CREDITO, TOTAL_VOLUMEN, TOTAL_PESO, MONTO_COBRADO, TOTAL_IMPUESTO1, FECHA, FECHA_ENTREGA, TOTAL_IMPUESTO2, PORC_DESCUENTO2, MONTO_FLETE, MONTO_SEGURO, " +
                    "MONTO_DOCUMENTACIO, TIPO_DESCUENTO1, TIPO_DESCUENTO2, MONTO_DESCUENTO1, MONTO_DESCUENTO2, PORC_DESCUENTO1, TOTAL_FACTURA, FECHA_PEDIDO, FECHA_HORA_ANULA, FECHA_ORDEN, TOTAL_MERCADERIA, COMISION_VENDEDOR, FECHA_HORA, " +
                    "TOTAL_UNIDADES, NUMERO_PAGINAS, TIPO_CAMBIO, ANULADA, MODULO, CARGADO_CG, CARGADO_CXC, EMBARCAR_A, DIREC_EMBARQUE, DIRECCION_FACTURA, MULTIPLICADOR_EV, OBSERVACIONES, VERSION_NP, MONEDA, NIVEL_PRECIO, COBRADOR, RUTA, " +
                    "USUARIO, USUARIO_ANULA, CONDICION_PAGO, ZONA, VENDEDOR, CLIENTE_DIRECCION, CLIENTE_CORPORAC, CLIENTE_ORIGEN, CLIENTE, PAIS, SUBTIPO_DOC_CXC, TIPO_DOC_CXC, MONTO_ANTICIPO, TOTAL_PESO_NETO, FECHA_RIGE, PORC_INTCTE, " +
                    "USA_DESPACHOS, COBRADA, DESCUENTO_CASCADA, DIRECCION_EMBARQUE, CONSECUTIVO, REIMPRESO, BASE_IMPUESTO1, BASE_IMPUESTO2, NOMBRE_CLIENTE, NOMBREMAQUINA, SERIE_RESOLUCION, CONSEC_RESOLUCION, GENERA_DOC_FE, " +
                    "TASA_IMPOSITIVA_PORC, TASA_CREE1_PORC, TASA_CREE2_PORC, TASA_GAN_OCASIONAL_PORC, COMENTARIO_CXC, ASIENTO_DOCUMENTO, FACTURA_ORIGINAL ) " +
                        "SELECT TIPO_DOCUMENTO, @Factura, @Audit, ESTA_DESPACHADO, EN_INVESTIGACION, TRANS_ADICIONALES, ESTADO_REMISION, DESCUENTO_VOLUMEN, MONEDA_FACTURA, @Fecha, " +
                            "CLASE_DOCUMENTO, @Fecha, PEDIDO, COMISION_COBRADOR, TARJETA_CREDITO, TOTAL_VOLUMEN, TOTAL_PESO, MONTO_COBRADO, TOTAL_IMPUESTO1, @Fecha, @Fecha, TOTAL_IMPUESTO2, PORC_DESCUENTO2, MONTO_FLETE, MONTO_SEGURO, " +
                            "MONTO_DOCUMENTACIO, TIPO_DESCUENTO1, TIPO_DESCUENTO2, MONTO_DESCUENTO1, MONTO_DESCUENTO2, PORC_DESCUENTO1, TOTAL_FACTURA, @Fecha, FECHA_HORA_ANULA, @Fecha, TOTAL_MERCADERIA, COMISION_VENDEDOR, FECHA_HORA, " +
                            "TOTAL_UNIDADES, NUMERO_PAGINAS, TIPO_CAMBIO, ANULADA, MODULO, CARGADO_CG, CARGADO_CXC, EMBARCAR_A, DIREC_EMBARQUE, DIRECCION_FACTURA, MULTIPLICADOR_EV, @Observaciones, VERSION_NP, MONEDA, NIVEL_PRECIO, COBRADOR, RUTA, " +
                            "USUARIO, USUARIO_ANULA, CONDICION_PAGO, ZONA, VENDEDOR, CLIENTE_DIRECCION, CLIENTE_CORPORAC, CLIENTE_ORIGEN, CLIENTE, PAIS, SUBTIPO_DOC_CXC, TIPO_DOC_CXC, MONTO_ANTICIPO, TOTAL_PESO_NETO, @Fecha, PORC_INTCTE, " +
                            "USA_DESPACHOS, COBRADA, DESCUENTO_CASCADA, DIRECCION_EMBARQUE, @Consecutivo, REIMPRESO, BASE_IMPUESTO1, BASE_IMPUESTO2, NOMBRE_CLIENTE, NOMBREMAQUINA, SERIE_RESOLUCION, CONSEC_RESOLUCION, GENERA_DOC_FE, " +
                            "TASA_IMPOSITIVA_PORC, TASA_CREE1_PORC, TASA_CREE2_PORC, TASA_GAN_OCASIONAL_PORC, COMENTARIO_CXC, ASIENTO_DOCUMENTO, @FacturaAnular " +
                        "FROM crediplus.FACTURA WHERE FACTURA = @FacturaAnular";
                mCommand.ExecuteNonQuery();

                mCommand.CommandText = "INSERT INTO crediplus.DOCUMENTOS_CC ( DOCUMENTO, TIPO, APLICACION, FECHA_DOCUMENTO, FECHA, MONTO, SALDO, MONTO_LOCAL, SALDO_LOCAL, MONTO_DOLAR, " +
                    "SALDO_DOLAR, MONTO_CLIENTE, SALDO_CLIENTE, TIPO_CAMBIO_MONEDA, TIPO_CAMBIO_DOLAR, TIPO_CAMBIO_CLIENT, TIPO_CAMB_ACT_LOC, TIPO_CAMB_ACT_DOL, TIPO_CAMB_ACT_CLI, SUBTOTAL, DESCUENTO, IMPUESTO1, IMPUESTO2, RUBRO1, RUBRO2, " +
                    "MONTO_RETENCION, SALDO_RETENCION, DEPENDIENTE, FECHA_ULT_CREDITO, CARGADO_DE_FACT, APROBADO, ASIENTO_PENDIENTE, FECHA_ULT_MOD, NOTAS, CLASE_DOCUMENTO, FECHA_VENCE, NUM_PARCIALIDADES, COBRADOR, USUARIO_ULT_MOD, CONDICION_PAGO, " +
                    "MONEDA, VENDEDOR, CLIENTE_REPORTE, CLIENTE_ORIGEN, CLIENTE, SUBTIPO, PORC_INTCTE, FECHA_ANUL, AUD_USUARIO_ANUL, AUD_FECHA_ANUL, USUARIO_APROBACION, FECHA_APROBACION, ANULADO, PAIS, BASE_IMPUESTO1, BASE_IMPUESTO2, " +
                    "DEPENDIENTE_GP, SALDO_TRANS, SALDO_TRANS_LOCAL, SALDO_TRANS_DOLAR, SALDO_TRANS_CLI, FACTURADO, GENERA_DOC_FE, ASIENTO ) " +
                        "SELECT @Factura, TIPO, REPLACE(APLICACION, @FacturaAnular, @Factura), @Fecha, @Fecha, MONTO, SALDO, MONTO_LOCAL, SALDO_LOCAL, MONTO_DOLAR, " +
                            "SALDO_DOLAR, MONTO_CLIENTE, SALDO_CLIENTE, TIPO_CAMBIO_MONEDA, TIPO_CAMBIO_DOLAR, TIPO_CAMBIO_CLIENT, TIPO_CAMB_ACT_LOC, TIPO_CAMB_ACT_DOL, TIPO_CAMB_ACT_CLI, SUBTOTAL, DESCUENTO, IMPUESTO1, IMPUESTO2, RUBRO1, RUBRO2, " +
                            "MONTO_RETENCION, SALDO_RETENCION, DEPENDIENTE, FECHA_ULT_CREDITO, CARGADO_DE_FACT, APROBADO, ASIENTO_PENDIENTE, FECHA_ULT_MOD, NOTAS, CLASE_DOCUMENTO, @Fecha, NUM_PARCIALIDADES, COBRADOR, USUARIO_ULT_MOD, CONDICION_PAGO, " +
                            "MONEDA, VENDEDOR, CLIENTE_REPORTE, CLIENTE_ORIGEN, CLIENTE, SUBTIPO, PORC_INTCTE, FECHA_ANUL, AUD_USUARIO_ANUL, AUD_FECHA_ANUL, USUARIO_APROBACION, @Fecha, ANULADO, PAIS, BASE_IMPUESTO1, BASE_IMPUESTO2, " +
                            "DEPENDIENTE_GP, SALDO_TRANS, SALDO_TRANS_LOCAL, SALDO_TRANS_DOLAR, SALDO_TRANS_CLI, FACTURADO, GENERA_DOC_FE, ASIENTO " +
                        "FROM crediplus.DOCUMENTOS_CC WHERE DOCUMENTO = @FacturaAnular";
                mCommand.ExecuteNonQuery();

                mCommand.CommandText = "INSERT INTO crediplus.FACTURA_LINEA ( FACTURA, TIPO_DOCUMENTO, LINEA, BODEGA, COSTO_TOTAL_DOLAR, PEDIDO, ARTICULO, ANULADA, FECHA_FACTURA, CANTIDAD, PRECIO_UNITARIO, TOTAL_IMPUESTO1, TOTAL_IMPUESTO2, " +
                    "DESC_TOT_LINEA, DESC_TOT_GENERAL, COSTO_TOTAL, PRECIO_TOTAL, DESCRIPCION, COMENTARIO, CANTIDAD_DEVUELT, DESCUENTO_VOLUMEN, TIPO_LINEA, CANTIDAD_ACEPTADA, CANT_NO_ENTREGADA, COSTO_TOTAL_LOCAL, PEDIDO_LINEA, " +
                    "MULTIPLICADOR_EV, CANT_DESPACHADA, COSTO_ESTIM_LOCAL, COSTO_ESTIM_DOLAR, CANT_ANUL_PORDESPA, MONTO_RETENCION, BASE_IMPUESTO1, BASE_IMPUESTO2, COSTO_TOTAL_COMP, COSTO_TOTAL_COMP_LOCAL, " +
                    "COSTO_TOTAL_COMP_DOLAR, COSTO_ESTIM_COMP_LOCAL, COSTO_ESTIM_COMP_DOLAR, CANT_DEV_PROCESO ) " +
                        "SELECT @Factura, TIPO_DOCUMENTO, LINEA, BODEGA, COSTO_TOTAL_DOLAR, PEDIDO, ARTICULO, ANULADA, @Fecha, CANTIDAD, PRECIO_UNITARIO, TOTAL_IMPUESTO1, TOTAL_IMPUESTO2, " +
                            "DESC_TOT_LINEA, DESC_TOT_GENERAL, COSTO_TOTAL, PRECIO_TOTAL, DESCRIPCION, COMENTARIO, CANTIDAD_DEVUELT, DESCUENTO_VOLUMEN, TIPO_LINEA, CANTIDAD_ACEPTADA, CANT_NO_ENTREGADA, COSTO_TOTAL_LOCAL, PEDIDO_LINEA, " +
                            "MULTIPLICADOR_EV, CANT_DESPACHADA, COSTO_ESTIM_LOCAL, COSTO_ESTIM_DOLAR, CANT_ANUL_PORDESPA, MONTO_RETENCION, BASE_IMPUESTO1, BASE_IMPUESTO2, COSTO_TOTAL_COMP, COSTO_TOTAL_COMP_LOCAL, " +
                            "COSTO_TOTAL_COMP_DOLAR, COSTO_ESTIM_COMP_LOCAL, COSTO_ESTIM_COMP_DOLAR, CANT_DEV_PROCESO " +
                        "FROM crediplus.FACTURA_LINEA WHERE FACTURA = @FacturaAnular";
                mCommand.ExecuteNonQuery();

                mCommand.CommandText = "UPDATE crediplus.DOCUMENTOS_CC SET APLICACION = '** ANULADO **', MONTO = 0, SALDO = 0, MONTO_LOCAL = 0, SALDO_LOCAL = 0, MONTO_DOLAR = 0, SALDO_DOLAR = 0, MONTO_CLIENTE = 0, SALDO_CLIENTE = 0, SUBTOTAL = 0, " +
                    "IMPUESTO1 = 0, NOTAS = 'Aplicación: Cargado de factura: ' + @FacturaAnular, USUARIO_ULT_MOD = 'sa', AUD_USUARIO_ANUL = 'sa', AUD_FECHA_ANUL = GETDATE(), ANULADO = 'S', ASIENTO = NULL " +
                    "WHERE DOCUMENTO = @FacturaAnular";
                mCommand.ExecuteNonQuery();

                mCommand.CommandText = string.Format("UPDATE crediplus.FACTURA SET FECHA_HORA_ANULA = GETDATE(), ANULADA = 'S', MULTIPLICADOR_EV = 0, PEDIDO = NULL, OBSERVACIONES = 'Refacturación desde el PDV, se generó la factura {0}', USUARIO_ANULA = 'SA', ASIENTO_DOCUMENTO = NULL, COMENTARIO_CXC = NULL " +
                    "WHERE FACTURA = @FacturaAnular", mFactura);
                mCommand.ExecuteNonQuery();

                mCommand.CommandText = "UPDATE crediplus.FACTURA_LINEA SET ANULADA = 'S', PEDIDO = NULL, PEDIDO_LINEA = NULL, MULTIPLICADOR_EV = 0 WHERE FACTURA = @FacturaAnular";
                mCommand.ExecuteNonQuery();

                if (mFacElectronica == "N")
                {
                    mCommand.CommandText = "UPDATE crediplus.CONSECUTIVO_FA SET VALOR_CONSECUTIVO = @Factura WHERE CODIGO_CONSECUTIVO = @Consecutivo";
                    mCommand.ExecuteNonQuery();

                    if (mFactura == mUltimoValor)
                    {
                        mNumeroFactura = mNumeroFacturaBk;
                        mConsecutivo = mConsecutivoBK;

                        mCommand.CommandText = $"UPDATE crediplus.COBRADOR SET E_MAIL = @Consecutivo + ',' WHERE COBRADOR = 'ND'";
                        mCommand.ExecuteNonQuery();
                    }
                }

                mTransaction.Commit();
                mConexion.Close();

                if (mFactura == mUltimoValor) EnviarError("Se utilizó el consecutivo de BackUP de crediplus, es urgente agregar un consecutivo de BackUP nuevo.", "BK");
                mRetorna = string.Format("Factura {0} anulada, y se generó la {1}", id, mFactura);
            }
            catch (Exception ex)
            {
                if (mConexion.State == ConnectionState.Open)
                {
                    mTransaction.Rollback();
                    mConexion.Close();
                }

                if (mFacElectronica == "S" && mSeAnuloElectronica) mMensaje = string.Format("{0} ***NOTA: La factura fue anulada por el GFACE***", mMensaje);
                mRetorna = string.Format("{0} {1} {2}", CatchClass.ExMessage(ex, "ReFacturacionAtidController", "GET"), mMensaje, mCommand.CommandText);
            }

            return mRetorna;
        }

        private void EnviarError(string mensaje, string conta = "")
        {
            using (SmtpClient smtp = new SmtpClient())
            {
                using (MailMessage mail = new MailMessage())
                {
                    string mAsunto = "Error al cargar facturas ATID";

                    string mNombreUsuario = "Punto de Venta";
                    string mCuerpo = mensaje;
                    string mMailTo = WebConfigurationManager.AppSettings["ITMailNotification"].ToString();
                    string mMailUsuario = "puntodeventa@productosmultiples.com";

                    if (conta == "")
                    {
                        mail.To.Add(mMailTo);
                    }
                    else
                    {
                        var q = from c in db.MF_Catalogo where c.CODIGO_TABLA == 28 select c;
                        foreach (var item in q)
                        {
                            mail.To.Add(item.NOMBRE_CAT);
                        }

                        mAsunto = "Facturas disponibles de crediplus";
                        if (conta == "BK") mAsunto = "Consecutivo de BackUP de crediplus";
                    }

                    mail.Subject = mAsunto;

                    mail.ReplyToList.Add(mMailUsuario);
                    mail.From = new MailAddress(mMailUsuario, mNombreUsuario);
                    mail.Body = mCuerpo;

                    smtp.Host = WebConfigurationManager.AppSettings["MailHost2"];
                    smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort2"]);
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                    try
                    {
                        smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail2"], WebConfigurationManager.AppSettings["PwdMail2"]);
                        smtp.Send(mail);
                    }
                    catch
                    {
                        //Nothing
                    }
                }
            }
        }


    }
}
