﻿using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Contabilidad
{
    public class SaldosController : ApiController
    {
        /// <summary>
        /// Facturas sin pago
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public List<Clases.DocumentoConSaldo> FacturasSinPago(string id)
        {
            DAL.DALFactura saldoFactura = new DAL.DALFactura();
            var facturasSaldos = saldoFactura.FacturasConSaldo(id);
            return facturasSaldos;
        }
    }
}
