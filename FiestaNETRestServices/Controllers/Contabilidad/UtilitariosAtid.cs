﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Data;
using System.Data.SqlClient;
using System.Data.EntityClient;
using MF.DAL;
using MF_Clases;
using FiestaNETRestServices.Models;
using System.Web.Configuration;
using System.Net;
using System.Net.Mime;
using System.IO;
using System.Security.Cryptography;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Channels;
using FiestaNETRestServices.Content.Abstract;
using static MF_Clases.Clases;
using FiestaNETRestServices.DAL;
using System.Transactions;

namespace FiestaNETRestServices.Controllers.Contabilidad
{
    public class UtilitariosAtid : MFDAL
    {


        public Clases.RespuestaGuateFacturas firmarFactura(string xml, string Maquina)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            string mAmbiente = WebConfigurationManager.AppSettings["Ambiente"];
            string mUrl = ""; string mUsuario = ""; string mPassword = "";

            EXACTUSERPEntities db = new EXACTUSERPEntities();

            EntityConnection ec = (EntityConnection)db.Connection;
            SqlConnection sc = (SqlConnection)ec.StoreConnection;

            SqlConnection mConexion = new SqlConnection(sc.ConnectionString);
            SqlCommand mCommand = new SqlCommand();

            Clases.RespuestaGuateFacturas mRetorna = new Clases.RespuestaGuateFacturas(); string mMensaje = "";

            mRetorna.firmada = "N";
            mRetorna.firma = "";
            mRetorna.serie = "";
            mRetorna.numero = "";
            mRetorna.identificador = "";
            mRetorna.maquina = "";
            mRetorna.factura = "";
            mRetorna.facturaElectronica = "";
            mRetorna.nombre = "";
            mRetorna.direccion = "";
            mRetorna.telefono = "";
            mRetorna.error = "";
            mRetorna.xml = "";

            try
            {
                mConexion.Open();
                mCommand.Connection = mConexion;

                mCommand.CommandText = "SELECT NOMBRE_CAT FROM crediplus.CP_Catalogo WHERE CODIGO_TABLA = 1 AND CODIGO_CAT = 'URL_GFACE'";
                string mUrlProduccion = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = "SELECT NOMBRE_CAT FROM crediplus.CP_Catalogo WHERE CODIGO_TABLA = 1 AND CODIGO_CAT = 'USUARIO_GFACE'";
                string mUsuarioProduccion = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = "SELECT NOMBRE_CAT FROM crediplus.CP_Catalogo WHERE CODIGO_TABLA = 1 AND CODIGO_CAT = 'PASSWORD_GFACE'";
                string mPasswordProduccion = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = "SELECT NOMBRE_CAT FROM crediplus.CP_Catalogo WHERE CODIGO_TABLA = 1 AND CODIGO_CAT = 'URL_GFACE_PRUEBAS'";
                string mUrlPruebas = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = "SELECT NOMBRE_CAT FROM crediplus.CP_Catalogo WHERE CODIGO_TABLA = 1 AND CODIGO_CAT = 'USUARIO_GFACE_PRUEBAS'";
                string mUsuarioPruebas = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = "SELECT NOMBRE_CAT FROM crediplus.CP_Catalogo WHERE CODIGO_TABLA = 1 AND CODIGO_CAT = 'PASSWORD_GFACE_PRUEBAS'";
                string mPasswordPruebas = Convert.ToString(mCommand.ExecuteScalar());


                mConexion.Close();

                mUrl = mUrlProduccion;
                mUsuario = mUsuarioProduccion;
                mPassword = mPasswordProduccion;

                if (General.Ambiente != "PRO")
                {
                    mUrl = mUrlPruebas;
                    mUsuario = mUsuarioPruebas;
                    mPassword = mPasswordPruebas;
                }

                XmlDocument xmlDoc = new XmlDocument();

                xmlDoc.PreserveWhitespace = true;
                xmlDoc.LoadXml(xml);

                string mReferencia = xmlDoc.SelectNodes("DocElectronico/Encabezado/InfoDoc/Referencia").Item(0).InnerText;

                guateFacturas.Guatefac ws = new guateFacturas.Guatefac();
                ws.Url = mUrl;
                /**********************************************************************************/
                //-------------------------------------------------------------------------------
                System.Net.CredentialCache myCredentials = new System.Net.CredentialCache();
                NetworkCredential netCred = new NetworkCredential(WebConfigurationManager.AppSettings["UsrGuateFacturasBasicAuth"], WebConfigurationManager.AppSettings["PwdGuateFacturasBasicAuth"]);
                myCredentials.Add(new Uri(ws.Url), "Basic", netCred);
                ws.Credentials = myCredentials;
                //-------------------------------------------------------------------------------
                mRetorna.xml = ws.generaDocumento(mUsuario, mPassword, "101550790", 1, 1, Maquina, "R", xml);

                string mFirma = "";
                XmlDocument xmlRespuesta = new XmlDocument();

                xmlRespuesta.PreserveWhitespace = true;
                xmlRespuesta.LoadXml(mRetorna.xml);

                try
                {
                    mFirma = xmlRespuesta.SelectNodes("Resultado/Firma").Item(0).InnerText;
                }
                catch
                {
                    mFirma = "";
                }

                if (mFirma.Trim().Length == 0)
                {
                    string mError = xmlRespuesta.SelectNodes("Resultado").Item(0).InnerText;

                    if (mError.Contains("NO EXISTE EL NIT"))
                    {
                        XmlDocument xmlDocumento = new XmlDocument();

                        xmlDocumento.PreserveWhitespace = true;
                        xmlDocumento.LoadXml(xml);

                        string mNit = xmlDocumento.SelectNodes("DocElectronico/Encabezado/Receptor/NITReceptor").Item(0).InnerText;
                        xml = xml.Replace(mNit, "CF");

                        mRetorna.xml = ws.generaDocumento(mUsuario, mPassword, "101550790", 1, 1, Maquina, "R", xml);
                        xmlRespuesta.LoadXml(mRetorna.xml);

                        try
                        {
                            mFirma = xmlRespuesta.SelectNodes("Resultado/Firma").Item(0).InnerText;
                        }
                        catch
                        {
                            mFirma = "";
                            mRetorna.error = xmlRespuesta.SelectNodes("Resultado").Item(0).InnerText;
                        }
                    }
                    else
                    {
                        mRetorna.error = mError;
                    }
                }

                if (mFirma.Trim().Length > 0)
                {
                    mRetorna.firmada = "S";
                    mRetorna.firma = mFirma;
                    mRetorna.serie = xmlRespuesta.SelectNodes("Resultado/Serie").Item(0).InnerText;
                    mRetorna.numero = xmlRespuesta.SelectNodes("Resultado/Preimpreso").Item(0).InnerText;
                    mRetorna.identificador = "FACE-63";
                    mRetorna.maquina = "001";
                    mRetorna.factura = string.Format("{0}-{1}", mRetorna.serie, mRetorna.numero);
                    mRetorna.facturaElectronica = string.Format("{0}-{1}", mRetorna.serie, mRetorna.numero);
                    //mRetorna.facturaElectronica = string.Format("FACE-63-{0}-001-{1}", xmlRespuesta.SelectNodes("Resultado/Serie").Item(0).InnerText, xmlRespuesta.SelectNodes("Resultado/Preimpreso").Item(0).InnerText);
                    mRetorna.nombre = xmlRespuesta.SelectNodes("Resultado/Nombre").Item(0).InnerText;
                    mRetorna.direccion = xmlRespuesta.SelectNodes("Resultado/Direccion").Item(0).InnerText;
                    mRetorna.telefono = xmlRespuesta.SelectNodes("Resultado/Telefono").Item(0).InnerText;
                }
            }
            catch (Exception ex)
            {
                if (mConexion.State == ConnectionState.Open) mConexion.Close();
                mRetorna.error = string.Format("{0} {1} {2} {3} {4}", CatchClass.ExMessage(ex, "UtilitariosATID", "GET"), mMensaje, mUrl, mUsuario, mPassword);
            }

            return mRetorna;
        }

        public string xmlCrediplus(string info, bool exactus, string reversion)
        {
            Clases.FacturasAtid mFactura = new Clases.FacturasAtid();
            return generaXML(info, mFactura, exactus, reversion, false);
        }

        public string xmlCrediplus(string info, bool exactus, string reversion, bool reFacturacion)
        {
            Clases.FacturasAtid mFactura = new Clases.FacturasAtid();
            return generaXML(info, mFactura, exactus, reversion, reFacturacion);
        }


        public string xmlCrediplus(Clases.FacturasAtid factura, string reversion, string TipoVenta = "S")
        {
            return generaXML("", factura, false, reversion, false, TipoVenta);
        }

        string generaXML(string info, Clases.FacturasAtid fac, bool exactus, string reversion, bool reFacturacion, string TipoVenta = "S")
        {
            string mAmbiente = WebConfigurationManager.AppSettings["Ambiente"].ToString();
            EXACTUSERPEntities db = new EXACTUSERPEntities();

            EntityConnection ec = (EntityConnection)db.Connection;
            SqlConnection sc = (SqlConnection)ec.StoreConnection;

            SqlConnection mConexion = new SqlConnection(sc.ConnectionString);
            SqlCommand mCommand = new SqlCommand();

            StringBuilder mXML = new StringBuilder();

            decimal mValor = 0; decimal mIva = 0; decimal mTotal = 0; DateTime mFecha = DateTime.Now;
            string mNit = ""; string mEmail = ""; string mNombreCliente = ""; string mDireccion = ""; string mCredito = ""; string mReferencia = "";

            if (info == "")
            {

                mValor = fac.detalleFactura.Where(x => x.idFactura == fac.id).Sum(x => x.valor);
                mIva = fac.detalleFactura.Where(x => x.idFactura == fac.id).Sum(x => x.iva);
                mTotal = fac.detalleFactura.Where(x => x.idFactura == fac.id).Sum(x => x.total);

                mNombreCliente = fac.nombrecliente;
                mDireccion = fac.direccion;
                mFecha = fac.fechaRegistro;
                mCredito = fac.credito;

                mNit = fac.nit;
                if (mNit == "" || mNit == "C/F" || mNit == "C F") mNit = "CF";

                mEmail = fac.eMail;
                if (exactus) mEmail = WebConfigurationManager.AppSettings["ITMailNotification"].ToString();

                string mEnviar = "S";
                if (mEmail.Trim().Length == 0 || mEmail.Contains("tiene"))
                    mEnviar = "N";

                mXML.Append(string.Format("<DocElectronico>"));
                mXML.Append(string.Format("   <Encabezado>"));
                mXML.Append(string.Format("      <Receptor>"));
                mXML.Append(string.Format("         <NITReceptor>{0}</NITReceptor>", mNit));
                mXML.Append(string.Format("         <Nombre>{0}</Nombre>", mNombreCliente));
                mXML.Append(string.Format("         <Direccion>{0}</Direccion>", mDireccion));
                mXML.Append(string.Format("      </Receptor>"));
                mXML.Append(string.Format("      <InfoDoc>"));
                mXML.Append(string.Format("         <TipoVenta>{0}</TipoVenta>", TipoVenta));
                mXML.Append(string.Format("         <DestinoVenta>{0}</DestinoVenta>", "1"));
                mXML.Append(string.Format("         <Fecha>{0}</Fecha>", Utilitarios.FormatoDDMMYYYY(mFecha)));
                mXML.Append(string.Format("         <Moneda>{0}</Moneda>", "1"));
                mXML.Append(string.Format("         <Tasa>{0}</Tasa>", "1.00000"));
                mXML.Append(string.Format("         <Referencia>{0}</Referencia>", "REF_" + fac.credito));
                mXML.Append(string.Format("         <Reversion>{0}</Reversion>", reversion));
                mXML.Append(string.Format("      </InfoDoc>"));
                mXML.Append(string.Format("      <Totales>"));
                mXML.Append(string.Format("         <Bruto>{0}</Bruto>", Math.Round(mTotal, 2, MidpointRounding.AwayFromZero).ToString()));
                mXML.Append(string.Format("         <Descuento>{0}</Descuento>", "0"));
                mXML.Append(string.Format("         <Exento>{0}</Exento>", "0"));
                mXML.Append(string.Format("         <Otros>{0}</Otros>", "0"));
                mXML.Append(string.Format("         <Neto>{0}</Neto>", Math.Round(mValor, 2, MidpointRounding.AwayFromZero).ToString()));
                mXML.Append(string.Format("         <Isr>{0}</Isr>", "0"));
                mXML.Append(string.Format("         <Iva>{0}</Iva>", Math.Round(mIva, 2, MidpointRounding.AwayFromZero).ToString()));
                mXML.Append(string.Format("         <Total>{0}</Total>", Math.Round(mTotal, 2, MidpointRounding.AwayFromZero).ToString()));
                mXML.Append(string.Format("      </Totales>"));
                mXML.Append(string.Format("      <DatosAdicionales>"));
                mXML.Append(string.Format("         <Email>{0}</Email>", mEmail));
                mXML.Append(string.Format("         <Enviar>{0}</Enviar>", mEnviar));
                mXML.Append(string.Format("      </DatosAdicionales>"));
                mXML.Append(string.Format("   </Encabezado>"));

                mXML.Append(string.Format("   <Detalles>"));
                foreach (var linea in fac.detalleFactura)
                {
                    string mArticulo = "00000002";
                    string Descripcion = string.Format("{0} Préstamo No. {1}", linea.descripcion, mCredito);

                    if (linea.descripcion.ToLower().Contains("inter"))
                        mArticulo = "00000001";
                    if (linea.descripcion.ToLower().Contains("varios"))
                        mArticulo = "00000003";

                    if (TipoVenta != "S")
                    {
                        mArticulo = linea.producto;
                        Descripcion = linea.descripcion;
                    }

                    mXML.Append(string.Format("      <Productos>"));
                    mXML.Append(string.Format("         <Producto>{0}</Producto>", mArticulo));
                    mXML.Append(string.Format("         <Descripcion>{0}</Descripcion>", Descripcion));
                    mXML.Append(string.Format("         <Medida>{0}</Medida>", "1"));
                    mXML.Append(string.Format("         <Cantidad>{0}</Cantidad>", "1"));
                    mXML.Append(string.Format("         <Precio>{0}</Precio>", Math.Round(linea.total, 2, MidpointRounding.AwayFromZero).ToString()));
                    mXML.Append(string.Format("         <PorcDesc>{0}</PorcDesc>", "0"));
                    mXML.Append(string.Format("         <ImpBruto>{0}</ImpBruto>", Math.Round(linea.total, 2, MidpointRounding.AwayFromZero).ToString()));
                    mXML.Append(string.Format("         <ImpDescuento>{0}</ImpDescuento>", "0"));
                    mXML.Append(string.Format("         <ImpExento>{0}</ImpExento>", "0"));
                    mXML.Append(string.Format("         <ImpOtros>{0}</ImpOtros>", "0"));
                    mXML.Append(string.Format("         <ImpNeto>{0}</ImpNeto>", Math.Round(linea.valor, 2, MidpointRounding.AwayFromZero).ToString()));
                    mXML.Append(string.Format("         <ImpIsr>{0}</ImpIsr>", "0"));
                    mXML.Append(string.Format("         <ImpIva>{0}</ImpIva>", Math.Round(linea.iva, 2, MidpointRounding.AwayFromZero).ToString()));
                    mXML.Append(string.Format("         <ImpTotal>{0}</ImpTotal>", Math.Round(linea.total, 2, MidpointRounding.AwayFromZero).ToString()));
                    mXML.Append(string.Format("         <DatosAdicionalesProd><TIPO_VENTA>" + TipoVenta + "</TIPO_VENTA></DatosAdicionalesProd>"));
                    mXML.Append(string.Format("      </Productos>"));
                }

                mXML.Append(string.Format("   </Detalles>"));
                mXML.Append(string.Format("</DocElectronico>"));
            }
            else
            {
                mConexion.Open();
                mCommand.Connection = mConexion;

                string mSQL = ""; string mCliente = ""; string mFactura = "";

                if (exactus)
                {
                    mFactura = info;

                    mSQL = "SELECT b.CLIENTE, c.CONTRIBUYENTE AS NIT, c.NOMBRE AS NOMBRE_CLIENTE, '' AS CREDITO, b.DIRECCION_FACTURA AS DIRECCION, c.E_MAIL, a.ARTICULO AS PRODUCTO, CONVERT(VARCHAR(200), a.DESCRIPCION) AS DESCRIPCION, " +
                        "SUM(a.PRECIO_TOTAL) AS VALOR, SUM(a.TOTAL_IMPUESTO1) AS IVA, (SUM(a.PRECIO_TOTAL) + SUM(a.TOTAL_IMPUESTO1)) AS TOTAL, b.FECHA, b.COMENTARIO_CXC AS REFERENCIA " +
                        "FROM crediplus.FACTURA_LINEA a JOIN crediplus.FACTURA b ON a.FACTURA = b.FACTURA JOIN crediplus.CLIENTE c ON b.CLIENTE = c.CLIENTE " +
                        "WHERE b.FACTURA = '" + mFactura + "' " +
                        "GROUP BY b.CLIENTE, c.CONTRIBUYENTE, c.NOMBRE, b.DIRECCION_FACTURA, c.E_MAIL, a.ARTICULO, CONVERT(VARCHAR(200), a.DESCRIPCION), b.FECHA, b.COMENTARIO_CXC";
                }
                else
                {
                    mCliente = info;

                    mSQL = "SELECT b.CLIENTE, b.NIT, b.NOMBRE_CLIENTE, b.CREDITO, b.DIRECCION, b.E_MAIL, a.PRODUCTO, a.DESCRIPCION, SUM(a.VALOR) AS VALOR, SUM(a.IVA) AS IVA, SUM(a.TOTAL) AS TOTAL, MAX(a.ID_FACTURA) AS REFERENCIA " +
                        "FROM CREDIPLUS.CP_Factura_Phoenix_Linea a JOIN CREDIPLUS.CP_Factura_Phoenix b ON a.ID_FACTURA = b.ID " +
                        "WHERE b.EN_EXACTUS = 'N' AND b.CLIENTE = '" + mCliente + "' " +
                        "GROUP BY b.CLIENTE, b.NIT, b.NOMBRE_CLIENTE, B.CREDITO, b.DIRECCION, b.E_MAIL, a.PRODUCTO, a.DESCRIPCION";
                }

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(mSQL, mConexion);

                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    mValor = Convert.ToDecimal(dt.Compute("SUM(VALOR)", ""));
                    mIva = Convert.ToDecimal(dt.Compute("SUM(IVA)", ""));
                    mTotal = Convert.ToDecimal(dt.Compute("SUM(TOTAL)", ""));

                    mNombreCliente = Convert.ToString(dt.Rows[0]["NOMBRE_CLIENTE"]);
                    mDireccion = Convert.ToString(dt.Rows[0]["DIRECCION"]);
                    mCredito = Convert.ToString(dt.Rows[0]["CREDITO"]);

                    try
                    {
                        mReferencia = Convert.ToString(dt.Rows[0]["REFERENCIA"]);
                    }
                    catch
                    {
                        mReferencia = "";
                    }

                    if (exactus)
                    {
                        mFecha = Convert.ToDateTime(dt.Rows[0]["FECHA"]);

                        if (reFacturacion)
                        {
                            //mCommand.CommandText = "SELECT MAX(FECHA) FROM crediplus.CP_Factura";
                            //mFecha = Convert.ToDateTime(mCommand.ExecuteScalar());

                            DateTime mFechaTemp = mFecha.AddMonths(1);
                            mFechaTemp = new DateTime(mFechaTemp.Year, mFechaTemp.Month, 1);
                            mFecha = mFechaTemp.AddDays(-1);
                        }
                    }
                    else
                    {
                        mCommand.CommandText = "SELECT TOP 1 bb.FECHA_REGISTRO FROM crediplus.CP_Factura_Phoenix bb WHERE bb.EN_EXACTUS = 'N' AND bb.CLIENTE = '" + mCliente + "' ORDER BY bb.FECHA_REGISTRO DESC";
                        mFecha = Convert.ToDateTime(mCommand.ExecuteScalar());
                    }

                    mNit = Convert.ToString(dt.Rows[0]["NIT"]);
                    if (mNit == "" || mNit == "C/F" || mNit == "C F") mNit = "CF";

                    mEmail = Convert.ToString(dt.Rows[0]["E_MAIL"]);
                    if (exactus) mEmail = WebConfigurationManager.AppSettings["ITMailNotification"].ToString();

                    string mEnviar = "S";
                    if (mEmail.Trim().Length == 0 || mEmail.Contains("tiene")) mEnviar = "N";

                    mXML.Append(string.Format("<DocElectronico>"));
                    mXML.Append(string.Format("   <Encabezado>"));
                    mXML.Append(string.Format("      <Receptor>"));
                    mXML.Append(string.Format("         <NITReceptor>{0}</NITReceptor>", mNit));
                    mXML.Append(string.Format("         <Nombre>{0}</Nombre>", mNombreCliente));
                    mXML.Append(string.Format("         <Direccion>{0}</Direccion>", mDireccion));
                    mXML.Append(string.Format("      </Receptor>"));
                    mXML.Append(string.Format("      <InfoDoc>"));
                    mXML.Append(string.Format("         <TipoVenta>{0}</TipoVenta>", TipoVenta));
                    mXML.Append(string.Format("         <DestinoVenta>{0}</DestinoVenta>", "1"));
                    mXML.Append(string.Format("         <Fecha>{0}</Fecha>", Utilitarios.FormatoDDMMYYYY(mFecha)));
                    mXML.Append(string.Format("         <Moneda>{0}</Moneda>", "1"));
                    mXML.Append(string.Format("         <Tasa>{0}</Tasa>", "1.00000"));
                    mXML.Append(string.Format("         <Referencia>{0}</Referencia>", mReferencia));
                    mXML.Append(string.Format("         <Reversion>{0}</Reversion>", reversion));
                    mXML.Append(string.Format("      </InfoDoc>"));
                    mXML.Append(string.Format("      <Totales>"));
                    mXML.Append(string.Format("         <Bruto>{0}</Bruto>", Math.Round(mTotal, 2, MidpointRounding.AwayFromZero).ToString()));
                    mXML.Append(string.Format("         <Descuento>{0}</Descuento>", "0"));
                    mXML.Append(string.Format("         <Exento>{0}</Exento>", "0"));
                    mXML.Append(string.Format("         <Otros>{0}</Otros>", "0"));
                    mXML.Append(string.Format("         <Neto>{0}</Neto>", Math.Round(mValor, 2, MidpointRounding.AwayFromZero).ToString()));
                    mXML.Append(string.Format("         <Isr>{0}</Isr>", "0"));
                    mXML.Append(string.Format("         <Iva>{0}</Iva>", Math.Round(mIva, 2, MidpointRounding.AwayFromZero).ToString()));
                    mXML.Append(string.Format("         <Total>{0}</Total>", Math.Round(mTotal, 2, MidpointRounding.AwayFromZero).ToString()));
                    mXML.Append(string.Format("      </Totales>"));
                    mXML.Append(string.Format("      <DatosAdicionales>"));
                    mXML.Append(string.Format("         <Email>{0}</Email>", mEmail));
                    mXML.Append(string.Format("         <Enviar>{0}</Enviar>", mEnviar));
                    mXML.Append(string.Format("      </DatosAdicionales>"));
                    mXML.Append(string.Format("   </Encabezado>"));

                    mXML.Append(string.Format("   <Detalles>"));

                    foreach (DataRow row in dt.Rows)
                    {
                        string mArticulo = "00000002";
                        if (Convert.ToString(row["DESCRIPCION"]).ToLower().Contains("inter")) mArticulo = "00000001";
                        if (Convert.ToString(row["DESCRIPCION"]).ToLower().Contains("varios")) mArticulo = "00000003";

                        string mDescripcion = string.Format("{0} Préstamo No. {1}", Convert.ToString(row["DESCRIPCION"]), mCredito);
                        if (exactus) mDescripcion = Convert.ToString(row["DESCRIPCION"]);

                        mXML.Append(string.Format("      <Productos>"));
                        mXML.Append(string.Format("         <Producto>{0}</Producto>", mArticulo));
                        mXML.Append(string.Format("         <Descripcion>{0}</Descripcion>", mDescripcion));
                        mXML.Append(string.Format("         <Medida>{0}</Medida>", "1"));
                        mXML.Append(string.Format("         <Cantidad>{0}</Cantidad>", "1"));
                        mXML.Append(string.Format("         <Precio>{0}</Precio>", Math.Round(Convert.ToDecimal(row["TOTAL"]), 2, MidpointRounding.AwayFromZero).ToString()));
                        mXML.Append(string.Format("         <PorcDesc>{0}</PorcDesc>", "0"));
                        mXML.Append(string.Format("         <ImpBruto>{0}</ImpBruto>", Math.Round(Convert.ToDecimal(row["TOTAL"]), 2, MidpointRounding.AwayFromZero).ToString()));
                        mXML.Append(string.Format("         <ImpDescuento>{0}</ImpDescuento>", "0"));
                        mXML.Append(string.Format("         <ImpExento>{0}</ImpExento>", "0"));
                        mXML.Append(string.Format("         <ImpOtros>{0}</ImpOtros>", "0"));
                        mXML.Append(string.Format("         <ImpNeto>{0}</ImpNeto>", Math.Round(Convert.ToDecimal(row["VALOR"]), 2, MidpointRounding.AwayFromZero).ToString()));
                        mXML.Append(string.Format("         <ImpIsr>{0}</ImpIsr>", "0"));
                        mXML.Append(string.Format("         <ImpIva>{0}</ImpIva>", Math.Round(Convert.ToDecimal(row["IVA"]), 2, MidpointRounding.AwayFromZero).ToString()));
                        mXML.Append(string.Format("         <ImpTotal>{0}</ImpTotal>", Math.Round(Convert.ToDecimal(row["TOTAL"]), 2, MidpointRounding.AwayFromZero).ToString()));
                        mXML.Append(string.Format("         <DatosAdicionalesProd><TIPO_VENTA>S</TIPO_VENTA></DatosAdicionalesProd>"));
                        mXML.Append(string.Format("      </Productos>"));
                    }

                    mXML.Append(string.Format("   </Detalles>"));
                    mXML.Append(string.Format("</DocElectronico>"));
                }

                mConexion.Close();
            }

            return mXML.ToString();
        }

        private bool ValidarProductosCrediplus(string facturaMF, ref string msgError)
        {
            var qDetalle = new DALCrediplus().ObtenerProductosCrediplus(facturaMF);
            foreach (var item in qDetalle)
            {
                if (!item.Activo)
                {
                    msgError = "El artículo " + item.CodigoCP + " de Crediplus no se encuentra activo! no se puede facturar a Muebles Fiesta";
                    return false;
                }
                if (!item.HayExistencia)
                {
                    msgError = "No hay existencias del artículo " + item.CodigoCP + " de Crediplus no se puede facturar a Muebles Fiesta";
                    return false;
                }
                if (!item.Existe)
                {
                    msgError = "No existe artículo " + item.CodigoCP + " en Crediplus no se puede facturar a Muebles Fiesta";
                    return false;
                }
            }
            return true;
        }

        public Respuesta Facturar(string FacturaFiesta)
        {
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            Respuesta mresp = new Respuesta();
            EntityConnection ec = (EntityConnection)db.Connection;
            SqlConnection sc = (SqlConnection)ec.StoreConnection;
            SqlConnection mConexion = new SqlConnection(sc.ConnectionString);
            SqlCommand mCommand = new SqlCommand();
            SqlTransaction mTransaction = null;
            string msgError = string.Empty;
            try
            {


                Clases.FacturasAtid_MF mfFactura = ObtenerDatosFacturacionFiesta(FacturaFiesta);
                string mXML = xmlCrediplus(mfFactura, "N", MF_Clases.Utils.CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES);
                string mXMLReversion = xmlCrediplus(mfFactura, "S", MF_Clases.Utils.CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES);

                Clases.RespuestaGuateFacturas mRespuesta = new RespuestaGuateFacturas();

                log.Info("INTENTO (1) DE FIRMA ELECTRÓNICA DE FACTURA DE CREDIPLUS A MUEBLES FIESTA");
                log.Info("XML: " + mXML);
                mRespuesta = firmarFactura(mXML, MF_Clases.Utils.CONSTANTES.GUATEFAC.MAQUINA_CREDIPLUS.FACTURACION_DEFAULT);
                if (mRespuesta.firmada == "N")
                {
                    log.Info("INTENTO (2) DE FIRMA ELECTRÓNICA DE FACTURA DE CREDIPLUS A MUEBLES FIESTA");
                    mRespuesta = firmarFactura(mXML, MF_Clases.Utils.CONSTANTES.GUATEFAC.MAQUINA_CREDIPLUS.FACTURACION_DEFAULT);
                }
                if (mRespuesta.firmada == "N")
                {
                    log.Info("INTENTO (3) DE FIRMA ELECTRÓNICA DE FACTURA DE CREDIPLUS A MUEBLES FIESTA");
                    mRespuesta = firmarFactura(mXML, MF_Clases.Utils.CONSTANTES.GUATEFAC.MAQUINA_CREDIPLUS.FACTURACION_DEFAULT);
                }

                if (mRespuesta.firmada == "S")
                {



                    log.Info("Comenzando la transacción de facturación de CREDIPLUS a Productos Múltiples");
                    mConexion.Open();
                    mTransaction = mConexion.BeginTransaction();

                    mCommand.Connection = mConexion;
                    mCommand.Transaction = mTransaction;

                    mCommand.CommandText = "SELECT TOP 1 MONTO FROM prodmult.TIPO_CAMBIO_HIST WHERE TIPO_CAMBIO = 'REFR' ORDER BY FECHA DESC";
                    decimal mTipoCambio = Convert.ToDecimal(mCommand.ExecuteScalar());



                    string mFactura = mRespuesta.facturaElectronica;
                    if (mfFactura != null)
                    {
                        decimal mValor = 0m;
                        decimal mIva = 0m;
                        decimal mTotal = 0m;
                        foreach (var item in mfFactura.detalleFactura)
                        {
                            mValor += item.valor;
                            mIva += item.iva;
                            mTotal += item.total;
                        }




                        decimal mTotalDolar = Math.Round((mTotal / mTipoCambio), 2, MidpointRounding.AwayFromZero);

                        mCommand.Parameters.Add("@Cliente", SqlDbType.VarChar).Value = "";
                        mCommand.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = "";
                        mCommand.Parameters.Add("@DireccionDevuelveGFace", SqlDbType.VarChar).Value = "";
                        mCommand.Parameters.Add("@NombreDevuelveGFace", SqlDbType.VarChar).Value = "";
                        mCommand.Parameters.Add("@DireccionCompleta", SqlDbType.VarChar).Value = "";
                        mCommand.Parameters.Add("@error", SqlDbType.VarChar).Value = "";
                        mCommand.Parameters.Add("@XMLDocumento", SqlDbType.VarChar).Value = "";
                        mCommand.Parameters.Add("@XMLReversion", SqlDbType.VarChar).Value = "";
                        mCommand.Parameters.Add("@XMLRespuesta", SqlDbType.VarChar).Value = "";


                        mCommand.Parameters.Add("@Departamento", SqlDbType.VarChar).Value = "GUATEMALA";
                        mCommand.Parameters.Add("@Municipio", SqlDbType.VarChar).Value = "GUATEMALA";
                        mCommand.Parameters.Add("@Nit", SqlDbType.VarChar).Value = "544099-8";



                        mCommand.Parameters.Add("@Hoy", SqlDbType.DateTime).Value = DateTime.Now.Date;
                        mCommand.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = DateTime.Now.Date;
                        mCommand.Parameters.Add("@FechaUltima", SqlDbType.DateTime).Value = new DateTime(1980, 1, 1);



                        mCommand.Parameters["@Cliente"].Value = Convert.ToString(mfFactura.cliente);
                        mCommand.Parameters["@Nombre"].Value = Convert.ToString(mfFactura.nombrecliente);
                        mCommand.Parameters["@Fecha"].Value = DateTime.Now;
                        mCommand.Parameters["@DireccionDevuelveGFace"].Value = mRespuesta.direccion;
                        mCommand.Parameters["@NombreDevuelveGFace"].Value = mRespuesta.nombre;
                        
                        mCommand.Parameters["@error"].Value = mRespuesta.error;
                        mCommand.Parameters["@XMLDocumento"].Value = mXML;
                        mCommand.Parameters["@XMLReversion"].Value = mXMLReversion;
                        mCommand.Parameters["@XMLRespuesta"].Value = mRespuesta.xml;

                        /*Parametros de creación de pedido*/


                        mCommand.CommandText = "SELECT VALOR_CONSECUTIVO FROM crediplus.CONSECUTIVO_FA WHERE CODIGO_CONSECUTIVO = '02'";
                        Int64 mNumeroPedido = Convert.ToInt64(mCommand.ExecuteScalar().ToString().Replace("P", ""));


                        mCommand.CommandText = "SELECT E_MAIL FROM crediplus.COBRADOR WHERE COBRADOR = 'ND'";
                        string[] mConsecutivos = mCommand.ExecuteScalar().ToString().Split(new string[] { "," }, StringSplitOptions.None);

                        string mConsecutivo = mConsecutivos[0];


                        mCommand.CommandText = $"SELECT VALOR_MAXIMO FROM crediplus.CONSECUTIVO_FA WHERE CODIGO_CONSECUTIVO = '{mConsecutivo}'";
                        string mUltimoValor = Convert.ToString(mCommand.ExecuteScalar());

                        mCommand.CommandText = $"SELECT VALOR_CONSECUTIVO FROM crediplus.CONSECUTIVO_FA WHERE CODIGO_CONSECUTIVO = '{mConsecutivo}'";
                        string mValorActual = Convert.ToString(mCommand.ExecuteScalar());

                        string[] mConsecutivoFA = mValorActual.Split(new string[] { "-" }, StringSplitOptions.None);
                        Int32 mNumeroFactura = Convert.ToInt32(mConsecutivoFA[1]);

                        string[] mConsecutivoUltimoFA = mUltimoValor.Split(new string[] { "-" }, StringSplitOptions.None);
                        Int32 mNumeroFacturaUltimo = Convert.ToInt32(mConsecutivoUltimoFA[1]);


                        /**/
                        mNumeroPedido = mNumeroPedido + 1;
                        string mPedido = Convert.ToString(mNumeroPedido);
                        mPedido = "P" + mPedido.PadLeft(7, '0');



                        mCommand.CommandText = "INSERT INTO crediplus.PEDIDO ( PEDIDO, ESTADO, FECHA_PEDIDO, FECHA_PROMETIDA, FECHA_PROX_EMBARQU, FECHA_ULT_EMBARQUE, FECHA_ULT_CANCELAC, FECHA_ORDEN, TARJETA_CREDITO, EMBARCAR_A, DIREC_EMBARQUE, " +
                            "DIRECCION_FACTURA, OBSERVACIONES, TOTAL_MERCADERIA, MONTO_ANTICIPO, MONTO_FLETE, MONTO_SEGURO, MONTO_DOCUMENTACIO, TIPO_DESCUENTO1, TIPO_DESCUENTO2, MONTO_DESCUENTO1, MONTO_DESCUENTO2, PORC_DESCUENTO1, " +
                            "PORC_DESCUENTO2, TOTAL_IMPUESTO1, TOTAL_IMPUESTO2, TOTAL_A_FACTURAR, PORC_COMI_VENDEDOR, PORC_COMI_COBRADOR, TOTAL_CANCELADO, TOTAL_UNIDADES, IMPRESO, FECHA_HORA, DESCUENTO_VOLUMEN, TIPO_PEDIDO, MONEDA_PEDIDO, " +
                            "VERSION_NP, AUTORIZADO, DOC_A_GENERAR, CLASE_PEDIDO, MONEDA, NIVEL_PRECIO, COBRADOR, RUTA, USUARIO, CONDICION_PAGO, BODEGA, ZONA, VENDEDOR, CLIENTE, CLIENTE_DIRECCION, CLIENTE_CORPORAC, CLIENTE_ORIGEN, " +
                            "PAIS, SUBTIPO_DOC_CXC, TIPO_DOC_CXC, BACKORDER, PORC_INTCTE, DESCUENTO_CASCADA, FIJAR_TIPO_CAMBIO, ORIGEN_PEDIDO, DESC_DIREC_EMBARQUE, BASE_IMPUESTO1, BASE_IMPUESTO2, NOMBRE_CLIENTE, FECHA_PROYECTADA, " +
                            "TIPO_DOCUMENTO, TASA_IMPOSITIVA_PORC, TASA_CREE1_PORC, TASA_CREE2_PORC, TASA_GAN_OCASIONAL_PORC, CONTRATO_REVENTA ) " +
                            "VALUES ( '" + mPedido + "', 'F', @Fecha, @Fecha, @Fecha, @Fecha, @Fecha, @Fecha, '', @Cliente, 1, " +
                            "@DireccionCompleta, '', " + mValor + ", 0, 0, 0, 0, 'P', 'P', 0, 0, 0, " +
                            "0, " + mIva + ", 0, " + mTotal + ", 0, 0, 0, 0, 'N', GETDATE(), 0, 'N', 'L', " +
                            "1, 'N', 'F', 'N', 'L', 'CONTADOEFECT', 'ND', 'ND', 'sa', 0, 'CP01', '0101', 'ND', @Cliente, @Cliente, @Cliente, @Cliente, " +
                            "'GUA', 0, 'FAC', 'C', 0 , 'S', 'N', 'F', @DireccionCompleta, " + mValor + ", 0, @Nombre, @Hoy, " +
                            "'P', 0, 0, 0, 0, 'N' ) ";
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = $"UPDATE crediplus.CONSECUTIVO_FA SET VALOR_CONSECUTIVO = '{mPedido}' WHERE CODIGO_CONSECUTIVO = '02'";
                        mCommand.ExecuteNonQuery();

                        mNumeroFactura++;
                        int mNumeroFacturaLength = Convert.ToString(mNumeroFactura).Length;
                        int mConsecutivoFALength = mConsecutivoFA[1].Length;

                        string mCeros = "";
                        for (int ii = Convert.ToString(mNumeroFactura).Length; ii < mConsecutivoFA[1].Length; ii++)
                        {
                            mCeros = string.Format("0{0}", mCeros);
                        }

                        //mFactura = string.Format("{0}-{1}{2}", mConsecutivoFA[0], mCeros, Convert.ToString(mNumeroFactura));

                        mCommand.CommandText = "INSERT INTO crediplus.AUDIT_TRANS_INV ( USUARIO, FECHA_HORA, MODULO_ORIGEN, APLICACION, REFERENCIA ) " +
                            "VALUES ( 'sa', GETDATE(), 'FA', 'FAC#" + mFactura + "', 'Cliente: " + mfFactura.cliente + "' ) ";
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = "SELECT @@IDENTITY";
                        Int64 mAudit = Convert.ToInt64(mCommand.ExecuteScalar());


                        log.Info("insertando en crediplus.cp_factura");

                        mCommand.CommandText = "INSERT INTO crediplus.CP_Factura ( FACTURA, FACTURA_ELECTRONICA, CLIENTE, FECHA, FIRMADA, FIRMA, SERIE, NUMERO, " +
                            "IDENTIFICADOR, MAQUINA, NOMBRE, DIRECCION, TELEFONO, ERROR, XML_DOCUMENTO, XML_REVERSION, XML_RESPUESTA, " +
                            "FECHA_REGISTRO, ID_CREDIPLUS, CLIENTE_CREDIPLUS, CREDITO_CREDIPLUS, NOMBRE_CREDIPLUS, VALOR_CREDIPLUS, IVA_CREDIPLUS, TOTAL_CREDIPLUS, FECHA_REGISTRO_CREDIPLUS ) " +
                            "VALUES ( '" + mFactura + "', '" + mRespuesta.facturaElectronica + "', @Cliente, @Fecha, '" + mRespuesta.firmada + "', '" + mRespuesta.firma + "', '" + mRespuesta.serie + "', '" + mRespuesta.numero + "', " +
                            "'" + mRespuesta.identificador + "', '" + mRespuesta.maquina + "', @NombreDevuelveGFace, @DireccionDevuelveGFace, '" + mRespuesta.telefono + "', @error, @XMLDocumento, @XMLReversion, @XMLRespuesta, " +
                            "GETDATE(), 0, @Cliente, '" + mfFactura.cliente + "', @Nombre, " + mValor.ToString() + ", " + mIva.ToString() + ", " + mTotal.ToString() + ", @Fecha ) ";
                        mCommand.ExecuteNonQuery();
                        

                        log.Info("insertando en crediplus.crediplus.DOCUMENTOS_CC");

                        mCommand.CommandText = "INSERT INTO crediplus.DOCUMENTOS_CC ( DOCUMENTO, TIPO, APLICACION, FECHA_DOCUMENTO, FECHA, MONTO, SALDO, MONTO_LOCAL, SALDO_LOCAL, MONTO_DOLAR, " +
                            "SALDO_DOLAR, MONTO_CLIENTE, SALDO_CLIENTE, TIPO_CAMBIO_MONEDA, TIPO_CAMBIO_DOLAR, TIPO_CAMBIO_CLIENT, TIPO_CAMB_ACT_LOC, TIPO_CAMB_ACT_DOL, TIPO_CAMB_ACT_CLI, SUBTOTAL, DESCUENTO, IMPUESTO1, IMPUESTO2, RUBRO1, RUBRO2, " +
                            "MONTO_RETENCION, SALDO_RETENCION, DEPENDIENTE, FECHA_ULT_CREDITO, CARGADO_DE_FACT, APROBADO, ASIENTO_PENDIENTE, FECHA_ULT_MOD, NOTAS, CLASE_DOCUMENTO, FECHA_VENCE, NUM_PARCIALIDADES, COBRADOR, USUARIO_ULT_MOD, CONDICION_PAGO, " +
                            "MONEDA, VENDEDOR, CLIENTE_REPORTE, CLIENTE_ORIGEN, CLIENTE, SUBTIPO, PORC_INTCTE, FECHA_ANUL, AUD_USUARIO_ANUL, AUD_FECHA_ANUL, USUARIO_APROBACION, FECHA_APROBACION, ANULADO, PAIS, BASE_IMPUESTO1, BASE_IMPUESTO2, " +
                            "DEPENDIENTE_GP, SALDO_TRANS, SALDO_TRANS_LOCAL, SALDO_TRANS_DOLAR, SALDO_TRANS_CLI, FACTURADO, GENERA_DOC_FE ) " +
                            "VALUES ( '" + mFactura + "', 'FAC', 'Cargado de factura: " + mFactura + "', @Fecha, @Fecha, " + mTotal.ToString() + ", " + mTotal.ToString() + ", " + mTotal.ToString() + ", " + mTotal.ToString() + ", " + mTotalDolar + ", " +
                            mTotalDolar + ", " + mTotal + ", " + mTotal + ", 1, " + mTipoCambio.ToString() + ", 1, 1, " + mTipoCambio.ToString() + ", 1, " + mValor.ToString() + ", 0, " + mIva.ToString() + ", 0, 0, 0, " +
                            "0, 0, 'S', '1980-01-01', 'S', 'S', 'S', GETDATE(), '', 'N', @Fecha, 0, 'ND', 'sa', 0, " +
                            "'GTQ', 'ND', @Cliente, @Cliente, @Cliente, 0, 0, NULL, NULL, NULL, 'sa', @Fecha, 'N', 'GUA', " + mValor.ToString() + ", 0, " +
                            "'N', 0, 0, 0, 0, 'N', 'N' ) ";
                        mCommand.ExecuteNonQuery();

                        log.Info("insertando en crediplus.crediplus.FACTURA");

                        //
                        mCommand.CommandText = "INSERT INTO crediplus.FACTURA ( TIPO_DOCUMENTO, FACTURA, AUDIT_TRANS_INV, ESTA_DESPACHADO, EN_INVESTIGACION, TRANS_ADICIONALES, ESTADO_REMISION, DESCUENTO_VOLUMEN, MONEDA_FACTURA, FECHA_DESPACHO, " +
                           "CLASE_DOCUMENTO, FECHA_RECIBIDO, PEDIDO, COMISION_COBRADOR, TARJETA_CREDITO, TOTAL_VOLUMEN, TOTAL_PESO, MONTO_COBRADO, TOTAL_IMPUESTO1, FECHA, FECHA_ENTREGA, TOTAL_IMPUESTO2, PORC_DESCUENTO2, MONTO_FLETE, MONTO_SEGURO, " +
                           "MONTO_DOCUMENTACIO, TIPO_DESCUENTO1, TIPO_DESCUENTO2, MONTO_DESCUENTO1, MONTO_DESCUENTO2, PORC_DESCUENTO1, TOTAL_FACTURA, FECHA_PEDIDO, FECHA_HORA_ANULA, FECHA_ORDEN, TOTAL_MERCADERIA, COMISION_VENDEDOR, FECHA_HORA, " +
                           "TOTAL_UNIDADES, NUMERO_PAGINAS, TIPO_CAMBIO, ANULADA, MODULO, CARGADO_CG, CARGADO_CXC, EMBARCAR_A, DIREC_EMBARQUE, DIRECCION_FACTURA, MULTIPLICADOR_EV, OBSERVACIONES, VERSION_NP, MONEDA, NIVEL_PRECIO, COBRADOR, RUTA, " +
                           "USUARIO, USUARIO_ANULA, CONDICION_PAGO, ZONA, VENDEDOR, CLIENTE_DIRECCION, CLIENTE_CORPORAC, CLIENTE_ORIGEN, CLIENTE, PAIS, SUBTIPO_DOC_CXC, TIPO_DOC_CXC, MONTO_ANTICIPO, TOTAL_PESO_NETO, FECHA_RIGE, PORC_INTCTE, " +
                           "USA_DESPACHOS, COBRADA, DESCUENTO_CASCADA, DIRECCION_EMBARQUE, CONSECUTIVO, REIMPRESO, BASE_IMPUESTO1, BASE_IMPUESTO2, NOMBRE_CLIENTE, NOMBREMAQUINA, SERIE_RESOLUCION, CONSEC_RESOLUCION, GENERA_DOC_FE, " +
                           "TASA_IMPOSITIVA_PORC, TASA_CREE1_PORC, TASA_CREE2_PORC, TASA_GAN_OCASIONAL_PORC, COMENTARIO_CXC,RUBRO3,RUBRO5,doc_credito_cxc,tipo_credito_cxC ) " +
                           "VALUES ( 'F', '" + mFactura + "', " + mAudit + ", 'N', 'N', 'N', 'N', 0, 'L', @Fecha, " +
                           "'N', @Fecha, '" + mPedido + "', 0, '', 1, 1, 0, " + mIva.ToString() + ", @Fecha, @Fecha, 0, 0, 0, 0, " +
                           "0, 'P', 'P', 0, 0, 0, " + mTotal.ToString() + ", @Fecha, NULL, @Fecha, " + mValor.ToString() + ", 0, GETDATE(), " +
                           "1, 1, " + mTipoCambio.ToString() + ", 'N', 'FA', 'N', 'S', @Cliente, 'ND', @DireccionCompleta, 1, '', 1, 'L', 'CONTADOEFECT', 'ND', 'ND', " +
                           "'sa', NULL, 0, 001010, 'ND', @Cliente, @Cliente, @Cliente, @Cliente, 'GUA', 0, 'FAC', 0, 1, @Fecha, 0, " +
                           "'S', 'S', 'S','ND', '" + mConsecutivo + "', 0, " + mValor.ToString() + ", 0, @Nombre, NULL, NULL,NULL, 'N', " +
                           "0, 0, 0, 0, 'FACTURA A MF','" + FacturaFiesta + "','','" + mFactura + "','FAC' ) ";
                        mCommand.ExecuteNonQuery();

                        var mLinea = 1;
                        log.Info("insertando en detalle de pedido y factura");
                        foreach (var itemD in mfFactura.detalleFacturaCosto)
                        {
                            decimal mValorProducto = itemD.valor;
                            decimal mIvaProducto = itemD.iva;

                            mCommand.CommandText = $"SELECT costo_prom_loc FROM  crediplus.articulo  WHERE articulo = '{itemD.producto}'";
                            decimal CostoCP = Convert.ToDecimal(mCommand.ExecuteScalar());

                            mCommand.CommandText = $"SELECT costo_prom_dol FROM  crediplus.articulo  WHERE articulo = '{itemD.producto}'";
                            decimal CostoCPUSD = Convert.ToDecimal(mCommand.ExecuteScalar());


                            mCommand.CommandText = "INSERT INTO crediplus.PEDIDO_LINEA ( PEDIDO, PEDIDO_LINEA, BODEGA, ARTICULO, ESTADO, FECHA_ENTREGA, LINEA_USUARIO, PRECIO_UNITARIO, CANTIDAD_PEDIDA, CANTIDAD_A_FACTURA, " +
                                    "CANTIDAD_FACTURADA, CANTIDAD_RESERVADA, CANTIDAD_BONIFICAD, CANTIDAD_CANCELADA, TIPO_DESCUENTO, MONTO_DESCUENTO, PORC_DESCUENTO, DESCRIPCION, COMENTARIO, FECHA_PROMETIDA ) " +
                                    "VALUES ( '" + mPedido + "', " + mLinea.ToString() + ", 'CP01'," + "'" + itemD.producto + "', 'F', @Fecha, " + (mLinea - 1).ToString() + ", " + mValorProducto.ToString() + ", 1, 0, " +
                                    "1, 0, 0, 0, 'P', 0, 0, '" + itemD.descripcion + "', '', @Fecha )";
                            mCommand.ExecuteNonQuery();

                            mCommand.CommandText = "INSERT INTO crediplus.FACTURA_LINEA ( FACTURA, TIPO_DOCUMENTO, LINEA, BODEGA, COSTO_TOTAL_DOLAR, PEDIDO, ARTICULO, ANULADA, FECHA_FACTURA, CANTIDAD, PRECIO_UNITARIO, TOTAL_IMPUESTO1, TOTAL_IMPUESTO2, " +
                                "DESC_TOT_LINEA, DESC_TOT_GENERAL, COSTO_TOTAL, PRECIO_TOTAL, DESCRIPCION, COMENTARIO, CANTIDAD_DEVUELT, DESCUENTO_VOLUMEN, TIPO_LINEA, CANTIDAD_ACEPTADA, CANT_NO_ENTREGADA, COSTO_TOTAL_LOCAL, PEDIDO_LINEA, " +
                                "MULTIPLICADOR_EV, CANT_DESPACHADA, COSTO_ESTIM_LOCAL, COSTO_ESTIM_DOLAR, CANT_ANUL_PORDESPA, MONTO_RETENCION, BASE_IMPUESTO1, BASE_IMPUESTO2, COSTO_TOTAL_COMP, COSTO_TOTAL_COMP_LOCAL, " +
                                "COSTO_TOTAL_COMP_DOLAR, COSTO_ESTIM_COMP_LOCAL, COSTO_ESTIM_COMP_DOLAR, CANT_DEV_PROCESO ) " +
                                "VALUES ( '" + mFactura + "', 'F', " + (mLinea - 1).ToString() + ", 'CP01', " + CostoCPUSD + ", '" + mPedido + "', '" + itemD.producto + "', 'N', @Fecha, 1, " + mValorProducto.ToString() + ", " + mIvaProducto.ToString() + ", 0, " +
                                "0, 0," + CostoCP + ", " + mValorProducto.ToString() + ", '" + itemD.descripcion + "', '', 0, 0, 'N', 0, 0, " + CostoCP + ", " + mLinea.ToString() + ", " +
                                "1, 0, " + CostoCP + ", " + CostoCPUSD + ", 0, 0, " + mValorProducto.ToString() + ", 0, 0, 0, " +
                                "0, " + CostoCP + ", " + CostoCPUSD + ", 0 ) ";
                            mCommand.ExecuteNonQuery();


                            decimal nCostoDolar = Math.Round((itemD.costo / mTipoCambio), 2, MidpointRounding.AwayFromZero);

                            //actualizando en crediplus
                            mCommand.CommandText = string.Format("update crediplus.articulo set precio_base_local={0},precio_base_dolar={1},costo_ult_loc={0},costo_ult_dol={1} where articulo='{2}'", itemD.costo, nCostoDolar, itemD.producto);
                            mCommand.ExecuteNonQuery();

                            //actualizando costos en muebles fiesta


                            mCommand.CommandText = string.Format("update prodmult.articulo set costo_prom_loc={0},costo_prom_dol={1},costo_ult_loc={0},costo_ult_dol={1} where articulo='{2}'", itemD.costo, nCostoDolar, itemD.productoMF);
                            mCommand.ExecuteNonQuery();

                            mCommand.CommandText = string.Format("update prodmult.factura_linea set costo_total={0},costo_total_local={0},costo_estim_local={0},costo_estim_dolar={1} where factura='{2}' and ARTICULO='{3}'", itemD.costo, nCostoDolar, FacturaFiesta, itemD.productoMF);
                            mCommand.ExecuteNonQuery();
                            mLinea = mLinea + 1;
                        }
                    }
                    mTransaction.Commit();
                    mConexion.Close();

                    mresp.Exito = true;
                    mresp.Objeto = mRespuesta;
                }
                else
                {
                    mresp.Exito = false;
                    mresp.Mensaje = msgError;
                    log.Error("No se firmo la factura de CREDIPLUS PARA MF" + mRespuesta.error + " -- " + mRespuesta.xml);
                }


            }
            catch (Exception ex)
            {
                if (mConexion.State == ConnectionState.Open)
                {
                    mTransaction.Rollback();
                    mConexion.Close();
                }
                log.Error("Error al facturar en CREDIPLUS factura para Muebles Fiesta " + ex.Message + "|" + ex.InnerException);
                mresp.Mensaje = "Error al facturar en CREDIPLUS factura para Muebles Fiesta " + ex.Message + "|" + ex.InnerException;
            }
            return mresp;
        }

        /// <summary>
        /// Facturar Artículos directamente desde crediplus
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public Respuesta FacturarArticulos(FacturacionArticulos datos)
        {
            //MF.DAL.Exactus.Crediplus.CrediplusContext cre = new MF.DAL.Exactus.Crediplus.CrediplusContext();
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            Respuesta mresp = new Respuesta();
            EntityConnection ec = (EntityConnection)db.Connection;
            SqlConnection sc = (SqlConnection)ec.StoreConnection;
            SqlConnection mConexion = new SqlConnection(sc.ConnectionString);
            SqlCommand mCommand = new SqlCommand();
            SqlTransaction mTransaction = null;

           
            try
            {
                List<DetalleFacturaAtid> detalleFacturas = new List<DetalleFacturaAtid>();
                datos.Articulos.ForEach(x =>
                    detalleFacturas.Add(new Clases.DetalleFacturaAtid
                    {
                        linea = x.Linea,
                        descripcion = x.Descripcion,
                        iva = x.iva,
                        valor = x.TotalProductoNeto,
                        total = x.TotalProductoBruto,
                         producto=x.Producto

                    }
                     )
                    );
                Clases.FacturasAtid factura = new Clases.FacturasAtid
                {
                    cliente = datos.cliente,
                    fechaOperacion =DateTime.Today,
                    fechaRegistro=datos.FechaFactura,
                    nit = datos.NitCliente,
                    nombrecliente = datos.NombreCliente,
                    eMail = datos.Email,
                    direccion=datos.DirCliente,
                    credito=DateTime.Now.Second + "-"+datos.cliente+"-"+DateTime.Now.Minute.ToString()
                    ,
                    detalleFactura = detalleFacturas
                };

                string mXML = xmlCrediplus(factura, "N", MF_Clases.Utils.CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES);
                string mXMLReversion = xmlCrediplus(factura, "S", MF_Clases.Utils.CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES);

                Clases.RespuestaGuateFacturas mRespuesta = new RespuestaGuateFacturas();

                log.Info("INTENTO (1) DE FIRMA ELECTRÓNICA DE FACTURA DE ARTÍCULO DE CREDIPLUS ");
                mRespuesta = firmarFactura(mXML, MF_Clases.Utils.CONSTANTES.GUATEFAC.MAQUINA_CREDIPLUS.FACTURACION_DEFAULT);
                if (mRespuesta.firmada == "N")
                {
                    log.Info(mRespuesta.error);
                    log.Info("INTENTO (2) DE FIRMA ELECTRÓNICA DE FACTURA DE  ARTÍCULO DE  CREDIPLUS A MUEBLES FIESTA");
                    mRespuesta = firmarFactura(mXML, MF_Clases.Utils.CONSTANTES.GUATEFAC.MAQUINA_CREDIPLUS.FACTURACION_DEFAULT);
                }
                if (mRespuesta.firmada == "N")
                {
                    log.Info(mRespuesta.error);
                    log.Info("INTENTO (3) DE FIRMA ELECTRÓNICA DE FACTURA DE  ARTÍCULO DE CREDIPLUS A MUEBLES FIESTA");
                    mRespuesta = firmarFactura(mXML, MF_Clases.Utils.CONSTANTES.GUATEFAC.MAQUINA_CREDIPLUS.FACTURACION_DEFAULT);
                }

                if (mRespuesta.firmada == "S")
                {
                    log.Debug("Comenzando la transacción para guardar la factura de crediplus " + mRespuesta.facturaElectronica);
                    mConexion.Open();
                    mTransaction = mConexion.BeginTransaction();

                    mCommand.Connection = mConexion;
                    mCommand.Transaction = mTransaction;



                    decimal mTipoCambio = db.TIPO_CAMBIO_HIST.Where(x => x.TIPO_CAMBIO == "REFR").Take(1).OrderByDescending(x => x.FECHA).Select(x => x.MONTO).FirstOrDefault();



                    mCommand.CommandText = "SELECT VALOR_CONSECUTIVO FROM crediplus.CONSECUTIVO_FA WHERE CODIGO_CONSECUTIVO = '02'";
                    long mNumeroPedido = Convert.ToInt64(mCommand.ExecuteScalar().ToString().Replace("P", ""));


                    mCommand.CommandText = "SELECT E_MAIL FROM crediplus.COBRADOR WHERE COBRADOR = 'ND'";
                    string[] mConsecutivos = mCommand.ExecuteScalar().ToString().Split(new string[] { "," }, StringSplitOptions.None);

                    string mConsecutivo = mConsecutivos[0];


                    mCommand.CommandText = $"SELECT VALOR_MAXIMO FROM crediplus.CONSECUTIVO_FA WHERE CODIGO_CONSECUTIVO = '{mConsecutivo}'";
                    string mUltimoValor = Convert.ToString(mCommand.ExecuteScalar());

                    mCommand.CommandText = $"SELECT VALOR_CONSECUTIVO FROM crediplus.CONSECUTIVO_FA WHERE CODIGO_CONSECUTIVO = '{mConsecutivo}'";
                    string mValorActual = Convert.ToString(mCommand.ExecuteScalar());

                    string[] mConsecutivoFA = mValorActual.Split(new string[] { "-" }, StringSplitOptions.None);
                    Int32 mNumeroFactura = Convert.ToInt32(mConsecutivoFA[1]);

                    string[] mConsecutivoUltimoFA = mUltimoValor.Split(new string[] { "-" }, StringSplitOptions.None);
                    Int32 mNumeroFacturaUltimo = Convert.ToInt32(mConsecutivoUltimoFA[1]);

                    

                        #region "comentado"
                        //decimal mTotalDolar = Math.Round((datos.TotalBruto / mTipoCambio), 2, MidpointRounding.AwayFromZero);

                        //    //mCommand.CommandText = "SELECT VALOR_CONSECUTIVO FROM crediplus.CONSECUTIVO_FA WHERE CODIGO_CONSECUTIVO = '02'";


                        //mNumeroPedido = mNumeroPedido + 1;
                        //string mPedido = Convert.ToString(mNumeroPedido);
                        //mPedido = "P" + mPedido.PadLeft(7, '0');

                        //    DateTime FechaAplicacion = datos.FechaFactura.Date;
                        //    DateTime FechaOperacion = DateTime.Today.Date;

                        //    MF.DAL.Exactus.Crediplus.PEDIDO vPedido = new MF.DAL.Exactus.Crediplus.PEDIDO
                        //    {
                        //        PEDIDO1 = mPedido,
                        //        ESTADO = "F",
                        //        FECHA_PEDIDO = FechaAplicacion,
                        //        FECHA_PROMETIDA = FechaAplicacion,
                        //        FECHA_PROX_EMBARQU = FechaAplicacion,
                        //        FECHA_ULT_EMBARQUE = FechaAplicacion,
                        //        FECHA_ULT_CANCELAC = FechaAplicacion,
                        //        FECHA_ORDEN = FechaAplicacion,
                        //        TARJETA_CREDITO = "",
                        //        EMBARCAR_A = datos.cliente,
                        //        DIREC_EMBARQUE = "ND",
                        //        DIRECCION_FACTURA = datos.DirCliente,
                        //        OBSERVACIONES = "",
                        //        TOTAL_MERCADERIA = datos.TotalNeto,
                        //        MONTO_ANTICIPO = 0,
                        //        MONTO_FLETE = 0,
                        //        MONTO_SEGURO = 0,
                        //        MONTO_DOCUMENTACIO = 0,
                        //        TIPO_DESCUENTO1 = "P",
                        //        TIPO_DESCUENTO2 = "P",
                        //        MONTO_DESCUENTO1 = 0,
                        //        MONTO_DESCUENTO2 = 0,
                        //        PORC_DESCUENTO1 = 0,
                        //        PORC_DESCUENTO2 = 0,
                        //        TOTAL_IMPUESTO1 = datos.TotalImpuesto,
                        //        TOTAL_IMPUESTO2 = 0,
                        //        TOTAL_A_FACTURAR = datos.TotalBruto,
                        //        PORC_COMI_VENDEDOR = 0,
                        //        PORC_COMI_COBRADOR = 0,
                        //        TOTAL_CANCELADO = 0,
                        //        TOTAL_UNIDADES = 0,
                        //        IMPRESO = "N",
                        //        FECHA_HORA = DateTime.Now,
                        //        DESCUENTO_VOLUMEN = 0,
                        //        TIPO_PEDIDO = "N",
                        //        MONEDA_PEDIDO = "L",
                        //        VERSION_NP = 1,
                        //        AUTORIZADO = "N",
                        //        DOC_A_GENERAR = "F",
                        //        CLASE_PEDIDO = "N",
                        //        MONEDA = "L",
                        //        NIVEL_PRECIO = "CONTADOEFECT",
                        //        COBRADOR = "ND",
                        //        RUTA = "ND",
                        //        USUARIO = "sa",
                        //        CONDICION_PAGO = "0",
                        //        BODEGA = "CP01",
                        //        ZONA = "0101",
                        //        VENDEDOR = "ND",
                        //        CLIENTE = datos.cliente,
                        //        CLIENTE_DIRECCION = datos.cliente,
                        //        CLIENTE_CORPORAC = datos.cliente,
                        //        CLIENTE_ORIGEN = datos.cliente,
                        //        PAIS = "GUA",
                        //        SUBTIPO_DOC_CXC = 0,
                        //        TIPO_DOC_CXC = "FAC",
                        //        BACKORDER = "C",
                        //        PORC_INTCTE = 0,
                        //        DESCUENTO_CASCADA = "S",
                        //        FIJAR_TIPO_CAMBIO = "N",
                        //        ORIGEN_PEDIDO = "F",
                        //        DESC_DIREC_EMBARQUE = datos.DirCliente,
                        //        BASE_IMPUESTO1 = datos.TotalNeto,
                        //        BASE_IMPUESTO2 = 0,
                        //        NOMBRE_CLIENTE = datos.NombreCliente,
                        //        FECHA_PROYECTADA = FechaOperacion,

                        //        TIPO_DOCUMENTO = "P",
                        //        TASA_IMPOSITIVA_PORC = 0,
                        //        TASA_CREE1_PORC = 0,
                        //        TASA_CREE2_PORC = 0,
                        //        TASA_GAN_OCASIONAL_PORC = 0,
                        //        CONTRATO_REVENTA = "N"
                        //        ,CreatedBy="mf.fiestanet",
                        //        UpdatedBy="mf.fiestanet"

                        //    };
                        //    cre.PEDIDO.Add(vPedido);

                        //    foreach (var p in qPedido)
                        //    {
                        //        p.VALOR_CONSECUTIVO = mPedido;
                        //    }
                        //    //qPedido.ForEach(x => x.VALOR_CONSECUTIVO = mPedido);

                        //    //MF.DAL.Exactus.Crediplus.AUDIT_TRANS_INV audit = new MF.DAL.Exactus.Crediplus.AUDIT_TRANS_INV
                        //    //{
                        //    //    USUARIO = "sa",
                        //    //    FECHA_HORA = DateTime.Now,
                        //    //    MODULO_ORIGEN = "FA",
                        //    //    APLICACION = "FAC#" + mRespuesta.facturaElectronica,
                        //    //    REFERENCIA = datos.cliente
                        //    //    ,
                        //    //    CreatedBy = "mf.fiestanet",
                        //    //    UpdatedBy = "mf.fiestanet"
                        //    //};

                        //    //cre.AUDIT_TRANS_INV.Add(audit);
                        //    //var auditId = audit.AUDIT_TRANS_INV1;


                        //    //mCommand.CommandText = "INSERT INTO crediplus.AUDIT_TRANS_INV ( USUARIO, FECHA_HORA, MODULO_ORIGEN, APLICACION, REFERENCIA ) " +
                        //    //   "VALUES ( 'sa', GETDATE(), 'FA', 'FAC#" + mFactura + "', 'Cliente: " + mfFactura.cliente + "' ) ";
                        //    //mCommand.ExecuteNonQuery();
                        //    //mCommand.CommandText = "INSERT INTO crediplus.PEDIDO ( PEDIDO, ESTADO, FECHA_PEDIDO, FECHA_PROMETIDA, FECHA_PROX_EMBARQU, FECHA_ULT_EMBARQUE, FECHA_ULT_CANCELAC, FECHA_ORDEN, TARJETA_CREDITO, EMBARCAR_A, DIREC_EMBARQUE, " +
                        //    //    "DIRECCION_FACTURA, OBSERVACIONES, TOTAL_MERCADERIA, MONTO_ANTICIPO, MONTO_FLETE, MONTO_SEGURO, MONTO_DOCUMENTACIO, TIPO_DESCUENTO1, TIPO_DESCUENTO2, MONTO_DESCUENTO1, MONTO_DESCUENTO2, PORC_DESCUENTO1, " +
                        //    //    "PORC_DESCUENTO2, TOTAL_IMPUESTO1, TOTAL_IMPUESTO2, TOTAL_A_FACTURAR, PORC_COMI_VENDEDOR, PORC_COMI_COBRADOR, TOTAL_CANCELADO, TOTAL_UNIDADES, IMPRESO, FECHA_HORA, DESCUENTO_VOLUMEN, TIPO_PEDIDO, MONEDA_PEDIDO, " +
                        //    //    "VERSION_NP, AUTORIZADO, DOC_A_GENERAR, CLASE_PEDIDO, MONEDA, NIVEL_PRECIO, COBRADOR, RUTA, USUARIO, CONDICION_PAGO, BODEGA, ZONA, VENDEDOR, CLIENTE, CLIENTE_DIRECCION, CLIENTE_CORPORAC, CLIENTE_ORIGEN, " +
                        //    //    "PAIS, SUBTIPO_DOC_CXC, TIPO_DOC_CXC, BACKORDER, PORC_INTCTE, DESCUENTO_CASCADA, FIJAR_TIPO_CAMBIO, ORIGEN_PEDIDO, DESC_DIREC_EMBARQUE, BASE_IMPUESTO1, BASE_IMPUESTO2, NOMBRE_CLIENTE, FECHA_PROYECTADA, " +
                        //    //    "TIPO_DOCUMENTO, TASA_IMPOSITIVA_PORC, TASA_CREE1_PORC, TASA_CREE2_PORC, TASA_GAN_OCASIONAL_PORC, CONTRATO_REVENTA ) " +
                        //    //    "VALUES ( '" + mPedido + "', 'F', @Fecha, @Fecha, @Fecha, @Fecha, @Fecha, @Fecha, '', @Cliente, 1, " +
                        //    //    "@DireccionCompleta, '', " + mValor + ", 0, 0, 0, 0, 'P', 'P', 0, 0, 0, " +
                        //    //    "0, " + mIva + ", 0, " + mTotal + ", 0, 0, 0, 0, 'N', GETDATE(), 0, 'N', 'L', " +
                        //    //    "1, 'N', 'F', 'N', 'L', 'CONTADOEFECT', 'ND', 'ND', 'sa', 0, 'CP01', '0101', 'ND', @Cliente, @Cliente, @Cliente, @Cliente, " +
                        //    //    "'GUA', 0, 'FAC', 'C', 0 , 'S', 'N', 'F', @DireccionCompleta, " + mValor + ", 0, @Nombre, @Hoy, " +
                        //    //    "'P', 0, 0, 0, 0, 'N' ) ";
                        //    //mCommand.ExecuteNonQuery();

                        //    //mCommand.CommandText = $"UPDATE crediplus.CONSECUTIVO_FA SET VALOR_CONSECUTIVO = '{mPedido}' WHERE CODIGO_CONSECUTIVO = '02'";
                        //    //mCommand.ExecuteNonQuery();





                        //    //mCommand.CommandText = "SELECT @@IDENTITY";
                        //    //Int64 mAudit = Convert.ToInt64(mCommand.ExecuteScalar());


                        //    log.Info("insertando en crediplus.cp_factura");

                        //    //MF.DAL.Exactus.Crediplus.CP_Factura vFactura = new MF.DAL.Exactus.Crediplus.CP_Factura
                        //    //{
                        //    //    FACTURA = mRespuesta.facturaElectronica,
                        //    //    FACTURA_ELECTRONICA = mRespuesta.facturaElectronica,
                        //    //    CLIENTE = datos.cliente,
                        //    //    CLIENTE_CREDIPLUS=datos.cliente,
                        //    //    FECHA = FechaAplicacion,
                        //    //    FIRMADA = mRespuesta.firmada,
                        //    //    FIRMA = mRespuesta.firma,
                        //    //    SERIE = mRespuesta.serie,
                        //    //    NUMERO = mRespuesta.numero,
                        //    //    IDENTIFICADOR = mRespuesta.identificador,
                        //    //    MAQUINA = mRespuesta.maquina,
                        //    //    NOMBRE = mRespuesta.nombre,
                        //    //    DIRECCION = mRespuesta.direccion,
                        //    //    TELEFONO = mRespuesta.telefono,
                        //    //    ERROR = mRespuesta.error,
                        //    //    XML_DOCUMENTO = mXML,
                        //    //    XML_REVERSION = mXMLReversion,
                        //    //    XML_RESPUESTA = mRespuesta.xml,
                        //    //    FECHA_REGISTRO = DateTime.Now,
                        //    //    ID_CREDIPLUS = 0,
                        //    //    CREDITO_CREDIPLUS = datos.cliente,
                        //    //    NOMBRE_CREDIPLUS = datos.NombreCliente.ToUpper(),
                        //    //    VALOR_CREDIPLUS = datos.TotalNeto,
                        //    //    IVA_CREDIPLUS = datos.TotalImpuesto,
                        //    //    TOTAL_CREDIPLUS = datos.TotalBruto,
                        //    //    FECHA_REGISTRO_CREDIPLUS = FechaAplicacion


                        //    //};
                        //    //cre.CP_Factura.Add(vFactura);


                        //    //log.Info("insertando en crediplus.crediplus.DOCUMENTOS_CC");
                        //    //MF.DAL.Exactus.Crediplus.DOCUMENTOS_CC vDocumentos = new MF.DAL.Exactus.Crediplus.DOCUMENTOS_CC
                        //    //{
                        //    //    DOCUMENTO = mRespuesta.facturaElectronica,
                        //    //    TIPO = "FAC",
                        //    //    APLICACION = "Cargado de Factura" + mRespuesta.facturaElectronica,
                        //    //    FECHA_DOCUMENTO = FechaAplicacion,
                        //    //    FECHA = FechaAplicacion,
                        //    //    MONTO = datos.TotalBruto,
                        //    //    SALDO = datos.TotalBruto,
                        //    //    MONTO_LOCAL = datos.TotalBruto,
                        //    //    MONTO_DOLAR = mTotalDolar,
                        //    //    SALDO_DOLAR = mTotalDolar,
                        //    //    MONTO_CLIENTE = datos.TotalBruto,
                        //    //    SALDO_CLIENTE = datos.TotalBruto,
                        //    //    TIPO_CAMBIO_MONEDA = mTipoCambio,
                        //    //    TIPO_CAMBIO_DOLAR = 1,
                        //    //    TIPO_CAMBIO_CLIENT = 1,
                        //    //    TIPO_CAMB_ACT_LOC = mTipoCambio,
                        //    //    TIPO_CAMB_ACT_DOL = 1,
                        //    //    TIPO_CAMB_ACT_CLI = mTipoCambio,
                        //    //    SUBTOTAL = datos.TotalNeto,
                        //    //    DESCUENTO = 0,
                        //    //    IMPUESTO1 = datos.TotalImpuesto,
                        //    //    IMPUESTO2 = 0,
                        //    //    RUBRO1 = 0,
                        //    //    RUBRO2 = 0,
                        //    //    MONTO_RETENCION = 0,
                        //    //    SALDO_RETENCION = 0,
                        //    //    DEPENDIENTE = "S",
                        //    //    FECHA_ULT_CREDITO = DateTime.Parse("1980-01-01"),
                        //    //    CARGADO_DE_FACT = "S",
                        //    //    APROBADO = "S",
                        //    //    ASIENTO_PENDIENTE = "S",
                        //    //    FECHA_ULT_MOD = DateTime.Now,
                        //    //    NOTAS = "",
                        //    //    CLASE_DOCUMENTO = "N",
                        //    //    FECHA_VENCE = FechaAplicacion,
                        //    //    NUM_PARCIALIDADES = 0,
                        //    //    COBRADOR = "ND",
                        //    //    USUARIO_ULT_MOD = "sa",
                        //    //    CONDICION_PAGO = "0",
                        //    //    MONEDA = "GTQ",
                        //    //    VENDEDOR = "ND",
                        //    //    CLIENTE_REPORTE = datos.cliente,
                        //    //    CLIENTE_ORIGEN = datos.cliente,
                        //    //    CLIENTE = datos.cliente,
                        //    //    SUBTIPO = 0,
                        //    //    PORC_INTCTE = 0,
                        //    //    FECHA_ANUL = null,
                        //    //    AUD_USUARIO_ANUL = null,
                        //    //    AUD_FECHA_ANUL = null,
                        //    //    USUARIO_APROBACION = "sa",
                        //    //    FECHA_APROBACION = FechaAplicacion,
                        //    //    ANULADO = "N",
                        //    //    PAIS = "GUA",
                        //    //    BASE_IMPUESTO1 = datos.TotalNeto,
                        //    //    BASE_IMPUESTO2 = 0,
                        //    //    DEPENDIENTE_GP = "N",
                        //    //    SALDO_TRANS = 0,
                        //    //    SALDO_TRANS_LOCAL = 0,
                        //    //    SALDO_TRANS_DOLAR = 0,
                        //    //    SALDO_TRANS_CLI = 0,
                        //    //    FACTURADO = "S",
                        //    //    GENERA_DOC_FE = "N"
                        //    //     ,
                        //    //    CreatedBy = "mf.fiestanet",
                        //    //    UpdatedBy = "mf.fiestanet"
                        //    //};
                        //    //cre.DOCUMENTOS_CC.Add(vDocumentos);

                        //    //log.Info("insertando en crediplus.crediplus.FACTURA");

                        //    //MF.DAL.Exactus.Crediplus.FACTURA vFActura = new MF.DAL.Exactus.Crediplus.FACTURA
                        //    //{
                        //    //    TIPO_DOCUMENTO = "F",
                        //    //    FACTURA1 = mRespuesta.facturaElectronica,
                        //    //    AUDIT_TRANS_INV = auditId,
                        //    //    ESTA_DESPACHADO = "S",
                        //    //    EN_INVESTIGACION = "N",
                        //    //    TRANS_ADICIONALES = "N",
                        //    //    ESTADO_REMISION = "N",
                        //    //    DESCUENTO_VOLUMEN = 0,
                        //    //    MONEDA_FACTURA = "L",
                        //    //    FECHA_DESPACHO = FechaAplicacion,
                        //    //    CLASE_DOCUMENTO = "N",
                        //    //    FECHA_RECIBIDO = FechaAplicacion,
                        //    //    PEDIDO = mPedido,
                        //    //    COMISION_COBRADOR = 0,
                        //    //    TARJETA_CREDITO = "",
                        //    //    TOTAL_VOLUMEN = 1,
                        //    //    TOTAL_PESO = 1,
                        //    //    MONTO_COBRADO = 0,
                        //    //    TOTAL_IMPUESTO1 = datos.TotalImpuesto,
                        //    //    FECHA = FechaAplicacion,
                        //    //    FECHA_ENTREGA = FechaAplicacion,
                        //    //    TOTAL_IMPUESTO2 = 0,
                        //    //    PORC_DESCUENTO2 = 0,
                        //    //    MONTO_FLETE = 0,
                        //    //    MONTO_SEGURO = 0,
                        //    //    MONTO_DOCUMENTACIO = 0,
                        //    //    TIPO_DESCUENTO1 = "P",
                        //    //    TIPO_DESCUENTO2 = "P",
                        //    //    MONTO_DESCUENTO1 = 0,
                        //    //    MONTO_DESCUENTO2 = 0,
                        //    //    PORC_DESCUENTO1 = 0,
                        //    //    TOTAL_FACTURA = datos.TotalBruto,
                        //    //    FECHA_PEDIDO = FechaAplicacion,
                        //    //    FECHA_HORA_ANULA = null,
                        //    //    FECHA_ORDEN = FechaAplicacion,
                        //    //    TOTAL_MERCADERIA = datos.TotalNeto,
                        //    //    COMISION_VENDEDOR = 0,
                        //    //    FECHA_HORA = DateTime.Now,
                        //    //    TOTAL_UNIDADES = 1,
                        //    //    NUMERO_PAGINAS = 1,
                        //    //    TIPO_CAMBIO = mTipoCambio,
                        //    //    ANULADA = "N",
                        //    //    MODULO = "FA",
                        //    //    CARGADO_CG = "N",
                        //    //    CARGADO_CXC = "S",
                        //    //    EMBARCAR_A = datos.cliente,
                        //    //    DIREC_EMBARQUE = "ND",
                        //    //    DIRECCION_FACTURA = datos.DirCliente,
                        //    //    MULTIPLICADOR_EV = 1,
                        //    //    OBSERVACIONES = "",
                        //    //    VERSION_NP = 1,
                        //    //    MONEDA = "L",
                        //    //    NIVEL_PRECIO = "CONTADOEFECT",
                        //    //    COBRADOR = "ND",
                        //    //    RUTA = "ND",
                        //    //    USUARIO = "sa",
                        //    //    USUARIO_ANULA = null,
                        //    //    CONDICION_PAGO = "0",
                        //    //    ZONA = "1010",
                        //    //    VENDEDOR = "ND",
                        //    //    CLIENTE_DIRECCION = datos.cliente,
                        //    //    CLIENTE_CORPORAC = datos.cliente,
                        //    //    CLIENTE_ORIGEN = datos.cliente,
                        //    //    CLIENTE = datos.cliente,
                        //    //    PAIS = "GUA",
                        //    //    SUBTIPO_DOC_CXC = 0,
                        //    //    TIPO_DOC_CXC = "FAC",
                        //    //    MONTO_ANTICIPO = 0,
                        //    //    TOTAL_PESO_NETO = 1,
                        //    //    FECHA_RIGE = FechaAplicacion,
                        //    //    PORC_INTCTE = 0,
                        //    //    USA_DESPACHOS = "S",
                        //    //    COBRADA = "S",
                        //    //    DESCUENTO_CASCADA = "S",
                        //    //    DIRECCION_EMBARQUE = "ND",
                        //    //    CONSECUTIVO = mConsecutivo,
                        //    //    REIMPRESO = 0,
                        //    //    BASE_IMPUESTO1 = datos.TotalNeto,
                        //    //    BASE_IMPUESTO2 = 0,
                        //    //    NOMBRE_CLIENTE = datos.NombreCliente.ToUpper(),
                        //    //    NOMBREMAQUINA = null,
                        //    //    SERIE_RESOLUCION = null,
                        //    //    CONSEC_RESOLUCION = null,
                        //    //    GENERA_DOC_FE = "N",
                        //    //    TASA_IMPOSITIVA_PORC = 0
                        //    //    , TASA_CREE1_PORC = 0,
                        //    //    TASA_CREE2_PORC = 0,
                        //    //    TASA_GAN_OCASIONAL_PORC = 0,
                        //    //    COMENTARIO_CXC = "FACTURA DE ARTÍCULOS",
                        //    //    RUBRO3 = "",
                        //    //    RUBRO5 = "",
                        //    //    DOC_CREDITO_CXC = mRespuesta.facturaElectronica,
                        //    //    TIPO_CREDITO_CXC = "FAC"
                        //    //     ,
                        //    //    CreatedBy = "mf.fiestanet",
                        //    //    UpdatedBy = "mf.fiestanet"

                        //    //};
                        //    //cre.FACTURA.Add(vFActura);

                        //    //var mLinea = 1;
                        //    //log.Info("insertando en detalle de pedido y factura");

                        //    //MF.DAL.Exactus.Crediplus.PEDIDO_LINEA vpl = new MF.DAL.Exactus.Crediplus.PEDIDO_LINEA();
                        //    //foreach (var x in datos.Articulos)
                        //    //{
                        //    //    vpl = new MF.DAL.Exactus.Crediplus.PEDIDO_LINEA
                        //    //    {
                        //    //        PEDIDO = mPedido,
                        //    //        PEDIDO_LINEA1 = (short)mLinea,
                        //    //        BODEGA = "CP01",
                        //    //        ARTICULO = x.Producto,
                        //    //        ESTADO = "F",
                        //    //        FECHA_ENTREGA = FechaAplicacion,
                        //    //        LINEA_USUARIO = (short)(mLinea - 1),
                        //    //        PRECIO_UNITARIO = x.Precio,
                        //    //        CANTIDAD_PEDIDA = 1,
                        //    //        CANTIDAD_A_FACTURA = 0,
                        //    //        CANTIDAD_FACTURADA = 1,
                        //    //        CANTIDAD_RESERVADA = 0,
                        //    //        CANTIDAD_BONIFICAD = 0,
                        //    //        CANTIDAD_CANCELADA = 0,
                        //    //        TIPO_DESCUENTO = "P",
                        //    //        MONTO_DESCUENTO = 0,
                        //    //        PORC_DESCUENTO = 0,
                        //    //        DESCRIPCION = x.Descripcion.ToUpper(),
                        //    //        COMENTARIO = "",
                        //    //        FECHA_PROMETIDA = FechaAplicacion
                        //    //         ,
                        //    //        CreatedBy = "mf.fiestanet",
                        //    //        UpdatedBy = "mf.fiestanet"
                        //    //    };
                        //    //    cre.PEDIDO_LINEA.Add(vpl);

                        //    //    MF.DAL.Exactus.Crediplus.FACTURA_LINEA vFl = new MF.DAL.Exactus.Crediplus.FACTURA_LINEA
                        //    //    {
                        //    //        FACTURA = mRespuesta.facturaElectronica,
                        //    //        TIPO_DOCUMENTO = "F",
                        //    //        LINEA = (short)(mLinea - 1),
                        //    //        BODEGA = "CP01",
                        //    //        COSTO_TOTAL_DOLAR = 0,
                        //    //        PEDIDO = mPedido,
                        //    //        ARTICULO = x.Producto,
                        //    //        ANULADA = "N",
                        //    //        CANTIDAD = 1,
                        //    //        PRECIO_UNITARIO = x.Precio,
                        //    //        TOTAL_IMPUESTO1 = x.iva,
                        //    //        TOTAL_IMPUESTO2 = 0,
                        //    //        DESC_TOT_LINEA = 0,
                        //    //        DESC_TOT_GENERAL = 0,
                        //    //        COSTO_TOTAL = 0,
                        //    //        PRECIO_TOTAL = x.TotalProductoBruto,
                        //    //        DESCRIPCION = x.Descripcion,
                        //    //        COMENTARIO = "",
                        //    //        CANTIDAD_DEVUELT = 0,
                        //    //        DESCUENTO_VOLUMEN = 0,
                        //    //        TIPO_LINEA = "N",
                        //    //        CANTIDAD_ACEPTADA = 0,
                        //    //        CANT_NO_ENTREGADA = 0,
                        //    //        COSTO_TOTAL_LOCAL = 0,
                        //    //        PEDIDO_LINEA = (short)mLinea,
                        //    //        COSTO_TOTAL_COMP_DOLAR = 0,
                        //    //        COSTO_ESTIM_COMP_LOCAL = 0,
                        //    //        COSTO_ESTIM_COMP_DOLAR = 0,
                        //    //        CANT_DEV_PROCESO = 0
                        //    //         ,
                        //    //        CreatedBy = "mf.fiestanet",
                        //    //        UpdatedBy = "mf.fiestanet"

                        //    //    };

                        //    //    cre.FACTURA_LINEA.Add(vFl);
                        //    //    mLinea = mLinea + 1;


                        //    //}

                        //    cre.SaveChanges();
                        //    transactionScope.Complete();
                        //    mresp.Exito = true;
                        //    mresp.Exito = true;
                        //    mresp.Mensaje = " Pedido: " + mNumeroPedido;
                        #endregion
                        decimal mTotalDolar = Math.Round((datos.TotalBruto / mTipoCambio), 2, MidpointRounding.AwayFromZero);
                        decimal mTotal = datos.TotalBruto;
                        decimal mIva = datos.TotalImpuesto;
                        decimal mValor = datos.TotalNeto;
                        string mFactura = mRespuesta.facturaElectronica;

                        mCommand.Parameters.Add("@Cliente", SqlDbType.VarChar).Value = "";
                        mCommand.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = "";
                        mCommand.Parameters.Add("@DireccionDevuelveGFace", SqlDbType.VarChar).Value = "";
                    mCommand.Parameters.Add("@NombreDevuelveGFace", SqlDbType.VarChar).Value = "";
                    mCommand.Parameters.Add("@DireccionCompleta", SqlDbType.VarChar).Value = "";
                        mCommand.Parameters.Add("@error", SqlDbType.VarChar).Value = "";
                        mCommand.Parameters.Add("@XMLDocumento", SqlDbType.VarChar).Value = "";
                        mCommand.Parameters.Add("@XMLReversion", SqlDbType.VarChar).Value = "";
                        mCommand.Parameters.Add("@XMLRespuesta", SqlDbType.VarChar).Value = "";


                        mCommand.Parameters.Add("@Departamento", SqlDbType.VarChar).Value = "GUATEMALA";
                        mCommand.Parameters.Add("@Municipio", SqlDbType.VarChar).Value = "GUATEMALA";
                        mCommand.Parameters.Add("@Nit", SqlDbType.VarChar).Value = "544099-8";



                        mCommand.Parameters.Add("@Hoy", SqlDbType.DateTime).Value = DateTime.Now.Date;
                        mCommand.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = DateTime.Now.Date;
                        mCommand.Parameters.Add("@FechaUltima", SqlDbType.DateTime).Value = new DateTime(1980, 1, 1);



                        mCommand.Parameters["@Cliente"].Value = Convert.ToString(datos.cliente);
                        mCommand.Parameters["@Nombre"].Value = Convert.ToString(datos.NombreCliente);
                        mCommand.Parameters["@Fecha"].Value = DateTime.Now;
                        mCommand.Parameters["@DireccionDevuelveGFace"].Value = mRespuesta.direccion;
                        mCommand.Parameters["@NombreDevuelveGFace"].Value = mRespuesta.nombre;
                    
                        mCommand.Parameters["@error"].Value = mRespuesta.error;
                        mCommand.Parameters["@XMLDocumento"].Value = mXML;
                        mCommand.Parameters["@XMLReversion"].Value = mXMLReversion;
                        mCommand.Parameters["@XMLRespuesta"].Value = mRespuesta.xml;

                        


                        /**/
                        mNumeroPedido = mNumeroPedido + 1;
                        string mPedido = Convert.ToString(mNumeroPedido);
                        mPedido = "P" + mPedido.PadLeft(7, '0');


                        mCommand.CommandText = "INSERT INTO crediplus.PEDIDO ( PEDIDO, ESTADO, FECHA_PEDIDO, FECHA_PROMETIDA, FECHA_PROX_EMBARQU, FECHA_ULT_EMBARQUE, FECHA_ULT_CANCELAC, FECHA_ORDEN, TARJETA_CREDITO, EMBARCAR_A, DIREC_EMBARQUE, " +
                            "DIRECCION_FACTURA, OBSERVACIONES, TOTAL_MERCADERIA, MONTO_ANTICIPO, MONTO_FLETE, MONTO_SEGURO, MONTO_DOCUMENTACIO, TIPO_DESCUENTO1, TIPO_DESCUENTO2, MONTO_DESCUENTO1, MONTO_DESCUENTO2, PORC_DESCUENTO1, " +
                            "PORC_DESCUENTO2, TOTAL_IMPUESTO1, TOTAL_IMPUESTO2, TOTAL_A_FACTURAR, PORC_COMI_VENDEDOR, PORC_COMI_COBRADOR, TOTAL_CANCELADO, TOTAL_UNIDADES, IMPRESO, FECHA_HORA, DESCUENTO_VOLUMEN, TIPO_PEDIDO, MONEDA_PEDIDO, " +
                            "VERSION_NP, AUTORIZADO, DOC_A_GENERAR, CLASE_PEDIDO, MONEDA, NIVEL_PRECIO, COBRADOR, RUTA, USUARIO, CONDICION_PAGO, BODEGA, ZONA, VENDEDOR, CLIENTE, CLIENTE_DIRECCION, CLIENTE_CORPORAC, CLIENTE_ORIGEN, " +
                            "PAIS, SUBTIPO_DOC_CXC, TIPO_DOC_CXC, BACKORDER, PORC_INTCTE, DESCUENTO_CASCADA, FIJAR_TIPO_CAMBIO, ORIGEN_PEDIDO, DESC_DIREC_EMBARQUE, BASE_IMPUESTO1, BASE_IMPUESTO2, NOMBRE_CLIENTE, FECHA_PROYECTADA, " +
                            "TIPO_DOCUMENTO, TASA_IMPOSITIVA_PORC, TASA_CREE1_PORC, TASA_CREE2_PORC, TASA_GAN_OCASIONAL_PORC, CONTRATO_REVENTA ) " +
                            "VALUES ( '" + mPedido + "', 'F', @Fecha, @Fecha, @Fecha, @Fecha, @Fecha, @Fecha, '', @Cliente, 1, " +
                            "@DireccionCompleta, '', " + mValor + ", 0, 0, 0, 0, 'P', 'P', 0, 0, 0, " +
                            "0, " + mIva + ", 0, " + mTotal + ", 0, 0, 0, 0, 'N', GETDATE(), 0, 'N', 'L', " +
                            "1, 'N', 'F', 'N', 'L', 'CONTADOEFECT', 'ND', 'ND', 'sa', 0, 'CP01', '0101', 'ND', @Cliente, @Cliente, @Cliente, @Cliente, " +
                            "'GUA', 0, 'FAC', 'C', 0 , 'S', 'N', 'F', @DireccionCompleta, " + mValor + ", 0, @Nombre, @Hoy, " +
                            "'P', 0, 0, 0, 0, 'N' ) ";
                        mCommand.ExecuteNonQuery();

                    mCommand.CommandText = $"UPDATE crediplus.CONSECUTIVO_FA SET VALOR_CONSECUTIVO = '{mPedido}' WHERE CODIGO_CONSECUTIVO = '02'";
                    mCommand.ExecuteNonQuery();

                    mNumeroFactura++;
                    int mNumeroFacturaLength = Convert.ToString(mNumeroFactura).Length;
                    int mConsecutivoFALength = mConsecutivoFA[1].Length;

                    string mCeros = "";
                    for (int ii = Convert.ToString(mNumeroFactura).Length; ii < mConsecutivoFA[1].Length; ii++)
                    {
                        mCeros = string.Format("0{0}", mCeros);
                    }

                    //mFactura = string.Format("{0}-{1}{2}", mConsecutivoFA[0], mCeros, Convert.ToString(mNumeroFactura));

                    mCommand.CommandText = "INSERT INTO crediplus.AUDIT_TRANS_INV ( USUARIO, FECHA_HORA, MODULO_ORIGEN, APLICACION, REFERENCIA ) " +
                        "VALUES ( 'sa', GETDATE(), 'FA', 'FAC#" + mFactura + "', 'Cliente: " + datos.cliente + "' ) ";
                    mCommand.ExecuteNonQuery();

                    mCommand.CommandText = "SELECT @@IDENTITY";
                    Int64 mAudit = Convert.ToInt64(mCommand.ExecuteScalar());


                    log.Info("insertando en crediplus.cp_factura");

                    mCommand.CommandText = "INSERT INTO crediplus.CP_Factura ( FACTURA, FACTURA_ELECTRONICA, CLIENTE, FECHA, FIRMADA, FIRMA, SERIE, NUMERO, " +
                        "IDENTIFICADOR, MAQUINA, NOMBRE, DIRECCION, TELEFONO, ERROR, XML_DOCUMENTO, XML_REVERSION, XML_RESPUESTA, " +
                        "FECHA_REGISTRO, ID_CREDIPLUS, CLIENTE_CREDIPLUS, CREDITO_CREDIPLUS, NOMBRE_CREDIPLUS, VALOR_CREDIPLUS, IVA_CREDIPLUS, TOTAL_CREDIPLUS, FECHA_REGISTRO_CREDIPLUS ) " +
                        "VALUES ( '" + mFactura + "', '" + mRespuesta.facturaElectronica + "', @Cliente, @Fecha, '" + mRespuesta.firmada + "', '" + mRespuesta.firma + "', '" + mRespuesta.serie + "', '" + mRespuesta.numero + "', " +
                        "'" + mRespuesta.identificador + "', '" + mRespuesta.maquina + "', @NombreDevuelveGFace, @DireccionDevuelveGFace, '" + mRespuesta.telefono + "', @error, @XMLDocumento, @XMLReversion, @XMLRespuesta, " +
                        "GETDATE(), 0, @Cliente, '" + datos.cliente + "', @Nombre, " + mValor.ToString() + ", " + mIva.ToString() + ", " + mTotal.ToString() + ", @Fecha ) ";
                    mCommand.ExecuteNonQuery();


                    log.Info("insertando en crediplus.crediplus.DOCUMENTOS_CC");

                    mCommand.CommandText = "INSERT INTO crediplus.DOCUMENTOS_CC ( DOCUMENTO, TIPO, APLICACION, FECHA_DOCUMENTO, FECHA, MONTO, SALDO, MONTO_LOCAL, SALDO_LOCAL, MONTO_DOLAR, " +
                       "SALDO_DOLAR, MONTO_CLIENTE, SALDO_CLIENTE, TIPO_CAMBIO_MONEDA, TIPO_CAMBIO_DOLAR, TIPO_CAMBIO_CLIENT, TIPO_CAMB_ACT_LOC, TIPO_CAMB_ACT_DOL, TIPO_CAMB_ACT_CLI, SUBTOTAL, DESCUENTO, IMPUESTO1, IMPUESTO2, RUBRO1, RUBRO2, " +
                       "MONTO_RETENCION, SALDO_RETENCION, DEPENDIENTE, FECHA_ULT_CREDITO, CARGADO_DE_FACT, APROBADO, ASIENTO_PENDIENTE, FECHA_ULT_MOD, NOTAS, CLASE_DOCUMENTO, FECHA_VENCE, NUM_PARCIALIDADES, COBRADOR, USUARIO_ULT_MOD, CONDICION_PAGO, " +
                       "MONEDA, VENDEDOR, CLIENTE_REPORTE, CLIENTE_ORIGEN, CLIENTE, SUBTIPO, PORC_INTCTE, FECHA_ANUL, AUD_USUARIO_ANUL, AUD_FECHA_ANUL, USUARIO_APROBACION, FECHA_APROBACION, ANULADO, PAIS, BASE_IMPUESTO1, BASE_IMPUESTO2, " +
                       "DEPENDIENTE_GP, SALDO_TRANS, SALDO_TRANS_LOCAL, SALDO_TRANS_DOLAR, SALDO_TRANS_CLI, FACTURADO, GENERA_DOC_FE ) " +
                       "VALUES ( '" + mFactura + "', 'FAC', 'Cargado de factura: " + mFactura + "', @Fecha, @Fecha, " + mTotal.ToString() + ", " + mTotal.ToString() + ", " + mTotal.ToString() + ", " + mTotal.ToString() + ", " + mTotalDolar + ", " +
                       mTotalDolar + ", " + mTotal + ", " + mTotal + ", 1, " + mTipoCambio.ToString() + ", 1, 1, " + mTipoCambio.ToString() + ", 1, " + mValor.ToString() + ", 0, " + mIva.ToString() + ", 0, 0, 0, " +
                       "0, 0, 'S', '1980-01-01', 'S', 'S', 'S', GETDATE(), '', 'N', @Fecha, 0, 'ND', 'sa', 0, " +
                       "'GTQ', 'ND', @Cliente, @Cliente, @Cliente, 0, 0, NULL, NULL, NULL, 'sa', @Fecha, 'N', 'GUA', " + mValor.ToString() + ", 0, " +
                       "'N', 0, 0, 0, 0, 'N', 'N' ) ";
                    mCommand.ExecuteNonQuery();

                    log.Info("insertando en crediplus.crediplus.FACTURA");

                    //
                    mCommand.CommandText = "INSERT INTO crediplus.FACTURA ( TIPO_DOCUMENTO, FACTURA, AUDIT_TRANS_INV, ESTA_DESPACHADO, EN_INVESTIGACION, TRANS_ADICIONALES, ESTADO_REMISION, DESCUENTO_VOLUMEN, MONEDA_FACTURA, FECHA_DESPACHO, " +
                        "CLASE_DOCUMENTO, FECHA_RECIBIDO, PEDIDO, COMISION_COBRADOR, TARJETA_CREDITO, TOTAL_VOLUMEN, TOTAL_PESO, MONTO_COBRADO, TOTAL_IMPUESTO1, FECHA, FECHA_ENTREGA, TOTAL_IMPUESTO2, PORC_DESCUENTO2, MONTO_FLETE, MONTO_SEGURO, " +
                        "MONTO_DOCUMENTACIO, TIPO_DESCUENTO1, TIPO_DESCUENTO2, MONTO_DESCUENTO1, MONTO_DESCUENTO2, PORC_DESCUENTO1, TOTAL_FACTURA, FECHA_PEDIDO, FECHA_HORA_ANULA, FECHA_ORDEN, TOTAL_MERCADERIA, COMISION_VENDEDOR, FECHA_HORA, " +
                        "TOTAL_UNIDADES, NUMERO_PAGINAS, TIPO_CAMBIO, ANULADA, MODULO, CARGADO_CG, CARGADO_CXC, EMBARCAR_A, DIREC_EMBARQUE, DIRECCION_FACTURA, MULTIPLICADOR_EV, OBSERVACIONES, VERSION_NP, MONEDA, NIVEL_PRECIO, COBRADOR, RUTA, " +
                        "USUARIO, USUARIO_ANULA, CONDICION_PAGO, ZONA, VENDEDOR, CLIENTE_DIRECCION, CLIENTE_CORPORAC, CLIENTE_ORIGEN, CLIENTE, PAIS, SUBTIPO_DOC_CXC, TIPO_DOC_CXC, MONTO_ANTICIPO, TOTAL_PESO_NETO, FECHA_RIGE, PORC_INTCTE, " +
                        "USA_DESPACHOS, COBRADA, DESCUENTO_CASCADA, DIRECCION_EMBARQUE, CONSECUTIVO, REIMPRESO, BASE_IMPUESTO1, BASE_IMPUESTO2, NOMBRE_CLIENTE, NOMBREMAQUINA, SERIE_RESOLUCION, CONSEC_RESOLUCION, GENERA_DOC_FE, " +
                        "TASA_IMPOSITIVA_PORC, TASA_CREE1_PORC, TASA_CREE2_PORC, TASA_GAN_OCASIONAL_PORC, COMENTARIO_CXC,RUBRO3,RUBRO5,doc_credito_cxc,tipo_credito_cxC ) " +
                        "VALUES ( 'F', '" + mFactura + "', " + mAudit + ", 'N', 'N', 'N', 'N', 0, 'L', @Fecha, " +
                        "'N', @Fecha, '" + mPedido + "', 0, '', 1, 1, 0, " + mIva.ToString() + ", @Fecha, @Fecha, 0, 0, 0, 0, " +
                        "0, 'P', 'P', 0, 0, 0, " + mTotal.ToString() + ", @Fecha, NULL, @Fecha, " + mValor.ToString() + ", 0, GETDATE(), " +
                        "1, 1, " + mTipoCambio.ToString() + ", 'N', 'FA', 'N', 'S', @Cliente, 'ND', @DireccionCompleta, 1, '', 1, 'L', 'CONTADOEFECT', 'ND', 'ND', " +
                        "'sa', NULL, 0, 001010, 'ND', @Cliente, @Cliente, @Cliente, @Cliente, 'GUA', 0, 'FAC', 0, 1, @Fecha, 0, " +
                        "'S', 'S', 'S','ND', '" + mConsecutivo + "', 0, " + mValor.ToString() + ", 0, @Nombre, NULL, NULL,NULL, 'N', " +
                        "0, 0, 0, 0, 'FACTURA ARTICULOS','" + mFactura + "','','" + mFactura + "','FAC' ) ";
                    mCommand.ExecuteNonQuery();

                    var mLinea = 1;
                        log.Info("insertando en detalle de pedido y factura");
                        foreach (var itemD in datos.Articulos)
                        {
                        decimal mValorProducto = itemD.Precio;
                        decimal mIvaProducto = itemD.iva;

                        mCommand.CommandText = $"SELECT costo_prom_loc FROM  crediplus.articulo  WHERE articulo = '{itemD.Producto}'";
                        decimal CostoCP = Convert.ToDecimal(mCommand.ExecuteScalar());

                        mCommand.CommandText = $"SELECT costo_prom_dol FROM  crediplus.articulo  WHERE articulo = '{itemD.Producto}'";
                        decimal CostoCPUSD = Convert.ToDecimal(mCommand.ExecuteScalar());


                        mCommand.CommandText = "INSERT INTO crediplus.PEDIDO_LINEA ( PEDIDO, PEDIDO_LINEA, BODEGA, ARTICULO, ESTADO, FECHA_ENTREGA, LINEA_USUARIO, PRECIO_UNITARIO, CANTIDAD_PEDIDA, CANTIDAD_A_FACTURA, " +
                                "CANTIDAD_FACTURADA, CANTIDAD_RESERVADA, CANTIDAD_BONIFICAD, CANTIDAD_CANCELADA, TIPO_DESCUENTO, MONTO_DESCUENTO, PORC_DESCUENTO, DESCRIPCION, COMENTARIO, FECHA_PROMETIDA ) " +
                                "VALUES ( '" + mPedido + "', " + mLinea.ToString() + ", 'CP01'," + "'" + itemD.Producto + "', 'F', @Fecha, " + (mLinea - 1).ToString() + ", " + mValorProducto.ToString() + ", 1, 0, " +
                                "1, 0, 0, 0, 'P', 0, 0, '" + itemD.Descripcion + "', '', @Fecha )";
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = "INSERT INTO crediplus.FACTURA_LINEA ( FACTURA, TIPO_DOCUMENTO, LINEA, BODEGA, COSTO_TOTAL_DOLAR, PEDIDO, ARTICULO, ANULADA, FECHA_FACTURA, CANTIDAD, PRECIO_UNITARIO, TOTAL_IMPUESTO1, TOTAL_IMPUESTO2, " +
                            "DESC_TOT_LINEA, DESC_TOT_GENERAL, COSTO_TOTAL, PRECIO_TOTAL, DESCRIPCION, COMENTARIO, CANTIDAD_DEVUELT, DESCUENTO_VOLUMEN, TIPO_LINEA, CANTIDAD_ACEPTADA, CANT_NO_ENTREGADA, COSTO_TOTAL_LOCAL, PEDIDO_LINEA, " +
                            "MULTIPLICADOR_EV, CANT_DESPACHADA, COSTO_ESTIM_LOCAL, COSTO_ESTIM_DOLAR, CANT_ANUL_PORDESPA, MONTO_RETENCION, BASE_IMPUESTO1, BASE_IMPUESTO2, COSTO_TOTAL_COMP, COSTO_TOTAL_COMP_LOCAL, " +
                            "COSTO_TOTAL_COMP_DOLAR, COSTO_ESTIM_COMP_LOCAL, COSTO_ESTIM_COMP_DOLAR, CANT_DEV_PROCESO ) " +
                            "VALUES ( '" + mFactura + "', 'F', " + (mLinea - 1).ToString() + ", 'CP01', " + CostoCPUSD + ", '" + mPedido + "', '" + itemD.Producto + "', 'N', @Fecha, 1, " + mValorProducto.ToString() + ", " + mIvaProducto.ToString() + ", 0, " +
                            "0, 0," + CostoCP + ", " + mValorProducto.ToString() + ", '" + itemD.Descripcion + "', '', 0, 0, 'N', 0, 0, " + CostoCP + ", " + mLinea.ToString() + ", " +
                            "1, 0, " + CostoCP + ", " + CostoCPUSD + ", 0, 0, " + mValorProducto.ToString() + ", 0, 0, 0, " +
                            "0, " + CostoCP + ", " + CostoCPUSD + ", 0 ) ";
                        mCommand.ExecuteNonQuery();
                        mLinea++;
                    }

                        mTransaction.Commit();
                        mConexion.Close();
                    mresp.Exito = true;
                    mresp.Mensaje += "Factura " + mRespuesta.facturaElectronica;
                  
                }
               else
                mresp.Mensaje += mRespuesta.error;

            }
            catch (Exception Ex)
            {
                if (mConexion.State == ConnectionState.Open)
                {
                    mTransaction.Rollback();
                    mConexion.Close();
                }
                mresp.Exito = false;
                mresp.Mensaje= "Error al facturar un artículo desde crediplus "+Ex.Message+" "+Ex.InnerException;
                log.Error("Error al facturar un artículo desde crediplus "+Ex.Message+" "+Ex.InnerException);
            }
            return mresp;
        }
        private Clases.FacturasAtid_MF ObtenerDatosFacturacionFiesta(string facturaMF)
        {
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            Clases.FacturasAtid_MF facturascp = new Clases.FacturasAtid_MF();
            try
            {
                #region "conexión"




                EntityConnection ec = (EntityConnection)db.Connection;
                SqlConnection sc = (SqlConnection)ec.StoreConnection;

                SqlConnection mConexion = new SqlConnection(sc.ConnectionString);
                SqlCommand mCommand = new SqlCommand();
                mConexion.Open();
                mCommand.Connection = mConexion;

                mCommand.CommandText = "SELECT * from crediplus.mf_cliente C  join crediplus.cliente cli on C.cliente=cli.cliente where C.cliente ='" + MF_Clases.Utils.CONSTANTES.GUATEFAC.CLIENTE.MF + "'";
                mCommand.ExecuteNonQuery();
                using (SqlDataReader rdr = mCommand.ExecuteReader())
                {
                    while (rdr.Read())
                    {

                        facturascp = new Clases.FacturasAtid_MF
                        {
                            cliente = rdr["CLIENTE"].ToString(),
                            dpi = rdr["DPI"].ToString(),
                            nit = rdr["NIT_FACTURA"].ToString(),
                            eMail = rdr["E_MAIL"].ToString(),
                            nombrecliente = rdr["NOMBRE"].ToString(),
                            primerNombre = rdr["PRIMER_NOMBRE"].ToString(),
                            primerApellido = rdr["PRIMER_APELLIDO"].ToString(),
                            fechaRegistro = DateTime.Now,
                            id = 1,
                            credito = facturaMF//en el caso de la facturación a MF
                        };
                    }
                }

                mConexion.Close();

                var qDetalle = new DALCrediplus().ObtenerProductosCrediplus(facturaMF);
                int nlinea = 0;

                //--
                //detalle de facturación de prodcutos de Outlet para Muebles Fiesta
                //--
                facturascp.detalleFactura = new List<DetalleFacturaAtid>();
                foreach (var item in qDetalle)
                {
                    //--
                    //detalle de facturación de prodcutos de Outlet para Muebles Fiesta
                    //--
                    DetalleFacturaConCosto det = new DetalleFacturaConCosto
                    {
                        linea = nlinea,
                        descripcion = item.Descripcion,
                        iva = item.IvaCP,
                        valor = item.PrecioSinIvaCP,
                        total = item.PrecioCP,
                        producto = item.CodigoCP,
                        //costo para MF
                        costo = item.PrecioSinIvaCP,
                        productoMF = item.CodigoMF
                    };
                    facturascp.detalleFactura.Add(new DetalleFacturaAtid
                    {
                        linea = nlinea,
                        descripcion = item.Descripcion,
                        iva = item.IvaCP,
                        valor = item.PrecioSinIvaCP,
                        total = item.PrecioCP,
                        producto = item.CodigoCP,
                        idFactura = 1
                    });
                    facturascp.detalleFacturaCosto.Add(det);
                    nlinea++;
                }

                #endregion


            }
            catch (Exception ex)
            {
                log.Error("Error al obtener datos de la factura de Muebles Fiesta " + ex.Message + " | " + ex.InnerException);
            }

            return facturascp;

        }
    }
}