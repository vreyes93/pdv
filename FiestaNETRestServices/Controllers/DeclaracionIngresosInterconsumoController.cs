﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;
using MF_Clases;

namespace FiestaNETRestServices.Controllers
{
    public class DeclaracionIngresosInterconsumoController : ApiController
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET api/declaracioningresosinterconsumo
        public LinkInterconsumoController.Retorna Get(string documento)
        {
            log.Info("Consumiendo servicio de link de Interconsumo.");

            LinkInterconsumoController.Retorna retorna = new LinkInterconsumoController.Retorna();
            retorna = LinkInterconsumoController.LinkInter(documento);

            if (retorna.exito)
            {
                string mUrlLecturaDPI = "https://www.interbanking.com.gt/interconsumo.portalweb/WEBSolicitud_lecturaDPI.aspx?p=";
                string mUrlDeclaracion = "https://www.interbanking.com.gt/interconsumo.portalweb/WEBFormulario_declaracioningresos.aspx?p=";

                retorna.link = retorna.link.Replace(mUrlLecturaDPI, mUrlDeclaracion);
                log.Info("Link de declaración de ingresos recibido exitosamente.");
            }
            else
            {
                log.Error(retorna.mensaje);
            }

            return retorna;
        }
    }
}
