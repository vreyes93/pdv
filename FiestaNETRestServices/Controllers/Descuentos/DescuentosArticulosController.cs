﻿using FiestaNETRestServices.Content.Abstract;
using MF_Clases.Facturacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Descuentos
{
    [RoutePrefix("api/DescuentosArticulos")]
    public class DescuentosArticulosController : MFApiController
    {
        [HttpPost]
        [Route("ValidarDescuentoArticulo/{articulosValidar}")]
        [ActionName("ValidarDescuentoArticulo")]
        public IHttpActionResult ValidarArticuloDescuento([FromBody]  DescuentosArticulos articulosValidar)
        {
            bool blOk = true;
            decimal factor = articulosValidar.factor;

            #region "Antes"
            //bool ArticulosActualesOferta = false;
            //bool HayArticuloEnOferta = articulosActuales.Count() > 0 ? (articulosActuales.Any(x => x.originalDiscountPrice != x.originalPrice) || ArticuloAgregar.originalDiscountPrice != ArticuloAgregar.originalPrice)
            //                                            : (ArticuloAgregar.originalDiscountPrice != ArticuloAgregar.originalPrice);

            //ArticulosActualesOferta = articulosActuales.Count() > 0 && (articulosActuales.Any(x => x.originalDiscountPrice != x.originalPrice && x.originalDiscountPrice <= x.price));


            //if (HayArticuloEnOferta && (ArticuloAgregar.price != ArticuloAgregar.originalPrice)) //solo si se está aplicando a un descuento
            //{
            //    decimal TotalDescuento = articulosActuales.Count() > 0 && ArticulosActualesOferta ? articulosActuales.Sum(x => ((x.originalPrice / factor) - (x.originalDiscountPrice / factor))) + (ArticuloAgregar.originalPrice / factor) - (ArticuloAgregar.originalDiscountPrice / factor)
            //                                            : (ArticuloAgregar.originalPrice / factor) - (ArticuloAgregar.originalDiscountPrice / factor);

            //    decimal DescuentoUtilizado = articulosActuales.Count() > 0 && ArticulosActualesOferta ? articulosActuales.Sum(x => (x.originalPrice / factor - (x.price / factor))) + (ArticuloAgregar.originalPrice / factor) - ((ArticuloAgregar.price / factor))
            //                              : (ArticuloAgregar.originalPrice / factor) - ((ArticuloAgregar.price / factor));



            //    //decimal TotalAccesorios = articulosActuales.Where(x => x.EsDecoracion).Sum(x => x.originalPrice / factor);
            //    if ((TotalDescuento) - DescuentoUtilizado < 0)
            //        blOk = false;
            //}
            #endregion
            bool PrecioOriginal = WebConfigurationManager.AppSettings["ValidarDescuentos"].ToString().Equals("1");
            bool ArticulosActualesOferta = false;
            if (PrecioOriginal)
            {
                bool HayArticuloEnOferta = articulosValidar.articulosActuales.Count() > 0 ? (articulosValidar.articulosActuales.Any(x => x.originalDiscountPrice != x.originalPrice) || articulosValidar.ArticuloAgregar.originalDiscountPrice != articulosValidar.ArticuloAgregar.originalPrice)
                                                            : (articulosValidar.ArticuloAgregar.originalDiscountPrice != articulosValidar.ArticuloAgregar.originalPrice);

                ArticulosActualesOferta = articulosValidar.articulosActuales.Count() > 0 && (articulosValidar.articulosActuales.Any(x => x.originalDiscountPrice != x.originalPrice && x.originalDiscountPrice <= x.price));


                if (HayArticuloEnOferta && (articulosValidar.ArticuloAgregar.price != articulosValidar.ArticuloAgregar.originalPrice)) //solo si se está aplicando a un descuento
                {
                    decimal TotalDescuento = articulosValidar.articulosActuales.Count() > 0 && ArticulosActualesOferta ? articulosValidar.articulosActuales.Sum(x => ((x.originalPrice / factor) - (x.originalDiscountPrice / factor))) + (articulosValidar.ArticuloAgregar.originalPrice / factor) - (articulosValidar.ArticuloAgregar.originalDiscountPrice / factor)
                                                            : (articulosValidar.ArticuloAgregar.originalPrice / factor) - (articulosValidar.ArticuloAgregar.originalDiscountPrice / factor);

                    decimal DescuentoUtilizado = articulosValidar.articulosActuales.Count() > 0 && ArticulosActualesOferta ? articulosValidar.articulosActuales.Sum(x => (x.originalPrice  - (x.price ))) + (articulosValidar.ArticuloAgregar.originalPrice ) - ((articulosValidar.ArticuloAgregar.price))
                                              : (articulosValidar.ArticuloAgregar.originalPrice ) - ((articulosValidar.ArticuloAgregar.price));


                    if ((Math.Round(TotalDescuento,2)) - Math.Round(DescuentoUtilizado,2)< 0)
                        blOk = false;
                }

            }
            return Ok(blOk);
        }
    }
}
