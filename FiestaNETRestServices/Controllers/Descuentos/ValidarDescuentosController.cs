﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.DAL;
using MF.Comun.Dto.Facturacion;
using MF_Clases.Comun;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Descuentos
{
    [RoutePrefix("api/ValidarDescuentos")]
    public class ValidarDescuentosController : MFApiController
    {
        [HttpGet]
        [Route("ValidarDescuentos/{id}")]
        [ActionName("ValidarDescuentos")]
        public IHttpActionResult Get(string id)
        {

            DALDescuentos descuentos = new DALDescuentos();
            return Ok(descuentos.ValidarSumatoriasFel(id,0));
        }
        [HttpPost]
        [Route("ValidarDescuentos/{pedido}")]
        [ActionName("ValidarDescuentos")]
        public IHttpActionResult Post([FromBody] PromocionRegistro pedido)
        {

            DALDescuentos descuentos = new DALDescuentos();
            return Ok(descuentos.ValidarDescuentoVales(pedido));
        }

    }
}
