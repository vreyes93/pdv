﻿using FiestaNETRestServices.DAL;
using MF_Clases.GuateFacturas.FEL.ImpresionFEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.FacturaElectronica
{
    public class DocumentoElectronicoController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public IHttpActionResult Get(string id)
        {
            List<ImpDoc> lstdoc = new List<ImpDoc>();
            try
            {
                DALFel objDal = new DALFel();
                lstdoc = objDal.ObtenerDocumentoFirmado(id);
                if (lstdoc != null)
                    return Ok<List<ImpDoc>>(lstdoc);
            }
            catch (Exception ex)
            {
                log.Error(CatchClass.ExMessage(ex, "DocumentoElectronicoController", "Get"));
            }
            return BadRequest();
        }
    }
}
