﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.EntityClient;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;
using Newtonsoft.Json;
using RestSharp;

namespace FiestaNETRestServices.Controllers.FacturaElectronica
{
    public class FelActivadoController : MFApiController
    {
        // GET: api/FelActivado
        public string Get()
        {
            return (from c in db.MF_Catalogo where c.CODIGO_TABLA == 11 && c.CODIGO_CAT == "HABILITAR_FEL" select c).First().NOMBRE_CAT;
        }

    }
}
