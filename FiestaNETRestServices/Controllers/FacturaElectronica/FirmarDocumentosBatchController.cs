﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.EntityClient;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;
using Newtonsoft.Json;
using RestSharp;

namespace FiestaNETRestServices.Controllers.FacturaElectronica
{
    public class FirmarDocumentosBatchController : MFApiController
    {
        // GET: api/FirmarDocumentosBatch
        public string Get()
        {
            return "Debe enviar las fechas de los documentos a firmar";
        }

        // GET: api/FirmarDocumentosBatch/5
        public string Get(string fechaInicial, string fechaFinal)
        {
            return FirmarDocumentos("", fechaInicial, fechaFinal);
        }

        public string Get(string documento)
        {

            return FirmarDocumentos(documento, "", "");
        }

        string FirmarDocumentos(string documento, string fechaInicial, string fechaFinal)
        {
            string mRetorna = ""; string mMensaje = ""; string mNit = ""; string mDocumento = "";

            try
            {
                string mUrlRestServices = "http://localhost:53874/api";
                string mAmbiente = WebConfigurationManager.AppSettings["Ambiente"];

                if (mAmbiente == "PRO") mUrlRestServices = "http://sql.fiesta.local/RestServices/api";
                if (mAmbiente == "PRU") mUrlRestServices = "http://sql.fiesta.local/RestServicesPruebas/api";

                var qDocumentos = from g in db.MF_GFace where g.ANULADA == "N" orderby g.FECHA_DOCUMENTO, g.TIPO select g;

                if (documento == "")
                {
                    string[] mInicial = fechaInicial.Split(new string[] { "-" }, StringSplitOptions.None);
                    string[] mFinal = fechaFinal.Split(new string[] { "-" }, StringSplitOptions.None);

                    DateTime mFechaInicial = new DateTime(Convert.ToInt32(mInicial[0]), Convert.ToInt32(mInicial[1]), Convert.ToInt32(mInicial[2]));
                    DateTime mFechaFinal = new DateTime(Convert.ToInt32(mFinal[0]), Convert.ToInt32(mFinal[1]), Convert.ToInt32(mFinal[2]));

                    qDocumentos = (System.Linq.IOrderedQueryable<FiestaNETRestServices.Models.MF_GFace>)qDocumentos.Where(x => x.FECHA_DOCUMENTO >= mFechaInicial && x.FECHA_DOCUMENTO <= mFechaFinal).OrderBy(x => x.FECHA_DOCUMENTO);

                    if (mFechaInicial.Year == 2018)
                        qDocumentos = (System.Linq.IOrderedQueryable<FiestaNETRestServices.Models.MF_GFace>)qDocumentos.Where(x => x.TIPO == "FC").OrderBy(x => x.FECHA_DOCUMENTO).ThenBy(x => x.TIPO);
                }
                else
                {
                    qDocumentos = (System.Linq.IOrderedQueryable<FiestaNETRestServices.Models.MF_GFace>)qDocumentos.Where(x => x.DOCUMENTO == documento);
                }

                if (qDocumentos.Count() == 0) return "Error, no se encontraron documentos";
                Int32 mCuantas = (from g in db.MF_GFaceFelCertifica select g).Count();

                Int64 mReferencia = Convert.ToInt64((from c in db.CONSECUTIVO_FA where c.CODIGO_CONSECUTIVO == "FEL" select c).First().VALOR_CONSECUTIVO);

                int mFirmados = 0; int mYaFirmados = 0;
                foreach (var item in qDocumentos)
                {
                    string mFirma = "";
                    MF_Clases.GuateFacturas.FEL.Doc mFEL = new MF_Clases.GuateFacturas.FEL.Doc();

                    mCuantas++;
                    mReferencia++;
                    mDocumento = item.DOCUMENTO;

                    mNit = item.NIT.Trim().Replace(" ", "");
                    if (mNit == "EXPORT") mNit = "CF";
                    if (mNit != "CF")
                    {
                        Int64 mNitInt = 0;
                        string mCaracter = "";

                        try
                        {
                            mNitInt = Convert.ToInt64(mNit);
                        }
                        catch
                        {
                            mCaracter = mNit.Substring(mNit.Length - 1, 1);
                            try
                            {
                                mNitInt = Convert.ToInt64(mNit.Substring(0, mNit.Length - 1));
                            }
                            catch
                            {
                                mNit = "CF";
                                mCaracter = "";
                            }
                        }

                        mNit = string.Format("{0}{1}", mNitInt.ToString(), mCaracter);
                    }

                    string mNombreFactura = (from c in db.MF_Cliente where c.CLIENTE == item.CLIENTE select c).First().NOMBRE_FACTURA;
                    var qCobrador = (from c in db.MF_Cobrador where c.COBRADOR == item.COBRADOR select c).First();

                    int mTipoDocumento = 2;
                    string mIdMaquina = qCobrador.ID_MAQUINA_CONTADO;

                    if (item.COBRADOR != "F01" && item.TIPO == "FC")
                    {
                        mTipoDocumento = 1;
                        if ((from c in db.MF_NIVEL_PRECIO_COEFICIENTE join p in db.MF_Pedido on c.NIVEL_PRECIO equals p.NIVEL_PRECIO where p.FACTURA == item.DOCUMENTO select c).First().TIPO == "CR") mIdMaquina = qCobrador.ID_MAQUINA_CREDITO;
                    }

                    string mDestinoVenta = "1"; string mMonedaFactura = "1";
                    if (item.TIPO == "NC") mTipoDocumento = 10;

                    mFEL.EsExportacion = false;
                    if (item.SERIE == "EX")
                    {
                        mDestinoVenta = "69";
                        mMonedaFactura = "2";

                        string mConsignatario = "0614181182001-5";

                        mFEL.EsExportacion = true;
                        mFEL.Incoterm = "DAP";
                        mFEL.NombreConsignatarioODestinatario = mNombreFactura;
                        mFEL.CodigoConsignatarioODestinatario = mConsignatario;
                        mFEL.NombreComprador = mNombreFactura;
                        mFEL.DireccionComprador = item.DIRECCION.Trim();
                        mFEL.CodigoComprador = mConsignatario;
                        mFEL.OtraReferencia = "ENTREGA INMEDIATA";
                        mFEL.CodigoExportador = "M74841";
                        mFEL.DireccionConsignatarioODestinatario = item.DIRECCION.Trim();
                    }
                    
                    DateTime mFecha = item.FECHA_DOCUMENTO.Date;
                    if (mAmbiente == "DES") mFecha = DateTime.Now.Date;
                    
                    string mBienServicio = item.BIEN_SERVICIO.Trim().Substring(0, 1);
                    var qFactura = from f in db.FACTURA where f.FACTURA1 == item.DOCUMENTO select f;

                    mFEL.NITReceptor = mNit;
                    mFEL.Nombre = mNombreFactura.Trim().Replace("/", " ").Replace("?", " ").Replace("|", " ").Replace("&", "Y").Replace("<", "").Replace(">", "");
                    mFEL.Direccion = item.DIRECCION.Trim().Replace("/", " ").Replace("?", " ").Replace("|", " ").Replace("&", "Y").Replace("<", "").Replace(">", "");
                    mFEL.NITEmisor = "5440998";
                    mFEL.IdMaquina = mIdMaquina;
                    mFEL.Establecimiento = (int)qCobrador.ESTABLECIMIENTO;
                    mFEL.TipoVenta = mBienServicio;
                    mFEL.DestinoVenta = mDestinoVenta;
                    mFEL.Fecha = mFecha;
                    mFEL.Moneda = mMonedaFactura;
                    mFEL.Tasa = item.TASA_CAMBIO;
                    mFEL.Referencia = mReferencia.ToString(); //Convert.ToString(Convert.ToInt64(item.NUMERO_DOCUMENTO) + mCuantas);
                    mFEL.NumeroAcceso = "";
                    mFEL.SerieAdmin = item.SERIE;
                    mFEL.NumeroAdmin = item.NUMERO_DOCUMENTO;
                    mFEL.Reversion = "N";
                    mFEL.TipoDocumento = mTipoDocumento;
                    mFEL.Servicio = MF_Clases.Utils.CONSTANTES.GUATEFAC.SERVICIOS.GENERAR_DOCUMENTO;
                    mFEL.Bruto = 0;
                    mFEL.Descuento = 0;
                    mFEL.Exento = 0;
                    mFEL.Otros = 0;
                    mFEL.Neto = 0;
                    mFEL.Isr = 0;
                    mFEL.Iva = 0;
                    mFEL.Total = 0;
                    mFEL.Email = "";
                    mFEL.Enviar = "N";
                    mFEL.NumeroAbono = "1";
                    mFEL.FechaVencimiento = DateTime.Now.Date.AddMonths(6);
                    mFEL.MontoAbono = 0;
                    
                    if (item.TIPO == "NC")
                    {
                        string mFactura = (from a in db.AUXILIAR_CC where a.CREDITO == item.DOCUMENTO orderby a.CreateDate descending select a).First().DEBITO;
                        string mXmlFactura = (from g in db.MF_GFaceFelCertifica where g.DOCUMENTO == mFactura select g).First().XML_FIRMADO;

                        XmlDocument xmlMensaje = new XmlDocument();
                        xmlMensaje.PreserveWhitespace = true;

                        xmlMensaje.LoadXml(mXmlFactura);

                        mFEL.DASerie = xmlMensaje.SelectNodes("Resultado/Serie").Item(0).InnerText;
                        mFEL.DAPreimpreso = xmlMensaje.SelectNodes("Resultado/Preimpreso").Item(0).InnerText;
                    }

                    string mDocumentoBuscar = item.DOCUMENTO;
                    if (item.SERIE == "EX" || item.MONEDA == "USD") mDocumentoBuscar = "";

                    var qDetalle = from d in db.FACTURA_LINEA where d.FACTURA == mDocumentoBuscar select d;
                    decimal mBruto = 0; decimal mExento = 0; decimal mNeto = 0; decimal mIva = 0; decimal mTotal = 0;

                    foreach (var det in qDetalle)
                    {
                        double mFactor = 0.107142857;
                        double mIvaLinea = Math.Round((double)det.TOTAL_IMPUESTO1, 2, MidpointRounding.AwayFromZero);
                        double mBrutoLinea = mIvaLinea / mFactor;
                        double mNetoLinea = mIvaLinea == 0 ? 0 : mBrutoLinea - mIvaLinea;
                        double mExentoLinea = mIvaLinea == 0 ? mBrutoLinea : 0;
                        double mTotalLinea = mBrutoLinea;

                        decimal mBrutoLineaDecimal = Math.Round((decimal)mBrutoLinea, 2, MidpointRounding.AwayFromZero);
                        decimal mExentoLineaDecimal = Math.Round((decimal)mExentoLinea, 2, MidpointRounding.AwayFromZero);
                        decimal mNetoLineaDecimal = Math.Round((decimal)mNetoLinea, 2, MidpointRounding.AwayFromZero);
                        decimal mIvaLineaDecimal = Math.Round((decimal)mIvaLinea, 2, MidpointRounding.AwayFromZero);
                        decimal mTotalLineaDecimal = Math.Round((decimal)mTotalLinea, 2, MidpointRounding.AwayFromZero);

                        mBruto = mBruto + mBrutoLineaDecimal;
                        mIva = mIva + mIvaLineaDecimal;
                        mNeto = mNeto + mNetoLineaDecimal;
                        mExento = mExento + mExentoLineaDecimal;
                        mTotal = mTotal + mTotalLineaDecimal;

                        //mBruto = mBruto + det.PRECIO_TOTAL + det.TOTAL_IMPUESTO1;
                        //mExento = mExento + det.TOTAL_IMPUESTO1 == 0 ? det.PRECIO_TOTAL + det.TOTAL_IMPUESTO1 : 0;
                        //mNeto = mNeto + det.TOTAL_IMPUESTO1 == 0 ? 0 : det.PRECIO_TOTAL;
                        //mIva = mIva + det.TOTAL_IMPUESTO1;
                        //mTotal = mTotal + det.PRECIO_TOTAL + det.TOTAL_IMPUESTO1;

                        MF_Clases.GuateFacturas.FEL.DetalleDoc mDetalle = new MF_Clases.GuateFacturas.FEL.DetalleDoc();
                        mDetalle.Producto = det.ARTICULO;
                        mDetalle.Descripcion = det.DESCRIPCION.Trim().Replace("/", " ").Replace("?", " ").Replace("|", " ").Replace("&", "Y").Replace("<", "").Replace(">", "");
                        mDetalle.Cantidad = det.CANTIDAD;
                        //mDetalle.Precio = Math.Round((det.PRECIO_TOTAL + det.TOTAL_IMPUESTO1) / det.CANTIDAD, 2, MidpointRounding.AwayFromZero);
                        //mDetalle.Precio = Math.Round((decimal)mBrutoLinea / det.CANTIDAD, 2, MidpointRounding.AwayFromZero);
                        mDetalle.Precio = (decimal)mBrutoLinea / det.CANTIDAD;
                        mDetalle.PorcDesc = 0;
                        //mDetalle.ImpBruto = det.PRECIO_TOTAL + det.TOTAL_IMPUESTO1;
                        mDetalle.ImpBruto = mBrutoLineaDecimal;
                        mDetalle.ImpDescuento = 0;
                        //mDetalle.ImpExento = det.TOTAL_IMPUESTO1 == 0 ? det.PRECIO_TOTAL + det.TOTAL_IMPUESTO1 : 0;
                        mDetalle.ImpExento = mExentoLineaDecimal;
                        mDetalle.ImpOtros = 0;
                        //mDetalle.ImpNeto = det.TOTAL_IMPUESTO1 == 0 ? 0 : det.PRECIO_TOTAL;
                        mDetalle.ImpNeto = mNetoLineaDecimal;
                        mDetalle.ImpIsr = 0;
                        //mDetalle.ImpIva = det.TOTAL_IMPUESTO1;
                        mDetalle.ImpIva = mIvaLineaDecimal;
                        //mDetalle.ImpTotal = det.PRECIO_TOTAL + det.TOTAL_IMPUESTO1;
                        mDetalle.ImpTotal = mTotalLineaDecimal;
                        mDetalle.TipoVentaDet = mBienServicio;
                        mDetalle.Medida = MF_Clases.Utils.CONSTANTES.GUATEFAC.MEDIDA.UNIDAD;
                        mFEL.Detalle.Add(mDetalle);
                    }

                    mFEL.Bruto = mBruto;
                    mFEL.Exento = mExento;
                    mFEL.Neto = mNeto;
                    mFEL.Iva = mIva;
                    mFEL.Total = mTotal;

                    //mFEL.Bruto = Math.Round(mBruto, 2, MidpointRounding.AwayFromZero);
                    //mFEL.Exento = Math.Round(mExento, 2, MidpointRounding.AwayFromZero);
                    //mFEL.Neto = Math.Round(mNeto, 2, MidpointRounding.AwayFromZero);
                    //mFEL.Iva = Math.Round(mIva, 2, MidpointRounding.AwayFromZero);
                    //mFEL.Total = Math.Round(mTotal, 2, MidpointRounding.AwayFromZero);

                    if (mFEL.Detalle.Count() == 0)
                    {
                        if (item.SERIE == "EX" || item.MONEDA == "USD")
                        {
                            double mFactor = 0.107142857;
                            double mIvaLinea = Math.Round((double)item.IVA, 2, MidpointRounding.AwayFromZero);
                            double mBrutoLinea = mIvaLinea / mFactor;
                            double mNetoLinea = mIvaLinea == 0 ? 0 : mBrutoLinea - mIvaLinea;

                            decimal mBrutoLineaDecimal = Math.Round((decimal)mBrutoLinea, 2, MidpointRounding.AwayFromZero);
                            decimal mIvaLineaDecimal = Math.Round((decimal)mIvaLinea, 2, MidpointRounding.AwayFromZero);
                            decimal mNetoLineaDecimal = Math.Round((decimal)mNetoLinea, 2, MidpointRounding.AwayFromZero);

                            mFEL.Bruto = item.SERIE == "EX" ? item.TOTAL : mBrutoLineaDecimal;
                            mFEL.Exento = item.SERIE == "EX" ? item.TOTAL : 0;
                            mFEL.Neto = item.SERIE == "EX" ? 0 : mNetoLineaDecimal;
                            mFEL.Iva = item.SERIE == "EX" ? 0 : mIvaLineaDecimal;
                            mFEL.Total = item.SERIE == "EX" ? item.TOTAL : mBrutoLineaDecimal;
                        }
                        else
                        {
                            if (qFactura.Count() == 0)
                            {
                                var qCC = (from c in db.DOCUMENTOS_CC where c.DOCUMENTO == item.DOCUMENTO select c).First();

                                mFEL.Bruto = qCC.MONTO;
                                mFEL.Exento = 0;
                                mFEL.Neto = qCC.SUBTOTAL;
                                mFEL.Iva = qCC.IMPUESTO1;
                                mFEL.Total = qCC.MONTO;
                            }
                            else
                            {
                                mFEL.Bruto = qFactura.First().TOTAL_FACTURA;
                                mFEL.Exento = 0;
                                mFEL.Neto = qFactura.First().TOTAL_MERCADERIA;
                                mFEL.Iva = qFactura.First().TOTAL_IMPUESTO1;
                                mFEL.Total = qFactura.First().TOTAL_FACTURA;
                            }
                        }

                        MF_Clases.GuateFacturas.FEL.DetalleDoc mDetalle = new MF_Clases.GuateFacturas.FEL.DetalleDoc();
                        mDetalle.Producto = "S000";
                        mDetalle.Descripcion = "Servicios prestados";
                        mDetalle.Cantidad = 1;
                        mDetalle.Precio = mFEL.Bruto;
                        mDetalle.PorcDesc = 0;
                        mDetalle.ImpBruto = mFEL.Bruto;
                        mDetalle.ImpDescuento = 0;
                        mDetalle.ImpExento = mFEL.Exento;
                        mDetalle.ImpOtros = 0;
                        mDetalle.ImpNeto = mFEL.Neto;
                        mDetalle.ImpIsr = 0;
                        mDetalle.ImpIva = mFEL.Iva;
                        mDetalle.ImpTotal = mFEL.Total;
                        mDetalle.TipoVentaDet = mBienServicio;
                        mDetalle.Medida = MF_Clases.Utils.CONSTANTES.GUATEFAC.MEDIDA.UNIDAD;
                        mFEL.Detalle.Add(mDetalle);
                    }

                    string json = JsonConvert.SerializeObject(mFEL);
                    byte[] data = Encoding.ASCII.GetBytes(json);

                    WebRequest requestFEL = WebRequest.Create(string.Format("{0}/Fel/", mUrlRestServices));

                    requestFEL.Method = "POST";
                    requestFEL.ContentType = "application/json";
                    requestFEL.ContentLength = data.Length;

                    using (var stream = requestFEL.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }

                    var responseFEL = (HttpWebResponse)requestFEL.GetResponse();
                    var responseStringFEL = new StreamReader(responseFEL.GetResponseStream()).ReadToEnd();

                    MF_Clases.GuateFacturas.FEL.Resultado mResultado = new MF_Clases.GuateFacturas.FEL.Resultado();
                    mResultado = JsonConvert.DeserializeObject<MF_Clases.GuateFacturas.FEL.Resultado>(responseStringFEL);

                    //De no existir el NIT asignamos CF e intentamos de nuevo
                    if (mResultado.Error.Contains("NO EXISTE EL NIT"))
                    {
                        mFEL.NITReceptor = "CF";

                        string json2 = JsonConvert.SerializeObject(mFEL);
                        byte[] data2 = Encoding.ASCII.GetBytes(json2);

                        WebRequest requestFEL2 = WebRequest.Create(string.Format("{0}/Fel/", mUrlRestServices));

                        requestFEL2.Method = "POST";
                        requestFEL2.ContentType = "application/json";
                        requestFEL2.ContentLength = data2.Length;

                        using (var stream = requestFEL2.GetRequestStream())
                        {
                            stream.Write(data2, 0, data2.Length);
                        }

                        var responseFEL2 = (HttpWebResponse)requestFEL2.GetResponse();
                        var responseStringFEL2 = new StreamReader(responseFEL2.GetResponseStream()).ReadToEnd();

                        mResultado = JsonConvert.DeserializeObject<MF_Clases.GuateFacturas.FEL.Resultado>(responseStringFEL2);
                    }

                    try
                    {
                        mFirma = mResultado.NumeroAutorizacion;
                        if (mFirma.Trim().Length == 0) mFirma = "";
                    }
                    catch
                    {
                        mFirma = "";
                    }

                    if (mFirma.Trim().Length == 0) return string.Format("Se lograron firmar {0} documentos, error en {1} - {2}", mFirmados, item.DOCUMENTO, mResultado.Error);

                    EXACTUSERPEntities db1 = new EXACTUSERPEntities();

                    if ((from g in db.MF_GFaceFelCertifica where g.DOCUMENTO == item.DOCUMENTO select g).Count() == 0)
                    {
                        mFirmados++;
                        using (TransactionScope transactionScope = new TransactionScope())
                        {
                            MF_GFaceFelCertifica iGface = new MF_GFaceFelCertifica
                            {
                                DOCUMENTO = item.DOCUMENTO,
                                FECHA = mFecha,
                                COBRADOR = item.COBRADOR,
                                TIPO = item.TIPO,
                                SERIE = item.SERIE,
                                NUMERO_DOCUMENTO = item.NUMERO_DOCUMENTO,
                                SUBTOTAL = mFEL.Neto,
                                IVA = mFEL.Iva,
                                TOTAL = mFEL.Total,
                                BIEN_SERVICIO = mBienServicio,
                                XML_DOCUMENTO = mResultado.XmlDocumento,
                                XML_FIRMADO = mResultado.Xml,
                                XML_ERROR = mResultado.XmlError,
                                FIRMADO = "S",
                                FIRMA = mFirma,
                                FECHA_CREADO = DateTime.Now
                            };

                            db1.MF_GFaceFelCertifica.AddObject(iGface);

                            var qConsecutivo = from c in db1.CONSECUTIVO_FA where c.CODIGO_CONSECUTIVO == "FEL" select c;
                            foreach (var cons in qConsecutivo)
                            {
                                cons.VALOR_CONSECUTIVO = mReferencia.ToString();
                            }

                            db1.SaveChanges();
                            transactionScope.Complete();
                        }
                    }
                    else
                    {
                        mYaFirmados++;
                    }

                }

                mRetorna = string.Format("Se firmaron {0} documentos{1}", mFirmados, mYaFirmados == 0 ? "" : string.Format(" y {0} ya estaban firmados", mYaFirmados));
            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0} {1} {2} {3}", CatchClass.ExMessage(ex, "FirmarDocumentosBatchController", "GET"), mMensaje, mDocumento, mNit);
            }

            return mRetorna;
        }

    }
}
