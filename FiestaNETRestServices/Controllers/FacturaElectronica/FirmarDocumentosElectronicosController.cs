﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Utils;
using MF_Clases;
using MF_Clases.GuateFacturas.FEL;
using MF_Clases.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.FacturaElectronica
{
    public class FirmarDocumentosElectronicosController : MFApiController
    {
        [HttpGet]
        public IHttpActionResult Get()
        {
            Respuesta res = new Respuesta();
            try
            {
                //obtener todos los documentos que "no" tengan firma electrónica
                log.Info("\n\r Obteniendo los documentos electrónicos que no tengan firma (NO FACTURAS)-*-*-*--FEL----*-*-*-\n\r");

                DAL.DALFel fel = new DAL.DALFel();
                res = fel.FirmarOtrosDocumentosFEL();
                if (res.Exito)
                    return Ok(res);
                else
                    return BadRequest(res.Mensaje);
                
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            
        }
    }
}
