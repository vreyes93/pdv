﻿using FiestaNETRestServices.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.FacturaElectronica
{
    public class LoteContingenciaController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public async System.Threading.Tasks.Task<IHttpActionResult> Get()
        {
            DAL.DALFel fel = new DAL.DALFel();
            try
            {
                log.Info("Iniciando proceso para obtener los lotes de contingencia para FEL");

                var respp = await fel.ValidarLotesContingencia();
                if (respp.Exito)
                    return Ok<string>(respp.Mensaje);
                else
                    return BadRequest(respp.Mensaje);
            } catch (Exception ex)
            {
                log.Error("Problema al obtener los lotes de contingencia desde el GFFACE "+ex.Message+" "+ex.InnerException);
                return BadRequest(ex.Message);
            }
        }
    }
}
