﻿using FiestaNETRestServices.Content.Abstract;
using MF_Clases;
using MF_Clases.GuateFacturas.FEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.FacturaElectronica
{
    public class NotificarNumerosContingenciaUsadosController : MFApiController
    {
        [HttpGet]
        public Respuesta Get()
        {
            Respuesta mRespuesta = new Respuesta();
            DAL.DALFel fel = new DAL.DALFel();
            try
            {
                var res = fel.ObtenerNumerosDeContingenciaParaNotificar();
                if (res.Exito)
                {

                    return res;
                }
                else
                    mRespuesta.Exito = false;
            }
            catch (Exception ex)
            {
                log.Error(CatchClass.ExMessage(ex,"NotificarNumerosContingenciaUsadosController","Get"));
            }
            return mRespuesta;
        }

    }
}
