﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.FacturaElectronica
{
    public class NumeroContingenciaController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [HttpGet]
        [Route("~NumeroContingencia/{id}")]
        public IHttpActionResult Get(string id)
        {
            DAL.DALFel fel = new DAL.DALFel();
            var respContingencia = fel.UtilizarNumeroContingenciaTienda(id, DateTime.Now);
            if (respContingencia.Exito)
                return Ok(respContingencia.Objeto);
            else
                return BadRequest(respContingencia.Mensaje);
        }
    }
}
