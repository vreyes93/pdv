﻿using MF_Clases;
using MF_Clases.GuateFacturas.FEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.FacturaElectronica
{
    [RoutePrefix("~/ValidarNotasCreditoyDebito")]
    public class ValidarNotasCreditoyDebitoController : ApiController
    {
        /// <summary>
        /// servicio que servirá para validar las notas de crédito y sus valores, previo a enviarlos a firmar
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("NoFirmadas")]
        [Route("~/NoFirmadas")]
        public Respuesta NoFirmadas()
        {
            List<Doc> lstDocumentos = new List<Doc>();
            Respuesta mRespuesta = new Respuesta();
                DAL.DALFel fel = new DAL.DALFel();
            //obtiene notas de crédito y débito sin firmar
            mRespuesta = fel.ObtenerOtrosDocumentosSinFirmar();
                
            
            return mRespuesta;
        }

        [HttpGet]
        [ActionName("Firmadas")]
        [Route("~/Firmadas/{DateIni}/{DateFin}")]
        public IHttpActionResult Firmadas( string DateIni,string DateFin)
        {
            //tests: http://localhost:53874/api/ValidarNotasCreditoyDebito/Firmadas?DateIni=%22a%22&DateFin=%22b%22

            //id=FIRMADAS o NOFIRMADAS
            return Ok();
        }
    }
}
