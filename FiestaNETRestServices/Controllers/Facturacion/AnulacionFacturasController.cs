﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;
using Newtonsoft.Json;
using FiestaNETRestServices.Controllers.Tools;

namespace FiestaNETRestServices.Controllers.Facturacion
{
    public class AnulacionFacturasController : MFApiController
    {
        // GET: api/AnulacionFacturas
        public string Get()
        {
            return "Debe enviar la tienda o la factura";
        }
        
        // GET: api/AnulacionFacturas/5
        public FacturasAnular Get(string id)
        {
            string mTienda = id.ToUpper(); ;
            db.CommandTimeout = 999999999;
            
            FacturasAnular mRetorna = new FacturasAnular();
            UtilitariosFacturacion UtilitarioFacturacion = new UtilitariosFacturacion();

            DateTime mFecha = DateTime.Now.Date.AddMonths(-1);
            mFecha = new DateTime(mFecha.Year, mFecha.Month, 1);
            DateTime mFechaCC = mFecha.AddMonths(-6);

            var qFacturas = from f in db.FACTURA where f.ANULADA == "N" && f.TIPO_DOCUMENTO == "F" orderby f.FACTURA1 select f;

            if (mTienda.Trim().Length > 4)
            {
                qFacturas = qFacturas.Where(x => x.FACTURA1 == mTienda).OrderBy(x => x.FACTURA1);
            }
            else
            {
                qFacturas = qFacturas.Where(x => x.COBRADOR == mTienda && x.FECHA >= mFecha).OrderBy(x => x.FACTURA1);
            }

            if (qFacturas.Count() == 0)
                return mRetorna;

            foreach (var item in qFacturas)
            {
                string fact = item.FACTURA1;
               string articulos=string.Empty;
                FacturaAnular mFactura = new FacturaAnular();
                mFactura.Tienda = item.COBRADOR;
                mFactura.Factura = item.FACTURA1;
                mFactura.Fecha = item.FECHA;
                mFactura.Cliente = item.CLIENTE;
                mFactura.Nombre = (from c in db.CLIENTE where c.CLIENTE1 == item.CLIENTE select c).First().NOMBRE;
                mFactura.Monto = item.TOTAL_FACTURA;
                mFactura.Recibos = UtilitarioFacturacion.RecibosFactura(item.FACTURA1);
                mFactura.Despachos = UtilitarioFacturacion.DespachosFactura(item.FACTURA1);
                mFactura.Observaciones = "";
                mFactura.Usuario = "";
                mRetorna.Anular.Add(mFactura);

                //CC
                if (mRetorna.CC.Where(x => x.Factura == fact).Count() == 0)
                {
                    List<DespachosAnular> mCC = new List<DespachosAnular>();
                     mCC =db.MF_Despacho.Join(db.DESPACHO, d=>d.DESPACHO,des=>des.DESPACHO1,(d,des)=> new { d,des})
                        .Where(x => x.d.FACTURA == item.FACTURA1 && x.des.ESTADO=="D").Select( x=> new DespachosAnular{ 
                            Despacho=x.d.DESPACHO,
                            Factura=x.d.FACTURA,
                            DeTienda=x.d.DE_TIENDA,
                            Direccion=x.d.DIRECCION,
                            Fecha_Despacho=x.d.FECHA_DESPACHO,
                            Fecha_entrega=x.d.FECHA_ENTREGA,
                            Usuario=x.d.CreatedBy
                            }
                     ).ToList();
                    mCC.ForEach(d =>
                        {
                            articulos="";
                            var qDesp=db.DESPACHO.Join(db.DESPACHO_DETALLE, des => des.DESPACHO1,dd => dd.DESPACHO, (des,dd)=> new { des,dd})
                            .Join(db.ARTICULO, nw => nw.dd.ARTICULO,ar => ar.ARTICULO1, (nw,ar)=> new { nw,ar})
                               .Where(x => x.nw.dd.DESPACHO == d.Despacho && x.nw.des.ESTADO=="D").Select(y => new { articulo =y.ar.ARTICULO1,descripcion=y.ar.DESCRIPCION }).ToList();
                            
                            qDesp.ForEach(art =>
                            {
                            articulos+=""+art.articulo+"- "+art.descripcion+"<br/>";
                            });
                            
                        d.Articulos=articulos;
                        });
                    mRetorna.CC.AddRange(mCC); 
                    
                }

            }

            return mRetorna;
        }

        // POST: api/AnulacionFacturas
        public string Post([FromBody]FacturaAnular factura)
        {
            string mRetorna = "";
            List<Int32> mAuditTransINV = new List<Int32>();
            Correo correo = new Correo();
            try
            {
                string mInfoDespachos = ""; string mInfoRecibos = "";
                string mFactura = factura.Factura;
                string mUsuario = factura.Usuario;
                string mObservaciones = factura.Observaciones;
                if(factura.SoloDespachos)
                     mFactura= db.MF_Despacho.Where(x => x.DESPACHO== factura.Despachos).Select(x => x.FACTURA).FirstOrDefault();

                var qFactura = from f in db.FACTURA where f.FACTURA1 == mFactura select f;

                if (qFactura.Count() == 0)
                    return string.Format("Error, la factura {0} no existe", mFactura);
                if (qFactura.First().ANULADA == "S")
                    return string.Format("Error, la factura {0} ya está anulada", mFactura);
                DateTime dtFactura = qFactura.FirstOrDefault().FECHA;
                
                factura.Fecha = dtFactura;
                
                if ( DateTime.Today.Month != dtFactura.Month)
                        {
                        if (DateTime.Today.Day > int.Parse(WebConfigurationManager.AppSettings["Dias_Anulacion_Fact_Fel"].ToString()) && DateTime.Today.Month - dtFactura.Month >= 1)
                            return string.Format("Error, la factura {0} no se puede anular porque no es del mismo mes y ya no está en los primeros días del mes para poderla anular",mFactura);
                        }
                EXACTUSERPEntities db2 = new EXACTUSERPEntities();

                bool mSeAnularonDespachos = false;
                

                
                    

                    //    db2.SaveChanges();
                    //    mAuditTransINV.Add(iAudit_Trans_INV.AUDIT_TRANS_INV1);
                    //}
                

                if (mObservaciones.Trim().Length == 0)
                    mObservaciones = "Factura no procede.";
                
                string mAmbiente = WebConfigurationManager.AppSettings["Ambiente"];
                string mUrlRestServices = "http://localhost:53874/api";

                if (mAmbiente == "PRO") mUrlRestServices = "http://sql.fiesta.local/RestServices/api";
                if (mAmbiente == "PRU") mUrlRestServices = "http://sql.fiesta.local/RestServicesPruebas/api";

                string mXMLAnulacion = "";
                string mHabilitarFEL = (from c in db.MF_Catalogo where c.CODIGO_TABLA == 11 && c.CODIGO_CAT == "HABILITAR_FEL" select c).First().NOMBRE_CAT;
                int mCuantas = (from g in db.MF_GFace where g.DOCUMENTO == mFactura select g).Count();

                if (mHabilitarFEL == "S" && !factura.SoloDespachos && mCuantas > 0 && mFactura.Trim().Length > 15)
                {
                    MF_Clases.GuateFacturas.FEL.AnulacionDoc mFEL = new MF_Clases.GuateFacturas.FEL.AnulacionDoc();
                    string mXmlFirmado = (from g in db.MF_GFace where g.DOCUMENTO == mFactura select g).First().XML_FIRMADO;
                    string mXmlDocumento = (from g in db.MF_GFace where g.DOCUMENTO == mFactura select g).First().XML_DOCUMENTO;
                    mXmlFirmado = mXmlFirmado.Replace("&iacute;", "í").Replace("&ntilde;a", "a");
                    mXmlDocumento=mXmlDocumento.Replace("&iacute;", "í").Replace("&ntilde;a", "a");
                    XmlDocument xmlFirmado = new XmlDocument();
                    xmlFirmado.PreserveWhitespace = true;

                    XmlDocument xmlDocumento = new XmlDocument();
                    xmlDocumento.PreserveWhitespace = true;
                    
                    xmlFirmado.LoadXml(mXmlFirmado);
                    xmlDocumento.LoadXml(mXmlDocumento);

                    mFEL.NITEmisor = "5440998";
                    mFEL.Serie = xmlFirmado.SelectNodes("Resultado/Serie").Item(0).InnerText;
                    mFEL.Preimpreso = xmlFirmado.SelectNodes("Resultado/Preimpreso").Item(0).InnerText;
                    mFEL.NitComprador = xmlDocumento.SelectNodes("DocElectronico/Encabezado/Receptor/NITReceptor").Item(0).InnerText;
                    mFEL.FechaAnulacion = qFactura.First().FECHA.Date;
                    mFEL.MotivoAnulacion = mObservaciones.Trim();

                    string json = JsonConvert.SerializeObject(mFEL);
                    byte[] data = Encoding.ASCII.GetBytes(json);

                    WebRequest requestFEL = WebRequest.Create(string.Format("{0}/FelAnulacion/", mUrlRestServices));

                    requestFEL.Method = "POST";
                    requestFEL.ContentType = "application/json";
                    requestFEL.ContentLength = data.Length;

                    using (var stream = requestFEL.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }

                    var responseFEL = (HttpWebResponse)requestFEL.GetResponse();
                    var responseStringFEL = new StreamReader(responseFEL.GetResponseStream()).ReadToEnd();

                    MF_Clases.GuateFacturas.FEL.Resultado mResultado = new MF_Clases.GuateFacturas.FEL.Resultado();
                    mResultado = JsonConvert.DeserializeObject<MF_Clases.GuateFacturas.FEL.Resultado>(responseStringFEL);

                    XmlDocument xmlError = new XmlDocument();
                    xmlError.PreserveWhitespace = true;

                    xmlError.LoadXml(mResultado.Xml);

                    try
                    {
                        string mError = xmlError.SelectNodes("RESULTADO/ERROR").Item(0).InnerText;
                        if (!mError.Contains("DOCUMENTO ANULADO PREVIAMENTE")) //para que continue con la anulación, pudo haber un problema de comunicación en el cual la factura se anulo en guatefacturas, pero no aqui, por lo que debe proseguir anulando de nuestro lado
                        {
                            if (mError.Trim().Length > 0)
                            {
                                log.Error(string.Format("Error, no se pudo generar anulación electrónica {0}", mError));

                                return string.Format("Error, no se pudo generar anulación electrónica {0}", mError);
                            }
                        }
                    }
                    catch
                    {
                        //Nothing
                    }

                    mXMLAnulacion = mResultado.Xml;
                }

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    //Póliza de reversión si la factura ya fue cargada a contabilidad.
                    string mAsiento = "";
                    string mCargadaContabilidad = qFactura.First().CARGADO_CG;

                    if (!factura.SoloDespachos)
                    {
                        if (mCargadaContabilidad == "S")
                        {
                            string mAsientoFactura = qFactura.First().ASIENTO_DOCUMENTO;
                            string mFuente = string.Format("FAC#{0}", mFactura);
                            var qAsientoFactura = from a in db.DIARIO where a.ASIENTO == mAsientoFactura && a.FUENTE == mFuente select a;

                            DateTime mFechaTransaccion = qFactura.First().FECHA.Date;
                            DateTime mFechaFactura = qFactura.First().FECHA.Date;

                            if (mFechaFactura.Month != DateTime.Now.Date.Month)
                            {
                                DateTime mFechaTemp = new DateTime(DateTime.Now.Date.Year, DateTime.Now.Month, 1);
                                mFechaTransaccion = mFechaTemp.AddDays(-1);
                            }

                            decimal mTotalDebitoLocal = Convert.ToDecimal(qAsientoFactura.Sum(x => x.CREDITO_LOCAL));
                            decimal mTotalDebitoDolar = Convert.ToDecimal(qAsientoFactura.Sum(x => x.CREDITO_DOLAR));
                            decimal mTotalCreditoLocal = Convert.ToDecimal(qAsientoFactura.Sum(x => x.DEBITO_LOCAL));
                            decimal mTotalCreditoDolar = Convert.ToDecimal(qAsientoFactura.Sum(x => x.DEBITO_DOLAR));

                            string mCeros = "";
                            Int32 mAsientoInt = Convert.ToInt32((from p in db.PAQUETE where p.PAQUETE1 == "FA" select p).First().ULTIMO_ASIENTO.Replace("FA", "")) + 1;

                            switch (mAsientoInt.ToString().Length)
                            {
                                case 1:
                                    mCeros = "00000";
                                    break;
                                case 2:
                                    mCeros = "0000";
                                    break;
                                case 3:
                                    mCeros = "000";
                                    break;
                                case 4:
                                    mCeros = "00";
                                    break;
                                case 5:
                                    mCeros = "0";
                                    break;
                                default:
                                    mCeros = "";
                                    break;
                            }

                            mAsiento = string.Format("FA{0}{1}", mCeros, mAsientoInt.ToString());
                            ASIENTO_DE_DIARIO iAsientoDiario = new ASIENTO_DE_DIARIO
                            {
                                ASIENTO = mAsiento,
                                PAQUETE = "FA",
                                TIPO_ASIENTO = "FAC",
                                FECHA = mFechaTransaccion,
                                CONTABILIDAD = "A",
                                ORIGEN = "FA",
                                CLASE_ASIENTO = "O",
                                TOTAL_DEBITO_LOC = mTotalDebitoLocal,
                                TOTAL_DEBITO_DOL = mTotalDebitoDolar,
                                TOTAL_CREDITO_LOC = mTotalCreditoLocal,
                                TOTAL_CREDITO_DOL = mTotalCreditoDolar,
                                ULTIMO_USUARIO = mUsuario,
                                FECHA_ULT_MODIF = DateTime.Now.Date,
                                MARCADO = "N",
                                NOTAS = string.Format("Póliza de reversión aplicado a la póliza {0} y la referencia FAC#{1}", mAsientoFactura, mFactura),
                                TOTAL_CONTROL_LOC = mTotalDebitoLocal,
                                TOTAL_CONTROL_DOL = mTotalDebitoDolar,
                                USUARIO_CREACION = mUsuario,
                                FECHA_CREACION = DateTime.Now.Date,
                                NoteExistsFlag = 0,
                                RecordDate = DateTime.Now,
                                RowPointer = Guid.NewGuid(),
                                CreatedBy = string.Format("FA/{0}", mUsuario),
                                UpdatedBy = string.Format("FA/{0}", mUsuario),
                                CreateDate = DateTime.Now
                            };

                            db.ASIENTO_DE_DIARIO.AddObject(iAsientoDiario);

                            int mConsecutivo = 0;
                            foreach (var item in qAsientoFactura)
                            {
                                mConsecutivo++;
                                DIARIO iDiario = new DIARIO
                                {
                                    ASIENTO = mAsiento,
                                    CONSECUTIVO = mConsecutivo,
                                    NIT = "ND",
                                    CENTRO_COSTO = item.CENTRO_COSTO,
                                    CUENTA_CONTABLE = item.CUENTA_CONTABLE,
                                    FUENTE = item.FUENTE,
                                    REFERENCIA = item.REFERENCIA,
                                    DEBITO_LOCAL = item.CREDITO_LOCAL,
                                    DEBITO_DOLAR = item.CREDITO_DOLAR,
                                    CREDITO_LOCAL = item.DEBITO_LOCAL,
                                    CREDITO_DOLAR = item.DEBITO_DOLAR,
                                    NoteExistsFlag = 0,
                                    RecordDate = DateTime.Now,
                                    CreatedBy = string.Format("FA/{0}", mUsuario),
                                    UpdatedBy = string.Format("FA/{0}", mUsuario),
                                    CreateDate = DateTime.Now,
                                    RowPointer = Guid.NewGuid()
                                };

                                db.DIARIO.AddObject(iDiario);
                            }

                            var qUpdatePaqueteRec = from p in db.PAQUETE where p.PAQUETE1 == "FA" select p;
                            foreach (var itemPAQ in qUpdatePaqueteRec)
                            {
                                itemPAQ.ULTIMO_ASIENTO = mAsiento;
                            }

                        }

                        var qMFGface = from g in db.MF_GFace where g.DOCUMENTO == mFactura select g;
                        foreach (var item in qMFGface)
                        {
                            item.ANULADA = "S";
                            item.XML_ERROR = mXMLAnulacion;
                            item.USUARIO_MODIFICA = mUsuario;
                            item.FECHA_HORA_MODIFICA = DateTime.Now;
                        }

                        var qCC = from d in db.DOCUMENTOS_CC where d.DOCUMENTO == mFactura && d.TIPO == "FAC" select d;

                        decimal mMonto = qCC.First().MONTO;
                        decimal mMontoDolar = qCC.First().MONTO_DOLAR;

                        foreach (var item in qCC)
                        {
                            item.APLICACION = "** ANULADO **";
                            item.MONTO = 0;
                            item.SALDO = 0;
                            item.MONTO_LOCAL = 0;
                            item.SALDO_LOCAL = 0;
                            item.MONTO_DOLAR = 0;
                            item.SALDO_DOLAR = 0;
                            item.MONTO_CLIENTE = 0;
                            item.SALDO_CLIENTE = 0;
                            item.SUBTOTAL = 0;
                            item.IMPUESTO1 = 0;
                            item.NOTAS = string.Format("Aplicación: Cargado de factura: {0}", factura);
                            item.USUARIO_ULT_MOD = mUsuario;
                            item.AUD_USUARIO_ANUL = mUsuario;
                            item.AUD_FECHA_ANUL = DateTime.Now;
                            item.ANULADO = "S";

                            if (mCargadaContabilidad == "S") item.ASIENTO = mAsiento;
                        }


                        string mPedido = qFactura.First().PEDIDO;
                        string mNombreUsuario = (from u in db.USUARIO where u.USUARIO1 == mUsuario select u).First().NOMBRE;

                        foreach (var item in qFactura)
                        {
                            item.FECHA_HORA_ANULA = DateTime.Now;
                            item.ANULADA = "S";
                            item.MULTIPLICADOR_EV = 0;
                            item.PEDIDO = Convert.ToString(null);
                            item.OBSERVACIONES = string.Format("{0} Anuló {1}", mObservaciones.Trim(), mNombreUsuario);
                            item.USUARIO_ANULA = mUsuario;

                            if (mCargadaContabilidad == "S") item.ASIENTO_DOCUMENTO = mAsiento;
                        }

                        var qUpdateFL = from f in db.FACTURA_LINEA where f.FACTURA == mFactura select f;
                        foreach (var item in qUpdateFL)
                        {
                            item.PEDIDO = Convert.ToString(null);
                            item.ANULADA = "S";
                            item.PEDIDO_LINEA = Convert.ToInt16(null);
                            item.MULTIPLICADOR_EV = 0;
                        }

                        var qUpdateP = from p in db.PEDIDO where p.PEDIDO1 == mPedido select p;
                        foreach (var item in qUpdateP)
                        {
                            item.ESTADO = "N";
                        }

                        var qUpdatePL = from p in db.PEDIDO_LINEA where p.PEDIDO == mPedido select p;
                        foreach (var item in qUpdatePL)
                        {
                            item.ESTADO = "N";
                            item.CANTIDAD_A_FACTURA = item.CANTIDAD_PEDIDA;
                            item.CANTIDAD_FACTURADA = 0;
                        }

                        var qUpdateMFP = from p in db.MF_Pedido where p.PEDIDO == mPedido select p;
                        foreach (var item in qUpdateMFP)
                        {
                            item.FACTURA = Convert.ToString(null);
                        }

                        var qUpdateMFPL = from p in db.MF_Pedido_Linea where p.PEDIDO == mPedido select p;
                        foreach (var item in qUpdateMFPL)
                        {
                            item.FACTURA = Convert.ToString(null);
                            item.FACTURADO = "N";
                        }

                        string mCliente = qFactura.First().CLIENTE;

                        var qUpdateC = from c in db.CLIENTE where c.CLIENTE1 == mCliente select c;

                        foreach (var item in qUpdateC)
                        {
                            decimal mSaldoDolar = item.SALDO_DOLAR - mMontoDolar;
                            if (mSaldoDolar < 0) mSaldoDolar = 0;

                            item.SALDO = item.SALDO - mMonto;
                            item.SALDO_LOCAL = item.SALDO_LOCAL - mMonto;
                            item.SALDO_DOLAR = mSaldoDolar;
                        }

                        var qUpdateSC = from c in db.SALDO_CLIENTE where c.CLIENTE == mCliente select c;
                        foreach (var item in qUpdateSC)
                        {
                            item.SALDO = item.SALDO - mMonto;
                            item.RecordDate = DateTime.Now;
                        }

                        var qVale = from v in db.MF_Vale where v.FACTURA == mFactura select v;
                        foreach (var item in qVale)
                        {
                            item.FACTURADO = "N";
                            item.FACTURA = Convert.ToString(null);
                            item.FECHA_FACTURA = new DateTime(1980, 1, 1);
                        }

                        var qRecibo = from r in db.MF_Recibo where r.FACTURA == mFactura select r;
                        foreach (var item in qRecibo)
                        {
                            item.FACTURA = "";
                        }

                        //cambio s en asociación de recibos pr-T1447
                        var qFacturaRecibo = db.MF_FacturaRecibo.Where(x => x.FACTURA.Equals(mFactura));
                        foreach (var item in qFacturaRecibo)
                        {
                            db.MF_FacturaRecibo.DeleteObject(item);
                        }
                        //fin cambio  en asociación de recibos pr-T1447


                        //Aumentar el saldo del recibo el monto de la factura que se está anulando
                        var qRecibos = from d in db.AUXILIAR_CC join c in db.DOCUMENTOS_CC on d.DEBITO equals c.DOCUMENTO where d.DEBITO == mFactura && c.ANULADO == "N" orderby d.CREDITO select d;
                        foreach (var item in qRecibos)
                        {
                            string mRecibo = item.CREDITO;
                            var qRec = from d in db.DOCUMENTOS_CC where d.DOCUMENTO == mRecibo select d;

                            foreach (var rec in qRec)
                            {
                                rec.SALDO = rec.SALDO + item.MONTO_CREDITO;
                                rec.SALDO_LOCAL = rec.SALDO_LOCAL + item.MONTO_CREDITO;
                                rec.SALDO_DOLAR = rec.SALDO_DOLAR + Math.Round(item.MONTO_CREDITO / rec.TIPO_CAMBIO_DOLAR, 2, MidpointRounding.AwayFromZero);
                                rec.SALDO_CLIENTE = rec.SALDO_CLIENTE + item.MONTO_CREDITO;
                            }

                            mInfoRecibos = " Recibo(s) desaplicado(s) exitosamente.";
                        }

                        List<AUXILIAR_CC> mAuxiliarCC = db.AUXILIAR_CC.Where(x => x.DEBITO == mFactura).ToList();
                        foreach (AUXILIAR_CC itemBorrar in mAuxiliarCC) db.AUXILIAR_CC.DeleteObject(itemBorrar);

                        List<MF_PedidoArmadoLinea> mPedidoArmadoLinea = db.MF_PedidoArmadoLinea.Where(x => x.PEDIDO == mPedido).ToList();
                        foreach (MF_PedidoArmadoLinea itemBorrar in mPedidoArmadoLinea) db.MF_PedidoArmadoLinea.DeleteObject(itemBorrar);

                        List<MF_PedidoArmado> mPedidoArmado = db.MF_PedidoArmado.Where(x => x.PEDIDO == mPedido).ToList();
                        foreach (MF_PedidoArmado itemBorrar in mPedidoArmado) db.MF_PedidoArmado.DeleteObject(itemBorrar);

                        List<MF_FacturaAutorizaDespacho> mFacturaAutorizaDespachos = db.MF_FacturaAutorizaDespacho.Where(x => x.FACTURA == mFactura).ToList();
                        foreach (MF_FacturaAutorizaDespacho itemBorrar in mFacturaAutorizaDespachos) db.MF_FacturaAutorizaDespacho.DeleteObject(itemBorrar);
                    }


                    //Anulación del despachos
                    int jj = 0;
                   
                        var qDespachos= factura.SoloDespachos ? (from s in db.MF_Despacho where s.DESPACHO == factura.Despachos select s): ( from s in db.MF_Despacho where s.FACTURA == mFactura  select s);

                    foreach(var item in qDespachos)
                        {
                      bool   mAnularDespacho = false;
                       string mDespacho=item.DESPACHO;
                        
                        if ((from d in db.DESPACHO where d.DESPACHO1 == item.DESPACHO select d).First().ESTADO == "D")
                            mAnularDespacho = true;

                        if (mAnularDespacho)
                        {
                            mSeAnularonDespachos = true;
                           string mAplicacion = string.Format("DESP#{0}", mDespacho);
                            Int32 mAudit = (from a in db.AUDIT_TRANS_INV where a.APLICACION == mAplicacion && !a.REFERENCIA.Contains("Rever") select a).First().AUDIT_TRANS_INV1;
                            
                            
                        AUDIT_TRANS_INV iAudit_Trans_INV = new AUDIT_TRANS_INV
                        {
                            USUARIO = mUsuario,
                            FECHA_HORA = DateTime.Now,
                            MODULO_ORIGEN = "FA",
                            APLICACION = mAplicacion,
                            REFERENCIA = string.Format("Reversión por anulación de despacho : {0}", mDespacho),
                            NoteExistsFlag = 0,
                            RowPointer = Guid.NewGuid(),
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("FA/{0}", mUsuario),
                            UpdatedBy = string.Format("FA/{0}", mUsuario),
                            CreateDate = DateTime.Now
                        };
                        db2.AUDIT_TRANS_INV.AddObject(iAudit_Trans_INV);

                        db2.SaveChanges();
                        mAuditTransINV.Add(iAudit_Trans_INV.AUDIT_TRANS_INV1);

                            var qDespacho = from d in db.DESPACHO where d.DESPACHO1 == mDespacho select d;
                            foreach (var des in qDespacho)
                            {
                                des.ESTADO = "A";
                                des.FCH_HORA_ANULACION = DateTime.Now;
                                des.USUARIO_ANULACION = mUsuario;
                            }

                            foreach (var fac in qFactura)
                            {
                                fac.ESTA_DESPACHADO = "N";
                            }

                            var qDespachoDetalle = from d in db.DESPACHO_DETALLE where d.DESPACHO==mDespacho select d;
                            foreach (var linea in qDespachoDetalle)
                            {
                                var qUpdateFL = from f in db.FACTURA_LINEA where f.FACTURA == mFactura && f.LINEA == linea.LINEA_DOCUM_ORIG  select f;
                                foreach (var fac in qUpdateFL)
                                {
                                    fac.CANT_DESPACHADA = fac.CANT_DESPACHADA - linea.CANTIDAD;
                                }
                            }

                            DateTime mFechaDespacho = qDespacho.First().FECHA;
                            List<ExistenciaBodega> mExistenciaBodega = new List<ExistenciaBodega>();
                            var qInventario = from i in db.TRANSACCION_INV where i.AUDIT_TRANS_INV == mAudit orderby i.CONSECUTIVO select i;

                            foreach (var inv in qInventario)
                            {
                                var qArticulo = (from a in db.ARTICULO where a.ARTICULO1 == inv.ARTICULO select a).First();
                                string mTipo = qArticulo.TIPO;

                                TRANSACCION_INV iTransaccionINV = new TRANSACCION_INV
                                {
                                    AUDIT_TRANS_INV = mAuditTransINV[jj],
                                    CONSECUTIVO = inv.CONSECUTIVO,
                                    FECHA_HORA_TRANSAC = DateTime.Now,
                                    NIT = inv.NIT,
                                    AJUSTE_CONFIG = mTipo == "K" ? "~EE~" : mTipo == "C" ? "~CC~" : "~VV~",
                                    ARTICULO = inv.ARTICULO,
                                    BODEGA = inv.BODEGA,
                                    LOCALIZACION = inv.LOCALIZACION,
                                    TIPO = inv.TIPO,
                                    SUBTIPO = inv.SUBTIPO,
                                    SUBSUBTIPO = inv.SUBSUBTIPO,
                                    NATURALEZA = "E",
                                    CANTIDAD = inv.CANTIDAD * (-1),
                                    COSTO_TOT_FISC_LOC = inv.COSTO_TOT_FISC_LOC,
                                    COSTO_TOT_FISC_DOL = inv.COSTO_TOT_FISC_DOL,
                                    COSTO_TOT_COMP_LOC = inv.COSTO_TOT_COMP_LOC,
                                    COSTO_TOT_COMP_DOL = inv.COSTO_TOT_COMP_DOL,
                                    PRECIO_TOTAL_LOCAL = inv.PRECIO_TOTAL_LOCAL,
                                    PRECIO_TOTAL_DOLAR = inv.PRECIO_TOTAL_DOLAR,
                                    CONTABILIZADA = "N",
                                    FECHA = mFechaDespacho,
                                    NoteExistsFlag = 0,
                                    RowPointer = Guid.NewGuid(),
                                    RecordDate = DateTime.Now,
                                    CreatedBy = string.Format("FA/{0}", mUsuario),
                                    UpdatedBy = string.Format("FA/{0}", mUsuario),
                                    CreateDate = DateTime.Now
                                };

                                db.TRANSACCION_INV.AddObject(iTransaccionINV);

                                if (mTipo != "K")
                                {
                                    var qUpdateExistenciaLote = from e in db.EXISTENCIA_LOTE where e.BODEGA == inv.BODEGA && e.ARTICULO == inv.ARTICULO && e.LOCALIZACION == inv.LOCALIZACION && e.LOTE == "ND" select e;
                                    foreach (var lote in qUpdateExistenciaLote)
                                    {
                                        lote.CANT_DISPONIBLE = lote.CANT_DISPONIBLE + inv.CANTIDAD;
                                        lote.RecordDate = DateTime.Now;
                                        lote.UpdatedBy = string.Format("FA/{0}", mUsuario);
                                    }

                                    var qExistenciaBodega = from e in mExistenciaBodega where e.Bodega == inv.BODEGA & e.Articulo == inv.ARTICULO select e;
                                    if (qExistenciaBodega.Count() == 0)
                                    {
                                        ExistenciaBodega mExistBodega = new ExistenciaBodega();
                                        mExistBodega.Bodega = inv.BODEGA;
                                        mExistBodega.Articulo = inv.ARTICULO;
                                        mExistBodega.Cantidad = Convert.ToInt32(inv.CANTIDAD);
                                        mExistenciaBodega.Add(mExistBodega);
                                    }
                                    else
                                    {
                                        foreach (var bodega in qExistenciaBodega)
                                        {
                                            bodega.Cantidad = bodega.Cantidad + Convert.ToInt32(inv.CANTIDAD);
                                        }
                                    }

                                }
                            }

                            var qUpdateExistenciaBodega = from e in mExistenciaBodega select e;
                            foreach (var bodega in qUpdateExistenciaBodega)
                            {
                                var qUpdateBodega = from e in db.EXISTENCIA_BODEGA where e.BODEGA == bodega.Bodega && e.ARTICULO == bodega.Articulo select e;
                                foreach (var bod in qUpdateBodega)
                                {
                                    bod.CANT_DISPONIBLE = bod.CANT_DISPONIBLE + bodega.Cantidad;
                                    bod.RecordDate = DateTime.Now;
                                    bod.UpdatedBy = string.Format("FA/{0}", mUsuario);
                                }
                            }

                            jj++;
                            mInfoDespachos = " Despacho anulado exitosamente.";
                        
                    }

                    }

                    
                    ///
                    db.SaveChanges();
                    transactionScope.Complete();

                }
                
                mRetorna = string.Format("La factura {0} fue anulada exitosamente.{1}{2}", factura.Factura, mInfoDespachos, mInfoRecibos);
                if (factura.SoloDespachos)
                {
                    mRetorna = "Despacho anulado exitosamente, factura y recibo(s) no fueron alterados";
                    if (!mSeAnularonDespachos) 
                        mRetorna = "Error, el o los despachos ya se encuentran anulados";
                }else
                {
                //notificación al cliente
                EmailController email = new EmailController();
                email.EnviarNotificacionAnulaciónFactura(factura);
                }
            }
            catch (Exception ex)
            {
                EXACTUSERPEntities db3 = new EXACTUSERPEntities();

                foreach (Int32 item in mAuditTransINV)
                {
                    List<AUDIT_TRANS_INV> mBorrar = db3.AUDIT_TRANS_INV.Where(x => x.AUDIT_TRANS_INV1 == item).ToList();
                    foreach (AUDIT_TRANS_INV itemBorrar in mBorrar) db3.AUDIT_TRANS_INV.DeleteObject(itemBorrar);
                }

                db3.SaveChanges();

                mRetorna = string.Format("{0}", CatchClass.ExMessage(ex, "AnulacionFacturasController", "Post"));
            }

            return mRetorna;
        }

      
    }

}
