﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using FiestaNETRestServices.Content.Abstract;
using Newtonsoft.Json;
using MF_Clases;
using System.Web.Configuration;
using FiestaNETRestServices.Utils;

namespace FiestaNETRestServices.Controllers.Facturacion
{
    public class AnulacionRecibosController : MFApiController
    {

        public string GetAnulacionRecibos(string recibo, string usuario, string accion)
        {
            if (recibo == null) return "Debe enviar el número de recibo.";
            if (usuario == null) return "Debe enviar el usuario.";
            if (accion == null) return "Debe enviar la acción a realizar A ó R.";

            if (recibo.Trim().Length == 0) return "Debe enviar el número de recibo.";
            if (usuario.Trim().Length == 0) return "Debe enviar el usuario.";
            if (accion.Trim().Length == 0) return "Debe enviar la acción a realizar A ó R.";

            if (accion != "A" && accion != "R") return "Solo se permiten las acciones A ó R";

            string mMensaje = "";
            AnulaRecibo(recibo, usuario, "", "", "", accion, ref mMensaje, "");
            return mMensaje;
        }

        bool AnulaRecibo(string recibo, string usuario, string observaciones, string vendedor, string cobrador, string accion, ref string mensaje, string ambiente)
        {
            bool mRetorna = true;
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            db.CommandTimeout = 999999999;
            
            try
            {
                recibo = recibo.ToUpper();
                
                if ((from r in db.MF_Recibo where r.DOCUMENTO == recibo select r).Count() == 0)
                {
                    mensaje = string.Format("El recibo {0} no existe", recibo);
                    return false;
                }

                var qRecibo = (from r in db.MF_Recibo where r.DOCUMENTO == recibo select r).First();
                var qFac = from f in db.MF_FacturaRecibo
                           join fr in db.FACTURA on f.FACTURA equals fr.FACTURA1
                           where f.RECIBO == recibo
                           select fr;

                DateTime mFechaRecibo = qRecibo.FECHA;
                string mEstadoRecibo = qRecibo.ESTADO_AUTORIZACION;

                if (qRecibo.ANULADO == "S")
                {
                    mensaje = string.Format("El recibo {0} ya está anulado", recibo);
                    if (observaciones == "")
                    {
                        string mUsuarioAccion = ""; DateTime mFechaMostrar = Convert.ToDateTime(qRecibo.FECHA_HORA_ANULACION);

                        try
                        {
                            mUsuarioAccion = (from u in db.USUARIO where u.USUARIO1 == qRecibo.USUARIO_AUTORIZA_RECHAZA select u).First().NOMBRE;
                            mFechaMostrar = Convert.ToDateTime(qRecibo.FECHA_HORA_AUTORIZA_RECHAZA);
                        }
                        catch
                        {
                            mUsuarioAccion = (from u in db.USUARIO where u.USUARIO1 == qRecibo.USUARIO_ANULA select u).First().NOMBRE;
                        }
                        
                        mensaje = string.Format("El recibo {0} fue anulado por {1} el {2} a las {3}", recibo, mUsuarioAccion, Utilitario.FormatoDDMMYYYY(mFechaMostrar), mFechaMostrar.ToShortTimeString());
                    }
                    return false;
                }
                
                if (qRecibo.TIENE_DEPOSITO == "S")
                {
                    mensaje = string.Format("El recibo {0} tiene depósito relacionado, no es posible continuar", recibo);
                    return false;
                }
                
                
                if (mEstadoRecibo == "R" && accion == "N")
                {
                    mensaje = string.Format("La anulación del recibo {0} fue rechazada, no es posible enviar una nueva solicitud", recibo);
                    return false;
                }
                
                string mNombreVendedor = (from r in db.MF_Recibo join v in db.VENDEDOR on r.VENDEDOR equals v.VENDEDOR1 where r.DOCUMENTO == recibo select v).First().NOMBRE;
                string mEmailVendedor = (from r in db.MF_Recibo join v in db.MF_Vendedor on r.VENDEDOR equals v.VENDEDOR where r.DOCUMENTO == recibo select v).First().EMAIL;

                bool mValidarRestriccion = false;
                if ((from g in db.MF_Gerente where g.USUARIO == usuario && g.ANULA_RECIBOS == "N" select g).Count() == 0)
                    mValidarRestriccion = true;

                if (observaciones == "")
                {
                    vendedor = qRecibo.VENDEDOR;
                    cobrador = qRecibo.COBRADOR;

                    if ((from u in db.USUARIO where u.USUARIO1 == usuario select u).Count() == 0)
                    {
                        mensaje = string.Format("El usuario {0} no existe.", usuario);
                        return false;
                    }

                    if ((from g in db.MF_Gerente where g.ANULA_RECIBOS == "S" && g.USUARIO == usuario select g).Count() == 0)
                    {
                        mensaje = string.Format("El usuario {0} no tiene permisos para anular recibos.", usuario);
                        return false;
                    }
                }

                

                if ((accion == "A" || accion == "R") && (mEstadoRecibo == "A" || mEstadoRecibo == "R"))
                {
                    string mUsuarioAccion = (from u in db.USUARIO where u.USUARIO1 == qRecibo.USUARIO_AUTORIZA_RECHAZA select u).First().NOMBRE;

                    if (mEstadoRecibo == "A")
                    {
                        mensaje = string.Format("El recibo {0} fue anulado por {1} el {2} a las {3}", recibo, mUsuarioAccion, Utilitario.FormatoDDMMYYYY(Convert.ToDateTime(qRecibo.FECHA_HORA_AUTORIZA_RECHAZA)), Convert.ToDateTime(qRecibo.FECHA_HORA_AUTORIZA_RECHAZA).ToShortTimeString());
                        return false;
                    }
                    if (mEstadoRecibo == "R")
                    {
                        mensaje = string.Format("La anulación del recibo {0} fue rechazada por {1} el {2} a las {3}", recibo, mUsuarioAccion, Utilitario.FormatoDDMMYYYY(Convert.ToDateTime(qRecibo.FECHA_HORA_AUTORIZA_RECHAZA)), Convert.ToDateTime(qRecibo.FECHA_HORA_AUTORIZA_RECHAZA).ToShortTimeString());
                        return false;
                    }
                }

                if ((from c in db.MF_Configura select c).First().RESTRINGIR_ANULACION_RECIBOS == "S" && mValidarRestriccion && accion == "N")
                {
                    if (mEstadoRecibo == "P")
                    {
                        mensaje = string.Format("El recibo {0} se encuentra en proceso de anulación.", recibo);
                        return false;
                    }

                    using (TransactionScope transactionScope = new TransactionScope())
                    {
                       
                        var qUpdateRecibo = from r in db.MF_Recibo where r.DOCUMENTO == recibo select r;
                        foreach (var item in qUpdateRecibo)
                        {
                            item.USUARIO_ANULA = usuario;
                            item.FECHA_ANULACION = DateTime.Now.Date;
                            item.FECHA_HORA_ANULACION = DateTime.Now;
                            item.MOTIVO_ANULACION = observaciones;
                            item.ESTADO_AUTORIZACION = "P";
                        }

                        db.SaveChanges();
                        transactionScope.Complete();
                    }

                    string mTipo = "Efectivo";
                    string mFacturaAplicada = qRecibo.FACTURA == null ? "" : qRecibo.FACTURA;
                    string mCliente = qRecibo.CLIENTE;
                    string mNombreCliente = (from c in db.CLIENTE where c.CLIENTE1 == mCliente select c).First().NOMBRE;

                    if (qRecibo.CHEQUE > 0) mTipo = "Cheque";
                    if (qRecibo.TARJETA > 0)
                    {
                        mTipo = "VISANET";
                        if (qRecibo.POS == "C") mTipo = "CREDOMATIC";
                    }

                    var qEnviar = from g in db.MF_Gerente
                                  where g.ANULA_RECIBOS == "S"
                                  orderby g.NOMBRE
                                  select g;

                    foreach (var item in qEnviar)
                    {
                        string mAutorizar = string.Format("http://localhost:53874/api/AnulacionRecibos?recibo={0}&usuario={1}&accion={2}", recibo, item.USUARIO, "A");
                        string mRechazar = string.Format("http://localhost:53874/api/AnulacionRecibos?recibo={0}&usuario={1}&accion={2}", recibo, item.USUARIO, "R");

                        if (ambiente == "PRO")
                        {
                            mAutorizar = string.Format("http://sql.fiesta.local/RestServices/api/AnulacionRecibos?recibo={0}&usuario={1}&accion={2}", recibo, item.USUARIO, "A");
                            mRechazar = string.Format("http://sql.fiesta.local/RestServices/api/AnulacionRecibos?recibo={0}&usuario={1}&accion={2}", recibo, item.USUARIO, "R");
                        }
                        if (ambiente == "PRU")
                        {
                            mAutorizar = string.Format("http://sql.fiesta.local/RestServicesPruebas/api/AnulacionRecibos?recibo={0}&usuario={1}&accion={2}", recibo, item.USUARIO, "A");
                            mRechazar = string.Format("http://sql.fiesta.local/RestServicesPruebas/api/AnulacionRecibos?recibo={0}&usuario={1}&accion={2}", recibo, item.USUARIO, "R");
                        }

                        StringBuilder mBody = new StringBuilder();
                        
                        mBody.Append("<!DOCTYPE html><html lang='en-us'>");
                        mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } .style1 { color: #8C4510; background-color: #FFF7E7; font-size: xx-small; text-align: left; height:35px; } .style2 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; height:35px; } .style3 { color: #8C4510; background-color: #FFF7E7; font-size: x-small; text-align: left; height:35px; } .style4 { border: 1px solid #8C4510; } .style5 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; text-align: left; height:35px; } </style></head>");
                        mBody.Append("<body>");
                        mBody.Append("<table align='center'>");
                        mBody.Append(string.Format("<tr><td class='style5'><b>Recibo:&nbsp;&nbsp;</b></td><td class='style3'><b>&nbsp;&nbsp;{0} por valor de Q{1}&nbsp;&nbsp;</b></td></tr>", recibo, Utilitario.FormatoNumeroDecimal(qRecibo.MONTO)));
                        mBody.Append(string.Format("<tr><td class='style5'>Tipo:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td></tr>", mTipo));
                        mBody.Append(string.Format("<tr><td class='style5'>Vendedor:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0} - {1}&nbsp;&nbsp;</td></tr>", mNombreVendedor, cobrador));
                        mBody.Append(string.Format("<tr><td class='style5'>Cliente:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0} - {1}&nbsp;&nbsp;</td></tr>", mCliente, mNombreCliente));
                        mBody.Append(string.Format("<tr><td class='style5'>Factura:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td></tr>", mFacturaAplicada.Trim().Length == 0 ? "Es un anticipo" : mFacturaAplicada));
                        mBody.Append(string.Format("<tr><td class='style5'>Fecha del recibo:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td></tr>", Utilitario.FormatoDDMMYYYY(mFechaRecibo)));
                        mBody.Append(string.Format("<tr><td class='style5'>Observaciones del vendedor:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td></tr>", observaciones));
                        mBody.Append(string.Format("<tr><td class='style5'>Acci&#243;n:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}&nbsp;&nbsp;&nbsp;&nbsp;{1}&nbsp;&nbsp;</td></tr>", string.Format("<a href='{0}' tabindex='0'>Autorizar</a>", mAutorizar), string.Format("<a href='{0}' tabindex='0'>Rechazar</a>", mRechazar)));
                        mBody.Append("</table>");
                        mBody.Append("</body>");
                        mBody.Append("</html>");


                        Utils.Utilitarios Correo = new Utils.Utilitarios();
                        Correo.EnviarEmail(mEmailVendedor, item.EMAIL, string.Format("Solicitud de anulación de recibo {0}", recibo), mBody, mNombreVendedor);

                        
                    }


                    mensaje = "Se notificó a Contabilidad su solicitud de anulación, debe esperar su aprobación.";
                    return false;
                }

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    DateTime mFecha = DateTime.Now.Date;
                    string mCliente = (from r in db.MF_Recibo where r.DOCUMENTO == recibo select r).First().CLIENTE;
                    string mFactura = (from r in db.MF_Recibo where r.DOCUMENTO == recibo select r).First().FACTURA;
                    decimal mTipoCambio = (from tc in db.TIPO_CAMBIO_HIST where tc.TIPO_CAMBIO == "REFR" orderby tc.FECHA descending select tc).Take(1).First().MONTO;

                    bool mAutorizando = false; string mEstado = "N"; string mUsuarioAutoriza = "";
                    if ((from g in db.MF_Gerente where g.USUARIO == usuario && g.ANULA_RECIBOS == "S" select g).Count() > 0) mAutorizando = true;

                    if (accion == "R")
                    {
                        var qUpdateRecibo = from r in db.MF_Recibo where r.DOCUMENTO == recibo select r;
                        foreach (var item in qUpdateRecibo)
                        {
                            item.ESTADO_AUTORIZACION = "R";
                            item.USUARIO_AUTORIZA_RECHAZA = usuario;
                            item.FECHA_HORA_AUTORIZA_RECHAZA = DateTime.Now;
                        }
                    }
                    else
                    {
                        var qFactura = from f in db.MF_Factura where f.FACTURA == mFactura select f;
                        foreach (var item in qFactura)
                        {
                            item.RECIBO = Convert.ToString(null);
                        }

                        if (mAutorizando)
                        {
                            mEstado = "A";
                            mUsuarioAutoriza = usuario;
                        }
                        
                        var qUpdateRecibo = from r in db.MF_Recibo where r.DOCUMENTO == recibo select r;
                        if (mEstadoRecibo == "P")
                        {

                            foreach (var item in qUpdateRecibo)
                            {
                                item.MONTO = 0;
                                item.CHEQUE = 0;
                                item.EFECTIVO = 0;
                                item.TARJETA = 0;
                                item.EFECTIVO_DOLARES = 0;
                                item.FACTURA = "";
                                item.ANULADO = "S";
                                item.ESTADO_AUTORIZACION = mEstado;
                                item.USUARIO_AUTORIZA_RECHAZA = mUsuarioAutoriza;
                                item.FECHA_HORA_AUTORIZA_RECHAZA = DateTime.Now;
                            }
                        }
                        else
                        {
                            foreach (var item in qUpdateRecibo)
                            {
                                item.MONTO = 0;
                                item.CHEQUE = 0;
                                item.EFECTIVO = 0;
                                item.TARJETA = 0;
                                item.EFECTIVO_DOLARES = 0;
                                item.FACTURA = "";
                                item.ANULADO = "S";
                                item.USUARIO_ANULA = usuario;
                                item.FECHA_ANULACION = mFecha;
                                item.FECHA_HORA_ANULACION = DateTime.Now;
                                item.MOTIVO_ANULACION = observaciones;
                                item.ESTADO_AUTORIZACION = mEstado;
                                item.USUARIO_AUTORIZA_RECHAZA = mUsuarioAutoriza;
                                item.FECHA_HORA_AUTORIZA_RECHAZA = DateTime.Now;
                            }
                        }

                        var qFacturaRecibo = db.MF_FacturaRecibo.Where(x => x.RECIBO == recibo).ToList();

                        qFacturaRecibo.ForEach(x => db.MF_FacturaRecibo.DeleteObject(x));

                        DOCUMENTOS_CC iDocumentos_CC = new DOCUMENTOS_CC
                        {
                            DOCUMENTO = recibo,
                            TIPO = "REC",
                            APLICACION = "** ANULADO **",
                            FECHA_DOCUMENTO = mFecha,
                            FECHA = mFecha,
                            MONTO = 0,
                            SALDO = 0,
                            MONTO_LOCAL = 0,
                            SALDO_LOCAL = 0,
                            MONTO_DOLAR = 0,
                            SALDO_DOLAR = 0,
                            MONTO_CLIENTE = 0,
                            SALDO_CLIENTE = 0,
                            TIPO_CAMBIO_MONEDA = 1,
                            TIPO_CAMBIO_DOLAR = mTipoCambio,
                            TIPO_CAMBIO_CLIENT = 1,
                            TIPO_CAMB_ACT_LOC = 1,
                            TIPO_CAMB_ACT_DOL = mTipoCambio,
                            TIPO_CAMB_ACT_CLI = 1,
                            SUBTOTAL = 0,
                            DESCUENTO = 0,
                            IMPUESTO1 = 0,
                            IMPUESTO2 = 0,
                            RUBRO1 = 0,
                            RUBRO2 = 0,
                            MONTO_RETENCION = 0,
                            SALDO_RETENCION = 0,
                            DEPENDIENTE = "N",
                            FECHA_ULT_CREDITO = mFecha,
                            CARGADO_DE_FACT = "N",
                            APROBADO = "S",
                            ASIENTO_PENDIENTE = "S",
                            FECHA_ULT_MOD = mFecha,
                            NOTAS = observaciones,
                            CLASE_DOCUMENTO = "N",
                            FECHA_VENCE = mFecha,
                            NUM_PARCIALIDADES = 0,
                            COBRADOR = cobrador,
                            USUARIO_ULT_MOD = usuario,
                            CONDICION_PAGO = "0",
                            MONEDA = "GTQ",
                            VENDEDOR = vendedor,
                            CLIENTE_REPORTE = mCliente,
                            CLIENTE_ORIGEN = mCliente,
                            CLIENTE = mCliente,
                            SUBTIPO = 13,
                            PORC_INTCTE = 0,
                            USUARIO_APROBACION = usuario,
                            FECHA_APROBACION = mFecha,
                            ANULADO = "S",
                            NoteExistsFlag = 0,
                            RowPointer = Guid.NewGuid(),
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("FA/{0}", usuario),
                            UpdatedBy = string.Format("FA/{0}", usuario),
                            CreateDate = DateTime.Now,
                            BASE_IMPUESTO1 = 0,
                            BASE_IMPUESTO2 = 0,
                            DEPENDIENTE_GP = "N",
                            SALDO_TRANS = 0,
                            SALDO_TRANS_LOCAL = 0,
                            SALDO_TRANS_DOLAR = 0,
                            TIPO_ASIENTO = "REC",
                            PAQUETE = "CC",
                            SALDO_TRANS_CLI = 0,
                            FACTURADO = "N",
                            GENERA_DOC_FE = "N"
                        };

                        db.DOCUMENTOS_CC.AddObject(iDocumentos_CC);
                    }

                    string mNotificacion = "";
                    if (mAutorizando)
                    {
                        //SmtpClient smtp = new SmtpClient();
                        //MailMessage mail = new MailMessage();

                        string mEmailUsuario = (from u in db.USUARIO join v in db.MF_Vendedor on u.CELULAR equals v.VENDEDOR where u.USUARIO1 == usuario select v).First().EMAIL;
                        string mNombreUsuario = (from u in db.USUARIO join v in db.VENDEDOR on u.CELULAR equals v.VENDEDOR1 where u.USUARIO1 == usuario select v).First().NOMBRE;
                        string mAviso = string.Format("<p>Por este medio se le informa que el recibo {0} fue anulado exitosamente por {1}.</p>", recibo, mNombreUsuario);

                        if (accion == "R") mAviso = string.Format("<p>Por este medio se le informa que su solicitud de anulación del recibo {0} fue <span class='style1'><strong>RECHAZADA</strong></span>.</p>", recibo, mNombreUsuario);

                        //mail.To.Add(mEmailVendedor);

                        //mail.IsBodyHtml = true;
                        //mail.ReplyToList.Add(mEmailUsuario);
                        //mail.From = new MailAddress(mEmailUsuario, mNombreUsuario);
                        //mail.From = new MailAddress("puntodeventa@mueblesfiesta.com", mNombreVendedor);
                        //mail.Subject = string.Format("Anulación de recibo {0}", recibo);

                        StringBuilder mBody = new StringBuilder();

                        mBody.Append("<!DOCTYPE html><html lang='en-us'>");
                        mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } .style1 { color: #FF0000; } </style></head>");
                        mBody.Append("<body>");
                        mBody.Append(string.Format("<p>Estimado(a) {0}:</p>", mNombreVendedor));
                        mBody.Append(mAviso);
                        mBody.Append("<p>Atentamente,</p>");
                        mBody.Append(string.Format("<p>{0}<BR><b>Muebles Fiesta - {1}</b></p>", mNombreUsuario, "F01"));
                        mBody.Append("</body>");
                        mBody.Append("</html>");

						//mail.Body = mBody.ToString();

						//smtp.Host = WebConfigurationManager.AppSettings["MailHost"];
						//smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort"]);
						//smtp.EnableSsl = true;
						//smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

						//try
						//{
						//    smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail"], WebConfigurationManager.AppSettings["PwdMail"]);
						//    //smtp.Credentials = new System.Net.NetworkCredential("puntodeventa", "jh$%Pjma");
						//    smtp.Send(mail);
						//}
						//catch
						//{
						//    //Nothing
						//}

						Utils.Utilitarios Correo = new Utils.Utilitarios();
                        Correo.EnviarEmail(mEmailUsuario, mEmailVendedor, string.Format("Anulación de recibo {0}", recibo), mBody, mNombreUsuario);

                        mNotificacion = string.Format(", y se notificó a {0}", mNombreVendedor);
                    }

                    mensaje = string.Format("Anulación del recibo {0} exitosa{1}", recibo, mNotificacion);
                    if (accion == "R") mensaje = string.Format("Se rechazó la anulación del recibo {0} exitosamente{1}", recibo, mNotificacion);

                    db.SaveChanges();
                    transactionScope.Complete();
                }

            }
            catch (Exception ex)
            {
                mRetorna = false;
                mensaje = string.Format("{0}", CatchClass.ExMessage(ex, "AnulacionReciboController", "GetAnulacionRecibo"));
            }

            return mRetorna;
        }

        public Clases.RetornaExito PostAnulacionRecibos([FromBody]Clases.AnulaRecibo Anula)
        {
            string mensaje = "";
            Clases.RetornaExito mRetorna = new Clases.RetornaExito();

            mRetorna.exito = true;
            if (!AnulaRecibo(Anula.Recibo, Anula.Usuario, Anula.Observaciones, Anula.Vendedor, Anula.Cobrador, Anula.Accion, ref mensaje, Anula.Ambiente)) mRetorna.exito = false;

            mRetorna.mensaje = mensaje;
            return mRetorna;
        }

    }
}
