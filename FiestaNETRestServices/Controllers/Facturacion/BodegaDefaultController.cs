﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;

namespace FiestaNETRestServices.Controllers.Facturacion
{
    public class BodegaDefaultController : MFApiController
    {
        // GET: api/BodegaDefault
        public string Get()
        {
            return "Debe enviar la tienda";
        }
        
        // GET: api/BodegaDefault/5
        public string Get(string id)
        {
            var q = from c in db.MF_Cobrador where c.COBRADOR == id select c;

            if (q.Count() == 0)
            {
                return "Error, no se encontró la tienda";
            }
            else
            {
                return q.First().BODEGA_DEFAULT;
            }
        }
    }
}
