﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;

namespace FiestaNETRestServices.Controllers.Facturacion
{
    public class ClientesDolaresController : MFApiController
    {
        // GET: api/ClientesDolares
        public string Get()
        {
            string mRetorna = "";
            var q = from c in db.MF_Catalogo where c.CODIGO_TABLA == 31 && c.NOMBRE_CAT == "A" orderby c.CODIGO_CAT select c;

            int ii = 0;
            foreach (var item in q)
            {
                ii++;
                string mSeparador = ",";
                if (ii == 1) mSeparador = "";

                mRetorna = string.Format("{0}{1}{2}", mRetorna, mSeparador, item.CODIGO_CAT);
            }

            return mRetorna;
        }

        // GET: api/ClientesDolares/5
        public string Get(int id)
        {
            return "Error, método no disponible";
        }
    }
}
