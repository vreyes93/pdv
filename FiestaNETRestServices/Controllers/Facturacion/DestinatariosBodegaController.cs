﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;

namespace FiestaNETRestServices.Controllers.Facturacion
{
    public class DestinatariosBodegaController : MFApiController
    {
        // GET: api/DestinatariosBodega
        public string Get()
        {
            return "Error, debe enviar la bodega";
        }

        // GET: api/DestinatariosBodega/5
        public string Get(string id)
        {
            if (id != "F01" && id != "B05") return "Error, bodega inválida";

            int mCatalogo = 16;
            if (id == "F01") mCatalogo = 27;

            string mRetorna = "";
            var q = from c in db.MF_Catalogo where c.CODIGO_TABLA == mCatalogo select c;

            foreach (var item in q)
            {
                string mSeparador = "";
                if (mRetorna.Trim().Length > 0) mSeparador = ",";

                mRetorna = string.Format("{0}{1}{2}", mRetorna, mSeparador, item.NOMBRE_CAT);
            }

            return mRetorna;
        }

    }
}
