﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;

namespace FiestaNETRestServices.Controllers.Facturacion
{
    public class DevolucionesController : MFApiController
    {
        // GET: api/Devoluciones
        public string Get()
        {
            return "Error, debe enviar el número de factura";
        }

        // GET: api/Devoluciones/5
        public Devolucion Get(string id)
        {
            Devolucion mRetorna = new Devolucion();

            try
            {
                string mFactura = id;
                var qFactura = from f in db.FACTURA where f.FACTURA1 == mFactura select f;
                var qFacturaLinea = from f in db.FACTURA_LINEA where f.FACTURA == mFactura orderby f.LINEA select f;

                if (qFactura.Count() == 0)
                {
                    mRetorna.Mensaje = string.Format("Error, la factura {0} no existe", mFactura);
                    return mRetorna;
                }
                if (qFactura.First().ANULADA == "S")
                {
                    mRetorna.Mensaje = string.Format("Error, la factura {0} está anulada", mFactura);
                    return mRetorna;
                }

                DateTime mFechaFactura = qFactura.First().FECHA.Date;

                if (mFechaFactura.Year == DateTime.Now.Date.Year && mFechaFactura.Month == DateTime.Now.Date.Month)
                {
                    mRetorna.Mensaje = string.Format("Error, la factura {0} es del mes en curso, no aplica devolución", mFactura);
                    return mRetorna;
                }
                if (qFacturaLinea.Where(x => x.CANT_DESPACHADA > 0).Count() == 0)
                {
                    mRetorna.Mensaje = string.Format("Error, la factura {0} no tiene artículos despachados", mFactura);
                    return mRetorna;
                }


            }
            catch (Exception ex)
            {
                mRetorna.Mensaje = string.Format("{0}", CatchClass.ExMessage(ex, "DevolucionesController", "Post"));
            }

            return mRetorna;
        }

        // POST: api/Devoluciones
        public string Post([FromBody]Devolucion devolucion)
        {
            string mRetorna = "";

            try
            {
                string mFactura = devolucion.Factura;
                var qFactura = from f in db.FACTURA where f.FACTURA1 == mFactura select f;
                var qFacturaLinea = from f in db.FACTURA_LINEA where f.FACTURA == mFactura orderby f.LINEA select f;

                if (qFactura.Count() == 0) return string.Format("Error, la factura {0} no existe", mFactura);
                if (qFactura.First().ANULADA == "S") return string.Format("Error, la factura {0} está anulada", mFactura);

                DateTime mFechaFactura = qFactura.First().FECHA.Date;

                if (mFechaFactura.Year == DateTime.Now.Date.Year && mFechaFactura.Month == DateTime.Now.Date.Month) return string.Format("Error, la factura {0} es del mes en curso, no aplica devolución", mFactura);
                if (qFacturaLinea.Where(x => x.CANT_DESPACHADA > 0).Count() == 0) return string.Format("Error, la factura {0} no tiene artículos despachados", mFactura);


            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0}", CatchClass.ExMessage(ex, "DevolucionesController", "Post"));
            }

            return mRetorna;
        }

        // PUT: api/Devoluciones/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Devoluciones/5
        public void Delete(int id)
        {
        }
    }
}
