﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.EntityClient;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;
using Newtonsoft.Json;
using RestSharp;

namespace FiestaNETRestServices.Controllers.Facturacion
{
    public class FelAnulacionController : MFApiController
    {

        // GET: api/FelAnulacion/5
        public string Get(int id)
        {
            return "Debe enviar la factura a anular";
        }

        public MF_Clases.GuateFacturas.FEL.Resultado Post([FromBody]MF_Clases.GuateFacturas.FEL.AnulacionDoc Doc)
        {
            UtilitariosGuateFacturas mUtilitario = new UtilitariosGuateFacturas();
            MF_Clases.GuateFacturas.FEL.Resultado mResultado = new MF_Clases.GuateFacturas.FEL.Resultado();

            mResultado = mUtilitario.Servicio(MF_Clases.Utils.CONSTANTES.GUATEFAC.SERVICIOS.ANULAR_DOCUMENTO, Doc);

            return mResultado;
        }
    }
}
