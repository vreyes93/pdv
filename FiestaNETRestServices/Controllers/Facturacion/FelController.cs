﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.EntityClient;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;
using Newtonsoft.Json;
using RestSharp;

namespace FiestaNETRestServices.Controllers.Facturacion
{
    public class FelController : MFApiController
    {
        // GET: api/Fel
        public string Get()
        {
            return "Debe enviar la clase de la factura a firmar";
        }
        
        public MF_Clases.GuateFacturas.FEL.Resultado Post([FromBody]MF_Clases.GuateFacturas.FEL.Doc Doc)
        {
            UtilitariosGuateFacturas mUtilitario = new UtilitariosGuateFacturas();
            MF_Clases.GuateFacturas.FEL.Resultado mResultado = new MF_Clases.GuateFacturas.FEL.Resultado();

            mResultado = mUtilitario.Servicio(Doc.Servicio,  Doc);

            return mResultado;
        }

    }
}
