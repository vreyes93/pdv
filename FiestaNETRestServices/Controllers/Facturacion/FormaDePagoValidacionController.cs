﻿using FiestaNETRestServices.Content.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Facturacion
{
    public class FormaDePagoValidacionController : MFApiController
    {
        /// <summary>
        /// Servicio para obtener los niveles de precio que entran dentro de la forma de cálcular el precio del bien
        /// en facturación (solicitud Jimmy - junio 2018)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<string> Get()
        {
            List<string> lstResult = new List<string>();
            var qFormaDePago = (from a in db.MF_Catalogo where a.CODIGO_TABLA == 33 select a);
            if (qFormaDePago.Count() > 0)
            {
                foreach (var item in qFormaDePago)
                    lstResult.Add(item.NOMBRE_CAT);
            }
            return lstResult;
        }
    }
}
