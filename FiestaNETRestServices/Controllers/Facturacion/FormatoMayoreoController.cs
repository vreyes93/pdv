﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;

namespace FiestaNETRestServices.Controllers.Facturacion
{
    public class FormatoMayoreoController : MFApiController
    {
        // GET: api/FormatoMayoreo
        public string Get()
        {
            return "Error, debe enviar la serie o factura";
        }
        
        // GET: api/FormatoMayoreo/5
        public string Get(string id)
        {
            string mRetorna = "rptFacturaMayoreoA.rpt";
            string[] mFactura = id.Split(new string[] { "-" }, StringSplitOptions.None);
            string mFormato = mFactura[0];

            var q = from c in db.MF_Catalogo where c.CODIGO_TABLA == 30 && c.CODIGO_CAT == mFormato select c;
            if (q.Count() > 0) mRetorna = q.First().NOMBRE_CAT;

            return mRetorna;
        }

    }
}
