﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.DAL;
using MF_Clases.Facturacion;
using System;
using System.Web.Http;


namespace FiestaNETRestServices.Controllers.Facturacion
{
    public class GarantiaController : MFApiController
    {
        private new static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [HttpGet]
        [Route("Garantia/{id}")]
        public IHttpActionResult Get(string id)
        {
            try
            {
                Garantia factura = new DALComunes().facturaGarantia(id);

                if (factura == null)
                    throw new Exception("Factura no encontrada.");

                return Ok(factura);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
        }
    }
}
