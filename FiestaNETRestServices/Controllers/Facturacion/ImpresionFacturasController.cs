﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.EntityClient;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;
using Newtonsoft.Json;
using RestSharp;

namespace FiestaNETRestServices.Controllers.Facturacion
{
    public class ImpresionFacturasController : MFApiController
    {
        // GET: api/ImpresionFacturas
        public string Get()
        {
            return "Error, debe enviar la empresa y la factura";
        }

        // GET: api/ImpresionFacturas/5
        public List<ImpresionFactura> Get(string empresa, string factura)
        {
            return DevuelveFacturas(empresa, factura);
        }

        public List<ImpresionFactura> Get(string empresa, string inicial, string final)
        {
            return DevuelveFacturas(empresa, "", inicial, final);
        }

        List<ImpresionFactura> DevuelveFacturas(string empresa, string factura, string inicial = "", string final = "")
        {
            EntityConnection ec = (EntityConnection)db.Connection;
            SqlConnection sc = (SqlConnection)ec.StoreConnection;

            SqlConnection mConexion = new SqlConnection(sc.ConnectionString);
            SqlCommand mCommand = new SqlCommand(); SqlTransaction mTransaction = null;

            List<ImpresionFactura> mFacturas = new List<ImpresionFactura>();

            try
            {
                string[] mInicialString; string[] mFinalString;
                int mInicial = 0; int mFinal = 0; string mSerie = "";
                string[] mConsecutivoFA = factura.Split(new string[] { "-" }, StringSplitOptions.None);

                if (factura == "")
                {
                    mInicialString = inicial.Split(new string[] { "-" }, StringSplitOptions.None);
                    mFinalString = final.Split(new string[] { "-" }, StringSplitOptions.None);
                }
                else
                {
                    mInicialString = factura.Split(new string[] { "-" }, StringSplitOptions.None);
                    mFinalString = factura.Split(new string[] { "-" }, StringSplitOptions.None);
                }

                mSerie = mInicialString[0];
                mInicial = Convert.ToInt32(mInicialString[1]);
                mFinal = Convert.ToInt32(mFinalString[1]);

                for (int ii = mInicial; ii <= mFinal; ii++)
                {
                    string mNumero = ii.ToString();

                    for (int jj = 0; jj < mInicialString[1].Length; jj++)
                    {
                        if (mNumero.Length < mInicialString[1].Length) mNumero = string.Format("0{0}", mNumero);
                    }
                    
                    DataTable dt = new DataTable(); DataTable dtDetalle = new DataTable();
                    string mFact = string.Format("{0}-{1}", mSerie, mNumero);

                    SqlDataAdapter da = new SqlDataAdapter(string.Format("SELECT a.FACTURA AS NoFactura, a.FECHA AS Fecha, a.CLIENTE AS Cliente, b.NOMBRE AS Nombre, b.CONTRIBUYENTE AS Nit, " +
                        "REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(a.DIRECCION_EMBARQUE, 'Call/Av/Km/Mz: ', ''), 'Casa/Lote: ', ''), 'Zona/Aldea: ', ''), 'Col/Barr/Cant: ', ''), 'Departamento: ', ''), 'Municipio: ', '') AS DIRECCION, " +
                        "c.NOMBRE AS Vendedor, a.TOTAL_FACTURA AS Monto, prodmult.fcNumerosLetras(a.TOTAL_FACTURA) AS MontoLetras, " +
                        "(SELECT p.OBSERVACIONES FROM {0}.PEDIDO p WHERE p.PEDIDO = a.PEDIDO) AS Observaciones, a.MONEDA_FACTURA AS Moneda " +
                        "FROM {0}.FACTURA a JOIN {0}.CLIENTE b ON a.CLIENTE = b.CLIENTE JOIN {0}.VENDEDOR c ON a.VENDEDOR = c.VENDEDOR " +
                        "WHERE a.FACTURA = '{1}' AND a.ANULADA = 'N'", empresa, mFact), mConexion);
                    
                    SqlDataAdapter daDetalle = new SqlDataAdapter(string.Format("SELECT a.FACTURA AS NoFactura, a.ARTICULO AS Articulo, a.CANTIDAD AS Cantidad, a.DESCRIPCION AS Descripcion, " +
                        "ROUND(((a.PRECIO_TOTAL + a.TOTAL_IMPUESTO1) / a.CANTIDAD), 2) AS PrecioUnitario, ROUND((a.PRECIO_TOTAL + a.TOTAL_IMPUESTO1), 2) AS PrecioTotal " +
                        "FROM {0}.FACTURA_LINEA a JOIN {0}.ARTICULO b ON a.ARTICULO = b.ARTICULO WHERE a.FACTURA = '{1}' AND a.ANULADA = 'N'", empresa, mFact), mConexion);

                    da.Fill(dt);
                    daDetalle.Fill(dtDetalle);

                    if (dt.Rows.Count == 0)
                    {
                        ImpresionFactura mFactura = new ImpresionFactura();
                        mFactura.Nombre = string.Format("Factura {0} no existe o está anulada", mFact);
                        mFacturas.Add(mFactura);
                    }
                    else
                    {
                        ImpresionFactura mFactura = new ImpresionFactura();
                        mFactura.NoFactura = Convert.ToString(dt.Rows[0]["NoFactura"]);
                        mFactura.Fecha = Convert.ToDateTime(dt.Rows[0]["Fecha"]).Date;
                        mFactura.Cliente = Convert.ToString(dt.Rows[0]["Cliente"]);
                        mFactura.Nombre = Convert.ToString(dt.Rows[0]["Nombre"]);
                        mFactura.Nit = Convert.ToString(dt.Rows[0]["Nit"]);
                        mFactura.Direccion = Convert.ToString(dt.Rows[0]["Direccion"]);
                        mFactura.Vendedor = Convert.ToString(dt.Rows[0]["Vendedor"]);
                        mFactura.Monto = Convert.ToDecimal(dt.Rows[0]["Monto"]);
                        mFactura.MontoLetras = Convert.ToString(dt.Rows[0]["MontoLetras"]);
                        mFactura.Observaciones = Convert.ToString(dt.Rows[0]["Observaciones"]);
                        mFactura.Moneda = Convert.ToString(dt.Rows[0]["Moneda"]);

                        for (int jj = 0; jj < dtDetalle.Rows.Count; jj++)
                        {
                            ImpresionFacturaDetalle mDetalle = new ImpresionFacturaDetalle();
                            mDetalle.NoFactura = Convert.ToString(dtDetalle.Rows[jj]["NoFactura"]);
                            mDetalle.Articulo = Convert.ToString(dtDetalle.Rows[jj]["Articulo"]);
                            mDetalle.Cantidad = Convert.ToInt32(dtDetalle.Rows[jj]["Cantidad"]);
                            mDetalle.Descripcion = Convert.ToString(dtDetalle.Rows[jj]["Descripcion"]);
                            mDetalle.PrecioUnitario = Convert.ToDecimal(dtDetalle.Rows[jj]["PrecioUnitario"]);
                            mDetalle.PrecioTotal = Convert.ToDecimal(dtDetalle.Rows[jj]["PrecioTotal"]);
                            mFactura.detalleFactura.Add(mDetalle);
                        }

                        mFacturas.Add(mFactura);
                    }
                }

            }
            catch (Exception ex)
            {
                if (mConexion.State == ConnectionState.Open)
                {
                    mTransaction.Rollback();
                    mConexion.Close();
                }

                ImpresionFactura mFactura = new ImpresionFactura();
                mFactura.Nombre = string.Format("{0} {1}", CatchClass.ExMessage(ex, "ImpresionFacturasController", "GET"), mCommand.CommandText);
                mFacturas.Add(mFactura);
            }

            return mFacturas;
        }

    }
}
