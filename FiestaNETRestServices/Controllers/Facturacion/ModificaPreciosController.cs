﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;

namespace FiestaNETRestServices.Controllers.Facturacion
{
    public class ModificaPreciosController : MFApiController
    {
        // GET: api/ModificaPrecios
        public string Get()
        {
            return "Error, debe enviar el usuario";
        }

        // GET: api/ModificaPrecios/5
        public string Get(string id)
        {
            string mRetorna = "N";

            var q = from c in db.MF_Catalogo where c.CODIGO_TABLA == 29 && c.CODIGO_CAT == id select c;
            if (q.Count() > 0) mRetorna = "S";

            return mRetorna;
        }

    }
}
