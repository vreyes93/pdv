﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;

namespace FiestaNETRestServices.Controllers.Facturacion
{
    public class RefacturacionController : MFApiController
    {
        // GET: api/Refacturacion
        public string Get()
        {
            return "Debe enviar el código del cliente";
        }

        // GET: api/Refacturacion/5
        public List<FacturasCliente> Get(string id)
        {
            List<FacturasCliente> mRetorna = new List<FacturasCliente>();

            FacturasCliente mNo = new FacturasCliente();
            mNo.Factura = "N";
            mNo.Texto = "No";
            mRetorna.Add(mNo);

            var q = from f in db.FACTURA where f.CLIENTE == id && f.ANULADA == "N" orderby f.FECHA descending select f;

            foreach (var item in q)
            {
                FacturasCliente mItem = new FacturasCliente();
                mItem.Factura = item.FACTURA1;
                mItem.Texto = string.Format("{0} - {1} - Q{2}", item.FACTURA1, Utilitario.FormatoDDMMYYYY(item.FECHA), Utilitario.FormatoNumeroDecimal(item.TOTAL_FACTURA));
                mRetorna.Add(mItem);
            }

            return mRetorna;
        }

        // POST: api/Refacturacion
        public string Post(string pedido, string refacturacion, string usuario)
        {
            string mRetorna = "";

            try
            {
                var qPedido = from p in db.MF_Pedido where p.PEDIDO == pedido select p;
                var qFactura = from f in db.FACTURA where f.FACTURA1 == refacturacion select f;

                if (qPedido.Count() == 0) return string.Format("Error, el pedido {0} no existe", pedido);
                if (qPedido.First().FACTURA == null) return string.Format("Error, el pedido {0} no está facturado", pedido);

                string mFacturaNueva = qPedido.First().FACTURA;
                string mFacturaOriginal = refacturacion;

                var qPedidoExactus = from p in db.PEDIDO where p.PEDIDO1 == pedido select p;
                var qFacturaOriginal = from f in db.FACTURA where f.FACTURA1 == mFacturaOriginal select f;
                var qFacturaOriginalMF = from f in db.MF_Factura where f.FACTURA == mFacturaOriginal select f;
                var qFacturaMF = from f in db.MF_Factura where f.FACTURA == mFacturaNueva select f;

                if (mFacturaOriginal == mFacturaNueva) return "Error, La refacturación no puede ser la misma factura de este pedido";

                if (refacturacion == "N")
                {
                    using (TransactionScope transactionScope = new TransactionScope())
                    {
                        foreach (var item in qPedido)
                        {
                            item.RE_FACTURACION = "N";
                        }

                        foreach (var item in qFacturaOriginalMF)
                        {
                            item.RE_FACTURACION = "N";
                        }

                        foreach (var item in qPedidoExactus)
                        {
                            item.OBSERVACIONES = item.OBSERVACIONES.Replace(string.Format("Refacturación: {0} ", mFacturaOriginal), "");
                        }

                        List<MF_ComisionExcepcion> mBorrar = db.MF_ComisionExcepcion.Where(x => x.FACTURA == mFacturaOriginal || x.FACTURA == refacturacion).ToList();
                        foreach (MF_ComisionExcepcion item in mBorrar) db.MF_ComisionExcepcion.DeleteObject(item);

                        db.SaveChanges();
                        transactionScope.Complete();
                    }

                    return "Se eliminó la relación de refacturación";
                }

                if (qFactura.Count() == 0) return string.Format("Error, la factura {0} no existe", refacturacion);

                string mFactura = mFacturaNueva;
                decimal mTotal = (decimal)qPedido.First().TOTAL_FACTURAR;
                decimal mTotalOriginal = qFacturaOriginal.First().TOTAL_FACTURA;
                decimal mMontoComision = 0; string mTipo = "N";

                if (mTotal > mTotalOriginal)
                {
                    mTipo = "M";
                    mMontoComision = mTotal - mTotalOriginal;
                }

                int mFinanciera = qFacturaOriginalMF.First().FINANCIERA;
                var qFinanciera = from f in db.MF_Financiera where f.Financiera == mFinanciera select f;

                if (qFinanciera.First().ES_BANCO == "S")
                {
                    var qCC = from d in db.DOCUMENTOS_CC where d.DOCUMENTO == mFacturaOriginal && d.TIPO == "FAC" select d;

                    if (qCC.First().SALDO > 0)
                    {
                        mTipo = "N";
                        mMontoComision = 0;
                        mFactura = mFacturaOriginal;
                    }
                }

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    if (qFacturaOriginal.First().ANULADA == "S")
                    {
                        foreach (var item in qPedido)
                        {
                            item.RE_FACTURACION = "N";
                        }

                        foreach (var item in qFacturaMF)
                        {
                            item.RE_FACTURACION = "N";
                        }
                    }
                    else
                    {
                        //Poner las notas en el expediente
                        var qExcepcion = from e in db.MF_ComisionExcepcion where e.FACTURA == mFactura select e;
                        if (qExcepcion.Count() > 0) return "Error, esta refacturacion ya fue grabada";

                        foreach (var item in qPedido)
                        {
                            item.RE_FACTURACION = mFacturaOriginal;
                        }

                        foreach (var item in qFacturaMF)
                        {
                            item.RE_FACTURACION = mFacturaOriginal;
                        }

                        MF_ComisionExcepcion iComision = new MF_ComisionExcepcion
                        {
                            FACTURA = mFactura,
                            TIPO = mTipo,
                            MONTO_COMISION = mMontoComision,
                            COBRADOR = qFactura.First().COBRADOR,
                            VENDEDOR = qFactura.First().VENDEDOR,
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("FA/{0}", usuario),
                            UpdatedBy = string.Format("FA/{0}", usuario),
                            CreateDate = DateTime.Now
                        };

                        db.MF_ComisionExcepcion.AddObject(iComision);
                    }

                    db.SaveChanges();
                    transactionScope.Complete();
                }

                mRetorna = "Se grabó exitosamente la relación de refacturación";
            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0} {1}", CatchClass.ExMessage(ex, "RefacturacionController", "Post"), "");
            }

            return mRetorna;
        }

        // PUT: api/Refacturacion/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Refacturacion/5
        public void Delete(int id)
        {
        }
    }
}
