﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using FiestaNETRestServices.Content.Abstract;
using Newtonsoft.Json;
using MF_Clases;
using System.Web.Configuration;

namespace FiestaNETRestServices.Controllers.Facturacion
{
    public class SolicitudPagareController : MFApiController
    {

        public string GetSolicitudPagare()
        {
            return "Debe enviar el código de la financiera";
        }

        public string GetSolicitudPagare(string id)
        {
            string mRetorna = "";
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            db.CommandTimeout = 999999999;

            try
            {
                int mFinanciera = 0;

                try
                {
                    mFinanciera = Convert.ToInt32(id);
                }
                catch
                {
                    mFinanciera = 0;
                }

                if (mFinanciera == 0) return "Debe enviar un código de financiera válido.";
                var q = from f in db.MF_Financiera where f.Financiera == mFinanciera select f;

                if (q.Count() == 0) return string.Format("No se encontró la financiera {0}", mFinanciera);
                mRetorna = string.Format("{0},{1}", q.First().RptSolicitud, q.First().RptPagare);
            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0}", CatchClass.ExMessage(ex, "SolicitudPagareController", "GetSolicitudPagare"));
            }

            return mRetorna;
        }

    }
}
