﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Text;
using System.Xml;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Models;
using System.Web.Configuration;
using System.Net;
using System.Net.Mime;
using System.IO;
using System.Security.Cryptography;
using System.Configuration;

namespace FiestaNETRestServices.Controllers.Facturacion
{
    public class UtilitariosFacturacion
    {
        public string RecibosFactura(string factura)
        {
            string mRecibos = "";
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            db.CommandTimeout = 999999999;

            try
            {
                var qRecibos = from d in db.AUXILIAR_CC join c in db.DOCUMENTOS_CC on d.DEBITO equals c.DOCUMENTO where d.DEBITO == factura && c.ANULADO == "N" orderby d.CREDITO select d;

                int jj = 0;
                foreach (var rec in qRecibos)
                {
                    jj += 1;
                    string mSeparador = ", ";
                    if (jj == 1) mSeparador = "";

                    mRecibos = string.Format("{0}{1}{2}", mRecibos, mSeparador, rec.CREDITO);
                }
            }
            catch (Exception ex)
            {
                mRecibos = string.Format("{0}", CatchClass.ExMessage(ex, "RecibosFactura", "Post"));
            }

            return mRecibos;
        }

        public string DespachosFactura(string factura)
        {
            string mDespachos = "";
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            db.CommandTimeout = 999999999;

            try
            {
                var qDespachos = from d in db.MF_Despacho join de in db.DESPACHO on d.DESPACHO equals de.DESPACHO1 where d.FACTURA == factura && de.ESTADO == "D" orderby d.DESPACHO select d;

                int jj = 0;
                foreach (var des in qDespachos)
                {
                    jj += 1;
                    string mSeparador = ", ";
                    if (jj == 1) mSeparador = "";

                    mDespachos = string.Format("{0}{1}{2}", mDespachos, mSeparador, des.DESPACHO);
                }
            }
            catch (Exception ex)
            {
                mDespachos = string.Format("{0}", CatchClass.ExMessage(ex, "DespachosFactura", "Post"));
            }

            return mDespachos;
        }

        public List<DocumentoCC> EstadoCuenta(string cliente)
        {
            List<DocumentoCC> mRetorna = new List<DocumentoCC>();
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            db.CommandTimeout = 999999999;

            try
            {
                bool mYaSaldoInicial = false;
                DateTime mFecha = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                DateTime mFechaCC = mFecha.AddMonths(-6);

                decimal mSaldoInicial = 0; decimal mSaldo = 0;
                var qCC = from c in db.DOCUMENTOS_CC where c.CLIENTE == cliente orderby c.FECHA, c.TIPO select c;

                foreach (var cc in qCC)
                {
                    bool mCargo = false;
                    if (cc.TIPO == "FAC" || cc.TIPO == "N/D" || cc.TIPO == "O/D") mCargo = true;

                    if (cc.FECHA < mFechaCC)
                    {
                        if (mCargo)
                        {
                            mSaldoInicial = mSaldoInicial + cc.MONTO_LOCAL;
                        }
                        else
                        {
                            mSaldoInicial = mSaldoInicial - cc.MONTO_LOCAL;
                        }
                    }
                    else
                    {
                        if (!mYaSaldoInicial)
                        {
                            if (mSaldoInicial > 0)
                            {
                                DocumentoCC mCCSaldo = new DocumentoCC();
                                mCCSaldo.Fecha = mFechaCC.AddDays(-1);
                                mCCSaldo.Tipo = "";
                                mCCSaldo.Cliente = cliente;
                                mCCSaldo.Documento = "";
                                mCCSaldo.Descripcion = "SALDO INICIAL:";
                                mCCSaldo.Cargos = 0;
                                mCCSaldo.Abonos = 0;
                                mCCSaldo.Saldo = mSaldoInicial;
                                mCCSaldo.Recibos = "";
                                mCCSaldo.Despachos = "";
                                mRetorna.Add(mCCSaldo);
                            }

                            mSaldo = mSaldoInicial;
                            mYaSaldoInicial = true;
                        }

                        if (mCargo)
                        {
                            mSaldo = mSaldo + cc.MONTO_LOCAL;
                        }
                        else
                        {
                            mSaldo = mSaldo - cc.MONTO_LOCAL;
                        }

                        DocumentoCC mCC = new DocumentoCC();
                        mCC.Fecha = cc.FECHA;
                        mCC.Tipo = cc.TIPO;
                        mCC.Cliente = cliente;
                        mCC.Documento = cc.DOCUMENTO;
                        mCC.Descripcion = cc.APLICACION;
                        mCC.Cargos = mCargo ? cc.MONTO_LOCAL : 0;
                        mCC.Abonos = mCargo ? 0 : cc.MONTO_LOCAL;
                        mCC.Saldo = mSaldo;
                        mCC.Recibos = RecibosFactura(cc.DOCUMENTO);
                        mCC.Despachos = DespachosFactura(cc.DOCUMENTO);
                        mRetorna.Add(mCC);

                    }
                }

                DocumentoCC mCCFinal = new DocumentoCC();
                mCCFinal.Fecha = DateTime.Now.Date;
                mCCFinal.Tipo = "";
                mCCFinal.Cliente = cliente;
                mCCFinal.Documento = "";
                mCCFinal.Descripcion = "SALDO FINAL:";
                mCCFinal.Cargos = 0;
                mCCFinal.Abonos = 0;
                mCCFinal.Saldo = mSaldo;
                mCCFinal.Recibos = "";
                mCCFinal.Despachos = "";
                mRetorna.Add(mCCFinal);

            }
            catch (Exception ex)
            {
                DocumentoCC mCCSaldo = new DocumentoCC();
                mCCSaldo.Fecha = DateTime.Now.Date;
                mCCSaldo.Tipo = "";
                mCCSaldo.Cliente = cliente;
                mCCSaldo.Documento = "";
                mCCSaldo.Descripcion = string.Format("{0}", CatchClass.ExMessage(ex, "EstadoCuenta", "Post"));
                mCCSaldo.Cargos = 0;
                mCCSaldo.Abonos = 0;
                mCCSaldo.Saldo = 0;
                mCCSaldo.Recibos = "";
                mCCSaldo.Despachos = "";
                mRetorna.Add(mCCSaldo);
            }

            return mRetorna;
        }



    }
}