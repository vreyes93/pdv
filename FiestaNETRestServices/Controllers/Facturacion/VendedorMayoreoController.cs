﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;

namespace FiestaNETRestServices.Controllers.Facturacion
{
    public class VendedorMayoreoController : MFApiController
    {
        // GET: api/VendedorMayoreo
        public string Get()
        {
            return (from c in db.MF_Configura select c).First().VENDEDOR_MAYOREO;
        }

        // GET: api/VendedorMayoreo/5
        public string Get(int id)
        {
            return "Método no disponible";
        }

    }
}
