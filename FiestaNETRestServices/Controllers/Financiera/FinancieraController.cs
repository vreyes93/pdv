﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.DAL;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Financiera
{
    public class FinancieraController : MFApiController
    {
        /// <summary>
        /// Servicio para obtener la información de una financiera específica
        /// para debuguear se llama asi: http://localhost:53874/api/Financiera/GetFinanciera?Financiera=7
        /// </summary>
        /// <param name="Financiera">Id de la financiera</param>
        /// <returns></returns>
        [Route("~/GetFinanciera?Financiera={Financiera}")]
        [HttpGet]
        public Respuesta GetFinanciera(int Financiera)
        {
            Respuesta mRespuesta = new Respuesta();
            DALFinanciera mDALFinanciera = new DALFinanciera();
            try
            {
                mRespuesta = mDALFinanciera.ObtenerFinanciera(Financiera);
                return mRespuesta;
            }
            catch (Exception e)
            {
                return new Respuesta(false, e, "");
            }
        }
        /// <summary>
        /// Para obtener todas las financieras activas
        /// para debuguear se llama asi:http://localhost:53874/api/Financiera/GetFinancieras
        /// </summary>
        /// <returns></returns>
        [Route("~/GetFinancieras")]
        [HttpGet]
        public Respuesta GetFinancieras()
        {
            Respuesta mRespuesta = new Respuesta();
            DALFinanciera mDALFinanciera = new DALFinanciera();
            try
            {
                mRespuesta= mDALFinanciera.ObtenerFinancieras();
            }
            catch(Exception e)
            { return new Respuesta(false,e); }
            return mRespuesta;
        }
        /// <summary>
        /// Servicio para operar los desembolsos de la financiera
        /// cuando no hay un servicio automatizado se verifica en la conta
        /// el ingreso del desembolso
        /// para debuguear se llama asi: http://localhost:53874/api/Financiera/
        /// </summary>
        /// <returns>Respuesta</returns>
        [Route("~/OperarDesembolsos?nFinanciera={nFinanciera}")]
        [HttpGet]
        public Respuesta OperarDesembolsos(int nFinanciera)
        {
            Respuesta mRespuesta = new Respuesta();
            DALFinanciera mDALFinanciera = new DALFinanciera();
            try
            {
                mRespuesta = mDALFinanciera.OperarDesembolsosFinanciera(nFinanciera);
            }
            catch (Exception e)
            { return new Respuesta(false, e); }
            return mRespuesta;
            
        }
    }
}