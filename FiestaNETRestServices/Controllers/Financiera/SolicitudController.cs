﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.EntityClient;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;

namespace FiestaNETRestServices.Controllers.Financiera
{
    [RoutePrefix("api/Solicitud")]
    public class SolicitudController : MFApiController
    {
        // GET: api/Solicitud
        public string Get()
        {
            return "Debe enviar un número de pedido o cotización";
        }

        // GET: api/Solicitud/5
        public Solicitud Get(string id)
        {
            Solicitud mSolicitud = new Solicitud();
            DateTime? fechaSolicitud = null;

            try
            {
                string mTasa = "24%"; string mTasaLetras = "veinticuatro por ciento";
                string mDireccionFinanciera = "KILOMETRO VEINTE ZONA CUATRO, CARRETERA A LINDA VISTA, VILLA NUEVA, GUATEMALA";

                string mCliente = "";
                bool mEsPedido = true; string mPedido = id.ToUpper(); int mCotizacion = 0;

                if (mPedido.Substring(0, 1) != "P")
                {
                    try
                    {
                        mCotizacion = Convert.ToInt32(id);
                    }
                    catch
                    {
                        mSolicitud.PrimerNombre = "Número de cotización inválida.";
                        return mSolicitud;
                    }

                    mEsPedido = false;
                }
                
                var qPedido = from mp in db.MF_Pedido
                              join p in db.PEDIDO
                                on mp.PEDIDO equals p.PEDIDO1
                                where mp.PEDIDO == mPedido select new {mp.NIVEL_PRECIO
                                , mp.FECHA_SOLICITUD
                                , mp.CLIENTE
                                , mp.TOTAL_FACTURAR
                                , mp.ENGANCHE
                                , mp.SALDO_FINANCIAR
                                , mp.RECARGOS
                                , mp.MONTO
                                ,
                                    mp.MONTO_PAGOS1
                                ,
                                    mp.MONTO_PAGOS2
                                ,
                                    mp.FECHA_REGISTRO
                                ,
                                    mp.SOLICITUD
                                ,
                                    mp.PAGARE
                                ,
                                    mp.NUMERO_CREDITO
                                ,
                                    mp.FECHA_PRIMER_PAGO
                                    , p.MONTO_DESCUENTO1
                                };
                var qCotizacion = from c in db.MF_Cotizacion where c.COTIZACION == mCotizacion select c;

                string mNumeroCredito = ""; DateTime mFechaInicial;
                DateTime mFechaDocumento; string mNoCreditoATID = ""; string mUsuario = ""; string mTienda = ""; DateTime mFechaPagare; DateTime mFechaPrimerPago;
                string mArticulo = "Bienes de consumo - Muebles"; string mSucursal = ""; string mCuotasMenosUno = ""; string mCuotaFormato = ""; string mUltimaCuotaFormato = "";
                Int64 mNumeroSolicitud = 0; Int64 mNumeroPagare = 0; string mVendedor = ""; string mFecha = ""; string mNivel = ""; string mCuotas = ""; string mSaldoFinanciarFormato = "";
                decimal mPrecioDelBien = 0; decimal mEnganche = 0; decimal mSaldoFinanciar = 0; decimal mIntereses = 0; decimal mMonto = 0; string mPlazo = ""; decimal mCuota = 0; decimal mUltimaCuota = 0;
           

                if (mEsPedido)
                {
                    if (qPedido.Count() == 0)
                    {
                        mSolicitud.PrimerNombre = "Número de pedido inválido.";
                        return mSolicitud;
                    }

                    mNivel = qPedido.First().NIVEL_PRECIO;
                    fechaSolicitud = qPedido.First().FECHA_SOLICITUD;
                    var qNivel = (from n in db.MF_NIVEL_PRECIO_COEFICIENTE where n.NIVEL_PRECIO == mNivel select n).First();
                    var qVendedor = (from v in db.VENDEDOR join p in db.PEDIDO on v.VENDEDOR1 equals p.VENDEDOR where p.PEDIDO1 == mPedido select v).First();
                    var qMFVendedor = (from v in db.MF_Vendedor join p in db.PEDIDO on v.VENDEDOR equals p.VENDEDOR where p.PEDIDO1 == mPedido select v).First();
                    string[] mArregloCorreo = qMFVendedor.EMAIL.Split(new string[] { "@" }, StringSplitOptions.None);
                    var qPed = (from p in db.PEDIDO where p.PEDIDO1 == mPedido select p).First();

                    mSucursal = (from c in db.MF_Cobrador join p in db.PEDIDO on c.COBRADOR equals p.COBRADOR where p.PEDIDO1 == mPedido select c).First().NOMBRE;

                    mCliente = qPedido.First().CLIENTE;
                    mPrecioDelBien = Math.Round(Convert.ToDecimal(qPedido.First().TOTAL_FACTURAR), 2, MidpointRounding.AwayFromZero);
                    mEnganche = Convert.ToDecimal(qPedido.First().ENGANCHE);
                    mSaldoFinanciar = Math.Round(Convert.ToDecimal(qPedido.First().SALDO_FINANCIAR), 2, MidpointRounding.AwayFromZero);
                    mIntereses = Math.Round(Convert.ToDecimal(qPedido.First().RECARGOS), 2, MidpointRounding.AwayFromZero);
                    mMonto = Math.Round(Convert.ToDecimal(qPedido.First().MONTO), 2, MidpointRounding.AwayFromZero);
                    mPlazo = qNivel.PAGOS.Replace(" PAGOS", "");
                    mCuota = Math.Round(Convert.ToDecimal(qPedido.First().MONTO_PAGOS1), 2, MidpointRounding.AwayFromZero);
                    mUltimaCuota = Math.Round(Convert.ToDecimal(qPedido.First().MONTO_PAGOS2), 2, MidpointRounding.AwayFromZero);
                    mCuotasMenosUno = Convert.ToString(Convert.ToInt32(mPlazo) - 1);
                    mCuotas = string.Format("{0} cuotas de:", mCuotasMenosUno);
                    mVendedor = qVendedor.NOMBRE;
                    mFecha = Utilitario.FormatoDDMMYYYY(qPedido.First().FECHA_REGISTRO);
                    mNumeroSolicitud = Convert.ToInt64(qPedido.First().SOLICITUD);
                    mNumeroPagare = Convert.ToInt64(qPedido.First().PAGARE);
                    mFechaDocumento = qPed.FECHA_PEDIDO;
                    mNoCreditoATID = qPedido.First().NUMERO_CREDITO;
                    mUsuario = mArregloCorreo[0];
                    mTienda = qPed.COBRADOR;
                    mFechaPagare = DateTime.Now.Date;
                    mFechaPrimerPago = qPedido.First().FECHA_PRIMER_PAGO == null ? (DateTime)qPed.FECHA_PEDIDO.AddMonths(1) : (DateTime)qPedido.First().FECHA_PRIMER_PAGO; // DateTime.Now.Date.AddDays(30);
                    mNumeroCredito = qPedido.First().NUMERO_CREDITO;
                }
                else
                {

                    if (qCotizacion.Count() == 0)
                    {
                        mSolicitud.PrimerNombre = "Número de cotización inválido.";
                        return mSolicitud;
                    }

                    if (qCotizacion.First().CLIENTE == null)
                    {
                        mSolicitud.PrimerNombre = string.Format("La cotización {0} no tiene cliente asignado.", mCotizacion);
                        return mSolicitud;
                    }

                    mNivel = qCotizacion.First().NIVEL_PRECIO;
                    fechaSolicitud = qCotizacion.First().FECHA_SOLICITUD;
                    var qNivel = (from n in db.MF_NIVEL_PRECIO_COEFICIENTE where n.NIVEL_PRECIO == mNivel select n).First();

                    string mCodigoVendedor = qCotizacion.First().VENDEDOR;
                    var qVendedor = (from v in db.VENDEDOR where v.VENDEDOR1 == mCodigoVendedor select v).First();
                    var qMFVendedor = (from v in db.MF_Vendedor where v.VENDEDOR == mCodigoVendedor select v).First();
                    string[] mArregloCorreo = qMFVendedor.EMAIL.Split(new string[] { "@" }, StringSplitOptions.None);

                    string mCobrador = qCotizacion.First().COBRADOR;
                    mSucursal = (from c in db.MF_Cobrador where c.COBRADOR == mCobrador select c).First().NOMBRE;

                    mCliente = qCotizacion.First().CLIENTE;
                    mPrecioDelBien = Math.Round(Convert.ToDecimal(qCotizacion.First().TOTAL_FACTURAR), 2, MidpointRounding.AwayFromZero);
                    mEnganche = Convert.ToDecimal(qCotizacion.First().ENGANCHE);
                    mSaldoFinanciar = Math.Round(Convert.ToDecimal(qCotizacion.First().SALDO_FINANCIAR), 2, MidpointRounding.AwayFromZero);
                    mIntereses = Math.Round(Convert.ToDecimal(qCotizacion.First().RECARGOS), 2, MidpointRounding.AwayFromZero);
                    mMonto = Math.Round(Convert.ToDecimal(qCotizacion.First().MONTO), 2, MidpointRounding.AwayFromZero);
                    mPlazo = qNivel.PAGOS.Replace(" PAGOS", "");
                    mCuota = Math.Round(Convert.ToDecimal(qCotizacion.First().MONTO_PAGOS1), 2, MidpointRounding.AwayFromZero);
                    mUltimaCuota = Math.Round(Convert.ToDecimal(qCotizacion.First().MONTO_PAGOS2), 2, MidpointRounding.AwayFromZero);
                    mCuotasMenosUno = Convert.ToString(Convert.ToInt32(mPlazo) - 1);
                    mCuotas = string.Format("{0} cuotas de:", mCuotasMenosUno);
                    mVendedor = qVendedor.NOMBRE;
                    mFecha = Utilitario.FormatoDDMMYYYY(qCotizacion.First().FECHA);
                    mNumeroSolicitud = Convert.ToInt64(qCotizacion.First().SOLICITUD);
                    mNumeroPagare = Convert.ToInt64(qCotizacion.First().PAGARE);
                    mFechaDocumento = qCotizacion.First().FECHA;
                    mNoCreditoATID = qCotizacion.First().NUMERO_CREDITO;
                    mUsuario = mArregloCorreo[0];
                    mTienda = qCotizacion.First().COBRADOR;
                    mFechaPagare = qCotizacion.First().FECHA;
                    mFechaPrimerPago = qCotizacion.First().FECHA_PRIMER_PAGO == null ? (DateTime)qCotizacion.First().FECHA.AddMonths(1) : (DateTime)qCotizacion.First().FECHA_PRIMER_PAGO; //DateTime.Now.Date.AddDays(30);
                    mNumeroCredito = qCotizacion.First().NUMERO_CREDITO;
                }
                //Cambio de tasa : TODO: que se persista en la BD la tasa que devuelve ATID.
                if (mFechaDocumento >= new DateTime(2019, 1, 1))
                {
                    if (Int32.Parse(mPlazo) <= 18)
                    {
                        mTasa = "30%";
                        mTasaLetras = "treinta por ciento";
                    }
                    else
                    {
                        mTasa = "36%";
                        mTasaLetras = "treinta y seis por ciento";
                    }
                }

                string mSolicitudTexto = string.Format("No. de solicitud: {0}", mNumeroSolicitud.ToString());
                string mPagareTexto = string.Format("PAGARÉ LIBRE DE PROTESTO NO. {0}", mNumeroPagare.ToString());

                TimeSpan ts = mFechaPrimerPago - DateTime.Now.Date;
                mFechaInicial = mFechaPrimerPago;

                if (ts.Days <= 36) mFechaInicial = mFechaPrimerPago;
                string mMesInicial = Utilitario.Mes(mFechaInicial.Month);

                DateTime mFechaV = mFechaInicial.AddMonths(Convert.ToInt32(mPlazo) - 1);
                string mFechaVence = Utilitario.FormatoDDMMYYYY(mFechaV);
                string mMesVence = Utilitario.Mes(mFechaV.Month);

                var qCli = (from c in db.CLIENTE where c.CLIENTE1 == mCliente select c).First();
                var qCliente = (from c in db.MF_Cliente where c.CLIENTE == mCliente select c).First();

                string mIngresos = qCli.RUBRO11_CLIENTE.Trim().Replace(",", "").Replace(" ", "").Replace("Q", "").Replace("$", "");
                string mFechaLabores = Utilitario.FormatoDDMMYYYY(qCliente.FECHA_INGRESO_LABORES);
                string mFechaNacimiento = Utilitario.FormatoDDMMYYYY(qCliente.FECHA_NACIMIENTO);
                string mDireccionATID = string.Format("{0} {1} {2} {3} {4}", qCliente.CALLE_AVENIDA_FACTURA, qCliente.CASA_FACTURA, qCliente.ZONA_FACTURA == "" || qCliente.ZONA_FACTURA == "--" ? "" : string.Format("Zona {0}", qCliente.ZONA_FACTURA.Substring(0, 1) == "0" ? qCliente.ZONA_FACTURA.Substring(1, 1) : qCliente.ZONA_FACTURA), qCliente.APARTAMENTO_FACTURA, qCliente.COLONIA_FACTURA);
                string mDireccion = string.Format("{0}, {1}, {2}", mDireccionATID, qCliente.MUNICIPIO_FACTURA, qCliente.DEPARTAMENTO_FACTURA);
                string mNombreCompleto = string.Format("{0} {1}{2}{3} {4} {5}", qCliente.PRIMER_NOMBRE, qCliente.SEGUNDO_NOMBRE, qCliente.TERCER_NOMBRE.Trim().Length == 0 ? " " : "", qCliente.TERCER_NOMBRE, qCliente.PRIMER_APELLIDO, qCliente.SEGUNDO_APELLIDO);

                string mFechaNacimientoConyugue = "";
                try
                {
                    mFechaNacimientoConyugue = Utilitario.FormatoDDMMYYYY(qCliente.CONYUGUE_FECHA_NACIMIENTO);
                    if (mFechaNacimientoConyugue.Trim().Length < 5) mFechaNacimientoConyugue = "";
                }
                catch
                {
                    mFechaNacimientoConyugue = "";
                }

                string[] mArregloCalleAvenida = qCliente.CALLE_AVENIDA_FACTURA.Split(new string[] { " " }, StringSplitOptions.None);
                string[] mArregloCasa = qCliente.CASA_FACTURA.Split(new string[] { "-" }, StringSplitOptions.None);

                decimal mEdadDecimal = Convert.ToDecimal((DateTime.Now.Date - Convert.ToDateTime(qCliente.FECHA_NACIMIENTO)).TotalDays / 365);
                int mEdad = 0;
                try
                {
                    mEdad = Convert.ToInt32(mEdadDecimal.ToString().Substring(0, mEdadDecimal.ToString().IndexOf(".")));
                }
                catch
                {
                    mEdad = Convert.ToInt32(mEdadDecimal.ToString());
                }

                EntityConnection ec = (EntityConnection)db.Connection;
                SqlConnection sc = (SqlConnection)ec.StoreConnection;

                SqlConnection mConexion = new SqlConnection(sc.ConnectionString);
                SqlCommand mCommand = new SqlCommand();

                mConexion.Open();
                mCommand.Connection = mConexion;

                mCommand.CommandText = string.Format("SELECT prodmult.fcNumerosLetras({0})", mEdad);
                string mEdadString = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = string.Format("SELECT prodmult.fcNumerosLetras({0})", mSaldoFinanciar);
                string mSaldoFinanciarLetras = Convert.ToString(mCommand.ExecuteScalar()).Replace("CON", "QUETZALES CON");

                mCommand.CommandText = string.Format("SELECT prodmult.fcNumerosLetras({0})", mFechaV.Year.ToString());
                string mAnioVence = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = string.Format("SELECT prodmult.fcNumerosLetras({0})", mFechaV.Day.ToString());
                string mDiaVence = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = string.Format("SELECT prodmult.fcNumerosLetras({0})", mCuota.ToString());
                string mCuotasLetras = Convert.ToString(mCommand.ExecuteScalar()).Replace("CON", "QUETZALES CON");

                mCommand.CommandText = string.Format("SELECT prodmult.fcNumerosLetras({0})", mUltimaCuota.ToString());
                string mUltimaCuotaLetras = Convert.ToString(mCommand.ExecuteScalar()).Replace("CON", "QUETZALES CON");

                mCommand.CommandText = string.Format("SELECT prodmult.fcNumerosLetras({0})", mFechaInicial.Year.ToString());
                string mAnioInicial = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = string.Format("SELECT prodmult.fcNumerosLetras({0})", mFechaInicial.Day.ToString());
                string mDiaInicial = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = string.Format("SELECT prodmult.fcNumerosLetras({0})", mCuotasMenosUno);
                string mCuotasMenosUnoLetras = Convert.ToString(mCommand.ExecuteScalar());

                mCommand.CommandText = string.Format("SELECT prodmult.fcNumerosLetras({0})", mIngresos);
                string mIngresosLetras = Convert.ToString(mCommand.ExecuteScalar()).Replace("CON", "QUETZALES CON");

                string mNumeroString = "";
                string mDireccionLetras = "";

                for (int ii = 0; ii < mDireccion.Length; ii++)
                {
                    mDireccionLetras = string.Format("{0}{1}", mDireccionLetras, mDireccion.Substring(ii, 1));

                    try
                    {
                        int mNumero = Convert.ToInt32(mDireccion.Substring(ii, 1));
                        mNumeroString = string.Format("{0}{1}", mNumeroString, mNumero.ToString());
                    }
                    catch
                    {
                        if (mNumeroString.Trim().Length > 0)
                        {
                            mCommand.CommandText = string.Format("SELECT prodmult.fcNumerosLetras({0})", mNumeroString);
                            string mNumeroLetras = Convert.ToString(mCommand.ExecuteScalar());

                            mNumeroLetras = mNumeroLetras.Replace(" CON ", "&");
                            mNumeroLetras = mNumeroLetras.Substring(0, mNumeroLetras.IndexOf("&"));
                            //if (mNumeroLetras.Substring(mNumeroLetras.Length - 3, 3) == "UNO") mNumeroLetras = mNumeroLetras.Substring(0, mNumeroLetras.Length - 1);

                            mDireccionLetras = mDireccionLetras.Replace(mNumeroString, mNumeroLetras);
                        }
                        mNumeroString = "";
                    }
                }

                mDireccionLetras = mDireccionLetras.ToUpper().Replace("-", " GUION ");

                mCommand.CommandText = string.Format("SELECT prodmult.fcNumerosLetras({0})", mPlazo);
                string mPlazoLetras = Convert.ToString(mCommand.ExecuteScalar());

                mConexion.Close();

                mIngresos = Utilitario.FormatoNumeroDecimal(Convert.ToDecimal(mIngresos));
                string mEgresos = qCli.RUBRO12_CLIENTE.Trim().Replace(",", "").Replace(" ", "").Replace("Q", "").Replace("$", "");
                mEgresos = Utilitario.FormatoNumeroDecimal(Convert.ToDecimal(mEgresos));

                mEdadString = mEdadString.Replace(" CON ", "&");
                mEdadString = mEdadString.Substring(0, mEdadString.IndexOf("&"));
                if (mEdadString.Substring(mEdadString.Length - 3, 3) == "UNO") mEdadString = mEdadString.Substring(0, mEdadString.Length - 1);

                mPlazoLetras = mPlazoLetras.Replace(" CON ", "&");
                mPlazoLetras = mPlazoLetras.Substring(0, mPlazoLetras.IndexOf("&"));
                if (mPlazoLetras.Substring(mPlazoLetras.Length - 3, 3) == "UNO") mPlazoLetras = mPlazoLetras.Substring(0, mPlazoLetras.Length - 1);

                mCuotasMenosUnoLetras = mCuotasMenosUnoLetras.Replace(" CON ", "&");
                mCuotasMenosUnoLetras = mCuotasMenosUnoLetras.Substring(0, mCuotasMenosUnoLetras.IndexOf("&"));

                mAnioVence = mAnioVence.Replace(" CON ", "&");
                mAnioVence = mAnioVence.Substring(0, mAnioVence.IndexOf("&"));

                mDiaVence = mDiaVence.Replace(" CON ", "&");
                mDiaVence = mDiaVence.Substring(0, mDiaVence.IndexOf("&"));

                string mFechaVenceLetras = string.Format("{0} de {1} del año {2}", mDiaVence, mMesVence, mAnioVence);

                mAnioInicial = mAnioInicial.Replace(" CON ", "&");
                mAnioInicial = mAnioInicial.Substring(0, mAnioInicial.IndexOf("&"));

                mDiaInicial = mDiaInicial.Replace(" CON ", "&");
                mDiaInicial = mDiaInicial.Substring(0, mDiaInicial.IndexOf("&"));

                string mFechaLetras = string.Format("{0} de {1} de {2}", DateTime.Now.Date.Day.ToString(), Utilitario.Mes(DateTime.Now.Date), DateTime.Now.Date.Year.ToString());
                string mFechaInicialLetras = string.Format("{0} ({1}) de {2} del año {3} ({4})", mDiaInicial, mFechaInicial.Day.ToString(), mMesInicial, mAnioInicial, mFechaInicial.Year.ToString());

                mEdadString = mEdadString.ToLower();
                mSaldoFinanciarLetras = mSaldoFinanciarLetras.ToLower();
                mSaldoFinanciarFormato = Utilitario.FormatoNumeroDecimal(mSaldoFinanciar);
                mCuotaFormato = Utilitario.FormatoNumeroDecimal(mCuota);
                mUltimaCuotaFormato = Utilitario.FormatoNumeroDecimal(mUltimaCuota);
                mPlazoLetras = mPlazoLetras.ToLower();
                mFechaVenceLetras = mFechaVenceLetras.ToLower();
                mCuotasLetras = mCuotasLetras.ToLower();
                mUltimaCuotaLetras = mUltimaCuotaLetras.ToLower();
                mFechaInicialLetras = mFechaInicialLetras.ToLower();

                int mDependientes = 0;
                try
                {
                    string mCargasFamiliares = qCliente.CARGAS_FAMILIARES;
                    mDependientes = Convert.ToInt32(mCargasFamiliares.Trim().Replace(" ", "").Replace(".", "").Replace(",", "").Replace("/", "").Replace("-", "").Replace("_", ""));
                }
                catch
                {
                    mDependientes = 0;
                }

                int mCodigoProfesion = 0; string mProfesion = qCliente.PROFESION;
                try
                {
                    mCodigoProfesion = (int)(from p in db.MF_Profesion where p.DESCRIPCION == mProfesion select p).First().ATID_PROFESION;
                }
                catch
                {
                    mCodigoProfesion = 0;
                }
                mProfesion = mProfesion.Replace("(O)", "").Replace("(o)", "").Replace("(A)", "").Replace("(a)", "").Replace(" ", "").Trim();

                int mCodigoPuesto = 0; string mPuesto = qCli.RUBRO4_CLIENTE;
                try
                {
                    mCodigoPuesto = (int)(from p in db.MF_Puesto where p.DESCRIPCION == mPuesto select p).First().ATID_OCUPACION;
                }
                catch
                {
                    mCodigoPuesto = 0;
                }

                int mCodigoMunicipo = 0; string mMunicipio = qCliente.MUNICIPIO_FACTURA;
                try
                {
                    mCodigoMunicipo = (int)(from m in db.MF_Municipio where m.DESCRIPCION == mMunicipio select m).First().ATID_MUNICIPIO;
                }
                catch
                {
                    mCodigoMunicipo = 0;
                }

                int mCodigoDepartamento = 0; string mDepartamento = qCliente.DEPARTAMENTO_FACTURA;
                try
                {
                    mCodigoDepartamento = (int)(from m in db.MF_Departamento where m.DESCRIPCION == mDepartamento select m).First().ATID_DEPARTAMENTO;
                }
                catch
                {
                    mCodigoDepartamento = 0;
                }

                int mCodigoMunicipoEmpresa = 0; string mMunicipioEmpresa = qCliente.MUNICIPIO_EMPRESA;
                try
                {
                    mCodigoMunicipoEmpresa = (int)(from m in db.MF_Municipio where m.DESCRIPCION == mMunicipioEmpresa select m).First().ATID_MUNICIPIO;
                }
                catch
                {
                    mCodigoMunicipoEmpresa = 0;
                }

                int mCodigoDepartamentoEmpresa = 0; string mDepartamentoEmpresa = qCliente.DEPARTAMENTO_EMPRESA;
                try
                {
                    mCodigoDepartamentoEmpresa = (int)(from m in db.MF_Departamento where m.DESCRIPCION == mDepartamentoEmpresa select m).First().ATID_DEPARTAMENTO;
                }
                catch
                {
                    mCodigoDepartamentoEmpresa = 0;
                }


                int mFamiliar1EmpresaDepartamentoCodigo = 0; string mFamiliar1EmpresaDepartamento = "";
                try
                {
                    mFamiliar1EmpresaDepartamento = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "F1" select c).First().EMPRESA_DEPARTAMENTO;
                    mFamiliar1EmpresaDepartamentoCodigo = (int)(from m in db.MF_Municipio where m.DESCRIPCION == mFamiliar1EmpresaDepartamento select m).First().ATID_DEPARTAMENTO;
                }
                catch
                {
                    mFamiliar1EmpresaDepartamento = "";
                    mFamiliar1EmpresaDepartamentoCodigo = 0;
                }

                int mFamiliar1EmpresaMunicipioCodigo = 0; string mFamiliar1EmpresaMunicipio = "";
                try
                {
                    mFamiliar1EmpresaMunicipio = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "F1" select c).First().EMPRESA_MUNICIPIO;
                    mFamiliar1EmpresaMunicipioCodigo = (int)(from m in db.MF_Municipio where m.DESCRIPCION == mFamiliar1EmpresaMunicipio select m).First().ATID_MUNICIPIO;
                }
                catch
                {
                    mFamiliar1EmpresaMunicipio = "";
                    mFamiliar1EmpresaMunicipioCodigo = 0;
                }
                
                int mFamiliar1CasaDepartamentoCodigo = 0; string mFamiliar1CasaDepartamento = "";
                try
                {
                    mFamiliar1CasaDepartamento = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "F1" select c).First().CASA_DEPARTAMENTO;
                    mFamiliar1CasaDepartamentoCodigo = (int)(from m in db.MF_Municipio where m.DESCRIPCION == mFamiliar1CasaDepartamento select m).First().ATID_DEPARTAMENTO;
                }
                catch
                {
                    mFamiliar1CasaDepartamento = "";
                    mFamiliar1CasaDepartamentoCodigo = 0;
                }

                int mFamiliar1CasaMunicipioCodigo = 0; string mFamiliar1CasaMunicipio = "";
                try
                {
                    mFamiliar1CasaMunicipio = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "F1" select c).First().CASA_MUNICIPIO;
                    mFamiliar1CasaMunicipioCodigo = (int)(from m in db.MF_Municipio where m.DESCRIPCION == mFamiliar1CasaMunicipio select m).First().ATID_MUNICIPIO;
                }
                catch
                {
                    mFamiliar1CasaMunicipio = "";
                    mFamiliar1CasaMunicipioCodigo = 0;
                }

                string mFamiliar1Tipo = ""; string mFamiliar1TipoDescripcion = "";
                try
                {
                    mFamiliar1Tipo = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "F1" select c).First().TIPO;
                    mFamiliar1TipoDescripcion = (from c in db.MF_Catalogo where c.CODIGO_TABLA == 56 && c.CODIGO_CAT == mFamiliar1Tipo select c).First().NOMBRE_CAT;
                }
                catch
                {
                    mFamiliar1Tipo = "";
                    mFamiliar1TipoDescripcion = "";
                }

                int mFamiliar2EmpresaDepartamentoCodigo = 0; string mFamiliar2EmpresaDepartamento = "";
                try
                {
                    mFamiliar2EmpresaDepartamento = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "F2" select c).First().EMPRESA_DEPARTAMENTO;
                    mFamiliar2EmpresaDepartamentoCodigo = (int)(from m in db.MF_Municipio where m.DESCRIPCION == mFamiliar2EmpresaDepartamento select m).First().ATID_DEPARTAMENTO;
                }
                catch
                {
                    mFamiliar2EmpresaDepartamento = "";
                    mFamiliar2EmpresaDepartamentoCodigo = 0;
                }

                int mFamiliar2EmpresaMunicipioCodigo = 0; string mFamiliar2EmpresaMunicipio = "";
                try
                {
                    mFamiliar2EmpresaMunicipio = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "F2" select c).First().EMPRESA_MUNICIPIO;
                    mFamiliar2EmpresaMunicipioCodigo = (int)(from m in db.MF_Municipio where m.DESCRIPCION == mFamiliar2EmpresaMunicipio select m).First().ATID_MUNICIPIO;
                }
                catch
                {
                    mFamiliar2EmpresaMunicipio = "";
                    mFamiliar2EmpresaMunicipioCodigo = 0;
                }

                int mFamiliar2CasaDepartamentoCodigo = 0; string mFamiliar2CasaDepartamento = "";
                try
                {
                    mFamiliar2CasaDepartamento = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "F2" select c).First().CASA_DEPARTAMENTO;
                    mFamiliar2CasaDepartamentoCodigo = (int)(from m in db.MF_Municipio where m.DESCRIPCION == mFamiliar2CasaDepartamento select m).First().ATID_DEPARTAMENTO;
                }
                catch
                {
                    mFamiliar2CasaDepartamento = "";
                    mFamiliar2CasaDepartamentoCodigo = 0;
                }

                int mFamiliar2CasaMunicipioCodigo = 0; string mFamiliar2CasaMunicipio = "";
                try
                {
                    mFamiliar2CasaMunicipio = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "F2" select c).First().EMPRESA_MUNICIPIO;
                    mFamiliar2CasaMunicipioCodigo = (int)(from m in db.MF_Municipio where m.DESCRIPCION == mFamiliar2CasaMunicipio select m).First().ATID_MUNICIPIO;
                }
                catch
                {
                    mFamiliar2CasaMunicipio = "";
                    mFamiliar2CasaMunicipioCodigo = 0;
                }

                string mFamiliar2Tipo = ""; string mFamiliar2TipoDescripcion = "";
                try
                {
                    mFamiliar2Tipo = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "F2" select c).First().TIPO;
                    mFamiliar2TipoDescripcion = (from c in db.MF_Catalogo where c.CODIGO_TABLA == 56 && c.CODIGO_CAT == mFamiliar2Tipo select c).First().NOMBRE_CAT;
                }
                catch
                {
                    mFamiliar2Tipo = "";
                    mFamiliar2TipoDescripcion = "";
                }

                int mPersonal1EmpresaDepartamentoCodigo = 0; string mPersonal1EmpresaDepartamento = "";
                try
                {
                    mPersonal1EmpresaDepartamento = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "P1" select c).First().EMPRESA_DEPARTAMENTO;
                    mPersonal1EmpresaDepartamentoCodigo = (int)(from m in db.MF_Municipio where m.DESCRIPCION == mPersonal1EmpresaDepartamento select m).First().ATID_DEPARTAMENTO;
                }
                catch
                {
                    mPersonal1EmpresaDepartamento = "";
                    mPersonal1EmpresaDepartamentoCodigo = 0;
                }

                int mPersonal1EmpresaMunicipioCodigo = 0; string mPersonal1EmpresaMunicipio = "";
                try
                {
                    mPersonal1EmpresaMunicipio = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "P1" select c).First().EMPRESA_MUNICIPIO;
                    mPersonal1EmpresaMunicipioCodigo = (int)(from m in db.MF_Municipio where m.DESCRIPCION == mPersonal1EmpresaMunicipio select m).First().ATID_MUNICIPIO;
                }
                catch
                {
                    mPersonal1EmpresaMunicipio = "";
                    mPersonal1EmpresaMunicipioCodigo = 0;
                }

                int mPersonal1CasaDepartamentoCodigo = 0; string mPersonal1CasaDepartamento = "";
                try
                {
                    mPersonal1CasaDepartamento = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "P1" select c).First().CASA_DEPARTAMENTO;
                    mPersonal1CasaDepartamentoCodigo = (int)(from m in db.MF_Municipio where m.DESCRIPCION == mPersonal1CasaDepartamento select m).First().ATID_DEPARTAMENTO;
                }
                catch
                {
                    mPersonal1CasaDepartamento = "";
                    mPersonal1CasaDepartamentoCodigo = 0;
                }

                int mPersonal1CasaMunicipioCodigo = 0; string mPersonal1CasaMunicipio = "";
                try
                {
                    mPersonal1CasaMunicipio = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "P1" select c).First().EMPRESA_MUNICIPIO;
                    mPersonal1CasaMunicipioCodigo = (int)(from m in db.MF_Municipio where m.DESCRIPCION == mPersonal1CasaMunicipio select m).First().ATID_MUNICIPIO;
                }
                catch
                {
                    mPersonal1CasaMunicipio = "";
                    mPersonal1CasaMunicipioCodigo = 0;
                }

                string mPersonal1Tipo = ""; string mPersonal1TipoDescripcion = "";
                try
                {
                    mPersonal1Tipo = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "P1" select c).First().TIPO;
                    mPersonal1TipoDescripcion = (from c in db.MF_Catalogo where c.CODIGO_TABLA == 57 && c.CODIGO_CAT == mPersonal1Tipo select c).First().NOMBRE_CAT;
                }
                catch
                {
                    mPersonal1Tipo = "";
                    mPersonal1TipoDescripcion = "";
                }

                int mPersonal2EmpresaDepartamentoCodigo = 0; string mPersonal2EmpresaDepartamento = "";
                try
                {
                    mPersonal2EmpresaDepartamento = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "P2" select c).First().EMPRESA_DEPARTAMENTO;
                    mPersonal2EmpresaDepartamentoCodigo = (int)(from m in db.MF_Municipio where m.DESCRIPCION == mPersonal2EmpresaDepartamento select m).First().ATID_DEPARTAMENTO;
                }
                catch
                {
                    mPersonal2EmpresaDepartamento = "";
                    mPersonal2EmpresaDepartamentoCodigo = 0;
                }

                int mPersonal2EmpresaMunicipioCodigo = 0; string mPersonal2EmpresaMunicipio = "";
                try
                {
                    mPersonal2EmpresaMunicipio = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "P2" select c).First().EMPRESA_MUNICIPIO;
                    mPersonal2EmpresaMunicipioCodigo = (int)(from m in db.MF_Municipio where m.DESCRIPCION == mPersonal2EmpresaMunicipio select m).First().ATID_MUNICIPIO;
                }
                catch
                {
                    mPersonal2EmpresaMunicipio = "";
                    mPersonal2EmpresaMunicipioCodigo = 0;
                }
                
                int mPersonal2CasaDepartamentoCodigo = 0; string mPersonal2CasaDepartamento = "";
                try
                {
                    mPersonal2CasaDepartamento = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "P2" select c).First().CASA_DEPARTAMENTO;
                    mPersonal2CasaDepartamentoCodigo = (int)(from m in db.MF_Municipio where m.DESCRIPCION == mPersonal2CasaDepartamento select m).First().ATID_DEPARTAMENTO;
                }
                catch
                {
                    mPersonal2CasaDepartamento = "";
                    mPersonal2CasaDepartamentoCodigo = 0;
                }

                int mPersonal2CasaMunicipioCodigo = 0; string mPersonal2CasaMunicipio = "";
                try
                {
                    mPersonal2CasaMunicipio = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "P2" select c).First().EMPRESA_MUNICIPIO;
                    mPersonal2CasaMunicipioCodigo = (int)(from m in db.MF_Municipio where m.DESCRIPCION == mPersonal2CasaMunicipio select m).First().ATID_MUNICIPIO;
                }
                catch
                {
                    mPersonal2CasaMunicipio = "";
                    mPersonal2CasaMunicipioCodigo = 0;
                }

                string mPersonal2Tipo = ""; string mPersonal2TipoDescripcion = "";
                try
                {
                    mPersonal2Tipo = (from c in db.MF_ClienteReferencia where c.CLIENTE == mCliente && c.TIPO_REFERENCIA == "P2" select c).First().TIPO;
                    mPersonal2TipoDescripcion = (from c in db.MF_Catalogo where c.CODIGO_TABLA == 57 && c.CODIGO_CAT == mPersonal2Tipo select c).First().NOMBRE_CAT;
                }
                catch
                {
                    mPersonal2Tipo = "";
                    mPersonal2TipoDescripcion = "";
                }

                
                try
                {
                    int mLong = mNumeroCredito.Trim().Length;
                }
                catch
                {
                    mNumeroCredito = "";
                }
                
                string[] mArregloFechaLabores = mFechaLabores.Split(new string[] { "/" }, StringSplitOptions.None);
                string mFechaInicioLabores = string.Format("{0} de {1} de {2}", mArregloFechaLabores[0], Utilitario.Mes(Convert.ToInt32(mArregloFechaLabores[1])), mArregloFechaLabores[2]);

                Byte[] mFoto = null; Byte[] mHuella = null;
                var qImagenes = from c in db.MF_FotoCliente where c.Cliente == mCliente select c;
                if (qImagenes.Count() > 0)
                {
                    mFoto = qImagenes.First().Foto;
                    mHuella = qImagenes.First().Huella;
                }

                //numeroSolicitud":"00000000323"
                string mCeros = ""; int mLongitudNoCredito = 0;
                try
                {
                    mLongitudNoCredito = mNoCreditoATID.Length;
                }
                catch
                {
                    mNoCreditoATID = "";
                }

                switch (mLongitudNoCredito)
                {
                    case 1:
                        mCeros = "000000000";
                        break;
                    case 2:
                        mCeros = "00000000";
                        break;
                    case 3:
                        mCeros = "0000000";
                        break;
                    case 4:
                        mCeros = "000000";
                        break;
                    case 5:
                        mCeros = "00000";
                        break;
                    case 6:
                        mCeros = "0000";
                        break;
                    case 7:
                        mCeros = "000";
                        break;
                    case 8:
                        mCeros = "00";
                        break;
                    case 9:
                        mCeros = "0";
                        break;
                    default:
                        mCeros = "";
                        break;
                }
                
                mNoCreditoATID = string.Format("{0}{1}", mCeros, mNoCreditoATID);
                int mCanton = 0; int mCantonEmpresa = 0;
                //var qDatos = (from d in db.MF_FotoCliente where d.Cliente == "0022730" select d);
                
                var q = from c in db.CLIENTE
                        join mfc in db.MF_Cliente on c.CLIENTE1 equals mfc.CLIENTE
                        join refCO in db.MF_ClienteReferencia on new { mfc.CLIENTE, TIPO_REFERENCIA = "CO" } equals new { refCO.CLIENTE, refCO.TIPO_REFERENCIA } into con
                        join refF1 in db.MF_ClienteReferencia on new { mfc.CLIENTE, TIPO_REFERENCIA = "F1" } equals new { refF1.CLIENTE, refF1.TIPO_REFERENCIA } into fam1
                        join refF2 in db.MF_ClienteReferencia on new { mfc.CLIENTE, TIPO_REFERENCIA = "F2" } equals new { refF2.CLIENTE, refF2.TIPO_REFERENCIA } into fam2
                        join refP1 in db.MF_ClienteReferencia on new { mfc.CLIENTE, TIPO_REFERENCIA = "P1" } equals new { refP1.CLIENTE, refP1.TIPO_REFERENCIA } into per1
                        join refP2 in db.MF_ClienteReferencia on new { mfc.CLIENTE, TIPO_REFERENCIA = "P2" } equals new { refP2.CLIENTE, refP2.TIPO_REFERENCIA } into per2
                        join refJE in db.MF_ClienteReferencia on new { mfc.CLIENTE, TIPO_REFERENCIA = "JE" } equals new { refJE.CLIENTE, refJE.TIPO_REFERENCIA } into je
                        from conyugue in con.DefaultIfEmpty()
                        from familiar1 in fam1.DefaultIfEmpty()
                        from familiar2 in fam2.DefaultIfEmpty()
                        from personal1 in per1.DefaultIfEmpty()
                        from personal2 in per2.DefaultIfEmpty()
                        from jefe in je.DefaultIfEmpty()
                        where c.CLIENTE1 == mCliente
                        select new Solicitud()
                        {
                            FechaSolicitud = fechaSolicitud,
                            ClienteMF = c.CLIENTE1,
                            NumeroSolicitud = mNumeroSolicitud,
                            NumeroPagare = mNumeroPagare,
                            SolicitudTexto = mSolicitudTexto,
                            PagareTexto = mPagareTexto,
                            PrimerNombre = mfc.PRIMER_NOMBRE,
                            SegundoNombre = mfc.SEGUNDO_NOMBRE,
                            TercerNombre = mfc.TERCER_NOMBRE,
                            PrimerApellido = mfc.PRIMER_APELLIDO,
                            SegundoApellido = mfc.SEGUNDO_APELLIDO,
                            ApellidoCasada = mfc.APELLIDO_CASADA,
                            DPI = mfc.DPI,
                            Nit = mfc.NIT_FACTURA,
                            FechaNacimiento = mFechaNacimiento,
                            Genero = mfc.GENERO == "H" ? "Masculino" : "Femenino",
                            Profesion = mProfesion,
                            Nacionalidad = mfc.NACIONALIDAD.Substring(0, 5) == "GUATE" ? mfc.GENERO == "H" ? "Guatemalteco" : "Guatemalteca" : mfc.NACIONALIDAD,
                            EstadoCivil = mfc.ESTADO_CIVIL == "S" ? mfc.GENERO == "H" ? "Soltero" : "Soltera" : mfc.ESTADO_CIVIL == "C" ? mfc.GENERO == "H" ? "Casado" : "Casada" : mfc.ESTADO_CIVIL == "U" ? mfc.GENERO == "H" ? "Unido" : "Unida" : mfc.ESTADO_CIVIL == "D" ? mfc.GENERO == "H" ? "Divorciado" : "Divorciada" : mfc.GENERO == "H" ? "Viudo" : "Viuda",
                            Email = c.E_MAIL,
                            Direccion = mDireccion,
                            Municipio = mfc.MUNICIPIO_FACTURA,
                            Departamento = mfc.DEPARTAMENTO_FACTURA,
                            Telefono = c.TELEFONO1,
                            Celular = mfc.CELULAR,
                            TipoVivienda = mfc.VIVE_EN, //A = Alquildada; F = Familiares; P = Propia; N = Ninguna
                            Empresa = c.RUBRO1_CLIENTE,
                            Puesto = c.RUBRO4_CLIENTE,
                            FechaIngreso = mFechaLabores,
                            JefePrimerNombre = jefe.PRIMER_NOMBRE == null ? "" : jefe.PRIMER_NOMBRE,
                            JefeSegundoNombre = jefe.SEGUNDO_NOMBRE == null ? "" : jefe.SEGUNDO_NOMBRE,
                            JefeTercerNombre = jefe.TERCER_NOMBRE == null ? "" : jefe.TERCER_NOMBRE,
                            JefePrimerApellido = jefe.PRIMER_APELLIDO == null ? "" : jefe.PRIMER_APELLIDO,
                            JefeSegundoApellido = jefe.SEGUNDO_APELLIDO == null ? "" : jefe.SEGUNDO_APELLIDO,
                            JefeApellidoCasada = jefe.APELLIDO_CASADA == null ? "" : jefe.APELLIDO_CASADA,
                            Ingresos = mIngresos,
                            Egresos = mEgresos,
                            EmpresaTelefono = c.RUBRO3_CLIENTE,
                            EmpresaExtension = mfc.EXTENSION,
                            EmpresaDireccion = mfc.DIRECCION_EMPRESA,
                            EmpresaMunicipio = mfc.MUNICIPIO_EMPRESA,
                            EmpresaDepartamento = mfc.DEPARTAMENTO_EMPRESA,
                            Familiar1PrimerNombre = familiar1.PRIMER_NOMBRE == null ? "" : familiar1.PRIMER_NOMBRE,
                            Familiar1SegundoNombre = familiar1.SEGUNDO_NOMBRE == null ? "" : familiar1.SEGUNDO_NOMBRE,
                            Familiar1TercerNombre = familiar1.TERCER_NOMBRE == null ? "" : familiar1.TERCER_NOMBRE,
                            Familiar1PrimerApellido = familiar1.PRIMER_APELLIDO == null ? "" : familiar1.PRIMER_APELLIDO,
                            Familiar1SegundoApellido = familiar1.SEGUNDO_APELLIDO == null ? "" : familiar1.SEGUNDO_APELLIDO,
                            Familiar1ApellidoCasada = familiar1.APELLIDO_CASADA == null ? "" : familiar1.APELLIDO_CASADA,
                            Familiar1Celular = familiar1.CELULAR == null ? "" : familiar1.CELULAR,
                            Familiar1Tipo = mFamiliar1Tipo,
                            Familiar1TipoDescripcion = mFamiliar1TipoDescripcion,
                            Familiar1Empresa = familiar1.EMPRESA == null ? "" : familiar1.EMPRESA,
                            Familiar1EmpresaDireccion = familiar1.EMPRESA_DIRECCION == null ? "" : familiar1.EMPRESA_DIRECCION,
                            Familiar1EmpresaDepartamento = mFamiliar1EmpresaDepartamento,
                            Familiar1EmpresaDepartamentoCodigo = mFamiliar1EmpresaDepartamentoCodigo,
                            Familiar1EmpresaMunicipio = mFamiliar1EmpresaMunicipio,
                            Familiar1EmpresaMunicipioCodigo = mFamiliar1EmpresaMunicipioCodigo,
                            Familiar1EmpresaTelefono = familiar1.TELEFONO_TRABAJO == null ? "" : familiar1.TELEFONO_TRABAJO,
                            Familiar1CasaDireccion = familiar1.CASA_DIRECCION == null ? "" : familiar1.CASA_DIRECCION,
                            Familiar1CasaDepartamento = mFamiliar1CasaDepartamento,
                            Familiar1CasaDepartamentoCodigo = mFamiliar1CasaDepartamentoCodigo,
                            Familiar1CasaMunicipio = mFamiliar1CasaMunicipio,
                            Familiar1CasaMunicipioCodigo = mFamiliar1CasaMunicipioCodigo,
                            Familiar1CasaTelefono = familiar1.TELEFONO_CASA == null ? "" : familiar1.TELEFONO_CASA,
                            Familiar2PrimerNombre = familiar2.PRIMER_NOMBRE == null ? "" : familiar2.PRIMER_NOMBRE,
                            Familiar2SegundoNombre = familiar2.SEGUNDO_NOMBRE == null ? "" : familiar2.SEGUNDO_NOMBRE,
                            Familiar2TercerNombre = familiar2.TERCER_NOMBRE == null ? "" : familiar2.TERCER_NOMBRE,
                            Familiar2PrimerApellido = familiar2.PRIMER_APELLIDO == null ? "" : familiar2.PRIMER_APELLIDO,
                            Familiar2SegundoApellido = familiar2.SEGUNDO_APELLIDO == null ? "" : familiar2.SEGUNDO_APELLIDO,
                            Familiar2ApellidoCasada = familiar2.APELLIDO_CASADA == null ? "" : familiar2.APELLIDO_CASADA,
                            Familiar2Celular = familiar2.CELULAR == null ? "" : familiar2.CELULAR,
                            Familiar2Tipo = mFamiliar2Tipo,
                            Familiar2TipoDescripcion = mFamiliar2TipoDescripcion,
                            Familiar2Empresa = familiar2.EMPRESA == null ? "" : familiar2.EMPRESA,
                            Familiar2EmpresaDireccion = familiar2.EMPRESA_DIRECCION == null ? "" : familiar2.EMPRESA_DIRECCION,
                            Familiar2EmpresaDepartamento = mFamiliar2EmpresaDepartamento,
                            Familiar2EmpresaDepartamentoCodigo = mFamiliar2EmpresaDepartamentoCodigo,
                            Familiar2EmpresaMunicipio = mFamiliar2EmpresaMunicipio,
                            Familiar2EmpresaMunicipioCodigo = mFamiliar2EmpresaMunicipioCodigo,
                            Familiar2EmpresaTelefono = familiar2.TELEFONO_TRABAJO == null ? "" : familiar2.TELEFONO_TRABAJO,
                            Familiar2CasaDireccion = familiar2.CASA_DIRECCION == null ? "" : familiar2.CASA_DIRECCION,
                            Familiar2CasaDepartamento = mFamiliar2CasaDepartamento,
                            Familiar2CasaDepartamentoCodigo = mFamiliar2CasaDepartamentoCodigo,
                            Familiar2CasaMunicipio = mFamiliar2CasaMunicipio,
                            Familiar2CasaMunicipioCodigo = mFamiliar2CasaMunicipioCodigo,
                            Familiar2CasaTelefono = familiar2.TELEFONO_CASA == null ? "" : familiar2.TELEFONO_CASA,
                            Personal1PrimerNombre = personal1.PRIMER_NOMBRE == null ? "" : personal1.PRIMER_NOMBRE,
                            Personal1SegundoNombre = personal1.SEGUNDO_NOMBRE == null ? "" : personal1.SEGUNDO_NOMBRE,
                            Personal1TercerNombre = personal1.TERCER_NOMBRE == null ? "" : personal1.TERCER_NOMBRE,
                            Personal1PrimerApellido = personal1.PRIMER_APELLIDO == null ? "" : personal1.PRIMER_APELLIDO,
                            Personal1SegundoApellido = personal1.SEGUNDO_APELLIDO == null ? "" : personal1.SEGUNDO_APELLIDO,
                            Personal1ApellidoCasada = personal1.APELLIDO_CASADA == null ? "" : personal1.APELLIDO_CASADA,
                            Personal1Celular = personal1.CELULAR == null ? "" : personal1.CELULAR,
                            Personal1Tipo = mPersonal1Tipo,
                            Personal1TipoDescripcion = mPersonal1TipoDescripcion,
                            Personal1Empresa = personal1.EMPRESA == null ? "" : personal1.EMPRESA,
                            Personal1EmpresaDireccion = personal1.EMPRESA_DIRECCION == null ? "" : personal1.EMPRESA_DIRECCION,
                            Personal1EmpresaDepartamento = mPersonal1EmpresaDepartamento,
                            Personal1EmpresaDepartamentoCodigo = mPersonal1EmpresaDepartamentoCodigo,
                            Personal1EmpresaMunicipio = mPersonal1EmpresaMunicipio,
                            Personal1EmpresaMunicipioCodigo = mPersonal1EmpresaMunicipioCodigo,
                            Personal1EmpresaTelefono = personal1.TELEFONO_TRABAJO == null ? "" : personal1.TELEFONO_TRABAJO,
                            Personal1CasaDireccion = personal1.CASA_DIRECCION == null ? "" : personal1.CASA_DIRECCION,
                            Personal1CasaDepartamento = mPersonal1CasaDepartamento,
                            Personal1CasaDepartamentoCodigo = mPersonal1CasaDepartamentoCodigo,
                            Personal1CasaMunicipio = mPersonal1CasaMunicipio,
                            Personal1CasaMunicipioCodigo = mPersonal1CasaMunicipioCodigo,
                            Personal1CasaTelefono = personal1.TELEFONO_CASA == null ? "" : personal1.TELEFONO_CASA,
                            Personal2PrimerNombre = personal2.PRIMER_NOMBRE == null ? "" : personal2.PRIMER_NOMBRE,
                            Personal2SegundoNombre = personal2.SEGUNDO_NOMBRE == null ? "" : personal2.SEGUNDO_NOMBRE,
                            Personal2TercerNombre = personal2.TERCER_NOMBRE == null ? "" : personal2.TERCER_NOMBRE,
                            Personal2PrimerApellido = personal2.PRIMER_APELLIDO == null ? "" : personal2.PRIMER_APELLIDO,
                            Personal2SegundoApellido = personal2.SEGUNDO_APELLIDO == null ? "" : personal2.SEGUNDO_APELLIDO,
                            Personal2ApellidoCasada = personal2.APELLIDO_CASADA == null ? "" : personal2.APELLIDO_CASADA,
                            Personal2Celular = personal2.CELULAR == null ? "" : personal2.CELULAR,
                            Personal2Tipo = mPersonal2Tipo,
                            Personal2TipoDescripcion = mPersonal2TipoDescripcion,
                            Personal2Empresa = personal2.EMPRESA == null ? "" : personal2.EMPRESA,
                            Personal2EmpresaDireccion = personal2.EMPRESA_DIRECCION == null ? "" : personal2.EMPRESA_DIRECCION,
                            Personal2EmpresaDepartamento = mPersonal2EmpresaDepartamento,
                            Personal2EmpresaDepartamentoCodigo = mPersonal2EmpresaDepartamentoCodigo,
                            Personal2EmpresaMunicipio = mPersonal2EmpresaMunicipio,
                            Personal2EmpresaMunicipioCodigo = mPersonal2EmpresaMunicipioCodigo,
                            Personal2EmpresaTelefono = personal2.TELEFONO_TRABAJO == null ? "" : personal2.TELEFONO_TRABAJO,
                            Personal2CasaDireccion = personal2.CASA_DIRECCION == null ? "" : personal2.CASA_DIRECCION,
                            Personal2CasaDepartamento = mPersonal2CasaDepartamento,
                            Personal2CasaDepartamentoCodigo = mPersonal2CasaDepartamentoCodigo,
                            Personal2CasaMunicipio = mPersonal2CasaMunicipio,
                            Personal2CasaMunicipioCodigo = mPersonal2CasaMunicipioCodigo,
                            Personal2CasaTelefono = personal2.TELEFONO_CASA == null ? "" : personal2.TELEFONO_CASA,
                            PrecioDelBien = mPrecioDelBien,
                            Enganche = mEnganche,
                            SaldoFinanciar = mSaldoFinanciar,
                            Intereses = mIntereses,
                            Monto = mMonto,
                            Plazo = mPlazo,
                            Cuota = mCuota,
                            UltimaCuota = mUltimaCuota,
                            Cuotas = mCuotas,
                            Articulo = mArticulo,
                            Sucursal = mSucursal,
                            Vendedor = mVendedor,
                            Fecha = mFecha,
                            ConyuguePrimerNombre = conyugue.PRIMER_NOMBRE == null ? "" : conyugue.PRIMER_NOMBRE,
                            ConyugueSegundoNombre = conyugue.SEGUNDO_NOMBRE == null ? "" : conyugue.SEGUNDO_NOMBRE,
                            ConyugueTercerNombre = conyugue.TERCER_NOMBRE == null ? "" : conyugue.TERCER_NOMBRE,
                            ConyuguePrimerApellido = conyugue.PRIMER_APELLIDO == null ? "" : conyugue.PRIMER_APELLIDO,
                            ConyugueSegundoApellido = conyugue.SEGUNDO_APELLIDO == null ? "" : conyugue.SEGUNDO_APELLIDO,
                            ConyugueApellidoCasada = conyugue.APELLIDO_CASADA == null ? "" : conyugue.APELLIDO_CASADA,
                            ConyugueDPI = mfc.CONYUGUE_DPI == null ? "" : mfc.CONYUGUE_DPI,
                            ConyugueNIT = mfc.CONYUGUE_NIT == null ? "" : mfc.CONYUGUE_NIT,
                            ConyugueFechaNacimiento = mFechaNacimientoConyugue,
                            ConyugueCelular = conyugue.CELULAR == null ? "" : conyugue.CELULAR,
                            ConyugueEmpresa = mfc.CONYUGUE_EMPRESA,
                            ConyuguePuesto = "",
                            ConyugueIngresos = mfc.CONYUGUE_INGRESOS,
                            ConyugueEgresos = "",
                            ConyugueTelefono = mfc.CONYUGUE_EMPRESA_TELEFONO,
                            NombreCompleto = mNombreCompleto,
                            Edad = mEdadString,
                            SaldoFinanciarLetras = mSaldoFinanciarLetras,
                            DireccionLetras = mDireccionLetras,
                            PlazoLetras = mPlazoLetras,
                            FechaVence = mFechaVence,
                            FechaVenceLetras = mFechaVenceLetras,
                            CuotasLetras = mCuotasLetras,
                            UltimaCuotaLetras = mUltimaCuotaLetras,
                            FechaInicialLetras = mFechaInicialLetras,
                            Tasa = mTasa,
                            TasaLetras = mTasaLetras,
                            DireccionFinanciera = mDireccionFinanciera,
                            SaldoFinanciarFormato = mSaldoFinanciarFormato,
                            CuotaFormato = mCuotaFormato,
                            UltimaCuotaFormato = mUltimaCuotaFormato,
                            CuotasMenosUno = mCuotasMenosUno,
                            CuotasMenosUnoLetras = mCuotasMenosUnoLetras,
                            CodigoProfesion = mCodigoProfesion,
                            CodigoPuesto = mCodigoPuesto,
                            CodigoMunicipio = mCodigoMunicipo,
                            CodigoDepartamento = mCodigoDepartamento,
                            CodigoCanton = mCanton,
                            CodigoMunicipioEmpresa = mCodigoMunicipoEmpresa,
                            CodigoDepartamentoEmpresa = mCodigoDepartamentoEmpresa,
                            CodigoCantonEmpresa = mCantonEmpresa,
                            Dependientes = mDependientes,
                            Politico = mfc.POLITICO,
                            NoCreditoATID = mNoCreditoATID,
                            DireccionATID = mDireccionATID,
                            Usuario = mUsuario,
                            Tienda = mTienda,
                            FechaPagare = mFechaPagare,
                            FechaPrimerPago = mFechaPrimerPago,
                            FechaInicioLabores = mFechaInicioLabores,
                            SueldoLetras = mIngresosLetras,
                            FechaLetras = mFechaLetras,
                            Documento = id,
                            FechaDocumento = mFechaDocumento,
                            NumeroCredito = mNumeroCredito,
                            Foto = mFoto,
                            Huella = mHuella
                        };
                
                return q.First();
            }
            catch (Exception ex)
            {
                mSolicitud.PrimerNombre = string.Format("{0}", CatchClass.ExMessage(ex, "SolicitudController", "Get"));
            }

            return mSolicitud;
        }


    }
}
