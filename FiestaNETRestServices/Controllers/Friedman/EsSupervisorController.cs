﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;

namespace FiestaNETRestServices.Controllers.Friedman
{
    public class EsSupervisorController : MFApiController
    {
        // GET: api/EsSupervisor
        public string Get()
        {
            return "Error, debe enviar el usuario";
        }

        // GET: api/EsSupervisor/5
        public bool Get(string id)
        {
            return Utilitario.EsSupervisor(id);
        }

    }
}
