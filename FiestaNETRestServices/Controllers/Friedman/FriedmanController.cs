﻿using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static MF_Clases.Clases;

namespace FiestaNETRestServices.Controllers.Friedman
{

    /// <summary>
    /// Esta clase alberga funciones que son utilizadas para los temas de Friedman, tales como: Friedman semanal, Friedman trimestral.
    /// </summary>
    public class FriedmanController : ApiController
    {


        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función tiene como propósito obtener el listado de la evaluación trimestral Friedman, respecto a los vendedores.
        /// para debuguear se llama asi: http://localhost:53874/api/Friedman/GetResumenTrimestral?mUsuario=WILLQUIACAIN&Tienda=F05&Anio=2018&Periodo=12
        /// </summary>
        /// <param name="mUsuario">Usuario quien está haciendo la solicitud.</param>
        /// <param name="Tienda">Tienda que desea consultar.</param>
        /// <param name="Anio">Año correspondiente a la evaluación.</param>
        /// <param name="Periodo">Periodo el año a consultar..</param>
        /// <returns>Retorna un IHttpActionResult Ok((List<FriedmanResumenTrimestralDto>) o BadRequest(Mensaje de respuesta con el mensaje de error.), . </returns>
        //-----------------------------------------------------------------------------------------------------------------------
        #region GetResumenTrimestral
        [Route("~/GetResumenTrimestral?mUsuario={Usuario}&Anio={Anio}&Periodo={Periodo}")]
        [HttpGet]
        public IHttpActionResult GetResumenTrimestral(string mUsuario, string Tienda, int Anio, int Periodo)
        {
            Respuesta mRespuesta = new Respuesta();
            //Clase que almacena la informacion del resumen semanal, y totales de todas las tiendas.

            // - ---------------------------------------------------------------------------------
            // Clase que almacena la informacion del resumen trimestral, y totales de todas las tiendas.
            // - ---------------------------------------------------------------------------------
            List<FriedmanResumenTrimestralDto> mFriedmanResumenTrimestral = new List<FriedmanResumenTrimestralDto>();

            // - ---------------------------------------------------------------------------------
            // Lista de periodos vigentes a comprobar
            // - ---------------------------------------------------------------------------------
            List<Periodos> mPeriodos = new List<Periodos>();

            // - ---------------------------------------------------------------------------------
            // Resumen mensual por periodo, esta variable se utiliza para obtener el listado semanal
            // del periodo a evaluar. 1 vendedor tiene cero o un periodo a evaluar.
            // - ---------------------------------------------------------------------------------
            List<FriedmanResumenSemanal> mFriedmanResumenSemanal = new List<FriedmanResumenSemanal>();

            // - ---------------------------------------------------------------------------------
            // Listado que almacena la información Trimestral, es decir, el conjunto del resumen 
            // semanal por vendedor, 1 vendedor tiene uno o mas periodos.
            // - ---------------------------------------------------------------------------------
            List<FriedmanResumenSemanal> mFriedmanTrimestral = new List<FriedmanResumenSemanal>();

            // - ---------------------------------------------------------------------------------
            // Listado que almacena el total de la semana por tienda, es decir, se realizar una 
            // sumatoria, y se almacena en este listado.
            // - ---------------------------------------------------------------------------------
            List<FriedmanResumenSemanal> mFriedmanTrimestralTotales = new List<FriedmanResumenSemanal>();

            // - ---------------------------------------------------------------------------------
            // Listado de tiendas a las que tiene acceso el usuario
            // - ---------------------------------------------------------------------------------
            List<string> mTiendas = new List<string>();

            // - ---------------------------------------------------------------------------------
            // Variable que sirve para conecterse a la basee de datos.
            // - ---------------------------------------------------------------------------------
            DALFriedman mDALFriedman = new DALFriedman();

            // - ---------------------------------------------------------------------------------
            // Variable que sirve utilizar los metodos y funciones repetitivas, como el formato de fecha
            // encripcion, etc.
            // - ---------------------------------------------------------------------------------
            FiestaNETRestServices.Utils.Utilitarios mUtilitarios = new FiestaNETRestServices.Utils.Utilitarios();

            // - ---------------------------------------------------------------------------------
            // Variable que sirve para obtener el listado e acceso al la tienda segun el usuario.
            // - ---------------------------------------------------------------------------------
            TiendasAccesoController mAccesoTienda = new TiendasAccesoController();

            try
            {

                // - ---------------------------------------------------------------------------------
                // Variable que sirve para obtener el listado e acceso al la tienda segun el usuario.
                // - ---------------------------------------------------------------------------------
                List<Tiendas> mTiendasAcceso = mAccesoTienda.Get(mUsuario);

                var mAccesoReal = from v in mTiendasAcceso
                                  where v.Tienda == Tienda
                                  select v;


                if (mAccesoReal.Count() == 0)
                {
                    return BadRequest("El usuario no tiene acceso a esta tienda");
                }


                // - ---------------------------------------------------------------------------------
                // Obtenemos los periodos correspondientes al trimestre, segun el periodo que está 
                // posicionado, es decir, si el periodo es "5", este pertenece al primer trimestre, 
                // entonces obtenemos el listado de periodos correspondientes al prrimer trimestre.
                // Si el periodo es "15", este pertenece al segundo trimestre, entonces obtenemos el 
                // listao de periodos correspondientes al segundo trimestre.
                // - ---------------------------------------------------------------------------------
                mRespuesta = mDALFriedman.ObtenerPeriodosTrimestre(Anio, Periodo);

                if (mRespuesta.Exito)
                {
                    // - ---------------------------------------------------------------------------------
                    //Listado de periodos correspondientes al trimestre
                    // - ---------------------------------------------------------------------------------
                    mPeriodos = (List<Periodos>)mRespuesta.Objeto;
                    foreach (var item in mPeriodos)
                    {
                        // - ---------------------------------------------------------------------------------
                        //Obtener el resumen semanal por periodo de las tiendas a las cuales tiene privilegios el usuario.
                        // - ---------------------------------------------------------------------------------
                        mRespuesta = GetResumenSemanalXPeriodo(mUsuario, Tienda, item.Anio, item.Periodo);
                        if (mRespuesta.Exito)
                        {

                            mFriedmanResumenSemanal = (List<FriedmanResumenSemanal>)mRespuesta.Objeto;
                            foreach (var itemRS in mFriedmanResumenSemanal)
                            {
                                mFriedmanTrimestral.Add(itemRS);
                            }
                        }
                    }

                    // - ---------------------------------------------------------------------------------
                    // Obtenemos el lista de tiendas, en este caso como ya tiene una sola tienda.
                    // Esto está así por si desean que el servicio sea extensible, y devuelva todas las
                    // tiendas a las que tenga acceso.
                    // - ---------------------------------------------------------------------------------
                    mTiendas = mFriedmanTrimestral.Select(x => x.Cobrador).Distinct().ToList();


                    // - ---------------------------------------------------------------------------------
                    // Extensión, cuando el usuario quisiera obtener el informe trimestral de todas las 
                    // tiendas a las que tiene acceso, por el momento solamente es por una tienda, pero 
                    // esta logica ya maneja varias tiendas.
                    // Primero F02 -> F02 -> F-04 -> F09 .....
                    // - ---------------------------------------------------------------------------------
                    foreach (var itemTienda in mTiendas)
                    {
                        // - ---------------------------------------------------------------------------------
                        // Filtramos tienda por tienda. F02
                        // - ---------------------------------------------------------------------------------
                        var mFriedmanTrimestralXTienda = from m in mFriedmanTrimestral
                                                         where m.Cobrador == itemTienda
                                                         orderby m.Orden ascending
                                                         select m;

                        // - ---------------------------------------------------------------------------------
                        // Filtramos por vendedor
                        // - ---------------------------------------------------------------------------------
                        List<string> mVendedor = mFriedmanTrimestralXTienda.Select(x => x.Vendedor).Distinct().ToList();


                        // - ---------------------------------------------------------------------------------
                        // Evaluacion por vendededor, en este caso ya restringimos la tienda, eso quiere decir
                        // que unicamente vamos a obtener los datos del vendedor y sus periodos en esa tienda.
                        // - ---------------------------------------------------------------------------------
                        foreach (var itemVendedor in mVendedor)
                        {
                            FriedmanResumenTrimestralDto mEvaluacionVendedorTienda = new FriedmanResumenTrimestralDto();
                           

                            // - ---------------------------------------------------------------------------------
                            // Obtenemos le listado de la participacion el Vendedor
                            // - ---------------------------------------------------------------------------------
                            var mVendedorPeriodos = (from m in mFriedmanTrimestralXTienda
                                                     where m.Vendedor == itemVendedor
                                                     select m
                                                    ).OrderBy(x => x.Anio).ThenBy(x => x.Semana)
                                                    ;


                            mEvaluacionVendedorTienda.NombreVendedor = mVendedorPeriodos.FirstOrDefault().NombreVendedor;
                            mEvaluacionVendedorTienda.Cobrador = itemTienda;
                            mEvaluacionVendedorTienda.Vendedor = itemVendedor;
                            mEvaluacionVendedorTienda.Orden = (
                                                        from ftt in mFriedmanTrimestralXTienda
                                                        where ftt.Vendedor == itemVendedor
                                                        select ftt.Orden.ToString()
                                                        ).FirstOrDefault();

                            int mMes = 0;
                            int mMesPeriodo = 0;
                            int mSemana = 0;

                            // - ---------------------------------------------------------------------------------
                            // Llenamos la participacion en los periodos correspondientes del vendedor
                            // - ---------------------------------------------------------------------------------
                            foreach (var itemPeriodoVendedor in mPeriodos)
                            {

                                DateTime mFechaMes = DateTime.Parse(itemPeriodoVendedor.Fechas);
                                if (mMes == 0 || mMes != mFechaMes.Month)
                                {
                                    mMes = mFechaMes.Month;
                                    mMesPeriodo = mMesPeriodo + 1;
                                    mSemana = 1;
                                }


                                if (mMesPeriodo == 1)
                                {

                                    if (mSemana == 1)
                                    {
                                        // - ---------------------------------------------------------------------------------
                                        //TODO: Esto se puede factorizar, obteniendo los resultados en una sola clase.
                                        // - ---------------------------------------------------------------------------------
                                        mEvaluacionVendedorTienda.Valor01 = StatusQuetzXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Dias01 = DiasXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.VentaReal01 = VentaRealXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Periodo01 = itemPeriodoVendedor.Periodo.ToString();

                                    }
                                    if (mSemana == 2)
                                    {
                                        mEvaluacionVendedorTienda.Valor02 = StatusQuetzXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Dias02 = DiasXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.VentaReal02 = VentaRealXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Periodo02 = itemPeriodoVendedor.Periodo.ToString();
                                    }
                                    if (mSemana == 3)
                                    {
                                        mEvaluacionVendedorTienda.Valor03 = StatusQuetzXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Dias03 = DiasXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.VentaReal03 = VentaRealXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Periodo03 = itemPeriodoVendedor.Periodo.ToString();
                                    }
                                    if (mSemana == 4)
                                    {
                                        mEvaluacionVendedorTienda.Valor04 = StatusQuetzXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Dias04 = DiasXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.VentaReal04 = VentaRealXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Periodo04 = itemPeriodoVendedor.Periodo.ToString();
                                    }
                                    if (mSemana == 5)
                                    {
                                        mEvaluacionVendedorTienda.Valor05 = StatusQuetzXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Dias05 = DiasXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.VentaReal05 = VentaRealXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Periodo05 = itemPeriodoVendedor.Periodo.ToString();
                                    }
                                    mSemana = mSemana + 1;

                                    mEvaluacionVendedorTienda.Valor06 = StatusQuetzXVendedorXMes(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, mMes);
                                    mEvaluacionVendedorTienda.Dias06 = DiasXVendedorXMes(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, mMes);
                                    mEvaluacionVendedorTienda.VentaReal06 = VentaRealXVendedorXMes(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, mMes);
                                    mEvaluacionVendedorTienda.Periodo06 = "Mes";

                                    mEvaluacionVendedorTienda.Mes01 = mUtilitarios.Mes(mMes);
                                }

                                if (mMesPeriodo == 2)
                                {
                                    if (mSemana == 1)
                                    {
                                        mEvaluacionVendedorTienda.Valor07 = StatusQuetzXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Dias07 = DiasXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.VentaReal07 = VentaRealXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Periodo07 = itemPeriodoVendedor.Periodo.ToString();
                                    }
                                    if (mSemana == 2)
                                    {
                                        mEvaluacionVendedorTienda.Valor08 = StatusQuetzXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Dias08 = DiasXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.VentaReal08 = VentaRealXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Periodo08 = itemPeriodoVendedor.Periodo.ToString();
                                    }
                                    if (mSemana == 3)
                                    {
                                        mEvaluacionVendedorTienda.Valor09 = StatusQuetzXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Dias09 = DiasXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.VentaReal09 = VentaRealXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Periodo09 = itemPeriodoVendedor.Periodo.ToString();
                                    }
                                    if (mSemana == 4)
                                    {
                                        mEvaluacionVendedorTienda.Valor10 = StatusQuetzXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Dias10 = DiasXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.VentaReal10 = VentaRealXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Periodo10 = itemPeriodoVendedor.Periodo.ToString();
                                    }
                                    if (mSemana == 5)
                                    {
                                        mEvaluacionVendedorTienda.Valor11 = StatusQuetzXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Dias11 = DiasXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.VentaReal11 = VentaRealXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Periodo11 = itemPeriodoVendedor.Periodo.ToString();
                                    }
                                    mSemana = mSemana + 1;

                                    mEvaluacionVendedorTienda.Valor12 = StatusQuetzXVendedorXMes(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, mMes);
                                    mEvaluacionVendedorTienda.Dias12 = DiasXVendedorXMes(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, mMes);
                                    mEvaluacionVendedorTienda.VentaReal12 = VentaRealXVendedorXMes(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, mMes);
                                    mEvaluacionVendedorTienda.Periodo12 = "Mes";

                                    mEvaluacionVendedorTienda.Mes02 = mUtilitarios.Mes(mMes);
                                }
                                if (mMesPeriodo == 3)
                                {
                                    if (mSemana == 1)
                                    {
                                        mEvaluacionVendedorTienda.Valor13 = StatusQuetzXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Dias13 = DiasXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.VentaReal13 = VentaRealXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Periodo13 = itemPeriodoVendedor.Periodo.ToString();
                                    }
                                    if (mSemana == 2)
                                    {
                                        mEvaluacionVendedorTienda.Valor14 = StatusQuetzXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Dias14 = DiasXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.VentaReal14 = VentaRealXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Periodo14 = itemPeriodoVendedor.Periodo.ToString();
                                    }
                                    if (mSemana == 3)
                                    {
                                        mEvaluacionVendedorTienda.Valor15 = StatusQuetzXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Dias15 = DiasXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.VentaReal15 = VentaRealXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Periodo15 = itemPeriodoVendedor.Periodo.ToString();
                                    }
                                    if (mSemana == 4)
                                    {
                                        mEvaluacionVendedorTienda.Valor16 = StatusQuetzXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Dias16 = DiasXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.VentaReal16 = VentaRealXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Periodo16 = itemPeriodoVendedor.Periodo.ToString();
                                    }
                                    if (mSemana == 5)
                                    {
                                        mEvaluacionVendedorTienda.Valor17 = StatusQuetzXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Dias17 = DiasXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.VentaReal17 = VentaRealXVendedorXPeriodo(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, itemPeriodoVendedor.Periodo);
                                        mEvaluacionVendedorTienda.Periodo17 = itemPeriodoVendedor.Periodo.ToString();
                                    }
                                    mSemana = mSemana + 1;

                                    mEvaluacionVendedorTienda.Valor18 = StatusQuetzXVendedorXMes(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, mMes);
                                    mEvaluacionVendedorTienda.Dias18 = DiasXVendedorXMes(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, mMes);
                                    mEvaluacionVendedorTienda.VentaReal18 = VentaRealXVendedorXMes(mVendedorPeriodos.ToList(), itemPeriodoVendedor.Anio, mMes);
                                    mEvaluacionVendedorTienda.Periodo18 = "Mes";

                                    mEvaluacionVendedorTienda.Mes03 = mUtilitarios.Mes(mMes);
                                }
                            }
                            mFriedmanResumenTrimestral.Add(mEvaluacionVendedorTienda);
                        }

                    }

                }
                return Ok(mFriedmanResumenTrimestral);
            }
            catch (Exception e)
            {
                return BadRequest("excepcion " + e.Message);
            }
        }
        #endregion

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función tiene como propósito obtener el listado de la evaluación semanal Friedman, respecto a los vendedores.
        /// para debuguear se llama asi: http://localhost:53874/api/Friedman/GetResumenSemanall?Usuario=WILLQUIACAIN&Tienda=F05&Anio=2018&Periodo=12
        /// </summary>
        /// <param name="Usuario">Usuario quien está haciendo la solicitud.</param>
        /// <param name="Tienda">Tienda que desea consultar.</param>
        /// <param name="Anio">Año correspondiente a la evaluación.</param>
        /// <param name="Periodo">Periodo el año a consultar..</param>
        /// <returns>Retorna un IHttpActionResult Ok((List<FriedmanResumenSemanal>) o BadRequest(Mensaje de respuesta con el mensaje de error.), . </returns>
        //-----------------------------------------------------------------------------------------------------------------------
        #region GetResumenSemanal
        [Route("~/GetResumenSemanal?Usuario={Usuario}&Tienda={Tienda}&Anio={Anio}&Periodo={Periodo}")]
        [HttpGet]
        public IHttpActionResult GetResumenSemanal(string Usuario, string Tienda, int Anio, int Periodo)
        {
            // - ---------------------------------------------------------------------------------
            //Variable que utilizamos para obtener respuestas de los DAL en la logica del negocio
            // - ---------------------------------------------------------------------------------
            Respuesta mRespuesta = new Respuesta();

            try
            {
                // - ---------------------------------------------------------------------------------
                // Obtenemos los valores Friedman semanal x usuario x tienda x anio x periodo
                // - ---------------------------------------------------------------------------------
                mRespuesta = GetResumenSemanalXPeriodo(Usuario, Tienda, Anio, Periodo);
                if (mRespuesta.Exito)
                    return Ok((List<FriedmanResumenSemanal>)mRespuesta.Objeto);
                else return BadRequest(mRespuesta.Mensaje);
            }
            catch (Exception e)
            {
                return BadRequest("excepcion " + e.Message);
            }
        }
        #endregion


        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función tiene como propósito obtener el listado de la evaluación semanal Friedman, respecto a los vendedores.
        /// para debuguear se llama asi: http://localhost:53874/api/Friedman/GetResumenMensual?Usuario=WILLQUIACAIN&Tienda=F05&Anio=2018&Periodo=12
        /// </summary>
        /// <param name="Usuario">Usuario quien está haciendo la solicitud.</param>
        /// <param name="Tienda">Tienda que desea consultar.</param>
        /// <param name="Anio">Año correspondiente a la evaluación.</param>
        /// <param name="Periodo">Periodo el año a consultar..</param>
        /// <returns>Retorna un IHttpActionResult Ok((List<FriedmanResumenSemanal>) o BadRequest(Mensaje de respuesta con el mensaje de error.), . </returns>
        //-----------------------------------------------------------------------------------------------------------------------
        #region GetResumenSemanalXPeriodo
        private Respuesta GetResumenSemanalXPeriodo(string Usuario, string Tienda, int Anio, int Periodo)
        {
            // - ---------------------------------------------------------------------------------
            // Variable que utilizamos para obtener respuestas de los DAL en la logica del negocio
            // - ---------------------------------------------------------------------------------
            Respuesta mRespuesta = new Respuesta();

            // - ---------------------------------------------------------------------------------
            // Clase que almacena el resumen semana por tienda.
            // - ---------------------------------------------------------------------------------
            List<FriedmanResumenSemanal> mResumenSemanal = new List<FriedmanResumenSemanal>();

            // - ---------------------------------------------------------------------------------
            // Clase que almacena la informacion del resumen semanal, y totales de todas las tiendas.
            // - ---------------------------------------------------------------------------------
            List<FriedmanResumenSemanal> mFriedmanResumenSemanal = new List<FriedmanResumenSemanal>();


            // - ---------------------------------------------------------------------------------
            // Varible que nos indica el nivel de acceso que tiene un usuario, es decir a que tiendas
            // puede tener acceso a la informacion
            // - ---------------------------------------------------------------------------------
            TiendasAccesoController mAccesoTienda = new TiendasAccesoController();

            // - ---------------------------------------------------------------------------------
            // Variable que nos sirve para realizar las consultas correspondientes 
            // de Friedman
            // - ---------------------------------------------------------------------------------
            DALFriedman mDALFriedman = new DALFriedman();

            // - ---------------------------------------------------------------------------------
            // Variable que nos sirve para obtener el periodo actual, o el ultimo periodo en caso 
            // Que al servicio no le manden los parametros correspondientes.
            // - ---------------------------------------------------------------------------------
            Periodos mPeriodo = new Periodos();

            // - ---------------------------------------------------------------------------------
            //Variables que nos ayudan para realizar los calculos correspondientes, 
            //para determinar la estrategia a utilizar en cada vendedor
            // - ---------------------------------------------------------------------------------
            decimal mFacturaPromedio = 0;
            decimal mArticuloFactura = 0;
            decimal mVentasPorDia = 0;


            try
            {
                // - ---------------------------------------------------------------------------------
                //Si no mandan un periodo correcto, procedemos a obtener el perioo actual vigente o
                //el ultimo periodo registrado
                // - ---------------------------------------------------------------------------------
                if ((Anio == 0 && Periodo == 0) || Periodo > 60 || Anio < 1900 || Anio > 3000)
                {
                    mRespuesta = mDALFriedman.ObtenerPeriodoVigente();
                    if (mRespuesta.Exito)
                    {
                        mPeriodo = (Periodos)mRespuesta.Objeto;
                        Anio = mPeriodo.Anio;
                        Periodo = mPeriodo.Periodo;
                    }
                    else return mRespuesta;
                }

                // - ---------------------------------------------------------------------------------
                // Obtenemos el listado de tiendas a las cuales el Usuario tiene acceso
                // - ---------------------------------------------------------------------------------
                List<Tiendas> mTiendas = mAccesoTienda.Get(Usuario);

                var mAccesoReal = from v in mTiendas
                                  where v.Tienda == Tienda
                                  select v;


                if (mAccesoReal.Count() == 0)
                {
                    return new Respuesta(false, "El usuario no tiene acceso a esta tienda");
                }

                // - ---------------------------------------------------------------------------------
                //Procedemos a obtener la informacion correspondiente para cada tienda.
                // - ---------------------------------------------------------------------------------
                foreach (Tiendas mItem in mAccesoReal)
                {

                    // - ---------------------------------------------------------------------------------
                    //Obtenemos el resumen Friedman por tienda
                    // - ---------------------------------------------------------------------------------
                    mRespuesta = mDALFriedman.ObtenerResumenSemanalXTienda(mItem.Tienda, Anio, Periodo);

                    // - ---------------------------------------------------------------------------------
                    //Clase que almacenta los totales de la tienda, esto nos sirve para realizar 
                    //Los calculos respecto a los indicadores
                    // - ---------------------------------------------------------------------------------
                    FriedmanResumenSemanal mTotalesResumenSemanal = new FriedmanResumenSemanal();

                    if (mRespuesta.Exito)
                    {

                        //Si es correcta la consulta, procedemos a calcular los totales de tiendas.
                        mResumenSemanal = (List<FriedmanResumenSemanal>)mRespuesta.Objeto;
                        mTotalesResumenSemanal.NombreVendedor = "Total Tienda";
                        mTotalesResumenSemanal.Vendedor = "Total Tienda";
                        mTotalesResumenSemanal.Anio = Anio;
                        mTotalesResumenSemanal.Semana = Periodo;
                        mTotalesResumenSemanal.Cobrador = mItem.Tienda;
                        mTotalesResumenSemanal.Mes = mResumenSemanal.Select(x => x.Mes).Distinct().FirstOrDefault();
                        mTotalesResumenSemanal.Dias = mResumenSemanal.Sum(x => x.Dias);
                        mTotalesResumenSemanal.Meta = mResumenSemanal.Sum(x => x.Meta);
                        mTotalesResumenSemanal.Minimo = mResumenSemanal.Sum(x => x.Minimo);
                        mTotalesResumenSemanal.VentasReales = mResumenSemanal.Sum(x => x.VentasReales);
                        mTotalesResumenSemanal.Diferencia = mResumenSemanal.Sum(x => x.Diferencia);
                        mTotalesResumenSemanal.CantFacturas = mResumenSemanal.Sum(x => x.CantFacturas);
                        mTotalesResumenSemanal.CantArticulos = mResumenSemanal.Sum(x => x.CantArticulos);
                        mTotalesResumenSemanal.FacturaPromedio = mTotalesResumenSemanal.CantFacturas == 0 ? 0 : mTotalesResumenSemanal.VentasReales / mTotalesResumenSemanal.CantFacturas;
                        mTotalesResumenSemanal.ArticulosPorFactura = mTotalesResumenSemanal.CantFacturas == 0 ? 0 : mTotalesResumenSemanal.CantArticulos / mTotalesResumenSemanal.CantFacturas;
                        mTotalesResumenSemanal.VentasPorDia = mTotalesResumenSemanal.Dias == 0 ? 0 : mTotalesResumenSemanal.VentasReales / mTotalesResumenSemanal.Dias;
                        mTotalesResumenSemanal.QuetVTAvrsMeta = mTotalesResumenSemanal.Meta ==0 ? 0 : mTotalesResumenSemanal.VentasReales / mTotalesResumenSemanal.Meta;
                        mTotalesResumenSemanal.StatusQuetz = (mTotalesResumenSemanal.VentasReales >= mTotalesResumenSemanal.Meta) ? 1 :
                                                        ((mTotalesResumenSemanal.VentasReales < mTotalesResumenSemanal.Meta
                                                            && mTotalesResumenSemanal.VentasReales > mTotalesResumenSemanal.Minimo) ? 2 :
                                                            ((mTotalesResumenSemanal.VentasReales < mTotalesResumenSemanal.Minimo) ? 3 : 0
                                                            )
                                                        );
                        mTotalesResumenSemanal.Orden = 99;


                        // - ---------------------------------------------------------------------------------
                        //Procedemos a calcular los indicadores que nos sirven para definir la estrategia a tomar
                        // - ---------------------------------------------------------------------------------
                        foreach (FriedmanResumenSemanal item in mResumenSemanal)
                        {
                            mFacturaPromedio = mTotalesResumenSemanal.FacturaPromedio == 0 ? 0 : item.FacturaPromedio / mTotalesResumenSemanal.FacturaPromedio;
                            mArticuloFactura = mTotalesResumenSemanal.ArticulosPorFactura == 0 ? 0 : item.ArticulosPorFactura / mTotalesResumenSemanal.ArticulosPorFactura;
                            mVentasPorDia = mTotalesResumenSemanal.VentasPorDia == 0 ? 0 : item.VentasPorDia / mTotalesResumenSemanal.VentasPorDia;
                            if (item.Dias > 0)
                                item.Estrategia = Estrategia(mFacturaPromedio, mArticuloFactura, mVentasPorDia);
                            else item.Estrategia = "";

                            // - ---------------------------------------------------------------------------------
                            //Agregamos la informacion de cada uno de los vendedores a la lista corresponiente
                            // - ---------------------------------------------------------------------------------
                            mFriedmanResumenSemanal.Add(item);
                        }

                        // - ---------------------------------------------------------------------------------
                        //Agregamos la informacion de los totales de la tienda a la lista corresponiente
                        // - ---------------------------------------------------------------------------------
                        mFriedmanResumenSemanal.Add(mTotalesResumenSemanal);
                    }
                }

                mRespuesta.Exito = true;
                mRespuesta.Objeto = mFriedmanResumenSemanal;
                return mRespuesta;

            }
            catch (Exception e)
            {

                return new Respuesta(false, e, "");
            }
        }
        #endregion


        private string StatusQuetzXVendedorXPeriodo(List<FriedmanResumenSemanal> mFriedmanTrimestral, int Anio, int Periodo)
        {
            try
            {
                var resultado = (from MEVT in mFriedmanTrimestral
                                 where MEVT.Anio == Anio
                                        && MEVT.Semana == Periodo
                                 select MEVT.StatusQuetz).FirstOrDefault();
                if (resultado > 0)
                {
                    return resultado.ToString();
                }
                else return "";
            }
            catch
            {
                return "";
            }
        }



        private decimal DiasXVendedorXPeriodo(List<FriedmanResumenSemanal> mFriedmanTrimestral, int Anio, int Periodo)
        {
            try
            {
                var resultado = (from MEVT in mFriedmanTrimestral
                                 where MEVT.Anio == Anio
                                        && MEVT.Semana == Periodo
                                 select MEVT.Dias).FirstOrDefault();
                if (resultado > 0)
                {
                    return resultado;
                }
                else return 0;
            }
            catch
            {
                return 0;
            }
        }

        private decimal VentaRealXVendedorXPeriodo(List<FriedmanResumenSemanal> mFriedmanTrimestral, int Anio, int Periodo)
        {
            try
            {
                var resultado = (from MEVT in mFriedmanTrimestral
                                 where MEVT.Anio == Anio
                                        && MEVT.Semana == Periodo
                                 select MEVT.VentasReales).FirstOrDefault();
                if (resultado > 0)
                {
                    return resultado;
                }
                else return 0;
            }
            catch
            {
                return 0;
            }
        }

        private string StatusQuetzXVendedorXMes(List<FriedmanResumenSemanal> mFriedmanTrimestral, int Anio, int Mes)
        {
            try
            {

                var resultado = (from MEVT2 in mFriedmanTrimestral
                                 where MEVT2.Anio == Anio
                                        && MEVT2.Mes == Mes
                                 group new { MEVT2 } by new
                                 {
                                     MEVT2.Mes
                                 } into g
                                 select new
                                 {
                                     Mes = g.Key.Mes
                                     ,
                                     Dias = g.Sum(x => x.MEVT2.Dias)
                                     ,
                                     MetaMensual = g.Sum(x => x.MEVT2.Meta)
                                     ,
                                     MinimoMnesual = g.Sum(x => x.MEVT2.Minimo)
                                     ,
                                     VentasRealesMensual = g.Sum(x => x.MEVT2.VentasReales)
                                 });

                if (resultado.Count() > 0)
                {

                    if (resultado.FirstOrDefault().Dias > 0)
                    {
                        int status =

                                        (resultado.FirstOrDefault().VentasRealesMensual >= resultado.FirstOrDefault().MetaMensual) ? 1 :
                                                        ((resultado.FirstOrDefault().VentasRealesMensual < resultado.FirstOrDefault().MetaMensual
                                                            && resultado.FirstOrDefault().VentasRealesMensual > resultado.FirstOrDefault().MinimoMnesual) ? 2 :
                                                            ((resultado.FirstOrDefault().VentasRealesMensual < resultado.FirstOrDefault().MinimoMnesual) ? 3 : 0
                                                            )
                                                        );

                        return status.ToString();
                    }
                    else return "";

                }
                else return "";
            }
            catch
            {
                return "";
            }

        }

        private decimal VentaRealXVendedorXMes(List<FriedmanResumenSemanal> mFriedmanTrimestral, int Anio, int Mes)
        {
            try
            {

                var resultado = (from MEVT2 in mFriedmanTrimestral
                                 where MEVT2.Anio == Anio
                                        && MEVT2.Mes == Mes
                                 group new { MEVT2 } by new
                                 {
                                     MEVT2.Mes
                                 } into g
                                 select new
                                 {
                                     Mes = g.Key.Mes
                                     ,
                                     MetaMensual = g.Sum(x => x.MEVT2.Meta)
                                     ,
                                     MinimoMnesual = g.Sum(x => x.MEVT2.Minimo)
                                     ,
                                     VentasRealesMensual = g.Sum(x => x.MEVT2.VentasReales)
                                 });

                if (resultado.Count() > 0)
                {
                    return resultado.FirstOrDefault().VentasRealesMensual;
                }
                else return 0;
            }
            catch
            {
                return 0;
            }

        }

        private decimal DiasXVendedorXMes(List<FriedmanResumenSemanal> mFriedmanTrimestral, int Anio, int Mes)
        {
            try
            {

                var resultado = (from MEVT2 in mFriedmanTrimestral
                                 where MEVT2.Anio == Anio
                                        && MEVT2.Mes == Mes
                                 group new { MEVT2 } by new
                                 {
                                     MEVT2.Mes
                                 } into g
                                 select new
                                 {
                                     Mes = g.Key.Mes
                                     ,
                                     Dias = g.Sum(x => x.MEVT2.Dias)
                                     ,
                                     MinimoMnesual = g.Sum(x => x.MEVT2.Minimo)
                                     ,
                                     VentasRealesMensual = g.Sum(x => x.MEVT2.VentasReales)
                                 });

                if (resultado.Count() > 0)
                {
                    return resultado.FirstOrDefault().Dias;
                }
                else return 0;
            }
            catch
            {
                return 0;
            }

        }


        private string Estrategia(decimal mFacturaPromedio, decimal mArticuloFactura, decimal mVentasPorDia)
        {

            if (mFacturaPromedio > 1 && mArticuloFactura > 1 && mVentasPorDia > 1)
                return Const.FRIEDMAN.ESTRATEGIA.ELOGIO;

            if (mFacturaPromedio <= mArticuloFactura
                && mFacturaPromedio <= mVentasPorDia)
                return Const.FRIEDMAN.ESTRATEGIA.CFP;

            if (mArticuloFactura <= mFacturaPromedio
                && mArticuloFactura <= mVentasPorDia)
                return Const.FRIEDMAN.ESTRATEGIA.CAF;

            if (mVentasPorDia <= mFacturaPromedio
                && mVentasPorDia <= mArticuloFactura)
                return Const.FRIEDMAN.ESTRATEGIA.CVP;

            return "";

        }

    }
}
