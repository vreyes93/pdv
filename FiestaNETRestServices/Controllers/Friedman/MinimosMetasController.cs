﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;

namespace FiestaNETRestServices.Controllers.Friedman
{
    public class MinimosMetasController : MFApiController
    {
        public Respuesta GetMinimosMetas()
        {
            return new Respuesta(false, "Debe ingresar el período y el año a consultar");
        }

        // GET api/minimosmetas/5
        public Respuesta GetMinimosMetas(string periodo, string anio, string vendedor)
        {
            try
            {
                if (periodo == null) return new Respuesta(false, "Debe ingresar el período a consultar");
                if (periodo.Trim().Length == 0) return new Respuesta(false, "Debe ingresar el período a consultar");

                if (anio == null) return new Respuesta(false, "Debe ingresar el período a consultar");
                if (anio.Trim().Length == 0) return new Respuesta(false, "Debe ingresar el período a consultar");

                db.CommandTimeout = 999999999;

                int mPeriodo = 0; int mAnio = 0;

                try
                {
                    mPeriodo = Convert.ToInt32(periodo);
                }
                catch
                {
                    return new Respuesta(false, "Debe ingresar un período válido");
                }
                try
                {
                    mAnio = Convert.ToInt32(anio);
                }
                catch
                {
                    return new Respuesta(false, "Debe ingresar un año válido");
                }

                if (mAnio < 2017) return new Respuesta(false, "Debe ingresar un año válido");

                var q = from p in db.MF_Periodo where p.PERIODO == mPeriodo select p;
                if (q.Count() == 0) return new Respuesta(false, "Debe ingresar un período válido");

                DateTime mFechaInicial = new DateTime(mAnio, q.First().MES_INICIAL, q.First().DIA_INICIAL);
                DateTime mFechaFinal = new DateTime(mAnio, q.First().MES_FINAL, q.First().DIA_FINAL);

                List<MinimosMetas> mMinimosMetas = new List<MinimosMetas>();
                var qTiendas = from c in db.MF_Cobrador where c.ACTIVO == "S" && c.COBRADOR != "F01" orderby c.COBRADOR select c;

                if (qTiendas.Where(x => x.CODIGO_SUPERVISOR.Equals(vendedor)).Count() > 0) qTiendas = qTiendas.Where(x => x.CODIGO_SUPERVISOR.Equals(vendedor)).OrderBy(x => x.COBRADOR);
                string mUsuario = (from u in db.USUARIO where u.CELULAR == vendedor select u).First().USUARIO1;

                foreach (var item in qTiendas)
                {
                    decimal mMinimo = 0; decimal mMeta = 0;

                    var qMetas = from m in db.MF_MetaPeriodoTienda where m.FECHA_INICIAL == mFechaInicial && m.FECHA_FINAL == mFechaFinal && m.COBRADOR == item.COBRADOR select m;
                    if (qMetas.Count() > 0)
                    {
                        mMeta = qMetas.First().META;
                        mMinimo = qMetas.First().MINIMO;
                    }

                    MinimosMetas mItemMinimoMeta = new MinimosMetas();
                    mItemMinimoMeta.Tienda = item.COBRADOR;
                    mItemMinimoMeta.FechaInicial = mFechaInicial;
                    mItemMinimoMeta.FechaFinal = mFechaFinal;
                    mItemMinimoMeta.Periodo = mPeriodo;
                    mItemMinimoMeta.Minimo = mMinimo;
                    mItemMinimoMeta.Meta = mMeta;
                    mItemMinimoMeta.Usuario = mUsuario;
                    mMinimosMetas.Add(mItemMinimoMeta);
                }
                
                return new Respuesta(true, "", mMinimosMetas);
            }
            catch (Exception ex)
            {
                return new Respuesta(false, ex, "Error GetMinmioMetas");
            }
        }

        // POST api/minimosmetas
        public string PostMinimosMetas([FromBody]List<MinimosMetas> metas)
        {
            return "Error, opción deshabilitada";
            
            //string mRetorna = "";
            //db.CommandTimeout = 999999999;

            //try
            //{
            //    string mUsuario = metas[0].Usuario;
            //    int mPeriodo = metas[0].Periodo;

            //    DateTime mFechaInicial = new DateTime(metas[0].FechaInicial.Year, metas[0].FechaInicial.Month, 1);
            //    DateTime mFechaFinal = mFechaInicial.AddMonths(1).AddDays(-1);
                
            //    using (TransactionScope transactionScope = new TransactionScope())
            //    {
            //        foreach (var item in metas)
            //        {
            //            List<MF_MetaPeriodoTienda> mMetasBorrarMes = db.MF_MetaPeriodoTienda.Where(x => x.FECHA_INICIAL == mFechaInicial && x.FECHA_FINAL == mFechaFinal && x.COBRADOR == item.Tienda).ToList();
            //            foreach (MF_MetaPeriodoTienda itemBorrar in mMetasBorrarMes) db.MF_MetaPeriodoTienda.DeleteObject(itemBorrar);

            //            List<MF_MetaPeriodoTienda> mMetasBorrar = db.MF_MetaPeriodoTienda.Where(x => x.FECHA_INICIAL == item.FechaInicial && x.FECHA_FINAL == item.FechaFinal && x.COBRADOR == item.Tienda).ToList();
            //            foreach (MF_MetaPeriodoTienda itemBorrar in mMetasBorrar) db.MF_MetaPeriodoTienda.DeleteObject(itemBorrar);
                        
            //            MF_MetaPeriodoTienda iMeta = new MF_MetaPeriodoTienda
            //            {
            //                FECHA_INICIAL = item.FechaInicial,
            //                FECHA_FINAL = item.FechaFinal,
            //                COBRADOR = item.Tienda,
            //                ANIO = item.FechaInicial.Year,
            //                PERIODO = mPeriodo,
            //                MINIMO = item.Minimo,
            //                META = item.Meta,
            //                RecordDate = DateTime.Now,
            //                CreatedBy = string.Format("FA/{0}", mUsuario),
            //                UpdatedBy = string.Format("FA/{0}", mUsuario),
            //                CreateDate = DateTime.Now
            //            };

            //            db.MF_MetaPeriodoTienda.AddObject(iMeta);
            //        }

            //        db.SaveChanges();
            //        transactionScope.Complete();
            //    }
                
            //    using (TransactionScope transactionScope = new TransactionScope())
            //    {
            //        foreach (var item in metas)
            //        {
            //            var q = from m in db.MF_MetaPeriodoTienda where m.FECHA_INICIAL >= mFechaInicial && m.FECHA_INICIAL <= mFechaFinal select m;

            //            MF_MetaPeriodoTienda iMeta = new MF_MetaPeriodoTienda
            //            {
            //                FECHA_INICIAL = mFechaInicial,
            //                FECHA_FINAL = mFechaFinal,
            //                COBRADOR = item.Tienda,
            //                ANIO = mFechaInicial.Year,
            //                PERIODO = 0,
            //                MINIMO = (from s in q where s.COBRADOR == item.Tienda select s.MINIMO).Sum(),
            //                META = (from s in q where s.COBRADOR == item.Tienda select s.META).Sum(),
            //                RecordDate = DateTime.Now,
            //                CreatedBy = string.Format("FA/{0}", mUsuario),
            //                UpdatedBy = string.Format("FA/{0}", mUsuario),
            //                CreateDate = DateTime.Now
            //            };

            //            db.MF_MetaPeriodoTienda.AddObject(iMeta);
            //        }

            //        db.SaveChanges();
            //        transactionScope.Complete();
            //    }

            //    mRetorna = "Los mínmios y las metas fueron grabados exitosamente";
            //}
            //catch (Exception ex)
            //{
            //    mRetorna = string.Format("{0} {1}", CatchClass.ExMessage(ex, "MinimosMetasController", "PostMinimosMetas"), "");
            //}

            //return mRetorna;
        }
    }
}
