﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;

namespace FiestaNETRestServices.Controllers.Friedman
{
    public class MinimosMetasVendedoresController : MFApiController
    {
        // GET: api/MinimosMetasVendedores
        public Respuesta Get()
        {
            return new Respuesta(false, "Debe ingresar el período, el año y la tienda a consultar");
        }

        // GET: api/MinimosMetasVendedores/5
        public Respuesta Get(string periodo, string anio, string tienda, string usuario)
        {
            try
            {
                if (periodo == null) return new Respuesta(false, "Debe ingresar el período a consultar");
                if (periodo.Trim().Length == 0) return new Respuesta(false, "Debe ingresar el período a consultar");

                if (anio == null) return new Respuesta(false, "Debe ingresar el período a consultar");
                if (anio.Trim().Length == 0) return new Respuesta(false, "Debe ingresar el período a consultar");

                db.CommandTimeout = 999999999;

                int mPeriodo = 0; int mAnio = 0;

                try
                {
                    mPeriodo = Convert.ToInt32(periodo);
                }
                catch
                {
                    return new Respuesta(false, "Debe ingresar un período válido");
                }
                try
                {
                    mAnio = Convert.ToInt32(anio);
                }
                catch
                {
                    return new Respuesta(false, "Debe ingresar un año válido");
                }

                if (mAnio < 2017) return new Respuesta(false, "Debe ingresar un año válido");

                var q = from p in db.MF_Periodo where p.PERIODO == mPeriodo select p;
                if (q.Count() == 0) return new Respuesta(false, "Debe ingresar un período válido");

                DateTime mFechaInicial = new DateTime(mAnio, q.First().MES_INICIAL, q.First().DIA_INICIAL);
                DateTime mFechaFinal = new DateTime(mAnio, q.First().MES_FINAL, q.First().DIA_FINAL);
                TimeSpan mTimeSpan = mFechaFinal - mFechaInicial;

                var qTienda = from c in db.MF_MetaPeriodoTienda where c.FECHA_INICIAL == mFechaInicial && c.FECHA_FINAL == mFechaFinal && c.COBRADOR == tienda orderby c.COBRADOR select c;
                if (qTienda.Count() == 0) return new Respuesta(false, string.Format("{0} no tiene meta ingresada en este período", tienda));

                List<MinimosMetas> mMinimosMetas = new List<MinimosMetas>();
                //var qVendedores = from v in db.MF_Vendedor where v.COBRADOR == tienda && v.ACTIVO == "S" orderby v.NOMBRE select v;
                //var qVendedores = from v in db.MF_Vendedor
                //                  join c in db.MF_Catalogo on v.VENDEDOR equals c.CODIGO_CAT into z
                //                  from z1 in z.DefaultIfEmpty()
                //                  where v.COBRADOR == tienda && v.ACTIVO == "S" && z1.CODIGO_CAT == null
                //                  orderby v.NOMBRE
                //                  select v;
                //select new { VENDEDOR = v.VENDEDOR, NOMBRE = v.NOMBRE, APELLIDO = v.APELLIDO, COBRADOR = v.COBRADOR };
                var qVendedores = (from V in db.MF_Vendedor
                                   join MPV in db.MF_MetaPeriodoVendedor on V.VENDEDOR equals MPV.VENDEDOR
                                   where MPV.COBRADOR == tienda
                                   && MPV.ANIO == mAnio
                                   && MPV.PERIODO == mPeriodo
                                   select V
                                    ).Union(
                                        from v in db.MF_Vendedor
                                        where v.COBRADOR == tienda
                                            && v.ACTIVO == "S"
                                        && !db.MF_Catalogo.Any(ven => ven.CODIGO_TABLA == 25
                                                                    && v.VENDEDOR == ven.CODIGO_CAT
                                    )
                                        orderby v.NOMBRE
                                        select v);

                int mVendedores = qVendedores.Count();
                string mMensaje = "Haga clic en grabar para registrar las metas y mínimos de este período";

                string mUltimoVendedor = qVendedores.ToList().Last().VENDEDOR;
                var qVendedoresTienda = from m in db.MF_MetaPeriodoVendedor where m.FECHA_INICIAL == mFechaInicial && m.FECHA_FINAL == mFechaFinal && m.COBRADOR == tienda && m.DIAS > 0 select m;
                if (qVendedoresTienda.Count() > 0)
                {
                    mVendedores = qVendedoresTienda.Count();
                    mUltimoVendedor = qVendedoresTienda.ToList().Last().VENDEDOR;
                }

                bool mYaGrabados = false;
                var qVendedoresGrabados = from m in db.MF_MetaPeriodoVendedor where m.FECHA_INICIAL == mFechaInicial && m.FECHA_FINAL == mFechaFinal && m.COBRADOR == tienda select m;
                if (qVendedoresGrabados.Count() > 0) mYaGrabados = true;

                foreach (var item in qVendedores)
                {
                    decimal mDias = 0; decimal mMinimoDia = 0; decimal mMetaDia = 0; decimal mPctjMinimoDia = 0; decimal mPctjMetaDia = 0;
                    decimal mMinimo = Math.Round(qTienda.First().MINIMO / mVendedores, 2, MidpointRounding.AwayFromZero);
                    decimal mMeta = Math.Round(qTienda.First().META / mVendedores, 2, MidpointRounding.AwayFromZero);
                    decimal mPctjMinimo = Math.Round((mMinimo * 100) / qTienda.First().MINIMO, 2, MidpointRounding.AwayFromZero);
                    decimal mPctjMeta = Math.Round((mMeta * 100) / qTienda.First().META, 2, MidpointRounding.AwayFromZero);
                    
                    var qMetas = from m in db.MF_MetaPeriodoVendedor where m.FECHA_INICIAL == mFechaInicial && m.FECHA_FINAL == mFechaFinal && m.COBRADOR == tienda && m.VENDEDOR == item.VENDEDOR select m;
                    if (qMetas.Count() > 0)
                    {
                        mMensaje = "";
                        decimal mTotalDias = (from m in db.MF_MetaPeriodoVendedor where m.FECHA_INICIAL == mFechaInicial && m.FECHA_FINAL == mFechaFinal && m.COBRADOR == tienda select m.DIAS).Sum();
                        if (mTotalDias == 0) mTotalDias = 1;

                        mMeta = qMetas.First().META;
                        mMinimo = qMetas.First().MINIMO;
                        mPctjMeta = qMetas.First().PCTJ_META;
                        mPctjMinimo = qMetas.First().PCTJ_MINIMO;
                        mDias =  qMetas.First().DIAS;
                        mMinimoDia = Math.Round(qTienda.First().MINIMO / mTotalDias, 2, MidpointRounding.AwayFromZero);
                        mMetaDia = Math.Round(qTienda.First().META / mTotalDias, 2, MidpointRounding.AwayFromZero);
                        mPctjMinimoDia = Math.Round((decimal)100 / mTotalDias, 2, MidpointRounding.AwayFromZero);
                        mPctjMetaDia = Math.Round((decimal)100 / mTotalDias, 2, MidpointRounding.AwayFromZero);
                    }
                    else
                    {
                        if (mYaGrabados)
                        {
                            mDias = 0;
                            mMinimo = 0;
                            mMeta = 0;
                            mPctjMinimo = 0;
                            mPctjMeta = 0;

                            mMinimoDia = 0;
                            mMetaDia = 0;
                            mPctjMinimoDia = 0;
                            mPctjMetaDia = 0;
                        }
                        else
                        {
                            if (item.VENDEDOR == mUltimoVendedor)
                            {
                                decimal mMinimoTotal = mMinimo + mMinimosMetas.Sum(x => x.Minimo);
                                decimal mDiferenciaMinimo = qTienda.First().MINIMO - mMinimoTotal;
                                if (mDiferenciaMinimo != 0) mMinimo = mMinimo + mDiferenciaMinimo;

                                decimal mMetaTotal = mMeta + mMinimosMetas.Sum(x => x.Meta);
                                decimal mDiferenciaMeta = qTienda.First().META - mMetaTotal;
                                if (mDiferenciaMeta != 0) mMeta = mMeta + mDiferenciaMeta;

                                decimal mPctjMinimoTotal = mPctjMinimo + mMinimosMetas.Sum(x => x.PctjMinimo);
                                decimal mDiferenciaPctjMinimo = 100 - mPctjMinimoTotal;
                                if (mDiferenciaPctjMinimo != 0) mPctjMinimo = mPctjMinimo + mDiferenciaPctjMinimo;

                                decimal mPctjMetaTotal = mPctjMeta + mMinimosMetas.Sum(x => x.PctjMeta);
                                decimal mDiferenciaPctjMeta = 100 - mPctjMetaTotal;
                                if (mDiferenciaPctjMeta != 0) mPctjMeta = mPctjMeta + mDiferenciaPctjMeta;
                            }

                            mMinimoDia = Math.Round(qTienda.First().MINIMO / (mTimeSpan.Days * mVendedores), 2, MidpointRounding.AwayFromZero);
                            mMetaDia = Math.Round(qTienda.First().META / (mTimeSpan.Days * mVendedores), 2, MidpointRounding.AwayFromZero);
                            mPctjMinimoDia = Math.Round((decimal)100 / (mTimeSpan.Days * mVendedores), 2, MidpointRounding.AwayFromZero);
                            mPctjMetaDia = Math.Round((decimal)100 / (mTimeSpan.Days * mVendedores), 2, MidpointRounding.AwayFromZero);
                        }
                    }

                    MinimosMetas mItemMinimoMeta = new MinimosMetas();
                    mItemMinimoMeta.Tienda = tienda;
                    mItemMinimoMeta.Vendedor = item.VENDEDOR;
                    mItemMinimoMeta.NombreVendedor = string.Format("{0} {1}", item.NOMBRE, item.APELLIDO);
                    mItemMinimoMeta.FechaInicial = mFechaInicial;
                    mItemMinimoMeta.FechaFinal = mFechaFinal;
                    mItemMinimoMeta.Periodo = mPeriodo;
                    mItemMinimoMeta.Minimo = mMinimo;
                    mItemMinimoMeta.Meta = mMeta;
                    mItemMinimoMeta.Usuario = usuario;
                    mItemMinimoMeta.PctjMinimo = mPctjMinimo;
                    mItemMinimoMeta.PctjMeta = mPctjMeta;
                    mItemMinimoMeta.MinimoTienda = qTienda.First().MINIMO;
                    mItemMinimoMeta.MetaTienda = qTienda.First().META;
                    mItemMinimoMeta.Dias = mDias;
                    mItemMinimoMeta.DiasPeriodo = mTimeSpan.Days + 1;
                    mItemMinimoMeta.MinimoDia = mMinimoDia;
                    mItemMinimoMeta.MetaDia = mMetaDia;
                    mItemMinimoMeta.PctjMinimoDia = mPctjMinimoDia;
                    mItemMinimoMeta.PctjMetaDia = mPctjMetaDia;
                    mMinimosMetas.Add(mItemMinimoMeta);
                }

                return new Respuesta(true, mMensaje, mMinimosMetas);
            }
            catch (Exception ex)
            {
                return new Respuesta(false, ex, "Error Get");
            }
        }

        // POST: api/MinimosMetasVendedores
        public string Post([FromBody]List<MinimosMetas> metas)
        {
            string mRetorna = "";
            db.CommandTimeout = 999999999;
            
            try
            {
                decimal mPctjMinimo = Math.Round((from m in metas select m.PctjMinimo).Sum(), 2, MidpointRounding.AwayFromZero);
                decimal mPctjMeta = Math.Round((from m in metas select m.PctjMeta).Sum(), 2, MidpointRounding.AwayFromZero);

                decimal mMinimo = Math.Round((from m in metas select m.Minimo).Sum(), 2, MidpointRounding.AwayFromZero);
                decimal mMinimoTienda = (from m in metas select m).First().MinimoTienda;

                decimal mMeta = Math.Round((from m in metas select m.Meta).Sum(), 2, MidpointRounding.AwayFromZero);
                decimal mMetaTienda = (from m in metas select m).First().MetaTienda;

                if (mPctjMinimo != 100 || mPctjMeta != 100 || mMinimo != mMinimoTienda || mMeta != mMetaTienda) return "Error, la distribución no cuadra con los totales de la tienda";

                string mUsuario = metas[0].Usuario;
                int mPeriodo = metas[0].Periodo;

                DateTime mFechaInicial = new DateTime(metas[0].FechaInicial.Year, metas[0].FechaInicial.Month, 1);
                DateTime mFechaFinal = mFechaInicial.AddMonths(1).AddDays(-1);

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    foreach (var item in metas)
                    {
                        List<MF_MetaPeriodoVendedor> mMetasBorrarMes = db.MF_MetaPeriodoVendedor.Where(x => x.FECHA_INICIAL == mFechaInicial && x.FECHA_FINAL == mFechaFinal && x.COBRADOR == item.Tienda && x.VENDEDOR == item.Vendedor).ToList();
                        foreach (MF_MetaPeriodoVendedor itemBorrar in mMetasBorrarMes) db.MF_MetaPeriodoVendedor.DeleteObject(itemBorrar);

                        List<MF_MetaPeriodoVendedor> mMetasBorrar = db.MF_MetaPeriodoVendedor.Where(x => x.FECHA_INICIAL == item.FechaInicial && x.FECHA_FINAL == item.FechaFinal && x.COBRADOR == item.Tienda && x.VENDEDOR == item.Vendedor).ToList();
                        foreach (MF_MetaPeriodoVendedor itemBorrar in mMetasBorrar) db.MF_MetaPeriodoVendedor.DeleteObject(itemBorrar);

                        MF_MetaPeriodoVendedor iMeta = new MF_MetaPeriodoVendedor
                        {
                            FECHA_INICIAL = item.FechaInicial,
                            FECHA_FINAL = item.FechaFinal,
                            COBRADOR = item.Tienda,
                            VENDEDOR = item.Vendedor,
                            ANIO = item.FechaInicial.Year,
                            PERIODO = mPeriodo,
                            DIAS = item.Dias,
                            MINIMO = item.Minimo,
                            META = item.Meta,
                            PCTJ_MINIMO = item.PctjMinimo,
                            PCTJ_META = item.PctjMeta,
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("FA/{0}", mUsuario),
                            UpdatedBy = string.Format("FA/{0}", mUsuario),
                            CreateDate = DateTime.Now
                        };

                        db.MF_MetaPeriodoVendedor.AddObject(iMeta);

                        var qInicial = from m in db.MF_MetaPeriodoVendedorInicial where m.FECHA_INICIAL == item.FechaInicial && m.FECHA_FINAL == item.FechaFinal && m.COBRADOR == item.Tienda && m.VENDEDOR == item.Vendedor select m;
                        if (qInicial.Count() == 0)
                        {
                            MF_MetaPeriodoVendedorInicial iMetaInicial = new MF_MetaPeriodoVendedorInicial
                            {
                                FECHA_INICIAL = item.FechaInicial,
                                FECHA_FINAL = item.FechaFinal,
                                COBRADOR = item.Tienda,
                                VENDEDOR = item.Vendedor,
                                ANIO = item.FechaInicial.Year,
                                PERIODO = mPeriodo,
                                DIAS = item.Dias,
                                MINIMO = item.Minimo,
                                META = item.Meta,
                                PCTJ_MINIMO = item.PctjMinimo,
                                PCTJ_META = item.PctjMeta,
                                RecordDate = DateTime.Now,
                                CreatedBy = string.Format("FA/{0}", mUsuario),
                                UpdatedBy = string.Format("FA/{0}", mUsuario),
                                CreateDate = DateTime.Now
                            };

                            db.MF_MetaPeriodoVendedorInicial.AddObject(iMetaInicial);
                        }
                    }

                    db.SaveChanges();
                    transactionScope.Complete();
                }

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    foreach (var item in metas)
                    {
                        var q = from m in db.MF_MetaPeriodoVendedor where m.FECHA_INICIAL >= mFechaInicial && m.FECHA_INICIAL <= mFechaFinal && m.COBRADOR == item.Tienda select m;

                        MF_MetaPeriodoVendedor iMeta = new MF_MetaPeriodoVendedor
                        {
                            FECHA_INICIAL = mFechaInicial,
                            FECHA_FINAL = mFechaFinal,
                            COBRADOR = item.Tienda,
                            VENDEDOR = item.Vendedor,
                            ANIO = mFechaInicial.Year,
                            PERIODO = 0,
                            DIAS = (from s in q where s.VENDEDOR == item.Vendedor select s.DIAS).Sum(),
                            MINIMO = (from s in q where s.VENDEDOR == item.Vendedor select s.MINIMO).Sum(),
                            META = (from s in q where s.VENDEDOR == item.Vendedor select s.META).Sum(),
                            PCTJ_MINIMO = 0,
                            PCTJ_META = 0,
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("FA/{0}", mUsuario),
                            UpdatedBy = string.Format("FA/{0}", mUsuario),
                            CreateDate = DateTime.Now
                        };

                        db.MF_MetaPeriodoVendedor.AddObject(iMeta);

                        var qInicial = from m in db.MF_MetaPeriodoVendedorInicial where m.FECHA_INICIAL == mFechaInicial && m.FECHA_FINAL == mFechaFinal && m.COBRADOR == item.Tienda && m.VENDEDOR == item.Vendedor select m;
                        if (qInicial.Count() == 0)
                        {
                            MF_MetaPeriodoVendedorInicial iMetaInicial = new MF_MetaPeriodoVendedorInicial
                            {
                                FECHA_INICIAL = mFechaInicial,
                                FECHA_FINAL = mFechaFinal,
                                COBRADOR = item.Tienda,
                                VENDEDOR = item.Vendedor,
                                ANIO = mFechaInicial.Year,
                                PERIODO = 0,
                                DIAS = (from s in q where s.VENDEDOR == item.Vendedor select s.DIAS).Sum(),
                                MINIMO = (from s in q where s.VENDEDOR == item.Vendedor select s.MINIMO).Sum(),
                                META = (from s in q where s.VENDEDOR == item.Vendedor select s.META).Sum(),
                                PCTJ_MINIMO = 0,
                                PCTJ_META = 0,
                                RecordDate = DateTime.Now,
                                CreatedBy = string.Format("FA/{0}", mUsuario),
                                UpdatedBy = string.Format("FA/{0}", mUsuario),
                                CreateDate = DateTime.Now
                            };

                            db.MF_MetaPeriodoVendedorInicial.AddObject(iMetaInicial);
                        }
                    }
                    
                    db.SaveChanges();
                    transactionScope.Complete();
                }

                mRetorna = "Los mínmios y las metas fueron grabados exitosamente";
            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0} {1}", CatchClass.ExMessage(ex, "MinimosMetasVendedoresController", "PostMinimosMetas"), "");
            }

            return mRetorna;
        }
    }
}
