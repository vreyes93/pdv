﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;

namespace FiestaNETRestServices.Controllers.Friedman
{
    public class PeriodosController : MFApiController
    {

        public Respuesta GetPeriodos()
        {
            return new Respuesta(false, "Debe ingresar un año válido");
        }

        // GET api/periodos/5
        public Respuesta GetPeriodos(string id)
        {
            try
            {
                int mAnio = 0;
                try
                {
                    mAnio = Convert.ToInt32(id);
                }
                catch
                {
                    return new Respuesta(false, "Debe ingresar un año válido");
                }
                
                if (mAnio < 2017) return new Respuesta(false, "Debe ingresar un año válido");

                List<Periodos> mPeriodos = new List<Periodos>();
                var q = from p in db.MF_Periodo select p;

                foreach (var item in q)
                {
                    DateTime mFechaInicial = new DateTime(mAnio, item.MES_INICIAL, item.DIA_INICIAL);
                    DateTime mFechaFinal = DateTime.Now.Date;

                    try
                    {
                        mFechaFinal = new DateTime(mAnio, item.MES_FINAL, item.DIA_FINAL);
                    }
                    catch
                    {
                        mFechaFinal = new DateTime(mAnio, item.MES_FINAL, item.DIA_FINAL - 1);
                    }

                    Periodos mItemPeriodo = new Periodos();
                    mItemPeriodo.Periodo = item.PERIODO;
                    mItemPeriodo.Fechas = string.Format("{5} - Del {0}{1} al {2}{3} de {4}", mFechaInicial.Day < 10 ? "0" : "", mFechaInicial.Day.ToString(), mFechaFinal.Day < 10 ? "0" : "", mFechaFinal.Day.ToString(), Utilitario.Mes(mFechaInicial), item.PERIODO);
                    mPeriodos.Add(mItemPeriodo);
                }

                return new Respuesta(true, "", mPeriodos);
            }
            catch (Exception ex)
            {
                return new Respuesta(false, ex, "Error GetPeriodos");
            }
        }

    }
}
