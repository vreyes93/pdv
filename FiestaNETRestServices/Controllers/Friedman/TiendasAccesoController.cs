﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;

namespace FiestaNETRestServices.Controllers.Friedman
{
    public class TiendasAccesoController : MFApiController
    {
        // GET: api/TiendasAcceso
        public string Get()
        {
            return "Error, debe enviar el usuario";
        }

        // GET: api/TiendasAcceso/5
        public List<Tiendas> Get(string id)
        {
            bool mEsJefe = Utilitario.EsJefe(id);
            bool mEsSupervisor = Utilitario.EsSupervisor(id);

            string mVendedor = (from u in db.USUARIO where u.USUARIO1 == id select u).First().CELULAR;

            if (mEsJefe || mEsSupervisor)
            {
                if (mEsJefe)
                {
                    var q = from c in db.MF_Cobrador
                            where c.CODIGO_JEFE == mVendedor && c.ACTIVO == "S"
                            orderby c.COBRADOR
                            select new Tiendas()
                            {
                                Tienda = c.COBRADOR
                            };

                    return q.ToList();
                }
                else
                {
                    var q = from c in db.MF_Cobrador
                            where c.CODIGO_SUPERVISOR == mVendedor && c.ACTIVO == "S"
                            orderby c.COBRADOR
                            select new Tiendas()
                            {
                                Tienda = c.COBRADOR
                            };

                    return q.ToList();
                }
            }
            else
            {
                var q = from c in db.MF_Cobrador
                        where c.COBRADOR.Substring(0, 1) != "B" && c.ACTIVO == "S"
                        orderby c.COBRADOR
                        select new Tiendas()
                        {
                            Tienda = c.COBRADOR
                        };

                return q.ToList();
            }
        }

    }
}
