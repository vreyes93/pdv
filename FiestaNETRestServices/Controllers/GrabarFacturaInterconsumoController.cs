﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;
using MF_Clases;
using System.Web.Configuration;
using FiestaNETRestServices.Utils;

namespace FiestaNETRestServices.Controllers
{
    public class GrabarFacturaInterconsumoController : ApiController
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public class Retorna
        {
            public bool exito { get; set; }
            public string mensaje { get; set; }
        }

        // GET api/grabarfacturainterconsumo/5
        public Retorna GetFacturaInterconsumo(string documento)
        {
            Retorna retorna = new Retorna();
            EXACTUSERPEntities db = new EXACTUSERPEntities();

            Int64 mSolicitud = 0;
            string mFactura = "";
            string mAmbiente = "DES";

            retorna.exito = false;
            retorna.mensaje = "";
            
            if (db.Connection.ConnectionString.Contains("sql.fiesta.local")) mAmbiente = "PRO";
            if (db.Connection.ConnectionString.Contains("PRUEBAS")) mAmbiente = "PRU";
            
            try
            {
                log.Info("Inicio de validaciones.");

                if ((from p in db.MF_Pedido where p.PEDIDO == documento select p).Count() == 0)
                {
                    retorna.mensaje = "Pedido inválido.";
                    return retorna;
                }

                var qPedido = (from p in db.MF_Pedido where p.PEDIDO == documento select p).First();

                if (qPedido.FINANCIERA != 7)
                {
                    retorna.mensaje = "El pedido no pertenece a Interconsumo.";
                    return retorna;
                }

                try
                {
                    mSolicitud = Convert.ToInt64(qPedido.SOLICITUD);
                }
                catch
                {
                    mSolicitud = 0;
                }

                if (mSolicitud == 0)
                {
                    retorna.mensaje = "No se encontró la solicitud de crédito.";
                    return retorna;
                }
                
                var qFactura = (from f in db.FACTURA where f.PEDIDO == documento && f.ANULADA == "N" select f);

                if (qFactura.Count() == 0)
                {
                    retorna.mensaje = "No se encontró la factura de este pedido o está anulada.";
                    return retorna;
                }

                mFactura = qFactura.First().FACTURA1;

                log.Info("Generando XML para grabar en Interconsumo.");


                string xmlData = "<solicitud>";
                xmlData += "<autenticacion>";
                xmlData += "<aplicacion>Muebles Fiesta</aplicacion>";
                xmlData += string.Format("<usuario><![CDATA[{0}]]></usuario>", WebConfigurationManager.AppSettings["UsrInterconsumo"]);
                xmlData += string.Format("<clave><![CDATA[{0}]]></clave>", WebConfigurationManager.AppSettings["PwdInterconsumo"]);
                xmlData += "</autenticacion>";
                xmlData += string.Format("<referencia>{0}</referencia>", mSolicitud.ToString());
                xmlData += string.Format("<factura>{0}</factura>", mFactura);
                xmlData += "</solicitud>";

                log.Info("Consumiendo WS de grabación de factura de Interconsumo.");
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                FiestaNETRestServices.srvIntegracionInterConsumo.srvIntegracionInteconsumoPuente srvIntegra = new FiestaNETRestServices.srvIntegracionInterConsumo.srvIntegracionInteconsumoPuente();
                InterEncript.Encriptacion dllEncriptacion = new InterEncript.Encriptacion();

                string resultado = dllEncriptacion.Proceso(xmlData, "solicitud", "1", "xml");
                string respuesta = srvIntegra.EntradaXmlGrabarFactura(resultado);
                resultado = dllEncriptacion.Proceso(respuesta.Trim(), "", "2", "1");

                XmlDocument xmlRespuesta = new XmlDocument();

                xmlRespuesta.PreserveWhitespace = true;
                xmlRespuesta.LoadXml(resultado);

                log.Info("Leyendo respuesta de Interconsumo.");

                string mMensaje = string.Format("Factura {0} del pedido {1} y solicitud {2} grabada exitosamente en Interconsumo.", mFactura, documento, mSolicitud);
                log.Info(mMensaje);

                retorna.exito = true;
                retorna.mensaje = mMensaje;
            }
            catch (Exception ex)
            {
                string mMensajeError = CatchClass.ExMessage(ex, "GrabarFacturaInterconsumoController", "GetFacturaInterconsumo");

                try
                {
                    StringBuilder mBody = new StringBuilder();

                    mBody.Append("<!DOCTYPE html><html lang='en-us'>");
                    mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } .style1 { color: #8C4510; background-color: #FFF7E7; font-size: xx-small; text-align: left; height:35px; } .style2 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; height:35px; } .style3 { color: #8C4510; background-color: #FFF7E7; font-size: x-small; text-align: left; height:35px; } .style4 { border: 1px solid #8C4510; } .style5 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; text-align: left; height:35px; } </style></head>");
                    mBody.Append("<body>");
                    mBody.Append("<table align='center'>");
                    mBody.Append(string.Format("<tr><td class='style5'><b>Pedido:&nbsp;&nbsp;</b></td><td class='style3'><b>&nbsp;&nbsp;{1}&nbsp;&nbsp;</b></td></tr>", documento, mAmbiente == "PRO" ? "" : "-&nbsp;&nbsp;Estoy utilizando el sistema de PRUEBAS"));
                    mBody.Append(string.Format("<tr><td class='style5'>Solicitud:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td></tr>", mSolicitud.ToString()));
                    mBody.Append(string.Format("<tr><td class='style5'>Factura:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td></tr>", mFactura));
                    mBody.Append(string.Format("<tr><td class='style5'>Fecha y hora:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}{1}/{2}{3}/{4} {5}&nbsp;&nbsp;</td></tr>", DateTime.Now.Date.Day < 10 ? "0" : "", DateTime.Now.Date.Day.ToString(), DateTime.Now.Date.Month < 10 ? "0" : "", DateTime.Now.Date.Month.ToString(), DateTime.Now.Date.Year.ToString(), DateTime.Now.ToLongTimeString()));
                    mBody.Append(string.Format("<tr><td class='style5'>Error:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0} - Este es un envío automatico del sistema&nbsp;&nbsp;</td></tr>", mMensajeError));
                    mBody.Append("</table>");
                    mBody.Append("</body>");
                    mBody.Append("</html>");

                    List<string> CuentaCorreo = new List<string>();

                    //SmtpClient smtp = new SmtpClient();
                    //MailMessage mail = new MailMessage();

                    var qUsuarios = from u in db.MF_UsuarioIT where u.ACTIVO == "S" orderby u.NOMBRE select u;
                    foreach (var item in qUsuarios)
                    {
                        CuentaCorreo.Add(item.EMAIL);
                    }

					Utils.Utilitarios Correo = new Utils.Utilitarios();
                    Correo.EnviarEmail("puntodeventa@productosmultiples.com", CuentaCorreo, string.Format("Soporte para grabar factura {0} en línea", mFactura), mBody, "Punto de Venta");

                    //mail.IsBodyHtml = true;
                    //mail.From = new MailAddress("puntodeventa2@productosmultiples.com", "Punto de Venta");
                    ////mail.From = new MailAddress("puntodeventa@mueblesfiesta.com", mNombreVendedor);
                    //mail.Subject = string.Format("Soporte para grabar factura {0} en línea", mFactura);

                    //mail.Body = mBody.ToString();

                    //smtp.Host = WebConfigurationManager.AppSettings["MailHost"];
                    //smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort"]);
                    //smtp.EnableSsl = true;
                    //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                    //try
                    //{
                    //    smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail"], WebConfigurationManager.AppSettings["PwdMail"]);
                    //    //smtp.Credentials = new System.Net.NetworkCredential("puntodeventa", "jh$%Pjma");
                    //    smtp.Send(mail);
                    //}
                    //catch
                    //{
                    //    //Nothing
                    //}



                }
                catch
                {
                    //Nothing
                }

                log.Error(mMensajeError);
                retorna.mensaje = mMensajeError;
            }

            return retorna;
        }
    }
}
