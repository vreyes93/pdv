﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Identidad
{
    [RoutePrefix("api/BiometricosHabilitados}")]
    public class BiometricosHabilitadosController : MFApiController
    {
        [HttpGet]
        public Respuesta Get()
        {
            Respuesta mRespuesta = new Respuesta();
            DALComunes mDALFinanciera = new DALComunes();
            try
            {
                mRespuesta = mDALFinanciera.ObtenerCatalogo(Const.CATALOGO.CATALOGO_FINANCIERAS_HUELLA_FOTOGRAFIA);
            }
            catch (Exception e)
            { return new Respuesta(false, e); }
            return mRespuesta;

        }
    }
}
