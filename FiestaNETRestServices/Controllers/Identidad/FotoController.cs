﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.DAL;
using MF.Comun.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Identidad
{
    [RoutePrefix("api/Foto")]
    public class FotoController : MFApiController
    {
        private new static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        

        [HttpGet]
        
        public RegistroFoto Get(string id)
        {
            DALIdentidadCliente identidadCliente = new DALIdentidadCliente();
                return identidadCliente.ObtenerFoto(id);
        }
        [HttpPost]
        [Route("RegistrarFoto")]
        [ActionName("RegistrarFoto")]
        public IHttpActionResult RegistrarFoto([FromBody] RegistroFoto foto)
        {
            DALIdentidadCliente identidadCliente = new DALIdentidadCliente();
            if (identidadCliente.RegistrarFoto(foto.Cliente, foto.Foto))
                return Ok(true);
            else
                return BadRequest();

        }


    }
}
