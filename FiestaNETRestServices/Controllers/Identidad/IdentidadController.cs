﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using MF.Comun.Dto;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static MF_Clases.Clases;


namespace FiestaNETRestServices.Controllers.Identidad
{
    [RoutePrefix("api/Identidad")]
    public class IdentidadController : MFApiController
    {
        private new static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [HttpPost]
        [Route("RegistrarHuella")]
        [ActionName("RegistrarHuella")]
        public IHttpActionResult RegistrarHuella([FromBody] RegistroHuella huella)
        {
            DALIdentidadCliente identidadCliente = new DALIdentidadCliente();
            if (identidadCliente.RegistrarHuella(huella.Cliente, huella.Huella,huella.FotoHuella,huella.UsuarioRegistro,huella.Tienda))
                return Ok(true);
            else
                return BadRequest();

        }
        
        [HttpGet]
        public IEnumerable<IdentidadCliente> Get(string id)
        {
            DALIdentidadCliente identidadCliente = new DALIdentidadCliente();
            if(id!="")
            return identidadCliente.ObtenerHuella(id);
            else
                return identidadCliente.ObtenerHuella();
        }

       
    }
}
