﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MF_Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using static MF_Clases.Clases;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;

namespace FiestaNETRestServices.Controllers.Interconsumo
{
    public class AprobacionSolicitudInterconsumoController : MFApiController
    {

        public Respuesta GetAprobacionSolicitudInterconsumo()
        {
            Respuesta mRespuesta = new Respuesta();
            UtilitariosInterconsumo mInterconsumo = new UtilitariosInterconsumo();
            List<SolicitudInterconsumo> mSolicitudesPendienteInterconsumo = new List<SolicitudInterconsumo>();
            List<SolicitudInterconsumo> mSolicitudesConEstadoInterconsumo = new List<SolicitudInterconsumo>();
            try
            {
                //---------------------------------------------------------------------------------------------
                //Obtenemos el listado de las solicitudes pendientes de aprobación, 
                //que fueron grabadas en linea, que tienen menos de 30 dias de haber solicitado
                //---------------------------------------------------------------------------------------------
                DALSolicitudPendienteInterconsumo mSolicitudesPendientes = new DALSolicitudPendienteInterconsumo();
                mRespuesta = mSolicitudesPendientes.GetSolicitudesPendientes();


                DALCotizacionInterconsumo mCotizacionInterconsumo = new DALCotizacionInterconsumo();
                DALPedidoInterconsumo mPedidoInterconsumo = new DALPedidoInterconsumo();

                //---------------------------------------------------------------------------------------------
                //Si sucedió algún error devolvemos la descripcion del error
                //---------------------------------------------------------------------------------------------
                if (!mRespuesta.Exito)
                    return mRespuesta;

                //---------------------------------------------------------------------------------------------
                //Obtenemos el listado de las solicitudes de Interconsumo.
                //---------------------------------------------------------------------------------------------
                mSolicitudesPendienteInterconsumo = (List<SolicitudInterconsumo>)mRespuesta.Objeto;

                //---------------------------------------------------------------------------------------------
                //Verificamos que haya almenos una solicitud pendiente.
                //---------------------------------------------------------------------------------------------
                if (mSolicitudesPendienteInterconsumo.Count() > 0)
                {

                    //---------------------------------------------------------------------------------------------
                    //Con cada solicitud, verificamos en Interconsumo su estado.
                    //---------------------------------------------------------------------------------------------
                    foreach (var item in mSolicitudesPendienteInterconsumo)
                    {
                        SolicitudInterconsumo mSolicitudInterconsumo = item;

                        try
                        {
                            //------------------------------------------------------------------------------------
                            //Consultamos por medio del servicio de Interconsumo
                            //------------------------------------------------------------------------------------
                            log.Debug("Consumiendo WS de consulta de Interconsumo.");
                            XmlDocument xmlRespuesta = mInterconsumo.Servicio(Const.SERVICIOS_INTERCONSUMO.CONSULTAR_SOLICITUD_CREDITO, mInterconsumo.mensajeEntradaXmlConsultaSolicitud(item.SOLICITUD.ToString(), item.COBRADOR));

                            //------------------------------------------------------------------------------------
                            //Mapeamos el resutado del xml a la clase que tenemos
                            //------------------------------------------------------------------------------------
                            log.Debug("Leyendo respuesta de Interconsumo.");
                            mInterconsumo.CastClaseSolicitudCredito(xmlRespuesta, ref mSolicitudInterconsumo);

                            //------------------------------------------------------------------------------------
                            //Generamos un nuevo listado de las solicitudes con estado inicial, 
                            // Aaprobadas, Rechazadas, Diferidas o Preautorizada.
                            // Aprobada y rechazada, es porque el proceso de la solcitud a culminado.
                            // Diferido y preautorizado, es porque el proceso de la solicitud aún esta en análisis.
                            //------------------------------------------------------------------------------------
                            if (!string.IsNullOrEmpty(mSolicitudInterconsumo.ESTADO))
                            {
                                mSolicitudesConEstadoInterconsumo.Add(mSolicitudInterconsumo);
                            }

                        }
                        catch (Exception e)
                        {
                            log.Error(this.GetType().Name + " " + e.StackTrace);
                            //Utilitario.EnviarEmailIT(this.GetType().Name, e.StackTrace);
                        }

                    }

                    //---------------------------------------------------------------------------------------------
                    //A las solicitudes aprobadas, rechazada o diferidas, actualizamos la informacion correspondiente 
                    // en la base de datos. Para los casos de aprobado o rechazado enviamos un correo al vendedor 
                    // y jefe notificandole de la aprobación o rechazo del del mismo.
                    // Caso contrario, solamente actualizamos la información.
                    //---------------------------------------------------------------------------------------------
                    if (mSolicitudesConEstadoInterconsumo.Count() > 0)
                    {

                        //---------------------------------------------------------------------------------------------
                        //Con cada solicitud, verificamos en Interconsumo su estado.
                        //---------------------------------------------------------------------------------------------
                        foreach (var item in mSolicitudesConEstadoInterconsumo)
                        {
                            try
                            {
                                //---------------------------------------------------------------------------------------------
                                //Actualizamos las tablas correspondientes
                                //---------------------------------------------------------------------------------------------
                                Respuesta mRespuestaGrabacion = new Respuesta();

                                if (item.TIPO_OPERACION == Const.TIPO_DOCUMENTO.COTIZACION)
                                {
                                    mRespuestaGrabacion = mCotizacionInterconsumo.GrabarSolicitud(item);
                                }
                                else
                                {
                                    mRespuestaGrabacion = mPedidoInterconsumo.GrabarSolicitud(item);

                                }

                                //---------------------------------------------------------------------------------------------
                                //Enviamos la notificación a los vendedores de la solicitud aprobada o rechazada por Interconsumo.
                                //---------------------------------------------------------------------------------------------
                                if (mRespuestaGrabacion.Exito
                                    && (item.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.APROBADO
                                    || item.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.RECHAZADO)
                                    )
                                    EnviarCorreo(item);

                                //---------------------------------------------------------------------------------------------
                                //Enviamos la notificación a los vendedores de la solicitud aprobada o rechazada por Interconsumo.
                                //---------------------------------------------------------------------------------------------
                                if (item.ENVIAR_CORREO_INTERCONSUMO
                                    && !(   item.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.APROBADO
                                            || item.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.RECHAZADO))
                                    EnviarCorreoInterconsumo(item);
                            }
                            catch (Exception e)
                            {
                                log.Error(this.GetType().Name + " " + e.StackTrace);
                                //Utilitario.EnviarEmailIT(this.GetType().Name, e.StackTrace);
                            }
                        }

                    }
                }
            }
            catch (Exception e)
            {
                return new Respuesta(false, "ERROR " + e.StackTrace);
            }

            return mRespuesta;
        }

        private void EnviarCorreoInterconsumo(SolicitudInterconsumo Solicitud)
        {
            //---------------------------------------------------------------------------------------------------------
            // Variable necesarias para el envio de correo.
            //---------------------------------------------------------------------------------------------------------
            string mRemitente = Utils.Utilitarios.GetRemitenteNotificacion(Const.REMITENTE_CORREO.REMITENTE_RESULTADO_SOLICITUD, Const.LISTA_CORREO.REMITENTES_CORREO);
            string mDisplayName = "";

            
            //---------------------------------------------------------------------------------------------
            //Redactamos el Display name, crédito aprobado o rechazado.
            //---------------------------------------------------------------------------------------------
            mDisplayName = "Muebles Fiesta - Solicitud Pendiente";


            //---------------------------------------------------------------------------------------------
            //Redactamos el asunto del correo
            //---------------------------------------------------------------------------------------------
            string mAsunto = string.Format("Solicitud {0} con estado {1}.", Solicitud.SOLICITUD.ToString(), Solicitud.ESTADO);

            //---------------------------------------------------------------------------------------------
            //Agregamos a los destinatario
            //---------------------------------------------------------------------------------------------
            List<string> mCuentaCorreo = new List<string>();

            //---------------------------------------------------------------------------------------------
            //Variable para el cuerpo del correo
            //---------------------------------------------------------------------------------------------
            StringBuilder mMensaje = new StringBuilder();
			Utils.Utilitarios mUtilitario = new Utils.Utilitarios();

            mUtilitario.GetListaCorreo(Const.LISTA_CORREO.INTERCONSUMO_SOLICITUDES_PENDIENTES, ref mCuentaCorreo);

            mMensaje.Append(Utilitario.CorreoInicioFormatoCafe());

            mMensaje.Append(string.Format("<p>Estimados, buen día. Les informamos que la solicitud de crédito No. {0} tiene el estado {1}. Solicitamos su acostumbrado apoyo para darle el trámite correspondiente.</p>", Solicitud.SOLICITUD.ToString(), Solicitud.ESTADO));

            mMensaje.Append("<table align='center' class='style4'>");
            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Solicitud: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(Solicitud.SOLICITUD.ToString()));
            mMensaje.Append("</tr>");

            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Fecha de la solicitud: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(Utilitario.FormatoDDMMYYYY(Solicitud.FECHA_SOLICITUD??DateTime.Now.Date)));
            mMensaje.Append("</tr>");

            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Nombre del cliente: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(Solicitud.NOMBRE_CLIENTE));
            mMensaje.Append("</tr>");


            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Email vendedor: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(Solicitud.EMAIL_VENDEDOR));
            mMensaje.Append("</tr>");

            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Nombre vendedor: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(Solicitud.NOMBRE_VENDEDOR));
            mMensaje.Append("</tr>");


            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Valor solicitud: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(Utilitario.FormatoNumeroDecimal(Solicitud.VALOR_SOLICITUD)));
            mMensaje.Append("</tr>");

            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Plazo: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(Utilitario.FormatoNumeroEntero(Solicitud.PLAZO)));
            mMensaje.Append("</tr>");


            mMensaje.Append("</table>");

            mMensaje.Append(string.Format("<p>Gracias por su atención,<br>"));
            mMensaje.Append(string.Format("Saludos cordiales.</p>"));
            mMensaje.Append(Utilitario.CorreoFin());

            //---------------------------------------------------------------------------------------------------------
            //Procedemos a enviar el correo.
            //---------------------------------------------------------------------------------------------------------
            Utilitario.EnviarEmail(mRemitente, mCuentaCorreo, mAsunto, mMensaje, mDisplayName);
        }

        private void EnviarCorreo(SolicitudInterconsumo Solicitud)
        {
            //---------------------------------------------------------------------------------------------------------
            // Variable necesarias para el envio de correo.
            //---------------------------------------------------------------------------------------------------------
            string mRemitente = Utils.Utilitarios.GetRemitenteNotificacion(Const.REMITENTE_CORREO.REMITENTE_RESULTADO_SOLICITUD, Const.LISTA_CORREO.REMITENTES_CORREO);
            string mDisplayName = "";

            //---------------------------------------------------------------------------------------------
            //Redactamos el Display name, crédito aprobado o rechazado.
            //---------------------------------------------------------------------------------------------
            if (Solicitud.ESTADO==Const.ESTADO_SOLICITUD_INTERCONSUMO.APROBADO)
                mDisplayName = "Crédito Aprobado";

            if (Solicitud.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.RECHAZADO)
                mDisplayName = "Crédito Rechazado";

            //---------------------------------------------------------------------------------------------
            //Redactamos el asunto del correo
            //---------------------------------------------------------------------------------------------
            string mAsunto = string.Format("Solicitud {0} - {1} {2}", Solicitud.SOLICITUD.ToString(), Solicitud.TIPO_OPERACION, Solicitud.NUMERO_DOCUMENTO);

            //---------------------------------------------------------------------------------------------
            //Agregamos a los destinatario
            //---------------------------------------------------------------------------------------------
            List<string> mCuentaCorreo = new List<string>();

            //---------------------------------------------------------------------------------------------
            //Variable para el cuerpo del correo
            //---------------------------------------------------------------------------------------------
            StringBuilder mMensaje = new StringBuilder();

            mCuentaCorreo.Add("william.quiacain@mueblesfiesta.com");
            mCuentaCorreo.Add(Solicitud.EMAIL_VENDEDOR);
            mCuentaCorreo.Add(Solicitud.EMAIL_JEFE);

            mMensaje.Append(Utilitario.CorreoInicioFormatoCafe());

            if (Solicitud.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.APROBADO)
                mMensaje.Append(string.Format("<p>Estimado(a) {0}, le informamos que la solicitud de crédito No. {1} ha sido aprobada, favor de continuar con el proceso correspondiente.</p>", Solicitud.NOMBRE_VENDEDOR, Solicitud.SOLICITUD.ToString()));

            if (Solicitud.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.RECHAZADO)
                mMensaje.Append(string.Format("<p>Estimado(a) {0}, le informamos que la solicitud de crédito No. {1} ha sido rechazada. </p>", Solicitud.NOMBRE_VENDEDOR, Solicitud.SOLICITUD.ToString()));

            mMensaje.Append("<table align='center' class='style4'>");
            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Solicitud: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(Solicitud.SOLICITUD.ToString()));
            mMensaje.Append("</tr>");

            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Documento: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(Solicitud.TIPO_OPERACION + " No. " + Solicitud.NUMERO_DOCUMENTO));
            mMensaje.Append("</tr>");


            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Cliente: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(Solicitud.CLIENTE + " - " + Solicitud.NOMBRE_CLIENTE));
            mMensaje.Append("</tr>");


            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Estado: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(Solicitud.ESTADO));
            mMensaje.Append("</tr>");

            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Plazo: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(Utilitario.FormatoNumeroEntero(Solicitud.PLAZO)));
            mMensaje.Append("</tr>");

            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Valor solicitado: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(Utilitario.FormatoNumeroDecimal(Solicitud.VALOR_SOLICITUD)));
            mMensaje.Append("</tr>");

            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Cuota: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(Utilitario.FormatoNumeroDecimal(Solicitud.CUOTA)));
            mMensaje.Append("</tr>");

            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Última cuota: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(Utilitario.FormatoNumeroDecimal(Solicitud.ULTIMA_CUOTA)));
            mMensaje.Append("</tr>");

            mMensaje.Append("</table>");

            mMensaje.Append(string.Format("<p>Gracias por su atención,<br>"));
            mMensaje.Append(string.Format("Saludos cordiales.</p>"));
            mMensaje.Append(Utilitario.CorreoFin());

            //---------------------------------------------------------------------------------------------------------
            //Procedemos a enviar el correo.
            //---------------------------------------------------------------------------------------------------------
            Utilitario.EnviarEmail(mRemitente, mCuentaCorreo, mAsunto, mMensaje, mDisplayName);
        }
    }
}
