﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using FiestaNETRestServices.Utils;
using MF_Clases;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Interconsumo
{
    [RoutePrefix("api/ConsultasDesembolsos")]
    public class ConsultasDesembolsosController : MFApiController
    {
        /// <summary>
        /// Servicio para consultar en la base de datos la información requerida de los
        /// desembolsos.
        /// </summary>
        /// <param name="dtFechaDesembolso"></param>
        /// <returns></returns>
        [Route("~/Realizados?FechaDesembolso={FechaDesembolso}&nFinanciera={nFinanciera}")]
        [HttpGet]
        public Respuesta ObtenerDesembolsosRealizados(string FechaDesembolso, int nFinanciera)
        {
            Respuesta mRespuesta = new Respuesta();
            DALDesembolsoInterconsumo DALDesembolsos = new DALDesembolsoInterconsumo();
            mRespuesta = DALDesembolsos.ConsultaDesembolsos(MF_Clases.Utilitarios.dateParseFormatDMY(FechaDesembolso), nFinanciera);
            return mRespuesta;
        }
        /// <summary>
        /// Servicio para obtener las solicitudes rechazadas por la financiera o -1 si quiere todas las solicitudes rechazadas.
        /// http://localhost:53874/api/ConsultasDesembolsos/Rechazados?nFinanciera=7
        /// </summary>
        /// <param name="nFinanciera"></param>
        /// <returns></returns>
        [Route("~/Rechazados?nFinanciera={nFinanciera}")]
        [HttpGet]
        public Respuesta ObtenerSolicitudesRechazadas(int nFinanciera)
        {
            Respuesta mRespuesta = new Respuesta();
            DALSolicitudPendienteInterconsumo DALRechazos = new DALSolicitudPendienteInterconsumo();
            mRespuesta = DALRechazos.GetInfoRechazosActuales(nFinanciera);
            return mRespuesta;
        }
        /// <summary>
        /// Servicio para obtener las solicitudes pendientes de desembolso por parte de la financiera
        /// /// forma de llamar al servicio :
        /// http://localhost:53874/api/ConsultasDesembolsos/Pendientes?blnTodas=true
        /// </summary>
        /// <param name="blnTodas"></param>
        /// <returns></returns>
        [Route("~/Pendientes?blnTodas={blnTodas}&nFinanciera={nFinanciera}")]
        [HttpGet]
        public Respuesta ObtenerSolicitudesPendientes(bool blnTodas, int nFinanciera)
        {
            Respuesta mRespuesta = new Respuesta();
            DALSolicitudPendienteInterconsumo DALPendientes = new DALSolicitudPendienteInterconsumo();
            mRespuesta = DALPendientes.GetInfoSolicitudesPendientes(blnTodas, nFinanciera);
            return mRespuesta;
        }
        /// <summary>
        /// Servicio para obtener las solicitudes pendientes de desembolso por parte de la financiera
        /// /// forma de llamar al servicio :
        /// http://localhost:53874/api/ConsultasDesembolsos/PendientesTienda?nFinanciera={nFinanciera}
        /// </summary>
        /// <param name="blnTodas"></param>
        /// <returns></returns>
        [Route("~/ConsultasDesembolsos/ObtenerSolicitudesPendientesEnTienda?F={F}")]
        [HttpGet]
        public Respuesta ObtenerSolicitudesPendientesEnTienda(int F)
        {
            Respuesta mRespuesta = new Respuesta();
            DALSolicitudPendienteInterconsumo DALPendientes = new DALSolicitudPendienteInterconsumo();
            mRespuesta = DALPendientes.ObtenerSolicitudesPendientesEnTienda(F);
            return mRespuesta;
        }

        /// <summary>
        /// Servicio para enviar AVISOS (PASADOS 30 DIAS) Y NOTIFICACIONES MARCANDO LA FACTURA COMO NO COMISIONABLE (PASADOS 45 DÍAS)
        /// LAS CONFIGURACIÓN DE LOS DÍAS ESTÁ EN EL WEB.CONFIG
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("~/ConsultasDesembolsos/ObtenerRechazosTardios")]
        [HttpGet]
        public Respuesta ObtenerRechazosTardios()
        {
            Respuesta mRespuesta = new Respuesta();
            DALSolicitudPendienteInterconsumo DALPendientes = new DALSolicitudPendienteInterconsumo();
            DALFinanciera DalF = new DALFinanciera();
            log.Debug("Iniciando a verificar rechazos con más de 30 o 45 días.");
            try
            {
                var mFinancieras = DalF.ObtenerFinancieras();
                if (mFinancieras.Exito)
                    if (mFinancieras.Objeto != null)
                    {
                        List<Clases.Financiera> lstFinancieras = new List<Clases.Financiera>();
                        lstFinancieras = (List<Clases.Financiera>)mFinancieras.Objeto;

                        foreach (Clases.Financiera vf in lstFinancieras)
                        {
                            try
                            {
                                long lId = (!vf.ID.HasValue ? 0 : long.Parse(vf.ID.ToString()));
                                #region "aviso rechazo proximo a ser tardío"
                                var resp = DALPendientes.ObtenerSolcitudesRechazadasxMasdeNdias(lId, true);
                                if (resp.Exito)
                                {
                                    List<Clases.SolicitudInterconsumo> lstRechazos = (List<MF_Clases.Clases.SolicitudInterconsumo>)(resp.Objeto);

                                    EnviarCorreoNotifAlertaRechazoTardioARRHH(lstRechazos, vf.Nombre);

                                    mRespuesta.Mensaje += lstRechazos.Count() + " ALERTAS DE " + vf.Nombre + "\n";
                                    mRespuesta.Exito = true;
                                }
                                #endregion

                            }
                            catch (Exception ex)
                            {
                                mRespuesta.Mensaje = CatchClass.ExMessage(ex, MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name);
                                log.Error(mRespuesta.Mensaje);
                                mRespuesta.Exito = false;
                            }
                            #region "aviso RECHAZO TARDÍO (PLAZO VENCIDO)"
                            try
                            {
                                long lId = (!vf.ID.HasValue ? 0 : long.Parse(vf.ID.ToString()));
                                var resp = DALPendientes.ObtenerSolcitudesRechazadasxMasdeNdias(lId, false);
                                if (resp.Exito)
                                {
                                    List<Clases.SolicitudInterconsumo> lstRechazos = (List<MF_Clases.Clases.SolicitudInterconsumo>)(resp.Objeto);
                                    #region "no comisionables"
                                    if (lstRechazos.Count > 0)
                                        mRespuesta.Mensaje += DALPendientes.MarcarNoComisionableFacturaRechazada(lstRechazos);
                                    #endregion
                                    EnviarCorreoNotifRechazoTardioARRHH(lstRechazos, vf.Nombre);
                                    mRespuesta.Mensaje += "----" + lstRechazos.Count() + " NOTIFICACIONES DE " + vf.Nombre + "\n";
                                    mRespuesta.Exito &= true;
                                    log.Debug("RESULTADO: " + mRespuesta.Mensaje);
                                }
                            }
                            catch (Exception ix)
                            {
                                mRespuesta.Mensaje = CatchClass.ExMessage(ix, MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name);
                                log.Error(mRespuesta.Mensaje);
                                Utils.Utilitarios.EnviarMensajeSlack(string.Format(this.GetType().FullName + " \nERROR correo obtener rechazos tardíos - {0};", mRespuesta.Mensaje));
                            }
                            #endregion
                        }
                    }

            }
            catch (Exception x)
            {
                mRespuesta.Mensaje = CatchClass.ExMessage(x, MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name);
                log.Error(mRespuesta.Mensaje);
                Utils.Utilitarios.EnviarMensajeSlack(string.Format(this.GetType().FullName + " \nERROR correo obtener rechazos tardíos - {0};", mRespuesta.Mensaje));
            }
            return mRespuesta;
        }

        private void EnviarCorreoNotifAlertaRechazoTardioARRHH(List<Clases.SolicitudInterconsumo> Rechazos, string NombreFinanciera)
        {
            ////---------------------------------------------------------------------------------------------------------
            //// Variable necesarias para el envio de correo.
            ////---------------------------------------------------------------------------------------------------------
            string mRemitente = Utils.Utilitarios.GetRemitenteNotificacion(Const.REMITENTE_CORREO.REMITENTE_NOTIFICACION_RECHAZOS_TARDIOS, Const.LISTA_CORREO.REMITENTES_CORREO);
            string mDisplayName = "";
            var nDias = int.Parse(ConfigurationManager.AppSettings["DiasValidacionSolicitudesRechazadas"].ToString());
            ////---------------------------------------------------------------------------------------------
            ////Redactamos el Display name, crédito aprobado o rechazado.
            ////---------------------------------------------------------------------------------------------
            mDisplayName = $"Muebles Fiesta - NOTIFICACIONES " + NombreFinanciera + "-";

            ////---------------------------------------------------------------------------------------------
            ////Redactamos el asunto del correo
            ////---------------------------------------------------------------------------------------------

            string mAsunto = $"Muebles Fiesta - ALERTA - Rechazos tardíos";

            ////---------------------------------------------------------------------------------------------
            ////Agregamos a los destinatario
            ////---------------------------------------------------------------------------------------------
            List<string> mCuentaCorreo = new List<string>();

            ////---------------------------------------------------------------------------------------------
            ////Variable para el cuerpo del correo
            ////---------------------------------------------------------------------------------------------
            StringBuilder mMensaje = new StringBuilder();

            ///Para dar formato a los montos, con simbolo de moneda
            CultureInfo cultureToUse = new CultureInfo("es-GT");
            cultureToUse.NumberFormat.CurrencyDecimalDigits = 2;
            cultureToUse.NumberFormat.NumberDecimalDigits = 2;



            Utils.Utilitarios mUtilitario = new Utils.Utilitarios();
            mUtilitario.GetListaCorreo(Const.LISTA_CORREO.FINANCIERAS_ALERTA_RECHAZOS_TARDIOS, ref mCuentaCorreo);
            mCuentaCorreo.Add("patricia.rodriguez@mueblesfiesta.com");


            mMensaje.Append(Utilitario.CorreoInicioFormatoRojo());
            mMensaje.Append(string.Format("<p align=\"center\"><img  src =cid:EmailAlert alt =\"Alerta\"></p><p> <br/>Estimados(as):<BR/> Les informamos que la siguiente lista de rechazos, <B>está próxima a vencer el plazo para resolver.</B><BR/> Para su seguimiento.</p>"));
            mMensaje.Append("<BR/>");
            mMensaje.Append("<table align='center' class='style4'>");
            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloCentro("Solicitud"));
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Documento"));
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Cliente"));
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Monto Factura"));
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Saldo"));
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Fecha Rechazo"));
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Observaciones"));

            mMensaje.Append("</tr>");
            foreach (var item in Rechazos)
            {
                var dtFechaRechazo = item.FECHA_RECHAZO_SOLICITUD.HasValue ? (DateTime)item.FECHA_RECHAZO_SOLICITUD : DateTime.Now;
                mMensaje.Append(Utilitario.FormatoCuerpoCentro(item.SOLICITUD.ToString()));
                mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.NUMERO_DOCUMENTO.ToString()));
                mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.CLIENTE.ToString() + " - " + item.NOMBRE_CLIENTE.ToString()));
                mMensaje.Append(Utilitario.FormatoCuerpoDerecha(Utilitario.FormatoNumeroDecimal(item.VALOR_FACTURA)));
                mMensaje.Append(Utilitario.FormatoCuerpoDerecha(Utilitario.FormatoNumeroDecimal(item.VALOR_SOLICITUD)));
                mMensaje.Append(Utilitario.FormatoCuerpoCentro(Utilitario.FormatoDDMMYYYY(dtFechaRechazo)));
                mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.OBSERVACIONES.ToString()));
                mMensaje.Append("</tr>");
            }

            mMensaje.Append("</table><br/>");

            mMensaje.Append(string.Format("<p>Gracias por su atención,<br>"));
            mMensaje.Append(string.Format("Saludos cordiales.</p>"));
            mMensaje.Append(Utilitario.CorreoFin());

            //---------------------------------------------------------------------------------------------------------
            //Procedemos a enviar el correo.
            //---------------------------------------------------------------------------------------------------------
            if (Rechazos.Count > 0)
            {
                List<ImagenAdjunta> lstImagenes = new List<ImagenAdjunta>();
                lstImagenes.Add(new MF_Clases.ImagenAdjunta(System.Web.Hosting.HostingEnvironment.MapPath("~//Images/email_alert.png"), MediaTypeNames.Image.Jpeg, "EmailAlert"));
                Utilitario.EnviarEmail(mRemitente, mCuentaCorreo, mAsunto, mMensaje, mDisplayName, lstImagenes);
            }
        }

        private void EnviarCorreoNotifRechazoTardioARRHH(List<Clases.SolicitudInterconsumo> Rechazos, string NombreFinanciera)
        {
            ////---------------------------------------------------------------------------------------------------------
            //// Variable necesarias para el envio de correo.
            ////---------------------------------------------------------------------------------------------------------
            string mRemitente = Utils.Utilitarios.GetRemitenteNotificacion(Const.REMITENTE_CORREO.REMITENTE_NOTIFICACION_RECHAZOS_TARDIOS, Const.LISTA_CORREO.REMITENTES_CORREO);
            string mDisplayName = "";
            var nDias = int.Parse(ConfigurationManager.AppSettings["DiasValidacionSolicitudesRechazadas"].ToString());
            ////---------------------------------------------------------------------------------------------
            ////Redactamos el Display name, crédito aprobado o rechazado.
            ////---------------------------------------------------------------------------------------------
            mDisplayName = $"Muebles Fiesta - NOTIFICACIONES " + NombreFinanciera + "-";

            ////---------------------------------------------------------------------------------------------
            ////Redactamos el asunto del correo
            ////---------------------------------------------------------------------------------------------

            string mAsunto = $"Muebles Fiesta - NOTIFICACIÓN - Rechazos tardíos.";

            ////---------------------------------------------------------------------------------------------
            ////Agregamos a los destinatario
            ////---------------------------------------------------------------------------------------------
            List<string> mCuentaCorreo = new List<string>();

            ////---------------------------------------------------------------------------------------------
            ////Variable para el cuerpo del correo
            ////---------------------------------------------------------------------------------------------
            StringBuilder mMensaje = new StringBuilder();

            ///Para dar formato a los montos, con simbolo de moneda
            CultureInfo cultureToUse = new CultureInfo("es-GT");
            cultureToUse.NumberFormat.CurrencyDecimalDigits = 2;
            cultureToUse.NumberFormat.NumberDecimalDigits = 2;

            Utils.Utilitarios mUtilitario = new Utils.Utilitarios();
            mUtilitario.GetListaCorreo(Const.LISTA_CORREO.FINANCIERAS_RECHAZOS_TARDIOS, ref mCuentaCorreo);
            mCuentaCorreo.Add("patricia.rodriguez@mueblesfiesta.com");

            mMensaje.Append(Utilitario.CorreoInicioFormatoRojo());
            mMensaje.Append(string.Format("<p>Estimados(as):<BR/> Les informamos que a la siguiente lista de expedientes rechazados, <b>se les venció el plazo para ser comisionables</b>.<BR/> Para su seguimiento.</p>"));
            mMensaje.Append("<BR/>");
            mMensaje.Append("<table align='center' class='style4'>");
            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloCentro("Solicitud"));
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Documento"));
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Cliente"));
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Monto Factura"));
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Saldo"));
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Fecha Rechazo"));
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Observaciones"));

            mMensaje.Append("</tr>");
            foreach (var item in Rechazos)
            {
                var dtFechaRechazo = item.FECHA_RECHAZO_SOLICITUD.HasValue ? (DateTime)item.FECHA_RECHAZO_SOLICITUD : DateTime.Now;
                mMensaje.Append(Utilitario.FormatoCuerpoCentro(item.SOLICITUD.ToString()));
                mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.NUMERO_DOCUMENTO.ToString()));
                mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.CLIENTE.ToString() + " - " + item.NOMBRE_CLIENTE.ToString()));
                mMensaje.Append(Utilitario.FormatoCuerpoDerecha(Utilitario.FormatoNumeroDecimal(item.VALOR_FACTURA)));
                mMensaje.Append(Utilitario.FormatoCuerpoDerecha(Utilitario.FormatoNumeroDecimal(item.VALOR_SOLICITUD)));
                mMensaje.Append(Utilitario.FormatoCuerpoCentro(Utilitario.FormatoDDMMYYYY(dtFechaRechazo)));
                mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.OBSERVACIONES.ToString()));
                mMensaje.Append("</tr>");
            }

            mMensaje.Append("</table><br/>");
            mMensaje.Append(string.Format("<p>Gracias por su atención,<br>"));
            mMensaje.Append(string.Format("Saludos cordiales.</p>"));
            mMensaje.Append(Utilitario.CorreoFin());

            //---------------------------------------------------------------------------------------------------------
            //Procedemos a enviar el correo.
            //---------------------------------------------------------------------------------------------------------
            if (Rechazos.Count > 0)
                Utilitario.EnviarEmail(mRemitente, mCuentaCorreo, mAsunto, mMensaje, mDisplayName);
        }
        
    }
    
}