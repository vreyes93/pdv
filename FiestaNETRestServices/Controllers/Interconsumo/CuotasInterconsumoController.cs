﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Data;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;
using MF_Clases;
using System.Web.Configuration;

namespace FiestaNETRestServices.Controllers.Interconsumo
{
    public class CuotasInterconsumoController : ApiController
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET api/cuotasinterconsumo/5
        public Clases.CuotasInterconsumo GetCuotas(decimal monto, string nivel)
        {
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            Clases.CuotasInterconsumo mRetorna = new Clases.CuotasInterconsumo();
            
            try
            {
                mRetorna.Exito = true;
                mRetorna.Mensaje = "";

                log.Info("Generando XML para consultar en Interconsumo.");

                string mCampo = "saldo_financiar";
                decimal mMonto = monto;
                if (nivel == "InterCons12M")
                {
                    mCampo = "monto_mf";
                }
                else
                {
                    decimal mCoeficiente = (from n in db.MF_NIVEL_PRECIO_COEFICIENTE where n.NIVEL_PRECIO == nivel select n).First().COEFICIENTE_MULR;
                    mMonto = Math.Round(monto * mCoeficiente, 2, MidpointRounding.AwayFromZero);
                }
                string xmlData = "<solicitud>";
                xmlData += "<autenticacion>";
                xmlData += "<aplicacion>Muebles Fiesta</aplicacion>";
                xmlData += string.Format("<usuario><![CDATA[{0}]]></usuario>", WebConfigurationManager.AppSettings["UsrInterconsumo"]);
                xmlData += string.Format("<clave><![CDATA[{0}]]></clave>", WebConfigurationManager.AppSettings["PwdInterconsumo"]);
                xmlData += "</autenticacion>";
                xmlData += string.Format("<{0}>{1}</{0}>", mCampo, mMonto);
                xmlData += "<producto>1</producto>";
                xmlData += "</solicitud>";

                log.Info("Consumiendo WS de consulta de Interconsumo.");
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                FiestaNETRestServices.srvIntegracionInterConsumo.srvIntegracionInteconsumoPuente srvIntegra = new FiestaNETRestServices.srvIntegracionInterConsumo.srvIntegracionInteconsumoPuente();
                InterEncript.Encriptacion dllEncriptacion = new InterEncript.Encriptacion();

                string resultado = dllEncriptacion.Proceso(xmlData, "solicitud", "1", "xml");
                string respuesta = srvIntegra.EntradaXmlCotizacion(resultado);
                resultado = dllEncriptacion.Proceso(respuesta.Trim(), "", "2", "1");

                XmlDocument xmlRespuesta = new XmlDocument();

                xmlRespuesta.PreserveWhitespace = true;
                xmlRespuesta.LoadXml(resultado);
                
                log.Info("Leyendo respuesta de Interconsumo.");
                XElement xml = XElement.Parse(resultado);

                decimal mPrimera = 0; decimal mUltima = 0; decimal mSaldoFinanciar = 0;
                string mInicio = ""; string mFin = ""; string mPlazo = "12";

                if (nivel == "InterCons12M")
                {
                    mSaldoFinanciar = Convert.ToDecimal(xmlRespuesta.SelectNodes("respuesta/cotizacion/saldo_financiar").Item(0).InnerText);
                }
                else
                {
                    mPlazo = "3"; 
                    if (nivel == "InterCons06M") mPlazo = "6";
                    if (nivel == "InterCons09M") mPlazo = "9";
                    if (nivel == "InterCons18M") mPlazo = "18";
                    if (nivel == "InterCons24M") mPlazo = "24";
                    if (nivel == "InterCons36M") mPlazo = "36";

                    mSaldoFinanciar = mMonto;
                }

                XElement query = (from item in xml.XPathSelectElements("./cotizacion/opcion")
                                  where item.Element("plazo").Value == mPlazo
                                  select item).FirstOrDefault();

                mPrimera = Convert.ToDecimal(query.Element("cuota").Value);
                mUltima = Convert.ToDecimal(query.Element("ultima").Value);

                mInicio = Convert.ToString(query.Element("primer_pago").Value);
                mFin = Convert.ToString(query.Element("vencimiento").Value);

                string[] mPrimerPago = mInicio.Split(new string[] { "-" }, StringSplitOptions.None);
                string[] mUltimoPago = mFin.Split(new string[] { "-" }, StringSplitOptions.None);

                decimal mMontoTotal = (mPrimera * (Convert.ToInt32(mPlazo) - 1)) + mUltima;

                mRetorna.SaldoFinanciar = mSaldoFinanciar;
                mRetorna.Primera = mPrimera;
                mRetorna.Ultima = mUltima;
                mRetorna.Intereses = mMontoTotal - mSaldoFinanciar;
                mRetorna.MontoTotal = mMontoTotal;
                mRetorna.PrimerPago = new DateTime(Convert.ToInt32(mPrimerPago[0]), Convert.ToInt32(mPrimerPago[1]), Convert.ToInt32(mPrimerPago[2]));
                mRetorna.UltimoPago = new DateTime(Convert.ToInt32(mUltimoPago[0]), Convert.ToInt32(mUltimoPago[1]), Convert.ToInt32(mUltimoPago[2]));
            }
            catch (Exception ex)
            {
                string mMensajeError = string.Format("{0}", CatchClass.ExMessage(ex, "CuotasInterconsumoController", "GetCuotas"));

                mRetorna.Exito = false;
                mRetorna.Mensaje = mMensajeError;
            }

            return mRetorna;
        }

    }
}
