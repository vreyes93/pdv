﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Data;
using System.Web.Http;
using FiestaNETRestServices.Models;
using MF_Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using static MF_Clases.Clases;
using System.Globalization;
using System.Threading.Tasks;
using RestSharp;
using Newtonsoft.Json;
using System.IO;

namespace FiestaNETRestServices.Controllers.Interconsumo
{
    public class DesembolsosInterconsumoController : MFApiController
    {

        // GET api/desembolsosinterconsumo
        public Clases.LoteDesombolsosFinanciera GetDesembolsosInterconsumo(string anio, string mes, string dia,int Financiera)
        {

            // - ---------------------------------------------------------------------------------
            // Variable que identifica la fecha del desembolso que el usuario consulta
            // en el sistema de Interconsumo.
            // - ---------------------------------------------------------------------------------
            DateTime mFecha;

            // - ---------------------------------------------------------------------------------
            // Variable que identifica la fecha del plazo en realizar las consultas de clientes 
            // adeudos en el sistema MF, esta fecha se calcula a partir de la consulta del desembolso
            // menos 5 meses.
            // - ---------------------------------------------------------------------------------
            DateTime mFechaPlazo;

            // - ---------------------------------------------------------------------------------
            // Variable que identifica el lote de desembolsos, es decir, contiene el conjunto de 
            // facturas que Interconsumo ha hecho el desembolso.
            // - ---------------------------------------------------------------------------------
            Clases.LoteDesombolsosFinanciera mLoteDesembolsos = new Clases.LoteDesombolsosFinanciera();
            mLoteDesembolsos.exito = false;
            mLoteDesembolsos.Financiera = Financiera;
            // - ---------------------------------------------------------------------------------
            // Variable que identifica la conexion a la base de datos.
            // - ---------------------------------------------------------------------------------
            EXACTUSERPEntities db = new EXACTUSERPEntities();

            log.Info("Iniciando consulta de desembolsos.");

            try
            {
                try
                {
                    mFecha = new DateTime(Convert.ToInt32(anio), Convert.ToInt32(mes), Convert.ToInt32(dia));
                    mFechaPlazo = mFecha.AddMonths(-5);
                }
                catch
                {
                    mLoteDesembolsos.mensaje = "La fecha ingresada es inválida.";
                    return mLoteDesembolsos;
                }
                if (Financiera == 7)
                {
                    string xmlData = "<solicitud>";
                    xmlData += "<autenticacion>";
                    xmlData += "<aplicacion>Muebles Fiesta</aplicacion>";
                    xmlData += string.Format("<usuario><![CDATA[{0}]]></usuario>", WebConfigurationManager.AppSettings["UsrInterconsumo"]);
                    xmlData += string.Format("<clave><![CDATA[{0}]]></clave>", WebConfigurationManager.AppSettings["PwdInterconsumo"]);
                    xmlData += "</autenticacion>";
                    xmlData += string.Format("<fecha>{0}-{1}{2}-{3}{4}</fecha>", anio, mes.Length == 1 ? "0" : "", mes, dia.Length == 1 ? "0" : "", dia);
                    xmlData += "</solicitud>";

                    log.Info("Consumiendo WS de de desembolsos de Interconsumo.");


                    // - ---------------------------------------------------------------------------------
                    // Momento en que se consume el servicio.
                    // - ---------------------------------------------------------------------------------
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    FiestaNETRestServices.srvIntegracionInterConsumo.srvIntegracionInteconsumoPuente srvIntegra = new FiestaNETRestServices.srvIntegracionInterConsumo.srvIntegracionInteconsumoPuente();
                    InterEncript.Encriptacion dllEncriptacion = new InterEncript.Encriptacion();


                    // - ---------------------------------------------------------------------------------
                    // Momento en que obtenemos la respuesta
                    // - ---------------------------------------------------------------------------------

                    string resultado = dllEncriptacion.Proceso(xmlData, "solicitud", "1", "xml");
                    string respuesta = srvIntegra.EntradaXmlConsultaDesembolsos(resultado);
                    resultado = dllEncriptacion.Proceso(respuesta.Trim(), "", "2", "1");

                    XmlDocument xmlRespuesta = new XmlDocument();
                    xmlRespuesta.PreserveWhitespace = true;
                    xmlRespuesta.LoadXml(resultado);
                    #region "archivo"
                    try
                    {
                        using (StreamWriter outputFile = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory+"\\log\\", "interconsumo" + anio + mes + dia + ".xml")))
                        {
                            outputFile.WriteLine(resultado);
                        }
                    }
                    catch
                    {
                    }
                    #endregion
                    log.Info("Leyendo respuesta de Interconsumo.");

                    XmlNodeList mNode = xmlRespuesta.GetElementsByTagName("registro");

                    if (mNode.Count == 0)
                    {
                        XmlNodeList mRespuesta = xmlRespuesta.GetElementsByTagName("respuesta");
                        string codigo = mRespuesta[0].ChildNodes[0].InnerText.Trim();

                        if (codigo != "000")
                        {
                            mLoteDesembolsos.mensaje = string.Format("Avisar a Informática, error de Interconsumo: {0}", codigo);
                        }
                        else
                        {
                            mLoteDesembolsos.mensaje = "No se encontraron registros.";
                        }
                        return mLoteDesembolsos;
                    }

                    // - ---------------------------------------------------------------------------------
                    // Momento en que obtenemos datos complementarios en de los desembolsos, 
                    // se realizan busquedas con varios criterios para cuadrar y mapear la informacion
                    // que interconsumo nos devuelve y la informacion registrada en el ssitema.
                    // - ---------------------------------------------------------------------------------
                    for (int ii = 0; ii < mNode.Count; ii++)
                    {
                        bool Encontrado = false;

                        Clases.Desembolso mItem = new Clases.Desembolso();
                        mItem.Fecha = mFecha.Date;
                        mItem.Prestamo = mNode[ii].ChildNodes[0].InnerText.Trim();
                        mItem.Solicitud = mNode[ii].ChildNodes[1].InnerText.Trim();
                        mItem.Factura = mNode[ii].ChildNodes[3].InnerText.Trim();
                        mItem.Cliente = mNode[ii].ChildNodes[2].InnerText.Trim();

                        string mCliente = mNode[ii].ChildNodes[2].InnerText.Trim();
                        string[] factura;
                        string mSerie = "";
                        string mNoFactura = "";

                        try
                        {
                            factura = mNode[ii].ChildNodes[3].InnerText.Trim().Split('-');
                            factura[0] = factura[0].Length > 0 && factura[0].Substring(0, 1) == "F" ? factura[0] : "F" + factura[0];

                            mSerie = factura[0].ToString() + "-";
                            mNoFactura = factura[1].ToString();
                        }
                        catch
                        {
                            mSerie = "";
                            mNoFactura = "";
                        }

                        decimal mValor = Convert.ToDecimal(mNode[ii].ChildNodes[4].InnerText.Trim());
                        mItem.Valor = mValor;

                        string mNombreCompleto;
                        ////Adición de la validación por solicitud, a partir de que 
                        ////se valida el ingreso de la solicitud en la facturación.
                        long nSolicitud = long.Parse(mNode[ii].ChildNodes[1].InnerText.Trim());
                        string mPrimerNombre = mNode[ii].ChildNodes[5].InnerText.Trim();
                        string mSegundoNombre = mNode[ii].ChildNodes[6].InnerText.Trim();
                        string mTercerNombre = mNode[ii].ChildNodes[7].InnerText.Trim();
                        string mPrimerApellido = mNode[ii].ChildNodes[8].InnerText.Trim();
                        string mSegundoApellido = mNode[ii].ChildNodes[9].InnerText.Trim();
                        string mApellidoCasada = mNode[ii].ChildNodes[10].InnerText.Trim();

                        mApellidoCasada = string.IsNullOrEmpty(mApellidoCasada) ? "" : "DE " + mApellidoCasada;

                        mNombreCompleto = string.Format("{0} {1} {2} {3} {4} {5}", mPrimerNombre, mSegundoNombre, mTercerNombre, mPrimerApellido, mSegundoApellido, mApellidoCasada);

                        // - ---------------------------------------------------------------------------------
                        // Criterio de [Solicitud], utilizado para hacer cruce de información entre el 
                        // sistema de Interconsumo y nuestro sistema.
                        // - ---------------------------------------------------------------------------------
                        if (!Encontrado
                            && !String.IsNullOrEmpty(mPrimerApellido))
                        {
                            var qFactura = (from f in db.FACTURA
                                            join p in db.MF_Pedido on f.PEDIDO equals p.PEDIDO
                                            join FI in db.MF_Factura on f.FACTURA1 equals FI.FACTURA
                                            join c in db.MF_Cliente on f.CLIENTE equals c.CLIENTE
                                            join ve in db.MF_Vendedor on f.VENDEDOR equals ve.VENDEDOR //join agregado para obtener la información
                                            join co in db.MF_Cobrador on f.COBRADOR equals co.COBRADOR // del jefe de tienda y el vendedor APRF
                                            where f.ANULADA == "N"
                                                && f.TIPO_DOCUMENTO == "F"
                                                && f.FECHA <= mFecha
                                                && f.FECHA >= mFechaPlazo
                                                 && FI.FINANCIERA == Financiera
                                                && p.SOLICITUD == nSolicitud   //--->validando por solicitud
                                                //Agregamos excepciones que no incluya expedientes ya desembolsados.
                                                && !db.MF_Factura_Estado.Any(desembolso => desembolso.FACTURA == f.FACTURA1
                                                                                && desembolso.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                            )
                                            select new
                                            {
                                                FACTURA1 = f.FACTURA1
                                                ,
                                                CLIENTE = f.CLIENTE
                                                ,
                                                NOMBRE_CLIENTE = f.NOMBRE_CLIENTE
                                                ,
                                                TOTAL_FACTURA = f.TOTAL_FACTURA
                                                ,
                                                VENDEDOR = f.VENDEDOR
                                                ,
                                                ENGANCHE = p.ENGANCHE
                                                ,
                                                FECHA = f.FECHA
                                                ,
                                                COBRADOR = f.COBRADOR
                                                ,
                                                EMAILJEFETIENDA = co.JEFE
                                                ,
                                                EMAILVENDEDOR = ve.EMAIL
                                                ,
                                                EMAILSUPERVISOR = co.SUPERVISOR
                                            } into selection
                                            orderby selection.FECHA descending
                                            select selection);

                            if (qFactura.Count() == 1)
                            {
                                mItem.FacturaMF = qFactura.First().FACTURA1;
                                mItem.ClienteMF = qFactura.First().CLIENTE;
                                mItem.NombreCliente = qFactura.First().NOMBRE_CLIENTE;
                                mItem.ValorMF = qFactura.First().TOTAL_FACTURA;
                                mItem.Vendedor = qFactura.First().VENDEDOR;
                                mItem.EngancheMF = qFactura.First().ENGANCHE;
                                mItem.Criterio = "Solicitud/Nombre cliente";
                                mItem.Cobrador = qFactura.First().COBRADOR;
                                mItem.EmailJefeTienda = qFactura.First().EMAILJEFETIENDA;
                                mItem.EmailVendedor = qFactura.First().EMAILVENDEDOR;
                                mItem.EmailSupervisor = qFactura.First().EMAILSUPERVISOR;
                                Encontrado = true;

                            }


                        }
                        // - ---------------------------------------------------------------------------------
                        // Criterio de [Cliente, Factura], utilizado para hacer cruce de información entre el 
                        // sistema de Interconsumo y nuestro sistema.
                        // - ---------------------------------------------------------------------------------
                        if (!Encontrado
                            && !String.IsNullOrEmpty(mCliente)
                            && Regex.IsMatch(mCliente, @"^\d+$")
                            && !String.IsNullOrEmpty(mNoFactura) && mNoFactura.Length>1)
                        {
                            mCliente = Convert.ToInt32(mNode[ii].ChildNodes[2].InnerText.Trim()).ToString(Const.REPLICATE.LONGITUD_7);
                            var qFactura = (from f in db.FACTURA
                                            join p in db.MF_Pedido on f.PEDIDO equals p.PEDIDO
                                            join FI in db.MF_Factura on  f.FACTURA1 equals FI.FACTURA
                                            join ve in db.MF_Vendedor on f.VENDEDOR equals ve.VENDEDOR //join agregado para obtener la información
                                            join co in db.MF_Cobrador on f.COBRADOR equals co.COBRADOR // del jefe de tienda y el vendedor APRF
                                            where f.ANULADA == "N"
                                                    && f.TIPO_DOCUMENTO == "F"
                                                    && f.CLIENTE == mCliente
                                                    && f.FACTURA1==mSerie+"-"+ mNoFactura
                                                    && FI.FINANCIERA ==Financiera
                                                    && f.FECHA <= mFecha
                                                    && f.FECHA >= mFechaPlazo
                                                    //Agregamos excepciones que no incluya expedientes ya desembolsados.
                                                    && !db.MF_Factura_Estado.Any(desembolso => desembolso.FACTURA == f.FACTURA1
                                                                                    && desembolso.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                                )
                                            select new
                                            {
                                                FACTURA1 = f.FACTURA1
                                                ,
                                                CLIENTE = f.CLIENTE
                                                ,
                                                NOMBRE_CLIENTE = f.NOMBRE_CLIENTE
                                                ,
                                                TOTAL_FACTURA = f.TOTAL_FACTURA
                                                ,
                                                VENDEDOR = f.VENDEDOR
                                                ,
                                                ENGANCHE = p.ENGANCHE
                                                ,
                                                FECHA = f.FECHA
                                                ,
                                                COBRADOR = f.COBRADOR
                                                ,
                                                EMAILJEFETIENDA = co.JEFE
                                                ,
                                                EMAILVENDEDOR = ve.EMAIL
                                                ,
                                                EMAILSUPERVISOR = co.SUPERVISOR
                                            } into selection
                                            orderby selection.FECHA descending
                                            select selection);

                            if (qFactura.Count() > 0)
                            {

                                mItem.FacturaMF = qFactura.First().FACTURA1;
                                mItem.ClienteMF = qFactura.First().CLIENTE;
                                mItem.NombreCliente = qFactura.First().NOMBRE_CLIENTE;
                                mItem.ValorMF = qFactura.First().TOTAL_FACTURA;
                                mItem.Vendedor = qFactura.First().VENDEDOR;
                                mItem.EngancheMF = qFactura.First().ENGANCHE;
                                mItem.Criterio = "Cliente - Factura";
                                mItem.Cobrador = qFactura.First().COBRADOR;
                                mItem.EmailJefeTienda = qFactura.First().EMAILJEFETIENDA;
                                mItem.EmailVendedor = qFactura.First().EMAILVENDEDOR;
                                mItem.EmailSupervisor = qFactura.First().EMAILSUPERVISOR;
                                Encontrado = true;
                            }
                        }

                        // - ---------------------------------------------------------------------------------
                        // Criterio de [Factura, Valor], utilizado para hacer cruce de información entre el 
                        // sistema de Interconsumo y nuestro sistema.
                        // - ---------------------------------------------------------------------------------
                        if (!Encontrado
                            && !String.IsNullOrEmpty(mNoFactura)
                            && mValor > 0 && mNoFactura.Length > 1)
                        {
                            var qFactura = (from f in db.FACTURA
                                            join p in db.MF_Pedido on f.PEDIDO equals p.PEDIDO
                                            join FI in db.MF_Factura on f.FACTURA1 equals FI.FACTURA
                                            join ve in db.MF_Vendedor on f.VENDEDOR equals ve.VENDEDOR //join agregado para obtener la información
                                            join co in db.MF_Cobrador on f.COBRADOR equals co.COBRADOR // del jefe de tienda y el vendedor APRF
                                            where f.ANULADA == "N"
                                                    && f.TIPO_DOCUMENTO == "F"
                                                    && FI.FINANCIERA == Financiera
                                                    && f.FACTURA1 == mSerie + "-" + mNoFactura
                                                    && f.TOTAL_FACTURA == (mValor + p.ENGANCHE)
                                                    && f.FECHA <= mFecha
                                                    && f.FECHA >= mFechaPlazo
                                                    //Agregamos excepciones que no incluya expedientes ya desembolsados.
                                                    && !db.MF_Factura_Estado.Any(desembolso => desembolso.FACTURA == f.FACTURA1
                                                                                    && desembolso.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                                )
                                            select new
                                            {
                                                FACTURA1 = f.FACTURA1
                                                ,
                                                CLIENTE = f.CLIENTE
                                                ,
                                                NOMBRE_CLIENTE = f.NOMBRE_CLIENTE
                                                ,
                                                TOTAL_FACTURA = f.TOTAL_FACTURA
                                                ,
                                                VENDEDOR = f.VENDEDOR
                                                ,
                                                ENGANCHE = p.ENGANCHE
                                                ,
                                                FECHA = f.FECHA
                                                ,
                                                COBRADOR = f.COBRADOR
                                                ,
                                                EMAILJEFETIENDA = co.JEFE
                                                ,
                                                EMAILVENDEDOR = ve.EMAIL
                                                ,
                                                EMAILSUPERVISOR = co.SUPERVISOR
                                            } into selection
                                            orderby selection.FECHA descending
                                            select selection);

                            if (qFactura.Count() > 0)
                            {

                                mItem.FacturaMF = qFactura.First().FACTURA1;
                                mItem.ClienteMF = qFactura.First().CLIENTE;
                                mItem.NombreCliente = qFactura.First().NOMBRE_CLIENTE;
                                mItem.ValorMF = qFactura.First().TOTAL_FACTURA;
                                mItem.Vendedor = qFactura.First().VENDEDOR;
                                mItem.EngancheMF = qFactura.First().ENGANCHE;
                                mItem.Criterio = "Factura - Valor";
                                mItem.Cobrador = qFactura.First().COBRADOR;
                                mItem.EmailJefeTienda = qFactura.First().EMAILJEFETIENDA;
                                mItem.EmailVendedor = qFactura.First().EMAILVENDEDOR;
                                mItem.EmailSupervisor = qFactura.First().EMAILSUPERVISOR;
                                Encontrado = true;

                            }
                        }

                        // - ---------------------------------------------------------------------------------
                        // Criterio de [Factura], utilizado para hacer cruce de información entre el 
                        // sistema de Interconsumo y nuestro sistema.
                        // - ---------------------------------------------------------------------------------
                        if (!Encontrado
                            && !String.IsNullOrEmpty(mNoFactura) && mNoFactura.Length > 1)
                        {
                            var qFactura = (from f in db.FACTURA
                                            join p in db.MF_Pedido on f.PEDIDO equals p.PEDIDO
                                            join FI in db.MF_Factura on f.FACTURA1 equals FI.FACTURA
                                            join ve in db.MF_Vendedor on f.VENDEDOR equals ve.VENDEDOR //join agregado para obtener la información
                                            join co in db.MF_Cobrador on f.COBRADOR equals co.COBRADOR // del jefe de tienda y el vendedor APRF
                                            where f.ANULADA == "N"
                                                    && f.TIPO_DOCUMENTO == "F"
                                                    && FI.FINANCIERA == Financiera
                                                     && f.FACTURA1 == mSerie + "-" + mNoFactura
                                                    && f.FECHA <= mFecha
                                                    && f.FECHA >= mFechaPlazo
                                                    //Agregamos excepciones que no incluya expedientes ya desembolsados.
                                                    && !db.MF_Factura_Estado.Any(desembolso => desembolso.FACTURA == f.FACTURA1
                                                                                    && desembolso.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                                )
                                            select new
                                            {
                                                FACTURA1 = f.FACTURA1
                                                ,
                                                CLIENTE = f.CLIENTE
                                                ,
                                                NOMBRE_CLIENTE = f.NOMBRE_CLIENTE
                                                ,
                                                TOTAL_FACTURA = f.TOTAL_FACTURA
                                                ,
                                                VENDEDOR = f.VENDEDOR
                                                ,
                                                ENGANCHE = p.ENGANCHE
                                                ,
                                                FECHA = f.FECHA
                                                ,
                                                COBRADOR = f.COBRADOR
                                                ,
                                                EMAILJEFETIENDA = co.JEFE
                                                ,
                                                EMAILVENDEDOR = ve.EMAIL
                                                ,
                                                EMAILSUPERVISOR = co.SUPERVISOR
                                            } into selection
                                            orderby selection.FECHA descending
                                            select selection);

                            if (qFactura.Count() > 0)
                            {
                                mItem.FacturaMF = qFactura.First().FACTURA1;
                                mItem.ClienteMF = qFactura.First().CLIENTE;
                                mItem.NombreCliente = qFactura.First().NOMBRE_CLIENTE;
                                mItem.ValorMF = qFactura.First().TOTAL_FACTURA;
                                mItem.Vendedor = qFactura.First().VENDEDOR;
                                mItem.EngancheMF = qFactura.First().ENGANCHE;
                                mItem.Criterio = "Factura";
                                mItem.Cobrador = qFactura.First().COBRADOR;
                                mItem.EmailJefeTienda = qFactura.First().EMAILJEFETIENDA;
                                mItem.EmailVendedor = qFactura.First().EMAILVENDEDOR;
                                mItem.EmailSupervisor = qFactura.First().EMAILSUPERVISOR;
                                Encontrado = true;

                            }
                        }
                        // - ---------------------------------------------------------------------------------
                        // Criterio de [Cliente, Valor], utilizado para hacer cruce de información entre el 
                        // sistema de Interconsumo y nuestro sistema.
                        // - ---------------------------------------------------------------------------------
                        if (!Encontrado
                            && !String.IsNullOrEmpty(mCliente)
                            && Regex.IsMatch(mCliente, @"^\d+$")
                            && mValor > 0)
                        {

                            mCliente = Convert.ToInt32(mNode[ii].ChildNodes[2].InnerText.Trim()).ToString(Const.REPLICATE.LONGITUD_7);
                            var qFactura = (from f in db.FACTURA
                                            join p in db.MF_Pedido on f.PEDIDO equals p.PEDIDO
                                            join FI in db.MF_Factura on f.FACTURA1 equals FI.FACTURA
                                            join ve in db.MF_Vendedor on f.VENDEDOR equals ve.VENDEDOR //join agregado para obtener la información
                                            join co in db.MF_Cobrador on f.COBRADOR equals co.COBRADOR // del jefe de tienda y el vendedor APRF
                                            where f.ANULADA == "N"
                                                    && f.TIPO_DOCUMENTO == "F"
                                                    && FI.FINANCIERA == Financiera
                                                    && f.CLIENTE == mCliente
                                                    && f.TOTAL_FACTURA == (mValor + p.ENGANCHE)
                                                    && f.FECHA <= mFecha
                                                    && f.FECHA >= mFechaPlazo
                                                    //Agregamos excepciones que no incluya expedientes ya desembolsados.
                                                    && !db.MF_Factura_Estado.Any(desembolso => desembolso.FACTURA == f.FACTURA1
                                                                                    && desembolso.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                                )
                                            select new
                                            {
                                                FACTURA1 = f.FACTURA1
                                                ,
                                                CLIENTE = f.CLIENTE
                                                ,
                                                NOMBRE_CLIENTE = f.NOMBRE_CLIENTE
                                                ,
                                                TOTAL_FACTURA = f.TOTAL_FACTURA
                                                ,
                                                VENDEDOR = f.VENDEDOR
                                                ,
                                                ENGANCHE = p.ENGANCHE
                                                ,
                                                FECHA = f.FECHA
                                                ,
                                                COBRADOR = f.COBRADOR
                                                ,
                                                EMAILJEFETIENDA = co.JEFE
                                                ,
                                                EMAILVENDEDOR = ve.EMAIL
                                                ,
                                                EMAILSUPERVISOR = co.SUPERVISOR
                                            } into selection
                                            orderby selection.FECHA descending
                                            select selection);

                            if (qFactura.Count() > 0)
                            {
                                mItem.FacturaMF = qFactura.First().FACTURA1;
                                mItem.ClienteMF = qFactura.First().CLIENTE;
                                mItem.NombreCliente = qFactura.First().NOMBRE_CLIENTE;
                                mItem.ValorMF = qFactura.First().TOTAL_FACTURA;
                                mItem.Vendedor = qFactura.First().VENDEDOR;
                                mItem.EngancheMF = qFactura.First().ENGANCHE;
                                mItem.Criterio = "Cliente - Valor";
                                mItem.Cobrador = qFactura.First().COBRADOR;
                                mItem.EmailJefeTienda = qFactura.First().EMAILJEFETIENDA;
                                mItem.EmailVendedor = qFactura.First().EMAILVENDEDOR;
                                mItem.EmailSupervisor = qFactura.First().EMAILSUPERVISOR;
                                Encontrado = true;

                            }
                        }

                        // - ---------------------------------------------------------------------------------
                        // Criterio de [Cliente], utilizado para hacer cruce de información entre el 
                        // sistema de Interconsumo y nuestro sistema.
                        // - ---------------------------------------------------------------------------------
                        if (!Encontrado
                            && !String.IsNullOrEmpty(mCliente)
                            && Regex.IsMatch(mCliente, @"^\d+$"))
                        {
                            mCliente = Convert.ToInt32(mNode[ii].ChildNodes[2].InnerText.Trim()).ToString(Const.REPLICATE.LONGITUD_7);
                            var qFactura = (from f in db.FACTURA
                                            join p in db.MF_Pedido on f.PEDIDO equals p.PEDIDO
                                            join FI in db.MF_Factura on f.FACTURA1 equals FI.FACTURA
                                            join ve in db.MF_Vendedor on f.VENDEDOR equals ve.VENDEDOR //join agregado para obtener la información
                                            join co in db.MF_Cobrador on f.COBRADOR equals co.COBRADOR // del jefe de tienda y el vendedor APRF
                                            where f.ANULADA == "N"
                                                    && f.TIPO_DOCUMENTO == "F"
                                                    && FI.FINANCIERA == Financiera
                                                && f.CLIENTE == mCliente
                                                    && f.FECHA <= mFecha
                                                    && f.FECHA >= mFechaPlazo
                                                    //Agregamos excepciones que no incluya expedientes ya desembolsados.
                                                    && !db.MF_Factura_Estado.Any(desembolso => desembolso.FACTURA == f.FACTURA1
                                                                                    && desembolso.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                                )

                                            select new
                                            {
                                                FACTURA1 = f.FACTURA1
                                                ,
                                                CLIENTE = f.CLIENTE
                                                ,
                                                NOMBRE_CLIENTE = f.NOMBRE_CLIENTE
                                                ,
                                                TOTAL_FACTURA = f.TOTAL_FACTURA
                                                ,
                                                VENDEDOR = f.VENDEDOR
                                                ,
                                                ENGANCHE = p.ENGANCHE
                                                ,
                                                FECHA = f.FECHA
                                                ,
                                                COBRADOR = f.COBRADOR
                                                ,
                                                EMAILJEFETIENDA = co.JEFE
                                                ,
                                                EMAILVENDEDOR = ve.EMAIL
                                                ,
                                                EMAILSUPERVISOR = co.SUPERVISOR
                                            } into selection
                                            orderby selection.FECHA descending
                                            select selection);

                            if (qFactura.Count() > 0)
                            {
                                mItem.FacturaMF = qFactura.First().FACTURA1;
                                mItem.ClienteMF = qFactura.First().CLIENTE;
                                mItem.NombreCliente = qFactura.First().NOMBRE_CLIENTE;
                                mItem.ValorMF = qFactura.First().TOTAL_FACTURA;
                                mItem.Vendedor = qFactura.First().VENDEDOR;
                                mItem.EngancheMF = qFactura.First().ENGANCHE;
                                mItem.Criterio = "Cliente";
                                mItem.Cobrador = qFactura.First().COBRADOR;
                                mItem.EmailJefeTienda = qFactura.First().EMAILJEFETIENDA;
                                mItem.EmailVendedor = qFactura.First().EMAILVENDEDOR;
                                mItem.EmailSupervisor = qFactura.First().EMAILSUPERVISOR;
                                Encontrado = true;

                            }
                        }

                        // - ---------------------------------------------------------------------------------
                        // Criterio de [Nombre del cliente, Valor], utilizado para hacer cruce de información entre el 
                        // sistema de Interconsumo y nuestro sistema.
                        // - ---------------------------------------------------------------------------------
                        if (!Encontrado
                            && !String.IsNullOrEmpty(mPrimerApellido))
                        {
                            var qFactura = (from f in db.FACTURA
                                            join p in db.MF_Pedido on f.PEDIDO equals p.PEDIDO
                                            join c in db.MF_Cliente on f.CLIENTE equals c.CLIENTE
                                            join FI in db.MF_Factura on f.FACTURA1 equals FI.FACTURA
                                            join ve in db.MF_Vendedor on f.VENDEDOR equals ve.VENDEDOR //join agregado para obtener la información
                                            join co in db.MF_Cobrador on f.COBRADOR equals co.COBRADOR // del jefe de tienda y el vendedor APRF
                                            where f.ANULADA == "N"
                                                && f.TIPO_DOCUMENTO == "F"
                                                && FI.FINANCIERA == Financiera
                                                && f.TOTAL_FACTURA == (mValor + p.ENGANCHE)
                                                && f.FECHA <= mFecha
                                                && f.FECHA >= mFechaPlazo
                                                && c.PRIMER_NOMBRE == mPrimerNombre
                                                && c.SEGUNDO_NOMBRE == mSegundoNombre
                                                && c.TERCER_NOMBRE == mTercerNombre
                                                && c.PRIMER_APELLIDO == mPrimerApellido
                                                && c.SEGUNDO_APELLIDO == mSegundoApellido
                                                && c.APELLIDO_CASADA == mApellidoCasada
                                                //Agregamos excepciones que no incluya expedientes ya desembolsados.
                                                && !db.MF_Factura_Estado.Any(desembolso => desembolso.FACTURA == f.FACTURA1
                                                                                && desembolso.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                            )
                                            select new
                                            {
                                                FACTURA1 = f.FACTURA1
                                                ,
                                                CLIENTE = f.CLIENTE
                                                ,
                                                NOMBRE_CLIENTE = f.NOMBRE_CLIENTE
                                                ,
                                                TOTAL_FACTURA = f.TOTAL_FACTURA
                                                ,
                                                VENDEDOR = f.VENDEDOR
                                                ,
                                                ENGANCHE = p.ENGANCHE
                                                ,
                                                FECHA = f.FECHA
                                                ,
                                                COBRADOR = f.COBRADOR
                                                ,
                                                EMAILJEFETIENDA = co.JEFE
                                                ,
                                                EMAILVENDEDOR = ve.EMAIL
                                                ,
                                                EMAILSUPERVISOR = co.SUPERVISOR
                                            } into selection
                                            orderby selection.FECHA descending
                                            select selection);

                            if (qFactura.Count() == 1)
                            {
                                mItem.FacturaMF = qFactura.First().FACTURA1;
                                mItem.ClienteMF = qFactura.First().CLIENTE;
                                mItem.NombreCliente = qFactura.First().NOMBRE_CLIENTE;
                                mItem.ValorMF = qFactura.First().TOTAL_FACTURA;
                                mItem.Vendedor = qFactura.First().VENDEDOR;
                                mItem.EngancheMF = qFactura.First().ENGANCHE;
                                mItem.Criterio = "Nombre de cliente - Valor";
                                mItem.Cobrador = qFactura.First().COBRADOR;
                                mItem.EmailJefeTienda = qFactura.First().EMAILJEFETIENDA;
                                mItem.EmailVendedor = qFactura.First().EMAILVENDEDOR;
                                mItem.EmailSupervisor = qFactura.First().EMAILSUPERVISOR;
                                Encontrado = true;

                            }


                        }



                        if (Encontrado)
                            mItem.ReadOnly = true;
                        else
                        {
                            mItem.ReadOnly = false;
                            mItem.NombreCliente = mNombreCompleto;
                        }

                        //Excepciones
                        if (mItem.Prestamo != "201705197501"
                            && mItem.Prestamo != "201805699356"
                            && mItem.Prestamo != "201805699364"
                            && mItem.Prestamo != "201805699471"
                            && mItem.Prestamo != "201805699562"
                            && mItem.Prestamo != "201805699760"
                            && mItem.Prestamo != "201805699794"
                            && mItem.Prestamo != "201805699810"
                            && mItem.Prestamo != "201805699836"
                            && mItem.Prestamo != "201805699935"
                            && mItem.Prestamo != "201805699851"
                            && mItem.Prestamo != "201906101070"
                            && mItem.Prestamo != "201906103324"
                            && mItem.Prestamo != "201906103415"
                            && mItem.Prestamo != "201906103464"
                            && mItem.Prestamo != "201906103472"
                            && mItem.Prestamo != "201906103571"
                            && mItem.Prestamo != "201906103605"
                            && mItem.Prestamo != "201906103613"
                            && mItem.Prestamo != "201906103803"
                            && mItem.Prestamo != "201906103811"
                            && mItem.Prestamo != "201906104074"
                            && mItem.Prestamo != "201906104082"
                             && db.MF_Catalogo.Where(c => c.CODIGO_TABLA == Const.DESEMBOLSOS_EXCLUIDOS && c.NOMBRE_CAT ==mItem.Prestamo).Any()==false)
                            mLoteDesembolsos.Desembolsos.Add(mItem);
                            mLoteDesembolsos.exito = true;
                    }
                }
                else if (Financiera == 12)
                {

                    string mUrlCrediPlus = WebConfigurationManager.AppSettings["DefaultATIDServicesURL"];
                    var client = new RestClient(mUrlCrediPlus);
                    var restRequest = new RestRequest("api/DesembolsoCredito/" + anio + "-" + mes + "-" + dia + "/" + anio + "-" + mes + "-" + dia, Method.GET)
                    {
                        RequestFormat = DataFormat.Json
                    };
                    List<DesembolsoATID> oRespuesta;
                    var result = client.Get(restRequest);
                    if (result != null)
                    {
                        #region "archivo"
                        try
                        {
                            using (StreamWriter outputFile = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "\\log\\", "crediplus" + anio + mes + dia + ".json")))
                            {
                                outputFile.WriteLine(result.Content);
                            }
                        }
                        catch 
                        {
                           
                        }
                        #endregion

                        oRespuesta = JsonConvert.DeserializeObject<List<DesembolsoATID>>(result.Content);
                        if (oRespuesta != null)
                            if (oRespuesta.Count() > 0)
                            {
                                foreach (DesembolsoATID desembolso in oRespuesta)
                                {
                                    
                                        bool Encontrado = false;

                                        Clases.Desembolso mItem = new Clases.Desembolso();
                                        mItem.Fecha = mFecha.Date;
                                        mItem.Prestamo = desembolso.Credito;
                                        mItem.Solicitud = desembolso.solicitud;
                                        mItem.Factura = desembolso.Factura;
                                        mItem.Cliente = desembolso.Cliente;

                                        string mCliente = desembolso.Cliente;
                                        string[] factura;
                                        string mSerie = "";
                                        string mNoFactura = "";

                                        try
                                        {
                                            factura = desembolso.Factura.Trim().Split('-');
                                            factura[0] = factura[0].Length > 0 && factura[0].Substring(0, 1) == "F" ? factura[0] : "F" + factura[0];

                                            mSerie = factura[0].ToString() + "-";
                                            mNoFactura = factura[1].ToString();
                                        }
                                        catch
                                        {
                                            mSerie = "";
                                            mNoFactura = "";
                                        }

                                        decimal mValor = Convert.ToDecimal(desembolso.ValorDesembolso);
                                        mItem.Valor = mValor;

                                        string mNombreCompleto;
                                        ////Adición de la validación por solicitud, a partir de que 
                                        ////se valida el ingreso de la solicitud en la facturación.
                                        long nSolicitud = long.Parse(desembolso.solicitud);
                                        string mPrimerNombre = desembolso.PrimerNombre.Trim();
                                        string mSegundoNombre = desembolso.SegundoNombre.Trim();
                                        string mTercerNombre = desembolso.TercerNombre.Trim();
                                        string mPrimerApellido = desembolso.PrimerApellido.Trim();
                                        string mSegundoApellido = desembolso.SegundoApellido.Trim();
                                        string mApellidoCasada = desembolso.ApellidoCasada.Trim();

                                        mApellidoCasada = string.IsNullOrEmpty(mApellidoCasada) ? "" :(mApellidoCasada.ToUpper().Contains("DE")?"": "DE ") + mApellidoCasada;

                                        mNombreCompleto = string.Format("{0} {1} {2} {3} {4} {5}", mPrimerNombre, mSegundoNombre, mTercerNombre, mPrimerApellido, mSegundoApellido, mApellidoCasada);

                                    // - ---------------------------------------------------------------------------------
                                    // Criterio de [Solicitud], utilizado para hacer cruce de información entre el 
                                    // sistema de Interconsumo y nuestro sistema.
                                    // - ---------------------------------------------------------------------------------
                                    if (!Encontrado
                                            && !String.IsNullOrEmpty(desembolso.solicitud))
                                    {
                                        var qFactura = (from f in db.FACTURA
                                                        join fa in db.MF_Factura on f.FACTURA1 equals fa.FACTURA
                                                        join p in db.MF_Pedido on f.PEDIDO equals p.PEDIDO
                                                        join c in db.MF_Cliente on f.CLIENTE equals c.CLIENTE
                                                        join ve in db.MF_Vendedor on f.VENDEDOR equals ve.VENDEDOR //join agregado para obtener la información
                                                        join co in db.MF_Cobrador on f.COBRADOR equals co.COBRADOR // del jefe de tienda y el vendedor APRF
                                                        where f.ANULADA == "N"
                                                            && f.TIPO_DOCUMENTO == "F"
                                                            && f.FECHA <= mFecha
                                                            && f.FECHA >= mFechaPlazo
                                                            && p.SOLICITUD == nSolicitud   //--->validando por solicitud
                                                             && fa.FINANCIERA == Financiera
                                                            //Agregamos excepciones que no incluya expedientes ya desembolsados.
                                                            && !db.MF_Factura_Estado.Any(desembols => desembols.FACTURA == f.FACTURA1
                                                                                            && desembols.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                                        )
                                                        select new
                                                        {
                                                            FACTURA1 = f.FACTURA1
                                                            ,
                                                            CLIENTE = f.CLIENTE
                                                            ,
                                                            NOMBRE_CLIENTE = f.NOMBRE_CLIENTE
                                                            ,
                                                            TOTAL_FACTURA = f.TOTAL_FACTURA
                                                            ,
                                                            VENDEDOR = f.VENDEDOR
                                                            ,
                                                            ENGANCHE = p.ENGANCHE
                                                            ,
                                                            FECHA = f.FECHA
                                                            ,
                                                            COBRADOR = f.COBRADOR
                                                            ,
                                                            EMAILJEFETIENDA = co.JEFE
                                                            ,
                                                            EMAILVENDEDOR = ve.EMAIL
                                                            ,
                                                            EMAILSUPERVISOR = co.SUPERVISOR
                                                        } into selection
                                                        orderby selection.FECHA descending
                                                        select selection);

                                        if (qFactura.Count() == 1)
                                        {
                                            mItem.FacturaMF = qFactura.First().FACTURA1;
                                            mItem.ClienteMF = qFactura.First().CLIENTE;
                                            mItem.NombreCliente = qFactura.First().NOMBRE_CLIENTE;
                                            mItem.ValorMF = qFactura.First().TOTAL_FACTURA;
                                            mItem.Vendedor = qFactura.First().VENDEDOR;
                                            mItem.EngancheMF = qFactura.First().ENGANCHE;
                                            mItem.Criterio = "Solicitud/Nombre cliente";
                                            mItem.Cobrador = qFactura.First().COBRADOR;
                                            mItem.EmailJefeTienda = qFactura.First().EMAILJEFETIENDA;
                                            mItem.EmailVendedor = qFactura.First().EMAILVENDEDOR;
                                            mItem.EmailSupervisor = qFactura.First().EMAILSUPERVISOR;
                                            Encontrado = true;

                                        }


                                    }


                                    // - ---------------------------------------------------------------------------------
                                    // Criterio de [Cliente, Factura], utilizado para hacer cruce de información entre el 
                                    // sistema de CREDIPLUS y nuestro sistema.
                                    // - ---------------------------------------------------------------------------------
                                    if (!Encontrado
                                        && !String.IsNullOrEmpty(mCliente)
                                        && Regex.IsMatch(mCliente, @"^\d+$")
                                        && !String.IsNullOrEmpty(mNoFactura)
                                        && mNoFactura.Length > 0)
                                    {
                                        mCliente = Convert.ToInt32(desembolso.Cliente).ToString(Const.REPLICATE.LONGITUD_7);
                                        var qFactura = (from f in db.FACTURA
                                                        join fa in db.MF_Factura on f.FACTURA1 equals fa.FACTURA
                                                        join p in db.MF_Pedido on f.PEDIDO equals p.PEDIDO
                                                        join ve in db.MF_Vendedor on f.VENDEDOR equals ve.VENDEDOR //join agregado para obtener la información
                                                        join co in db.MF_Cobrador on f.COBRADOR equals co.COBRADOR // del jefe de tienda y el vendedor APRF
                                                        where f.ANULADA == "N"
                                                                && f.TIPO_DOCUMENTO == "F"
                                                                && f.CLIENTE == mCliente
                                                                && fa.FINANCIERA == Financiera
                                                                 && f.FACTURA1 == mSerie + "-" + mNoFactura
                                                                && f.FECHA <= mFecha
                                                                && f.FECHA >= mFechaPlazo
                                                                //Agregamos excepciones que no incluya expedientes ya desembolsados.
                                                                && !db.MF_Factura_Estado.Any(desembols => desembols.FACTURA == f.FACTURA1
                                                                                                && desembols.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                                            )
                                                        select new
                                                        {
                                                            FACTURA1 = f.FACTURA1
                                                            ,
                                                            CLIENTE = f.CLIENTE
                                                            ,
                                                            NOMBRE_CLIENTE = f.NOMBRE_CLIENTE
                                                            ,
                                                            TOTAL_FACTURA = f.TOTAL_FACTURA
                                                            ,
                                                            VENDEDOR = f.VENDEDOR
                                                            ,
                                                            ENGANCHE = p.ENGANCHE
                                                            ,
                                                            FECHA = f.FECHA
                                                            ,
                                                            COBRADOR = f.COBRADOR
                                                            ,
                                                            EMAILJEFETIENDA = co.JEFE
                                                            ,
                                                            EMAILVENDEDOR = ve.EMAIL
                                                            ,
                                                            EMAILSUPERVISOR = co.SUPERVISOR
                                                        } into selection
                                                        orderby selection.FECHA descending
                                                        select selection);

                                        if (qFactura.Count() > 0)
                                        {

                                            mItem.FacturaMF = qFactura.First().FACTURA1;
                                            mItem.ClienteMF = qFactura.First().CLIENTE;
                                            mItem.NombreCliente = qFactura.First().NOMBRE_CLIENTE;
                                            mItem.ValorMF = qFactura.First().TOTAL_FACTURA;
                                            mItem.Vendedor = qFactura.First().VENDEDOR;
                                            mItem.EngancheMF = qFactura.First().ENGANCHE;
                                            mItem.Criterio = "Cliente - Factura";
                                            mItem.Cobrador = qFactura.First().COBRADOR;
                                            mItem.EmailJefeTienda = qFactura.First().EMAILJEFETIENDA;
                                            mItem.EmailVendedor = qFactura.First().EMAILVENDEDOR;
                                            mItem.EmailSupervisor = qFactura.First().EMAILSUPERVISOR;
                                            Encontrado = true;
                                        }
                                    }
                                    
                                        

                                    // - ---------------------------------------------------------------------------------
                                    // Criterio de [Factura, Valor], utilizado para hacer cruce de información entre el 
                                    // sistema de CREDIPLUS y nuestro sistema.
                                    // - ---------------------------------------------------------------------------------
                                    if (!Encontrado
                                            && !String.IsNullOrEmpty(mNoFactura)
                                             && mNoFactura.Length>0
                                            && mValor > 0)
                                        {
                                            var qFactura = (from f in db.FACTURA
                                                            join fa in db.MF_Factura on f.FACTURA1 equals fa.FACTURA
                                                            join p in db.MF_Pedido on f.PEDIDO equals p.PEDIDO
                                                            join ve in db.MF_Vendedor on f.VENDEDOR equals ve.VENDEDOR //join agregado para obtener la información
                                                            join co in db.MF_Cobrador on f.COBRADOR equals co.COBRADOR // del jefe de tienda y el vendedor APRF
                                                            where f.ANULADA == "N"
                                                                    && f.TIPO_DOCUMENTO == "F"
                                                                     && fa.FINANCIERA == Financiera
                                                                     && f.FACTURA1 == mSerie + "-" + mNoFactura
                                                                    && f.TOTAL_FACTURA == (mValor + p.ENGANCHE)
                                                                    && f.FECHA <= mFecha
                                                                    && f.FECHA >= mFechaPlazo
                                                                    //Agregamos excepciones que no incluya expedientes ya desembolsados.
                                                                    && !db.MF_Factura_Estado.Any(desembols => desembols.FACTURA == f.FACTURA1
                                                                                                    && desembols.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                                                )
                                                            select new
                                                            {
                                                                FACTURA1 = f.FACTURA1
                                                                ,
                                                                CLIENTE = f.CLIENTE
                                                                ,
                                                                NOMBRE_CLIENTE = f.NOMBRE_CLIENTE
                                                                ,
                                                                TOTAL_FACTURA = f.TOTAL_FACTURA
                                                                ,
                                                                VENDEDOR = f.VENDEDOR
                                                                ,
                                                                ENGANCHE = p.ENGANCHE
                                                                ,
                                                                FECHA = f.FECHA
                                                                ,
                                                                COBRADOR = f.COBRADOR
                                                                ,
                                                                EMAILJEFETIENDA = co.JEFE
                                                                ,
                                                                EMAILVENDEDOR = ve.EMAIL
                                                                ,
                                                                EMAILSUPERVISOR = co.SUPERVISOR
                                                            } into selection
                                                            orderby selection.FECHA descending
                                                            select selection);

                                            if (qFactura.Count() > 0)
                                            {

                                                mItem.FacturaMF = qFactura.First().FACTURA1;
                                                mItem.ClienteMF = qFactura.First().CLIENTE;
                                                mItem.NombreCliente = qFactura.First().NOMBRE_CLIENTE;
                                                mItem.ValorMF = qFactura.First().TOTAL_FACTURA;
                                                mItem.Vendedor = qFactura.First().VENDEDOR;
                                                mItem.EngancheMF = qFactura.First().ENGANCHE;
                                                mItem.Criterio = "Factura - Valor";
                                                mItem.Cobrador = qFactura.First().COBRADOR;
                                                mItem.EmailJefeTienda = qFactura.First().EMAILJEFETIENDA;
                                                mItem.EmailVendedor = qFactura.First().EMAILVENDEDOR;
                                                mItem.EmailSupervisor = qFactura.First().EMAILSUPERVISOR;
                                                Encontrado = true;

                                            }
                                        }

                                    // - ---------------------------------------------------------------------------------
                                    // Criterio de [Factura], utilizado para hacer cruce de información entre el 
                                    // sistema de CREDIPLUS y nuestro sistema.
                                    // - ---------------------------------------------------------------------------------
                                    if (!Encontrado
                                            && !String.IsNullOrEmpty(mNoFactura))
                                        {
                                            var qFactura = (from f in db.FACTURA
                                                            join fa in db.MF_Factura on f.FACTURA1 equals fa.FACTURA
                                                            join p in db.MF_Pedido on f.PEDIDO equals p.PEDIDO
                                                            join ve in db.MF_Vendedor on f.VENDEDOR equals ve.VENDEDOR //join agregado para obtener la información
                                                            join co in db.MF_Cobrador on f.COBRADOR equals co.COBRADOR // del jefe de tienda y el vendedor APRF
                                                            where f.ANULADA == "N"
                                                                    && f.TIPO_DOCUMENTO == "F"
                                                                    && fa.FINANCIERA == Financiera
                                                                   && f.FACTURA1 == mSerie + "-" + mNoFactura
                                                                    && f.FECHA <= mFecha
                                                                    && f.FECHA >= mFechaPlazo
                                                                    //Agregamos excepciones que no incluya expedientes ya desembolsados.
                                                                    && !db.MF_Factura_Estado.Any(d => d.FACTURA == f.FACTURA1
                                                                                                    && d.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                                                )
                                                            select new
                                                            {
                                                                FACTURA1 = f.FACTURA1
                                                                ,
                                                                CLIENTE = f.CLIENTE
                                                                ,
                                                                NOMBRE_CLIENTE = f.NOMBRE_CLIENTE
                                                                ,
                                                                TOTAL_FACTURA = f.TOTAL_FACTURA
                                                                ,
                                                                VENDEDOR = f.VENDEDOR
                                                                ,
                                                                ENGANCHE = p.ENGANCHE
                                                                ,
                                                                FECHA = f.FECHA
                                                                ,
                                                                COBRADOR = f.COBRADOR
                                                                ,
                                                                EMAILJEFETIENDA = co.JEFE
                                                                ,
                                                                EMAILVENDEDOR = ve.EMAIL
                                                                ,
                                                                EMAILSUPERVISOR = co.SUPERVISOR
                                                            } into selection
                                                            orderby selection.FECHA descending
                                                            select selection);

                                            if (qFactura.Count() > 0)
                                            {
                                                mItem.FacturaMF = qFactura.First().FACTURA1;
                                                mItem.ClienteMF = qFactura.First().CLIENTE;
                                                mItem.NombreCliente = qFactura.First().NOMBRE_CLIENTE;
                                                mItem.ValorMF = qFactura.First().TOTAL_FACTURA;
                                                mItem.Vendedor = qFactura.First().VENDEDOR;
                                                mItem.EngancheMF = qFactura.First().ENGANCHE;
                                                mItem.Criterio = "Factura";
                                                mItem.Cobrador = qFactura.First().COBRADOR;
                                                mItem.EmailJefeTienda = qFactura.First().EMAILJEFETIENDA;
                                                mItem.EmailVendedor = qFactura.First().EMAILVENDEDOR;
                                                mItem.EmailSupervisor = qFactura.First().EMAILSUPERVISOR;
                                                Encontrado = true;

                                            }
                                        }
                                    // - ---------------------------------------------------------------------------------
                                    // Criterio de [Cliente, Valor], utilizado para hacer cruce de información entre el 
                                    // sistema de CREDIPLUS y nuestro sistema.
                                    // - ---------------------------------------------------------------------------------
                                    if (!Encontrado
                                            && !String.IsNullOrEmpty(mCliente)
                                            && Regex.IsMatch(mCliente, @"^\d+$")
                                            && mValor > 0)
                                        {

                                            mCliente = Convert.ToInt32(desembolso.Cliente).ToString(Const.REPLICATE.LONGITUD_7);
                                            var qFactura = (from f in db.FACTURA
                                                            join fa in db.MF_Factura on f.FACTURA1 equals fa.FACTURA
                                                            join p in db.MF_Pedido on f.PEDIDO equals p.PEDIDO
                                                            join ve in db.MF_Vendedor on f.VENDEDOR equals ve.VENDEDOR //join agregado para obtener la información
                                                            join co in db.MF_Cobrador on f.COBRADOR equals co.COBRADOR // del jefe de tienda y el vendedor APRF
                                                            where f.ANULADA == "N"
                                                                    && f.TIPO_DOCUMENTO == "F"
                                                                     && fa.FINANCIERA == Financiera
                                                                    && f.CLIENTE == mCliente
                                                                    && f.TOTAL_FACTURA == (mValor + p.ENGANCHE)
                                                                    && f.FECHA <= mFecha
                                                                    && f.FECHA >= mFechaPlazo
                                                                    //Agregamos excepciones que no incluya expedientes ya desembolsados.
                                                                    && !db.MF_Factura_Estado.Any(d => d.FACTURA == f.FACTURA1
                                                                                                    && d.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                                                )
                                                            select new
                                                            {
                                                                FACTURA1 = f.FACTURA1
                                                                ,
                                                                CLIENTE = f.CLIENTE
                                                                ,
                                                                NOMBRE_CLIENTE = f.NOMBRE_CLIENTE
                                                                ,
                                                                TOTAL_FACTURA = f.TOTAL_FACTURA
                                                                ,
                                                                VENDEDOR = f.VENDEDOR
                                                                ,
                                                                ENGANCHE = p.ENGANCHE
                                                                ,
                                                                FECHA = f.FECHA
                                                                ,
                                                                COBRADOR = f.COBRADOR
                                                                ,
                                                                EMAILJEFETIENDA = co.JEFE
                                                                ,
                                                                EMAILVENDEDOR = ve.EMAIL
                                                                ,
                                                                EMAILSUPERVISOR = co.SUPERVISOR
                                                            } into selection
                                                            orderby selection.FECHA descending
                                                            select selection);

                                            if (qFactura.Count() > 0)
                                            {
                                                mItem.FacturaMF = qFactura.First().FACTURA1;
                                                mItem.ClienteMF = qFactura.First().CLIENTE;
                                                mItem.NombreCliente = qFactura.First().NOMBRE_CLIENTE;
                                                mItem.ValorMF = qFactura.First().TOTAL_FACTURA;
                                                mItem.Vendedor = qFactura.First().VENDEDOR;
                                                mItem.EngancheMF = qFactura.First().ENGANCHE;
                                                mItem.Criterio = "Cliente - Valor";
                                                mItem.Cobrador = qFactura.First().COBRADOR;
                                                mItem.EmailJefeTienda = qFactura.First().EMAILJEFETIENDA;
                                                mItem.EmailVendedor = qFactura.First().EMAILVENDEDOR;
                                                mItem.EmailSupervisor = qFactura.First().EMAILSUPERVISOR;
                                                Encontrado = true;

                                            }
                                        }

                                    // - ---------------------------------------------------------------------------------
                                    // Criterio de [Cliente], utilizado para hacer cruce de información entre el 
                                    // sistema de CREDIPLUS y nuestro sistema.
                                    // - ---------------------------------------------------------------------------------
                                    if (!Encontrado
                                            && !String.IsNullOrEmpty(mCliente)
                                            && Regex.IsMatch(mCliente, @"^\d+$"))
                                        {
                                            mCliente = Convert.ToInt32(desembolso.Cliente).ToString(Const.REPLICATE.LONGITUD_7);
                                            var qFactura = (from f in db.FACTURA
                                                            join fa in db.MF_Factura on f.FACTURA1 equals fa.FACTURA
                                                            join p in db.MF_Pedido on f.PEDIDO equals p.PEDIDO
                                                            join ve in db.MF_Vendedor on f.VENDEDOR equals ve.VENDEDOR //join agregado para obtener la información
                                                            join co in db.MF_Cobrador on f.COBRADOR equals co.COBRADOR // del jefe de tienda y el vendedor APRF
                                                            where f.ANULADA == "N"
                                                                    && f.TIPO_DOCUMENTO == "F"
                                                                    && fa.FINANCIERA == Financiera
                                                                    && f.CLIENTE == mCliente
                                                                    && f.FECHA <= mFecha
                                                                    && f.FECHA >= mFechaPlazo
                                                                    //Agregamos excepciones que no incluya expedientes ya desembolsados.
                                                                    && !db.MF_Factura_Estado.Any(d => d.FACTURA == f.FACTURA1
                                                                                                    && d.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                                                )

                                                            select new
                                                            {
                                                                FACTURA1 = f.FACTURA1
                                                                ,
                                                                CLIENTE = f.CLIENTE
                                                                ,
                                                                NOMBRE_CLIENTE = f.NOMBRE_CLIENTE
                                                                ,
                                                                TOTAL_FACTURA = f.TOTAL_FACTURA
                                                                ,
                                                                VENDEDOR = f.VENDEDOR
                                                                ,
                                                                ENGANCHE = p.ENGANCHE
                                                                ,
                                                                FECHA = f.FECHA
                                                                ,
                                                                COBRADOR = f.COBRADOR
                                                                ,
                                                                EMAILJEFETIENDA = co.JEFE
                                                                ,
                                                                EMAILVENDEDOR = ve.EMAIL
                                                                ,
                                                                EMAILSUPERVISOR = co.SUPERVISOR
                                                            } into selection
                                                            orderby selection.FECHA descending
                                                            select selection);

                                            if (qFactura.Count() > 0)
                                            {
                                                mItem.FacturaMF = qFactura.First().FACTURA1;
                                                mItem.ClienteMF = qFactura.First().CLIENTE;
                                                mItem.NombreCliente = qFactura.First().NOMBRE_CLIENTE;
                                                mItem.ValorMF = qFactura.First().TOTAL_FACTURA;
                                                mItem.Vendedor = qFactura.First().VENDEDOR;
                                                mItem.EngancheMF = qFactura.First().ENGANCHE;
                                                mItem.Criterio = "Cliente";
                                                mItem.Cobrador = qFactura.First().COBRADOR;
                                                mItem.EmailJefeTienda = qFactura.First().EMAILJEFETIENDA;
                                                mItem.EmailVendedor = qFactura.First().EMAILVENDEDOR;
                                                mItem.EmailSupervisor = qFactura.First().EMAILSUPERVISOR;
                                                Encontrado = true;

                                            }
                                        }

                                    // - ---------------------------------------------------------------------------------
                                    // Criterio de [Nombre del cliente, Valor], utilizado para hacer cruce de información entre el 
                                    // sistema de CREDIPLUS y nuestro sistema.
                                    // - ---------------------------------------------------------------------------------
                                    if (!Encontrado
                                            && !String.IsNullOrEmpty(mPrimerApellido))
                                        {
                                            var qFactura = (from f in db.FACTURA
                                                            join fa in db.MF_Factura on f.FACTURA1 equals fa.FACTURA
                                                            join p in db.MF_Pedido on f.PEDIDO equals p.PEDIDO
                                                            join c in db.MF_Cliente on f.CLIENTE equals c.CLIENTE
                                                            join ve in db.MF_Vendedor on f.VENDEDOR equals ve.VENDEDOR //join agregado para obtener la información
                                                            join co in db.MF_Cobrador on f.COBRADOR equals co.COBRADOR // del jefe de tienda y el vendedor APRF
                                                            where f.ANULADA == "N"
                                                                && f.TIPO_DOCUMENTO == "F"
                                                                 && fa.FINANCIERA == Financiera
                                                                && f.TOTAL_FACTURA == (mValor + p.ENGANCHE)
                                                                && f.FECHA <= mFecha
                                                                && f.FECHA >= mFechaPlazo
                                                                && c.PRIMER_NOMBRE == mPrimerNombre
                                                                && c.SEGUNDO_NOMBRE == mSegundoNombre
                                                                && c.TERCER_NOMBRE == mTercerNombre
                                                                && c.PRIMER_APELLIDO == mPrimerApellido
                                                                && c.SEGUNDO_APELLIDO == mSegundoApellido
                                                                && c.APELLIDO_CASADA == mApellidoCasada
                                                                //Agregamos excepciones que no incluya expedientes ya desembolsados.
                                                                && !db.MF_Factura_Estado.Any(d => d.FACTURA == f.FACTURA1
                                                                                                && d.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                                            )
                                                            select new
                                                            {
                                                                FACTURA1 = f.FACTURA1
                                                                ,
                                                                CLIENTE = f.CLIENTE
                                                                ,
                                                                NOMBRE_CLIENTE = f.NOMBRE_CLIENTE
                                                                ,
                                                                TOTAL_FACTURA = f.TOTAL_FACTURA
                                                                ,
                                                                VENDEDOR = f.VENDEDOR
                                                                ,
                                                                ENGANCHE = p.ENGANCHE
                                                                ,
                                                                FECHA = f.FECHA
                                                                ,
                                                                COBRADOR = f.COBRADOR
                                                                ,
                                                                EMAILJEFETIENDA = co.JEFE
                                                                ,
                                                                EMAILVENDEDOR = ve.EMAIL
                                                                ,
                                                                EMAILSUPERVISOR = co.SUPERVISOR
                                                            } into selection
                                                            orderby selection.FECHA descending
                                                            select selection);

                                            if (qFactura.Count() == 1)
                                            {
                                                mItem.FacturaMF = qFactura.First().FACTURA1;
                                                mItem.ClienteMF = qFactura.First().CLIENTE;
                                                mItem.NombreCliente = qFactura.First().NOMBRE_CLIENTE;
                                                mItem.ValorMF = qFactura.First().TOTAL_FACTURA;
                                                mItem.Vendedor = qFactura.First().VENDEDOR;
                                                mItem.EngancheMF = qFactura.First().ENGANCHE;
                                                mItem.Criterio = "Nombre de cliente - Valor";
                                                mItem.Cobrador = qFactura.First().COBRADOR;
                                                mItem.EmailJefeTienda = qFactura.First().EMAILJEFETIENDA;
                                                mItem.EmailVendedor = qFactura.First().EMAILVENDEDOR;
                                                mItem.EmailSupervisor = qFactura.First().EMAILSUPERVISOR;
                                                Encontrado = true;

                                            }
                                        }
                                        
                                        if (Encontrado)
                                            mItem.ReadOnly = true;
                                        else
                                        {
                                            mItem.ReadOnly = false;
                                            mItem.NombreCliente = mNombreCompleto;
                                        }
                                    //Excepciones
                                    if (mItem.Prestamo != "2020000918"
                                         && db.MF_Catalogo.Where(c => c.CODIGO_TABLA == Const.DESEMBOLSOS_EXCLUIDOS && c.NOMBRE_CAT == mItem.Prestamo).Any() == false)
                                        mLoteDesembolsos.Desembolsos.Add(mItem);
                                    mLoteDesembolsos.exito = true;
                                }
                            }
                            else
                            {
                                mLoteDesembolsos.mensaje = "No se encontraron registros.";
                                mLoteDesembolsos.exito = false;
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                string mMensajeError = CatchClass.ExMessage(ex, "DesembolsosInterconsumoController", "GetDesembolsos");

                log.Error(mMensajeError);
                mLoteDesembolsos.exito = false;
                mLoteDesembolsos.mensaje = mMensajeError;
                return mLoteDesembolsos;

            }

           
            return mLoteDesembolsos;
        }

        /// <summary>
        /// Esta funcion tiene como proposito cargar el desembolso en el sistema de Exactus.
        /// </summary>
        public async System.Threading.Tasks.Task<Respuesta> PostDesembolsosInterconsumoAsync([FromBody]Clases.LoteDesombolsosFinanciera LoteDesembolsos)
        {
            Respuesta mRespuesta = new Respuesta();
            DALDesembolsoInterconsumo DALDesembolsos = new DALDesembolsoInterconsumo();
            List<Desembolso> lstDesmbolsosOK = new List<Desembolso>();
            try
            {

                //se verifica que los datos esten incompletos.
                if (!validarDatosIncompletos(LoteDesembolsos))
                {
                    mRespuesta.Mensaje = "Favor de completar la información en las casillas de Cliente MF y Factura MF";
                    return mRespuesta;
                }

                //Validamos si los datos que ingresaron manualmente corresponden a datos reales 
                //en el sistema.
                Respuesta mDatosManuales = ValidarDatosManuales(ref LoteDesembolsos);
                if (!mDatosManuales.Exito)
                    return mDatosManuales;


                //Se verifica que los consecutivos esten creados correctamente.
                Respuesta mConsecutivo = DALDesembolsos.ConsecutivoExists(LoteDesembolsos.Financiera==7? Const.DESEMBOLSO_INTERCONSUMO.NOMBRE_CONSECUTIVO:Const.DESEMBOLSO_CREDIPLUS.NOMBRE_CONSECUTIVO);
                if (!mConsecutivo.Exito)
                    return mConsecutivo;


                //Se verifica que no se haya realizado el desembolso correspondiente.
                Respuesta mDesembolsoYaRealizado = DALDesembolsos.DesembolsoYaRegistrado(decimal.Parse(ObtenerFecha(LoteDesembolsos.FechaDesembolsos).Replace("/", "").Replace("-", "")),LoteDesembolsos.Financiera);
                if (mDesembolsoYaRealizado.Exito)
                {
                    mRespuesta.Mensaje = mDesembolsoYaRealizado.Mensaje;
                    return mRespuesta;
                }


                //////Se carga el desembolso
                Respuesta mDesembolsoCargado = DALDesembolsos.CargarDesembolso( LoteDesembolsos);
                if (!mDesembolsoCargado.Exito)
                    return mDesembolsoCargado;

                //VALIDAR DIFERENCIAS ENTRE FACTURA Y DESEMBOLSADO
                #region "Validación diferencias Factura-Desembolso"
                List<Desembolso> lstDesmbolsosConDif = new List<Desembolso>();
                foreach (var item in LoteDesembolsos.Desembolsos)
                {
                    if (item.Valor != item.ValorMF )
                    { 
                        if(item.ValorMF != item.Valor + item.EngancheMF)
                        {
                            lstDesmbolsosConDif.Add(item); 
                        }else
                            lstDesmbolsosOK.Add(item);
                    }else
                        lstDesmbolsosOK.Add(item);
                }
                #endregion
                #region "Notificaciones"
                //ENVIAR NOTIFICACION DESEMBOLSO PARCIAL
                    await EnviarCorreoNotifDesembolsoParcial(lstDesmbolsosConDif,LoteDesembolsos.Financiera);
                #endregion
                //ENVIAR NOTIFICACION DESEMBOLSO 
                //1)obtener las facturas por vendedor
                //2) enviar correo para cada vendedor 
                List<Desembolso> lstxFiltro = new List<Desembolso>();
                List<string> lstNombreVendedor = new List<string>();
                List<string> lstTienda = new List<string>();

                
                
                foreach (var desembolsoOk in lstDesmbolsosOK)
                {
                    if (desembolsoOk.Vendedor != null)
                        if (!lstNombreVendedor.Contains(desembolsoOk.Vendedor))
                        {
                            lstxFiltro = new List<Desembolso>();
                            lstNombreVendedor.Add(desembolsoOk.Vendedor);
                            lstxFiltro = lstDesmbolsosOK.FindAll(t => t.Vendedor == desembolsoOk.Vendedor);
                            if (lstxFiltro.Count > 0)
                               await  EnviarCorreoNotifDesembolso(lstxFiltro, "VENDEDOR",LoteDesembolsos.Financiera);
                        }
                    if (desembolsoOk.Cobrador != null)
                        if (!lstTienda.Contains(desembolsoOk.Cobrador))
                        {
                            lstxFiltro = new List<Desembolso>();
                            lstTienda.Add(desembolsoOk.Cobrador);
                            lstxFiltro = lstDesmbolsosOK.FindAll(t => t.Cobrador == desembolsoOk.Cobrador);
                            if (lstxFiltro.Count > 0)
                               await  EnviarCorreoNotifDesembolso(lstxFiltro, "TIENDA",LoteDesembolsos.Financiera);
                        }
                }
                

                mRespuesta.Objeto = lstDesmbolsosOK;
                mRespuesta.Exito = Const.OK;
                mRespuesta.Mensaje = mDesembolsoCargado.Mensaje;

            }
            catch (Exception ex)
            {
                mRespuesta.Mensaje = CatchClass.ExMessage(ex, MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name);
                log.Error(mRespuesta.Mensaje);
            }
            return mRespuesta;
        }

        private bool validarDatosIncompletos(Clases.LoteDesombolsosFinanciera LoteDesembolsos)
        {
            foreach (var item in LoteDesembolsos.Desembolsos)
            {
                if (String.IsNullOrEmpty(item.ClienteMF)
                        || String.IsNullOrEmpty(item.FacturaMF)
                    )
                {
                    return false;
                }
            }
            return true;
        }

        private Respuesta ValidarDatosManuales(ref Clases.LoteDesombolsosFinanciera LoteDesembolsos)
        {
            Respuesta mRespuesta = new Respuesta();

            foreach (var item in LoteDesembolsos.Desembolsos)
            {
                if (String.IsNullOrEmpty(item.Criterio))
                {

                    var qFactura = (from f in dbExactus.FACTURA
                                    where f.ANULADA == "N"
                                        && f.CLIENTE == item.ClienteMF
                                        && f.FACTURA1 == item.FacturaMF
                                    select f);

                    if (qFactura.Count() == 0)
                    {
                        mRespuesta.Mensaje = "Verificar los datos ingresados manualmente ClienteMF y FacturaMF.";
                        return mRespuesta;
                    }
                    else
                    {
                        item.Vendedor = qFactura.First().VENDEDOR;
                    }

                }
            }

            mRespuesta.Exito = true;
            return mRespuesta;
        }

        private string ObtenerFecha(DateTime Fecha)
        {
            return string.Format("{0}{1}/{2}{3}/{4}", Fecha.Day < 10 ? "0" : "", Fecha.Day.ToString(), Fecha.Month < 10 ? "0" : "", Fecha.Month.ToString(), Fecha.Year.ToString());
        }

        /// <summary>
        /// APRF
        /// Método para notificar a los interesados de este proceso (contabilidad, jefe de tienda y vendedor)
        /// acerca del desembolso parcial por parte de interconsumo, para que se le pueda dar el seguimiento
        /// correspondiente
        /// </summary>
        /// <param name="lteDesembolsos">Lista de desembolsos parciales (valor interconsumo <> valor Factura</param>
        private void EnviarCorreoNotifDesembolsoParcialATienda(Desembolso lDesembolso,int nFinanciera)
        {
            ////---------------------------------------------------------------------------------------------------------
            //// Variable necesarias para el envio de correo.
            ////---------------------------------------------------------------------------------------------------------
            string mRemitente = Utils.Utilitarios.GetRemitenteNotificacion(Const.REMITENTE_CORREO.REMITENTE_DESEMBOLSOS_PARCIALES, Const.LISTA_CORREO.REMITENTES_CORREO);
            string mDisplayName = "";
            string nombreFinanciera = nFinanciera == 7 ? "INTERCONSUMO" : (nFinanciera==12? "CREDIPLUS":"");
            ////---------------------------------------------------------------------------------------------
            ////Redactamos el Display name, crédito aprobado o rechazado.
            ////---------------------------------------------------------------------------------------------
            mDisplayName = "Muebles Fiesta - "+nombreFinanciera+" - Desembolsos Parciales";

            ////---------------------------------------------------------------------------------------------
            ////Redactamos el asunto del correo
            ////---------------------------------------------------------------------------------------------

            string mAsunto = string.Format("- {1} - Aviso de desembolso parcial de Factura {0}",lDesembolso.FacturaMF,nombreFinanciera);

            ////---------------------------------------------------------------------------------------------
            ////Agregamos a los destinatario
            ////---------------------------------------------------------------------------------------------
            List<string> mCuentaCorreo = new List<string>();

            ////---------------------------------------------------------------------------------------------
            ////Variable para el cuerpo del correo
            ////---------------------------------------------------------------------------------------------
            StringBuilder mMensaje = new StringBuilder();

            ///Para dar formato a los montos, con simbolo de moneda
            CultureInfo cultureToUse = new CultureInfo("es-GT");
            cultureToUse.NumberFormat.CurrencyDecimalDigits = 2;
            cultureToUse.NumberFormat.NumberDecimalDigits = 2;

           
            mCuentaCorreo.Add(lDesembolso.EmailVendedor);
            mCuentaCorreo.Add(lDesembolso.EmailJefeTienda);

            mMensaje.Append(nFinanciera==7? Utilitario.CorreoInicioFormatoRojo():Utilitario.CorreoInicioFormatoAzulYGris());
            mMensaje.Append(string.Format("<p>Estimados(as):<BR/> Les informamos sobre el siguiente desembolso parcial por parte de "+nombreFinanciera+", para su seguimiento.</p>"));
            mMensaje.Append("<BR/>");
            mMensaje.Append("<table align='center' class='style4'>");
            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Solicitud: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(lDesembolso.Solicitud.ToString()));
            mMensaje.Append("</tr>");

            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Documento: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(lDesembolso.FacturaMF));
            mMensaje.Append("</tr>");


            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Cliente: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(lDesembolso.ClienteMF + " - " + lDesembolso.NombreCliente));
            mMensaje.Append("</tr>");


            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Monto Factura: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(lDesembolso.ValorMF.ToString("C",cultureToUse)));
            mMensaje.Append("</tr>");

            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Monto Enganche: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda((lDesembolso.EngancheMF.ToString())));
            mMensaje.Append("</tr>");

            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Monto Desembolso: "));
            mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(lDesembolso.Valor.ToString("C",cultureToUse)));
            mMensaje.Append("</tr>");


            mMensaje.Append("</table>");

            mMensaje.Append(string.Format("<p>Gracias por su atención,<br>"));
            mMensaje.Append(string.Format("Saludos cordiales.</p>"));
            mMensaje.Append(Utilitario.CorreoFin());

            //---------------------------------------------------------------------------------------------------------
            //Procedemos a enviar el correo.
            //---------------------------------------------------------------------------------------------------------
            Utilitario.EnviarEmail(mRemitente, mCuentaCorreo, mAsunto, mMensaje, mDisplayName);
        }

        
                    
                

        private async Task EnviarCorreoNotifDesembolsoParcial(List<Desembolso> lteDesembolsos,int Financiera)
        {
            ////---------------------------------------------------------------------------------------------------------
            //// Variable necesarias para el envio de correo.
            ////---------------------------------------------------------------------------------------------------------
            string mRemitente = Utils.Utilitarios.GetRemitenteNotificacion(Const.REMITENTE_CORREO.REMITENTE_DESEMBOLSOS_PARCIALES, Const.LISTA_CORREO.REMITENTES_CORREO);
            string mDisplayName = "";
            string MnombreFinanciera = Financiera == 7 ? "INTERCONSUMO" : "CREDIPLUS";
            ////---------------------------------------------------------------------------------------------
            ////Redactamos el Display name, crédito aprobado o rechazado.
            ////---------------------------------------------------------------------------------------------
            mDisplayName = "Muebles Fiesta - "+ MnombreFinanciera + " - Desembolsos Parciales";

            ////---------------------------------------------------------------------------------------------
            ////Redactamos el asunto del correo
            ////---------------------------------------------------------------------------------------------

            string mAsunto = string.Format("Notificación de {0} desembolsos parciales de {1}",lteDesembolsos.Count, MnombreFinanciera);

            ////---------------------------------------------------------------------------------------------
            ////Agregamos a los destinatario
            ////---------------------------------------------------------------------------------------------
            List<string> mCuentaCorreo = new List<string>();

            ////---------------------------------------------------------------------------------------------
            ////Variable para el cuerpo del correo
            ////---------------------------------------------------------------------------------------------
            StringBuilder mMensaje = new StringBuilder();
			Utils.Utilitarios mUtilitario = new Utils.Utilitarios();
            mUtilitario.GetListaCorreo(Const.LISTA_CORREO.INTERCONSUMO_DESEMBOLSOS_PARCIALES, ref mCuentaCorreo);
            if(Financiera==7)
            mMensaje.Append(Utilitario.CorreoInicioFormatoRojo());
                else
            mMensaje.Append(Utilitario.CorreoInicioFormatoAzulYGris());

            mMensaje.Append(string.Format("<p>Estimados(as):<BR/> Les informamos que se han encontrado {0} desembolsos parciales por parte de "+MnombreFinanciera+", para su seguimiento.</p>",lteDesembolsos.Count));
            mMensaje.Append("<BR/>");
            mMensaje.Append("<table align='center' class='style4'>");
            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloCentro("Solicitud"));
            mMensaje.Append(Utilitario.FormatoTituloCentro("Factura"));
            mMensaje.Append(Utilitario.FormatoTituloCentro("Cliente"));
            mMensaje.Append(Utilitario.FormatoTituloCentro("Total Factura"));
            mMensaje.Append(Utilitario.FormatoTituloCentro("Monto Enganche"));
            mMensaje.Append(Utilitario.FormatoTituloCentro("Monto Desembolso"));
            mMensaje.Append(Utilitario.FormatoTituloCentro("Diferencia"));
            mMensaje.Append("</tr>");
            CultureInfo cultureToUse = new CultureInfo("es-GT");
            cultureToUse.NumberFormat.CurrencyDecimalDigits = 2;
            cultureToUse.NumberFormat.NumberDecimalDigits = 2;
            foreach (var item in lteDesembolsos)
            {
                mMensaje.Append("<tr>");
                decimal dEnganche = Convert.ToDecimal(item.EngancheMF.ToString().Equals("") ? "0" : item.EngancheMF.ToString());
                mMensaje.Append(Utilitario.FormatoCuerpoCentro(item.Solicitud));
                mMensaje.Append(Utilitario.FormatoCuerpoCentro(item.FacturaMF));
                mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.ClienteMF +" - "+item.NombreCliente));
                mMensaje.Append(Utilitario.FormatoCuerpoCentro(item.ValorMF.ToString("C",cultureToUse)));
                mMensaje.Append(Utilitario.FormatoCuerpoCentro(dEnganche.ToString("C",cultureToUse)));
                mMensaje.Append(Utilitario.FormatoCuerpoCentro( item.Valor.ToString("C",cultureToUse)));
                mMensaje.Append(Utilitario.FormatoCuerpoCentro("<b>"+  (item.Valor-(item.ValorMF-dEnganche)).ToString("C",cultureToUse)+"</b>"));
                
                mMensaje.Append("</tr>");
                try
                {
                    EnviarCorreoNotifDesembolsoParcialATienda(item,Financiera);
                }
                catch(Exception ex)
                {
                    log.Error("Error al enviar el correo de notificación: "+ex.Message);
                }
            }
            
           

            mMensaje.Append("</table>");
            mMensaje.Append("<br/><br/>");
            mMensaje.Append(string.Format("<p>Gracias por su atención,<br>"));
            mMensaje.Append(string.Format("Saludos cordiales.</p>"));
            mMensaje.Append(Utilitario.CorreoFin());

            //---------------------------------------------------------------------------------------------------------
            //Procedemos a enviar el correo.
            //---------------------------------------------------------------------------------------------------------
             Utilitario.EnviarEmail(mRemitente, mCuentaCorreo, mAsunto, mMensaje, mDisplayName);
        }

        private async Task EnviarCorreoNotifDesembolso(List<Desembolso> lDesembolso, string opcion,int financiera)
        {
            string mRemitente = Utils.Utilitarios.GetRemitenteNotificacion(Const.REMITENTE_CORREO.REMITENTE_DESEMBOLSOS_REALIZADOS, Const.LISTA_CORREO.REMITENTES_CORREO);
            string mDisplayName = "";

            ////---------------------------------------------------------------------------------------------
            ////Redactamos el Display name, crédito aprobado o rechazado.
            ////---------------------------------------------------------------------------------------------
            mDisplayName = "Muebles Fiesta - NOTIFICACIONES --";

            ////---------------------------------------------------------------------------------------------
            ////Redactamos el asunto del correo
            ////---------------------------------------------------------------------------------------------

            string mAsunto = string.Format("{0} - Aviso de desembolsos realizados ", opcion.Equals("VENDEDOR") ? "VENDEDOR: " + lDesembolso[0].Vendedor : "TIENDA: " + lDesembolso[0].Cobrador);

            ////---------------------------------------------------------------------------------------------
            ////Agregamos a los destinatario
            ////---------------------------------------------------------------------------------------------
            List<string> mCuentaCorreo = new List<string>();

            ////---------------------------------------------------------------------------------------------
            ////Variable para el cuerpo del correo
            ////---------------------------------------------------------------------------------------------
            StringBuilder mMensaje = new StringBuilder();

            ///Para dar formato a los montos, con simbolo de moneda
            CultureInfo cultureToUse = new CultureInfo("es-GT");
            cultureToUse.NumberFormat.CurrencyDecimalDigits = 2;
            cultureToUse.NumberFormat.NumberDecimalDigits = 2;

            if (opcion.Equals("VENDEDOR"))
            {
                mCuentaCorreo.Add(lDesembolso[0].EmailVendedor);
                log.Debug("Notificación a (VENDEDOR): " + lDesembolso[0].Vendedor + " " + lDesembolso[0].EmailVendedor);
            }
            else
            {
                mCuentaCorreo.Add(lDesembolso[0].EmailJefeTienda);
                mCuentaCorreo.Add(lDesembolso[0].EmailSupervisor);
                log.Debug("Notificación a (TIENDA): JEFE: " + lDesembolso[0].EmailJefeTienda + ", SUPERVISOR:" + lDesembolso[0].EmailSupervisor);
            }
            if(financiera==7)
                mMensaje.Append(Utilitario.CorreoInicioFormatoRojo());
            else
                mMensaje.Append(Utilitario.CorreoInicioFormatoAzulYGris());

            mMensaje.Append(string.Format("<p>Estimados(as):<BR/> Les informamos sobre los siguientes desembolsos  por parte de "+(financiera==7?"INTERCONSUMO":"CREDIPLUS")+".</p>"));
            mMensaje.Append("<BR/>");
            mMensaje.Append("<table align='center' class='style4'>");
            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloCentro("Solicitud"));
            mMensaje.Append(Utilitario.FormatoTituloCentro("Vendedor"));
            mMensaje.Append(Utilitario.FormatoTituloCentro("Factura"));
            mMensaje.Append(Utilitario.FormatoTituloCentro("Cliente"));
            mMensaje.Append(Utilitario.FormatoTituloCentro("Total Factura"));
            mMensaje.Append(Utilitario.FormatoTituloCentro("Monto Enganche"));
            mMensaje.Append(Utilitario.FormatoTituloCentro("Monto Desembolso"));
            mMensaje.Append("</tr>");

            foreach (var item in lDesembolso)
            {
                mMensaje.Append("<tr>");
                decimal dEnganche = Convert.ToDecimal(item.EngancheMF.ToString().Equals("") ? "0" : item.EngancheMF.ToString());
                mMensaje.Append(Utilitario.FormatoCuerpoCentro(item.Solicitud));
                mMensaje.Append(Utilitario.FormatoCuerpoCentro(item.Vendedor));
                mMensaje.Append(Utilitario.FormatoCuerpoCentro(item.FacturaMF));
                mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.ClienteMF + " - " + item.NombreCliente));
                mMensaje.Append(Utilitario.FormatoCuerpoCentro(item.ValorMF.ToString("C", cultureToUse)));
                mMensaje.Append(Utilitario.FormatoCuerpoCentro(dEnganche.ToString("C", cultureToUse)));
                mMensaje.Append(Utilitario.FormatoCuerpoCentro(item.Valor.ToString("C", cultureToUse)));
                mMensaje.Append("</tr>");
            }
            mMensaje.Append("</table>");

            mMensaje.Append(string.Format("<p>Gracias por su atención,<br>"));
            mMensaje.Append(string.Format("Saludos cordiales.</p>"));
            mMensaje.Append(Utilitario.CorreoFin());

            //---------------------------------------------------------------------------------------------------------
            //Procedemos a enviar el correo.
            //---------------------------------------------------------------------------------------------------------

            Utilitario.EnviarEmail(mRemitente, mCuentaCorreo, mAsunto, mMensaje, mDisplayName);
            
        }

    }
}
