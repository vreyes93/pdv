﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MF_Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;

namespace FiestaNETRestServices.Controllers.Interconsumo
{
    public class DocumentoPdfInterconsumoController : MFApiController
    {

        //public byte[] GetDocumentoPdfInterconsumo(string Tipo, string NumeroReferencia)
        public HttpResponseMessage GetDocumentoPdfInterconsumo(string Tipo, string NumeroReferencia)
        {
            byte[] DocumentoPdf;
            try
            {
                string xmlData = "<solicitud>";
                xmlData += "<autenticacion>";
                        xmlData += "<aplicacion>Muebles Fiesta</aplicacion>";

                        xmlData += string.Format("<usuario><![CDATA[{0}]]></usuario>", WebConfigurationManager.AppSettings["UsrInterconsumo"]);
                        xmlData += string.Format("<clave><![CDATA[{0}]]></clave>", WebConfigurationManager.AppSettings["PwdInterconsumo"]);
                        xmlData += "</autenticacion>";
                        xmlData += string.Format("<referencia>{0}</referencia>", NumeroReferencia);
                xmlData += string.Format("<formulario>{0}</formulario>", Tipo);
                xmlData += "</solicitud>";

                log.Info("Consumiendo WS de documenots pdf de Interconsumo.");

                // - ---------------------------------------------------------------------------------
                // Definimos el protocolo de seguridad TLS 1.2
                // - ---------------------------------------------------------------------------------
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                // - ---------------------------------------------------------------------------------
                // Definimos la variable para el servicio
                // - ---------------------------------------------------------------------------------
                FiestaNETRestServices.srvIntegracionInterConsumo.srvIntegracionInteconsumoPuente srvIntegra = new FiestaNETRestServices.srvIntegracionInterConsumo.srvIntegracionInteconsumoPuente();

                // - ---------------------------------------------------------------------------------
                // Definimos la variable de encripcion.
                // - ---------------------------------------------------------------------------------
                InterEncript.Encriptacion dllEncriptacion = new InterEncript.Encriptacion();

                // - ---------------------------------------------------------------------------------
                // Realizamos la encripción del mensaje
                // - ---------------------------------------------------------------------------------
                string resultado = dllEncriptacion.Proceso(xmlData, "solicitud", "1", "xml");

                // - ---------------------------------------------------------------------------------
                // Consumimos el servicio
                // - ---------------------------------------------------------------------------------

                DocumentoPdf = srvIntegra.EntradaXmlFormulario(resultado);

               
                HttpResponseMessage result = null;

                result = Request.CreateResponse(HttpStatusCode.OK);
                if (DocumentoPdf != null && DocumentoPdf.Length > 0)
                {
                    result.Content = new ByteArrayContent(DocumentoPdf);
                    result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                    result.Content.Headers.ContentDisposition.FileName = NumeroReferencia + ".pdf";
                }
                return result;
                //return DocumentoPdf;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + CatchClass.ExMessage(ex, MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name) + ex.StackTrace);
                return null;
            }
        }

    }
}
