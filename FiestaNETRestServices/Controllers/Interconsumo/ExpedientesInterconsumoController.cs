﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Reflection;
using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Models;
using System.Transactions;
using MF_Clases;
using Newtonsoft.Json;


namespace FiestaNETRestServices.Content.Controllers.Interconsumo
{
    public class ExpedientesInterconsumoController : MFApiController
    {
        // GET api/expedienteinterconsumo
        public Clases.LoteFacturasFinanciera GetExpedientesInterconsumo(string Anio, string Mes, string Dia,int nFinanciera=-1)
        {
            DateTime mFechaInicio;
            DateTime mFechaFin;
            Clases.LoteFacturasFinanciera mLoteFacturasCredito = new Clases.LoteFacturasFinanciera();
            mLoteFacturasCredito.exito = false;

            try
            {


                if (!Utilitario.FechaValida(Anio, Mes, Dia))
                {
                    mLoteFacturasCredito.mensaje = "La fecha inicial es inválida.";
                    return mLoteFacturasCredito;
                }




                mFechaInicio = Utilitario.ConvertirFecha(Anio, Mes, Dia);
                mFechaFin = mFechaInicio.AddDays(1);

                //TODO: Pasar esto a la capa DALFacturaInterconsumo
                //  var qFacturasInterconsumo = GetFacturasInterconsumo(mFechaInicial,mFechaFinal,Tienda);
                var qExpedientes = from fds in dbExactus.MF_Factura_Documentos_Soporte
                                               join f in dbExactus.FACTURA
                                                   on fds.FACTURA equals f.FACTURA1
                                               join p in dbExactus.MF_Pedido
                                                   on f.PEDIDO equals  p.PEDIDO
                                                join ue in dbExactus.MF_Factura_Estado 
                                                    on fds.FACTURA equals ue.FACTURA
                                               where fds.RecordDate >= mFechaInicio
                                                       && fds.RecordDate <= mFechaFin
                                                       && (p.FINANCIERA==nFinanciera || (nFinanciera==-1))// filtrar por financiera
                                                       && ue.CreateDate ==(
                                                       dbExactus.MF_Factura_Estado.Where(
                                                            estado => estado.FACTURA == fds.FACTURA
                                                        ).Max(fecha => fecha.CreateDate)
                                                       )

                                                       
                                               orderby f.COBRADOR, f.FACTURA1
                                               select new { FACTURA = fds.FACTURA
                                                            , COBRADOR = f.COBRADOR
                                                            , CLIENTE = f.CLIENTE
                                                            , NOMBRE_CLIENTE = f.NOMBRE_CLIENTE
                                                            , FECHA_FACTURA = f.FECHA
                                                            , VALOR = f.TOTAL_FACTURA
                                                            , IDENTIFICACION = fds.IDENTIFIACION
                                                            , OTRA_IDENTIFICACION = fds.OTRA_IDENTIFIACION
                                                            , CONSTANCIA_INGRESOS = fds.CONSTANCIA_INGRESOS
                                                            , DOCUMENTOS_SERVICIOS_PUBLICOS = fds.DOCUMENTO_SERVICIO_PUBLICO
                                                            , ESTADOS_CUENTA = fds.ESTADOS_CUENTA
                                                            , PAGARE = fds.PAGARE
                                                            , OBSERVACIONES = fds.OBSERVACIONES
                                                            , SOLICITUD = p.SOLICITUD
                                                            , VENDEDOR = f.VENDEDOR
                                                            , ESTADO_ACTUAL = ue.ESTADO
                                                            , FINANCIERA= p.FINANCIERA
                                                            ,NOMBRE_FINANCIERA= (from q in dbExactus.MF_Financiera where q.Financiera == p.FINANCIERA select q).FirstOrDefault().Nombre
                                                            };




                foreach (var item in qExpedientes)
                {
                    Clases.FacturasFinanciera mFacturaFinanciera = new Clases.FacturasFinanciera();
                    mFacturaFinanciera.FechaFactura = item.FECHA_FACTURA;
                    mFacturaFinanciera.NoFactura = item.FACTURA;
                    mFacturaFinanciera.Valor = item.VALOR;
                    mFacturaFinanciera.Cliente = item.CLIENTE;
                    mFacturaFinanciera.NombreCliente = item.NOMBRE_CLIENTE;
                    mFacturaFinanciera.Solicitud = item.SOLICITUD.ToString();
                    mFacturaFinanciera.Identificacion = ObtenerTipoDocumento(item.IDENTIFICACION);
                    mFacturaFinanciera.OtraIdentificacion= ObtenerTipoDocumento(item.OTRA_IDENTIFICACION);
                    mFacturaFinanciera.ConstanciaIngresos = item.CONSTANCIA_INGRESOS;
                    mFacturaFinanciera.DocumentoServiciosPublicos = ObtenerTipoDocumento(item.DOCUMENTOS_SERVICIOS_PUBLICOS);
                    mFacturaFinanciera.EstadosDeCuenta = item.ESTADOS_CUENTA;
                    mFacturaFinanciera.Pagare = item.PAGARE;
                    mFacturaFinanciera.Observaciones = item.OBSERVACIONES;
                    mFacturaFinanciera.Vendedor = item.VENDEDOR;
                    mFacturaFinanciera.EstadoActual = item.ESTADO_ACTUAL;
                    mFacturaFinanciera.Tienda = item.COBRADOR;
                    mFacturaFinanciera.NombreFinanciera =item.NOMBRE_FINANCIERA;
                    mLoteFacturasCredito.Facturas.Add(mFacturaFinanciera);

                }

                mLoteFacturasCredito.exito = true;
                mLoteFacturasCredito.mensaje = "Exito";
            }
            catch (Exception ex)
            {
                mLoteFacturasCredito.mensaje = CatchClass.ExMessage(ex, MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name);
            }
            return mLoteFacturasCredito;
        }
        
        public Respuesta PostExpedientesInterconsumo([FromBody]Clases.LoteFacturasFinanciera Expedientes)
        {
            Respuesta mRespuesta = new Respuesta();
            
             try
            {
                if (Expedientes.Operacion == Const.Expediente.INGRESADO_SISTEMA)
                {
                    mRespuesta = GrabarExpedientes(Expedientes, Const.Expediente.INGRESADO_SISTEMA);
                    if (!mRespuesta.Exito)
                        return mRespuesta;
                }

                if (Expedientes.Operacion == Const.Expediente.ENVIADO_FINANCIERA)
                {
                    mRespuesta = GrabarExpedientes(Expedientes, Const.Expediente.INGRESADO_SISTEMA);
                    if (!mRespuesta.Exito)
                        return mRespuesta;
                    mRespuesta = GrabarExpedientes(Expedientes, Const.Expediente.ENVIADO_FINANCIERA);
                    if (!mRespuesta.Exito)
                        return mRespuesta;
                }

                log.Info("Expedientes de grabados Resultado:"+mRespuesta.Exito);
                mRespuesta.Exito = true;
                mRespuesta.Mensaje = "Expedientes almacenados correctamente.";
            }
             catch (Exception ex)
             {
                 mRespuesta.Mensaje = CatchClass.ExMessage(ex, MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name);
                log.Error("Problema al guardar los envios de la financiera "+Expedientes.Financiera+" Error: "+ex.Message);
                Utils.Utilitarios.EnviarMensajeSlack("Problema al guardar los envios de la financiera " + Expedientes.Financiera + ", Error: " + ex.Message);
                
            }
             return mRespuesta;
        }

        public Respuesta DeleteExpedientesInterconsumo(string Factura)
        {
            Respuesta mRespuesta = new Respuesta();
            try
            {

                using (TransactionScope transactionScope = new TransactionScope())
                {

                   List<MF_Factura_Estado> mFacturaEstado = dbExactus.MF_Factura_Estado.Where(x => x.FACTURA == Factura).ToList();
                    foreach (MF_Factura_Estado item in mFacturaEstado) dbExactus.MF_Factura_Estado.DeleteObject(item);
                    
                   List<MF_Factura_Documentos_Soporte> mDocumentoSoporte = dbExactus.MF_Factura_Documentos_Soporte.Where(x => x.FACTURA == Factura).ToList();
                   foreach (MF_Factura_Documentos_Soporte item in mDocumentoSoporte) dbExactus.MF_Factura_Documentos_Soporte.DeleteObject(item);

                    dbExactus.SaveChanges();
                    transactionScope.Complete();
                }

                mRespuesta.Exito = true;
                mRespuesta.Mensaje = "Registro eliminado exitosamente.";
            }
            catch (Exception ex)
            {
                mRespuesta.Mensaje = CatchClass.ExMessage(ex, MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name);
            }


            return mRespuesta;
        
        }

        private Respuesta GrabarExpedientes(Clases.LoteFacturasFinanciera ExpedientesFinanciera, string Estado = "") 
        {
            Respuesta mRespuesta = new Respuesta();
            try
            {

                using (TransactionScope transactionScope = new TransactionScope())
                {

                    var list = ExpedientesFinanciera.Facturas;
                    foreach (var item in list)
                    {
                        if (!String.IsNullOrEmpty(item.NoFactura))
                        {
                            if (Estado == Const.Expediente.INGRESADO_SISTEMA)
                            {
                                MF_Factura_Documentos_Soporte iDocumentosSoporte = new MF_Factura_Documentos_Soporte
                                {
                                    FACTURA = item.NoFactura,
                                    IDENTIFIACION = ObtenerCodigoTipoDocumento(item.Identificacion),
                                    OTRA_IDENTIFIACION = ObtenerCodigoTipoDocumento(item.OtraIdentificacion),
                                    CONSTANCIA_INGRESOS = item.ConstanciaIngresos,
                                    DOCUMENTO_SERVICIO_PUBLICO = ObtenerCodigoTipoDocumento(item.DocumentoServiciosPublicos),
                                    ESTADOS_CUENTA = item.EstadosDeCuenta,
                                    PAGARE = item.Pagare,
                                    OBSERVACIONES = item.Observaciones,
                                    RecordDate = DateTime.Now,
                                    CreatedBy = string.Format("FA/{0}", ExpedientesFinanciera.Usuario),
                                    UpdatedBy = string.Format("FA/{0}", ExpedientesFinanciera.Usuario),
                                    CreateDate = DateTime.Now
                                };


                                var FacturaDocumentosSoporte = dbExactus.MF_Factura_Documentos_Soporte
                                                           .Where(c => c.FACTURA == item.NoFactura)
                                                           .SingleOrDefault();

                                if (FacturaDocumentosSoporte != null)
                                {
                                    iDocumentosSoporte.RecordDate = DateTime.Now;
                                    iDocumentosSoporte.CreateDate = FacturaDocumentosSoporte.CreateDate;
                                    iDocumentosSoporte.CreatedBy = FacturaDocumentosSoporte.CreatedBy;
                                    dbExactus.MF_Factura_Documentos_Soporte.ApplyCurrentValues(iDocumentosSoporte);
                                }
                                else
                                    dbExactus.MF_Factura_Documentos_Soporte.AddObject(iDocumentosSoporte);
                            }

                            MF_Factura_Estado iFacturaEstado = new MF_Factura_Estado
                            {
                                FACTURA = item.NoFactura,
                                ESTADO = Estado,
                                FECHA = DateTime.Now,
                                RecordDate = DateTime.Now,
                                CreatedBy = string.Format("FA/{0}", ExpedientesFinanciera.Usuario),
                                UpdatedBy = string.Format("FA/{0}", ExpedientesFinanciera.Usuario),
                                CreateDate = DateTime.Now
                            };

                            var FacturaEstado = dbExactus.MF_Factura_Estado
                            .Where(c => c.FACTURA == item.NoFactura
                                    && c.ESTADO == Estado)
                            .SingleOrDefault();


                            if (FacturaEstado != null)
                            {
                                iFacturaEstado.RecordDate = DateTime.Now;
                                iFacturaEstado.CreateDate = FacturaEstado.CreateDate;
                                iFacturaEstado.CreatedBy = FacturaEstado.CreatedBy;
                                dbExactus.MF_Factura_Estado.ApplyCurrentValues(iFacturaEstado);
                            }
                            else
                                dbExactus.MF_Factura_Estado.AddObject(iFacturaEstado);
                        }
                    }

                    dbExactus.SaveChanges();
                    transactionScope.Complete();
                }

                mRespuesta.Exito = true;
            }
            catch (Exception ex)
            {
                mRespuesta.Mensaje = CatchClass.ExMessage(ex, MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name);
            }
            return mRespuesta;
        }

        private string ObtenerCodigoTipoDocumento(string documento="")
        {
            string mTipoDocumento="";
            if (!String.IsNullOrEmpty(documento))
            {
                if (documento == "DPI") mTipoDocumento = "1";
                if (documento == "Pasaporte") mTipoDocumento = "2";
                if (documento == "IGSS") mTipoDocumento = "3";
                if (documento == "IRTRA") mTipoDocumento = "4";
                if (documento == "Licencia") mTipoDocumento = "5";
                if (documento == "Luz") mTipoDocumento = "6";
                if (documento == "Teléfono") mTipoDocumento = "7";
                if (documento == "Otro") mTipoDocumento = "11";

            }
            return mTipoDocumento;
        }

        private string ObtenerTipoDocumento(string documento = "")
        {
            string mTipoDocumento = "";
            if (!String.IsNullOrEmpty(documento))
            {
                if (documento == "1") mTipoDocumento = "DPI";
                if (documento == "2") mTipoDocumento = "Pasaporte";
                if (documento == "3") mTipoDocumento = "IGSS";
                if (documento == "4") mTipoDocumento = "IRTRA";
                if (documento == "5") mTipoDocumento = "Licencia";
                if (documento == "6") mTipoDocumento = "Luz";
                if (documento == "7") mTipoDocumento = "Teléfono";
                if (documento == "11") mTipoDocumento = "Otro";

            }
            return mTipoDocumento;
        }

    }
}
