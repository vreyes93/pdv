﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Data;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using System.Reflection;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;
using MF_Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Web.Http.Description;
using System.Data.SqlClient;
using FiestaNETRestServices.DAL;

namespace FiestaNETRestServices.Controllers.Interconsumo
{
    public class FacturasInterconsumoController : MFApiController
    {
        [HttpGet]
        public Clases.LoteFacturasFinanciera GetFacturasInterconsumo(string AnioInicio, string MesInicio, string DiaInicio, string AnioFinal, string MesFinal, string DiaFinal, string Tienda, int nFinanciera)
        {
            DALFacturaInterconsumo dALFacturaFinanciera = new DALFacturaInterconsumo();
            return dALFacturaFinanciera.getExpedientesPendientesEnvio(AnioInicio,MesInicio,DiaInicio,AnioFinal,MesFinal,DiaFinal,Tienda,nFinanciera);

        }

    }
}
