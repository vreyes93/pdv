﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using FiestaNETRestServices.Utils;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Xml;
using static MF_Clases.Clases;

namespace FiestaNETRestServices.Controllers.Interconsumo
{
    [RoutePrefix("api/HistoricoRechazo")]
    public class HistoricoRechazoController : MFApiController
    {


        /// <summary>
        /// Servicio para obtener el registro historico de los expedientes
        /// que fueron rechazados por interconsumo
        /// forma de llamarlo: http://localhost:53874/api/HistoricoRechazo/GetHistoricoRechazo?FechaI=06/01/2018&FechaF=07/01/2018
        /// </summary>
        /// <param name="FechaI">Fecha Del:</param>
        /// <param name="FechaF">Fecha Al: </param>
        /// <returns></returns>
        [Route("~/GetHistoricoRechazo?FechaI={FechaI}&FechaF={FechaF}")]
        public Respuesta GetHistoricoRechazoAsync(string FechaI, string FechaF)
        {
            Respuesta mRespuesta = new Respuesta();
            mRespuesta.Objeto = 0;
            DateTime dtFechaI = new DateTime();
            DateTime dtFechaF = new DateTime();
            try
            {
                dtFechaI = MF_Clases.Utilitarios.dateParseFormatDMY(FechaI);
                dtFechaF = MF_Clases.Utilitarios.dateParseFormatDMY(FechaF);
            }
            catch
            { }
            List<MF_Clases.infoInterconsumoMensajeRechazo> lstInfoEnvioATienda = new List<MF_Clases.infoInterconsumoMensajeRechazo>();

            
            bool blExitoGral = true;
            ///*
            //Consumiendo el web service de Interconsumo
            //*/
            try
            {
                //    //------------------------------------------------------------------------------------
                //    //Consultamos por medio del servicio de Interconsumo
                //    //------------------------------------------------------------------------------------
                log.Info("Consumiendo WS de consulta de Rechazos de Interconsumo.");
                DateTime dtFechaOp = dtFechaI;
                DALSolicitudPendienteInterconsumo mSolicitudesPendientes = new DALSolicitudPendienteInterconsumo();
                XmlDocument xmlRespuesta=new XmlDocument();
                UtilitariosInterconsumo mInterconsumo = new UtilitariosInterconsumo();
                if (dtFechaI <= dtFechaF)
                {
                    while (dtFechaOp <= dtFechaF)
                    {
                        Task ObtenerINTERCONSUMOinfo = new Task(() => xmlRespuesta = mInterconsumo.Servicio(Const.SERVICIOS_INTERCONSUMO.CONSULTAR_SOLICITUDES_RECHAZADAS, mInterconsumo.mensajeEntradaXmlConsultaRechazos(dtFechaOp)));
                        ObtenerINTERCONSUMOinfo.Start();
                        ObtenerINTERCONSUMOinfo.Wait();
                        if (ObtenerINTERCONSUMOinfo.IsCompleted)
                        {
                            //    //------------------------------------------------------------------------------------
                            //    //Mapeamos el resutado del xml a la clase que tenemos
                            //    //------------------------------------------------------------------------------------

                            List<SolicitudInterconsumo> lstrechazos = UtilitariosInterconsumo.ParsearRechazosInterconsumo(xmlRespuesta);

                            //    ////------------------------------------------------------------------------------------
                            //    ////Se verifica que existan rechazos, de ser asi, se guarda/actualiza la información en 
                            //    ////la tabla MF_Factura_Estado 
                            //    ////----------------------------------------------------------------------------------
                            if (lstrechazos.Count > 0)
                            {
                                foreach (SolicitudInterconsumo rechazo in lstrechazos)
                                {
                                    try
                                    {
                                        //---------------------------------------------------------------------------------------------
                                        //Actualizamos las tablas correspondientes
                                        //---------------------------------------------------------------------------------------------

                                        bool blExito= mSolicitudesPendientes.GrabarRechazoSolicitud(rechazo, dtFechaOp).Exito;
                                        blExitoGral &= blExito;
                                        //if (!blExito)
                                        //{
                                        //    mRespuesta.Mensaje += "FECHA: "+Utilitario.FormatoDDMMYYYY(dtFechaOp)+"- Problema al grabar rechazo de factura: " + rechazo.NUMERO_DOCUMENTO + " - Cli: " + rechazo.CLIENTE + "- " + rechazo.PRIMER_NOMBRE+" "+rechazo.SEGUNDO_NOMBRE+" "+rechazo.PRIMER_APELLIDO+" "+rechazo.SEGUNDO_APELLIDO+" "+rechazo.APELLIDO_CASADA + " \n";
                                        //}

                                    }
                                    catch (Exception e)
                                    {
                                        log.Error(this.GetType().Name + " " + e.StackTrace);
                                    }
                                }
                                mRespuesta.Exito = blExitoGral;

                            }
                            mRespuesta.Objeto = int.Parse(((int)mRespuesta.Objeto).ToString()) + lstrechazos.Count;
                            dtFechaOp = dtFechaOp.AddDays(1);
                        }
                        

                    }
                }
                else
                {
                    mRespuesta.Exito = false;
                    mRespuesta.Mensaje = "(FORMATO DE FECHA INVÁLIDO) La fecha inicial debe ser menor o igual a la fecha final.";
                }
                
            }
            catch (Exception e)
            {
                mRespuesta.Exito = false;
                mRespuesta.StackError = e.StackTrace;
                mRespuesta.Mensaje = e.Message;
                log.Error(this.GetType().Name + " " + e.StackTrace);
                //Utilitario.EnviarEmailIT(this.GetType().Name, e.StackTrace);
            }

            return mRespuesta;
            }



        
    }
}