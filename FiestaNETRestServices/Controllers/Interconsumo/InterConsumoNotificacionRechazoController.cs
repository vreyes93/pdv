﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using FiestaNETRestServices.Utils;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using static FiestaNETRestServices.Utils.Utilitarios;
using static MF_Clases.Clases;

namespace FiestaNETRestServices.Controllers.Interconsumo
{
    
    public class InterConsumoNotificacionRechazoController : MFApiController
    {
        /// <summary>
        /// Servicio automático para solicitar los rechazos del día anterior a interconsumo (financiera 7)
        /// forma de llamar al servicio para debug: api/InterConsumoNotificacionRechazo/
        /// </summary>
        /// <returns></returns>
        public Respuesta GetNotificacionRechazosInterConsumo()
        {
            var dtHoy =  DateTime.Now;
            var dtAyer = dtHoy.AddDays(-1);
            Respuesta mRespuesta = new Respuesta();
            List < MF_Clases.infoInterconsumoMensajeRechazo > lstInfoEnvioATienda=new List< MF_Clases.infoInterconsumoMensajeRechazo > ();
            UtilitariosInterconsumo mInterconsumo = new UtilitariosInterconsumo();
            DALSolicitudPendienteInterconsumo mSolicitudesPendientes = new DALSolicitudPendienteInterconsumo();
            bool blExitoGral = true;
            /*
            Consumiendo el web service de Interconsumo
            */
            try
            {
                //------------------------------------------------------------------------------------
                //Consultamos por medio del servicio de Interconsumo
                //------------------------------------------------------------------------------------
                log.Info("Consumiendo WS de consulta de Rechazos de Interconsumo.");

                XmlDocument xmlRespuesta = mInterconsumo.Servicio(Const.SERVICIOS_INTERCONSUMO.CONSULTAR_SOLICITUDES_RECHAZADAS, mInterconsumo.mensajeEntradaXmlConsultaRechazos());


                //------------------------------------------------------------------------------------
                //Validamos la respuesta de interconsumo
                //------------------------------------------------------------------------------------
                if (!xmlRespuesta.InnerText.Contains("ERROR"))
                    {
                    //------------------------------------------------------------------------------------
                    //Mapeamos el resutado del xml a la clase que tenemos
                    //------------------------------------------------------------------------------------
                    log.Info("Leyendo respuesta de Interconsumo.");
                    List<SolicitudInterconsumo> lstrechazos = UtilitariosInterconsumo.ParsearRechazosInterconsumo(xmlRespuesta);


                    ////------------------------------------------------------------------------------------
                    ////Se verifica que existan rechazos, de ser asi, se guarda/actualiza la información en 
                    ////la tabla MF_Factura_Estado y posteriormente se envia una notificación por email
                    ////-----------------------------------------------------------------------------------
                    if (lstrechazos.Count > 0)
                        {
                        foreach (SolicitudInterconsumo rechazo in lstrechazos)
                            {
                            try
                                {
                                //---------------------------------------------------------------------------------------------
                                //Actualizamos las tablas correspondientes
                                //---------------------------------------------------------------------------------------------
                                Respuesta mRespuestaGrabacion = new Respuesta();
                                mRespuestaGrabacion = mSolicitudesPendientes.GrabarRechazoSolicitud(rechazo, dtAyer);
                                blExitoGral &= mRespuestaGrabacion.Exito;
                                //---------------------------------------------------------------------------------------------
                                //Enviamos la notificación a los vendedores|interesados de la solicitud aprobada o rechazada por Interconsumo.
                                //---------------------------------------------------------------------------------------------
                                if (mRespuestaGrabacion.Exito)
                                    {
                                    var lstInfo = EnviarCorreoRechazosInterconsumoATienda(rechazo, dtAyer);
                                    if (lstInfo.Count > 0)
                                        lstInfoEnvioATienda.AddRange(lstInfo);
                                    }
                                else
                                    {
                                    log.Warn("Notificaciones rechazos interconsumo: No se encontró al cliente " + rechazo.CLIENTE + " - " + rechazo.PRIMER_NOMBRE + " " + rechazo.SEGUNDO_NOMBRE + " " + rechazo.PRIMER_APELLIDO + " " + rechazo.SEGUNDO_APELLIDO);
                                    }
                                }
                            catch (Exception e)
                                {
                                log.Error(this.GetType().Name + " " + e.StackTrace);
                                }
                            }
                        if (blExitoGral)
                            {
                            EnviarCorreoRechazosInterconsumoGeneral(lstrechazos, dtAyer, lstInfoEnvioATienda);

                            }

                        }

                    mRespuesta.Exito = true;
                    mRespuesta.Mensaje = lstrechazos.Count().ToString() + " -InterConsumo- solicitudes rechazadas procesadas.";
                    }
                else
                    {
                    //Enviamos una notificación a IT por medio de correo y al canal de SLACK para informar.
                    #region "Cuerpo de EMAIL a IT y mensaje por medio de SLACK"
                    ////---------------------------------------------------------------------------------------------------------
                    //// Variable necesarias para el envio de correo.
                    ////---------------------------------------------------------------------------------------------------------
                    string mRemitente = Utils.Utilitarios.GetRemitenteNotificacion(Const.REMITENTE_CORREO.REMITENTE_IT_PDV_ERRORES, Const.LISTA_CORREO.REMITENTES_CORREO);
                    string mDisplayName = "";


                        ////---------------------------------------------------------------------------------------------
                        ////Redactamos el Display name, crédito aprobado o rechazado.
                        ////---------------------------------------------------------------------------------------------
                        mDisplayName = "Muebles Fiesta - INTERCONSUMO- Notificacioines de ERRORES -";


                        ////---------------------------------------------------------------------------------------------
                        ////Redactamos el asunto del correo
                        ////---------------------------------------------------------------------------------------------
                        string mAsunto = string.Format("Problemas al consumir Servicio de INTERCONSUMO de fecha - " + Utilitario.FormatoDDMMYYYY(dtAyer) + " MUEBLES FIESTA");

                        ////---------------------------------------------------------------------------------------------
                        ////Agregamos a los destinatario
                        ////---------------------------------------------------------------------------------------------
                        List<string> mCuentaCorreo = new List<string>();

                        ////---------------------------------------------------------------------------------------------
                        ////Variable para el cuerpo del correo
                        ////---------------------------------------------------------------------------------------------
                        StringBuilder mMensaje = new StringBuilder();
					Utils.Utilitarios mUtilitario = new Utils.Utilitarios();

                        mUtilitario.GetListaCorreo(Const.LISTA_CORREO.IT_PDV_ERRORES, ref mCuentaCorreo);
                        mMensaje.Append(Utilitario.CorreoInicioFormatoRojo());
                        mMensaje.Append(string.Format("<p>Estimados, buen día, <BR/> <BR/>Les informamos la respuesta del servicio de INTERCONSUMO para las solicitudes rechazadas del día {0}, se les ruega darles el trámite que corresponde.</p>", Utilitario.FormatoDDMMYYYY(dtAyer)));

                        mMensaje.Append("<br/>");
                        mMensaje.Append("<p>");
                        mMensaje.Append(xmlRespuesta.InnerText);
                        mMensaje.Append("</p>");
                        mMensaje.Append("<BR/> Saludos cordiales.");
                        mMensaje.Append(Utilitario.CorreoFin());
                    ///Enviamos el correo
                        Utilitario.EnviarEmail(mRemitente, mCuentaCorreo, mAsunto, mMensaje, mDisplayName);
                    ///-----
                    ///MENSAJE CON SLACK
                    ///-----
                    Utils.Utilitarios.EnviarMensajeSlack("ERROR SERVICIO DE INTERCONSUMO: \nEl servicio de notificaciones de rechazo para los rechazos del día "+Utilitario.FormatoDDMMYYYY(dtAyer)+", devolvio el siguiente mensaje: "+xmlRespuesta.InnerText);
                    #endregion
                    }
            }
            catch (Exception e)
            {
                mRespuesta.Exito = false;
                mRespuesta.StackError = e.StackTrace;
                mRespuesta.Mensaje = e.Message;
                log.Error(this.GetType().Name + " " + e.StackTrace);
                //Utilitario.EnviarEmailIT(this.GetType().Name, e.StackTrace);
            }

            return mRespuesta;
        }

        /// <summary>
        /// Método para enviar el correo de notificación de rechazos de interconsumo dirigido a la Conta
        /// </summary>
        /// <param name="LstSolicitudRechazada">Lista de solicitudes rechazadas obtenido de Interconsumo</param>
        /// <param name="dtFechaRechazo">Fecha de rechazo de Interconsumo  </param>
        private void EnviarCorreoRechazosInterconsumoGeneral(List<SolicitudInterconsumo> LstSolicitudRechazada,DateTime dtFechaRechazo,List<MF_Clases.infoInterconsumoMensajeRechazo> lstInfoAdic)

        {
            DALSolicitudPendienteInterconsumo mSolicitudes = new DALSolicitudPendienteInterconsumo();
            ////---------------------------------------------------------------------------------------------------------
            //// Variable necesarias para el envio de correo.
            ////---------------------------------------------------------------------------------------------------------
            string mRemitente = Utils.Utilitarios.GetRemitenteNotificacion(Const.REMITENTE_CORREO.REMITENTE_SOLICITUDES_RECHAZADAS, Const.LISTA_CORREO.REMITENTES_CORREO);
            string mDisplayName = "";
            string mIconoEnvioATienda = "";

            ////---------------------------------------------------------------------------------------------
            ////Redactamos el Display name, crédito aprobado o rechazado.
            ////---------------------------------------------------------------------------------------------
            mDisplayName = "Muebles Fiesta - INTERCONSUMO- Solicitudes Rechazadas -";


            ////---------------------------------------------------------------------------------------------
            ////Redactamos el asunto del correo
            ////---------------------------------------------------------------------------------------------
            string mAsunto = string.Format("Rechazos InterConsumo - "+Utilitario.FormatoDDMMYYYY(dtFechaRechazo) +" MUEBLES FIESTA", Const.ESTADO_SOLICITUD_INTERCONSUMO.RECHAZADO);

            ////---------------------------------------------------------------------------------------------
            ////Agregamos a los destinatario
            ////---------------------------------------------------------------------------------------------
            List<string> mCuentaCorreo = new List<string>();

            ////---------------------------------------------------------------------------------------------
            ////Variable para el cuerpo del correo
            ////---------------------------------------------------------------------------------------------
            StringBuilder mMensaje = new StringBuilder();
			Utils.Utilitarios mUtilitario = new Utils.Utilitarios();

            mUtilitario.GetListaCorreo(Const.LISTA_CORREO.INTERCONSUMO_SOLICITUDES_RECHAZADAS, ref mCuentaCorreo);

            
            
            mMensaje.Append(Utilitario.CorreoInicioFormatoRojo());

            mMensaje.Append(string.Format("<p>Estimados, buen día, <BR/> <BR/>Les informamos que el lote siguiente de solicitudes han sido rechazadas por INTERCONSUMO el día {0}, se les ruega darles el trámite que corresponde.</p>", Utilitario.FormatoDDMMYYYY(dtFechaRechazo)));

            mMensaje.Append("<br/>");
            mMensaje.Append("<table align='center' class='style4'>");
            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloCentro("Solicitud"));
            mMensaje.Append(Utilitario.FormatoTituloCentro("Fecha <br/> Solicitud"));
            mMensaje.Append(Utilitario.FormatoTituloCentro("Tienda"));
            mMensaje.Append(Utilitario.FormatoTituloCentro("Cliente"));
            mMensaje.Append(Utilitario.FormatoTituloCentro("Factura"));
           
            mMensaje.Append(Utilitario.FormatoTituloCentro("Motivo Rechazo"));
            mMensaje.Append(Utilitario.FormatoTituloCentro("Notificado a tienda"));
            mMensaje.Append("</tr>");
          
           ///Buscamos la información extra que haga match con la información que ya tenemos.
            foreach (SolicitudInterconsumo cSol in LstSolicitudRechazada)
            {
                try
                {
                    var blEncontrado1 = false;
                    List<MF_Clases.infoInterconsumoMensajeRechazo> InfoExtraRechazoSolicitud = new List<MF_Clases.infoInterconsumoMensajeRechazo>();
                    try
                    {
                        //obtenemos la información del venedor y la tienda para enviar la notificación del rechazo.
                        InfoExtraRechazoSolicitud = ((List<MF_Clases.infoInterconsumoMensajeRechazo>)mSolicitudes.GetInfoRechazoSolicitud(cSol).Objeto);
                    }
                    catch (Exception et)
                    {
                        log.Error(this.GetType().Name + " " + et.StackTrace);
                    }
                    mMensaje.Append(Utilitario.FormatoCuerpoCentro(cSol.SOLICITUD.ToString()));

                    //Adición de la fecha de solicitud del rechazo
                    if (InfoExtraRechazoSolicitud.Count > 0)
                        mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(Utilitario.FormatoDDMMYYYY(InfoExtraRechazoSolicitud[0].FechaSolicitud)));
                    else
                        mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(""));

                    //Adición de la tienda
                    if (InfoExtraRechazoSolicitud.Count > 0)
                        mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(InfoExtraRechazoSolicitud[0].Tienda));
                    else
                        mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(""));

                    //Nombre completo del cliente proveniente de las tablas de MF
                    if (InfoExtraRechazoSolicitud.Count > 0)
                        mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(InfoExtraRechazoSolicitud[0].ClienteMF + " - " + InfoExtraRechazoSolicitud[0].nombreCliente));
                    else
                        mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(cSol.PRIMER_NOMBRE + " " + cSol.SEGUNDO_APELLIDO + " " + cSol.TERCER_NOMBRE + " " + cSol.PRIMER_APELLIDO + " " + cSol.SEGUNDO_APELLIDO));

                    //NUMERO DE FACTURA
                    if (InfoExtraRechazoSolicitud.Count > 0)
                    {
                        mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(InfoExtraRechazoSolicitud[0].Factura));
                        mIconoEnvioATienda = "<img  src=cid:EmailGreen alt=\"Notificación enviada a vendedor\">";
                    }
                    else
                    {
                        #region "Otro medio para mostrar la factura correcta"
                        //Buscamos el número de factura de la información extra obtenida anteriormente, ya que INTERCONSUMO no siempre manda el número de factura correcto
                        foreach (var item in lstInfoAdic)
                        {
                            var strSerie = string.Empty;
                            var strFact = string.Empty;
                            if (cSol.NUMERO_DOCUMENTO.Contains("-"))
                            {
                                strSerie = cSol.NUMERO_DOCUMENTO.Substring(0, cSol.NUMERO_DOCUMENTO.IndexOf("-"));
                                strFact = cSol.NUMERO_DOCUMENTO.Substring(cSol.NUMERO_DOCUMENTO.IndexOf("-") + 1, (cSol.NUMERO_DOCUMENTO.Length - cSol.NUMERO_DOCUMENTO.IndexOf("-")) - 1);

                                if ((item.Factura.StartsWith(strSerie) && item.Factura.EndsWith(strFact)) || (item.solicitud == cSol.SOLICITUD))
                                    if (!blEncontrado1)
                                    {
                                        foreach (var itemX in LstSolicitudRechazada)
                                            if (((item.Factura == cSol.NUMERO_DOCUMENTO) || (item.solicitud == cSol.SOLICITUD)) && (!blEncontrado1))
                                            {
                                                mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.Factura.ToString()));
                                                mIconoEnvioATienda = "<img  src=cid:EmailGreen alt=\"Notificación enviada a vendedor\">";
                                                blEncontrado1 = true;
                                            }
                                    }
                            }
                        }
                        if (!blEncontrado1)
                        {
                           mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(cSol.NUMERO_DOCUMENTO));
                            mIconoEnvioATienda = "<img src=cid:EmailRed alt=\"Notificación no enviada a vendedor\">";
                        }
                        #endregion  
                    }



                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(cSol.DESCRIPCION_ERROR));
                    mMensaje.Append(Utilitario.FormatoCuerpoCentro(mIconoEnvioATienda.Length>0?mIconoEnvioATienda: "<img src=cid:EmailRed alt=\"Notificación no enviada a vendedor\">"));
                    mMensaje.Append("</tr>");
                   
                }
                catch (Exception im)
                {
                    log.Error(this.GetType().Name + " " + im.StackTrace);
                }
            }

            mMensaje.Append("</table>");
           

            mMensaje.Append(string.Format("<p>Gracias por su atención,</p>"));
            mMensaje.Append(string.Format("<p padding-left:15em>Saludos cordiales.</p>"));
            mMensaje.Append(Utilitario.CorreoFin());

			////---------------------------------------------------------------------------------------------------------
			////Procedemos a enviar el correo.
			////---------------------------------------------------------------------------------------------------------
			List<ImagenAdjunta> lstImagenes = new List<ImagenAdjunta>();
            lstImagenes.Add(new MF_Clases.ImagenAdjunta(System.Web.Hosting.HostingEnvironment.MapPath("~//Images/email_green.png"), MediaTypeNames.Image.Jpeg,"EmailGreen"));
            lstImagenes.Add(new MF_Clases.ImagenAdjunta(System.Web.Hosting.HostingEnvironment.MapPath("~//Images/email_red.png"), MediaTypeNames.Image.Jpeg, "EmailRed"));
            Utilitario.EnviarEmail(mRemitente, mCuentaCorreo, mAsunto, mMensaje, mDisplayName, lstImagenes);
          
        }

        private List<MF_Clases.infoInterconsumoMensajeRechazo>  EnviarCorreoRechazosInterconsumoATienda(SolicitudInterconsumo cSol, DateTime dtFechaRechazo)
        {
            DALSolicitudPendienteInterconsumo mSolicitudes = new DALSolicitudPendienteInterconsumo();
            ////---------------------------------------------------------------------------------------------------------
            //// Variable necesarias para el envio de correo.
            ////---------------------------------------------------------------------------------------------------------
            string mRemitente = Utils.Utilitarios.GetRemitenteNotificacion(Const.REMITENTE_CORREO.REMITENTE_SOLICITUDES_RECHAZADAS, Const.LISTA_CORREO.REMITENTES_CORREO);
            string mDisplayName = "";


            ////---------------------------------------------------------------------------------------------
            ////Redactamos el Display name, crédito aprobado o rechazado.
            ////---------------------------------------------------------------------------------------------
            mDisplayName = "Muebles Fiesta - INTERCONSUMO - Solicitud Pendiente";


            ////---------------------------------------------------------------------------------------------
            ////Redactamos el asunto del correo
            ////---------------------------------------------------------------------------------------------
            string mAsunto = string.Format("Rechazos InterConsumo - " + Utilitario.FormatoDDMMYYYY(dtFechaRechazo) + " MUEBLES FIESTA", Const.ESTADO_SOLICITUD_INTERCONSUMO.RECHAZADO);

            ////---------------------------------------------------------------------------------------------
            ////Agregamos a los destinatario
            ////---------------------------------------------------------------------------------------------
            List<string> mCuentaCorreo = new List<string>();

            ////---------------------------------------------------------------------------------------------
            ////Variable para el cuerpo del correo
            ////---------------------------------------------------------------------------------------------
            StringBuilder mMensaje = new StringBuilder();
			Utils.Utilitarios mUtilitario = new Utils.Utilitarios();
            List<MF_Clases.infoInterconsumoMensajeRechazo> InfoExtraRechazoSolicitud = new List<MF_Clases.infoInterconsumoMensajeRechazo>();
            try
            {
                try
                {
                    InfoExtraRechazoSolicitud = ((List<MF_Clases.infoInterconsumoMensajeRechazo>)mSolicitudes.GetInfoRechazoSolicitud(cSol).Objeto);
                     foreach (var Destinatatio in InfoExtraRechazoSolicitud)
                    {
                        if (!mCuentaCorreo.Contains(Destinatatio.EmailJefeTienda))
                            mCuentaCorreo.Add(Destinatatio.EmailJefeTienda);
                        if (!mCuentaCorreo.Contains(Destinatatio.EmailVendedor))
                            mCuentaCorreo.Add(Destinatatio.EmailVendedor);
                        if (!mCuentaCorreo.Contains(Destinatatio.EmailSupervisor))
                            mCuentaCorreo.Add(Destinatatio.EmailSupervisor);
                    }
                }
                catch (Exception i)
                {
                    log.Error(this.GetType().Name + " " + i.StackTrace);
                }
            mMensaje.Append(Utilitario.CorreoInicioFormatoRojo());

                mMensaje.Append(string.Format("<p>Estimados, buen día, <BR/> <BR/>Les informamos que la siguiente solicitud ha sido rechazada por INTERCONSUMO el día {0}, se les ruega darle el trámite que corresponde.</p>", Utilitario.FormatoDDMMYYYY(dtFechaRechazo)));


                mMensaje.Append("<table align='center' class='style4'>");
                mMensaje.Append("<tr>");
                mMensaje.Append(Utilitario.FormatoTituloCentro("Solicitud"));
                mMensaje.Append(Utilitario.FormatoTituloCentro("Cliente"));
                mMensaje.Append(Utilitario.FormatoTituloCentro("Factura"));
                
                mMensaje.Append(Utilitario.FormatoTituloCentro("Motivo Rechazo"));
                mMensaje.Append("</tr>");


                mMensaje.Append(Utilitario.FormatoCuerpoCentro(cSol.SOLICITUD.ToString()));
                if(InfoExtraRechazoSolicitud.Count>0)
                mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(InfoExtraRechazoSolicitud[0].ClienteMF+" - "+InfoExtraRechazoSolicitud[0].nombreCliente));
                else
                mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(cSol.NOMBRE_CLIENTE));

                if (InfoExtraRechazoSolicitud.Count > 0)
                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(InfoExtraRechazoSolicitud[0].Factura));
                else
                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(cSol.NUMERO_DOCUMENTO.ToString()));

                mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(cSol.DESCRIPCION_ERROR));
                mMensaje.Append("</tr>");


                mMensaje.Append("</table>");


                mMensaje.Append(string.Format("<BR/><p>Gracias por su atención,<br>"));
                mMensaje.Append(string.Format("Saludos cordiales.</p>"));
                mMensaje.Append(Utilitario.CorreoFin());

                ////---------------------------------------------------------------------------------------------------------
                ////Procedemos a enviar el correo.
                ////---------------------------------------------------------------------------------------------------------
                Utilitario.EnviarEmail(mRemitente, mCuentaCorreo, mAsunto, mMensaje, mDisplayName);
            } catch (Exception e)
            {
                log.Error(this.GetType().Name + " " + e.StackTrace);
            }
            return InfoExtraRechazoSolicitud;
        }

        
       
    }
}
