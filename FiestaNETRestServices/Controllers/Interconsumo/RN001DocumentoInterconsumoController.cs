﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MF_Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;

namespace FiestaNETRestServices.Controllers.Interconsumo
{
    public class RN001DocumentoInterconsumoController : MFApiController
    {

        public Respuesta GetRN001DocumentoInterconsumo(String NumeroDocumento)
        { 
            string mTipoDocumento = Const.TIPO_DOCUMENTO.COTIZACION;
            Int32 mNumeroCotizacion = 0;

            Respuesta mRespuesta = new Respuesta();
            DALCotizacionInterconsumo DALCotizacionInterconsumo = new DALCotizacionInterconsumo();
            DALPedidoInterconsumo DALPedidoInterconsumo = new DALPedidoInterconsumo();

            try
            {
                //--------------------------------------------------------------------------
                //Validamos que el numero del parametro tenga información.
                //--------------------------------------------------------------------------
                log.Debug("Valida parametro y tipo de documento.");
                if (String.IsNullOrEmpty(NumeroDocumento))
                {
                    return new Respuesta(false, "El numero de documento no es válido.");
                }

                //--------------------------------------------------------------------------
                //Obtenemos el tipo de documento en base al numero del mismo.
                //--------------------------------------------------------------------------
                if (NumeroDocumento.Substring(0, 1) == "P") mTipoDocumento = Const.TIPO_DOCUMENTO.PEDIDO;

                //--------------------------------------------------------------------------
                //Si el documento es una cotización lo convertimos a un entero, 
                //ya que es lo que esta definido en el modelo.
                //--------------------------------------------------------------------------
                if (mTipoDocumento == Const.TIPO_DOCUMENTO.COTIZACION) mNumeroCotizacion = Convert.ToInt32(NumeroDocumento);

                //--------------------------------------------------------------------------
                //Si el documento es cotizacion, procedemos a realizar las validaciones correspondientes.
                //--------------------------------------------------------------------------
                if (mTipoDocumento == Const.TIPO_DOCUMENTO.COTIZACION)
                {
                    log.Debug("Valida Cotizacion.");
                    //--------------------------------------------------------------------------
                    //Se realizan las validaciones correspondientes con la cotizacion e Interconsumo.
                    //--------------------------------------------------------------------------
                    mRespuesta = DALCotizacionInterconsumo.ValidaCotizacionInterconsumo(mNumeroCotizacion);

                    if (!mRespuesta.Exito)
                        return mRespuesta;

                    DALCotizacionInterconsumo.GrabarFechaImpresionPagare(mNumeroCotizacion);

                }
                //--------------------------------------------------------------------------
                //Si el documento es un pedido, procedemos a realizar las validaciones correspondientes.
                //--------------------------------------------------------------------------
                else
                {
                    log.Debug("Valida Pedido.");
                    //--------------------------------------------------------------------------
                    //Se realizan las validaciones correspondientes con el pedido e interconsumo.
                    //--------------------------------------------------------------------------
                    mRespuesta = DALPedidoInterconsumo.ValidaPedidoInterconsumo(NumeroDocumento);
                    if (!mRespuesta.Exito)
                        return mRespuesta;

                    DALPedidoInterconsumo.GrabarFechaImpresionPagare(NumeroDocumento);
                }

                return mRespuesta;
            }
            catch (Exception e)
            {
                log.Error("Error: "+e.StackTrace);
                return new Respuesta(false, e.StackTrace);
            }
        }
    }
}
