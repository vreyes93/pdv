﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Reflection;
using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Models;
using System.Transactions;
using MF_Clases;
using Newtonsoft.Json;


//using System.Net.Mail;
using System.Text;
//using System.Xml;

//using System.Transactions;
using FiestaNETRestServices.DAL;
using System.Data.Objects;
using System.Web;

namespace FiestaNETRestServices.Content.Controllers.Interconsumo
{
    public class SaldosDesembolsoInterconsumoController : MFApiController
    {
        // GET api/<controller>
        public Respuesta PostSaldosDesembolsoInterconsumo([FromBody]List<string> lstParametros)
        {
            Respuesta mRespuesta = new Respuesta();
            //Para obtener varios parametros desde el POST
            string TipoReporte = lstParametros[0].ToString();
            int nFinanciera=int.Parse(lstParametros[1].ToString());
            string NombreFinanciera = HttpUtility.UrlDecode(lstParametros[2].ToString());

            Clases.SaldosDesembolso mSaldosDesembolso = new Clases.SaldosDesembolso();
            decimal mMontoFinanciado = 0;
            StringBuilder Mensaje = new StringBuilder();
            List<string> CuentaCorreo = new List<string>();
            string Remitente = Utils.Utilitarios.GetRemitenteNotificacion(Const.REMITENTE_CORREO.REMITENTE_DESEMBOLSOS_REALIZADOS, Const.LISTA_CORREO.REMITENTES_CORREO);


            DateTime mFecha = new DateTime(2017, 4, 1);
            DateTime mFechaFacturaInicial = new DateTime(2017, 3, 1);
            DateTime mFechaQuincenal = DateTime.Now.Date.AddDays(-10);

            try
            {


                if (TipoReporte == "RESUMEN_SALDOS")
                {
                    var qEstados = new string[] { "E" };

                    Mensaje.Append("<!DOCTYPE html><html lang='en-us'>");
                    Mensaje.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } .style1 { color: #8C4510; background-color: #FFF7E7; font-size: xx-small; text-align: left; height:35px; } .style2 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; height:35px; } .style3 { color: #8C4510; background-color: #FFF7E7; font-size: x-small; text-align: center; height:35px; } .style4 { color: #8C4510; background-color: #FFF7E7; font-size: small; text-align: left; height:35px; } .style5 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; text-align: center; height:35px; } .style6 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; text-align: right; height:35px; }  .style7 { color: #8C4510; background-color: #FFF7E7; font-size: x-small; text-align: right; height:35px; }</style></head>");
                    Mensaje.Append("<body>");

                    var qSaldoDesembolso =
                               from f in dbExactus.FACTURA.Where(z => z.ANULADA == "N")
                               from p in dbExactus.PEDIDO.Where(a => f.PEDIDO == a.PEDIDO1)
                               from mf in dbExactus.MF_Factura.Where(b => f.FACTURA1 == b.FACTURA)
                               from mp in dbExactus.MF_Pedido.Where(c => f.PEDIDO == c.PEDIDO && c.FINANCIERA == nFinanciera) //parametrización de la financiera
                               from mfe in dbExactus.MF_Factura_Estado.Where(
                                                            fes => fes.FACTURA == f.FACTURA1
                                                            && fes.ESTADO== Const.Expediente.ENVIADO_FINANCIERA
                                                        ) 

                               //from mfe in dbExactus.MF_Factura_Estado.Where(enviado =>
                               //                                        f.FACTURA1 == enviado.FACTURA
                               //                                        && enviado.ESTADO == Const.Expediente.ENVIADO_FINANCIERA)//.DefaultIfEmpty()
                               where !dbExactus.MF_Factura_Estado.Any(desembolsado => desembolsado.FACTURA == f.FACTURA1
                                                           && (desembolsado.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                || desembolsado.ESTADO == Const.Expediente.ANULADO )
                                                             )
                                                             &&
                                dbExactus.MF_Factura_Estado.Where(
                                                            fes => fes.FACTURA == f.FACTURA1
                                                        ).OrderByDescending(x =>x.RecordDate).Take(1).FirstOrDefault().ESTADO == Const.Expediente.ENVIADO_FINANCIERA

                                                        
                                       
                               group new { f, p, mf, mp, mfe } by new
                               {
                                   FECHA = EntityFunctions.TruncateTime(mfe.FECHA == null ? new DateTime(1990, 1, 1) : mfe.FECHA)
                               } into groupMF
                               select new
                               {
                                   FECHA = groupMF.Key.FECHA
                                   ,
                                   SALDO_PENDIENTE = groupMF.Sum(x => x.mp.SALDO_FINANCIAR != null ? x.mp.SALDO_FINANCIAR : 0)
                               } into selection
                               orderby selection.FECHA descending
                               select selection;


                    if (qSaldoDesembolso.Count() > 0)
                    {
                        StringBuilder m = new StringBuilder();
                        m.Append("<p>&nbsp;</p>");
                        m.Append("<center>Resumen de saldos pendientes a desembolsar:</center>");
                        m.Append("<table align='center' class='style4'>");
                        m.Append("<tr><td class='style2'>&nbsp;Dia&nbsp;</td>");
                        m.Append("<td class='style6'>&nbsp;Saldo Pendiente&nbsp;</td></tr>");
                        mMontoFinanciado = 0;
                        decimal mSaldoMayor10Dias = 0;
                        int dias = 0;
                        foreach (var item in qSaldoDesembolso)
                        {

                            dias = (DateTime.Now - item.FECHA.Value).Days;

                            if (dias < 10)
                            {
                                m.Append(string.Format("<tr><td class='style1'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td>", dias));
                                m.Append(string.Format("<td class='style7'>&nbsp;{0}&nbsp;</td></tr>", String.Format("{0:0,0.00}", item.SALDO_PENDIENTE)));

                                mMontoFinanciado = mMontoFinanciado + item.SALDO_PENDIENTE.Value;
                            }
                            else
                            {
                                mSaldoMayor10Dias = mSaldoMayor10Dias + item.SALDO_PENDIENTE.Value;
                                mMontoFinanciado = mMontoFinanciado + item.SALDO_PENDIENTE.Value;
                            }
                        }
                        if (dias > 9)
                        {
                            m.Append(string.Format("<tr><td class='style3'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td>", "Mayor a 10"));
                            m.Append(string.Format("<td class='style7'>&nbsp;{0}&nbsp;</td></tr>", String.Format("{0:0,0.00}", mSaldoMayor10Dias)));
                        }
                        m.Append(string.Format("<tr><td class='style1'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td>", "Total"));
                        m.Append(string.Format("<td class='style7'>&nbsp;{0}&nbsp;</td></tr>", String.Format("{0:0,0.00}", mMontoFinanciado)));

                        m.Append("</table>");
                        Mensaje.Append(string.Format("<center>Total saldo pendiente a desembolsar: {0}</center>", String.Format("{0:0,0.00}", mMontoFinanciado)));
                        Mensaje.Append(m.ToString());

                    }



                    var qDesembolso =
                                        from f in dbExactus.FACTURA.Where(z => z.ANULADA == "N")
                                        from p in dbExactus.PEDIDO.Where(a => f.PEDIDO == a.PEDIDO1)
                                        from mf in dbExactus.MF_Factura.Where(b => f.FACTURA1 == b.FACTURA)
                                        from mp in dbExactus.MF_Pedido.Where(c => f.PEDIDO == c.PEDIDO && c.FINANCIERA == nFinanciera) //parametrización de la financiera
                                        from mfe in dbExactus.MF_Factura_Estado.Where(enviado =>
                                                                                f.FACTURA1 == enviado.FACTURA
                                                                                && enviado.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                                && enviado.FECHA > mFechaQuincenal)
                                        group new { f, p, mf, mp, mfe } by new
                                        {
                                            FECHA = EntityFunctions.TruncateTime(mfe.FECHA == null ? new DateTime(1990, 1, 1) : mfe.FECHA)
                                        } into groupMF
                                        select new
                                        {

                                            FECHA = groupMF.Key.FECHA
                                            ,
                                            MONTO_DESEMBOLSADO = groupMF.Sum(x => x.mp.SALDO_FINANCIAR != null ? x.mp.SALDO_FINANCIAR : 0)
                                        } into selection
                                        orderby selection.FECHA descending
                                        select selection;


                    if (qDesembolso.Count() > 0)
                    {
                        StringBuilder m = new StringBuilder();
                        m.Append("<p>&nbsp;</p>");
                        m.Append("<center>Resumen de Desembolsos:</center>");

                        m.Append("<table align='center' class='style4'>");
                        m.Append("<tr><td class='style2'>&nbsp;Dia&nbsp;</td>");
                        m.Append("<td class='style6'>&nbsp;Monto desembolsado&nbsp;</td></tr>");
                        mMontoFinanciado = 0;
                        decimal mSaldoMayor10Dias = 0;
                        int dias = 0;
                        foreach (var item in qDesembolso)
                        {

                            dias = (DateTime.Now - item.FECHA.Value).Days;

                            if (dias < 10)
                            {
                                m.Append(string.Format("<tr><td class='style1'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td>", dias));
                                m.Append(string.Format("<td class='style7'>&nbsp;{0}&nbsp;</td></tr>", String.Format("{0:0,0.00}", item.MONTO_DESEMBOLSADO)));

                                mMontoFinanciado = mMontoFinanciado + item.MONTO_DESEMBOLSADO.Value;
                            }
                            else
                            {
                                mSaldoMayor10Dias = mSaldoMayor10Dias + item.MONTO_DESEMBOLSADO.Value;
                                mMontoFinanciado = mMontoFinanciado + item.MONTO_DESEMBOLSADO.Value;
                            }
                        }
                        if (dias > 9)
                        {
                            m.Append(string.Format("<tr><td class='style3'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td>", "Mayor a 10"));
                            m.Append(string.Format("<td class='style7'>&nbsp;{0}&nbsp;</td></tr>", String.Format("{0:0,0.00}", mSaldoMayor10Dias)));
                        }
                        m.Append(string.Format("<tr><td class='style1'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td>", "Total"));
                        m.Append(string.Format("<td class='style7'>&nbsp;{0}&nbsp;</td></tr>", String.Format("{0:0,0.00}", mMontoFinanciado)));

                        m.Append("</table>");
                        Mensaje.Append(m.ToString());
                    }



                    var qListaCorreo =
                                        from mf_c in dbExactus.MF_Catalogo
                                        where mf_c.CODIGO_TABLA == 5
                                        select mf_c;

                    if (qListaCorreo.Count() > 0)
                    {
                        foreach (var item in qListaCorreo)
                        {
                            CuentaCorreo.Add(item.NOMBRE_CAT);
                        }
                    }
                    else
                    {
                        CuentaCorreo.Add("william.quiacain@mueblesfiesta.com");
                    }


                    Mensaje.Append("</body>");
                    Mensaje.Append("</html>");

                    Utilitario.EnviarEmail(Remitente, CuentaCorreo, string.Format("Resumen de desembolsos al {0}", this.ObtenerFecha(DateTime.Now)), Mensaje, "Desembolsos - "+NombreFinanciera.ToUpper());
                }


                if (TipoReporte == "DESEMBOLSOS_CARGADOS")
                {
                    var qEstados = new string[] { "E" };
                    mFechaFacturaInicial = DateTime.Now.Date;
                    Mensaje.Append("<!DOCTYPE html><html lang='en-us'>");
                    Mensaje.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } .style1 { color: #8C4510; background-color: #FFF7E7; font-size: xx-small; text-align: left; height:35px; } .style2 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; height:35px; } .style3 { color: #8C4510; background-color: #FFF7E7; font-size: x-small; text-align: center; height:35px; } .style4 { color: #8C4510; background-color: #FFF7E7; font-size: small; text-align: left; height:35px; } .style5 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; text-align: center; height:35px; } .style6 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; text-align: right; height:35px; }  .style7 { color: #8C4510; background-color: #FFF7E7; font-size: x-small; text-align: right; height:35px; }</style></head>");
                    Mensaje.Append("<body>");

                    var qSaldoDesembolso =
                               from f in dbExactus.FACTURA.Where(z => z.ANULADA == "N")
                               from p in dbExactus.PEDIDO.Where(a => f.PEDIDO == a.PEDIDO1)
                               from mf in dbExactus.MF_Factura.Where(b => f.FACTURA1 == b.FACTURA)
                               from mp in dbExactus.MF_Pedido.Where(c => f.PEDIDO == c.PEDIDO && c.FINANCIERA == nFinanciera) //parametrización de la financiera
                               from mr in dbExactus.MF_Recibo.Where(z =>
                                                                       f.FACTURA1 == z.FACTURA
                                                                       && z.DOCUMENTO.StartsWith("INTER")
                                                                       && z.ANULADO == "N").DefaultIfEmpty()
                               from mfe in dbExactus.MF_Factura_Estado.Where(y =>
                                                                       f.FACTURA1 == y.FACTURA
                                                                       && y.CreateDate >= mFechaFacturaInicial
                                                                       && y.ESTADO == Const.Expediente.DESEMBOLSADO)//.DefaultIfEmpty()
                               where !dbExactus.MF_Factura_Estado.Any(fe => fe.FACTURA == f.FACTURA1
                                                                        && fe.ESTADO == Const.Expediente.ANULADO
                                                                       )
                               select new
                               {
                                   FACTURA = mf.FACTURA
                                   ,
                                   FECHA_PEDIDO = p.FECHA_PEDIDO
                                   ,
                                   FECHA_FACTURA = f.FECHA
                                   ,
                                   FECHA_ENVIO_INTERCONSUMO = mfe.FECHA == null ? new DateTime(1990, 1, 1) : mfe.FECHA
                                   ,
                                   CLIENTE = f.CLIENTE
                                   ,
                                   NOMBRE_CLIENTE = f.NOMBRE_CLIENTE
                                   ,
                                   TOTAL_FACTURA = f != null ? f.TOTAL_FACTURA : 0
                                   ,
                                   ENGANCHE = mp.ENGANCHE != null ? mp.ENGANCHE : 0
                                   ,
                                   MONTO_FINANCIADO = mp.SALDO_FINANCIAR != null ? mp.SALDO_FINANCIAR : 0
                               } into selection
                               orderby selection.FECHA_ENVIO_INTERCONSUMO, selection.FACTURA
                               select selection
                                    ;


                    if (qSaldoDesembolso.Count() > 0)
                    {
                        StringBuilder m = new StringBuilder();
                        m.Append("<p>&nbsp;</p>");
                        m.Append(string.Format("<center>Detalle de los expedientes con desembolso cargados al sistema el dia {0}:</center>", this.ObtenerFecha(DateTime.Now)));

                        m.Append("<table align='center' class='style4'>");
                        m.Append("<tr><td class='style2'>&nbsp;Factura&nbsp;</td>");
                        m.Append("<td class='style5'>&nbsp;Fecha factura&nbsp;</td>");
                        m.Append("<td class='style5'>&nbsp;Fecha del desembolso&nbsp;</td>");
                        m.Append("<td class='style5'>&nbsp;Cliente&nbsp;</td>");
                        m.Append("<td class='style2'>&nbsp;Nombre del cliente&nbsp;</td>");
                        m.Append("<td class='style6'>&nbsp;Total factura&nbsp;</td>");
                        m.Append("<td class='style6'>&nbsp;Enganche&nbsp;</td>");
                        m.Append("<td class='style6'>&nbsp;Monto desembolsado&nbsp;</td></tr>");

                        mMontoFinanciado = 0;

                        foreach (var item in qSaldoDesembolso)
                        {

                            m.Append(string.Format("<tr><td class='style1'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td>", item.FACTURA));
                            m.Append(string.Format("<td class='style3'>&nbsp;{0}&nbsp;</td>", ObtenerFecha(item.FECHA_FACTURA)));
                            m.Append(string.Format("<td class='style3'>&nbsp;{0}&nbsp;</td>", ObtenerFecha(item.FECHA_ENVIO_INTERCONSUMO)));
                            m.Append(string.Format("<td class='style3'>&nbsp;{0}&nbsp;</td>", item.CLIENTE));
                            m.Append(string.Format("<td class='style1'>&nbsp;{0}&nbsp;</td>", item.NOMBRE_CLIENTE));
                            m.Append(string.Format("<td class='style7'>&nbsp;{0}&nbsp;</td>", String.Format("{0:0,0.00}", item.TOTAL_FACTURA)));
                            m.Append(string.Format("<td class='style7'>&nbsp;{0}&nbsp;</td>", String.Format("{0:0,0.00}", item.ENGANCHE.Value)));
                            m.Append(string.Format("<td class='style7'>&nbsp;{0}&nbsp;</td></tr>", String.Format("{0:0,0.00}", item.MONTO_FINANCIADO.Value)));

                            mMontoFinanciado = mMontoFinanciado + item.MONTO_FINANCIADO.Value;
                        }
                        m.Append("<tr><td class='style1'></td>");
                        m.Append("<td class='style3'></td>");
                        m.Append("<td class='style3'></td>");
                        m.Append("<td class='style3'></td>");
                        m.Append("<td class='style1'></td>");
                        m.Append("<td class='style7'></td>");
                        m.Append("<td class='style7'>Total:</td>");
                        m.Append(string.Format("<td class='style7'>&nbsp;{0}&nbsp;</td></tr>", String.Format("{0:0,0.00}", mMontoFinanciado)));
                        m.Append("</table>");
                        Mensaje.Append(m.ToString());
                    }


                    var qListaCorreo =
                                        from mf_c in dbExactus.MF_Catalogo
                                        where mf_c.CODIGO_TABLA == 3
                                        select mf_c;

                    if (qListaCorreo.Count() > 0)
                    {
                        foreach (var item in qListaCorreo)
                        {
                            CuentaCorreo.Add(item.NOMBRE_CAT);
                        }
                    }
                    else
                    {
                        CuentaCorreo.Add("william.quiacain@mueblesfiesta.com");
                    }


                    Mensaje.Append("</body>");
                    Mensaje.Append("</html>");

                    Utilitario.EnviarEmail(Remitente, CuentaCorreo, string.Format("Desembolsos cargados el {0}", this.ObtenerFecha(DateTime.Now)), Mensaje, "Desembolsos");


                }

                if (TipoReporte == "ENVIO_EXPEDIENTES")
                {
                    Remitente = Utils.Utilitarios.GetRemitenteNotificacion(Const.REMITENTE_CORREO.REMITENTE_DESEMBOLSOS_PENDIENTES, Const.LISTA_CORREO.REMITENTES_CORREO);
                    var qEstados = new string[] { "E" };
                    mFechaFacturaInicial = DateTime.Now.Date;

                    Mensaje.Append("<!DOCTYPE html><html lang='en-us'>");
                    Mensaje.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } .style1 { color: #8C4510; background-color: #FFF7E7; font-size: xx-small; text-align: left; height:35px; } .style2 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; height:35px; } .style3 { color: #8C4510; background-color: #FFF7E7; font-size: x-small; text-align: center; height:35px; } .style4 { color: #8C4510; background-color: #FFF7E7; font-size: small; text-align: left; height:35px; } .style5 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; text-align: center; height:35px; } .style6 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; text-align: right; height:35px; }  .style7 { color: #8C4510; background-color: #FFF7E7; font-size: x-small; text-align: right; height:35px; }</style></head>");
                    Mensaje.Append("<body>");

                    var qSaldoDesembolso =
                               from f in dbExactus.FACTURA.Where(z => z.ANULADA == "N")
                               from p in dbExactus.PEDIDO.Where(a => f.PEDIDO == a.PEDIDO1)
                               from mf in dbExactus.MF_Factura.Where(b => f.FACTURA1 == b.FACTURA)
                               from mp in dbExactus.MF_Pedido.Where(c => f.PEDIDO == c.PEDIDO && c.FINANCIERA == nFinanciera) //parametrización de la financiera
                               from mr in dbExactus.MF_Recibo.Where(z =>
                                                                       f.FACTURA1 == z.FACTURA
                                                                       && z.DOCUMENTO.StartsWith("INTER")
                                                                       && z.ANULADO == "N").DefaultIfEmpty()
                               from mfe in dbExactus.MF_Factura_Estado.Where(y =>
                                                                       f.FACTURA1 == y.FACTURA
                                                                       && y.FECHA >= mFechaFacturaInicial
                                                                       && y.ESTADO == Const.Expediente.ENVIADO_FINANCIERA)//.DefaultIfEmpty()
                               where !dbExactus.MF_Factura_Estado.Any(fe => fe.FACTURA == f.FACTURA1
                                                           && (fe.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                || fe.ESTADO == Const.Expediente.ANULADO )
                                                           )
                                && (
                                       dbExactus.MF_Factura_Estado.Where(
                                                            fes => fes.FACTURA == f.FACTURA1
                                                        ).OrderByDescending(F => F.RecordDate).FirstOrDefault().ESTADO != "R" //que su último estado no sea rechazado

                                                        )
                               select new
                               {
                                   FACTURA = mf.FACTURA
                                   ,
                                   FECHA_PEDIDO = p.FECHA_PEDIDO
                                   ,
                                   FECHA_FACTURA = f.FECHA
                                   ,
                                   FECHA_ENVIO_INTERCONSUMO = mfe.FECHA == null ? new DateTime(1990, 1, 1) : mfe.FECHA
                                   ,
                                   CLIENTE = f.CLIENTE
                                   ,
                                   NOMBRE_CLIENTE = f.NOMBRE_CLIENTE
                                   ,
                                   TOTAL_FACTURA = f != null ? f.TOTAL_FACTURA : 0
                                   ,
                                   ENGANCHE = mp.ENGANCHE != null ? mp.ENGANCHE : 0
                                   ,
                                   MONTO_FINANCIADO = mp.SALDO_FINANCIAR != null ? mp.SALDO_FINANCIAR : 0
                               } into selection
                               orderby selection.FECHA_ENVIO_INTERCONSUMO, selection.FACTURA
                               select selection
                                    ;


                    if (qSaldoDesembolso.Count() > 0)
                    {
                        StringBuilder m = new StringBuilder();
                        m.Append("<center><H2 style=\"color: " + (nFinanciera==7?"red":(nFinanciera==11?"blue":"black"))+"; \">"+ NombreFinanciera + "</H2>");
                        m.Append(string.Format("Detalle de los expedientes enviados el día {0}</center><BR/>", this.ObtenerFecha(DateTime.Now)));


                        m.Append("<table align='center' class='style4'>");
                        m.Append("<tr><td class='style2'>&nbsp;Factura&nbsp;</td>");
                        m.Append("<td class='style5'>&nbsp;Fecha factura&nbsp;</td>");
                        m.Append("<td class='style5'>&nbsp;Fecha envio <br/>a financiera</td>");
                        m.Append("<td class='style5'>&nbsp;Cliente&nbsp;</td>");
                        m.Append("<td class='style2'>&nbsp;Nombre del cliente&nbsp;</td>");
                        m.Append("<td class='style6'>&nbsp;Total factura&nbsp;</td>");
                        m.Append("<td class='style6'>&nbsp;Enganche&nbsp;</td>");
                        m.Append("<td class='style6'>&nbsp;Monto a financiar&nbsp;</td></tr>");

                        mMontoFinanciado = 0;

                        foreach (var item in qSaldoDesembolso)
                        {

                            m.Append(string.Format("<tr><td class='style1'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td>", item.FACTURA));
                            m.Append(string.Format("<td class='style3'>&nbsp;{0}&nbsp;</td>", ObtenerFecha(item.FECHA_FACTURA)));
                            m.Append(string.Format("<td class='style3'>&nbsp;{0}&nbsp;</td>", ObtenerFecha(item.FECHA_ENVIO_INTERCONSUMO)));
                            m.Append(string.Format("<td class='style3'>&nbsp;{0}&nbsp;</td>", item.CLIENTE));
                            m.Append(string.Format("<td class='style1'>&nbsp;{0}&nbsp;</td>", item.NOMBRE_CLIENTE));
                            m.Append(string.Format("<td class='style7'>&nbsp;{0}&nbsp;</td>", String.Format("{0:0,0.00}", item.TOTAL_FACTURA)));
                            m.Append(string.Format("<td class='style7'>&nbsp;{0}&nbsp;</td>", String.Format("{0:0,0.00}", item.ENGANCHE.Value)));
                            m.Append(string.Format("<td class='style7'>&nbsp;{0}&nbsp;</td></tr>", String.Format("{0:0,0.00}", item.MONTO_FINANCIADO.Value)));

                            mMontoFinanciado = mMontoFinanciado + item.MONTO_FINANCIADO.Value;

                        }

                        m.Append("<tr><td class='style1'></td>");
                        m.Append("<td class='style3'></td>");
                        m.Append("<td class='style3'></td>");
                        m.Append("<td class='style3'></td>");
                        m.Append("<td class='style1'></td>");
                        m.Append("<td class='style7'></td>");
                        m.Append("<td class='style7'>Total:</td>");
                        m.Append(string.Format("<td class='style7'>&nbsp;{0}&nbsp;</td></tr>", String.Format("{0:0,0.00}", mMontoFinanciado)));


                        m.Append("</table>");
                        Mensaje.Append(m.ToString());

                        var qListaCorreo =
                                            from mf_c in dbExactus.MF_Catalogo
                                            where mf_c.CODIGO_TABLA == 4
                                            select mf_c;

                        if (qListaCorreo.Count() > 0)
                        {
                            foreach (var item in qListaCorreo)
                            {
                                CuentaCorreo.Add(item.NOMBRE_CAT);
                            }
                        }
                        else
                        {
                            CuentaCorreo.Add("william.quiacain@mueblesfiesta.com");
                        }


                        Mensaje.Append("</body>");
                        Mensaje.Append("</html>");

                        Utilitario.EnviarEmail(Remitente, CuentaCorreo, string.Format("Expedientes enviados a "+ NombreFinanciera+" el {0}", this.ObtenerFecha(DateTime.Now)), Mensaje, NombreFinanciera.ToUpper());

                    }

                }


                if (TipoReporte == "EXPEDIENTES_PENDIENTES")
                {
                    Remitente = Utils.Utilitarios.GetRemitenteNotificacion(Const.REMITENTE_CORREO.REMITENTE_DESEMBOLSOS_PENDIENTES, Const.LISTA_CORREO.REMITENTES_CORREO);
                    var qEstados = new string[] { "E" };
                    mFechaFacturaInicial = DateTime.Now.Date;

                    Mensaje.Append("<!DOCTYPE html><html lang='en-us'>");
                    Mensaje.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } .style1 { color: #8C4510; background-color: #FFF7E7; font-size: xx-small; text-align: left; height:35px; } .style2 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; height:35px; } .style3 { color: #8C4510; background-color: #FFF7E7; font-size: x-small; text-align: center; height:35px; } .style4 { color: #8C4510; background-color: #FFF7E7; font-size: small; text-align: left; height:35px; } .style5 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; text-align: center; height:35px; } .style6 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; text-align: right; height:35px; }  .style7 { color: #8C4510; background-color: #FFF7E7; font-size: x-small; text-align: right; height:35px; }</style></head>");
                    Mensaje.Append("<body>");

                    Int16 mDiasRestar = -5;
                    if (DateTime.Now.Date.DayOfWeek == DayOfWeek.Friday)
                        mDiasRestar = -3;

                    DateTime mFechaDias = DateTime.Now.Date.AddDays(mDiasRestar);

                    var qExpedientesPend = from f in dbExactus.FACTURA
                                           join p in dbExactus.PEDIDO on f.PEDIDO equals p.PEDIDO1
                                           join mf in dbExactus.MF_Factura on f.FACTURA1 equals mf.FACTURA
                                           join mp in dbExactus.MF_Pedido on f.PEDIDO equals mp.PEDIDO 
                                           join dcc in dbExactus.DOCUMENTOS_CC //on f.CLIENTE equals dcc.CLIENTE
                                                                    on new { Key1 = f.CLIENTE, Key2 = f.FACTURA1 } equals new { Key1 = dcc.CLIENTE, Key2 = dcc.DOCUMENTO }
                                           join mfe in dbExactus.MF_Factura_Estado on f.FACTURA1 equals mfe.FACTURA
                                           where f.ANULADA == "N" && mfe.FECHA < mFechaDias && mfe.ESTADO == Const.Expediente.ENVIADO_FINANCIERA
                                               //&& dcc.DOCUMENTO == f.FACTURA1
                                               && mp.FINANCIERA == nFinanciera //parametrización de la financiera
                                           && !dbExactus.MF_Factura_Estado.Any(fe => fe.FACTURA == f.FACTURA1
                                                            && (fe.ESTADO == Const.Expediente.DESEMBOLSADO
                                                                || fe.ESTADO == Const.Expediente.ANULADO))
                                                  && (
                                                        dbExactus.MF_Factura_Estado.Where(
                                                            fes => fes.FACTURA == f.FACTURA1
                                                        ).OrderByDescending(F => F.RecordDate).FirstOrDefault().ESTADO != "R"  //que su último estado no sea rechazado

                                                        )

                                           orderby mfe.FECHA descending
                                           select new
                                           {
                                               FACTURA = mf.FACTURA,
                                               FECHA_PEDIDO = p.FECHA_PEDIDO,
                                               FECHA_FACTURA = f.FECHA,
                                               FECHA_ENVIO_INTERCONSUMO = mfe.FECHA == null ? new DateTime(1990, 1, 1) : mfe.FECHA,
                                               CLIENTE = f.CLIENTE,
                                               NOMBRE_CLIENTE = f.NOMBRE_CLIENTE,
                                               TOTAL_FACTURA = f != null ? f.TOTAL_FACTURA : 0,
                                               ENGANCHE = mp.ENGANCHE != null ? mp.ENGANCHE : 0,
                                               MONTO_FINANCIADO = mp.SALDO_FINANCIAR != null ? mp.SALDO_FINANCIAR : 0,
                                               SALDO = dcc.SALDO
                                           };

                    //var qExpedientesPendientes =
                    //            from f in dbExactus.FACTURA.Where(z => z.ANULADA == "N")
                    //            from p in dbExactus.PEDIDO.Where(a => f.PEDIDO == a.PEDIDO1)
                    //            from mf in dbExactus.MF_Factura.Where(b => f.FACTURA1 == b.FACTURA)
                    //            from mp in dbExactus.MF_Pedido.Where(c => f.PEDIDO == c.PEDIDO)
                    //            from mfe in dbExactus.MF_Factura_Estado.Where(y =>
                    //                                                    f.FACTURA1 == y.FACTURA
                    //                                                    && y.FECHA < mFechaDias
                    //                                                    && y.ESTADO == Const.ExpedienteInterconsumo.ENVIADO_INTERCONSUMO)//.DefaultIfEmpty()
                    //            where !dbExactus.MF_Factura_Estado.Any(fe => fe.FACTURA == f.FACTURA1
                    //                                        && (fe.ESTADO == Const.ExpedienteInterconsumo.DESEMBOLSADO
                    //                                            || fe.ESTADO == Const.ExpedienteInterconsumo.ANULADO)
                    //                                        )
                    //            select new
                    //            {
                    //                FACTURA = mf.FACTURA
                    //                ,
                    //                FECHA_PEDIDO = p.FECHA_PEDIDO
                    //                ,
                    //                FECHA_FACTURA = f.FECHA
                    //                ,
                    //                FECHA_ENVIO_INTERCONSUMO = mfe.FECHA == null ? new DateTime(1990, 1, 1) : mfe.FECHA
                    //                ,
                    //                CLIENTE = f.CLIENTE
                    //                ,
                    //                NOMBRE_CLIENTE = f.NOMBRE_CLIENTE
                    //                ,
                    //                TOTAL_FACTURA = f != null ? f.TOTAL_FACTURA : 0
                    //                ,
                    //                ENGANCHE = mp.ENGANCHE != null ? mp.ENGANCHE : 0
                    //                ,
                    //                MONTO_FINANCIADO = mp.SALDO_FINANCIAR != null ? mp.SALDO_FINANCIAR : 0
                    //            } into selection
                    //            orderby selection.FECHA_ENVIO_INTERCONSUMO descending
                    //            select selection
                    //                ;


                    if (qExpedientesPend.Count() > 0)
                    {
                        StringBuilder m = new StringBuilder();
                        m.Append("<p>&nbsp;</p>");
                        m.Append(string.Format("<center>Detalle de los expedientes pendientes con mas de tres dias.</center>", this.ObtenerFecha(DateTime.Now)));


                        m.Append("<table align='center' class='style4'>");
                        m.Append("<tr><td class='style2'>&nbsp;Factura&nbsp;</td>");
                        m.Append("<td class='style5'>&nbsp;Fecha factura&nbsp;</td>");
                        m.Append("<td class='style5'>&nbsp;Fecha envio a Interconsumo&nbsp;</td>");
                        m.Append("<td class='style5'>&nbsp;Cliente&nbsp;</td>");
                        m.Append("<td class='style2'>&nbsp;Nombre del cliente&nbsp;</td>");
                        m.Append("<td class='style6'>&nbsp;Total factura&nbsp;</td>");
                        m.Append("<td class='style6'>&nbsp;Enganche&nbsp;</td>");
                        m.Append("<td class='style6'>&nbsp;Monto a financiar&nbsp;</td></tr>");

                        mMontoFinanciado = 0;

                        foreach (var item in qExpedientesPend)
                        {
                            if (item.SALDO > 0)
                            {
                                m.Append(string.Format("<tr><td class='style1'>&nbsp;&nbsp;{0}&nbsp;&nbsp;</td>", item.FACTURA));
                                m.Append(string.Format("<td class='style3'>&nbsp;{0}&nbsp;</td>", ObtenerFecha(item.FECHA_FACTURA)));
                                m.Append(string.Format("<td class='style3'>&nbsp;{0}&nbsp;</td>", ObtenerFecha(item.FECHA_ENVIO_INTERCONSUMO)));
                                m.Append(string.Format("<td class='style3'>&nbsp;{0}&nbsp;</td>", item.CLIENTE));
                                m.Append(string.Format("<td class='style1'>&nbsp;{0}&nbsp;</td>", item.NOMBRE_CLIENTE));
                                m.Append(string.Format("<td class='style7'>&nbsp;{0}&nbsp;</td>", String.Format("{0:0,0.00}", item.TOTAL_FACTURA)));
                                m.Append(string.Format("<td class='style7'>&nbsp;{0}&nbsp;</td>", String.Format("{0:0,0.00}", item.ENGANCHE.Value)));
                                m.Append(string.Format("<td class='style7'>&nbsp;{0}&nbsp;</td></tr>", String.Format("{0:0,0.00}", item.MONTO_FINANCIADO.Value)));

                                mMontoFinanciado = mMontoFinanciado + item.MONTO_FINANCIADO.Value;
                            }
                            else
                            {

                                var qFacturaEstado =
                                            from f in dbExactus.MF_Factura_Estado
                                            where f.FACTURA == item.FACTURA
                                                    && f.ESTADO == Const.Expediente.ANULADO
                                            select f;
                                if (qFacturaEstado.Count() == 0)
                                {
                                    MF_Factura_Estado iFacturaEstado = new MF_Factura_Estado
                                    {
                                        FACTURA = item.FACTURA,
                                        ESTADO = Const.Expediente.ANULADO,
                                        FECHA = DateTime.Now.Date,
                                        RecordDate = DateTime.Now,
                                        CreatedBy = string.Format("FA/{0}", "SYSTEM"),
                                        UpdatedBy = string.Format("FA/{0}", "SYSTEM"),
                                        CreateDate = DateTime.Now
                                    };
                                    dbExactus.MF_Factura_Estado.AddObject(iFacturaEstado);
                                }
                                else
                                {
                                    foreach (var iFacturaEstado in qFacturaEstado)
                                    {
                                        iFacturaEstado.RecordDate = DateTime.Now;
                                    }

                                }
                            }
                        }

                        m.Append("<tr><td class='style1'></td>");
                        m.Append("<td class='style3'></td>");
                        m.Append("<td class='style3'></td>");
                        m.Append("<td class='style3'></td>");
                        m.Append("<td class='style1'></td>");
                        m.Append("<td class='style7'></td>");
                        m.Append("<td class='style7'>Total:</td>");
                        m.Append(string.Format("<td class='style7'>&nbsp;{0}&nbsp;</td></tr>", String.Format("{0:0,0.00}", mMontoFinanciado)));

                        m.Append("</table>");
                        Mensaje.Append(m.ToString());

                        var qListaCorreo =
                                            from mf_c in dbExactus.MF_Catalogo
                                            where mf_c.CODIGO_TABLA == 6
                                            select mf_c;

                        if (qListaCorreo.Count() > 0)
                        {
                            foreach (var item in qListaCorreo)
                            {
                                CuentaCorreo.Add(item.NOMBRE_CAT);
                            }
                        }
                        else
                        {
                            CuentaCorreo.Add("william.quiacain@mueblesfiesta.com");
                        }


                        Mensaje.Append("</body>");
                        Mensaje.Append("</html>");

                        Utilitario.EnviarEmail(Remitente, CuentaCorreo, string.Format("Expedientes pendientes de {0} al {1}", NombreFinanciera, this.ObtenerFecha(DateTime.Now)), Mensaje, NombreFinanciera);
                    }
                }

                mRespuesta.Exito = true;
                mRespuesta.Mensaje = "Email enviado exitosamente.";
            }
            catch (Exception ex)
            {
                mRespuesta.Mensaje = CatchClass.ExMessage(ex, MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name);
            }
            return mRespuesta;

        }

        private string ObtenerFecha(DateTime Fecha)
        {
            return string.Format("{0}{1}/{2}{3}/{4}", Fecha.Day < 10 ? "0" : "", Fecha.Day.ToString(), Fecha.Month < 10 ? "0" : "", Fecha.Month.ToString(), Fecha.Year.ToString());
        }


        public static double ObtenerDiasLaborales(DateTime startD, DateTime endD)
        {
            double calcBusinessDays =
                1 + ((endD - startD).TotalDays * 5 -
                (startD.DayOfWeek - endD.DayOfWeek) * 2) / 7;

            if (endD.DayOfWeek == DayOfWeek.Saturday) calcBusinessDays--;
            if (startD.DayOfWeek == DayOfWeek.Sunday) calcBusinessDays--;

            return calcBusinessDays;
        }

    }
}