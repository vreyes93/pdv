﻿using FiestaNETRestServices.Models;
using FiestaNETRestServices.Utils;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;

namespace FiestaNETRestServices.Controllers.Interconsumo
{
    [RoutePrefix("api/ValidarSolicitudInterconsumo")]
    public class ValidarSolicitudInterconsumoController : ApiController
    {
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        UtilitariosInterconsumo mInterconsumo = new UtilitariosInterconsumo();
        
        
       
        public string Get()
        {
            return "Hola Mundo";
        }

        [HttpPost]
        public Respuesta PostValidarSolicitudInterconsumo([FromBody]MF_Clases.Clases.SolicitudInterconsumo solicitudInterconsumo)
        {
            Respuesta mRespuesta = new Respuesta();
            try
            {
                solicitudInterconsumo.PRIMER_NOMBRE = MF_Clases.Utilitarios.DecodeStringFrom64(solicitudInterconsumo.PRIMER_NOMBRE);
                solicitudInterconsumo.SEGUNDO_NOMBRE = MF_Clases.Utilitarios.DecodeStringFrom64(solicitudInterconsumo.SEGUNDO_NOMBRE);
                solicitudInterconsumo.PRIMER_APELLIDO = MF_Clases.Utilitarios.DecodeStringFrom64(solicitudInterconsumo.PRIMER_APELLIDO);
                solicitudInterconsumo.SEGUNDO_APELLIDO = MF_Clases.Utilitarios.DecodeStringFrom64(solicitudInterconsumo.SEGUNDO_APELLIDO);
                string Errores = string.Empty;
                MF_Clases.Clases.SolicitudInterconsumo mSolicitudInterconsumoRespuesta = new MF_Clases.Clases.SolicitudInterconsumo();
                log.Info("Consumiendo WS de consulta de solicitud Interconsumo, solicitud: " + solicitudInterconsumo);
                XmlDocument xmlRespuesta = mInterconsumo.Servicio(Const.SERVICIOS_INTERCONSUMO.CONSULTAR_SOLICITUD_CREDITO, mInterconsumo.mensajeEntradaXmlConsultaSolicitud(solicitudInterconsumo.SOLICITUD.ToString(), solicitudInterconsumo.COBRADOR));
                log.Debug("Leyendo respuesta de Interconsumo.");
                mInterconsumo.CastClaseSolicitudCredito(xmlRespuesta, ref mSolicitudInterconsumoRespuesta);
                //validando información obtenida, con la ingresada por el vendedor en el punto de venta
                if (solicitudInterconsumo.SOLICITUD != mSolicitudInterconsumoRespuesta.SOLICITUD)
                    Errores += "<li>Número de solicitud no coincide con la asignada por interconsumo (" + mSolicitudInterconsumoRespuesta.SOLICITUD + ").</li>";
                if (solicitudInterconsumo.PLAZO != mSolicitudInterconsumoRespuesta.PLAZO)
                    Errores += "<li>Plazo no coincide con el plazo ingresado a interconsumo (" + mSolicitudInterconsumoRespuesta.PLAZO + ").</li>";
                if (solicitudInterconsumo.VALOR_SOLICITUD != mSolicitudInterconsumoRespuesta.VALOR_SOLICITUD)
                    Errores += "<li>Monto a financiar no coincide con el valor de solicitud ingresado a Interconsumo. (Q"+ solicitudInterconsumo.VALOR_SOLICITUD + ") vrs (Q. " + mSolicitudInterconsumoRespuesta.VALOR_SOLICITUD + ")</li>";
                if (solicitudInterconsumo.PRIMER_NOMBRE != mSolicitudInterconsumoRespuesta.PRIMER_NOMBRE )
                    Errores += "<li>El primer nombre del cliente no coincide con el ingresado a Interconsumo ("+mSolicitudInterconsumoRespuesta.PRIMER_NOMBRE+").</li>";
                if (solicitudInterconsumo.SEGUNDO_NOMBRE != mSolicitudInterconsumoRespuesta.SEGUNDO_NOMBRE )
                    Errores += "<li>El segundo nombre del cliente no coincide con el ingresado a Interconsumo (" + mSolicitudInterconsumoRespuesta.SEGUNDO_NOMBRE + ").</li>";
                if (solicitudInterconsumo.PRIMER_APELLIDO != mSolicitudInterconsumoRespuesta.PRIMER_APELLIDO )
                    Errores += "<li>El primer apellido del cliente no coincide con el ingresado a Interconsumo (" + mSolicitudInterconsumoRespuesta.PRIMER_APELLIDO + ").</li>";
                if (solicitudInterconsumo.SEGUNDO_APELLIDO != mSolicitudInterconsumoRespuesta.SEGUNDO_APELLIDO )
                    Errores += "<li>El segundo apellido del cliente no coincide con el ingresado a Interconsumo (" + mSolicitudInterconsumoRespuesta.SEGUNDO_APELLIDO + ").</li>";
                if (Errores.Equals(""))
                    mRespuesta.Exito = true;
                else
                    mRespuesta.Exito = false;
                if (!mRespuesta.Exito)
                    mRespuesta.Mensaje = "<ul>" + Errores + "</ul>";
                if (!mRespuesta.Exito && mRespuesta.Mensaje.Contains("Número de solicitud no coincide"))
                    mRespuesta.Mensaje += "<br><p>*Se encontraron varias diferencias con la información ingresada en interconsumo, revise primero que el número de solicitud sea el correcto.</p>";
                        
            } catch (Exception ex)
            {
                log.Error("Problema al validar solicitud con interconsumo: "+ex.Message+" Solicitud:"+solicitudInterconsumo.SOLICITUD+", Tienda: "+solicitudInterconsumo.COBRADOR+", se facturó sin poder validar.");
                mRespuesta.Exito = true;//para dejar guardar la factura
            }
            return mRespuesta;
        }

        [HttpGet]
        [Route("Test/{solicitud}/{cobrador}")]
        [ActionName("Test")]
        public Respuesta PostValidarSolicitudInterconsumoTest(string solicitud,string cobrador)
        {
            Respuesta mRespuesta = new Respuesta();
            try
            {
                string Errores = string.Empty;
                MF_Clases.Clases.SolicitudInterconsumo mSolicitudInterconsumoRespuesta = new MF_Clases.Clases.SolicitudInterconsumo();
                log.Info("Consumiendo WS de consulta de solicitud Interconsumo, solicitud: " + solicitud);
                XmlDocument xmlRespuesta = mInterconsumo.Servicio(Const.SERVICIOS_INTERCONSUMO.CONSULTAR_SOLICITUD_CREDITO, mInterconsumo.mensajeEntradaXmlConsultaSolicitud(solicitud, cobrador));
                log.Debug("Leyendo respuesta de Interconsumo.");
                mInterconsumo.CastClaseSolicitudCredito(xmlRespuesta, ref mSolicitudInterconsumoRespuesta);
                mRespuesta.Objeto = mSolicitudInterconsumoRespuesta;

            }
            catch (Exception ex)
            {
                log.Error("Problema al validar solicitud con interconsumo: " + ex.Message + " Solicitud:" + solicitud + ", Tienda: " +cobrador);
                mRespuesta.Exito = true;//para dejar guardar la factura
            }
            return mRespuesta;
        }
    }
}