﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;
using MF_Clases;

namespace FiestaNETRestServices.Controllers
{
    public class LinkInterconsumoController : ApiController
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public class Retorna
        {
            public bool exito { get; set; }
            public string mensaje { get; set; }
            public string link { get; set; }
        }
        
        public static Retorna LinkInter(string documento)
        {
            Retorna retorna = new Retorna();
            EXACTUSERPEntities db = new EXACTUSERPEntities();

            retorna.exito = true;

            string tipo = "C";
            if (documento.Substring(0, 1) == "P") tipo = "P";

            log.Info("Inicio de validaciones.");

            string mLink = "";
            if (tipo == "C")
            {
                Int32 mCotizacion = 0;
                if (tipo == "C") mCotizacion = Convert.ToInt32(documento);

                if ((from c in db.MF_Cotizacion where c.COTIZACION == mCotizacion select c).Count() == 0)
                {
                    retorna.exito = false;
                    retorna.mensaje = string.Format("La cotización {0} no existe.", documento);
                    return retorna;
                }

                var qCotizacion = (from c in db.MF_Cotizacion where c.COTIZACION == mCotizacion select c).First();

                if (qCotizacion.FINANCIERA != 7)
                {
                    retorna.exito = false;
                    retorna.mensaje = string.Format("La cotización {0} no pertenece a Interconsumo.", documento);
                    return retorna;
                }

                if (qCotizacion.GRABADA_EN_LINEA == null)
                {
                    retorna.exito = false;
                    retorna.mensaje = string.Format("La cotización {0} no fue grabada en línea.", documento);
                    return retorna;
                }

                if (qCotizacion.GRABADA_EN_LINEA == "N")
                {
                    retorna.exito = false;
                    retorna.mensaje = string.Format("La cotización {0} no fue grabada en línea.", documento);
                    return retorna;
                }

                mLink = qCotizacion.LINK;
            }
            else
            {
                if ((from p in db.MF_Pedido where p.PEDIDO == documento select p).Count() == 0)
                {
                    retorna.exito = false;
                    retorna.mensaje = string.Format("El pedido {0} no existe.", documento);
                    return retorna;
                }

                var qPedido = (from p in db.MF_Pedido where p.PEDIDO == documento select p).First();

                if (qPedido.FINANCIERA != 7)
                {
                    retorna.exito = false;
                    retorna.mensaje = string.Format("El pedido {0} no pertenece a Interconsumo.", documento);
                    return retorna;
                }

                if (qPedido.GRABADA_EN_LINEA == null)
                {
                    retorna.exito = false;
                    retorna.mensaje = string.Format("El pedido {0} no fue grabado en línea.", documento);
                    return retorna;
                }
                
                if (qPedido.GRABADA_EN_LINEA == "N")
                {
                    retorna.exito = false;
                    retorna.mensaje = string.Format("El pedido {0} no fue grabado en línea.", documento);
                    return retorna;
                }

                if ((from f in db.FACTURA where f.PEDIDO == documento && f.ANULADA == "N" select f).Count() > 0)
                {
                    retorna.exito = false;
                    retorna.mensaje = string.Format("El pedido {0} ya se encuentra facturado.", documento);
                    return retorna;
                }

                mLink = qPedido.LINK;
            }

            if (mLink == null) mLink = "";

            if (mLink.Trim().Length == 0)
            {
                string mMensajeError = string.Format("El pedido {0} no fue enviado a Interconsumo.", documento);

                retorna.exito = false;
                retorna.mensaje = mMensajeError;
                log.Error(mMensajeError);
            }
            else
            {
                log.Info("Link obtenido exitosamente.");
            }

            retorna.link = mLink;
            return retorna;
        }

        // GET api/linkinterconsumo
        public Retorna Get(string documento)
        {
            return LinkInter(documento);
        }

    }
}
