﻿using FiestaNETRestServices.DAL;
using MF.Comun.Dto.TiendaVirtual;
using MF_Clases;
using MF_Clases.Restful;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Results;

namespace FiestaNETRestServices.Controllers.Magento
{
    [RoutePrefix("api/Facturacion")]
    public class FacturacionController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Crear pedido y facturar desde la tienda en linea
        /// febrero 2019
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("Facturacion/{Pedido}")]
        [ActionName("Facturacion")]
        public IHttpActionResult Post([FromBody] Pedido pedido)
        {
            log.Info("tocando la puerta..");
            string mensaje = string.Empty;
            if (pedido != null)
            {
                log.Info("Comenzando la facturación desde la tienda en línea");
                string json = JsonConvert.SerializeObject(pedido);
                log.Debug("POST FACTURACION: " + json);
                DAL.DALCliente cliente = new DAL.DALCliente();
                //***
                //SI NO ENCUENTRA AL CLIENTE, LO CREA
                //***
                var respuesta = cliente.ObtenerCodigoCliente(pedido.cliente);
                if (respuesta.Exito)
                {
                    string cCLIENTE = respuesta.Objeto.ToString();
                    char[] aCli = cCLIENTE.ToArray();
                    //crear pedido 
                    pedido.cliente.codigo = new string(aCli);
                    respuesta = new Respuesta();
                    respuesta = DAL.DALTiendaVirtual.Facturar(pedido);
                    //LOG respuesta
                    json = JsonConvert.SerializeObject(respuesta);
                    log.Debug("RESULTADO FACTURACION: " + json);
                    //----
                    if (respuesta.Exito)
                        return Ok<Respuesta>(respuesta);
                    else
                        return Ok<Respuesta>(respuesta);
                }
                else
                {
                    log.Info(respuesta.Mensaje);
                    return Ok<Respuesta>(respuesta);
                }
            }
            else
            {
                log.Error("BadRequest FacturacionController");
                return BadRequest("Debe enviar un pedido válido."); }
        }


        [HttpGet]
        [Route("Facturacion/{Pedido}")]
        [ActionName("Facturacion")]
        public IHttpActionResult Get()
        {
            string urlApi = "";
            switch (General.Ambiente)
            {
                case "DES": urlApi = WebConfigurationManager.AppSettings["UrlNetunimAPIDES"].ToString(); break;
                case "PRU": urlApi = WebConfigurationManager.AppSettings["UrlNetunimAPIPRU"].ToString(); break;
                case "PRO": urlApi = WebConfigurationManager.AppSettings["UrlNetunimAPIPRO"].ToString(); break;
            }
            Api api = new Api(urlApi);

            Respuesta resp = new Respuesta();
            resp.Exito = true;

            Dictionary<string, string> header = new Dictionary<string, string>();
            header.Add("AppKey", General.AppkeyAutenticacion.ToString());
            header.Add("Token", HttpContext.Current.Request.Cookies.Get(General.CookieName)?.Value);
            var dataResponse = api.Process(RestSharp.Method.GET, "Discounts/BillingDiscount", null, header);
            if (dataResponse.StatusCode == HttpStatusCode.OK)
                return Ok(dataResponse.Content);
            return BadRequest();
        }
    }
}
