﻿using Magento.RestApi;
using MF_Clases;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace FiestaNETRestServices.Controllers.Magento
{
    [RoutePrefix("api/InventarioTienda")]
    public class InventarioTiendaController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //private const string URL = "http://172.31.45.118:92/index.php/rest/V1/integration/admin/token";
        //private const string URL1 = "http://172.31.45.118:92/index.php/rest/V1/products/";
        private const string DATA = @"{""username"": ""Admin"",""password"": ""Magento2017""}";
        


        /// <summary>
        /// Método para obtener el token de autorización de magento
        /// </summary>
        /// <returns></returns>
        private string Autorizar()
        {
            try
            {

                string URLAutenticacion = DAL.DALTiendaVirtual.ObtenerDirMagento() + DAL.DALTiendaVirtual.ObtenerDirAutenticacion();
                string Credenciales = "{\"username\": \"" + DAL.DALTiendaVirtual.ObtenerUsuarioTiendaVirtual() + "\",\"password\": \"" + DAL.DALTiendaVirtual.ObtenerPasswordTiendaVirtual() + "\"}";
                System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
                byte[] cred = UTF8Encoding.UTF8.GetBytes("username:password");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Convert.ToBase64String(cred));
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                System.Net.Http.HttpContent content = new StringContent(Credenciales, UTF8Encoding.UTF8, "application/json");
                HttpResponseMessage messge = client.PostAsync(URLAutenticacion, content).Result;
                string description = string.Empty;

                if (messge.IsSuccessStatusCode)
                {
                    string result = messge.Content.ReadAsStringAsync().Result;
                    description = result;
                    return description;
                }
                else
                {
                    string result = messge.Content.ReadAsStringAsync().Result;
                    description = result;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error al obtener Token de Magento " + ex.Message);
            }
            return "";
        }
        [HttpGet]
        [Route("Actualizar")]
        [ActionName("Actualizar")]
        public async Task<List<string>> ActualizarInventarioAsync()
        {
            List<string> respuesta = new List<string>();
            string Token = Autorizar();
            if (Token != string.Empty)
            {
                List<Object> lstProductos = new List<Object>();
                lstProductos = ObtenerProductos();
                foreach (var produ in lstProductos)
                {
                   respuesta.Add( await UpdateComponentAsync(Token, produ));
                   
                }
            }
            log.Info("-- SERVICIO DE ACTUALIZACIÓN DEL INVENTARIO FINALIZADO--");
            return respuesta;
        }

        private List<Object> ObtenerProductos()
        {
            return DAL.DALTiendaVirtual.ObtenerInventarioArticulos();
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="producto"></param>
        /// <param name="TipoProducto">1 Regular, 2 Ofertado</param>
        /// <returns></returns>
        public async Task<string> UpdateComponentAsync(string key,Object producto)
        {
            string sku = string.Empty;
            try
            {
                var rowString = key.Replace('"', ' ').Trim();
                sku = ((ProductoPrecioRegular)producto).sku;
                
                //--------
                ///Referencia, el path para los articulos debe quedar asi
                //http://172.31.45.118:92/index.php/rest/V1/products/;
                //--------

                string URL1 = DAL.DALTiendaVirtual.ObtenerDirMagento() + DAL.DALTiendaVirtual.ObtenerPathCatalogoProductos();
                Uri geturi = new Uri(URL1 + sku); //replace your url  
                System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", rowString);
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                string p = new JavaScriptSerializer().Serialize(producto);
                p = @"{""product"": " + p + " }";

                client.BaseAddress = geturi;
                client.DefaultRequestHeaders
                      .Accept
                      .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Put, geturi);
                request.Content = new StringContent(p,Encoding.UTF8, "application/json");//CONTENT-TYPE header
                
                ///------
                ///Hacer el put del articulo
               await client.SendAsync(request);
                ///-------
                ///

            } catch (Exception ex)
            {
                log.Error("ERROR AL ACTUALIZAR EL PRODUCTO EN LA TIENDA VIRTUAL ->>sku: "+sku+" : "+ex.Message);
                return sku + "NOT OK";
            }
            return sku+" OK";
        }

        /// <summary>
        /// Obtener un producto por medio de su sku
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetComponent(string key)
        {
            string URL1 = "http://172.31.45.118:92/index.php/rest/V1/products/";
            var rowString = key.Replace('"', ' ').Trim();
            Uri geturi = new Uri(URL1 + "413005-121"); //replace your url  
            System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", rowString);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            System.Net.Http.HttpResponseMessage messge = client.GetAsync(geturi,0).Result;
            if (messge.IsSuccessStatusCode)
            {
               return  messge.Content.ReadAsStringAsync().Result;
            }
            else
            {
                return messge.Content.ReadAsStringAsync().Result;
            }
           
        }
        
    }
}
