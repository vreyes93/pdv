﻿using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Magento
{
    [RoutePrefix("api/ValidarExistencias")]
    public class ValidarExistenciasController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Servicio para válidar la existencia de un artículo
        /// para la compra en línea
        /// </summary>
        /// <param name="CodArticulo"></param>
        /// <returns></returns>
        /// [HttpGet]
        [Route("ValidarExistencias/{CodArticulo}")]
        [ActionName("ValidarExistencias")]
        public IHttpActionResult Get(string CodArticulo)
        {

            Respuesta existencias = new Respuesta();

            existencias = DAL.DALTiendaVirtual.ValidarExistencias(CodArticulo);
            if (existencias.Exito)
                return Ok(existencias.Objeto);
            else return BadRequest(existencias.Mensaje);
        }


    }
}
