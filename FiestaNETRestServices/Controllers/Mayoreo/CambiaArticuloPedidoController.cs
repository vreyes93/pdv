﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Data;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;
using System.Globalization;
using System.Text.RegularExpressions;
using MF_Clases;

namespace FiestaNETRestServices.Controllers.Mayoreo
{
    public class CambiaArticuloPedidoController : ApiController
    {
        
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        // PUT api/cambiaarticulopedido/5
        public Clases.ArticuloPedido PutArticulo([FromBody]Clases.ArticuloPedido pedido)
        {
            Clases.ArticuloPedido mRetorna = new Clases.ArticuloPedido();
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            db.CommandTimeout = 999999999;
            
            try
            {
                var q = from a in db.ARTICULO where a.ARTICULO1 == pedido.ArticuloNuevo && a.ACTIVO == "S" select a;

                if (q.Count() == 0)
                {
                    mRetorna.Exito = false;
                    mRetorna.Mensaje = string.Format("El artículo {0} no existe o está inactivo.", pedido.ArticuloNuevo);
                    return mRetorna;
                }

                decimal mPrecioBase = q.First().PRECIO_BASE_LOCAL;
                decimal mIva = 1 + ((from im in db.IMPUESTO where im.IMPUESTO1 == "IVA" select im).First().IMPUESTO11 / 100);

                var qArticuloMayoreo = from a in db.MF_ArticulosMayoreo where a.CLIENTE == pedido.Cliente && a.ARTICULO == pedido.ArticuloNuevo select a;
                if (qArticuloMayoreo.Count() > 0) mPrecioBase = qArticuloMayoreo.First().PRECIO_BASE_LOCAL;
                
                if (pedido.PrecioBase == 0) mPrecioBase = 0;
                
                string mTooltip = "";
                var qExistencias = from e in db.EXISTENCIA_LOTE where e.CANT_DISPONIBLE > 0 && e.ARTICULO == pedido.ArticuloNuevo orderby e.BODEGA, e.LOCALIZACION select e;

                int ii = 0;
                int mExistencias = 0;
                
                foreach (var itemE in qExistencias)
                {
                    string mSeparador = "";
                    if (ii == 0) mSeparador = System.Environment.NewLine;

                    mExistencias += Convert.ToInt32(itemE.CANT_DISPONIBLE);
                    mTooltip = string.Format("{0}{1}{2} ({3}) = {4}", mTooltip, mSeparador, itemE.BODEGA, itemE.LOCALIZACION, itemE.CANT_DISPONIBLE.ToString().Replace(".00000000", ""));
                }

                if (mTooltip.Trim().Length == 0) mTooltip = string.Format("No hay existencias del artículo {0}", pedido.ArticuloNuevo);
                if (pedido.Tipo == "K") mTooltip = "";
                
                decimal mPrecioNuevo = mPrecioBase * mIva;
                decimal mPrecioTotal = Math.Round(mPrecioNuevo * pedido.Cantidad, 2, MidpointRounding.AwayFromZero);

                mRetorna.Pedido = pedido.Pedido;
                mRetorna.Linea = pedido.Linea;
                mRetorna.ArticuloNuevo = pedido.ArticuloNuevo;
                mRetorna.DescripcionNueva = q.First().DESCRIPCION;
                mRetorna.PrecioBaseNuevo = mPrecioBase;
                mRetorna.PrecioNuevo = mPrecioNuevo;
                mRetorna.Cantidad = pedido.Cantidad;
                mRetorna.PrecioTotal = mPrecioTotal;
                mRetorna.Tooltip = mTooltip;
                
                mRetorna.Exito = true;
                mRetorna.Mensaje = "Artículo cambiado exitosamente.";
            }
            catch (Exception ex)
            {
                mRetorna.Exito = false;
                mRetorna.Mensaje = CatchClass.ExMessage(ex, "CambiaArticuloPedidoController", "PutArticulo");
            }

            return mRetorna;
        }

    }
}
