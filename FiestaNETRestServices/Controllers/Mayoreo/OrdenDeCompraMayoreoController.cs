﻿
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Mayoreo
{
    public class OrdenDeCompraMayoreoController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// Rest
        /// </summary>
        /// <param name="OrdenDeCompra"></param>
        /// <param name="Fecha"></param>
        /// <param name="Cliente"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("OrdenDeCompraMayoreo/OrdenDeCompra={OrdenDeCompra}&FechaIni={FechaIni}&FechaFin={FechaFin}&Cliente={Cliente}&NombreCliente={NombreCliente}&Tienda={Tienda}")]
        public IHttpActionResult Get(string OrdenDeCompra,string FechaIni,string FechaFin,string Cliente,string NombreCliente,string Tienda)
        {
            try
            {
                log.Debug("DalMayoreo obteniendo resultado de consulta de ordenes de compra: Orden de compra: [" + OrdenDeCompra + "] FechaIni: [" + FechaIni + "] FechaFin: [" + FechaFin + "] Cliente: [" + Cliente + "] Nombre Cliente: ["+NombreCliente+"] Tienda: ["+Tienda+"]");
                DAL.DALMayoreo mayoreo = new DAL.DALMayoreo();
                var resp = mayoreo.getFacturas(OrdenDeCompra,FechaIni,FechaFin,Cliente,NombreCliente,Tienda);
                if (resp.Exito)
                {
                    return Ok<List<FacturaMayoreo>>((List<FacturaMayoreo>)resp.Objeto);
                }
                else
                {
                    log.Error("DAL MAYOREO :" + resp.Mensaje);
                    return BadRequest(resp.Mensaje);
                }
            }
            catch(Exception ex)
            {
                log.Error(CatchClass.ExMessage(ex,"OrdenDeCompra","Get"));
                return BadRequest(ex.Message);
            }
        }
    }
}
