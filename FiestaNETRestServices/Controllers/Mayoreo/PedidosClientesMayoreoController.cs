﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using FiestaNETRestServices.Content.Abstract;
using Newtonsoft.Json;
using MF_Clases;
using System.Web.Configuration;

namespace FiestaNETRestServices.Controllers.Mayoreo
{
    public class PedidosClientesMayoreoController : MFApiController
    {
        
        // GET api/pedidosclientesmayoreo/5
        public string GetPedidosClientesMayoreoController(int dias = -1, string destinatario = "")
        {
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            db.CommandTimeout = 999999999;
            
            try
            {
                DateTime mFechaInicial = DateTime.Now.Date.AddDays(dias);
                DateTime mFechaFinal = mFechaInicial;

                StringBuilder mBody = new StringBuilder();
                StringBuilder mBodyResumen = new StringBuilder();
                StringBuilder mBodyDetalle = new StringBuilder();

                List<string> mDestinatarios = new List<string>();

                string mRemitente = Utils.Utilitarios.GetRemitenteNotificacion(Const.REMITENTE_CORREO.REMITENTE_PEDIDOS_MAYOREO, Const.LISTA_CORREO.REMITENTES_CORREO);
                string mDisplayName = "Pedidos de Mayoreo";
                string mAsunto = string.Format("Pedidos de mayoreo del {0}", Utilitario.FormatoDDMMYYYY(mFechaInicial));

                mBody.Append(Utilitario.CorreoInicioFormatoAzul());

                var qClientes = (from p in db.PEDIDO
                                 join c in db.CLIENTE on p.CLIENTE equals c.CLIENTE1
                                 where p.FECHA_PEDIDO >= mFechaInicial && p.FECHA_PEDIDO <= mFechaFinal && p.CLIENTE.Substring(0, 2) == "MA"
                                 select new
                                 {
                                     Cliente = p.CLIENTE,
                                     Nombre = c.NOMBRE
                                 }).Distinct().OrderBy(x => x.Cliente);

                decimal mTotalGeneral = 0;

                mBodyResumen.Append("<table align='center' class='style4'>");
                mBodyResumen.Append("<tr>");
                mBodyResumen.Append(Utilitario.FormatoTituloIzquierdo("Cliente"));
                mBodyResumen.Append(Utilitario.FormatoTituloCentro("Total"));
                mBodyResumen.Append("</tr>");

                foreach (var c in qClientes)
                {
                    string mCliente = c.Cliente;
                    string mNombreCliente = c.Nombre;

                    var q = from p in db.PEDIDO
                            join pl in db.MF_Pedido_Linea_Original on p.PEDIDO1 equals pl.PEDIDO
                            join a in db.ARTICULO on pl.ARTICULO equals a.ARTICULO1
                            where p.CLIENTE == mCliente && pl.ARTICULO.Substring(0, 1) != "0" && pl.ARTICULO.Substring(0, 1) != "S" && pl.PRECIO_TOTAL  > 0
                            && p.FECHA_PEDIDO >= mFechaInicial && p.FECHA_PEDIDO <= mFechaFinal
                            orderby p.PEDIDO1
                            select new
                            {
                                Cantidad = pl.CANTIDAD_PEDIDA,
                                Articulo = pl.ARTICULO,
                                Descripcion = a.DESCRIPCION,
                                Precio = pl.PRECIO_TOTAL,
                                Pedido = p.PEDIDO1
                            };

                    mBodyDetalle.Append(string.Format("<left><strong>Pedidos de {0} - {1}:</strong></left>", mCliente, mNombreCliente));
                    mBodyDetalle.Append("<br />");
                    mBodyDetalle.Append("<br />");
                    mBodyDetalle.Append("<table align='center' class='style4'>");
                    mBodyDetalle.Append("<tr>");
                    mBodyDetalle.Append(Utilitario.FormatoTituloCentro("Pedido"));
                    mBodyDetalle.Append(Utilitario.FormatoTituloIzquierdo("Artículo"));
                    mBodyDetalle.Append(Utilitario.FormatoTituloIzquierdo("Descripción"));
                    mBodyDetalle.Append(Utilitario.FormatoTituloCentro("Cantidad"));
                    mBodyDetalle.Append(Utilitario.FormatoTituloCentro("Total"));
                    mBodyDetalle.Append("</tr>");

                    decimal mTotal = 0; decimal mCantidad = 0;

                    foreach (var item in q)
                    {
                        mCantidad += Convert.ToDecimal(item.Cantidad);
                        mTotal += Convert.ToDecimal(item.Precio);
                        mTotalGeneral += Convert.ToDecimal(item.Precio);

                        mBodyDetalle.Append("<tr>");
                        mBodyDetalle.Append(Utilitario.FormatoCuerpoCentro(item.Pedido));
                        mBodyDetalle.Append(Utilitario.FormatoCuerpoIzquierda(item.Articulo));
                        mBodyDetalle.Append(Utilitario.FormatoCuerpoIzquierda(item.Descripcion));
                        mBodyDetalle.Append(Utilitario.FormatoCuerpoCentro(Utilitario.FormatoNumeroEntero(item.Cantidad)));
                        mBodyDetalle.Append(Utilitario.FormatoCuerpoDerecha(Utilitario.FormatoNumeroDecimal(Convert.ToDecimal(item.Precio))));
                        mBodyDetalle.Append("</tr>");
                    }

                    mBodyDetalle.Append("<tr>");
                    mBodyDetalle.Append(Utilitario.FormatoTituloCentro(""));
                    mBodyDetalle.Append(Utilitario.FormatoTituloIzquierdo(""));
                    mBodyDetalle.Append(Utilitario.FormatoTituloDerecha("TOTALES:"));
                    mBodyDetalle.Append(Utilitario.FormatoTituloCentro(Utilitario.FormatoNumeroEntero(mCantidad)));
                    mBodyDetalle.Append(Utilitario.FormatoTituloDerecha(Utilitario.FormatoNumeroDecimal(mTotal)));
                    mBodyDetalle.Append("</tr>");

                    mBodyDetalle.Append("</table>");
                    mBodyDetalle.Append("<br />");
                    mBodyDetalle.Append("<br />");
                    
                    mBodyResumen.Append("<tr>");
                    mBodyResumen.Append(Utilitario.FormatoCuerpoIzquierda(string.Format("{0} - {1}", mCliente, mNombreCliente)));
                    mBodyResumen.Append(Utilitario.FormatoCuerpoDerecha(Utilitario.FormatoNumeroDecimal(mTotal)));
                    mBodyResumen.Append("</tr>");
                }
                
                mBodyResumen.Append("<tr>");
                mBodyResumen.Append(Utilitario.FormatoTituloDerecha("TOTALES:"));
                mBodyResumen.Append(Utilitario.FormatoTituloDerecha(Utilitario.FormatoNumeroDecimal(mTotalGeneral)));
                mBodyResumen.Append("</tr>");

                mBodyResumen.Append("</table>");
                mBodyResumen.Append("<br />");
                mBodyResumen.Append("<br />");

                mBody.Append(mBodyResumen.ToString());
                mBody.Append(mBodyDetalle.ToString());

                mBody.Append(string.Format("<left><strong>TOTAL GENERAL: Q{0}</strong></left>", Utilitario.FormatoNumeroDecimal(mTotalGeneral)));
                mBody.Append(Utilitario.CorreoFin());

                if (destinatario == "")
                {
                    var qListaCorreo =
                                        from mf_c in dbExactus.MF_Catalogo
                                        where mf_c.CODIGO_TABLA == 9
                                        select mf_c;

                    if (qListaCorreo.Count() > 0)
                    {
                        foreach (var item2 in qListaCorreo)
                        {
                            mDestinatarios.Add(item2.NOMBRE_CAT);
                        }
                    }
                    else
                    {
                        mDestinatarios.Add(WebConfigurationManager.AppSettings["ITMailNotification"].ToString());
                    }
                }
                else
                {
                    if (destinatario == "mf") mDestinatarios.Add(WebConfigurationManager.AppSettings["ITMailNotification"].ToString());
                    if (destinatario == "gmail") mDestinatarios.Add(WebConfigurationManager.AppSettings["ITMailNotification"].ToString());
                }

                if (mTotalGeneral > 0) Utilitario.EnviarEmail(mRemitente, mDestinatarios, mAsunto, mBody, mDisplayName);
            }
            catch (Exception ex)
            {
                return CatchClass.ExMessage(ex, "PedidosClientesMayoreoController", "GetPedidosClientesMayoreo");
            }

            return "Correo enviado exitosamente";
        }

    }
}
