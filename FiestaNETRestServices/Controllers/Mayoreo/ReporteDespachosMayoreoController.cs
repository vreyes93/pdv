﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Web.Http;
using FiestaNETRestServices.Content.Abstract;
using MF_Clases;
using FiestaNETRestServices.Models;
using System.Web.Configuration;

namespace FiestaNETRestServices.Controllers.Mayoreo
{
    public class ReporteDespachosMayoreoController : MFApiController
    {

        private new static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string GetReporteDespachosMayoreoController()
        {
            string mRetorna = "Correo enviado exitosamente";

            DateTime mFechaInicial = DateTime.Now.Date;
            DateTime mFechaFinal = DateTime.Now.Date;
            StringBuilder mMensaje = new StringBuilder();
            List<string> mCuentaCorreo = new List<string>();
            
            string mCliente = "MA0017";
            string mRemitente = Utils.Utilitarios.GetRemitenteNotificacion(Const.REMITENTE_CORREO.REMITENTE_REPORTE_DESPACHOS_MAYOREO, Const.LISTA_CORREO.REMITENTES_CORREO);
            //  string mRemitente = "puntodeventa2@productosmultiples.com";

            try
            {
                string mFactura = ""; string mEnvio = "";


                mMensaje.Append(Utilitario.CorreoInicioFormatoAzul());
                mMensaje.Append(string.Format("<center>Detalle de las entregas programadas para el día {0}</center>", Utilitario.FormatoDDMMYYYY(mFechaInicial)));
                mMensaje.Append("<table align='center' class='style4'>");
                mMensaje.Append("<tr>");

                mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Tienda"));
                mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Nombre del cliente"));
                mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Caja"));
                mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Transacción"));
                mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Vendedor"));
                mMensaje.Append(Utilitario.FormatoTituloCentro("Cantidad"));
                mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Artículo"));
                mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Descripción"));
                mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Factura"));
                mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Envío"));

                mMensaje.Append("</tr>");

                DataTable dt = Pedidos(mFechaInicial, mFechaFinal, mCliente);

                for (int ii = 0; ii < dt.Rows.Count; ii++)
                {
                    mFactura = "No tiene factura";
                    mEnvio = "No tiene envío";

                    string mPedido = dt.Rows[ii]["Pedido"].ToString();

                    if ((from f in db.FACTURA where f.PEDIDO == mPedido && f.ANULADA == "N" select f).Count() > 0)
                    {
                        mFactura = (from f in db.FACTURA where f.PEDIDO == mPedido && f.ANULADA == "N" select f).First().FACTURA1;

                        if ((from d in db.MF_Despacho join de in db.DESPACHO on d.DESPACHO equals de.DESPACHO1 where d.FACTURA == mFactura && de.ESTADO == "D" select d).Count() > 0)
                            mEnvio = (from d in db.MF_Despacho join de in db.DESPACHO on d.DESPACHO equals de.DESPACHO1 where d.FACTURA == mFactura && de.ESTADO == "D" select d).First().DESPACHO;
                    }

                    mMensaje.Append("<tr>");

                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(dt.Rows[ii]["Tienda"].ToString()));
                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(dt.Rows[ii]["Cliente"].ToString()));
                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(dt.Rows[ii]["Caja"].ToString()));
                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(dt.Rows[ii]["Transaccion"].ToString()));
                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(dt.Rows[ii]["Vendedor"].ToString()));
                    mMensaje.Append(Utilitario.FormatoCuerpoCentro(Utilitario.FormatoNumeroEntero(Convert.ToInt32(dt.Rows[ii]["Cantidad"]))));
                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(dt.Rows[ii]["Articulo"].ToString()));
                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(dt.Rows[ii]["Descripcion"].ToString()));
                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(mFactura));
                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(mEnvio));

                    mMensaje.Append("</tr>");
                }

                mMensaje.Append("</table>");

                mMensaje.Append("<br />");
                mMensaje.Append("<br />");
                mMensaje.Append("<br />");

                mMensaje.Append(string.Format("<center>Pedidos pendientes</center>"));
                mMensaje.Append("<table align='center' class='style4'>");
                mMensaje.Append("<tr>");

                mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Tienda"));
                mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Nombre del cliente"));
                mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Caja"));
                mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Transacción"));
                mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Vendedor"));
                mMensaje.Append(Utilitario.FormatoTituloCentro("Cantidad"));
                mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Artículo"));
                mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Descripción"));
                mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Factura"));
                mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Envío"));

                mMensaje.Append("</tr>");

                DataTable dtPendientes = Pedidos(new DateTime(2040, 1, 1), new DateTime(2040, 1, 1), mCliente);

                for (int ii = 0; ii < dtPendientes.Rows.Count; ii++)
                {
                    mFactura = "No tiene factura";
                    mEnvio = "No tiene envío";

                    string mPedido = dtPendientes.Rows[ii]["Pedido"].ToString();

                    if ((from f in db.FACTURA where f.PEDIDO == mPedido && f.ANULADA == "N" select f).Count() > 0)
                    {
                        mFactura = (from f in db.FACTURA where f.PEDIDO == mPedido && f.ANULADA == "N" select f).First().FACTURA1;

                        if ((from d in db.MF_Despacho join de in db.DESPACHO on d.DESPACHO equals de.DESPACHO1 where d.FACTURA == mFactura && de.ESTADO == "D" select d).Count() > 0)
                            mEnvio = (from d in db.MF_Despacho join de in db.DESPACHO on d.DESPACHO equals de.DESPACHO1 where d.FACTURA == mFactura && de.ESTADO == "D" select d).First().DESPACHO;
                    }

                    mMensaje.Append("<tr>");

                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(dtPendientes.Rows[ii]["Tienda"].ToString()));
                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(dtPendientes.Rows[ii]["Cliente"].ToString()));
                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(dtPendientes.Rows[ii]["Caja"].ToString()));
                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(dtPendientes.Rows[ii]["Transaccion"].ToString()));
                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(dtPendientes.Rows[ii]["Vendedor"].ToString()));
                    mMensaje.Append(Utilitario.FormatoCuerpoCentro(Utilitario.FormatoNumeroEntero(Convert.ToInt32(dtPendientes.Rows[ii]["Cantidad"]))));
                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(dtPendientes.Rows[ii]["Articulo"].ToString()));
                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(dtPendientes.Rows[ii]["Descripcion"].ToString()));
                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(mFactura));
                    mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(mEnvio));

                    mMensaje.Append("</tr>");
                }

                mMensaje.Append("</table>");

                var qListaCorreo =  from mf_c in dbExactus.MF_Catalogo
                                    where mf_c.CODIGO_TABLA == 8
                                    select mf_c;

                if (qListaCorreo.Count() > 0)
                {
                    foreach (var item2 in qListaCorreo)
                    {
                        mCuentaCorreo.Add(item2.NOMBRE_CAT);
                    }
                }
                else
                {
                    mCuentaCorreo.Add(WebConfigurationManager.AppSettings["ITMailNotification"].ToString());
                }

                mMensaje.Append(Utilitario.CorreoFin());
                Utilitario.EnviarEmail(mRemitente, mCuentaCorreo, string.Format("Despachos de Cemaco del día {0}", Utilitario.FormatoDDMMYYYY(mFechaInicial)), mMensaje, "Despachos de Cemaco");
            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0} {1}", CatchClass.ExMessage(ex, "ReporteDespachosMayoreoController", "GetComisiones"), "GetReporteDespachosMayoreoController");
            }

            return mRetorna;
        }
        
        DataTable Pedidos(DateTime FechaInicial, DateTime FechaFinal, string cliente)
        {
            var q = from p in db.MF_Pedido
                    join pl in db.PEDIDO_LINEA on p.PEDIDO equals pl.PEDIDO
                    join a in db.ARTICULO on pl.ARTICULO equals a.ARTICULO1
                    join pe in db.PEDIDO on p.PEDIDO equals pe.PEDIDO1
                    join u in db.USUARIO on p.USUARIO equals u.USUARIO1
                    where p.CLIENTE == cliente && p.FECHA_ENTREGA >= FechaInicial && p.FECHA_ENTREGA <= FechaFinal && pl.ARTICULO.Substring(0, 1) != "0"
                    orderby p.CEMACO_TIENDA
                    select new
                    {
                        Tienda = p.CEMACO_TIENDA,
                        Cliente = p.CEMACO_CLIENTE_NOMBRE,
                        Caja = p.CEMACO_CAJA,
                        Transaccion = p.CEMACO_TRANSACCION,
                        Cantidad = pl.CANTIDAD_PEDIDA,
                        Articulo = pl.ARTICULO,
                        Descripcion = a.DESCRIPCION,
                        Vendedor = u.NOMBRE,
                        Pedido = p.PEDIDO
                    };

            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Tienda", typeof(System.String)));
            dt.Columns.Add(new DataColumn("Cliente", typeof(System.String)));
            dt.Columns.Add(new DataColumn("Caja", typeof(System.String)));
            dt.Columns.Add(new DataColumn("Transaccion", typeof(System.String)));
            dt.Columns.Add(new DataColumn("Cantidad", typeof(System.Int32)));
            dt.Columns.Add(new DataColumn("Articulo", typeof(System.String)));
            dt.Columns.Add(new DataColumn("Descripcion", typeof(System.String)));
            dt.Columns.Add(new DataColumn("Vendedor", typeof(System.String)));
            dt.Columns.Add(new DataColumn("Pedido", typeof(System.String)));


            foreach (var item in q)
            {
                if (Convert.ToInt32(dt.Compute("COUNT(Caja)", string.Format("Caja = '{0}' AND Transaccion = '{1}'", item.Caja, item.Transaccion))) == 0)
                {
                    DataRow mRow = dt.NewRow();
                    mRow["Tienda"] = item.Tienda;
                    mRow["Cliente"] = item.Cliente;
                    mRow["Caja"] = item.Caja;
                    mRow["Transaccion"] = item.Transaccion;
                    mRow["Cantidad"] = item.Cantidad;
                    mRow["Articulo"] = item.Articulo;
                    mRow["Descripcion"] = item.Descripcion;
                    mRow["Vendedor"] = item.Vendedor;
                    mRow["Pedido"] = item.Pedido;
                    dt.Rows.Add(mRow);
                }
            }

            return dt;
        }

    }
}
