﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Models;
using MF_Clases;

namespace FiestaNETRestServices.Controllers.Mayoreo
{
    public class ReportePedidosDelDiaController : MFApiController
    {

        //-----------------------------------------------------------------------------------------------------------------------
        // se documenta el query para tomar como referencia el codigo en linq de C#
        //-----------------------------------------------------------------------------------------------------------------------
        //        select	mp.cemaco_tienda			AS TIENDA
        //		, mp.cemaco_cliente_nombre	AS NOMBRE_CLIENTE
        //		, mp.CEMACO_CAJA			AS CAJA
        //		, mp.CEMACO_TRANSACCION		AS NUMERO_TRANSACCION
        //		, u.nombre					AS VENDEDOR
        //		, pl.CANTIDAD_PEDIDA		AS CANTIDAD
        //		, pl.articulo				AS ARTICULO
        //		, pl.DESCRIPCION			AS DESCRIPCION
        //		, mp.PEDIDO
        //		, pl.PEDIDO_LINEA
        //		, p.FECHA_HORA
        //      , mp.FECHA_ENTREGA
        //from	prodmult.mf_pedido mp
        //		, prodmult.pedido p
        //		, prodmult.pedido_linea pl
        //		, erpadmin.USUARIO U
        //where	ltrim(rtrim(mp.cemaco_tienda)) <> ''
        //		and P.CLIENTE ='MA0017'
        //		and p.pedido = mp.pedido
        //		and u.usuario = mp.usuario
        //		and pl.pedido = p.pedido
        //		and substring(pl.ARTICULO,1,1) <> '0'
        //		and mp.FECHA_REGISTRO >= '2017-08-01'
        //		and mp.FECHA_REGISTRO < '2017-08-18'
        //		and 
        //			(
        //				mp.PEDIDO = (
        //					select	max(mp2.PEDIDO)
        //					from	prodmult.mf_pedido mp2
        //					where	mp2.CEMACO_CAJA				= mp.CEMACO_CAJA
        //							and mp2.CEMACO_TRANSACCION	= mp.CEMACO_TRANSACCION
        //							and substring(mp.CEMACO_CAJA,1,1) in ('1','2','3','4','5','6','7','8','9')
        //							and substring(mp.CEMACO_TRANSACCION,1,1) in ('1','2','3','4','5','6','7','8','9')
        //					)
        //				)
        //UNION ALL
        //select	mp.cemaco_tienda			AS TIENDA
        //		, mp.cemaco_cliente_nombre	AS NOMBRE_CLIENTE
        //		, mp.CEMACO_CAJA			AS CAJA
        //		, mp.CEMACO_TRANSACCION		AS NUMERO_TRANSACCION
        //		, u.nombre					AS VENDEDOR
        //		, pl.CANTIDAD_PEDIDA		AS CANTIDAD
        //		, pl.articulo				AS ARTICULO
        //		, pl.DESCRIPCION			AS DESCRIPCION
        //		, mp.PEDIDO
        //		, pl.PEDIDO_LINEA
        //		, p.FECHA_HORA
        //      , mp.FECHA_ENTREGA
        //from	prodmult.mf_pedido mp
        //		, prodmult.pedido p
        //		, prodmult.pedido_linea pl
        //		, erpadmin.USUARIO U
        //where	ltrim(rtrim(mp.cemaco_tienda)) <> ''
        //		and P.CLIENTE ='MA0017'
        //		and p.pedido = mp.pedido
        //		and u.usuario = mp.usuario
        //		and pl.pedido = p.pedido
        //		and substring(pl.ARTICULO,1,1) <> '0'
        //		and mp.FECHA_REGISTRO >= '2017-08-01'
        //		and mp.FECHA_REGISTRO < '2017-08-18'
        //		and substring(mp.CEMACO_CAJA,1,1) NOT in ('1','2','3','4','5','6','7','8','9')
        //		and substring(mp.CEMACO_TRANSACCION,1,1) NOT in ('1','2','3','4','5','6','7','8','9')
        //-----------------------------------------------------------------------------------------------------------------------
        


        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función Obtiene el reporte de ventas del dia de Cemaco, y las envia por correo.
        /// Este reporte se ejecuta todos los dias a las 2:00 a.m.
        /// </summary>
        /// <returns>Retorna OK</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public string GetReportePedidosDelDiaController(string destinatario = "")
        {

            //---------------------------------------------------------------------------------------------------------
            // Variables necesarias para el reporte.
            //---------------------------------------------------------------------------------------------------------
            DateTime mFechaInicial = DateTime.Now.Date.AddDays(-1);
            DateTime mFechaFinal = mFechaInicial;
            var mNumeros = new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
            string mCliente = "MA0017";//codigo de cliente de Cemaco

            //---------------------------------------------------------------------------------------------------------
            // Variable necesarias para el envio de correo.
            //---------------------------------------------------------------------------------------------------------
            StringBuilder mMensaje = new StringBuilder();
            List<string> mCuentaCorreo = new List<string>();
            //  string mRemitente = "william.quiacain@mueblesfiesta.com";
            string mRemitente = Utils.Utilitarios.GetRemitenteNotificacion(Const.REMITENTE_CORREO.REMITENTE_REPORTE_PEDIDOS_DEL_DIA, Const.LISTA_CORREO.REMITENTES_CORREO);
            string mDisplayName = "Ventas Cemaco";
            string mAsunto = string.Format("Ventas de Cemaco del día {0}", Utilitario.FormatoDDMMYYYY(mFechaInicial));

            try
            {
                

                //---------------------------------------------------------------------------------------------------------
                //Realizamos la consulta a la base de datos.
                //Esta se divide en dos partes:
                // 1. Los casos en que otenemos el último pedido de varios que tienen la misma transaccion y caja de Cemaco
                //    que empiecen con un número, esto es para descriminar los repetidos.
                //
                // 2. Los casos en que otenemos los pedidos que tienen la transaccion y caja de Cemaco distinto a numero
                //    a veces les ponene "---", "A CREDITO", "00", "000", etc. estos casos se pueden repetir, ya que no
                //    existe se registra manualmente una transaccion y numero de caja valido.
                //---------------------------------------------------------------------------------------------------------
                var qPedidosMayoreo =   (
                                            from mp in dbExactus.MF_Pedido
                                            join p in dbExactus.PEDIDO on mp.PEDIDO equals p.PEDIDO1
                                            join pl in dbExactus.MF_Pedido_Linea_Original on mp.PEDIDO equals pl.PEDIDO
                                            join u in dbExactus.USUARIO on mp.USUARIO equals u.USUARIO1
                                            where p.FECHA_PEDIDO >= mFechaInicial
                                                    && p.FECHA_PEDIDO <= mFechaFinal
                                                    && p.CLIENTE == mCliente
                                                    && pl.ARTICULO.Substring(0,1) != "0"
                                                    && mp.CEMACO_TIENDA != ""
                                                    && mp.PEDIDO == (
                                                                dbExactus.MF_Pedido.Where(
                                                                    u => u.CEMACO_CAJA == mp.CEMACO_CAJA
                                                                    && u.CEMACO_TRANSACCION == mp.CEMACO_TRANSACCION
                                                                    && mNumeros.Contains(mp.CEMACO_CAJA.Substring(0, 1))
                                                                    && mNumeros.Contains(mp.CEMACO_TRANSACCION.Substring(0, 1))
                                                                    ).Select(us => us.PEDIDO).Max()
                                                            )
                                            select new
                                            {
                                                TIENDA = mp.CEMACO_TIENDA
                                                ,NOMBRE_CLIENTE = mp.CEMACO_CLIENTE_NOMBRE
                                                ,TRANSACCION = mp.CEMACO_TRANSACCION
                                                ,CAJA = mp.CEMACO_CAJA
                                                ,VENDEDOR = u.NOMBRE
                                                ,CANTIDAD = pl.CANTIDAD_PEDIDA
                                                ,PEDIDO = p.PEDIDO1
                                                ,PEDIDO_LINEA = pl.PEDIDO_LINEA
                                                ,ARTICULO = pl.ARTICULO
                                                ,DESCRIPCION_ARTICULO = pl.DESCRIPCION
                                                ,FECHA_ENTREGA = mp.FECHA_ENTREGA 
                                            }
                                        ).Concat(
                                            from mp2 in dbExactus.MF_Pedido
                                            join p2 in dbExactus.PEDIDO on mp2.PEDIDO equals p2.PEDIDO1
                                            join pl2 in dbExactus.MF_Pedido_Linea_Original on mp2.PEDIDO equals pl2.PEDIDO
                                            join u2 in dbExactus.USUARIO on mp2.USUARIO equals u2.USUARIO1
                                            where p2.FECHA_PEDIDO >= mFechaInicial
                                                    && p2.FECHA_PEDIDO <= mFechaFinal
                                                    && p2.CLIENTE == mCliente
                                                    && pl2.ARTICULO.Substring(0, 1) != "0"
                                                    && mp2.CEMACO_TIENDA != ""
                                                    && !mNumeros.Contains(mp2.CEMACO_CAJA.Substring(0, 1))
                                                    && !mNumeros.Contains(mp2.CEMACO_TRANSACCION.Substring(0, 1))

                                            select new
                                            {
                                                TIENDA = mp2.CEMACO_TIENDA
                                                ,NOMBRE_CLIENTE = mp2.CEMACO_CLIENTE_NOMBRE
                                                ,TRANSACCION = mp2.CEMACO_TRANSACCION
                                                ,CAJA = mp2.CEMACO_CAJA
                                                ,VENDEDOR = u2.NOMBRE
                                                ,CANTIDAD = pl2.CANTIDAD_PEDIDA
                                                ,PEDIDO = p2.PEDIDO1
                                                ,PEDIDO_LINEA = pl2.PEDIDO_LINEA
                                                ,ARTICULO = pl2.ARTICULO
                                                ,DESCRIPCION_ARTICULO = pl2.DESCRIPCION
                                                ,FECHA_ENTREGA = mp2.FECHA_ENTREGA
                                            }
                                        )
                                        ;

                //---------------------------------------------------------------------------------------------------------
                //Verificamos que tenga al menos un registro
                //---------------------------------------------------------------------------------------------------------
                if (qPedidosMayoreo.Count() > 0)
                {

                    //---------------------------------------------------------------------------------------------------------
                    //Ordenamos el arreglo
                    //---------------------------------------------------------------------------------------------------------
                    var qPedidosMayoreoOrdenado = qPedidosMayoreo
                                            .OrderBy(a => a.TIENDA)
                                            .ThenBy(b => b.VENDEDOR)
                                            .ThenBy(c => c.PEDIDO)
                                            .ThenBy(d => d.PEDIDO_LINEA);

                    //---------------------------------------------------------------------------------------------------------
                    //Redactamos el encabezado del correo
                    //---------------------------------------------------------------------------------------------------------
                    mMensaje.Append(Utilitario.CorreoInicioFormatoAzul());
                    mMensaje.Append(string.Format("<center>Detalle de ventas del día {0}</center>", Utilitario.FormatoDDMMYYYY(mFechaInicial)));
                    mMensaje.Append("<table align='center' class='style4'>");
                    mMensaje.Append("<tr>");
                    mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Tienda"));
                    mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Pedido"));
                    mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Nombre del cliente"));
                    mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Caja"));
                    mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Transacción"));
                    mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Vendedor"));
                    mMensaje.Append(Utilitario.FormatoTituloCentro("Fecha de entrega"));
                    mMensaje.Append(Utilitario.FormatoTituloCentro("Cantidad"));
                    mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Artículo"));
                    mMensaje.Append(Utilitario.FormatoTituloIzquierdo("Descripción"));
                    mMensaje.Append("</tr>");

                    //---------------------------------------------------------------------------------------------------------
                    //Recorremos el arreglo para imprimir las ventas del dia.
                    //---------------------------------------------------------------------------------------------------------
                    foreach (var item in qPedidosMayoreoOrdenado)
                    {
                        mMensaje.Append("<tr>");
                        mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.TIENDA));
                        mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.PEDIDO));
                        mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.NOMBRE_CLIENTE));
                        mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.CAJA));
                        mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.TRANSACCION));
                        mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.VENDEDOR));
                        mMensaje.Append(Utilitario.FormatoCuerpoCentro(Utilitario.FormatoDDMMYYYY(item.FECHA_ENTREGA ?? DateTime.Now.AddYears(40))));
                        mMensaje.Append(Utilitario.FormatoCuerpoCentro(Utilitario.FormatoNumeroEntero(item.CANTIDAD)));
                        mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.ARTICULO));
                        mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.DESCRIPCION_ARTICULO));
                        mMensaje.Append("</tr>");
                    }

                    mMensaje.Append("</table>");
                    mMensaje.Append(Utilitario.CorreoFin());

                    //---------------------------------------------------------------------------------------------------------
                    //Redactamos el encabezado del correo
                    //---------------------------------------------------------------------------------------------------------
                    if (destinatario == "")
                    {
                        var qListaCorreo =
                                            from mf_c in dbExactus.MF_Catalogo
                                            where mf_c.CODIGO_TABLA == 7
                                            select mf_c;

                        if (qListaCorreo.Count() > 0)
                        {
                            foreach (var item2 in qListaCorreo)
                            {
                                mCuentaCorreo.Add(item2.NOMBRE_CAT);
                            }
                        }
                        else
                        {
                            mCuentaCorreo.Add("william.quiacain@mueblesfiesta.com");
                        }
                    }
                    else
                    {
                        if (destinatario == "mf") mCuentaCorreo.Add(WebConfigurationManager.AppSettings["ITMailNotification"].ToString());
                        if (destinatario == "gmail") mCuentaCorreo.Add(WebConfigurationManager.AppSettings["ITMailNotification"].ToString());
                    }

                    //---------------------------------------------------------------------------------------------------------
                    //Procedemos a enviar el correo.
                    //---------------------------------------------------------------------------------------------------------
                    Utilitario.EnviarEmail(mRemitente, mCuentaCorreo, mAsunto, mMensaje, mDisplayName);

                }
                else
                {
                }

            }
            catch (Exception ex)
            {
                return "" + ex.StackTrace; 
            }

            return "";
        }

    }
}
