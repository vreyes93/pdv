﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Mayoreo
{
    [RoutePrefix("api/ReporteSets")]
    public class ReporteSetsController : MFApiController
    {
       
        [HttpGet]
        public string getReporteSets()
        {
            DALMayoreo dMayoreo = new DALMayoreo();
            try
            {
                Respuesta r = dMayoreo.getSetCamasIncompletos();
                List<SetCamas> lstSets = new List<SetCamas>();
                if (r.Exito)
                    lstSets = ((List<SetCamas>)(r.Objeto));
                if (lstSets.Count > 0)
                {
                    EnviarNotificacion(lstSets);
                }
                return lstSets.Count().ToString() + " inválidos encontrados.";
            }
            catch(Exception ex)
            {
                log.Error("Error al enviar notificación los sets de mayoreo " + ex.Message);
            }
            return null;
        }

        /// <summary>
        /// Arma el formato de notificaciones para avisar sobre los sets de
        /// mayoreo que le hagan falta componentes, por ejemplo patas o bases.
        /// </summary>
        /// <param name="lstSetsInvalidos"></param>
        private void EnviarNotificacion(List<SetCamas> lstSetsInvalidos)
        {
            ////---------------------------------------------------------------------------------------------------------
            //// Variable necesarias para el envio de correo.
            ////---------------------------------------------------------------------------------------------------------
            string mRemitente = Utils.Utilitarios.GetRemitenteNotificacion(Const.REMITENTE_CORREO.REMITENTE_PEDIDOS_MAYOREO, Const.LISTA_CORREO.REMITENTES_CORREO);
            string mDisplayName = "";
            List<string> lstBcc = new List<string>();
            string mBcc = "patricia.rodriguez@mueblesfiesta.com";
            var qBcc = db.MF_Catalogo.Where(x => x.CODIGO_TABLA == MF_Clases.Utils.CONSTANTES.CATALOGOS.NOTIFICACIONES_DESARROLLO_PRUEBAS).ToList();
            qBcc.ForEach(x => lstBcc.Add(x.NOMBRE_CAT));
            ////---------------------------------------------------------------------------------------------
            ////Redactamos el Display name, crédito aprobado o rechazado.
            ////---------------------------------------------------------------------------------------------
            mDisplayName = $"Muebles Fiesta - ALERTA DE SETS-";

            ////---------------------------------------------------------------------------------------------
            ////Redactamos el asunto del correo
            ////---------------------------------------------------------------------------------------------

            string mAsunto = $"Muebles Fiesta - ALERTA - Sets inválidos";

            ////---------------------------------------------------------------------------------------------
            ////Agregamos a los destinatario
            ////---------------------------------------------------------------------------------------------
            List<string> mCuentaCorreo = new List<string>();

            ////---------------------------------------------------------------------------------------------
            ////Variable para el cuerpo del correo
            ////---------------------------------------------------------------------------------------------
            StringBuilder mMensaje = new StringBuilder();

            Utils.Utilitarios mUtilitario = new Utils.Utilitarios();
            mUtilitario.GetListaCorreo(Const.LISTA_CORREO.MAYOREO_NOTIFICACIONES_SETS_INVALIDOS, ref mCuentaCorreo);
            


            mMensaje.Append(Utilitario.CorreoInicioFormatoRojo());
            mMensaje.Append(string.Format("<p align=\"center\"><img  src =cid:EmailAlert alt =\"Alerta\"></p><p> <br/>Estimados(as):<BR/> Les informamos que la siguiente lista de SETS de mayoreo, <B> está incompleto, le hace falta un componente</B><BR/> Por favor revise.</p>"));
            mMensaje.Append("<BR/>");
            mMensaje.Append("<table align='center' class='style4'>");
            mMensaje.Append("<tr>");
            mMensaje.Append(Utilitario.FormatoTituloCentro("Artículo"));
            mMensaje.Append(Utilitario.FormatoTituloCentro("Descripción"));
            mMensaje.Append("</tr>");

            foreach (var item in lstSetsInvalidos)
            {
                
                mMensaje.Append(Utilitario.FormatoCuerpoCentro(item.codArticulo));
                mMensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.DescripcionSet));
                mMensaje.Append("</tr>");
            }

            mMensaje.Append("</table><br/>");

            mMensaje.Append(string.Format("<p>Gracias por su atención,<br>"));
            mMensaje.Append(string.Format("Saludos cordiales.</p>"));
            mMensaje.Append(Utilitario.CorreoFin());

            //---------------------------------------------------------------------------------------------------------
            //Procedemos a enviar el correo.
            //---------------------------------------------------------------------------------------------------------
            if (lstSetsInvalidos.Count > 0)
            {
                List<ImagenAdjunta> lstImagenes = new List<ImagenAdjunta>();
                lstImagenes.Add(new MF_Clases.ImagenAdjunta(System.Web.Hosting.HostingEnvironment.MapPath("~//Images/email_alert.png"), MediaTypeNames.Image.Jpeg, "EmailAlert"));
                Utilitario.EnviarEmail(mRemitente, mCuentaCorreo, mAsunto, mMensaje, mDisplayName, lstImagenes, BccTo: lstBcc);
            }
        }

        [HttpPost]
        [ActionName("PatasPedido")]
        public Respuesta PatasPedido(List<DtoLoteBases> lstBases)
        {
            //List<DtoLoteBases> lstBases = new List<DtoLoteBases>();
            //lstBases.Add(new DtoLoteBases { Base=new DtoBaseCama {  codArticulo= "142043-087" },Cantidad=1 });
            //lstBases.Add(new DtoLoteBases { Base= new DtoBaseCama { codArticulo = "142045-087" }, Cantidad = 1 });
            //lstBases.Add(new DtoLoteBases { Base = new DtoBaseCama { codArticulo = "142041-087" }, Cantidad = 1 });
            DALMayoreo dMayoreo = new DALMayoreo();
            var mRespuesta  = dMayoreo.getLotesPatas(lstBases);
            return mRespuesta;
        }
    }
}
