﻿using FiestaNETRestServices.Content.Abstract;
using MF.Comun.Dto.Facturacion;
using MF_Clases.Comun;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Mercadeo
{
   
    public class PromocionesController : MFApiController
    {

        [Route("~/Promociones")]// 
        [HttpPost]
        public IHttpActionResult Post([FromBody] PromocionRegistro reg)
        {
            DAL.DALPromociones pr = new DAL.DALPromociones();
            
            
                return Ok(pr.PromocionesTienda(reg));
            
        }
    }
}
