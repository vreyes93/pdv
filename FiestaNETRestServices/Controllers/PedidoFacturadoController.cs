﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;
using MF_Clases;


namespace FiestaNETRestServices.Controllers
{
    public class PedidoFacturadoController : ApiController
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public class Retorna
        {
            public bool exito { get; set; }
            public string mensaje { get; set; }
            public string facturado { get; set; }
        }

        // GET api/pedidofacturado
        public Retorna Get(string documento)
        {
            Retorna retorna = new Retorna();
            EXACTUSERPEntities db = new EXACTUSERPEntities();

            retorna.exito = true;
            retorna.facturado = "N";

            log.Info("Validando si el pedido ya está facturado.");

            if ((from p in db.PEDIDO where p.PEDIDO1 == documento select p).Count() == 0)
            {
                retorna.exito = false;
                retorna.facturado = "";
                retorna.mensaje = string.Format("El pedido {0} no existe.", documento);
                return retorna;
            }

            if ((from f in db.FACTURA where f.PEDIDO == documento && f.ANULADA == "N" select f).Count() > 0)
            {
                retorna.facturado = "S";
                retorna.mensaje = "Este pedido ya se encuentra facturado.";
            }

            return retorna;
        }
    }
}
