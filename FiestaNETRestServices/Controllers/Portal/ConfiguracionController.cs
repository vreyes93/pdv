﻿using FiestaNETRestServices.DAL;
using MF.Comun.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Portal
{
    [RoutePrefix("api/Configuracion")]
    public class ConfiguracionController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [HttpPost]
        [Route("CambiarTienda/{usuario}/{nuevaTienda}")]
        [ActionName("CambiarTienda")]
        public IHttpActionResult CambiarTienda(string usuario,string nuevaTienda)
        {
            DALAutenticacionUsuario aUsuario = new DALAutenticacionUsuario();
            string mensaje = string.Empty;
            bool respuesta = aUsuario.CambiarTienda(usuario, nuevaTienda, ref mensaje);
            if (respuesta)
                return Ok(true);
            else
                return BadRequest(mensaje);

        }
        [HttpPost]
        [Route("CambiarTiendaVendedor/{vendedor}/{nuevaTienda}")]
        [ActionName("CambiarTiendaVendedor")]
        public IHttpActionResult CambiarTiendaVendedor(string vendedor, string nuevaTienda)
        {
            DALAutenticacionUsuario aUsuario = new DALAutenticacionUsuario();
            string mensaje = string.Empty;
            bool respuesta = aUsuario.CambiarTiendaVendedor(vendedor, nuevaTienda, ref mensaje);
            if (respuesta)
                return Ok(true);
            else
                return BadRequest(mensaje);

        }
        [HttpGet]
        [ActionName("Tiendas")]
        public IEnumerable<Tienda> GetTienda()
        {
            string mensaje = string.Empty;
            DALTienda objTienda = new DALTienda();
            var i = objTienda.ObtenerTiendas();
            if (i != null)
                return i;
            else
                return null;

        }
    }
}
