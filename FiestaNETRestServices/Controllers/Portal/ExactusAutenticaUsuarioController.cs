﻿using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers
{
    public class ExactusAutenticaUsuarioController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public IHttpActionResult Post(string pUsuario, string pContrasena)
        {
            log.Debug("Init");
            ////-----------------------------------------------------------------------------------------------------------------
            ////Instancia para conectarse  la capa DataAccessLayer.
            DALAutenticacionUsuario mDALUsuarioExactus = new DALAutenticacionUsuario();

            ////-----------------------------------------------------------------------------------------------------------------
            ////Instancia para desencriptar el texto.
            //DataDirectory mRestServiceDD = new DataDirectory();

            //-----------------------------------------------------------------------------------------------------------------
            //Desencriptar los parametros de Usuario y Contraseña.
            log.Debug("DataDirectory");
            string mUsuario =FiestaNETRestServices.Utils.Utilitarios.Decrypt(pUsuario, true);
            string mContrasena = FiestaNETRestServices.Utils.Utilitarios.Decrypt(pContrasena, true);

            //-----------------------------------------------------------------------------------------------------------------
            //Para devolver los datos del usuario.
            UsuarioPDVDto mUsuarioPDV = new UsuarioPDVDto();
            try
            {

                //-----------------------------------------------------------------------------------------------------------------
                //Valida la autenticacion del usuario en la base de datos.
                log.Info("ExactusAutenticaUsuario");
                ExatusConnectionModels authExactus = new ExatusConnectionModels();
                if (authExactus.TestConnection(mUsuario, mContrasena))
                {

                    log.Debug("Find Usuario");
                    //-----------------------------------------------------------------------------------------------------------------
                    //Obtenemos el codigo del vendedor respecto al atributo Celular

                    USUARIO mUsuarioExactus = mDALUsuarioExactus.BuscarUsuarioByUsuario(mUsuario);
                    if (mUsuarioExactus == null)
                        return BadRequest("No tiene privilegios en el sistema, contacte al administrador del sistema.");


                    log.Debug("Find Vendedor");
                    //-----------------------------------------------------------------------------------------------------------------
                    //Obtenemos los datos del vendedor, tales como Tienda, Codigo vendedor, Usuario, Nombre
                    VENDEDOR mVendedor = mDALUsuarioExactus.BuscarVendedorByVendedor(mUsuarioExactus.CELULAR);
                    if (mVendedor == null)
                        return BadRequest("No tiene vendedor asignado|Contacte al administrador del sistema|Alerta.");

                    MF_Vendedor Vendedor = mDALUsuarioExactus.BuscarMF_VendedorByVendedor(mUsuarioExactus.CELULAR);
                    if (Vendedor == null)
                        return BadRequest("No tiene vendedor asignado|Contacte al administrador del sistema|Alerta.");

                    mUsuarioPDV.UsuarioExactus = mUsuario;
                    mUsuarioPDV.Vendedor = mVendedor.VENDEDOR1;
                    mUsuarioPDV.Tienda = mVendedor.E_MAIL; //ESTA COLUMNA CONTIENE EL CÓDIGO DE LA TIENDA DE CADA VENDEDOR
                    mUsuarioPDV.Email = Vendedor.EMAIL;
                    mUsuarioPDV.Nombre = mVendedor.NOMBRE; // ESTA COLUMNA CONTIENE EL NOMBRE COMPLETO DEL USUARIO
                    mUsuarioPDV.Exito = true;

                    log.Debug("Find Roles");
                    mUsuarioPDV.Roles = mDALUsuarioExactus.BuscarRolesByUsuario(mUsuario);

                    log.Debug("Finish");
                    return Ok(mUsuarioPDV);
                }
                log.Debug("Finish");
                return BadRequest("Usuario o contraseña inválidos.");

            }
            catch (Exception e)
            {
                log.Error(string.Format("{0} - {1}", e.Message, e.StackTrace));
                return BadRequest("Problemas al autenticar, contacte al administrador del sistema." + e.ToString());
            }
        }


    }


}