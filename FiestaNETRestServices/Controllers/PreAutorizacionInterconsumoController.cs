﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;
using MF_Clases;
using System.Web.Configuration;
using FiestaNETRestServices.Utils;

namespace FiestaNETRestServices.Controllers
{
    public class PreAutorizacionInterconsumoController : ApiController
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public class Retorna
        {
            public bool exito { get; set; }
            public string mensaje { get; set; }
            public string solicitud { get; set; }
            public string autorizacion { get; set; }
            public string resultadoSolicitud { get; set; }
            public string quienAutorizo { get; set; }
        }

        // GET api/preautorizacioninterconsumo
        public Retorna GetRespuestaSolicitud(string documento)
        {
            Retorna retorna = new Retorna();
            EXACTUSERPEntities db = new EXACTUSERPEntities();

            retorna.exito = true;
            string mInfoError = string.Format(" - Pedido: {0}", documento);

            try
            {
                log.Info("Iniciando validación");

                if (documento.Trim().Length == 0)
                {
                    retorna.exito = false;
                    retorna.mensaje = "Debe seleccionar un pedido o una cotización.";
                    return retorna;
                }

                string tipo = "C";
                if (documento.Substring(0, 1) == "P") tipo = "P";

                string mAmbiente = "DES";

                if (db.Connection.ConnectionString.Contains("sql.fiesta.local")) mAmbiente = "PRO";
                if (db.Connection.ConnectionString.Contains("PRUEBAS")) mAmbiente = "PRU";

                if (documento == "P000000")
                {
                    retorna.exito = false;
                    retorna.mensaje = "Debe seleccionar un pedido.";
                    return retorna;
                }

                if (documento == "000000")
                {
                    retorna.exito = false;
                    retorna.mensaje = "Debe seleccionar una cotización.";
                    return retorna;
                }

                Int32 mCotizacion = 0;
                if (tipo == "C") mCotizacion = Convert.ToInt32(documento);

                string mCliente = "";
                if (tipo == "C")
                {
                    if ((from c in db.MF_Cotizacion where c.COTIZACION == mCotizacion select c).Count() == 0)
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("La cotización {0} no existe.", documento);
                        return retorna;
                    }

                    var qCotizacion = (from c in db.MF_Cotizacion where c.COTIZACION == mCotizacion select c).First();

                    if (qCotizacion.FINANCIERA != 7)
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("La cotización {0} no pertenece a Interconsumo.", documento);
                        return retorna;
                    }

                    try
                    {
                        mCliente = qCotizacion.CLIENTE;
                    }
                    catch
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("La cotización {0} debe tener cliente asignado.", documento);
                        return retorna;
                    }

                    if (mCliente == null)
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("La cotización {0} debe tener cliente asignado.", documento);
                        return retorna;
                    }

                    if (mCliente.Trim().Length == 0)
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("La cotización {0} debe tener cliente asignado.", documento);
                        return retorna;
                    }

                    if (qCotizacion.GRABADA_EN_LINEA == "S")
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("La cotización {0} ya fue grabada en Interconsumo.", documento);
                        return retorna;
                    }

                    Int32 mDiasGracia = Convert.ToInt32((from c in db.MF_Configura select c).First().diasGraciaCotizacion);
                    DateTime mFechaVence = (from c in db.MF_Cotizacion where c.COTIZACION == mCotizacion select c).First().FECHA_VENCE;

                    mFechaVence = mFechaVence.AddDays(mDiasGracia);

                    if (DateTime.Now.Date > mFechaVence)
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("Esta cotización ya está vencida y se terminaron los {0} dias de gracia autorizados, no es posible utilizarla.", mDiasGracia);
                        return retorna;
                    }

                    mInfoError = string.Format(" - Cotización: {0}", documento);
                }
                else
                {
                    if ((from p in db.MF_Pedido where p.PEDIDO == documento select p).Count() == 0)
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("El pedido {0} no existe.", documento);
                        return retorna;
                    }

                    var qPedido = (from p in db.MF_Pedido where p.PEDIDO == documento select p).First();

                    if (qPedido.FINANCIERA != 7)
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("El pedido {0} no pertenece a Interconsumo.", documento);
                        return retorna;
                    }

                    if ((from f in db.FACTURA where f.PEDIDO == documento && f.ANULADA == "N" select f).Count() > 0)
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("El pedido {0} ya se encuentra facturado.", documento);
                        return retorna;
                    }

                    if (qPedido.GRABADA_EN_LINEA == "S")
                    {
                        retorna.exito = false;
                        retorna.mensaje = string.Format("El pedido {0} ya fue grabado en Interconsumo.", documento);
                        return retorna;
                    }
                }

                if (!ValidaDatos(tipo, documento))
                {
                    retorna.exito = false;
                    retorna.mensaje = "cliente";
                    return retorna;
                }


                decimal mMontoMinimo = Convert.ToDecimal((from c in db.MF_Configura select c).First().MINIMO_INTERCONSUMO);
                int mFinanciera = 0; decimal mSaldoFinanciar = 0; string mVendedorInterconsumo = ""; string mTienda = ""; decimal mCuota = 0; decimal mUltimaCuota = 0; decimal mEnganche = 0; string mNivel = "";
                string mIdProducto = ""; string mTelefonoTienda = ""; string mPlazo = ""; decimal mPrecioBien = 0; string mEnLinea = ""; string mNombreVendedor = ""; string mEmailVendedor = ""; string mNombreCliente = "";

                if (tipo == "C")
                {
                    var qCotizacion = from c in db.MF_Cotizacion where c.COTIZACION == mCotizacion select c;
                    foreach (var item in qCotizacion)
                    {
                        mTienda = item.COBRADOR;
                        mFinanciera = Convert.ToInt32(item.FINANCIERA);
                        mSaldoFinanciar = Convert.ToDecimal(item.SALDO_FINANCIAR);
                        mCuota = Convert.ToDecimal(item.MONTO_PAGOS1);
                        mUltimaCuota = Convert.ToDecimal(item.MONTO_PAGOS2);
                        mEnganche = Convert.ToDecimal(item.ENGANCHE);
                        mNivel = item.NIVEL_PRECIO;
                        mPrecioBien = Convert.ToDecimal(item.TOTAL_FACTURAR);
                        mEnLinea = item.AUTORIZACION_EN_LINEA.ToString();

                        try
                        {
                            mVendedorInterconsumo = (from v in db.MF_Vendedor where v.VENDEDOR == item.VENDEDOR select v).First().INTERCONSUMO;
                        }
                        catch
                        {
                            retorna.exito = false;
                            retorna.mensaje = "Usted no tiene código en Interconsumo, debe solicitarlo al kiosko y confirmarlo en su perfil del sistema.";
                            return retorna;
                        }
                    }
                }
                else
                {
                    var qPedido = from p in db.MF_Pedido where p.PEDIDO == documento select p;
                    foreach (var item in qPedido)
                    {
                        mCliente = item.CLIENTE;
                        mFinanciera = Convert.ToInt32(item.FINANCIERA);
                        mSaldoFinanciar = Convert.ToDecimal(item.SALDO_FINANCIAR);
                        mTienda = (from pe in db.PEDIDO where pe.PEDIDO1 == documento select pe).First().COBRADOR;
                        mCuota = Convert.ToDecimal(item.MONTO_PAGOS1);
                        mUltimaCuota = Convert.ToDecimal(item.MONTO_PAGOS2);
                        mEnganche = Convert.ToDecimal(item.ENGANCHE);
                        mNivel = item.NIVEL_PRECIO;
                        mPrecioBien = Convert.ToDecimal(item.TOTAL_FACTURAR);
                        mEnLinea = item.AUTORIZACION_EN_LINEA.ToString();

                        if (item.FACTURA != null)
                        {
                            retorna.exito = false;
                            retorna.mensaje = "Este pedido ya está facturado, no es posible enviarlo a Interconsumo.";
                            return retorna;
                        }

                        try
                        {
                            mVendedorInterconsumo = (from pe in db.PEDIDO join v in db.MF_Vendedor on pe.VENDEDOR equals v.VENDEDOR where pe.PEDIDO1 == documento select v).First().INTERCONSUMO;
                        }
                        catch
                        {
                            retorna.exito = false;
                            retorna.mensaje = "Usted no tiene código en Interconsumo, debe solicitarlo al kiosko y enviarlo inmediatamente al departamento de IT.";
                            return retorna;
                        }
                    }
                }

                if (mFinanciera != 7)
                {
                    retorna.exito = false;
                    retorna.mensaje = "Esta opción solamente está disponible para Interconsumo.";
                    return retorna;
                }

                if (mEnLinea == "S")
                {
                    retorna.exito = false;
                    retorna.mensaje = "Este pedido ya fue enviado a Interconsumo.";
                    if (tipo == "C") retorna.mensaje = "Esta cotización ya fue enviada a Interconsumo.";
                    return retorna;
                }

                try
                {
                    if (mVendedorInterconsumo.Trim().Length > 0)
                    {
                        mEmailVendedor = (from v in db.MF_Vendedor where v.INTERCONSUMO == mVendedorInterconsumo select v).First().EMAIL;
                        mNombreVendedor = (from v in db.MF_Vendedor join ve in db.VENDEDOR on v.VENDEDOR equals ve.VENDEDOR1 where v.INTERCONSUMO == mVendedorInterconsumo select ve).First().NOMBRE;
                    }
                }
                catch
                {
                    if (mAmbiente == "PRO")
                    {
                        retorna.exito = false;
                        retorna.mensaje = "Usted no tiene código en Interconsumo, debe solicitarlo al kiosko y enviarlo inmediatamente al departamento de IT.";
                        return retorna;
                    }
                }
                
                if (mVendedorInterconsumo == null)
                {
                    retorna.exito = false;
                    retorna.mensaje = "Usted no tiene código en Interconsumo, debe solicitarlo al kiosko y grabarlo en su perfil del PDV.";
                    return retorna;
                }

                if (mVendedorInterconsumo.Trim().Length == 0)
                {
                    retorna.exito = false;
                    retorna.mensaje = "Usted no tiene código en Interconsumo, debe solicitarlo al kiosko y grabarlo en su perfil del PDV.";
                    return retorna;
                }

                mNombreCliente = (from c in db.CLIENTE where c.CLIENTE1 == mCliente select c).First().NOMBRE;

                if (mSaldoFinanciar < mMontoMinimo)
                {
                    retorna.exito = false;
                    retorna.mensaje = string.Format("El saldo a financiar debe ser mayor o igual a Q{0}", String.Format("{0:0,0.00}", mMontoMinimo));
                    return retorna;
                }

                log.Info("Iniciando revisión de los datos del cliente.");

                DateTime mFechaNacimiento = DateTime.Now.Date; DateTime mFechaIngreso = DateTime.Now; string mGenero = ""; Int32 mEstadoCivil = 0;
                string mNit = ""; string mVivienda = ""; string mPagoMensual = ""; string mIngresos = ""; string mEgresos = ""; string mDpi = "";
                string mPrimerNombre = ""; string mSegundoNombre = ""; string mTercerNombre = ""; string mPrimerApellido = ""; string mSegundoApellido = ""; string mApellidoCasada = "";
                string mProfesion = ""; string mCelular = ""; string mConsumidorFinal = ""; string mEmail = ""; string mCalle = ""; string mCasa = ""; string mZona = ""; string mApartamento = ""; string mColonia = "";
                string mDepartamento = ""; string mMunicipio = ""; string mTelefono1 = ""; string mTelefono2 = ""; string mCargasFamiliares = ""; string mEmpresa = ""; string mPuesto = ""; string mTelefonoEmpresa = "";
                string mCalleEmpresa = ""; string mCasaEmpresa = ""; string mZonaEmpresa = ""; string mApartamentoEmpresa = ""; string mColoniaEmpresa = ""; string mDepartamentoEmpresa = ""; string mMunicipioEmpresa = "";
                string mBanco = ""; string mIdBanco = ""; string mCuenta = ""; string mPersonalNombre1 = ""; string mPersonalResidencia1 = ""; string mPersonalMovil1 = ""; string mPersonalTrabajo1 = "";
                string mPersonalNombre2 = ""; string mPersonalResidencia2 = ""; string mPersonalMovil2 = ""; string mPersonalTrabajo2 = "";
                string mComercialNombre1 = ""; string mComercialResidencia1 = ""; string mComercialMovil1 = ""; string mComercialTrabajo1 = ""; string mComercialTelefono1 = "";
                string mComercialNombre2 = ""; string mComercialResidencia2 = ""; string mComercialMovil2 = ""; string mComercialTrabajo2 = ""; string mComercialTelefono2 = "";

                var q = from c in db.MF_Cliente where c.CLIENTE == mCliente select c;
                foreach (var item in q)
                {
                    mDpi = item.DPI;
                    mPrimerNombre = item.PRIMER_NOMBRE;
                    mSegundoNombre = item.SEGUNDO_NOMBRE;
                    mPrimerApellido = item.PRIMER_APELLIDO;
                    mSegundoApellido = item.SEGUNDO_APELLIDO;
                    mApellidoCasada = item.APELLIDO_CASADA;
                    mFechaNacimiento = Convert.ToDateTime(item.FECHA_NACIMIENTO);
                    mGenero = item.GENERO == "H" ? "0" : "1";
                    mEstadoCivil = Convert.ToInt32((from e in db.MF_EstadoCivil where e.EstadoCivil == item.ESTADO_CIVIL select e).First().VALOR_INTERCONSUMO);
                    mNit = (from cl in db.CLIENTE where cl.CLIENTE1 == item.CLIENTE select cl).First().CONTRIBUYENTE;
                    mPagoMensual = item.PAGO_MENSUAL;
                    mProfesion = (from p in db.MF_Profesion where p.DESCRIPCION == item.PROFESION select p).First().PROFESION.ToString();
                    mCelular = item.CELULAR;
                    mConsumidorFinal = item.CF.ToString();
                    mCargasFamiliares = item.CARGAS_FAMILIARES.Trim().Length == 0 ? "0" : item.CARGAS_FAMILIARES;
                    mFechaIngreso = Convert.ToDateTime(item.FECHA_INGRESO_LABORES);

                    if (mPagoMensual.Trim().Length > 0)
                    {
                        mVivienda = "2";
                        mPagoMensual = mPagoMensual.Trim().Replace(",", "").Replace(" ", "").Replace("Q", "").Replace("q", "");
                    }
                    else
                    {
                        mVivienda = "1";
                        if (item.VIVE_EN == "F") mVivienda = "3";
                        if (item.VIVE_EN == "A") mVivienda = "4";
                    }

                    var qCliente = from cl in db.CLIENTE where cl.CLIENTE1 == item.CLIENTE select cl;
                    foreach (var itemCliente in qCliente)
                    {
                        mIngresos = itemCliente.RUBRO11_CLIENTE.Trim().Replace(",", "").Replace(" ", "").Replace("Q.", "").Replace("q.", "").Replace("Q", "").Replace("q", "").Replace(" ", "").Replace("$.", "");
                        mEgresos = itemCliente.RUBRO12_CLIENTE.Trim().Replace(",", "").Replace(" ", "").Replace("Q.", "").Replace("q.", "").Replace("Q", "").Replace("q", "").Replace(" ", "").Replace("$.", "");

                        mEmail = itemCliente.E_MAIL;
                        mTelefono1 = itemCliente.TELEFONO1;
                        mTelefono2 = itemCliente.TELEFONO2;

                        mEmpresa = itemCliente.RUBRO1_CLIENTE;
                        mTelefonoEmpresa = itemCliente.RUBRO3_CLIENTE;
                        mPuesto = (from pu in db.MF_Puesto where pu.DESCRIPCION == itemCliente.RUBRO4_CLIENTE select pu).First().PUESTO.ToString();
                    }


                    var qDireccion = from d in db.DETALLE_DIRECCION join cl in db.CLIENTE on d.DETALLE_DIRECCION1 equals cl.DETALLE_DIRECCION where cl.CLIENTE1 == mCliente select d;
                    foreach (var itemDireccion in qDireccion)
                    {
                        mCalle = itemDireccion.CAMPO_1;
                        mCasa = itemDireccion.CAMPO_2;
                        mZona = itemDireccion.CAMPO_4;
                        mApartamento = itemDireccion.CAMPO_3;
                        mColonia = itemDireccion.CAMPO_5;

                        if (mCalle == null) mCalle = "";
                        if (mCasa == null) mCasa = "";
                        if (mZona == null) mZona = "";
                        if (mApartamento == null) mApartamento = "";
                        if (mColonia == null) mColonia = "";

                        string mNombreZona = string.Format("{0}, {1}", itemDireccion.CAMPO_6.ToUpper(), itemDireccion.CAMPO_7.ToUpper());
                        string mZonaCodigo = (from z in db.ZONA where z.NOMBRE.ToUpper() == mNombreZona select z).First().ZONA1;

                        var qZona = (from m in db.MF_Municipio where m.ZONA == mZonaCodigo select m).First();

                        mMunicipio = qZona.MUNICIPIO.ToString();
                        mDepartamento = qZona.DEPARTAMENTO.ToString();
                    }

                    mCalleEmpresa = item.CALLE_EMPRESA;
                    mCasaEmpresa = item.CASA_EMPRESA;
                    mZonaEmpresa = item.ZONA_EMPRESA;
                    mApartamentoEmpresa = item.APARTAMENTO_EMPRESA;
                    mColoniaEmpresa = item.COLONIA_EMPRESA;

                    string mNombreZonaEmpresa = string.Format("{0}, {1}", item.DEPARTAMENTO_EMPRESA.ToUpper(), item.MUNICIPIO_EMPRESA.ToUpper());
                    string mZonaCodigoEmpresa = (from z in db.ZONA where z.NOMBRE.ToUpper() == mNombreZonaEmpresa select z).First().ZONA1;

                    var qZonaEmpresa = (from m in db.MF_Municipio where m.ZONA == mZonaCodigoEmpresa select m).First();

                    mMunicipioEmpresa = qZonaEmpresa.MUNICIPIO.ToString();
                    mDepartamentoEmpresa = qZonaEmpresa.DEPARTAMENTO.ToString();

                    mPersonalNombre1 = item.REF_PERSONAL_NOMBRE1;
                    mPersonalResidencia1 = item.REF_PERSONAL_TEL_RESIDENCIA1;
                    mPersonalMovil1 = item.REF_PERSONAL_CELULAR1;
                    mPersonalTrabajo1 = item.REF_PERSONAL_TEL_TRABAJO1;

                    mPersonalNombre2 = item.REF_PERSONAL_NOMBRE2;
                    mPersonalResidencia2 = item.REF_PERSONAL_TEL_RESIDENCIA2;
                    mPersonalMovil2 = item.REF_PERSONAL_CELULAR2;
                    mPersonalTrabajo2 = item.REF_PERSONAL_TEL_TRABAJO2;

                    mComercialNombre1 = item.REF_COMERCIAL_NOMBRE1;
                    mComercialResidencia1 = item.REF_COMERCIAL_TELEFONO1;
                    mComercialMovil1 = item.REF_COMERCIAL_CELULAR1;
                    mComercialTrabajo1 = item.REF_COMERCIAL_FAX1;

                    mComercialNombre2 = item.REF_COMERCIAL_NOMBRE2;
                    mComercialResidencia2 = item.REF_COMERCIAL_TELEFONO2;
                    mComercialMovil2 = item.REF_COMERCIAL_CELULAR2;
                    mComercialTrabajo2 = item.REF_COMERCIAL_FAX2;

                    mComercialTelefono1 = mComercialTrabajo1;
                    if (mComercialTrabajo1.Trim().Length == 0)
                    {
                        mComercialTrabajo1 = mComercialMovil1;
                        if (mComercialMovil1.Trim().Length == 0) mComercialTrabajo1 = mComercialResidencia1;
                    }

                    mComercialTelefono2 = mComercialTrabajo2;
                    if (mComercialTrabajo2.Trim().Length == 0)
                    {
                        mComercialTrabajo2 = mComercialMovil2;
                        if (mComercialMovil2.Trim().Length == 0) mComercialTrabajo2 = mComercialResidencia2;
                    }

                    if (item.REFERENCIAS_BANCARIAS.Trim().Length > 0)
                    {
                        mBanco = item.BANCO;
                        mCuenta = item.REFERENCIAS_BANCARIAS;
                        mIdBanco = (from b in db.MF_Banco where b.NOMBRE == item.BANCO select b).First().BANCO.ToString();
                    }
                }

                if (mConsumidorFinal == "S") mNit = "";

                mTelefonoTienda = (from c in db.MF_Cobrador where c.COBRADOR == mTienda select c).First().TELEFONO1;
                if (mTelefonoTienda.Length > 8) mTelefonoTienda.Substring(0, 8);

                string mPagosNivel = (from n in db.MF_NIVEL_PRECIO_COEFICIENTE where n.NIVEL_PRECIO == mNivel select n).First().PAGOS;

                mPlazo = mPagosNivel.Replace(" PAGOS", "");

                mIdProducto = "1";
                if (mPagosNivel.Contains("60")) mIdProducto = "11";


                //Validaciones
                if (mPrimerNombre.Length > 50) mPrimerNombre = mPrimerNombre.Substring(0, 50);
                if (mSegundoNombre.Length > 50)
                {
                    mTercerNombre = mSegundoNombre.Substring(50, mSegundoNombre.Length - 50);
                    mSegundoNombre = mSegundoNombre.Substring(0, 50);
                }
                if (mPrimerApellido.Length > 50) mPrimerApellido = mPrimerApellido.Substring(0, 50);
                if (mSegundoApellido.Length > 50) mSegundoApellido = mSegundoApellido.Substring(0, 50);
                if (mApellidoCasada.Length > 50) mPrimerApellido = mPrimerApellido.Substring(0, 50);

                if (mCelular.Length > 8)
                {
                    retorna.exito = false;
                    retorna.mensaje = "El celular del cliente es inválido, debe tener 8 dígitos, sin espacios ni guiones.";
                    return retorna;
                }

                if (mEmail.Length > 40)
                {
                    retorna.exito = false;
                    retorna.mensaje = "El correo del cliente es inválido. debe tener un máximo de 40 caracteres.";
                    return retorna;
                }

                if (mCalle.Length > 50)
                {
                    retorna.exito = false;
                    retorna.mensaje = "La calle de la dirección del cliente no puede ser mayor a 50 caracteres.";
                    return retorna;
                }
                if (mCasa.Length > 15)
                {
                    retorna.exito = false;
                    retorna.mensaje = "La casa de la dirección del cliente no puede ser mayor a 15 caracteres.";
                    return retorna;
                }
                if (mZona.Length > 2)
                {
                    retorna.exito = false;
                    retorna.mensaje = "La zona de la casa del cliente es inválida, por favor vaya a la ficha del cliente y actualice la dirección.";
                    return retorna;
                }
                if (mColonia.Length > 60)
                {
                    retorna.exito = false;
                    retorna.mensaje = "La colonia de la dirección del cliente no puede ser mayor a 60 caracteres.";
                    return retorna;
                }
                if (mApartamento.Length > 15)
                {
                    retorna.exito = false;
                    retorna.mensaje = "El apartamento de la dirección del cliente no puede ser mayor a 15 caracteres.";
                    return retorna;
                }
                if (mTelefono1.Length > 8)
                {
                    retorna.exito = false;
                    retorna.mensaje = "El teléfono de la casa del cliente no puede ser mayor a 8 dígitos, sin espacios ni guiones.";
                    return retorna;
                }
                if (mTelefono2.Length > 8)
                {
                    retorna.exito = false;
                    retorna.mensaje = "El teléfono del trabajo del cliente no puede ser mayor a 8 dígitos, sin espacios ni guiones.";
                    return retorna;
                }

                if (mEmpresa.Length > 255)
                {
                    retorna.exito = false;
                    retorna.mensaje = "El nombre de la empresa donde labora el cliente no puede ser mayor a 255 caracteres.";
                    return retorna;
                }

                if (mCalleEmpresa.Length > 50)
                {
                    retorna.exito = false;
                    retorna.mensaje = "La calle de la dirección del cliente no puede ser mayor a 50 caracteres.";
                    return retorna;
                }
                if (mCasaEmpresa.Length > 15)
                {
                    retorna.exito = false;
                    retorna.mensaje = "La casa de la dirección del cliente no puede ser mayor a 15 caracteres.";
                    return retorna;
                }
                if (mZonaEmpresa.Length > 2)
                {
                    retorna.exito = false;
                    retorna.mensaje = "La zona de la casa del lugar de trabajo del cliente es inválida, por favor vaya a la ficha del cliente y actualice la dirección.";
                    return retorna;
                }
                if (mColoniaEmpresa.Length > 60)
                {
                    retorna.exito = false;
                    retorna.mensaje = "La colonia de la dirección del cliente no puede ser mayor a 60 caracteres.";
                    return retorna;
                }
                if (mApartamentoEmpresa.Length > 15)
                {
                    retorna.exito = false;
                    retorna.mensaje = "El apartamento de la dirección del cliente no puede ser mayor a 15 caracteres.";
                    return retorna;
                }
                if (mTelefonoEmpresa.Length > 8)
                {
                    retorna.exito = false;
                    retorna.mensaje = "El teléfono de la empresa del cliente no puede ser mayor a 8 dígitos, sin espacios ni guiones.";
                    return retorna;
                }


                string mPruebas = "";
                if (mAmbiente == "DES" || mAmbiente == "PRU")
                {
                    Random r = new Random();
                    int mValorPruebas = r.Next(1, 6);

                    //mPruebas = string.Format("<pruebas>{0}</pruebas>", mValorPruebas.ToString());
                }

                log.Info("Generación de XML para Interconsumo.");

                string xmlData = "<solicitud>";
                xmlData += mPruebas;
                xmlData += "<autenticacion>";
                xmlData += "<aplicacion>Muebles Fiesta</aplicacion>";
                xmlData += string.Format("<usuario><![CDATA[{0}]]></usuario>", WebConfigurationManager.AppSettings["UsrInterconsumo"]);
                xmlData += string.Format("<clave><![CDATA[{0}]]></clave>", WebConfigurationManager.AppSettings["PwdInterconsumo"]);
                xmlData += "</autenticacion>";
                xmlData += "<cliente>";
                xmlData += string.Format("<dpi>{0}</dpi>", mDpi);
                xmlData += string.Format("<primer_nombre>{0}</primer_nombre>", mPrimerNombre);
                xmlData += string.Format("<segundo_nombre>{0}</segundo_nombre>", mSegundoNombre);
                xmlData += string.Format("<tercer_nombre>{0}</tercer_nombre>", mTercerNombre);
                xmlData += string.Format("<primer_apellido>{0}</primer_apellido>", mPrimerApellido);
                xmlData += string.Format("<segundo_apellido>{0}</segundo_apellido>", mSegundoApellido);
                xmlData += string.Format("<apellido_casada>{0}</apellido_casada>", mApellidoCasada);
                xmlData += string.Format("<fecha_nacimiento>{0}/{1}{2}/{3}{4}</fecha_nacimiento>", mFechaNacimiento.Year, mFechaNacimiento.Month < 10 ? "0" : "", mFechaNacimiento.Month, mFechaNacimiento.Day < 10 ? "0" : "", mFechaNacimiento.Day);
                xmlData += string.Format("<genero>{0}</genero>", mGenero);
                xmlData += string.Format("<estado_civil>{0}</estado_civil>", mEstadoCivil.ToString());
                xmlData += string.Format("<nit>{0}</nit>", mNit);
                xmlData += "<nacionalidad>GT</nacionalidad>";
                xmlData += string.Format("<idtipovivienda>{0}</idtipovivienda>", mVivienda);
                xmlData += string.Format("<idprofesion>{0}</idprofesion>", mProfesion);
                xmlData += string.Format("<tel_movil>{0}</tel_movil>", mCelular);
                xmlData += string.Format("<ingresos>{0}</ingresos>", mIngresos);
                xmlData += string.Format("<egresos>{0}</egresos>", mEgresos);
                xmlData += string.Format("<cargafamiliar>{0}</cargafamiliar>", mCargasFamiliares);
                xmlData += string.Format("<email><![CDATA[{0}]]></email>", mEmail);
                xmlData += "<domicilio>";
                xmlData += string.Format("<calle><![CDATA[{0}]]></calle>", mCalle);
                xmlData += string.Format("<numero><![CDATA[{0}]]></numero>", mCasa);
                xmlData += string.Format("<zona>{0}</zona>", mZona);
                xmlData += string.Format("<colonia><![CDATA[{0}]]></colonia>", mColonia);
                xmlData += string.Format("<apto_casa>{0}</apto_casa>", mApartamento);
                xmlData += "<edificio></edificio>";
                xmlData += "<nivel></nivel>";
                xmlData += string.Format("<idmuni>{0}</idmuni>", mMunicipio);
                xmlData += string.Format("<iddepto>{0}</iddepto>", mDepartamento);
                xmlData += "<contador></contador>";
                xmlData += "<idempresaservicio></idempresaservicio>";
                xmlData += string.Format("<telefono>{0}{1}{2}</telefono>", mTelefono1.Trim(), mTelefono2.Trim().Length == 0 ? "" : " ", mTelefono2.Trim());
                xmlData += "</domicilio>";
                xmlData += "<trabajo>";
                xmlData += string.Format("<empresa><![CDATA[{0}]]></empresa>", mEmpresa);
                xmlData += "<departamento></departamento>";
                xmlData += string.Format("<idpuesto>{0}</idpuesto>", mPuesto);
                xmlData += string.Format("<fecha_ingreso>{0}/{1}{2}/{3}{4}</fecha_ingreso>", mFechaIngreso.Year, mFechaIngreso.Month < 10 ? "0" : "", mFechaIngreso.Month, mFechaIngreso.Day < 10 ? "0" : "", mFechaIngreso.Day);
                xmlData += "<direccion>";
                xmlData += string.Format("<calle><![CDATA[{0}]]></calle>", mCalleEmpresa);
                xmlData += string.Format("<numero>{0}</numero>", mCasaEmpresa);
                xmlData += string.Format("<zona>{0}</zona>", mZonaEmpresa);
                xmlData += string.Format("<colonia>{0}</colonia>", mColoniaEmpresa);
                xmlData += string.Format("<apto_casa>{0}</apto_casa>", mApartamentoEmpresa);
                xmlData += "<edificio></edificio>";
                xmlData += "<nivel></nivel>";
                xmlData += "<oficina></oficina>";
                xmlData += string.Format("<idmuni>{0}</idmuni>", mMunicipioEmpresa);
                xmlData += string.Format("<iddepto>{0}</iddepto>", mDepartamentoEmpresa);
                xmlData += string.Format("<telefono>{0}</telefono>", mTelefonoEmpresa);
                xmlData += "</direccion>";
                xmlData += "</trabajo>";
                xmlData += string.Format("<codigo_externo>{0}</codigo_externo>", mCliente);
                xmlData += "</cliente>";
                xmlData += "<referencias>";
                xmlData += "<bancarias>";
                xmlData += string.Format("<idbanco>{0}</idbanco>", mIdBanco);
                xmlData += string.Format("<banco>{0}</banco>", mBanco);
                xmlData += string.Format("<cuenta>{0}</cuenta>", mCuenta);
                xmlData += "<idtipocuenta></idtipocuenta>";
                xmlData += "</bancarias>";
                xmlData += "<personal1>";
                xmlData += string.Format("<nombre>{0}</nombre>", mPersonalNombre1);
                xmlData += "<genero></genero>";
                xmlData += string.Format("<telresidencia>{0}</telresidencia>", mPersonalResidencia1);
                xmlData += string.Format("<telmovil>{0}</telmovil>", mPersonalMovil1);
                xmlData += string.Format("<teltrabajo>{0}</teltrabajo>", mPersonalTrabajo1);
                xmlData += "<exttrabajo></exttrabajo>";
                xmlData += "<parentezco></parentezco>";
                xmlData += "</personal1>";
                xmlData += "<personal2>";
                xmlData += string.Format("<nombre>{0}</nombre>", mPersonalNombre2);
                xmlData += "<genero></genero>";
                xmlData += string.Format("<telresidencia>{0}</telresidencia>", mPersonalResidencia2);
                xmlData += string.Format("<telmovil>{0}</telmovil>", mPersonalMovil2);
                xmlData += string.Format("<teltrabajo>{0}</teltrabajo>", mPersonalTrabajo2);
                xmlData += "<exttrabajo></exttrabajo>";
                xmlData += "<parentezco></parentezco>";
                xmlData += "</personal2>";
                xmlData += "<comercial1>";
                xmlData += string.Format("<nombre>{0}</nombre>", mComercialNombre1);
                xmlData += string.Format("<telefono>{0}</telefono>", mComercialTelefono1);
                xmlData += "<extension></extension>";
                xmlData += "</comercial1>";
                xmlData += "<comercial2>";
                xmlData += string.Format("<nombre>{0}</nombre>", mComercialNombre2);
                xmlData += string.Format("<telefono>{0}</telefono>", mComercialTelefono2);
                xmlData += "<extension></extension>";
                xmlData += "</comercial2>";
                xmlData += "</referencias>";
                xmlData += "<compra>";
                xmlData += string.Format("<precio>{0}</precio>", mPrecioBien.ToString().Replace("000000", ""));
                xmlData += string.Format("<enganche>{0}</enganche>", mEnganche.ToString().Replace("000000", ""));
                xmlData += string.Format("<monto_financiar>{0}</monto_financiar>", mSaldoFinanciar.ToString().Replace("000000", ""));
                xmlData += string.Format("<plazo>{0}</plazo>", mPlazo);
                xmlData += string.Format("<cuota>{0}</cuota>", mCuota.ToString().Replace("000000", ""));
                xmlData += string.Format("<ultima_cuota>{0}</ultima_cuota>", mUltimaCuota.ToString().Replace("000000", ""));
                xmlData += string.Format("<idusuario>{0}</idusuario>", mVendedorInterconsumo);
                xmlData += string.Format("<idtienda>{0}</idtienda>", mTienda);
                xmlData += string.Format("<idvendedor>{0}</idvendedor>", mVendedorInterconsumo);
                xmlData += string.Format("<tel_vendedor>{0}</tel_vendedor>", mTelefonoTienda);
                xmlData += string.Format("<idproducto>{0}</idproducto>", mIdProducto);
                xmlData += "<idtasa></idtasa>";
                xmlData += "<desc_destino></desc_destino>";
                xmlData += "</compra>";
                xmlData += "</solicitud>";

                log.Info("Consumo de WS de Interconsumo.");
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                FiestaNETRestServices.srvIntegracionInterConsumo.srvIntegracionInteconsumoPuente srvIntegra = new FiestaNETRestServices.srvIntegracionInterConsumo.srvIntegracionInteconsumoPuente();
                InterEncript.Encriptacion dllEncriptacion = new InterEncript.Encriptacion();

                string resultado = dllEncriptacion.Proceso(xmlData, "solicitud", "1", "xml");
                string respuesta = srvIntegra.EntradaXml(resultado);
                resultado = dllEncriptacion.Proceso(respuesta.Trim(), "", "2", "1");

                XmlDocument xmlRespuesta = new XmlDocument();

                xmlRespuesta.PreserveWhitespace = true;
                xmlRespuesta.LoadXml(resultado);

                log.Info("Lectura de respuesta devuelta por Interconsumo.");

                string mCodigoRespuesta = xmlRespuesta.SelectNodes("solicitud/respuesta/codigo").Item(0).InnerText;
                string mDetalleRespuesta = xmlRespuesta.SelectNodes("solicitud/respuesta/descripcion").Item(0).InnerText;
                retorna.solicitud = xmlRespuesta.SelectNodes("solicitud/respuesta/solicitud").Item(0).InnerText;
                retorna.resultadoSolicitud = xmlRespuesta.SelectNodes("solicitud/respuesta/resultado").Item(0).InnerText;

                try
                {
                    retorna.autorizacion = xmlRespuesta.SelectNodes("solicitud/respuesta/autorizacion").Item(0).InnerText;
                }
                catch
                {
                    retorna.autorizacion = "";
                }

                retorna.quienAutorizo = "";
                if (retorna.autorizacion.Trim().Length > 0) retorna.quienAutorizo = "Autorización en línea";

                string mAutorizacionEnLinea = "N";

                string xmlSolicitud = "<solicitud>";
                xmlSolicitud += "<autenticacion>";
                xmlSolicitud += "<aplicacion>Muebles Fiesta</aplicacion>";
                xmlSolicitud += string.Format("<usuario><![CDATA[{0}]]></usuario>", WebConfigurationManager.AppSettings["UsrInterconsumo"]);
                xmlSolicitud += string.Format("<clave><![CDATA[{0}]]></clave>", WebConfigurationManager.AppSettings["PwdInterconsumo"]);

                xmlSolicitud += "</autenticacion>";
                xmlSolicitud += string.Format("<referencia>{0}</referencia>", retorna.solicitud);
                xmlSolicitud += "</solicitud>";

                string xmlSolicitudEncriptado = dllEncriptacion.Proceso(xmlSolicitud, "solicitud", "1", "xml").Replace("+", "-").Replace("/", "_").Replace("=", ",");
                string mLink = string.Format("https://www.interbanking.com.gt/interconsumo.portalweb/WEBSolicitud_lecturaDPI.aspx?p={0}", xmlSolicitudEncriptado);

                if (xmlData.Length >= 8000) xmlData = xmlData.Substring(0, 7998);
                if (mCodigoRespuesta.Length >= 100) mCodigoRespuesta = mCodigoRespuesta.Substring(0, 98);
                if (mDetalleRespuesta.Length >= 800) mDetalleRespuesta = mDetalleRespuesta.Substring(0, 798);
                if (retorna.resultadoSolicitud.Length >= 800) retorna.resultadoSolicitud = retorna.resultadoSolicitud.Substring(0, 798);
                if (resultado.Length >= 8000) resultado = resultado.Substring(0, 7998);

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    if (tipo == "C")
                    {
                        var qUpdateCotizacion = from c in db.MF_Cotizacion where c.COTIZACION == mCotizacion select c;
                        foreach (var item in qUpdateCotizacion)
                        {
                            if (retorna.solicitud.Trim().Length > 0)
                            {
                                item.SOLICITUD = Convert.ToInt64(retorna.solicitud);
                                item.PAGARE = Convert.ToInt64(retorna.solicitud);
                            }

                            item.NOMBRE_AUTORIZACION = retorna.quienAutorizo;
                            item.AUTORIZACION_INTERCONSUMO = retorna.autorizacion;
                            item.AUTORIZACION_EN_LINEA = mAutorizacionEnLinea;
                            item.GRABADA_EN_LINEA = "S";
                            item.XML_INTERCONSUMO = xmlData;
                            item.CODIGO_RESPUESTA = mCodigoRespuesta;
                            item.DESCRIPCION_RESPUESTA = mDetalleRespuesta;
                            item.RESULTADO_RESPUESTA = retorna.resultadoSolicitud;
                            item.XML_RESPUESTA = resultado;
                            item.FECHA_AUTORIZACION = DateTime.Now.Date;
                            item.FECHA_HORA_AUTORIZACION = DateTime.Now;
                            item.LINK = mLink;
                        }
                    }
                    else
                    {
                        var qUpdateMFPedido = from p in db.MF_Pedido where p.PEDIDO == documento select p;
                        foreach (var item in qUpdateMFPedido)
                        {
                            if (retorna.solicitud.Trim().Length > 0)
                            {
                                item.SOLICITUD = Convert.ToInt64(retorna.solicitud);
                                item.PAGARE = Convert.ToInt64(retorna.solicitud);
                            }

                            item.NOMBRE_AUTORIZACION = retorna.quienAutorizo;
                            item.AUTORIZACION = retorna.autorizacion;
                            item.AUTORIZACION_EN_LINEA = mAutorizacionEnLinea;
                            item.GRABADA_EN_LINEA = "S";
                            item.XML_INTERCONSUMO = xmlData;
                            item.CODIGO_RESPUESTA = mCodigoRespuesta;
                            item.DESCRIPCION_RESPUESTA = mDetalleRespuesta;
                            item.RESULTADO_RESPUESTA = retorna.resultadoSolicitud;
                            item.XML_RESPUESTA = resultado;
                            item.FECHA_AUTORIZACION = DateTime.Now.Date;
                            item.FECHA_HORA_AUTORIZACION = DateTime.Now;
                            item.LINK = mLink;
                        }
                    }

                    db.SaveChanges();
                    transactionScope.Complete();
                }

                log.Info("Grabación de datos devueltos por Interconsumo finalizada.");

                if (mCodigoRespuesta == "000")
                {
                    retorna.mensaje = string.Format("Solicitud {0} grabada exitosamente en Interconsumo.", retorna.solicitud);
                }
                else
                {
                    string mTipo1 = "Pedido"; string mTipo2 = "pedido"; string mVendedorBuscar = "";

                    if (tipo == "C")
                    {
                        mTipo1 = "Cotización";
                        mTipo2 = "cotización";

                        mVendedorBuscar = (from c in db.MF_Cotizacion where c.COTIZACION == mCotizacion select c).First().VENDEDOR;
                    }
                    else
                    {
                        mVendedorBuscar = (from p in db.PEDIDO where p.PEDIDO1 == documento select p).First().VENDEDOR;
                    }

                    if (mEmailVendedor.Trim().Length == 0)
                    {
                        mEmailVendedor = (from v in db.MF_Vendedor where v.VENDEDOR == mVendedorBuscar select v).First().EMAIL;
                        mNombreVendedor = (from v in db.VENDEDOR where v.VENDEDOR1 == mVendedorBuscar select v).First().NOMBRE;
                    }
                    
                    try
                    {
                        StringBuilder mBody = new StringBuilder();

                        mBody.Append("<!DOCTYPE html><html lang='en-us'>");
                        mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } .style1 { color: #8C4510; background-color: #FFF7E7; font-size: xx-small; text-align: left; height:35px; } .style2 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; height:35px; } .style3 { color: #8C4510; background-color: #FFF7E7; font-size: x-small; text-align: left; height:35px; } .style4 { border: 1px solid #8C4510; } .style5 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; text-align: left; height:35px; } </style></head>");
                        mBody.Append("<body>");
                        mBody.Append("<table align='center'>");
                        //mBody.Append(string.Format("<tr><td class='style5'><b>{0}:&nbsp;&nbsp;</b></td><td class='style3'><b>&nbsp;&nbsp;{1}&nbsp;&nbsp;{2}&nbsp;&nbsp;</b></td></tr>", mTipo1, documento, mAmbiente == "PRO" ? "" : "-&nbsp;&nbsp;Estoy utilizando el sistema de PRUEBAS"));
                        mBody.Append(string.Format("<tr><td class='style5'><b>{0}:&nbsp;&nbsp;</b></td><td class='style3'><b>&nbsp;&nbsp;{1}&nbsp;&nbsp;{2}&nbsp;&nbsp;</b></td></tr>", mTipo1, documento, ""));
                        mBody.Append(string.Format("<tr><td class='style5'>Vendedor:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0} - {1}&nbsp;&nbsp;</td></tr>", mNombreVendedor, mTienda));
                        mBody.Append(string.Format("<tr><td class='style5'>Cliente:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0} - {1}&nbsp;&nbsp;</td></tr>", mCliente, mNombreCliente));
                        mBody.Append(string.Format("<tr><td class='style5'>Fecha y hora:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0}{1}/{2}{3}/{4} {5}&nbsp;&nbsp;</td></tr>", DateTime.Now.Date.Day < 10 ? "0" : "", DateTime.Now.Date.Day.ToString(), DateTime.Now.Date.Month < 10 ? "0" : "", DateTime.Now.Date.Month.ToString(), DateTime.Now.Date.Year.ToString(), DateTime.Now.ToLongTimeString()));
                        mBody.Append(string.Format("<tr><td class='style5'>Descripci&#243;n:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;Interconsumo devolvió {0}{1}&nbsp;&nbsp;</td></tr>", mDetalleRespuesta, mInfoError));
                        mBody.Append(string.Format("<tr><td class='style5'>Error:&nbsp;&nbsp;</td><td class='style3'>&nbsp;&nbsp;{0} - Este es un envío automatico del sistema&nbsp;&nbsp;</td></tr>", mCodigoRespuesta));
                        mBody.Append("</table>");
                        mBody.Append("</body>");
                        mBody.Append("</html>");

                        //SmtpClient smtp = new SmtpClient();
                        //MailMessage mail = new MailMessage();

                        List<string> CuentaCorreo = new List<string>();
                        var qUsuarios = from u in db.MF_UsuarioIT where u.ACTIVO == "S" orderby u.NOMBRE select u;
                        foreach (var item in qUsuarios)
                        {
                            CuentaCorreo.Add(item.EMAIL);
                        }

						//mail.IsBodyHtml = true;
						//mail.ReplyToList.Add(mEmailVendedor);
						//mail.From = new MailAddress(mEmailVendedor, mNombreVendedor);
						////mail.From = new MailAddress("puntodeventa@mueblesfiesta.com", mNombreVendedor);
						//mail.Subject = string.Format("Soporte para {0} - {1} para autorizar {2} en línea", mNombreVendedor, mTienda, mTipo2);

						//mail.Body = mBody.ToString();

						//smtp.Host = WebConfigurationManager.AppSettings["MailHost"];
						//smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort"]);
						//smtp.EnableSsl = true;
						//smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

						//try
						//{
						//    smtp.Credentials = new System.Net.NetworkCredential("puntodeventa2@productosmultiples.com", "Estrella01");
						//    //smtp.Credentials = new System.Net.NetworkCredential("puntodeventa", "jh$%Pjma");
						//    smtp.Send(mail);
						//}
						//catch
						//{
						//    //Nothing
						//}

						Utils.Utilitarios Correo = new Utils.Utilitarios();
                        Correo.EnviarEmail(mEmailVendedor, CuentaCorreo, string.Format("Soporte para {0} - {1} para autorizar {2} en línea", mNombreVendedor, mTienda, mTipo2), mBody, mNombreVendedor);
                    }
                    catch
                    {
                        //Nothing
                    }

                    retorna.exito = false;
                    retorna.mensaje = string.Format("Interconsumo devolvió el error {0} {3}. Enviar en soporte el número de {1} para darle seguimiento al error en IT o bien intente usted corregir los datos del cliente.{2}", mCodigoRespuesta, mTipo2, mInfoError, mDetalleRespuesta);
                }
            }
            catch (Exception ex)
            {
                string mMensajeError = CatchClass.ExMessage(ex, "PreAutorizacionInterconsumoController", "GetRespuestaSolicitud");
                log.Error(mMensajeError);

                retorna.exito = false;
                retorna.mensaje = mMensajeError;
            }

            return retorna;
        }

        bool ValidaDatos(string tipo, string documento)
        {
            EXACTUSERPEntities db = new EXACTUSERPEntities();

            bool mRetorna = true;
            string mCliente = ""; Int32 mFinanciera = 0;

            Int32 mCotizacion = 0;
            if (tipo == "C") mCotizacion = Convert.ToInt32(documento);

            if (tipo == "C")
            {
                var q = from c in db.MF_Cotizacion where c.COTIZACION == mCotizacion select c;
                foreach (var item in q)
                {
                    if (item.CLIENTE == Convert.ToString(null)) return false;
                    mCliente = item.CLIENTE;
                    mFinanciera = Convert.ToInt32(item.FINANCIERA);
                }
            }
            else
            {
                var q = from p in db.MF_Pedido where p.PEDIDO == documento select p;
                foreach (var item in q)
                {
                    mCliente = item.CLIENTE;
                    mFinanciera = Convert.ToInt32(item.FINANCIERA);
                }
            }
            
            if (mFinanciera == 3 || mFinanciera == 7 || mFinanciera == 10)
            {
                string mProfesion = ""; string mReferenciaBancaria = ""; string mBanco = "";

                string mRefPersonalNombre1 = "";
                string mRefPersonalTelResidencia1 = "";
                string mRefPersonalTelTrabajo1 = "";
                string mRefPersonalCelular1 = "";
                string mRefPersonalNombre2 = "";
                string mRefPersonalTelResidencia2 = "";
                string mRefPersonalTelTrabajo2 = "";
                string mRefPersonalCelular2 = "";
                string mRefComercialNombre1 = "";
                string mRefComercialTelefono1 = "";
                string mRefComercialFax1 = "";
                string mRefComercialCelular1 = "";
                string mRefComercialNombre2 = "";
                string mRefComercialTelefono2 = "";
                string mRefComercialFax2 = "";
                string mRefComercialCelular2 = "";
                string mCelular = "";
                string mIngresos = "";
                string mGastos = "";
                string mPuesto = "";
                string mCalleEmpresa = "";
                Int64 mDpi = 0;

                var q = from c in db.MF_Cliente where c.CLIENTE == mCliente select c;
                foreach (var item in q)
                {
                    mProfesion = item.PROFESION;
                    mBanco = item.BANCO;
                    mReferenciaBancaria = item.REFERENCIAS_BANCARIAS;

                    mRefPersonalNombre1 = item.REF_PERSONAL_NOMBRE1;
                    mRefPersonalTelResidencia1 = item.REF_PERSONAL_TEL_RESIDENCIA1;
                    mRefPersonalTelTrabajo1 = item.REF_PERSONAL_TEL_TRABAJO1;
                    mRefPersonalCelular1 = item.REF_PERSONAL_CELULAR1;
                    mRefPersonalNombre2 = item.REF_PERSONAL_NOMBRE2;
                    mRefPersonalTelResidencia2 = item.REF_PERSONAL_TEL_RESIDENCIA2;
                    mRefPersonalTelTrabajo2 = item.REF_PERSONAL_TEL_TRABAJO2;
                    mRefPersonalCelular2 = item.REF_PERSONAL_CELULAR2;
                    mRefComercialNombre1 = item.REF_COMERCIAL_NOMBRE1;
                    mRefComercialTelefono1 = item.REF_COMERCIAL_TELEFONO1;
                    mRefComercialFax1 = item.REF_COMERCIAL_FAX1;
                    mRefComercialCelular1 = item.REF_COMERCIAL_CELULAR1;
                    mRefComercialNombre2 = item.REF_COMERCIAL_NOMBRE2;
                    mRefComercialTelefono2 = item.REF_COMERCIAL_TELEFONO2;
                    mRefComercialFax2 = item.REF_COMERCIAL_FAX2;
                    mRefComercialCelular2 = item.REF_COMERCIAL_CELULAR2;

                    mCelular = item.CELULAR;
                    mCalleEmpresa = item.CALLE_EMPRESA;

                    var qCliente = (from cl in db.CLIENTE where cl.CLIENTE1 == mCliente select cl).First();

                    mIngresos = qCliente.RUBRO11_CLIENTE;
                    mGastos = qCliente.RUBRO12_CLIENTE;
                    mPuesto = qCliente.RUBRO4_CLIENTE;

                    int mAnio = 0;

                    try
                    {
                        mAnio = Convert.ToInt32(Convert.ToDateTime(item.FECHA_INGRESO_LABORES).Year);
                    }
                    catch
                    {
                        return false;
                    }

                    if (mAnio <= 0) return false;

                    try
                    {
                        mDpi = Convert.ToInt64(item.DPI);
                    }
                    catch
                    {
                        mDpi = 0;
                    }
                }

                if (mDpi <= 0) mRetorna = false;
                if (mDpi.ToString().Length > 13) mRetorna = false;
                if (mProfesion.Trim().Length == 0) mRetorna = false;
                if (mPuesto.Trim().Length == 0) mRetorna = false;
                if (mCalleEmpresa.Trim().Length == 0) mRetorna = false;

                if (mReferenciaBancaria.Trim().Length > 0)
                {
                    Int64 mCuenta = 0;

                    try
                    {
                        mCuenta = Convert.ToInt64(mReferenciaBancaria.Trim());
                    }
                    catch
                    {
                        return false;
                    }

                    if (mCuenta <= 0 || mCuenta.ToString().Length < 7) return false;
                }

                if ((mCelular.Trim().Length > 0 && mCelular.Trim().Length < 8) || mCelular.Contains("-")) return false;

                if (mGastos == null) return false;
                if (mIngresos == null) return false;

                if (mGastos.Trim().Length == 0) return false;
                if (mIngresos.Trim().Length == 0) return false;

                mIngresos = mIngresos.Trim().Replace(" ", "").Replace(",", "").Replace("Q", "").Replace("q", "").Replace("$", "");
                mGastos = mGastos.Trim().Replace(" ", "").Replace(",", "").Replace("Q", "").Replace("q", "").Replace("$", "");

                decimal mIngresosMes = 0;
                decimal mGastosMes = 0;

                try
                {
                    mIngresosMes = Convert.ToDecimal(mIngresos);
                }
                catch
                {
                    return false;
                }

                if (mIngresosMes <= 0) return false;

                try
                {
                    mGastosMes = Convert.ToDecimal(mGastos);
                }
                catch
                {
                    return false;
                }

                if (mGastosMes <= 0) return false;


                if ((mRefPersonalNombre1.Trim().Length > 0 && mRefPersonalNombre1.Trim().Length < 8) || mRefPersonalNombre1.Contains("-")) return false;
                if ((mRefPersonalTelResidencia1.Trim().Length > 0 && mRefPersonalTelResidencia1.Trim().Length < 8) || mRefPersonalTelResidencia1.Contains("-")) return false;
                if ((mRefPersonalTelTrabajo1.Trim().Length > 0 && mRefPersonalTelTrabajo1.Trim().Length < 8) || mRefPersonalTelTrabajo1.Contains("-")) return false;
                if ((mRefPersonalCelular1.Trim().Length > 0 && mRefPersonalCelular1.Trim().Length < 8) || mRefPersonalCelular1.Contains("-")) return false;
                if ((mRefPersonalNombre2.Trim().Length > 0 && mRefPersonalNombre2.Trim().Length < 8) || mRefPersonalNombre2.Contains("-")) return false;
                if ((mRefPersonalTelResidencia2.Trim().Length > 0 && mRefPersonalTelResidencia2.Trim().Length < 8) || mRefPersonalTelResidencia2.Contains("-")) return false;
                if ((mRefPersonalTelTrabajo2.Trim().Length > 0 && mRefPersonalTelTrabajo2.Trim().Length < 8) || mRefPersonalTelTrabajo2.Contains("-")) return false;
                if ((mRefPersonalCelular2.Trim().Length > 0 && mRefPersonalCelular2.Trim().Length < 8) || mRefPersonalCelular2.Contains("-")) return false;
                if ((mRefComercialNombre1.Trim().Length > 0 && mRefComercialNombre1.Trim().Length < 8) || mRefComercialNombre1.Contains("-")) return false;
                if ((mRefComercialTelefono1.Trim().Length > 0 && mRefComercialTelefono1.Trim().Length < 8) || mRefComercialTelefono1.Contains("-")) return false;
                if ((mRefComercialFax1.Trim().Length > 0 && mRefComercialFax1.Trim().Length < 8) || mRefComercialFax1.Contains("-")) return false;
                if ((mRefComercialCelular1.Trim().Length > 0 && mRefComercialCelular1.Trim().Length < 8) || mRefComercialCelular1.Contains("-")) return false;
                if ((mRefComercialNombre2.Trim().Length > 0 && mRefComercialNombre2.Trim().Length < 8) || mRefComercialNombre2.Contains("-")) return false;
                if ((mRefComercialTelefono2.Trim().Length > 0 && mRefComercialTelefono2.Trim().Length < 8) || mRefComercialTelefono2.Contains("-")) return false;
                if ((mRefComercialFax2.Trim().Length > 0 && mRefComercialFax2.Trim().Length < 8) || mRefComercialFax2.Contains("-")) return false;
                if ((mRefComercialCelular2.Trim().Length > 0 && mRefComercialCelular2.Trim().Length < 8) || mRefComercialCelular2.Contains("-")) return false;

            }

            return mRetorna;
        }
    }
}
