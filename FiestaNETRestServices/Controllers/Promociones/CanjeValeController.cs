﻿using FiestaNETRestServices.DAL;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Web.Http;
using System.Web.Mvc;

namespace FiestaNETRestServices.Controllers.Promociones
{
    public class CanjeValeController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [System.Web.Http.HttpPost]
        public IHttpActionResult Post([FromBody] List<CanjeVale> vales)
        {
            Respuesta mResp = new Respuesta();
            Clases.RetornaExito resp = new Clases.RetornaExito();
            resp.exito = true;
            ValidaValeController valida = new ValidaValeController();
            vales.ForEach(x =>
              {
                  if(resp.exito)
                  resp = valida.Get(x.Codigo);
                  
               }
            ); 
            
            if (resp.exito)
            {
                DALVale objVale = new DALVale();
                mResp = objVale.CanjearVale(vales);

            }
            else
                return BadRequest(resp.mensaje);

            return Ok(mResp);
        }
    }
}
