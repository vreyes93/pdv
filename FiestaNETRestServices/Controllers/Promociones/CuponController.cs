﻿using MF_Clases;
using MF_Clases.Comun;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Promociones
{
    public class CuponController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            DAL.DALCupon cupon = new DAL.DALCupon();
            var respuesta = cupon.ObtenerCuponCrediplus(id);
            if (respuesta.Exito)
                return Ok(respuesta.Objeto);
            else
                return BadRequest();
        }
        [HttpPost]
        [Route("Cupon/EschangeInfo")]
        public ExchangeInfoResult ChangeStatus(ExchangeInfoApiRequest data)
        {
            DAL.DALCupon cupon = new DAL.DALCupon();
            //switch (data.method)
            //    {
            //    case "ExchangeStatus":
            //        return cupon.checkStatus(data.CouponCode, data.advertisingCompany);

            //    case "Exchange":
                        return cupon.changeStatus(data.CouponCode, data.exchangeReferente, data.invoiceAmount, data.invoiceNumber, data.advertisingCompany);
            //   default:
            //        return cupon.checkStatus(data.CouponCode, data.advertisingCompany);
            //}
            
        }
    }
}
