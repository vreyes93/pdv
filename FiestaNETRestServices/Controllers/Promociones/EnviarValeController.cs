﻿using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Promociones
{
    [RoutePrefix("api/EnviarVale")]
    public class EnviarValeController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [HttpPost]
        public IHttpActionResult Post([FromBody] MF_Clases.Correo email)
        {
           
            Utils.Utilitarios utilitarios = new Utils.Utilitarios();
            try
            {
                log.Debug("Enviando correo  de " + email.Asunto);
                List<ImagenAdjunta> lstImagenes = new List<ImagenAdjunta>();
                lstImagenes.Add(new MF_Clases.ImagenAdjunta(System.Web.Hosting.HostingEnvironment.MapPath("~//Images/email_alert.png"), MediaTypeNames.Image.Jpeg, "EmailAlert"));
                var Respuesta = utilitarios.EnviarEmail(email.Remitente, email.Destinatarios, email.Asunto, email.CuerpoCorreo, email.NombreAMostrar, email.lstImagenes, email.ConCopiaOcultaA);
            }
            catch (Exception ex)
            {
                log.Error("Ocurrió un error al intentar enviar el correo", ex);
                return BadRequest();
            }
            return Ok();
        
    }
    }
}
