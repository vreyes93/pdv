﻿using FiestaNETRestServices.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Promociones
{
    [RoutePrefix("api/IPVale")]
    public class IPValeController : ApiController
    {
        [HttpGet]
        [Route("~Get/Ip={Ip}")]
        public IHttpActionResult Get(string Ip)
        {
            DALVale objVale = new DALVale();
            return Ok(objVale.BuscarValeporIP(Ip));
        }

        //public IHttpActionResult GrabarIpVale(string Ip,string CodVale)
        //{

        //}
    }
}
