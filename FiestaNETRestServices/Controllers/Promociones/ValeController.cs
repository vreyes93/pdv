﻿using FiestaNETRestServices.DAL;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Promociones
{
    [RoutePrefix("api/Vale")]
    public class ValeController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Servicio para obtener la información de un vale previamente grabado
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Get(string Id)
        {
            log.Info("Obteniendo información del vale "+Id);
            Respuesta mRespuesta = new Respuesta();
            DALVale objVale = new DALVale();
            mRespuesta = objVale.ObtenerInfoValeDeDescuento(Id);
            if (mRespuesta.Exito)
                return Ok(mRespuesta.Objeto);
            else
                return BadRequest();
        }

        /// <summary>
        /// Servicio para grabar un vale
        /// </summary>
        /// <param name="vale"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult Post([FromBody] Vale vale)
        {
            //verficar que no se haya granado el vale
            //grabar el vale
            log.Info("Grabando información del vale "+vale.Codigo+", de "+vale.Nombres+" "+vale.Apellidos);
            Respuesta mRespuesta = new Respuesta();
            DALVale objVale = new DALVale();
            if (!objVale.ExisteVale(vale.Codigo))
            {
                mRespuesta = objVale.GrabarValeDeDescuento(vale);
                if (mRespuesta.Exito)
                {
                    log.Info("Vale grabado con éxito");
                    return Ok();
                }
                else
                {
                    log.Info("Error al grabar el vale "+mRespuesta.Mensaje);
                    return BadRequest(mRespuesta.Mensaje);
                }
            }
            else
                return BadRequest("¡El vale ya existe!");
        }

        
       
    }
}
