﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Data;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;
using MF_Clases;

namespace FiestaNETRestServices.Controllers.Promociones
{
    public class ValidaValeController : ApiController
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("~/ValidaVale/{id}")]// 
        [HttpGet]
        public Clases.RetornaExito Get(string id)
        {
            Clases.RetornaExito mRetorna = new Clases.RetornaExito();
            
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            db.CommandTimeout = 999999999;
            
            mRetorna.exito = false;
            
            try
            {
                if (id == null)
                {
                    mRetorna.mensaje = "Debe ingresar el número de vale.";
                    return mRetorna;
                }
                if (id.Trim().Length == 0)
                {
                    mRetorna.mensaje = "Debe ingresar el número de vale.";
                    return mRetorna;
                }
                
                var qVale = from v in db.MF_Vale where v.VALE == id select v;

                if (qVale.Count() == 0)
                {
                    mRetorna.mensaje = "No se encontró el número de vale.";
                    return mRetorna;
                }
                
                if (qVale.First().FACTURADO == "S")
                {
                    bool mProcede = false;
                    string mFactura = qVale.First().FACTURA;

                    var qValeAnulado = from v in db.MF_ValeFacturaAnulada where v.VALE == id select v;

                    if (qValeAnulado.Count() > 0)
                    {
                        string mFacturaAnulada = qValeAnulado.First().FACTURA;
                        if ((from f in db.FACTURA where f.FACTURA1 == mFacturaAnulada select f).First().ANULADA == "S") 
                            mProcede = true;
                    }

                    if (!mProcede)
                    {
                        mRetorna.mensaje = string.Format("El vale {0} ya fue utilizado en la factura {1}.", id, mFactura);
                        return mRetorna;
                    }
                }

                mRetorna.exito = true;
                mRetorna.mensaje = String.Format("{0:0}", qVale.FirstOrDefault().VALOR);
            }
            catch (Exception ex)
            {
                mRetorna.exito = false;
                mRetorna.mensaje = CatchClass.ExMessage(ex, "ValidaValePrensaLibreController", "GetValidaValePrensaLibre");
            }

            return mRetorna;
        }

    }
}
