﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using FiestaNETRestServices.Content.Abstract;
using Newtonsoft.Json;
using MF_Clases;
using System.Web.Configuration;

namespace FiestaNETRestServices.Controllers.Reportes
{
    public class CumpleanierosController : MFApiController
    {
        
        // GET api/cumpleanieros/5
        public string GetCumpleanieros(int id = 1)
        {
            string mRetorna = "";
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            db.CommandTimeout = 999999999;
            
            try
            {
                int mSumar = 1;
                if (DateTime.Now.Date.Day <= 5) mSumar = 0;
                DateTime mFechaInicial = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1); DateTime mFechaFinal;

                mFechaInicial = mFechaInicial.AddMonths(mSumar);
                mFechaInicial = new DateTime(mFechaInicial.Year, mFechaInicial.Month, 1);

                mFechaFinal = mFechaInicial.AddMonths(1);
                mFechaFinal = new DateTime(mFechaFinal.Year, mFechaFinal.Month, 1);
                mFechaFinal = mFechaFinal.AddDays(-1);

                if (id == 0)
                {
                    mFechaInicial = new DateTime(2017, 10, 1);
                    mFechaFinal = new DateTime(2017, 10, 31);
                }

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    var qUpdateClientes = from c in db.MF_Cliente select c;
                    foreach (var item in qUpdateClientes)
                    {
                        try
                        {
                            int mMes = Convert.ToDateTime(item.FECHA_NACIMIENTO).Month;
                            if (mMes > 0) item.MES_NACIMIENTO = Convert.ToDateTime(item.FECHA_NACIMIENTO).Month;
                        }
                        catch
                        {
                            //Nothing
                        }
                    }

                    db.SaveChanges();
                    transactionScope.Complete();
                }

                var qClientes = from c in db.MF_Cliente
                                join cc in db.CLIENTE on c.CLIENTE equals cc.CLIENTE1
                                where c.MES_NACIMIENTO == mFechaInicial.Month
                                orderby cc.NOMBRE
                                select new
                                {
                                    Cliente = c.CLIENTE,
                                    Nombre = cc.NOMBRE,
                                    Telefono1 = cc.TELEFONO1,
                                    Telefono2 = cc.TELEFONO2,
                                    Telefono3 = cc.FAX,
                                    Telefono4 = c.CELULAR,
                                    Email = cc.E_MAIL,
                                    FechaNacimiento = c.FECHA_NACIMIENTO
                                };
                
                DataTable dt = new DataTable("clientes");
                dt.Columns.Add(new DataColumn("Tienda", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Factura", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Fecha", typeof(System.DateTime)));
                dt.Columns.Add(new DataColumn("Monto", typeof(System.Decimal)));
                dt.Columns.Add(new DataColumn("FormaPago", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Pedido", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Cliente", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Nombre", typeof(System.String)));
                dt.Columns.Add(new DataColumn("FechaNacimiento", typeof(System.DateTime)));
                dt.Columns.Add(new DataColumn("Telefonos", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Email", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Articulos", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Vendedor", typeof(System.String)));
                dt.Columns.Add(new DataColumn("VendedorActivo", typeof(System.String)));
                dt.Columns.Add(new DataColumn("EmailVendedor", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Jefe", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Supervisor", typeof(System.String)));

                foreach (var item in qClientes)
                {
                    var qFactura = from f in db.FACTURA
                                   join v in db.VENDEDOR on f.VENDEDOR equals v.VENDEDOR1
                                   join ff in db.MF_Factura on f.FACTURA1 equals ff.FACTURA
                                   join vv in db.MF_Vendedor on f.VENDEDOR equals vv.VENDEDOR
                                   join c in db.MF_Cobrador on f.COBRADOR equals c.COBRADOR
                                   where f.CLIENTE == item.Cliente && f.COBRADOR != "F01" && f.ANULADA == "N" && f.TOTAL_FACTURA > 500 && f.VENDEDOR != "0107"
                                   orderby f.CreateDate descending
                                   select new
                                   {
                                       Factura = f.FACTURA1,
                                       Cobrador = f.COBRADOR,
                                       Fecha = f.FECHA,
                                       Total = f.TOTAL_FACTURA,
                                       FormaPago = ff.NIVEL_PRECIO,
                                       Vendedor = v.NOMBRE,
                                       EmailVendedor = vv.EMAIL,
                                       VendedorActivo = vv.ACTIVO,
                                       Jefe = c.JEFE,
                                       Supervisor = c.SUPERVISOR,
                                       Pedido = f.PEDIDO
                                   };

                    if (qFactura.Count() > 0)
                    {
                        string mFactura = qFactura.First().Factura;

                        string mArticulos = "";
                        var qLinea = from fl in db.FACTURA_LINEA
                                     join a in db.ARTICULO on fl.ARTICULO equals a.ARTICULO1
                                     where fl.FACTURA == mFactura && fl.ARTICULO.Substring(0, 1) != "0" && fl.ARTICULO.Substring(0, 1) != "S" && (fl.TIPO_LINEA == "K" || fl.TIPO_LINEA == "N")
                                     orderby fl.LINEA
                                     select a;

                        foreach (var ar in qLinea)
                        {
                            mArticulos = string.Format("{0}{1}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", mArticulos, ar.DESCRIPCION.Length > 30 ? ar.DESCRIPCION.Substring(0, 30) : ar.DESCRIPCION);
                        }

                        DateTime mFechaNacimiento;

                        try
                        {
                            mFechaNacimiento = new DateTime(mFechaInicial.Year, Convert.ToDateTime(item.FechaNacimiento).Month, Convert.ToDateTime(item.FechaNacimiento).Day);
                        }
                        catch
                        {
                            mFechaNacimiento = mFechaFinal;
                        }

                        mRetorna = string.Format("Cliente: {0}", item.Cliente);
                        DataRow mRow = dt.NewRow();
                        mRow["Tienda"] = qFactura.First().Cobrador;
                        mRow["Factura"] = qFactura.First().Factura;
                        mRow["Fecha"] = qFactura.First().Fecha;
                        mRow["Monto"] = qFactura.First().Total;
                        mRow["FormaPago"] = qFactura.First().FormaPago;
                        mRow["Pedido"] = qFactura.First().Pedido;
                        mRow["Cliente"] = item.Cliente;
                        mRow["Nombre"] = item.Nombre;
                        mRow["FechaNacimiento"] = mFechaNacimiento;
                        mRow["Telefonos"] = string.Format("{0} - {1} - {2} - {3}", item.Telefono1, item.Telefono2, item.Telefono3, item.Telefono4);
                        mRow["Email"] = item.Email;
                        mRow["Articulos"] = mArticulos;
                        mRow["Vendedor"] = qFactura.First().Vendedor;
                        mRow["VendedorActivo"] = qFactura.First().VendedorActivo;
                        mRow["EmailVendedor"] = qFactura.First().EmailVendedor;
                        mRow["Jefe"] = qFactura.First().Jefe;
                        mRow["Supervisor"] = qFactura.First().Supervisor;
                        dt.Rows.Add(mRow);
                    }
                }

                mRetorna = "No se encontraron clientes para enviar el reporte.";
                //return string.Format("FechaInicial: {0}; mFechaFinal: {1}; {2}", mFechaInicial.ToShortDateString(), mFechaFinal.ToShortDateString(), mRetorna);

                if (dt.Rows.Count > 0)
                {
                    DataTable dtVendedores = new DataTable();
                    DataView dvVendedores = dt.DefaultView;

                    dtVendedores = dvVendedores.ToTable(true, "Vendedor");

                    foreach (DataRow row in dtVendedores.Rows)
                    {
                        StringBuilder mBody = new StringBuilder();
                        mBody.Append(Utilitario.CorreoInicioFormatoCafe());

                        string mSaludo = string.Format("Estimado(a) {1}:<br/><br/>Estos clientes cumplen años en el mes de {0} {2}, haga contacto con ellos para saludarlos e intentar relizar una nueva venta, aproveche a realizar comentarios sobre los artículos adquiridos en la última compra.<br/><br/>", Utilitario.Mes(mFechaInicial), row["Vendedor"].ToString(), mFechaInicial.Year.ToString());
                        mBody.Append(mSaludo);

                        DataRow[] rows = dt.Select(string.Format("Vendedor = '{0}'", row["Vendedor"].ToString()), "Cliente");

                        if (rows[0]["VendedorActivo"].ToString() == "N")
                            mBody.Append("<strong>NOTA: Atención JEFE y SUPERVISOR, este vendedor no está activo, deben re-enviar este correo a un vendedor que se haga cargo de darle seguimiento.</strong><br/><br/>");

                        mBody.Append("<table align='center' class='style4'>");
                        mBody.Append("<tr>");
                        mBody.Append(Utilitario.FormatoTituloIzquierdo("Tienda"));
                        mBody.Append(Utilitario.FormatoTituloIzquierdo("Factura"));
                        mBody.Append(Utilitario.FormatoTituloIzquierdo("Fecha"));
                        mBody.Append(Utilitario.FormatoTituloIzquierdo("Monto"));
                        mBody.Append(Utilitario.FormatoTituloIzquierdo("Forma Pago"));
                        mBody.Append(Utilitario.FormatoTituloIzquierdo("Pedido"));
                        mBody.Append(Utilitario.FormatoTituloIzquierdo("Cliente"));
                        mBody.Append(Utilitario.FormatoTituloIzquierdo("Cumplea&#241;os"));
                        mBody.Append(Utilitario.FormatoTituloCentro("Telefonos"));
                        mBody.Append(Utilitario.FormatoTituloCentro("Email"));
                        mBody.Append(Utilitario.FormatoTituloIzquierdo("Articulos"));
                        mBody.Append("</tr>");

                        for (int ii = 0; ii < rows.Length; ii++)
                        {
                            mBody.Append("<tr>");
                            mBody.Append(Utilitario.FormatoCuerpoCentro(Convert.ToString(rows[ii]["Tienda"])));
                            mBody.Append(Utilitario.FormatoCuerpoCentro(Convert.ToString(rows[ii]["Factura"])));
                            mBody.Append(Utilitario.FormatoCuerpoCentro(Utilitario.FormatoDDMMYYYY(Convert.ToDateTime(rows[ii]["Fecha"]))));
                            mBody.Append(Utilitario.FormatoCuerpoDerecha(Utilitario.FormatoNumeroDecimal(Convert.ToDecimal(rows[ii]["Monto"]))));
                            mBody.Append(Utilitario.FormatoCuerpoIzquierda(Convert.ToString(rows[ii]["FormaPago"])));
                            mBody.Append(Utilitario.FormatoCuerpoCentro(Convert.ToString(rows[ii]["Pedido"])));
                            mBody.Append(Utilitario.FormatoCuerpoIzquierda(string.Format("{0} - {1}", Convert.ToString(rows[ii]["Cliente"]), Convert.ToString(rows[ii]["Nombre"]))));
                            mBody.Append(Utilitario.FormatoCuerpoCentro(Utilitario.FormatoDDMMYYYY(Convert.ToDateTime(rows[ii]["FechaNacimiento"]))));
                            mBody.Append(Utilitario.FormatoCuerpoIzquierda(string.Format("{0}", Convert.ToString(rows[ii]["Telefonos"]))));
                            mBody.Append(Utilitario.FormatoCuerpoIzquierda(Convert.ToString(rows[ii]["Email"])));
                            mBody.Append(Utilitario.FormatoCuerpoIzquierda(Convert.ToString(rows[ii]["Articulos"])));
                            mBody.Append("</tr>");
                        }
                        
                        mBody.Append("</table>");
                        mBody.Append(Utilitario.CorreoFin());

                        List<string> mDestinatarios = new List<string>();
                        string mRemitente = "puntodeventa2@productosmultiples.com";
                        string mDisplayName = "Cumpleañeros del mes";
                        string mAsunto = string.Format("Cumleañeros del mes de {0} {1}", Utilitario.Mes(mFechaInicial), mFechaInicial.Year.ToString());

                        if (rows[0]["VendedorActivo"].ToString() == "S") mDestinatarios.Add(rows[0]["EmailVendedor"].ToString());
                        mDestinatarios.Add(rows[0]["Jefe"].ToString());
                        mDestinatarios.Add(rows[0]["Supervisor"].ToString());

                        Utilitario.EnviarEmail(mRemitente, mDestinatarios, mAsunto, mBody, mDisplayName);
                    }

                    mRetorna = "Correo enviado exitosamente.";
                }
            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0} {1}", CatchClass.ExMessage(ex, "CumpleanierosController", "GetCumpleanieros"), mRetorna);

                StringBuilder mBody = new StringBuilder();
                mBody.Append(mRetorna);

                List<string> mDestinatarios = new List<string>();
                mDestinatarios.Add(WebConfigurationManager.AppSettings["ITMailNotification"].ToString());

                Utilitario.EnviarEmail("puntodeventa@productosmultiples.com", mDestinatarios, "Error en reporte de cumpleañeros", mBody, "Cumpleañeros del mes");
            }

            return mRetorna;

        }
    }
}
