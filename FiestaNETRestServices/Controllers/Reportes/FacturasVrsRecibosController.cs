﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;
using MF_Clases;

namespace FiestaNETRestServices.Controllers.Reportes
{
    public class FacturasVrsRecibosController : ApiController
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET api/facturasvrsrecibos
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/facturasvrsrecibos/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/facturasvrsrecibos
        public void Post([FromBody]string value)
        {
        }

        // PUT api/facturasvrsrecibos/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/facturasvrsrecibos/5
        public void Delete(int id)
        {
        }
    }
}
