﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using FiestaNETRestServices.Content.Abstract;
using Newtonsoft.Json;
using MF_Clases;

namespace FiestaNETRestServices.Controllers.Reportes
{
    public class VentasTresMesesController : MFApiController
    {

        // GET api/ventastresmeses/5
        public string GetVentasTresMeses(int id = 1, string tienda = "", string vendedor = "", string ambiente = "")
        {
            string mRetorna = "";
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            db.CommandTimeout = 999999999;

            try
            {
                DateTime mFechaInicial = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1); DateTime mFechaFinal;

                mFechaInicial = mFechaInicial.AddMonths(-3);
                mFechaInicial = new DateTime(mFechaInicial.Year, mFechaInicial.Month, 1);

                mFechaFinal = mFechaInicial.AddMonths(1);
                mFechaFinal = new DateTime(mFechaFinal.Year, mFechaFinal.Month, 1);
                mFechaFinal = mFechaFinal.AddDays(-1);

                if (id == 0)
                {
                    mFechaInicial = new DateTime(2017, 1, 1);
                    mFechaFinal = new DateTime(2017, 6, 30);
                }

                var qTiendas = (from f in db.FACTURA
                                where f.FECHA >= mFechaInicial && f.FECHA <= mFechaFinal && f.ANULADA == "N" && f.COBRADOR != "F01"
                                select new
                                {
                                    Tienda = f.COBRADOR
                                }).Distinct().OrderBy(x => x.Tienda);

                if (tienda.Trim().Length > 0) qTiendas = qTiendas.Where(x => x.Tienda.Equals(tienda)).OrderBy(x => x.Tienda);

                foreach (var t in qTiendas)
                {
                    string mTienda = t.Tienda;
                    var qTienda = (from c in db.MF_Cobrador where c.COBRADOR == mTienda select c).First();

                    string mJefe = qTienda.JEFE;
                    string mSupervisor = qTienda.SUPERVISOR;

                    List<string> VendedoresExcluidos = db.MF_Catalogo
                        .Where(x => x.CODIGO_TABLA == 66 && x.ACTIVO == "S").Select(x => x.CODIGO_CAT).ToList();

                    var qVendedores = (from f in db.FACTURA
                                       join v in db.VENDEDOR on f.VENDEDOR equals v.VENDEDOR1
                                       join ve in db.MF_Vendedor on f.VENDEDOR equals ve.VENDEDOR
                                       where f.FECHA >= mFechaInicial
                                             && f.FECHA <= mFechaFinal
                                             && f.ANULADA == "N"
                                             && f.COBRADOR == mTienda
                                             && !VendedoresExcluidos.Contains(f.VENDEDOR)
                                       select new
                                       {
                                           Vendedor = f.VENDEDOR,
                                           Nombre = v.NOMBRE,
                                           Email = ve.EMAIL,
                                           Activo = v.ACTIVO
                                       }).Distinct().OrderBy(x => x.Vendedor);

                    if (vendedor.Trim().Length > 0) qVendedores = qVendedores.Where(x => x.Vendedor.Equals(vendedor)).OrderBy(x => x.Vendedor);

                    foreach (var v in qVendedores)
                    {
                        var qFacturas = (from f in db.FACTURA
                                         join c in db.CLIENTE on f.CLIENTE equals c.CLIENTE1
                                         join cl in db.MF_Cliente on f.CLIENTE equals cl.CLIENTE
                                         join fa in db.MF_Factura on f.FACTURA1 equals fa.FACTURA
                                         where f.FECHA >= mFechaInicial && f.FECHA <= mFechaFinal && f.ANULADA == "N" && f.COBRADOR == mTienda && f.TOTAL_FACTURA > 500 && f.VENDEDOR == v.Vendedor
                                         select new
                                         {
                                             Factura = f.FACTURA1,
                                             Fecha = f.FECHA,
                                             Total = f.TOTAL_FACTURA,
                                             FormaPago = fa.NIVEL_PRECIO,
                                             Cliente = f.CLIENTE,
                                             Nombre = c.NOMBRE,
                                             Telefono1 = c.TELEFONO1,
                                             Telefono2 = c.TELEFONO2,
                                             Telefono3 = c.FAX,
                                             Telefono4 = cl.CELULAR,
                                             Email = c.E_MAIL,
                                             Pedido =  f.PEDIDO
                                         }).OrderBy(x => x.Factura);

                        StringBuilder mBody = new StringBuilder();
                        mBody.Append(Utilitario.CorreoInicioFormatoCafe());

                        string mSaludo = string.Format("Estimado(a) {1}:<br/><br/>Estos clientes le compraron en el mes de {0} {2}, intente contactarlos para relizar una nueva venta.<br/><br/>", Utilitario.Mes(mFechaInicial), v.Nombre, mFechaInicial.Year.ToString());
                        if (id == 0) mSaludo = string.Format("Estimado(a) {1}:<br/><br/>Estos clientes le compraron entre los meses de Enero y Diciembre 2017, intente contactarlos para relizar una nueva venta.<br/><br/>", Utilitario.Mes(mFechaInicial), v.Nombre, mFechaInicial.Year.ToString());
                        mBody.Append(mSaludo);

                        if (v.Activo == "N")
                            mBody.Append("<strong>NOTA: Atención JEFE y SUPERVISOR, este vendedor no está activo, deben re-enviar este correo a un vendedor que se haga cargo de darle seguimiento.</strong><br/><br/>");

                        mBody.Append("<table align='center' class='style4'>");
                        mBody.Append("<tr>");
                        mBody.Append(Utilitario.FormatoTituloIzquierdo("Tienda"));
                        mBody.Append(Utilitario.FormatoTituloIzquierdo("Factura"));
                        mBody.Append(Utilitario.FormatoTituloIzquierdo("Fecha"));
                        mBody.Append(Utilitario.FormatoTituloIzquierdo("Monto"));
                        mBody.Append(Utilitario.FormatoTituloIzquierdo("Forma Pago"));
                        mBody.Append(Utilitario.FormatoTituloIzquierdo("Pedido"));
                        mBody.Append(Utilitario.FormatoTituloIzquierdo("Cliente"));
                        mBody.Append(Utilitario.FormatoTituloCentro("Telefonos"));
                        mBody.Append(Utilitario.FormatoTituloCentro("Email"));
                        mBody.Append(Utilitario.FormatoTituloIzquierdo("Articulos"));
                        mBody.Append("</tr>");

                        foreach (var f in qFacturas)
                        {
                            string mArticulos = "";
                            var qLinea = from fl in db.FACTURA_LINEA join a in db.ARTICULO on fl.ARTICULO equals a.ARTICULO1
                                         where fl.FACTURA == f.Factura && fl.ARTICULO.Substring(0, 1) != "0" && fl.ARTICULO.Substring(0, 1) != "S" && (fl.TIPO_LINEA == "K" || fl.TIPO_LINEA == "N") orderby fl.LINEA select a;

                            foreach (var ar in qLinea)
                            {
                                mArticulos = string.Format("{0}{1}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", mArticulos, ar.DESCRIPCION.Length > 30 ? ar.DESCRIPCION.Substring(0, 30) : ar.DESCRIPCION);
                            }

                            mBody.Append("<tr>");
                            mBody.Append(Utilitario.FormatoCuerpoCentro(mTienda));
                            mBody.Append(Utilitario.FormatoCuerpoCentro(f.Factura));
                            mBody.Append(Utilitario.FormatoCuerpoCentro(Utilitario.FormatoDDMMYYYY(Convert.ToDateTime(f.Fecha))));
                            mBody.Append(Utilitario.FormatoCuerpoDerecha(Utilitario.FormatoNumeroDecimal(f.Total)));
                            mBody.Append(Utilitario.FormatoCuerpoIzquierda(f.FormaPago));
                            mBody.Append(Utilitario.FormatoCuerpoCentro(f.Pedido));
                            mBody.Append(Utilitario.FormatoCuerpoIzquierda(string.Format("{0} - {1}", f.Cliente, f.Nombre)));
                            mBody.Append(Utilitario.FormatoCuerpoIzquierda(string.Format("{0} - {1} - {2} - {3}", f.Telefono1, f.Telefono2, f.Telefono3, f.Telefono4)));
                            mBody.Append(Utilitario.FormatoCuerpoIzquierda(f.Email));
                            mBody.Append(Utilitario.FormatoCuerpoIzquierda(mArticulos));
                            mBody.Append("</tr>");
                        }
                        
                        mBody.Append("</table>");
                        mBody.Append(Utilitario.CorreoFin());
                        
                        List<string> mDestinatarios = new List<string>();
                        // string mRemitente = "puntodeventa2@productosmultiples.com";
                        string mRemitente = Utils.Utilitarios.GetRemitenteNotificacion(Const.REMITENTE_CORREO.REMITENTE_REPORTE_VENTAS_TRES_MESES, Const.LISTA_CORREO.REMITENTES_CORREO);
                        string mDisplayName = "Ventas Mensuales";
                        string mAsunto = string.Format("Ventas del mes de {0} {1}", Utilitario.Mes(mFechaInicial), mFechaInicial.Year.ToString());
                        if (id == 0) mAsunto = "Ventas de los meses de Enero a Diciembre 2017";
                        
                        if (ambiente == "")
                        {
                            if (v.Activo == "S") mDestinatarios.Add(v.Email);
                            mDestinatarios.Add(mJefe);
                            mDestinatarios.Add(mSupervisor);
                        }
                        else
                        {
                            mAsunto = string.Format("{0} - Ventas Mensuales", mAsunto);
                            mDestinatarios.Add("noreply@mueblesfiesta.com");
                        }

                        Utilitario.EnviarEmail(mRemitente, mDestinatarios, mAsunto, mBody, mDisplayName);
                    }

                }
                
                mRetorna = "Correo enviado exitosamente.";
            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0}", CatchClass.ExMessage(ex, "VentasTresMesesController", "GetVentasTresMeses"));

                StringBuilder mBody = new StringBuilder();
                mBody.Append(mRetorna);

                List<string> mDestinatarios = new List<string>();
                mDestinatarios.Add("noreply@mueblesfiesta.com");

                Utilitario.EnviarEmail("puntodeventa@productosmultiples.com", mDestinatarios, "Error en reporte de ventas de tres meses", mBody, "Ventas mensuales");
            }

            return mRetorna;
        }

    }
}
