﻿using FiestaNETRestServices.Content.Abstract;
using MF_Clases;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web.Configuration;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Solicitudes
{
    /// <summary>
    /// Servicio para actualizar el número de factura
    /// en los registros de las solicitudes de CREDIPLUS
    /// </summary>
    public class ActualizarFacturasController : MFApiController
    {
        private new static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [HttpGet]
        public Respuesta Get()
        {
            Respuesta mRespuesta = new Respuesta();
            //Validacion de estado de la solicitud en CrediPlus
            string mUrlCrediPlus = WebConfigurationManager.AppSettings["DefaultATIDServicesURL"];
            string mAmbiente= WebConfigurationManager.AppSettings["Ambiente"];
            try
            {
                List<long> lstPedidos=new List<long>();
                lstPedidos.Add(2018000361);
                lstPedidos.Add(2018000327);
                lstPedidos.Add(2018000378);
                lstPedidos.Add(2018000230);
                lstPedidos.Add(2018000184);
                lstPedidos.Add(2018000219);
                lstPedidos.Add(2018000165);
                lstPedidos.Add(2018000411);
                lstPedidos.Add(2018000115);
                lstPedidos.Add(2018000117);
                lstPedidos.Add(2018000190);
                lstPedidos.Add(2018000347);
                lstPedidos.Add(2018000155);
                lstPedidos.Add(2018000197);
                lstPedidos.Add(2018000359);
                lstPedidos.Add(2018000338);
                lstPedidos.Add(2018000136);
                lstPedidos.Add(2018000089);
                lstPedidos.Add(2018000217);
                lstPedidos.Add(2018000273);
                lstPedidos.Add(2018000264);
                lstPedidos.Add(2018000222);
                lstPedidos.Add(2018000320);
                lstPedidos.Add(2018000285);
                lstPedidos.Add(2018000244);
                lstPedidos.Add(2018000424);
                lstPedidos.Add(2018000088);
                lstPedidos.Add(2018000124);
                lstPedidos.Add(2018000154);
                lstPedidos.Add(2018000237);
                lstPedidos.Add(2018000410);
                lstPedidos.Add(2018000182);
                lstPedidos.Add(2018000288);
                lstPedidos.Add(2018000227);
                lstPedidos.Add(2018000339);
                lstPedidos.Add(2018000207);
                lstPedidos.Add(2018000148);
                lstPedidos.Add(2018000218);
                lstPedidos.Add(2018000157);
                lstPedidos.Add(2018000105);
                lstPedidos.Add(2018000092);
                lstPedidos.Add(2018000093);
                lstPedidos.Add(2018000099);
                lstPedidos.Add(2018000102);
                lstPedidos.Add(2018000110);
                lstPedidos.Add(2018000111);
                lstPedidos.Add(2018000113);
                lstPedidos.Add(2018000118);
                lstPedidos.Add(2018000119);
                lstPedidos.Add(2018000129);
                lstPedidos.Add(2018000128);
                lstPedidos.Add(2018000131);
                lstPedidos.Add(2018000134);
                lstPedidos.Add(2018000135);
                lstPedidos.Add(2018000139);
                lstPedidos.Add(2018000140);
                lstPedidos.Add(2018000162);
                lstPedidos.Add(2018000142);
                lstPedidos.Add(2018000146);
                lstPedidos.Add(2018000149);
                lstPedidos.Add(2018000153);
                lstPedidos.Add(2018000152);
                lstPedidos.Add(2018000159);
                lstPedidos.Add(2018000177);
                lstPedidos.Add(2018000179);
                lstPedidos.Add(2018000181);
                lstPedidos.Add(2018000323);
                lstPedidos.Add(2018000188);
                lstPedidos.Add(2018000191);
                lstPedidos.Add(2018000192);
                lstPedidos.Add(2018000193);
                lstPedidos.Add(2018000194);
                lstPedidos.Add(2018000195);
                lstPedidos.Add(2018000208);
                lstPedidos.Add(2018000249);
                lstPedidos.Add(2018000221);
                lstPedidos.Add(2018000226);
                lstPedidos.Add(2018000229);
                lstPedidos.Add(2018000228);
                lstPedidos.Add(2018000231);
                lstPedidos.Add(2018000232);
                lstPedidos.Add(2018000236);
                lstPedidos.Add(2018000243);
                lstPedidos.Add(2018000250);
                lstPedidos.Add(2018000256);
                lstPedidos.Add(2018000261);
                lstPedidos.Add(2018000272);
                lstPedidos.Add(2018000276);
                lstPedidos.Add(2018000279);
                lstPedidos.Add(2018000292);
                lstPedidos.Add(2018000297);
                lstPedidos.Add(2018000304);
                lstPedidos.Add(2018000306);
                lstPedidos.Add(2018000314);
                lstPedidos.Add(2018000312);
                lstPedidos.Add(2018000313);
                lstPedidos.Add(2018000315);
                lstPedidos.Add(2018000319);
                lstPedidos.Add(2018000322);
                lstPedidos.Add(2018000329);
                lstPedidos.Add(2018000334);
                lstPedidos.Add(2018000332);
                lstPedidos.Add(2018000340);
                lstPedidos.Add(2018000341);
                lstPedidos.Add(2018000345);
                lstPedidos.Add(2018000350);
                lstPedidos.Add(2018000351);
                lstPedidos.Add(2018000354);
                lstPedidos.Add(2018000358);
                lstPedidos.Add(2018000362);
                lstPedidos.Add(2018000366);
                lstPedidos.Add(2018000416);
                lstPedidos.Add(2018000368);
                lstPedidos.Add(2018000367);
                lstPedidos.Add(2018000369);
                lstPedidos.Add(2018000371);
                lstPedidos.Add(2018000376);
                lstPedidos.Add(2018000383);
                lstPedidos.Add(2018000384);
                lstPedidos.Add(2018000407);
                lstPedidos.Add(2018000387);
                lstPedidos.Add(2018000390);
                lstPedidos.Add(2018000391);
                lstPedidos.Add(2018000392);
                lstPedidos.Add(2018000395);
                lstPedidos.Add(2018000402);
                lstPedidos.Add(2018000408);
                lstPedidos.Add(2018000412);
                lstPedidos.Add(2018000419);
                lstPedidos.Add(2018000437);
                lstPedidos.Add(2018000486);
                var qFacturas = (from p in dbExactus.MF_Pedido join s in dbExactus.MF_Factura on p.PEDIDO equals s.PEDIDO into sp
                                 from x in sp.DefaultIfEmpty()
                                 where lstPedidos.Contains(p.SOLICITUD.Value) && x.FINANCIERA == 12
                                 select new { CLIENTE=x.CLIENTE, FACTURA=x.FACTURA, SOLICITUD=p.SOLICITUD.Value, DOCUMENTO=p.PEDIDO});
                if (qFacturas.Count() > 0)
                {
                    foreach (var factura in qFacturas)
                    {
                        
                        log.Debug("Actualizando factura en crediplus factura " + factura.FACTURA);
                        var client = new RestSharp.RestClient(string.Format("{0}api", mUrlCrediPlus));
                        var request = new RestRequest("/FacturaSolicitud", Method.PUT);
                        FacturaSolicitudATID _factura = new FacturaSolicitudATID();
                        _factura.Cliente = factura.CLIENTE;
                        _factura.Factura = factura.FACTURA;
                        _factura.Solicitud = factura.SOLICITUD.ToString();
                        _factura.Documento = factura.DOCUMENTO;

                        request.ReadWriteTimeout = 36000000;

                        client.AddDefaultHeader("Content-Type", "application/json");
                        request.AddJsonBody(_factura);

                        string json = JsonConvert.SerializeObject(_factura);
                        log.Debug("Parametros:" + json);

                        if (!mAmbiente.Equals("PRO"))
                            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(OnValidationCallback);
                        IRestResponse response = client.Put(request);
                        if (response.StatusCode != HttpStatusCode.OK)
                        {
                            log.Error(string.Format("No se pudo grabar la actualización de la factura {0}, mensaje {1} ", factura.FACTURA, response.ErrorMessage));
                        }
                    }
                }
                mRespuesta.Exito = true;
                mRespuesta.Mensaje = lstPedidos.Count().ToString() + " solicitudes actualizadas.";
            }
            catch (Exception ex)
            {
                log.Error("NO se pudo grabar la actualización de No. de factura en CREDIPLUS, Error: " + ex.Message + " | " + ex.InnerException);
                mRespuesta.Exito = false;
                mRespuesta.Mensaje = "NO se pudo grabar la actualización de No. de factura en CREDIPLUS, Error: " + ex.Message + " | " + ex.InnerException;
            }
            return mRespuesta;
        }

        public static bool OnValidationCallback(object sender, X509Certificate cert, X509Chain chain, System.Net.Security.SslPolicyErrors errors)
        {
            return true;
        }

    }
}
