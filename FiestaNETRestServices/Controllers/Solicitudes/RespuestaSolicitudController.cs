﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.DAL;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Solicitudes
{
    public class RespuestaSolicitudController : MFApiController
    {
        private new static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [HttpGet]
        public RespuestaSolicitud Get(string id)
        {

            DALSolicitudCredito mSolicitud = new DAL.DALSolicitudCredito();
            try
            {

                RespuestaSolicitud oRespuesta = mSolicitud.ObtenerRespuesta(id);
                if (oRespuesta.ResultadoPrecalificacion != null)
                    return oRespuesta;
                else
                    return null;
            }
            catch
            {
                return null;
            }
        }
    }
}
