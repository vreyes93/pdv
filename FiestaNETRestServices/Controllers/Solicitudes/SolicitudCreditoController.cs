﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.DAL;
using MF_Clases;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using static MF_Clases.Clases;
using System.Security.Cryptography.X509Certificates;
using MF_Clases.Restful;

namespace FiestaNETRestServices.Controllers.Solicitudes
{
    [RoutePrefix("api/SolicitudCredito")]
    public class SolicitudCreditoController : MFApiController
    {
        private new static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [HttpGet]
        public Respuesta Get(string id)
        {
            Respuesta mRespuesta = new Respuesta();
            DALSolicitudCredito mSolicitud = new DAL.DALSolicitudCredito();
            try
            {
                mRespuesta = mSolicitud.ObtenerSolicitudCredito(id);
                return mRespuesta;
            }
            catch (Exception e)
            {
                return new Respuesta(false, e, "");
            }
        }

        [HttpPost]
        [Route("EnviarSolicitud")]
        //public async System.Threading.Tasks.Task<Respuesta> EnviarSolicitud([FromBody] string pedido)
        public async System.Threading.Tasks.Task<Respuesta> EnviarSolicitud(HttpRequestMessage request)

        {

            string pedido = "";
            Respuesta resp = new Respuesta();
            try
            {

                try
                {
                    pedido = request.Content.ReadAsStringAsync().Result;
                    log.Info("Enviar solicitud a CREDIPLUS, del pedido: " + pedido);
                }
                catch (Exception ix)
                {
                    log.Error("Problema al obtener el pedido  para enviar la solicitud a CREDIPLUS, mensaje de error: " + ix.Message);
                }
                var respSolicitud = new RespuestaSolicitud();
                //obtener la solicitud
                Solicitud oSolicitud = new Solicitud();
                string URLRest = string.Empty;
                #region "Ambiente"

                string mAmbiente = ConfigurationManager.AppSettings["Ambiente"].ToString();
                string mURLAtid = mAmbiente.Equals("PRO") ? ConfigurationManager.AppSettings["DefaultATIDServicesURL"].ToString() :
                                                            ConfigurationManager.AppSettings["DefaultATIDServicesURLDev"].ToString();

                URLRest = mAmbiente.Equals("DES") ? "http://localhost:53874/" :
                    (mAmbiente.Equals("PRU") ? "http://SQl.fiesta.local/RestServicesPruebas/" :
                    "http://SQl.fiesta.local/RestServices/");
                #endregion
                var client = new RestClient(URLRest);
                var restRequest = new RestRequest("api/Solicitud/" + pedido, Method.GET)
                {
                    RequestFormat = DataFormat.Json
                };

                log.Info("URL: " + client.BaseUrl);
                //SOLICITUD DE CREDITO
                try
                {
                    var result = client.Get(restRequest);
                    if (result != null)
                    {
                        log.Info("result.content: " + result.Content);
                        oSolicitud = JsonConvert.DeserializeObject<Solicitud>(result.Content);

                    }
                }
                catch (Exception ex)
                {
                    log.Error("Problema al obtener la solicitud de crédito previo a enviarlo a CREDIPLUS", ex);
                }
                //POST ATID
                try
                {


                    string json = JsonConvert.SerializeObject(oSolicitud);
                    log.Info("POST ATID: " + json);
                    byte[] data = Encoding.ASCII.GetBytes(json);
                    var uri = string.Format("{0}", mURLAtid);
                    DALSolicitudCredito dALSolicitud = new DALSolicitudCredito();

                    //hacemos la solicitud de crédito a los servicios de ATID
                    respSolicitud = await GetATIDAsync(uri, oSolicitud);
                    //respSolicitud = await GetATIDAsync(uri, pedido);
                    if (respSolicitud.MensajeError.Length == 0)
                    {
                        resp.Exito = true;
                        resp.Objeto = respSolicitud;

                        var resultado = dALSolicitud.GrabarRespuestaSolicitudCredito(respSolicitud, pedido);
                        if (!resultado.Exito)
                        {
                            resp.Exito = false;
                            resp.Mensaje = "Error al grabar la solicitud de crédito:" + resultado.Mensaje;
                            return resp;
                        }

                    }
                    else
                    {
                        resp.Exito = false;
                        if (respSolicitud.MensajeError.Contains("An error occurred while sending the request"))
                            resp.Mensaje = "Ocurrió un inconveniente con el servicio de CREDIPLUS, por favor intente nuevamente en unos minutos.";
                        else
                            resp.Mensaje = "Mensaje de error de CREDIPLUS: [" + respSolicitud.MensajeError + "] " + respSolicitud.ResultadoPrecalificacion;
                    }

                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    resp.Exito = false;
                    resp.Mensaje = "Ocurrió un inconveniente con el servicio de CREDIPLUS, por favor intente nuevamente en unos minutos.";
                    log.Error("Problema al obtener la solicitud de crédito CREDIPLUS", ex);
                }

            }
            catch (Exception ex)
            {
                resp.Exito = false;
                resp.Mensaje = "Ocurrió un inconveniente con el servicio de CREDIPLUS, por favor intente nuevamente en unos minutos.";
                log.Error("Problema en la solicitud de crédito referencia: [" + pedido + "]", ex);
            }
            return resp;
        }


        public static bool OnValidationCallback(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }

        private async Task<RespuestaSolicitud> GetATIDAsync(string UrlATID, Solicitud data)
        {
            //Dictionary<string, string> header = new Dictionary<string, string>();
            //header.Add("Content-Type", "application/json");
            
            //Api api = new Api(UrlATID);
            //IRestResponse resp = api.Process(Method.POST, "api/SolicitudPersonaIndividualMF/", data, header, null);
            //if (resp.StatusCode == HttpStatusCode.OK)
            //{
            //    RespuestaSolicitud test = JsonConvert.DeserializeObject<RespuestaSolicitud>(resp.Content);
            //}

            RespuestaSolicitud result = new RespuestaSolicitud();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(UrlATID);
                string mAmbiente = ConfigurationManager.AppSettings["Ambiente"].ToString();
                ///Para pasar el certificado de seguridad en ambiente de Pruebas
                if (!mAmbiente.Equals("PRO"))
                {
                    ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(OnValidationCallback);
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                }


                HttpResponseMessage response = await client.PostAsJsonAsync("api/SolicitudPersonaIndividualMF/", data);
                if (response.IsSuccessStatusCode)
                {
                    //result = await response.Content.ReadAsAsync<RespuestaSolicitud>();
                    result = JsonConvert.DeserializeObject<RespuestaSolicitud>(response.Content.ReadAsStringAsync().Result);
                    return result;
                }
                else
                {
                    result.MensajeError = response.Content.ToString() + " " + response.StatusCode;
                }
            }
            return result;

        }

        private async Task<RespuestaSolicitud> GetATIDAsync(string UrlATID, string Pedido)
        {
            RespuestaSolicitud result = new RespuestaSolicitud();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(UrlATID);
                ///Para pasar el certificado de seguridad en ambiente de pruebas
                string mAmbiente = ConfigurationManager.AppSettings["Ambiente"].ToString();
                if (!mAmbiente.Equals("PRO"))
                    ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(OnValidationCallback);
                //fin certificado

                HttpResponseMessage response = await client.GetAsync("api/SolicitudPersonaIndividualMF?psolicitud=" + Pedido);
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<RespuestaSolicitud>();
                    return result;
                }
                else
                {
                    result.MensajeError = response.Content.ToString() + " " + response.StatusCode;
                }
            }
            return result;

        }
    }
}
