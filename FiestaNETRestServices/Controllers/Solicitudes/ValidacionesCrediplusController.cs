﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;
using System.Reflection;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using System.IO;
using System.Web.Configuration;
using System.Xml;
using FiestaNETRestServices.Utils;
using System.Text;
using Newtonsoft.Json;

namespace FiestaNETRestServices.Controllers.Solicitudes
{
    public class ValidacionesCrediplusController : MFApiController
    {
        // GET: api/ValidacionesCrediplus
        public string Get()
        {
            return "Debe enviar el código del cliente";
        }

        // GET: api/ValidacionesCrediplus/5
        public string Get(string id)
        {
            string mRetorna = "";

            try
            {
                string mCliente = "";
                string documento = id;

                string tipo = "C";
                if (documento.Substring(0, 1) == "P") tipo = "P";

                if (documento == "P000000") return "Debe seleccionar un pedido.";
                if (documento == "000000") return "Debe seleccionar una cotización.";

                Int32 mCotizacion = 0;
                if (tipo == "C") mCotizacion = Convert.ToInt32(documento);

                if (tipo == "C")
                {
                    if ((from c in db.MF_Cotizacion where c.COTIZACION == mCotizacion select c).Count() == 0) return string.Format("Error, la cotización {0} no existe.", documento);

                    var qCotizacion = (from c in db.MF_Cotizacion where c.COTIZACION == mCotizacion select c).First();
                    if (qCotizacion.FINANCIERA != 12) return string.Format("Error, la cotización {0} no pertenece a Crediplus.", documento);

                    try
                    {
                        mCliente = qCotizacion.CLIENTE;
                    }
                    catch
                    {
                        return  string.Format("Error, la cotización {0} debe tener cliente asignado.", documento);
                    }

                    if (mCliente == null) return string.Format("Error, la cotización {0} debe tener cliente asignado.", documento);
                    if (mCliente.Trim().Length == 0) return string.Format("Error, la cotización {0} debe tener cliente asignado.", documento);

                    Int32 mDiasGracia = Convert.ToInt32((from c in db.MF_Configura select c).First().diasGraciaCotizacion);
                    DateTime mFechaVence = (from c in db.MF_Cotizacion where c.COTIZACION == mCotizacion select c).First().FECHA_VENCE;
                    
                    mFechaVence = mFechaVence.AddDays(mDiasGracia);
                    if (DateTime.Now.Date > mFechaVence) return string.Format("Error, ésta cotización ya está vencida y se terminaron los {0} dias de gracia autorizados, no es posible utilizarla.", mDiasGracia);
                }
                else
                {
                    if ((from p in db.MF_Pedido where p.PEDIDO == documento select p).Count() == 0) return string.Format("Error, el pedido {0} no existe.", documento);

                    var qPedido = (from p in db.MF_Pedido where p.PEDIDO == documento select p).First();
                    if (qPedido.FINANCIERA != 12) return string.Format("Error, el pedido {0} no pertenece a Crediplus.", documento);

                    mCliente = qPedido.CLIENTE;

                    if ((from f in db.FACTURA where f.PEDIDO == documento && f.ANULADA == "N" select f).Count() > 0) return string.Format("Error, el pedido {0} ya se encuentra facturado.", documento);
                }

                DateTime mFechaNacimiento = DateTime.Now.Date;

                try
                {
                    mFechaNacimiento = Convert.ToDateTime((from c in db.MF_Cliente where c.CLIENTE == mCliente select c).First().FECHA_NACIMIENTO);
                    if (mFechaNacimiento.Year == 1) return "Error, debe ingresar la fecha de nacimiento del cliente.";
                }
                catch
                {
                    return "Error, debe ingresar la fecha de nacimiento del cliente.";
                }

                string mUrl = "http://localhost:53874/api";
                string mAmbiente = WebConfigurationManager.AppSettings["Ambiente"];

                if (mAmbiente == "PRO") mUrl = "http://sql.fiesta.local/RestServices/api";
                if (mAmbiente == "PRU") mUrl = "http://sql.fiesta.local/RestServicesPruebas/api";

                WebRequest request = WebRequest.Create(string.Format("{0}/solicitud/{1}", mUrl, documento));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                Clases.Solicitud mSolicitud = new Clases.Solicitud();
                mSolicitud = JsonConvert.DeserializeObject<Clases.Solicitud>(responseString);

                if (mSolicitud.DPI.Trim().Length < 13) mRetorna = string.Format("{0} DPI inválido", mRetorna);
                if (mSolicitud.Profesion.Trim().Length == 0) mRetorna = string.Format("{0} Profesión inválida", mRetorna);
                if (mSolicitud.Puesto.Trim().Length == 0) mRetorna = string.Format("{0} Puesto inválido", mRetorna);
                if (mSolicitud.EmpresaDireccion.Trim().Length == 0) mRetorna = string.Format("{0} Dirección de empresa inválida", mRetorna);

                if ((mSolicitud.Celular.Trim().Length > 0 && mSolicitud.Celular.Trim().Length < 8) || mSolicitud.Celular.Contains("-")) mRetorna = string.Format("{0} celular inválido", mRetorna);
                
                decimal mIngresosMes = 0;
                decimal mGastosMes = 0;

                try
                {
                    mIngresosMes = Convert.ToDecimal(mSolicitud.Ingresos.Trim().Replace(" ", "").Replace(",", "").Replace("Q", "").Replace("q", "").Replace("$", ""));
                    if (mIngresosMes <= 0) mRetorna = string.Format("{0} Egresos inválidos", mRetorna);
                }
                catch
                {
                    mRetorna = string.Format("{0} Ingresos inválidos", mRetorna);
                }

                try
                {
                    mGastosMes = Convert.ToDecimal(mSolicitud.Egresos.Trim().Replace(" ", "").Replace(",", "").Replace("Q", "").Replace("q", "").Replace("$", ""));
                    if (mGastosMes <= 0) mRetorna = string.Format("{0} Egresos inválidos", mRetorna);
                }
                catch
                {
                    mRetorna = string.Format("{0} Egresos inválidos", mRetorna);
                }

                if (!mSolicitud.Email.Contains("@")) mRetorna = string.Format("{0} Email inválido", mRetorna);

                int mCuantasReferencias = 0;

                if (mSolicitud.Familiar1PrimerNombre.Trim().Length > 0 && !mSolicitud.Familiar1PrimerNombre.Contains("-") && mSolicitud.Familiar1PrimerApellido.Trim().Length > 0 && !mSolicitud.Familiar1PrimerApellido.Contains("-") && mSolicitud.Familiar1Celular.Trim().Length == 8)
                    mCuantasReferencias++;

                if (mSolicitud.Familiar2PrimerNombre.Trim().Length > 0 && !mSolicitud.Familiar2PrimerNombre.Contains("-") && mSolicitud.Familiar2PrimerApellido.Trim().Length > 0 && !mSolicitud.Familiar2PrimerApellido.Contains("-") && mSolicitud.Familiar2Celular.Trim().Length == 8)
                    mCuantasReferencias++;

                if (mSolicitud.Personal1PrimerNombre.Trim().Length > 0 && !mSolicitud.Personal1PrimerNombre.Contains("-") && mSolicitud.Personal1PrimerApellido.Trim().Length > 0 && !mSolicitud.Personal1PrimerApellido.Contains("-") && mSolicitud.Personal1Celular.Trim().Length == 8)
                    mCuantasReferencias++;

                if (mSolicitud.Personal2PrimerNombre.Trim().Length > 0 && !mSolicitud.Personal2PrimerNombre.Contains("-") && mSolicitud.Personal2PrimerApellido.Trim().Length > 0 && !mSolicitud.Personal2PrimerApellido.Contains("-") && mSolicitud.Personal2Celular.Trim().Length == 8)
                    mCuantasReferencias++;

                if (mCuantasReferencias < 2) mRetorna = string.Format("{0} Debe ingresar por lo menos dos referencias con sus datos completos", mRetorna);
            }
            catch (Exception ex)
            {
                mRetorna = string.Format("{0}", CatchClass.ExMessage(ex, "ValidacionesCrediplusController", "Get"));
            }

            if (mRetorna.Length > 0) mRetorna = string.Format("Errores en datos de cliente:{0}", mRetorna);
            return mRetorna;
        }

    }
}
