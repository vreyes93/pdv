﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;

namespace FiestaNETRestServices.Controllers.Tools
{
    public class ActivarLog4NetController : ApiController
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public class Retorna
        {
            public bool log4net { get; set; }
        }

        // GET api/activarlog4net
        public Retorna GetLog4Net()
        {
            bool mLog4Net = false;
            Retorna mRetorna = new Retorna();
            EXACTUSERPEntities db = new EXACTUSERPEntities();

            try
            {
                if ((from c in db.MF_Configura select c).First().ACTIVAR_LOG4NET == "S") mLog4Net = true;
            }
            catch (Exception ex)
            {
                string m = ex.InnerException.Message;
            }

            mRetorna.log4net = mLog4Net;
            return mRetorna;
        }

        // GET api/activarlog4net/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/activarlog4net
        public void Post([FromBody]string value)
        {
        }

        // PUT api/activarlog4net/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/activarlog4net/5
        public void Delete(int id)
        {
        }
    }
}
