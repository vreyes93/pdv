﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MF_Clases;
using FiestaNETRestServices.Utils;
using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Models;
using System.Text;
using System.Net.Http.Headers;
using System.Web;

namespace FiestaNETRestServices.Controllers.Tools
{

    public class ChequeBAMDolaresController : MFApiController
    {
        public HttpResponseMessage GetChequeBamDolares(string id)
        {
            var response = new HttpResponseMessage();
            StringBuilder mensaje = new StringBuilder();
            //Utils.Utilitarios mUtilitarios = new Utils.Utilitarios();

            EXACTUSERPEntities db = new EXACTUSERPEntities();
            log.Info("Iniciando consulta de cheques.");
            DateTime mFechaConsulta;

            mensaje.Append(Utilitario.CorreoInicioFormatoRojo());

            if (!Utilitario.FechaValida(id))
            {
                response.Content = new StringContent("<br>Favor de validar el formato de la fecha <b>YYYY-MM-DD</b>. Ejemplo: <b>2019-03-21</b></br>");
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                return response;
            }

            mFechaConsulta = Utilitario.Fecha(id);
                var respuesta = from mb in db.MOV_BANCOS
                                where mb.CUENTA_BANCO == "31-4004688-0"
                                && mb.TIPO_DOCUMENTO == "CHQ"
                                && mb.FECHA == mFechaConsulta
                                //&& mb.NUMERO == id
                                select new { CHEQUE = mb.NUMERO, MONTO = mb.MONTO, ANOMBREDE = mb.PAGADERO_A, FECHA = mb.FECHA };

                if (respuesta.Count() > 0)
                {
                    mensaje.Append("<table align='center' class='style4'>");
                    mensaje.Append("<tr>");
                    mensaje.Append(Utilitario.FormatoTituloIzquierdo("CHEQUE"));
                    mensaje.Append(Utilitario.FormatoTituloIzquierdo("MONTO"));
                    mensaje.Append(Utilitario.FormatoTituloIzquierdo("FECHA"));
                    mensaje.Append(Utilitario.FormatoTituloIzquierdo("A NOMBRE DE"));

                    mensaje.Append("</tr>");
                    foreach (var item in respuesta)
                    {
                        mensaje.Append("<tr>");
                        mensaje.Append(Utilitario.FormatoCuerpoIzquierda(item.CHEQUE.ToString()));
                        mensaje.Append(Utilitario.FormatoCuerpoDerecha(Utilitario.FormatoNumeroDecimal(item.MONTO)));
                        mensaje.Append(Utilitario.FormatoCuerpoCentro(Utilitario.FormatoDDMMYYYY(item.FECHA)));
                        mensaje.Append(Utilitario.FormatoCuerpoIzquierda(HttpUtility.HtmlEncode(item.ANOMBREDE.ToString())));
                        mensaje.Append("</tr>");
                    }
                    mensaje.Append("</table><br/>");
                }
                else
                    mensaje.Append("No existen datos de Cheque");

            mensaje.Append(Utilitario.CorreoFin());
            response.Content = new StringContent(mensaje.ToString());
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }
        
        // POST api/chequebdr
        public void Post([FromBody]string value)
        {
        }

        // PUT api/chequebdr/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/chequebdr/5
        public void Delete(int id)
        {
        }
    }
}