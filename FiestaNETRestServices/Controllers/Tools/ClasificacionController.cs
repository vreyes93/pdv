﻿using FiestaNETRestServices.Content.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Tools
{
    public class ClasificacionController : MFApiController
    {
        //obtiene la clasificacion de un artículo
        public bool Get(string id)
        {
            DAL.DALClasificacion clasificacion = new DAL.DALClasificacion();
            return clasificacion.EsDecoracion(id);
        }
    }
}
