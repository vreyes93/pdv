﻿using MF_Clases;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Web.Configuration;
using System.Web.Http;
using FiestaNETRestServices.DAL;
using MF_Clases.Facturacion;
using MF_Clases.Restful;
using Newtonsoft.Json;
using System.IO;
using System.Net.Mime;
using FiestaNETRestServices.Content.Abstract;
using MF.Comun.Dto;
using System.Linq;
using System.Globalization;
using static MF_Clases.Clases;
using FiestaNETRestServices.Models;

namespace FiestaNETRestServices.Controllers.Tools
{
    
    public class EmailController : MFApiController
    {
        private string EmailSender = "facturas@mueblesfiesta.com";
        private new static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [HttpPost]
        public IHttpActionResult EnviarEmail([FromBody] Correo email)
        {
            Utils.Utilitarios utilitarios = new Utils.Utilitarios();
        
            try
            {
                log.Debug("Enviando correo  de "+email.Asunto);
               
              var Respuesta = utilitarios.EnviarEmail(email.Remitente, email.Destinatarios, email.Asunto, email.CuerpoCorreo, email.NombreAMostrar, email.lstImagenes, email.ConCopiaOcultaA);
             } catch (Exception ex)
            {
                log.Error("Ocurrió un error al intentar enviar el correo",ex);
                return BadRequest();
}
            return Ok();
        }
        [HttpGet]
        [ActionName("EnviarNotaCreditoDebito")]
        [Route("~api/Email/EnviarNotaCreditoDebito?id={id}")]
        public IHttpActionResult EnviarNotaCreditoDebito(string id)
        {
            try
            {
                
                log.Info("Enviar Nota Crédito / Débito: Iniciado");
                string mAmbiente = WebConfigurationManager.AppSettings["Ambiente"];
                if (string.IsNullOrEmpty(id) || string.IsNullOrWhiteSpace(id))
                    throw new Exception("el código del documento es necesario");
                Garantia documento = new DALComunes().NcND(id);
                if (documento == null)
                    throw new Exception("documento no encontrado.");
                if (string.IsNullOrEmpty(documento.Email) || string.IsNullOrWhiteSpace(documento.Email) || documento.Email.ToUpper().Contains("notiene"))
                {
                    log.Info("Email del Documento: " + documento.Factura + ", no se envía porque no hay un correo electrónico a donde enviar");
                    return Ok(false);
                }
                else
                {
                    Api api = new Api(General.PortalService);
                    List<string> mDestinatarios = new List<string>();
                    List<string> mBcc = new List<string>();
                    MemoryStream archivo = new MemoryStream();
                    List<Attachment> attachments = new List<Attachment>();
                    List<ImagenAdjunta> lstImagenes = new List<ImagenAdjunta>();
                    string DisplayName = "";
                    string uriFactura = string.Format("/Report/Factura/{0}/{1}", documento.Factura, "BYTE");
                    DisplayName = "Productos Múltiples - Documentos Electrónicos Generados";

                    string responseFactura = api.Process(RestSharp.Method.GET, uriFactura, null);
                    archivo = new MemoryStream(JsonConvert.DeserializeObject<byte[]>(responseFactura));
                    attachments.Add(new Attachment(archivo, string.Format("{0}.pdf", documento.Factura), "application/pdf"));
                    mDestinatarios.Add(documento.Email);


                    MF_Catalogo qBcc = db.MF_Catalogo.Where(x => x.CODIGO_TABLA == MF_Clases.Utils.CONSTANTES.CATALOGOS.GUATEFACTURAS_FEL && x.CODIGO_CAT == MF_Clases.Utils.CONSTANTES.CATALOGOS.COPIA_CORREO_FIRMA_NC_ND).FirstOrDefault();
                    List<string> Bcc = qBcc.NOMBRE_CAT.Split('|').ToList();
                    Bcc.ForEach(x => mBcc.Add(x));


                    if (General.Ambiente !="PRO")
                    {
                        mDestinatarios.Clear();
                        mBcc.Clear();
                        List<MF_Catalogo> qDest = db.MF_Catalogo.Where(x => x.CODIGO_TABLA == MF_Clases.Utils.CONSTANTES.CATALOGOS.NOTIFICACIONES_DESARROLLO_PRUEBAS).ToList();
                        qDest.ForEach(x => mDestinatarios.Add(x.NOMBRE_CAT));
                       
                    }

                    StringBuilder mBody = EmailBody(documento.Nombre, true, false, false);
                    string asunto = "Documento Electrónico Productos Mültiples, S.A.";

                    Utilitario.EnviarEmail(this.EmailSender, mDestinatarios, asunto, mBody, DisplayName, lstImagenes, mBcc, attachments);

                    return Ok(true);

                }
            }
            catch (Exception ex)
            {
                log.Error("Problema al enviar el correo con la nota de crédito o débito al cliente " + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult EnviarFacturaGarantia(string codigoFactura)
        {
            try
            {
                log.Info("Enviar GarantiaFactura: Iniciado");
                string mAmbiente = WebConfigurationManager.AppSettings["Ambiente"];

                if (string.IsNullOrEmpty(codigoFactura) || string.IsNullOrWhiteSpace(codigoFactura))
                    throw new Exception("el código de factura es necesario");

                Garantia factura = new DALComunes().facturaGarantia(codigoFactura);

                if (factura == null)
                    throw new Exception("Factura no encontrada.");

                if (string.IsNullOrEmpty(factura.Email) || string.IsNullOrWhiteSpace(factura.Email) || factura.Email.ToUpper().Contains("notiene"))
                {
                    log.Info("Factura: " + factura.Factura + ", no se envía porque no hay un correo electrónico a donde enviar");
                    return Ok(false);
                }
                else
                {
                    Api api = new Api(General.PortalService);
                    bool esMayoreo = factura.CodigoCliente[0].ToString().ToUpper() == "M";
                    bool esOutlet = factura.Visibilidad == "OUTLET";
                    List<string> mDestinatarios = new List<string>();
                    MemoryStream archivo = new MemoryStream();
                    List<Attachment> attachments = new List<Attachment>();
                    List<ImagenAdjunta> lstImagenes = new List<ImagenAdjunta>();
                    string DisplayName = "";

                    string uriGarantia = string.Format("/Report/CertificadoGarantia/{0}/{1}", factura.Factura, "BYTE");
                    string uriFactura = string.Format("/Report/Factura/{0}/{1}", factura.Factura, "BYTE");
                    string uriFacturaMayoreo = string.Format("/Report/FacturaMayoreo/{0}/{1}", factura.Factura, "BYTE");

                    if (esMayoreo)
                    {
                        DisplayName = "Facturas Productos Multiples";
                        lstImagenes.Add(new ImagenAdjunta(System.Web.Hosting.HostingEnvironment.MapPath("~//Images/LogoPM.jpg"), MediaTypeNames.Image.Jpeg, "LogoMF"));

                        string responseFacturaMayoreo = api.Process(RestSharp.Method.GET, uriFacturaMayoreo, null);
                        archivo = new MemoryStream(JsonConvert.DeserializeObject<byte[]>(responseFacturaMayoreo));
                        attachments.Add(new Attachment(archivo, string.Format("{0}.pdf", factura.Factura), "application/pdf"));
                    }
                    else
                    {
                        DisplayName = esOutlet ? "Facturas Muebles Fiesta Outlet" : "Facturas Muebles Fiesta";
                        if (esOutlet)
                            lstImagenes.Add(new ImagenAdjunta(System.Web.Hosting.HostingEnvironment.MapPath("~//Images/LogoOutlet.jpg"), MediaTypeNames.Image.Jpeg, "LogoOL"));
                        else
                            lstImagenes.Add(new ImagenAdjunta(System.Web.Hosting.HostingEnvironment.MapPath("~//Images/LogoMF.jpg"), MediaTypeNames.Image.Jpeg, "LogoMF"));

                        string responseGarantia = api.Process(RestSharp.Method.GET, uriGarantia, null);
                        archivo = new MemoryStream(JsonConvert.DeserializeObject<byte[]>(responseGarantia));
                        attachments.Add(new Attachment(archivo, string.Format("Garantia {0}.pdf", factura.Factura), "application/pdf"));

                        string responseFactura = api.Process(RestSharp.Method.GET, uriFactura, null);
                        archivo = new MemoryStream(JsonConvert.DeserializeObject<byte[]>(responseFactura));
                        attachments.Add(new Attachment(archivo, string.Format("Factura {0}.pdf", factura.Factura), "application/pdf"));
                    }


                    mDestinatarios.Add(factura.Email);
                    if (mAmbiente == "DES" || mAmbiente == "PRU")
                    {
                        mDestinatarios.Clear();
                        mDestinatarios.Add("noreply@mueblesfiesta.com");
                    }

                    StringBuilder mBody = EmailBody(factura.Nombre, esMayoreo, esOutlet);
                    string asunto = "";

                    if(esMayoreo)
                        asunto = "Factura electrónica Productos Múltiples, S.A.";
                    else
                        asunto = esOutlet == true ? "Muebles Fiesta Outlet: Factura electrónica y garantía" : "Muebles Fiesta: Factura electrónica y garantía";

                    Utilitario.EnviarEmail(this.EmailSender, mDestinatarios, asunto, mBody, DisplayName, lstImagenes, null, attachments);
                    
                    return Ok(true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                log.Error("Exception: " + errorMessage);
                return BadRequest(errorMessage);
            }
            finally
            {
                log.Info("Enviar GarantiaFactura: Finalizado");
            }
        }

        [ActionName("EnviarResultadoComisiones")]
        [HttpGet]
        [Route("~api/Email/EnviarResultadoComisiones?pFechaInicio={pFechaInicio}&pFechaFin={pFechaFin}&pNomina={pNomina}&pcodNomina={pcodNomina}")]
        public List<ResultadoComisiones> EnviarResultadoComisiones(string pFechaInicio, string pFechaFin)
        {
            List<ResultadoComisiones> lstComisiones = new List<ResultadoComisiones>();
           
            try
            {
                Utils.Utilitarios utilitarios = new Utils.Utilitarios();

                DateTime FechaInicio = utilitarios.Fecha(pFechaInicio);
                DateTime FechaFin = utilitarios.Fecha(pFechaFin);

                log.Info(string.Format("Enviando el resultado de la generación de las comisiones del {0} al {1}", pFechaInicio, pFechaFin));
                DALComunes comunes = new DALComunes();
                lstComisiones = comunes.ObtenerResultadosComisiones(FechaInicio, FechaFin);

                //armar el cuerpo del correo y enviarlo(3) intentos
                var lstExito = lstComisiones.Where(x => x.Resultado == "EXITO").ToList();
                var lstNoNomina = lstComisiones.Where(x => x.Resultado == "NO ESTÁ EN LA NÓMINA").ToList();
                var lstNoActivo = lstComisiones.Where(x => x.Resultado == "NO ESTA ACTIVO").ToList();
                var lstNoRegistroNomina = lstComisiones.Where(x => x.Resultado == "EMPLEADO NO TIENE REGISTRO EN NOMINA").ToList();
                var lstComisionCero = lstComisiones.Where(x => x.Resultado == "COMISION CERO").ToList();
                string Cuerpo = Utilitario.CorreoInicioFormatoRojo()+" <br/> A continuación se muestra el resultado de la generación de la nómina ";
                Cuerpo += "<BR/>";
                if (lstComisiones.Count() == 0)
                    Cuerpo += "No hay registros para mostrar.<BR/>";
                if (lstExito.Count() > 0)
                {
                    Cuerpo += $"<BR/><h3 align='center'>COMISIONES GRABADAS EXITOSAMENTE {lstExito.Count()}</h3>";
                    Cuerpo += "<table align='center' class='style4'>";
                    Cuerpo += "<tr>";
                    Cuerpo += Utilitario.FormatoTituloCentro("Tienda");
                    Cuerpo += Utilitario.FormatoTituloCentro("Empleado");
                    Cuerpo += Utilitario.FormatoTituloCentro("Vendedor");
                    Cuerpo += Utilitario.FormatoTituloCentro("Nombre");
                    Cuerpo += Utilitario.FormatoTituloCentro("Monto Comisión");
                    Cuerpo += "</tr>";
                    CultureInfo cultureToUse = new CultureInfo("es-GT");
                    cultureToUse.NumberFormat.CurrencyDecimalDigits = 2;
                    cultureToUse.NumberFormat.NumberDecimalDigits = 2;
                    lstExito.ForEach(x =>
                    {
                        Cuerpo += "<tr>" + Utilitario.FormatoCuerpoCentro(x.Tienda) + Utilitario.FormatoCuerpoCentro(x.Empleado) + Utilitario.FormatoCuerpoCentro(x.Vendedor) + Utilitario.FormatoCuerpoCentro(x.Nombre) + Utilitario.FormatoCuerpoCentro(x.MontoComision.ToString()) + "</tr>";
                    });
                    Cuerpo += "</tr></table>";
                }
                if (lstNoNomina.Count() > 0)
                {
                    Cuerpo += $"<BR/><h3 align='center'>NO ESTÁN EN LA NÓMINA (NO PROCESADOS {lstNoNomina.Count()})</h3>";

                    Cuerpo += "<table align='center' class='style4'>";
                    Cuerpo += "<tr>";
                    Cuerpo += Utilitario.FormatoTituloCentro("Tienda");
                    Cuerpo += Utilitario.FormatoTituloCentro("Empleado");
                    Cuerpo += Utilitario.FormatoTituloCentro("Vendedor");
                    Cuerpo += Utilitario.FormatoTituloCentro("Nombre");
                    Cuerpo += Utilitario.FormatoTituloCentro("Monto Comisión");
                    Cuerpo += "</tr>";

                    lstNoNomina.ForEach(x =>
                    {
                        Cuerpo += "<tr>" + Utilitario.FormatoCuerpoCentro(x.Tienda) + Utilitario.FormatoCuerpoCentro(x.Empleado) + Utilitario.FormatoCuerpoCentro(x.Vendedor) + Utilitario.FormatoCuerpoCentro(x.Nombre) + Utilitario.FormatoCuerpoCentro(x.MontoComision.ToString()) + "</tr>";
                    });
                    Cuerpo += "</tr></table>";

                }
                if (lstNoActivo.Count() > 0)
                {
                    Cuerpo += $"<BR/><h3 align='center'>NO ESTA ACTIVO (NO PROCESADOS { lstNoActivo.Count()})</h3>";

                    Cuerpo += "<table align='center' class='style4'>";
                    Cuerpo += "<tr>";
                    Cuerpo += Utilitario.FormatoTituloCentro("Tienda");
                    Cuerpo += Utilitario.FormatoTituloCentro("Empleado");
                    Cuerpo += Utilitario.FormatoTituloCentro("Vendedor");
                    Cuerpo += Utilitario.FormatoTituloCentro("Nombre");
                    Cuerpo += Utilitario.FormatoTituloCentro("Monto Comisión");
                    Cuerpo += "</tr>";

                    lstNoActivo.ForEach(x =>
                    {
                        Cuerpo += "<tr>" + Utilitario.FormatoCuerpoCentro(x.Tienda) + Utilitario.FormatoCuerpoCentro(x.Empleado) + Utilitario.FormatoCuerpoCentro(x.Vendedor) + Utilitario.FormatoCuerpoCentro(x.Nombre) + Utilitario.FormatoCuerpoCentro(x.MontoComision.ToString()) + "</tr>";
                    });
                    Cuerpo += "</tr></table>";
                }
                if (lstNoRegistroNomina.Count() > 0)
                {
                    Cuerpo += $"<BR/><h3 align='center'>VENDEDOR NO TIENE CÓDIGO DE EMPLEADO ASOCIADO (NO PROCESADOS {lstNoRegistroNomina.Count()})</h3>";

                    Cuerpo += "<table align='center' class='style4'>";
                    Cuerpo += "<tr>";
                    Cuerpo += Utilitario.FormatoTituloCentro("Tienda");
                    Cuerpo += Utilitario.FormatoTituloCentro("Empleado");
                    Cuerpo += Utilitario.FormatoTituloCentro("Vendedor");
                    Cuerpo += Utilitario.FormatoTituloCentro("Nombre");
                    Cuerpo += Utilitario.FormatoTituloCentro("Monto Comisión");
                    Cuerpo += "</tr>";

                    lstNoRegistroNomina.ForEach(x =>
                    {
                        Cuerpo += "<tr>" + Utilitario.FormatoCuerpoCentro(x.Tienda) + Utilitario.FormatoCuerpoCentro(x.Empleado) + Utilitario.FormatoCuerpoCentro(x.Vendedor) + Utilitario.FormatoCuerpoCentro(x.Nombre) + Utilitario.FormatoCuerpoCentro(x.MontoComision.ToString()) + "</tr>";
                    });
                    Cuerpo += "</tr></table>";
                }
                if (lstComisionCero.Count() > 0)
                {
                    Cuerpo += $"<BR/><h3 align='center'>EMPLEADO CON COMISION CERO ({lstComisionCero.Count()})</h3>";

                    Cuerpo += "<table align='center' class='style4'>";
                    Cuerpo += "<tr>";
                    Cuerpo += Utilitario.FormatoTituloCentro("Tienda");
                    Cuerpo += Utilitario.FormatoTituloCentro("Empleado");
                    Cuerpo += Utilitario.FormatoTituloCentro("Vendedor");
                    Cuerpo += Utilitario.FormatoTituloCentro("Nombre");
                    Cuerpo += Utilitario.FormatoTituloCentro("Monto Comisión");
                    Cuerpo += "</tr>";

                    lstComisionCero.ForEach(x =>
                    {
                        Cuerpo += "<tr>" + Utilitario.FormatoCuerpoCentro(x.Tienda) + Utilitario.FormatoCuerpoCentro(x.Empleado) + Utilitario.FormatoCuerpoCentro(x.Vendedor) + Utilitario.FormatoCuerpoCentro(x.Nombre) + Utilitario.FormatoCuerpoCentro(x.MontoComision.ToString()) + "</tr>";
                    });
                    Cuerpo += "</tr></table>";
                }
                
                Cuerpo += "<br/><sub>Fecha-Hora generación: " + DateTime.Now +"</sub>";
                Cuerpo += Utilitario.CorreoFin();

                DALComunes dalc = new DALComunes();
                Respuesta car = dalc.ObtenerCatalogo(General.Ambiente == "PRO" ? 46 : 63);
                List<string> destinatarios = new List<string>();
                if (car.Objeto != null)
                {
                    List<Catalogo> dest = new List<Catalogo>();
                    dest = (List<Catalogo>)car.Objeto;
                    dest.ForEach(d =>
                    {
                        destinatarios.Add(d.Descripcion);
                    });
                }

                Respuesta rem = dalc.ObtenerCatalogo(20);
                string remitente =string.Empty;
                if (rem.Objeto != null)
                {
                    List<Catalogo> re = new List<Catalogo>();
                    re = (List<Catalogo>)rem.Objeto;
                    re.ForEach(d =>
                    {
                        remitente =d.Descripcion;
                    });
                }
                StringBuilder mBody = new StringBuilder();
                mBody.Append(Cuerpo);
                var Respuesta = utilitarios.EnviarEmail(remitente, destinatarios, "COMISIONES GENERADAS", mBody, "PUNTO DE VENTA -NOTIFICACIONES-");

                log.Info("Comisiones enviadas con éxito");
            }
            catch (Exception ex)
            {
               
                log.Error("Problema para enviar la notificación del resultado de la operación de las comisiones" +ex.Message);
            }
            return lstComisiones;
        }

       
        [HttpPost]
        public bool EnviarNotificacionAnulaciónFactura([FromBody]FacturaAnular fact)
        {
            Utils.Utilitarios utilitarios = new Utils.Utilitarios();
            bool blExito = true;
            try
            {
                MemoryStream archivo = new MemoryStream();
                List<Attachment> attachments = new List<Attachment>();
                Api api = new Api(General.PortalService);
                StringBuilder mBody = EmailAnulacionBody(fact.Nombre, fact.Factura, fact.Fecha);
                List<string> mDestinatarios=new List<string>();
                try
                {
                    string uriFactura = string.Format("/Report/Factura/{0}/{1}", fact.Factura, "BYTE");
                    string responseFactura = api.Process(RestSharp.Method.GET, uriFactura, null);
                    archivo = new MemoryStream(JsonConvert.DeserializeObject<byte[]>(responseFactura));
                    attachments.Add(new Attachment(archivo, string.Format("Factura {0}.pdf", fact.Factura), "application/pdf"));
                }
                catch
                { }

                //cliente
                var strDest = dbExactus.CLIENTE.Where(x => x.CLIENTE1 == fact.Cliente).Select(x => x.E_MAIL).FirstOrDefault();
                //imagen adjunta
                List<ImagenAdjunta> lstImagenes = new List<ImagenAdjunta>();
                lstImagenes.Add(new ImagenAdjunta(System.Web.Hosting.HostingEnvironment.MapPath("~//Images/LogoMF.jpg"), MediaTypeNames.Image.Jpeg, "LogoMF"));

                Garantia factura = new DALComunes().facturaGarantia(fact.Factura);


                if ((string.IsNullOrEmpty(strDest) || string.IsNullOrWhiteSpace(strDest) || strDest.ToUpper().Contains("notiene")) == false)
                {
                    mDestinatarios.Add(strDest);
                }
                    
                    if (General.Ambiente != "PRO")
                    {
                        mDestinatarios.Clear();
                        List<MF_Catalogo> qDest = dbExactus.MF_Catalogo.Where(x => x.CODIGO_TABLA == MF_Clases.Utils.CONSTANTES.CATALOGOS.NOTIFICACIONES_DESARROLLO_PRUEBAS).ToList();
                        qDest.ForEach(x => mDestinatarios.Add(x.NOMBRE_CAT));

                    }

                    if(mDestinatarios.Count()>0)
                    utilitarios.EnviarEmail(this.EmailSender, mDestinatarios, General.Ambiente.Equals("PRO") ? "Notificaciones - Muebles Fiesta -" : "Notificaciones en PRUEBAS anulación de facturas", mBody, "Facturas Muebles Fiesta", lstImagenes, adjuntos: attachments);

            }
            catch (Exception ex)
            {
                blExito = false;
                log.Error("No se envió la notificación de la factura anulada ["+fact.Factura+"] al cliente "+ex.Message);
            }
            return blExito;
        }
        #region Private
        private StringBuilder EmailBody(string nombreCliente, bool esMayoreo, bool esOutlet,bool esFactura=true)
        {
            string body = "";
            string header = "<!doctype html>" +
                            " <html lang='es'>" +
                            " <head>" +
                              " <meta charset='utf-8'>" +
                            " </head>" +
                            " <body>";
            string style = "</body>" +
                            " </html>" +
                            " <style>" +
                                " .container{" +
                                    " width: 600px;" +
                                    " margin: 0 auto;" +
                                    " overflow: hidden;" +
                                    " padding: 15px;" +
                                    " font-family: arial;" +
                                    " font-size: 18px;" +
                                " }" +
                                " .container > .logo{" +
                                    " width: 200px;" +
                                " }" +
                                " p.justify{" +
                                    " text-align: justify;" +
                                " }" +
                            " </style>";
            if (esFactura)
            {
                if (esMayoreo)
                    body = "<div class='container'>" +
                            " <p class='justify'>Estimado socio comercial, adjunto encontrará la Factura Electrónica en la cual se detallan los productos adquiridos por su empresa.</p>" +
                            " <p>Saludos cordiales.</p>" +
                            " <img class='logo' src =cid:LogoMF>" +
                          " </div>";
                else
                {
                    if (esOutlet)
                        body = "<div class='container'>" +
                            " <p><strong>¡Bienvenido(a) a la familia Muebles fiesta!</strong></p>" +
                            " <p>Estimado(a) " + nombreCliente + " </p>" +
                            " <p class='justify'>Adjunto encontrarás la Factura Electrónica en la cual se detallan los productos que adquiriste y el Certificado de Garantía de Muebles Fiesta Outlet, lee detenidamente la información y si tienes alguna pregunta, no dudes en comunicarte:</p>" +
                            " <p>" +
                                " <ul>" +
                                    " <li>Teléfono: 6626-1200</li>" +
                                    " <li>Extensión capital: 104</li>" +
                                    " <li>Extensión departamental: 144</li>" +
                                " </ul>" +
                            " </p>" +
                            " <p>¡Gracias por preferirnos!</p>" +
                            " <img class='logo' src =cid:LogoOL>" +
                          " </div>";
                    else
                        body = "<div class='container'>" +
                            " <p><strong>¡Bienvenido(a) a la familia Muebles fiesta!</strong></p>" +
                            " <p>Estimado(a) " + nombreCliente + " </p>" +
                            " <p class='justify'>Adjunto encontrarás la Factura Electrónica en la cual se detallan los productos que adquiriste y el Certificado de Garantía de Muebles Fiesta, lee detenidamente la información y si tienes alguna pregunta, no dudes en comunicarte:</p>" +
                            " <p>" +
                                " <ul>" +
                                    " <li>Teléfono: 6626-1200</li>" +
                                    " <li>Extensión capital: 104</li>" +
                                    " <li>Extensión departamental: 144</li>" +
                                " </ul>" +
                            " </p>" +
                            " <p>¡Gracias por preferirnos!</p>" +
                            " <img class='logo' src =cid:LogoMF>" +
                          " </div>";
                }
            }
            else
            {
                    body = "<div class='container'>" +
                            " <p class='justify'>Estimado socio comercial, adjunto encontrará el documento electrónico emitido para su empresa.</p>" +
                            " <p>Saludos cordiales.</p>" +
                          " </div>";
            }


            return new StringBuilder().Append(header).Append(body).Append(style);
        }
        private StringBuilder EmailAnulacionBody(string NombreCliente, string factura,DateTime dtFechaEmision)
        {

            string body = "";
            string header = "<!doctype html>" +
                            " <html lang='es'>" +
                            " <head>" +
                              " <meta charset='utf-8'>" +
                            " </head>" +
                            " <body>";
            string style = "</body>" +
                            " </html>" +
                            " <style>" +
                                " .container{" +
                                    " width: 600px;" +
                                    " margin: 0 auto;" +
                                    " overflow: hidden;" +
                                    " padding: 15px;" +
                                    " font-family: arial;" +
                                    " font-size: 18px;" +
                                " }" +
                                " .container > .logo{" +
                                    " width: 200px;" +
                                " }" +
                                " p.justify{" +
                                    " text-align: justify;" +
                                " }" +
                            " </style>";


            body = "<div class='container'>" +
                " <p><strong>¡Bienvenido(a) a la familia Muebles fiesta!</strong></p>" +
                " <p>Estimado(a) " + NombreCliente + " </p>" +
                " <p class='justify'> Por este medio hacemos de tu conocimiento que se ha anulado la factura " + factura + " con fecha de emisión "+ MF_Clases.Utilitarios.FormatoDDMMYYYY(dtFechaEmision) + ", si tienes alguna pregunta, no dudes en comunicarte:</p>" +
                " <p>" +
                    " <ul>" +
                        " <li>Teléfono: 6626-1111</li>" +
                        " <li>Extensión capital: 104</li>" +
                        " <li>Extensión departamental: 144</li>" +
                    " </ul>" +
                " </p>" +
                " <p>¡Gracias por preferirnos!</p>" +
                " <img class='logo' src =cid:LogoMF>" +
              " </div>";

            return new StringBuilder().Append(header).Append(body).Append(style);
        }
        #endregion
    }
}
