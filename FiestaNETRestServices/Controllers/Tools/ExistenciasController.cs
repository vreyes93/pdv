﻿using System;
using System.Linq;
using System.Data;
using System.Web.Http;
using FiestaNETRestServices.Models;
using MF_Clases;

namespace FiestaNETRestServices.Controllers.Tools
{
    public class ExistenciasController : ApiController
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET api/existencias/5
        public Clases.RetornaExistencias GetExistencias(string id)
        {
            Clases.RetornaExistencias mRetorna = new Clases.RetornaExistencias();
            
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            db.CommandTimeout = 999999999;
                        
            try
            {
                if (id == null)
                {
                    Clases.RetornaExito mItemExito = new Clases.RetornaExito();
                    mItemExito.exito = false;
                    mItemExito.mensaje = "Debe ingresar un criterio de búsqueda.";
                    mRetorna.RetornaExito.Add(mItemExito);
                    return mRetorna;
                }
                if (id.Trim().Length == 0)
                {
                    Clases.RetornaExito mItemExito = new Clases.RetornaExito();
                    mItemExito.exito = false;
                    mItemExito.mensaje = "Debe ingresar un criterio de búsqueda.";
                    mRetorna.RetornaExito.Add(mItemExito);
                    return mRetorna;
                }

                DateTime mFechaValidar = (DateTime)(from c in db.MF_Configura select c).First().FECHA_VALIDAR_RESERVAS;
                string filtro = id.ToUpper();

                var qArticulos = db.ARTICULO
                                .Where(x => x.ACTIVO == "S"
                                && (filtro.Length == 0 ? x.DESCRIPCION == x.DESCRIPCION : x.DESCRIPCION.Contains(filtro))
                                || (filtro.Length == 0 ? x.ARTICULO1 == x.ARTICULO1 : x.ARTICULO1.Contains(filtro)))
                                .OrderBy(x => x.ARTICULO1)
                                .Select(x => new { Articulo = x.ARTICULO1, Descripcion = x.DESCRIPCION }).ToList();

                int ii = 0;
                foreach (var item in qArticulos)
                {
                    int mExistenciasF01 = 0; int mReservas = 0; string mTooltip = "Existencias en F01:"; string mTooltipReservas = "";
                    var qF01 = from e in db.EXISTENCIA_LOTE where e.ARTICULO == item.Articulo && e.BODEGA == "F01" && e.CANT_DISPONIBLE > 0 select e;

                    bool mValidarReservas = !(item.Articulo.StartsWith("0") ||
                                              item.Articulo.StartsWith("S") ||
                                              item.Articulo.StartsWith("AIA") ||
                                              item.Articulo.StartsWith("141") ||
                                              item.Articulo.StartsWith("142") ||
                                              item.Articulo.StartsWith("149") ||
                                              item.Articulo.StartsWith("M141") ||
                                              item.Articulo.StartsWith("M142") ||
                                              item.Articulo.StartsWith("M149"));

                    foreach (var itemLote in qF01)
                    {
                        Int32 mCantidad = Convert.ToInt32(itemLote.CANT_DISPONIBLE);
                        mExistenciasF01 += mCantidad;

                        mTooltip = string.Format("{0}{1}{2}: {3}", mTooltip, System.Environment.NewLine, itemLote.LOCALIZACION, mCantidad.ToString());
                    }

                    ii += 1;
                    int mExistenciasTiendas = 0; int mDisponibleTiendas = 0;
                    var qTiendas = from e in db.EXISTENCIA_LOTE where e.ARTICULO == item.Articulo && e.BODEGA != "F01" && (e.BODEGA.StartsWith("F") || e.BODEGA.StartsWith("B")) && e.CANT_DISPONIBLE > 0 orderby e.BODEGA, e.LOCALIZACION select e;

                    foreach (var itemLote in qTiendas)
                    {
                        int mReservasTienda = 0; string mTooltipTienda = "";
                        Int32 mCantidad = Convert.ToInt32(itemLote.CANT_DISPONIBLE);
                        mExistenciasTiendas += mCantidad;

                        //Reservas en Tiendas
                        if (mValidarReservas)
                        {
                            var qTienda = from fl in db.FACTURA_LINEA
                                join f in db.FACTURA on fl.FACTURA equals f.FACTURA1 join v in db.VENDEDOR on f.VENDEDOR equals v.VENDEDOR1 join c in db.CLIENTE on f.CLIENTE equals c.CLIENTE1
                                where fl.BODEGA == itemLote.BODEGA && fl.ANULADA == "N" && fl.CANT_DESPACHADA < fl.CANTIDAD && fl.FECHA_FACTURA >= mFechaValidar && fl.TIPO_DOCUMENTO == "F" && fl.ARTICULO == item.Articulo
                                      && f.ANULADA == "N"
                                orderby fl.BODEGA
                                select new { Factura = f.FACTURA1, Bodega = fl.BODEGA, Vendedor = v.NOMBRE, Cantidad = fl.CANTIDAD, Cliente = f.CLIENTE, Nombre = c.NOMBRE, Pedido = f.PEDIDO };

                            int jj = 0;
                            foreach (var itemTienda in qTienda)
                            {
                                string mSeparador = "";
                                mReservasTienda += Convert.ToInt32(itemTienda.Cantidad);

                                jj += 1;
                                if (jj > 1) mSeparador = System.Environment.NewLine;

                                mTooltipTienda = string.Format("{0}{1}Factura: {2}, Vendedor: {3}, Bodega: {4}, Cliente: {5} - {6}, Pedido: {7}", mTooltipTienda, mSeparador, itemTienda.Factura, itemTienda.Vendedor, itemTienda.Bodega, itemTienda.Cliente, itemTienda.Nombre, itemTienda.Pedido);
                            }

                            if (mReservasTienda > mCantidad) mReservasTienda = mCantidad;
                        }

                        int mDisponibleTienda = mCantidad - mReservasTienda;
                        mDisponibleTiendas += mDisponibleTienda;

                        Clases.ExistenciasTiendas mItemExistenciasTiendas = new Clases.ExistenciasTiendas();
                        mItemExistenciasTiendas.id = ii;
                        mItemExistenciasTiendas.Articulo = item.Articulo;
                        mItemExistenciasTiendas.Descripcion = item.Descripcion;
                        mItemExistenciasTiendas.Existencia = mCantidad;
                        mItemExistenciasTiendas.Reserva = mReservasTienda;
                        mItemExistenciasTiendas.Disponible = mDisponibleTienda;
                        mItemExistenciasTiendas.DisponibleTiendas = 0;
                        mItemExistenciasTiendas.Localizacion = itemLote.LOCALIZACION;
                        mItemExistenciasTiendas.Tienda = itemLote.BODEGA;
                        mItemExistenciasTiendas.Tooltip = "";
                        mItemExistenciasTiendas.TooltipReservas = mTooltipTienda;
                        mItemExistenciasTiendas.ArticuloDescripcion = string.Format("{0} - {1}", item.Articulo, item.Descripcion);
                        mRetorna.ExistenciasTiendas.Add(mItemExistenciasTiendas);
                    }
                    
                    //Reservas en F01
                    if (mValidarReservas)
                    {
                        var qReservas = from fl in db.FACTURA_LINEA
                            join f in db.FACTURA on fl.FACTURA equals f.FACTURA1 join v in db.VENDEDOR on f.VENDEDOR equals v.VENDEDOR1 join c in db.CLIENTE on f.CLIENTE equals c.CLIENTE1
                            where fl.BODEGA == "F01" && fl.ANULADA == "N" && fl.CANT_DESPACHADA < fl.CANTIDAD && fl.FECHA_FACTURA >= mFechaValidar && fl.TIPO_DOCUMENTO == "F" && fl.ARTICULO == item.Articulo
                            orderby f.COBRADOR
                            select new { Factura = f.FACTURA1, Bodega = fl.BODEGA, Vendedor = v.NOMBRE, Cantidad = fl.CANTIDAD, Cliente = f.CLIENTE, Nombre = c.NOMBRE, Pedido = f.PEDIDO };

                        int jj = 0;
                        foreach (var itemReserva in qReservas)
                        {
                            string mSeparador = "";
                            mReservas += Convert.ToInt32(itemReserva.Cantidad);

                            jj += 1;
                            if (jj > 1) mSeparador = System.Environment.NewLine;

                            mTooltipReservas = string.Format("{0}{1}Factura: {2}, Vendedor: {3}, Bodega: {4}, Cliente: {5} - {6}, Pedido: {7}", mTooltipReservas, mSeparador, itemReserva.Factura, itemReserva.Vendedor, itemReserva.Bodega, itemReserva.Cliente, itemReserva.Nombre, itemReserva.Pedido);
                        }

                        if (mReservas > mExistenciasF01) mReservas = mExistenciasF01;
                    }

                    Clases.Existencias mItemExistencias = new Clases.Existencias();
                    mItemExistencias.id = ii;
                    mItemExistencias.Articulo = item.Articulo;
                    mItemExistencias.Descripcion = item.Descripcion;
                    mItemExistencias.Existencia = mExistenciasF01;
                    mItemExistencias.Reserva = mReservas;
                    mItemExistencias.Disponible = mExistenciasF01 - mReservas;
                    mItemExistencias.DisponibleTiendas = mDisponibleTiendas;
                    mItemExistencias.Tooltip = mTooltip;
                    mItemExistencias.TooltipReservas = mTooltipReservas;
                    mItemExistencias.ArticuloDescripcion = string.Format("{0} - {1}", item.Articulo, item.Descripcion);
                    mRetorna.Existencias.Add(mItemExistencias);
                }

                Clases.RetornaExito mItemExitoTrue = new Clases.RetornaExito();
                mItemExitoTrue.exito = true;
                mItemExitoTrue.mensaje = "Existencias consultadas exitosamente.";
                mRetorna.RetornaExito.Add(mItemExitoTrue);
            }
            catch (Exception ex)
            {
                Clases.RetornaExito mItemExito = new Clases.RetornaExito();
                mItemExito.exito = false;
                mItemExito.mensaje = CatchClass.ExMessage(ex, "ExistenciasController", "GetExistencias");
                mRetorna.RetornaExito.Add(mItemExito);
                return mRetorna;
            }

            return mRetorna;
        }

    }
}
