﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Data;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;
using System.Globalization;
using System.Text.RegularExpressions;
using MF_Clases;

namespace FiestaNETRestServices.Controllers.Tools
{
    public class GelController : ApiController
    {
        
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        // PUT api/gel/5
        public Clases.RetornaExito Put(int id, [FromBody]Clases.Info info)
        {
            string linea = Convert.ToString(id);
            string documento = info.Documento;

            Clases.RetornaExito mRetorna = new Clases.RetornaExito();
            EXACTUSERPEntities db = new EXACTUSERPEntities();

            mRetorna.exito = false;
            db.CommandTimeout = 999999999;

            try
            {
                log.Info("Inicio de validaciones.");

                if (linea == null || documento == null)
                {
                    mRetorna.mensaje = "Línea o documento inválido.";
                    return mRetorna;
                }

                if (linea.Trim().Length == 0 || documento.Trim().Length == 0)
                {
                    mRetorna.mensaje = "Línea o documento inválido.";
                    return mRetorna;
                }

                int mLinea = 0;
                try
                {
                    mLinea = Convert.ToInt32(linea);
                }
                catch
                {
                    mRetorna.mensaje = "Línea inválida.";
                    return mRetorna;
                }
                
                if (mLinea < 0)
                {
                    mRetorna.mensaje = "Línea inválida.";
                    return mRetorna;
                }
                
                string mTipo = "C";
                string mNuevoGel = "S";
                string mDescripcion = "";
                string mGel = " - GARANTÍA ESPECIAL LIMITADA";

                if (documento.Substring(0, 1) == "P") mTipo = "P";
                if (documento.Substring(0, 1) == "F") mTipo = "F";

                if (mTipo == "C")
                {
                    int mCotizacion = 0;
                    try
                    {
                        mCotizacion = Convert.ToInt32(documento);
                    }
                    catch
                    {
                        mCotizacion = 0;
                    }

                    if (mCotizacion <= 0)
                    {
                        mRetorna.mensaje = "Cotización inválida.";
                        return mRetorna;
                    }

                    var qCotizacion = (from c in db.MF_Cotizacion_Linea where c.COTIZACION == mCotizacion && c.COTIZACION_LINEA == mLinea select c);

                    if (qCotizacion.Count() == 0)
                    {
                        mRetorna.mensaje = "No se encontró la línea de la cotización, debe grabarla antes.";
                        return mRetorna;
                    }

                    mDescripcion = string.Format("{0}{1}", (from c in db.MF_Cotizacion_Linea join a in db.ARTICULO on c.ARTICULO equals a.ARTICULO1 where c.COTIZACION == mCotizacion && c.COTIZACION_LINEA == mLinea select a).First().DESCRIPCION, mGel);

                    if (qCotizacion.First().GEL == "S")
                    {
                        mNuevoGel = "N";
                        mDescripcion = mDescripcion.Replace(mGel, "");
                    }

                    using (TransactionScope transactionScope = new TransactionScope())
                    {
                        var qUpdateCotizacion = from c in db.MF_Cotizacion_Linea where c.COTIZACION == mCotizacion && c.COTIZACION_LINEA == mLinea select c;
                        foreach (var item in qUpdateCotizacion)
                        {
                            item.GEL = mNuevoGel;
                        }

                        db.SaveChanges();
                        transactionScope.Complete();
                    }
                }

                if (mTipo == "P")
                {
                    var qPedido = (from p in db.MF_Pedido_Linea where p.PEDIDO == documento && p.PEDIDO_LINEA == mLinea select p);

                    if (qPedido.Count() == 0)
                    {
                        mRetorna.mensaje = "No se encontró la línea del pedido, debe grabarlo antes.";
                        return mRetorna;
                    }

                    mDescripcion = string.Format("{0}{1}", (from p in db.MF_Pedido_Linea join a in db.ARTICULO on p.ARTICULO equals a.ARTICULO1 where p.PEDIDO == documento && p.PEDIDO_LINEA == mLinea select a).First().DESCRIPCION, mGel);

                    if (qPedido.First().GEL == "S")
                    {
                        mNuevoGel = "N";
                        mDescripcion = mDescripcion.Replace(mGel, "");
                    }

                    string mFactura = "";
                    if ((from f in db.FACTURA where f.PEDIDO == documento && f.ANULADA == "N" select f).Count() > 0)
                    {
                        var qFactura = (from f in db.FACTURA where f.PEDIDO == documento && f.ANULADA == "N" select f).First();
                        mFactura = qFactura.FACTURA1;

                        if (qFactura.ESTA_DESPACHADO == "S")
                        {
                            mRetorna.mensaje = string.Format("La factura {0} de este pedido ya se encuentra despachada.", mFactura);
                            return mRetorna;
                        }
                    }

                    using (TransactionScope transactionScope = new TransactionScope())
                    {
                        var qUpdatePedido = from p in db.MF_Pedido_Linea where p.PEDIDO == documento && p.PEDIDO_LINEA == mLinea select p;
                        foreach (var item in qUpdatePedido)
                        {
                            item.GEL = mNuevoGel;
                        }

                        var qUpdatePedidoEx = from p in db.PEDIDO_LINEA where p.PEDIDO == documento && p.PEDIDO_LINEA1 == mLinea select p;
                        foreach (var item in qUpdatePedidoEx)
                        {
                            item.DESCRIPCION = mDescripcion;
                        }

                        if (mFactura.Trim().Length > 0)
                        {
                            var qUpdateFactura = from f in db.FACTURA_LINEA where f.FACTURA == mFactura && f.PEDIDO_LINEA == mLinea select f;
                            foreach (var item in qUpdateFactura)
                            {
                                item.DESCRIPCION = mDescripcion;
                            }
                        }

                        db.SaveChanges();
                        transactionScope.Complete();
                    }
                }

                if (mTipo == "F")
                {
                    var qFactura = (from f in db.FACTURA_LINEA where f.FACTURA == documento && f.LINEA == mLinea select f);

                    if (qFactura.Count() == 0)
                    {
                        mRetorna.mensaje = "No se encontró la línea de la factura.";
                        return mRetorna;
                    }

                    if (qFactura.First().ANULADA == "S")
                    {
                        mRetorna.mensaje = "La factura está anulada.";
                        return mRetorna;
                    }

                    mDescripcion = qFactura.First().DESCRIPCION;

                    if (mDescripcion.Contains(mGel))
                    {
                        mNuevoGel = "N";
                        mDescripcion = mDescripcion.Replace(mGel, "");
                    }
                    else
                    {
                        mNuevoGel = "S";
                        mDescripcion = string.Format("{0}{1}", mDescripcion, mGel);
                    }

                    using (TransactionScope transactionScope = new TransactionScope())
                    {
                        var qUpdateFactura = from f in db.FACTURA_LINEA where f.FACTURA == documento && f.LINEA == mLinea select f;
                        foreach (var item in qUpdateFactura)
                        {
                            item.DESCRIPCION = mDescripcion;
                        }

                        var qUpdatePedido = from p in db.MF_Pedido_Linea where p.PEDIDO == qFactura.First().PEDIDO && p.PEDIDO_LINEA == qFactura.First().PEDIDO_LINEA select p;
                        foreach (var item in qUpdatePedido)
                        {
                            item.GEL = mNuevoGel;
                        }

                        var qUpdatePedidoEx = from p in db.PEDIDO_LINEA where p.PEDIDO == documento && p.PEDIDO_LINEA1 == mLinea select p;
                        foreach (var item in qUpdatePedidoEx)
                        {
                            item.DESCRIPCION = mDescripcion;
                        }

                        db.SaveChanges();
                        transactionScope.Complete();
                    }

                }

                mRetorna.exito = true;
                mRetorna.mensaje = mDescripcion;
            }
            catch (Exception ex)
            {
                mRetorna.mensaje = CatchClass.ExMessage(ex, "GelController", "Put");
            }

            return mRetorna;
        }

    }
}
