﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Tools
{
    public class ModuloHabilitadoController : MFApiController
    {
        public bool GetModuloHabilitado(string Modulo)
        {
            //--------------------------------------------------------------------------
            //Verificamos si el modulo esta habilitado o no. Antes de proceder a las validaciones.
            //--------------------------------------------------------------------------
            return Utilitario.ModuloDelSistemaHabilitado(Modulo);
                
        }
    }
}
