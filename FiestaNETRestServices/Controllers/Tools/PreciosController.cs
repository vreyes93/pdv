﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Data;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;
using MF_Clases;
using System.Web.Configuration;

namespace FiestaNETRestServices.Controllers.Tools
{
    public class PreciosController : ApiController
    {
        
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET api/precios/5
        public Clases.Precios GetPrecios(string articulo = "", string financiera = "7", string nivel = "T", string lista = "N", string ambiente = "DES", decimal saldoFinanciar = 0, Int32 cotizacion = 0, string pedido = "")
        {
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            Clases.Precios mRetorna = new Clases.Precios();

            string mError = "";
            db.CommandTimeout = 999999999;

            string mNivelBuscar = "";
            var qPedido = from p in db.MF_Pedido where p.PEDIDO == pedido select p;
            var qCotizacion = from c in db.MF_Cotizacion where c.COTIZACION == cotizacion select c;

            try
            {
                log.Debug("Generando XML para consultar en INTERCONSumo.");

                DateTime mFecha = DateTime.Now.Date.AddDays(-1);

                if (articulo.Trim().Length == 0 && saldoFinanciar == 0 && cotizacion == 0 && pedido.Trim().Length == 0)
                {
                    var qNivelHistorico = from n in db.MF_NIVEL_PRECIO_COEFICIENTE_HISTORICO where n.FECHA == mFecha && n.FINANCIERA == 7 select n;

                    if (qNivelHistorico.Count() > 0)
                    {
                        Clases.RetornaExito mItemExito1 = new Clases.RetornaExito();
                        mItemExito1.exito = false;
                        mItemExito1.mensaje = "Los niveles de precio de ayer ya se encuentran actualizados.";
                        mRetorna.RetornaExito.Add(mItemExito1);

                        return mRetorna;
                    }
                }

                if (cotizacion > 0)
                {
                    if (qCotizacion.Count() == 0)
                    {
                        Clases.RetornaExito mItemExito1 = new Clases.RetornaExito();
                        mItemExito1.exito = false;
                        mItemExito1.mensaje = string.Format("La cotización {0} no existe.", cotizacion);
                        mRetorna.RetornaExito.Add(mItemExito1);

                        return mRetorna;
                    }

                    if (qCotizacion.First().FINANCIERA != 7)
                    {
                        Clases.RetornaExito mItemExito1 = new Clases.RetornaExito();
                        mItemExito1.exito = false;
                        mItemExito1.mensaje = string.Format("La cotización {0} no pertenece a Interconsumo.", cotizacion);
                        mRetorna.RetornaExito.Add(mItemExito1);

                        return mRetorna;
                    }

                    mNivelBuscar = qCotizacion.First().NIVEL_PRECIO;
                }

                if (pedido.Trim().Length > 0)
                {
                    if (qPedido.Count() == 0)
                    {
                        Clases.RetornaExito mItemExito1 = new Clases.RetornaExito();
                        mItemExito1.exito = false;
                        mItemExito1.mensaje = string.Format("El pedido {0} no existe.", pedido);
                        mRetorna.RetornaExito.Add(mItemExito1);

                        return mRetorna;
                    }

                    if (qPedido.First().FINANCIERA != 7)
                    {
                        Clases.RetornaExito mItemExito1 = new Clases.RetornaExito();
                        mItemExito1.exito = false;
                        mItemExito1.mensaje = string.Format("El pedido {0} no pertenece a Interconsumo.", pedido);
                        mRetorna.RetornaExito.Add(mItemExito1);

                        return mRetorna;
                    }
                    mNivelBuscar = qPedido.First().NIVEL_PRECIO;
                }

                string mTipo = "N";
                try
                {
                    Int32 mNumero = Convert.ToInt32(articulo.Substring(0, 1));
                    mTipo = "C";
                }
                catch
                {
                    mTipo = "N";
                }

                DataTable dtArticulos = new DataTable("articulos");
                decimal mSaldoFinanciarCotizacion = 0; decimal mSaldoFinanciarPedido = 0;

                if (articulo.Trim().Length == 0 || saldoFinanciar > 0 || cotizacion > 0 || pedido.Trim().Length > 0)
                {
                    dtArticulos.Columns.Add(new DataColumn("Articulo", typeof(System.String)));
                    dtArticulos.Columns.Add(new DataColumn("Nombre", typeof(System.String)));
                    dtArticulos.Columns.Add(new DataColumn("PrecioBase", typeof(System.Decimal)));
                    dtArticulos.Columns.Add(new DataColumn("PrecioLista", typeof(System.Decimal)));

                    decimal mPrecioMuestra = Convert.ToDecimal(Math.Round(24999 / 1.12, 2, MidpointRounding.AwayFromZero));
                    if (saldoFinanciar > 0) mPrecioMuestra = saldoFinanciar;

                    if (qCotizacion.Count() > 0)
                    {
                        mSaldoFinanciarCotizacion = Convert.ToDecimal(qCotizacion.First().SALDO_FINANCIAR);

                        if (qCotizacion.First().NIVEL_PRECIO == "InterCons12M")
                        {
                            mPrecioMuestra = Convert.ToDecimal(qCotizacion.First().MONTO);
                        }
                        else
                        {
                            mPrecioMuestra = Convert.ToDecimal(qCotizacion.First().SALDO_FINANCIAR);
                        }
                    }

                    if (qPedido.Count() > 0)
                    {
                        mSaldoFinanciarPedido = Convert.ToDecimal(qPedido.First().SALDO_FINANCIAR);

                        if (qPedido.First().NIVEL_PRECIO == "InterCons12M")
                        {
                            mPrecioMuestra = Convert.ToDecimal(qPedido.First().MONTO);
                        }
                        else
                        {
                            mPrecioMuestra = Convert.ToDecimal(qPedido.First().SALDO_FINANCIAR);
                        }
                    }

                    DataRow mRow = dtArticulos.NewRow();
                    mRow["Articulo"] = "000000-000";
                    mRow["Nombre"] = "Artículo para consumir cuotas de Interconsumo";
                    mRow["PrecioBase"] = mPrecioMuestra;
                    mRow["PrecioLista"] = mPrecioMuestra;
                    dtArticulos.Rows.Add(mRow);
                }
                else
                {
                    if (articulo.Substring(0, 1) == "M" || articulo.Substring(0, 1) == "A")
                    {
                        try
                        {
                            Int32 mNumero = Convert.ToInt32(articulo.Substring(1, 1));
                            mTipo = "C";
                        }
                        catch
                        {
                            mTipo = "N";
                        }
                    }

                    string mWhere = string.Format(" AND a.DESCRIPCION LIKE '%{0}%' ", articulo);
                    if (mTipo == "C") mWhere = string.Format(" AND a.ARTICULO LIKE '%{0}%' ", articulo);

                    string mSQL = string.Format("SELECT a.ARTICULO AS Articulo, a.DESCRIPCION AS Nombre, a.PRECIO_BASE_LOCAL AS PrecioBase, ROUND(a.PRECIO_BASE_LOCAL * (SELECT 1 + (IMPUESTO1 / 100) FROM prodmult.IMPUESTO WHERE IMPUESTO = 'IVA'), 2) AS PrecioLista FROM prodmult.ARTICULO a WHERE a.ACTIVO = 'S' {0} ORDER BY a.ARTICULO", mWhere);

                    string mServer = "(local)"; string mDataBase = "EXACTUSERP";
                    if (ambiente == "PRU") mDataBase = "PRUEBAS";
                    if (ambiente == "PRO" || ambiente == "PRU") mServer = "sql.fiesta.local";

                    System.Data.SqlClient.SqlConnection mConexion = new System.Data.SqlClient.SqlConnection(string.Format("Data Source={0};Initial Catalog={1};User id=mf.fiestanet;password=54321", mServer, mDataBase));
                    System.Data.SqlClient.SqlDataAdapter daArticulos = new System.Data.SqlClient.SqlDataAdapter(mSQL, mConexion);

                    daArticulos.SelectCommand.CommandTimeout = 999999999;
                    daArticulos.Fill(dtArticulos);
                }

                Int32 mFinanciera = Convert.ToInt32(financiera);
                var qNiveles = from n in db.MF_NIVEL_PRECIO_COEFICIENTE where n.FINANCIERA == mFinanciera && n.ACTIVO == "S" select n;
                if (nivel != "T") qNiveles = qNiveles.Where(f => f.NIVEL_PRECIO.Equals(nivel));

                qNiveles = qNiveles.OrderBy(f => f.NIVEL_PRECIO);
                if (financiera == "5" || financiera == "9") qNiveles = qNiveles.OrderBy(f => f.FACTOR);

                decimal mIVA = 1 + ((from im in db.IMPUESTO where im.IMPUESTO1 == "IVA" select im).First().IMPUESTO11 / 100);
                decimal mRestar = (100 - Convert.ToDecimal((from f in db.MF_Financiera where f.Financiera == mFinanciera select f).First().pctjRestar)) / 100;

                DataTable dtPlazos = new DataTable("plazos");
                dtPlazos.Columns.Add(new DataColumn("plazo", typeof(System.String)));
                dtPlazos.Columns.Add(new DataColumn("pagos", typeof(System.Int32)));
                dtPlazos.Columns.Add(new DataColumn("nivel", typeof(System.String)));

                DataRow mRow3 = dtPlazos.NewRow();
                mRow3["plazo"] = "3";
                mRow3["pagos"] = 3;
                mRow3["nivel"] = "InterCons03M";
                dtPlazos.Rows.Add(mRow3);

                DataRow mRow6 = dtPlazos.NewRow();
                mRow6["plazo"] = "6";
                mRow6["pagos"] = 6;
                mRow6["nivel"] = "InterCons06M";
                dtPlazos.Rows.Add(mRow6);

                DataRow mRow9 = dtPlazos.NewRow();
                mRow9["plazo"] = "9";
                mRow9["pagos"] = 9;
                mRow9["nivel"] = "InterCons09M";
                dtPlazos.Rows.Add(mRow9);

                if (cotizacion == 0 && pedido.Trim().Length == 0)
                {
                    DataRow mRow12 = dtPlazos.NewRow();
                    mRow12["plazo"] = "12";
                    mRow12["pagos"] = 12;
                    mRow12["nivel"] = "InterCons12M";
                    dtPlazos.Rows.Add(mRow12);
                }

                DataRow mRow12_MF = dtPlazos.NewRow();
                mRow12_MF["plazo"] = "12_MF";
                mRow12_MF["pagos"] = 12;
                mRow12_MF["nivel"] = "InterCons12M";
                dtPlazos.Rows.Add(mRow12_MF);

                DataRow mRow18 = dtPlazos.NewRow();
                mRow18["plazo"] = "18";
                mRow18["pagos"] = 18;
                mRow18["nivel"] = "InterCons18M";
                dtPlazos.Rows.Add(mRow18);

                DataRow mRow24 = dtPlazos.NewRow();
                mRow24["plazo"] = "24";
                mRow24["pagos"] = 24;
                mRow24["nivel"] = "InterCons24M";
                dtPlazos.Rows.Add(mRow24);

                DataRow mRow36 = dtPlazos.NewRow();
                mRow36["plazo"] = "36";
                mRow36["pagos"] = 36;
                mRow36["nivel"] = "InterCons36M";
                dtPlazos.Rows.Add(mRow36);
                
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    for (int ii = 0; ii < dtArticulos.Rows.Count; ii++)
                    {
                        string mArticulo = dtArticulos.Rows[ii]["Articulo"].ToString();
                        string mNombre = dtArticulos.Rows[ii]["Nombre"].ToString();
                        decimal mPrecio = Math.Round(Convert.ToDecimal(dtArticulos.Rows[ii]["PrecioLista"]), 2, MidpointRounding.AwayFromZero);
                        string mOferta = "N"; decimal mPrecioOriginal = 0; decimal mPrecioOferta = 0; string mPagos = "1"; DateTime mFechaVence = new DateTime(1980, 1, 1); DateTime mFechaDesde = new DateTime(1980, 1, 1);

                        var qCama = from a in db.MF_ArticuloNivelPrecioOferta where a.Articulo == mArticulo select a;
                        if (qCama.Count() > 0) mPrecio = mPrecio * (1 - (Convert.ToDecimal(qCama.First().pctjDescuento) / 100));

                        var qOferta = from a in db.MF_ArticuloPrecioOferta where a.Articulo == mArticulo && a.Estatus == "V" select a;
                        if (qOferta.Count() > 0)
                        {
                            mOferta = "S";
                            mPrecioOferta = Math.Round(qOferta.First().PrecioOferta * mIVA, 2, MidpointRounding.AwayFromZero);
                            mPrecioOriginal = Math.Round(qOferta.First().PrecioOriginal * mIVA, 2, MidpointRounding.AwayFromZero);
                            mFechaDesde = qOferta.First().FechaOfertaDesde;
                            mFechaVence = qOferta.First().FechaOfertaHasta;
                        }

                        if (lista == "N")
                        {
                            if (nivel == "T")
                            {
                                Clases.Precio mItemPrecio = new Clases.Precio();
                                mItemPrecio.Articulo = mArticulo;
                                mItemPrecio.Nombre = mNombre;
                                mItemPrecio.MontoTotal = Math.Round(Convert.ToDecimal(dtArticulos.Rows[ii]["PrecioLista"]), 2, MidpointRounding.AwayFromZero);
                                mItemPrecio.NivelPrecio = "Precio de Lista";
                                mItemPrecio.Factor = 1;
                                mItemPrecio.Tooltip = "Precio de Lista";
                                mItemPrecio.Oferta = mOferta;
                                mItemPrecio.PrecioOriginal = mPrecioOriginal;
                                mItemPrecio.PrecioOferta = mPrecioOferta;
                                mItemPrecio.Pagos = mPagos;
                                mItemPrecio.FechaVence = mFechaVence;
                                mItemPrecio.FechaDesde = mFechaDesde;
                                mRetorna.Precio.Add(mItemPrecio);
                            }

                            decimal mSaldoFinanciar = mPrecio;
                            if (qCama.Count() == 0) mSaldoFinanciar = Math.Round(mPrecio * mRestar, 2, MidpointRounding.AwayFromZero);
                            if (saldoFinanciar > 0 || cotizacion > 0 || pedido.Trim().Length > 0) mSaldoFinanciar = mPrecio;

                            if (financiera == "7")
                            {
                                string xmlData = "<solicitud>";
                                xmlData += "<autenticacion>";
                                xmlData += "<aplicacion>Muebles Fiesta</aplicacion>";
                                xmlData += string.Format("<usuario><![CDATA[{0}]]></usuario>", WebConfigurationManager.AppSettings["UsrInterconsumo"]);
                                xmlData += string.Format("<clave><![CDATA[{0}]]></clave>", WebConfigurationManager.AppSettings["PwdInterconsumo"]);
                                xmlData += "</autenticacion>";
                                xmlData += string.Format("<saldo_financiar>{0}</saldo_financiar>", mSaldoFinanciar);
                                xmlData += string.Format("<monto_mf>{0}</monto_mf>", mPrecio);
                                xmlData += "<producto>1</producto>";
                                xmlData += "</solicitud>";
                                
                                log.Info("Consumiendo WS de consulta de Interconsumo.");

                                XmlDocument xmlRespuesta = new XmlDocument();
                                decimal mSaldoFinanciar12 = 0; string resultado = ""; string respuesta = "";

                                XElement xml = XElement.Parse(xmlData);
                                decimal mMontoMinimo = Convert.ToDecimal((from c in db.MF_Configura select c).First().MINIMO_INTERCONSUMO);

                                if (mSaldoFinanciar >= mMontoMinimo)
                                {
                                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                                    FiestaNETRestServices.srvIntegracionInterConsumo.srvIntegracionInteconsumoPuente srvIntegra = new FiestaNETRestServices.srvIntegracionInterConsumo.srvIntegracionInteconsumoPuente();
                                    InterEncript.Encriptacion dllEncriptacion = new InterEncript.Encriptacion();

                                    resultado = dllEncriptacion.Proceso(xmlData, "solicitud", "1", "xml");
                                    respuesta = srvIntegra.EntradaXmlCotizacion(resultado);
                                    resultado = dllEncriptacion.Proceso(respuesta.Trim(), "", "2", "1");

                                    xmlRespuesta.PreserveWhitespace = true;
                                    xmlRespuesta.LoadXml(resultado);

                                    if (xmlRespuesta.SelectNodes("respuesta/codigo").Item(0).InnerText != "000")
                                    {
                                        Clases.RetornaExito mItemExito1 = new Clases.RetornaExito();
                                        mItemExito1.exito = false;
                                        mItemExito1.mensaje = string.Format("Interconsumo devolvio el error: {0}.", xmlRespuesta.SelectNodes("respuesta/descripcion").Item(0).InnerText);
                                        mRetorna.RetornaExito.Add(mItemExito1);

                                        return mRetorna;
                                    }

                                    log.Info("Leyendo respuesta de Interconsumo.");
                                    xml = XElement.Parse(resultado);

                                    try
                                    {
                                        mSaldoFinanciar12 = Convert.ToDecimal(xmlRespuesta.SelectNodes("respuesta/cotizacion/saldo_financiar").Item(0).InnerText);
                                    }
                                    catch
                                    {
                                        mSaldoFinanciar12 = 0;
                                    }
                                }

                                if (nivel == "T")
                                {
                                    for (int jj = 0; jj < dtPlazos.Rows.Count; jj++)
                                    {
                                        if (mSaldoFinanciar >= mMontoMinimo)
                                        {
                                            XElement query = (from item in xml.XPathSelectElements("./cotizacion/opcion")
                                                              where item.Element("plazo").Value == dtPlazos.Rows[jj]["plazo"].ToString()
                                                              select item).FirstOrDefault();

                                            mError = string.Format("jj = {0}, plazo = {1}", jj, dtPlazos.Rows[jj]["plazo"].ToString());

                                            bool mTieneElementos = false;
                                            try
                                            {
                                                if (query.HasElements) mTieneElementos = true;
                                            }
                                            catch
                                            {
                                                mTieneElementos = false;
                                            }

                                            if (mTieneElementos)
                                            {
                                                decimal mPrimera = 0; decimal mUltima = 0;
                                                string mInicio = ""; string mFin = ""; bool mAgregar = true;

                                                if (qCama.Count() == 0 && dtPlazos.Rows[jj]["plazo"].ToString() == "12") mAgregar = false;
                                                if (qCama.Count() > 0 && dtPlazos.Rows[jj]["plazo"].ToString() == "12_MF") mAgregar = false;
                                                if (cotizacion > 0 || pedido.Trim().Length > 0) mAgregar = true;

                                                if (mAgregar)
                                                {
                                                    mPrimera = Convert.ToDecimal(query.Element("cuota").Value);
                                                    mUltima = Convert.ToDecimal(query.Element("ultima").Value);

                                                    mInicio = Convert.ToString(query.Element("primer_pago").Value);
                                                    mFin = Convert.ToString(query.Element("vencimiento").Value);

                                                    string[] mPrimerPago = mInicio.Split(new string[] { "-" }, StringSplitOptions.None);
                                                    string[] mUltimoPago = mFin.Split(new string[] { "-" }, StringSplitOptions.None);

                                                    DateTime mFechaInicial = new DateTime(Convert.ToInt32(mPrimerPago[0]), Convert.ToInt32(mPrimerPago[1]), Convert.ToInt32(mPrimerPago[2]));
                                                    DateTime mFechaFinal = new DateTime(Convert.ToInt32(mUltimoPago[0]), Convert.ToInt32(mUltimoPago[1]), Convert.ToInt32(mUltimoPago[2]));

                                                    mPagos = dtPlazos.Rows[jj]["pagos"].ToString();
                                                    decimal mMontoTotal = (mPrimera * (Convert.ToInt32(mPagos) - 1)) + mUltima;
                                                    decimal mSaldoFinanciarDefintivo = dtPlazos.Rows[jj]["plazo"].ToString() == "12_MF" ? mSaldoFinanciar12 : mSaldoFinanciar;
                                                    decimal mFactor = Math.Round(mMontoTotal / mSaldoFinanciarDefintivo, 6, MidpointRounding.AwayFromZero);

                                                    string mNivel = dtPlazos.Rows[jj]["nivel"].ToString();
                                                    if (qCama.Count() > 0) mSaldoFinanciarDefintivo = mSaldoFinanciar;

                                                    Clases.Precio mItemPrecio = new Clases.Precio();
                                                    mItemPrecio.Articulo = mArticulo;
                                                    mItemPrecio.Nombre = mNombre;
                                                    mItemPrecio.SaldoFinanciar = mSaldoFinanciarDefintivo;
                                                    mItemPrecio.Primera = mPrimera;
                                                    mItemPrecio.Ultima = mUltima;
                                                    mItemPrecio.Intereses = mMontoTotal - mSaldoFinanciarDefintivo;
                                                    mItemPrecio.MontoTotal = mMontoTotal;
                                                    mItemPrecio.PrimerPago = mFechaInicial;
                                                    mItemPrecio.UltimoPago = mFechaFinal;
                                                    mItemPrecio.NivelPrecio = mNivel;
                                                    mItemPrecio.Factor = mFactor;
                                                    mItemPrecio.Tooltip = mNivel;
                                                    mItemPrecio.Oferta = mOferta;
                                                    mItemPrecio.PrecioOriginal = mPrecioOriginal;
                                                    mItemPrecio.PrecioOferta = mPrecioOferta;
                                                    mItemPrecio.Pagos = mPagos;
                                                    mItemPrecio.FechaVence = mFechaVence;
                                                    mItemPrecio.FechaDesde = mFechaDesde;
                                                    mRetorna.Precio.Add(mItemPrecio);
                                                      
                                                    var qUpdateNivel = from n in db.MF_NIVEL_PRECIO_COEFICIENTE where n.NIVEL_PRECIO == mNivel select n;
                                                    decimal mFactorAnterior = Convert.ToDecimal(qUpdateNivel.First().FACTOR);

                                                    DateTime mFechaInicialAnterior = Convert.ToDateTime(qUpdateNivel.First().FECHA_INICIAL);
                                                    DateTime mFechaFinalAnterior = Convert.ToDateTime(qUpdateNivel.First().FECHA_FINAL);

                                                    foreach (var itemNivel in qUpdateNivel)
                                                    {
                                                        itemNivel.FACTOR = mFactor;
                                                        itemNivel.FECHA_INICIAL = mFechaInicial;
                                                        itemNivel.FECHA_FINAL = mFechaFinal;
                                                    }

                                                    if (cotizacion > 0 && mNivelBuscar == mNivel)
                                                    {
                                                        foreach (var itemCotizacion in qCotizacion)
                                                        {
                                                            itemCotizacion.FACTOR = mFactor;
                                                            itemCotizacion.FECHA_INICIAL = mFechaInicial;
                                                            itemCotizacion.FECHA_FINAL = mFechaFinal;
                                                            itemCotizacion.MONTO_PAGOS1 = mPrimera;
                                                            itemCotizacion.MONTO_PAGOS2 = mUltima;
                                                            itemCotizacion.RECARGOS = mMontoTotal - mSaldoFinanciarDefintivo;
                                                            itemCotizacion.MONTO = mMontoTotal;
                                                            itemCotizacion.XML_CUOTAS = xmlData;
                                                            itemCotizacion.XML_CUOTAS_RESPUESTA = resultado;
                                                            itemCotizacion.FECHA_CONSULTA_CUOTAS = DateTime.Now.Date;
                                                            itemCotizacion.FECHA_HORA_CONSULTA_CUOTAS = DateTime.Now;
                                                            itemCotizacion.SALDO_FINANCIAR_CONSULTA = mSaldoFinanciarCotizacion;

                                                            if (qCotizacion.First().NIVEL_PRECIO == "InterCons12M")
                                                            {
                                                                itemCotizacion.SALDO_FINANCIAR = mSaldoFinanciar12;
                                                                itemCotizacion.TOTAL_FACTURAR = mSaldoFinanciar12 + Convert.ToDecimal(qCotizacion.First().ENGANCHE);
                                                            }
                                                        }

                                                        var qCotizacionLinea = from c in db.MF_Cotizacion_Linea where c.COTIZACION == cotizacion select c;
                                                        foreach (var itemLinea in qCotizacionLinea)
                                                        {
                                                            if (qCotizacion.First().NIVEL_PRECIO == "InterCons12M")
                                                            {
                                                                itemLinea.PRECIO_TOTAL_FACTURAR = Math.Round(Convert.ToDecimal(itemLinea.PRECIO_TOTAL) / mFactor, 2, MidpointRounding.AwayFromZero);
                                                            }
                                                            else
                                                            {
                                                                itemLinea.PRECIO_UNITARIO = Math.Round(Convert.ToDecimal(itemLinea.PRECIO_TOTAL_FACTURAR) * mFactor, 2, MidpointRounding.AwayFromZero);
                                                                itemLinea.PRECIO_TOTAL = Math.Round(Convert.ToDecimal(itemLinea.PRECIO_TOTAL_FACTURAR) * mFactor, 2, MidpointRounding.AwayFromZero);
                                                            }
                                                        }
                                                    }

                                                    if (pedido.Trim().Length > 0 && mNivelBuscar == mNivel)
                                                    {
                                                        foreach (var itemPedido in qPedido)
                                                        {
                                                            itemPedido.FACTOR = mFactor;
                                                            itemPedido.FECHA_INICIAL = mFechaInicial;
                                                            itemPedido.FECHA_FINAL = mFechaFinal;
                                                            itemPedido.MONTO_PAGOS1 = mPrimera;
                                                            itemPedido.MONTO_PAGOS2 = mUltima;
                                                            itemPedido.RECARGOS = mMontoTotal - mSaldoFinanciarDefintivo;
                                                            itemPedido.MONTO = mMontoTotal;
                                                            itemPedido.XML_CUOTAS = xmlData;
                                                            itemPedido.XML_CUOTAS_RESPUESTA = resultado;
                                                            itemPedido.FECHA_CONSULTA_CUOTAS = DateTime.Now.Date;
                                                            itemPedido.FECHA_HORA_CONSULTA_CUOTAS = DateTime.Now;
                                                            itemPedido.SALDO_FINANCIAR_CONSULTA = mSaldoFinanciarPedido;

                                                            if (qPedido.First().NIVEL_PRECIO == "InterCons12M")
                                                            {
                                                                itemPedido.SALDO_FINANCIAR = mSaldoFinanciar12;
                                                                itemPedido.TOTAL_FACTURAR = mSaldoFinanciar12 + Convert.ToDecimal(qPedido.First().ENGANCHE);
                                                            }
                                                        }

                                                        var qPedidoLinea = from p in db.MF_Pedido_Linea where p.PEDIDO == pedido select p;
                                                        foreach (var itemLinea in qPedidoLinea)
                                                        {
                                                            if (qPedido.First().NIVEL_PRECIO == "InterCons12M")
                                                            {
                                                                itemLinea.PRECIO_TOTAL_FACTURAR = Math.Round(Convert.ToDecimal(itemLinea.PRECIO_TOTAL) / mFactor, 2, MidpointRounding.AwayFromZero);
                                                            }
                                                            else
                                                            {
                                                                itemLinea.PRECIO_UNITARIO = Math.Round(Convert.ToDecimal(itemLinea.PRECIO_TOTAL_FACTURAR) * mFactor, 2, MidpointRounding.AwayFromZero);
                                                                itemLinea.PRECIO_TOTAL = Math.Round(Convert.ToDecimal(itemLinea.PRECIO_TOTAL_FACTURAR) * mFactor, 2, MidpointRounding.AwayFromZero);
                                                            }
                                                        }                                                        
                                                    }

                                                    var qUpdateNivelHist = from n in db.MF_NIVEL_PRECIO_COEFICIENTE_HISTORICO where n.FECHA == mFecha && n.FINANCIERA == 7 && n.NIVEL_PRECIO == mNivel select n;
                                                    if (qUpdateNivelHist.Count() == 0)
                                                    {
                                                        MF_NIVEL_PRECIO_COEFICIENTE_HISTORICO iNivel = new MF_NIVEL_PRECIO_COEFICIENTE_HISTORICO
                                                        {
                                                            FECHA = mFecha,
                                                            FINANCIERA = 7,
                                                            NIVEL_PRECIO = mNivel,
                                                            FACTOR = mFactorAnterior,
                                                            FECHA_INICIAL = mFechaInicialAnterior,
                                                            FECHA_FINAL = mFechaFinalAnterior,
                                                            RecordDate = DateTime.Now,
                                                            CreatedBy = string.Format("FA/{0}", "ROLAPINE"),
                                                            UpdatedBy = string.Format("FA/{0}", "ROLAPINE"),
                                                            CreateDate = DateTime.Now
                                                        };
                                                        db.MF_NIVEL_PRECIO_COEFICIENTE_HISTORICO.AddObject(iNivel);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    decimal mPrimera = 0; decimal mUltima = 0;
                                    string mInicio = ""; string mFin = ""; string mPlazo = Convert.ToInt32(nivel.Replace("InterCons", "").Replace("0", "").Replace("M", "")).ToString();

                                    if (qCama.Count() == 0 && mPlazo == "12") mPlazo = "12_MF";

                                    if (mSaldoFinanciar >= mMontoMinimo)
                                    {
                                        XElement query = (from item in xml.XPathSelectElements("./cotizacion/opcion")
                                                          where item.Element("plazo").Value == mPlazo
                                                          select item).FirstOrDefault();

                                        bool mTieneElementos = false;
                                        try
                                        {
                                            if (query.HasElements) mTieneElementos = true;
                                        }
                                        catch
                                        {
                                            mTieneElementos = false;
                                        }

                                        if (mTieneElementos)
                                        {
                                            mPrimera = Convert.ToDecimal(query.Element("cuota").Value);
                                            mUltima = Convert.ToDecimal(query.Element("ultima").Value);

                                            mInicio = Convert.ToString(query.Element("primer_pago").Value);
                                            mFin = Convert.ToString(query.Element("vencimiento").Value);

                                            string[] mPrimerPago = mInicio.Split(new string[] { "-" }, StringSplitOptions.None);
                                            string[] mUltimoPago = mFin.Split(new string[] { "-" }, StringSplitOptions.None);

                                            DateTime mFechaInicial = new DateTime(Convert.ToInt32(mPrimerPago[0]), Convert.ToInt32(mPrimerPago[1]), Convert.ToInt32(mPrimerPago[2]));
                                            DateTime mFechaFinal = new DateTime(Convert.ToInt32(mUltimoPago[0]), Convert.ToInt32(mUltimoPago[1]), Convert.ToInt32(mUltimoPago[2]));

                                            mPlazo = mPlazo.Replace("_MF", "");
                                            decimal mMontoTotal = (mPrimera * (Convert.ToInt32(mPlazo) - 1)) + mUltima;
                                            decimal mSaldoFinanciarDefintivo = mPlazo == "12" ? mSaldoFinanciar12 : mSaldoFinanciar;
                                            decimal mFactor = Math.Round(mMontoTotal / mSaldoFinanciarDefintivo, 6, MidpointRounding.AwayFromZero);

                                            if (qCama.Count() > 0) mSaldoFinanciarDefintivo = mSaldoFinanciar;

                                            Clases.Precio mItemPrecio = new Clases.Precio();
                                            mItemPrecio.Articulo = mArticulo;
                                            mItemPrecio.Nombre = mNombre;
                                            mItemPrecio.SaldoFinanciar = mSaldoFinanciarDefintivo;
                                            mItemPrecio.Primera = mPrimera;
                                            mItemPrecio.Ultima = mUltima;
                                            mItemPrecio.Intereses = mMontoTotal - mSaldoFinanciarDefintivo;
                                            mItemPrecio.MontoTotal = mMontoTotal;
                                            mItemPrecio.PrimerPago = mFechaInicial;
                                            mItemPrecio.UltimoPago = mFechaFinal;
                                            mItemPrecio.NivelPrecio = nivel;
                                            mItemPrecio.Factor = mFactor;
                                            mItemPrecio.Tooltip = nivel;
                                            mItemPrecio.Oferta = mOferta;
                                            mItemPrecio.PrecioOriginal = mPrecioOriginal;
                                            mItemPrecio.PrecioOferta = mPrecioOferta;
                                            mItemPrecio.Pagos = mPlazo;
                                            mItemPrecio.FechaVence = mFechaVence;
                                            mItemPrecio.FechaDesde = mFechaDesde;
                                            mRetorna.Precio.Add(mItemPrecio);

                                            var qUpdateNivel = from n in db.MF_NIVEL_PRECIO_COEFICIENTE where n.NIVEL_PRECIO == nivel select n;
                                            foreach (var itemNivel in qUpdateNivel)
                                            {
                                                itemNivel.FACTOR = mFactor;
                                                itemNivel.FECHA_INICIAL = mFechaInicial;
                                                itemNivel.FECHA_FINAL = mFechaFinal;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach (var item in qNiveles)
                                {
                                    decimal mSaldoFinanciarDefinitivo = mPrecio; decimal mIntereses = 0; decimal mMontoTotal = 0; decimal mPrimera = 0; decimal mUltima = 0;
                                    if (qCama.Count() == 0) mSaldoFinanciarDefinitivo = Math.Round(mPrecio * item.COEFICIENTE_MULR, 2, MidpointRounding.AwayFromZero);
                                    
                                    if (financiera == "1" || financiera == "5" || financiera == "9")
                                    {
                                        if (financiera == "1") mSaldoFinanciarDefinitivo = Convert.ToInt32(mSaldoFinanciarDefinitivo);
                                        mMontoTotal = mSaldoFinanciarDefinitivo;
                                    }
                                    else
                                    {
                                        mSaldoFinanciarDefinitivo = mPrecio;
                                        mMontoTotal = Math.Round(mPrecio * (decimal)item.FACTOR, 2, MidpointRounding.AwayFromZero);
                                        mIntereses = mMontoTotal - mPrecio;

                                        mPagos = item.PAGOS.Replace(" PAGOS", "");
                                        decimal mCuota = Math.Round(mMontoTotal / Convert.ToInt32(mPagos), 2, MidpointRounding.AwayFromZero);
                                        mPrimera = mCuota;
                                        mUltima = mCuota;
                                    }
                                    
                                    Clases.Precio mItemPrecio = new Clases.Precio();
                                    mItemPrecio.Articulo = mArticulo;
                                    mItemPrecio.Nombre = mNombre;
                                    mItemPrecio.SaldoFinanciar = mSaldoFinanciarDefinitivo;
                                    mItemPrecio.Primera = mPrimera;
                                    mItemPrecio.Ultima = mUltima;
                                    mItemPrecio.Intereses = mIntereses;
                                    mItemPrecio.MontoTotal = mMontoTotal;
                                    mItemPrecio.NivelPrecio = item.NIVEL_PRECIO;
                                    mItemPrecio.Factor = (decimal)item.FACTOR;
                                    mItemPrecio.Tooltip = item.NIVEL_PRECIO;
                                    mItemPrecio.Oferta = mOferta;
                                    mItemPrecio.PrecioOriginal = mPrecioOriginal;
                                    mItemPrecio.PrecioOferta = mPrecioOferta;
                                    mItemPrecio.Pagos = mPagos;
                                    mItemPrecio.FechaVence = mFechaVence;
                                    mItemPrecio.FechaDesde = mFechaDesde;
                                    mRetorna.Precio.Add(mItemPrecio);
                                }
                            }
                        }
                        else
                        {
                            Clases.Precio mItemPrecio = new Clases.Precio();
                            mItemPrecio.Articulo = mArticulo;
                            mItemPrecio.Nombre = mNombre;
                            mItemPrecio.MontoTotal = Math.Round(Convert.ToDecimal(dtArticulos.Rows[ii]["PrecioLista"]), 2, MidpointRounding.AwayFromZero);
                            mItemPrecio.NivelPrecio = "Precio de Lista";
                            mItemPrecio.Factor = 1;
                            mItemPrecio.Tooltip = "Precio de Lista";
                            mItemPrecio.Oferta = mOferta;
                            mItemPrecio.PrecioOriginal = mPrecioOriginal;
                            mItemPrecio.PrecioOferta = mPrecioOferta;
                            mItemPrecio.Pagos = mPagos;
                            mItemPrecio.FechaVence = mFechaVence;
                            mItemPrecio.FechaDesde = mFechaDesde;
                            mRetorna.Precio.Add(mItemPrecio);
                        }
                    }
                    
                    db.SaveChanges();
                    transactionScope.Complete();
                }

                string mMensaje = "Los precios fueron consultados exitosamente.";
                if (cotizacion > 0) mMensaje = string.Format("La cotización {0} fue exitosamente actualizada en línea con Interconsumo", cotizacion);
                if (pedido.Trim().Length > 0) mMensaje = string.Format("El pedido {0} fue exitosamente actualizado en línea con Interconsumo", pedido);

                Clases.RetornaExito mItemExito = new Clases.RetornaExito();
                mItemExito.exito = true;
                mItemExito.mensaje = mMensaje;
                mRetorna.RetornaExito.Add(mItemExito);
            }
            catch (Exception ex)
            {
                string mMensajeError = string.Format("{0} {1}", CatchClass.ExMessage(ex, "PreciosController", "GetPrecios"), mError);
                
                if (cotizacion > 0 || pedido.Trim().Length > 0)
                {
                    using (TransactionScope transactionScope = new TransactionScope())
                    {
                        var qNivel = (from n in db.MF_NIVEL_PRECIO_COEFICIENTE where n.NIVEL_PRECIO == mNivelBuscar select n).First();

                        foreach (var itemCotizacion in qCotizacion)
                        {
                            itemCotizacion.FACTOR = qNivel.FACTOR;
                            itemCotizacion.FECHA_INICIAL = qNivel.FECHA_INICIAL;
                            itemCotizacion.FECHA_FINAL = qNivel.FECHA_FINAL;
                            itemCotizacion.XML_CUOTAS_RESPUESTA = mMensajeError;
                            itemCotizacion.FECHA_CONSULTA_CUOTAS = DateTime.Now.Date;
                            itemCotizacion.FECHA_HORA_CONSULTA_CUOTAS = DateTime.Now;
                            itemCotizacion.SALDO_FINANCIAR_CONSULTA = itemCotizacion.SALDO_FINANCIAR;
                        }

                        foreach (var itemPedido in qPedido)
                        {
                            itemPedido.FACTOR = qNivel.FACTOR;
                            itemPedido.FECHA_INICIAL = qNivel.FECHA_INICIAL;
                            itemPedido.FECHA_FINAL = qNivel.FECHA_FINAL;
                            itemPedido.XML_CUOTAS_RESPUESTA = mMensajeError;
                            itemPedido.FECHA_CONSULTA_CUOTAS = DateTime.Now.Date;
                            itemPedido.FECHA_HORA_CONSULTA_CUOTAS = DateTime.Now;
                            itemPedido.SALDO_FINANCIAR_CONSULTA = itemPedido.SALDO_FINANCIAR;
                        }

                        db.SaveChanges();
                        transactionScope.Complete();

                        mMensajeError = string.Format("No se pudo conectar con Interconsumo, se grabó el factor del sistema de hoy a las 06:00am. {0}", mMensajeError);
                    }
                }

                Clases.RetornaExito mItemExito = new Clases.RetornaExito();
                mItemExito.exito = false;
                mItemExito.mensaje = mMensajeError;
                mRetorna.RetornaExito.Add(mItemExito);
            }

            return mRetorna;

        }

        public static decimal NuevoPrecio(decimal total, decimal precio, decimal valor)
        {
            decimal mPctj = (precio * 100) / total;
            decimal mNuevoPrecio = (mPctj * valor) / 100;
            return Math.Round(mNuevoPrecio, 2, MidpointRounding.AwayFromZero);
        }

    }
}
