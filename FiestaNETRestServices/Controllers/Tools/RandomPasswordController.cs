﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;
using MF_Clases;

namespace FiestaNETRestServices.Controllers.Tools
{
    public class RandomPasswordController : ApiController
    {

        // GET api/randompassword/5
        public string GetPassword(string id = "S")
        {
            int mLongitud = 8;
            string mCaracteres = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-";

            if (id == "N") mCaracteres = "ABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";

            char[] chars = new char[mLongitud];
            Random rd = new Random();

            for (int i = 0; i < mLongitud; i++)
            {
                chars[i] = mCaracteres[rd.Next(0, mCaracteres.Length)];
            }

            return new string(chars);
        }

    }
}
