﻿using MF_Clases;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Configuration;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Tools
{
    public class SolicitudSoporteController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Crear un ticket
        /// </summary>
        /// <param name="UsuarioTeams"></param>
        /// <param name="mensaje"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult Post(string UsuarioTeams,string mensaje)
        {
            try
            {
                string strAmbiente = WebConfigurationManager.AppSettings["Ambiente"];

                DAL.DALSoporte soporte = new DAL.DALSoporte();
                var qRespuesta = soporte.ObtenerVendedor(UsuarioTeams);
                if (qRespuesta != null)
                {
                    if (qRespuesta.Exito)
                    {
                        var Vendedor =(DtoVendedor)qRespuesta.Objeto;
                        string mAttachment = string.Format("|uploads|: [ {0} ], ", "");
                        string mCategoria = strAmbiente.Equals("PRO")  ? "131" : "1";

                        #region "Data"
                        string mData = "{|issue|: { " +
                           "|project_id|: "+(strAmbiente.Equals("PRO")? "39": "1")+", " +
                           "|tracker_id|: " + (strAmbiente.Equals("PRO") ? "10" : "3") + ", " +
                           "|priority_id|: 2, " +
                           "|subject|: |Problema en "+Vendedor.COBRADOR+" "+(mensaje.Length>35?mensaje.Substring(0,34): mensaje)+" |, " +
                           mAttachment +
                           "|description|: |" + mensaje + "|, " +
                           "|category_id|: " + mCategoria + 
                             " ,|custom_fields|: "+ ((strAmbiente.Equals("PRO")) ? " [{|id|: "+(strAmbiente.Equals("PRO") ? "4" : "3") +", " +
                               "|value|: [|" + Convert.ToString(Vendedor.COBRADOR.ToUpper()).Replace("F", "F-") + "|]}, " :" [") +
                               "{|id|: "+(strAmbiente.Equals("PRO") ? "9" : "6")+", " +
                               "|value|: |"+Vendedor.EMAIL.Substring(0,Vendedor.EMAIL.IndexOf("@"))+"|}, " +
                               "{|id|: "+(strAmbiente.Equals("PRO") ? "14" : "7") +", " + // Mensaje de error
                               "|value|: | |}] " 
                            + "}}";
                        #endregion
                        var postData = mData.Replace('|', '"');
                        byte[] data = Encoding.ASCII.GetBytes(postData);


                        var response = TicketRequest(data,0, "POST", "issues.json");
                        if(response!=null)
                        return Ok<RedmineResponse>(response);
                    }
                }
                else
                    log.Error("No se encontró a ningún vendedor con estas credenciales: "+UsuarioTeams);
            }
            catch (Exception ex)
            {
                log.Error("Problema al generar el soporte desde TEAMS " + ex.Message);
                return BadRequest(ex.Message);
            }

            return BadRequest();
        }

        /// <summary>
        /// Actualizar un ticket
        /// </summary>
        /// <param name="redmineTicket"></param>
        /// <returns></returns>
        [HttpPut]
        public IHttpActionResult Put([FromBody] Issue redmineTicket)
        {
            try
            {

            RedmineUpdate iUpdate = new RedmineUpdate
            {
                issue = new IssueUpdate { status_id = redmineTicket.status.id, notes = redmineTicket.notes }
            };
               
            string json = JsonConvert.SerializeObject(iUpdate);

            byte[] data = Encoding.ASCII.GetBytes(json);

            var response = TicketRequest(data,redmineTicket.id, "PUT", "issues/"+redmineTicket.id+".json");
                if (response != null)
                    return Ok<RedmineResponse>(response);
            }
            catch
            {
            }
            return BadRequest();
        }

        /// <summary>
        /// Método para obtener los tickets finalizados
        /// </summary>
        /// <param name="UsuarioTeams"></param>
        /// <returns></returns>
        public IHttpActionResult Get(string UsuarioTeams)
        {
            try
            {
                ///Obtener los últimos 3 tickets abiertos (sin importar su estado) debe filtrar por el estado
                //Se debe colocar un project_id sino no filtra correctamente

                if (UsuarioTeams != null)
                {
                    string strAmbiente = WebConfigurationManager.AppSettings["Ambiente"].ToString();
                    var field_vendedor = strAmbiente.Equals("PRO") ? "9" : "6";
                    var field_proyecto= strAmbiente.Equals("PRO") ? "39" : "1";
                    var field_Url= strAmbiente.Equals("PRO") ? WebConfigurationManager.AppSettings["RedmineUrl"] : WebConfigurationManager.AppSettings["RedmineUrlTests"];
                    var request = (HttpWebRequest)WebRequest.Create(string.Format("{0}/{1}", field_Url, "issues.json?cf_"+(field_vendedor) +"=" + UsuarioTeams + "&status_id=*&sort=id:desc&limit=3&project_id="+field_proyecto+"&include=attachments,journals"));
                    request.Headers["X-Redmine-API-Key"] = strAmbiente.Equals("PRO") ?  WebConfigurationManager.AppSettings["SoportitoApiKey"] : WebConfigurationManager.AppSettings["SoportitoApiKeyTests"];

                    request.Method = "GET";
                    request.ContentType = "application/json";


                    var response = (HttpWebResponse)request.GetResponse();

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                        
                        var lstissue = JsonConvert.DeserializeObject<RedmineQueryResponse>(responseString);
                        return Ok<IEnumerable<Issue>>(lstissue.issues.AsEnumerable());
                    }
                }

                return null;
                
            }
            catch (Exception ex)
            {
                log.Error(string.Format("Error: {0}", CatchClass.ExMessage(ex, "utilitarios", "TicketRequest")));
                return null;
            }
        }
        private RedmineResponse TicketRequest(byte[] data,int ticket, string metodo, string urlParcial, string contentType = "application/json")
        {
            try
            {
                string strAmbiente = WebConfigurationManager.AppSettings["Ambiente"];
                var field_Url = strAmbiente.Equals("PRO") ? WebConfigurationManager.AppSettings["RedmineUrl"] : WebConfigurationManager.AppSettings["RedmineUrlTests"];
                var request = (HttpWebRequest)WebRequest.Create(string.Format("{0}/{1}", field_Url, urlParcial));
                    request.Headers["X-Redmine-API-Key"] = strAmbiente.Equals("PRO") ? WebConfigurationManager.AppSettings["SoportitoApiKey"] : WebConfigurationManager.AppSettings["SoportitoApiKeyTests"];

                    request.Method = metodo;
                    request.ContentType = contentType;
                    request.ContentLength = data.Length;

                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
               
                var response = (HttpWebResponse)request.GetResponse();


                if (metodo.Equals("POST"))
                {
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    var issue = JsonConvert.DeserializeObject<RedmineResponse>(responseString);
                    return issue;
                }
                else if (metodo.Equals("PUT"))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        Issue issuex = new Issue
                        {
                             id=ticket,
                             status=new Estado_Ticket {  id=3,name="RESUELTO"}
                        };
                        RedmineResponse rresponse = new RedmineResponse
                        {  issue=issuex};
                        return rresponse;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(string.Format("Error: {0}", CatchClass.ExMessage(ex, "utilitarios", "TicketRequest")));
                return null;
            }
            return null;
        }
    }
}
