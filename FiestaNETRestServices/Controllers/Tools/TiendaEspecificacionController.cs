﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using MF_Clases.Comun;
using System;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Tools
{
    public class TiendaEspecificacionController : MFApiController
    {
        private new static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [HttpGet]
        public IHttpActionResult Obtener(string tienda, string atributo)
        {
            try
            {
                TiendaEspecificacion propiedad = new DALComunes().ObtenerAtributo(tienda, atributo);
                propiedad = propiedad == null ? new TiendaEspecificacion() : propiedad;

                return Ok(propiedad);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
        }
    }
}
