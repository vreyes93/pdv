﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MF_Clases;
using static MF_Clases.Clases;
using FiestaNETRestServices.Content.Abstract;

namespace FiestaNETRestServices.Controllers.Tools
{
    public class TiendasController : MFApiController
    {
        // GET: api/Tiendas
        public List<Tiendas> Get()
        {
            return (from c in db.MF_Cobrador where c.ACTIVO == "S" orderby c.COBRADOR select new Tiendas { Tienda = c.COBRADOR, Nombre=c.NOMBRE, Departamento=c.DEPARTAMENTO,Municipio=c.MUNICIPIO }).ToList();
        }

    }
}
