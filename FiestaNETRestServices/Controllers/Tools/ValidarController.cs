﻿using FiestaNETRestServices.DAL;
using System;
using System.Web.Http;

namespace FiestaNETRestServices.Controllers.Tools
{
    [RoutePrefix("api/Validar")]
    public class ValidarController : ApiController
    {
        [HttpGet]
        [ActionName("RequiereArmado")]
        [Route("RequiereArmado")]
        public IHttpActionResult RequiereArmado(string codigoArticulo)
        {
            try
            {
                if (EsNulo(codigoArticulo)) return BadRequest("El código del artículo no debe venir vacío o nulo");

                return Ok(DALValidar.RequiereArmado(codigoArticulo));

            }
            catch
            {
                return BadRequest("Excepcion, favor de comunicarse con Informática.");
            }
        }

        [HttpGet]
        [ActionName("CompatibilidadArmadoEntreTiendaProducto")]
        [Route("CompatibilidadArmadoEntreTiendaProducto")]
        public IHttpActionResult CompatibilidadArmadoEntreTiendaProducto(string codigoTienda, string codigoArticulo)
        {
            try
            {
                if (EsNulo(codigoTienda)) return BadRequest("El código de la tienda no debe venir vacío o nulo");
                if (EsNulo(codigoArticulo)) return BadRequest("El código del artículo no debe venir vacío o nulo");

                return Ok(DALValidar.CompatibilidadArmadoEntreTiendaProducto(codigoTienda, codigoArticulo));

            }
            catch
            {
                return BadRequest("Excepcion, favor de comunicarse con Informática.");
            }
        }

        [HttpGet]
        [ActionName("CompatibilidadTraspasoTiendaProducto")]
        [Route("CompatibilidadTraspasoTiendaProducto")]
        public IHttpActionResult CompatibilidadTraspasoTiendaProducto(string bodegaOrigen, string bodegaDestino, string codigoArticulo, string descripcionArticulo)
        {
            try
            {
                if (EsNulo(bodegaOrigen))
                    throw new Exception("El código de bodega orígen no debe ser vacío o nulo");

                if (EsNulo(bodegaDestino))
                    throw new Exception("El código de bodega destino no debe ser vacío o nulo");

                if (EsNulo(codigoArticulo))
                    throw new Exception("El código del artículo no debe ser vacío o nulo");

                DALValidar.CompatibilidadTraspasoTiendaProducto(bodegaOrigen, bodegaDestino, codigoArticulo, descripcionArticulo);

                return Ok(true);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
        }

        private bool EsNulo(string valor)
        {
            return string.IsNullOrEmpty(valor) || string.IsNullOrWhiteSpace(valor);
        }
    }
}
