﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Data;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;
using System.Globalization;
using System.Text.RegularExpressions;
using MF_Clases;

namespace FiestaNETRestServices.Controllers.Tools
{
    public class isEmailValidoController : ApiController
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET api/isemailvalido/5
        public Clases.RetornaExito Get(string email)
        {
            Clases.RetornaExito mRetorna = new Clases.RetornaExito();

            mRetorna.exito = false;
            mRetorna.mensaje = "Correo electrónico inválido.";

            if (email == null) return mRetorna;

            string strIn = email;

            if (strIn.Trim().Length == 0) return mRetorna;

            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper, RegexOptions.None);
            }
            catch
            {
                return mRetorna;
            }

            // Return true if strIn is in valid e-mail format.
            try
            {
                if (!Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase))
                {
                    return mRetorna;
                }
            }
            catch
            {
                return mRetorna;
            }

            mRetorna.exito = true;
            mRetorna.mensaje = "Correo electrónico válido.";

            return mRetorna;
        }

        private string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                return "";
            }
            return match.Groups[1].Value + domainName;
        }

    }
}
