﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Data;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;
using MF_Clases;

namespace FiestaNETRestServices.Controllers.Tools
{
    public class isJefeController : ApiController
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public class Retorna
        {
            public bool isJefe { get; set; }
            public string Mail { get; set; }
            public string Tienda { get; set; }
        }

        // GET api/isjefe
        public string Get()
        {
            return "Error, debe enviar el usuario";
        }

        // GET api/isjefe/5
        public Retorna GetIsJefe(string usuario)
        {
            Retorna mRetorna = new Retorna();
            EXACTUSERPEntities db = new EXACTUSERPEntities();

            log.Info("Validando si el usuario es jefe.");

            mRetorna.Mail = "";
            mRetorna.Tienda = "";
            mRetorna.isJefe = false;
            
            try
            {
                var qUsuario = from u in db.USUARIO where u.USUARIO1 == usuario select u;

                if (qUsuario.Count() == 0) return mRetorna;
                string mVendedor = qUsuario.First().CELULAR;

                var qVendedor = from v in db.VENDEDOR where v.VENDEDOR1 == mVendedor select v;
                if (qVendedor.Count() == 0) return mRetorna;

                var qCobrador = from c in db.MF_Cobrador where c.CODIGO_JEFE == mVendedor && c.ACTIVO == "S" select c;
                if (qCobrador.Count() == 0) return mRetorna;

                mRetorna.isJefe = true;
                mRetorna.Mail = qVendedor.First().E_MAIL;
                mRetorna.Tienda = qCobrador.First().COBRADOR;
            }
            catch
            {
                //Nothing
            }

            return mRetorna;
        }

    }
}
