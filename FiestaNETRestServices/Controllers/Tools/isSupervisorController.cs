﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Data;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using FiestaNETRestServices.Models;
using Newtonsoft.Json;
using MF_Clases;

namespace FiestaNETRestServices.Controllers.Tools
{
    public class isSupervisorController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public class Retorna
        {
            public bool isSupervisor { get; set; }
        }
        
        // GET api/issupervisor
        public string Get()
        {
            return "Error, Debe enviar el usuario";
        }

        // GET api/issupervisor/5
        public Retorna GetIsSupervisor(string usuario)
        {
            Retorna mRetorna = new Retorna();
            EXACTUSERPEntities db = new EXACTUSERPEntities();

            log.Info("Validando si el usuario es supervisor.");

            try
            {
                mRetorna.isSupervisor = false;
                if ((from s in db.MF_Supervisor where s.USUARIO == usuario && s.ACTIVO == "S" select s).Count() > 0) mRetorna.isSupervisor = true;
            }
            catch
            {
                mRetorna.isSupervisor = false;
            }

            return mRetorna;
        }

    }
}
