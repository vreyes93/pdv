﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.DAL;
using FiestaNETRestServices.Models;
using FiestaNETRestServices.Utils;
using MF_Clases;
using MF_Clases.GuateFacturas.FEL;
using MF_Clases.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using static FiestaNETRestServices.Utils.Utilitarios;
using static MF_Clases.Clases;

namespace FiestaNETRestServices.Controllers.UnitTest
{
    public class TestFacturaFELController : MFApiController
    {
        [HttpGet]

        public async System.Threading.Tasks.Task<Resultado> GetTestFacturaFEL(string pNit, string pFecha, int pTipoDoc, string pIDDoc, string DASerie, string DAPreimpreso, int Moneda)

        {

            UtilitariosGuateFacturas nGF = new UtilitariosGuateFacturas();
            Resultado mResultado = new Resultado();

            Respuesta mRespuesta = new Respuesta();
            Doc mDoc = new Doc();





            fillDocument(ref mDoc, pNit, pFecha, pTipoDoc, pIDDoc, DASerie, DAPreimpreso, Moneda);
            mResultado = nGF.Servicio(CONSTANTES.GUATEFAC.SERVICIOS.GENERAR_DOCUMENTO, mDoc);

            /*
            ContingenciaSolicitud sol = new ContingenciaSolicitud();
            sol.Cantidad = 50;
            sol.Establecimiento = 1;
            sol.NitEmisor = "5440998";
            sol.Lote = "1";
            mRespuesta = nGF.SolicitudLoteContingencia(sol);


            */
            return mResultado;
        }
        private void fillDocument(ref Doc pDoc, string pNit, string pFecha, int pTipoDoc, string pIDDoc, string DASerie, string DAPreimpreso, int Moneda)
        {
            pDoc.NITEmisor = "5440998";
            pDoc.Establecimiento = 1;
            pDoc.IdMaquina = "001";
            pDoc.Enviar = "S";
            pDoc.Email = "test@mueblesfiesta.com";
            pDoc.Fecha = new DateTime(
    Int32.Parse(pFecha.Substring(0, 4)), Int32.Parse(pFecha.Substring(4, 2)), Int32.Parse(pFecha.Substring(6, 2))
    );

            pDoc.TipoDocumento = pTipoDoc; // CONSTANTES.GUATEFAC.DOCUMENTO.FACTURA_ESPECIAL;

            if (pDoc.TipoDocumento == CONSTANTES.GUATEFAC.DOCUMENTO.FACTURA_ESPECIAL)
            {
                pDoc.NITReceptor = "CF";
                pDoc.Nombre = "JUAN PEREZ GALINDO";
                pDoc.Direccion = "CALLE REAL SAN JUAN Zona 4";
                pDoc.NumeroIdentificacion = "1970268180718";
                pDoc.PaisEmision = 1;
                pDoc.Departamento = 1;
                pDoc.Municipio = 1;
                pDoc.PorcISR = (decimal)0.05;

            }
            else if (pDoc.TipoDocumento == CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_CREDITO
                || pDoc.TipoDocumento == CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_DEBITO
                || pDoc.TipoDocumento == CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_ABONO
                )
            {
                pDoc.NITReceptor = pNit;
                pDoc.Nombre = "ROBIN DENILSON CALDERON MAZARIEGOS";
                pDoc.Direccion = "CALLE REAL SAN JUAN  Zona 3";
                pDoc.DASerie = DASerie;
                pDoc.DAPreimpreso = DAPreimpreso;
            }
            else if (pDoc.TipoDocumento == CONSTANTES.GUATEFAC.DOCUMENTO.FACTURA_CAMBIARIA)
            {
                pDoc.NITReceptor = pNit;
                pDoc.Nombre = "ROBIN DENILSON CALDERON MAZARIEGOS";
                pDoc.Direccion = "CALLE REAL SAN JUAN  Zona 3";
                pDoc.NumeroAbono = "1";
                pDoc.FechaVencimiento = new DateTime(2019, 04, 05);
                pDoc.MontoAbono = (decimal)49.97;
            }
            else
            {
                ///****NORMAL
                pDoc.NITReceptor = pNit;
                pDoc.Nombre = "ROBIN DENILSON CALDERON MAZARIEGOS";
                pDoc.Direccion = "CALLE REAL SAN JUAN  Zona 3";
                //****/

                /*****
                pDoc.NITReceptor = "CF";
                pDoc.Nombre = "INDUFOAM";
                pDoc.Direccion = "CALLE REAL SAN JUAN  Zona 3";
                */
                //pDoc.Direccion = "KM. 34.5 CARR. A SANTA ANA  Zona -- HACIENDA SAN ANDRES Guatemala Guatemala";

            }
            //pDoc.TipoVenta = CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES;
            //pDoc.DestinoVenta = CONSTANTES.GUATEFAC.DESTINO_VENTA.GT;

            //pDoc.Fecha = new DateTime(2019, 1, 1);
            //pDoc.Moneda = CONSTANTES.GUATEFAC.MONEDA.QTZ;
            //pDoc.Tasa = CONSTANTES.GUATEFAC.TASA.QTZ;

            if (Moneda != 1)
            {
                pDoc.Moneda = CONSTANTES.GUATEFAC.MONEDA.DOL;
                pDoc.DestinoVenta = "69";
                pDoc.Tasa = (decimal)7.7;
            }

            ///*****************Exportacion
            pDoc.EsExportacion = true;
            pDoc.Incoterm = "DAP";
            pDoc.CodigoConsignatarioODestinatario = "001";
            pDoc.NombreConsignatarioODestinatario = "PEMEX";
            pDoc.DireccionConsignatarioODestinatario = "INSURGENTES CENTRO O.28";
            pDoc.CodigoComprador = "002";
            pDoc.NombreComprador = "PETROLEOS MEXICANOS";
            pDoc.DireccionComprador = "CIUDAD MEXICO";
            pDoc.OtraReferencia = "ENTREGA INMEDIATA";
            pDoc.CodigoExportador = "001";

//*/
            pDoc.Referencia = pIDDoc;
            List<DetalleDoc> mDetalles = new List<DetalleDoc>();
            DetalleDoc mDetalle = new DetalleDoc();

            if (pDoc.TipoDocumento == CONSTANTES.GUATEFAC.DOCUMENTO.FACTURA_ESPECIAL)
            {
                pDoc.Bruto = (decimal)577.92;
                //pDoc.Descuento = (decimal)0;
                //pDoc.Exento = (decimal)0;
                //pDoc.Otros = (decimal)0;
                pDoc.Neto = (decimal)516;
                pDoc.Isr = (decimal)25.80;
                pDoc.Iva = (decimal)61.92;
                pDoc.Total = (decimal)577.92;
                mDetalle.Producto = "1";
                mDetalle.Descripcion = "Producto de prueba";
                //mDetalle.Medida = CONSTANTES.GUATEFAC.MEDIDA.UNIDAD;
                mDetalle.Cantidad = 1;
                mDetalle.Precio = (decimal)577.92;
                //mDetalle.PorcDesc = 0;
                mDetalle.ImpBruto = (decimal)577.92;
                //mDetalle.ImpDescuento = 0;
                //mDetalle.ImpExento = 0;
                //mDetalle.ImpOtros = 0;
                mDetalle.ImpNeto = (decimal)516.00;
                mDetalle.ImpIsr = (decimal)25.80;
                mDetalle.ImpIva = (decimal)61.92;
                mDetalle.ImpTotal = (decimal)577.92;


            }
            else if (pDoc.TipoDocumento == CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_CREDITO)
            {
                pDoc.Bruto = (decimal)7.5;
                //pDoc.Descuento = (decimal)0;
                //pDoc.Exento = (decimal)0;
                //pDoc.Otros = (decimal)0;
                pDoc.Neto = (decimal)6.6;
                //pDoc.Isr = (decimal)0;
                pDoc.Iva = (decimal)0.9;
                pDoc.Total = (decimal)7.5;

                mDetalle.Producto = "00000002";
                mDetalle.Descripcion = "nota credito por descuento";
                //mDetalle.Medida = CONSTANTES.GUATEFAC.MEDIDA.UNIDAD;
                mDetalle.Cantidad = 1;
                mDetalle.Precio = (decimal)7.5;
                //mDetalle.PorcDesc = 0;
                mDetalle.ImpBruto = (decimal)7.5;
                //mDetalle.ImpDescuento = 0;
                //mDetalle.ImpExento = 0;
                //mDetalle.ImpOtros = 0;
                mDetalle.ImpNeto = (decimal)6.6;
                //mDetalle.ImpIsr = 0;
                mDetalle.ImpIva = (decimal)0.9;
                mDetalle.ImpTotal = (decimal)7.5;
            }
            else if (pDoc.TipoDocumento == CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_ABONO)
            {
                pDoc.Bruto = (decimal)42.62;
                //pDoc.Descuento = (decimal)0;
                pDoc.Exento = (decimal)42.62;
                //pDoc.Otros = (decimal)0;
                //pDoc.Neto = (decimal)6.6;
                //pDoc.Isr = (decimal)0;
                //pDoc.Iva = (decimal)0.9;
                pDoc.Total = (decimal)42.62;

                mDetalle.Producto = "00000003";
                mDetalle.Descripcion = "nota credito por descuento";
                //mDetalle.Medida = CONSTANTES.GUATEFAC.MEDIDA.UNIDAD;
                mDetalle.Cantidad = 1;
                mDetalle.Precio = (decimal)42.62;
                //mDetalle.PorcDesc = 0;
                mDetalle.ImpBruto = (decimal)42.62;
                //mDetalle.ImpDescuento = 0;
                mDetalle.ImpExento = (decimal)42.62;
                //mDetalle.ImpOtros = 0;
                //mDetalle.ImpNeto = (decimal)6.6;
                //mDetalle.ImpIsr = 0;
                //mDetalle.ImpIva = (decimal)0.9;
                mDetalle.ImpTotal = (decimal)42.62;
            }
            else
            {
                /*
                                pDoc.Bruto = (decimal)49.97;
                                //pDoc.Descuento = (decimal)0;
                                //pDoc.Exento = (decimal)0;
                                //pDoc.Otros = (decimal)0;
                                pDoc.Neto = (decimal)44.62;
                                //pDoc.Isr = (decimal)0;
                                pDoc.Iva = (decimal)5.35;
                                pDoc.Total = (decimal)49.97;

                                mDetalle.Producto = "00000001";
                                mDetalle.Descripcion = "Producto de prueba";
                                //mDetalle.Medida = CONSTANTES.GUATEFAC.MEDIDA.UNIDAD;
                                mDetalle.Cantidad = 1;
                                mDetalle.Precio = (decimal)49.97;
                                //mDetalle.PorcDesc = 0;
                                mDetalle.ImpBruto = (decimal)49.97;
                                //mDetalle.ImpDescuento = 0;
                                //mDetalle.ImpExento = 0;
                                //mDetalle.ImpOtros = 0;
                                mDetalle.ImpNeto = (decimal)44.62;
                                //mDetalle.ImpIsr = 0;
                                mDetalle.ImpIva = (decimal)5.35;
                                mDetalle.ImpTotal = (decimal)49.97;
                */









                ///*************NORMAL
                                pDoc.Bruto = (decimal)49.97;
                                //pDoc.Descuento = (decimal)0;
                                //pDoc.Exento = (decimal)0;
                                //pDoc.Otros = (decimal)0;
                                pDoc.Neto = (decimal)44.62;
                                //pDoc.Isr = (decimal)0;
                                pDoc.Iva = (decimal)5.35;
                                pDoc.Total = (decimal)49.97;

                                mDetalle.Producto = "00000001";
                                mDetalle.Descripcion = "Producto de prueba";
                                //mDetalle.Medida = CONSTANTES.GUATEFAC.MEDIDA.UNIDAD;
                                mDetalle.Cantidad = 1;
                                mDetalle.Precio = (decimal)49.97;
                                //mDetalle.PorcDesc = 0;
                                mDetalle.ImpBruto = (decimal)49.97;
                                //mDetalle.ImpDescuento = 0;
                                //mDetalle.ImpExento = 0;
                                //mDetalle.ImpOtros = 0;
                                mDetalle.ImpNeto = (decimal)44.62;
                                //mDetalle.ImpIsr = 0;
                                mDetalle.ImpIva = (decimal)5.35;
                                mDetalle.ImpTotal = (decimal)49.97;
                //******/
                /*************EXENTO
                pDoc.Bruto = (decimal)42.62;
                //pDoc.Descuento = (decimal)0;
                pDoc.Exento = (decimal)42.62;
                //pDoc.Otros = (decimal)0;
                //pDoc.Neto = (decimal)6.6;
                //pDoc.Isr = (decimal)0;
                //pDoc.Iva = (decimal)0.9;
                pDoc.Total = (decimal)42.62;

                mDetalle.Producto = "00000003";
                mDetalle.Descripcion = "nota credito por descuento";
                //mDetalle.Medida = CONSTANTES.GUATEFAC.MEDIDA.UNIDAD;
                mDetalle.Cantidad = 1;
                mDetalle.Precio = (decimal)42.62;
                //mDetalle.PorcDesc = 0;
                mDetalle.ImpBruto = (decimal)42.62;
                //mDetalle.ImpDescuento = 0;
                mDetalle.ImpExento = (decimal)42.62;
                //mDetalle.ImpOtros = 0;
                //mDetalle.ImpNeto = (decimal)6.6;
                //mDetalle.ImpIsr = 0;
                //mDetalle.ImpIva = (decimal)0.9;
                mDetalle.ImpTotal = (decimal)42.62;
                */
            }
            mDetalle.TipoVentaDet = CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES;

            mDetalles.Add(mDetalle);
            pDoc.Detalle = mDetalles;

        }
    }
}