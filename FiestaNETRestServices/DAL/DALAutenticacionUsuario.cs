﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Web;

namespace FiestaNETRestServices.DAL
{
    /// <summary>
    /// Para los métodos necesarios para ExactusAutenticaUsuarioController
    /// </summary>
    public class DALAutenticacionUsuario : MFDAL
    {
        private new static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        EXACTUSERPEntities Exactus = new EXACTUSERPEntities();

        /// <summary>
        /// Método que devuelve la información del usuario de EXACTUS derivado de su username
        /// </summary>
        /// <param name="pUsuario">usuario exactus</param>
        /// <returns></returns>
        public Models.USUARIO BuscarUsuarioByUsuario(string pUsuario) => Exactus.USUARIO.Where(u => u.USUARIO1 == pUsuario).FirstOrDefault();

        /// <summary>
        /// Método que devuelve la información del vendedor
        /// </summary>
        /// <param name="pVendedor"></param>
        /// <returns></returns>
        public Models.VENDEDOR BuscarVendedorByVendedor(string pVendedor) => Exactus.VENDEDOR.Where(v => v.VENDEDOR1 == pVendedor).FirstOrDefault();

        //=================================================================================================================================================================
        public Models.MF_Vendedor BuscarMF_VendedorByVendedor(string pVendedor)
        {
            return Exactus.MF_Vendedor.Where(v => v.VENDEDOR == pVendedor).FirstOrDefault();
        }
        //=================================================================================================================================================================
        /// <summary>
        /// Método que devuelve los roles según el usuario
        /// </summary>
        /// <param name="pUsuario"></param>
        /// <returns></returns>
        public  List<string> BuscarRolesByUsuario(string pUsuario)

        {

            List<string> mRoles = new List<string>();
            try
            {
                log.Debug("Find Usuario");

                USUARIO mUsuario = Exactus.USUARIO.Where(u => u.USUARIO1 == pUsuario).FirstOrDefault();

                if (mUsuario != null)
                {
                    if (!string.IsNullOrEmpty(mUsuario.CELULAR))
                    {
                        log.Debug("Find Vendedor");
                        //VENDEDOR mVendedor = dbProdmult.VENDEDOR.Find(mUsuario.CELULAR);

                        VENDEDOR mVendedor = Exactus.VENDEDOR.Where(v => v.VENDEDOR1 == mUsuario.CELULAR).FirstOrDefault();
                        if (mVendedor != null)
                        {
                            //-------------------------------------------------------------------------------------------------------
                            //Si el usuario esta registrado en la tabla de Vendedor, tiene el Rol de Vendedor
                            mRoles.Add(Const.ROLES.RL_TIENDA_VENDEDOR);


                            //-------------------------------------------------------------------------------------------------------
                            //Si el codigo de vendedor esta registrado en el campo codigo_jefe de la tabla mf_cobrador
                            //entonces tiene el Rol de Jefe de Tienda

                            log.Debug("Find Jefe");

                            var mJefeCobrador = (from c in Exactus.MF_Cobrador
                                                 where c.CODIGO_JEFE == mVendedor.EMPLEADO
                                                        && c.ACTIVO == Const.ACTIVO
                                                 select c
                                             );
                            if (mJefeCobrador.Count() > 0) mRoles.Add(Const.ROLES.RL_TIENDA_JEFE);


                            //-------------------------------------------------------------------------------------------------------
                            //Si el codigo de vendedor esta registrado en el campo codigo_supervisor de la tabla mf_cobrador
                            //entonces tiene el Rol de Supervisor de Tienda

                            log.Debug("Find Supervisor");

                            var mSupervisorCobrador = (from s in Exactus.MF_Cobrador
                                                       where s.CODIGO_SUPERVISOR == mVendedor.EMPLEADO
                                                              && s.ACTIVO == Const.ACTIVO
                                                       select s
                                             );
                            if (mSupervisorCobrador.Count() > 0) mRoles.Add(Const.ROLES.RL_TIENDA_SUPERVISOR);

                            //-------------------------------------------------------------------------------------------------------
                            //Si el usuario esta registrado en la tabla de Gerentes, es probable que tenga acceso a ciertos 
                            //modulos del PDV, tales como Contabilidad, Bodega, Administración, Utilitarios, etc.

                            log.Debug("Find Otros Roles");

                            IQueryable<MF_Gerente> mGerente = (from g in Exactus.MF_Gerente
                                                               where g.USUARIO == mUsuario.USUARIO1
                                                               select g
                                                 );

                            foreach (var item in mGerente)
                            {
                                if (item.VER_OPCIONES_GERENCIA == Const.SI) mRoles.Add(Const.ROLES.RL_GERENTE);

                                if (item.ADMINISTRACION == Const.SI) mRoles.Add(Const.ROLES.RL_ADMINISTRACION_JEFE);

                                if (item.VER_OPCIONES_BODEGA == Const.SI) mRoles.Add(Const.ROLES.RL_BODEGA_JEFE);

                                if (item.CONTABILIDAD == Const.SI) mRoles.Add(Const.ROLES.RL_CONTABILIDAD_JEFE);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message);
            }
            log.Debug("Finish");
            return mRoles;
        }

        public bool CambiarTienda(string Usuario, string TiendaNueva, ref string Mensaje)
        {
            
            bool mRetorna = true;
            //-----------------------------------------------------------------------------------------------------------------
            //Desencriptar los parametros de Usuario y Nueva Tienda
            log.Debug("DataDirectory");
            Usuario = FiestaNETRestServices.Utils.Utilitarios.Decrypt(Usuario, true);
            TiendaNueva = FiestaNETRestServices.Utils.Utilitarios.Decrypt(TiendaNueva, true);

            //-----------------------------------------------------------------------------------------------------------------
            log.Info(string.Format("Iniciando servicio para cambiar de tienda Usuario: {0}, Tienda Nueva: {1}", Usuario, TiendaNueva));
            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    
                    Int32 mSupervisor = (from s in Exactus.MF_Supervisor where s.USUARIO == Usuario && s.ACTIVO == "S" select s).Count();
                    if (mSupervisor == 0)
                    {
                        Mensaje = "Usted no es supervisor";
                        return false;
                    }

                    var qUbicacion = (from b in Exactus.BODEGA where b.BODEGA1 == TiendaNueva select b).First().NOMBRE;
                    string mVendedor = (from u in Exactus.USUARIO where u.USUARIO1 == Usuario select u).First().CELULAR;

                    var qVendedor = from v in Exactus.VENDEDOR where v.VENDEDOR1 == mVendedor select v;
                    foreach (var item in qVendedor)
                    {
                        item.E_MAIL = TiendaNueva;
                    }

                    var qVendedorMF = from v in Exactus.MF_Vendedor where v.VENDEDOR == mVendedor select v;
                    foreach (var item in qVendedorMF)
                    {
                        item.COBRADOR = TiendaNueva;
                    }

                    Exactus.SaveChanges();
                    transactionScope.Complete();
                    Mensaje = string.Format("El cambio a {0} fue realizado exitosamente.", TiendaNueva);
                    log.Info(Mensaje);
                }
            }
            catch (Exception ex)
            {
                mRetorna = false;
                Mensaje = string.Format("Error al cambiar la tienda WS {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
                log.Debug(Mensaje);
            }

            return mRetorna;
        }
        public bool CambiarTiendaVendedor(string Usuario, string TiendaNueva, ref string Mensaje)
        {
            log.Info(string.Format("Iniciando servicio para cambiar de tienda Vendedor: {0}, Tienda Nueva: {1}", Usuario, TiendaNueva));
            bool mRetorna = true;
            //-----------------------------------------------------------------------------------------------------------------
            //Desencriptar los parametros de Usuario y Nueva Tienda
            log.Debug("DataDirectory");
            Usuario = FiestaNETRestServices.Utils.Utilitarios.Decrypt(Usuario, true);
            TiendaNueva = FiestaNETRestServices.Utils.Utilitarios.Decrypt(TiendaNueva, true);

            //-----------------------------------------------------------------------------------------------------------------

            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    var qVendedor = from v in Exactus.VENDEDOR where v.VENDEDOR1 == Usuario select v;
                    foreach (var item in qVendedor)
                    {
                        item.E_MAIL = TiendaNueva;
                    }

                    var qVendedorMF = from v in Exactus.MF_Vendedor where v.VENDEDOR == Usuario select v;
                    foreach (var item in qVendedorMF)
                    {
                        item.COBRADOR = TiendaNueva;
                    }

                    Exactus.SaveChanges();
                    transactionScope.Complete();
                    Mensaje = string.Format("El cambio a {0} fue realizado exitosamente.", TiendaNueva);
                    log.Info(Mensaje);
                }
            }
            catch (Exception ex)
            {
                mRetorna = false;
                Mensaje = string.Format("Error al cambiar la tienda WS {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
                log.Debug(Mensaje);
            }

            return mRetorna;
        }
    }
    #region "clases utilitarias para la conexión a la bdd"
    public class ExactusDB : DbContext
    {
        public ExactusDB(String connectionString) : base(connectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    }
    public class ExatusConnectionModels
    {
        //=================================================================================================================================================================
        //Variable que nos sirve para grabar la trazabilidad del sistema en archivos de texto.
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ExatusConnectionModels() { }

        /// <summary>
        /// Método para comprobar la conexión de un usuario y contraseña dados
        /// con la base de datos EXACTUSERP
        /// </summary>
        /// <param name="User"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public bool TestConnection(string User, string Password)
        {
            log.Debug("Init");
            ExactusDB dbExactus = new ExactusDB(ConnectionString(User, Password));
            try
            {
                dbExactus.Database.Connection.Open();
                dbExactus.Database.Connection.Close();
            }
            catch (SqlException e)
            {
                log.Error("Error: " + e.ErrorCode + " " + e.Errors);
                return false;
            }
            log.Debug("Finish");
            return true;
        }

        public static string ConnectionString(string User, string Password)
        {
            SqlConnectionStringBuilder sqlString = new SqlConnectionStringBuilder()
            {
                DataSource = "sql.fiesta.local",
                InitialCatalog = "EXACTUSERP",
                UserID = User,
                Password = Password,
            };

            EntityConnectionStringBuilder entityString = new EntityConnectionStringBuilder()
            {
                Provider = "System.Data.SqlClient",
                Metadata = "res://*/Models.Prodmult.ExactusModel.csdl|res://*/Models.Prodmult.ExactusModel.ssdl|res://*/Models.Prodmult.ExactusModel.msl",
                ProviderConnectionString = sqlString.ToString()
            };
            return entityString.ConnectionString;
        }
    }
    #endregion
}