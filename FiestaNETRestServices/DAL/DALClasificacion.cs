﻿using FiestaNETRestServices.Content.Abstract;
using MF_Clases.Comun;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static MF_Clases.Utils.CONSTANTES;

namespace FiestaNETRestServices.DAL
{
    public class DALClasificacion : MFDAL
    {
        public bool EsDecoracion(string codArticulo)
        {
            var qDecoracion = (from c in dbExactus.MF_Clasificacion  join cl in dbExactus.MF_ClasificacionDetalle on c.MF_CLASIFICACION1 equals cl.MF_CLASIFICACION
                               join ar in dbExactus.ARTICULO on cl.CLASIFICACION equals ar.CLASIFICACION_3 where c.MF_CLASIFICACION1 == CLASIFICACION_ARTICULOS.DECORACION 
                               && ar.ARTICULO1==codArticulo select ar);
            if(qDecoracion.Count()>0)
                return true;

            return false;
        }
    }
}