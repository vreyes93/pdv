﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Models;
using MF.DAL.Exactus.Crediplus;
using MF_Clases;
using MF_Clases.Comun;
using MF_Clases.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using CLIENTE = FiestaNETRestServices.Models.CLIENTE;
using CLIENTE_VENDEDOR = FiestaNETRestServices.Models.CLIENTE_VENDEDOR;
using DETALLE_DIRECCION = FiestaNETRestServices.Models.DETALLE_DIRECCION;
using MF_Cliente = MF.DAL.Exactus.Crediplus.MF_Cliente;
using NIT = MF.DAL.Exactus.Crediplus.NIT;

namespace FiestaNETRestServices.DAL
{
    public class DALCliente : MFDAL
    {

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función tiene como propósito obtener el listado de un catalogo en específico
        /// </summary>
        /// <param name="pCatalogo">Catalogo registrado en el sistema.</param>
        /// <returns>Retorma una lista List<Catalogo> </returns>
        public Respuesta ObtenerCalificacionClienteByDPI(string Documento)
        {
            Respuesta mRespuesta = new Respuesta();
            BuroCalificacion mCalificacion = new BuroCalificacion();
            mCalificacion.Calificacion = 0;
            //int mCalificacion = 0;
            try
            {
                var mListadoClientes = (from MC in dbExactus.MF_Cliente
                                        where MC.DPI == Documento
                                        orderby MC.RecordDate
                                        select MC
                                );

                if (mListadoClientes.Count() > 0)
                {

                    foreach (var item in mListadoClientes)
                    {
                        //--Se obtiene la mejor calificacion cuando hayan dos DPIs para dos codigos de clientes distintos
                        if (mCalificacion.Calificacion < (item.CALIFICACION ?? 0))
                            mCalificacion.Calificacion = (item.CALIFICACION ?? 0);
                    }
                }
                else
                {
                    return new Respuesta(false, string.Format("El Cliente con DPI {0} no existe en la base de datos de Muebles Fiesta.", Documento));
                }

                mRespuesta.Exito = true;
                mRespuesta.Objeto = mCalificacion;
                return mRespuesta;
            }
            catch (Exception e)
            {
                log.Error(string.Format("{0}", e));
                return new Respuesta(false, e);
            }
        }

        /// <summary>
        /// Dado una estructura, se buscará a un cliente existente dentro de exactus
        /// de lo contrario, se procederá a crear un cliente nuevo.
        /// </summary>
        /// <param name="cliente"></param>
        /// <returns></returns>
        public Respuesta ObtenerCodigoCliente(MF.Comun.Dto.Cliente cliente,string empresa="prodmult")
        {
            string codigoCliente = string.Empty;
            Respuesta mRespuesta = new Respuesta();
            bool blExisteCliente = false;
            #region "Validar campos"
            bool blCamposValidos = true;
            string mensaje = string.Empty;
            try
            {
                #region validando nulos
                if (cliente.Nombres == null)
                {
                    blCamposValidos = false;
                    mensaje += "(*) Falta el Nombre";
                }
                if (cliente.Apellidos == null)
                {
                    blCamposValidos = false;
                    mensaje += "(*) Faltan los apellidos";
                }
                if (cliente.Casa == null)
                {
                    blCamposValidos = false;
                    mensaje += "(*) Falta el número de casa";
                }
                if (cliente.Calle_avenida == null)
                {
                    blCamposValidos = false;
                    mensaje += "(*) Falta el número de calle o avenida";
                }
                if (cliente.NIT == null)
                {
                    blCamposValidos = false;
                    mensaje += "(*) Falta el NIT";
                }
                if (cliente.Identificacion == null)
                {
                    blCamposValidos = false;
                    mensaje += "(*) Falta su Identificación";
                }
                if (cliente.Departamento == null)
                {
                    blCamposValidos = false;
                    mensaje += "(*) Falta el departamento";
                }
                if (cliente.Municipio == null)
                {
                    blCamposValidos = false;
                    mensaje += "(*) Falta el municipio";
                }
                if (cliente.Colonia == null)
                {
                    blCamposValidos = false;
                    mensaje += "(*) Falta la colonia";
                }
                if (cliente.Apartamento == null)
                {
                    blCamposValidos = false;
                    mensaje += "(*) Falta el apartamento";
                }
                if (cliente.FechaNacimiento == null)
                {
                    blCamposValidos = false;
                    mensaje += "(*) Falta la fecha de nacimiento";
                }
                if (cliente.Genero == null)
                {
                    blCamposValidos = false;
                    mensaje += "(*) Falta el genero";
                }
                if (cliente.Indicaciones == null)
                {
                    blCamposValidos = false;
                    mensaje += "(*) Faltan las indicaciones";
                }
                
                #endregion
                if (blCamposValidos)
                {

                    if (cliente.Telefono == null)
                    {
                        cliente.Telefono = string.Empty;
                    }
                    if (cliente.Nombres.Trim().Equals(".") || cliente.Nombres.Trim().Equals(string.Empty))
                    {
                        blCamposValidos = false;
                        mensaje += "(*) Nombre inválido";
                    }
                    if (cliente.Apellidos.Trim().Equals(".") || cliente.Apellidos.Trim().Equals(string.Empty))
                    {
                        mensaje += "(*) Apellidos inválidos";
                        blCamposValidos = false;
                    }
                    if (cliente.Casa.Trim().Equals(".") || cliente.Casa.Trim().Equals(string.Empty))
                    {
                        //blCamposValidos = false;
                        //mensaje += "(*) Número de casa inválido";
                        cliente.Casa = string.Empty;
                    }

                    if (cliente.Calle_avenida.Trim().Equals(".") || cliente.Calle_avenida.Trim().Equals(string.Empty))
                    {
                        //blCamposValidos = false;
                        //mensaje += "(*) Número de calle/avenida inválida";
                        cliente.Calle_avenida = string.Empty;
                    }

                    if (cliente.NIT.Trim().Equals(".") || cliente.NIT.Trim().Equals(string.Empty))
                    {
                        //blCamposValidos = false;
                        //mensaje += "(*) NIT inválido";
                        cliente.NIT = "CF";
                    }

                    if (!cliente.NIT.Contains("-") && cliente.NIT.Length > 0 && !cliente.NIT.Equals("CF"))
                    {
                        cliente.NIT = cliente.NIT.Substring(0, cliente.NIT.Length - 1) + "-" + cliente.NIT.Substring(cliente.NIT.Length - 1, 1);
                    }
                    cliente.NIT = cliente.NIT.Trim();

                    if (cliente.Identificacion.Trim().Equals(".") || cliente.Identificacion.Trim().Equals(string.Empty))
                    {
                        blCamposValidos = false;
                        mensaje += "(*) Identificación inválida";
                    }
                    if (cliente.Departamento.Trim().Equals(".") || cliente.Departamento.Trim().Equals(string.Empty))
                    {
                        blCamposValidos = false;
                        mensaje += "(*) Departamento inválido";
                    }
                    if (cliente.Municipio.Trim().Equals(".") || cliente.Municipio.Trim().Equals(string.Empty))
                    {
                        blCamposValidos = false;
                        mensaje += "(*) Municipio inválido";
                    }
                    if (cliente.Email.Trim().Equals(".") || cliente.Email.Trim().Equals(string.Empty))
                    {
                        blCamposValidos = false;
                        mensaje += "(*) Email inválido";
                    }
                    if (!cliente.Email.Trim().Contains("@"))
                    {
                        blCamposValidos = false;
                        mensaje += "(*) Email inválido";
                    }
                }
            }
            catch (Exception ix)
            {
                log.Error("Error al validar el cliente para la compra de artículos en línea " + ix.Message);
                mRespuesta.Exito = false;
                mensaje = "Error al validar los datos ingresados por el cliente.";
            }
            #endregion
            if (blCamposValidos)
            {
                try
                {
                    //1) buscamos el cliente


                    if (empresa == "prodmult")
                    {
                        if (!cliente.NIT.ToUpper().Contains("CF") && !cliente.NIT.ToUpper().Contains("C/F") && !cliente.NIT.ToUpper().Contains("CONSUMIDOR FINAL") && (!cliente.NIT.ToUpper().Substring(0, 1).Equals("C") && !cliente.NIT.ToUpper().Contains("F")))
                        {
                            var qCliente2 =

                                      (
                                        //búsqueda por DPI
                                        from cli in dbExactus.MF_Cliente
                                        join c in dbExactus.CLIENTE on cli.CLIENTE equals c.CLIENTE1
                                        where cli.DPI == cliente.Identificacion && cliente.Identificacion.Contains("0") == false
                                        select c
                                        );
                            if (qCliente2.Count() == 0)
                                qCliente2 = (
                                    //búsqueda por nombres
                                    from cli in dbExactus.MF_Cliente
                                    join c in dbExactus.CLIENTE on cli.CLIENTE equals c.CLIENTE1
                                    where cliente.Nombres.Contains(cli.PRIMER_NOMBRE) && cliente.Nombres.Contains(cli.SEGUNDO_NOMBRE)
                                    && cliente.Apellidos.Contains(cli.PRIMER_APELLIDO) && cliente.Apellidos.Contains(cli.SEGUNDO_APELLIDO)
                                    select c
                                    );
                            //).Concat
                            //(
                            //  //búsqueda por PRIMER NOMBRE PRIMER APELLIDO
                            //  from cli in dbExactus.MF_Cliente
                            //  join c in dbExactus.CLIENTE on cli.CLIENTE equals c.CLIENTE1
                            //  where cli.PRIMER_NOMBRE.Contains(cliente.Nombres) && cli.SEGUNDO_NOMBRE.Contains(cliente.Nombres)
                            //  && cli.PRIMER_APELLIDO.Contains(cliente.Apellidos)
                            //  select c
                            //).Concat
                            //(
                            //  //búsqueda por DOS NOMBRES Y UN APELLIDO
                            //  from cli in dbExactus.MF_Cliente
                            //  join c in dbExactus.CLIENTE on cli.CLIENTE equals c.CLIENTE1
                            //  where cli.PRIMER_NOMBRE.Contains(cliente.Nombres) && cli.SEGUNDO_APELLIDO.Contains(cliente.Apellidos)
                            //  && cli.PRIMER_APELLIDO.Contains(cliente.Apellidos)
                            //  select c
                            //);

                            if (qCliente2.Count() > 0)
                            {

                                blExisteCliente = true;
                                codigoCliente = qCliente2.FirstOrDefault().CLIENTE1;
                                mRespuesta.Exito = true;
                                mRespuesta.Objeto = codigoCliente;
                            }
                            else
                            {
                                var qCliente = dbExactus.CLIENTE.Where(cli => cli.CONTRIBUYENTE == cliente.NIT);
                                if (qCliente.Count() == 0)
                                {
                                    qCliente = from cli in dbExactus.CLIENTE
                                               where cli.E_MAIL == cliente.Email
                                               select cli;
                                }

                                if (qCliente.Count() == 0)
                                {

                                    var nit = cliente.Identificacion;
                                    if (nit.Length > 0)
                                    {
                                        try
                                        {
                                            cliente.Identificacion = long.Parse(nit).ToString();
                                        }
                                        catch
                                        { }
                                    }
                                }
                                else
                                {

                                    blExisteCliente = true;
                                    codigoCliente = qCliente.FirstOrDefault().CLIENTE1;
                                    mRespuesta.Exito = true;
                                    mRespuesta.Objeto = codigoCliente;
                                }
                            }
                        }
                    }
                    else {
                        var qCliente = dbCrediplus.CLIENTE.Where(cli => cli.CONTRIBUYENTE == cliente.NIT);

                        
                        if (qCliente.Count() == 0 || cliente.NIT.ToUpper().Contains("CF"))
                        {



                           
                                //qCliente2 = (
                                //   //búsqueda por nombres
                                //   from cli in dbCrediplus.MF_Cliente
                                //   join c in dbCrediplus.CLIENTE on cli.CLIENTE equals c.CLIENTE1
                                //   where cliente.Nombres.Contains(cli.PRIMER_NOMBRE) && cliente.Nombres.Contains(cli.SEGUNDO_NOMBRE)
                                //   && cliente.Apellidos.Contains(cli.PRIMER_APELLIDO) && cliente.Apellidos.Contains(cli.SEGUNDO_APELLIDO)
                                //   select c
                                // ).Concat
                              var  qCliente2 = (
                                   //búsqueda por PRIMER NOMBRE PRIMER APELLIDO
                                   from cli in dbCrediplus.MF_Cliente
                                   join c in dbCrediplus.CLIENTE on cli.CLIENTE equals c.CLIENTE1
                                   where cli.PRIMER_NOMBRE.Contains(cliente.Nombres) && cli.SEGUNDO_NOMBRE.Contains(cliente.Nombres)
                                   && cli.PRIMER_APELLIDO.Contains(cliente.Apellidos)
                                   select c
                                 )
                                 //.Concat
                                 //(
                                 //  //búsqueda por DOS NOMBRES Y UN APELLIDO
                                 //  from cli in dbCrediplus.MF_Cliente
                                 //  join c in dbCrediplus.CLIENTE on cli.CLIENTE equals c.CLIENTE1
                                 //  where cli.PRIMER_NOMBRE.Contains(cliente.Nombres) && cli.SEGUNDO_APELLIDO.Contains(cliente.Apellidos)
                                 //  && cli.PRIMER_APELLIDO.Contains(cliente.Apellidos)
                                 //  select c
                                 //)
                                 ;
                            


                            if (qCliente2.Count() > 0)
                            {

                                blExisteCliente = true;
                                codigoCliente = qCliente2.FirstOrDefault().CLIENTE1;
                                mRespuesta.Exito = true;
                                mRespuesta.Objeto = codigoCliente;
                            }
                        }
                        else
                        {

                            blExisteCliente = true;
                            codigoCliente = qCliente.FirstOrDefault().CLIENTE1;
                            mRespuesta.Exito = true;
                            mRespuesta.Objeto = codigoCliente;
                        }

                    }
                    
                    
                    
                }
                catch (Exception ex)
                {
                    mRespuesta.Exito = false;
                    mRespuesta.Objeto = "";
                    mRespuesta.Mensaje = (string.Format("Ocurrió un error al buscar al cliente { 0} para la tienda virtual", cliente.Nombres));
                    log.Error(string.Format("Ocurrió un error al buscar al cliente {0} para la tienda virtual", cliente.Nombres), ex);
                }

                if (!blExisteCliente)
                {

                    //2 Crear cliente

                    try
                    {
                        var mNivelPrecio = cliente.FormaDePago;


                        if (empresa == "prodmult")
                        {
                            using (TransactionScope transactionScope = new TransactionScope())
                            {


                                #region DetalleDireccion
                                var qDetalleDireccion = from dd in dbExactus.DETALLE_DIRECCION select dd;
                                int mDetalleDireccion = qDetalleDireccion.Max(p => p.DETALLE_DIRECCION1) + 1;
                                int mDetalleDireccion2 = mDetalleDireccion + 1;
                                string mZona = "0101";
                                try
                                {
                                    mZona = (from z in dbExactus.ZONA where z.NOMBRE.ToUpper() == cliente.Departamento.ToUpper().Trim() + ", " + cliente.Municipio.ToUpper() select z).First().ZONA1;
                                }
                                catch
                                {
                                    log.Error(string.Format("---> error al obtener la zona del cliente, para la creación de clientes desde la tienda en línea: {0},{0}", cliente.Departamento, cliente.Municipio));
                                }
                                string mDireccionFacturacion = string.Format("Call/Av/Km/Mz: {0}  Casa/Lote: {1}  Apto/Piso: {2}  Zona/Aldea: {3}  Col/Barr/Cant: {4}  Departamento: {5}  Municipio: {6}", cliente.Calle_avenida, cliente.Casa, cliente.Apartamento, cliente.Zona, cliente.Colonia, cliente.Departamento, cliente.Municipio);

                                DETALLE_DIRECCION iDetalleDireccion = new DETALLE_DIRECCION
                                {
                                    DETALLE_DIRECCION1 = mDetalleDireccion,
                                    DIRECCION = "DETALLADA",
                                    CAMPO_1 = string.Empty,
                                    CAMPO_2 = string.Empty,
                                    CAMPO_3 = string.Empty,
                                    CAMPO_4 = string.Empty,
                                    CAMPO_5 = string.Empty,
                                    CAMPO_6 = cliente.Departamento.ToUpper(),
                                    CAMPO_7 = cliente.Municipio.ToUpper(),
                                    CAMPO_8 = "Sí",
                                    CAMPO_9 = "Sí",
                                    CAMPO_10 = cliente.Indicaciones.ToUpper() + " " + cliente.Calle_avenida.ToUpper(),
                                    NoteExistsFlag = 0,
                                    RowPointer = Guid.NewGuid(),
                                    RecordDate = DateTime.Now,
                                    CreatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    UpdatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    CreateDate = DateTime.Now
                                };

                                dbExactus.DETALLE_DIRECCION.AddObject(iDetalleDireccion);

                                DETALLE_DIRECCION iDetalleDireccion2 = new DETALLE_DIRECCION
                                {
                                    DETALLE_DIRECCION1 = mDetalleDireccion2,
                                    DIRECCION = "DETALLADA",
                                    CAMPO_1 = cliente.Calle_avenida.ToUpper(),
                                    CAMPO_2 = cliente.Casa.ToUpper(),
                                    CAMPO_3 = cliente.Apartamento.ToUpper(),
                                    CAMPO_4 = cliente.Zona.ToUpper(),
                                    CAMPO_5 = cliente.Colonia.ToUpper(),
                                    CAMPO_6 = cliente.Departamento.ToUpper(),
                                    CAMPO_7 = cliente.Municipio.ToUpper(),
                                    CAMPO_8 = "Sí",
                                    CAMPO_9 = "Sí",
                                    CAMPO_10 = cliente.Indicaciones.ToUpper(),
                                    NoteExistsFlag = 0,
                                    RowPointer = Guid.NewGuid(),
                                    RecordDate = DateTime.Now,
                                    CreatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    UpdatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    CreateDate = DateTime.Now
                                };

                                dbExactus.DETALLE_DIRECCION.AddObject(iDetalleDireccion2);
                                #endregion

                                //NIT
                                var qNit = (from n in dbExactus.NIT where n.NIT1 == cliente.NIT select n);
                                if (qNit.Count() == 0)
                                {
                                    Models.NIT iNit = new Models.NIT
                                    {
                                        NIT1 = cliente.NIT.ToUpper(),
                                        RAZON_SOCIAL = cliente.Nombres.ToUpper() + " " + cliente.Apellidos,
                                        ALIAS = cliente.Nombres.ToUpper() + " " + cliente.Apellidos,
                                        NOTAS = "",
                                        TIPO = "ND",
                                        ORIGEN = "O",
                                        ACTIVO = "S",
                                        NoteExistsFlag = 0,
                                        RowPointer = Guid.NewGuid(),
                                        RecordDate = DateTime.Now,
                                        CreatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                        UpdatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                        CreateDate = DateTime.Now
                                    };

                                    dbExactus.NIT.AddObject(iNit);
                                }

                                //CLIENTE

                                //OBTENER EL CONSECUTIVO SIGUIENTE
                                var qConsecutivo = (from c in dbExactus.CONSECUTIVO where c.CONSECUTIVO1.Equals("CLIENTES") select c);
                                
                                string Consecutivo = (int.Parse(qConsecutivo.FirstOrDefault().ULTIMO_VALOR) + 1).ToString();

                                Consecutivo = Consecutivo.ToString().PadLeft((int)qConsecutivo.FirstOrDefault().LONGITUD, '0');

                                CLIENTE cli = new CLIENTE
                                {
                                    CLIENTE1 = Consecutivo,

                                    NOMBRE = cliente.Nombres.ToUpper() + " " + cliente.Apellidos.ToUpper(),
                                    DETALLE_DIRECCION = mDetalleDireccion,
                                    ALIAS = cliente.Nombres.ToUpper() + " " + cliente.Apellidos.ToUpper(),
                                    DIRECCION = mDireccionFacturacion,
                                    TELEFONO1 = cliente.Telefono,
                                    TELEFONO2 = string.Empty,
                                    FAX = string.Empty,
                                    CONTRIBUYENTE = cliente.NIT.ToUpper(),
                                    NIVEL_PRECIO = mNivelPrecio,

                                    ZONA = mZona,
                                    VENDEDOR = Const.TIENDA_VIRTUAL.VENDEDOR_FACTURACION,
                                    COBRADOR = Const.TIENDA_VIRTUAL.COBRADOR,
                                    CATEGORIA_CLIENTE = Const.TIENDA_VIRTUAL.CATEGORIA_CLIENTE,
                                    E_MAIL = cliente.Email.ToLower(),
                                    RUBRO1_CLIENTE = string.Empty, //nombre de la empresa donde labora
                                    RUBRO2_CLIENTE = "", //q[0].EmpresaDireccion,
                                    RUBRO3_CLIENTE = string.Empty,
                                    NOTAS = string.Empty,
                                    RUBRO4_CLIENTE = string.Empty,
                                    RUBRO5_CLIENTE = string.Empty,
                                    RUBRO11_CLIENTE = string.Empty,
                                    RUBRO12_CLIENTE = string.Empty,
                                    RUBRO13_CLIENTE = string.Empty,
                                    RUBRO14_CLIENTE = string.Empty,
                                    RUBRO15_CLIENTE = "", //q[0].Anticipo,
                                    CONTACTO = (cliente.RecibirInfo ? "Quiero recibir información de ofertas/promos por email" : string.Empty),
                                    CARGO = "",
                                    DIR_EMB_DEFAULT = "ND",
                                    FECHA_INGRESO = new DateTime(DateTime.Now.Date.Year, DateTime.Now.Date.Month, DateTime.Now.Date.Day),
                                    MULTIMONEDA = "N",
                                    MONEDA = "GTQ",
                                    SALDO = 0,
                                    SALDO_LOCAL = 0,
                                    SALDO_DOLAR = 0,
                                    SALDO_CREDITO = 0,
                                    SALDO_NOCARGOS = 0,
                                    EXCEDER_LIMITE = "N",
                                    TASA_INTERES = 0,
                                    TASA_INTERES_MORA = 0,
                                    FECHA_ULT_MORA = new DateTime(1980, 1, 1),
                                    FECHA_ULT_MOV = new DateTime(DateTime.Now.Date.Year, DateTime.Now.Date.Month, DateTime.Now.Date.Day),
                                    CONDICION_PAGO = "1",
                                    DESCUENTO = 0,
                                    MONEDA_NIVEL = "L",
                                    ACEPTA_BACKORDER = "N",
                                    PAIS = "GUA",
                                    RUTA = "ND",
                                    ACEPTA_FRACCIONES = "N",
                                    ACTIVO = "S",
                                    CODIGO_IMPUESTO = "IVA",
                                    EXENTO_IMPUESTOS = "N",
                                    EXENCION_IMP1 = 0,
                                    EXENCION_IMP2 = 0,
                                    COBRO_JUDICIAL = "N",
                                    CLASE_ABC = "A",
                                    DIAS_ABASTECIMIEN = 0,
                                    USA_TARJETA = "N",
                                    REQUIERE_OC = "N",
                                    ES_CORPORACION = "S",
                                    REGISTRARDOCSACORP = "N",
                                    USAR_DIREMB_CORP = "N",
                                    APLICAC_ABIERTAS = "N",
                                    VERIF_LIMCRED_CORP = "N",
                                    USAR_DESC_CORP = "N",
                                    DOC_A_GENERAR = "F",
                                    TIENE_CONVENIO = "N",
                                    DIAS_PROMED_ATRASO = 0,
                                    ASOCOBLIGCONTFACT = "N",
                                    USAR_PRECIOS_CORP = "N",
                                    RowPointer = Guid.NewGuid(),
                                    USAR_EXENCIMP_CORP = "N",
                                    AJUSTE_FECHA_COBRO = "A",
                                    CLASE_DOCUMENTO = "N",
                                    LOCAL = "L",
                                    DETALLAR_KITS = "N",
                                    TIPO_CONTRIBUYENTE = "F",
                                    ACEPTA_DOC_ELECTRONICO = "N",
                                    CONFIRMA_DOC_ELECTRONICO = "N",
                                    ACEPTA_DOC_EDI = "N",
                                    NOTIFICAR_ERROR_EDI = "N",
                                    NoteExistsFlag = 0,
                                    RecordDate = DateTime.Now,
                                    CreatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    UpdatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    CreateDate = DateTime.Now,
                                    MOROSO = "N",
                                    MODIF_NOMB_EN_FAC = "N",
                                    SALDO_TRANS = 0,
                                    SALDO_TRANS_LOCAL = 0,
                                    SALDO_TRANS_DOLAR = 0,
                                    PERMITE_DOC_GP = "N",
                                    PARTICIPA_FLUJOCAJA = "N",
                                    USUARIO_CREACION = "VIRTUAL",
                                    FECHA_HORA_CREACION = DateTime.Now
                                };

                                dbExactus.CLIENTE.AddObject(cli);



                                CLIENTE_VENDEDOR iCLIENTE_VENDEDOR = new CLIENTE_VENDEDOR
                                {
                                    CLIENTE = Consecutivo,
                                    VENDEDOR = Const.TIENDA_VIRTUAL.VENDEDOR_FACTURACION,
                                    NoteExistsFlag = 0,
                                    RecordDate = DateTime.Now,
                                    CreatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    UpdatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    CreateDate = DateTime.Now,
                                    RowPointer = Guid.NewGuid()
                                };

                                dbExactus.CLIENTE_VENDEDOR.AddObject(iCLIENTE_VENDEDOR);

                                Models.DIRECC_EMBARQUE iDIRECC_EMBARQUE = new Models.DIRECC_EMBARQUE
                                {
                                    CLIENTE = Consecutivo,
                                    DIRECCION = "ND",
                                    DETALLE_DIRECCION = mDetalleDireccion2,
                                    DESCRIPCION = mDireccionFacturacion,
                                    NoteExistsFlag = 0,
                                    RecordDate = DateTime.Now,
                                    CreatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    UpdatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    CreateDate = DateTime.Now,
                                    RowPointer = Guid.NewGuid()
                                };

                                dbExactus.DIRECC_EMBARQUE.AddObject(iDIRECC_EMBARQUE);

                                //GRABAR EN MF_CLIENTE

                                Models.MF_Cliente iMF_Cliente = new Models.MF_Cliente
                                {
                                    CLIENTE = Consecutivo,
                                    PRIMER_NOMBRE = cliente.Nombres.Substring(0, cliente.Nombres.Contains(" ") ? cliente.Nombres.IndexOf(" ") : cliente.Nombres.Length).ToUpper(),
                                    SEGUNDO_NOMBRE = cliente.Nombres.Substring(cliente.Nombres.Contains(" ") ? cliente.Nombres.IndexOf(" ") + 1 : cliente.Nombres.Length).ToUpper(),
                                    TERCER_NOMBRE = string.Empty,
                                    PRIMER_APELLIDO = cliente.Apellidos.Substring(0, cliente.Apellidos.Contains(" ") ? cliente.Apellidos.IndexOf(" ") : cliente.Apellidos.Length).ToUpper(),
                                    SEGUNDO_APELLIDO = cliente.Apellidos.Substring(cliente.Apellidos.Contains(" ") ? cliente.Apellidos.IndexOf(" ") + 1 : cliente.Apellidos.Length).ToUpper(),
                                    APELLIDO_CASADA = string.Empty,
                                    CF = "N",
                                    DPI = cliente.Identificacion,
                                    GENERO = cliente.Genero,
                                    FECHA_NACIMIENTO = cliente.FechaNacimiento.Year == 1 ? DateTime.Now : cliente.FechaNacimiento,
                                    CELULAR = cliente.Telefono,
                                    FACTURAR_OTRO_NOMBRE = "N",
                                    NIT_FACTURA = cliente.NIT,
                                    NOMBRE_FACTURA = cliente.Nombres + " " + cliente.Apellidos,
                                    USAR_DIRECC_EN_FACTURA = "S",
                                    ENTRA_CAMION = "S",
                                    ENTRA_PICKUP = "S",
                                    ES_SEGUNDO_PISO = "S",
                                    INDICACIONES = cliente.Indicaciones + " " + cliente.Calle_avenida,
                                    CALLE_AVENIDA_FACTURA = cliente.Calle_avenida.Length > 4 ? cliente.Calle_avenida.Substring(0, 4) : cliente.Calle_avenida,
                                    CASA_FACTURA = cliente.Casa.ToUpper().Contains("CASA") ? cliente.Casa.Substring(cliente.Casa.ToUpper().IndexOf("CASA") + 4, 10) : cliente.Calle_avenida.Length > 4 ? cliente.Calle_avenida.Substring(0, 4) : cliente.Calle_avenida,
                                    ZONA_FACTURA = mZona,
                                    APARTAMENTO_FACTURA = string.Empty,
                                    COLONIA_FACTURA = string.Empty,
                                    DEPARTAMENTO_FACTURA = cliente.Departamento.ToUpper(),
                                    MUNICIPIO_FACTURA = cliente.Municipio,
                                    PROFESION = string.Empty,
                                    ESTADO_CIVIL = "S",
                                    NOMBRE_CONYUGE = string.Empty,
                                    PROFESION2 = string.Empty,
                                    PUESTO2 = string.Empty,
                                    NACIONALIDAD = string.Empty,
                                    TIENE_VEHICULO = "N",
                                    MARCA_MODELO = string.Empty,
                                    PLACAS = string.Empty,
                                    VIVE_EN = "N",
                                    PAGO_MENSUAL = string.Empty,
                                    TIEMPO_RESIDIR = string.Empty,
                                    FINANCIERA_PAGA = string.Empty,
                                    CONTADOR = string.Empty,
                                    RecordDate = DateTime.Now,
                                    CreatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    UpdatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    CreateDate = DateTime.Now,
                                    DEPARTAMENTO_EMPRESA = cliente.Departamento.ToUpper(),
                                    MUNICIPIO_EMPRESA = cliente.Municipio.ToUpper(),
                                    FECHA_REGISTRO = DateTime.Now,
                                    CALLE_EMPRESA = "",
                                    CASA_EMPRESA = cliente.Casa.Length > 10 ? cliente.Casa.ToUpper().Contains("CASA") ? cliente.Casa.Substring(cliente.Casa.ToUpper().IndexOf("CASA") + 4, 10) : "" : string.Empty,
                                    ZONA_EMPRESA = "--",
                                    COLONIA_EMPRESA = "",
                                    APARTAMENTO_EMPRESA = cliente.Apartamento.ToUpper(),
                                    //adicionales
                                    REF_COMERCIAL_CELULAR1 = "",
                                    REF_COMERCIAL_CELULAR2 = "",
                                    REF_COMERCIAL_FAX1 = "",
                                    REF_COMERCIAL_FAX2 = "",
                                    REF_COMERCIAL_NOMBRE1 = "",
                                    REF_COMERCIAL_NOMBRE2 = "",
                                    REF_COMERCIAL_TELEFONO1 = "",
                                    REF_COMERCIAL_TELEFONO2 = "",
                                    REF_PERSONAL_CELULAR1 = "",
                                    REF_PERSONAL_CELULAR2 = "",
                                    REF_PERSONAL_NOMBRE1 = "",
                                    REF_PERSONAL_NOMBRE2 = "",
                                    REF_PERSONAL_TEL_RESIDENCIA1 = "",
                                    REF_PERSONAL_TEL_RESIDENCIA2 = "",
                                    REF_PERSONAL_TEL_TRABAJO1 = "",
                                    REF_PERSONAL_TEL_TRABAJO2 = "",
                                    REFERENCIAS_BANCARIAS = "",
                                    DIRECCION_EMPRESA = "",
                                    EXTENSION = "",
                                    CARGAS_FAMILIARES = "",
                                    POLITICO = "N",
                                    CONYUGUE_CARGO = "",
                                    CONYUGUE_DPI = "",
                                    CONYUGUE_EMPRESA = "",
                                    CONYUGUE_EMPRESA_DIRECCION = "",
                                    CONYUGUE_EMPRESA_TELEFONO = string.Empty

                                };

                                dbExactus.MF_Cliente.AddObject(iMF_Cliente);


                                //ACTUALIZAR EL CONSECUTIVO
                                var nConsecutivo = (from c in dbExactus.CONSECUTIVO where c.CONSECUTIVO1.Equals("CLIENTES") select c);
                                foreach (var item in nConsecutivo)
                                    item.ULTIMO_VALOR = Consecutivo;

                                dbExactus.SaveChanges();
                                transactionScope.Complete();
                                codigoCliente = Consecutivo;
                            }
                        }
                        else
                        {
                            var CategoriaCliente = dbExactus.MF_Catalogo.Where(x => x.CODIGO_TABLA== 11 
                            && x.CODIGO_CAT == "CATEGORIA CLIENTE ART DECOMISADOS CREDIPLUS").FirstOrDefault().NOMBRE_CAT;
                            using (TransactionScope transactionScope = new TransactionScope())
                            {


                                #region DetalleDireccion
                                var qDetalleDireccion = from dd in dbCrediplus.DETALLE_DIRECCION  select dd;
                                int mDetalleDireccion = qDetalleDireccion.Max(p => p.DETALLE_DIRECCION1) + 1;
                                int mDetalleDireccion2 = mDetalleDireccion + 1;
                                string mZona = "ND";

                                string mDireccionFacturacion = cliente.Indicaciones;

                                MF.DAL.Exactus.Crediplus.DETALLE_DIRECCION iDetalleDireccion = new MF.DAL.Exactus.Crediplus.DETALLE_DIRECCION
                                {
                                    DETALLE_DIRECCION1 = mDetalleDireccion,
                                    DIRECCION = "DETALLADA",
                                    CAMPO_1 = string.Empty,
                                    CAMPO_2 = string.Empty,
                                    CAMPO_3 = string.Empty,
                                    CAMPO_4 = string.Empty,
                                    CAMPO_5 = string.Empty,
                                    CAMPO_6 = cliente.Departamento.ToUpper(),
                                    CAMPO_7 = cliente.Municipio.ToUpper(),
                                    CAMPO_8 = "Sí",
                                    CAMPO_9 = "Sí",
                                    CAMPO_10 = cliente.Indicaciones.ToUpper() + " " + cliente.Calle_avenida.ToUpper(),
                                    NoteExistsFlag = 0,
                                    RowPointer = Guid.NewGuid(),
                                    RecordDate = DateTime.Now,
                                    CreatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    UpdatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    CreateDate = DateTime.Now
                                };

                                dbCrediplus.DETALLE_DIRECCION.Add(iDetalleDireccion);

                                MF.DAL.Exactus.Crediplus.DETALLE_DIRECCION iDetalleDireccion2 = new MF.DAL.Exactus.Crediplus.DETALLE_DIRECCION
                                {
                                    DETALLE_DIRECCION1 = mDetalleDireccion2,
                                    DIRECCION = "DETALLADA",
                                    CAMPO_1 = cliente.Calle_avenida.ToUpper(),
                                    CAMPO_2 = cliente.Casa.ToUpper(),
                                    CAMPO_3 = cliente.Apartamento.ToUpper(),
                                    CAMPO_4 = cliente.Zona.ToUpper(),
                                    CAMPO_5 = cliente.Colonia.ToUpper(),
                                    CAMPO_6 = cliente.Departamento.ToUpper(),
                                    CAMPO_7 = cliente.Municipio.ToUpper(),
                                    CAMPO_8 = "Sí",
                                    CAMPO_9 = "Sí",
                                    CAMPO_10 = cliente.Indicaciones.ToUpper(),
                                    NoteExistsFlag = 0,
                                    RowPointer = Guid.NewGuid(),
                                    RecordDate = DateTime.Now,
                                    CreatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    UpdatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    CreateDate = DateTime.Now
                                };

                                dbCrediplus.DETALLE_DIRECCION.Add(iDetalleDireccion2);
                                #endregion

                                //NIT
                                var qNit = (from n in dbCrediplus.NIT where n.NIT1 == cliente.NIT select n);
                                if (qNit.Count() == 0)
                                {
                                    NIT iNit = new NIT
                                    {
                                        NIT1 = cliente.NIT.ToUpper(),
                                        RAZON_SOCIAL = cliente.Nombres.ToUpper() + " " + cliente.Apellidos,
                                        ALIAS = cliente.Nombres.ToUpper() + " " + cliente.Apellidos,
                                        NOTAS = "",
                                        TIPO = "ND",
                                        ORIGEN = "O",
                                        ACTIVO = "S",
                                        NoteExistsFlag = 0,
                                        RowPointer = Guid.NewGuid(),
                                        RecordDate = DateTime.Now,
                                        CreatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                        UpdatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                        CreateDate = DateTime.Now
                                    };

                                    dbCrediplus.NIT.Add(iNit);
                                }

                                //CLIENTE

                                //OBTENER EL CONSECUTIVO SIGUIENTE
                                var Consecutivo = (from c in dbCrediplus.CONSECUTIVO where c.CONSECUTIVO1.Equals("CLIENTES") select c).FirstOrDefault().ULTIMO_VALOR;
                                Consecutivo = (int.Parse(Consecutivo) + 1).ToString();
                                
                                Consecutivo = Consecutivo.ToString().PadLeft(CONSTANTES.CREDIPLUS.LONGITUD_CLIENTE, '0');

                                MF.DAL.Exactus.Crediplus.CLIENTE cli = new MF.DAL.Exactus.Crediplus.CLIENTE
                                {
                                    CLIENTE1 = Consecutivo,

                                    NOMBRE = cliente.Nombres.ToUpper() + " " + cliente.Apellidos.ToUpper(),
                                    DETALLE_DIRECCION = mDetalleDireccion,
                                    ALIAS = cliente.Nombres.ToUpper() + " " + cliente.Apellidos.ToUpper(),
                                    DIRECCION = mDireccionFacturacion,
                                    TELEFONO1 = cliente.Telefono,
                                    TELEFONO2 = string.Empty,
                                    FAX = string.Empty,
                                    CONTRIBUYENTE = cliente.NIT.ToUpper(),
                                    NIVEL_PRECIO = mNivelPrecio,

                                    ZONA = mZona,
                                    VENDEDOR = CONSTANTES.CREDIPLUS.VENDEDOR,
                                    COBRADOR = CONSTANTES.CREDIPLUS.COBRADOR,
                                    CATEGORIA_CLIENTE = CategoriaCliente,
                                    E_MAIL = cliente.Email.ToLower(),
                                    RUBRO1_CLIENTE = string.Empty, //nombre de la empresa donde labora
                                    RUBRO2_CLIENTE = "", //q[0].EmpresaDireccion,
                                    RUBRO3_CLIENTE = string.Empty,
                                    NOTAS = string.Empty,
                                    RUBRO4_CLIENTE = string.Empty,
                                    RUBRO5_CLIENTE = string.Empty,
                                    RUBRO11_CLIENTE = string.Empty,
                                    RUBRO12_CLIENTE = string.Empty,
                                    RUBRO13_CLIENTE = string.Empty,
                                    RUBRO14_CLIENTE = string.Empty,
                                    RUBRO15_CLIENTE = "", //q[0].Anticipo,
                                    CONTACTO = string.Empty,
                                    CARGO = "",
                                    DIR_EMB_DEFAULT = "ND",
                                    FECHA_INGRESO = new DateTime(DateTime.Now.Date.Year, DateTime.Now.Date.Month, DateTime.Now.Date.Day),
                                    MULTIMONEDA = "N",
                                    MONEDA = "GTQ",
                                    SALDO = 0,
                                    SALDO_LOCAL = 0,
                                    SALDO_DOLAR = 0,
                                    SALDO_CREDITO = 0,
                                    SALDO_NOCARGOS = 0,
                                    EXCEDER_LIMITE = "N",
                                    TASA_INTERES = 0,
                                    TASA_INTERES_MORA = 0,
                                    FECHA_ULT_MORA = new DateTime(1980, 1, 1),
                                    FECHA_ULT_MOV = new DateTime(DateTime.Now.Date.Year, DateTime.Now.Date.Month, DateTime.Now.Date.Day),
                                    CONDICION_PAGO = CONSTANTES.CREDIPLUS.CONDICION_PAGO,
                                    DESCUENTO = 0,
                                    MONEDA_NIVEL = "L",
                                    ACEPTA_BACKORDER = "N",
                                    PAIS = "GUA",
                                    RUTA = "ND",
                                    ACEPTA_FRACCIONES = "N",
                                    ACTIVO = "S",
                                    CODIGO_IMPUESTO = "IVA",
                                    EXENTO_IMPUESTOS = "N",
                                    EXENCION_IMP1 = 0,
                                    EXENCION_IMP2 = 0,
                                    COBRO_JUDICIAL = "N",
                                    CLASE_ABC = "A",
                                    DIAS_ABASTECIMIEN = 0,
                                    USA_TARJETA = "N",
                                    REQUIERE_OC = "N",
                                    ES_CORPORACION = "S",
                                    REGISTRARDOCSACORP = "N",
                                    USAR_DIREMB_CORP = "N",
                                    APLICAC_ABIERTAS = "N",
                                    VERIF_LIMCRED_CORP = "N",
                                    USAR_DESC_CORP = "N",
                                    DOC_A_GENERAR = "F",
                                    TIENE_CONVENIO = "N",
                                    DIAS_PROMED_ATRASO = 0,
                                    ASOCOBLIGCONTFACT = "N",
                                    USAR_PRECIOS_CORP = "N",
                                    RowPointer = Guid.NewGuid(),
                                    USAR_EXENCIMP_CORP = "N",
                                    AJUSTE_FECHA_COBRO = "A",
                                    CLASE_DOCUMENTO = "N",
                                    LOCAL = "L",
                                    DETALLAR_KITS = "N",
                                    TIPO_CONTRIBUYENTE = "F",
                                    ACEPTA_DOC_ELECTRONICO = "N",
                                    CONFIRMA_DOC_ELECTRONICO = "N",
                                    ACEPTA_DOC_EDI = "N",
                                    NOTIFICAR_ERROR_EDI = "N",
                                    NoteExistsFlag = 0,
                                    RecordDate = DateTime.Now,
                                    CreatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    UpdatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    CreateDate = DateTime.Now,
                                    MOROSO = "N",
                                    MODIF_NOMB_EN_FAC = "N",
                                    SALDO_TRANS = 0,
                                    SALDO_TRANS_LOCAL = 0,
                                    SALDO_TRANS_DOLAR = 0,
                                    PERMITE_DOC_GP = "N",
                                    PARTICIPA_FLUJOCAJA = "N",
                                    USUARIO_CREACION = "VIRTUAL",
                                    FECHA_HORA_CREACION = DateTime.Now
                                };

                                dbCrediplus.CLIENTE.Add(cli);



                                MF.DAL.Exactus.Crediplus.CLIENTE_VENDEDOR iCLIENTE_VENDEDOR = new MF.DAL.Exactus.Crediplus.CLIENTE_VENDEDOR
                                {
                                    CLIENTE = Consecutivo,
                                    VENDEDOR = CONSTANTES.CREDIPLUS.VENDEDOR,
                                    NoteExistsFlag = 0,
                                    RecordDate = DateTime.Now,
                                    CreatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    UpdatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    CreateDate = DateTime.Now,
                                    RowPointer = Guid.NewGuid()
                                };

                                dbCrediplus.CLIENTE_VENDEDOR.Add(iCLIENTE_VENDEDOR);

                                MF.DAL.Exactus.Crediplus.DIRECC_EMBARQUE iDIRECC_EMBARQUE = new MF.DAL.Exactus.Crediplus.DIRECC_EMBARQUE
                                {
                                    CLIENTE = Consecutivo,
                                    DIRECCION = "ND",
                                    DETALLE_DIRECCION = mDetalleDireccion2,
                                    DESCRIPCION = mDireccionFacturacion,
                                    NoteExistsFlag = 0,
                                    RecordDate = DateTime.Now,
                                    CreatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    UpdatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    CreateDate = DateTime.Now,
                                    RowPointer = Guid.NewGuid()
                                };

                                dbCrediplus.DIRECC_EMBARQUE.Add(iDIRECC_EMBARQUE);

                                //GRABAR EN MF_CLIENTE

                                MF_Cliente iMF_Cliente = new MF_Cliente
                                {
                                    CLIENTE = Consecutivo,
                                    PRIMER_NOMBRE = cliente.Nombres.Substring(0, cliente.Nombres.Contains(" ") ? cliente.Nombres.IndexOf(" ") : cliente.Nombres.Length).ToUpper(),
                                    SEGUNDO_NOMBRE = cliente.Nombres.Substring(cliente.Nombres.Contains(" ") ? cliente.Nombres.IndexOf(" ") + 1 : cliente.Nombres.Length).ToUpper(),
                                    TERCER_NOMBRE = string.Empty,
                                    PRIMER_APELLIDO = cliente.Apellidos.Substring(0, cliente.Apellidos.Contains(" ") ? cliente.Apellidos.IndexOf(" ") : cliente.Apellidos.Length).ToUpper(),
                                    SEGUNDO_APELLIDO = cliente.Apellidos.Substring(cliente.Apellidos.Contains(" ") ? cliente.Apellidos.IndexOf(" ") + 1 : cliente.Apellidos.Length).ToUpper(),
                                    APELLIDO_CASADA = string.Empty,
                                    CF = "N",
                                    DPI = cliente.Identificacion,
                                    GENERO = cliente.Genero,
                                    FECHA_NACIMIENTO = cliente.FechaNacimiento.Year == 1 ? DateTime.Now : cliente.FechaNacimiento,
                                    CELULAR = cliente.Telefono,
                                    FACTURAR_OTRO_NOMBRE = "N",
                                    NIT_FACTURA = cliente.NIT,
                                    NOMBRE_FACTURA = cliente.Nombres + " " + cliente.Apellidos,
                                    USAR_DIRECC_EN_FACTURA = "S",
                                    ENTRA_CAMION = "S",
                                    ENTRA_PICKUP = "S",
                                    ES_SEGUNDO_PISO = "S",
                                    INDICACIONES = cliente.Indicaciones + " " + cliente.Calle_avenida,
                                    CALLE_AVENIDA_FACTURA = cliente.Calle_avenida.Length > 4 ? cliente.Calle_avenida.Substring(0, 4) : cliente.Calle_avenida,
                                    CASA_FACTURA = cliente.Casa.ToUpper().Contains("CASA") ? cliente.Casa.Substring(cliente.Casa.ToUpper().IndexOf("CASA") + 4, 10) : cliente.Calle_avenida.Length > 4 ? cliente.Calle_avenida.Substring(0, 4) : cliente.Calle_avenida,
                                    ZONA_FACTURA = mZona,
                                    APARTAMENTO_FACTURA = string.Empty,
                                    COLONIA_FACTURA = string.Empty,
                                    DEPARTAMENTO_FACTURA = cliente.Departamento.ToUpper(),
                                    MUNICIPIO_FACTURA = cliente.Municipio,
                                    PROFESION = string.Empty,
                                    ESTADO_CIVIL = "S",
                                    NOMBRE_CONYUGE = string.Empty,
                                    PROFESION2 = string.Empty,
                                    PUESTO2 = string.Empty,
                                    NACIONALIDAD = string.Empty,
                                    TIENE_VEHICULO = "N",
                                    MARCA_MODELO = string.Empty,
                                    PLACAS = string.Empty,
                                    VIVE_EN = "N",
                                    PAGO_MENSUAL = string.Empty,
                                    TIEMPO_RESIDIR = string.Empty,
                                    FINANCIERA_PAGA = string.Empty,
                                    CONTADOR = string.Empty,
                                    RecordDate = DateTime.Now,
                                    CreatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    UpdatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                    CreateDate = DateTime.Now,
                                    DEPARTAMENTO_EMPRESA = cliente.Departamento.ToUpper(),
                                    MUNICIPIO_EMPRESA = cliente.Municipio.ToUpper(),
                                    FECHA_REGISTRO = DateTime.Now,
                                    CALLE_EMPRESA = "",
                                    CASA_EMPRESA = cliente.Casa.Length > 10 ? cliente.Casa.ToUpper().Contains("CASA") ? cliente.Casa.Substring(cliente.Casa.ToUpper().IndexOf("CASA") + 4, 10) : "" : string.Empty,
                                    ZONA_EMPRESA = "--",
                                    COLONIA_EMPRESA = "",
                                    APARTAMENTO_EMPRESA = cliente.Apartamento.ToUpper(),
                                    //adicionales
                                    REF_COMERCIAL_CELULAR1 = "",
                                    REF_COMERCIAL_CELULAR2 = "",
                                    REF_COMERCIAL_FAX1 = "",
                                    REF_COMERCIAL_FAX2 = "",
                                    REF_COMERCIAL_NOMBRE1 = "",
                                    REF_COMERCIAL_NOMBRE2 = "",
                                    REF_COMERCIAL_TELEFONO1 = "",
                                    REF_COMERCIAL_TELEFONO2 = "",
                                    REF_PERSONAL_CELULAR1 = "",
                                    REF_PERSONAL_CELULAR2 = "",
                                    REF_PERSONAL_NOMBRE1 = "",
                                    REF_PERSONAL_NOMBRE2 = "",
                                    REF_PERSONAL_TEL_RESIDENCIA1 = "",
                                    REF_PERSONAL_TEL_RESIDENCIA2 = "",
                                    REF_PERSONAL_TEL_TRABAJO1 = "",
                                    REF_PERSONAL_TEL_TRABAJO2 = "",
                                    REFERENCIAS_BANCARIAS = "",
                                    DIRECCION_EMPRESA = "",
                                    EXTENSION = "",
                                    CARGAS_FAMILIARES = "",
                                    POLITICO = "N",
                                    CONYUGUE_CARGO = "",
                                    CONYUGUE_EMPRESA = "",
                                    CONYUGUE_EMPRESA_DIRECCION = "",
                                    CONYUGUE_EMPRESA_TELEFONO = string.Empty

                                };

                                dbCrediplus.MF_Cliente.Add(iMF_Cliente);


                                //ACTUALIZAR EL CONSECUTIVO
                                var qConsecutivo = (from c in dbCrediplus.CONSECUTIVO where c.CONSECUTIVO1.Equals("CLIENTES") select c);
                                foreach (var item in qConsecutivo)
                                    item.ULTIMO_VALOR = Consecutivo;

                                dbCrediplus.SaveChanges();
                                
                                transactionScope.Complete();
                                codigoCliente = Consecutivo;
                            }
                        }
                        mRespuesta.Exito = true;
                        mRespuesta.Objeto = codigoCliente;
                    }

                    catch (Exception ex)
                    {
                        mRespuesta.Exito = false;
                        mRespuesta.Objeto = "";
                        mRespuesta.Mensaje = string.Format("Ocurrió un error al crear al cliente {0} para la tienda virtual", cliente.Nombres+" "+ex.Message + " " + ex.InnerException);
                        log.Error(string.Format("Ocurrió un error al crear al cliente {0} para la tienda virtual "+ex.Message+" "+ex.InnerException, cliente.Nombres), ex);
                    }
                }
            }
            else
            {
                mRespuesta.Exito = false;
                mRespuesta.Mensaje = mensaje;
            }
            return mRespuesta;
        }

        /// <summary>
        /// Consulta de datos de entrega de los clientes de crediplus
        /// </summary>
        /// <param name="numCredito"></param>
        /// <param name="Financiera"></param>
        /// <returns></returns>
        public Respuesta ObtenerDireccionEntrega(string numCredito, int Financiera)
        {
            long Credito = 0;
            long.TryParse(numCredito, out Credito);
            log.Info("---- obteniendo dirección de entrega del crédito: " + numCredito + "-- financiera " + Financiera);
            Respuesta mRespuesta = new Respuesta();
            dtoDireccionCreditos datos = new dtoDireccionCreditos();
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            List<ArticuloCredito> lstArticulos = new List<ArticuloCredito>();
            db.CommandTimeout = 999999999;
            try
            {
                var qConsulta = (from p in db.MF_Pedido
                                 join f in db.FACTURA on p.FACTURA equals f.FACTURA1
                                 join c in db.CLIENTE on p.CLIENTE equals c.CLIENTE1
                                 join dd in db.DETALLE_DIRECCION on c.DETALLE_DIRECCION equals dd.DETALLE_DIRECCION1
                                 where
                                 p.FINANCIERA == Financiera
                                 && p.SOLICITUD == Credito
                                 select new
                                 {
                                     CLIENTE = c.NOMBRE,
                                     dd.CAMPO_1,
                                     dd.CAMPO_2,
                                     dd.CAMPO_5,
                                     dd.CAMPO_3,
                                     dd.CAMPO_6,
                                     dd.CAMPO_7,
                                     dd.CAMPO_4,
                                     INDICACIONES = dd.CAMPO_10,
                                     ESTADO = f.ANULADA.Equals("S") ? "FACTURA ANULADA" : f.ESTA_DESPACHADO.Equals("S") ? "DESPACHADO" : "NO DESPACHADO",
                                     Factura = f.FACTURA1
                                 }
                                 );
                if (qConsulta.Count() > 0)
                {
                    foreach (var item in qConsulta)
                    {
                        datos.CLIENTE = item.CLIENTE;
                        datos.DIRECCION = string.Format("{0} {1} {2} {3} {4} {5}, {6} ", item.CAMPO_1, item.CAMPO_2, item.CAMPO_5, item.CAMPO_3, (item.CAMPO_4.Equals("") == false || item.CAMPO_4.Equals("00") == false) ? "ZONA " + item.CAMPO_4 : string.Empty, item.CAMPO_6, item.CAMPO_7);
                        datos.INDICACIONES = item.INDICACIONES;
                        datos.ESTADO = item.ESTADO;
                        datos.FACTURA = item.Factura;
                    }

                    var qArticulosFactura = db.FACTURA_LINEA.Join(db.ARTICULO, fl => fl.ARTICULO, u => u.ARTICULO1, (fl, u) => new { fl, u }).Where(fl => fl.fl.FACTURA == datos.FACTURA);
                    if (qArticulosFactura.Count() > 0)
                        foreach (var a in qArticulosFactura)
                        {
                            lstArticulos.Add(new ArticuloCredito { Articulo = a.fl.ARTICULO, Descripcion = a.u.DESCRIPCION, cantidad = a.fl.CANTIDAD, Precio = a.fl.PRECIO_TOTAL });
                        }
                    datos.ARTICULOS = lstArticulos;
                    mRespuesta.Exito = true;
                    mRespuesta.Objeto = datos;
                }
            }
            catch (Exception ex)
            {
                mRespuesta.Exito = false;
                log.Error("Error al consultar la dirección del crédito " + numCredito + " error: " + ex.Message);
            }
            return mRespuesta;
        }
    }
}