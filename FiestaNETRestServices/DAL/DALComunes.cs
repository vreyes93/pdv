﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Models.ATID;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using static MF_Clases.Clases;
using MF_Clases.Facturacion;
using FiestaNETRestServices.Models;
using MF_Clases.Comun;
using MF.Comun.Dto;
using FiestaNETRestServices.Controllers.FacturaElectronica;
using MF_Clases.GuateFacturas.FEL.ImpresionFEL;

namespace FiestaNETRestServices.DAL
{
    /// <summary>
    /// Esta clase tiene como funcion almacenar todas las consultas comunes que se realizan a las tablas y que pueden ser utilizadas
    /// por las distintas areas de negocio. (Facturación, bodega, contabilidad, etc.)
    /// </summary>

    public class DALComunes : MFDAL
    {

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función tiene como propósito obtener el listado de un catalogo en específico
        /// </summary>
        /// <param name="pCatalogo">Catalogo registrado en el sistema.</param>
        /// <returns>Retorna una lista List<Catalogo> </returns>
        public Respuesta ObtenerCatalogo(int pCatalogo)
        {
            Respuesta mRespuesta = new Respuesta();
            List<Catalogo> mListado = new List<Catalogo>();
            try
            {


                var mListadoCatalogo = (from TS in dbExactus.MF_Catalogo
                                        where TS.CODIGO_TABLA == pCatalogo
                                        orderby TS.ORDEN
                                        select TS
                                );

                if (mListadoCatalogo.Count() > 0)
                {
                    foreach (var item in mListadoCatalogo)
                    {
                        Catalogo mCatalogo = new Catalogo();
                        mCatalogo.Codigo = item.CODIGO_CAT;
                        mCatalogo.Descripcion = item.NOMBRE_CAT;
                        mListado.Add(mCatalogo);
                    }
                }
                else
                {
                    return new Respuesta(false, string.Format("Verificar la tabla de catálogos, ID - {0}.", pCatalogo));
                }

                mRespuesta.Exito = true;
                mRespuesta.Objeto = mListado;
                return mRespuesta;
            }
            catch (Exception e)
            {
                log.Error(string.Format("{0}", e));
                return new Respuesta(false, e);
            }
        }


        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función tiene como propósito obtener el listado de un catalogo en específico
        /// </summary>
        /// <param name="pCatalogo">Catalogo registrado en el sistema.</param>
        /// <returns>Retorna una lista List<Catalogo> </returns>
        public Respuesta ObtenerCatalogo(List<int> pCatalogos)
        {
            Respuesta mRespuesta = new Respuesta();
            List<Catalogo> mListado = new List<Catalogo>();
            try
            {
                var mListadoCatalogo = (from TS in dbExactus.MF_Catalogo
                                        where pCatalogos.Contains(TS.CODIGO_TABLA)
                                        orderby TS.ORDEN
                                        select TS
                                );
                if (mListadoCatalogo.Count() > 0)
                {
                    foreach (var item in mListadoCatalogo)
                    {
                        Catalogo mCatalogo = new Catalogo();
                        mCatalogo.Codigo = item.CODIGO_CAT;
                        mCatalogo.Descripcion = item.NOMBRE_CAT;
                        mListado.Add(mCatalogo);
                    }
                }
                else
                {
                    return new Respuesta(false, string.Format("Verificar la tabla de catálogos, ID - {0}.", pCatalogos.ToString()));
                }
                mRespuesta.Exito = true;
                mRespuesta.Objeto = mListado;
                return mRespuesta;
            }
            catch (Exception e)
            {
                log.Error(string.Format("{0}", e));
                return new Respuesta(false, e);
            }

        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función tiene como objetivo obtener el listado de las regiones de tiendas según la empresa
        /// </summary>
        /// <param name="pEmpresa">Codigo de la empresa a solicitar sus regiones.</param>
        /// <returns>Retorna una lista List<Catalogo> </returns>
        public Respuesta ObtenerRegion(byte pEmpresa)
        {
            Respuesta mRespuesta = new Respuesta();
            List<MF_Region> mListado = new List<MF_Region>();
            try
            {


                var mListadoCatalogo = (from RT in dbExactus.MF_RegionTienda
                                        join R in dbExactus.MF_Region on RT.REGION equals R.REGION
                                        join E in dbExactus.MF_Empresa on RT.EMPRESA equals E.EMPRESA
                                        where R.ACTIVO == true
                                                && E.ACTIVO == true
                                                && E.EMPRESA == pEmpresa
                                        orderby E.EMPRESA
                                        select R
                                ).Distinct();

                if (mListadoCatalogo.Count() > 0)
                {
                    foreach (var item in mListadoCatalogo)
                    {

                        MF_Region mItem = new MF_Region();
                        mItem.REGION = item.REGION;
                        mItem.NOMBRE = item.NOMBRE;
                        mListado.Add(mItem);
                    }
                }
                else
                {
                    return new Respuesta(false, string.Format("Verificarlas regiones de la empresa on ID - {0}.", pEmpresa));
                }

                mRespuesta.Exito = true;
                mRespuesta.Objeto = mListado;
                return mRespuesta;
            }
            catch (Exception e)
            {
                log.Error(string.Format("{0}", e));
                return new Respuesta(false, e);
            }
        }

        public void SincronizaCatalogoOcupacion(List<Ocupacion> mListadoOcupacion)
        {
            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    // Departamento[] mListadoDepartamentos = mListadoProfesion;

                    foreach (var itemOcupacion in mListadoOcupacion)
                    {
                        var mOcupacion = itemOcupacion;
                        //Actualizamos departamento
                        var queryOcupacion = from p in dbExactus.MF_Puesto where p.DESCRIPCION == mOcupacion.descripcion && p.ATID_OCUPACION == null select p;

                        if (queryOcupacion.Count() > 0)
                        {
                            foreach (var profesion in queryOcupacion)
                            {
                                profesion.ATID_OCUPACION = mOcupacion.codigo;
                            }
                            //dbExactus.SaveChanges();
                        }
                    }
                    dbExactus.SaveChanges();
                    transactionScope.Complete();
                    log.Debug("Actualizacion finalizada");
                }

            }
            catch
            {
            }
        }


        public void SincronizaCatalogoProfesion(List<Profesion> mListadoProfesion)
        {
            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    // Departamento[] mListadoDepartamentos = mListadoProfesion;

                    foreach (var itemProfesion in mListadoProfesion)
                    {
                        var mProfesion = itemProfesion;
                        //Actualizamos departamento
                        var queryProfesion = from p in dbExactus.MF_Profesion where p.DESCRIPCION == mProfesion.descripcion && p.ATID_PROFESION == null select p;

                        if (queryProfesion.Count() > 0)
                        {
                            foreach (var profesion in queryProfesion)
                            {
                                profesion.ATID_PROFESION = mProfesion.codigo;
                            }
                            //dbExactus.SaveChanges();
                        }
                    }
                    dbExactus.SaveChanges();
                    transactionScope.Complete();
                    log.Debug("Actualizacion finalizada");
                }

            }
            catch
            {
            }
        }

        public void SincronizaCatalogoDeptoMunicipioCanton(Departamento[] mDepartamentos)
        {
            int depto = 0;
            int muni = 0;
            try
            {
                dbExactus.CommandTimeout = 5 * 60;
                using (TransactionScope transactionScope = new TransactionScope())
                {

                    Departamento[] mListadoDepartamentos = mDepartamentos;

                    foreach (var itemDepartamento in mListadoDepartamentos)
                    {
                        var mDepartamento = itemDepartamento;
                        //Actualizamos departamento
                        var queryDepartamento = from d in dbExactus.MF_Departamento where d.DESCRIPCION == mDepartamento.Descripcion select d;

                        if (queryDepartamento.Count() > 0)
                        {

                            foreach (var departamento in queryDepartamento)
                            {
                                departamento.ATID_DEPARTAMENTO = (int)mDepartamento.Codigo;
                                depto = (int)departamento.DEPARTAMENTO;
                                Ciudad[] mListadoCiudad = mDepartamento.Ciudades;

                                foreach (var itemCiudades in mListadoCiudad)
                                {
                                    var mCiudad = itemCiudades;
                                    //actualizacmos municipio
                                    //Actualizamos departamento
                                    var queryMunicipio = from m in dbExactus.MF_Municipio
                                                         where m.DESCRIPCION == mCiudad.Descripcion && m.DEPARTAMENTO == departamento.DEPARTAMENTO
                        && m.ATID_MUNICIPIO == null
                                                         select m;

                                    if (queryMunicipio.Count() > 0)
                                    {

                                        foreach (var municipio in queryMunicipio)
                                        {
                                            municipio.ATID_DEPARTAMENTO = (int)mDepartamento.Codigo;
                                            municipio.ATID_MUNICIPIO = (int)mCiudad.Codigo;
                                            muni = (int)municipio.MUNICIPIO;
                                            //Canton[] mListadoCanton = mCiudad.Cantones;

                                            //foreach (var itemCantones in mListadoCanton)
                                            //{
                                            //    var mCanton = itemCantones;

                                            //}
                                            //dbExactus.SaveChanges();
                                        }
                                    }

                                }
                            }
                        }

                    }

                    dbExactus.SaveChanges();
                    transactionScope.Complete();
                    log.Debug("Actualizacion finalizada");
                }
            }
            catch
            {
                log.Error("Error al actualizar el canton");
            }

        }

        public Respuesta ObtenerCatalogoHuellaYFoto(int pCatalogo, string tienda)
        {
            Respuesta mRespuesta = new Respuesta();
            List<Catalogo> mListado = new List<Catalogo>();
            try
            {

                var qTienda = (from t in dbExactus.MF_Catalogo
                               where t.CODIGO_TABLA == Models.Const.CATALOGO.CATALOGO_TIENDAS_HUELLA_FOTO
                            && t.CODIGO_CAT == tienda && t.ACTIVO == "S"
                               select t);

                if (qTienda.Count() > 0)
                {
                    var mListadoCatalogo = (from TS in dbExactus.MF_Catalogo
                                            where TS.CODIGO_TABLA == pCatalogo
                                            orderby TS.ORDEN
                                            select TS
                                    );

                    if (mListadoCatalogo.Count() > 0)
                    {
                        foreach (var item in mListadoCatalogo)
                        {
                            Catalogo mCatalogo = new Catalogo();
                            mCatalogo.Codigo = item.CODIGO_CAT;
                            mCatalogo.Descripcion = item.NOMBRE_CAT;
                            mListado.Add(mCatalogo);
                        }
                    }
                    else
                    {
                        return new Respuesta(false, string.Format("Verificar la tabla de catálogos, ID - {0}.", pCatalogo));
                    }
                }
                mRespuesta.Exito = true;
                mRespuesta.Objeto = mListado;
                return mRespuesta;
            }
            catch (Exception e)
            {
                log.Error(string.Format("{0}", e));
                return new Respuesta(false, e);
            }
        }

        public Garantia facturaGarantia(string numeroFactura)
        {
            return dbExactus.MF_Factura
                    .Join(dbExactus.CLIENTE, f => f.CLIENTE, c => c.CLIENTE1, (f, c) => new { f, c })
                    .GroupJoin(dbExactus.MF_CobradorEspecificaciones, f1 => f1.f.COBRADOR, e => e.COBRADOR, (f1, e) => new { f1, e = e.DefaultIfEmpty() })
                    .Where(x => x.f1.f.FACTURA == numeroFactura)
                    .Select(x => new Garantia()
                    {
                        Factura = x.f1.f.FACTURA,
                        Nombre = x.f1.f.NOMBRE_FEL,
                        Fecha = x.f1.f.FECHA,
                        Email = x.f1.c.E_MAIL,
                        CodigoCliente = x.f1.c.CLIENTE1,
                        CodigoTienda = x.f1.f.COBRADOR,
                        Visibilidad = x.e.FirstOrDefault() == null ? null : x.e.FirstOrDefault().VALOR
                    })
                    .FirstOrDefault();
        }

        public Garantia NcND(string numeroFactura)
        {
            return dbExactus.MF_Factura
                    .Join(dbExactus.CLIENTE, f => f.CLIENTE, c => c.CLIENTE1, (f, c) => new { f, c })
                    .Where(x => x.f.FACTURA == numeroFactura)
                    .Select(x => new Garantia()
                    {
                        Factura = x.f.FACTURA,
                        CodigoCliente = x.c.CLIENTE1,
                        Email=x.c.E_MAIL
                    }).Concat(dbExactus.DOCUMENTOS_CC.Join(dbExactus.CLIENTE, d => d.CLIENTE, cli => cli.CLIENTE1, (d, cli) => new { d, cli })
                    .Where(x => x.d.DOCUMENTO == numeroFactura
                    && (x.d.TIPO.Contains("N/C") || x.d.TIPO.Contains("N/D"))
                    && x.d.DOCUMENTO_GLOBAL != null
                    )
                   .Select(x => new Garantia()
                   {
                       Factura = x.d.DOCUMENTO,
                       CodigoCliente = x.cli.CLIENTE1,
                       Email = x.cli.E_MAIL
                   })).FirstOrDefault();

        }

        public TiendaEspecificacion ObtenerAtributo(string tienda, string atributo)
        {
            return dbExactus.MF_CobradorEspecificaciones
                .Where(x => x.COBRADOR == tienda && x.ATRIBUTO == atributo)
                .Select(x => new TiendaEspecificacion() { CodigoTienda = x.COBRADOR, Atributo = x.ATRIBUTO, Valor = x.VALOR })
                .FirstOrDefault();
        }


        public List<ResultadoComisiones> ObtenerResultadosComisiones(DateTime dtFechaIni, DateTime dtFechaFin)
        {
            List<ResultadoComisiones> lstComisiones = new List<ResultadoComisiones>();
            try
            {
                NOMINA_HISTORICO nomina = dbExactus.NOMINA_HISTORICO.FirstOrDefault(x => x.NOMINA == General.ComisionesNomina && x.FECHA_INICIO == dtFechaFin && x.FECHA_FIN == dtFechaFin);

                var qComisiones = (from sC in dbExactus.MF_SincronizacionComision join sD in dbExactus.MF_SincronizacionComisionDetalle on sC.Sincronizacion equals sD.Sincronizacion
                                   join ve in dbExactus.VENDEDOR on sD.Vendedor equals ve.VENDEDOR1
                                   //join emp in dbExactus.EMPLEADO on sD.Empleado equals emp.EMPLEADO1.DefaultIfEmpty()
                                   where
                                   sC.FechaInicio >= dtFechaIni
                                   && sC.FechaFin<= dtFechaFin
                                   && sC.Nomina == nomina.NUMERO_NOMINA
                                   && sC.CodigoNomina == General.ComisionesNomina
                                   && sC.CodigoConcepto == General.ComisionesConcepto //nomina de ventas}
                                   && sD.Actualizado == true
                                   && sD.EstadoEmpleado == "ACTV"
                                   select new
                                   {
                                       Vendedor=sD.Vendedor,
                                       Nombre = ve.NOMBRE,
                                       Tienda =sD.Cobrador,
                                       Empleado=sD.Empleado,
                                       Comision=sD.Comision
                                   }
                                  );
                if (qComisiones.Count() > 0)
                {
                    foreach (var item in qComisiones)
                        lstComisiones.Add(
                            new ResultadoComisiones
                            {
                                Empleado= item.Empleado,
                                MontoComision=item.Comision,
                                Nombre= item.Nombre,
                                Resultado="EXITO",
                                Vendedor=item.Vendedor,
                                Tienda=item.Tienda
                            }
                            );
                }

                var qComisionesNOTNOmina = (from sC in dbExactus.MF_SincronizacionComision
                                            join sD in dbExactus.MF_SincronizacionComisionDetalle on sC.Sincronizacion equals sD.Sincronizacion
                                            join ve in dbExactus.VENDEDOR on sD.Vendedor equals ve.VENDEDOR1
                                            //join emp in dbExactus.EMPLEADO on sD.Empleado equals emp.EMPLEADO1.DefaultIfEmpty()
                                            where
                                            sC.FechaInicio >= dtFechaIni
                                            && sC.FechaFin <= dtFechaFin
                                            && sC.Nomina == nomina.NUMERO_NOMINA
                                            && sC.CodigoNomina == General.ComisionesNomina
                                            && sC.CodigoConcepto == General.ComisionesConcepto //nomina de ventas}
                                            && sD.Actualizado == false
                                            && sD.EstadoEmpleado == "ACTV"
                                            select new
                                            {
                                                Vendedor = sD.Vendedor,
                                                Nombre=ve.NOMBRE,
                                                Tienda = sD.Cobrador,
                                                Empleado = sD.Empleado,
                                                Comision = sD.Comision
                                            }
                                  );
                if (qComisionesNOTNOmina.Count() > 0)
                {
                    foreach (var item in qComisionesNOTNOmina)
                        lstComisiones.Add(
                            new ResultadoComisiones
                            {
                                Empleado = item.Empleado,
                                MontoComision = item.Comision,
                                
                                Nombre =item.Nombre,
                                Resultado = "NO ESTÁ EN LA NÓMINA",
                                Vendedor = item.Vendedor,
                                Tienda = item.Tienda
                            }
                            );
                }

                var qComisionesNOTActivo = (from sC in dbExactus.MF_SincronizacionComision
                                            join sD in dbExactus.MF_SincronizacionComisionDetalle on sC.Sincronizacion equals sD.Sincronizacion
                                            join ve in dbExactus.VENDEDOR on sD.Vendedor equals ve.VENDEDOR1
                                            //join emp in dbExactus.EMPLEADO on sD.Empleado equals emp.EMPLEADO1.DefaultIfEmpty()
                                            where
                                            sC.FechaInicio >= dtFechaIni
                                            && sC.FechaFin <= dtFechaFin
                                            && sC.Nomina == nomina.NUMERO_NOMINA
                                            && sC.CodigoNomina == General.ComisionesNomina
                                            && sC.CodigoConcepto == General.ComisionesConcepto //nomina de ventas}
                                            && sD.Actualizado == false
                                            && sD.EstadoEmpleado != "ACTV"
                                            select new
                                            {
                                                Vendedor = sD.Vendedor,
                                                Nombre = ve.NOMBRE,
                                                Tienda = sD.Cobrador,
                                                Empleado = sD.Empleado,
                                                Comision = sD.Comision
                                            }
                                  );
                if (qComisionesNOTActivo.Count() > 0)
                {
                    foreach (var item in qComisionesNOTActivo)
                        lstComisiones.Add(
                            new ResultadoComisiones
                            {
                                Empleado = item.Empleado,
                                MontoComision = item.Comision,
                                Nombre = item.Nombre,
                                Resultado = "NO ESTA ACTIVO",
                                Vendedor = item.Vendedor,
                                Tienda = item.Tienda
                            }
                            );
                }

                var qComisionesNOTEMPLEADONOMI = (from sC in dbExactus.MF_SincronizacionComision
                                                  join sD in dbExactus.MF_SincronizacionComisionDetalle on sC.Sincronizacion equals sD.Sincronizacion
                                                  join ve in dbExactus.VENDEDOR on sD.Vendedor equals ve.VENDEDOR1

                                                  where
                                                  sC.FechaInicio >= dtFechaIni
                                                  && sC.FechaFin <= dtFechaFin
                                                  && sC.Nomina == nomina.NUMERO_NOMINA
                                                  && sC.CodigoNomina == General.ComisionesNomina
                                                  && sC.CodigoConcepto == General.ComisionesConcepto //nomina de ventas}
                                                  && sD.Actualizado == false
                                                 && sD.Empleado == null
                                                  && sD.Comision > 0
                                                  select new
                                                  {
                                                      Vendedor = sD.Vendedor,
                                                      Nombre = ve.NOMBRE,
                                                      Tienda = sD.Cobrador,
                                                      Empleado = sD.Empleado,
                                                      Comision = sD.Comision
                                                  }
                                  );
                if (qComisionesNOTEMPLEADONOMI.Count() > 0)
                {
                    foreach (var item in qComisionesNOTEMPLEADONOMI)
                        lstComisiones.Add(
                            new ResultadoComisiones
                            {
                                Empleado = item.Empleado,
                                MontoComision = item.Comision,
                                Nombre = item.Nombre,
                                Resultado = "EMPLEADO NO TIENE REGISTRO EN NOMINA",
                                Vendedor = item.Vendedor,
                                Tienda = item.Tienda
                            }
                            );
                }

                var qComisionesNOTEMP = (from sC in dbExactus.MF_SincronizacionComision
                                         join sD in dbExactus.MF_SincronizacionComisionDetalle on sC.Sincronizacion equals sD.Sincronizacion
                                         join ve in dbExactus.VENDEDOR on sD.Vendedor equals ve.VENDEDOR1

                                         where
                                         sC.FechaInicio >= dtFechaIni
                                         && sC.FechaFin <= dtFechaFin
                                         && sC.Nomina == nomina.NUMERO_NOMINA
                                         && sC.CodigoNomina == General.ComisionesNomina
                                         && sC.CodigoConcepto == General.ComisionesConcepto //nomina de ventas}
                                         && sD.Actualizado == false
                                        && sD.Empleado == null
                                         && sD.Comision == 0
                                         select new
                                         {
                                             Vendedor = sD.Vendedor,
                                             Nombre = ve.NOMBRE,
                                             Tienda = sD.Cobrador,
                                             Empleado = sD.Empleado,
                                             Comision = sD.Comision
                                         }
                                  );
                if (qComisionesNOTEMP.Count() > 0)
                {
                    foreach (var item in qComisionesNOTEMP)
                        lstComisiones.Add(
                            new ResultadoComisiones
                            {
                                Empleado = item.Empleado,
                                MontoComision = item.Comision,
                                Nombre = item.Nombre,
                                Resultado = "EMPLEADO NO TIENE REGISTRO EN NOMINA",
                                Vendedor = item.Vendedor,
                                Tienda = item.Tienda
                            }
                            );
                }


                var qComisionesZERO = (from sC in dbExactus.MF_SincronizacionComision
                                         join sD in dbExactus.MF_SincronizacionComisionDetalle on sC.Sincronizacion equals sD.Sincronizacion
                                         join ve in dbExactus.VENDEDOR on sD.Vendedor equals ve.VENDEDOR1

                                         where
                                         sC.FechaInicio >= dtFechaIni
                                         && sC.FechaFin <= dtFechaFin
                                         && sC.Nomina == nomina.NUMERO_NOMINA
                                         && sC.CodigoNomina == General.ComisionesNomina
                                         && sC.CodigoConcepto == General.ComisionesConcepto //nomina de ventas}
                                         && sD.Actualizado == false
                                        && sD.Empleado != null
                                         && sD.Comision == 0
                                         select new
                                         {
                                             Vendedor = sD.Vendedor,
                                             Nombre = ve.NOMBRE,
                                             Tienda = sD.Cobrador,
                                             Empleado = sD.Empleado,
                                             Comision = sD.Comision
                                         }
                                 );
                if (qComisionesZERO.Count() > 0)
                {
                    foreach (var item in qComisionesZERO)
                        lstComisiones.Add(
                            new ResultadoComisiones
                            {
                                Empleado = item.Empleado,
                                MontoComision = item.Comision,
                                Nombre = item.Nombre,
                                Resultado = "COMISION CERO",
                                Vendedor = item.Vendedor,
                                Tienda = item.Tienda
                            }
                            );
                }


            }
            catch (Exception ex)
            {
                log.Error("NO se pudo obtener el resultado de las comisiones "+ex.Message);
            }
            return lstComisiones;
        }
    }
}