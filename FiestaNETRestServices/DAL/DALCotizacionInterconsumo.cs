﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Models;
using MF_Clases;
using System;
using System.Linq;
using System.Transactions;
using static MF_Clases.Clases;

namespace FiestaNETRestServices.DAL
{
    public class DALCotizacionInterconsumo : MFDAL
    {

        public DALCotizacionInterconsumo()
        {

        }

        /// <summary>
        /// Esta función tiene como propósito verificar si la cotización de interconsumo existe.
        /// </summary>
        public Respuesta ExisteCotizacion(Int32 Cotizacion)
        {
            MF_Clases.Clases.Cotizacion mCotizacion;

            if ((from c in dbExactus.MF_Cotizacion where c.COTIZACION == Cotizacion && c.FINANCIERA == 7  select c).Count() == 0)
            {
                return new Respuesta(false, string.Format("La cotización {0} no existe o no pertenece a Interconsumo.", Cotizacion));
            }

            var qCotizacionInterconsumo = ( from    c  in dbExactus.MF_Cotizacion
                            where   c.COTIZACION == Cotizacion 
                                    && c.FINANCIERA == 7
                            select new {CLIENTE = c.CLIENTE 
                                        , GRABADA_EN_LINEA  =c.GRABADA_EN_LINEA 
                                        , FECHA_VENCE  = c.FECHA_VENCE 
                                        , TOTAL_FACTURAR  = c.TOTAL_FACTURAR 
                                        , COBRADOR = c.COBRADOR 
                                        , SOLICITUD  =c.SOLICITUD 
                                        , ENGANCHE = c.ENGANCHE }
                                ).FirstOrDefault();

            mCotizacion = new MF_Clases.Clases.Cotizacion();
            mCotizacion.CLIENTE = qCotizacionInterconsumo.CLIENTE;
            mCotizacion.COBRADOR = qCotizacionInterconsumo.COBRADOR;
            mCotizacion.ENGANCHE = qCotizacionInterconsumo.ENGANCHE;
            mCotizacion.FECHA_VENCE = qCotizacionInterconsumo.FECHA_VENCE;
            mCotizacion.GRABADA_EN_LINEA = qCotizacionInterconsumo.GRABADA_EN_LINEA;
            mCotizacion.SOLICITUD = qCotizacionInterconsumo.SOLICITUD;
            mCotizacion.TOTAL_FACTURAR = qCotizacionInterconsumo.TOTAL_FACTURAR;

            return new Respuesta(true, "", mCotizacion);
        }

        /// <summary>
        /// Esta función tiene como propósito verificar si la cotización es de interconsumo existe.
        /// </summary>
        public Respuesta CotizacionEsDeInterconsumo(Int32 Cotizacion)
        {
            Respuesta mRespuesta = new Respuesta();
            if ((from c in dbExactus.MF_Cotizacion where c.COTIZACION == Cotizacion && c.FINANCIERA == 7 select c).Count()==0)
            {
                return new Respuesta(false, string.Format("La cotización {0} no pertenece a Interconsumo.", Cotizacion));
             }

            return new Respuesta(true, "");
        }

        /// <summary>
        /// Esta función obtiene los dias que puede estar vigente una cotizacion.
        /// </summary>
        public Int32 DiasGraciaCotizacion()
        {
            Int32 mDiasGracia = 0;
            mDiasGracia = Convert.ToInt32((from c in dbExactus.MF_Configura select c).First().diasGraciaCotizacion);
            return mDiasGracia;
        }

        /// <summary>
        /// Esta función tiene como proposito obtener la fecha en que vence la cotizacion.
        /// </summary>
        public DateTime FechaVencimientoCotizacion(DateTime Fecha_Vencimiento)
        {
            return Fecha_Vencimiento.AddDays(DiasGraciaCotizacion());
        }

        /// <summary>
        /// Esta función tiene como propósito verificar si el pedido tiene estado inicial
        /// </summary>
        public Respuesta CotizacionConEstadoInicial(Int32 Cotizacion)
        {
            if ((from c in dbExactus.MF_Cotizacion where c.COTIZACION == Cotizacion  && c.ESTADO_INICIAL != null select c).Count() > 0)
            {
                return new Respuesta(false, string.Format("La cotización {0} aún no tiene un estado inicial, puede ser porque no ha capturado el DPI, la huella y la fotografía del cliente en el portal.", Cotizacion.ToString()));
            }

            return new Respuesta(true, "");
        }

        public Respuesta ValidaCotizacionInterconsumo(Int32 NumeroCotizacion)
        {
            //--------------------------------------------------------------------------
            //Validamos que exista la cotizacion en el sistema
            //--------------------------------------------------------------------------
            Respuesta mRespuesta = ExisteCotizacion(NumeroCotizacion);

            if (!mRespuesta.Exito)
                return mRespuesta;

            MF_Clases.Clases.Cotizacion mCotizacion = (MF_Clases.Clases.Cotizacion)mRespuesta.Objeto;

            //--------------------------------------------------------------------------
            //Validamos que la cotizacion tenga asociada el codigo de algun cliente
            //--------------------------------------------------------------------------
            if (string.IsNullOrEmpty(mCotizacion.CLIENTE) || string.IsNullOrWhiteSpace(mCotizacion.CLIENTE))
            {
                return new Respuesta(false, string.Format("La cotización {0} debe tener cliente asignado.", NumeroCotizacion));
            }

            //--------------------------------------------------------------------------
            //Validamos que la cotizacion haya sido grabada en línea.
            //--------------------------------------------------------------------------
            if (string.IsNullOrEmpty(mCotizacion.GRABADA_EN_LINEA) || mCotizacion.GRABADA_EN_LINEA == "N")
            {
                return new Respuesta(false, string.Format("La cotización {0} no fue grabada en línea.", NumeroCotizacion));
            }

            DateTime mFechaVence = FechaVencimientoCotizacion(mCotizacion.FECHA_VENCE);

            //--------------------------------------------------------------------------
            //Validamos que la cotización aún esté vigente.
            //--------------------------------------------------------------------------
            if (DateTime.Now.Date > mFechaVence)
            {
                return new Respuesta(false, string.Format("Esta cotización ya está vencida y se terminaron los {0} dias de gracia autorizados, no es posible utilizarla.", DiasGraciaCotizacion()));
            }

            //--------------------------------------------------------------------------
            //Validamos que exista una solicitud de crédito asignada.
            //--------------------------------------------------------------------------
            if (string.IsNullOrEmpty(mCotizacion.SOLICITUD.ToString()) && mCotizacion.SOLICITUD > 0)
            {
                return new Respuesta(false, string.Format("La cotización {0} no tiene solicitud de crédito asignada.", NumeroCotizacion));
            }

            //--------------------------------------------------------------------------
            //Validamos que el pedido tenga un estado inicial.
            //--------------------------------------------------------------------------
            mRespuesta = CotizacionConEstadoInicial(NumeroCotizacion);
            if (!mRespuesta.Exito)
                return mRespuesta;

            return new Respuesta(true, "", mCotizacion);
        }

        public Respuesta GrabarSolicitud(SolicitudInterconsumo SolicitudInterconsumo)
        {

            decimal mPrecioTotal = 0;
            decimal? diferencia = 0;
            int mCantidadLineas=0, itemNoMCL = 0;

            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    var qUpdateCotizacion = from c in dbExactus.MF_Cotizacion where c.COTIZACION == Convert.ToInt32(SolicitudInterconsumo.NUMERO_DOCUMENTO) select c;
                    foreach (var item in qUpdateCotizacion)
                    {
                        item.XML_CONSULTA = SolicitudInterconsumo.XML_CONSULTA;
                        item.XML_CONSULTA_RESPUESTA = SolicitudInterconsumo.XML_CONSULTA_RESPUESTA;
                        item.AUTORIZACION_EN_LINEA = "S";
                        item.NOMBRE_AUTORIZACION = SolicitudInterconsumo.USUARIO_AUTORIZO;
                        item.AUTORIZACION_INTERCONSUMO = SolicitudInterconsumo.AUTORIZACION;

                        //---------------------------------------------------------------------------------------------------
                        //Actualizamos el estado inicial, si solo si, es la primera vez que se obtiene un estado
                        //---------------------------------------------------------------------------------------------------
                        if (string.IsNullOrEmpty(item.ESTADO_INICIAL) && SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.APROBADO)
                            item.ESTADO_INICIAL = "A";

                        if (string.IsNullOrEmpty(item.ESTADO_INICIAL) && SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.RECHAZADO)
                            item.ESTADO_INICIAL = "R";

                        if (string.IsNullOrEmpty(item.ESTADO_INICIAL) && SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.DIFERIDO)
                            item.ESTADO_INICIAL = "D";

                        if (string.IsNullOrEmpty(item.ESTADO_INICIAL) && SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.PREAUTORIZADO)
                            item.ESTADO_INICIAL = "P";

                        //---------------------------------------------------------------------------------------------------
                        //Actualizamos el estado final, es el estado que devuelve el servicio de interconsumo.
                        //---------------------------------------------------------------------------------------------------
                        if (SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.APROBADO)
                            item.ESTADO_FINAL = "A";

                        if (SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.RECHAZADO)
                            item.ESTADO_FINAL = "R";

                        if (SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.DIFERIDO)
                            item.ESTADO_FINAL = "D";

                        if (SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.PREAUTORIZADO)
                            item.ESTADO_FINAL = "P";

                        //---------------------------------------------------------------------------------------------------
                        //Actualizamos la fecha de aprobacion_solicitud, si el nuevo estado es aprobado, o rechazado.
                        //---------------------------------------------------------------------------------------------------
                        if ((SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.APROBADO)
                            || ( SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.RECHAZADO))
                                item.FECHA_APROBACION_SOLICITUD = DateTime.Now;

                        //---------------------------------------------------------------------------------------------------
                        //Actualizamos el factor y los respectivos montos
                        //---------------------------------------------------------------------------------------------------
                        item.MONTO_PAGOS1 = Decimal.Round(SolicitudInterconsumo.CUOTA,2);
                        item.MONTO_PAGOS2 = Decimal.Round(SolicitudInterconsumo.ULTIMA_CUOTA,2);
                        item.MONTO = Decimal.Round(((SolicitudInterconsumo.PLAZO - 1) * SolicitudInterconsumo.CUOTA) + SolicitudInterconsumo.ULTIMA_CUOTA,2);
                        item.FACTOR = Decimal.Round((((SolicitudInterconsumo.PLAZO - 1) * SolicitudInterconsumo.CUOTA) + SolicitudInterconsumo.ULTIMA_CUOTA) / (SolicitudInterconsumo.VALOR_SOLICITUD),6);

                        //---------------------------------------------------------------------------------------------------
                        //Recorremos las lineas correspondientes
                        //---------------------------------------------------------------------------------------------------
                        var qUpdateCotizacionLinea = from c in dbExactus.MF_Cotizacion_Linea where c.COTIZACION == Convert.ToInt32(SolicitudInterconsumo.NUMERO_DOCUMENTO) select c;


                        if (qUpdateCotizacionLinea.Count()>0)
                            mCantidadLineas = qUpdateCotizacionLinea.Count();

                        foreach (var itemMCL in qUpdateCotizacionLinea)
                        {
                            itemNoMCL = itemNoMCL + 1;
                            //---------------------------------------------------------------------------------------------------
                            //Realizamos el ajuste necesario para para ajustar los centavos faltantes o sobrantes
                            //respecto a los calculos del factor de Interconsumo
                            //---------------------------------------------------------------------------------------------------
                            if (itemNoMCL == mCantidadLineas)
                            {
                                itemNoMCL = itemNoMCL + 1;

                                itemMCL.PRECIO_UNITARIO = Decimal.Round((itemMCL.PRECIO_UNITARIO_FACTURAR * Decimal.Round((((SolicitudInterconsumo.PLAZO - 1) * SolicitudInterconsumo.CUOTA) + SolicitudInterconsumo.ULTIMA_CUOTA) / (SolicitudInterconsumo.VALOR_SOLICITUD), 6)) ?? 0, 2);
                                itemMCL.PRECIO_TOTAL = Decimal.Round((itemMCL.PRECIO_TOTAL_FACTURAR * Decimal.Round((((SolicitudInterconsumo.PLAZO - 1) * SolicitudInterconsumo.CUOTA) + SolicitudInterconsumo.ULTIMA_CUOTA) / (SolicitudInterconsumo.VALOR_SOLICITUD), 6)) ?? 0, 2);

                                mPrecioTotal = mPrecioTotal + Decimal.Round((itemMCL.PRECIO_TOTAL_FACTURAR * Decimal.Round((((SolicitudInterconsumo.PLAZO - 1) * SolicitudInterconsumo.CUOTA) + SolicitudInterconsumo.ULTIMA_CUOTA) / (SolicitudInterconsumo.VALOR_SOLICITUD), 6)) ?? 0, 2);

                                //---------------------------------------------------------------------------------------------------
                                //Realizamos el ajuste necesario para para ajustar los centavos faltantes o sobrantes
                                //respecto a los calculos del factor de Interconsumo
                                //---------------------------------------------------------------------------------------------------
                                if (itemNoMCL == mCantidadLineas)
                                {
                                    //Si el precio calculado es menor al monto, le sumamos la diferencia.
                                    if (mPrecioTotal < Decimal.Round(((SolicitudInterconsumo.PLAZO - 1) * SolicitudInterconsumo.CUOTA) + SolicitudInterconsumo.ULTIMA_CUOTA, 2))
                                    {
                                        diferencia = Decimal.Round(((SolicitudInterconsumo.PLAZO - 1) * SolicitudInterconsumo.CUOTA) + SolicitudInterconsumo.ULTIMA_CUOTA, 2) - mPrecioTotal;
                                        itemMCL.PRECIO_TOTAL = mPrecioTotal + diferencia;
                                    }

                                    //Si el precio calculado es mayo al monto, le restamos la diferencia.
                                    if (mPrecioTotal > Decimal.Round(((SolicitudInterconsumo.PLAZO - 1) * SolicitudInterconsumo.CUOTA) + SolicitudInterconsumo.ULTIMA_CUOTA, 2))
                                    {
                                        diferencia = mPrecioTotal - Decimal.Round(((SolicitudInterconsumo.PLAZO - 1) * SolicitudInterconsumo.CUOTA) + SolicitudInterconsumo.ULTIMA_CUOTA, 2);
                                        itemMCL.PRECIO_TOTAL = mPrecioTotal - diferencia;
                                    }
                                }
                            }
                        }


                        //---------------------------------------------------------------------------------------------------
                        //Actualizamos los nombres del cliente
                        //---------------------------------------------------------------------------------------------------
                        if (SolicitudInterconsumo.PRIMER_NOMBRE.Trim().Length > 0)
                        {
                            var qUpdateMF_Cliente = from c in dbExactus.MF_Cliente where c.CLIENTE == SolicitudInterconsumo.CLIENTE select c;
                            foreach (var itemMC in qUpdateMF_Cliente)
                            {
                                itemMC.PRIMER_NOMBRE = SolicitudInterconsumo.PRIMER_NOMBRE;
                                itemMC.SEGUNDO_NOMBRE = SolicitudInterconsumo.SEGUNDO_NOMBRE;
                                itemMC.TERCER_NOMBRE = SolicitudInterconsumo.TERCER_NOMBRE;
                                itemMC.PRIMER_APELLIDO = SolicitudInterconsumo.PRIMER_APELLIDO;
                                itemMC.SEGUNDO_APELLIDO = SolicitudInterconsumo.SEGUNDO_APELLIDO;
                                itemMC.APELLIDO_CASADA = SolicitudInterconsumo.APELLIDO_CASADA;
                                itemMC.NOMBRE_FACTURA = SolicitudInterconsumo.NOMBRE;
                            }

                            var qUpdateCliente = from c in dbExactus.CLIENTE where c.CLIENTE1 == SolicitudInterconsumo.CLIENTE select c;
                            foreach (var itemC in qUpdateCliente)
                            {
                                itemC.NOMBRE = SolicitudInterconsumo.NOMBRE;
                            }
                        }
                    }

                    dbExactus.SaveChanges();
                    transactionScope.Complete();
                }
            }
            catch (Exception e)
            {
                log.Error(this.GetType().Name + " " + e.StackTrace);
                Utilitario.EnviarEmailIT(this.GetType().Name, e.StackTrace);
                return new Respuesta(false, "" + e.StackTrace);
            }
            return new Respuesta(true, "");
        }

        public Respuesta GrabarFechaImpresionPagare(Int32 Cotizacion)
        {
            Respuesta mRespuesta = new Respuesta();
            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    var qUpdateCotizacion = from c in dbExactus.MF_Cotizacion where c.COTIZACION == Convert.ToInt32(Cotizacion) select c;
                    foreach (var item in qUpdateCotizacion)
                    {
                        item.FECHA_IMPRESION_PAGARE = DateTime.Now;
                    }
                    dbExactus.SaveChanges();
                    transactionScope.Complete();
                }
            }
            catch (Exception e)
            {
                return new Respuesta(false, "" + e.StackTrace);
            }
            return mRespuesta;
        }

        public Respuesta GrabarXML(SolicitudInterconsumo SolicitudInterconsumo)
        {
            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    var qUpdateCotizacion = from c in dbExactus.MF_Cotizacion where c.COTIZACION == Convert.ToInt32(SolicitudInterconsumo.NUMERO_DOCUMENTO) select c;
                    foreach (var item in qUpdateCotizacion)
                    {
                        item.XML_CONSULTA = SolicitudInterconsumo.XML_CONSULTA;
                        item.XML_CONSULTA_RESPUESTA = SolicitudInterconsumo.XML_CONSULTA_RESPUESTA;
                    }
                    dbExactus.SaveChanges();
                    transactionScope.Complete();
                }
            }
            catch (Exception e)
            {
                return new Respuesta(false, "" + e.StackTrace);
            }
            return new Respuesta(true, "");
        }

    }
}