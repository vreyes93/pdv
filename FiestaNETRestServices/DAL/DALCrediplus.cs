﻿using FiestaNETRestServices.Content.Abstract;
using System.Linq;
using MF_Clases.Comun;
using System.Collections.Generic;
using MF_Clases;

namespace FiestaNETRestServices.DAL
{
    public class DALCrediplus : MFDAL
    {
        public bool ValidarFacturaEnCrediplus(string numeroFactura)
        {
            return dbCrediplus.FACTURA.Count(x => x.RUBRO3 == numeroFactura) > 0;
        }

        public List<ArticuloCrediplus> ObtenerProductosCrediplus(string numeroFactura)
        {
            List<ArticuloCrediplus> data = new List<ArticuloCrediplus>();
            IQueryable<ExistenciaProductos> existencia;
            List<string> bodegas = new List<string>(); //bodegas.Add("CP01"); bodegas.Add("CP00");

            decimal IVA = dbExactus.IMPUESTO.Where(x => x.IMPUESTO1 == "IVA").FirstOrDefault().IMPUESTO11 / 100;
            decimal porcentajeCP = decimal.Parse(dbExactus.MF_Catalogo.Where(x => x.CODIGO_TABLA == 62 && x.CODIGO_CAT == "MargenPorcentajeCP").FirstOrDefault().NOMBRE_CAT);
            decimal porcentajeMF = decimal.Parse(dbExactus.MF_Catalogo.Where(x => x.CODIGO_TABLA == 62 && x.CODIGO_CAT == "MargenPorcentajeMF").FirstOrDefault().NOMBRE_CAT);

            var vBodegas = dbExactus.MF_Catalogo.Where(x => x.CODIGO_TABLA == 70).Select(x => new { x.NOMBRE_CAT});

            if (vBodegas.Count() > 0)
            {
                foreach (var item in vBodegas)
                {
                    bodegas.Add(item.NOMBRE_CAT);
                }
            }

            data = dbExactus.FACTURA_LINEA
                .Join(dbExactus.ARTICULO_ESPE.Where(x => x.ATRIBUTO == "ORIGEN" && x.VALOR == "CREDIPLUS"), fl => fl.ARTICULO, ae => ae.ARTICULO, (fl, ae) => new { fl, ae })
                .Where(x => x.fl.FACTURA == numeroFactura)
                .Select(x => new ArticuloCrediplus()
                {
                    Cantidad = (int)x.fl.CANTIDAD,
                    CodigoMF = x.fl.ARTICULO,
                    CodigoCP = x.fl.ARTICULO.Substring(1, x.fl.ARTICULO.Length),
                    Descripcion = x.fl.DESCRIPCION,
                    PrecioMF = x.fl.PRECIO_UNITARIO + x.fl.TOTAL_IMPUESTO1,
                    PrecioSinIvaMF = x.fl.PRECIO_UNITARIO,
                    IvaMF = x.fl.TOTAL_IMPUESTO1,
                    MargenMF = x.fl.PRECIO_UNITARIO * porcentajeMF,
                    PrecioCP = (x.fl.PRECIO_UNITARIO * porcentajeCP) * (1 + IVA),
                    PrecioSinIvaCP = x.fl.PRECIO_UNITARIO * porcentajeCP
                }).ToList();

            existencia = dbCrediplus.EXISTENCIA_BODEGA
                            .Where(x => bodegas.Contains(x.BODEGA))
                            .GroupBy(g => g.ARTICULO)
                            .Select(x => new ExistenciaProductos() {
                                CantidadDisponible = x.Sum(g=>g.CANT_DISPONIBLE),
                                CodigoArticulo = x.Key
                            });

            data = data.GroupJoin(dbCrediplus.ARTICULO, d => d.CodigoCP, a => a.ARTICULO1, (d, a) => new { d, a = a.DefaultIfEmpty() })
                .GroupJoin(existencia, d1 => d1.d.CodigoCP, e => e.CodigoArticulo, (d1, e) => new { d1, e = e.DefaultIfEmpty() })
                .Select(x => new ArticuloCrediplus()
                {
                    Cantidad = x.d1.d.Cantidad,
                    CodigoMF = x.d1.d.CodigoMF,
                    CodigoCP = x.d1.d.CodigoCP,
                    Descripcion = x.d1.d.Descripcion,
                    PrecioMF = x.d1.d.PrecioMF,
                    PrecioSinIvaMF = x.d1.d.PrecioSinIvaMF,
                    IvaMF = x.d1.d.IvaMF,
                    MargenMF = x.d1.d.MargenMF,
                    PrecioCP = x.d1.d.PrecioCP,
                    PrecioSinIvaCP = x.d1.d.PrecioSinIvaCP,
                    Activo = x.d1.a.FirstOrDefault() == null ? false : x.d1.a.FirstOrDefault().ACTIVO == "S",
                    Existe = x.d1.a.FirstOrDefault() != null,
                    HayExistencia = x.e.FirstOrDefault() == null ? false : x.e.FirstOrDefault().CantidadDisponible > 0
                }).ToList();

            return data;
        }

        public List<string> ObtenerDestinatariosFacturacionCrediplus()
        {
            List<string> result = new List<string>(),
                excludeCatalogo = new List<string>();

            excludeCatalogo.Add("MargenPorcentajeCP");
            excludeCatalogo.Add("MargenPorcentajeMF");

            if (General.Ambiente == "PRO")
                dbExactus.MF_Catalogo.Where(x => x.CODIGO_TABLA == 62 && !excludeCatalogo.Contains(x.CODIGO_CAT))
                    .ToList().ForEach(x =>
                    {
                        result.Add(x.NOMBRE_CAT);
                    });
            else
                dbExactus.MF_Catalogo.Where(x => x.CODIGO_TABLA == 63)
                    .ToList().ForEach(x =>
                    {
                        result.Add(x.NOMBRE_CAT);
                    });

            return result;
        }

        public List<string> DestinatariosInformatica()
        {
            List<string> result = new List<string>();

            dbExactus.MF_Catalogo.Where(x => x.CODIGO_TABLA == 63)
                    .ToList().ForEach(x =>
                    {
                        result.Add(x.NOMBRE_CAT);
                    });

            return result;
        }

        public bool ValidarVisibilidadTienda(string numeroFactura)
        {
            var query = dbExactus.FACTURA
                .Join(dbExactus.MF_CobradorEspecificaciones.Where(x => x.ATRIBUTO == "VISIBILIDAD"), f => f.COBRADOR, ce => ce.COBRADOR, (f, ce) => new { f, ce })
                .Where(x => x.f.FACTURA1 == numeroFactura);

            if (query.Count() == 0)
                return false;

            string valor = query.Select(x => x.ce.VALOR).FirstOrDefault();

            return valor == "OUTLET" || valor == "TODO";
        }
        /// <summary>
        /// obtener la tienda de facturación de
        /// artículos en consignación
        /// </summary>
        /// <param name="numeroFactura"></param>
        /// <returns></returns>
        public string TiendaFacturacionOutlet(string numeroFactura)
        {
            var query = dbExactus.FACTURA
               .Where(x => x.FACTURA1 == numeroFactura);
            return query.Select(x => x.COBRADOR).FirstOrDefault();
        }
    }
}