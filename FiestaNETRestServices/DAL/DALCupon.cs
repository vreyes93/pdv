﻿using FiestaNETRestServices.Content.Abstract;
using MF_Clases;
using MF_Clases.Comun;
using MF_Clases.Restful;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FiestaNETRestServices.DAL
{
    public class DALCupon : MFDAL
    {
        private new static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Respuesta ObtenerCuponCrediplus(string Pedido)
        {
            try
            {
                var qCupon = (from p in dbExactus.MF_Pedido join cli in dbExactus.MF_Cliente on p.CLIENTE equals cli.CLIENTE where p.PEDIDO.Equals(Pedido)
                              select new dtoCuponCrediplus
                              {
                                  NombreCliente = cli.PRIMER_NOMBRE.Trim().ToUpper()+" "+cli.SEGUNDO_NOMBRE.Trim().ToUpper()+" "+cli.PRIMER_APELLIDO.Trim().ToUpper()+" "+cli.SEGUNDO_APELLIDO.Trim().ToUpper(),
                                   CreditoCrediplus=p.NUMERO_CREDITO
                              });
                if (qCupon.Count() > 0)
                {
                    return new Respuesta { Objeto = qCupon.First<dtoCuponCrediplus>(), Exito = true };
                }
                else
                    return new Respuesta { Exito = false };
            }
            catch (Exception ex)
            {
                log.Error("Error al obtener información para la generación del cupón de Crediplus pedido:"+Pedido);
                return new Respuesta { Exito = false, Mensaje=ex.Message };
            }
        }

        public ExchangeInfoResult checkStatus(string pCoupon, string advertisingCompany = "OKY")
        {
            ExchangeInfoResult result = new ExchangeInfoResult();

            string apiUrl = string.Empty;
            string apiUser = string.Empty;
            string apiCredentials = string.Empty;
            string latamBranch = string.Empty;
            string CatalogCode = string.Empty;
            
                var Catalog = dbExactus.MF_Tabla_Catalogo.Where(x => x.NOMBRE_TABLA == advertisingCompany).FirstOrDefault().CODIGO_TABLA;
                var CouponExchangeConfiguration = dbExactus.MF_Catalogo.Where(x => x.CODIGO_TABLA == Catalog);
            string CODIGO_CAT_API = $"UrlApi{General.Ambiente}";
                if (CouponExchangeConfiguration.Count() > 0)
                {
                    apiUrl = CouponExchangeConfiguration.Where(x => x.CODIGO_CAT == CODIGO_CAT_API).FirstOrDefault().NOMBRE_CAT;
                    apiUser = CouponExchangeConfiguration.Where(x => x.CODIGO_CAT == "LatamUser").FirstOrDefault().NOMBRE_CAT;
                    apiCredentials = CouponExchangeConfiguration.Where(x => x.CODIGO_CAT == "TokenUser").FirstOrDefault().NOMBRE_CAT;
                    latamBranch = CouponExchangeConfiguration.Where(x => x.CODIGO_CAT == "latamBranch").FirstOrDefault().NOMBRE_CAT;
                }
                else
                    throw new Exception($"No se encontró la configuración para {advertisingCompany}");

            

            var client = new RestClient(string.Format("{0}", apiUrl));
            var request = new RestRequest("", Method.POST);
            ExchangeInfoResult infoResp = new ExchangeInfoResult();
            request.ReadWriteTimeout = 36000000;
            ExchangeInfoRequest infoRequest = new ExchangeInfoRequest
            {
                
                data ={ new DataExchange
                {
                    LatamUser = apiUser,
                    TokenUser = apiCredentials,
                    Code = pCoupon
                } },
                method = "ExchangeStatus"
            };
            //request.AddHeader("Content-type", "application/json");
            request.AddJsonBody(infoRequest); // AddJsonBody serializes the object automatically
            string json = JsonConvert.SerializeObject(infoRequest);
            log.Info("POST ATID: " + json);
             var response = client.Post(request);
            Api api = new Api(apiUrl);
            //api.Process(Method.POST,null, json);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                //var resp = JsonConvert.DeserializeObject(api.Process(Method.POST, null, json));
                string msg = string.Empty;
                CheckStatusResult.statusResultOptions.TryGetValue(int.Parse(infoResp.MsgCode), out msg);
                infoResp.Msg = msg;

            }



            return infoResp;
        }
        public ExchangeInfoResult changeStatus(string pCoupon, string exRefence, string invoiceAmount, string invoiceNumber, string advertisingCompany = "OKY")
        {
            ExchangeInfoResult result = new ExchangeInfoResult();

            string apiUrl = string.Empty;
            string apiUser = string.Empty;
            string apiCredentials = string.Empty;
            string latamBranch = string.Empty;
            string CatalogCode = string.Empty;
            
                var Catalog = dbExactus.MF_Tabla_Catalogo.Where(x => x.NOMBRE_TABLA == advertisingCompany).FirstOrDefault().CODIGO_TABLA;
                var CouponExchangeConfiguration = dbExactus.MF_Catalogo.Where(x => x.CODIGO_TABLA == Catalog);
                if (CouponExchangeConfiguration.Count() > 0)
                {
                    apiUrl = CouponExchangeConfiguration.Where(x => x.CODIGO_CAT == $"UrlApi{General.Ambiente}").FirstOrDefault().NOMBRE_CAT;
                    apiUrl = CouponExchangeConfiguration.Where(x => x.CODIGO_CAT == "LatamUser").FirstOrDefault().NOMBRE_CAT;
                    apiCredentials = CouponExchangeConfiguration.Where(x => x.CODIGO_CAT == "TokenUser").FirstOrDefault().NOMBRE_CAT;
                    latamBranch = CouponExchangeConfiguration.Where(x => x.CODIGO_CAT == "latamBranch").FirstOrDefault().NOMBRE_CAT;
                }
                else
                    throw new Exception($"No se encontró la configuración para {advertisingCompany}");

            

            var client = new RestClient(string.Format("{0}", apiUrl));
            var request = new RestRequest("", Method.POST);
            ExchangeInfoResult infoResp = new ExchangeInfoResult();
            request.ReadWriteTimeout = 36000000;
            ExchangeInfoRequest infoRequest = new ExchangeInfoRequest
            {
                method = "Exchange",
                data = {new DataExchange
                            {
                                LatamUser = apiUser,
                                TokenUser = apiCredentials,
                                Code = pCoupon,
                                //exchangeReference = exRefence,
                                //invoiceAmount = invoiceAmount,
                                //invoiceNumber = invoiceNumber,
                                //latamBranch = latamBranch

                            }
                }
            };
            request.AddHeader("Content-type", "application/json");
            request.AddJsonBody(infoRequest); // AddJsonBody serializes the object automatically

            var response = client.Post(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                infoResp = JsonConvert.DeserializeObject<ExchangeInfoResult>(response.Content);

            }

            return infoResp;
        }
    }
    
}