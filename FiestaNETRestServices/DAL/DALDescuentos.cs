﻿using FiestaNETRestServices.Content.Abstract;
using MF.Comun.Dto.Facturacion;
using MF_Clases.Comun;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;

namespace FiestaNETRestServices.DAL
{
    public class DALDescuentos : MFDAL
    {
        public MF_Clases.GuateFacturas.FEL.Doc  ValidarSumatoriasFel(string pPedido,decimal DescuentoVale)
        {
            MF_Clases.GuateFacturas.FEL.Doc mFEL = new MF_Clases.GuateFacturas.FEL.Doc();
            decimal mUnidades = 0;
            decimal mMercaderia = 0;
            decimal mImpuesto = 0;
            decimal mFacturar = 0;
            decimal mDescuentoTotal = 0;
            decimal mDescuentoAcumulado = 0;
            decimal mIvaAcumulado = 0;
            decimal mNetoAcumulado = 0;
            decimal mTotalAcumulado = 0;
            decimal mBrutoAcumulado = 0;
            decimal iva= Utils.Utilitarios.ObtenerIva();

            var qPedido = dbExactus.PEDIDO.Join(dbExactus.MF_Cliente, p => p.CLIENTE, c => c.CLIENTE, (p, c) => new { p, c }).Where(pedido => pedido.p.PEDIDO1 == pPedido).Select(p => new
                {
                                CLIENTE = p.p.CLIENTE,
                                TOTAL_UNIDADES = p.p.TOTAL_UNIDADES,
                                TOTAL_MERCADERIA = p.p.TOTAL_MERCADERIA,
                                TOTAL_IMPUESTO1 = p.p.TOTAL_IMPUESTO1,
                                TOTAL_A_FACTURAR = p.p.TOTAL_A_FACTURAR,
                                FECHA_PEDIDO = p.p.FECHA_PEDIDO,
                                OBSERVACIONES = p.p.OBSERVACIONES,
                                SUBTIPO_DOC_CXC = p.p.SUBTIPO_DOC_CXC,
                                PRIMER_NOMBRE = p.c.PRIMER_NOMBRE,
                                SEGUNDO_NOMBRE = p.c.SEGUNDO_NOMBRE,
                                PRIMER_APELLIDO = p.c.PRIMER_APELLIDO,
                                SEGUNDO_APELLIDO = p.c.SEGUNDO_APELLIDO,
                                DESCUENTO = p.p.MONTO_DESCUENTO1
                                
            }).ToList();
            var qPedidoLinea = dbExactus.PEDIDO_LINEA.Where(x => x.PEDIDO == pPedido).ToList();
            int vFinanciera = (int)dbExactus.MF_Pedido.Where(x => x.PEDIDO == pPedido).FirstOrDefault().FINANCIERA;
           

                #region "ENCABEZADO"

                mMercaderia = qPedido.FirstOrDefault().TOTAL_MERCADERIA;
                mImpuesto = qPedido.FirstOrDefault().TOTAL_IMPUESTO1;
                mFacturar = qPedido.FirstOrDefault().TOTAL_A_FACTURAR;
                mUnidades = qPedido.FirstOrDefault().TOTAL_UNIDADES;
                mDescuentoTotal = mDescuentoTotal==0? qPedido.FirstOrDefault().DESCUENTO: mDescuentoTotal;

                mFEL.Bruto = mFacturar;
                mFEL.Descuento = mDescuentoTotal;
                mFEL.Exento = 0;
                mFEL.Otros = 0;
                mFEL.Neto = mMercaderia ;
                mFEL.Isr = 0;
                mFEL.Iva = mImpuesto;
                mFEL.Total = mFacturar - mDescuentoTotal;


                #endregion

                #region "Detalle"
                qPedidoLinea.ForEach(item =>

               {
                   decimal mMontoDescuento = Math.Round(item.MONTO_DESCUENTO, 2, MidpointRounding.AwayFromZero);
                   decimal mBaseImpuesto1 = Math.Round(((item.PRECIO_UNITARIO * item.CANTIDAD_PEDIDA * iva) - mMontoDescuento) / iva, 2, MidpointRounding.AwayFromZero);
                   decimal mPorcDescuento = item.PORC_DESCUENTO;
                   decimal mTotalImpuesto1 = Math.Round((((item.PRECIO_UNITARIO) * item.CANTIDAD_PEDIDA * iva - mMontoDescuento)), 2, MidpointRounding.AwayFromZero) - Math.Round((((item.PRECIO_UNITARIO) * item.CANTIDAD_PEDIDA * iva) - mMontoDescuento) / iva, 2, MidpointRounding.AwayFromZero);
                   decimal PrecioBruto = ((Math.Round((((item.PRECIO_UNITARIO) * item.CANTIDAD_PEDIDA * iva) - mMontoDescuento) / iva, 2, MidpointRounding.AwayFromZero) + mTotalImpuesto1) + mMontoDescuento);
                   mMontoDescuento = mPorcDescuento < 100 ? mMontoDescuento : PrecioBruto;
                   mTotalImpuesto1 = Math.Round((((item.PRECIO_UNITARIO) * item.CANTIDAD_PEDIDA * iva - mMontoDescuento)), 2, MidpointRounding.AwayFromZero) - Math.Round((((item.PRECIO_UNITARIO) * item.CANTIDAD_PEDIDA * iva) - mMontoDescuento) / iva, 2, MidpointRounding.AwayFromZero);
                   decimal mPrecioTotal = mBaseImpuesto1 + mTotalImpuesto1;




                   MF_Clases.GuateFacturas.FEL.DetalleDoc mDetalle = new MF_Clases.GuateFacturas.FEL.DetalleDoc();
                   mDetalle.Producto = item.ARTICULO;
                   mDetalle.Descripcion = item.DESCRIPCION.Replace("** VALE POR Q200 DESC. ** ", "");
                   mDetalle.Cantidad = item.CANTIDAD_PEDIDA;
                   mDetalle.Precio = Math.Round(PrecioBruto/item.CANTIDAD_PEDIDA,2,MidpointRounding.AwayFromZero);
                   mDetalle.PorcDesc = item.PORC_DESCUENTO;
                   mDetalle.ImpBruto = PrecioBruto;
                   mDetalle.ImpDescuento = mMontoDescuento;
                   mDetalle.ImpExento = 0;
                   mDetalle.ImpOtros = 0;
                   mDetalle.ImpNeto = mBaseImpuesto1;
                   mDetalle.ImpIsr = 0;
                   mDetalle.ImpIva = mTotalImpuesto1;
                   mDetalle.ImpTotal = mPrecioTotal;


                   // 1) diferencias MONTO_DESCUENTOS Y % DESCUENTO
                   if (mDetalle.ImpDescuento > 0 )
                   {
                       mDetalle = ValidarDescuentos(mDetalle, iva, vFinanciera, 2);
                       mDetalle = ValidarPrecioYCantidad(mDetalle);

                       //actualizando montos de este detalle
                       mMontoDescuento = mDetalle.ImpDescuento;
                       mTotalImpuesto1 = mDetalle.ImpIva;
                       mBaseImpuesto1 = mDetalle.ImpNeto;
                       mPrecioTotal = mDetalle.ImpTotal;
                       PrecioBruto = mDetalle.ImpBruto;
                   }
                   mFEL.Detalle.Add(mDetalle);

                   
                       

                   mDescuentoAcumulado += mMontoDescuento;
                   mIvaAcumulado = mIvaAcumulado + mTotalImpuesto1;
                   mNetoAcumulado = mNetoAcumulado + mBaseImpuesto1;
                   mTotalAcumulado = mTotalAcumulado + mPrecioTotal;
                   mBrutoAcumulado = mBrutoAcumulado + PrecioBruto;
               });
            #endregion
           
                #region "diferencias montos"



                //2) Diferencia entre encabezados y detalle
                decimal mDiferencia = mFacturar - mTotalAcumulado - mDescuentoAcumulado;

                if (mDiferencia != 0)
                {

                    bool valido = false;
                    int i = mFEL.Detalle.Count() - 1;
                    while (!valido && i >= 0)
                    {
                        log.Debug("count" + mFEL.Detalle.Count(x => x.Producto == mFEL.Detalle[i].Producto) + " articulo: " + mFEL.Detalle[i].Producto);

                        log.Debug(" ImpTotal " + mFEL.Detalle[i].ImpTotal + " i:" + i);
                    if (mFEL.Detalle[i].ImpTotal > 0 && mTotalAcumulado != mFEL.Total && mFEL.Detalle[i].Cantidad==1)
                        {
                            log.Debug("articulo" + mFEL.Detalle[i].Producto + " ImpTotal " + mFEL.Detalle[i].ImpTotal + " i:" + i);

                            mFEL.Detalle[i].ImpBruto = mFEL.Detalle[i].ImpBruto + mDiferencia;
                            mFEL.Detalle[i].ImpNeto = Math.Round((mFEL.Detalle[i].ImpBruto - mFEL.Detalle[i].ImpDescuento) / iva, 2, MidpointRounding.AwayFromZero);
                            mFEL.Detalle[i].ImpIva = mFEL.Descuento > 0? Math.Round(mFEL.Detalle[i].ImpNeto * (iva - 1), 2, MidpointRounding.AwayFromZero) : mFEL.Detalle[i].ImpBruto - mFEL.Detalle[i].ImpNeto;//Math.Round(mFEL.Detalle[i].ImpNeto*(iva-1),2,MidpointRounding.AwayFromZero);
                            mFEL.Detalle[i].Precio = Math.Round(mFEL.Detalle[i].ImpBruto/mFEL.Detalle[i].Cantidad, 2, MidpointRounding.AwayFromZero);
                            mFEL.Detalle[i].ImpTotal = mFEL.Detalle[i].ImpTotal + mDiferencia;
                        valido = true;
                        

                    }
                        else if (mFEL.Descuento != mDescuentoAcumulado && mFEL.Detalle[i].ImpDescuento > 0 && mFEL.Detalle.Count(x => x.Producto == mFEL.Detalle[i].Producto) == 1)
                        {
                            log.Debug("articulo" + mFEL.Detalle[i].Producto + " mFEL.Descuento " + mFEL.Descuento + " i:" + i);
                            mFEL.Detalle[i].ImpDescuento = mFEL.Detalle[i].ImpDescuento + mDiferencia;
                            mFEL.Detalle[i].ImpBruto = mFEL.Detalle[i].ImpBruto + mDiferencia;
                            mFEL.Detalle[i].Precio = Math.Round(mFEL.Detalle[i].ImpBruto / mFEL.Detalle[i].Cantidad, 2, MidpointRounding.AwayFromZero);
                            mFEL.Detalle[i].ImpNeto = Math.Round((mFEL.Detalle[i].ImpBruto - mFEL.Detalle[i].ImpDescuento) / iva, 2, MidpointRounding.AwayFromZero);
                            mDescuentoAcumulado += mDiferencia;
                        valido = true;
                       
                    }
                        
                        i--;
                    }
                }

                log.Debug("*-*-*mDiferencia:*-*- " + mDiferencia);
                log.Debug("mFacturar " + mFacturar);
                log.Debug("mFEL.Total " + mFEL.Total);
                log.Debug("mTotalAcumulado " + mTotalAcumulado);
                log.Debug("mFEL.Descuento " + mFEL.Descuento);
                log.Debug("mDescuentoAcumulado " + mDescuentoAcumulado);
                log.Debug("mFEL.Bruto " + mFEL.Bruto);
                log.Debug("mBrutoAcumulado " + mBrutoAcumulado);

                log.Debug("mFEL.iva " + mFEL.Iva);
                log.Debug("mFel.neto " + mFEL.Neto);
                log.Debug("mNetoAcumulado " + mNetoAcumulado);
                log.Debug("mDescuentoAcumulado " + mDescuentoAcumulado);

            if (mDescuentoAcumulado != mFEL.Descuento || (mDiferencia==0 && mFEL.Neto != mNetoAcumulado))
            {
                if (mFEL.Neto != mNetoAcumulado)
                    mFEL.Neto = mNetoAcumulado;
                if (mFEL.Descuento != mDescuentoAcumulado)
                    mFEL.Descuento = mDescuentoAcumulado;
                if (mFEL.Total != mTotalAcumulado)
                       mFEL.Total = mTotalAcumulado;
                mFEL.Iva = Math.Round(mFEL.Detalle.Sum(x => x.ImpIva), 2, MidpointRounding.AwayFromZero);
                mFEL.Total = Math.Round(mFEL.Detalle.Sum(x => x.ImpTotal), 2, MidpointRounding.AwayFromZero);

            }
            //if (mDiferencia > 0)
            //{
            //    if (mFEL.Iva != mIvaAcumulado)
            //        mFEL.Iva = mIvaAcumulado;
            //    if (mFEL.Neto != mNetoAcumulado)
            //        mFEL.Neto = mNetoAcumulado;
            //    if (mFEL.Total != mTotalAcumulado)
            //        mFEL.Total = mTotalAcumulado;
            //    if (mFEL.Bruto != mBrutoAcumulado && mBrutoAcumulado> mFEL.Bruto)
            //        mFEL.Bruto = mBrutoAcumulado;
            //} 

           

                #endregion

                string json = JsonConvert.SerializeObject(mFEL);
            log.Debug("PEDIDO PARA FEL " + json);

            return mFEL;
        }

        public MF_Clases.GuateFacturas.FEL.Doc ValidarSumatoriasFel(MF.Comun.Dto.TiendaVirtual.Pedido pPedido)
        {
            MF_Clases.GuateFacturas.FEL.Doc mFEL = new MF_Clases.GuateFacturas.FEL.Doc();
            decimal mUnidades = 0;
            decimal mMercaderia = 0;
            decimal mImpuesto = 0;
            decimal mFacturar = 0;
            decimal mDescuentoTotal = 0;
            decimal mDescuentoAcumulado = 0;
            decimal mIvaAcumulado = 0;
            decimal mNetoAcumulado = 0;
            decimal mTotalAcumulado = 0;
            decimal mBrutoAcumulado = 0;
            decimal iva = Utils.Utilitarios.ObtenerIva();
            int financiera = 1; //se refiere unicamente a pagos al contado

            
            #region "ENCABEZADO"
            
            mDescuentoTotal = pPedido.articulos.Sum(x => x.Descuento);
            mMercaderia = Math.Round((pPedido.TotalPedido-mDescuentoTotal)/iva,2,MidpointRounding.AwayFromZero);
            mImpuesto = Math.Round(mMercaderia*(iva-1),2,MidpointRounding.AwayFromZero);
            mFacturar = pPedido.TotalPedido;
            mUnidades = pPedido.TotalItems;
            

            mFEL.Bruto = mFacturar;
            mFEL.Descuento = mDescuentoTotal;
            mFEL.Exento = 0;
            mFEL.Otros = 0;
            mFEL.Neto = mMercaderia;
            mFEL.Isr = 0;
            mFEL.Iva = mImpuesto;
            mFEL.Total = mFacturar - mDescuentoTotal;


            #endregion

            #region "Detalle"
            pPedido.articulos.ForEach(a =>
            {
                decimal mMontoDescuento = Math.Round(a.Descuento, 2, MidpointRounding.AwayFromZero);
                decimal mBaseImpuesto1 = Math.Round(((a.Precio * a.cantidad) - mMontoDescuento) / iva, 2, MidpointRounding.AwayFromZero);
                decimal mPorcDescuento = Math.Round((a.Precio > 0 && a.cantidad > 0) ? (a.Descuento / (a.Precio * a.cantidad))*100 :0,6,MidpointRounding.AwayFromZero);
                decimal mTotalImpuesto1 = Math.Round((((a.Precio) * a.cantidad - mMontoDescuento)), 2, MidpointRounding.AwayFromZero) - Math.Round((((a.Precio) * a.cantidad) - mMontoDescuento) / iva, 2, MidpointRounding.AwayFromZero);
                decimal PrecioBruto = ((Math.Round((((a.Precio) * a.cantidad) - mMontoDescuento) / iva, 2, MidpointRounding.AwayFromZero) + mTotalImpuesto1) + mMontoDescuento);
                mMontoDescuento = mPorcDescuento < 100 ? mMontoDescuento : PrecioBruto;
                //mTotalImpuesto1 = Math.Round((((item.PRECIO_UNITARIO) * item.CANTIDAD_PEDIDA * iva - mMontoDescuento)), 2, MidpointRounding.AwayFromZero) - Math.Round((((item.PRECIO_UNITARIO) * item.CANTIDAD_PEDIDA * iva) - mMontoDescuento) / iva, 2, MidpointRounding.AwayFromZero);
                decimal mPrecioTotal = mBaseImpuesto1 + mTotalImpuesto1;




                MF_Clases.GuateFacturas.FEL.DetalleDoc mDetalle = new MF_Clases.GuateFacturas.FEL.DetalleDoc();
                mDetalle.Linea = a.Linea;
                mDetalle.Producto = a.CodigoArticulo;
                mDetalle.Descripcion = a.Descripcion;
                mDetalle.Cantidad = a.cantidad;
                mDetalle.Precio = Math.Round(PrecioBruto / a.cantidad, 2, MidpointRounding.AwayFromZero);
                mDetalle.PorcDesc = mPorcDescuento;
                mDetalle.ImpBruto = PrecioBruto;
                mDetalle.ImpDescuento = mMontoDescuento;
                mDetalle.ImpExento = 0;
                mDetalle.ImpOtros = 0;
                mDetalle.ImpNeto = mBaseImpuesto1;
                mDetalle.ImpIsr = 0;
                mDetalle.ImpIva = mTotalImpuesto1;
                mDetalle.ImpTotal = mPrecioTotal;


                //// 1) diferencias MONTO_DESCUENTOS Y % DESCUENTO
                if (mDetalle.ImpDescuento > 0)
                {
                    mDetalle = ValidarDescuentos(mDetalle, iva, 2, financiera);

                    //actualizando montos de este detalle
                    mMontoDescuento = mDetalle.ImpDescuento;
                    mTotalImpuesto1 = mDetalle.ImpIva;
                    mBaseImpuesto1 = mDetalle.ImpNeto;
                    mPrecioTotal = mDetalle.ImpTotal;
                    PrecioBruto = mDetalle.ImpBruto;
                }
                mFEL.Detalle.Add(mDetalle);

                mDescuentoAcumulado += mMontoDescuento;
                mIvaAcumulado = mIvaAcumulado + mTotalImpuesto1;
                mNetoAcumulado = mNetoAcumulado + mBaseImpuesto1;
                mTotalAcumulado = mTotalAcumulado + mPrecioTotal;
                mBrutoAcumulado = mBrutoAcumulado + PrecioBruto;
            }
            ); ;//hasta aqui
            #endregion
            #region "diferencias montos"



            //2) Diferencia entre encabezados y detalle
            decimal mDiferencia = mFacturar - mTotalAcumulado - mDescuentoAcumulado;

            if (mDiferencia != 0)
            {

                bool valido = false;
                int i = mFEL.Detalle.Count() - 1;
                while (!valido && i >= 0)
                {
                    log.Debug("count" + mFEL.Detalle.Count(x => x.Producto == mFEL.Detalle[i].Producto) + " articulo: " + mFEL.Detalle[i].Producto);

                    log.Debug(" ImpTotal " + mFEL.Detalle[i].ImpTotal + " i:" + i);
                    if (mFEL.Detalle[i].ImpTotal > 0 && mTotalAcumulado != mFEL.Total)
                    {
                        log.Debug("articulo" + mFEL.Detalle[i].Producto + " ImpTotal " + mFEL.Detalle[i].ImpTotal + " i:" + i);

                        mFEL.Detalle[i].ImpBruto = mFEL.Detalle[i].ImpBruto + mDiferencia;
                        mFEL.Detalle[i].ImpNeto = Math.Round((mFEL.Detalle[i].ImpBruto - mFEL.Detalle[i].ImpDescuento) / iva, 2, MidpointRounding.AwayFromZero);
                        mFEL.Detalle[i].ImpIva = mFEL.Descuento > 0 ? Math.Round(mFEL.Detalle[i].ImpNeto * (iva - 1), 2, MidpointRounding.AwayFromZero) : mFEL.Detalle[i].ImpBruto - mFEL.Detalle[i].ImpNeto;//Math.Round(mFEL.Detalle[i].ImpNeto*(iva-1),2,MidpointRounding.AwayFromZero);
                        mFEL.Detalle[i].Precio = Math.Round(mFEL.Detalle[i].ImpBruto / mFEL.Detalle[i].Cantidad, 2, MidpointRounding.AwayFromZero);
                        mFEL.Detalle[i].ImpTotal = mFEL.Detalle[i].ImpTotal + mDiferencia;
                        valido = true;


                    }
                    else if (mFEL.Descuento != mDescuentoAcumulado && mFEL.Detalle[i].ImpDescuento > 0 && mFEL.Detalle.Count(x => x.Producto == mFEL.Detalle[i].Producto) == 1)
                    {
                        log.Debug("articulo" + mFEL.Detalle[i].Producto + " mFEL.Descuento " + mFEL.Descuento + " i:" + i);
                        mFEL.Detalle[i].ImpDescuento = mFEL.Detalle[i].ImpDescuento + mDiferencia;
                        mFEL.Detalle[i].ImpBruto = mFEL.Detalle[i].ImpBruto + mDiferencia;
                        mFEL.Detalle[i].Precio = Math.Round(mFEL.Detalle[i].ImpBruto / mFEL.Detalle[i].Cantidad, 2, MidpointRounding.AwayFromZero);
                        mFEL.Detalle[i].ImpNeto = Math.Round((mFEL.Detalle[i].ImpBruto - mFEL.Detalle[i].ImpDescuento) / iva, 2, MidpointRounding.AwayFromZero);
                        mDescuentoAcumulado += mDiferencia;
                        valido = true;

                    }

                    i--;
                }
            }

            log.Debug("*-*-*mDiferencia:*-*- " + mDiferencia);
            log.Debug("mFacturar " + mFacturar);
            log.Debug("mFEL.Total " + mFEL.Total);
            log.Debug("mTotalAcumulado " + mTotalAcumulado);
            log.Debug("mFEL.Descuento " + mFEL.Descuento);
            log.Debug("mDescuentoAcumulado " + mDescuentoAcumulado);
            log.Debug("mFEL.Bruto " + mFEL.Bruto);
            log.Debug("mBrutoAcumulado " + mBrutoAcumulado);

            log.Debug("mFEL.iva " + mFEL.Iva);
            log.Debug("mFel.neto " + mFEL.Neto);
            log.Debug("mNetoAcumulado " + mNetoAcumulado);
            log.Debug("mDescuentoAcumulado " + mDescuentoAcumulado);

            if (mDescuentoAcumulado != mFEL.Descuento || (mDiferencia == 0 && mFEL.Neto != mNetoAcumulado))
            {
                if (mFEL.Neto != mNetoAcumulado)
                    mFEL.Neto = mNetoAcumulado;
                if (mFEL.Descuento != mDescuentoAcumulado)
                    mFEL.Descuento = mDescuentoAcumulado;
                if (mFEL.Total != mTotalAcumulado)
                    mFEL.Total = mTotalAcumulado;
                mFEL.Iva = Math.Round(mFEL.Detalle.Sum(x => x.ImpIva), 2, MidpointRounding.AwayFromZero);
                mFEL.Total = Math.Round(mFEL.Detalle.Sum(x => x.ImpTotal), 2, MidpointRounding.AwayFromZero);

            }
            
            #endregion

            string json = JsonConvert.SerializeObject(mFEL);
            log.Debug("PEDIDO PARA FEL " + json);

            return mFEL;
        }
        //metodo interno para validar los descuentos de una
        private MF_Clases.GuateFacturas.FEL.DetalleDoc ValidarDescuentos(MF_Clases.GuateFacturas.FEL.DetalleDoc det, decimal IVA, int financiera,int decimales=0)
        {

           
                //obtener el monto del descuento
                if (det.ImpDescuento > 0)
                {
                    decimal Descuento_calculado = 0;
                    decimal PorDescuento = det.PorcDesc;
                    decimal dDiferencia = 0;
                        
                    //1) obtener el porcentaje de descuento en base al monto de descuento
                    PorDescuento = Math.Round(det.ImpDescuento / det.ImpBruto * 100, 7, MidpointRounding.AwayFromZero);
                
                    //1.5 obtener el monto del descuento en base al porcentaje
                    Descuento_calculado = Math.Round(det.ImpBruto * PorDescuento / 100, 2, MidpointRounding.AwayFromZero);
                    //2) obtener la diferencia entre el monto calculado y el monto ingresado por el usuario
                    dDiferencia = det.ImpDescuento - Descuento_calculado;
                    if (dDiferencia > 0)
                    {
                        PorDescuento = Math.Round(det.ImpDescuento * 100 / det.ImpBruto, decimales, MidpointRounding.AwayFromZero);
                        det.PorcDesc = PorDescuento;

                        ValidarDescuentos(det, IVA, financiera, decimales + 1);
                    }
                    else //si la diferencia es negativa aumentar el monto del descuento al descuento calculado
                    {
                        det.ImpDescuento = Descuento_calculado;
                        det.PorcDesc = PorDescuento;

                        //recalcular valores del detalle
                        det.ImpIva = Math.Round((det.ImpBruto - det.ImpDescuento) / IVA * (IVA - 1), 2, MidpointRounding.AwayFromZero);
                        det.ImpNeto = Math.Round(det.ImpBruto - det.ImpDescuento - det.ImpIva, 2, MidpointRounding.AwayFromZero);
                        det.ImpTotal = det.ImpBruto - det.ImpDescuento;
                    }
                
                }
                
            return det;

        }
        private MF_Clases.GuateFacturas.FEL.DetalleDoc ValidarPrecioYCantidad(MF_Clases.GuateFacturas.FEL.DetalleDoc det)
        {
            if (det.Cantidad > 1)
            {
                var totalArticulo = det.Cantidad * det.Precio;
                var diferencia = det.ImpBruto - totalArticulo;
                if (diferencia != 0)
                {
                    var diferenciaArticulo = Math.Round(diferencia / det.Cantidad,4,MidpointRounding.AwayFromZero);
                    det.Precio = det.Precio + diferenciaArticulo;
                }
            }
            return det;
        }
        public PromocionRegistro ValidarDescuentoVales(PromocionRegistro pedido)
        {
           
            var qNivel = (from n in dbExactus.MF_NIVEL_PRECIO_COEFICIENTE where n.NIVEL_PRECIO == pedido.nivelPrecio select n).First();
            decimal iva = Utils.Utilitarios.ObtenerIva();
            decimal MOntoDescuentoTotal = 0;
            decimal MontoDescuentoVale = 0;
            #region "Canje de vales"
            if (pedido.DsctoVales >= 0)
            {

                List<PedidoLinea> articulosAfectados = new List<PedidoLinea>();
                List<PedidoLinea> articulosNoAfectados = new List<PedidoLinea>();
                if (pedido.Descuento > 0)
                {
                    articulosAfectados= pedido.articulos.Where(x => x.PrecioTotal > 0).ToList();
                   articulosNoAfectados= pedido.articulos.Where(x => x.PrecioTotal == 0).ToList();
                }
                else
                {
                    articulosAfectados = pedido.articulos;
                }
                var porDesc = articulosAfectados.Sum(x =>x.PrecioFacturar) > 0 ? pedido.DsctoVales / articulosAfectados.Sum(x => x.PrecioFacturar) : 0;
                int nTotalArticulos = articulosAfectados.Count();
                var DescuentoVale = pedido.DsctoVales + (pedido.Descuento-articulosNoAfectados.Sum(x =>x.Descuento));
                
                foreach (var x in articulosAfectados)
                {
                    x.PorcentajeDescuento=pedido.Descuento>0? x.PorcentajeDescuento:0;
                    var NuevoPorcDscto = Math.Round(x.PorcentajeDescuento / 100 + porDesc, 4, MidpointRounding.AwayFromZero) ;

                    if (nTotalArticulos == (x.Linea + 1))
                    {
                       
                        x.Descuento = Math.Round(DescuentoVale - MOntoDescuentoTotal, 2, MidpointRounding.AwayFromZero);
                        MontoDescuentoVale = x.Descuento;
                        x.PrecioTotal = Math.Round(
                            ( x.PrecioFacturar - x.Descuento) *Convert.ToDecimal(qNivel.FACTOR)
                            , 2, MidpointRounding.AwayFromZero);
                        x.NetoFacturar = x.PrecioTotal;
                        NuevoPorcDscto = x.Descuento / x.PrecioFacturar;
                    }
                    else
                    {

                        x.Descuento = Math.Round(x.PrecioUnitario / Convert.ToDecimal(qNivel.FACTOR) * x.CantidadPedida * NuevoPorcDscto, 2, MidpointRounding.AwayFromZero);
                        MontoDescuentoVale = Math.Round(x.PrecioUnitario / Convert.ToDecimal(qNivel.FACTOR) * x.CantidadPedida * NuevoPorcDscto, 2, MidpointRounding.AwayFromZero);
                        x.PrecioTotal = Math.Round(( (x.PrecioFacturar*Convert.ToDecimal(qNivel.FACTOR)) * (1 - NuevoPorcDscto)), 2, MidpointRounding.AwayFromZero);
                        x.NetoFacturar = x.PrecioTotal;

                    }
                    x.PorcentajeDescuento = NuevoPorcDscto * 100;
                    MOntoDescuentoTotal += MontoDescuentoVale;

                    //x.PrecioFacturar = Math.Round(x.PrecioFacturar * NuevoPorcDscto, 2, MidpointRounding.AwayFromZero);
                    x.PrecioFacturar = Math.Round(x.PrecioFacturar, 4, MidpointRounding.AwayFromZero);
                    
                    //x.PrecioUnitario = Math.Round(x.PrecioUnitario * NuevoPorcDscto, 2, MidpointRounding.AwayFromZero);

                }
                pedido.Descuento = articulosNoAfectados.Sum(x => x.Descuento);
            }
            
            #endregion

            //Esta parte es fija p
            pedido.Descuento += MOntoDescuentoTotal - pedido.DsctoVales <0.1M ? pedido.DsctoVales : MOntoDescuentoTotal;
           
            var qFinanciera = (from f in dbExactus.MF_Financiera where f.Financiera == pedido.Financiera select f).FirstOrDefault();


            decimal mPrecioBien = 0;
            decimal mMonto = 0;
            foreach (var item in pedido.articulos)
            {
                if (qFinanciera.ES_BANCO == "S")
                {
                    decimal mPrecioFacturar = Math.Round(Convert.ToDecimal(item.PrecioTotal) / Convert.ToDecimal(qNivel.FACTOR), 2, MidpointRounding.AwayFromZero);

                    //item.PrecioFacturar = mPrecioFacturar;
                    mPrecioBien = mPrecioBien + mPrecioFacturar;
                    mMonto = mMonto + item.PrecioTotal;
                    pedido.EsBanco = true;
                }
                else
                {
                    pedido.EsBanco = false;
                    mPrecioBien = mPrecioBien + item.PrecioFacturar;
                    mMonto = mMonto + item.PrecioTotal;
                }
            }

            // pedido.TotalFacturar =  mPrecioBien;
            pedido.Monto = mMonto;
           
            return pedido;

        }
    }
}