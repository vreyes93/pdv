﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Utils;
using FiestaNETRestServices.Models;
using MF_Clases;

using System.Text;
using System.Xml;
using System.Data;
using System.Transactions;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using System.Reflection;
using FiestaNETRestServices.Models.InterConsumo;
using static MF_Clases.Clases;
using System.Globalization;

namespace FiestaNETRestServices.DAL
{
    public class DALDesembolsoInterconsumo : MFDAL
    {
        public DALDesembolsoInterconsumo()
        {
        }

        /// <summary>
        /// Esta función tiene como propósito verificar si el consecutivo existe.
        /// </summary>
        public Respuesta ConsecutivoExists(string NombreConsecutivo)
        {
            Respuesta Respuesta = new Respuesta();

            if ((from c in dbExactus.CONSECUTIVO where c.CONSECUTIVO1 == NombreConsecutivo select c).Count() == 0)
            {
                Respuesta.Mensaje = "No existe consecutivo de recibos para su tienda, debe comunicarse con el departamento de informática";
                return Respuesta;
            }

            Respuesta.Exito = Const.OK;
            return Respuesta;
        }

        /// <summary>
        /// Esta función tiene como propósito verificar si el desembolso ya ha sido registrado.
        /// </summary>
        public Respuesta DesembolsoYaRegistrado(decimal BoletaNo,int financiera)
        {
            Respuesta mRespuesta = new Respuesta();
            string CUENTA_BANCARIA = string.Empty;
            switch (financiera)
            {
                case 7: CUENTA_BANCARIA = Const.CUENTA_BANCARIA.INTERNACIONAL; break;
                case 12: CUENTA_BANCARIA = Const.CUENTA_BANCARIA.BAM;break;
            }
            var mCuentaBanco = from     mv in dbExactus.MOV_BANCOS
                               where    mv.CUENTA_BANCO == CUENTA_BANCARIA
                                        && mv.TIPO_DOCUMENTO == "T/C"
                                        && mv.NUMERO == BoletaNo
                               select   mv;

            if (mCuentaBanco.Count() > 0)
            {
                mRespuesta.Exito = Const.OK;
                mRespuesta.Mensaje = string.Format("Desembolso de la financiera Numero {0} ya registrado en el sistema.", BoletaNo);
            }
            return mRespuesta;
        }

        /// <summary>
        /// Esta función tiene como propósito cargar el desembolso al sistema de Exactus.
        /// </summary>
        public Respuesta CargarDesembolso( LoteDesombolsosFinanciera LoteDesembolsos)
        {
            Respuesta Respuesta = new Respuesta();

            string mRecibo = "";
            int mReciboInt;
            string mAsientoRecibo="";
            int mAsientoReciboInt;

            string mAsientoDeposito = "";
            int mAsientoDepositoInt;

            decimal BoletaNo;
            string BoletaFecha;

            DateTime mFechaRigeDesembolso = LoteDesembolsos.FechaRigeDesembolso;

            int cantidadDesembolsos = 0;
            decimal mMontoDeposito = 0;
            decimal mMontoDepositoDolar = 0;
            

            string mCuentaBancaria = string.Empty;
            string mNombreConsecutivo = string.Empty;
            string mFormatoConsecutivo = string.Empty;
            string mDescripcionPago = string.Empty;

            switch (LoteDesembolsos.Financiera)
            {
                case 7:
                    mCuentaBancaria = Const.CUENTA_BANCARIA.INTERNACIONAL;
                    mNombreConsecutivo = Const.DESEMBOLSO_INTERCONSUMO.NOMBRE_CONSECUTIVO;
                    mFormatoConsecutivo = Const.DESEMBOLSO_INTERCONSUMO.FORMATO_CONSECUTIVO;
                    mDescripcionPago = Const.DESEMBOLSO_INTERCONSUMO.DESCRIPCION_PAGO;
                    break;
                case 12:
                    mCuentaBancaria = Const.CUENTA_BANCARIA.BAM;
                    mNombreConsecutivo = Const.DESEMBOLSO_CREDIPLUS.NOMBRE_CONSECUTIVO;
                    mFormatoConsecutivo = Const.DESEMBOLSO_CREDIPLUS.FORMATO_CONSECUTIVO;
                    mDescripcionPago = Const.DESEMBOLSO_CREDIPLUS.DESCRIPCION_PAGO;
                    break;
            }

            string mNombreBanco = "";
            string mCuentaContable = "";
            Int16 mSubTipoTrans;

            decimal mTipoCambio;
            decimal mIVA;

            EXACTUSERPEntities db = new EXACTUSERPEntities();
            db.CommandTimeout = 999999999;

            try
            {

                BoletaNo = decimal.Parse(ObtenerFecha(LoteDesembolsos.FechaDesembolsos).Replace("/", "").Replace("-", ""));

                BoletaFecha = ObtenerFecha(LoteDesembolsos.FechaDesembolsos);

                //BoletaFecha = (DateTime.Now.Day * 10000000 ) + (DateTime.Now.Month *5) + (DateTime.Now.Year);


                var qCuentaBancaria = (from c in db.CUENTA_BANCARIA
                                       where c.CUENTA_BANCO == mCuentaBancaria
                                       select c);



                if (qCuentaBancaria.Count() == 1)
                {
                    mCuentaContable = qCuentaBancaria.First().CTA_CONTABLE;
                    mNombreBanco = qCuentaBancaria.First().NOMBRE;
                }
                else {

                    Respuesta.Mensaje = string.Format("Error al verificar la cuenta contable {0}, informar a Informática.", mCuentaBancaria);
                    return Respuesta;
                }


                mTipoCambio = (from tc in db.TIPO_CAMBIO_HIST
                               where tc.TIPO_CAMBIO == "REFR"
                               orderby tc.FECHA
                               descending
                               select tc).Take(1).First().MONTO;

                mIVA = 1 + (
                                        (from im in db.IMPUESTO
                                         where im.IMPUESTO1 == "IVA"
                                         select im).First().IMPUESTO11 / 100);

                mSubTipoTrans = Convert.ToInt16(
                                        (from c in db.MF_Cobrador
                                         where c.COBRADOR == "F01"
                                         select c).First().SUBTIPO_TRANSFERENCIA);

                mAsientoReciboInt = Convert.ToInt32(
                                        (from p in db.PAQUETE
                                         where p.PAQUETE1 == Const.CONTABILIDAD.CUENTAS_POR_COBRAR
                                         select p).First().ULTIMO_ASIENTO.Replace(Const.CONTABILIDAD.CUENTAS_POR_COBRAR, ""));

                mAsientoDepositoInt = Convert.ToInt32(
                                        (from p in db.PAQUETE
                                         where p.PAQUETE1 == Const.CONTABILIDAD.CONTROL_BANCARIO
                                         select p).First().ULTIMO_ASIENTO.Replace(Const.CONTABILIDAD.CONTROL_BANCARIO, "")) + 1;

                mReciboInt = Convert.ToInt32(
                                        (from c in db.CONSECUTIVO
                                         where c.CONSECUTIVO1 == (mNombreConsecutivo)
                                         select c).First().ULTIMO_VALOR.Replace(string.Format(mFormatoConsecutivo), ""));

                mAsientoDeposito = Const.CONTABILIDAD.CONTROL_BANCARIO + mAsientoDepositoInt.ToString(Const.REPLICATE.LONGITUD_6);
                mAsientoRecibo = Const.CONTABILIDAD.CUENTAS_POR_COBRAR + mAsientoReciboInt.ToString(Const.REPLICATE.LONGITUD_7);


                using (TransactionScope transactionScope = new TransactionScope())
                {

                    foreach (var item in LoteDesembolsos.Desembolsos)
                    {


                        mReciboInt++;
                        mRecibo = mFormatoConsecutivo + mReciboInt.ToString(Const.REPLICATE.LONGITUD_7);

                        mAsientoReciboInt++;
                        mAsientoRecibo = Const.CONTABILIDAD.CUENTAS_POR_COBRAR + mAsientoReciboInt.ToString(Const.REPLICATE.LONGITUD_7);

                        decimal mMonto = item.Valor;

                        decimal mMontoDolar = Math.Round((mMonto / mTipoCambio), 2, MidpointRounding.AwayFromZero);

                        mMontoDeposito = mMontoDeposito + mMonto;
                        mMontoDepositoDolar = mMontoDepositoDolar + mMontoDolar;


                        ASIENTO_DE_DIARIO iAsientoDiarioRecibo = new ASIENTO_DE_DIARIO
                        {
                            ASIENTO = mAsientoRecibo,
                            PAQUETE = Const.CONTABILIDAD.CUENTAS_POR_COBRAR,
                            TIPO_ASIENTO = Const.TIPO_ASIENTO.RECIBO,
                            FECHA = mFechaRigeDesembolso,
                            CONTABILIDAD = "A",
                            ORIGEN = Const.CONTABILIDAD.CUENTAS_POR_COBRAR,
                            CLASE_ASIENTO = "N",
                            TOTAL_DEBITO_LOC = mMonto,
                            TOTAL_DEBITO_DOL = mMontoDolar,
                            TOTAL_CREDITO_LOC = mMonto,
                            TOTAL_CREDITO_DOL = mMontoDolar,
                            ULTIMO_USUARIO = LoteDesembolsos.Usuario,
                            FECHA_ULT_MODIF = DateTime.Now,
                            MARCADO = "N",
                            NOTAS = string.Format("Documento introducido desde Cuentas por Cobrar  Documento:  REC - {0}  Cliente: {1} - {2}  Monto: {3}  Moneda: GTQ  Subtipo Documento: {4}   Notas del Documento: Generado desde el Punto de Venta"
                            , mRecibo, item.ClienteMF
                            , item.NombreCliente, item.Valor.ToString(), "Depósito"),
                            TOTAL_CONTROL_LOC = item.Valor,
                            TOTAL_CONTROL_DOL = mMontoDolar,
                            USUARIO_CREACION = LoteDesembolsos.Usuario,
                            FECHA_CREACION = DateTime.Now,
                            NoteExistsFlag = 0,
                            RecordDate = DateTime.Now,
                            RowPointer = //(System.Guid)db.fcNewID()
                                            Guid.NewGuid(),
                            CreatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                            UpdatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                            CreateDate = DateTime.Now.Date
                        };

                        db.ASIENTO_DE_DIARIO.AddObject(iAsientoDiarioRecibo);

                        DIARIO iDiario1Recibo = new DIARIO
                        {
                            ASIENTO = mAsientoRecibo,
                            CONSECUTIVO = 1,
                            NIT = "ND",
                            CENTRO_COSTO = Const.CENTRO_COSTOS.OTROS_EGRESOS,
                            CUENTA_CONTABLE = Const.CUENTA_CONTABLE.CLIENTES,
                            FUENTE = string.Format("REC{0}", mRecibo),
                            REFERENCIA = string.Format("{0}{1}", mDescripcionPago, item.Prestamo),
                            CREDITO_LOCAL = item.Valor,
                            CREDITO_DOLAR = mMontoDolar,
                            NoteExistsFlag = 0,
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                            UpdatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                            CreateDate = DateTime.Now,
                            RowPointer = Guid.NewGuid()
                        };

                        db.DIARIO.AddObject(iDiario1Recibo);

                        DIARIO iDiario2Recibo = new DIARIO
                        {
                            ASIENTO = mAsientoRecibo,
                            CONSECUTIVO = 2,
                            NIT = "ND",
                            CENTRO_COSTO = Const.CENTRO_COSTOS.OTROS_EGRESOS,
                            CUENTA_CONTABLE = Const.CUENTA_CONTABLE.CAJA_MONEDA_NACIONAL,
                            FUENTE = string.Format("REC{0}", mRecibo),
                            REFERENCIA = string.Format("{0}{1}", mDescripcionPago, item.Prestamo),
                            DEBITO_LOCAL = item.Valor,
                            DEBITO_DOLAR = mMontoDolar,
                            NoteExistsFlag = 0,
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                            UpdatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                            CreateDate = DateTime.Now,
                            RowPointer = Guid.NewGuid()
                        };

                        db.DIARIO.AddObject(iDiario2Recibo);

                        //----------------------------------------------------------------------------------------------------
                        // SaldoTotal es la variable que se registra en el saldo de este desembolso, dependiendo de la 
                        // cuenta corriente del cliente, este restará el saldo de la factura relacionada, o en su defecto
                        // de las facturas que tengan saldo.
                        //----------------------------------------------------------------------------------------------------
                        decimal SaldoTotal = item.Valor;
                        decimal mDebitar = 0;

                        //----------------------------------------------------------------------------------------------------
                        // Verificamos si la factura relacionada al desembolso tiene saldo mayor a cero y su valor es igual
                        // o mayor al mismo
                        //----------------------------------------------------------------------------------------------------
                        var qUpdateCC = from c in db.DOCUMENTOS_CC
                                        where   c.DOCUMENTO == item.FacturaMF 
                                                && c.TIPO == "FAC" 
                                                && c.SALDO > 0
                                                && c.SALDO >= item.Valor
                                        select c;

                        
                        if (qUpdateCC.Count() > 0)
                        {
                            //----------------------------------------------------------------------------------------------------
                            // Este es el caso normal, se resta el saldo de la factura, con el monto del desembolso.
                            // Caso normal, se aplica la resta del saldo a la factura correspondiente
                            //----------------------------------------------------------------------------------------------------

                            foreach (var itemCC in qUpdateCC)
                            {
                                itemCC.SALDO = Math.Round(itemCC.SALDO - (decimal)item.Valor, 2, MidpointRounding.AwayFromZero);
                                itemCC.SALDO_CLIENTE = Math.Round(itemCC.SALDO_CLIENTE - (decimal)item.Valor, 2, MidpointRounding.AwayFromZero);
                                itemCC.SALDO_LOCAL = Math.Round(itemCC.SALDO_LOCAL - (decimal)item.Valor, 2, MidpointRounding.AwayFromZero);
                                itemCC.SALDO_DOLAR = Math.Round(itemCC.SALDO_DOLAR - (decimal)mMontoDolar, 2, MidpointRounding.AwayFromZero);
                                itemCC.FECHA_ULT_CREDITO = (DateTime)DateTime.Now.Date;
                            }
                            SaldoTotal = 0;
                        }
                        else
                        {
                            //----------------------------------------------------------------------------------------------------
                            // Este es el caso el desembolso es mayor al saldo de la factura ó la factura correspondiente ya 
                            // no tiene saldo, en este caso hay que aplicarle a otras facturas con saldo.
                            //----------------------------------------------------------------------------------------------------

                            //Excepcion, tomar en consideracion el caso del cliente 0082914, con las facturas F16-000378 y F16C-000648
                            //en el que se realiza una venta con Interconsumo y otra venta al contado. El PDV al realizar un desposito 
                            //de la factura de contado, realiza la resta del saldo de la factura mas antigua, siempre y cuando tenga un saldo mayor a 0, 
                            //en este caso, la factura correspondia a interconsumo, y no la de contado. Esta parte de codigo resulve esos casos.


                            var qUpdateCCPDV = from c in db.DOCUMENTOS_CC
                                               where    c.TIPO == "FAC" 
                                                        && c.CLIENTE_ORIGEN == item.ClienteMF 
                                                        && c.SALDO > 0
                                                orderby c.CreateDate
                                               select c;

                            foreach (var itemCCPDV in qUpdateCCPDV)
                            {

                                if (SaldoTotal > 0)
                                {

                                    mDebitar = (itemCCPDV.SALDO >= SaldoTotal) ? SaldoTotal : itemCCPDV.SALDO;
                                    itemCCPDV.SALDO = Math.Round(itemCCPDV.SALDO - (decimal)mDebitar, 2, MidpointRounding.AwayFromZero);
                                    itemCCPDV.SALDO_CLIENTE = Math.Round(itemCCPDV.SALDO_CLIENTE - (decimal)mDebitar, 2, MidpointRounding.AwayFromZero);
                                    itemCCPDV.SALDO_LOCAL = Math.Round(itemCCPDV.SALDO_LOCAL - (decimal)mDebitar, 2, MidpointRounding.AwayFromZero);
                                    itemCCPDV.SALDO_DOLAR = Math.Round(itemCCPDV.SALDO_DOLAR - (decimal)Math.Round((mDebitar / mTipoCambio), 2, MidpointRounding.AwayFromZero), 2, MidpointRounding.AwayFromZero);
                                    itemCCPDV.FECHA_ULT_CREDITO = (DateTime)DateTime.Now.Date;

                                    SaldoTotal = SaldoTotal - mDebitar;
                                }
                            }
                        }

                        System.Guid mGuidRec = Guid.NewGuid();

                        DOCUMENTOS_CC iDocumentos_CCRecibo = new DOCUMENTOS_CC
                        {
                            DOCUMENTO = mRecibo,
                            TIPO = Const.TIPO_ASIENTO.RECIBO,
                            APLICACION = string.Format("{0}{1}", mDescripcionPago, item.Prestamo),
                            FECHA_DOCUMENTO = mFechaRigeDesembolso,
                            FECHA = mFechaRigeDesembolso,
                            MONTO = item.Valor,
                            SALDO = SaldoTotal,
                            MONTO_LOCAL = item.Valor,
                            SALDO_LOCAL = SaldoTotal,
                            MONTO_DOLAR = mMontoDolar,
                            SALDO_DOLAR = (decimal)Math.Round((SaldoTotal / mTipoCambio), 2, MidpointRounding.AwayFromZero),
                            MONTO_CLIENTE = item.Valor,
                            SALDO_CLIENTE = SaldoTotal,
                            TIPO_CAMBIO_MONEDA = 1,
                            TIPO_CAMBIO_DOLAR = mTipoCambio,
                            TIPO_CAMBIO_CLIENT = 1,
                            TIPO_CAMB_ACT_LOC = 1,
                            TIPO_CAMB_ACT_DOL = mTipoCambio,
                            TIPO_CAMB_ACT_CLI = 1,
                            SUBTOTAL = item.Valor,
                            DESCUENTO = 0,
                            IMPUESTO1 = 0,
                            IMPUESTO2 = 0,
                            RUBRO1 = 0,
                            RUBRO2 = 0,
                            MONTO_RETENCION = 0,
                            SALDO_RETENCION = 0,
                            DEPENDIENTE = "N",
                            FECHA_ULT_CREDITO = mFechaRigeDesembolso,
                            CARGADO_DE_FACT = "N",
                            APROBADO = "S",
                            ASIENTO = mAsientoRecibo,
                            ASIENTO_PENDIENTE = "N",
                            FECHA_ULT_MOD = DateTime.Now.Date,
                            NOTAS = "",
                            CLASE_DOCUMENTO = "N",
                            FECHA_VENCE = mFechaRigeDesembolso,
                            NUM_PARCIALIDADES = 0,
                            COBRADOR = "F01",
                            USUARIO_ULT_MOD = LoteDesembolsos.Usuario,
                            CONDICION_PAGO = "0",
                            MONEDA = "GTQ",
                            VENDEDOR = item.Vendedor,
                            CLIENTE_REPORTE = item.ClienteMF,
                            CLIENTE_ORIGEN = item.ClienteMF,
                            CLIENTE = item.ClienteMF,
                            SUBTIPO = 13,
                            PORC_INTCTE = 0,
                            USUARIO_APROBACION = LoteDesembolsos.Usuario,
                            FECHA_APROBACION = DateTime.Now.Date,
                            ANULADO = "N",
                            NoteExistsFlag = 0,
                            RowPointer = mGuidRec,
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                            UpdatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                            CreateDate = DateTime.Now,
                            BASE_IMPUESTO1 = 0,
                            BASE_IMPUESTO2 = 0,
                            DEPENDIENTE_GP = "N",
                            SALDO_TRANS = 0,
                            SALDO_TRANS_LOCAL = 0,
                            SALDO_TRANS_DOLAR = 0,
                            TIPO_ASIENTO = "REC",
                            PAQUETE = Const.CONTABILIDAD.CUENTAS_POR_COBRAR,
                            SALDO_TRANS_CLI = 0,
                            FACTURADO = "N",
                            GENERA_DOC_FE = "N"
                        };

                        db.DOCUMENTOS_CC.AddObject(iDocumentos_CCRecibo);

                        CG_AUX iCG_AUX1Recibo = new CG_AUX
                        {
                            GUID_ORIGEN = mGuidRec.ToString(),
                            TABLA_ORIGEN = "DOCUMENTOS_CC",
                            ASIENTO = mAsientoRecibo,
                            LINEA = 1,
                            COMENTARIO = "GUID - Origen",
                            NoteExistsFlag = 0,
                            RowPointer = Guid.NewGuid(),
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                            UpdatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                            CreateDate = DateTime.Now
                        };

                        db.CG_AUX.AddObject(iCG_AUX1Recibo);
                        CG_AUX iCG_AUX2Recibo = new CG_AUX
                        {
                            GUID_ORIGEN = mGuidRec.ToString(),
                            TABLA_ORIGEN = "DOCUMENTOS_CC",
                            ASIENTO = mAsientoRecibo,
                            LINEA = 2,
                            COMENTARIO = "GUID - Origen",
                            NoteExistsFlag = 0,
                            RowPointer = Guid.NewGuid(),
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                            UpdatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                            CreateDate = DateTime.Now
                        };

                        db.CG_AUX.AddObject(iCG_AUX2Recibo);

                        var qUpdateCliente = from c in db.CLIENTE where c.CLIENTE1 == item.ClienteMF select c;

                        foreach (var itemCliente in qUpdateCliente)
                        {
                            decimal mNuevoSaldo = Math.Round(itemCliente.SALDO - (decimal)item.Valor, 2, MidpointRounding.AwayFromZero);

                            itemCliente.SALDO = mNuevoSaldo;
                            itemCliente.SALDO_LOCAL = Math.Round(itemCliente.SALDO_LOCAL - (decimal)item.Valor, 2, MidpointRounding.AwayFromZero);
                            itemCliente.SALDO_DOLAR = Math.Round(itemCliente.SALDO_DOLAR - (decimal)mMontoDolar, 2, MidpointRounding.AwayFromZero);
                            itemCliente.FECHA_ULT_MOV = DateTime.Now;

                        }

                        var qUpdateSC = from c in db.SALDO_CLIENTE where c.CLIENTE == item.ClienteMF select c;
                        foreach (var itemSC in qUpdateSC)
                        {
                            decimal mNuevoSaldo = Math.Round(itemSC.SALDO - (decimal)item.Valor, 2, MidpointRounding.AwayFromZero);

                            itemSC.SALDO = mNuevoSaldo;
                            itemSC.FECHA_ULT_MOV = DateTime.Now;

                        }

                        AUXILIAR_CC iAuxiliar_CCRecibo = new AUXILIAR_CC
                        {
                            TIPO_CREDITO = Const.TIPO_ASIENTO.RECIBO,
                            TIPO_DEBITO = "FAC",
                            FECHA = mFechaRigeDesembolso,
                            CREDITO = mRecibo,
                            DEBITO = item.FacturaMF,
                            MONTO_DEBITO = item.Valor,
                            MONTO_CREDITO = item.Valor,
                            MONTO_LOCAL = item.Valor,
                            MONTO_DOLAR = mMontoDolar,
                            MONTO_CLI_CREDITO = item.Valor,
                            MONTO_CLI_DEBITO = item.Valor,
                            MONEDA_CREDITO = "GTQ",
                            MONEDA_DEBITO = "GTQ",
                            CLI_REPORTE_CREDIT = item.ClienteMF,
                            CLI_REPORTE_DEBITO = item.ClienteMF,
                            CLI_DOC_CREDIT = item.ClienteMF,
                            CLI_DOC_DEBITO = item.ClienteMF,
                            RowPointer = Guid.NewGuid(),
                            RecordDate = DateTime.Now,
                            NoteExistsFlag = 0,
                            FOLIOSAT_CREDITO="",
                            TIPO_CAMBIO_APLICA = 1,
                            CreatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                            UpdatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                            CreateDate = DateTime.Now
                        };
                        db.AUXILIAR_CC.AddObject(iAuxiliar_CCRecibo);

                        var qUpdateFactura = from f in db.MF_Factura
                                             where f.FACTURA == item.FacturaMF select f;
                        foreach (var itemFac in qUpdateFactura)
                        {
                            itemFac.RECIBO = mRecibo;
                            itemFac.PAGADA = "S";
                        }
                    
                        MF_Recibo iRecibo = new MF_Recibo
                        {
                            DOCUMENTO = mRecibo,
                            TIPO = Const.TIPO_ASIENTO.RECIBO,
                            CLIENTE = item.ClienteMF,
                            FECHA = mFechaRigeDesembolso,
                            COBRADOR = "F01",
                            VENDEDOR = item.Vendedor,
                            FACTURA = item.FacturaMF,
                            EFECTIVO = item.Valor,
                            CHEQUE = 0,
                            TARJETA = 0,
                            MONTO = item.Valor,
                            NUMERO_CHEQUE = "",
                            ENTIDAD_FINANCIERA_CHEQUE = "ND",
                            EMISOR_TARJETA = 0,
                            ENTIDAD_FINANCIERA_TARJETA = "ND",
                            ES_ANTICIPO = "N",
                            DEPOSITO = 0,
                            ENTIDAD_FINANCIERA_DEPOSITO = "ND",
                            OBSERVACIONES = string.Format("{0}{1}", mDescripcionPago, item.Prestamo),
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                            UpdatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                            CreateDate = DateTime.Now,
                            CONSECUTIVO = (mNombreConsecutivo),
                            USUARIO = LoteDesembolsos.Usuario,
                            FECHA_REGISTRO = DateTime.Now,
                            TIENE_DEPOSITO = "S",
                            POS = "N",
                            ANULADO = "N",
                            EFECTIVO_ORIGINAL = item.Valor,
                            CHEQUE_ORIGINAL = 0,
                            TARJETA_ORIGINAL = 0,
                            MONTO_ORIGINAL = item.Valor,
                            CERRADO = "S",
                            EFECTIVO_DOLARES = 0,
                            EFECTIVO_DOLARES_ORIGINAL = 0,
                            MONEDA = "GTQ",
                            TASA_CAMBIO = 1,
                            CUENTA_BANCO = mCuentaBancaria,
                            TIPO_DOCUMENTO = "T/C",
                            NUMERO = BoletaNo,
                            ESTADO_AUTORIZACION = "N"

                        };
                        db.MF_Recibo.AddObject(iRecibo);

                        var qFacturaEstado =
                                            from f in db.MF_Factura_Estado
                                            where f.FACTURA == item.FacturaMF
                                                    && f.ESTADO == Const.Expediente.DESEMBOLSADO
                                            select f;
                        if (qFacturaEstado.Count() == 0)
                        {
                            MF_Factura_Estado iFacturaEstado = new MF_Factura_Estado
                            {
                                FACTURA = item.FacturaMF,
                                ESTADO = Const.Expediente.DESEMBOLSADO,
                                FECHA = LoteDesembolsos.FechaDesembolsos,
                                RecordDate = DateTime.Now,
                                CreatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                                UpdatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                                CreateDate = DateTime.Now
                            };
                            db.MF_Factura_Estado.AddObject(iFacturaEstado);
                        }
                        else
                        {
                            foreach (var iFacturaEstado in qFacturaEstado)
                            {
                                iFacturaEstado.RecordDate = DateTime.Now;
                            }

                        }


                        var qMFPedido =
                                            from p in db.MF_Pedido
                                            where p.FACTURA == item.FacturaMF
                                            select p;
                        
                        if (qMFPedido.Count() > 0)
                        {
                            foreach (var iMFPedido in qMFPedido)
                            {
                                iMFPedido.RecordDate = DateTime.Now;
                                iMFPedido.MONTO_DESEMBOLSADO = item.Valor;
                                iMFPedido.PRESTAMO = item.Prestamo;
                            }
                        }

                        cantidadDesembolsos++;

                    }//foreach

                    var qUpdateConsecutivo = from c in db.CONSECUTIVO where c.CONSECUTIVO1 == (mNombreConsecutivo) select c;
                    foreach (var itemC in qUpdateConsecutivo)
                    {
                        itemC.ULTIMO_VALOR = mRecibo;
                    }


                    var qUpdatePaqueteRec = from p in db.PAQUETE where p.PAQUETE1 == Const.CONTABILIDAD.CUENTAS_POR_COBRAR select p;
                    foreach (var itemPAQ in qUpdatePaqueteRec)
                    {
                        itemPAQ.ULTIMO_ASIENTO = mAsientoRecibo;
                    }

                    System.Guid mGuid = Guid.NewGuid();

                    MOV_BANCOS iMovBanco = new MOV_BANCOS
                    {
                        CUENTA_BANCO = mCuentaBancaria,
                        TIPO_DOCUMENTO = "T/C",
                        NUMERO = BoletaNo,
                        SUBTIPO = 0,
                        FECHA = mFechaRigeDesembolso,
                        REFERENCIA = string.Format("Pago " + (LoteDesembolsos.Financiera == 7 ? "INTERCONSUMO":"CREDIPLUS") +" {0} ({1})", BoletaFecha, cantidadDesembolsos),
                        MONTO = mMontoDeposito,
                        DETALLE = "",
                        CONFIRMADO = "S",
                        ORIGEN = Const.CONTABILIDAD.CONTROL_BANCARIO,
                        ASIENTO = mAsientoDeposito,
                        ANULADO = "N",
                        FCH_HORA_CREACION = DateTime.Now,
                        USUARIO_CREACION = LoteDesembolsos.Usuario,
                        ESTADO = "N",
                        IMPRESO = "N",
                        CLASE_DIF = "X",
                        ACLARADA_DIF = "X",
                        CLASE_DOCUMENTO = "N",
                        MODO_REGISTRO = "M",
                        LIQUIDADO = "N",
                        FCH_HORA_MODIFIC = DateTime.Now,
                        USUARIO_MODIFIC = LoteDesembolsos.Usuario,
                        TIPO_CAMBIO_LOCAL = 1,
                        TIPO_CAMBIO_DOLAR = mTipoCambio,
                        APROBADO = "S",
                        USUARIO_APROBACION = LoteDesembolsos.Usuario,
                        FECHA_APROBACION = DateTime.Now,
                        NoteExistsFlag = 0,
                        RecordDate = DateTime.Now,
                        RowPointer = mGuid,
                        CreatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                        UpdatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                        CreateDate = DateTime.Now,
                        TIPO_ASIENTO = Const.TIPO_ASIENTO.TRANSFERENCIA_CREDITO,
                        PAQUETE = Const.CONTABILIDAD.CONTROL_BANCARIO,
                        DEPENDIENTE_GP = "N",
                        FECHA_CONTABLE = mFechaRigeDesembolso,
                        IMPUESTO1 = 0,
                        IMP1_NODES =0

                    };

                    db.MOV_BANCOS.AddObject(iMovBanco);

                    MF_Deposito iDeposito = new MF_Deposito
                    {
                        CUENTA_BANCO = mCuentaBancaria,
                        TIPO_DOCUMENTO = "T/C",
                        NUMERO = BoletaNo,
                        FECHA = mFechaRigeDesembolso,
                        COBRADOR = "F01",
                        MONTO = mMontoDeposito,
                        OBSERVACIONES = "",
                        RecordDate = DateTime.Now,
                        CreatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                        UpdatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                        CreateDate = DateTime.Now,
                        CERRADO = "S"
                    };

                    db.MF_Deposito.AddObject(iDeposito);


                    ASIENTO_DE_DIARIO iAsientoDiarioDeposito = new ASIENTO_DE_DIARIO
                    {
                        ASIENTO = mAsientoDeposito,
                        PAQUETE = Const.CONTABILIDAD.CONTROL_BANCARIO,
                        TIPO_ASIENTO = Const.TIPO_ASIENTO.TRANSFERENCIA_CREDITO,
                        FECHA = mFechaRigeDesembolso,
                        CONTABILIDAD = "A",
                        ORIGEN = Const.CONTABILIDAD.CONTROL_BANCARIO,
                        CLASE_ASIENTO = "N",
                        TOTAL_DEBITO_LOC = mMontoDeposito,
                        TOTAL_DEBITO_DOL = mMontoDepositoDolar,
                        TOTAL_CREDITO_LOC = mMontoDeposito,
                        TOTAL_CREDITO_DOL = mMontoDepositoDolar,
                        ULTIMO_USUARIO = LoteDesembolsos.Usuario,
                        FECHA_ULT_MODIF = DateTime.Now,
                        MARCADO = "N",
                        NOTAS = string.Format("La TEF de Crédito #{0} fue introducido desde Control Bancario.  Cuenta Bancaria: {1} {2} Usuario: {3} Fecha / Hora: {4}", BoletaNo,
                         mCuentaBancaria, mNombreBanco, LoteDesembolsos.Usuario, DateTime.Now.ToString()),
                        TOTAL_CONTROL_LOC = mMontoDeposito,
                        TOTAL_CONTROL_DOL = mMontoDepositoDolar,
                        USUARIO_CREACION = LoteDesembolsos.Usuario,
                        FECHA_CREACION = DateTime.Now,
                        NoteExistsFlag = 0,
                        RecordDate = DateTime.Now,
                        RowPointer = Guid.NewGuid(),
                        CreatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                        UpdatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                        CreateDate = DateTime.Now
                    };

                    db.ASIENTO_DE_DIARIO.AddObject(iAsientoDiarioDeposito);

                    DIARIO iDiario1Deposito = new DIARIO
                    {
                        ASIENTO = mAsientoDeposito,
                        CONSECUTIVO = 1,
                        NIT = "ND",
                        CENTRO_COSTO = Const.CENTRO_COSTOS.OTROS_EGRESOS,
                        CUENTA_CONTABLE = mCuentaContable,
                        FUENTE = string.Format("T/C{0}", BoletaNo),
                        REFERENCIA = string.Format("Pago "+(LoteDesembolsos.Financiera == 7 ? "INTERCONSUMO" : "CREDIPLUS") + " {0} ({1})", BoletaFecha, cantidadDesembolsos),
                        DEBITO_LOCAL = mMontoDeposito,
                        DEBITO_DOLAR = mMontoDepositoDolar,
                        NoteExistsFlag = 0,
                        RecordDate = DateTime.Now,
                        CreatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                        UpdatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                        CreateDate = DateTime.Now,
                        RowPointer = Guid.NewGuid()
                    };

                    db.DIARIO.AddObject(iDiario1Deposito);


                    DIARIO iDiario2Deposito = new DIARIO
                    {
                        ASIENTO = mAsientoDeposito,
                        CONSECUTIVO = 2,
                        NIT = "ND",
                        CENTRO_COSTO = Const.CENTRO_COSTOS.OTROS_EGRESOS,
                        CUENTA_CONTABLE = Const.CUENTA_CONTABLE.CAJA_MONEDA_NACIONAL,
                        FUENTE = string.Format("T/C{0}", BoletaNo),
                        REFERENCIA = string.Format("Pago "+ (LoteDesembolsos.Financiera == 7 ? "INTERCONSUMO" : "CREDIPLUS") + " {0} ({1})", BoletaFecha, cantidadDesembolsos),
                        CREDITO_LOCAL = mMontoDeposito,
                        CREDITO_DOLAR = mMontoDepositoDolar,
                        NoteExistsFlag = 0,
                        RecordDate = DateTime.Now,
                        CreatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                        UpdatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                        CreateDate = DateTime.Now,
                        RowPointer = Guid.NewGuid()
                    };
                    db.DIARIO.AddObject(iDiario2Deposito);

                    CG_AUX iCG_AUX1Deposito = new CG_AUX
                    {
                        GUID_ORIGEN = mGuid.ToString(),
                        TABLA_ORIGEN = "MOV_BANCOS",
                        ASIENTO = mAsientoDeposito,
                        LINEA = 1,
                        COMENTARIO = "GUID - Origen",
                        NoteExistsFlag = 0,
                        RowPointer = Guid.NewGuid(),
                        RecordDate = DateTime.Now,
                        CreatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                        UpdatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                        CreateDate = DateTime.Now
                    };

                    db.CG_AUX.AddObject(iCG_AUX1Deposito);


                    CG_AUX iCG_AUX2deposito = new CG_AUX
                    {
                        GUID_ORIGEN = mGuid.ToString(),
                        TABLA_ORIGEN = "MOV_BANCOS",
                        ASIENTO = mAsientoDeposito,
                        LINEA = 2,
                        COMENTARIO = "GUID - Origen",
                        NoteExistsFlag = 0,
                        RowPointer = Guid.NewGuid(),
                        RecordDate = DateTime.Now,
                        CreatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                        UpdatedBy = string.Format("FA/{0}", LoteDesembolsos.Usuario),
                        CreateDate = DateTime.Now
                    };

                    db.CG_AUX.AddObject(iCG_AUX2deposito);

                    var qUpdateCuenta = from c in db.CUENTA_BANCARIA where c.CUENTA_BANCO == mCuentaBancaria select c;
                    foreach (var item in qUpdateCuenta)
                    {
                        item.SALDO = item.SALDO + mMontoDeposito;
                        item.SALDO_BANCOS = item.SALDO_BANCOS + mMontoDeposito;
                        item.POSICION_DE_CAJA = item.POSICION_DE_CAJA + mMontoDeposito;
                        item.FECHA_ULT_MOV = DateTime.Now;
                    };

                    var qUpdatePaquete = from p in db.PAQUETE where p.PAQUETE1 == Const.CONTABILIDAD.CONTROL_BANCARIO select p;
                    foreach (var item in qUpdatePaquete)
                    {
                        item.ULTIMO_ASIENTO = mAsientoDeposito;
                    }
                    db.SaveChanges();
                    transactionScope.Complete();
                }

                Respuesta.Exito=Const.OK;
                Respuesta.Mensaje = string.Format("Desembolso {0} cargado exitosamente a cuenta {1}",BoletaNo, mCuentaBancaria);
            }
            catch(Exception ex)
            {

                Respuesta.Mensaje = CatchClass.ExMessage(ex, MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name);
                log.Error(Respuesta.Mensaje);
            }

            return Respuesta;
        }

        private string ObtenerFecha(DateTime Fecha)
        {
            return string.Format("{0}{1}/{2}{3}/{4}", Fecha.Day < 10 ? "0" : "", Fecha.Day.ToString(), Fecha.Month < 10 ? "0" : "", Fecha.Month.ToString(), Fecha.Year.ToString());
        }

        public Respuesta ConsultaDesembolsos(DateTime dtFechaDesembolso,int nFinanciera)
        {
            Respuesta mRespuesta = new Respuesta();
            EXACTUSERPEntities db = new EXACTUSERPEntities();
			List<MF_Clases.Clases.Desembolso> lstDesembolsos = new List<MF_Clases.Clases.Desembolso>();
            DateTime dtManiana = dtFechaDesembolso.Date.AddDays(1);
            db.CommandTimeout = 999999999;
            try
            {
                
                        log.Info("Consultando desembolsos de interconsumo en bdd, de fecha: " + Utilitario.FormatoDDMMYYYY(dtFechaDesembolso));
                        var qDesembolsos = from fa in db.FACTURA
                                           join ds in db.MF_Factura_Documentos_Soporte on fa.FACTURA1 equals ds.FACTURA
                                           join ped in db.MF_Pedido on fa.PEDIDO equals ped.PEDIDO
                                           join ve in db.MF_Vendedor on fa.VENDEDOR equals ve.VENDEDOR
                                           join co in db.MF_Cobrador on fa.COBRADOR equals co.COBRADOR
                                           join fe in db.MF_Factura_Estado on ds.FACTURA equals fe.FACTURA
                                           join cli in db.MF_Cliente on ped.CLIENTE equals cli.CLIENTE
                                           where
                                            db.MF_Factura_Estado.Any(fe => fe.FACTURA == fa.FACTURA1 && fe.ESTADO == "D")
                                             && fe.FECHA >= dtFechaDesembolso.Date && fe.FECHA < dtManiana.Date
                                             && (ped.FINANCIERA == nFinanciera || nFinanciera == -1)
                                            && fe.ESTADO == "D"

                                           select new
                                           {
                                               FECHA = fe.FECHA,
                                               PRESTAMO = ped.PRESTAMO == null ? ped.NUMERO_CREDITO : ped.PRESTAMO,
                                               SOLICITUD = ped.SOLICITUD,
                                               CLIENTE = fa.CLIENTE,
                                               NOMBRE_CLIENTE = cli.PRIMER_APELLIDO + " " + cli.SEGUNDO_APELLIDO + ", " + cli.PRIMER_NOMBRE + " " + cli.SEGUNDO_NOMBRE,
                                               VALOR = ped.MONTO_DESEMBOLSADO == null ? (fa.TOTAL_FACTURA) : ped.MONTO_DESEMBOLSADO,
                                               FACTURA = fa.FACTURA1,
                                               CLIENTEMF = ped.CLIENTE,
                                               MONTOMF = ped.TOTAL_FACTURAR,
                                               ENGANCHE = ped.ENGANCHE,
                                               EMAILVENDEDOR = ve.EMAIL,
                                               COBRADOR = fa.COBRADOR,
                                               VENDEDOR = fa.VENDEDOR,
                                               EMAILJEFETIENDA = co.JEFE,
                                               EMAILSUPERVISOR = co.SUPERVISOR,
                                               CRITERIO = "-" //Se le agrega un guión para que en el FrontEnd no lo marque como un error (forecolor red)

                                           };
                        ///Se verifica que hayan desembolsos en la estructura, y se almacenan en una lista de desembolso
                        if (qDesembolsos != null)
                            if (qDesembolsos.Any())
                            {
                                foreach (var item in qDesembolsos)
                                    lstDesembolsos.Add(
                                    new Clases.Desembolso
                                    {
                                        Fecha = item.FECHA,
                                        Prestamo = item.PRESTAMO,
                                        Solicitud = item.SOLICITUD.ToString(),
                                        Cliente = item.CLIENTE,
                                        NombreCliente = item.NOMBRE_CLIENTE,
                                        Valor = item.VALOR.Value,
                                        Factura = item.FACTURA,
                                        ValorMF = item.MONTOMF.Value,
                                        FacturaMF = item.FACTURA,
                                        ClienteMF = item.CLIENTEMF,
                                        EngancheMF = item.ENGANCHE,
                                        Criterio = item.CRITERIO,
                                        Vendedor = item.VENDEDOR,
                                        Cobrador = item.COBRADOR,
                                        EmailJefeTienda = item.EMAILJEFETIENDA,
                                        EmailSupervisor = item.EMAILSUPERVISOR,
                                        EmailVendedor = item.EMAILVENDEDOR
                                    }
                                    );
                            }
                        mRespuesta.Objeto = lstDesembolsos;
                        mRespuesta.Exito = true;
                        mRespuesta.Mensaje = lstDesembolsos.Count() + " Desembolsos devueltos.";
                        log.Info(mRespuesta.Mensaje);
                    }
				
            catch (Exception ex)
            {
                log.Error("Consultar desembolsos en bdd "+this.GetType().Name + " " + ex.StackTrace);
				mRespuesta.Exito = false;
            }
            return mRespuesta;
        }

        

       
        
    }
}