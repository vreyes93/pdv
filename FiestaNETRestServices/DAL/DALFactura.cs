﻿using FiestaNETRestServices.Content.Abstract;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Web;
using static MF_Clases.Clases;

namespace FiestaNETRestServices.DAL
{
    public class DALFactura : MFDAL
    {
        public List<Clases.DocumentoConSaldo> FacturasConSaldo(string customerId)
        {
            List<Clases.DocumentoConSaldo> Facturas = new List<DocumentoConSaldo>();
            try
            {
                var qFacturaRecibos = (from fr in dbExactus.MF_FacturaRecibo
                                       join rec in dbExactus.MF_Recibo.Where(x => x.CLIENTE == customerId) on fr.RECIBO equals rec.DOCUMENTO
                                       select new { MONTO = rec.MONTO, Factura = fr.FACTURA, RECIBO = fr.RECIBO });
                var qFactura =

                    (from f in dbExactus.DOCUMENTOS_CC
                     join fac in dbExactus.MF_Factura on f.DOCUMENTO equals fac.FACTURA
                     where f.TIPO == "FAC" && f.SALDO > 0
                     && fac.SE_ANULARA == "N" &&
                     f.ANULADO != "S" && f.CLIENTE == customerId
                     && dbExactus.MF_FacturaRecibo.Any(x => x.FACTURA.Equals(f.DOCUMENTO)) == false
                     select new DocumentoConSaldo
                     {
                         Documento = f.DOCUMENTO,
                         Saldo = f.MONTO
                     }).Concat
                    (
                         dbExactus.FACTURA.Where(x => x.CLIENTE == customerId && x.ANULADA !="S" && EntityFunctions.DiffDays(x.FECHA, DateTime.Now) < 360 && x.TOTAL_FACTURA > (qFacturaRecibos.Any(s => s.Factura == x.FACTURA1) ? qFacturaRecibos.Where(z => z.Factura == x.FACTURA1).Sum(f => f.MONTO) : 0))
                        .Select(d => new DocumentoConSaldo
                        {
                            Documento = d.FACTURA1,
                            Saldo = d.TOTAL_FACTURA - (qFacturaRecibos.Any(x => x.Factura == d.FACTURA1) ?
                                                dbExactus.MF_FacturaRecibo.Where(y => y.FACTURA == d.FACTURA1).Join(dbExactus.MF_Recibo, y => y.RECIBO, r => r.DOCUMENTO, (f, r) => new { f, r }).Sum(y => y.r.MONTO)
                                                : 0)
                        })
                    ).ToList();
                if (qFactura.Count() > 0)
                {
                    qFactura.ForEach(x =>
                    {
                        x.Descripcion = string.Format("Fact. {0} con saldo de Q.{1:#.00}", x.Documento, x.Saldo);
                       if(!Facturas.Any(r => r.Documento == x.Documento))
                            Facturas.Add(x);
                    }
                    );
                }
               
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;

            }
            return Facturas;
        }





    }
}