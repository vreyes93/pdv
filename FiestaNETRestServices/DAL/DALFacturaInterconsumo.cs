﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using FiestaNETRestServices.Models;
using FiestaNETRestServices.Utils;
using MF_Clases;
using Utilitarios = FiestaNETRestServices.Utils.Utilitarios;

namespace FiestaNETRestServices.DAL
{
    public class DALFacturaInterconsumo
    {
       
        /// <summary>
        /// Obtener los expedientes con envio de papelería pendiente
        /// </summary>
        /// <param name="AnioInicio"></param>
        /// <param name="MesInicio"></param>
        /// <param name="DiaInicio"></param>
        /// <param name="AnioFinal"></param>
        /// <param name="MesFinal"></param>
        /// <param name="DiaFinal"></param>
        /// <param name="Tienda"></param>
        /// <param name="nFinanciera"></param>
        /// <returns></returns>
        public Clases.LoteFacturasFinanciera getExpedientesPendientesEnvio(string AnioInicio, string MesInicio, string DiaInicio, string AnioFinal, string MesFinal, string DiaFinal, string Tienda, int nFinanciera)
        {
            EXACTUSERPEntities dbExactus = new EXACTUSERPEntities();
            dbExactus.CommandTimeout = 999999999;

            DateTime mFechaInicial;
            DateTime mFechaFinal;
            Clases.LoteFacturasFinanciera mLoteFacturasFinanciera = new Clases.LoteFacturasFinanciera();
            mLoteFacturasFinanciera.exito = false;
            Utilitarios Utilitario = new Utilitarios();
            try
            {


                if (!Utilitario.FechaValida(AnioInicio, MesInicio, DiaInicio))
                {
                    mLoteFacturasFinanciera.mensaje = "La fecha inicial es inválida.";
                    return mLoteFacturasFinanciera;
                }

                if (!Utilitario.FechaValida(AnioFinal, MesFinal, DiaFinal))
                {
                    mLoteFacturasFinanciera.mensaje = "La fecha final es inválida.";
                    return mLoteFacturasFinanciera;
                }



                mFechaInicial = Utilitario.ConvertirFecha(AnioInicio, MesInicio, DiaInicio);
                mFechaFinal = Utilitario.ConvertirFecha(AnioFinal, MesFinal, DiaFinal);


                //TODO: Pasar esto a la capa DALFacturaInterconsumo
                //  var qFacturasInterconsumo = GetFacturasInterconsumo(mFechaInicial,mFechaFinal,Tienda);
                var qFacturasF = from f in dbExactus.FACTURA
                                 join mfp in dbExactus.MF_Pedido
                                         on f.PEDIDO equals mfp.PEDIDO
                                 where f.FECHA >= mFechaInicial
                                         && f.FECHA <= mFechaFinal
                                         && mfp.FINANCIERA == nFinanciera
                                         && f.ANULADA == "N"
                                         && !dbExactus.MF_Factura_Estado.Any(fe => fe.FACTURA == f.FACTURA1
                                            && (fe.ESTADO == Const.Expediente.ENVIADO_FINANCIERA
                                                 || fe.ESTADO == Const.Expediente.ANULADO)
                                            )
                                 orderby f.COBRADOR, f.FACTURA1
                                 //select f;
                                 select new
                                 {
                                     FACTURA1 = f.FACTURA1
                                             ,
                                     FECHA = f.FECHA
                                             ,
                                     CLIENTE = f.CLIENTE
                                             ,
                                     NOMBRE_CLIENTE = f.NOMBRE_CLIENTE
                                             ,
                                     FECHA_FACTURA = f.FECHA
                                             ,
                                     TOTAL_FACTURA = f.TOTAL_FACTURA
                                             ,
                                     FECHA_ENTREGA = f.FECHA_ENTREGA
                                             ,
                                     VENDEDOR = f.VENDEDOR
                                             ,
                                     COBRADOR = f.COBRADOR
                                             ,
                                     NIVEL_PRECIO = f.NIVEL_PRECIO
                                             ,
                                     SOLICITUD = mfp.SOLICITUD
                                 };


                if (!String.IsNullOrEmpty(Tienda)
                        && Tienda.Trim().Length > 0)
                    qFacturasF = qFacturasF.Where(x => x.COBRADOR.Equals(Tienda));




                foreach (var item in qFacturasF)
                {
                    Clases.FacturasFinanciera mFactura = new Clases.FacturasFinanciera();
                    mFactura.FechaFactura = item.FECHA;
                    mFactura.Tienda = item.COBRADOR;
                    mFactura.NoFactura = item.FACTURA1;
                    mFactura.Valor = item.TOTAL_FACTURA;
                    mFactura.FechaEntrega = item.FECHA_ENTREGA;
                    mFactura.Vendedor = item.VENDEDOR;
                    mFactura.Tienda = item.COBRADOR;
                    mFactura.NivelPrecio = item.NIVEL_PRECIO;
                    mFactura.Cliente = item.CLIENTE;
                    mFactura.NombreCliente = item.NOMBRE_CLIENTE;
                    mFactura.Pagare = "N";
                    mFactura.Financiera = nFinanciera;
                    mFactura.ConstanciaIngresos = "N";
                    mFactura.Solicitud = item.SOLICITUD.ToString();
                    mLoteFacturasFinanciera.Facturas.Add(mFactura);

                }

                mLoteFacturasFinanciera.exito = true;
                mLoteFacturasFinanciera.mensaje = "Exito";
            }
            catch (Exception ex)
            {
                //mLoteFacturasInterconsumo.exito = false;
                mLoteFacturasFinanciera.mensaje = CatchClass.ExMessage(ex, MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name);
            }
            return mLoteFacturasFinanciera;
        }
    }
}