﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Controllers.Tools;
using FiestaNETRestServices.Models;
using FiestaNETRestServices.Utils;
using Humanizer;
using MF_Clases;
using MF_Clases.GuateFacturas.FEL;
using MF_Clases.GuateFacturas.FEL.ImpresionFEL;
using MF_Clases.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using System.Web.Script.Serialization;
using WebGrease.Css.Extensions;
using static MF_Clases.Utils.CONSTANTES;
using static MF_Clases.Utils.CONSTANTES.GUATEFAC;

namespace FiestaNETRestServices.DAL
{
    public class DALFel : MFDAL
    {
        #region "Contingencia"
        /// <summary>
        /// Método para validar si cada tienda tiene números de acceso disponibles en el caso que no haya conexión con el GFase de
        /// la factura electrónica.
        /// </summary>
        /// <returns></returns>
        public async Task<Respuesta> ValidarLotesContingencia()
        {
            Respuesta mRespuesta = new Respuesta();
            Respuesta mRespuestaGral = new Respuesta();
            //
            //Obtener las tiendas
            //
            try
            {
                //**
                //Verificar que esté habilitado el módulo de FEL
                //**
                if (Utilitario.ModuloDelSistemaHabilitado(CONSTANTES.CATALOGOS.MODULO_FEL))
                {
                    var qCobrador = (from c in dbExactus.MF_Cobrador where c.ACTIVO.Equals("S") select c);
                    var qContingencia = (from g in dbExactus.MF_ContingenciaFEL where g.Usado.Equals(false) group g by g.Establecimiento into r
                                         select new {
                                             Establecimiento = r.Key,
                                             Libres = r.Count()
                                         });
                    var qContingenciaLote = (from g in dbExactus.MF_ContingenciaFEL orderby g.IdContingencia
                                             group g by g.Establecimiento into r
                                             select new
                                             {
                                                 Establecimiento = r.Key,
                                                 LoteUltimo = r.Max(x => x.Lote)
                                             });

                    if (qContingencia != null)
                    {
                        if (qContingencia.ToList().Count > 0 && qContingenciaLote.ToList().Count > 0)
                        {


                            if (qCobrador.Count() > 0)

                            {

                                foreach (var item in qCobrador)
                                {
                                    //nCantidad corresponde a la cantidad de números de acceso utilizados y que deben ser solicitados nuevamente
                                    int nCantidad = 0;
                                    try
                                    {
                                        nCantidad = qContingencia.Where(c => c.Establecimiento == item.ESTABLECIMIENTO).FirstOrDefault().Libres;
                                    }
                                    catch
                                    { }
                                    //var nCantidad = (from c in qContingencia where c.Cobrador.Equals(item.ESTABLECIMIENTO) select c).FirstOrDefault().Libres;
                                    string UltimoLote = string.Empty;
                                    try
                                    {
                                        UltimoLote = (from L in qContingenciaLote where L.Establecimiento == item.ESTABLECIMIENTO select L).FirstOrDefault().LoteUltimo;
                                    }
                                    catch
                                    { }
                                    //El formato del Lote debe ser único para cada tienda, este es YYYYMMDDEE donde YYYY= año, MM= Mes, DD= día, EE=Código de tienda sin F
                                    string mLoteTienda = item.COBRADOR + DateTime.Now.Year.ToString("0000") + DateTime.Now.Month.ToString("00") + (DateTime.Now.Day).ToString("00");

                                    if (mLoteTienda != UltimoLote)
                                    {

                                        //verificamos que exista la necesidad de pedir más números de contingencia al GFace para cada Establecimiento
                                        if (CONSTANTES.CONTINGENCIA.TAMANIO_LOTE_SOLICITUD - nCantidad > 0)
                                        {

                                            UtilitariosGuateFacturas utilitarios = new UtilitariosGuateFacturas();
                                            MF_Clases.GuateFacturas.FEL.ContingenciaSolicitud solicitud =
                                                                                                        new MF_Clases.GuateFacturas.FEL.ContingenciaSolicitud
                                                                                                        {
                                                                                                            Establecimiento = item.ESTABLECIMIENTO ?? 1,
                                                                                                            Cantidad = CONTINGENCIA.TAMANIO_LOTE_SOLICITUD - nCantidad,
                                                                                                            Lote = mLoteTienda,
                                                                                                            NitEmisor = EMISOR.NIT
                                                                                                        };
                                            //---------------------------------------------------------------
                                            //Procedemos a consumir el servicio para obtener los números de contingencia "para cada tienda"
                                            //---------------------------------------------------------------
                                            log.Debug(string.Format("Solicitando lote de contingencia para la tienda {0}, parámetros del servicio: Establecimiento:{1}, Cantidad: {2}, Lote: {3}, NitEmisor: {4}", item.COBRADOR, item.ESTABLECIMIENTO, CONSTANTES.CONTINGENCIA.TAMANIO_LOTE_SOLICITUD - nCantidad, mLoteTienda, EMISOR.NIT));
                                            mRespuesta = await utilitarios.SolicitudLoteContingencia(solicitud);


                                            if (mRespuesta.Exito)
                                            {
                                                var Objeto = ((Respuesta)mRespuesta.Objeto);
                                                ContingenciaRespuesta LoteAcceso = ((ContingenciaRespuesta)Objeto.Objeto);
                                                //---------------------------------------
                                                //Procedemos a guardar el lote en la base de datos
                                                //---------------------------------------
                                                GuardarLoteContingencia(LoteAcceso.LstAccesos, solicitud);

                                                //respuesta
                                                mRespuestaGral.Mensaje += string.Format("Lote de contingencia {0} para la tienda {1} guardado con éxito <br/>", mLoteTienda, item.COBRADOR);
                                            }
                                            else
                                            {
                                                mRespuestaGral.Mensaje += string.Format("El servicio de GuateFacturas no devolvio contingencias para la tienda {0}, Numeros de Acceso libres {1} <br/>", item.COBRADOR, nCantidad);

                                            }

                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (qCobrador.Count() > 0)
                            {
                                foreach (var item in qCobrador)
                                {

                                    string mLoteTienda = item.COBRADOR + DateTime.Now.Year.ToString("0000") + DateTime.Now.Month.ToString("00") + (DateTime.Now.Day).ToString("00");
                                    //El formato del Lote debe ser único para cada tienda, este es YYYYMMDDEE donde YYYY= año, MM= Mes, DD= día, EE=Código de tienda sin F
                                    UtilitariosGuateFacturas utilitarios = new UtilitariosGuateFacturas();
                                    MF_Clases.GuateFacturas.FEL.ContingenciaSolicitud solicitud =
                                                                        new MF_Clases.GuateFacturas.FEL.ContingenciaSolicitud
                                                                        {
                                                                            Establecimiento = item.ESTABLECIMIENTO ?? 1,
                                                                            Cantidad = CONSTANTES.CONTINGENCIA.TAMANIO_LOTE_SOLICITUD,
                                                                            Lote = mLoteTienda,
                                                                            NitEmisor = CONSTANTES.GUATEFAC.EMISOR.NIT
                                                                        };

                                    //---------------------------------------------------------------------        
                                    //Procedemos a consumir el servicio para obtener los números de contingencia "para cada tienda"
                                    mRespuesta = await utilitarios.SolicitudLoteContingencia(solicitud);
                                    //-----------------------------------------------------------------------


                                    if (mRespuesta.Exito)
                                    {
                                        var Objeto = ((Respuesta)mRespuesta.Objeto);
                                        ContingenciaRespuesta LoteAcceso = ((ContingenciaRespuesta)Objeto.Objeto);
                                        List<string> LstNumAcceso = new List<string>();
                                        foreach (var acceso in LoteAcceso.LstAccesos)
                                            LstNumAcceso.Add(acceso);

                                        //---------------------------------------
                                        //Procedemos a guardar el lote en la base de datos
                                        //---------------------------------------
                                        GuardarLoteContingencia(LstNumAcceso, solicitud);
                                        //respuesta
                                        mRespuestaGral.Mensaje += string.Format("Lote de contingencia {0} para la tienda {1} guardado con éxito <br/>", mLoteTienda, item.COBRADOR);
                                    }
                                    else
                                    {
                                        mRespuestaGral.Mensaje += string.Format("El servicio de GuateFacturas no devolvio contingencias para la tienda {0}, números de acceso libres {1} <br/>", item.COBRADOR, 0);
                                        log.Error("Respuesta de GuateFacturas para la solicitud de lote de contingencia para la tienda " + item.COBRADOR + " " + mRespuesta.Mensaje);
                                    }
                                }
                            }
                        }
                        mRespuestaGral.Exito = true;
                    }
                }
            } catch (Exception ex)
            {
                log.Error("Ocurrió un problema al validar los lotes de contingencia " + ex.Message);
                mRespuestaGral.Exito = false;
                mRespuestaGral.Mensaje = CatchClass.ExMessage(ex, "DALFel", "ValidarLotesContingencia()");
            }
            return mRespuestaGral;
        }
        /// <summary>
        /// Guardar en la base de datos los lotes de contingencia para cada tienda
        /// </summary>
        /// <param name="lstNumAcceso"></param>
        /// <param name="cgia"></param>
        private bool GuardarLoteContingencia(List<string> lstNumAcceso, MF_Clases.GuateFacturas.FEL.ContingenciaSolicitud cgia)
        {
            try
            {
                EXACTUSERPEntities db = new EXACTUSERPEntities();

                using (TransactionScope transactionScope = new TransactionScope())

                {
                    foreach (string NumAcceso in lstNumAcceso)
                    {

                        MF_ContingenciaFEL contingenciaFEL = new MF_ContingenciaFEL
                        {
                            Establecimiento = cgia.Establecimiento,
                            Lote = cgia.Lote,
                            ValorDeAcceso = NumAcceso,
                            Usado = false

                        };

                        db.MF_ContingenciaFEL.AddObject(contingenciaFEL);
                    }

                    db.SaveChanges();
                    transactionScope.Complete();
                    return true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Ocurrió un problema al guardar el lote de la contingencia recibido, para la tienda: " + cgia.Establecimiento + ", Error: " + ex.Message);
                return false;
            }

        }

        /// <summary>
        /// Servicio que devuelve un número de acceso como contingencia en el caso de falta de comunicación entre la tienda y el GFace
        /// Este número de contingencia o de acceso es marcado como utilizado en la tabla mf_ContingenciaFEL
        /// En Respuesta.objeto se devuelve el valor del número de acceso válido
        /// </summary>
        /// <param name="Cobrador"></param>
        /// <param name="dtFechaInicioContingencia"></param>
        /// <returns>Respuesta</returns>
        public Respuesta UtilizarNumeroContingenciaTienda(string cobrador, DateTime dtFechaInicioContingencia)
        {
            Respuesta mRespuesta = new Respuesta();
            try
            {
                var nEstablecimiento = (from n in dbExactus.MF_Cobrador where n.COBRADOR.Equals(cobrador) select n).FirstOrDefault().ESTABLECIMIENTO;
                //Obteniendo el número de contingencia para la tienda
                var qContingencia = (from c in dbExactus.MF_ContingenciaFEL where c.Establecimiento == nEstablecimiento && c.Usado == false orderby c.IdContingencia ascending select c).FirstOrDefault();
                if (qContingencia != null)
                {
                    mRespuesta.Objeto = qContingencia.ValorDeAcceso;
                    mRespuesta.Exito = true;

                    //Marcar este número como usado y colocar la fecha de inicio de contingencia

                    var qContingenciaUsado = (from c in dbExactus.MF_ContingenciaFEL where c.Establecimiento == nEstablecimiento && c.ValorDeAcceso.Equals(qContingencia.ValorDeAcceso) select c);
                    foreach (var item in qContingenciaUsado)
                    {
                        item.Usado = true;
                        item.FechaContingenciaIni = dtFechaInicioContingencia;
                    }
                    dbExactus.SaveChanges();
                }
                else
                {
                    mRespuesta.Exito = false;
                    mRespuesta.Mensaje = "No quedan números de acceso disponibles para la tienda " + cobrador;
                }
            } catch (Exception ex)
            {
                mRespuesta.Exito = false;
                mRespuesta.Mensaje = "(DAL) Error al marcar un número de acceso como utilizado para la tienda " + cobrador + " Error: " + ex.Message;
                log.Error(mRespuesta.Mensaje);
            }
            return mRespuesta;
        }

        public Respuesta ObtenerNumerosDeContingenciaParaNotificar()
        {
            List<string> lstCNotificar = new List<string>();
            Respuesta mRespuesta = new Respuesta();
            try
            {

                log.Debug("Obteniendo los números de contingencia para notificar.");
                var qNotificarContingencia = (from cn in dbExactus.MF_ContingenciaFEL join cob in dbExactus.MF_Cobrador on cn.Establecimiento equals cob.ESTABLECIMIENTO where !cn.Notificado.HasValue && cn.NumFactura !=null && cob.ACTIVO == "S" && cn.Usado == true select new
                {
                    TIENDA = cob.COBRADOR,
                    ESTABLECIMIENTO = cob.ESTABLECIMIENTO,
                    VALORACCESO = cn.ValorDeAcceso,
                    LOTE = cn.Lote,
                    FECHAINI = cn.FechaContingenciaIni,
                    FECHAFIN = cn.FechaContingenciaFin,
                    NOTIFICADO = cn.Notificado,
                    FACTURA_ELECTRONICA = cn.NumFactura
                });
                if (qNotificarContingencia.Count() > 0)
                {



                    #region "formar cuerpo de la notificación"
                    string mCuerpoCorreo = string.Empty;
                    StringBuilder mBody = new StringBuilder();
                    //--
                    //ARMAMOS EL CUERPO DEL CORREO
                    //--
                    mCuerpoCorreo = Utilitario.CorreoInicioFormatoAzulYGris();
                    mCuerpoCorreo += "Estimados: <br/><br/> Por este medio se les notifica sobre los documentos que fueron firmados con números de acceso o contingencia, (en total " + qNotificarContingencia.Count() + ") por favor, darles el trámite que corresponde.<br/>";
                    mCuerpoCorreo += "<br/>";
                    mCuerpoCorreo += "<table align='center' cellpadding='10'><tr>" + Utilitario.FormatoTituloCentro("Tienda") + Utilitario.FormatoTituloCentro("Establecimiento") + Utilitario.FormatoTituloCentro("Valor de Acceso") + Utilitario.FormatoTituloCentro("Documento Electrónico") + Utilitario.FormatoTituloCentro("Lote") + Utilitario.FormatoTituloCentro("Fecha inicial contingencia") + Utilitario.FormatoTituloCentro("Fecha final contingencia") + "</tr>";
                    foreach (var item in qNotificarContingencia)
                    {
                        try
                        {
                            mCuerpoCorreo += "<tr>" + Utilitario.FormatoCuerpoCentro(item.TIENDA) + Utilitario.FormatoCuerpoCentro(item.ESTABLECIMIENTO.ToString()) + Utilitario.FormatoCuerpoCentro(item.VALORACCESO) + Utilitario.FormatoCuerpoCentro(item.FACTURA_ELECTRONICA) + Utilitario.FormatoCuerpoCentro(item.LOTE) + Utilitario.FormatoCuerpoCentro(item.FECHAINI.Value.ToString("dd/MM/yyyy HH:mm:ss")) + Utilitario.FormatoCuerpoCentro(item.FECHAFIN.Value.ToString("dd/MM/yyyy HH:mm:ss")) + "</tr>";
                            lstCNotificar.Add(item.FACTURA_ELECTRONICA);
                        }
                        catch (Exception ex)
                        {
                            log.Error("No se pudo agregar a la lista de notificación el documento " + item.VALORACCESO + " " + ex.Message);
                        }
                    }
                    mCuerpoCorreo += "</table><br/><div>Saludos,</div>" + Utilitario.CorreoFin();
                    mCuerpoCorreo = mCuerpoCorreo.Replace("#428BCA", "#F44336");
                    mBody.Append(mCuerpoCorreo);
                    #endregion
                    #region "Enviar Notificación"
                    List<string> mDestinatarios = new List<string>();
                    string mCuentaCorreo = string.Empty;
                    var qDestinatarios = (from q in dbExactus.MF_Catalogo where q.CODIGO_TABLA == CONSTANTES.CATALOGOS.GUATEFACTURAS_FEL && q.CODIGO_CAT.Equals("DESTINATIARIOS NUMEROS DE CONTINGENCIA UTILIZADOS") select q);
                    var qCuentaCorreo = (from q in dbExactus.MF_Catalogo where q.CODIGO_TABLA == CONSTANTES.CATALOGOS.GUATEFACTURAS_FEL && q.CODIGO_CAT.Equals("REMITENTE CORREO") select q);
                    if (qDestinatarios.Count() > 0)
                    {
                        string[] mDet = qDestinatarios.FirstOrDefault().NOMBRE_CAT.ToString().Split('|');
                        foreach (string item in mDet)
                            mDestinatarios.Add(item);
                    }
                    if (qCuentaCorreo.Count() > 0)
                        mCuentaCorreo = qCuentaCorreo.FirstOrDefault().NOMBRE_CAT;

                    var resEmail = Utilitario.EnviarEmail(mCuentaCorreo, mDestinatarios, "NOTIFICACIÓN DE DOCUMENTOS CON NÚMEROS EN CONTINGENCIA", mBody, "MUEBLES FIESTA -NOTIFICACIONES-");
                    if (resEmail.Exito)
                    {
                        //------
                        //Actualizamos en la tabla el valor de Notificado a verdadero
                        //-----
                        var qActualizarContingencia = (from cn in dbExactus.MF_ContingenciaFEL
                                                       join cob in dbExactus.MF_Cobrador on cn.Establecimiento equals cob.ESTABLECIMIENTO
                                                       where !cn.Notificado.HasValue && cob.ACTIVO == "S" && cn.Usado == true
                                                       select cn);
                        if (qActualizarContingencia.Count() > 0)
                        {
                            foreach (var notificado in qActualizarContingencia)
                                notificado.Notificado = true;
                        }
                        dbExactus.SaveChanges();
                    }

                    #endregion

                }
                mRespuesta.Exito = true;
                mRespuesta.Objeto = lstCNotificar;
            }
            catch (Exception ex)
            {
                mRespuesta.Exito = false;
                mRespuesta.Mensaje = CatchClass.ExMessage(ex, "DALFel", "ObtenerNumerosDeContingenciaParaNotificar()");
                log.Error(CatchClass.ExMessage(ex, "DALFel", "ObtenerNumerosDeContingenciaParaNotificar()"));
            }
            return mRespuesta;
        }
        #endregion

        #region "NOTAS DE CREDITO, DEBITO Y DOCUMENTOS FIRMADOS CON CONTINGENCIA"

        /// <summary>
        /// Servicio para obtener notas de crédito/débito para enviar a firmar electrónicamente
        /// </summary>
        /// <returns></returns>
        public Respuesta ObtenerOtrosDocumentosSinFirmar()
        {
            List<Doc> lstDocumentos = new List<Doc>();
            log.Debug("Obteniendo las notas de crédito y débito para firmar electrónicamente");
            Respuesta mRespuesta = new Respuesta();
            mRespuesta.Exito = true;
            string _DaSerie = string.Empty;
            string _DaPreimpreso = string.Empty;
           
            try
            {
                decimal nCantidad = 1;
                var qConsultaEmisor = (from mf_g in dbExactus.MF_GFaceConfigura select mf_g);
                //catálogo para parametrizar el envío de correo a facturas de fiesta o mayoreo
                var qConsultaEnviarEmailF = (from cs in dbExactus.MF_Catalogo where cs.CODIGO_TABLA == 11 && cs.CODIGO_CAT == "ENVIAR_CORREO_FEL_FIESTA" select cs).FirstOrDefault();
                var qConsultaEnviarEmailM = (from cs in dbExactus.MF_Catalogo where cs.CODIGO_TABLA == 11 && cs.CODIGO_CAT == "ENVIAR_CORREO_FEL_MAYOREO" select cs).FirstOrDefault();
                var blnEnviarFiesta = qConsultaEnviarEmailF.NOMBRE_CAT.Equals("S");
                var blnEnviarMayoreo = qConsultaEnviarEmailM.NOMBRE_CAT.Equals("S");
                //----


                var qConsultaTests = (from cs in dbExactus.MF_Catalogo where cs.CODIGO_TABLA == CONSTANTES.CATALOGOS.GUATEFACTURAS_FEL && cs.CODIGO_CAT == "PRODUCCION_FEL" select cs).FirstOrDefault();
                bool blTests = qConsultaTests.NOMBRE_CAT.Equals("N");

                
                var qConsulta =
                        (from fa in dbExactus.FACTURA
                         join cli in dbExactus.CLIENTE on fa.CLIENTE equals cli.CLIENTE1
                         join mfCli in dbExactus.MF_Cliente on fa.CLIENTE equals mfCli.CLIENTE
                         
                         where
                          //que correspondan a partir de una fecha determinada
                          EntityFunctions.DiffDays(CONSTANTES.GUATEFAC.MODALIDAD_FEL.FECHA_INICIO_MODALIDAD_FEL, fa.FECHA) >= 0
                          //que no pasen de un día atrás
                          && ((EntityFunctions.DiffDays(EntityFunctions.AddDays(DateTime.Today, -5), fa.FECHA) >= 0 && blTests == false) || (blTests))
                          && fa.TIPO_DOCUMENTO != "F" && fa.ANULADA != "S"
                          && (from gf in dbExactus.MF_GFace where gf.DOCUMENTO == fa.FACTURA1 && gf.FIRMADO.Equals("S") select gf).Any() == false

                         select new
                         {
                             CLIENTE = cli.CLIENTE1,
                             EMAIL = cli.E_MAIL,
                             NIT = mfCli.NIT_FACTURA.Equals(string.Empty) ? cli.CONTRIBUYENTE : mfCli.NIT_FACTURA,
                             NOMBRE = cli.NOMBRE,
                             DIRECCION = (mfCli.CALLE_AVENIDA_FACTURA.Trim() + " " + mfCli.CASA_FACTURA.Trim() + " " + mfCli.ZONA_FACTURA.Trim() + " " + mfCli.APARTAMENTO_FACTURA.Trim() + " " + mfCli.COLONIA_FACTURA.Trim() + " " + mfCli.MUNICIPIO_FACTURA.Trim() + " " + mfCli.DEPARTAMENTO_FACTURA.Trim()),
                             TIPOVTA = (fa.SUBTIPO_DOC_CXC),//CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES,//
                             DESTINO = CONSTANTES.GUATEFAC.DESTINO_VENTA.GT,
                             MONEDA = fa.MONEDA.Equals("D") ? CONSTANTES.GUATEFAC.MONEDA.DOL : CONSTANTES.GUATEFAC.MONEDA.GTQ,//(CASE a.MONEDA_FACTURA WHEN 'D' THEN 'USD' ELSE 'GTQ' END)
                             TASACAMBIO = fa.MONEDA_FACTURA.Equals("D") ? fa.TIPO_CAMBIO : CONSTANTES.GUATEFAC.TASA.GTQ,//(CASE a.MONEDA_FACTURA WHEN 'D' THEN a.TIPO_CAMBIO ELSE 1 END) AS TasaCambio
                             TIENDA = fa.COBRADOR,
                             REFERENCIA = 0,
                             DOCUMENTO = fa.FACTURA1,
                             TIPO = (fa.TIPO_DOCUMENTO.Equals("D") && EntityFunctions.DiffDays((from xfa in dbExactus.FACTURA where xfa.FACTURA1.Equals(fa.FACTURA_ORIGINAL) select xfa).FirstOrDefault().FECHA, fa.FECHA) > 61 ? CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_ABONO : CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_CREDITO),
                             fa.FECHA,
                             BRUTO = fa.TOTAL_FACTURA,
                             DESCUENTO = 0,
                             NETO = fa.TOTAL_MERCADERIA,
                             IVA = (fa.TOTAL_IMPUESTO1),
                             TOTAL = fa.TOTAL_FACTURA,
                             EXENTO=fa.MONEDA.Equals("D") ? fa.TOTAL_FACTURA: 0,
                             //si el tipo de documento es una devolución, se puede obtener la factura que afectará la nota de crédito en el campo FACTURA_ORIGINAL
                             DASERIE = fa.TIPO_DOCUMENTO.Equals("D") ? fa.FACTURA_ORIGINAL : "",
                             PREIMPRESO = fa.TIPO_DOCUMENTO.Equals("D") ? fa.FACTURA_ORIGINAL : ""

                         }
                                 )
                                 .Concat
                                 (
                                    from cc in dbExactus.DOCUMENTOS_CC

                                    join cli in dbExactus.CLIENTE on cc.CLIENTE equals cli.CLIENTE1
                                    join mfCli in dbExactus.MF_Cliente on cli.CLIENTE1 equals mfCli.CLIENTE
                                    
                                    where
                                    //BUSCAMOS A PARTIR DE LA FECHA DE IMPLEMENTACIÓN
                                    EntityFunctions.DiffDays(CONSTANTES.GUATEFAC.MODALIDAD_FEL.FECHA_INICIO_MODALIDAD_FEL, cc.FECHA) > 0
                                     && ((EntityFunctions.DiffDays(EntityFunctions.AddDays(DateTime.Today, -5), cc.FECHA) >= 0 && blTests == false) || (blTests))
                                    && (cc.TIPO.Contains("N/C") || cc.TIPO.Contains("N/D"))
                                    //SOLO LOS DOCUMENTOS ACTIVOS, LAS ANULACIONES SE HACEN EN OTRO SERVICIO
                                    && cc.PAQUETE.Equals("CC") && cc.ANULADO != "S"
                                    // QUE NO ESTEN FIRMADOS EN LA TABLA MF_GFACE
                                    && (from gf in dbExactus.MF_GFace where gf.DOCUMENTO == cc.DOCUMENTO && gf.FIRMADO.Equals("S") select gf).Any() == false


                                    select new
                                    {
                                        //PARA VALIDACIONES DE FIESTA/MAYOREO
                                        CLIENTE = cli.CLIENTE1,
                                        EMAIL = cli.E_MAIL,
                                        NIT = mfCli.NIT_FACTURA.Equals(string.Empty) ? cli.CONTRIBUYENTE : mfCli.NIT_FACTURA,
                                        NOMBRE = cli.NOMBRE,
                                        DIRECCION = (mfCli.CALLE_AVENIDA_FACTURA.Trim() + " " + mfCli.CASA_FACTURA.Trim() + " " + mfCli.ZONA_FACTURA.Trim() + " " + mfCli.APARTAMENTO_FACTURA.Trim() + " " + mfCli.COLONIA_FACTURA.Trim() + " " + mfCli.MUNICIPIO_FACTURA.Trim() + " " + mfCli.DEPARTAMENTO_FACTURA.Trim()),
                                        TIPOVTA = (short?)cc.SUBTIPO,//CONSTANTES.GUATEFAC.TIPO_VENTA.SERVICIOS,
                                        DESTINO = CONSTANTES.GUATEFAC.DESTINO_VENTA.GT,
                                        MONEDA = cc.TIPO_CAMBIO_MONEDA>1 ? CONSTANTES.GUATEFAC.MONEDA.DOL: CONSTANTES.GUATEFAC.MONEDA.GTQ,
                                        TASACAMBIO = cc.TIPO_CAMBIO_MONEDA,
                                        TIENDA = cc.COBRADOR,
                                        REFERENCIA = 0,
                                        DOCUMENTO = cc.DOCUMENTO,
                                        TIPO =(cc.TIPO.Replace("/", "").Equals("NC") ? 
                                                 EntityFunctions.DiffDays((from xfa in dbExactus.FACTURA where xfa.FACTURA1.Equals((from aux_cc in dbExactus.AUXILIAR_CC where aux_cc.CREDITO == cc.DOCUMENTO select aux_cc).FirstOrDefault().DEBITO) select xfa).FirstOrDefault().FECHA, cc.FECHA) > 61 ? CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_ABONO : CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_CREDITO
                                                : CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_DEBITO),
                                        FECHA = cc.FECHA,
                                        BRUTO = cc.MONTO,
                                        DESCUENTO = 0,
                                        NETO = cc.SUBTOTAL,
                                        IVA =  cc.IMPUESTO1,
                                        TOTAL = cc.MONTO,
                                        EXENTO= cc.TIPO_CAMBIO_MONEDA>1?cc.MONTO:0,
                                        //--------
                                        //SI ES UNA NOTA DE CREDITO SE OBTIENE LA FACTURA QUE ESTARÁ SIENDO AFECTADA EN LA TABLA AUTILIAR_CC EN LA COLUMNA CREDITO, DE LO CONTRARIO PARA UNA NOTA DE DEBITO, SE DEBE HACER JOIN EN LA COLUMNA DEBITO
                                        // OJO QUE ESTE DATO APARECE CUANDO SE "APLICA" EL DOCUMENTO EN EXACTUS (ESTE PROCESO ES MANUAL E INDEPENDIENTE DEL INGRESO DEL DOCUMENTO ASI QUE HAY QUE ESTAR PENDIENTE QUE PUEDEN NO APLICAR LA NOTA DE CREDITO/DEBITO)
                                        //-------
                                        DASERIE = cc.TIPO.Replace("/", "").Equals("NC") ? (from aux_cc in dbExactus.AUXILIAR_CC where aux_cc.CREDITO == cc.DOCUMENTO select aux_cc).FirstOrDefault().DEBITO
                                                    : (from aux_cc in dbExactus.AUXILIAR_CC where aux_cc.DEBITO == cc.DOCUMENTO select aux_cc).FirstOrDefault().CREDITO,
                                        PREIMPRESO = cc.TIPO.Replace("/", "").Equals("NC") ? (from aux_cc in dbExactus.AUXILIAR_CC where aux_cc.CREDITO == cc.DOCUMENTO select aux_cc).FirstOrDefault().DEBITO
                                                    : (from aux_cc in dbExactus.AUXILIAR_CC where aux_cc.DEBITO == cc.DOCUMENTO select aux_cc).FirstOrDefault().CREDITO
                                    }
                                  );

                qConsulta = qConsulta.OrderBy(x => x.FECHA);

                //DETALLE DE LAS NOTAS DE CREDITO O DE DEBITO
                var qDetalleConsulta = (
                                        from fl in dbExactus.FACTURA_LINEA
                                        join f in dbExactus.FACTURA on fl.FACTURA equals f.FACTURA1
                                        join ar in dbExactus.ARTICULO on fl.ARTICULO equals ar.ARTICULO1
                                        join cli in dbExactus.CLIENTE on f.CLIENTE equals cli.CLIENTE1
                                        join dd in dbExactus.DETALLE_DIRECCION on cli.DETALLE_DIRECCION equals dd.DETALLE_DIRECCION1
                                        //join pl in dbExactus.MF_Pedido_Linea on fl.ARTICULO equals pl.ARTICULO 

                                        where
                                        fl.TIPO_DOCUMENTO == f.TIPO_DOCUMENTO
                                        //&& pl.PEDIDO_LINEA == fl.PEDIDO_LINEA
                                        //BUSCAMOS DOCUMENTOS QUE NO SEAN FACTURAS
                                        && f.TIPO_DOCUMENTO != "F"
                                        //QUE NO ESTÉN ANULADOS
                                        && f.ANULADA != "S"
                                        && (fl.TIPO_LINEA == Const.TIENDA_VIRTUAL.TIPO_LINEA_ARTICULO_NORMAL || fl.TIPO_LINEA == Const.TIENDA_VIRTUAL.TIPO_LINEA_ARTICULO_SET)

                                        //QUE NO ESTEN EN LA TABLA MF_GFACE FIRMADOS
                                        && (from gf in dbExactus.MF_GFace where gf.DOCUMENTO == f.FACTURA1 && gf.FIRMADO.Equals("S") select gf).Any() == false
                                        //QUE BUSQUE A PARTIR DE LA FECHA DE IMPLEMENTACION
                                        && EntityFunctions.DiffDays(CONSTANTES.GUATEFAC.MODALIDAD_FEL.FECHA_INICIO_MODALIDAD_FEL, f.FECHA) >= 0
                                         && ((EntityFunctions.DiffDays(EntityFunctions.AddDays(DateTime.Today, -5), f.FECHA) >= 0 && blTests == false) || (blTests))
                                        select new
                                        {
                                            DOCUMENTO = f.FACTURA1,
                                            PRODUCTO = ar.ARTICULO1,
                                            DESCRIPCION = fl.DESCRIPCION,
                                            CANTIDAD = fl.CANTIDAD,
                                            PRECIO =  fl.DESC_TOT_LINEA>0?(fl.PRECIO_TOTAL + fl.TOTAL_IMPUESTO1) :((fl.PRECIO_UNITARIO + (fl.TOTAL_IMPUESTO1 / fl.CANTIDAD) + fl.DESC_TOT_LINEA ) ),
                                            IMpBRUTO = fl.DESC_TOT_LINEA > 0 ? (fl.PRECIO_TOTAL + fl.TOTAL_IMPUESTO1) :(fl.PRECIO_UNITARIO * fl.CANTIDAD + fl.TOTAL_IMPUESTO1 + fl.DESC_TOT_LINEA),
                                            IMpNETO = fl.DESC_TOT_LINEA > 0 ? fl.PRECIO_TOTAL : (fl.PRECIO_UNITARIO * fl.CANTIDAD),

                                            IMpIVA = fl.TOTAL_IMPUESTO1,
                                            IMpTOTAL = (fl.PRECIO_TOTAL+ fl.TOTAL_IMPUESTO1),
                                            TIPOVTADet = f.SUBTIPO_DOC_CXC,
                                            PORCDESC = 0,
                                            IMPdCTO = 0,
                                            IMPExento = f.MONEDA.Equals("D") ? (fl.PRECIO_UNITARIO * fl.CANTIDAD + fl.TOTAL_IMPUESTO1):0

                                        }
                                        ).Concat
                                        (
                                           from cc in dbExactus.DOCUMENTOS_CC

                                           where
                                           //BUSCAMOS A PARTIR DE LA FECHA DE IMPLEMENTACIÓN
                                           EntityFunctions.DiffDays(CONSTANTES.GUATEFAC.MODALIDAD_FEL.FECHA_INICIO_MODALIDAD_FEL, cc.FECHA) > 0
                                            && ((EntityFunctions.DiffDays(EntityFunctions.AddDays(DateTime.Today, -5), cc.FECHA) >= 0 && blTests == false) || (blTests))
                                            && (cc.TIPO.Contains("N/C") || cc.TIPO.Contains("N/D"))
                                            //SOLO LOS DOCUMENTOS ACTIVOS, LAS ANULACIONES SE HACEN EN OTRO SERVICIO
                                            && cc.PAQUETE.Equals("CC") && cc.ANULADO != "S"
                                            // QUE NO ESTEN FIRMADOS EN LA TABLA MF_GFACE
                                            && (from gf in dbExactus.MF_GFace where gf.DOCUMENTO == cc.DOCUMENTO && gf.FIRMADO.Equals("S") select gf).Any() == false


                                           select new
                                           {
                                               DOCUMENTO = cc.DOCUMENTO,
                                               PRODUCTO = cc.DOCUMENTO,
                                               DESCRIPCION = cc.APLICACION.Replace("|", "").Replace("\\", "").Replace("''", "").Substring(0, 70),
                                               CANTIDAD = nCantidad,
                                               PRECIO = (cc.SUBTOTAL + cc.IMPUESTO1),
                                               IMpBRUTO = (cc.SUBTOTAL + cc.IMPUESTO1),
                                               IMpNETO = (cc.SUBTOTAL),
                                               IMpIVA = cc.IMPUESTO1,
                                               IMpTOTAL = (cc.SUBTOTAL + cc.IMPUESTO1),
                                               TIPOVTADet = cc.SUBTIPO,
                                               PORCDESC = 0,
                                               IMPdCTO = 0,
                                               IMPExento = cc.TIPO_CAMBIO_MONEDA > 1 ? (cc.SUBTOTAL + cc.IMPUESTO1) : 0
                                           }
                                       )
                                        ;
                if (qConsulta.Count() > 0)
                {

                    foreach (var item in qConsulta)
                    {
                        //formamos el detalle primero
                        List<DetalleDoc> detalles = new List<DetalleDoc>();
                        var blnResultadoDocumento = true;




                        //----------------
                        //Detalle
                        //---------------
                        if (qDetalleConsulta.Count() > 0)
                        {
                            //validar el total del detalle vrs total del documento
                            decimal TotalDet = 0;
                            decimal NetoDet = 0;
                            decimal IvaDet = 0;
                            decimal BrutoDet = 0;
                            decimal TotalExento = 0;
                            var qDetalle = qDetalleConsulta.Where(x => x.DOCUMENTO.Equals(item.DOCUMENTO));
                            foreach (var d in qDetalle)
                            {
                                //if (d.PRECIO > 0)
                                {
                                    DetalleDoc det = new DetalleDoc
                                    {
                                        Producto = d.PRODUCTO, // detalles.Count==0 ? "M141061-103" : "M149066-000", //
                                        Descripcion = d.DESCRIPCION,
                                        Cantidad = d.CANTIDAD,
                                        Precio = Math.Round(d.PRECIO,2,MidpointRounding.AwayFromZero),
                                        PorcDesc = d.PORCDESC,
                                        ImpBruto = Math.Round(d.IMpBRUTO,2,MidpointRounding.AwayFromZero),
                                        ImpDescuento = d.IMPdCTO,
                                        ImpExento = (item.TIPO == DOCUMENTO.NOTA_ABONO || item.MONEDA == CONSTANTES.GUATEFAC.MONEDA.DOL) ?Math.Round(d.IMpBRUTO,2,MidpointRounding.AwayFromZero) : Math.Round(d.IMPExento,2,MidpointRounding.AwayFromZero),
                                        ImpNeto = (item.TIPO == DOCUMENTO.NOTA_ABONO || item.MONEDA == CONSTANTES.GUATEFAC.MONEDA.DOL) ? 0 : d.IMpNETO,
                                        ImpIva = (item.TIPO == DOCUMENTO.NOTA_ABONO || item.MONEDA == CONSTANTES.GUATEFAC.MONEDA.DOL) ? 0 : d.IMpIVA,
                                        ImpTotal = d.IMpTOTAL,
                                        TipoVentaDet = d.TIPOVTADet.ToString().Equals("0") ? CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES : CONSTANTES.GUATEFAC.TIPO_VENTA.SERVICIOS
                                    };
                                    TotalDet += d.IMpTOTAL;
                                    TotalExento += d.IMPExento;
                                    NetoDet += d.IMpNETO;
                                    IvaDet += d.IMpIVA;
                                    BrutoDet += d.IMpBRUTO;
                                    detalles.Add(det);
                                }

                            }
                            if (item.BRUTO != TotalExento && TotalExento > 0)
                            {
                                
                                foreach (var x in detalles)
                                {
                                    decimal ndif = x.ImpTotal - x.ImpExento;
                                    x.ImpBruto =   x.ImpBruto + ndif ;
                                    x.ImpExento = item.TIPO == DOCUMENTO.NOTA_ABONO ? x.ImpExento + ndif : detalles[detalles.Count - 1].ImpExento;
                                }
                                
                                
                            }
                            if (item.TOTAL != TotalDet && (Math.Abs(item.TOTAL - TotalDet) < 1))
                            {

                                decimal ndif = item.TOTAL - TotalDet;
                                detalles[detalles.Count - 1].ImpBruto = detalles[detalles.Count - 1].ImpBruto + ndif;
                                detalles[detalles.Count - 1].ImpNeto = item.TIPO == DOCUMENTO.NOTA_ABONO ? 0 : detalles[detalles.Count - 1].ImpNeto + ndif;
                                detalles[detalles.Count - 1].ImpTotal = detalles[detalles.Count - 1].ImpTotal + ndif;
                                //notas de abono solamente
                                detalles[detalles.Count - 1].ImpExento = item.TIPO == DOCUMENTO.NOTA_ABONO ? detalles[detalles.Count - 1].ImpExento + ndif : detalles[detalles.Count - 1].ImpExento;
                            }
                            else
                                if (item.BRUTO != BrutoDet && detalles[detalles.Count - 1].ImpTotal >0)
                            {
                                decimal ndif = item.BRUTO - BrutoDet;
                                detalles[detalles.Count - 1].ImpBruto = detalles[detalles.Count - 1].ImpBruto + ndif;
                                detalles[detalles.Count - 1].ImpNeto = item.TIPO == DOCUMENTO.NOTA_ABONO ? 0 : detalles[detalles.Count - 1].ImpNeto + ndif;
                               
                            }
                        }

                        //----------------------
                        //el documento
                        //----------------------

                        try
                        {

                            _DaSerie = (item.DASERIE.Substring(0, item.DASERIE.IndexOf("-")).Length > 4) ? item.DASERIE.Substring(0, item.DASERIE.IndexOf("-")):(from fd in dbExactus.MF_GFace where fd.DOCUMENTO.Equals(item.PREIMPRESO) select fd).FirstOrDefault().TIPO_ACTIVO+"-"+ item.DASERIE.Substring(0, item.DASERIE.IndexOf("-"));  //item.DASERIE.Length > 11 ? item.DASERIE.Substring(item.DASERIE.LastIndexOf("-") - 1, 1) : item.DASERIE.Substring(0, item.DASERIE.IndexOf("-"));
                            _DaPreimpreso = item.PREIMPRESO.Length > 11 ? item.PREIMPRESO.Substring(item.PREIMPRESO.LastIndexOf("-") + 1) : item.PREIMPRESO.Substring(item.PREIMPRESO.IndexOf("-") + 1);
                            _DaPreimpreso = _DaPreimpreso.Length > 11 ? _DaPreimpreso.Substring(0, _DaPreimpreso.IndexOf(" ")).Trim() : long.Parse(_DaPreimpreso.Trim()).ToString();
                            log.Debug("DASerie:" + _DaSerie + " DaPreimpreso: " + _DaPreimpreso);
                        }
                        catch (Exception Exc)
                        {
                            blnResultadoDocumento = false;

                            mRespuesta.Mensaje += CatchClass.ExMessage(Exc, "DALFel", "ObtenerOtrosDocumentosSinFirmar()") + "|";
                            log.Error(string.Format("-------->>>>>>>>>>Error al obtener los documentos que afectaron las notas de crédito. {0} {1} ", item.DASERIE, mRespuesta.Mensaje));
                        }
                        if (blnResultadoDocumento)
                        {
                            var qConsultaAsociado = (from fa in dbExactus.FACTURA where fa.FACTURA1.Equals(_DaSerie+"-"+_DaPreimpreso) select fa);
                            DateTime dtFEchaAsociado = new DateTime();
                            if (qConsultaAsociado.Count() > 0)
                            {
                                dtFEchaAsociado = qConsultaAsociado.FirstOrDefault().FECHA;
                            }
                            Doc documento = new Doc
                            {
                                ///información del emisor
                                NITEmisor = int.Parse(qConsultaEmisor.FirstOrDefault().NIT).ToString(),
                                TipoDocumento = item.TIPO,
                                Establecimiento = 1,
                                IdMaquina = MAQUINA.FACTURACION_DEFAULT,
                                SerieAdmin =  item.DOCUMENTO.Contains("-") ? item.DOCUMENTO.Substring(0, item.DOCUMENTO.IndexOf("-")) : "",
                                NumeroAdmin = item.DOCUMENTO.Contains("-") ? item.DOCUMENTO.Substring(item.DOCUMENTO.IndexOf("-") + 1) : item.DOCUMENTO,
                                // Receptor
                                NITReceptor = blTests ? "67998429" : item.NIT,//TESTS "42067219",//LA COLCHONERIA
                                Nombre = item.NOMBRE,
                                Direccion = item.DIRECCION,
                                //InfoDoc
                                TipoVenta = item.TIPOVTA.Equals(0) ? CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES : CONSTANTES.GUATEFAC.TIPO_VENTA.SERVICIOS,
                                DestinoVenta = item.DESTINO,
                                Fecha = blTests ? DateTime.Today.AddDays(-2) : item.FECHA,

                                Tasa = item.TASACAMBIO,
                                Moneda = item.MONEDA,

                                //Totales
                                Bruto = item.BRUTO,
                                Descuento = item.DESCUENTO,
                                Exento = (item.TIPO == DOCUMENTO.NOTA_ABONO || item.MONEDA == CONSTANTES.GUATEFAC.MONEDA.DOL) ? item.BRUTO : 0,
                                Neto = (item.TIPO == DOCUMENTO.NOTA_ABONO || item.MONEDA == CONSTANTES.GUATEFAC.MONEDA.DOL )? 0 : item.NETO,
                                Iva = (item.TIPO == DOCUMENTO.NOTA_ABONO || item.MONEDA == CONSTANTES.GUATEFAC.MONEDA.DOL) ? 0 : item.IVA,
                                Total = item.TOTAL
                                   //Detalles
                                   ,
                                Detalle = detalles,               
                                DASerie = _DaSerie, 
                                DAPreimpreso = ((dtFEchaAsociado.Month < 7 && dtFEchaAsociado.Year == 2019) || ( dtFEchaAsociado.Year < 2019)) ?long.Parse( _DaPreimpreso).ToString():_DaPreimpreso// DOCUMENTO ASOCIADO A ESTA NOTA
                                , Enviar = (item.CLIENTE.StartsWith("M") ? (blnEnviarMayoreo ? "S" : "N") : (blnEnviarFiesta ? "S" : "N")),
                                Email = (item.CLIENTE.StartsWith("M") ? (blnEnviarMayoreo ? item.EMAIL : string.Empty) : (blnEnviarFiesta ? item.EMAIL : string.Empty))

                            };
                            //tests
                            //var SERIE = item.DASERIE.Substring(0, item.DASERIE.IndexOf("-"));
                            //var PREIMPRESO = item.PREIMPRESO.Substring(item.PREIMPRESO.IndexOf("-") + 1);
                            lstDocumentos.Add(documento);

                        }
                        mRespuesta.Exito &= blnResultadoDocumento;
                    }
                }
                mRespuesta.Objeto = lstDocumentos;
                mRespuesta.Exito = true;

            }
            catch (Exception ex)
            {
                mRespuesta.Exito = false;
                mRespuesta.Mensaje += CatchClass.ExMessage(ex, "DALFel", "ObtenerOtrosDocumentosSinFirmar()") + "|";
                log.Error("Error al obtener los documentos para firmar electrónicamente. " + mRespuesta.Mensaje);
            }
            //var json = new JavaScriptSerializer().Serialize(mRespuesta);

            return mRespuesta;
        }
        //Método para obtener los documentos firmados con número de acceso o contingencia
        //para enviarlos a firmar cuando el servicio haya sido restablecido
        //y estos valores de contingencia, anotar el número de factura devuelvo para cada documento
        private Respuesta ObtenerDocumentosEnContingencia()
        {
            log.Debug("Obteniendo las notas de crédito y débito para firmar electrónicamente");
            Respuesta mRespuesta = new Respuesta();
            List<Doc> lstDocumentos = new List<Doc>();
            try
            {
                decimal nCantidad = 1;
                var qConsultaEmisor = (from mf_g in dbExactus.MF_GFaceConfigura select mf_g);
                log.Debug("Obteniendo los documentos firmados con número de acceso o contingencia para enviarlos a firmar");
                var qContingencia = (
                                from fa in dbExactus.FACTURA
                                join cli in dbExactus.CLIENTE on fa.CLIENTE equals cli.CLIENTE1
                                join dd in dbExactus.DETALLE_DIRECCION on cli.DETALLE_DIRECCION equals dd.DETALLE_DIRECCION1
                                join mfCli in dbExactus.MF_Cliente on fa.CLIENTE equals mfCli.CLIENTE

                                join cn in dbExactus.MF_ContingenciaFEL on fa.FACTURA1 equals cn.ValorDeAcceso
                                where

                                cn.Usado == true
                                && cn.NumFactura == null
                                && fa.ANULADA != "S"
                               
                                select new
                                {
                                    TIPO_ORIGEN = fa.TIPO_DOCUMENTO, //SERVIRA PARA ARMAR LA ESTRUCTURA DEL DOCUMENTO Y SABER SI ES FACTURA, DEVOLUCIÓN DE FACTURACION O NOTA DE CREDITO O DEBITO DE CXC
                                    NIT = cli.CONTRIBUYENTE,
                                    NOMBRE = (cli.CONTRIBUYENTE.Contains("CF") ? cli.NOMBRE : string.Empty),
                                    DIRECCION = (cli.CONTRIBUYENTE.Contains("CF") ?
                                                 (mfCli.CALLE_AVENIDA_FACTURA.Trim() + " " + mfCli.CASA_FACTURA.Trim() + " " + mfCli.ZONA_FACTURA.Trim() + " " + mfCli.APARTAMENTO_FACTURA.Trim() + " " + mfCli.COLONIA_FACTURA.Trim() + " " + mfCli.MUNICIPIO_FACTURA.Trim() + " " + mfCli.DEPARTAMENTO_FACTURA.Trim()) : string.Empty),
                                    TIPOVTA = CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES,//(CASE a.SUBTIPO_DOC_CXC WHEN 0 THEN 'BIEN' ELSE 'SERVICIO' END)
                                    DESTINO = CONSTANTES.GUATEFAC.DESTINO_VENTA.GT,
                                    MONEDA = fa.MONEDA.Equals("D") ? CONSTANTES.GUATEFAC.MONEDA.DOL : CONSTANTES.GUATEFAC.MONEDA.GTQ,//(CASE a.MONEDA_FACTURA WHEN 'D' THEN 'USD' ELSE 'GTQ' END)
                                    TASACAMBIO = fa.MONEDA_FACTURA.Equals("D") ? fa.TIPO_CAMBIO : CONSTANTES.GUATEFAC.TASA.GTQ,//(CASE a.MONEDA_FACTURA WHEN 'D' THEN a.TIPO_CAMBIO ELSE 1 END) AS TasaCambio
                                    TIENDA = fa.COBRADOR,
                                    DOCUMENTO = fa.FACTURA1,
                                    TIPO = (fa.TIPO_DOCUMENTO.Equals("D") && EntityFunctions.DiffDays((from xfa in dbExactus.FACTURA where xfa.FACTURA1.Equals(fa.FACTURA_ORIGINAL) select xfa).FirstOrDefault().FECHA, fa.FECHA) > 61 ? CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_ABONO : CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_CREDITO),
                                    fa.FECHA,
                                    BRUTO = (fa.TOTAL_FACTURA),
                                    DESCUENTO = 0,
                                    NETO = (fa.TOTAL_MERCADERIA),
                                    IVA = (fa.TOTAL_IMPUESTO1),
                                    TOTAL = (fa.TOTAL_FACTURA),
                                    //valor que indica la contingencia
                                    NUMEROACCESO = fa.FACTURA1,
                                    DASERIE = fa.FACTURA1,
                                    PREIMPRESO = fa.FACTURA1
                                    
                                }


                    ).Concat
                                 (
                                    from cc in dbExactus.DOCUMENTOS_CC
                                    join cli in dbExactus.CLIENTE on cc.CLIENTE equals cli.CLIENTE1
                                    join mfCli in dbExactus.MF_Cliente on cli.CLIENTE1 equals mfCli.CLIENTE
                                    join dd in dbExactus.DETALLE_DIRECCION on cli.DETALLE_DIRECCION equals dd.DETALLE_DIRECCION1

                                    join cn in dbExactus.MF_ContingenciaFEL on cc.DOCUMENTO equals cn.ValorDeAcceso
                                    where
                                    (cc.TIPO.Contains("N/C") || cc.TIPO.Contains("N/D"))
                                    //SOLO LOS DOCUMENTOS ACTIVOS, LAS ANULACIONES SE HACEN EN OTRO SERVICIO
                                    && cc.PAQUETE.Equals("CC") && cc.ANULADO != "S"
                                    && cn.Usado == true && cn.NumFactura == null // que no haya sido firmado

                                    select new
                                    {
                                        TIPO_ORIGEN = cc.TIPO.Replace("/", ""),
                                        NIT = cli.CONTRIBUYENTE,
                                        NOMBRE = (cli.CONTRIBUYENTE.Contains("CF") ? cli.NOMBRE : string.Empty),
                                        DIRECCION = (cli.CONTRIBUYENTE.Contains("CF") ?
                                                    (mfCli.CALLE_AVENIDA_FACTURA.Trim() + " " + mfCli.CASA_FACTURA.Trim() + " " + mfCli.ZONA_FACTURA.Trim() + " " + mfCli.APARTAMENTO_FACTURA.Trim() + " " + mfCli.COLONIA_FACTURA.Trim() + " " + mfCli.MUNICIPIO_FACTURA.Trim() + " " + mfCli.DEPARTAMENTO_FACTURA.Trim()) : string.Empty),
                                        TIPOVTA = CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES,
                                        DESTINO = CONSTANTES.GUATEFAC.DESTINO_VENTA.GT,
                                        MONEDA = CONSTANTES.GUATEFAC.MONEDA.GTQ,
                                        TASACAMBIO = CONSTANTES.GUATEFAC.TASA.GTQ,
                                        TIENDA = cc.COBRADOR,
                                        DOCUMENTO = cc.DOCUMENTO,
                                        TIPO = (cc.TIPO.Replace("/", "").Equals("NC") ? CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_CREDITO : CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_DEBITO),
                                        FECHA = cc.FECHA,
                                        BRUTO = (cc.MONTO),
                                        DESCUENTO = 0,
                                        NETO = (cc.MONTO - cc.IMPUESTO1),
                                        IVA = (cc.IMPUESTO1),
                                        TOTAL = (cc.MONTO),
                                        //valor que indica la contingencia
                                        NUMEROACCESO = cc.DOCUMENTO,
                                        DASERIE = cc.DOCUMENTO,
                                        PREIMPRESO = cc.DOCUMENTO
                                    }
                                  );
                //DETALLE DE LAS NOTAS DE CREDITO O DE DEBITO
                var qDetalleConsulta = (
                                        from fl in dbExactus.FACTURA_LINEA
                                        join f in dbExactus.FACTURA on fl.FACTURA equals f.FACTURA1
                                        join ar in dbExactus.ARTICULO on fl.ARTICULO equals ar.ARTICULO1
                                        join cli in dbExactus.CLIENTE on f.CLIENTE equals cli.CLIENTE1
                                        join dd in dbExactus.DETALLE_DIRECCION on cli.DETALLE_DIRECCION equals dd.DETALLE_DIRECCION1

                                        join cn in dbExactus.MF_ContingenciaFEL on f.FACTURA1 equals cn.ValorDeAcceso
                                        where
                                        fl.TIPO_DOCUMENTO == f.TIPO_DOCUMENTO
                                        //BUSCAMOS DOCUMENTOS QUE NO SEAN FACTURAS
                                        && cn.Usado == true && cn.NumFactura == null // que no haya sido firmado
                                        //QUE NO ESTÉN ANULADOS
                                        && f.ANULADA != "S"
                                        //&& fl.PRECIO_TOTAL > 0

                                        //QUE BUSQUE A PARTIR DE LA FECHA DE IMPLEMENTACION
                                        && EntityFunctions.DiffDays(CONSTANTES.GUATEFAC.MODALIDAD_FEL.FECHA_INICIO_MODALIDAD_FEL, f.FECHA) > 0
                                        select new
                                        {

                                            DOCUMENTO = f.FACTURA1,
                                            PRODUCTO = ar.ARTICULO1,
                                            DESCRIPCION = ar.DESCRIPCION,
                                            CANTIDAD = fl.CANTIDAD,
                                            PRECIO = (fl.PRECIO_TOTAL + fl.TOTAL_IMPUESTO1) / fl.CANTIDAD,
                                            IMpBRUTO = (fl.PRECIO_TOTAL) + fl.TOTAL_IMPUESTO1,
                                            IMpNETO = fl.PRECIO_TOTAL,
                                            IMpIVA = fl.TOTAL_IMPUESTO1,
                                            IMpTOTAL = (fl.PRECIO_TOTAL) + fl.TOTAL_IMPUESTO1,
                                            TIPOVTADet = f.SUBTIPO_DOC_CXC,
                                            PORCDESC = 0,
                                            IMPdCTO = 0,
                                            IMPExento = (cli.CLIENTE1.Equals(CONSTANTES.GUATEFAC.EXPORTACION.CLIENTE_EXPORTACION) ? fl.PRECIO_TOTAL + fl.TOTAL_IMPUESTO1 : (decimal)0)

                                        }
                                        ).Concat
                                        (
                                           from cc in dbExactus.DOCUMENTOS_CC

                                           join cn in dbExactus.MF_ContingenciaFEL on cc.DOCUMENTO equals cn.ValorDeAcceso

                                           where
                                           //BUSCAMOS A PARTIR DE LA FECHA DE IMPLEMENTACIÓN
                                           EntityFunctions.DiffDays(CONSTANTES.GUATEFAC.MODALIDAD_FEL.FECHA_INICIO_MODALIDAD_FEL, cc.FECHA) > 0
                                            && (cc.TIPO.Contains("N/C") || cc.TIPO.Contains("N/D"))
                                            //SOLO LOS DOCUMENTOS ACTIVOS, LAS ANULACIONES SE HACEN EN OTRO SERVICIO
                                            && cc.PAQUETE.Equals("CC") && cc.ANULADO != "S"
                                            // QUE NO ESTEN FIRMADOS EN LA TABLA MF_GFACE
                                            && (from gf in dbExactus.MF_GFace where gf.DOCUMENTO == cc.DOCUMENTO && gf.FIRMADO.Equals("S") select gf).Any() == false

                                             //que no haya sido firmado aún
                                             && cn.Usado == true && cn.NumFactura == null // que no haya sido firmado

                                           select new
                                           {

                                               DOCUMENTO = cc.DOCUMENTO,
                                               PRODUCTO = cc.DOCUMENTO,
                                               DESCRIPCION = cc.APLICACION.Replace("|", "").Replace("\\", "").Replace("''", "").Substring(0, 70),
                                               CANTIDAD = nCantidad,
                                               PRECIO = cc.MONTO,
                                               IMpBRUTO = cc.MONTO,
                                               IMpNETO = cc.MONTO - cc.IMPUESTO1,
                                               IMpIVA = cc.IMPUESTO1,
                                               IMpTOTAL = cc.MONTO,
                                               TIPOVTADet = cc.SUBTIPO,
                                               PORCDESC = 0,
                                               IMPdCTO = 0,
                                               IMPExento = (decimal)0
                                           }
                                        );
                if (qContingencia != null)
                    if (qContingencia.Count() > 0)
                    {

                        foreach (var item in qContingencia)
                        {
                            //formamos el detalle primero
                            Doc documento;
                            List<DetalleDoc> detalles = new List<DetalleDoc>();

                            //validar el total del detalle vrs total del documento
                            decimal TotalDet = 0;
                            //----------------
                            //Detalle
                            //---------------
                            if (qDetalleConsulta.Count() > 0)
                            {

                                var qDetalle = qDetalleConsulta.Where(x => x.DOCUMENTO.Equals(item.DOCUMENTO));
                                foreach (var d in qDetalle)
                                {
                                    DetalleDoc det = new DetalleDoc
                                    {
                                        Producto = item.TIPO_ORIGEN.Equals("F") ? d.PRODUCTO : d.PRODUCTO.Substring(d.PRODUCTO.IndexOf("-") + 1),
                                        Descripcion = d.DESCRIPCION,
                                        Cantidad = d.CANTIDAD,
                                        Precio = Math.Round(d.PRECIO, 2, MidpointRounding.AwayFromZero),
                                        PorcDesc = d.PORCDESC,
                                        ImpBruto = Math.Round(d.IMpBRUTO, 2, MidpointRounding.AwayFromZero),
                                        ImpDescuento = d.IMPdCTO,
                                        ImpExento = Math.Round(d.IMPExento, 2, MidpointRounding.AwayFromZero),
                                        ImpNeto = Math.Round(d.IMpNETO, 4, MidpointRounding.AwayFromZero),
                                        ImpIva = d.IMpIVA,
                                        ImpTotal = Math.Round(d.IMpTOTAL, 2, MidpointRounding.AwayFromZero),
                                        TipoVentaDet = d.TIPOVTADet.ToString().Equals("0") ? CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES : CONSTANTES.GUATEFAC.TIPO_VENTA.SERVICIOS
                                    };

                                    TotalDet += d.IMpTOTAL;
                                    detalles.Add(det);
                                }

                                if (item.TOTAL != TotalDet && (Math.Abs(item.TOTAL - TotalDet) < 1))
                                {

                                    decimal ndif = item.TOTAL - TotalDet;
                                    detalles[detalles.Count - 1].ImpBruto = detalles[detalles.Count - 1].ImpBruto + ndif;
                                    detalles[detalles.Count - 1].ImpNeto = item.TIPO == DOCUMENTO.NOTA_ABONO ? 0 : detalles[detalles.Count - 1].ImpNeto + ndif;
                                    detalles[detalles.Count - 1].ImpTotal = detalles[detalles.Count - 1].ImpTotal + ndif;
                                    //notas de abono solamente
                                    detalles[detalles.Count - 1].ImpExento = item.TIPO == DOCUMENTO.NOTA_ABONO ? detalles[detalles.Count - 1].ImpExento + ndif : detalles[detalles.Count - 1].ImpExento;
                                }
                            }

                            //----------------------
                            //el documento
                            //----------------------
                            if (item.TIPO_ORIGEN.Equals("F")) //NO NOTAS DE CREDITO Y DEBITO
                            {
                                documento = new Doc
                                {
                                    ///información del emisor
                                    NITEmisor = int.Parse(qConsultaEmisor.FirstOrDefault().NIT).ToString(),
                                    Establecimiento = 1,
                                    IdMaquina = "001",
                                    TipoDocumento = CONSTANTES.GUATEFAC.DOCUMENTO.FACTURA,
                                    // Receptor
                                    NITReceptor = item.NIT,
                                    Nombre = item.NOMBRE,
                                    Direccion = item.DIRECCION,
                                    //InfoDoc
                                    TipoVenta = item.TIPOVTA,
                                    DestinoVenta = item.DESTINO,
                                    Fecha = item.FECHA,
                                    Moneda = item.MONEDA,
                                    Tasa = item.TASACAMBIO,

                                    //Totales
                                    Bruto = item.BRUTO,
                                    Descuento = item.DESCUENTO,
                                    Neto = item.NETO,
                                    Iva = item.IVA,
                                    Total = item.TOTAL
                                    //Detalles
                                    ,
                                    Detalle = detalles,

                                    NumeroAcceso = item.NUMEROACCESO//item.NUMEROACCESO.Substring(item.NUMEROACCESO.IndexOf("-") + 1)
                                   
                                };

                            }
                            else
                            {
                                documento = new Doc
                                {
                                    ///información del emisor
                                    NITEmisor = int.Parse(qConsultaEmisor.FirstOrDefault().NIT).ToString(),
                                    Establecimiento = 1,
                                    IdMaquina = "001",
                                    TipoDocumento = (item.TIPO_ORIGEN.Equals("NC") ? CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_CREDITO : (item.TIPO_ORIGEN.Equals("ND") ? CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_DEBITO : CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_ABONO)),
                                    // Receptor
                                    NITReceptor = item.NIT,
                                    Nombre = item.NOMBRE,
                                    Direccion = item.DIRECCION,
                                    //InfoDoc
                                    TipoVenta = item.TIPOVTA,
                                    DestinoVenta = item.DESTINO,
                                    Fecha = item.FECHA,
                                    Moneda = item.MONEDA,
                                    Tasa = item.TASACAMBIO,

                                    //Totales
                                    Bruto = item.BRUTO,
                                    Descuento = item.DESCUENTO,
                                    Neto = item.NETO,
                                    Iva = item.IVA,
                                    Total = item.TOTAL
                                        //Detalles
                                        ,
                                    Detalle = detalles,
                                    DASerie = item.DASERIE.Substring(0, item.DASERIE.IndexOf("-")),
                                    DAPreimpreso = item.PREIMPRESO.Substring(item.PREIMPRESO.IndexOf("-") + 1),
                                    NumeroAcceso = item.NUMEROACCESO//item.NUMEROACCESO.Substring(item.NUMEROACCESO.IndexOf("-") + 1)
                                };
                            }
                            //AGREGAMOS LOS DOCUMENTOS
                            lstDocumentos.Add(documento);
                        }
                    }
                mRespuesta.Objeto = lstDocumentos;
                mRespuesta.Exito = true;
            }
            catch (Exception ex)
            {
                log.Error("Error al obtener los documentos con número de contingencia | Acceso, previo a enviarlos a firmar electrónicamente." + ex.Message + "|" + ex.InnerException);
                mRespuesta.Exito = false;
            }
            return mRespuesta;
        }

        private Respuesta ObtenerDocumentosAnulados()
        {
            log.Debug("Obteniendo notas de crédito y débito anulados");
            Respuesta mRespuesta = new Respuesta();
            List<Doc> lstDocumentos = new List<Doc>();
            try
            {
                decimal nCantidad = 1;
                var qConsultaEmisor = (from mf_g in dbExactus.MF_GFaceConfigura select mf_g);
                var qConsultaTests = (from cs in dbExactus.MF_Catalogo where cs.CODIGO_TABLA == CONSTANTES.CATALOGOS.GUATEFACTURAS_FEL && cs.CODIGO_CAT == "PRODUCCION_FEL" select cs).FirstOrDefault();
                bool blTests = qConsultaTests.NOMBRE_CAT.Equals("N");

                var qAnulados =
                    (
                                from fa in dbExactus.FACTURA
                                join cli in dbExactus.CLIENTE on fa.CLIENTE equals cli.CLIENTE1
                                join dd in dbExactus.DETALLE_DIRECCION on cli.DETALLE_DIRECCION equals dd.DETALLE_DIRECCION1
                                join mfCli in dbExactus.MF_Cliente on fa.CLIENTE equals mfCli.CLIENTE
                                join cFA in dbExactus.CONSECUTIVO_FA on fa.CONSECUTIVO equals cFA.CODIGO_CONSECUTIVO
                                join gce in dbExactus.MF_GFace on fa.FACTURA1 equals gce.DOCUMENTO
                                where
                                 fa.ANULADA == "S"
                                 && fa.TIPO_DOCUMENTO != "F"
                                 && gce.ANULADA == "N"
                                 && fa.RUBRO4 != null
                                 && fa.RUBRO4 != ""
                                 && (fa.RUBRO4 !=("NC-758") || fa.RUBRO4 != ("NC-757") || fa.RUBRO4 != ("NC-756")) 
                                select new
                                {
                                    TIPO_ORIGEN = fa.TIPO_DOCUMENTO, //SERVIRA PARA ARMAR LA ESTRUCTURA DEL DOCUMENTO Y SABER SI ES FACTURA, DEVOLUCIÓN DE FACTURACION O NOTA DE CREDITO O DEBITO DE CXC
                                    NIT = cli.CONTRIBUYENTE,
                                    NOMBRE = (cli.CONTRIBUYENTE.Contains("CF") ? cli.NOMBRE : string.Empty),


                                    DIRECCION = (cli.CONTRIBUYENTE.Contains("CF") ?
                                                                    fa.DIRECCION_FACTURA.Replace("Call/Av/Km/Mz:", "").Replace("Casa/Lote:", "").Replace("Apto/Piso:", "").Replace("Zona/Aldea:", "").Replace("Col/Barr/Cant:", "").Replace("Departamento:", "").Replace("Municipio:", "").Replace("Entra Camión: SI", "").Replace("Entra Pickup: SI", "").Replace("Entra Camión: NO", "").Trim().Equals(string.Empty) ? "CIUDAD" :
                                                                    fa.DIRECCION_FACTURA.Replace("Call/Av/Km/Mz:", "").Replace("Casa/Lote:", "").Replace("Apto/Piso:", "").Replace("Zona/Aldea:", "").Replace("Col/Barr/Cant:", "").Replace("Departamento:", "").Replace("Municipio:", "").Replace("Entra Camión: SI", "").Replace("Entra Pickup: SI", "").Replace("Entra Camión: NO", "").Trim() : ""),
                                    TIPOVTA = CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES,//(CASE a.SUBTIPO_DOC_CXC WHEN 0 THEN 'BIEN' ELSE 'SERVICIO' END)
                                    DESTINO = CONSTANTES.GUATEFAC.DESTINO_VENTA.GT,
                                    MONEDA = fa.MONEDA.Equals("D") ? CONSTANTES.GUATEFAC.MONEDA.DOL : CONSTANTES.GUATEFAC.MONEDA.GTQ,//(CASE a.MONEDA_FACTURA WHEN 'D' THEN 'USD' ELSE 'GTQ' END)
                                    TASACAMBIO = fa.MONEDA_FACTURA.Equals("D") ? fa.TIPO_CAMBIO : CONSTANTES.GUATEFAC.TASA.GTQ,//(CASE a.MONEDA_FACTURA WHEN 'D' THEN a.TIPO_CAMBIO ELSE 1 END) AS TasaCambio
                                    TIENDA = fa.COBRADOR,
                                    DOCUMENTO = fa.FACTURA1,
                                    TIPO = (fa.TIPO_DOCUMENTO.Equals("D") && EntityFunctions.DiffMonths(fa.FECHA, fa.FECHA) >= 2 ? CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_ABONO : CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_CREDITO),
                                    fa.FECHA,
                                    BRUTO = (fa.TOTAL_FACTURA),
                                    DESCUENTO = 0,
                                    NETO = (fa.TOTAL_MERCADERIA),
                                    IVA = (fa.TOTAL_IMPUESTO1),
                                    TOTAL = (fa.TOTAL_FACTURA),
                                    //valor que indica la contingencia
                                    REFERENCIA = fa.RUBRO4,
                                    DASERIE = fa.FACTURA1,
                                    PREIMPRESO = fa.FACTURA1
                                }


                                 ).Concat
                                 (
                                    from cc in dbExactus.DOCUMENTOS_CC
                                    join cli in dbExactus.CLIENTE on cc.CLIENTE equals cli.CLIENTE1
                                    join dd in dbExactus.DETALLE_DIRECCION on cli.DETALLE_DIRECCION equals dd.DETALLE_DIRECCION1
                                    join cFa in dbExactus.CONSECUTIVO_FA on cc.CONTRARECIBO equals cFa.CODIGO_CONSECUTIVO
                                    join gce in dbExactus.MF_GFace on cc.DOCUMENTO equals gce.DOCUMENTO
                                    where
                                    (cc.TIPO.Contains("N/C") || cc.TIPO.Contains("N/D"))
                                    //SOLO LOS DOCUMENTOS ACTIVOS, LAS ANULACIONES SE HACEN EN OTRO SERVICIO
                                    && cc.PAQUETE.Equals("CC") && cc.ANULADO == "S"
                                    && gce.ANULADA == "N"
                                    && cc.DOCUMENTO_FISCAL != null



                                    select new
                                    {
                                        TIPO_ORIGEN = cc.TIPO.Replace("/", ""),
                                        NIT = cli.CONTRIBUYENTE,
                                        NOMBRE = (cli.CONTRIBUYENTE.Contains("CF") ? cli.NOMBRE : string.Empty),
                                        DIRECCION = (cli.CONTRIBUYENTE.Contains("CF") ?
                                        dd.DIRECCION.Replace("Call/Av/Km/Mz:", "").Replace("Casa/Lote:", "").Replace("Apto/Piso:", "").Replace("Zona/Aldea:", "").Replace("Col/Barr/Cant:", "").Replace("Departamento:", "").Replace("Municipio:", "").Replace("Entra Camión: SI", "").Replace("Entra Pickup: SI", "").Replace("Entra Camión: NO", "").Trim().Equals(string.Empty) ? "CIUDAD" :
                                        dd.DIRECCION.Replace("Call/Av/Km/Mz:", "").Replace("Casa/Lote:", "").Replace("Apto/Piso:", "").Replace("Zona/Aldea:", "").Replace("Col/Barr/Cant:", "").Replace("Departamento:", "").Replace("Municipio:", "").Replace("Entra Camión: SI", "").Replace("Entra Pickup: SI", "").Replace("Entra Camión: NO", "").Trim() : ""),
                                        TIPOVTA = CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES,
                                        DESTINO = CONSTANTES.GUATEFAC.DESTINO_VENTA.GT,
                                        MONEDA = CONSTANTES.GUATEFAC.MONEDA.GTQ,
                                        TASACAMBIO = CONSTANTES.GUATEFAC.TASA.GTQ,
                                        TIENDA = cc.COBRADOR,
                                        DOCUMENTO = cc.DOCUMENTO,
                                        TIPO = (cc.TIPO.Replace("/", "").Equals("NC") ? CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_CREDITO : CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_DEBITO),
                                        FECHA = cc.FECHA,
                                        BRUTO = (cc.MONTO),
                                        DESCUENTO = 0,
                                        NETO = (cc.MONTO - cc.IMPUESTO1),
                                        IVA = (cc.IMPUESTO1),
                                        TOTAL = (cc.MONTO),
                                        //valor que indica la contingencia
                                        REFERENCIA = cc.DOCUMENTO_FISCAL,
                                        DASERIE = cc.DOCUMENTO,
                                        PREIMPRESO = cc.DOCUMENTO
                                    }
                                  );
                //DETALLE DE LAS NOTAS DE CREDITO O DE DEBITO
                var qDetalleConsulta = (
                                        from fl in dbExactus.FACTURA_LINEA
                                        join f in dbExactus.FACTURA on fl.FACTURA equals f.FACTURA1
                                        join ar in dbExactus.ARTICULO on fl.ARTICULO equals ar.ARTICULO1
                                        join cli in dbExactus.CLIENTE on f.CLIENTE equals cli.CLIENTE1
                                        join dd in dbExactus.DETALLE_DIRECCION on cli.DETALLE_DIRECCION equals dd.DETALLE_DIRECCION1
                                        join c_fa in dbExactus.CONSECUTIVO_FA on f.CONSECUTIVO equals c_fa.CODIGO_CONSECUTIVO
                                        join gce in dbExactus.MF_GFace on f.FACTURA1 equals gce.DOCUMENTO
                                        where
                                        fl.TIPO_DOCUMENTO == f.TIPO_DOCUMENTO
                                        //BUSCAMOS DOCUMENTOS QUE NO SEAN FACTURAS
                                        && gce.ANULADA == "N"
                                        //QUE ESTÉN ANULADOS
                                        && f.ANULADA == "S"
                                        //&& fl.PRECIO_TOTAL > 0

                                        //QUE BUSQUE A PARTIR DE LA FECHA DE IMPLEMENTACION
                                        && EntityFunctions.DiffDays(CONSTANTES.GUATEFAC.MODALIDAD_FEL.FECHA_INICIO_MODALIDAD_FEL, f.FECHA) > 0
                                        select new
                                        {

                                            DOCUMENTO = f.FACTURA1,
                                            PRODUCTO = ar.ARTICULO1,
                                            DESCRIPCION = ar.DESCRIPCION,
                                            CANTIDAD = fl.CANTIDAD,
                                            PRECIO = (fl.PRECIO_TOTAL + fl.TOTAL_IMPUESTO1) / fl.CANTIDAD,
                                            IMpBRUTO = (fl.PRECIO_TOTAL) + fl.TOTAL_IMPUESTO1,
                                            IMpNETO = fl.PRECIO_TOTAL,
                                            IMpIVA = fl.TOTAL_IMPUESTO1,
                                            IMpTOTAL = (fl.PRECIO_TOTAL) + fl.TOTAL_IMPUESTO1,
                                            TIPOVTADet = f.SUBTIPO_DOC_CXC,
                                            PORCDESC = 0,
                                            IMPdCTO = 0,
                                            IMPExento = (cli.CLIENTE1.Equals(CONSTANTES.GUATEFAC.EXPORTACION.CLIENTE_EXPORTACION) ? fl.PRECIO_TOTAL + fl.TOTAL_IMPUESTO1 : (decimal)0)

                                        }
                                        ).Concat
                                        (
                                           from cc in dbExactus.DOCUMENTOS_CC
                                           join c_fa in dbExactus.CONSECUTIVO_FA on cc.CONTRARECIBO equals c_fa.CODIGO_CONSECUTIVO
                                           join gce in dbExactus.MF_GFace on cc.DOCUMENTO equals gce.DOCUMENTO

                                           where
                                           //BUSCAMOS A PARTIR DE LA FECHA DE IMPLEMENTACIÓN
                                           EntityFunctions.DiffDays(CONSTANTES.GUATEFAC.MODALIDAD_FEL.FECHA_INICIO_MODALIDAD_FEL, cc.FECHA) > 0
                                            && (cc.TIPO.Contains("N/C") || cc.TIPO.Contains("N/D"))
                                            //SOLO LOS DOCUMENTOS ACTIVOS, LAS ANULACIONES SE HACEN EN OTRO SERVICIO
                                            && cc.PAQUETE.Equals("CC") && cc.ANULADO == "S"
                                            // QUE NO ESTEN FIRMADOS EN LA TABLA MF_GFACE
                                            && (from gf in dbExactus.MF_GFace where gf.DOCUMENTO == cc.DOCUMENTO && gf.FIRMADO.Equals("S") select gf).Any() == false

                                               //que no haya sido firmado aún
                                               && gce.ANULADA == "N"

                                           select new
                                           {

                                               DOCUMENTO = cc.DOCUMENTO,
                                               PRODUCTO = cc.DOCUMENTO,
                                               DESCRIPCION = cc.APLICACION.Replace("|", "").Replace("\\", "").Replace("''", "").Substring(0, 70),
                                               CANTIDAD = nCantidad,
                                               PRECIO = cc.MONTO,
                                               IMpBRUTO = cc.MONTO,
                                               IMpNETO = cc.MONTO - cc.IMPUESTO1,
                                               IMpIVA = cc.IMPUESTO1,
                                               IMpTOTAL = cc.MONTO,
                                               TIPOVTADet = cc.SUBTIPO,
                                               PORCDESC = 0,
                                               IMPdCTO = 0,
                                               IMPExento = (decimal)0
                                           }
                                        );
                if (qAnulados != null)
                    if (qAnulados.Count() > 0)
                    {

                        foreach (var item in qAnulados)
                        {
                            //formamos el detalle primero
                            Doc documento;
                            List<DetalleDoc> detalles = new List<DetalleDoc>();
                            //----------------
                            //Detalle
                            //---------------
                            if (qDetalleConsulta.Count() > 0)
                            {

                                var qDetalle = qDetalleConsulta.Where(x => x.DOCUMENTO.Equals(item.DOCUMENTO));
                                foreach (var d in qDetalle)
                                {
                                    DetalleDoc det = new DetalleDoc
                                    {
                                        Producto = item.TIPO_ORIGEN.Equals("F") ? d.PRODUCTO : d.PRODUCTO.Substring(d.PRODUCTO.IndexOf("-") + 1),
                                        Descripcion = d.DESCRIPCION,
                                        Cantidad = d.CANTIDAD,
                                        Precio = Math.Round(d.PRECIO, 2, MidpointRounding.AwayFromZero),
                                        PorcDesc = d.PORCDESC,
                                        ImpBruto = Math.Round(d.IMpBRUTO, 2, MidpointRounding.AwayFromZero),
                                        ImpDescuento = d.IMPdCTO,
                                        ImpExento = Math.Round(d.IMPExento, 2, MidpointRounding.AwayFromZero),
                                        ImpNeto = Math.Round(d.IMpNETO, 4, MidpointRounding.AwayFromZero),
                                        ImpIva = d.IMpIVA,
                                        ImpTotal = Math.Round(d.IMpTOTAL, 2, MidpointRounding.AwayFromZero),
                                        TipoVentaDet = d.TIPOVTADet.ToString().Equals("0") ? CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES : CONSTANTES.GUATEFAC.TIPO_VENTA.SERVICIOS
                                    };

                                    detalles.Add(det);
                                }
                            }

                            //----------------------
                            //el documento
                            //----------------------
                            if (item.TIPO_ORIGEN.Equals("F")) //NO NOTAS DE CREDITO Y DEBITO
                            {
                                documento = new Doc
                                {
                                    ///información del emisor
                                    NITEmisor = int.Parse(qConsultaEmisor.FirstOrDefault().NIT).ToString(),
                                    Establecimiento = 1,
                                    IdMaquina = "001",
                                    TipoDocumento = CONSTANTES.GUATEFAC.DOCUMENTO.FACTURA,
                                    // Receptor
                                    NITReceptor = item.NIT,
                                    Nombre = item.NOMBRE,
                                    Direccion = item.DIRECCION,
                                    //InfoDoc
                                    TipoVenta = item.TIPOVTA,
                                    DestinoVenta = item.DESTINO,
                                    Fecha = blTests ? DateTime.Today : item.FECHA,
                                    Moneda = item.MONEDA,
                                    Tasa = item.TASACAMBIO,

                                    //Totales
                                    Bruto = item.BRUTO,
                                    Descuento = item.DESCUENTO,
                                    Neto = item.NETO,
                                    Iva = item.IVA,
                                    Total = item.TOTAL
                                    //Detalles
                                    ,
                                    SerieAdmin = item.DASERIE.Substring(0, item.DASERIE.IndexOf("-")),
                                    NumeroAdmin = item.PREIMPRESO.Substring(item.PREIMPRESO.IndexOf("-") + 1),
                                    Detalle = detalles,
                                    Referencia = item.REFERENCIA,
                                    Reversion = "S"
                                };

                            }
                            else
                            {
                                documento = new Doc
                                {
                                    ///información del emisor
                                    NITEmisor = int.Parse(qConsultaEmisor.FirstOrDefault().NIT).ToString(),
                                    Establecimiento = 1,
                                    IdMaquina = "001",
                                    TipoDocumento = (item.TIPO_ORIGEN.Equals("NC") ? CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_CREDITO : (item.TIPO_ORIGEN.Equals("ND") ? CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_DEBITO : CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_ABONO)),
                                    // Receptor
                                    NITReceptor = item.NIT,
                                    Nombre = item.NOMBRE,
                                    Direccion = item.DIRECCION,
                                    //InfoDoc
                                    TipoVenta = item.TIPOVTA,
                                    DestinoVenta = item.DESTINO,
                                    Fecha = item.FECHA,
                                    Moneda = item.MONEDA,
                                    Tasa = item.TASACAMBIO,

                                    //Totales
                                    Bruto = item.BRUTO,
                                    Descuento = item.DESCUENTO,
                                    Neto = item.NETO,
                                    Iva = item.IVA,
                                    Total = item.TOTAL
                                        //Detalles
                                        ,
                                    Detalle = detalles,
                                    SerieAdmin = item.DASERIE.Substring(0, item.DASERIE.IndexOf("-")),
                                    NumeroAdmin = item.PREIMPRESO.Substring(item.PREIMPRESO.IndexOf("-") + 1),
                                    Referencia = item.REFERENCIA,
                                    Reversion = "S"
                                };
                            }
                            //AGREGAMOS LOS DOCUMENTOS
                            lstDocumentos.Add(documento);
                        }
                    }
                mRespuesta.Objeto = lstDocumentos;
                mRespuesta.Exito = true;
            }
            catch (Exception ex)
            {
                log.Error("Error al obtener los documentos con número de contingencia | Acceso, previo a enviarlos a firmar electrónicamente." + ex.Message + "|" + ex.InnerException);
                mRespuesta.Exito = false;
            }
            return mRespuesta;
        }

        public List<ImpDoc> ObtenerDocumentoFirmado(string NumDoc)
        {
            ImpDoc documento = new ImpDoc();
            List<ImpDoc> docs = new List<ImpDoc>();

            log.Debug("Obteniendo información del documento " + NumDoc);

            try
            {
                decimal nCantidad = 1;
                var qConsultaEmisor = (from mf_g in dbExactus.MF_GFaceConfigura select mf_g);
                var qConsultaContingencia = (from cCont in dbExactus.MF_ContingenciaFEL where cCont.ValorDeAcceso.Equals(NumDoc) && cCont.NumFactura == null select cCont);
                var qConsultaContingenciaPast = (from cCont in dbExactus.MF_ContingenciaFEL where cCont.NumFactura.Equals(NumDoc) && cCont.FechaContingenciaIni != null select cCont);
                var MensajeCong = qConsultaContingencia.Count() > 0 ? CONSTANTES.IMPRESION.MENSAJE_CONTINGENCIA : null;
                var NumAcceso = qConsultaContingenciaPast.Count() > 0 ? qConsultaContingenciaPast.FirstOrDefault().ValorDeAcceso : null;
                #region "Encabezado"
                var  qConsulta = (
                                    from fa in dbExactus.FACTURA
                                    join fac in dbExactus.MF_Factura on fa.FACTURA1 equals fac.FACTURA
                                    join fl in dbExactus.FACTURA_LINEA on fa.FACTURA1 equals fl.FACTURA
                                    join ar in dbExactus.ARTICULO on fl.ARTICULO equals ar.ARTICULO1
                                    join co in dbExactus.MF_Cobrador on fa.COBRADOR equals co.COBRADOR
                                    join cli in dbExactus.CLIENTE on fa.CLIENTE equals cli.CLIENTE1
                                    join mfCli in dbExactus.MF_Cliente on fa.CLIENTE equals mfCli.CLIENTE
                                    join pe in dbExactus.PEDIDO on  fa.PEDIDO equals pe.PEDIDO1
                                    where
                                    //BUSCAMOS UN DOCUMENTO
                                    fa.FACTURA1 == NumDoc
                                    //&& fa.ANULADA != "S"
                                    && fl.TIPO_DOCUMENTO == fa.TIPO_DOCUMENTO
                                    && (fl.TIPO_LINEA == Const.TIENDA_VIRTUAL.TIPO_LINEA_ARTICULO_NORMAL || fl.TIPO_LINEA == Const.TIENDA_VIRTUAL.TIPO_LINEA_ARTICULO_SET)
                                    && co.ACTIVO == "S"
                                    && fa.RUBRO5 != null // que esté firmada electrónicamente
                                    select new
                                    {
                                        //empresa
                                        RazonSocial = CONSTANTES.IMPRESION.RAZON_SOCIAL,
                                        NombreEmpresa = CONSTANTES.IMPRESION.NOMBRE_EMPRESA,
                                        DireccionEmpresa = co.DIRECCION,
                                        TelefonoEmpresa = co.TELEFONO1,
                                        NitEmpresa = CONSTANTES.GUATEFAC.EMISOR.NIT,
                                        Tienda=fa.COBRADOR,
                                        OrdenDeCompra=pe.OBSERVACIONES,
                                        Cliente=pe.CLIENTE,
                                        //cliente
                                        NIT = mfCli.NIT_FACTURA,
                                        NOMBRE = fac.NOMBRE_FEL,
                                        DIRECCION = fac.DIRECCION_FEL,
                                        DOCUMENTO = fa.FACTURA1,
                                        TIPO = fa.TIPO_DOCUMENTO.Equals("F") ? CONSTANTES.GUATEFAC.DOCUMENTO.FACTURA : (fa.TIPO_DOCUMENTO.Equals("D") && EntityFunctions.DiffDays((from no in dbExactus.FACTURA where no.FACTURA1.Equals(fa.FACTURA_ORIGINAL) select no).FirstOrDefault().FECHA, fa.FECHA) >= 61 ? CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_ABONO : CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_CREDITO),
                                        fa.FECHA,
                                        BRUTO = (fa.TOTAL_FACTURA),
                                        DESCUENTO = fl.DESC_TOT_LINEA,
                                        NETO = fa.TOTAL_MERCADERIA,
                                        IVA = (fa.TOTAL_IMPUESTO1),
                                        GRANTOTAL = fa.TOTAL_FACTURA,
                                        ANULADA = fa.ANULADA,

                                        CertificadorFel = CONSTANTES.IMPRESION.CERTIFICADOR_FEL,
                                        NItCertificadorFel = CONSTANTES.IMPRESION.NIT_CERTIFICADOR_FEL,
                                        MensajeContingencia = MensajeCong,
                                        FirmaFEl = fa.RUBRO5,
                                        //DETALLE
                                        PRODUCTO = ar.ARTICULO1,
                                        DESCRIPCION = fl.DESCRIPCION,
                                        CANTIDAD = fl.CANTIDAD,
                                        PRECIO = fl.CANTIDAD>0?((decimal)(fl.PRECIO_TOTAL + fl.TOTAL_IMPUESTO1)+fl.DESC_TOT_LINEA)/fl.CANTIDAD: ((decimal)(fl.PRECIO_TOTAL + fl.TOTAL_IMPUESTO1) + fl.DESC_TOT_LINEA),
                                        IMpIVA = fl.TOTAL_IMPUESTO1,
                                        IMpTOTAL = (decimal)(fl.PRECIO_TOTAL) + fl.TOTAL_IMPUESTO1,
                                        Moneda = fa.MONEDA_FACTURA,
                                        //DOCUMENTO ASOCIADO
                                        DASerie = fa.TIPO_DOCUMENTO.Equals("D") ? fa.FACTURA_ORIGINAL : "",
                                        DAPreimpreso = fa.TIPO_DOCUMENTO.Equals("D") ? fa.FACTURA_ORIGINAL : ""

                                    }

                                                       
                                 ).Concat
                                 (
                                    from cc in dbExactus.DOCUMENTOS_CC

                                    join cli in dbExactus.CLIENTE on cc.CLIENTE equals cli.CLIENTE1
                                    join mfCli in dbExactus.MF_Cliente on cli.CLIENTE1 equals mfCli.CLIENTE
                                    join co in dbExactus.MF_Cobrador on cc.COBRADOR equals co.COBRADOR
                                    where
                                    //BUSCAMOS UN DOCUMENTO
                                    cc.DOCUMENTO == NumDoc
                                    && (cc.TIPO.Contains("N/C") || cc.TIPO.Contains("N/D"))
                                    //SOLO LOS DOCUMENTOS ACTIVOS, LAS ANULACIONES SE HACEN EN OTRO SERVICIO
                                    && cc.PAQUETE.Equals("CC") 
                                    //&& cc.ANULADO != "S"
                                   && cc.DOCUMENTO_GLOBAL != null //que este firmado electrónicamente

                                    select new
                                    {
                                        //encabezado
                                        RazonSocial = CONSTANTES.IMPRESION.RAZON_SOCIAL,
                                        NombreEmpresa = CONSTANTES.IMPRESION.NOMBRE_EMPRESA,
                                        DireccionEmpresa = co.DIRECCION,
                                        TelefonoEmpresa = co.TELEFONO1,
                                        NitEmpresa = CONSTANTES.GUATEFAC.EMISOR.NIT,
                                        Tienda=co.COBRADOR,
                                        OrdenDeCompra = string.Empty,
                                        Cliente = cli.CLIENTE1,
                                        //cliente
                                        NIT = cli.CONTRIBUYENTE,
                                        NOMBRE = cc.U_CONCEPTO_PAGO != null? cc.U_CONCEPTO_PAGO : cli.NOMBRE,
                                        DIRECCION = cc.OBSERVACIONES_COBRO !=null? cc.OBSERVACIONES_COBRO: (mfCli.CALLE_AVENIDA_FACTURA.Trim() + " " + mfCli.CASA_FACTURA.Trim() + " zona " + mfCli.ZONA_FACTURA.Trim() + " " + mfCli.APARTAMENTO_FACTURA.Trim() + " " + mfCli.COLONIA_FACTURA.Trim() + " " + mfCli.MUNICIPIO_FACTURA.Trim() + " " + mfCli.DEPARTAMENTO_FACTURA.Trim()),
                                        DOCUMENTO = cc.DOCUMENTO,
                                        TIPO = (cc.TIPO.Replace("/", "").Equals("NC") ? CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_CREDITO : CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_DEBITO),
                                        FECHA = cc.FECHA,
                                        BRUTO = cc.MONTO,
                                        DESCUENTO = cc.DESCUENTO,
                                        NETO = cc.SUBTOTAL,
                                        IVA = cc.IMPUESTO1,
                                        GRANTOTAL = cc.MONTO,
                                        ANULADA=cc.ANULADO
                                         ,
                                        CertificadorFel = CONSTANTES.IMPRESION.CERTIFICADOR_FEL,
                                        NItCertificadorFel = CONSTANTES.IMPRESION.NIT_CERTIFICADOR_FEL,
                                        MensajeContingencia = MensajeCong,
                                        FirmaFEl = cc.DOCUMENTO_GLOBAL,
                                        //DETALLE

                                        PRODUCTO = cc.DOCUMENTO,
                                        DESCRIPCION = cc.APLICACION.Replace("|", "").Replace("\\", "").Replace("''", ""),
                                        CANTIDAD = nCantidad,
                                        PRECIO = (cc.SUBTOTAL + cc.IMPUESTO1),
                                        IMpIVA = cc.IMPUESTO1,
                                        IMpTOTAL = (cc.SUBTOTAL + cc.IMPUESTO1),
                                        Moneda = cc.MONEDA.Equals("GTQ")? "L" :"D",
                                        //--------
                                        //SI ES UNA NOTA DE CREDITO SE OBTIENE LA FACTURA QUE ESTARÁ SIENDO AFECTADA EN LA TABLA AUTILIAR_CC EN LA COLUMNA CREDITO, DE LO CONTRARIO PARA UNA NOTA DE DEBITO, SE DEBE HACER JOIN EN LA COLUMNA DEBITO
                                        // OJO QUE ESTE DATO APARECE CUANDO SE "APLICA" EL DOCUMENTO EN EXACTUS (ESTE PROCESO ES MANUAL E INDEPENDIENTE DEL INGRESO DEL DOCUMENTO ASI QUE HAY QUE ESTAR PENDIENTE QUE PUEDEN NO APLICAR LA NOTA DE CREDITO/DEBITO)
                                        //-------
                                        DASerie = cc.TIPO.Replace("/", "").Equals("NC") ? (from aux_cc in dbExactus.AUXILIAR_CC where aux_cc.CREDITO == cc.DOCUMENTO select aux_cc).FirstOrDefault().DEBITO
                                                    : (from aux_cc in dbExactus.AUXILIAR_CC where aux_cc.DEBITO == cc.DOCUMENTO select aux_cc).FirstOrDefault().CREDITO,
                                        DAPreimpreso = cc.TIPO.Replace("/", "").Equals("NC") ? (from aux_cc in dbExactus.AUXILIAR_CC where aux_cc.CREDITO == cc.DOCUMENTO select aux_cc).FirstOrDefault().DEBITO
                                                    : (from aux_cc in dbExactus.AUXILIAR_CC where aux_cc.DEBITO == cc.DOCUMENTO select aux_cc).FirstOrDefault().CREDITO
                                    }
                                  
                                  );
                                        //para devoluciones
                                    if (qConsulta.Count() == 0)
                                    {
                                    qConsulta = (from fa in dbExactus.FACTURA
                                                 join fl in dbExactus.FACTURA_LINEA on fa.FACTURA1 equals fl.FACTURA
                                                 join ar in dbExactus.ARTICULO on fl.ARTICULO equals ar.ARTICULO1
                                                 join co in dbExactus.MF_Cobrador on fa.COBRADOR equals co.COBRADOR
                                                 join cli in dbExactus.CLIENTE on fa.CLIENTE equals cli.CLIENTE1
                                                 join mfCli in dbExactus.MF_Cliente on fa.CLIENTE equals mfCli.CLIENTE

                                                 where
                                                //BUSCAMOS UN DOCUMENTO
                                                fa.FACTURA1 == NumDoc
                                                //&& fa.ANULADA != "S"
                                                && fl.TIPO_DOCUMENTO == fa.TIPO_DOCUMENTO
                                                && (fl.TIPO_LINEA == Const.TIENDA_VIRTUAL.TIPO_LINEA_ARTICULO_NORMAL || fl.TIPO_LINEA == Const.TIENDA_VIRTUAL.TIPO_LINEA_ARTICULO_SET)
                                                && co.ACTIVO == "S"
                                                && fa.RUBRO5 != null // que esté firmada electrónicamente
                                                 select new
                                                     {
                                                         //empresa
                                                         RazonSocial = CONSTANTES.IMPRESION.RAZON_SOCIAL,
                                                         NombreEmpresa = CONSTANTES.IMPRESION.NOMBRE_EMPRESA,
                                                         DireccionEmpresa = co.DIRECCION,
                                                         TelefonoEmpresa = co.TELEFONO1,
                                                         NitEmpresa = CONSTANTES.GUATEFAC.EMISOR.NIT, 
                                                         Tienda=fa.COBRADOR,
                                                        OrdenDeCompra =string.Empty,
                                                        Cliente =fa.CLIENTE,
                                                     //cliente
                                                     NIT = cli.CONTRIBUYENTE,
                                                         NOMBRE = cli.NOMBRE,
                                                         DIRECCION = (mfCli.CALLE_AVENIDA_FACTURA.Trim() + " " + mfCli.CASA_FACTURA.Trim() + " zona " + mfCli.ZONA_FACTURA.Trim() + " " + mfCli.APARTAMENTO_FACTURA.Trim() + " " + mfCli.COLONIA_FACTURA.Trim() + " " + mfCli.MUNICIPIO_FACTURA.Trim() + " " + mfCli.DEPARTAMENTO_FACTURA.Trim()),
                                                         DOCUMENTO = fa.FACTURA1,
                                                         TIPO = fa.TIPO_DOCUMENTO.Equals("F") ? CONSTANTES.GUATEFAC.DOCUMENTO.FACTURA : (fa.TIPO_DOCUMENTO.Equals("D") && EntityFunctions.DiffDays((from no in dbExactus.FACTURA where no.FACTURA1.Equals(fa.FACTURA_ORIGINAL) select no).FirstOrDefault().FECHA, fa.FECHA) >= 61 ? CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_ABONO : CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_CREDITO),
                                                         fa.FECHA,
                                                         BRUTO = (fa.TOTAL_FACTURA),
                                                         DESCUENTO = fl.DESC_TOT_LINEA,
                                                         NETO = fa.TOTAL_MERCADERIA,
                                                         IVA = (fa.TOTAL_IMPUESTO1),
                                                         GRANTOTAL = fa.TOTAL_FACTURA,
                                                         ANULADA = fa.ANULADA,

                                                         CertificadorFel = CONSTANTES.IMPRESION.CERTIFICADOR_FEL,
                                                         NItCertificadorFel = CONSTANTES.IMPRESION.NIT_CERTIFICADOR_FEL,
                                                         MensajeContingencia = MensajeCong,
                                                         FirmaFEl = fa.RUBRO5,
                                                         //DETALLE
                                                         PRODUCTO = ar.ARTICULO1,
                                                         DESCRIPCION = fl.DESCRIPCION,
                                                         CANTIDAD = fl.CANTIDAD,
                                                         PRECIO = ((decimal)(fl.PRECIO_TOTAL + fl.TOTAL_IMPUESTO1) / fl.CANTIDAD)+fl.DESC_TOT_LINEA,
                                                         IMpIVA = fl.TOTAL_IMPUESTO1,
                                                         IMpTOTAL = (decimal)(fl.PRECIO_TOTAL) + fl.TOTAL_IMPUESTO1,
                                                         Moneda = fa.MONEDA_FACTURA,
                                                         //DOCUMENTO ASOCIADO
                                                         DASerie = fa.TIPO_DOCUMENTO.Equals("D") ? fa.FACTURA_ORIGINAL : "",
                                                         DAPreimpreso = fa.TIPO_DOCUMENTO.Equals("D") ? fa.FACTURA_ORIGINAL : ""

                                                     }
                                                    );

                                    }
                #endregion
                ImpDoc impDoc = null;

                if (qConsulta.Count() > 0)
                {
                    //validar el total del detalle vrs total del documento
                    decimal TotalDet = 0;

                    string CantLetras = string.Empty;
                    
                    foreach (var item in qConsulta)
                    {
                        decimal nDiferencia = 0;
                        if (qConsulta.Count() == 1)
                            nDiferencia = item.GRANTOTAL - item.IMpTOTAL;
                        
                        try
                        {
                            CantLetras = (from s in dbExactus.MF_GFace where s.DOCUMENTO == item.DOCUMENTO select s).FirstOrDefault().NUMEROS_LETRAS;
                        }
                        catch
                        {
                            log.Error("Problema al obtener la cantidad en letras del documento " + item.DOCUMENTO + " para la impresión de la factura electrónica");
                        }
                        TotalDet += item.IMpTOTAL;

                        impDoc = new ImpDoc
                        {
                            RazonSocial = item.RazonSocial,
                            NombreEmpresa = item.NombreEmpresa,
                            DireccionEmpresa = item.DireccionEmpresa,
                            TelefonoEmpresa = item.TelefonoEmpresa,
                            NitEmpresa = Utils.Utilitarios.FormatoNIT(item.NitEmpresa),
                            OrdenDeCompra=item.OrdenDeCompra,
                            Cliente=item.Cliente,
                             Tienda=item.Tienda,
                            ///información del emisor

                            SerieFEL = item.DOCUMENTO.Contains("-") ? item.DOCUMENTO.Substring(0, item.DOCUMENTO.IndexOf("-")) : item.DOCUMENTO.Trim(),
                            NumeroFEL = item.DOCUMENTO.Contains("-") ? item.DOCUMENTO.Substring(item.DOCUMENTO.IndexOf("-") + 1) : item.DOCUMENTO.Trim(),
                            FechaFactura = item.FECHA,
                            CantidadEnLetras = CantLetras,
                            GranTotal = item.GRANTOTAL,
                            TipoDoc = item.TIPO,
                             Anulado=item.ANULADA,
                            // Receptor

                            NitCliente = item.NIT,
                            NombreCliente = item.NOMBRE,
                            DirCliente = item.DIRECCION,

                            NombreCertificadorFEL = item.CertificadorFel,
                            NItCertificadorFEL = item.NItCertificadorFel,
                            MesajeEnContingencia = item.MensajeContingencia,
                            FirmaFEl = item.FirmaFEl,
                            Moneda=item.Moneda,
                             Descuento=item.DESCUENTO,
                            //detalle

                            Producto = item.PRODUCTO,
                            Descripcion = item.DESCRIPCION,
                            Cantidad = item.CANTIDAD,
                            Precio = Math.Round(item.PRECIO+nDiferencia, 2, MidpointRounding.AwayFromZero),
                            TotalProducto =  item.IMpTOTAL+nDiferencia,
                            //documento asociado
                            SeriePreimpreso = item.DASerie.Contains("-") ? item.DASerie.Substring(0, item.DASerie.IndexOf("-")) : item.DASerie.Trim(),
                            NumeroPreimpreso = item.DAPreimpreso.Contains("-") ? item.DAPreimpreso.Substring(item.DAPreimpreso.IndexOf("-") + 1) : item.DAPreimpreso.Trim()
                            //
                            , ValorDeAcceso = NumAcceso
                        };


                        docs.Add(impDoc);
                    }

                    //if (impDoc.GranTotal != TotalDet && (Math.Abs(impDoc.GranTotal - TotalDet) < 1))
                    //{
                    //    decimal ndif = impDoc.GranTotal - TotalDet;
                    //    docs[docs.Count - 1].TotalProducto = docs[docs.Count - 1].TotalProducto + ndif;
                    //}

                    return docs;

                }
            } catch (Exception ex)
            {
                log.Error("Error al obtener los documentos para firmar electrónicamente. " + CatchClass.ExMessage(ex, "DALFel", "ObtenerDocumentoFirmado()"));
            }
            return docs;
        }
        #endregion

        #region "Errores de respuesta FEL"

        /// <summary>
        /// Método para devolver el código de error de GuateFacturas
        /// para tomar determinadas acciones dependiendo de este código
        /// </summary>
        /// <param name="ErrorFirma"></param>
        /// <returns></returns>
        private int CodigoErrorFirmaElectronica(string ErrorFirma)
        {
            try
            {
                string msgError = ErrorFirma.Replace("<Resultado>", "").Replace("</Resultado>", "");
                return int.Parse(msgError.Substring(0, msgError.IndexOf("-")).Trim());
            }
            catch (Exception ex)
            {
                log.Error("Problema al castear el resultado del error al enviar documentos para la firma electrónica, el texto es: " + ErrorFirma + "| " + ex.Message);
                return 0;
            }
        }


        /// <summary>
        /// Método con las acciones a seguir dependiendo del código de error devuelto
        /// por GuateFacturas
        /// </summary>
        /// <param name="ErrorFirma"></param>
        /// <param name="docFEL"></param>
        /// <returns></returns>
        public Resultado ManejoErroresFEL(string ErrorFirma, Doc docFEL)
        {
            int CodigoError = CodigoErrorFirmaElectronica(ErrorFirma);
            Resultado mResultadoOperacion = new Resultado();
            //obtener severidad y  a quien se le notifica en la tabla catalogo

            switch (CodigoError)
            {
                case 1: break;
                case ERRORES.NO_EXISTE_EL_NIT_DEL_CONTRIBUYENTE:
                    {
                        //VOLVER A FIRMAR EL DOCUMENTO ENVIANDO COMO NitReceptor=CF
                        try
                        {
                            docFEL.NITReceptor = "CF";
                            mResultadoOperacion = ServicioFirmaElectronica(docFEL);
                            return mResultadoOperacion;
                        }
                        catch (Exception ER)
                        {
                            var json = new JavaScriptSerializer().Serialize(docFEL);
                            log.Error(string.Format("Error al intentar volver a firmar el documento [ {0} ] CAUSA: {1}", json, ER.Message));
                        }
                    }
                    break;

                default: return null;
            }

            return null;
        }
        #endregion

        #region "Firma electrónica"

        /// <summary>
        /// Método para firmar electrónicamente todos los documentos (nc/nd y facturas en contingencia) 
        /// que no estén firmadas
        /// </summary>
        /// <returns></returns>
        public Respuesta FirmarOtrosDocumentosFEL()
        {
            Respuesta mRespuesta = new Respuesta();
            string mMensajeResp = string.Empty;
            int nFirmadas = 0;
            int nNoFirmadas = 0;
            string cMensaje = string.Empty;
            int nTotalDoctos = 0;
            bool blnProcesoExitoso = true;
            List<string> lstResultados = new List<string>();
            List<string> lstErrores = new List<string>();
            List<string> lstContingenciasFirmadas = new List<string>();
            string mMensajeError = string.Empty;
            try
            {
                //**
                //Verificar que esté el módulo FEL habilitado en catalogos
                //**
                if (Utilitario.ModuloDelSistemaHabilitado(CONSTANTES.CATALOGOS.MODULO_FEL))
                {
                    //1) NOTAS DE CREDITO Y DEBITO
                    var result = ObtenerOtrosDocumentosSinFirmar();
                    if (result.Exito)
                    {
                        Resultado resultadoFirma = new Resultado();


                        List<Doc> lstDocumentos = ((List<Doc>)result.Objeto);
                        UtilitariosGuateFacturas utils = new UtilitariosGuateFacturas();
                        nTotalDoctos = lstDocumentos.Count;
                        if (lstDocumentos.Count > 0)
                            foreach (var item in lstDocumentos)
                            {
                                if (((TimeSpan)(DateTime.Today - item.Fecha)).Days <= CONSTANTES.GUATEFAC.MODALIDAD_FEL.NUMERO_DIAS_MAX_FIRMA_DOCUMENTOS)
                                {

                                    try
                                    {
                                        log.Debug("intento 1");
                                        resultadoFirma = ServicioFirmaElectronica(item);
                                        blnProcesoExitoso = true;
                                    }
                                    catch (Exception ix1)
                                    {
                                        log.Error("Error al firmar otros documentos intento (2) " + CatchClass.ExMessage(ix1, "DALFel", "FirmarOtrosDocumentosFEL()"));
                                        try
                                        {
                                            log.Debug("intento 2");
                                            resultadoFirma = ServicioFirmaElectronica(item);
                                            blnProcesoExitoso = true;
                                        }
                                        catch (Exception ix2)
                                        {
                                            log.Error("Error al firmar otros documentos intento (3) " + CatchClass.ExMessage(ix2, "DALFel", "FirmarOtrosDocumentosFEL()"));
                                            try
                                            {
                                                log.Debug("intento 3");
                                                resultadoFirma = ServicioFirmaElectronica(item);
                                                blnProcesoExitoso = true;
                                            }
                                            catch (Exception cnt)
                                            {
                                                //contingencia
                                                log.Error("Firmando otros documentos  " + CatchClass.ExMessage(cnt, "DALFel", "FirmarOtrosDocumentosFEL"));
                                                mMensajeError = CatchClass.ExMessage(cnt, "DALFel", "FirmarOtrosDocumentosFEL");
                                                blnProcesoExitoso = false;

                                            }
                                        }
                                    }
                                    if (blnProcesoExitoso)
                                    {

                                        if (resultadoFirma.FacturaElectronica != null)
                                        {
                                            nFirmadas++;
                                            string strResp = string.Format("Documento : {0}  Factura electrónica: {1}", item.Nombre + " Q." + item.Total, resultadoFirma.FacturaElectronica);
                                            lstResultados.Add(strResp);
                                            NotificacionesAlCliente(resultadoFirma.FacturaElectronica,false);
                                        }
                                        else
                                        {
                                            nNoFirmadas++;
                                            string strResp = string.Format("Documento : {0} Resultado: {1}", item.Nombre + " Q." + item.Total, resultadoFirma.Xml);
                                            lstErrores.Add(strResp);
                                        }

                                    }
                                    else
                                    {
                                        nNoFirmadas++;
                                        string strResp = string.Format("Documento : {0} Resultado: {1} ", item.Nombre + " Q." + item.Total, mMensajeError);
                                        lstErrores.Add(strResp);
                                    }

                                }
                                else
                                {
                                    nNoFirmadas++;
                                    string strResp = string.Format("Documento : {0} Resultado: {1} ", item.Nombre + " Q." + item.Total, "No se puede firmar, porque fué emitida hace más de " + CONSTANTES.GUATEFAC.MODALIDAD_FEL.NUMERO_DIAS_MAX_FIRMA_DOCUMENTOS + " días.");
                                    lstErrores.Add(strResp);

                                }
                                mMensajeResp = string.Format("Total de [Notas de crédito y débito]: Firmadas {0} | No Firmadas {1} de un total de {2}", nFirmadas, nNoFirmadas, lstDocumentos.Count());

                            }
                        else
                        {
                            //cuando hubo un error en la obtención de los documentos
                            mMensajeResp = result.Mensaje;
                        }
                        //2) FACTURAS CON CONTINGENCIA
                        //obtener documentos con contingencia para firmarlos correctamente
                        var resultContingencia = ObtenerDocumentosEnContingencia();
                        if (resultContingencia.Exito)
                        {
                            resultadoFirma = new Resultado();
                            nFirmadas = 0;
                            nNoFirmadas = 0;

                            lstDocumentos = ((List<Doc>)resultContingencia.Objeto);
                            utils = new UtilitariosGuateFacturas();
                            if (lstDocumentos.Count > 0)
                            {
                                nTotalDoctos += lstDocumentos.Count;

                                foreach (var item in lstDocumentos)
                                {

                                    try
                                    {

                                        resultadoFirma = ServicioFirmaElectronica(item);
                                        blnProcesoExitoso &= true;
                                    }
                                    catch (Exception ex)
                                    {
                                        log.Error("valores en contingencia firmarlos intento (2) " + ex.Message);
                                        try
                                        {
                                            resultadoFirma = ServicioFirmaElectronica(item);
                                            blnProcesoExitoso &= true;
                                        }
                                        catch
                                        {
                                            try
                                            {
                                                log.Error("Valores en contingencia firmarlos intento (3)");
                                                resultadoFirma = ServicioFirmaElectronica(item);
                                                blnProcesoExitoso &= true;
                                            }
                                            catch (Exception es)
                                            {
                                                log.Error("Error al intentar firmar documentos que tienen número de acceso (contingencia), luego de 3 intentos " + es.Message + "|" + es.InnerException);
                                                blnProcesoExitoso = false;
                                                lstErrores.Add(string.Format(" Documento : {0} Resultado: {1} Error de Comunicación {2}", item.NumeroAcceso, es.Message, resultadoFirma.ErrorComunicacion));
                                            }
                                        }

                                    }

                                    if (blnProcesoExitoso)
                                    {
                                        if (resultadoFirma.FacturaElectronica != null)
                                        {
                                            nFirmadas++;
                                            string strResp = string.Format("ValorDeAcceso : {0} Factura electrónica: {1}", item.NumeroAcceso, resultadoFirma.FacturaElectronica);
                                            lstResultados.Add(strResp);
                                            NotificacionesAlCliente(resultadoFirma.FacturaElectronica);
                                            lstContingenciasFirmadas.Add(string.Format("{2}|{1}|{0}</td><td>{2}</td></tr>",item.NumeroAcceso,item.Establecimiento,resultadoFirma.FacturaElectronica));
                                        }
                                        else
                                        {
                                            nNoFirmadas++;
                                            string strResp = string.Format("ValorDeAcceso : {0} Resultado: {1}", item.NumeroAcceso, resultadoFirma.Xml);
                                            lstErrores.Add(strResp);
                                        }

                                    }
                                    else
                                    {
                                        nNoFirmadas++;
                                        string strResp = string.Format("ValorDeAcceso : {0} Resultado: {1}", item.NumeroAcceso, mMensajeError);
                                        lstErrores.Add(strResp);
                                    }
                                }
                                mMensajeResp += string.Format("\n\t Total de [facturas en contingencia]: Firmadas {0} | No Firmadas {1} de un total de {2}", nFirmadas, nNoFirmadas, lstDocumentos.Count());
                            }
                            if (lstContingenciasFirmadas.Count > 0)
                            {
                                EnviarMailATienda(lstContingenciasFirmadas);
                                
                            }
                        }

                        //
                        var resultAnulados = ObtenerDocumentosAnulados();
                        if (resultAnulados.Exito)
                        {
                            resultadoFirma = new Resultado();
                            nFirmadas = 0;
                            nNoFirmadas = 0;

                            lstDocumentos = ((List<Doc>)resultAnulados.Objeto);
                            utils = new UtilitariosGuateFacturas();
                            if (lstDocumentos.Count > 0)
                            {
                                nTotalDoctos += lstDocumentos.Count;

                                foreach (var item in lstDocumentos)
                                {

                                    try
                                    {

                                        resultadoFirma = ServicioAnulacionDocumentos(item);
                                        blnProcesoExitoso &= true;
                                    }
                                    catch (Exception ex)
                                    {
                                        log.Error("valores en contingencia firmarlos intento (2) " + ex.Message);
                                        try
                                        {
                                            resultadoFirma = ServicioAnulacionDocumentos(item);
                                            blnProcesoExitoso &= true;
                                        }
                                        catch
                                        {
                                            try
                                            {
                                                log.Error("Valores en contingencia firmarlos intento (3)");
                                                resultadoFirma = ServicioAnulacionDocumentos(item);
                                                blnProcesoExitoso &= true;
                                            }
                                            catch (Exception es)
                                            {
                                                log.Error("Error al intentar anular documentos, luego de 3 intentos " + es.Message + "|" + es.InnerException);
                                                blnProcesoExitoso = false;
                                                lstErrores.Add(string.Format(" Documento : {0} Resultado: {1} ", item.SerieAdmin + "-" + item.NumeroAdmin, es.Message));
                                            }
                                        }

                                    }

                                    if (blnProcesoExitoso)
                                    {
                                        if (resultadoFirma.NumeroAutorizacion != null)
                                        {
                                            nFirmadas++;
                                            string strResp = string.Format("Documento : {0} Resultado: {1}", item.Nombre+" Q."+item.Total, resultadoFirma.NumeroAutorizacion);
                                            lstResultados.Add(strResp);
                                        }
                                        else
                                        {
                                            nNoFirmadas++;
                                            string strResp = string.Format("Documento : {0} Resultado: {1}", item.Nombre + " Q." + item.Total, resultadoFirma.Xml);
                                            lstErrores.Add(strResp);
                                        }

                                    }
                                    else
                                    {
                                        nNoFirmadas++;
                                        string strResp = string.Format("Documento : {0} Resultado: {1}", item.Nombre + " Q." + item.Total, mMensajeError);
                                        lstErrores.Add(strResp);
                                    }
                                }
                                mMensajeResp += string.Format("\n\t Total de [Documentos anulados]: Firmadas {0} | No Firmadas {1} de un total de {2}", nFirmadas, nNoFirmadas, lstDocumentos.Count());
                            }
                        }
                        //
                        mRespuesta.Mensaje = mMensajeResp;
                        mRespuesta.Objeto = lstResultados;
                        mRespuesta.Exito = blnProcesoExitoso;


                        //EnviarMail
                        EnviarMailInterno(lstResultados, lstErrores, nTotalDoctos);
                    }
                }
            }

            catch (Exception ex)
            {
                log.Error(CatchClass.ExMessage(ex, "DALFel", "FirmarOtrosDocumentosFEL"));
                mRespuesta.Exito = false;
                mRespuesta.Mensaje = CatchClass.ExMessage(ex, "DALFel", "FirmarOtrosDocumentosFEL");
            }
            return mRespuesta;
        }

        private void EnviarMailATienda(List<string> lstContingenciasF)
        {
            try
            {
                var blTest = (from bl in dbExactus.MF_Catalogo where bl.CODIGO_TABLA.Equals(CONSTANTES.CATALOGOS.GUATEFACTURAS_FEL) && bl.CODIGO_CAT.Equals("PRODUCCION_FEL") select bl).FirstOrDefault().NOMBRE_CAT.Equals("N");
                var qTiendas = (from c in dbExactus.MF_Cobrador where c.ACTIVO.Equals("S") select c);
                
                foreach (var q in qTiendas)
                {
                    string mCuerpoCorreo = Utilitario.CorreoInicioFormatoAzulYGris();
                    var lstFacturasxTienda = lstContingenciasF.Where(x=>x.Split('|')[1].Equals(q.ESTABLECIMIENTO.ToString())).ToList();//FindAll(x => x.Split('|')[1].Equals(q.ESTABLECIMIENTO));
                    if (lstFacturasxTienda.Count > 0)
                    {
                        string mFactura = lstFacturasxTienda[0].Split('|')[0];
                        var qVendedor = (from v in dbExactus.MF_Vendedor join f in dbExactus.FACTURA on v.VENDEDOR equals f.VENDEDOR where f.FACTURA1 == mFactura select v);
                        if (qVendedor != null)
                        {
                            var MailVendedor = blTest ? "patricia.rodriguez@mueblesfiesta.com" : qVendedor.FirstOrDefault().EMAIL;
                            var NombreVendedor =  qVendedor.FirstOrDefault().NOMBRE + " " + qVendedor.FirstOrDefault().APELLIDO;
                            var MailRemitente = (from conf in dbExactus.MF_Catalogo where conf.CODIGO_TABLA.Equals(CONSTANTES.CATALOGOS.GUATEFACTURAS_FEL) && conf.CODIGO_CAT.Equals("REMITENTE CORREO") select conf).FirstOrDefault().NOMBRE_CAT;

                            mCuerpoCorreo += "<br/>Estimado:" + NombreVendedor + " <br/><br/>";
                            mCuerpoCorreo += "Las siguientes facturas en contingencia han sido firmadas:<br/>";
                            mCuerpoCorreo += "<br/><center><table><tr>" + Utilitario.FormatoTituloCentro("Número de Contingencia") + Utilitario.FormatoTituloCentro("Factura electrónica")+"</tr>";

                            foreach (var item in lstFacturasxTienda)
                            {
                                mCuerpoCorreo += "<tr><td>" + item.Split('|')[2];

                            }
                            mCuerpoCorreo += "</table></center><br/><br/> Saludos</body></html>";
                            mCuerpoCorreo = mCuerpoCorreo.Replace("#428BCA", "#f44336");
                            StringBuilder mBody = new StringBuilder();
                            mBody.Append(mCuerpoCorreo);
                            Utils.Utilitarios SL = new Utils.Utilitarios();
                           var resp= SL.EnviarEmail(MailRemitente, MailVendedor, "FIRMA ELECTRÓNICA PARA FACTURAS EN CONTINGENCIA", mBody, "MUEBLES FIESTA - NOTIFICACIONES");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(CatchClass.ExMessage(ex,"DALFel","EnviarMailATienda()"));
            }
        }
       
        //Método para llamar al servico de GuateFActuras para firmar electrónicamente y
        //guardar los resultados de la firma
        //
        private Resultado ServicioFirmaElectronica(Doc docSolicitud)
        {
            UtilitariosGuateFacturas utils = new UtilitariosGuateFacturas();
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            string cCobrador = string.Empty;
            string NombreSucursal = string.Empty;
            string DireccionSucursal = string.Empty;
            string DeptoSucursal = string.Empty;
            string MuniSucursal = string.Empty;
            string Tipo = string.Empty;
            string DirCliente = string.Empty;
            string NombreCliente = string.Empty;
            string DeptoCliente = string.Empty;
            string MuniCliente = string.Empty;
            string cCliente = string.Empty;
            string Vendedor = string.Empty;
            string DirVendedor = string.Empty;
            int nEstablecimiento = 0;
            Resultado resultadoFirma = new Resultado();
            string mCantidadLetras = string.Empty;
            string cResolucion = string.Empty;
            string cFolioIni = string.Empty;
            string cFolioFin = string.Empty;
            DateTime dtFechaResolucion=DateTime.Now;
            var json = new JavaScriptSerializer().Serialize(docSolicitud);
            bool blnuevo = false;

            string cDocumentoOriginal = string.Empty;
            try
            {
                cDocumentoOriginal= docSolicitud.SerieAdmin.Equals("") ? docSolicitud.NumeroAdmin : docSolicitud.SerieAdmin + "-" + docSolicitud.NumeroAdmin;
            } catch
            { }

            //PARA FACTURAS QUE ESTABAN EN CONTINGENCIA, EL NÚMERO QUE SE BUSCARÁ
            //SERÁ EL NÚMERO DE ACCESO COMO DOCUMENTO original
            if (docSolicitud.NumeroAcceso != null && docSolicitud.TipoDocumento != DOCUMENTO.NOTA_ABONO
                    && docSolicitud.TipoDocumento != DOCUMENTO.NOTA_CREDITO
                    && docSolicitud.TipoDocumento != DOCUMENTO.NOTA_DEBITO)
            {
               
                cDocumentoOriginal = docSolicitud.NumeroAcceso;
                docSolicitud.Referencia = cDocumentoOriginal;
            }

            //----------(0)--- REFERENCIA---------------
            //-----------------------------------------------
            var qMFfac0 = (from f in db.FACTURA where f.FACTURA1.Equals(cDocumentoOriginal) select f);
            //if (qMFfac0.Count() > 0 && qMFfac0.First().RUBRO4 !=null)
            //{
               
            //    docSolicitud.Referencia = qMFfac0.First().RUBRO4.ToString();
            //}

            var qreferencia = (from s in db.CONSECUTIVO_FA where s.CODIGO_CONSECUTIVO.Equals("FEL") select s);

            if (docSolicitud.Referencia == "" || docSolicitud.Referencia ==null)
            {
                if (qreferencia.Count() > 0)
                {
                    blnuevo = true;
                    docSolicitud.Referencia =  "NC-" + (long.Parse(qreferencia.First().VALOR_CONSECUTIVO) + 1).ToString();
                }
            }


            //----------(1)---FIRMAR EL DOCUMENTO---------------
            //-----------------------------------------------

            resultadoFirma = utils.Servicio(SERVICIOS.GENERAR_DOCUMENTO, docSolicitud);
            log.Debug("resultado firma: " + resultadoFirma.Xml + " autorización: [" + resultadoFirma.NumeroAutorizacion+"]");
                //------------------------------------------------
                //------------------------------------------------

                if (resultadoFirma.NumeroAutorizacion.Trim() != string.Empty)
                {
                    
                    //obtener el cobrador de la factura (1) o de la nota de crédito|débito (2)

                    var qCobrador = (from c in dbExactus.MF_Cobrador join f in dbExactus.MF_Factura on c.COBRADOR equals f.COBRADOR where (f.FACTURA == docSolicitud.NumeroAcceso || f.FACTURA == cDocumentoOriginal) select c)
                                    .Concat(from c in dbExactus.MF_Cobrador join cc in dbExactus.DOCUMENTOS_CC on c.COBRADOR equals cc.COBRADOR where (cc.DOCUMENTO == docSolicitud.NumeroAcceso || cc.DOCUMENTO == cDocumentoOriginal)
                                    select c);

                    if (qCobrador.Count() > 0)
                    {
                        cCobrador = qCobrador.FirstOrDefault().COBRADOR;
                        NombreSucursal = qCobrador.FirstOrDefault().NOMBRE;
                        DireccionSucursal = qCobrador.FirstOrDefault().DIRECCION;
                        DeptoSucursal = qCobrador.FirstOrDefault().DEPARTAMENTO;
                        MuniSucursal = qCobrador.FirstOrDefault().MUNICIPIO;
                        nEstablecimiento = qCobrador.FirstOrDefault().ESTABLECIMIENTO ?? 0;
                    
                    }

                    //obtener la información del cliente por medio de la factura (en contingencia) o de la nota de crédito|débito
                    var qCliente=( from c in dbExactus.MF_Cliente join f in dbExactus.MF_Factura on c.CLIENTE equals f.CLIENTE where (f.FACTURA == docSolicitud.NumeroAcceso || f.FACTURA == cDocumentoOriginal)
                                   select  c
                        ).Concat
                        (
                            from c in dbExactus.MF_Cliente join cc in dbExactus.DOCUMENTOS_CC on c.CLIENTE equals cc.CLIENTE where(cc.DOCUMENTO == docSolicitud.NumeroAcceso || cc.DOCUMENTO == cDocumentoOriginal)
                            select c
                        );

                    if (qCliente.Count() > 0)
                    {
                        cCliente = qCliente.FirstOrDefault().CLIENTE;
                        NombreCliente = qCliente.FirstOrDefault().PRIMER_NOMBRE.ToUpper();
                        DirCliente = string.Format("{0} {1} {2} Z.{3} {4}", qCliente.FirstOrDefault().CALLE_AVENIDA_FACTURA, qCliente.FirstOrDefault().CASA_FACTURA, qCliente.FirstOrDefault().APARTAMENTO_FACTURA, qCliente.FirstOrDefault().ZONA_FACTURA, qCliente.FirstOrDefault().COLONIA_FACTURA).ToUpper();
                        DeptoCliente = qCliente.FirstOrDefault().DEPARTAMENTO_FACTURA;
                        MuniCliente = qCliente.FirstOrDefault().MUNICIPIO_FACTURA;
                    }
                var qSucursal = (from x in dbExactus.MF_GFaceConfigura select x);
                if (qSucursal.Count() > 0)
                {
                    Vendedor = qSucursal.FirstOrDefault().SUCURSAL;
                    DirVendedor = qSucursal.FirstOrDefault().DIRECCION;
                }

                string mConsecutivoDoc = string.Empty;

                

                switch (docSolicitud.TipoDocumento)
                {
                    //factura crédito o factura contado
                    case DOCUMENTO.FACTURA: {
                            Tipo = "FC";
                            var qResolucion = (from c in dbExactus.MF_Factura where c.FACTURA == docSolicitud.NumeroAcceso select c);

                            if (qResolucion.Count() > 0)
                                mConsecutivoDoc = cCobrador+(qResolucion.FirstOrDefault().TIPO.Equals("CO")? RESOLUCION_DOC.CONSECUTIVO_RESOLUCION_FACTURA_CONTADO : RESOLUCION_DOC.CONSECUTIVO_RESOLUCION_FACTURA_CREDITO); //F01CRE O F01CON SERIA F##CRE O F##CON

                            } break;
                    
                    case DOCUMENTO.NOTA_ABONO: {
                            Tipo = "ND";
                            mConsecutivoDoc = RESOLUCION_DOC.CONSECUTIVO_RESOLUCION_NOTA_ABONO;

                        } break;
                    case DOCUMENTO.NOTA_DEBITO:
                        {
                            Tipo = "ND";
                            mConsecutivoDoc = RESOLUCION_DOC.CONSECUTIVO_RESOLUCION_NOTA_DEBITO;
                        }
                        break;
                   //nota de crédito
                    case DOCUMENTO.NOTA_CREDITO: {
                            Tipo = "NC";
                            mConsecutivoDoc = RESOLUCION_DOC.CONSECUTIVO_RESOLUCION_NOTA_CREDITO;

                        } break;
                }
                var qRes = (from c in dbExactus.CONSECUTIVO_FA join r in dbExactus.RESOLUCION_DOC_ELECTRONICO on c.RESOLUCION equals r.RESOLUCION where c.CODIGO_CONSECUTIVO == mConsecutivoDoc select r);
                if (qRes.Count() > 0)
                {
                    cResolucion = qRes.FirstOrDefault().RESOLUCION;
                    cFolioIni = qRes.FirstOrDefault().FOLIO_INICIAL.ToString();
                    cFolioFin = qRes.FirstOrDefault().FOLIO_FINAL.ToString();
                    dtFechaResolucion= qRes.FirstOrDefault().FECHA_RESOLUCION ?? DateTime.Now;
                    log.Debug(string.Format("Resolución {0}, cfolioIni {1}, cFolioFin {2}, FechaResolucion: {3}",cResolucion,cFolioIni,cFolioFin,dtFechaResolucion));
                }
                ObjectParameter objFuncion = new ObjectParameter("Total", docSolicitud.Total);
                
                try
                {
                    
                    int mTotal = 0;
                    if (int.TryParse(docSolicitud.Total.ToString(), out mTotal))
                        mCantidadLetras = mTotal.ToWords(System.Globalization.CultureInfo.CreateSpecificCulture("es-GT")).ToUpper();
                    else
                    {
                        mTotal = int.Parse(docSolicitud.Total.ToString().Substring(0, docSolicitud.Total.ToString().IndexOf('.')));
                        Decimal nDecimal =(Decimal)(docSolicitud.Total - mTotal);
                        int ValorCentavos = (int)(nDecimal * 100);
                        mCantidadLetras = mTotal.ToWords(System.Globalization.CultureInfo.CreateSpecificCulture("es-GT")).ToUpper() +  " CON "+ValorCentavos.ToString()+"/100";
                        
                    }
                    log.Debug("--->"+mCantidadLetras);
                }
                catch(Exception ex)
                {
                    log.Error("No se pudo convertir la cantidad del Total "+docSolicitud.Total+", a letras "+CatchClass.ExMessage(ex,"DALFel","ServicioFirmaElectronica()"));
                    string err = ex.Message;
                }
                    log.Debug("Grabando en mf_gface " + resultadoFirma.FacturaElectronica);
                
                    using (TransactionScope transactionScope = new TransactionScope())
                    {
                    
                    
                    //---------(2) guardar en mf_gface--------------

                    MF_GFace gFace = new MF_GFace
                        {
                            ANIO = docSolicitud.Fecha.Year,
                            MES = docSolicitud.Fecha.Month,
                            COBRADOR = cCobrador,
                            DOCUMENTO = resultadoFirma.FacturaElectronica,
                            NIT_VENDEDOR = docSolicitud.NITEmisor.PadLeft(12,'0'),
                            RAZON_SOCIAL = Vendedor,
                            TIPO = Tipo,
                            FECHA = docSolicitud.Fecha.ToString("yyyy-MM-ddT00:00:00"),
                            SERIE = resultadoFirma.Serie,
                            NUMERO_DOCUMENTO = resultadoFirma.Preimpreso
                           ,
                            NOMBRE_CLIENTE = docSolicitud.Nombre,
                            NIT = docSolicitud.NITReceptor.PadLeft(12, '0'),
                            DIRECCION = DirCliente,
                            DEPARTAMENTO = DeptoCliente,
                            MUNICIPIO = MuniCliente,
                            SUBTOTAL = docSolicitud.Bruto,
                            IVA = docSolicitud.Iva,
                            TOTAL = docSolicitud.Total,
                            ANULADA = "N",
                            FECHA_RESOLUCION = dtFechaResolucion.ToString("yyyy-MM-dd"),
                            NUMERO_RESOLUCION = cResolucion,
                            FOLIO_FINAL = cFolioFin,
                            FOLIO_INICIAL = cFolioIni,
                            SUCURSAL = nEstablecimiento.ToString(),
                            NOMBRE_SUCURSAL = nEstablecimiento>1 ?"MUEBLES FIESTA" : "MULTIPRODUCTOS",
                            DIRECCION_SUCURSAL = DireccionSucursal.ToUpper(),
                            DEPARTAMENTO_SUCURSAL = DeptoSucursal.ToUpper(),
                            MUNICIPIO_SUCURSAL = MuniSucursal.ToUpper(),
                            NUMEROS_LETRAS = mCantidadLetras,
                            TIPO_ACTIVO = GFACE.TIPO_ACTIVO,
                            MONEDA = docSolicitud.Moneda ==MONEDA.GTQ ? "GTQ":"USD",
                            TASA_CAMBIO = docSolicitud.Tasa,
                            CLIENTE = cCliente,
                            BIEN_SERVICIO = docSolicitud.TipoVenta.Equals("B") ? "BIEN" : "SERVICIO",
                            XML_DOCUMENTO = resultadoFirma.XmlDocumento,
                            XML_FIRMADO = resultadoFirma.Xml,
                            XML_ERROR = resultadoFirma.XmlError,
                            FIRMADO = "S",
                            FIRMA = resultadoFirma.NumeroAutorizacion,
                            FECHA_DOCUMENTO = docSolicitud.Fecha,
                            DIRECCION_VENDEDOR = DirVendedor,
                            USUARIO = "VIRTUAL",
                            FECHA_CREADO = DateTime.Now.Date,
                            FECHA_HORA_CREADO = DateTime.Now

                        };
                        db.MF_GFace.AddObject(gFace);

                        foreach (var item in docSolicitud.Detalle)
                        {
                            MF_GFaceDetalle det = new MF_GFaceDetalle
                            {
                                ANIO = DateTime.Now.Year,
                                MES = DateTime.Now.Month,
                                COBRADOR = cCobrador,//<-------COLOCAMOS EL CÓDIGO DE LA TIENDA
                                DOCUMENTO = resultadoFirma.FacturaElectronica,
                                FECHA = docSolicitud.Fecha.ToString("dd/MM/yyyy"),
                                TIPO = string.Empty,
                                SERIE = resultadoFirma.Serie,
                                NUMERO_DOCUMENTO = resultadoFirma.FacturaElectronica,
                                DESCRIPCION = item.Descripcion,
                                CANTIDAD = (int)item.Cantidad,
                                PRECIO_UNITARIO = item.Precio,
                                SUBTOTAL = item.ImpBruto,
                                IVA = item.ImpIva,
                                TOTAL = item.ImpTotal,
                                FECHA_DOCUMENTO = docSolicitud.Fecha,
                                USUARIO = "VIRTUAL",
                                FECHA_CREADO = DateTime.Now.Date,
                                FECHA_HORA_CREADO = DateTime.Now

                            };

                            db.MF_GFaceDetalle.AddObject(det);

                            
                         }

                        //validar si el documento es una factura, entonces eliminar el registro anterior ya que ya habia un registro previo
                        if (docSolicitud.TipoDocumento != DOCUMENTO.NOTA_CREDITO && docSolicitud.TipoDocumento != DOCUMENTO.NOTA_ABONO && docSolicitud.TipoDocumento != DOCUMENTO.NOTA_DEBITO)
                        {
                            var qDetD = (from det in db.MF_GFaceDetalle where det.DOCUMENTO ==docSolicitud.NumeroAcceso select det);
                            if(qDetD.Count()>0)
                                foreach(var qDet in qDetD)
                                    db.MF_GFaceDetalle.DeleteObject(qDet);


                            var qdetC = (from D in db.MF_GFace where D.DOCUMENTO == docSolicitud.NumeroAcceso select D);
                             if(qdetC.Count()>0)
                                foreach (var qDetC in qdetC)
                                    db.MF_GFace.DeleteObject(qDetC);
                        }
                        //-------------------------------------------------------------
                        //-------------(3)--FINALIZAR LA CONTINGENCIA SI FUERA EL CASO
                        //-------------------------------------------------------------


                        

                        var qContingencia = (from c in db.MF_ContingenciaFEL where c.ValorDeAcceso.Equals(docSolicitud.NumeroAcceso)  select c);
                        if (qContingencia!=null)
                            if (qContingencia.Count() > 0)
                            {
                            log.Info("Fin contingencia para el número de acceso" + docSolicitud.NumeroAcceso + " factura: " + resultadoFirma.FacturaElectronica);
                                foreach (var cDoc in qContingencia)
                                {
                                    cDoc.NumFactura = resultadoFirma.FacturaElectronica;
                                    cDoc.FechaContingenciaFin = DateTime.Now;
                                }

                            

                        }

                    //------------------------------------------------------------------------------
                    //---------(4)  actualizar el número de documento por el electrónico---
                    //-----------------------------------------------------------------------------

                    #region "cambiarle el número de factura por la factura electronica"
                    /*
                     * DECLARE @Comodin AS VARCHAR(50) = 'A-000000'
                        DECLARE @Contingencia AS VARCHAR(50) = 'F02C-007967'
                        DECLARE @Electronica AS VARCHAR(50) = 'FACE-63-FECP-001-190000000011'

                        UPDATE prodmult.FACTURA_LINEA SET FACTURA = @Comodin, DOCUMENTO_ORIGEN = @Electronica --ya
                        WHERE FACTURA = @Contingencia

                        UPDATE prodmult.FACTURA SET FACTURA = @Electronica, DOC_CREDITO_CXC = NULL, TIPO_CREDITO_CXC = NULL --ya
                        WHERE FACTURA = @Contingencia

                        UPDATE prodmult.FACTURA_LINEA SET FACTURA = @Electronica --ya
                        WHERE FACTURA = @Comodin 

                        UPDATE prodmult.mf_FACTURA SET FACTURA = @Electronica, DOC_CREDITO_CXC = NULL, TIPO_CREDITO_CXC = NULL --ya
                        WHERE FACTURA = @Contingencia

                        UPDATE prodmult.DOCUMENTOS_CC SET DOCUMENTO = @Electronica
                        WHERE DOCUMENTO = @Contingencia
                     */

                    #endregion

                    //(4.01) PEDIDO
                    var qPed = (from ped in db.MF_Pedido where ped.FACTURA.Equals(cDocumentoOriginal) select ped);
                    if (qPed.Count() > 0)
                    {
                        foreach (var p in qPed)
                            p.FACTURA = resultadoFirma.FacturaElectronica;

                    }
                    ////(4.1) FACTURA
                    var qFac = (from fa in db.FACTURA where fa.FACTURA1.Equals(cDocumentoOriginal) && fa.ANULADA != "S" select fa);
                            if (qFac.Count() > 0)
                            {
                                foreach (var fa in qFac)
                                {

                                    FACTURA fac = new FACTURA
                                    {
                                        TIPO_DOCUMENTO = fa.TIPO_DOCUMENTO,
                                        FACTURA1 = resultadoFirma.FacturaElectronica,
                                        AUDIT_TRANS_INV = fa.AUDIT_TRANS_INV,
                                        ESTA_DESPACHADO = fa.ESTA_DESPACHADO,
                                        EN_INVESTIGACION = fa.EN_INVESTIGACION,
                                        TRANS_ADICIONALES = fa.TRANS_ADICIONALES,
                                        ESTADO_REMISION = fa.ESTADO_REMISION,
                                        ASIENTO_DOCUMENTO = fa.ASIENTO_DOCUMENTO,
                                        DESCUENTO_VOLUMEN = fa.DESCUENTO_VOLUMEN,
                                        MONEDA_FACTURA = fa.MONEDA_FACTURA,
                                        COMENTARIO_CXC = fa.COMENTARIO_CXC,
                                        FECHA_DESPACHO = fa.FECHA_DESPACHO,
                                        CLASE_DOCUMENTO = fa.CLASE_DOCUMENTO,
                                        FECHA_RECIBIDO = fa.FECHA_RECIBIDO,
                                        PEDIDO = fa.PEDIDO,
                                        FACTURA_ORIGINAL = fa.FACTURA_ORIGINAL,
                                        TIPO_ORIGINAL = fa.TIPO_ORIGINAL,
                                        COMISION_COBRADOR = fa.COMISION_COBRADOR,
                                        TARJETA_CREDITO = fa.TARJETA_CREDITO,
                                        TOTAL_VOLUMEN = fa.TOTAL_VOLUMEN,
                                        NUMERO_AUTORIZA = fa.NUMERO_AUTORIZA,
                                        TOTAL_PESO = fa.TOTAL_PESO,
                                        MONTO_COBRADO = fa.MONTO_COBRADO,
                                        TOTAL_IMPUESTO1 = fa.TOTAL_IMPUESTO1,
                                        FECHA = fa.FECHA,
                                        FECHA_ENTREGA = fa.FECHA_ENTREGA,
                                        TOTAL_IMPUESTO2 = fa.TOTAL_IMPUESTO2,
                                        PORC_DESCUENTO2 = fa.PORC_DESCUENTO2,
                                        PORC_DESCUENTO1 = fa.PORC_DESCUENTO1,
                                        ANULADA = fa.ANULADA,
                                        BASE_IMPUESTO1 = fa.BASE_IMPUESTO1,
                                        BASE_IMPUESTO2 = fa.BASE_IMPUESTO2,
                                        CARGADO_CG = fa.CARGADO_CG,
                                        CARGADO_CXC = fa.CARGADO_CXC,
                                        CLIENTE = fa.CLIENTE,
                                        CLIENTE_CORPORAC = fa.CLIENTE_CORPORAC,
                                        CLIENTE_DIRECCION = fa.CLIENTE_DIRECCION,
                                        CLIENTE_ORIGEN = fa.CLIENTE_ORIGEN,
                                        COBRADA = fa.COBRADA,
                                        COBRADOR = fa.COBRADOR,
                                        COMISION_VENDEDOR = fa.COMISION_VENDEDOR,
                                        ORDEN_COMPRA = fa.ORDEN_COMPRA,
                                        FECHA_ORDEN = fa.FECHA_ORDEN,
                                        FECHA_HORA = fa.FECHA_HORA,
                                        FECHA_HORA_ANULA = fa.FECHA_HORA_ANULA,
                                        USUARIO_ANULA = fa.USUARIO_ANULA,
                                        CONSECUTIVO = fa.CONSECUTIVO,
                                        CONDICION_PAGO = fa.CONDICION_PAGO,
                                        CONSEC_RESOLUCION = fa.CONSEC_RESOLUCION,
                                        CONTRATO = fa.CONTRATO,
                                        CONTRATO_AC = fa.CONTRATO_AC,
                                        CreateDate = fa.CreateDate,
                                        CreatedBy = fa.CreatedBy,
                                        UpdatedBy = fa.UpdatedBy,
                                        DESCUENTO_CASCADA = fa.DESCUENTO_CASCADA,
                                        TIPO_DESCUENTO1 = fa.TIPO_DESCUENTO1,
                                        TIPO_DESCUENTO2 = fa.TIPO_DESCUENTO2,
                                        TIPO_CAMBIO = fa.TIPO_CAMBIO,
                                        DIRECCION_EMBARQUE = fa.DIRECCION_EMBARQUE,
                                        DIRECCION_FACTURA = fa.DIRECCION_FACTURA,
                                        DIREC_EMBARQUE = fa.DIREC_EMBARQUE,
                                        DOC_CREDITO_CXC = fa.TIPO_DOCUMENTO.Equals("F")? resultadoFirma.FacturaElectronica:null,
                                        EMBARCAR_A = fa.EMBARCAR_A,
                                        FECHA_PEDIDO = fa.FECHA_PEDIDO,
                                        TOTAL_FACTURA = fa.TOTAL_FACTURA,
                                        SERIE_RESOLUCION = fa.SERIE_RESOLUCION,
                                        OBSERVACIONES = fa.OBSERVACIONES,
                                        FECHA_RIGE = fa.FECHA_RIGE,
                                        GENERA_DOC_FE = fa.GENERA_DOC_FE,
                                        MONTO_ANTICIPO = fa.MONTO_ANTICIPO,
                                        MODULO = fa.MODULO,
                                        MONEDA = fa.MONEDA,
                                        RUTA = fa.RUTA,
                                        MONTO_DESCUENTO1 = fa.MONTO_DESCUENTO1,
                                        MONTO_DESCUENTO2 = fa.MONTO_DESCUENTO2,
                                        MONTO_DOCUMENTACIO = fa.MONTO_DOCUMENTACIO,
                                        MONTO_FLETE = fa.MONTO_FLETE,
                                        MONTO_SEGURO = fa.MONTO_SEGURO,
                                        NUMERO_PAGINAS = fa.NUMERO_PAGINAS,
                                        TOTAL_PESO_NETO = fa.TOTAL_PESO_NETO,
                                        TASA_CREE1 = fa.TASA_CREE1,
                                        TASA_CREE1_PORC = fa.TASA_CREE1_PORC,
                                        TASA_CREE2 = fa.TASA_CREE2,
                                        TASA_CREE2_PORC = fa.TASA_CREE2_PORC,
                                        SUBTIPO_DOC_CXC = fa.SUBTIPO_DOC_CXC,
                                         TIPO_DOC_CXC=fa.TIPO_DOC_CXC,
                                        NOMBREMAQUINA = fa.NOMBREMAQUINA,
                                        NOMBRE_CLIENTE = fa.NOMBRE_CLIENTE,
                                        NIVEL_PRECIO = fa.NIVEL_PRECIO,
                                        USA_DESPACHOS = fa.USA_DESPACHOS,
                                        MULTIPLICADOR_EV = fa.MULTIPLICADOR_EV,
                                        PAIS = fa.PAIS,
                                        ZONA = fa.ZONA,
                                        REIMPRESO = fa.REIMPRESO,
                                        PORC_INTCTE = fa.PORC_INTCTE,
                                        RecordDate = fa.RecordDate,
                                        VENDEDOR = fa.VENDEDOR,
                                        U_GARANTIA = fa.U_GARANTIA,
                                        USUARIO = fa.USUARIO
                                        ,TIPO_CREDITO_CXC = fa.TIPO_DOCUMENTO.Equals("F") ? "FAC":null
                                        ,TOTAL_MERCADERIA = fa.TOTAL_MERCADERIA
                                        ,TOTAL_UNIDADES = fa.TOTAL_UNIDADES,
                                        VERSION_NP = fa.VERSION_NP,
                                        NoteExistsFlag = fa.NoteExistsFlag,
                                        RowPointer = Guid.NewGuid(),
                                        //**********
                                        //firma electrónica y referencia
                                        //*********
                                        RUBRO5=resultadoFirma.NumeroAutorizacion
                                        ,RUBRO4=docSolicitud.Referencia
                                    };
                                    db.FACTURA.AddObject(fac);

                                }
                            }

                    ////(4.2) FACTURA_LINEA

                    var qFL = (from fl in db.FACTURA_LINEA
                                       where fl.FACTURA.Equals(cDocumentoOriginal)
                                       select fl);

                    ///4.2.1 ACTUALIZAR DESPACHO_DETALLE 
                    var qDespDet = db.DESPACHO_DETALLE.Where(x => x.DOCUM_ORIG == cDocumentoOriginal);
                    if (qDespDet.Count() > 0)
                        foreach (var item in qDespDet)
                            item.DOCUM_ORIG = resultadoFirma.FacturaElectronica;

                    if (qFL.Count() > 0)
                            {

                                foreach (var fl in qFL)
                                {
                                    var descripcion = (fl.DESCRIPCION == "" ? db.ARTICULO.Where(x => x.ARTICULO1 == fl.ARTICULO).FirstOrDefault().DESCRIPCION : fl.DESCRIPCION);

                                    FACTURA_LINEA lINEA = new FACTURA_LINEA
                                    {
                                        FACTURA = resultadoFirma.FacturaElectronica,
                                        ANULADA = fl.ANULADA,
                                        ARTICULO = fl.ARTICULO,
                                        BASE_IMPUESTO1 = fl.BASE_IMPUESTO1,
                                        BASE_IMPUESTO2 = fl.BASE_IMPUESTO2,
                                        BODEGA = fl.BODEGA,
                                        CANTIDAD = fl.CANTIDAD,
                                        FECHA_FACTURA = fl.FECHA_FACTURA,
                                        TIPO_DOCUMENTO = fl.TIPO_DOCUMENTO
                                        ,
                                        TIPO_LINEA = fl.TIPO_LINEA,
                                        TIPO_ORIGEN = fl.TIPO_ORIGEN,
                                        TOTAL_IMPUESTO1 = fl.TOTAL_IMPUESTO1,
                                        TOTAL_IMPUESTO2 = fl.TOTAL_IMPUESTO2,
                                        CANTIDAD_ACEPTADA = fl.CANTIDAD_ACEPTADA,
                                        PRECIO_TOTAL = fl.PRECIO_TOTAL,
                                        PRECIO_UNITARIO = fl.PRECIO_UNITARIO,
                                        CANTIDAD_DEVUELT = fl.CANTIDAD_DEVUELT,
                                        LOCALIZACION = fl.LOCALIZACION,
                                        CANT_ANUL_PORDESPA = fl.CANT_ANUL_PORDESPA,
                                        CANT_DESPACHADA = fl.CANT_DESPACHADA,
                                        CANT_DEV_PROCESO = fl.CANT_DEV_PROCESO,
                                        CANT_NO_ENTREGADA = fl.CANT_NO_ENTREGADA,
                                        COSTO_ESTIM_COMP_LOCAL = fl.COSTO_ESTIM_COMP_LOCAL,
                                        CENTRO_COSTO = fl.CENTRO_COSTO,
                                        COMENTARIO = fl.COMENTARIO,
                                        COSTO_ESTIM_COMP_DOLAR = fl.COSTO_ESTIM_COMP_DOLAR,
                                        COSTO_ESTIM_DOLAR = fl.COSTO_ESTIM_DOLAR,
                                        COSTO_ESTIM_LOCAL = fl.COSTO_ESTIM_LOCAL,
                                        COSTO_TOTAL = fl.COSTO_TOTAL,
                                        COSTO_TOTAL_COMP = fl.COSTO_TOTAL_COMP,
                                        COSTO_TOTAL_COMP_DOLAR = fl.COSTO_TOTAL_COMP_DOLAR,
                                        COSTO_TOTAL_COMP_LOCAL = fl.COSTO_TOTAL_COMP_LOCAL,
                                        COSTO_TOTAL_DOLAR = fl.COSTO_TOTAL_DOLAR,
                                        COSTO_TOTAL_LOCAL = fl.COSTO_TOTAL_LOCAL,
                                        LINEA = fl.LINEA,
                                        LOTE = fl.LOTE,
                                        LINEA_ORIGEN = fl.LINEA_ORIGEN,
                                        PEDIDO = fl.PEDIDO,
                                        PEDIDO_LINEA = fl.PEDIDO_LINEA,
                                        DESCRIPCION = descripcion,
                                        DESCUENTO_VOLUMEN = fl.DESCUENTO_VOLUMEN,
                                        DESC_TOT_GENERAL = fl.DESC_TOT_GENERAL,
                                        DESC_TOT_LINEA = fl.DESC_TOT_LINEA,
                                        CreateDate = fl.CreateDate,
                                        CreatedBy = fl.CreatedBy,
                                        UpdatedBy = fl.UpdatedBy
                                            ,
                                        RowPointer = Guid.NewGuid()
                                            ,
                                        MULTIPLICADOR_EV = fl.MULTIPLICADOR_EV,
                                        DOCUMENTO_ORIGEN = fl.DOCUMENTO_ORIGEN,
                                        NoteExistsFlag = fl.NoteExistsFlag,
                                        RecordDate = fl.RecordDate,
                                        CUENTA_CONTABLE = fl.CUENTA_CONTABLE
                                        ,
                                        U_ARMADOR = fl.U_ARMADOR,

                                    };

                                    log.Info("linea 2579: documento "+fl.FACTURA+" Descripcion de artículo " + fl.ARTICULO + " [" + lINEA.DESCRIPCION + "]");

                                    db.FACTURA_LINEA.AddObject(lINEA);
                                }
                            }

                            var qFaL2 = (from fl2 in db.FACTURA_LINEA where fl2.FACTURA.Equals(cDocumentoOriginal) select fl2);
                            if (qFaL2.Count() > 0)
                                foreach (var fl2 in qFaL2)
                                {
                                    
                                    db.FACTURA_LINEA.DeleteObject(fl2);
                                }
                    ////(4.3) ELIMINAR EL REGISTRO DE LA FACTURA ORIGINAL

                    var qMFfac = (from f in db.FACTURA where f.FACTURA1.Equals(cDocumentoOriginal) select f);
                            if (qMFfac.Count() > 0)
                            {
                                foreach (var fa in qMFfac)
                                {
                                    
                                    db.FACTURA.DeleteObject(fa);
                                }
                            }
                   
                    //(4.3 0) MF_FACTURA AQUI SE ACTUALIZA EL NÚMERO DE FACTURA DE CONTINGENCIA POR EL ORIGINAL
                    var qMF_fac = (from f in db.MF_Factura where f.FACTURA.Equals(cDocumentoOriginal) select f);
                    if (qMF_fac.Count() > 0)
                    {
                        
                        MF_Factura objFact = new MF_Factura
                        {
                            FACTURA=resultadoFirma.FacturaElectronica,
                            DIRECCION_FEL = resultadoFirma.Direccion,
                            NOMBRE_FEL = resultadoFirma.Nombre,
                            AUTORIZACION_PUNTOS = qMF_fac.FirstOrDefault().AUTORIZACION_PUNTOS,
                            CLIENTE= qMF_fac.FirstOrDefault().CLIENTE,
                            COBRADOR= qMF_fac.FirstOrDefault().COBRADOR,
                            EXPEDIENTE_BODEGA= qMF_fac.FirstOrDefault().EXPEDIENTE_BODEGA,
                            FACTURADA_EN_BODEGA= qMF_fac.FirstOrDefault().FACTURADA_EN_BODEGA,
                            FACTURA_REFERENCIA_PUNTOS= qMF_fac.FirstOrDefault().FACTURA_REFERENCIA_PUNTOS,
                            FECHA= qMF_fac.FirstOrDefault().FECHA,
                            FECHA_ENVIO_FEL= qMF_fac.FirstOrDefault().FECHA_ENVIO_FEL,
                            FECHA_REGISTRO= qMF_fac.FirstOrDefault().FECHA_REGISTRO,
                            FECHA_REGISTRO_MODIFICA= qMF_fac.FirstOrDefault().FECHA_REGISTRO_MODIFICA,
                            FINANCIERA= qMF_fac.FirstOrDefault().FINANCIERA,
                            ID_DEPOSITO= qMF_fac.FirstOrDefault().ID_DEPOSITO,
                            JEFE= qMF_fac.FirstOrDefault().JEFE,
                            MONTO= qMF_fac.FirstOrDefault().MONTO,
                            NIVEL_PRECIO= qMF_fac.FirstOrDefault().NIVEL_PRECIO,
                            PAGADA= qMF_fac.FirstOrDefault().PAGADA,
                            PCTJ_IVA= qMF_fac.FirstOrDefault().PCTJ_IVA,
                            PEDIDO= qMF_fac.FirstOrDefault().PEDIDO,
                            PUNTOS= qMF_fac.FirstOrDefault().PUNTOS,
                            RECIBO= qMF_fac.FirstOrDefault().RECIBO,
                            REVISADA= qMF_fac.FirstOrDefault().REVISADA,
                            RE_FACTURACION= qMF_fac.FirstOrDefault().RE_FACTURACION,
                            SE_ANULARA= qMF_fac.FirstOrDefault().SE_ANULARA,
                            SUPERVISOR= qMF_fac.FirstOrDefault().SUPERVISOR,
                            TIPO= qMF_fac.FirstOrDefault().TIPO,
                            TIPOVENTA= qMF_fac.FirstOrDefault().TIPOVENTA,
                            TIPO_ENVIO_FEL= qMF_fac.FirstOrDefault().TIPO_ENVIO_FEL,
                            USUARIO= qMF_fac.FirstOrDefault().USUARIO,
                            USUARIO_MODIFICA= qMF_fac.FirstOrDefault().USUARIO_MODIFICA,
                            VALOR_PUNTOS= qMF_fac.FirstOrDefault().VALOR_PUNTOS,
                            VENDEDOR= qMF_fac.FirstOrDefault().VENDEDOR
                        };
                        db.MF_Factura.AddObject(objFact);

                        foreach (var itemf in qMF_fac)
                            db.MF_Factura.DeleteObject(itemf);
                    }
                    
                    ////(4.4 a) DOCUMENTO_ASOCIADO
                    //Se verificó que tiene poco o nulo uso, en tests dio un error con un documento que estaba aqui
                    // pero al haber solo doce registros, no se considera que esta tabla sea usada normalmente. este
                    //registro que dio un error pudo haber sido por tests que se hicieron con Victor Hugo a inicios de mayo 2019
                    //la tabla que se usa como documento asociado es auxiliar_cc
                    var qDocA = (from a in db.DOCUMENTO_ASOCIADO where a.DOCUMENTO == cDocumentoOriginal select a);
                    if (qDocA.Count() > 0)
                        foreach (var itema in qDocA)
                            db.DOCUMENTO_ASOCIADO.DeleteObject(itema);

                    ///(4.4) DOCUMENTOS_CC       

                            var qDoc = (from c in db.DOCUMENTOS_CC where c.DOCUMENTO.Equals(cDocumentoOriginal) select c);
                            if (qDoc.Count() > 0)
                                foreach (var doc in qDoc)
                                {

                                    DOCUMENTOS_CC DOC_CC = new DOCUMENTOS_CC
                                    {
                                        DOCUMENTO=resultadoFirma.FacturaElectronica,
                                        ANULADO = doc.ANULADO,
                                        APLICACION = doc.APLICACION
                                        ,
                                        APROBADO = doc.APROBADO
                                        ,
                                        ASIENTO = doc.ASIENTO,
                                        ASIENTO_PENDIENTE = doc.ASIENTO_PENDIENTE,
                                        AUDITORIA_COBRO = doc.AUDITORIA_COBRO,
                                        AUD_FECHA_ANUL = doc.AUD_FECHA_ANUL,
                                        AUD_USUARIO_ANUL = doc.AUD_USUARIO_ANUL,
                                        TIPO = doc.TIPO,
                                        CONTRARECIBO = doc.CONTRARECIBO,
                                        FECHA_DOCUMENTO = doc.FECHA_DOCUMENTO,
                                        FECHA = doc.FECHA,
                                        MONTO = doc.MONTO,
                                        SALDO = doc.SALDO
                                                       ,
                                        MONTO_LOCAL = doc.MONTO_LOCAL,
                                        SALDO_LOCAL = doc.SALDO_LOCAL,
                                        MONTO_DOLAR = doc.MONTO_DOLAR,
                                        SALDO_DOLAR = doc.SALDO_DOLAR,
                                        MONTO_CLIENTE = doc.MONTO_CLIENTE,
                                        SALDO_CLIENTE = doc.SALDO_CLIENTE,
                                        TIPO_CAMBIO_MONEDA = doc.TIPO_CAMBIO_MONEDA,
                                        TIPO_CAMBIO_DOLAR = doc.TIPO_CAMBIO_DOLAR,
                                        TIPO_CAMBIO_CLIENT = doc.TIPO_CAMBIO_CLIENT,
                                        TIPO_CAMB_ACT_LOC = doc.TIPO_CAMB_ACT_LOC,
                                        TIPO_CAMB_ACT_DOL = doc.TIPO_CAMB_ACT_DOL,
                                        TIPO_CAMB_ACT_CLI = doc.TIPO_CAMB_ACT_CLI,
                                        SUBTOTAL = doc.SUBTOTAL,
                                        DESCUENTO = doc.DESCUENTO,
                                        IMPUESTO1 = doc.IMPUESTO1
                                                       ,
                                        IMPUESTO2 = doc.IMPUESTO2,
                                        RUBRO1 = doc.RUBRO1,
                                        RUBRO2 = doc.RUBRO2,
                                        MONTO_RETENCION = doc.MONTO_RETENCION,
                                        SALDO_RETENCION = doc.SALDO_RETENCION,
                                        DEPENDIENTE = doc.DEPENDIENTE,
                                        FECHA_ULT_CREDITO = doc.FECHA_ULT_CREDITO,
                                        CARGADO_DE_FACT = doc.CARGADO_DE_FACT,
                                        FECHA_ULT_MOD = doc.FECHA_ULT_MOD,
                                        NOTAS = doc.NOTAS,
                                        CLASE_DOCUMENTO = doc.CLASE_DOCUMENTO,
                                        FECHA_VENCE = doc.FECHA_VENCE,
                                        NUM_PARCIALIDADES = doc.NUM_PARCIALIDADES,
                                        FECHA_REVISION = doc.FECHA_REVISION,
                                        COBRADOR = doc.COBRADOR,
                                        USUARIO_ULT_MOD = doc.USUARIO_ULT_MOD,
                                        CONDICION_PAGO = doc.CONDICION_PAGO,
                                        MONEDA = doc.MONEDA,
                                                       CTA_BANCARIA = doc.CTA_BANCARIA,
                                        VENDEDOR1 = doc.VENDEDOR1,
                                        CLIENTE_REPORTE = doc.CLIENTE_REPORTE,
                                        CLIENTE_ORIGEN = doc.CLIENTE_ORIGEN,
                                        CLIENTE = doc.CLIENTE,
                                        SUBTIPO = doc.SUBTIPO,
                                        PORC_INTCTE = doc.PORC_INTCTE,
                                        TIPO_DOC_ORIGEN = doc.TIPO_DOC_ORIGEN,
                                        DOC_DOC_ORIGEN = doc.DOC_DOC_ORIGEN,
                                        FECHA_ANUL = doc.FECHA_ANUL,
                                        NUM_DOC_CB = doc.NUM_DOC_CB,
                                        FECHA_COBRO = doc.FECHA_COBRO,
                                        FECHA_SEGUIMIENTO = doc.FECHA_SEGUIMIENTO,
                                        USUARIO_APROBACION = doc.USUARIO_APROBACION,
                                        FECHA_APROBACION = doc.FECHA_APROBACION,
                                        NoteExistsFlag = doc.NoteExistsFlag,
                                        RowPointer = Guid.NewGuid(),
                                        CreatedBy = doc.CreatedBy,
                                        UpdatedBy = doc.UpdatedBy,
                                        CreateDate = doc.CreateDate,
                                        CODIGO_IMPUESTO = doc.CODIGO_IMPUESTO,
                                        PAIS = doc.PAIS,
                                        DIVISION_GEOGRAFICA1 = doc.DIVISION_GEOGRAFICA1,
                                        DIVISION_GEOGRAFICA2 = doc.DIVISION_GEOGRAFICA2,
                                        BASE_IMPUESTO1 = doc.BASE_IMPUESTO1,
                                        BASE_IMPUESTO2 = doc.BASE_IMPUESTO2,
                                        DEPENDIENTE_GP = doc.DEPENDIENTE_GP,
                                        SALDO_TRANS = doc.SALDO_TRANS,
                                        SALDO_TRANS_LOCAL = doc.SALDO_TRANS_LOCAL,
                                        SALDO_TRANS_DOLAR = doc.SALDO_TRANS_DOLAR,
                                        FECHA_PROYECTADA = doc.FECHA_PROYECTADA,
                                        PORC_RECUPERACION = doc.PORC_RECUPERACION,
                                        TIPO_ASIENTO = doc.TIPO_ASIENTO,
                                        PAQUETE1 = doc.PAQUETE1,
                                        SALDO_TRANS_CLI = doc.SALDO_TRANS_CLI,
                                        FACTURADO = doc.FACTURADO,
                                                       U_AUTORIZACION_PAGO_TARJETA = doc.U_AUTORIZACION_PAGO_TARJETA,
                                        U_NUMERO_CHEQUE = doc.U_NUMERO_CHEQUE,
                                        CONTRATO=doc.CONTRATO,
                                        U_NUMERO_TARJETA_CREDITO_DEBITO = doc.U_NUMERO_TARJETA_CREDITO_DEBITO,
                                        U_POS = doc.U_POS,
                                        U_BANCO = doc.U_BANCO
                                        ,GENERA_DOC_FE=doc.GENERA_DOC_FE,
                                        
                                        //firma electrónica y referencia
                                        DOCUMENTO_GLOBAL=resultadoFirma.NumeroAutorizacion
                                        ,DOCUMENTO_FISCAL=docSolicitud.Referencia

                                        //Nombre y dirección del cliente devuelto por el gface
                                        //estos datos son los que deben imprimirse en la factura
                                        ,
                                        U_CONCEPTO_PAGO = resultadoFirma.Nombre,
                                        OBSERVACIONES_COBRO=resultadoFirma.Direccion
                                    };
                                    
                                    //agregar el nuevo elemento con el número electrónico
                                    db.DOCUMENTOS_CC.AddObject(DOC_CC);
                                    //eliminar el original

                                    //---------------------------------------
                                    //AUXILIAR_CC AUXILIAR CUENTAS POR COBRAR
                                    //---------------------------------------
                                    if (docSolicitud.TipoDocumento == DOCUMENTO.NOTA_CREDITO || docSolicitud.TipoDocumento == DOCUMENTO.NOTA_ABONO)
                                    {
                                        var qDocCC = (from cc in db.AUXILIAR_CC where cc.CREDITO.Equals(cDocumentoOriginal) select cc);
                                        if (qDocCC.Count() > 0)
                                            foreach (var itemCc in qDocCC)
                                            {
                                                AUXILIAR_CC cc = new AUXILIAR_CC
                                                {
                                                TIPO_CREDITO = itemCc.TIPO_CREDITO,
                                                TIPO_DEBITO=itemCc.TIPO_DEBITO,
                                                FECHA=itemCc.FECHA,
                                                CREDITO=resultadoFirma.FacturaElectronica,
                                                DEBITO=itemCc.DEBITO,
                                                MONTO_DEBITO=itemCc.MONTO_DEBITO,
                                                MONTO_CREDITO=itemCc.MONTO_CREDITO,
                                                MONTO_LOCAL=itemCc.MONTO_LOCAL,
                                                MONTO_DOLAR=itemCc.MONTO_DOLAR,
                                                MONTO_CLI_CREDITO=itemCc.MONTO_CLI_CREDITO,
                                                ASIENTO=itemCc.ASIENTO,
                                                TIPO_DOCPPAGO=itemCc.TIPO_DOCPPAGO,
                                                MONTO_CLI_DEBITO=itemCc.MONTO_CLI_DEBITO,
                                                MONEDA_CREDITO=itemCc.MONEDA_CREDITO,
                                                MONEDA_DEBITO=itemCc.MONEDA_DEBITO,
                                                CLI_REPORTE_CREDIT=itemCc.CLI_REPORTE_CREDIT,
                                                CLI_REPORTE_DEBITO=itemCc.CLI_REPORTE_DEBITO,
                                                CLI_DOC_CREDIT=itemCc.CLI_DOC_CREDIT,
                                                CLI_DOC_DEBITO=itemCc.CLI_DOC_DEBITO,
                                                TIPO_DOCINTCTE=itemCc.TIPO_DOCINTCTE,
                                                NoteExistsFlag=itemCc.NoteExistsFlag,
                                                RecordDate=itemCc.RecordDate,
                                                RowPointer= Guid.NewGuid(),
                                                CreatedBy=itemCc.CreatedBy,
                                                UpdatedBy=itemCc.UpdatedBy,
                                                CreateDate=itemCc.CreateDate,
                                                FOLIOSAT_DEBITO=itemCc.FOLIOSAT_DEBITO,
                                                FOLIOSAT_CREDITO=itemCc.FOLIOSAT_CREDITO,
                                                TIPO_CAMBIO_APLICA=itemCc.TIPO_CAMBIO_APLICA
                                            };
                                                db.AUXILIAR_CC.AddObject(cc);
                                                db.AUXILIAR_CC.DeleteObject(itemCc);
                                            }
                                    }
                                    else if (docSolicitud.TipoDocumento == DOCUMENTO.NOTA_DEBITO)
                                    {
                                        var qDocCC = (from cc in db.AUXILIAR_CC where cc.DEBITO.Equals(cDocumentoOriginal) select cc);
                                        if (qDocCC.Count() > 0)
                                            foreach (var itemCc in qDocCC)
                                            {
                                                AUXILIAR_CC cc = new AUXILIAR_CC
                                                {
                                                    TIPO_CREDITO = itemCc.TIPO_CREDITO,
                                                    TIPO_DEBITO = itemCc.TIPO_DEBITO,
                                                    FECHA = itemCc.FECHA,
                                                    DEBITO = resultadoFirma.FacturaElectronica,
                                                    CREDITO= itemCc.CREDITO,
                                                    MONTO_DEBITO = itemCc.MONTO_DEBITO,
                                                    MONTO_CREDITO = itemCc.MONTO_CREDITO,
                                                    MONTO_LOCAL = itemCc.MONTO_LOCAL,
                                                    MONTO_DOLAR = itemCc.MONTO_DOLAR,
                                                    MONTO_CLI_CREDITO = itemCc.MONTO_CLI_CREDITO,
                                                    ASIENTO = itemCc.ASIENTO,
                                                    TIPO_DOCPPAGO = itemCc.TIPO_DOCPPAGO,
                                                    MONTO_CLI_DEBITO = itemCc.MONTO_CLI_DEBITO,
                                                    MONEDA_CREDITO = itemCc.MONEDA_CREDITO,
                                                    MONEDA_DEBITO = itemCc.MONEDA_DEBITO,
                                                    CLI_REPORTE_CREDIT = itemCc.CLI_REPORTE_CREDIT,
                                                    CLI_REPORTE_DEBITO = itemCc.CLI_REPORTE_DEBITO,
                                                    CLI_DOC_CREDIT = itemCc.CLI_DOC_CREDIT,
                                                    CLI_DOC_DEBITO = itemCc.CLI_DOC_DEBITO,
                                                    TIPO_DOCINTCTE = itemCc.TIPO_DOCINTCTE,
                                                    NoteExistsFlag = itemCc.NoteExistsFlag,
                                                    RecordDate = itemCc.RecordDate,
                                                    RowPointer = Guid.NewGuid(),
                                                    CreatedBy = itemCc.CreatedBy,
                                                    UpdatedBy = itemCc.UpdatedBy,
                                                    CreateDate = itemCc.CreateDate,
                                                    FOLIOSAT_DEBITO = itemCc.FOLIOSAT_DEBITO,
                                                    FOLIOSAT_CREDITO = itemCc.FOLIOSAT_CREDITO,
                                                    TIPO_CAMBIO_APLICA = itemCc.TIPO_CAMBIO_APLICA
                                                };
                                                db.AUXILIAR_CC.AddObject(cc);
                                                //eliminar el documento anterior auxiliar_cc
                                                db.AUXILIAR_CC.DeleteObject(itemCc);
                                            }
                                    }

                                    //eliminar el documento anterior documentos_cc
                                    db.DOCUMENTOS_CC.DeleteObject(doc);

                                }

                    //(4.5) ACTUALIZAR LA REFERENCIA
                    if (qreferencia.Count() > 0 & blnuevo)
                        foreach (var item in qreferencia)
                        {
                            item.VALOR_CONSECUTIVO = docSolicitud.Referencia.Replace("NC-", "");
                        }
                    //4.6 MF_Despacho
                    var qMfDespacho = db.MF_Despacho.Where(x => x.FACTURA == cDocumentoOriginal);
                    foreach (var item in qMfDespacho)
                        item.FACTURA = resultadoFirma.FacturaElectronica;

                    // pedido armado
                    var qMfPedidoArmado = db.MF_PedidoArmado.Where(x => x.FACTURA == cDocumentoOriginal);
                    if(qMfPedidoArmado != null)
                    foreach (var item in qMfPedidoArmado)
                        item.FACTURA = resultadoFirma.FacturaElectronica;
                    //4.7 factura_recibo

                    var qmfRecibos = db.MF_FacturaRecibo.Where(x => x.FACTURA==cDocumentoOriginal);
                    if(qmfRecibos !=null)
                    if (qmfRecibos.Count() > 0)
                    {

                        qmfRecibos.ForEach(x =>
                        {
                            MF_FacturaRecibo objfr = new MF_FacturaRecibo
                            {
                                FACTURA = resultadoFirma.FacturaElectronica,
                                RECIBO = x.RECIBO,
                                 CreateDate=x.CreateDate,
                                 CreatedBy=x.CreatedBy
                                 , UpdatedBy=x.UpdatedBy,
                                 RecordDate=x.RecordDate
                            };
                            db.MF_FacturaRecibo.AddObject(objfr);

                        });
                        
                        foreach (var item in qmfRecibos)
                            db.MF_FacturaRecibo.DeleteObject(item);
                        
                        
                    }
                    //4.8 mf_recibo
                    var qmf_Recibo = db.MF_Recibo.Where(x => x.FACTURA == cDocumentoOriginal);
                    if (qmf_Recibo != null)
                        foreach (var item in qmf_Recibo)
                        item.FACTURA = resultadoFirma.FacturaElectronica;


                    #region "workarround"
                    try
                    {
                        //workarround issue notas de crédito que se generan sin descripción , se le actualiza la descripción proveniente del artículo

                        var qDet = (from fl in db.FACTURA_LINEA where fl.FACTURA == resultadoFirma.FacturaElectronica select fl);
                        //log.Info("\n\r Firmar documentos electrónicos, documento: " + resultadoFirma.FacturaElectronica + " se encontraron " + qDet.Count() + " filas sin descripción (FACTURA_LINEA).\n\r");
                        foreach (var linea in qDet)
                        {
                            linea.DESCRIPCION = db.ARTICULO.Where(x => x.ARTICULO1 == linea.ARTICULO).FirstOrDefault().DESCRIPCION;
                            log.Info("LInea 2862: documento " + resultadoFirma.FacturaElectronica + " descripción del artículo: [" + linea.DESCRIPCION + "]");
                        }

                    }
                    catch (Exception exi)
                    {
                        log.Error("Línea 2868 Error al intentar actualizar la descripción del artículo de la factura" + resultadoFirma.FacturaElectronica + " " + exi.Message + " " + exi.InnerException + " ->" + exi.Source);
                    }
                    #endregion


                        db.SaveChanges();
                        transactionScope.Complete();

                    }

               
            }
                else
                {
                    var result = ManejoErroresFEL(resultadoFirma.Xml, docSolicitud);
                    if (result != null)
                        resultadoFirma = result;
                }
           
            return resultadoFirma;
        }

        private Resultado ServicioAnulacionDocumentos(Doc docSolicitud)
        {
            UtilitariosGuateFacturas utils = new UtilitariosGuateFacturas();
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            string cCobrador = string.Empty;
            string NombreSucursal = string.Empty;
            string DireccionSucursal = string.Empty;
            string DeptoSucursal = string.Empty;
            string MuniSucursal = string.Empty;
            string Tipo = string.Empty;
            string DirCliente = string.Empty;
            string NombreCliente = string.Empty;
            string DeptoCliente = string.Empty;
            string MuniCliente = string.Empty;
            string cCliente = string.Empty;
            string Vendedor = string.Empty;
            string DirVendedor = string.Empty;
            Resultado resultadoFirma = new Resultado();
            string mCantidadLetras = string.Empty;
            string cResolucion = string.Empty;
            string cFolioIni = string.Empty;
            string cFolioFin = string.Empty;
            DateTime dtFechaResolucion = DateTime.Now;
            var json = new JavaScriptSerializer().Serialize(docSolicitud);
            string cDocumentoOriginal = docSolicitud.SerieAdmin + "-" + docSolicitud.NumeroAdmin;

            
            //----------(1)---FIRMAR EL DOCUMENTO---------------
            //-----------------------------------------------

            resultadoFirma = utils.Servicio(SERVICIOS.GENERAR_DOCUMENTO, docSolicitud);
            log.Debug("resultado firma: " + resultadoFirma.Xml + " autorización: [" + resultadoFirma.NumeroAutorizacion + "]");
            //------------------------------------------------
            //------------------------------------------------

            if (resultadoFirma.NumeroAutorizacion.Trim() != string.Empty)
            {

                
                log.Debug("Grabando en mf_gface " + resultadoFirma.FacturaElectronica);

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    //---------(2) guardar en mf_gface--------------
                    var qGface = (from gce in db.MF_GFace where gce.DOCUMENTO == cDocumentoOriginal select gce);
                    if (qGface.Count() > 0)
                    {
                        foreach (var item in qGface)
                        {
                            item.ANULADA = "S";
                            item.FECHA_HORA_MODIFICA = DateTime.Now;
                            item.XML_DOCUMENTO = resultadoFirma.XmlDocumento;
                            item.XML_FIRMADO = resultadoFirma.Xml;
                            item.XML_ERROR = resultadoFirma.XmlError;
                        }
                    }

                    db.SaveChanges();
                    transactionScope.Complete();

                }


            }
            else
            {
                var result = ManejoErroresFEL(resultadoFirma.Xml, docSolicitud);
                if (result != null)
                    resultadoFirma = result;
            }

            return resultadoFirma;
        }

        private void  NotificacionesAlCliente(string codigoFactura,bool esFactura=true)
        {
            
                try
                {
                    EmailController email = new EmailController();
                if (esFactura)
                    email.EnviarFacturaGarantia(codigoFactura);
                else
                    email.EnviarNotaCreditoDebito(codigoFactura);
                   
                }
                catch (Exception ex)
                {
                log.Error("Error al enviar documento electrónico al cliente "+codigoFactura);
                }
           
        }
        private void EnviarMailInterno(List<string> lstResultados,List<string> lstErrores,int nTotalDoctos)
        {
            string CuerpoCorreo = "";
            List<string> mCuentaCorreo = new List<string>();
            string mRemitente = string.Empty;
            StringBuilder mBody=new StringBuilder();
            //Obtener los destinatarios del correo
            var qCatalogo = (from s in dbExactus.MF_Catalogo where s.CODIGO_TABLA.Equals(CATALOGOS.GUATEFACTURAS_FEL) select s);
            if (qCatalogo.Count() > 0)
            {
               var  xi = qCatalogo.Where(x => x.CODIGO_CAT.Equals(MF_Clases.Utils.CONSTANTES.CATALOGOS.DESTINATARIOS_CORREO_FIRMA_NC_ND));
                if (xi.Count() > 0)
                {
                    var dest = xi.FirstOrDefault().NOMBRE_CAT.Split('|');
                    foreach(string s in dest)
                        mCuentaCorreo.Add(s);
                }
                    
                mRemitente = qCatalogo.Where(x => x.CODIGO_CAT.Equals("REMITENTE CORREO")).FirstOrDefault().NOMBRE_CAT.ToString();
            }
            //armar el cuerpo del correo
            #region "Cuerpo Correo"
            Utils.Utilitarios utilitarios = new Utils.Utilitarios();
            CuerpoCorreo = utilitarios.CorreoInicioFormatoAzulYGris();
            CuerpoCorreo += "<br/><br/>Estimados:<br/><br/>Les informamos sobre los documentos electrónicos enviados a Guatefacturas para la firma electrónica, "+nTotalDoctos+" en total,  a continuación los resultados:<br/><br/>";

            if (lstResultados.Count > 0)
            {
                CuerpoCorreo += "<table><tr>" + utilitarios.FormatoTituloCentro("Documentos Operados") + "</tr>";
                CuerpoCorreo = CuerpoCorreo.Replace("<tr><td class='style5'", "<tr><td class='style5' colspan='2'");
                foreach (string s in lstResultados)
                {
                    try
                    {
                        CuerpoCorreo += "<tr>" + utilitarios.FormatoCuerpoCentro(s.Substring(0, s.IndexOf("Factura electrónica")).Replace("Documento :", "").Trim()) + utilitarios.FormatoCuerpoCentro(s.Substring(s.IndexOf("Factura electrónica:")).Replace("Factura electrónica:", "")) + "</tr>";
                    }
                    catch
                    {
                        try
                        {
                            CuerpoCorreo += "<tr>" + utilitarios.FormatoCuerpoCentro(s.Substring(0, s.IndexOf("Resultado")).Replace("Documento :", "").Replace("Resultado :", ""))+utilitarios.FormatoCuerpoCentro("DOCUMENTO ANULADO")+"</tr>";
                        }
                        catch(Exception exi)
                        { log.Error(CatchClass.ExMessage(exi, "DALFel", "EnviarMail")); }
                    }
                }
                CuerpoCorreo += "</table><br/>";
            }
            if (lstErrores.Count > 0)
            {
                CuerpoCorreo += "<table width='70%'><tr>" + utilitarios.FormatoTituloCentro("Documentos No operados") + "</tr>";
                CuerpoCorreo = CuerpoCorreo.Replace("<tr><td class='style5'", "<tr><td class='style5' colspan='2'");
                foreach (string s in lstErrores)
                {
                    CuerpoCorreo += "<tr>" +utilitarios.FormatoCuerpoCentro(s.Replace("Documento :", "").Substring(0, s.IndexOf("Resultado:")).Replace("Resultado:", "")) +utilitarios.FormatoCuerpoCentro( s.Substring((s.IndexOf(":línea")>0? s.IndexOf(":línea") : s.IndexOf("Resultado:")) + 6).Replace("Resultado:","").Replace("<Resultado>","").Replace("</Resultado>","")) + "</tr>";
                }
                CuerpoCorreo = CuerpoCorreo.Replace("ado:","");
                
                CuerpoCorreo += "</table><br/>";
            }
            CuerpoCorreo = CuerpoCorreo.Replace("#428BCA", "#F44336");
            CuerpoCorreo += "Saludos cordiales,"+utilitarios.CorreoFin();
            #endregion
            mBody.Append(CuerpoCorreo);

            if (lstResultados.Count > 0 || lstErrores.Count > 0)
            {
                //ENVIAR EL CORREO A LOS DESTINATARIOS DE CONTA
                if (mCuentaCorreo.Count > 0 && mRemitente != string.Empty)
                    utilitarios.EnviarEmail(mRemitente, mCuentaCorreo, "Documentos enviados a firma electrónica", mBody, "MUEBLES FIESTA -NOTIFICACIONES-");
            }
            else
            {
                log.Info("Se generó el servicio de firma de documentos electrónicos sin nada que firmar.");
            }
        }
        #endregion

    }
}