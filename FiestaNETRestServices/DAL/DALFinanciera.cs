﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Models;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static MF_Clases.Clases;

namespace FiestaNETRestServices.DAL
{
    public  class DALFinanciera : MFDAL
    {
        /// <summary>
        /// Esta función tiene como propósito obtener los datos principales de la financiera
        /// </summary>
        public  Respuesta ObtenerFinanciera(int financiera)
        {
            Respuesta mRespuesta = new Respuesta();
            try
            {
                var mCuentaBanco = from mf in dbExactus.MF_Financiera
                                   where mf.Financiera == financiera
                                   select new Financiera()
                                   {
                                       ID = mf.Financiera
                                        ,
                                       Nombre = mf.Nombre.ToUpper()
                                        ,
                                       Activa = mf.Activa
                                        ,
                                       EsBanco = mf.ES_BANCO
                                        ,
                                       CuotasIguales = mf.CuotasIguales
                                        ,
                                       RptSolicitud = mf.RptSolicitud
                                        ,
                                       RptPagare = mf.RptPagare
                                        ,
                                       Prefijo = mf.prefijo
                                        ,
                                       PcjRestar = mf.pctjRestar
                                   }
                                   ;

                if (mCuentaBanco.Count() > 0)
                {
                    mRespuesta.Exito = Const.OK;
                    mRespuesta.Objeto = mCuentaBanco;
                }

                return mRespuesta;
            }
            catch (Exception e)
            {
                return new Respuesta(false, e, "");
            }
        }
        /// <summary>
        /// Obtiene las financieras disponibles
        /// </summary>
        /// <returns></returns>
        public  Respuesta ObtenerFinancieras()
        {
            log.Debug("Obteniendo la lista de financieras");
            List<Financiera> lstFinanciera = new List<Financiera>();
            Respuesta mRespuesta = new Respuesta();
            try
            {
                var mFinanciera = from mf in dbExactus.MF_Financiera
                                  where mf.ES_BANCO == "S" && mf.Activa == "S"
                                 orderby mf.RptSolicitud descending
                                  select new Financiera()
                                  {
                                      ID = mf.Financiera
                                        ,
                                      Nombre = mf.Nombre.ToUpper()
                                          ,
                                      Activa = mf.Activa
                                          ,
                                      EsBanco = mf.ES_BANCO
                                          ,
                                      CuotasIguales = mf.CuotasIguales
                                          ,
                                      RptSolicitud = mf.RptSolicitud
                                          ,
                                      RptPagare = mf.RptPagare
                                          ,
                                      Prefijo = mf.prefijo
                                          ,
                                      PcjRestar = mf.pctjRestar
                                  };
                      if (mFinanciera != null)
                        if (mFinanciera.Count() > 0)
                        {
                        foreach (var item in mFinanciera)
                            lstFinanciera.Add(
                                    new Financiera()
                                    {
                                        ID = item.ID
                                            ,
                                        Nombre = item.Nombre
                                            ,
                                        Activa = item.Activa
                                            ,
                                        EsBanco = item.EsBanco
                                            ,
                                        CuotasIguales = item.CuotasIguales
                                            ,
                                        RptSolicitud = item.RptSolicitud
                                            ,
                                        RptPagare = item.RptPagare
                                            ,
                                        Prefijo = item.Prefijo
                                            ,
                                        PcjRestar =item.PcjRestar
                                    }
                                );
                        }
                mRespuesta.Exito = Const.OK;
                mRespuesta.Objeto = lstFinanciera;
                mRespuesta.Mensaje = lstFinanciera.Count + " financieras activas.";
                return mRespuesta;
            }
            catch (Exception e)
            {
                log.Error("ERROR AL OBTENER LAS FINANCIERAS "+e.Message);
                return new Respuesta(false, e, "");
            }
        }

        /// <summary>
        /// Método para agregar el estado D (desembolsado)
        /// a los expedientes de las financieras (excepto interconsumo ya que ese es un proceso aparte)
        /// El filtro son los movimientos de un día anterior al día en que se ejecuta la tarea
        /// </summary>
        /// <param name="nFinanciera">Id de la financiera a operar</param>
        /// <returns></returns>
        public Respuesta OperarDesembolsosFinanciera(int nFinanciera)
        {
            log.Info("---- FINANCIERA: "+nFinanciera+"-- Iniciando el servicio para operar desembolsos");
            Respuesta mRespuesta = new Respuesta();
            try
            {
                #region "Query utilizada"
                /*
                 * select Factura,'D',data.[Fecha Desembolso],getDate(),'FIESTANET','',getDate()
             
                    from  (
                    select f.factura    as 'Factura'
                                 , f.fecha as 'Fecha'
                                 ,dc.fecha_documento as 'Fecha Desembolso'
                                 , f.cliente as 'Cliente'
                                 , f.NOMBRE_CLIENTE as 'Nombre Cliente'
                                 , f.nivel_precio as 'Nivel Precio'
                                 , f.total_factura as 'Total Factura'
                                 , mp.enganche as 'Enganche'
                                 , mp.saldo_financiar as 'Saldo Financiar'

                                 , ( select  sum( case      when tipo = 'FAC' then DCC.monto
                                                      when tipo = 'N/D' then DCC.monto
                                                      when tipo = 'O/D' then DCC.monto
                                                      --abono
                                                      when tipo = 'DEP' then -DCC.monto
                                                      when tipo = 'N/C' then -DCC.monto
                                                      when tipo = 'O/C' then -DCC.monto
                                                      when tipo = 'REC' then -DCC.monto
                                                      when tipo = 'RET' then -DCC.monto
                                               end
                                        ) pendiente
                                        from  prodmult.documentos_cc DCC
                                        where f.cliente = DCC.cliente
                    
                                        ) as Pendiente
                    from   prodmult.factura f
                                 , prodmult.mf_pedido mp,prodmult.documentos_cc dc
                    where		mp.financiera=11
		                    and (select max(h.tipo) FROM prodmult.documentos_cc h
				                    where h.cliente=f.cliente)=dc.Tipo
			                    AND dc.cliente=f.cliente
			                    and dc.Tipo='REC'
			                    and dc.Fecha_Documento>=convert(datetime,'07/03/2018',103)
			                    and dc.Fecha_Documento<=convert(datetime,'07/03/2018 23:59:59',103)
                                 and f.pedido = mp.pedido
                                 and f.ANULADA = 'N'
			                    and not exists
			                    (
				                    select 1 from prodmult.mf_factura_estado fa where
				                    fa.factura=f.factura
				                    and fa.estado='D'
			                    )            
             
                    ) data
                    where data.Pendiente=0 -- QUE NO HAYA SALDO PENDIENTE
 
                    order by data.Fecha
                 */
                #endregion
                EXACTUSERPEntities db = new EXACTUSERPEntities();
                db.CommandTimeout = 999999999;
                
                //Query para ubicar los desembolsos de BANCREDIT
                // Se hará mediante el asiento contable de la factura con un abono a favor del cliente.
                var qDesembolsos = from f in dbExactus.FACTURA
                                   join p in dbExactus.MF_Pedido on f.FACTURA1 equals p.FACTURA
                                  
                                   where
                                            //(
                                            //  dbExactus.DOCUMENTOS_CC.Where(
                                            //        c => c.CLIENTE == f.CLIENTE
                                            //    ).OrderByDescending(c => c.CreateDate).FirstOrDefault().TIPO == "REC"
                                            //)
                                            //&&
                                            (dbExactus.DOCUMENTOS_CC.Where(c => c.CLIENTE==f.CLIENTE).Sum(d => (d.TIPO =="FAC" || d.TIPO == "N/D" || d.TIPO == "O/D") ? d.MONTO: -d.MONTO) == 0)
                                           
                                            && f.PEDIDO == p.PEDIDO
                                            && f.ANULADA == "N"
                                            && p.FINANCIERA == nFinanciera // FINANCIERA A OPERAR
                                            &&  (from pfx in dbExactus.MF_Factura_Estado where pfx.FACTURA == f.FACTURA1 && pfx.ESTADO == "D" select pfx).Any()==false

                                   select new
                                   {
                                       FACTURA=f.FACTURA1,
                                       FECHA_DOC=DateTime.Now,
                                       FINANCIERA=(from fi in dbExactus.MF_Financiera where fi.Financiera == p.FINANCIERA select fi).FirstOrDefault().Nombre
                                   };
                if (qDesembolsos != null)
                    if (qDesembolsos.Any())
                    {
                        //llenando la información para la tabla MF_Factura_Estado
                        foreach (var item in qDesembolsos)
                        {
                            MF_Factura_Estado dbFE = new MF_Factura_Estado
                            {
                                FACTURA = item.FACTURA,
                                ESTADO = "D",
                                CreateDate = DateTime.Now,
                                CreatedBy = "FIESTANET",
                                FECHA = item.FECHA_DOC,
                                RecordDate = DateTime.Now
                            };
                            db.MF_Factura_Estado.AddObject(dbFE);
                        }
                        mRespuesta.Exito = true;
                        mRespuesta.Mensaje = "----FINANCIERA "+nFinanciera+" -- " + qDesembolsos.Count() + " desembolsos procesados (MF_Factura_Estado).";
                        db.SaveChanges();
                        log.Info(mRespuesta.Mensaje);
                    }
                    else
                    {
                        mRespuesta.Exito = true;
                        mRespuesta.Mensaje = "----FINANCIERA " + nFinanciera + " -- 0 desembolsos procesados (MF_Factura_Estado).";
                        log.Info(mRespuesta.Mensaje);
                    }


            } catch (Exception e)
            {
                log.Error("---- BANCREDIT --ERROR OPERAR LOS DESEMBOLSOS " + e.Message+" - "+e.StackTrace);
                return new Respuesta(false, e,"DESEMBOLSOS");
            }
            log.Info("----  BANCREDIT-- operación de registro de desembolsos FINALIZADA!");

            return mRespuesta;
        }
    }
}