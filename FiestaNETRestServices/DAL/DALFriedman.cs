﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Models;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static MF_Clases.Clases;

namespace FiestaNETRestServices.DAL
{
    public class DALFriedman : MFDAL
    {

        /// <summary>
        /// Esta función tiene como propósito obtener los datos principales de la financiera
        /// </summary>
        public Respuesta ObtenerPeriodoVigente()
        {
            Respuesta mRespuesta = new Respuesta();
            DateTime mFechaActual = DateTime.Now.Date;
            Periodos mPeriodo = new Periodos();
            try
            {
                //Obtenemos el periodo actual, verificamos que exista al menos un periodo ingresado.
                var mPeriodoActual = (from MPV in dbExactus.MF_MetaPeriodoVendedor
                                      where mFechaActual >= MPV.FECHA_INICIAL &&
                                                mFechaActual <= MPV.FECHA_FINAL &&
                                                MPV.PERIODO != 0
                                      select MPV).First();
                ;

                if (mPeriodoActual == null)
                {
                    //Si no existe informacion del periodo actual, entonces se obtiene el ultimo periodo ingresado.
                    var mUltimaFechaIngresada = (from MPV in dbExactus.MF_MetaPeriodoVendedor
                                                 select MPV.FECHA_INICIAL).Max();

                    mPeriodoActual = (from MPV in dbExactus.MF_MetaPeriodoVendedor
                                      where mUltimaFechaIngresada >= MPV.FECHA_INICIAL &&
                                                mFechaActual <= MPV.FECHA_FINAL &&
                                                MPV.PERIODO != 0
                                      select MPV).First();
                }

                if (mPeriodoActual == null)
                {
                    mPeriodo.Anio = mFechaActual.Year;
                    mPeriodo.Periodo = 1;
                }
                mPeriodo.Anio = mPeriodoActual.ANIO;
                mPeriodo.Periodo = mPeriodoActual.PERIODO;

                mRespuesta.Exito = true;
                mRespuesta.Objeto = mPeriodo;
                return mRespuesta;
            }
            catch
            {
                return new Respuesta(false, "");
            }
        }


        /// <summary>
        /// Esta función tiene como propósito obtener los datos principales de la financiera
        /// </summary>
        public Respuesta ObtenerPeriodosTrimestre(int Anio, int Periodo)
        {
            Respuesta mRespuesta = new Respuesta();
            List<Periodos> mPeriodos = new List<Periodos>();

            int mMesInicialTrimestre = 1;

            try
            {
                //En base al periodo seleccionamos, verificamos que exista al menos uno ingresado
                var mMesEvaluacion = (from MPV in dbExactus.MF_Periodo
                                      where MPV.PERIODO == Periodo
                                      select MPV).FirstOrDefault();

                if (mMesEvaluacion != null)
                {
                    mMesInicialTrimestre = mMesEvaluacion.MES_INICIAL - ((mMesEvaluacion.MES_INICIAL % 3 == 0) ? 2 : (mMesEvaluacion.MES_INICIAL % 3) - 1); ;
                }

                var mPeriodosTmp = (from MPV1 in dbExactus.MF_Periodo
                                    where MPV1.MES_INICIAL >= mMesInicialTrimestre
                                            && MPV1.MES_INICIAL <= mMesInicialTrimestre + 2
                                    select new
                                    {
                                        MPV1.PERIODO
                                        ,
                                        MPV1.MES_INICIAL
                                        ,
                                        MPV1.DIA_INICIAL
                                    }).OrderBy(x => x.PERIODO);

                if (mPeriodosTmp.Count() > 0)
                {
                    foreach (var item in mPeriodosTmp)
                    {
                        Periodos mPeriodo = new Periodos();
                        mPeriodo.Fechas = new DateTime(Anio, item.MES_INICIAL, item.DIA_INICIAL).ToShortDateString();
                        mPeriodo.Anio = Anio;
                        mPeriodo.Periodo = item.PERIODO;
                        mPeriodos.Add(mPeriodo);
                    }
                }

                mRespuesta.Exito = true;
                mRespuesta.Objeto = mPeriodos;
                return mRespuesta;
            }
            catch
            {
                return new Respuesta(false, "");
            }
        }

        /// <summary>
        /// Esta función tiene como propósito obtener los datos principales de la financiera
        /// </summary>
        public Respuesta ObtenerResumenSemanalXTienda(string Tienda, int Anio, int Periodo)
        {
            Respuesta mRespuesta = new Respuesta();
            try
            {
                /*
                 
                 select	v.Nombre
		                , MPV.Dias
		                , MPV.Meta
		                , isnull(sum(f.total_mercaderia),0) VentasReales
		                , isnull(sum(f.total_mercaderia),0) - MPV.meta Diferencia
		                , count(f.factura) CantidadFacturas
		                , isnull(sum(f.total_unidades),0) CantidadArticulos
		                , MPV.minimo Minimo
		                , case when count(f.factura) = 0 then 0 else sum(f.total_mercaderia) / count(f.factura) end FacturaPromedio
		                , case when count(f.factura) = 0 then 0 else sum(f.total_unidades) / count(f.factura) end ArticulosPorFactura
		                , case when MPV.Dias = 0  then 0 else sum(f.total_mercaderia) / MPV.Dias end VentasPorDia
		                , case when MPV.Meta = 0 then 0 else (sum(f.total_mercaderia) /MPV.Meta)*100 end QuetVTAVrsMeta
		                , isnull(case	when sum(f.total_mercaderia) >= MPV.Meta then 1
				                when sum(f.total_mercaderia) < MPV.Meta 
				                    and sum(f.total_mercaderia) > MPV.minimo then 2
				                when sum(f.total_mercaderia) < MPV.minimo  then 3
			                end,3)
                from	prodmult.MF_MetaPeriodoVendedor MPV
		                inner join prodmult.vendedor v
		                on(MPV.VENDEDOR = v.VENDEDOR)
		                inner join prodmult.mf_vendedor mv
		                on (mv.vendedor = v.vendedor )
		                left outer join prodmult.factura f
		                on (f.vendedor = v.vendedor 
			                and MPV.COBRADOR = f.cobrador
			                and MPV.vendedor = f.VENDEDOR
			                and f.fecha between MPV.fecha_inicial and MPV.fecha_final
			                and f.anulada = 'N')
                where	mpv.COBRADOR = 'F25' --> parametro
		                and anio = 2018 --> parametro
		                and periodo =  14--> parametro
		                and MPV.periodo <> 0
                group by --v.vendedor
		                v.nombre
		                , MPV.dias
		                , MPV.minimo
		                , MPV.meta
                order by 1;
                 */

                /**
                 Debido a que entity Framework con ling no soporta joins con sintaxis ">" "<" o between
                        por ejemplo and f.fecha between MPV.fecha_inicial and MPV.fecha_final
                
                 Entonces se tuvo que hacer dos consultas a la base de datos, y luego hacer el left outer join correspondiente.
                 */

                var mVentaSemanal = from MPV1 in dbExactus.MF_MetaPeriodoVendedor
                                    join f in dbExactus.FACTURA on new { MPV1.VENDEDOR, MPV1.COBRADOR } equals new { f.VENDEDOR, f.COBRADOR }
                                    where f.FECHA >= MPV1.FECHA_INICIAL &&
                                              f.FECHA <= MPV1.FECHA_FINAL &&
                                              f.ANULADA == "N" &&
                                              MPV1.PERIODO != 0
                                              && MPV1.COBRADOR == Tienda
                                              && MPV1.ANIO == Anio
                                              && MPV1.PERIODO == Periodo
                                    group new { MPV1, f } by new
                                    {
                                        MPV1.COBRADOR,
                                        MPV1.VENDEDOR,
                                        MPV1.ANIO,
                                        MPV1.PERIODO,
                                        MPV1.FECHA_INICIAL.Month
                                    } into g
                                    select new FriedmanResumenSemanal()
                                    {
                                        Cobrador = g.Key.COBRADOR
                                      ,
                                        Vendedor = g.Key.VENDEDOR
                                      ,
                                        Anio = g.Key.ANIO
                                      ,
                                        Semana = g.Key.PERIODO
                                      ,
                                        VentasReales = (g.Sum(p => p.f.TOTAL_MERCADERIA))
                                      ,
                                        CantFacturas = g.Count(p => p.f.FACTURA1 != null)
                                      ,
                                        CantArticulos = (g.Sum(p => p.f.TOTAL_UNIDADES))
                                      ,
                                        Dias = 0
                                      ,
                                        Meta = 0
                                      ,
                                        Diferencia = 0
                                      ,
                                        ArticulosPorFactura = 0
                                      ,
                                        Minimo = 0
                                      ,
                                        FacturaPromedio = 0
                                      ,
                                        VentasPorDia = 0
                                      ,
                                        QuetVTAvrsMeta = 0
                                      ,
                                        StatusQuetz = 0
                                      ,
                                        Estrategia = ""
                                      ,
                                        Mes = g.Key.Month
                                    }
                    ;

                if (mVentaSemanal.Count() == 0)
                { //return new Respuesta(false, "Aun no existen ventas para esta tienda");
                }

                var mMetaSemanal = from MPV in dbExactus.MF_MetaPeriodoVendedor
                                   join V in dbExactus.VENDEDOR on MPV.VENDEDOR equals V.VENDEDOR1
                                   join MC in dbExactus.MF_Cobrador on Tienda equals MC.COBRADOR
                                   where MPV.COBRADOR == Tienda
                                           && MPV.ANIO == Anio
                                           && MPV.PERIODO == Periodo
                                   select new FriedmanResumenSemanal()
                                   {
                                       Cobrador = MPV.COBRADOR
                                       ,
                                       Vendedor = MPV.VENDEDOR
                                       ,
                                       Anio = MPV.ANIO
                                       ,
                                       Semana = MPV.PERIODO
                                       ,
                                       NombreVendedor = V.NOMBRE
                                       ,
                                       CodigoJefe = MC.CODIGO_JEFE
                                       ,
                                       Dias = MPV.DIAS
                                       ,
                                       Meta = MPV.META
                                       ,
                                       VentasReales = 0
                                       ,
                                       Diferencia = 0
                                       ,
                                       CantFacturas = 0
                                       ,
                                       CantArticulos = 0
                                       ,
                                       Minimo = MPV.MINIMO
                                       ,
                                       FacturaPromedio = 0
                                       ,
                                       ArticulosPorFactura = 0
                                       ,
                                       VentasPorDia = 0
                                       ,
                                       QuetVTAvrsMeta = 0
                                       ,
                                       StatusQuetz = 0
                                       ,
                                       Estrategia = ""
                                       ,
                                       Mes = MPV.FECHA_INICIAL.Month
                                   }
                    ;


                if (mMetaSemanal.Count() == 0)
                {
                    return new Respuesta(false, "No estan efinidas las metas");
                }

                /*
                  en esta parte es donde se hace el left outer join respecto a las metas y sus respectivas ventas
                  esto es porque puede que un o mas vendedores realmente no tengan ventas reales.
                */
                var mResumenSemanal = from MS in mMetaSemanal.ToList()
                                      join VS in mVentaSemanal.ToList()
                                            on new { MS.Cobrador, MS.Vendedor, MS.Anio, MS.Semana }
                                            equals new { VS.Cobrador, VS.Vendedor, VS.Anio, VS.Semana } into MPVE_join
                                      from MPVE in MPVE_join.DefaultIfEmpty()
                                      select new FriedmanResumenSemanal()
                                      {
                                          Cobrador = MS.Cobrador
                                          ,
                                          Vendedor = MS.Vendedor
                                          ,
                                          Anio = MS.Anio
                                          ,
                                          Semana = MS.Semana
                                          ,
                                          NombreVendedor = MS.NombreVendedor
                                          ,
                                          Dias = MS.Dias
                                          ,
                                          Meta = MS.Meta
                                          ,
                                          VentasReales = (MPVE == null ? 0 : MPVE.VentasReales)
                                          ,
                                          Diferencia = (MPVE == null ? 0 - MS.Meta : MPVE.VentasReales - MS.Meta)
                                          ,
                                          CantFacturas = (MPVE == null ? 0 : MPVE.CantFacturas)
                                          ,
                                          CantArticulos = (MPVE == null ? 0 : MPVE.CantArticulos)
                                          ,
                                          Minimo = MS.Minimo
                                          ,
                                          FacturaPromedio = (MPVE == null ? 0 : MPVE.VentasReales / MPVE.CantFacturas)
                                          ,
                                          ArticulosPorFactura = (MPVE == null ? 0 : MPVE.CantArticulos / MPVE.CantFacturas)
                                          ,
                                          VentasPorDia = (MPVE == null || MS.Dias == 0 ? 0 : MPVE.VentasReales / MS.Dias)
                                          ,
                                          QuetVTAvrsMeta = (MPVE == null || MS.Meta == 0 ? 0 : MPVE.VentasReales / MS.Meta)
                                          ,
                                          StatusQuetz = MPVE == null ? (MS.Dias == 0 ? 0 : 3) :
                                                                                (MS.Dias == 0 ? 0 :
                                                                                        (MPVE.VentasReales >= MS.Meta) ? 1 :
                                                                                       ((MPVE.VentasReales < MS.Meta
                                                                                           && MPVE.VentasReales > MS.Minimo) ? 2 :
                                                                                                ((MPVE.VentasReales < MS.Minimo) ? 3 : 0)
                                                                                       )
                                                                               )
                                          ,
                                          Estrategia = ""
                                          ,
                                          Orden = MS.CodigoJefe == null ? 2 : (MS.CodigoJefe == MS.Vendedor ? 1 : 2)
                                          ,
                                          Mes = MS.Mes
                                      }
                                        ;

                if (mResumenSemanal.Count() > 0)
                {
                    mRespuesta.Exito = Const.OK;
                    var mResumenSemanalOrdenada = mResumenSemanal.OrderBy(x => x.Orden);
                    mRespuesta.Objeto = mResumenSemanalOrdenada.ToList();
                }

                return mRespuesta;
            }
            catch (Exception e)
            {
                return new Respuesta(false, e, "");
            }
        }
    }
}