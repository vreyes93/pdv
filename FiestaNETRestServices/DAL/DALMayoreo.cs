﻿using FiestaNETRestServices.Content.Abstract;
using MF.Comun.Dto;
using MF_Clases;
using MF_Clases.Utils;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace FiestaNETRestServices.DAL
{
    public class DALMayoreo : MFDAL
    {
        private new static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public Respuesta getSetCamasIncompletos()
        {
            List<SetCamas> lstSets = new List<SetCamas>();
            Respuesta mRespuesta = new Respuesta();
            try
            {
                log.Info("--Iniciando la validación de los SETS para mayoreo--");
                

                /*
                 * Query sql
                 * SELECT a.articulo,a.descripcion
                    FROM PRODMULT.ARTICULO_ENSAMBLE e,
                    prodmult.articulo a
                    WHERE ARTICULO_PADRE LIKE 'M149%'
                    and e.articulo_padre=a.articulo
                    GROUP BY a.articulo,a.descripcion
                    HAVING COUNT(*)<3
	
                 */
                var qArticulosSets = (from a in dbExactus.ARTICULO
                                      join ae in dbExactus.ARTICULO_ENSAMBLE on a.ARTICULO1 equals ae.ARTICULO_PADRE
                                      where
                                      ae.ARTICULO_PADRE.Contains("M149")
                                      && a.ACTIVO == "S"
                                      group ae by new { a.ARTICULO1, a.DESCRIPCION } into grp
                                      where grp.Count() < 3//patas, cama y colchón, adicionalmente pueden venir almohadas, pero lo indispensable son 3

                                      select grp.Key);
                                      
                if (qArticulosSets.Count() > 0)
                {
                    foreach (var item in qArticulosSets)
                        lstSets.Add(new SetCamas { codArticulo = item.ARTICULO1, DescripcionSet = item.DESCRIPCION });
                }
                mRespuesta.Objeto = lstSets;
                mRespuesta.Exito = true;
            } catch (Exception ex)
            {
                mRespuesta.Exito = false;
                mRespuesta.Mensaje = "Problema al obtener el reporte de sets de camas. "+ex.Message;
                log.Error(mRespuesta.Mensaje);
            }
            log.Info("--Finalizando la validación de los sets de Mayoreo, se encontraron "+lstSets.Count+" sets inválidos--");
            return mRespuesta;
        }

        public Respuesta getLotesPatas(List<DtoLoteBases> LoteBases)
        {
            //Debido a que la configuración de la cantidad de patas se obtiene
            // a partir de los sets, es necesario obtener la cantidad de bases por set
            //para dividir la cantidad de patas / cantidad de bases ya que en el set ya se encuentra 
            //la cantidad completa de patas por set y lo que necesitamos es cantidad de 
            //patas por 'BASE'
            Respuesta mRespuesta = new Respuesta();
            mRespuesta.Exito = true;
            List<DtoLotePatas> lstPatas = new List<DtoLotePatas>();
            string baseError = string.Empty;
            for (int i=0;i<LoteBases.Count;i++)
            {
                try
                {
                    string codigoBase = string.Empty;
                    //string descripcionBase = string.Empty;
                    int cantidadBases= LoteBases[i].Cantidad;//Cantidad de bases solicitadas
                    codigoBase = LoteBases[i].Base.codArticulo;
                    baseError = codigoBase;
                    //descripcionBase= LoteBases[i].Base.Descripcion;
                    //codigoBase = codigoBase.Replace("M", "");
                    //var s = (from ae in dbExactus.ARTICULO_ENSAMBLE join a in dbExactus.ARTICULO on ae.ARTICULO_PADRE equals a.ARTICULO1 where a.DESCRIPCION.Contains("SET") && a.ARTICULO1.Contains("M") == false && ae.ARTICULO_HIJO.Contains(codigoBase) select ae);
                    //if (s.Count() == 0)
                    //    s = (from ae in dbExactus.ARTICULO_ENSAMBLE join a in dbExactus.ARTICULO on ae.ARTICULO_PADRE equals a.ARTICULO1 where a.DESCRIPCION.Contains("SET") && a.ARTICULO1.Contains("M") == true && ae.ARTICULO_HIJO.Contains(codigoBase) select ae);

                    //decimal nCantBasesxSet;


                    //nCantBasesxSet = decimal.Parse(s.FirstOrDefault().CANTIDAD.ToString());

                    //string codSET = s.FirstOrDefault().ARTICULO_PADRE;

                    //var qY = (from pata in dbExactus.ARTICULO_ENSAMBLE
                    //          join a in dbExactus.ARTICULO on pata.ARTICULO_HIJO equals a.ARTICULO1
                    //          where a.DESCRIPCION.Contains("PATA")
                    //            && codSET == pata.ARTICULO_PADRE
                    //          select new { CANTIDAD = pata.CANTIDAD, ARTICULO = a.ARTICULO1, DeSCRIPCION = a.DESCRIPCION }).FirstOrDefault();

                    //Select de la informacion del articulo
                    String modeloPatas = string.Empty;
                    

                    //validacion de articulo inicia con la letra M
                    try 
                    {
                        modeloPatas = (from p in dbExactus.ARTICULO_ESPE where p.ARTICULO == codigoBase && p.ATRIBUTO == "MODELO_PATAS" select p).First().VALOR;
                    }
                    catch 
                    {
                        //cambiando el codgo de la base
                        String xbase = codigoBase.Substring(0, 1);
                        
                        if (xbase.Equals("M")) 
                        {
                            //quitando la m del inicio
                            codigoBase = codigoBase.Replace("M","");
                            modeloPatas = (from p in dbExactus.ARTICULO_ESPE where p.ARTICULO == codigoBase && p.ATRIBUTO == "MODELO_PATAS" select p).First().VALOR;
                        }
                        else 
                        {
                            //colocando una M al incio de la cadena que identifica la base
                            codigoBase = "M" + codigoBase;
                            modeloPatas = (from p in dbExactus.ARTICULO_ESPE where p.ARTICULO == codigoBase && p.ATRIBUTO == "MODELO_PATAS" select p).First().VALOR;
                        }

                    }

                    
                    String cantidadPatas = Convert.ToInt32((from p in dbExactus.ARTICULO_ESPE where p.ARTICULO == codigoBase && p.ATRIBUTO == "CANTIDAD_PATAS" select p).First().NORMAL).ToString();
                    String descripcionPatas = (from p in dbExactus.ARTICULO where p.ARTICULO1 == modeloPatas select p).First().DESCRIPCION;
                    String xIncluye;
                    try
                    {
                        xIncluye = (from p in dbExactus.ARTICULO_ESPE where p.ARTICULO == codigoBase && p.ATRIBUTO == "INCLUYE_PATAS" select p).First().VALOR;
                        
                    
                    }
                    catch (Exception ex)
                    {
                        //Si no encuentra la configuracion en db por defecto incluye patas
                        log.Info("No se encontro la configuracion incluye patas, por defecto incluye.");
                        xIncluye = "S";
                    }

                    //valida si el articulo incluye patas
                    if (!xIncluye.Equals("S"))
                    {
                        //No incluye patas
                    }
                    else
                    {
                        lstPatas.Add(new DtoLotePatas { Cantidad = decimal.Parse(cantidadPatas), ModeloPata = new DtoPatas { codArticulo = modeloPatas, Descripcion = descripcionPatas } });
                    }

                } catch(Exception ex)
                {
                    //mRespuesta.Exito &= false;
                    log.Error("No se encontró la configuración incluye_patas para el artículo: " + baseError);

                    var response = new HttpResponseMessage(HttpStatusCode.NotFound)
                    {
                        Content = new StringContent("No se encontró la configuración incluye_patas para el artículo: " + baseError+", para mas informacion comuniquese con el departamento de importaciones.", System.Text.Encoding.UTF8, "text/plain"),
                        StatusCode = HttpStatusCode.NotFound
                    };
                     throw new HttpResponseException(response);
                }
            }

           //lstPatas.Add(new DtoLotePatas { Cantidad = (qY.CANTIDAD / nCantBasesxSet) * cantidadBases, ModeloPata = new DtoPatas { codArticulo = qY.ARTICULO, Descripcion = qY.DeSCRIPCION } });

            mRespuesta.Objeto = lstPatas;
           
            return mRespuesta;
        }

        /// <summary>
        /// Método para devolver la información de las facturas de mayoreo en base a la orden de compra
        /// la cual se guarda dentro del campo de observaciones.
        /// </summary>
        /// <param name="OrdenDeCompra"></param>
        /// <returns></returns>
        public Respuesta getFacturas(string OrdenDeCompra,string FechaFacturaI=null,string FechaFacturaF=null,string Cliente=null,string NombreCliente=null,string Tienda=null)
        {
            List<MF_Clases.FacturaMayoreo> lstFacturas = new List<FacturaMayoreo>();
           
            try
            {
                DateTime dtFechaFacI=new DateTime();
                DateTime dtFechaFacF = new DateTime();

                log.Debug("Fecha Factura: "+FechaFacturaI+" Fecha Factura Final "+FechaFacturaF);
                
                if (FechaFacturaI != null && FechaFacturaI != string.Empty)
                {
                    try
                    {
                        dtFechaFacI = Utilitarios.dateParseFormatDMY(FechaFacturaI.Contains(" ")? FechaFacturaI.Substring(0, FechaFacturaI.IndexOf(" ")) : FechaFacturaI);
                    }
                    catch
                    {
                        log.Error("Error al parsear la fecha inicial ingresada para la consulta de Orden de Compra Mayoreo " + FechaFacturaI);
                    }
                }
                if (FechaFacturaF != null && FechaFacturaF != string.Empty)
                {
                    try
                    {
                        dtFechaFacF = Utilitarios.dateParseFormatDMY(FechaFacturaF.Contains(" ") ? FechaFacturaF.Substring(0, FechaFacturaF.IndexOf(" ")) : FechaFacturaF);
                        dtFechaFacF= dtFechaFacF.AddHours(23).AddMinutes(59);
                    }
                    catch
                    {
                        log.Error("Error al parsear la fecha inicial ingresada para la consulta de Orden de Compra Mayoreo " + FechaFacturaI);
                    }
                }
                var qConsulta = (OrdenDeCompra != string.Empty && OrdenDeCompra !=null) ?
                                (
                                  from ped in dbExactus.PEDIDO
                                  join fa in dbExactus.FACTURA on ped.PEDIDO1 equals fa.PEDIDO
                                  join f in dbExactus.MF_Factura on fa.FACTURA1 equals f.FACTURA
                                  join cli in dbExactus.CLIENTE on fa.CLIENTE equals cli.CLIENTE1
                                  where
                                  ped.OBSERVACIONES.Contains(OrdenDeCompra)
                                  && fa.ANULADA != "S"
                                  && fa.RUBRO5 != null // que sean FEL
                                  && fa.RUBRO5 != ""
                                  orderby fa.CreateDate ascending


                                  select new 
                                  {
                                      Factura = fa.FACTURA1,
                                      Cliente = cli.CLIENTE1,
                                      NombreCliente = cli.NOMBRE,
                                      OrdenDeCompra = ped.OBSERVACIONES,
                                      FechaPedido = fa.FECHA,
                                      CantArtículos = fa.TOTAL_UNIDADES,
                                      TotalFactura = fa.TOTAL_FACTURA,
                                      Estado=fa.ANULADA.Equals("S")?"Anulada":"Vigente",
                                      SolAnulacion=f.SE_ANULARA,
                                      Tienda = f.COBRADOR,
                                      linkFactura = !fa.ANULADA.Equals("S") ? (((cli.CLIENTE1.Contains("MA") || fa.MONEDA.Equals("D"))? CONSTANTES.IMPRESION.URL_IMPRESION_MAYOREO_DEFAULT  : CONSTANTES.IMPRESION.URL_IMPRESION_FIESTA_DEFAULT)+ fa.FACTURA1):string.Empty
                                  }
                                 )
                                 :  (FechaFacturaI != null && FechaFacturaI != string.Empty && dtFechaFacI.Year>2018
                                     && FechaFacturaF != null && FechaFacturaF != string.Empty && dtFechaFacF.Year > 2018)?
                                 (
                                 
                                 from fa in dbExactus.FACTURA 
                                 join ped in dbExactus.PEDIDO on fa.PEDIDO equals ped.PEDIDO1 into joinP from x in joinP.DefaultIfEmpty()
                                 join f in dbExactus.MF_Factura on fa.FACTURA1 equals f.FACTURA
                                 join cli in dbExactus.CLIENTE on fa.CLIENTE equals cli.CLIENTE1
                                 where
                                 EntityFunctions.DiffDays(fa.FECHA, dtFechaFacI) >= 0
                                 && EntityFunctions.DiffDays(fa.FECHA, dtFechaFacF) <= 0
                                 && fa.RUBRO5 != null // que sean FEL
                                 && fa.ANULADA != "S"
                                 && fa.RUBRO5 != ""
                                 orderby fa.CreateDate ascending


                                 select new 
                                 {
                                     Factura = fa.FACTURA1,
                                     Cliente = cli.CLIENTE1,
                                     NombreCliente = cli.NOMBRE,
                                     OrdenDeCompra = x.OBSERVACIONES,
                                     FechaPedido = fa.FECHA,
                                     CantArtículos = fa.TOTAL_UNIDADES,
                                     TotalFactura = fa.TOTAL_FACTURA,
                                     Estado = fa.ANULADA.Equals("S") ? "Anulada" : "Vigente",
                                     SolAnulacion = f.SE_ANULARA,
                                     Tienda = f.COBRADOR,
                                     linkFactura = !fa.ANULADA.Equals("S") ? (((cli.CLIENTE1.Contains("MA") || fa.MONEDA.Equals("D"))? CONSTANTES.IMPRESION.URL_IMPRESION_MAYOREO_DEFAULT : CONSTANTES.IMPRESION.URL_IMPRESION_FIESTA_DEFAULT) + fa.FACTURA1) : string.Empty
                                 }
                                 )
                                 : (Cliente != null)?
                                (
                                 from fa in dbExactus.FACTURA
                                 join ped in dbExactus.PEDIDO on fa.PEDIDO equals ped.PEDIDO1 into joinP
                                 from x in joinP.DefaultIfEmpty()
                                 join f in dbExactus.MF_Factura on fa.FACTURA1 equals f.FACTURA
                                  join cli in dbExactus.CLIENTE on fa.CLIENTE equals cli.CLIENTE1
                                  where
                                    cli.CLIENTE1.Equals(Cliente) && Cliente != null
                                  && fa.ANULADA != "S"
                                    && fa.RUBRO5 != ""
                                   && fa.RUBRO5 != null // que sean FEL
                                  orderby fa.CreateDate ascending


                                  select new 
                                  {
                                      Factura = fa.FACTURA1,
                                     
                                      Cliente = cli.CLIENTE1,
                                      NombreCliente = cli.NOMBRE,
                                      OrdenDeCompra = x.OBSERVACIONES,
                                      FechaPedido = fa.FECHA,
                                      CantArtículos = fa.TOTAL_UNIDADES,
                                      TotalFactura = fa.TOTAL_FACTURA,
                                      Estado = fa.ANULADA.Equals("S") ? "Anulada" : "Vigente",
                                      SolAnulacion = f.SE_ANULARA,
                                      Tienda = f.COBRADOR,
                                      linkFactura = !fa.ANULADA.Equals("S") ? (((cli.CLIENTE1.Contains("MA") || fa.MONEDA.Equals("D")) ? CONSTANTES.IMPRESION.URL_IMPRESION_MAYOREO_DEFAULT : CONSTANTES.IMPRESION.URL_IMPRESION_FIESTA_DEFAULT) + fa.FACTURA1) : string.Empty
                                  }
                                 )
                                 : (Tienda != null) ?
                                  (
                                 from fa in dbExactus.FACTURA
                                 join ped in dbExactus.PEDIDO on fa.PEDIDO equals ped.PEDIDO1 into joinP
                                 from x in joinP.DefaultIfEmpty()
                                 join f in dbExactus.MF_Factura on fa.FACTURA1 equals f.FACTURA
                                  join cli in dbExactus.CLIENTE on fa.CLIENTE equals cli.CLIENTE1
                                  where
                                    fa.COBRADOR.Equals(Tienda) && Tienda != ""
                                    && fa.ANULADA != "S"
                                    && fa.RUBRO5 != ""
                                    && fa.RUBRO5 != null // que sean FEL
                                  orderby fa.CreateDate ascending


                                  select new
                                  {
                                      Factura = fa.FACTURA1,
                                    
                                      Cliente = cli.CLIENTE1,
                                      NombreCliente = cli.NOMBRE,
                                      OrdenDeCompra = x.OBSERVACIONES,
                                      FechaPedido = fa.FECHA,
                                      CantArtículos = fa.TOTAL_UNIDADES,
                                      TotalFactura = fa.TOTAL_FACTURA,
                                      Estado = fa.ANULADA.Equals("S") ? "Anulada" : "Vigente",
                                      SolAnulacion = f.SE_ANULARA,
                                      Tienda = f.COBRADOR,
                                      linkFactura = !fa.ANULADA.Equals("S") ? (((cli.CLIENTE1.Contains("MA") || fa.MONEDA.Equals("D")) ? CONSTANTES.IMPRESION.URL_IMPRESION_MAYOREO_DEFAULT : CONSTANTES.IMPRESION.URL_IMPRESION_FIESTA_DEFAULT) + fa.FACTURA1) : string.Empty
                                  }
                                 )
                                 :
                                 (
                                 from fa in dbExactus.FACTURA
                                 join ped in dbExactus.PEDIDO on fa.PEDIDO equals ped.PEDIDO1 into joinP
                                 from x in joinP.DefaultIfEmpty()
                                 join f in dbExactus.MF_Factura on fa.FACTURA1 equals f.FACTURA
                                  join cli in dbExactus.CLIENTE on fa.CLIENTE equals cli.CLIENTE1
                                  where
                                    cli.NOMBRE.Contains(NombreCliente) && NombreCliente != null
                                    && fa.ANULADA != "S"
                                    && fa.RUBRO5 != ""
                                    && fa.RUBRO5 != null // que sean FEL
                                  orderby fa.CreateDate ascending


                                  select new
                                  {
                                      Factura = fa.FACTURA1,
                                     
                                      Cliente = cli.CLIENTE1,
                                      NombreCliente = cli.NOMBRE,
                                      OrdenDeCompra = x.OBSERVACIONES,
                                      FechaPedido = fa.FECHA,
                                      CantArtículos = fa.TOTAL_UNIDADES,
                                      TotalFactura = fa.TOTAL_FACTURA,
                                      Estado = fa.ANULADA.Equals("S") ? "Anulada" : "Vigente",
                                      SolAnulacion = f.SE_ANULARA,
                                      Tienda=f.COBRADOR,
                                      linkFactura = !fa.ANULADA.Equals("S") ? (((cli.CLIENTE1.Contains("MA") || fa.MONEDA.Equals("D")) ? CONSTANTES.IMPRESION.URL_IMPRESION_MAYOREO_DEFAULT : CONSTANTES.IMPRESION.URL_IMPRESION_FIESTA_DEFAULT) + fa.FACTURA1) : string.Empty
                                  }
                                 )
                                 ;


                if(Cliente!=null)
                    if(qConsulta.Count()>0 && Cliente != string.Empty)
                         qConsulta = qConsulta.Where(x => x.Cliente.Equals(Cliente));

                if (FechaFacturaI != null && FechaFacturaI != string.Empty && dtFechaFacI.Year > 2018
                    && FechaFacturaF != null && FechaFacturaF != string.Empty && dtFechaFacF.Year > 2018)
                    qConsulta=qConsulta.Where(x => EntityFunctions.DiffDays(x.FechaPedido, dtFechaFacI) >= 0  &&
                        EntityFunctions.DiffDays(x.FechaPedido, dtFechaFacF) <= 0);

                if (NombreCliente != null)
                    if (qConsulta.Count() > 0 && NombreCliente != string.Empty)
                        qConsulta = qConsulta.Where(x => x.NombreCliente.Contains(NombreCliente));

                if (Tienda != null)
                    if (qConsulta.Count() > 0 && Tienda != string.Empty)
                        qConsulta = qConsulta.Where(x => x.Tienda.Equals(Tienda));
                if (qConsulta.Count() > 0)
                {
                    
                    foreach (var item in qConsulta)
                    {
                        
                        lstFacturas.Add(new FacturaMayoreo
                        {
                            Factura= item.Factura,
                            Cliente = item.Cliente,
                            NombreCliente = item.NombreCliente,
                            OrdenDeCompra = item.OrdenDeCompra,
                            FechaPedido = item.FechaPedido.ToString ("dd/MM/yyyy"),
                            CantArtículos = item.CantArtículos,
                            TotalFactura = item.TotalFactura,
                            Estado=item.Estado,
                            SolAnulacion=item.SolAnulacion,
                            linkFactura = item.linkFactura
                        });
                    }
                }
                Respuesta.Exito = true;
                
                Respuesta.Objeto = lstFacturas;
            }
            catch (Exception ex)
            {
                Respuesta.Exito = false;
                Respuesta.Mensaje = CatchClass.ExMessage(ex, "DALMayoreo", "getFacturas(ordenDeCompra)");
                log.Error(Respuesta.Mensaje);
            }
            return Respuesta;
        }
    }
}
                                    
                                    
                                    
                 