﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Models;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using static MF_Clases.Clases;

namespace FiestaNETRestServices.DAL
{
    public class DALPedidoInterconsumo : MFDAL
    {

        /// <summary>
        /// Esta función tiene como propósito verificar si la cotización de interconsumo existe.
        /// </summary>
        public Respuesta ExistePedido(String Pedido)
        {
            MF_Clases.Clases.Pedido mPedido;
            

            if ((from p in dbExactus.MF_Pedido where p.PEDIDO == Pedido && p.FINANCIERA ==7 select p).Count() == 0)
            {
                return new Respuesta(false, string.Format("El pedido {0} no existe no pertenece a Interconsumo.", Pedido));
            }

            var qPedidoInterconsumo = (from mp in dbExactus.MF_Pedido
                                       join p in dbExactus.PEDIDO on mp.PEDIDO equals p.PEDIDO1
                                       where    mp.PEDIDO == Pedido
                                                   && mp.FINANCIERA == 7
                                           select new
                                           {
                                               CLIENTE = mp.CLIENTE
                                                       ,
                                               GRABADA_EN_LINEA = mp.GRABADA_EN_LINEA
                                                       ,
                                               TOTAL_FACTURAR = mp.TOTAL_FACTURAR
                                                       ,
                                               SOLICITUD = mp.SOLICITUD
                                                       ,
                                               COBRADOR = p.COBRADOR
                                                       ,
                                               ENGANCHE = mp.ENGANCHE
                                           }
                                ).FirstOrDefault();

            mPedido = new MF_Clases.Clases.Pedido();
            mPedido.CLIENTE = qPedidoInterconsumo.CLIENTE;
            mPedido.ENGANCHE = qPedidoInterconsumo.ENGANCHE;
            mPedido.GRABADA_EN_LINEA = qPedidoInterconsumo.GRABADA_EN_LINEA;
            mPedido.SOLICITUD = qPedidoInterconsumo.SOLICITUD;
            mPedido.COBRADOR = qPedidoInterconsumo.COBRADOR;
            mPedido.TOTAL_FACTURAR = qPedidoInterconsumo.TOTAL_FACTURAR;

            
            return new Respuesta(true, "", mPedido);
        }

        /// <summary>
        /// Esta función tiene como propósito verificar si la cotización es de interconsumo existe.
        /// </summary>
        public Respuesta PedidosDeInterconsumo(string Pedido)
        {
            if ((from p in dbExactus.MF_Pedido where p.PEDIDO == Pedido && p.FINANCIERA == 7 select p).Count() == 0)
            {
                return new Respuesta(false, string.Format("El pedido {0} no pertenece a Interconsumo.", Pedido));
            }

            return new Respuesta(true);
        }


        /// <summary>
        /// Esta función tiene como propósito verificar si la cotización es de interconsumo existe.
        /// </summary>
        public Respuesta PedidoFacturado(string Pedido)
        {
            if ((from f in dbExactus.FACTURA where f.PEDIDO == Pedido && f.ANULADA == "N" select f).Count() > 0)
            {
                return new Respuesta(false, string.Format("El pedido {0} ya se encuentra facturado.", Pedido));
            }

            return new Respuesta(true, "");
        }


        /// <summary>
        /// Esta función tiene como propósito verificar si el pedido tiene estado inicial
        /// </summary>
        public Respuesta PedidoConEstadoInicial(string Pedido)
        {
            if ((from p in dbExactus.MF_Pedido where p.PEDIDO == Pedido && p.ESTADO_INICIAL == null select p).Count() > 0)
            {
                return new Respuesta(false, string.Format("El pedido {0} aún no tiene un estado inicial, puede ser porque no ha capturado el DPI, la huella y la fotografía del cliente en el portal.", Pedido));
            }

            return new Respuesta(true, "");
        }

        /// <summary>
        /// Esta función tiene como objetivo validar el pedido
        /// </summary>
        public Respuesta ValidaPedidoInterconsumo(string Pedido)
        {


            //Validamos que exista el pedido en el sistema
            Respuesta mRespuesta = ExistePedido(Pedido);

            if (!mRespuesta.Exito)
                return mRespuesta;

            MF_Clases.Clases.Pedido mPedido = (MF_Clases.Clases.Pedido)mRespuesta.Objeto;

            //--------------------------------------------------------------------------
            //Validamos que la cotizacion haya sido grabada en línea.
            //--------------------------------------------------------------------------
            if (string.IsNullOrEmpty(mPedido.GRABADA_EN_LINEA) || mPedido.GRABADA_EN_LINEA == "N")
            {
                return new Respuesta(false, string.Format("El pedido {0} no fue grabada en línea.", Pedido));
            }

            //--------------------------------------------------------------------------
            //Verificamos si el pedido ya esta facturado
            //--------------------------------------------------------------------------
            mRespuesta = PedidoFacturado(Pedido);
            if (!mRespuesta.Exito)
                return mRespuesta;

            //--------------------------------------------------------------------------
            //Verificamos que el pedido tenga asociada una solicitud de crédito
            //--------------------------------------------------------------------------
            if (string.IsNullOrEmpty(mPedido.SOLICITUD.ToString()) && mPedido.SOLICITUD > 0)
            {
                return new Respuesta(false, string.Format("El pedido {0} no tiene solicitud de crédito asignada.", Pedido));
            }

            //--------------------------------------------------------------------------
            //Validamos que la cotizacion haya sido grabada en línea.
            //--------------------------------------------------------------------------
            if (string.IsNullOrEmpty(mPedido.GRABADA_EN_LINEA) || mPedido.GRABADA_EN_LINEA == "N")
            {
                return new Respuesta(false, string.Format("El pedido {0} no fue grabada en línea.", Pedido));
            }

            //--------------------------------------------------------------------------
            //Validamos que el pedido tenga un estado inicial.
            //--------------------------------------------------------------------------
            mRespuesta = PedidoConEstadoInicial(Pedido);
            if (!mRespuesta.Exito)
                return mRespuesta;

            return new Respuesta(true, "", mPedido);
        }


        public Respuesta GrabarSolicitud(SolicitudInterconsumo SolicitudInterconsumo)
        {
            decimal? mPrecioTotal = 0;
            decimal? diferencia = 0;
            int mCantidadLineas = 0, itemNoMPL = 0;
            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    var qUpdateMFPedido = from p in dbExactus.MF_Pedido where p.PEDIDO == SolicitudInterconsumo.NUMERO_DOCUMENTO select p;
                    foreach (var item in qUpdateMFPedido)
                    {
                        item.XML_CONSULTA = SolicitudInterconsumo.XML_CONSULTA;
                        item.XML_CONSULTA_RESPUESTA = SolicitudInterconsumo.XML_CONSULTA_RESPUESTA;
                        item.AUTORIZACION_EN_LINEA = "S";
                        item.NOMBRE_AUTORIZACION = SolicitudInterconsumo.USUARIO_AUTORIZO;
                        item.AUTORIZACION = SolicitudInterconsumo.AUTORIZACION;


                        //---------------------------------------------------------------------------------------------------
                        //Actualizamos el estado inicial, si solo si, es la primera vez que se obtiene un estado
                        //---------------------------------------------------------------------------------------------------
                        if (string.IsNullOrEmpty(item.ESTADO_INICIAL) && SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.APROBADO)
                            item.ESTADO_INICIAL = "A";

                        if (string.IsNullOrEmpty(item.ESTADO_INICIAL) && SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.RECHAZADO)
                            item.ESTADO_INICIAL = "R";

                        if (string.IsNullOrEmpty(item.ESTADO_INICIAL) && SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.DIFERIDO)
                            item.ESTADO_INICIAL = "D";

                        if (string.IsNullOrEmpty(item.ESTADO_INICIAL) && SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.PREAUTORIZADO)
                            item.ESTADO_INICIAL = "P";

                        //---------------------------------------------------------------------------------------------------
                        //Actualizamos el estado final, es el estado que devuelve el servicio de interconsumo.
                        //---------------------------------------------------------------------------------------------------
                        if (SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.APROBADO)
                            item.ESTADO_FINAL = "A";

                        if (SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.RECHAZADO)
                            item.ESTADO_FINAL = "R";

                        if (SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.DIFERIDO)
                            item.ESTADO_FINAL = "D";

                        if (SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.PREAUTORIZADO)
                            item.ESTADO_FINAL = "P";

                        //---------------------------------------------------------------------------------------------------
                        //Actualizamos la fecha de aprobacion_solicitud, si el nuevo estado es aprobado, o rechazado.
                        //---------------------------------------------------------------------------------------------------
                        if ((SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.APROBADO)
                            || (SolicitudInterconsumo.ESTADO == Const.ESTADO_SOLICITUD_INTERCONSUMO.RECHAZADO))
                            item.FECHA_APROBACION_SOLICITUD = DateTime.Now;

                        //---------------------------------------------------------------------------------------------------
                        //Actualizamos el factor y los respectivos montos
                        //---------------------------------------------------------------------------------------------------
                        item.MONTO_PAGOS1 = Decimal.Round(SolicitudInterconsumo.CUOTA, 2);
                        item.MONTO_PAGOS2 = Decimal.Round(SolicitudInterconsumo.ULTIMA_CUOTA, 2);
                        item.MONTO = Decimal.Round(((SolicitudInterconsumo.PLAZO - 1) * SolicitudInterconsumo.CUOTA) + SolicitudInterconsumo.ULTIMA_CUOTA, 2);
                        item.FACTOR = Decimal.Round((((SolicitudInterconsumo.PLAZO - 1) * SolicitudInterconsumo.CUOTA) + SolicitudInterconsumo.ULTIMA_CUOTA) / (SolicitudInterconsumo.VALOR_SOLICITUD), 6);

                        //---------------------------------------------------------------------------------------------------
                        //Recorremos las lineas correspondientes
                        //---------------------------------------------------------------------------------------------------
                        var qUpdatePedidoLinea = from c in dbExactus.MF_Pedido_Linea where c.PEDIDO == SolicitudInterconsumo.NUMERO_DOCUMENTO select c;


                        if (qUpdatePedidoLinea.Count() > 0)
                            mCantidadLineas = qUpdatePedidoLinea.Count();

                        foreach (var itemMPL in qUpdatePedidoLinea)
                        {
                            itemNoMPL = itemNoMPL + 1;

                            itemMPL.PRECIO_UNITARIO = Decimal.Round( (itemMPL.PRECIO_UNITARIO_FACTURAR * Decimal.Round((((SolicitudInterconsumo.PLAZO - 1) * SolicitudInterconsumo.CUOTA) + SolicitudInterconsumo.ULTIMA_CUOTA) / (SolicitudInterconsumo.VALOR_SOLICITUD), 6)) ?? 0 ,2);
                            itemMPL.PRECIO_TOTAL = Decimal.Round( (itemMPL.PRECIO_TOTAL_FACTURAR * Decimal.Round((((SolicitudInterconsumo.PLAZO - 1) * SolicitudInterconsumo.CUOTA) + SolicitudInterconsumo.ULTIMA_CUOTA) / (SolicitudInterconsumo.VALOR_SOLICITUD), 6)) ?? 0, 2);

                            mPrecioTotal = mPrecioTotal +  Decimal.Round( (itemMPL.PRECIO_TOTAL_FACTURAR * Decimal.Round((((SolicitudInterconsumo.PLAZO - 1) * SolicitudInterconsumo.CUOTA) + SolicitudInterconsumo.ULTIMA_CUOTA) / (SolicitudInterconsumo.VALOR_SOLICITUD), 6)) ?? 0, 2);

                            //---------------------------------------------------------------------------------------------------
                            //Realizamos el ajuste necesario para para ajustar los centavos faltantes o sobrantes
                            //respecto a los calculos del factor de Interconsumo
                            //---------------------------------------------------------------------------------------------------
                            if (itemNoMPL == mCantidadLineas)
                            {
                                //Si el precio calculado es menor al monto, le sumamos la diferencia.
                                if (mPrecioTotal < Decimal.Round(((SolicitudInterconsumo.PLAZO - 1) * SolicitudInterconsumo.CUOTA) + SolicitudInterconsumo.ULTIMA_CUOTA, 2))
                                {
                                    diferencia = Decimal.Round(((SolicitudInterconsumo.PLAZO - 1) * SolicitudInterconsumo.CUOTA) + SolicitudInterconsumo.ULTIMA_CUOTA, 2) - mPrecioTotal;
                                    itemMPL.PRECIO_TOTAL = mPrecioTotal + diferencia;
                                }

                                //Si el precio calculado es mayo al monto, le restamos la diferencia.
                                if (mPrecioTotal > Decimal.Round(((SolicitudInterconsumo.PLAZO - 1) * SolicitudInterconsumo.CUOTA) + SolicitudInterconsumo.ULTIMA_CUOTA, 2))
                                {
                                    diferencia = mPrecioTotal - Decimal.Round(((SolicitudInterconsumo.PLAZO - 1) * SolicitudInterconsumo.CUOTA) + SolicitudInterconsumo.ULTIMA_CUOTA, 2);
                                    itemMPL.PRECIO_TOTAL = mPrecioTotal - diferencia;
                                }
                            }
                        }

                        //---------------------------------------------------------------------------------------------------
                        //Actualizamos los nombres del cliente
                        //---------------------------------------------------------------------------------------------------
                        if (SolicitudInterconsumo.PRIMER_NOMBRE.Trim().Length > 0)
                        {
                            var qUpdateMF_Cliente = from c in dbExactus.MF_Cliente where c.CLIENTE == SolicitudInterconsumo.CLIENTE select c;
                            foreach (var itemMC in qUpdateMF_Cliente)
                            {
                                itemMC.PRIMER_NOMBRE = SolicitudInterconsumo.PRIMER_NOMBRE;
                                itemMC.SEGUNDO_NOMBRE = SolicitudInterconsumo.SEGUNDO_NOMBRE;
                                itemMC.TERCER_NOMBRE = SolicitudInterconsumo.TERCER_NOMBRE;
                                itemMC.PRIMER_APELLIDO = SolicitudInterconsumo.PRIMER_APELLIDO;
                                itemMC.SEGUNDO_APELLIDO = SolicitudInterconsumo.SEGUNDO_APELLIDO;
                                itemMC.APELLIDO_CASADA = SolicitudInterconsumo.APELLIDO_CASADA;
                                itemMC.NOMBRE_FACTURA = SolicitudInterconsumo.NOMBRE;
                            }

                            var qUpdateCliente = from c in dbExactus.CLIENTE where c.CLIENTE1 == SolicitudInterconsumo.CLIENTE select c;
                            foreach (var itemC in qUpdateCliente)
                            {
                                itemC.NOMBRE = SolicitudInterconsumo.NOMBRE;
                            }
                        }
                    }

                    dbExactus.SaveChanges();
                    transactionScope.Complete();
                }
            }
            catch (Exception e)
            {
                log.Error(this.GetType().Name + " " + e.StackTrace);
                Utilitario.EnviarEmailIT(this.GetType().Name, e.StackTrace);
                return new Respuesta(false, "" + e.StackTrace);
            }
            return new Respuesta(true, "");
        }

        public Respuesta GrabarFechaImpresionPagare(string Pedido )
        {
            Respuesta mRespuesta = new Respuesta();
            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    var qUpdatePedido = from p in dbExactus.MF_Pedido where p.PEDIDO == Pedido select p;
                    foreach (var item in qUpdatePedido)
                    {
                        item.FECHA_IMPRESION_PAGARE = DateTime.Now;
                    }

                    dbExactus.SaveChanges();
                    transactionScope.Complete();
                }
            }
            catch (Exception e)
            {
                return new Respuesta(false, "" + e.StackTrace);
            }
            return mRespuesta;
        }
        public Respuesta GrabarXML(SolicitudInterconsumo SolicitudInterconsumo)
        {
            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    var qUpdateMFPedido = from p in dbExactus.MF_Pedido where p.PEDIDO == SolicitudInterconsumo.NUMERO_DOCUMENTO select p;
                    foreach (var item in qUpdateMFPedido)
                    {
                        item.XML_CONSULTA = SolicitudInterconsumo.XML_CONSULTA;
                        item.XML_CONSULTA_RESPUESTA = SolicitudInterconsumo.XML_CONSULTA_RESPUESTA;
                    }

                    dbExactus.SaveChanges();
                    transactionScope.Complete();
                }
            }
            catch (Exception e)
            {
                return new Respuesta(false, "" + e.StackTrace);
            }
            return new Respuesta(true, "");
        }
    }
}