﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Models;
using FiestaNETRestServices.Utils;
using MF.Comun.Dto.Facturacion;
using MF_Clases.Comun;
using Newtonsoft.Json;
using RestSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;
using WebGrease.Css.Extensions;

namespace FiestaNETRestServices.DAL
{
    public class DALPromociones : MFDAL
    {
        private decimal DescuentoArticulo(PedidoLinea item, decimal desctoPromocion)
        {
            return 0;
        }
        public PromocionRegistro PromocionesTienda(PromocionRegistro articulosOriginales)
        {
            PromocionRegistro articulosConPromocion = new PromocionRegistro();
            List<PedidoLinea> articulosPromocionPedido = new List<PedidoLinea>();
            var qNivel = dbExactus.MF_NIVEL_PRECIO_COEFICIENTE.Where(c => c.NIVEL_PRECIO == articulosOriginales.nivelPrecio).FirstOrDefault();//(from n in db.MF_NIVEL_PRECIO_COEFICIENTEs where n.NIVEL_PRECIO == nivel select n).First();
            string json = JsonConvert.SerializeObject(articulosOriginales);
            log.Info("PromocionRegistro oferta: " + json);

            int mFinanciera = (int)qNivel.FINANCIERA;
            var qFinanciera = (from f in dbExactus.MF_Financiera where f.Financiera == mFinanciera select f).First();
            decimal factor = (decimal)qNivel.FACTOR;
            decimal mIVA = Utilitarios.ObtenerIva();
            bool mHay = false;

            List<PedidoLinea> plPromo = new List<PedidoLinea>();
            try
            {
                if (articulosOriginales.notas.Length == 0)
                {

                    var qArticulosPromocion = dbExactus.MF_TipoVenta.Join(dbExactus.MF_TipoVentaCondicionArticulo,
                                            tp => tp.TipoVenta,
                                            tpca => tpca.TipoVenta,
                                            (tp, tpca) => new { tp, tpca }).
                                            Where(x => x.tp.TipoVenta == articulosOriginales.TipoVenta
                                           );

                    if (qArticulosPromocion.Count() > 0)
                    {
                        //por condición de artículos
                        var qArticuloClasificacion = (
                            from c in dbExactus.MF_Clasificacion
                            join cl in dbExactus.MF_ClasificacionDetalle on c.MF_CLASIFICACION1 equals cl.MF_CLASIFICACION
                            join ar in dbExactus.ARTICULO on cl.CLASIFICACION equals ar.CLASIFICACION_3
                            where
                            ar.ACTIVO == "S"
                            select new ArticuloClasificacion { Articulo = ar.ARTICULO1, Clasificacion = SqlFunctions.StringConvert((decimal)cl.MF_CLASIFICACION) }
                            );
                        if (dbExactus.MF_TipoVentaCondicionArticulo.Where(y => y.TipoVenta == articulosOriginales.TipoVenta).Any(y => y.ArticuloRequerido == ""))
                            mHay = true;
                        else
                            articulosOriginales.articulos.ForEach(x =>
                            {

                                if (dbExactus.MF_TipoVentaCondicionArticulo.Where(y => y.TipoVenta == articulosOriginales.TipoVenta)
                                .Any(y1 => y1.ArticuloRequerido == x.Articulo))
                                    mHay = true;
                            });

                        if (mHay)
                        {
                            var q1 = (from tp in dbExactus.MF_TipoVentaCondicionArticulo
                                      join tv in dbExactus.MF_TipoVenta on tp.TipoVenta equals tv.TipoVenta
                                      where tp.TipoVenta == articulosOriginales.TipoVenta
                                      from ar in qArticuloClasificacion
                                      where

                                     ((tp.Articulo == ar.Articulo || (tp.Articulo == "*" && tp.Articulo != ar.Articulo))
                                      && (ar.Clasificacion.Trim() == tp.Clasificacion || (tp.Clasificacion == "*" && ar.Clasificacion != tp.Clasificacion)))
                                    && ((bool)tp.Condicion_Ofertado && (dbExactus.MF_ArticuloPrecioOferta.Any(x => x.Articulo.Equals(ar.Articulo) && x.Estatus.Equals("V") || dbExactus.MF_ArticuloNivelPrecioOferta.Any(y => y.Articulo.Equals(ar.Articulo)
                                        && y.Estatus.Equals("V") && y.NivelPrecio.Equals("ContadoEfect") && y.pctjDescuento > 0)))
                                        || ((bool)tp.Condicion_PrecioRegular && !dbExactus.MF_ArticuloPrecioOferta.Any(x => x.Articulo.Equals(ar.Articulo) && x.Estatus.Equals("V"))
                                        && !dbExactus.MF_ArticuloNivelPrecioOferta.Any(y => y.Articulo.Equals(ar.Articulo) && y.Estatus.Equals("V") && y.NivelPrecio.Equals("ContadoEfect") && y.pctjDescuento > 0))
                                        )
                                        //validación por cliente requerido, si no existe en la tabla la promoción, entonces no hay validación por cliente
                                        && (dbExactus.MF_TipoVentaCondicionCliente.Any(c => c.TipoVenta.Equals(articulosOriginales.TipoVenta)
                                               && c.Cliente.Equals(articulosOriginales.cliente)) || !dbExactus.MF_TipoVentaCondicionCliente.Any(c => c.TipoVenta.Equals(articulosOriginales.TipoVenta))
                                            )
                                      orderby tp.Articulo, tp.Precedencia ascending
                                      select new { ARTICULO = ar.Articulo, porDescuento = tp.Porcentaje_Descuento }).ToList();


                            List<string> articulo = new List<string>();

                            q1.ForEach(ar => articulo.Add(ar.ARTICULO));


                            articulosOriginales.articulos.ForEach(ar =>
                            {
                                if (articulo.Contains(ar.Articulo))
                                {
                                    ar.PorcentajeDescuento = (decimal)q1.Where(x => x.ARTICULO == ar.Articulo).FirstOrDefault().porDescuento;
                                    plPromo.Add(ar);
                                }
                            });
                            plPromo.ForEach(ar =>
                            {
                                articulosPromocionPedido.Add(ar);//agregándolo a la lista de articulos con promoción
                                articulosOriginales.articulos.Remove(ar);//
                            });
                        }
                        articulosOriginales.notas = qArticulosPromocion.FirstOrDefault().tp.Descripcion;
                    }
                    else
                    {
                        //por financiera
                        var qTipoVEnta = dbExactus.MF_TipoVenta.Where(x => x.TipoVenta == articulosOriginales.TipoVenta
                         && x.Financiera == articulosOriginales.Financiera
                         && (x.NivelPrecio == articulosOriginales.nivelPrecio || x.NivelPrecio == null)
                        //validación por cliente requerido, si no existe en la tabla la promoción, entonces no hay validación por cliente
                        && (dbExactus.MF_TipoVentaCondicionCliente.Any(c => c.TipoVenta.Equals(articulosOriginales.TipoVenta)
                               && c.Cliente.Equals(articulosOriginales.cliente)) || !dbExactus.MF_TipoVentaCondicionCliente.Any(c => c.TipoVenta.Equals(articulosOriginales.TipoVenta))
                            ));
                        if (qTipoVEnta.Count() > 0)
                        {
                            articulosOriginales.articulos.ForEach(y =>
                            {
                                y.PorcentajeDescuento = y.PorcentajeDescuento > 100 ? Math.Round(y.PorcentajeDescuento / y.PrecioFacturar, 2) * 100 : y.PorcentajeDescuento;

                                y.PorcentajeDescuento = (decimal)((qTipoVEnta.FirstOrDefault().DescuentoNormal > 0 && qTipoVEnta.FirstOrDefault().DescuentoOfertado > 0) ?
                                                            y.PorcentajeDescuento + qTipoVEnta.FirstOrDefault().DescuentoOfertado :
                                                            qTipoVEnta.FirstOrDefault().DescuentoNormal);
                                articulosPromocionPedido.Add(y);
                            }
                            );
                            articulosPromocionPedido.ForEach(promo =>
                                articulosOriginales.articulos.RemoveAll(x => x.Articulo == promo.Articulo)
                             );

                            articulosOriginales.notas = qTipoVEnta.FirstOrDefault().Descripcion;
                        }
                        else
                            articulosOriginales.mensaje = "El nivel de precio o el cliente no aplica en esta promoción";
                    }
                    if (articulosPromocionPedido.Count() == 0)
                    {
                        if (articulosOriginales.mensaje == string.Empty)
                            articulosOriginales.mensaje = "No hay artículos en esta promoción";
                    }
                    else
                    {
                        articulosPromocionPedido.ForEach(a =>
                        {

                            decimal PrecioBase = 0;
                            try
                            {
                                var qPrecio = dbExactus.MF_ArticuloPrecioOferta.Where(x => x.Estatus == "V" && x.Articulo == a.Articulo).FirstOrDefault();
                                decimal? qPrecioOriginal = qPrecio != null ? qPrecio.PrecioOriginal : 0;
                                string clasificacion = dbExactus.ARTICULO.Where(x => x.ARTICULO1 == a.Articulo).FirstOrDefault().CLASIFICACION_3;

                                if (qPrecioOriginal != null)
                                    PrecioBase = qPrecioOriginal.GetValueOrDefault();

                                if (PrecioBase == 0 && (a.Articulo.Substring(0, 3) == "149") || (a.Articulo.Substring(0, 3) == "141") || (a.Articulo.Substring(0, 3) == "142"))
                                {
                                    mHay = true;
                                    decimal PrecioTotal = (decimal)a.PrecioBaseLocal * mIVA * a.CantidadPedida;
                                    a.Descuento = Math.Round(((PrecioTotal) * (a.PorcentajeDescuento / 100)), 4, MidpointRounding.AwayFromZero);
                                    a.PrecioTotal = Math.Round((PrecioTotal) - (PrecioTotal * a.PorcentajeDescuento / 100), 2, MidpointRounding.AwayFromZero);
                                    a.PorcentajeDescuento = a.PorcentajeDescuento;
                                    a.NetoFacturar = a.PrecioTotal;
                                }
                                else
                                if (PrecioBase == 0)
                                {
                                    mHay = true;
                                    a.Descuento = Math.Round(((a.PrecioTotal / factor) * (a.PorcentajeDescuento / 100)), 4, MidpointRounding.AwayFromZero);
                                    a.PrecioTotal = Math.Round((a.PrecioTotal / factor) - (a.PrecioTotal / factor) * (a.PorcentajeDescuento / 100), 2, MidpointRounding.AwayFromZero);
                                    a.PorcentajeDescuento = a.PorcentajeDescuento;
                                    a.NetoFacturar = a.PrecioTotal;

                                }
                                else
                                {
                                    mHay = true;
                                    a.PrecioTotal = Math.Round(PrecioBase * a.CantidadPedida * mIVA - (decimal)(PrecioBase * a.CantidadPedida * (a.PorcentajeDescuento / 100) * mIVA), 2, MidpointRounding.AwayFromZero);
                                    a.Descuento = Math.Round(((decimal)(PrecioBase * a.CantidadPedida * (a.PorcentajeDescuento / 100))) * mIVA, 4, MidpointRounding.AwayFromZero);
                                    a.PorcentajeDescuento = a.PorcentajeDescuento;

                                }
                            }
                            catch
                            {
                                mHay = true;

                                a.Descuento = Math.Round(((a.PrecioTotal / factor) * (a.PorcentajeDescuento / 100)), 2, MidpointRounding.AwayFromZero);
                                a.PrecioTotal = Math.Round((a.PrecioTotal / factor) - (a.PrecioTotal / factor) * (a.PorcentajeDescuento / 100), 2, MidpointRounding.AwayFromZero);
                                a.PorcentajeDescuento = a.PorcentajeDescuento;
                                a.NetoFacturar = a.PrecioTotal;

                            }

                        });

                        articulosOriginales.articulos.AddRange(articulosPromocionPedido);

                        decimal mPrecioTotal = 0;
                        decimal mMonto = 0;
                        decimal mDescuento = 0;
                        articulosOriginales.articulos.ForEach(ar =>
                        {
                            if (qFinanciera.ES_BANCO.Equals("S"))
                            {
                                decimal mMontoTotal = Math.Round(Convert.ToDecimal(ar.PrecioTotal) * Convert.ToDecimal(qNivel.FACTOR), 2, MidpointRounding.AwayFromZero);

                                ar.PrecioTotal = mMontoTotal;
                                mPrecioTotal += ar.PrecioFacturar;
                                mDescuento += ar.Descuento;
                                mMonto += mMontoTotal;
                            }
                            else
                            {
                                mPrecioTotal += ar.PrecioFacturar;
                                mDescuento += ar.Descuento;
                                mMonto += ar.PrecioTotal;
                            }
                            ar.NetoFacturar = ar.PrecioTotal;
                        });
                        articulosOriginales.Monto = mMonto;
                        articulosOriginales.TotalFacturar = mPrecioTotal;
                        articulosOriginales.Descuento = mDescuento;

                    }


                    if (!mHay)
                    {
                        if (articulosOriginales.mensaje == string.Empty)
                            articulosOriginales.mensaje = "No se encontró ningún artíuclo para aplicar en la promoción.";
                    }


                }
                else
                {
                    articulosOriginales.mensaje = "La promoción ya fué aplicada";
                }

            }
            catch (Exception ex)
            {
                articulosOriginales.mensaje = CatchClass.ExMessage(ex, "DALPromociones", "PromocionesTienda");
            }
            return articulosOriginales;
        }


    }
}
