﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Models;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web;
using static MF_Clases.Utils.CONSTANTES;

namespace FiestaNETRestServices.DAL
{
    public class DALRecibo : MFDAL
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        EXACTUSERPEntities db = new EXACTUSERPEntities();

        public Respuesta AsociarRecibo(string pFactura, string pREcibo, decimal pMonto,string usuario)
        {
            /*
             * validar en front end que no puede mezclar recibos con monto de tarjeta con efectivo|cheque
             * 
             */

            string pCuenta = "";
            Respuesta respuesta = new Respuesta();
            decimal mTasa = 1;
            string mMoneda = "GTQ";
            decimal mEfectivoDolares = 0;
            decimal mEfectivo = pMonto;
            string asitento_contable = string.Empty;
            List<string> mCuentaCorreo = new List<string>();
            var mRemitente = db.MF_Catalogo.Where(s => s.CODIGO_TABLA.Equals(Const.CATALOGO.REMITENTE_CORREOS_PDV)).FirstOrDefault().NOMBRE_CAT;
            //destinatatios IT
            var qCatalogo = db.MF_Catalogo.Where(x => x.CODIGO_TABLA.Equals(Const.CATALOGO.DESTINATARIOS_IT_ERRORES)).ToList();
            if (qCatalogo.Count() > 0)
            {
                foreach (var item in qCatalogo)
                {
                    mCuentaCorreo.Add(item.NOMBRE_CAT);
                }

            }
            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    log.Debug(string.Format("INICIANDO PROCESO PARA ASOCIAR EL RECIBO {0} A LA FACTURA {1} POR EL MONTO {2}", pREcibo, pFactura, pMonto));
                    //1) obtener la infor del recibo a asociar

                    var qRecibo = db.MF_Recibo.Where(r => r.DOCUMENTO.Equals(pREcibo)).FirstOrDefault();
                    if (pMonto <= qRecibo.MONTO)
                    {
                        // 2) MF_Recibo
                        if (qRecibo.OBSERVACIONES.ToString().Length == 0)
                            qRecibo.OBSERVACIONES = string.Format("Pago factura {0}", pFactura);


                        if (qRecibo.FACTURA.Equals(string.Empty))
                            qRecibo.FACTURA = pFactura;

                        var nFacturaRecibo = db.MF_FacturaRecibo.Where(x => x.FACTURA.Equals(pFactura) && x.RECIBO.Equals(pREcibo)).Count();
                        //MF_FacturaRecibo
                        if (nFacturaRecibo == 0)
                        {
                            MF_FacturaRecibo recibo = new MF_FacturaRecibo
                            {
                                FACTURA = pFactura,
                                RECIBO = pREcibo,
                                CreateDate = DateTime.Now,
                                CreatedBy = usuario,
                                RecordDate = DateTime.Now
                            };
                            db.MF_FacturaRecibo.AddObject(recibo);
                        }
                        #region "Saldos"
                        decimal mTipoCambio = (from tc in db.TIPO_CAMBIO_HIST where tc.TIPO_CAMBIO == "REFR" orderby tc.FECHA descending select tc).Take(1).First().MONTO;
                        decimal mMontoDolar = Math.Round((pMonto / mTipoCambio), 2, MidpointRounding.AwayFromZero);

                        Utils.Utilitarios utilitarios = new Utils.Utilitarios();
                        decimal mIVA = Utils.Utilitarios.ObtenerIva();

                        int mAuxCC = (from a in db.AUXILIAR_CC where a.TIPO_CREDITO == "REC" && a.TIPO_DEBITO == "FAC" && a.CREDITO == pREcibo && a.DEBITO == pFactura select a).Count();

                        if (mAuxCC == 0)
                        {
                            AUXILIAR_CC iAuxiliar_CC = new AUXILIAR_CC
                            {
                                TIPO_CREDITO = "REC",
                                TIPO_DEBITO = "FAC",
                                FECHA = DateTime.Now.Date,
                                CREDITO = pREcibo,
                                DEBITO = pFactura,
                                MONTO_DEBITO = pMonto,
                                MONTO_CREDITO = pMonto,
                                MONTO_LOCAL = pMonto,
                                MONTO_DOLAR = mMontoDolar,
                                MONTO_CLI_CREDITO = pMonto,
                                MONTO_CLI_DEBITO = pMonto,
                                MONEDA_CREDITO = "GTQ",
                                MONEDA_DEBITO = "GTQ",
                                CLI_REPORTE_CREDIT = qRecibo.CLIENTE,
                                CLI_REPORTE_DEBITO = qRecibo.CLIENTE,
                                CLI_DOC_CREDIT = qRecibo.CLIENTE,
                                CLI_DOC_DEBITO = qRecibo.CLIENTE,
                                RecordDate = DateTime.Now,
                                RowPointer = Guid.NewGuid(),
                                CreatedBy = string.Format("FA/{0}", usuario),
                                UpdatedBy = string.Format("FA/{0}", usuario),
                                CreateDate = DateTime.Now
                            };

                            db.AUXILIAR_CC.AddObject(iAuxiliar_CC);
                        }
                        //ACTUALIZANDO SALDO DE RECIBOS
                        var qReciboCC = from c in db.DOCUMENTOS_CC where c.DOCUMENTO == pREcibo && c.TIPO == "REC" select c;

                        //solo para recibos existentes
                        foreach (var recCC in qReciboCC)
                        {
                            decimal mNuevoSaldo = recCC.SALDO - pMonto;

                            recCC.SALDO = recCC.SALDO >= 0 ? mNuevoSaldo : recCC.SALDO;
                            recCC.SALDO_CLIENTE = recCC.SALDO_CLIENTE >= 0 ? recCC.SALDO_CLIENTE - pMonto : recCC.SALDO_CLIENTE;
                            recCC.SALDO_DOLAR = recCC.SALDO_DOLAR >= 0 ? recCC.SALDO_DOLAR - mMontoDolar : recCC.SALDO_DOLAR;
                            recCC.SALDO_LOCAL = recCC.SALDO_LOCAL >= 0 ? recCC.SALDO_LOCAL - mMontoDolar : recCC.SALDO_LOCAL;

                            Enviar_notificacionApliacionRecibo(mRemitente, mCuentaCorreo, pREcibo, pFactura, mNuevoSaldo, usuario);
                        }

                        //ACTUALIZANDO SALDO DE FACTURAS
                        var qFacturaCC = (from fc in db.DOCUMENTOS_CC where fc.DOCUMENTO == pFactura && fc.TIPO == "FAC" select fc);
                        foreach (var itemF in qFacturaCC)
                        {
                            itemF.SALDO = itemF.SALDO - pMonto;
                            itemF.RecordDate = DateTime.Now;
                            itemF.UpdatedBy = usuario;
                        }
                        #endregion

                        db.SaveChanges();
                        transactionScope.Complete();
                        respuesta.Exito = true;
                    }
                }
                    
            }
            catch (Exception EX)
            {
                respuesta.Exito = false;
                respuesta.Mensaje = CatchClass.ExMessage(EX, "DALRecibo", "AsociarRecibo");
            }
            return respuesta;
        }

        public Respuesta DesasociarRecibo(string pFactura, string pREcibo, decimal pMonto, string usuario)
        {
           
            string pCuenta = "";
            Respuesta respuesta = new Respuesta();
            decimal mTasa = 1;
            string mMoneda = "GTQ";
            decimal mEfectivoDolares = 0;
            decimal mEfectivo = pMonto;
            string asitento_contable = string.Empty;
            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    log.Debug(string.Format("INICIANDO PROCESO PARA DESASOCIAR EL RECIBO {0} DE LA FACTURA {1} POR EL MONTO {2}", pREcibo, pFactura, pMonto));
                    //1) obtener la infor del recibo a asociar

                    var qRecibo = db.MF_Recibo.Where(r => r.DOCUMENTO.Equals(pREcibo)).FirstOrDefault();

                    //MF_Recibo
                    if (qRecibo.OBSERVACIONES.ToString().Length > 0)
                        qRecibo.OBSERVACIONES =string.Empty;


                    if ( qRecibo.FACTURA.Equals(pFactura))
                        qRecibo.FACTURA = string.Empty;


                    //MF_FacturaRecibo
                    var qFacturaRecibo = db.MF_FacturaRecibo.Where(x => x.FACTURA.Equals(pFactura) && x.RECIBO.Equals(pREcibo));
                    foreach (var item in qFacturaRecibo)
                        db.MF_FacturaRecibo.DeleteObject(item);


                    #region "Saldos"
                    decimal mTipoCambio = (from tc in db.TIPO_CAMBIO_HIST where tc.TIPO_CAMBIO == "REFR" orderby tc.FECHA descending select tc).Take(1).First().MONTO;
                    decimal mMontoDolar = Math.Round((pMonto / mTipoCambio), 2, MidpointRounding.AwayFromZero);

                    Utils.Utilitarios utilitarios = new Utils.Utilitarios();
                    decimal mIVA = Utils.Utilitarios.ObtenerIva();

                     var qAuxCC = (from a in db.AUXILIAR_CC where a.TIPO_CREDITO == "REC" && a.TIPO_DEBITO == "FAC" && a.CREDITO == pREcibo && a.DEBITO == pFactura select a);

                    if (qAuxCC.Count()>0)
                    {
                        foreach (var item in qAuxCC)
                        {
                            log.Info(string.Format("\n\r*-*-*DESAPLICANDO RECIBO {0} DE LA FACTURA {1} CON UN MONTO DE {2} usuario {3}*--*-*-*\n\r", pREcibo, pFactura, pMonto,usuario));
                            db.AUXILIAR_CC.DeleteObject(item);
                        }

                        var qReciboCC = from c in db.DOCUMENTOS_CC where c.DOCUMENTO == pREcibo && c.TIPO == "REC" select c;
                        foreach (var recCC in qReciboCC)
                        {
                            decimal mNuevoSaldo = recCC.SALDO + pMonto;

                            recCC.SALDO = mNuevoSaldo;
                            recCC.SALDO_CLIENTE = recCC.SALDO_CLIENTE + pMonto;
                            recCC.SALDO_DOLAR = recCC.SALDO_DOLAR + mMontoDolar;
                            recCC.SALDO_LOCAL = recCC.SALDO_LOCAL + mMontoDolar;
                            recCC.UpdatedBy = string.Format("FA/{0}", usuario);
                            recCC.RecordDate = DateTime.Now;
                        }
                    }
                    #endregion


                    db.SaveChanges();
                    transactionScope.Complete();
                    respuesta.Exito = true;
                }

            }
            catch (Exception EX)
            {
                respuesta.Exito = false;
                respuesta.Mensaje = CatchClass.ExMessage(EX, "DALRecibo", "AsociarRecibo");
            }
            return respuesta;
        }

        private static async void Enviar_notificacionApliacionRecibo(string pRemitente,List<string> mCuentaCorreo,string pRecibo, string pFactura, decimal nNuevoSaldo, string Usuario)
        {
            await System.Threading.Tasks.Task.Run(() =>
            {
                Utils.Utilitarios utilitarios = new Utils.Utilitarios();
                string mSubject = string.Format("Recibo {0} con " + (nNuevoSaldo < 0 ? "saldo menor que cero en grabación de factura {1}, usuario {3}" : " saldo de {2} aplicado desde el PDV, usuario: {3}"), pRecibo, pFactura, nNuevoSaldo, Usuario);

                StringBuilder mBody = new StringBuilder();
                mBody.Append(string.Format("El recibo {0} se le aplicó la factura {1}, quedándo con saldo {2}", pRecibo, pFactura, nNuevoSaldo));
                try
                {
                    if (nNuevoSaldo < 0)
                        log.Error(string.Format("El recibo {0} resultó con saldo menor que cero la grabación de la factura {1}.", pRecibo, pFactura));
                    else
                        log.Info(string.Format("El recibo {0} se le aplicó la factura {1}, quedándo con saldo {2}", pRecibo, pFactura, nNuevoSaldo));
                   
                     //remitente de correos
                     
                    utilitarios.EnviarEmail(pRemitente, mCuentaCorreo, mSubject, mBody, "PUNTO DE VENTA -NOTIFICACIONES-");

                }
                catch (Exception ex)
                {
                    log.Error(string.Format("Error al enviar la alerta del recibo {0} con saldo {2}, mensaje de error {1}", pRecibo, ex.Message,nNuevoSaldo));
                }
            });
        }

        public List<Clases.DocumentoConSaldo> obtenerRecibosconSaldo(string Recibo)
        {
            List<Clases.DocumentoConSaldo> lstRecibos = new List<Clases.DocumentoConSaldo>();
            var qCliente = db.CLIENTE.Join(db.MF_Recibo, c=>c.CLIENTE1,R => R.CLIENTE, (c,R)=> new {c,R }).Where(c => c.R.DOCUMENTO == Recibo).FirstOrDefault();
            string NombreCliente = qCliente.c.NOMBRE;
            string CodCliente = qCliente.c.CLIENTE1;

            try
            {
                if (qCliente != null)
                {

                    var qRecibos = (from r in db.MF_Recibo
                                    join rf in db.MF_FacturaRecibo on r.DOCUMENTO equals rf.RECIBO
                                    join fac in db.FACTURA on rf.FACTURA equals fac.FACTURA1
                                    where
                                    r.ANULADO == "N"
                                    && fac.ANULADA =="N"
                                    && r.DOCUMENTO == Recibo
                                    select new
                                    {
                                        DOCUMENTO = rf.FACTURA,
                                        SALDO = fac.TOTAL_FACTURA-((from fr in db.MF_FacturaRecibo join re in db.MF_Recibo on fr.RECIBO 
                                                                   equals re.DOCUMENTO  where fr.FACTURA == rf.FACTURA 
                                                                   && re.ANULADO.Equals("N") select new { total_recibo = re.MONTO }).Sum(x => x.total_recibo)+
                                                                   (from c in db.DOCUMENTOS_CC where c.CLIENTE == r.CLIENTE 
                                                                    && c.FECHA >= fac.FECHA 
                                                                    && db.MF_Recibo.Any(r => r.DOCUMENTO == c.DOCUMENTO)== false
                                                                    && c.ANULADO=="N" && c.TIPO.Equals("REC") select c).Sum(x =>x.MONTO))
                                    });
                    foreach (var itemR in qRecibos)
                    {
                        lstRecibos.Add(
                            new Clases.DocumentoConSaldo
                            {
                                 Documento=itemR.DOCUMENTO,
                                 Saldo= itemR.SALDO>0?itemR.SALDO :0
                            }
                            );
                    }
                }
                
            } catch (Exception ex)
            {
                log.Error("No se pudo obtener  saldo del cliente "+ NombreCliente+", error: " + ex.Message);
            }
            return lstRecibos;
        }
        public List<Clases.DocumentoConSaldo> obtenerFacturasSinPago(string cliente)
        {
            List<Clases.DocumentoConSaldo> lstRecibos = new List<Clases.DocumentoConSaldo>();
            var qCliente = db.CLIENTE.Where(c => c.CLIENTE1.Equals(cliente)).FirstOrDefault();
            string NombreCliente = qCliente.NOMBRE;

            try
            {
                if (qCliente != null)
                {
                    //var qRecibos = (from r in db.DOCUMENTOS_CC 
                    //                where r.TIPO.Equals("REC") && r.CLIENTE.Equals(cliente) && r.ANULADO=="N" && r.SALDO>0    && (EntityFunctions.DiffDays(EntityFunctions.AddDays(DateTime.Today, -15), r.FECHA) >= 0)
                    //                select r);
                    var qRecibos = (from r in db.MF_Recibo
                                    join rec in db.DOCUMENTOS_CC on r.CLIENTE equals rec.CLIENTE
                                    where r.CLIENTE.Equals(cliente) && rec.DOCUMENTO == r.DOCUMENTO && r.ANULADO == "N" && rec.SALDO > 0 && rec.TIPO.Equals("REC") && (r.FACTURA.Equals("")
                                     || (r.FACTURA.Equals("") == false && (from fr in db.MF_FacturaRecibo join fa in db.FACTURA on fr.FACTURA equals fa.FACTURA1 where fr.RECIBO.Equals(r.DOCUMENTO) select fa).Sum(x => x.TOTAL_FACTURA) < r.MONTO))
                                    select new
                                    {
                                        DOCUMENTO = r.DOCUMENTO,
                                        SALDO = r.FACTURA.Equals("") ? r.MONTO : r.MONTO - (from fr in db.MF_FacturaRecibo join fa in db.FACTURA on fr.FACTURA equals fa.FACTURA1 where fr.RECIBO.Equals(r.DOCUMENTO) && fa.ANULADA.Equals("N") select fa).Sum(x => x.TOTAL_FACTURA)
                                    });
                    foreach (var itemR in qRecibos)
                    {
                        lstRecibos.Add(
                            new Clases.DocumentoConSaldo
                            {
                                Documento = itemR.DOCUMENTO,
                                Saldo = itemR.SALDO
                            }
                            );
                    }
                }

            }
            catch (Exception ex)
            {
                log.Error("No se pudo obtener los recibos con saldo del cliente " + NombreCliente + ", error: " + ex.Message);
            }
            return lstRecibos;
        }
    }
}