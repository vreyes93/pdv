﻿using FiestaNETRestServices.Models;
using MF.Comun.Dto;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;

namespace FiestaNETRestServices.DAL
{
    public class DALSolicitudCredito
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        EXACTUSERPEntities Exactus = new EXACTUSERPEntities();

        public Respuesta ObtenerSolicitudCredito(string pedido)
        {
           var respuesta  = new Respuesta();
            IdentidadCliente identidadCliente=null;
            try
            {
                log.Info(string.Format("Obteniendo la información del pedido"));
                var qCliente = from pe in Exactus.MF_Pedido
                               join c in Exactus.MF_Cliente on pe.CLIENTE equals c.CLIENTE
                               join f in Exactus.MF_Financiera on pe.FINANCIERA equals f.Financiera
                               where pe.PEDIDO == pedido

                               select new
                               {
                                   NombreCompleto = c.PRIMER_NOMBRE + " " + c.SEGUNDO_NOMBRE + " " + c.PRIMER_APELLIDO + " " + c.SEGUNDO_APELLIDO,
                                   Cliente = c.CLIENTE,
                                   Financiera=f.Nombre,
                                   NivelPrecio=pe.NIVEL_PRECIO,
                                   MontoAFacturar=pe.TOTAL_FACTURAR,
                                   Fecha=pe.FECHA_REGISTRO,
                                   Enganche=pe.ENGANCHE,
                                   SaldoFinanciar=pe.SALDO_FINANCIAR,
                                   Npagos1=pe.CANTIDAD_PAGOS1,
                                   Npagos2=pe.CANTIDAD_PAGOS2,
                                   MontoPagos1=pe.MONTO_PAGOS1,
                                   MontoPagos2=pe.MONTO_PAGOS2,
                                   Observacioines=pe.OBSERVACIONES_REFERENCIA
                                   
                               };
                if (qCliente.Count() > 0)
                {
                    identidadCliente = new IdentidadCliente
                    {
                        Cliente = qCliente.First().Cliente,
                        NombreCompleto = qCliente.First().NombreCompleto,
                        preSolicitud = new PreSolicitud
                        {


                            NombreFinanciera = qCliente.First().Financiera,
                            NivelPrecio = qCliente.First().NivelPrecio,
                            MontoTotal = qCliente.First().MontoAFacturar,
                            FechaSolicitud = qCliente.First().Fecha,
                            Enganche = qCliente.First().Enganche,
                            SaldoFinanciar = qCliente.First().SaldoFinanciar,
                            CantPagos1 = qCliente.First().Npagos1,
                            CantPagos2 = qCliente.First().Npagos2,
                            MontoPagos1 = qCliente.First().MontoPagos1,
                            MontoPagos2 = qCliente.First().MontoPagos2,
                            ObservacionesCredito = qCliente.First().Observacioines
                        }
                    };
                }
                else
                {
                    int nCotizacion = int.Parse(pedido);//castear a int la cotización
                    var qCotiCli = from pe in Exactus.MF_Cotizacion
                                   join c in Exactus.MF_Cliente on pe.CLIENTE equals c.CLIENTE
                                   join f in Exactus.MF_Financiera on pe.FINANCIERA equals f.Financiera
                                where pe.COTIZACION == nCotizacion

                                select new
                                {
                                    NombreCompleto = c.PRIMER_NOMBRE + " " + c.SEGUNDO_NOMBRE + " " + c.PRIMER_APELLIDO + " " + c.SEGUNDO_APELLIDO,
                                    Cliente = c.CLIENTE,
                                    Financiera = f.Nombre,
                                    NivelPrecio = pe.NIVEL_PRECIO,
                                    MontoAFacturar = pe.TOTAL_FACTURAR,
                                    Fecha = pe.FECHA_INICIAL,
                                    Enganche = pe.ENGANCHE,
                                    SaldoFinanciar = pe.SALDO_FINANCIAR,
                                    Npagos1 = pe.CANTIDAD_PAGOS1,
                                    Npagos2 = pe.CANTIDAD_PAGOS2,
                                    MontoPagos1 = pe.MONTO_PAGOS1,
                                    MontoPagos2 = pe.MONTO_PAGOS2,
                                    Observacioines = pe.OBSERVACIONES_SOLICITUD

                                };
                    if (identidadCliente == null && qCotiCli.Count() > 0)
                    {
                        identidadCliente = new IdentidadCliente
                        {
                            Cliente = qCotiCli.First().Cliente,
                            NombreCompleto = qCotiCli.First().NombreCompleto,
                            preSolicitud = new PreSolicitud
                            {


                                NombreFinanciera = qCotiCli.First().Financiera,
                                NivelPrecio = qCotiCli.First().NivelPrecio,
                                MontoTotal = qCotiCli.First().MontoAFacturar,
                                FechaSolicitud = qCotiCli.First().Fecha,
                                Enganche = qCotiCli.First().Enganche,
                                SaldoFinanciar = qCotiCli.First().SaldoFinanciar,
                                CantPagos1 = qCotiCli.First().Npagos1,
                                CantPagos2 = qCotiCli.First().Npagos2,
                                MontoPagos1 = qCotiCli.First().MontoPagos1,
                                MontoPagos2 = qCotiCli.First().MontoPagos2,
                                ObservacionesCredito = qCotiCli.First().Observacioines
                            }
                        };
                    }
                    else if (qCotiCli.Count() == 0)
                    {
                        var qCoti = from pe in Exactus.MF_Cotizacion
                                    join f in Exactus.MF_Financiera on pe.FINANCIERA equals f.Financiera
                                    where pe.COTIZACION == nCotizacion

                                    select new
                                    {
                                        NombreCompleto = pe.NOMBRE,
                                        Cliente = pe.CLIENTE,
                                        Financiera = f.Nombre,
                                        NivelPrecio = pe.NIVEL_PRECIO,
                                        MontoAFacturar = pe.MONTO,
                                        Fecha = pe.FECHA_INICIAL,
                                        Enganche = pe.ENGANCHE,
                                        SaldoFinanciar = pe.SALDO_FINANCIAR,
                                        Npagos1 = pe.CANTIDAD_PAGOS1,
                                        Npagos2 = pe.CANTIDAD_PAGOS2,
                                        MontoPagos1 = pe.MONTO_PAGOS1,
                                        MontoPagos2 = pe.MONTO_PAGOS2,
                                        Observacioines = pe.OBSERVACIONES_SOLICITUD

                                    };

                        if (identidadCliente == null && qCoti.Count() > 0)
                        {
                            identidadCliente = new IdentidadCliente
                            {
                                Cliente = qCoti.First().Cliente,
                                NombreCompleto = qCoti.First().NombreCompleto,
                                preSolicitud = new PreSolicitud
                                {


                                    NombreFinanciera = qCoti.First().Financiera,
                                    NivelPrecio = qCoti.First().NivelPrecio,
                                    MontoTotal = qCoti.First().MontoAFacturar,
                                    FechaSolicitud = qCoti.First().Fecha,
                                    Enganche = qCoti.First().Enganche,
                                    SaldoFinanciar = qCoti.First().SaldoFinanciar,
                                    CantPagos1 = qCoti.First().Npagos1,
                                    CantPagos2 = qCoti.First().Npagos2,
                                    MontoPagos1 = qCoti.First().MontoPagos1,
                                    MontoPagos2 = qCoti.First().MontoPagos2,
                                    ObservacionesCredito = qCoti.First().Observacioines
                                }
                            };
                        }
                    }
                   
                }
                
                respuesta.Exito = true;
                respuesta.Objeto = identidadCliente;
            }
            catch (Exception ex)
            {
                respuesta.Exito = false;
                respuesta.Mensaje = string.Format("Problema al obtener la solicitud de crédito, pedido|cotización: {0}, Problema {1}", pedido, ex.Message);
                log.Error(respuesta.Mensaje);
            }
            return respuesta;
        }

        public Respuesta GrabarRespuestaSolicitudCredito(RespuestaSolicitud resp,string pedido)
        {
            //  Cálculo del monto y los recargos
            //  Se actualizarán con lo que viene de resultado de la solicitud de CREDIPLUS para evitar
            //  diferencias con el redondeo de los decimales

            //SELECT (monto_pagos1*CANTIDAD_PAGOS1)+(CANTIDAD_PAGOS2*MONTO_PAGOS2)-SALDO_FINANCIAR as 'Recargos',
            // (monto_pagos1*CANTIDAD_PAGOS1)+(CANTIDAD_PAGOS2*MONTO_PAGOS2) as 'Monto',monto,recargos,SALDO_FINANCIAR 
            //            FROM PRODMULT.MF_PEDIDO WHERE pedido='P01392624'


            Respuesta respuesta = new Respuesta();
            bool resultado = false;
            try
            {
                log.Info(string.Format("Grabando en bdd la respuesta a la solicitud de credito, referencia: {0}", pedido));
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    var qPedido = from pe in Exactus.MF_Pedido where pe.PEDIDO == pedido select pe;
                    if (qPedido.Count() > 0)
                    {
                        foreach (var item in qPedido)
                        {
                            item.FECHA_SOLICITUD = DateTime.Now;
                            item.SOLICITUD = long.Parse(resp.NumeroSolicitud);
                            item.PAGARE = long.Parse(resp.NumeroCredito);
                            item.NUMERO_CREDITO = resp.NumeroCredito;
                            item.CODIGO_RESPUESTA = resp.PunteoPrecalificacion;
                            item.RESULTADO_RESPUESTA = resp.ResultadoPrecalificacion;
                            item.MONTO_PAGOS1 = resp.Cuota;
                            item.MONTO_PAGOS2 = resp.UltimaCuota;
                            item.MONTO = (resp.Cuota * item.CANTIDAD_PAGOS1) + (resp.UltimaCuota * item.CANTIDAD_PAGOS2);
                            item.RECARGOS= (resp.Cuota * item.CANTIDAD_PAGOS1) + (resp.UltimaCuota * item.CANTIDAD_PAGOS2)-item.SALDO_FINANCIAR;
                        }
                    }
                    else
                    {
                        int nCotizacion;
                        if (int.TryParse(pedido, out nCotizacion))
                        {
                            var qCotizacion = from co in Exactus.MF_Cotizacion where co.COTIZACION == nCotizacion select co;
                            if (qCotizacion.Count() > 0)
                            {
                                foreach (var item in qCotizacion)
                                {
                                    item.FECHA_SOLICITUD = DateTime.Now;
                                    item.SOLICITUD = long.Parse(resp.NumeroSolicitud);
                                    item.PAGARE = long.Parse(resp.NumeroCredito);
                                    item.NUMERO_CREDITO = resp.NumeroCredito;
                                    item.CODIGO_RESPUESTA = resp.PunteoPrecalificacion;
                                    item.RESULTADO_RESPUESTA = resp.ResultadoPrecalificacion;
                                    item.MONTO_PAGOS1 = resp.Cuota;
                                    item.MONTO_PAGOS2 = resp.UltimaCuota;
                                    item.MONTO = (resp.Cuota * item.CANTIDAD_PAGOS1) + (resp.UltimaCuota * item.CANTIDAD_PAGOS2);
                                    item.RECARGOS = (resp.Cuota * item.CANTIDAD_PAGOS1) + (resp.UltimaCuota * item.CANTIDAD_PAGOS2) - item.SALDO_FINANCIAR;
                                }
                            }
                        }
                    }
                    Exactus.SaveChanges();
                    transactionScope.Complete();
                    resultado = true;
                }

            }
            catch (Exception ex)
            {
                resultado = false;
                log.Error(string.Format("Problema al guardar la respuesta de la solicitud de credito del pedido/cotizacion {0}, Problema {1}",pedido, ex.Message));
            }
            respuesta.Exito = resultado;
            return respuesta;
        }

        public RespuestaSolicitud ObtenerRespuesta( string pedido)
        {
            var respuesta = new Respuesta();
            RespuestaSolicitud RespAtid = null;
            try
            {
                log.Info(string.Format("Obteniendo la información del pedido"));
                var qCliente = from pe in Exactus.MF_Pedido
                               join c in Exactus.MF_Cliente on pe.CLIENTE equals c.CLIENTE
                               join f in Exactus.MF_Financiera on pe.FINANCIERA equals f.Financiera
                               where pe.PEDIDO == pedido

                               select new
                               {
                                SOLICITUD=  pe.SOLICITUD,
                                NUMERO_CREDITO=pe.NUMERO_CREDITO,
                                CODIGO_REPUESTA=pe.CODIGO_RESPUESTA,
                                RESULTADO_RESPUESTA=pe.RESULTADO_RESPUESTA

                                 };
                if (qCliente.Count() > 0)
                {
                    RespAtid = new RespuestaSolicitud
                    {
                        NumeroCredito = qCliente.First().NUMERO_CREDITO,
                        NumeroSolicitud = string.Format("{0}",qCliente.First().SOLICITUD),
                         PunteoPrecalificacion=qCliente.First().CODIGO_REPUESTA,
                         ResultadoPrecalificacion=qCliente.First().RESULTADO_RESPUESTA
                    };
                }
                else
                {
                    int nCotizacion = int.Parse(pedido);//castear a int la cotización
                    var qCotiCli = from pe in Exactus.MF_Cotizacion
                                   join c in Exactus.MF_Cliente on pe.CLIENTE equals c.CLIENTE
                                   join f in Exactus.MF_Financiera on pe.FINANCIERA equals f.Financiera
                                   where pe.COTIZACION == nCotizacion

                                   select new
                                   {
                                       SOLICITUD = pe.SOLICITUD,
                                       NUMERO_CREDITO = pe.NUMERO_CREDITO,
                                       CODIGO_REPUESTA = pe.CODIGO_RESPUESTA,
                                       RESULTADO_RESPUESTA = pe.RESULTADO_RESPUESTA

                                   };
                    if (RespAtid == null && qCotiCli.Count() > 0)
                    {
                        RespAtid = new RespuestaSolicitud
                        {
                            NumeroCredito = qCotiCli.First().NUMERO_CREDITO,
                            NumeroSolicitud = string.Format("{0}", qCotiCli.First().SOLICITUD),
                            PunteoPrecalificacion = qCotiCli.First().CODIGO_REPUESTA,
                            ResultadoPrecalificacion = qCotiCli.First().RESULTADO_RESPUESTA
                        };
                    }
                    else if (qCotiCli.Count() == 0)
                    {
                        var qCoti = from pe in Exactus.MF_Cotizacion
                                    join f in Exactus.MF_Financiera on pe.FINANCIERA equals f.Financiera
                                    where pe.COTIZACION == nCotizacion

                                    select new
                                    {
                                        SOLICITUD = pe.SOLICITUD,
                                        NUMERO_CREDITO = pe.NUMERO_CREDITO,
                                        CODIGO_REPUESTA = pe.CODIGO_RESPUESTA,
                                        RESULTADO_RESPUESTA = pe.RESULTADO_RESPUESTA

                                    };

                        if (RespAtid == null && qCoti.Count() > 0)
                        {
                            RespAtid = new RespuestaSolicitud
                            {
                                NumeroCredito = qCliente.First().NUMERO_CREDITO,
                                NumeroSolicitud = string.Format("{0}", qCliente.First().SOLICITUD),
                                PunteoPrecalificacion = qCliente.First().CODIGO_REPUESTA,
                                ResultadoPrecalificacion = qCliente.First().RESULTADO_RESPUESTA
                            };
                        }
                    }

                }

               
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
            return RespAtid;
        }

    }
}