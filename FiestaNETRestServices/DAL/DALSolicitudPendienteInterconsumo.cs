﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FiestaNETRestServices.Models;
using MF_Clases;
using FiestaNETRestServices.Content.Abstract;
using static MF_Clases.Clases;
using System.Data.Objects.SqlClient;
using System.Transactions;
using FiestaNETRestServices.Utils;
using System.Data.Linq.SqlClient;
using System.Configuration;

namespace FiestaNETRestServices.DAL
{
    public class DALSolicitudPendienteInterconsumo : MFDAL
    {

        public Respuesta GetSolicitudesPendientes()
        {
            //Respuesta mRespuesta =  new Respuesta();

            List<SolicitudInterconsumo> mSolicitudesInterconsumo = new List<SolicitudInterconsumo>();
            /*
             SELECT  'Cotizacion' AS TIPO_OPERACION
		            , MCOT.PEDIDO  AS NUMERO_DOCUMENTO
		            , MCOT.SOLICITUD
		            , MCOT.CLIENTE
		            , MV.VENDEDOR
		            , MCOT.NOMBRE
		            , MCOT.FECHA_AUTORIZACION		AS FECHA_SOLICITUD
		            , MCOT.FECHA_IMPRESION_PAGARE
		            , MCOT.FECHA_APROBACION_SOLICITUD
		            , MCOT.COBRADOR
		            , MV.EMAIL
		            , MC.JEFE
		            , MC.SUPERVISOR
            FROM	PRODMULT.MF_COTIZACION MCOT
		            , PRODMULT.MF_VENDEDOR MV
		            , PRODMULT.MF_COBRADOR MC
            --SELECT * FROM PRODMULT.MF_COTIZACION
            WHERE	--Que la cotizacion
		            MCOT.VENDEDOR = MV.VENDEDOR
		            AND MC.COBRADOR = MCOT.COBRADOR
		            --sea de Interconsumo
		            AND MCOT.FINANCIERA = 7
		            --TENGA INFORMACION DEL CLIENTE
		            --AND MCOT.CLIENTE IS NOT NULL
		            --grabada en linea
		            AND MCOT.GRABADA_EN_LINEA = 'S'
		            --menor a 30 dias
		            and isnull(MCOT.FECHA_AUTORIZACION,getdate()-31) > getdate()-30
		            --no este facturado
		            AND NOT EXISTS(
			            select	1
			            from	PRODMULT.FACTURA F
					            , PRODMULT.MF_PEDIDO MP
			            where	F.PEDIDO = MP.PEDIDO
					            AND MP.COTIZACION = MCOT.COTIZACION
		            )
		            --No ha sido aprobado
		            AND MCOT.FECHA_APROBACION_SOLICITUD IS NULL
            UNION ALL
            SELECT  'Pedido' AS TIPO_OPERACION
		            , MP.PEDIDO AS NUMERO_DOCUMENTO
		            , MP.SOLICITUD
		            , MP.CLIENTE
		            , P.VENDEDOR
		            , P.NOMBRE_CLIENTE
		            , MP.FECHA_AUTORIZACION		AS FECHA_SOLICITUD
		            , MP.FECHA_IMPRESION_PAGARE
		            , MP.FECHA_APROBACION_SOLICITUD
		            , P.COBRADOR
		            , MV.EMAIL
		            , MC.JEFE
		            , MC.SUPERVISOR
            FROM	PRODMULT.MF_PEDIDO MP
		            , PRODMULT.PEDIDO P
		            , PRODMULT.MF_VENDEDOR MV
		            , PRODMULT.MF_COBRADOR MC
            WHERE	--Que el pedido
		            P.PEDIDO = mp.PEDIDO
		            AND P.VENDEDOR = MV.VENDEDOR
		            AND MC.COBRADOR = P.COBRADOR
		            --sea de Interconsumo
		            AND MP.FINANCIERA = 7
		            --grabada en linea
		            AND MP.GRABADA_EN_LINEA = 'S'
		            --menor a 30 dias
		            and isnull(MP.FECHA_AUTORIZACION,getdate()-31) > getdate()-30
		            --no este facturado
		            AND NOT EXISTS(
			            select	1
			            from	PRODMULT.FACTURA F
			            where	F.PEDIDO = MP.PEDIDO
		            )
		            --No ha sido aprobado
		            AND MP.FECHA_APROBACION_SOLICITUD IS NULL
            ORDER BY MV.VENDEDOR
             
             */

            DateTime mFechaSolicitudTmp = DateTime.Now.AddDays(-31);
            DateTime mFechaVigencia = DateTime.Now.AddDays(-30);

            try
            {
                log.Debug("Get Solicitudes Pendientes DB");

                var qSolicitudesPendientes = (
                        from mc in dbExactus.MF_Cotizacion
                        join mv in dbExactus.MF_Vendedor on mc.VENDEDOR equals mv.VENDEDOR
                        join c in dbExactus.MF_Cobrador on mc.COBRADOR equals c.COBRADOR
                        where   //Que sea de Interconsumo
                                mc.FINANCIERA == 7
                                && mc.SOLICITUD > 0
                                //Que sea solicitada por medio del punto de venta 
                                && mc.GRABADA_EN_LINEA == "S"
                                //Que la fecha de solicitud sea menor a 30 dias.
                                //&& (mc.FECHA_AUTORIZACION ?? DateTime.Now.AddDays(-31)) > DateTime.Now.AddDays(-30)
                                && (mc.FECHA_AUTORIZACION ?? mFechaSolicitudTmp) > mFechaVigencia
                                //Que no haya sido aprobada
                                && mc.FECHA_APROBACION_SOLICITUD == null
                                //Que tenga un numero de solicitud
                                && mc.SOLICITUD != null
                                && !dbExactus.MF_Pedido.Any(x => x.COTIZACION == mc.COTIZACION
                                && x.FACTURA == (dbExactus.FACTURA.Where(
                                                           y => y.PEDIDO == x.PEDIDO
                                                       ).FirstOrDefault().FACTURA1)
                                    )

                        select new
                        {
                            TIPO_OPERACION = "Cotizacion"
                            ,
                            NUMERO_DOCUMENTO = SqlFunctions.StringConvert((double)mc.COTIZACION).Trim()
                            ,
                            SOLICITUD = mc.SOLICITUD
                            ,
                            CLIENTE = mc.CLIENTE
                            ,
                            NOMBRE_CLIENTE = mc.NOMBRE
                            ,
                            VENDEDOR = mv.VENDEDOR
                            ,
                            NOMBRE_VENDEDOR = mv.NOMBRE + " " + mv.APELLIDO
                            ,
                            FECHA_SOLICITUD = mc.FECHA_AUTORIZACION
                            ,
                            FECHA_IMPRESION_PAGARE = mc.FECHA_IMPRESION_PAGARE
                            ,
                            FECHA_APROBACION_SOLICITUD = mc.FECHA_APROBACION_SOLICITUD
                            ,
                            COBRADOR = mc.COBRADOR
                            ,
                            EMAIL_VENDEDOR = mv.EMAIL
                            ,
                            EMAIL_JEFE = c.JEFE
                            ,
                            EMAIL_SUPERVISOR = c.SUPERVISOR
                            ,
                            ESTADO = mc.ESTADO_INICIAL
                        }

                       ).Concat(
                        from mp in dbExactus.MF_Pedido
                        join p in dbExactus.PEDIDO on mp.PEDIDO equals p.PEDIDO1
                        join mv in dbExactus.MF_Vendedor on p.VENDEDOR equals mv.VENDEDOR
                        join c in dbExactus.MF_Cobrador on p.COBRADOR equals c.COBRADOR
                        where   //Que sea de Interconsumo
                                mp.FINANCIERA == 7
                                && mp.SOLICITUD > 0
                                //Que sea solicitada por medio del punto de venta 
                                && mp.GRABADA_EN_LINEA == "S"
                                //Que la fecha de solicitud sea menor a 30 dias.
                                //&& (mp.FECHA_AUTORIZACION ?? DateTime.Now.AddDays(-31)) > DateTime.Now.AddDays(-30)
                                && (mp.FECHA_AUTORIZACION ?? mFechaSolicitudTmp) > mFechaVigencia
                                //Que no haya sido aprobada
                                && mp.FECHA_APROBACION_SOLICITUD == null
                                //Que tenga un numero de solicitud
                                && mp.SOLICITUD != null
                                //QUe no exista algun pedido facturado
                                && !dbExactus.FACTURA.Any(x => x.PEDIDO == p.PEDIDO1)
                        select new
                        {
                            TIPO_OPERACION = "Pedido"
                            ,
                            NUMERO_DOCUMENTO = mp.PEDIDO
                            ,
                            SOLICITUD = mp.SOLICITUD
                            ,
                            CLIENTE = mp.CLIENTE
                            ,
                            NOMBRE_CLIENTE = p.NOMBRE_CLIENTE
                            ,
                            VENDEDOR = mv.VENDEDOR
                            ,
                            NOMBRE_VENDEDOR = mv.NOMBRE + " " + mv.APELLIDO
                            ,
                            FECHA_SOLICITUD = mp.FECHA_AUTORIZACION
                            ,
                            FECHA_IMPRESION_PAGARE = mp.FECHA_IMPRESION_PAGARE
                            ,
                            FECHA_APROBACION_SOLICITUD = mp.FECHA_APROBACION_SOLICITUD
                            ,
                            COBRADOR = p.COBRADOR
                            ,
                            EMAIL_VENDEDOR = mv.EMAIL
                            ,
                            EMAIL_JEFE = c.JEFE
                            ,
                            EMAIL_SUPERVISOR = c.SUPERVISOR
                            ,
                            ESTADO = mp.ESTADO_INICIAL
                        }

                    ).OrderByDescending(x => x.VENDEDOR);

                foreach (var item in qSolicitudesPendientes)
                {
                    SolicitudInterconsumo mSolicitud = new SolicitudInterconsumo();
                    mSolicitud.TIPO_OPERACION = item.TIPO_OPERACION;
                    mSolicitud.NUMERO_DOCUMENTO = item.NUMERO_DOCUMENTO;
                    mSolicitud.SOLICITUD = item.SOLICITUD;
                    mSolicitud.CLIENTE = item.CLIENTE;
                    mSolicitud.NOMBRE_CLIENTE = item.NOMBRE_CLIENTE;
                    mSolicitud.VENDEDOR = item.VENDEDOR;
                    mSolicitud.NOMBRE_VENDEDOR = item.NOMBRE_VENDEDOR;
                    mSolicitud.FECHA_SOLICITUD = item.FECHA_SOLICITUD;
                    mSolicitud.FECHA_IMPRESION_PAGARE = item.FECHA_IMPRESION_PAGARE;
                    mSolicitud.FECHA_APROBACION_SOLICITUD = item.FECHA_APROBACION_SOLICITUD;
                    mSolicitud.COBRADOR = item.COBRADOR;
                    mSolicitud.EMAIL_VENDEDOR = item.EMAIL_VENDEDOR;
                    mSolicitud.EMAIL_JEFE = item.EMAIL_JEFE;
                    mSolicitud.EMAIL_SUPERVISOR = item.EMAIL_SUPERVISOR;
                    mSolicitud.ESTADO = item.ESTADO;
                    mSolicitudesInterconsumo.Add(mSolicitud);
                }


                return new Respuesta(true, "", mSolicitudesInterconsumo);
            }
            catch (Exception ex)
            {
                log.Debug("Error: " + ex.StackTrace);
                return new Respuesta(false, "" + ex.StackTrace + ex.Message);
            }
        }

        /// <summary>
        /// Graba / Actualiza la información relacionada al rechazo de interconsumo para cada solicitud
        /// </summary>
        /// <param name="objRechazo">Soicitud</param>
        /// <param name="dtFechaNotifRechazo">Fecha de solicitud de rechazos a interconsumo (1 dia previo)</param>
        /// <returns>respuesta</returns>
        public Respuesta GrabarRechazoSolicitud(SolicitudInterconsumo objRechazo, DateTime dtFechaNotifRechazo)
        {
            Respuesta mRespuesta = new Respuesta();
            mRespuesta.Exito = true;
            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    var strFactura = objRechazo.NUMERO_DOCUMENTO.Substring(objRechazo.NUMERO_DOCUMENTO.IndexOf("-")+1,objRechazo.NUMERO_DOCUMENTO.Length- (objRechazo.NUMERO_DOCUMENTO.IndexOf("-")+1));
                    var strSerieFact = objRechazo.NUMERO_DOCUMENTO.Substring(0, objRechazo.NUMERO_DOCUMENTO.IndexOf("-"));

                    var qUpdateMFFactura_Estado = (
                        //búsqueda por factura (esperando que el dato de la factura venga correcto de INTERCONSUMO
                        from p in dbExactus.MF_Factura_Estado where p.FACTURA == objRechazo.NUMERO_DOCUMENTO && p.ESTADO.Equals(Const.Expediente.RECHAZADO) select p)
                        .Concat(
                        //Búsqueda por factura por la serie y el número de factura separados como un like
                        from p in dbExactus.MF_Factura_Estado
                        join pe in dbExactus.MF_Pedido on p.FACTURA  equals pe.FACTURA
                        where  p.ESTADO.Equals(Const.Expediente.RECHAZADO)
                        && (((pe.FACTURA.EndsWith(strFactura)) && (pe.FACTURA.StartsWith(strSerieFact)) && strSerieFact.Length > 0))
                        select p
                        ).Concat(
                        //búsqueda por el número de solicitud a interconsumo
                        from p in dbExactus.MF_Factura_Estado
                        join pe in dbExactus.MF_Pedido on p.FACTURA equals pe.FACTURA
                        where  p.ESTADO.Equals(Const.Expediente.RECHAZADO)
                        && pe.SOLICITUD==objRechazo.SOLICITUD
                        select p)
                        .Concat(
                        //búsqueda por el número de solicitud a interconsumo
                        from p in dbExactus.MF_Factura_Estado
                        join pe in dbExactus.MF_Pedido on p.FACTURA equals pe.FACTURA
                        join cli in dbExactus.MF_Cliente on pe.CLIENTE equals cli.CLIENTE
                        where objRechazo.CLIENTE.Length > 0 && p.ESTADO.Equals(Const.Expediente.RECHAZADO)
                        && cli.CLIENTE.EndsWith(objRechazo.CLIENTE)
                        && cli.PRIMER_NOMBRE == objRechazo.PRIMER_NOMBRE
                        && cli.PRIMER_APELLIDO == objRechazo.PRIMER_APELLIDO 
                        && pe.CreateDate.CompareTo(dtFechaNotifRechazo) < 90 //que la factura para ese cliente sea en un rango de 3 meses
                        && pe.NIVEL_PRECIO.StartsWith("InterCons")
                        select p
                        ).Concat(
                        //búsqueda por el número de solicitud a interconsumo
                        from p in dbExactus.MF_Factura_Estado
                        join pe in dbExactus.MF_Pedido on p.FACTURA equals pe.FACTURA
                        join cli in dbExactus.MF_Cliente on pe.CLIENTE equals cli.CLIENTE
                        where  p.ESTADO.Equals(Const.Expediente.RECHAZADO)
                        && cli.PRIMER_NOMBRE==objRechazo.PRIMER_NOMBRE && cli.SEGUNDO_NOMBRE==(objRechazo.SEGUNDO_NOMBRE.Substring(0).Contains(" ")? objRechazo.SEGUNDO_NOMBRE.Substring(0,objRechazo.SEGUNDO_NOMBRE.IndexOf(" ")-1):objRechazo.SEGUNDO_NOMBRE)
                        && cli.PRIMER_APELLIDO== objRechazo.PRIMER_APELLIDO && cli.SEGUNDO_APELLIDO==objRechazo.SEGUNDO_APELLIDO
                        && pe.CreateDate.CompareTo(dtFechaNotifRechazo) < 90 //que la factura para ese cliente sea en un rango de 3 meses
                        && pe.NIVEL_PRECIO.StartsWith("InterCons")
                        select p
                        )
                        ;

                    foreach (var item in qUpdateMFFactura_Estado)
                    {
                        item.FECHA = dtFechaNotifRechazo;
                        item.RecordDate = DateTime.Now;
                        item.ESTADO = Const.Expediente.RECHAZADO;

                    }
                    ///-----------------------------------
                    ///Si no hay ningun rechazo previo
                    ///-----------------------------------
                    if (qUpdateMFFactura_Estado.Count() == 0)
                    {
                        var qMFFactura_Estado = (
                        //búsqueda por factura (esperando que el dato de la factura venga correcto de INTERCONSUMO
                        from p in dbExactus.MF_Factura  where p.FACTURA == objRechazo.NUMERO_DOCUMENTO select p)
                        .Concat(
                        //Búsqueda por factura por la serie y el número de factura separados como un like
                        from p in dbExactus.MF_Factura
                        join pe in dbExactus.MF_Pedido on p.FACTURA equals pe.FACTURA
                        where
                         (((pe.FACTURA.EndsWith(strFactura)) && (pe.FACTURA.StartsWith(strSerieFact))) && strSerieFact.Length > 0)
                        select p
                        ).Concat(
                        //búsqueda por el número de solicitud a interconsumo
                        from p in dbExactus.MF_Factura
                        join pe in dbExactus.MF_Pedido on p.FACTURA equals pe.FACTURA
                        where pe.SOLICITUD == objRechazo.SOLICITUD
                        select p
                        )
                        .Concat(
                        //búsqueda por el número de cliente
                        from p in dbExactus.MF_Factura
                        join pe in dbExactus.MF_Pedido on p.FACTURA equals pe.FACTURA
                        join cli in dbExactus.MF_Cliente on pe.CLIENTE equals cli.CLIENTE
                        where objRechazo.CLIENTE.Length > 0 && cli.CLIENTE.EndsWith(objRechazo.CLIENTE)
                        && cli.PRIMER_NOMBRE == objRechazo.PRIMER_NOMBRE
                        && cli.PRIMER_APELLIDO == objRechazo.PRIMER_APELLIDO
                        
                        && pe.CreateDate.CompareTo(dtFechaNotifRechazo) < 90 //que la factura para ese cliente sea en un rango de 3 meses
                        && pe.NIVEL_PRECIO.StartsWith("InterCons")
                        select p
                        ).Concat(
                        //búsqueda por la información del cliente
                        from p in dbExactus.MF_Factura
                        join pe in dbExactus.MF_Pedido on p.FACTURA equals pe.FACTURA
                        join cli in dbExactus.MF_Cliente on pe.CLIENTE equals cli.CLIENTE
                        where cli.PRIMER_NOMBRE == objRechazo.PRIMER_NOMBRE && (cli.SEGUNDO_NOMBRE == (objRechazo.SEGUNDO_NOMBRE.Substring(0).Contains(" ") ? objRechazo.SEGUNDO_NOMBRE.Substring(0, objRechazo.SEGUNDO_NOMBRE.IndexOf(" ") - 1) : objRechazo.SEGUNDO_NOMBRE)
                            || cli.SEGUNDO_NOMBRE== objRechazo.SEGUNDO_NOMBRE)
                        && cli.PRIMER_APELLIDO == objRechazo.PRIMER_APELLIDO && cli.SEGUNDO_APELLIDO == objRechazo.SEGUNDO_APELLIDO
                        
                        && pe.CreateDate.CompareTo(dtFechaNotifRechazo) < 90 //que la factura para ese cliente sea en un rango de 3 meses
                        && pe.NIVEL_PRECIO.StartsWith("InterCons")
                        select p
                        );

                        //si no encontró rechazos para actualizar, pero si encontró registros relacionados a esa factura
                        if (qUpdateMFFactura_Estado.Count() == 0 && qMFFactura_Estado.Count() > 0)
                        {
                            dbExactus.MF_Factura_Estado.AddObject(new MF_Factura_Estado
                            {
                                FACTURA = qMFFactura_Estado.First().FACTURA,
                                ESTADO = Const.Expediente.RECHAZADO,
                                FECHA = dtFechaNotifRechazo.Date,
                                RecordDate = DateTime.Now,
                                CreateDate = DateTime.Now,
                                CreatedBy = "MF.FIESTANET"

                            });
                        }
                        else if (qMFFactura_Estado.Count() == 0)
                        {
                            //dbExactus.MF_Factura_Estado.AddObject(new MF_Factura_Estado
                            //{
                            //    FACTURA = objRechazo.NUMERO_DOCUMENTO,
                            //    ESTADO = Const.ExpedienteInterconsumo.RECHAZADO,
                            //    FECHA = dtFechaNotifRechazo.Date,
                            //    RecordDate = DateTime.Now,
                            //    CreateDate = DateTime.Now,
                            //    CreatedBy = "MF.FIESTANET"

                            //});
                            mRespuesta.Exito = false;
                        }

                    }

                    if (mRespuesta.Exito)
                    {
                        var qUpdateMFFactura_DocSoporte = (from p in dbExactus.MF_Factura_Documentos_Soporte where p.FACTURA == objRechazo.NUMERO_DOCUMENTO select p).Concat(
                            from p in dbExactus.MF_Factura_Documentos_Soporte
                            join pe in dbExactus.MF_Pedido on p.FACTURA equals pe.FACTURA
                            where pe.SOLICITUD == objRechazo.SOLICITUD
                            select p
                            ).Concat(
                            from p in dbExactus.MF_Factura_Documentos_Soporte
                            join pe in dbExactus.MF_Pedido on p.FACTURA equals pe.FACTURA
                            where (((pe.FACTURA.EndsWith(strFactura)) && (pe.FACTURA.StartsWith(strSerieFact))) && strSerieFact.Length > 0)
                            select p
                            )
                            .Concat(
                             //búsqueda por el número de solicitud a interconsumo
                             from p in dbExactus.MF_Factura_Documentos_Soporte
                             join pe in dbExactus.MF_Pedido on p.FACTURA equals pe.FACTURA
                             join cli in dbExactus.MF_Cliente on pe.CLIENTE equals cli.CLIENTE
                             where objRechazo.CLIENTE.Length > 0 && cli.CLIENTE.EndsWith(objRechazo.CLIENTE)
                         && cli.PRIMER_NOMBRE == objRechazo.PRIMER_NOMBRE && cli.PRIMER_APELLIDO == objRechazo.PRIMER_APELLIDO
                         && pe.CreateDate.CompareTo(dtFechaNotifRechazo) < 90 //que la factura para ese cliente sea en un rango de 3 meses
                         && pe.NIVEL_PRECIO.StartsWith("InterCons")
                             select p
                            ).Concat(
                             //búsqueda por el número de solicitud a interconsumo
                             from p in dbExactus.MF_Factura_Documentos_Soporte
                             join pe in dbExactus.MF_Pedido on p.FACTURA equals pe.FACTURA
                             join cli in dbExactus.MF_Cliente on pe.CLIENTE equals cli.CLIENTE
                             where cli.PRIMER_NOMBRE == objRechazo.PRIMER_NOMBRE && cli.SEGUNDO_NOMBRE == (objRechazo.SEGUNDO_NOMBRE.Substring(0).Contains(" ") ? objRechazo.SEGUNDO_NOMBRE.Substring(0, objRechazo.SEGUNDO_NOMBRE.IndexOf(" ") - 1) : objRechazo.SEGUNDO_NOMBRE)
                         && cli.PRIMER_APELLIDO == objRechazo.PRIMER_APELLIDO && cli.SEGUNDO_APELLIDO == objRechazo.SEGUNDO_APELLIDO
                         && cli.APELLIDO_CASADA == objRechazo.APELLIDO_CASADA
                         && pe.CreateDate.CompareTo(dtFechaNotifRechazo) < 90 //que la factura para ese cliente sea en un rango de 3 meses
                         && pe.NIVEL_PRECIO.StartsWith("InterCons")//que sea por interconsumo
                         select p
                            )
                            ;
                        foreach (var item in qUpdateMFFactura_DocSoporte)
                        {
                            item.OBSERVACIONES_INTERCONSUMO = objRechazo.DESCRIPCION_ERROR.Equals(string.Empty) ? "" : objRechazo.DESCRIPCION_ERROR.Substring(objRechazo.DESCRIPCION_ERROR.LastIndexOf("[") + 1, objRechazo.DESCRIPCION_ERROR.Length - (objRechazo.DESCRIPCION_ERROR.LastIndexOf("[") + 1) - 1);

                        }

                        ///-----------------------------------
                        ///Si no hay ningun rechazo previo, insertar el estado de rechazado
                        ///-----------------------------------
                        if (qUpdateMFFactura_DocSoporte.Count() == 0)
                            dbExactus.MF_Factura_Documentos_Soporte.AddObject(
                                new MF_Factura_Documentos_Soporte
                                {
                                    FACTURA = objRechazo.NUMERO_DOCUMENTO,
                                    IDENTIFIACION = "",
                                    OTRA_IDENTIFIACION = "",
                                    CONSTANCIA_INGRESOS = "",
                                    DOCUMENTO_SERVICIO_PUBLICO = "",
                                    RecordDate = DateTime.Now,
                                    PAGARE = "",
                                    CreatedBy = "MF.FIESTANET",
                                    CreateDate = DateTime.Now,
                                    OBSERVACIONES_INTERCONSUMO = objRechazo.DESCRIPCION_ERROR.Equals(string.Empty) ? "" : objRechazo.DESCRIPCION_ERROR.Substring(objRechazo.DESCRIPCION_ERROR.LastIndexOf("[") + 1, objRechazo.DESCRIPCION_ERROR.Length - (objRechazo.DESCRIPCION_ERROR.LastIndexOf("[") + 1) - 1)
                                }
                                );
                        mRespuesta.Exito = true;
                    }
                    dbExactus.SaveChanges();

                    transactionScope.Complete();
                    
                    
                }
            }
            catch (Exception e)
            {
                mRespuesta.Exito = false;
                mRespuesta.StackError = e.StackTrace;
                mRespuesta.Mensaje = e.Message;
                log.Error(this.GetType().Name + " " + e.StackTrace);
            }
            return mRespuesta;
        }

		/// <summary>
		/// Método para obtener la información del rechazo de una solicitud de interconsumo
		/// se obtiene información del jefe de tienda, vendedor, con el objetivo de notificarles
		/// acerca de este rechazo.
		/// </summary>
		/// <param name="solRechazada"> la solicitud interconsumo a buscar</param>
		/// <returns>Respuesta con EMAIL del vendedor, EMAIL del Jefe de Tienda (como prioridad entre la info)</returns>
        public Respuesta GetInfoRechazoSolicitud(SolicitudInterconsumo solRechazada)
        {
            Respuesta mRespuesta = new Respuesta();
            long nSolicitudes = 0;
            string strFactura = string.Empty;
            string strSerieFact = string.Empty;
            nSolicitudes = (long)solRechazada.SOLICITUD;
            strFactura = solRechazada.NUMERO_DOCUMENTO;
            if (strFactura.Contains("-"))
            {
                strSerieFact = strFactura.Substring(0, strFactura.IndexOf("-"));
                strFactura = strFactura.Substring(strFactura.IndexOf("-") + 1, (strFactura.Length - (strFactura.IndexOf("-") + 1)));
            }
            
            try
            {
                log.Debug("Get Info Solicitudes Rechazadas");
                ///------------Query utilizada
                ///
                ///select FA.cobrador,VE.vendedor,VE.EMAIL 'EMAILVENDEDOR',CO.JEFE AS 'EMAILJEFETIENDA',FA.FECHA
                //from prodmult.factura FA, prodmult.mf_vendedor VE, prodmult.mf_cobrador CO
                // where FA.factura in ('F05C-009160', 'F25C-1591', 'F023C-001126', 'F27C-609')
                // AND FA.VENDEDOR = VE.VENDEDOR
                // AND FA.COBRADOR = CO.COBRADOR
                ///------------
                var qSolicitudesRechazadas = (
                        from FA in dbExactus.MF_Factura
                        join F in dbExactus.FACTURA on FA.FACTURA equals F.FACTURA1
                        join VE in dbExactus.MF_Vendedor on FA.VENDEDOR equals VE.VENDEDOR
                        join CO in dbExactus.MF_Cobrador on FA.COBRADOR equals CO.COBRADOR
                        join PE in dbExactus.MF_Pedido on FA.PEDIDO equals PE.PEDIDO
                        where (((PE.FACTURA.EndsWith(strFactura)) && (PE.FACTURA.StartsWith(strSerieFact))))
                        && F.TIPO_DOCUMENTO == "F"
                        select new
                        {
                            EmailJefeTienda = CO.JEFE,
                            Tienda = FA.COBRADOR,
                            EmailVendedor = VE.EMAIL,
                            FechaFactura = FA.FECHA,
                            FActura = FA.FACTURA,
                            Solicitud = PE.SOLICITUD,
                            ClienteMF = FA.CLIENTE,
                            nombreCliente = F.NOMBRE_CLIENTE
                            ,
                            EmailSupervisor = CO.SUPERVISOR,
                            FechaSolicitud = ((from q in dbExactus.MF_Factura_Estado where q.FACTURA == FA.FACTURA && q.ESTADO == "E" select q).Any() ?
                                                    (from q in dbExactus.MF_Factura_Estado where q.FACTURA == FA.FACTURA && q.ESTADO == "E" select q).FirstOrDefault().FECHA : DateTime.Now)
                        }).Concat(
                            from FA in dbExactus.MF_Factura
                            join F in dbExactus.FACTURA on FA.FACTURA equals F.FACTURA1
                            join VE in dbExactus.MF_Vendedor on FA.VENDEDOR equals VE.VENDEDOR
                            join CO in dbExactus.MF_Cobrador on FA.COBRADOR equals CO.COBRADOR
                            join PE in dbExactus.MF_Pedido on FA.PEDIDO equals PE.PEDIDO
                            where PE.SOLICITUD == solRechazada.SOLICITUD
                            && F.TIPO_DOCUMENTO == "F"
                            select new
                            {
                                EmailJefeTienda = CO.JEFE,
                                Tienda = FA.COBRADOR,
                                EmailVendedor = VE.EMAIL,
                                FechaFactura = FA.FECHA,
                                FActura = FA.FACTURA,
                                Solicitud = PE.SOLICITUD,
                                ClienteMF = FA.CLIENTE,
                                nombreCliente = F.NOMBRE_CLIENTE,
                                EmailSupervisor = CO.SUPERVISOR,
                                FechaSolicitud = ((from q in dbExactus.MF_Factura_Estado where q.FACTURA == FA.FACTURA && q.ESTADO == "E" select q).Any() ?
                                                    (from q in dbExactus.MF_Factura_Estado where q.FACTURA == FA.FACTURA && q.ESTADO == "E" select q).FirstOrDefault().FECHA : DateTime.Now)
                            }
                    )
                    .Concat(
                            from FA in dbExactus.MF_Factura
                            join F in dbExactus.FACTURA on FA.FACTURA equals F.FACTURA1
                            join VE in dbExactus.MF_Vendedor on FA.VENDEDOR equals VE.VENDEDOR
                            join CO in dbExactus.MF_Cobrador on FA.COBRADOR equals CO.COBRADOR
                            join PE in dbExactus.MF_Pedido on FA.PEDIDO equals PE.PEDIDO
                            join cli in dbExactus.MF_Cliente on PE.CLIENTE equals cli.CLIENTE
                            where PE.CLIENTE.EndsWith(solRechazada.CLIENTE) && solRechazada.CLIENTE.Length > 0
                            && F.TIPO_DOCUMENTO == "F"
                             && PE.CreateDate.CompareTo(DateTime.Now) < 90 //que la factura para ese cliente sea en un rango de 3 meses
                            && cli.PRIMER_NOMBRE == solRechazada.PRIMER_NOMBRE && cli.PRIMER_APELLIDO == solRechazada.PRIMER_APELLIDO
                            && PE.NIVEL_PRECIO.StartsWith("InterCons")

                            select new
                            {
                                EmailJefeTienda = CO.JEFE,
                                Tienda = FA.COBRADOR,
                                EmailVendedor = VE.EMAIL,
                                FechaFactura = FA.FECHA,
                                FActura = FA.FACTURA,
                                Solicitud = PE.SOLICITUD,
                                ClienteMF = FA.CLIENTE,
                                nombreCliente = F.NOMBRE_CLIENTE,
                                EmailSupervisor = CO.SUPERVISOR,
                                FechaSolicitud = ((from q in dbExactus.MF_Factura_Estado where q.FACTURA == FA.FACTURA && q.ESTADO == "E" select q).Any() ?
                                                    (from q in dbExactus.MF_Factura_Estado where q.FACTURA == FA.FACTURA && q.ESTADO == "E" select q).FirstOrDefault().FECHA : DateTime.Now)
                            }
                    )
                    .Concat(
                            from FA in dbExactus.MF_Factura
                            join F in dbExactus.FACTURA on FA.FACTURA equals F.FACTURA1
                            join VE in dbExactus.MF_Vendedor on FA.VENDEDOR equals VE.VENDEDOR
                            join CO in dbExactus.MF_Cobrador on FA.COBRADOR equals CO.COBRADOR
                            join PE in dbExactus.MF_Pedido on FA.PEDIDO equals PE.PEDIDO
                            join cli in dbExactus.MF_Cliente on PE.CLIENTE equals cli.CLIENTE
                            where cli.PRIMER_NOMBRE == solRechazada.PRIMER_NOMBRE && (cli.SEGUNDO_NOMBRE == (solRechazada.SEGUNDO_NOMBRE.Substring(0).Contains(" ") ? solRechazada.SEGUNDO_NOMBRE.Substring(0, solRechazada.SEGUNDO_NOMBRE.IndexOf(" ") - 1) : solRechazada.SEGUNDO_NOMBRE)
                            || cli.SEGUNDO_NOMBRE == solRechazada.SEGUNDO_NOMBRE)
                        && cli.PRIMER_APELLIDO == solRechazada.PRIMER_APELLIDO && cli.SEGUNDO_APELLIDO == solRechazada.SEGUNDO_APELLIDO

                        && PE.CreateDate.CompareTo(DateTime.Now) < 90 //que la factura para ese cliente sea en un rango de 3 meses
                        && PE.NIVEL_PRECIO.StartsWith("InterCons")
                            select new
                            {
                                EmailJefeTienda = CO.JEFE,
                                Tienda = FA.COBRADOR,
                                EmailVendedor = VE.EMAIL,
                                FechaFactura = FA.FECHA,
                                FActura = FA.FACTURA,
                                Solicitud = PE.SOLICITUD,
                                ClienteMF = FA.CLIENTE,
                                nombreCliente = F.NOMBRE_CLIENTE,
                                EmailSupervisor = CO.SUPERVISOR,
                                FechaSolicitud = ((from q in dbExactus.MF_Factura_Estado where q.FACTURA == FA.FACTURA && q.ESTADO == "E" select q).Any() ?
                                                    (from q in dbExactus.MF_Factura_Estado where q.FACTURA == FA.FACTURA && q.ESTADO == "E" select q).FirstOrDefault().FECHA:DateTime.Now) 
                            }
                    );
                    //.OrderByDescending(x => x.FechaFactura);

                
                List<MF_Clases.infoInterconsumoMensajeRechazo> ObjInfo = new List<MF_Clases.infoInterconsumoMensajeRechazo>();
                if (qSolicitudesRechazadas.Count() > 0)
                {
                    var item = qSolicitudesRechazadas.ToList()[0];
                    ObjInfo.Add(
                        new MF_Clases.infoInterconsumoMensajeRechazo
                        {
                            EmailJefeTienda = item.EmailJefeTienda,
                            EmailVendedor = item.EmailVendedor,
                            FechaFact = item.FechaFactura,
                            Factura = item.FActura,
                            Tienda = item.Tienda,
                            solicitud = item.Solicitud,
                            ClienteMF=item.ClienteMF,
                            nombreCliente=item.nombreCliente,
                            EmailSupervisor=item.EmailSupervisor,
                            FechaSolicitud=item.FechaSolicitud.Date!=DateTime.Now.Date?item.FechaSolicitud: (DateTime?)null
                        }
                        );
                }
                if (qSolicitudesRechazadas.Count() > 0)
                    mRespuesta.Objeto = ObjInfo;
                mRespuesta.Exito = true;

            }
            catch (Exception e)
            {
                mRespuesta.Exito = false;
                log.Error(this.GetType().Name + " " + e.StackTrace);
            }
            return mRespuesta;

        }
        /// <summary>
        /// Obtener las solicitudes rechazadas actuales
        /// 
        /// </summary>
        /// <returns></returns>
        public Respuesta GetInfoRechazosActuales(int nFinanciera)
			{
			Respuesta mRespuesta = new Respuesta();
			log.Debug("Inicio de método para consultar las solicitudes rechazadas al día de hoy.");
			try
				{
				List<SolicitudInterconsumo> lstSolRechazadas = new List<SolicitudInterconsumo>();
				mRespuesta.Exito = false;
                #region "Query utilizada"
                /*
				 * 
					select   fe.fecha,
							FA.COBRADOR,
							ped.solicitud,
							FE.ESTADO,
							fa.cliente,
							cli.primer_apellido+' '+cli.segundo_apellido +', '+cli.primer_nombre+' '+cli.segundo_nombre 'Nombre del Cliente',
							fa.factura,
							ped.total_facturar,
							ped.enganche,
							(
								SELECT TOP 1 FECHA FROM prodmult.mf_factura_estado fae
									where fae.factura=fa.factura
										and fae.ESTADO='E'
							) as 'Fecha_Envio',
							FE.FECHA AS 'Fecha Rechazo',
							sp.OBSERVACIONES_INTERCONSUMO
							from prodmult.mf_factura_documentos_soporte sp with (NOLOCK) INNER JOIN prodmult.factura fa on sp.factura=fa.factura
								INNER JOIN prodmult.mf_pedido ped on ped.pedido=fa.pedido inner join prodmult.mf_factura_estado fe on sp.factura=fe.factura
								inner join prodmult.mf_cliente cli on ped.cliente=cli.cliente
								where 
								(
									SELECT TOP 1 ESTADO FROM prodmult.mf_factura_estado fae
									where fae.factura=fa.factura
									ORDER BY recorddate DESC
									)='R'
									and cc.saldo>0
									and not exists
									(
									SELECT  1  FROM prodmult.mf_factura_estado fae
									where fae.factura=fa.factura
									and fae.ESTADO='D'
									)
							and fe.estado='R'
	
						order by fe.fecha desc,fa.factura 
				 */
                #endregion

                var qSolicitudesRechazadas = (
                            from FA in dbExactus.FACTURA
                            join SP in dbExactus.MF_Factura_Documentos_Soporte on FA.FACTURA1 equals SP.FACTURA
                            join PE in dbExactus.MF_Pedido on FA.PEDIDO equals PE.PEDIDO
                            join FE in dbExactus.MF_Factura_Estado on SP.FACTURA equals FE.FACTURA
                            join CLI in dbExactus.MF_Cliente on PE.CLIENTE equals CLI.CLIENTE
                            join cc in dbExactus.DOCUMENTOS_CC on FA.FACTURA1 equals cc.DOCUMENTO
                            where
                              (
                                       dbExactus.MF_Factura_Estado.Where(
                                                            fes => fes.FACTURA == FA.FACTURA1
                                                        ).OrderByDescending(F=>F.RecordDate).FirstOrDefault().ESTADO == "R"

                                                        )
                             &&
                             (
                                       dbExactus.MF_Factura_Estado.Where(
                                                            fes => fes.FACTURA == FA.FACTURA1
                                                            && fes.ESTADO=="D"

                                                        ).Any()==false)
                            && cc.SALDO>0
                            &&	(PE.FINANCIERA == nFinanciera || nFinanciera==-1)
							&& FE.ESTADO == "R"
                            && FA.ANULADA != "S"
                            select new
								{
								FECHA_FACT=FA.FECHA,
								TIENDA=FA.COBRADOR,
								SOLICITUD=PE.SOLICITUD,
								ESTADO=FE.ESTADO,
								CLIENTE_NOMBRE=CLI.PRIMER_APELLIDO+" "+CLI.SEGUNDO_APELLIDO+", "+CLI.PRIMER_NOMBRE+" "+CLI.SEGUNDO_NOMBRE,
								CLIENTE=CLI.CLIENTE,
								FACTURA=FA.FACTURA1,
								MONTOFACT=FA.TOTAL_FACTURA,
								ENGANCHE=PE.ENGANCHE,
								FECHAENVIO=( from Fae in dbExactus.MF_Factura_Estado where Fae.ESTADO =="E" && Fae.FACTURA == FE.FACTURA select Fae).FirstOrDefault().FECHA,
								FECHARECHAZO=FE.FECHA,
								OBSERVACIONES=SP.OBSERVACIONES_INTERCONSUMO
                                ,NOMBRE_FINANCIERA=(from p in dbExactus.MF_Financiera where p.Financiera == PE.FINANCIERA select p).FirstOrDefault().Nombre
								}
							);
                if(qSolicitudesRechazadas!=null)
				    if (qSolicitudesRechazadas.Count() > 0)
					{
					    foreach (var item in qSolicitudesRechazadas)
						    lstSolRechazadas.Add(
							    new SolicitudInterconsumo { 
								    FECHA_FACTURA=item.FECHA_FACT,
								    CLIENTE=item.CLIENTE,
								    NOMBRE_CLIENTE=item.CLIENTE_NOMBRE,
								    FECHA_SOLICITUD=item.FECHAENVIO,
								    FECHA_RECHAZO_SOLICITUD=item.FECHARECHAZO,
								    COBRADOR=item.TIENDA,
								    SOLICITUD=item.SOLICITUD,
								     VALOR_FACTURA=item.MONTOFACT,
								     NUMERO_DOCUMENTO=item.FACTURA,
								     VALOR_ENGANCHE=item.ENGANCHE,
								     OBSERVACIONES=item.OBSERVACIONES,
                                      NOMBRE_FINANCIERA=item.NOMBRE_FINANCIERA
							    }
						    );
					}
				mRespuesta.Exito = true;
				mRespuesta.Objeto = lstSolRechazadas;
				mRespuesta.Mensaje = lstSolRechazadas.Count() + ((nFinanciera != -1) ? " Solicitudes rechazadas de la financiera " + nFinanciera : " En total de solicitudes rechazadas.");
				log.Debug(mRespuesta.Mensaje);
				} catch (Exception e)
				{
				mRespuesta.Exito = false;
				log.Error(this.GetType().Name + " " + e.StackTrace);
				}
			
			return mRespuesta;
			}

        /// <summary>
        /// Obtener las solicitudes pendientes de aprobación de la financiera
        /// </summary>
        /// <param name="blnTodas"></param>
        /// <param name="nFinanciera"></param>
        /// <returns></returns>
        public Respuesta GetInfoSolicitudesPendientes(bool blnTodas,int nFinanciera)
        {
            //si blnTodas=false, se obtendran las solicitudes pendientes de hace más de una semana
            // si blnTodas=true se obtendran todas las solicitudes pendientes, sin importar cuando se mandaron
            Respuesta mRespuesta = new Respuesta();
            log.Debug("Inicio de método para consultar las solicitudes pendientes al día de hoy.");
            try
            {
                List<SolicitudInterconsumo> lstSolPendientes = new List<SolicitudInterconsumo>();
                DateTime dtSemanaAnterior = DateTime.Now.AddDays(-7);
                mRespuesta.Exito = false;
                var qSolicitudesPendientes = (
                           from FA in dbExactus.FACTURA
                           join SP in dbExactus.MF_Factura_Documentos_Soporte on FA.FACTURA1 equals SP.FACTURA
                           join PE in dbExactus.MF_Pedido on FA.PEDIDO equals PE.PEDIDO
                           join FE in dbExactus.MF_Factura_Estado on SP.FACTURA equals FE.FACTURA
                           join CLI in dbExactus.MF_Cliente on PE.CLIENTE equals CLI.CLIENTE
                           where
                             (
                                      dbExactus.MF_Factura_Estado.Where(
                                                           fes => fes.FACTURA == FA.FACTURA1
                                                       ).OrderByDescending(F => F.RecordDate).FirstOrDefault().ESTADO == "E"

                                                       )
                            &&
                             (
                                      dbExactus.MF_Factura_Estado.Any(
                                                           fes => fes.FACTURA == FA.FACTURA1
                                                           && fes.ESTADO=="A"
                                                       )==false
                              )
                               &&
                             (
                                      dbExactus.MF_Factura_Estado.Any(
                                                           fes => fes.FACTURA == FA.FACTURA1
                                                           && fes.ESTADO == "D"
                                                       ) == false
                              )
                           && ((FE.FECHA<= dtSemanaAnterior && (!blnTodas)) || (blnTodas))
                           && (PE.FINANCIERA==nFinanciera || nFinanciera ==-1) //todas las financieras -1 o una en especial
                           && FE.ESTADO == "E"
                           && FA.ANULADA != "S"
                           select new
                           {
                               FECHA_FACT = FA.FECHA,
                               TIENDA = FA.COBRADOR,
                               SOLICITUD = PE.SOLICITUD,
                               ESTADO = FE.ESTADO,
                               CLIENTE_NOMBRE = CLI.PRIMER_APELLIDO + " " + CLI.SEGUNDO_APELLIDO + ", " + CLI.PRIMER_NOMBRE + " " + CLI.SEGUNDO_NOMBRE,
                               CLIENTE = CLI.CLIENTE,
                               FACTURA = FA.FACTURA1,
                               MONTOFACT = FA.TOTAL_FACTURA,
                               ENGANCHE = PE.ENGANCHE,
                               FECHAENVIO = FE.FECHA,
                               OBSERVACIONES=SP.OBSERVACIONES,
                               FINANCIERA =PE.FINANCIERA,
                               NOMBRE_FINANCIERA = ( from p in dbExactus.MF_Financiera where p.Financiera == PE.FINANCIERA select p).FirstOrDefault().Nombre
                           }
                           );
                if (qSolicitudesPendientes != null)
                    if (qSolicitudesPendientes.Count() > 0)
                    {
                        foreach (var item in qSolicitudesPendientes)
                            lstSolPendientes.Add(
                                new SolicitudInterconsumo
                                {
                                    COBRADOR = item.TIENDA,
                                    FECHA_FACTURA = item.FECHA_FACT,
                                    NUMERO_DOCUMENTO = item.FACTURA,
                                    SOLICITUD = item.SOLICITUD,
                                    FECHA_SOLICITUD = item.FECHAENVIO,
                                    CLIENTE = item.CLIENTE,
                                    NOMBRE_CLIENTE = item.CLIENTE_NOMBRE,
                                    VALOR_FACTURA = item.MONTOFACT,
                                    VALOR_ENGANCHE = item.ENGANCHE,
                                    VALOR_SALDO=0,
                                    OBSERVACIONES = item.OBSERVACIONES,
                                    FINANCIERA=Convert.ToInt32(item.FINANCIERA),
                                    NOMBRE_FINANCIERA=item.NOMBRE_FINANCIERA
                                }
                            );
                    }
                mRespuesta.Exito = true;
                mRespuesta.Objeto = lstSolPendientes;
                mRespuesta.Mensaje = lstSolPendientes.Count() + ((!blnTodas) ? " Solicitudes pendientes de más de una semana. " : " En Total de Solicitudes Pendientes.");
                log.Debug(mRespuesta.Mensaje);

            } catch (Exception i)
            {
                mRespuesta.Exito = false;
                log.Error(this.GetType().Name + " " + i.StackTrace);
            }
                return mRespuesta;
        }

        /// <summary>
        /// Obtener las solicitudes pendientes de aprobación de la financiera
        /// </summary>
        /// <param name="nFinanciera"></param>
        /// <returns></returns>
        public Respuesta ObtenerSolicitudesPendientesEnTienda(int nFinanciera)
        {
            //BUSCANDO LAS FACTURAS QUE YA ESTEN EN EL SISTEMA PERO QUE NO SE HAYAN ENVIADO A LAS FINANCIERAS
            // ES DECIR,QUE LAS TIENDAS NO HAN ENVIADO AÚN LA PAPELERÍA

            Respuesta mRespuesta = new Respuesta();
            log.Debug("Inicio de método para consultar las solicitudes pendientes que tiendas envien a oficina");
            try
            {
                List<SolicitudInterconsumo> lstSolPendientes = new List<SolicitudInterconsumo>();
                DateTime dtFechaValidacion = new DateTime(2017,12,1);//fecha de implementación de mf_factura_estado
                DateTime dtFechaValidacionf = new DateTime(2018, 3, 1); //FECHA DE VALIDACION PARA 2 FINANCIERAS ESPECIFICAMENTE
                //validacion de implementacion con bancredit y bi-facil
                if (nFinanciera == 11 || nFinanciera == 3)
                {
                    dtFechaValidacion = new DateTime(2018, 3, 1);//implementación de mf_factura_estado en esas 2 financieras
                }
                mRespuesta.Exito = false;
                var qSolicitudesPendientes = (
                           from FA in dbExactus.FACTURA
                           
                           join PE in dbExactus.MF_Pedido on FA.PEDIDO equals PE.PEDIDO
                           join doc in dbExactus.DOCUMENTOS_CC on FA.FACTURA1 equals doc.DOCUMENTO
                          
                           join CLI in dbExactus.MF_Cliente on PE.CLIENTE equals CLI.CLIENTE
                           where
                             doc.TIPO == "FAC"
                           && doc.SALDO > 1
                           && (PE.FINANCIERA== nFinanciera ||(nFinanciera==-1 && (PE.FINANCIERA == 7 || PE.FINANCIERA == 11 || PE.FINANCIERA == 3)))
                           && FA.ANULADA != "S"
                            && (SqlFunctions.DateDiff("DAY", dtFechaValidacion, FA.FECHA) > 0 ) 
                            && !dbExactus.MF_Factura_Estado.Any(s => s.FACTURA.Contains(FA.FACTURA1))

                           select new
                           {
                               FECHA_FACT = FA.FECHA,
                               TIENDA = FA.COBRADOR,
                               SOLICITUD = PE.SOLICITUD,
                               ESTADO = "",
                               CLIENTE_NOMBRE = CLI.PRIMER_APELLIDO + " " + CLI.SEGUNDO_APELLIDO + ", " + CLI.PRIMER_NOMBRE + " " + CLI.SEGUNDO_NOMBRE,
                               CLIENTE = CLI.CLIENTE,
                               FACTURA = FA.FACTURA1,
                               MONTOFACT = FA.TOTAL_FACTURA,
                               ENGANCHE = PE.ENGANCHE,
                               FECHAENVIO = "",
                               OBSERVACIONES ="",
                               FINANCIERA = PE.FINANCIERA,
                               NOMBRE_FINANCIERA = (from p in dbExactus.MF_Financiera where p.Financiera == PE.FINANCIERA select p).FirstOrDefault().Nombre,
                               SALDO=doc.SALDO
                           }
                           );

                if (qSolicitudesPendientes != null)
                    if (qSolicitudesPendientes.Count() > 0)
                    {
                        foreach (var item in qSolicitudesPendientes)
                            lstSolPendientes.Add(
                                new SolicitudInterconsumo
                                {
                                    COBRADOR = item.TIENDA,
                                    FECHA_FACTURA = item.FECHA_FACT,
                                    NUMERO_DOCUMENTO = item.FACTURA,
                                    SOLICITUD = item.SOLICITUD,
                                    VALOR_SALDO=item.SALDO,
                                    CLIENTE = item.CLIENTE,
                                    NOMBRE_CLIENTE = item.CLIENTE_NOMBRE,
                                    VALOR_FACTURA = item.MONTOFACT,
                                    VALOR_ENGANCHE = item.ENGANCHE,
                                    OBSERVACIONES = item.OBSERVACIONES,
                                    FINANCIERA = Convert.ToInt32(item.FINANCIERA),
                                    NOMBRE_FINANCIERA = item.NOMBRE_FINANCIERA
                                }
                            );
                    }
                mRespuesta.Exito = true;
                mRespuesta.Objeto = lstSolPendientes;
                mRespuesta.Mensaje = lstSolPendientes.Count() + " Solicitudes Pendientes de envio desde tiendas.";
                log.Debug(mRespuesta.Mensaje);

            }
            catch (Exception i)
            {
                mRespuesta.Exito = false;
                log.Error(this.GetType().Name + " " + i.StackTrace);
            }
            return mRespuesta;
        }

        /// <summary>
        /// Obtener un reporte de las solicitudes con estado Rechazado, que no han sido
        /// solventadas en más de N días
        /// </summary>
        /// <param name="nFinanciera"></param>
        /// <returns></returns>
        public Respuesta ObtenerSolcitudesRechazadasxMasdeNdias(long nFinanciera,bool blAviso)
        {
            //BUSCANDO LOS RECHAZOS QUE LLEVEN MÁS DE N DÍAS SIN RESOLVER, SI blAviso=true, se buscan las que lleven >30 (aviso
            // si blAviso=false, se buscan las que lleven >45 (se ingresan a la tabla mf_comisionexcepcion para que no se le apliquen comisiones

            Respuesta mRespuesta = new Respuesta();
            log.Debug("Inicio de método para consultar las solicitudes rechazadas por N días");
            try
            {
                DateTime dtFechaFE = new DateTime(2018, 04, 1);//fecha de implementación de mf_factura_estado
                List<SolicitudInterconsumo> lstSolPendientes = new List<SolicitudInterconsumo>();
                int nDias = int.Parse(ConfigurationManager.AppSettings["DiasValidacionSolicitudesRechazadas"].ToString());
                int nDiasDef= int.Parse(ConfigurationManager.AppSettings["DiasValidacionSolicitudesRechazadas"].ToString());
                if (blAviso)//aviso de alerta a los vendedores
                    nDias  = int.Parse(ConfigurationManager.AppSettings["DiasValidacionALERTASolicitudesRechazadas"].ToString());
                DateTime dtFechaValidacion = DateTime.Now.AddDays(-nDias);
                DateTime dtFechaValidacionDef = DateTime.Now.AddDays(-nDiasDef);


                mRespuesta.Exito = false;

               
                var qSolicitudesPendientes = (
                           from FA in dbExactus.FACTURA
                            join PE in dbExactus.MF_Pedido on FA.PEDIDO equals PE.PEDIDO
                           join fe in dbExactus.MF_Factura_Estado on FA.FACTURA1 equals fe.FACTURA
                           join dc in dbExactus.MF_Factura_Documentos_Soporte on FA.FACTURA1 equals dc.FACTURA
                           join CLI in dbExactus.MF_Cliente on PE.CLIENTE equals CLI.CLIENTE
                           join CO in dbExactus.MF_Cobrador on FA.COBRADOR equals CO.COBRADOR
                           join CC in dbExactus.DOCUMENTOS_CC on FA.FACTURA1 equals CC.DOCUMENTO
                           join ve in dbExactus.MF_Vendedor on FA.VENDEDOR equals ve.VENDEDOR
                           
                           where 
                            fe.ESTADO == "R"
                           && (PE.FINANCIERA == nFinanciera )
                           && FA.ANULADA != "S"
                           && CC.SALDO>0
                           && SqlFunctions.DateDiff("DAY", fe.FECHA, dtFechaFE) <= 0 //implementado a partir de abril 2018
                           && !dbExactus.MF_ComisionFactura.Any(cf => cf.FACTURA.Contains(FA.FACTURA1)) // que no haya sido pagada ya 
                         &&   !dbExactus.MF_ComisionExcepcion.Any(s => s.FACTURA.Contains(FA.FACTURA1)) //que no se haya insertado en las excepciones de comisión aún
                           && ((SqlFunctions.DateDiff("DAY",  fe.FECHA, dtFechaValidacion) >= 0 && blAviso==false) //RECHAZO TARDIO >45 DIAS
                           ||   ((blAviso && SqlFunctions.DateDiff("DAY",  fe.FECHA, dtFechaValidacionDef) < 0) //que sea menor a 45 dias (PARA UN AVISO)
                                  && (SqlFunctions.DateDiff("DAY",  fe.FECHA, dtFechaValidacion) >= 0))) //que sea mayor a 30 dias

                            && (       dbExactus.MF_Factura_Estado.Where(
                                                            fes => fes.FACTURA == FA.FACTURA1
                                                        ).OrderByDescending(F => F.FECHA).FirstOrDefault().ESTADO == "R"
                                )
                           select new
                           {
                               FECHA_FACT = FA.FECHA,
                               TIENDA = FA.COBRADOR,
                               SOLICITUD = PE.SOLICITUD,
                               ESTADO = fe.ESTADO,
                               CLIENTE_NOMBRE = CLI.PRIMER_APELLIDO + " " + CLI.SEGUNDO_APELLIDO + ", " + CLI.PRIMER_NOMBRE + " " + CLI.SEGUNDO_NOMBRE,
                               CLIENTE = CLI.CLIENTE,
                               FACTURA = FA.FACTURA1,
                               MONTOFACT = FA.TOTAL_FACTURA,
                               ENGANCHE = PE.ENGANCHE,
                               FECHARECHAZO = fe.FECHA,
                               OBSERVACIONES =dc.OBSERVACIONES+ " "+dc.OBSERVACIONES_INTERCONSUMO,
                               FINANCIERA = PE.FINANCIERA,
                               VENDEDOR=FA.VENDEDOR,
                               Emailvendedor=ve.EMAIL,
                               EmailJefe=CO.JEFE,
                               SALDO_FACT=CC.SALDO,
                               EmailSupervisor=CO.SUPERVISOR,
                               NOMBRE_FINANCIERA = (from p in dbExactus.MF_Financiera where p.Financiera == PE.FINANCIERA select p).FirstOrDefault().Nombre
                           }
                           );

                if (qSolicitudesPendientes != null)
                {
                    if (qSolicitudesPendientes.Count() > 0)
                    {
                        foreach (var item in qSolicitudesPendientes)
                            lstSolPendientes.Add(
                                new SolicitudInterconsumo
                                {
                                    COBRADOR = item.TIENDA,
                                    VENDEDOR = item.VENDEDOR,
                                    FECHA_FACTURA = item.FECHA_FACT,
                                    NUMERO_DOCUMENTO = item.FACTURA,
                                    SOLICITUD = item.SOLICITUD,
                                    FECHA_RECHAZO_SOLICITUD = item.FECHARECHAZO,
                                    CLIENTE = item.CLIENTE,
                                    NOMBRE_CLIENTE = item.CLIENTE_NOMBRE,
                                    VALOR_FACTURA = item.MONTOFACT,
                                    VALOR_ENGANCHE = item.ENGANCHE,
                                    OBSERVACIONES = item.OBSERVACIONES,
                                    FINANCIERA = Convert.ToInt32(item.FINANCIERA),
                                    NOMBRE_FINANCIERA = item.NOMBRE_FINANCIERA,
                                    EMAIL_VENDEDOR = item.Emailvendedor,
                                    EMAIL_JEFE = item.EmailJefe,
                                    EMAIL_SUPERVISOR = item.EmailSupervisor,
                                    VALOR_SOLICITUD = item.SALDO_FACT

                                }
                            );
                    }
                    mRespuesta.Exito = true;
                    mRespuesta.Objeto = lstSolPendientes;
                    mRespuesta.Mensaje = lstSolPendientes.Count() + " Solicitudes Pendientes de envio desde tiendas.";
                    log.Debug(mRespuesta.Mensaje);
                }

            }
            catch (Exception i)
            {
                mRespuesta.Exito = false;
                log.Error(this.GetType().Name + " " + i.StackTrace);
            }
            return mRespuesta;
        }
        /// <summary>
        /// Método para colocar un expediente como no comisionable
        /// </summary>
        /// <param name="rechazo"></param>
        /// <returns></returns>
        public Respuesta MarcarNoComisionableFacturaRechazada(List<SolicitudInterconsumo> lstrechazo)
        {
            Respuesta mRespuesta = new Respuesta();
            log.Debug("Inicio de método para marcar una factura como no comisionable mf_comisionexcepcion.");

            try
            {

                foreach (SolicitudInterconsumo rechazo in lstrechazo)
                {
                    var qCE = (
                          from CE in dbExactus.MF_ComisionExcepcion
                          where CE.FACTURA == rechazo.NUMERO_DOCUMENTO
                          select new
                          {
                              Factura = CE.FACTURA
                          }
                          );
                    if (qCE != null)
                        if (!qCE.Any())
                        {
                            dbExactus.MF_ComisionExcepcion.AddObject(
                                                new MF_ComisionExcepcion
                                                {
                                                    FACTURA = rechazo.NUMERO_DOCUMENTO,
                                                    COBRADOR = rechazo.COBRADOR,
                                                    VENDEDOR = rechazo.VENDEDOR,
                                                    TIPO = "N",
                                                    CreatedBy = "MF.FIESTANET",
                                                    CreateDate = DateTime.Now,
                                                    RecordDate = DateTime.Now

                                                }
                                                );
                            log.Debug("---> Factura " + rechazo.NUMERO_DOCUMENTO + " no comisionable.");

                            dbExactus.SaveChanges();
                        }
                }
                mRespuesta.Exito = true;
                mRespuesta.Mensaje = lstrechazo.Count + " expedientes rechazados marcados como no comisionables.";
            }
            catch (Exception ex)
            {
                mRespuesta.Exito = false;
                mRespuesta.Mensaje = ex.Message;
                log.Error(this.GetType().Name + " " + ex.StackTrace);
            }
            return mRespuesta;
        }
    }
}