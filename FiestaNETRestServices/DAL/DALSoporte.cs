﻿using FiestaNETRestServices.Content.Abstract;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FiestaNETRestServices.DAL
{
    public class DALSoporte : MFDAL
    {
        public Respuesta ObtenerVendedor(string Usuario)
        {
            Respuesta mRespuesta = new Respuesta();
            mRespuesta.Exito = false;
            try
            {
               
               
                var qVendedor = (from v in dbExactus.MF_Vendedor where v.EMAIL.Contains(Usuario) select v).FirstOrDefault();
                mRespuesta.Exito = true;
                mRespuesta.Objeto = new DtoVendedor
                {
                    NOMBRE = qVendedor.NOMBRE,
                    APELLIDO = qVendedor.APELLIDO,
                    CELULAR = qVendedor.CELULAR,
                    COBRADOR = qVendedor.COBRADOR,
                    EMAIL = qVendedor.EMAIL,
                    VENDEDOR = qVendedor.VENDEDOR
                };

            } catch (Exception ex)
            {
                log.Error(CatchClass.ExMessage(ex,"DALSoporte","ObtenerVendedor"));
                mRespuesta.Mensaje = CatchClass.ExMessage(ex, "DALSoporte", "ObtenerVendedor");
            }
            return mRespuesta;
        }
    }
}