﻿using FiestaNETRestServices.Content.Abstract;
using MF.Comun.Dto;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FiestaNETRestServices.DAL
{
    public class DALTienda : MFDAL
    {
        public IEnumerable<Tienda> ObtenerTiendas()
        {
            List<Tienda> lstTienda = new List<Tienda>();
            Respuesta mRespuesta = new Respuesta();
            var qTiendas = (from c in dbExactus.MF_Cobrador where c.ACTIVO == "S" && c.CATEGORIA != null select c);
            if(qTiendas.Count()>0)
            {
                foreach (var i in qTiendas)
                    lstTienda.Add(new Tienda { CodigoTienda=i.COBRADOR, Nombre= i.ALIAS, Activo=i.ACTIVO });
            }

            return lstTienda.AsEnumerable();
        }
    }
}