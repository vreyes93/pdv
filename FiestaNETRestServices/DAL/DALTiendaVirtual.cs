﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Controllers.Tools;
using FiestaNETRestServices.Models;
using FiestaNETRestServices.Utils;
using Humanizer;
using MF.Comun.Dto.TiendaVirtual;
using MF_Clases;
using MF_Clases.Facturacion;
using MF_Clases.GuateFacturas.FEL;
using MF_Clases.Restful;
using MF_Clases.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Transactions;
using System.Web;
using System.Web.Configuration;
using static MF_Clases.Utils.CONSTANTES.GUATEFAC;

namespace FiestaNETRestServices.DAL
{
    public class DALTiendaVirtual : MFDAL
    {
        
        
        /// <summary>
        /// Método para obtener los artículos para actualizar en la tienda virtual
        /// </summary>
        /// <returns></returns>
        public static List<Object> ObtenerInventarioArticulos()
        {
            #region "background"
            /*
             * Antecedentes--
             * 
             * --Inventario con precio especial		
                --Inventario 2017-11-13
                select	A.articulo as sku
		                , case when isnull(APO.precioOriginal,-1) = -1 then round(A.PRECIO_BASE_LOCAL*1.12,2) else round(APO.precioOriginal*1.12,2) end price
		                , case when isnull(APO.precioOferta,-1) = -1 then null else round(A.PRECIO_BASE_LOCAL*1.12,2) end special_price
		                , case when isnull(APO.precioOferta,-1) = -1 then '' else convert(nvarchar(MAX),APO.FechaOfertaDesde, 1) end special_price_from_date
		                , case when isnull(APO.precioOferta,-1) = -1 then '' else convert(nvarchar(MAX),APO.FechaOfertaHasta, 1) end special_price_to_date
		                --, round(A.PRECIO_BASE_LOCAL*1.12,2) as price
		                , CASE WHEN EB.CANT_DISPONIBLE <5 THEN 0 ELSE round(EB.CANT_DISPONIBLE,0) END qty
                from	prodmult.articulo A
		                left outer join prodmult.EXISTENCIA_BODEGA EB
			                ON (A.articulo = EB.ARTICULO AND EB.BODEGA = 'F01')
		                left outer join [prodmult].[MF_ArticuloPrecioOferta] APO
			                on(A.articulo = APO.articulo and apo.estatus = 'V' )
                where	A. articulo IN	--('115104-001')	
		                ('111439-021','111439-077','111452-021','111466-053','111485-126','111486-021','111486-077','111491-021','111498-142'
		                ,'111503-168','111504-168','111506-078','111507-171','111508-172','111509-173','111520-078','114439-021','114452-021'
		                ,'114466-053','114485-126','114486-021','114491-021','114500-010','114503-168','114504-168','114506-078','114507-171'
		                ,'114508-172','114509-173','115104-001','115105-001','115220-078','115282-002','115295-010','115296-057','115298-142'
		                ,'116185-126','121015-002','121018-030','121019-002','121020-002','121021-002','121022-002','122009-002','131006-002'
		                ,'135117-042','135207-002','135208-002','151304-001','151304-065','151406-001','151406-012','152304-153','152304-154'
		                ,'152310-002','152310-021','152310-130','152310-176','152409-153','152409-154','152414-002','152414-133','152414-146'
		                ,'152415-003','152421-082','152422-042','152425-174','152505-136','152616-153','152616-154','152625-153','152625-154'
		                ,'152628-002','152628-133','152628-146','152631-125','152631-153','152634-012','152643-084','152644-133','152644-145'
		                ,'152645-082','152647-147','152648-135','152649-135','152651-176','152652-096','152653-002','152653-057','152653-148'
		                ,'152654-021','152654-039','152654-130','152654-148','152654-175','152655-039','152655-130','152655-148','152656-002'
		                ,'152656-021','152656-130','152656-148','152656-174','152657-002','152657-125','152658-042','152659-084','152659-145'
		                ,'152660-174','152661-176','153606-125','153606-153','153612-125','153612-154','211012-160','211076-056','212072-001'
		                ,'212075-144','212076-056','212077-042','212079-130','212079-162','212080-163','212081-165','212082-166','212085-096'
		                ,'212086-096','213025-096','213026-096','221113-096','223010-096','411009-174','411009-178','411010-179','412007-122'
		                ,'412008-052','412009-052','412010-121','412012-074','412013-043','412014-177','413002-120','413003-121','413004-120'
		                ,'413005-121','413006-018','413006-152','413007-005','413007-083','421008-120','421010-119','421011-121','421012-120'
		                ,'422030-120','422031-002','422032-003','422033-040','422034-003','422034-005','422035-175','422036-175','423019-057'
		                ,'423020-101','423021-018','423022-005','511430-007','511430-011','511433-011','511433-064','511435-022','511435-037'
		                ,'511437-004','511437-007','511440-007','511441-004','511442-003','511443-010','511445-066','511446-067','511447-007'
		                ,'511448-059','511449-068','511455-067','511456-066','511465-007','511471-072','511474-004','511474-015','512023-011'
		                ,'512023-057','512024-004','512024-023','512027-056','512027-075','512029-011','512030-057','513012-007','513013-003'
		                ,'513014-011','522020-137','523012-042','523015-021','523017-021','531035-133','531036-014','531038-156','532078-021'
		                ,'532079-137','532082-137','532083-137','532087-137','532089-002','532089-016','532089-151','532090-155','532091-083'
		                ,'114439-077','152414-082','152628-082','320018-002','320018-021','320019-002','320019-021','531037-137','532080-137'
		                ,'532081-137','532088-014','532092-156','532093-157','532094-157','532095-158','532096-096','532097-158','532098-159'
		                ,'532099-160','532100-002','512022-007','512022-015','512022-011','511451-022','511451-069','511451-004');	
             */
            #endregion  
            EXACTUSERPEntities Exactus = new EXACTUSERPEntities();
            log.Info("--ACTUALIZAR INVENTARIO TIENDA VIRTUAL-- Obteniendo existencias");
            List<Object> lstProductos = new List<Object>();
            try
            {
                var qBodega = (from eb in Exactus.EXISTENCIA_BODEGA where eb.BODEGA == "F01" select new { ARTICULO = eb.ARTICULO, CANT_DISPONIBLE = eb.CANT_DISPONIBLE });
                var qArticulosOferta = (from af in Exactus.MF_ArticuloPrecioOferta where af.Estatus == "V" select new { ARTICULO = af.Articulo, FECHADESDE = af.FechaOfertaDesde, FECHAHASTA = af.FechaOfertaHasta, PRECIO_OFERTA = af.PrecioOferta, PRECIO_ORIGINAL = af.PrecioOriginal });
                var aArticulos = (from ar in Exactus.ARTICULO
                                   join aCat in Exactus.MF_Catalogo on ar.ARTICULO1 equals aCat.CODIGO_CAT
                                   where
                                   aCat.CODIGO_TABLA == Const.CATALOGO.CATALOGO_TIENDA_VIRTUAL_INVENTARIO_ARTICULOS
                                    && aCat.NOMBRE_CAT == "MUEBLES" && aCat.ACTIVO=="S"
                                   select new { ARTICULO=ar.ARTICULO1, ARTICULO_PRECIO_BASE=ar.PRECIO_BASE_LOCAL});
                              

                var qProductos = (from Ar in aArticulos.ToList()
                                  join art in qArticulosOferta.ToList()
                                    on new { Ar.ARTICULO } equals new { art.ARTICULO } into ARTOFF_join
                                  from ARTBOD in ARTOFF_join.DefaultIfEmpty()
                                  select new
                                  {
                                       Ar.ARTICULO,
                                      FECHAOFERTAD = (ARTBOD?.FECHADESDE),
                                      FECHAOFERTAA = (ARTBOD?.FECHAHASTA),
                                      PRECIO_OFERTA = (ARTBOD?.PRECIO_OFERTA),
                                      PRECIO_ORIGINAL = (ARTBOD?.PRECIO_ORIGINAL),
                                                PRECIO_BASE_LOCAL = Ar.ARTICULO_PRECIO_BASE
                                  }
                    );
                var Existencias = (from qpro in qProductos.ToList()
                                   join qbod in qBodega.ToList()
                                   on new { qpro.ARTICULO } equals new { qbod.ARTICULO } into ARTEXIST_join
                                   from ARTEXIST in ARTEXIST_join.DefaultIfEmpty()
                                   select new
                                   {
                                       qpro.ARTICULO, 
                                       CANT_DISPONIBLE = (ARTEXIST?.CANT_DISPONIBLE),
                                       qpro.FECHAOFERTAD,
                                       qpro.FECHAOFERTAA,
                                       qpro.PRECIO_OFERTA,
                                       qpro.PRECIO_ORIGINAL,
                                       qpro.PRECIO_BASE_LOCAL
                                   }
                                 );
                if (Existencias.Count() > 0)
                {
                    decimal precio = 0;
                    decimal iva = Utils.Utilitarios.ObtenerIva();//obtenemos el iva actual (preparado multiplicar).
                    foreach (var item in Existencias)
                    {
                        
                        var nCantidadMinimaInventario = ObtenerCantidadMinimaProducto();
                        ///
                        //Procedemos a hacer los cálculos y ajustes de precios por redondeo
                        ///
                        var precioEspecial = (item.PRECIO_OFERTA == null ? "" : decimal.Round(item.PRECIO_BASE_LOCAL * iva, 2).ToString());
                        precio = (item.PRECIO_ORIGINAL == null ? decimal.Round((item.PRECIO_BASE_LOCAL * iva), 2) : decimal.Round((decimal)item.PRECIO_ORIGINAL*iva,2));
                        custom_attributes attr_SpecialPrice = new custom_attributes("special_price", precioEspecial);
                        custom_attributes attr_SPFromDate = new custom_attributes("special_from_date", precioEspecial==string.Empty?string.Empty:item.FECHAOFERTAD.ToString());//PRECIO OFERTA DESDE
                        custom_attributes attr_SPToDate = new custom_attributes("special_to_date", precioEspecial == string.Empty ? string.Empty : item.FECHAOFERTAA.ToString());//PRECIO OFERTA HASTA
                        custom_attributes attr_SPOferta = new custom_attributes("oferta", precioEspecial == string.Empty ? "20" : "21");//Está en oferta si | no
                        List<custom_attributes> lstAtributos = new List<custom_attributes>();
                        ///
                        //* Tanto los productos a precio regular, como los ofertados tienen este atributo: Oferta
                        ///
                        lstAtributos.Add(attr_SPOferta);
                        ////
                        ////

                        /////Se separan para agregar el precio especial, la fecha de inicio y fin de la oferta, solo a los productos ofertados
                        /////
                        if (precioEspecial != string.Empty)
                        {
                            lstAtributos.Add(attr_SpecialPrice);
                            lstAtributos.Add(attr_SPFromDate);
                            lstAtributos.Add(attr_SPToDate);

                            lstProductos.Add(new Producto_Ofertado
                            {
                                sku = item.ARTICULO,
                                status = 1,
                                price = precio.ToString(),
                                extension_attributes = new Extension_attributes(((int)item.CANT_DISPONIBLE > nCantidadMinimaInventario ? (int)item.CANT_DISPONIBLE : 0)),
                                custom_attributes = lstAtributos
                            });
                        }
                        else
                        {
                            lstProductos.Add(new ProductoPrecioRegular
                            {
                                sku = item.ARTICULO,
                                status = 1,
                                price = precio.ToString(),
                                extension_attributes = new Extension_attributes(((int)item.CANT_DISPONIBLE> nCantidadMinimaInventario? (int)item.CANT_DISPONIBLE:0)),
                                custom_attributes = lstAtributos
                            });
                        }
                    }
                }
            } catch (Exception ex)
            {
                log.Error("PROBLEMA AL OBTENER LAS EXISTENCIAS PARA LA TIENDA VIRTUAL, ERROR--->"+ex.Message);
            }
            return lstProductos;
        }
        /// <summary>
        /// Obtener la url de magento ya sea de pruebas o la de producción
        /// para esto, asegurarse también que el password sea el correcto según el ambiente
        /// ya que para el password solo hay un campo.
        /// </summary>
        /// <param name="ambiente"></param>
        /// <returns></returns>
        public static string ObtenerDirMagento()
        {
            EXACTUSERPEntities Exactus = new EXACTUSERPEntities();
            #region "ambiente"
            string mAmbiente = ConfigurationManager.AppSettings["Ambiente"].ToString(); 
            
            #endregion
            log.Debug("--Obteniendo dirección de Magento (url)");
            string path= (mAmbiente.Equals("PRO") ? Const.TIENDA_VIRTUAL.URL_TIENDA_VIRTUAL : Const.TIENDA_VIRTUAL.URL_TIENDA_VIRTUAL_PRUEBAS);
            var qCatalogo = (from cat in Exactus.MF_Catalogo where cat.CODIGO_TABLA == Const.CATALOGO.CATALOGO_TIENDA_VIRTUAL select cat);
            if (qCatalogo.Count() > 0)
            {
                if (qCatalogo.Where(x => x.CODIGO_CAT == path)
                    .FirstOrDefault() != null)
                {
                    return qCatalogo.Where(x => x.CODIGO_CAT == path)
                    .FirstOrDefault().NOMBRE_CAT + "/" + qCatalogo.Where(x => x.CODIGO_CAT == Const.TIENDA_VIRTUAL.VERSION_API_REST_MAGENTO).FirstOrDefault().NOMBRE_CAT;
                }
            }
            return String.Empty;
        }
        
        /// <summary>
        ///Obtener el path para hacer el llamado para autenticarse en magento
        ///devuelve el path para el metodo autorizar que devuelve un token
        /// </summary>
        /// <returns></returns>
        public static string ObtenerDirAutenticacion()
        {
            EXACTUSERPEntities Exactus = new EXACTUSERPEntities();
            log.Debug("--Obteniendo path de autenticación de magento (token)");
            var qCatalogo = (from cat in Exactus.MF_Catalogo where cat.CODIGO_TABLA == Const.CATALOGO.CATALOGO_TIENDA_VIRTUAL select cat);
            if (qCatalogo.Count() > 0)
            {
                return qCatalogo.Where(x => x.CODIGO_CAT ==  Const.TIENDA_VIRTUAL.OBTENER_TOKEN_AUTHORIZACION).First().NOMBRE_CAT;
            }
            return String.Empty;
        }
        /// <summary>
        /// Obtener usuario para la tienda virtual
        /// </summary>
        /// <returns></returns>
        public static string ObtenerUsuarioTiendaVirtual()
        {
            EXACTUSERPEntities Exactus = new EXACTUSERPEntities();
            log.Debug("--Obteniendo usuario de magento");
            var qCatalogo = (from cat in Exactus.MF_Catalogo where cat.CODIGO_TABLA == Const.CATALOGO.CATALOGO_TIENDA_VIRTUAL select cat);
            if (qCatalogo.Count() > 0)
            {
                return qCatalogo.Where(x => x.CODIGO_CAT == Const.TIENDA_VIRTUAL.USUARIO_TIENDA_VIRTUAL).First().NOMBRE_CAT;
            }
            return String.Empty;
        }

        /// <summary>
        /// Obtener el password para la tienda virtual
        /// </summary>
        /// <returns></returns>
        public static string ObtenerPasswordTiendaVirtual()
        {
            EXACTUSERPEntities Exactus = new EXACTUSERPEntities();
            log.Debug("--Obteniendo password de autenticación de magento");
            var qCatalogo = (from cat in Exactus.MF_Catalogo where cat.CODIGO_TABLA == Const.CATALOGO.CATALOGO_TIENDA_VIRTUAL select cat);
            if (qCatalogo.Count() > 0)
            {
                return qCatalogo.Where(x => x.CODIGO_CAT == Const.TIENDA_VIRTUAL.PASSWORD_TIENDA_VIRTUAL).First().NOMBRE_CAT;
            }
            return String.Empty;
        }

        /// <summary>
        /// Obtener el path para los productos de la tienda virtual
        /// </summary>
        /// <returns></returns>
        public static string ObtenerPathCatalogoProductos()
        {
            EXACTUSERPEntities Exactus = new EXACTUSERPEntities();
            log.Debug("--Obteniendo path de los productos de magento (token)");
            var qCatalogo = (from cat in Exactus.MF_Catalogo where cat.CODIGO_TABLA == Const.CATALOGO.CATALOGO_TIENDA_VIRTUAL select cat);
            if (qCatalogo.Count() > 0)
            {
                return "/"+qCatalogo.Where(x => x.CODIGO_CAT == Const.TIENDA_VIRTUAL.PRODUCTOS).First().NOMBRE_CAT+"/";
            }
            return String.Empty;
        }

        public static int ObtenerCantidadMinimaProducto()
        {
            EXACTUSERPEntities Exactus = new EXACTUSERPEntities();
            log.Debug("--Obteniendo cantidad minima de los productos de la tienda virtual");
            var qCatalogo = (from cat in Exactus.MF_Catalogo where cat.CODIGO_TABLA == Const.CATALOGO.CATALOGO_TIENDA_VIRTUAL select cat);
            if (qCatalogo.Count() > 0)
            {
                return int.Parse(qCatalogo.Where(x => x.CODIGO_CAT == Const.TIENDA_VIRTUAL.CANTIDAD_MINIMA_PRODUCTO).First().NOMBRE_CAT);
            }
            return 0;
        }

        /// <summary>
        ///Método para validar las existencias y precios
        /// </summary>
        /// <param name="CodArticulo"></param>
        /// <returns>verdadero o falso</returns>
        public static Respuesta ValidarExistencias(string CodArticulo)
        {
            Respuesta mResp = new Respuesta();

            decimal mIVA = Utils.Utilitarios.ObtenerIva(); //FACTOR DEL IVA +1;
            
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            try
            {   
                dtoArticulo cama = new dtoArticulo();
                string nombre = string.Empty;

                var alias = (from al in db.ALIAS_PRODUCCION where al.ARTICULO == CodArticulo && al.DESCRIPCION != null && al.ALIAS_PRODUCCION1.Contains("NC") select al).FirstOrDefault();
                if (alias != null)
                    nombre = alias.DESCRIPCION;
                else
                    nombre = (from ar in db.ARTICULO.Where(a => a.ARTICULO1.Equals(CodArticulo)) select ar).FirstOrDefault().DESCRIPCION;


                var qArticulos = (
                                        from ar in db.ARTICULO.Where(a => a.ARTICULO1.Equals(CodArticulo))
                                        join po in db.MF_ArticuloPrecioOferta.Where(x =>
                                                                                       x.Estatus.Equals("V")
                                                                                    )
                                        on ar.ARTICULO1 equals po.Articulo into joinarts
                                        from left in joinarts.DefaultIfEmpty()
                                        join npo in db.MF_ArticuloNivelPrecioOferta.Where(x =>
                                                                                       x.Estatus.Equals("V")
                                                                                       && x.NivelPrecio.Equals("ContadoEfect")
                                                                                       && ((x.Articulo.Substring(0, 3).Equals("149")) || (x.Articulo.Substring(0, 3) != "149"))
                                                                                     && ((x.Articulo.Substring(0, 3).Equals("141")) || (x.Articulo.Substring(0, 3) != "141"))
                                                                                        )
                                        on ar.ARTICULO1 equals npo.Articulo into joinCamas
                                        from leftCamas in joinCamas.DefaultIfEmpty()


                                        select new
                                        {
                                            Articulo = ar.ARTICULO1,
                                            Nombre = nombre,
                                            Activo = ar.ACTIVO.Equals("S"),
                                            Clasificacion = ar.CLASIFICACION_3,
                                            PrecioOriginal = (left.PrecioOriginal == null ? ar.PRECIO_BASE_LOCAL : left.PrecioOriginal) * mIVA,
                                            PrecioOferta = left.PrecioOferta == null ? (
                                                            leftCamas == null ? 0 : ar.PRECIO_BASE_LOCAL * mIVA * (1 - (leftCamas.pctjDescuento / 100))
                                                        ) : left.PrecioOferta * mIVA,
                                            OfertaDesde = left.FechaOfertaDesde == null ? (
                                                                leftCamas.FechaOfertaDesde == null ?
                                                                new DateTime(1900, 1, 1) : leftCamas.FechaOfertaDesde
                                                                ) : left.FechaOfertaDesde,
                                            OfertaHasta = left.FechaOfertaHasta == null ? (
                                                                leftCamas.FechaOfertaHasta == null ?
                                                            new DateTime(1900, 1, 1) : leftCamas.FechaOfertaHasta
                                            ) : left.FechaOfertaHasta
                                        }
                              ); 
                if (qArticulos.Count() > 0)
                {
                    //validar si es una cama o parte de la cama
                   var blnEsCama = qArticulos.FirstOrDefault().Clasificacion.Equals("149") || 
                                   qArticulos.FirstOrDefault().Clasificacion.Equals("142") || 
                                   qArticulos.FirstOrDefault().Clasificacion.Equals("141");
                    //obtenemos disponible
                    int cantidadDisponible = ObtenerCantidadDisponibleProducto(CodArticulo, blnEsCama);
                    dtoArticulo ArticuloResp = new dtoArticulo
                    {
                        sku=qArticulos.FirstOrDefault().Articulo,
                        Nombre=qArticulos.FirstOrDefault().Nombre,
                        Activo=qArticulos.FirstOrDefault().Activo,
                        cantidad = cantidadDisponible,
                        EstaEnOferta=qArticulos.FirstOrDefault().PrecioOferta != 0? true:false,
                        PrecioNormal=Math.Round(qArticulos.FirstOrDefault().PrecioOriginal,2,MidpointRounding.AwayFromZero),
                        PrecioOferta= Math.Round(Convert.ToDecimal(qArticulos.FirstOrDefault().PrecioOferta), 2, MidpointRounding.AwayFromZero),
                        OfertaDesde = qArticulos.FirstOrDefault().OfertaDesde.Year==1900 ? null:  qArticulos.FirstOrDefault().OfertaDesde.ToString("dd-MM-yyyy"),
                        OfertaHasta= qArticulos.FirstOrDefault().OfertaHasta.Year == 1900 ? null : qArticulos.FirstOrDefault().OfertaHasta.ToString("dd-MM-yyyy")
                    };
                    mResp.Exito = true;
                    mResp.Objeto = ArticuloResp;
                }
                return mResp;
            }
            catch (Exception ex)
            {
                mResp.Exito = false;
                mResp.Mensaje = "Problema al obtener las existencias del artículo " + CodArticulo + " para la tienda virtual";
                log.Error("Problema al obtener las existencias del artículo " + CodArticulo + " para la tienda virtual", ex);
            }
            return mResp;
        }
        //verifica las existencias tanto para productos simples como para los combos (set) validando existencia de cada componente 
        public static int ObtenerCantidadDisponibleProducto(string CodArticulo, bool articuloCama)
        {
            //si es cama retorna por defecto 10
            if (articuloCama)
                return 10;

            EXACTUSERPEntities db = new EXACTUSERPEntities();
            int cantidadDisponible = 0;
            int cantidadMinima = ObtenerCantidadMinimaProducto();
            bool TomaBodegaF01 = WebConfigurationManager.AppSettings["InventarioTVTodasLasBodegas"].ToString().Equals("0");
            //verificamos si es SET
            var articuloSet = (from s in db.ARTICULO_ENSAMBLE where s.ARTICULO_PADRE == CodArticulo select s);
            //si es set
            if (articuloSet.Count() > 0) {
                int menorExistencia = 0;
                int noExitencia = 0;
                int iteracion = 0;
                foreach (var i in articuloSet) {
                    var existencias = ((from e in db.EXISTENCIA_BODEGA where e.ARTICULO ==  i.ARTICULO_HIJO 
                                        && ((e.BODEGA == "F01" && TomaBodegaF01) || TomaBodegaF01==false) select e)
                                        .Sum(x => x.CANT_DISPONIBLE));
                    try
                    {
                        var pendientesDespacho = (
                                               from f in db.FACTURA
                                               join fl in db.FACTURA_LINEA on f.FACTURA1 equals fl.FACTURA
                                               where
                                               f.ANULADA == "N"
                                               && fl.CANTIDAD > fl.CANT_DESPACHADA
                                               && fl.ARTICULO == CodArticulo
                                               select fl
                                                ).Sum(x => x.CANTIDAD - x.CANT_DESPACHADA);

                        existencias -= pendientesDespacho;
                    }
                    catch
                    { }
                    //si hay existencias mayores a la cantidad minima y si las existencias son mayores a la cantidad de items requeridos por set  
                    if (existencias >= cantidadMinima && existencias >= i.CANTIDAD) {
                        menorExistencia = (int)(iteracion > 0 ? (menorExistencia > existencias ? existencias : menorExistencia) : existencias);
                    }
                    else {
                        noExitencia++;
                    }
                    iteracion++;
                }
                cantidadDisponible = noExitencia > 0 ? 0: menorExistencia;
                    
            } else {
                var existencias = ((from e in db.EXISTENCIA_BODEGA where e.ARTICULO == CodArticulo 
                                    && ((e.BODEGA == "F01" && TomaBodegaF01) || TomaBodegaF01 == false) select e)
                                    .Sum(x => x.CANT_DISPONIBLE));
                try
                {
                    var pendientesDespacho = (
                                               from f in db.FACTURA
                                               join fl in db.FACTURA_LINEA on f.FACTURA1 equals fl.FACTURA
                                               where
                                               f.ANULADA == "N"
                                               && fl.CANTIDAD > fl.CANT_DESPACHADA
                                               && fl.ARTICULO == CodArticulo
                                               select fl
                                                ).Sum(x => x.CANTIDAD - x.CANT_DESPACHADA);

                    existencias -= pendientesDespacho;
                }
                catch
                {
                    
                }
                cantidadDisponible = (int)(existencias >= cantidadMinima ? existencias : 0);
            }
            return cantidadDisponible;
        }

        //para el pedido

        public static Respuesta Facturar(Pedido ped)
        {
            Respuesta respuesta = new Respuesta();
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            log.Debug("--Grabar Pedido de cliente " + ped.cliente);

            //1) obtener el vendedor (código) por defecto
            //2) obtener un correo y el nombre del vendedor para notificarle (un correo que indique que se realizó una facturación nueva)
            //3)obtener la dirección default del cliente
            //4) obtener los usuarios para notifcar acerca de lpedido (mf_gerentes)
            //iniciar transacción
            //5)  Verificar si está guardado en tabla CLIENTE_VENDEDOR de lo contrario guardarlo
            //6)  formar tabla de regalos (por artículos comprados)
            //7) para cada articulo, revisar en la tabla articulo ensamble si este código pertenece a un kit y agregar a la tabla de artículos
            //12) si no se encuentra en articulos mayoreo, buscarlo en la tabla normal de artículos
            //15) obtener el correlativo del pedido
            //16) Grabar el pedido PEDIDO Y MF_PEDIDO
            //17) Actualizar el correlativo del pedido
            //18) Pedido_linea y MF_Pedido_Linea y mf_pedido_linea_original
            //19) Documentos_cc y correlativo de MF_Recibo
            //20) Actualizar correlativo de recibo y grabar mf_Factura, Factura
            //21) para cada articulo del pedido grabar Factura_linea
            
            //finalizar transacción
            //22) Enviar correo

            
            string mCantidadLetras = string.Empty;
            int nEstablecimiento = 0;
            string cCobrador = string.Empty;
            string codJefe = string.Empty;
            string codSupervisor = string.Empty;
            string NombreSucursal = string.Empty;
            string DireccionSucursal = string.Empty;
            string DeptoSucursal = string.Empty;
            string MuniSucursal = string.Empty;
            string DirCliente = string.Empty;
            string NombreCliente = string.Empty;
            string DeptoCliente = string.Empty;
            string MuniCliente = string.Empty;
            string cCliente = string.Empty;
            string mDireccionEntrega = (from d in db.DIRECC_EMBARQUE where d.CLIENTE == ped.cliente.codigo && d.DIRECCION == "ND" select d).First().DESCRIPCION;
            decimal mIVA = ObtenerIVA();
            decimal mTotalFacturar = 0;
            decimal mUnidades = 0;
            decimal mMontoTotal = 0;
            string  mConsecutivoFEL = string.Empty;
            #region "Configuración (A)"
            var qConfiguracion = (from conf in db.MF_Catalogo where conf.CODIGO_TABLA == Const.CATALOGO.CATALOGO_FACTURACION_EN_LINEA select conf);
            string mCodVendedor = qConfiguracion.Where(x => x.CODIGO_CAT.Equals("VENDEDOR")).FirstOrDefault().NOMBRE_CAT;
            string mValidarExistencias = qConfiguracion.Where(x => x.CODIGO_CAT.Equals("VALIDAR_EXISTENCIAS_FACTURAR")).FirstOrDefault().NOMBRE_CAT;
            int DiasEntrega = int.Parse(qConfiguracion.Where(x => x.CODIGO_CAT.Equals("DIAS_ENTREGA_ARTICULOS")).FirstOrDefault().NOMBRE_CAT);
            int ConsecutivoFactIndex = int.Parse(qConfiguracion.Where(x => x.CODIGO_CAT.Equals("POSICION_CONSECUTIVO_FACTURA")).FirstOrDefault().NOMBRE_CAT);
            string mMensajeErrorGral = qConfiguracion.Where(x => x.CODIGO_CAT.Equals("MENSAJE_ERROR_GENERAL")).FirstOrDefault().NOMBRE_CAT;
            string[] mFinancieras = qConfiguracion.Where(x => x.CODIGO_CAT.Equals("FINANCIERA")).FirstOrDefault().NOMBRE_CAT.Split(',');

            List<string> lstAdmonNotificar = qConfiguracion.Where(x => x.CODIGO_CAT.Equals("NOTIFICAR_ADMON")).FirstOrDefault().NOMBRE_CAT.Split('|').ToList();
            List<string> lstBodegaNotificar = qConfiguracion.Where(x => x.CODIGO_CAT.Equals("NOTIFICAR_BODEGA")).FirstOrDefault().NOMBRE_CAT.Split('|').ToList();
            string mRemitenteNotificacion = qConfiguracion.Where(x => x.CODIGO_CAT.Equals("REMITENTE")).FirstOrDefault().NOMBRE_CAT;
            decimal mTipoCambio = (from tc in db.TIPO_CAMBIO_HIST where tc.TIPO_CAMBIO == "REFR" orderby tc.FECHA descending select tc).Take(1).First().MONTO;
            //****
            //variable para determinar si FEL está habilitado
            //****
            bool blnFEL = (from conf in db.MF_Catalogo where conf.CODIGO_TABLA == 11 && conf.CODIGO_CAT.Equals(MF_Clases.Utils.CONSTANTES.CATALOGOS.MODULO_FEL) select conf).FirstOrDefault().NOMBRE_CAT == "S";
            var Vendedor = string.Empty;
            var DirVendedor = string.Empty;
            string cResolucion = string.Empty;
            string cFolioIni = string.Empty;
            string cFolioFin = string.Empty;
            DateTime dtFechaResolucion = DateTime.Now;

            var qRes = (from c in db.CONSECUTIVO_FA join r in db.RESOLUCION_DOC_ELECTRONICO on c.RESOLUCION equals r.RESOLUCION where c.CODIGO_CONSECUTIVO == "FVIRCRE" select r);
            if (qRes.Count() > 0)
            {
                cResolucion = qRes.FirstOrDefault().RESOLUCION;
                cFolioIni = qRes.FirstOrDefault().FOLIO_INICIAL.ToString();
                cFolioFin = qRes.FirstOrDefault().FOLIO_FINAL.ToString();
                dtFechaResolucion = qRes.FirstOrDefault().FECHA_RESOLUCION ?? DateTime.Now;
                log.Debug(string.Format("Resolución {0}, cfolioIni {1}, cFolioFin {2}, FechaResolucion: {3}", cResolucion, cFolioIni, cFolioFin, dtFechaResolucion));
            }


            var qSucursal = (from x in db.MF_GFaceConfigura select x);
            if (qSucursal.Count() > 0)
            {
                Vendedor = qSucursal.FirstOrDefault().SUCURSAL;
                DirVendedor = qSucursal.FirstOrDefault().DIRECCION;
            }
            #endregion
            respuesta = Validaciones(ped, mValidarExistencias.Equals("S"),mFinancieras.ToList());
            if (respuesta.Exito)
            {

                try
                {
                    //prorratear descuento
                    if (ped.DescuentoGeneral > 0 && ped.articulos.Sum(x => x.Descuento)==0)
                    {
                        var porDesc = ped.TotalPedido > 0 ? ped.DescuentoGeneral / ped.TotalPedido : 0;
                        ped.articulos.ForEach(x => {
                            x.Descuento = Math.Round(x.Precio*x.cantidad * porDesc,2,MidpointRounding.AwayFromZero);
                        });
                    }
                    
                    #region "Configuración (B)"
                    
                    string mCategoriaCliente = Const.TIENDA_VIRTUAL.COBRADOR_FACTURACION;
                    string mCuentaContable = (from c in db.CATEGORIA_CLIENTE where c.CATEGORIA_CLIENTE1 == mCategoriaCliente.Replace("F", "0") select c).First().CTA_VENTAS;
                    string mCentroCosto = (from c in db.CATEGORIA_CLIENTE where c.CATEGORIA_CLIENTE1 == mCategoriaCliente.Replace("F", "0") select c).First().CTR_VENTAS;
                    string mTipoNivel = (from n in db.MF_NIVEL_PRECIO_COEFICIENTE where n.NIVEL_PRECIO == ped.cliente.FormaDePago select n).First().TIPO;
                    string mCodigoZona= "0101";

                    var qConsultaEmisor = (from mf_g in db.MF_GFaceConfigura select mf_g);
                    var qConsultaTests = (from cs in db.MF_Catalogo where cs.CODIGO_TABLA == CONSTANTES.CATALOGOS.GUATEFACTURAS_FEL && cs.CODIGO_CAT == "PRODUCCION_FEL" select cs).FirstOrDefault();
                    bool blTests = qConsultaTests.NOMBRE_CAT.Equals("N");
                    try
                    {
                        mCodigoZona = (from z in db.ZONA where z.NOMBRE.ToUpper() == ped.cliente.Departamento.ToUpper() + ", " + ped.cliente.Municipio.ToUpper() select z).First().ZONA1;
                    }
                    catch
                    { }
                    string mDireccionEmbarque = string.Empty; //(from d in db.DIRECC_EMBARQUE where d.CLIENTE == ped.cliente.codigo select d).First().DESCRIPCION;

                    string mDescripcionTipoVenta = (from tv in db.MF_TipoVenta where tv.TipoVenta == Const.TIENDA_VIRTUAL.TIPO_VENTA_DEFAULT select tv).First().Descripcion;
                    //  Verificar si está guardado en tabla CLIENTE_VENDEDOR de lo contrario guardarlo
                    int mExisteRelacionVendedor = (from cv in db.CLIENTE_VENDEDOR where cv.CLIENTE == ped.cliente.codigo && cv.VENDEDOR == mCodVendedor select cv).Count();
                    //**-*-*-*-*-*-
                    //referencia fel
                    //*-*-*--*-*-*-*
                    int mReferencia = Convert.ToInt32((from c in db.CONSECUTIVO_FA where c.CODIGO_CONSECUTIVO == "FEL" select c).First().VALOR_CONSECUTIVO) + 1;
                    #endregion

                    #region "Bases y patas"
                    List<DtoLoteBases> lstBases = new List<DtoLoteBases>();
                    List<DtoLotePatas> lstPatas = new List<DtoLotePatas>();
                    DataTable dtPatasxBase = new DataTable();
                    dtPatasxBase.Columns.Add(new DataColumn("Base", typeof(System.String)));
                    dtPatasxBase.Columns.Add(new DataColumn("TienePatas", typeof(System.Boolean)));
                    #endregion
                    //---------Obtener descripción para mostrar en la facturación de las patas (si se llegara a mostrar)

                    string mDescripcion36 = (from a in db.ARTICULO where a.ARTICULO1 == "091036-000" select a).First().DESCRIPCION;
                    string mDescripcion37 = (from a in db.ARTICULO where a.ARTICULO1 == "091037-000" select a).First().DESCRIPCION;
                    string mDescripcion38 = (from a in db.ARTICULO where a.ARTICULO1 == "091038-000" select a).First().DESCRIPCION;
                    string mDescripcion39 = (from a in db.ARTICULO where a.ARTICULO1 == "091039-000" select a).First().DESCRIPCION;
                    var Financiera = (from n in db.MF_NIVEL_PRECIO_COEFICIENTE where n.NIVEL_PADRE == ped.cliente.FormaDePago select n).FirstOrDefault().FINANCIERA;
                    //15) obtener el correlativo del pedido, factura, recibo, etc
                    #region "Correlativos"
                    string mConsecutivoPed = (from c in db.CONSECUTIVO_FA where c.CODIGO_CONSECUTIVO == "PEDIDO" select c).First().VALOR_CONSECUTIVO;
                    var qUpdateConsecutivo = from c in db.CONSECUTIVO_FA where c.CODIGO_CONSECUTIVO == "PEDIDO" select c;
                    var qEnviarMailGuatefacturas = db.MF_Catalogo.Where(x => x.CODIGO_TABLA.Equals(11) && x.CODIGO_CAT.Equals("ENVIAR_CORREO_FEL_FIESTA"));
                    string EnviarMailGuateFacturas = qEnviarMailGuatefacturas.FirstOrDefault().NOMBRE_CAT;


                    int mNumeroPedido = int.Parse(mConsecutivoPed.Replace("P", ""));
                    mNumeroPedido = mNumeroPedido + 1;
                    mConsecutivoPed = Convert.ToString(mNumeroPedido);
                    mConsecutivoPed = "P" + mConsecutivoPed.ToString().PadLeft(8, '0');

                    //conta facturas
                    string[] mConsecutivosCnt = (from c in db.COBRADOR where c.COBRADOR1 == Const.TIENDA_VIRTUAL.COBRADOR_FACTURACION select c).First().E_MAIL.ToString().Split(new string[] { "," }, StringSplitOptions.None);
                    string mConsecutivoCnt = mConsecutivosCnt[ConsecutivoFactIndex];
                    string TipoVenta = (from nv in db.MF_NIVEL_PRECIO_COEFICIENTE.Where(nv => nv.FINANCIERA == Financiera && nv.NIVEL_PRECIO == ped.cliente.FormaDePago) select nv).FirstOrDefault().TIPO;

                    string mFactura = blnFEL? string.Empty : ObtenerNumeroFactura(Const.TIENDA_VIRTUAL.COBRADOR_FACTURACION, TipoVenta);
                                       
                    //recibo
                    string mRecibo = "";
                    string mConsecutivoRec = string.Format("REC{0}-PV", Const.TIENDA_VIRTUAL.COBRADOR_FACTURACION);
                    int mReciboInt = int.Parse((from c in db.CONSECUTIVO where c.CONSECUTIVO1 == mConsecutivoRec select c).First().ULTIMO_VALOR.Replace(string.Format("PV{0}-", Const.TIENDA_VIRTUAL.COBRADOR_FACTURACION), "")) + 1;
                    string mCeros = string.Empty;
                    UtilitariosGuateFacturas utils = new UtilitariosGuateFacturas();
                    Resultado resultadoFirma = new Resultado();
                    #endregion
                    //lista para generar los artículos de un kit previo a agregarlos a la estructura principal del pedido
                    List<Articulo> Hijos = new List<Articulo>();
                    
                    //Lista para el detalle de articulos a enviar al gface para la firma electrónica
                    List<DetalleDoc> detalles = new List<DetalleDoc>();
                    Doc docSolicitud = new Doc();
                    mMontoTotal =0;
                    //para validar el total en el detalle
                    decimal nTotalDet = 0;
                    decimal nTotalImpuesto = 0;
                    foreach (Articulo articulo in ped.articulos)
                    {
                        string mTipo = string.Empty;
                        string mClasif3 = string.Empty;
                        string mDescripion = string.Empty;

                        try
                        {
                            var qArticulo = (from a in db.ARTICULO where a.ARTICULO1 == articulo.CodigoArticulo 
                                             select new { Tipo = a.TIPO, Clasficacion3 = a.CLASIFICACION_3, Descripcion = a.DESCRIPCION }).First();
                            if (qArticulo != null)
                            {
                                mTipo = qArticulo.Tipo;
                                articulo.TipoLinea = mTipo;
                                mClasif3 = qArticulo.Clasficacion3 != null ? qArticulo.Clasficacion3.ToString() : "";
                                mDescripion = qArticulo.Descripcion.ToString();
                            }
                        }catch
                        { throw new ArgumentException("El articulo no existe " + articulo.CodigoArticulo); }

                        mTotalFacturar += Math.Round(articulo.Precio * articulo.cantidad,2,MidpointRounding.AwayFromZero);
                        mUnidades += articulo.cantidad;

                        //tratamiento de las bases
                        if (mTipo == "T" && mClasif3.Equals("142")) //bases sueltas solamente
                        {
                            lstBases.Add(new DtoLoteBases { Cantidad = articulo.cantidad, Base = new DtoBaseCama { codArticulo = articulo.CodigoArticulo, Descripcion = mDescripion } });
                        }
                        #region "Descomposición del kit (si hubiese)"
                        //para cada articulo se verifica si es de tipo K que significa que es un kit
                        if (mTipo == "K")
                        {
                            int LineaIndex = int.Parse((ped.articulos.Max(x => x.Linea) + Hijos.Count()).ToString());
                            //COLOCÁNDOLE EL TIPO DE ARTÍCULO 
                            articulo.TipoLinea = Const.TIENDA_VIRTUAL.TIPO_LINEA_ARTICULO_SET;

                            var qComponentes = from ae in db.ARTICULO_ENSAMBLE 
                                               
                                               where ae.ARTICULO_PADRE == articulo.CodigoArticulo.ToUpper().Replace("M","") orderby ae.ARTICULO_HIJO select ae;
                            foreach (var item in qComponentes)
                            {
                                //marcar el articulo padre como un kit
                                string mDescripcion = "";
                                decimal  mDescuento = 0; 
                                double mPrecioBase = 0;
                                string mTieneRegalo = "N"; 
                                string mRegalo = ""; 
                                Int32 mCantidadRegalo = 1;

                                switch (item.ARTICULO_HIJO)
                                {
                                    case "091036-000":
                                        mDescripcion = mDescripcion36;
                                        break;
                                    case "091037-000":
                                        mDescripcion = mDescripcion37;
                                        break;
                                    case "091038-000":
                                        mDescripcion = mDescripcion38; break;
                                    case "091039-000":
                                        mDescripcion = mDescripcion39;
                                        break;
                                    default:

                                        //busca información en mayoreo primero
                                        var qInfo = from a in db.MF_ArticulosMayoreo where a.ARTICULO == item.ARTICULO_HIJO && a.CLIENTE == ped.cliente.codigo select a;
                                        foreach (var itemInfo in qInfo)
                                        {
                                            double mDescuentoKit = (double)(from a in db.MF_ArticulosMayoreo where a.ARTICULO == articulo.CodigoArticulo && a.CLIENTE == ped.cliente.codigo select a).First().PORCENTAJE_DESCUENTO / 100;

                                            mTieneRegalo = itemInfo.TIENE_REGALO.ToString();
                                            mRegalo = itemInfo.REGALO;
                                            mCantidadRegalo = itemInfo.CANTIDAD_REGALO;
                                            mDescripcion = itemInfo.DESCRIPCION;
                                            mPrecioBase =  0;
                                            mDescuento = decimal.Parse(itemInfo.PORCENTAJE_DESCUENTO.ToString().Equals("") ? "0" : itemInfo.PORCENTAJE_DESCUENTO.ToString());
                                        }
                                        

                                        if (qInfo.Count() == 0)
                                        {
                                            var qInfoArticulo = from a in db.ARTICULO where a.ARTICULO1 == item.ARTICULO_HIJO select a;
                                            foreach (var itemInfo in qInfoArticulo)
                                            {
                                                mDescripcion = itemInfo.DESCRIPCION;
                                                mPrecioBase =  0;
                                            }
                                        }
                                        break;
                                }

                                int mCantidadPedido = articulo.cantidad * Convert.ToInt32(item.CANTIDAD);
                                LineaIndex++;

                                Articulo hijo = new Articulo
                                {
                                    CodigoArticulo = item.ARTICULO_HIJO,
                                    Descripcion = mDescripcion,
                                    cantidad = mCantidadPedido,
                                    CodigoPadre = articulo.CodigoArticulo,
                                    Precio = 0 ,
                                    Linea = LineaIndex,
                                    TipoLinea = Const.TIENDA_VIRTUAL.TIPO_LINEA_ARTICULO_COMPONENTE_SET
                                };

                                //agregar a la estructura para la facturación en línea
                                DetalleDoc det1 = new DetalleDoc
                                {
                                    Producto = item.ARTICULO_HIJO,
                                    Descripcion = mDescripcion,
                                    Cantidad = articulo.cantidad,
                                    Precio = 0,
                                    ImpBruto =0
                                    ,
                                    ImpNeto = 0,
                                    ImpIva = 0,
                                    ImpTotal =0,
                                    TipoVentaDet = CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES,
                                    PorcDesc = 0,
                                    ImpDescuento = 0,
                                    ImpExento = 0
                                };

                                nTotalDet += Math.Round((decimal)(mPrecioBase) * item.CANTIDAD, 2, MidpointRounding.AwayFromZero);
                                nTotalImpuesto += mPrecioBase>0? Math.Round((articulo.Precio - articulo.Precio / mIVA)*articulo.cantidad, 2, MidpointRounding.AwayFromZero):0;
                                Hijos.Add(hijo);
                                detalles.Add(det1);
                               
                            }

                        }
                        
                        
                            //solo para el detalle de FEL porque los artículos ya se encuentran en ped.articulos
                            DetalleDoc det = new DetalleDoc
                            {
                                Producto = articulo.CodigoArticulo,
                                Descripcion =articulo.Descripcion,
                                Cantidad = articulo.cantidad,
                                Precio = articulo.Precio,
                                ImpBruto = Math.Round(articulo.Precio * articulo.cantidad, 2, MidpointRounding.AwayFromZero)
                                    ,
                                ImpNeto = Math.Round((articulo.Precio / mIVA)*articulo.cantidad, 2, MidpointRounding.AwayFromZero),
                                ImpIva = Math.Round((articulo.Precio - articulo.Precio / mIVA)*articulo.cantidad, 2, MidpointRounding.AwayFromZero),
                                ImpTotal = Math.Round(articulo.Precio * articulo.cantidad, 2, MidpointRounding.AwayFromZero),
                                TipoVentaDet = CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES,
                                PorcDesc = (articulo.Precio>0 && articulo.cantidad>0) ? 
                                            articulo.Descuento/(articulo.Precio*articulo.cantidad):0,
                                ImpDescuento = articulo.Descuento,
                                ImpExento = 0
                            };

                        //if (Math.Round(det.ImpNeto + det.ImpIva, 2, MidpointRounding.AwayFromZero) != det.ImpTotal)
                        //    det.ImpNeto += det.ImpTotal - Math.Round(det.ImpNeto + det.ImpIva, 2, MidpointRounding.AwayFromZero);
                        //if (Math.Round((articulo.Precio - articulo.Precio / mIVA) * articulo.cantidad, 2, MidpointRounding.AwayFromZero) != det.ImpIva)
                        //    det.ImpIva = det.ImpIva + ((Math.Round((articulo.Precio - articulo.Precio / mIVA) * articulo.cantidad, 2, MidpointRounding.AwayFromZero) - det.ImpIva));

                        //    nTotalDet += Math.Round(det.ImpNeto + det.ImpIva, 2, MidpointRounding.AwayFromZero);
                        //    nTotalImpuesto += Math.Round((articulo.Precio - articulo.Precio / mIVA)*articulo.cantidad, 2, MidpointRounding.AwayFromZero);
                            detalles.Add(det);
                        

                        #endregion

                    }

                   

                    DALDescuentos descuentos = new DALDescuentos();
                    docSolicitud = descuentos.ValidarSumatoriasFel(ped);

                    docSolicitud.NITEmisor = int.Parse(qConsultaEmisor.FirstOrDefault().NIT).ToString();
                    docSolicitud.TipoDocumento = CONSTANTES.GUATEFAC.DOCUMENTO.FACTURA;
                    docSolicitud.Establecimiento = 1;
                    docSolicitud.IdMaquina = MAQUINA.FACTURACION_TIENDA_VIRTUAL_CRED;
                    docSolicitud.SerieAdmin = "FVIRCRE";

                    //Receptor
                    docSolicitud.NITReceptor = ped.cliente.NIT;
                    docSolicitud.Nombre = ped.cliente.Nombres + " " + ped.cliente.Apellidos;
                    docSolicitud.Direccion = ped.cliente.Calle_avenida + " " + ped.cliente.Casa + " " + ped.cliente.Colonia + " " + ped.cliente.Apartamento + " " + ped.cliente.Municipio + "; " + ped.cliente.Departamento;
                    //InfoDoc
                    docSolicitud.TipoVenta = CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES;
                    docSolicitud.DestinoVenta = CONSTANTES.GUATEFAC.DESTINO_VENTA.GT;
                    docSolicitud.Fecha = DateTime.Today;
                    docSolicitud.Moneda = CONSTANTES.GUATEFAC.MONEDA.GTQ;
                    docSolicitud.Tasa = CONSTANTES.GUATEFAC.TASA.GTQ;
                    //Referencia FEL es el número del pedido
                    mConsecutivoFEL = mConsecutivoPed;
                    docSolicitud.Referencia = mConsecutivoFEL;
                    //adicionales
                    docSolicitud.Enviar = EnviarMailGuateFacturas;
                    docSolicitud.Email = ped.cliente.Email;
                   




                    
                    //agregando los artículos hijos a la estructura principal
                   
                    List<OrderDataDetail> articulosFacturacion = new List<OrderDataDetail>();

                    docSolicitud.Detalle.ForEach(
                        x => {
                            articulosFacturacion.Add(
                                new OrderDataDetail
                                {
                                    line = x.Linea,
                                    item = x.Producto,
                                    orderQty = (int)x.Cantidad,
                                    unitPrice = x.Precio,
                                    discount = x.ImpDescuento,
                                    percentajeDiscount = x.PorcDesc,
                                    totalPrice = x.ImpTotal,
                                    lineType = "N"
                                }
                                ) ;
                            }
                        );
                    //agregando los articulos del set (con valores 0, solo es para el inventario
                    foreach (Articulo articuloHijo in Hijos)
                    {
                        ped.articulos.Add(articuloHijo);

                        articulosFacturacion.Add(new OrderDataDetail
                        {
                            line = articulosFacturacion.Max(x => x.line) + 1,
                            item=articuloHijo.CodigoArticulo,
                            orderQty=articuloHijo.cantidad,
                            unitPrice=articuloHijo.Precio,
                            lineType=articuloHijo.TipoLinea,
                            discount=0,
                            percentajeDiscount=0,
                            totalPrice=0
                             
                        }) ;
                    }
                    OrderData  insumosFacturacion = new OrderData();
                    insumosFacturacion.details = articulosFacturacion;
                    insumosFacturacion.store = ped.Tienda;
                    insumosFacturacion.total_invoice = docSolicitud.Total;



                    string urlApi = "";
                    switch (General.Ambiente)
                    {
                        case "DES": urlApi = WebConfigurationManager.AppSettings["UrlNetunimAPIDES"].ToString(); break;
                        case "PRU": urlApi = WebConfigurationManager.AppSettings["UrlNetunimAPIPRU"].ToString(); break;
                        case "PRO": urlApi = WebConfigurationManager.AppSettings["UrlNetunimAPIPRU"].ToString(); break;
                    }
                    Api api = new Api(urlApi);                    
                    Respuesta resp = new Respuesta();
                    resp.Exito = true;


                    //Dictionary<string, string> header = new Dictionary<string, string>();
                    //header.Add("AppKey", General.AppkeyAutenticacion.ToString());
                    //header.Add("Token", HttpContext.Current.Request.Cookies.Get(General.CookieName)?.Value);
                    string json = JsonConvert.SerializeObject(insumosFacturacion);
                    var response=api.Process(RestSharp.Method.POST, "Discounts/BillingDiscount",insumosFacturacion,null);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var DataResult = JsonConvert.DeserializeObject<DescuentosFacturacionResult>(response.Content);
                        if (DataResult != null)
                        {

                            using (TransactionScope transactionScope = new TransactionScope())
                            {
                                try
                                {
                                    log.Debug("Iniciando proceso para generar la factura electrónica " + ped.cliente.codigo);
                                    var blnContingencia = false;

                                    var qCliente0 = (from c in db.CLIENTE where c.CLIENTE1.Equals(ped.cliente.codigo) select c);
                                    foreach (var item0 in qCliente0)
                                        item0.E_MAIL = ped.cliente.Email;

                                    #region "FEL"
                                    if (blnFEL)
                                    {


                                        if (detalles.Count > 0)
                                        {

                                            //2) firmar
                                            try
                                            {
                                                //1er intento
                                                resultadoFirma = utils.Servicio(SERVICIOS.GENERAR_DOCUMENTO, docSolicitud);
                                                if (resultadoFirma.NumeroAutorizacion == null && resultadoFirma.ErrorComunicacion.Equals("S"))
                                                    //segundo intento
                                                    resultadoFirma = utils.Servicio(SERVICIOS.GENERAR_DOCUMENTO, docSolicitud);

                                                if (resultadoFirma.NumeroAutorizacion == null && resultadoFirma.ErrorComunicacion.Equals("S"))
                                                    // tercer intento
                                                    resultadoFirma = utils.Servicio(SERVICIOS.GENERAR_DOCUMENTO, docSolicitud);

                                                if (resultadoFirma.NumeroAutorizacion == null && resultadoFirma.ErrorComunicacion.Equals("S"))
                                                {
                                                    DAL.DALFel fel = new DAL.DALFel();
                                                    var respContingencia = fel.UtilizarNumeroContingenciaTienda("F01", DateTime.Now);
                                                    if (respContingencia.Exito)
                                                    {
                                                        mFactura = respContingencia.Objeto.ToString();
                                                        blnContingencia = true;
                                                    }
                                                }
                                                else if (resultadoFirma.Error == string.Empty && resultadoFirma.NumeroAutorizacion != null)
                                                {
                                                    log.Debug("resultado firma: " + resultadoFirma.Xml + " autorización: [" + resultadoFirma.NumeroAutorizacion + "]");
                                                    //asignando la factura electrónica
                                                    mFactura = resultadoFirma.FacturaElectronica;

                                                }
                                                else
                                                {

                                                    log.Error("Resultado Firma factura electrónica desde la tienda virtual: " + resultadoFirma.Error.ToString());

                                                }

                                            }
                                            catch (Exception exi)
                                            {
                                                log.Error("Error al firmar la factura electrónicamente, " + exi.Message);
                                            }

                                            //------------------------------------------------
                                            //------------------------------------------------
                                        }

                                    }
                                    #endregion

                                    if ((resultadoFirma.NumeroAutorizacion != null && resultadoFirma.NumeroAutorizacion != string.Empty) || blnContingencia || blnFEL == false)
                                    {
                                        log.Debug("Iniciando proceso para guardar el pedido del cliente " + ped.cliente.codigo);

                                        #region "cantidad a letras"
                                        try
                                        {
                                            int mTotal = 0;
                                            if (int.TryParse(docSolicitud.Total.ToString(), out mTotal))
                                                mCantidadLetras = mTotal.ToWords(System.Globalization.CultureInfo.CreateSpecificCulture("es-GT")).ToUpper();
                                            else
                                            {
                                                mTotal = int.Parse(docSolicitud.Total.ToString().Substring(0, docSolicitud.Total.ToString().IndexOf('.')));
                                                Decimal nDecimal = (Decimal)(docSolicitud.Total - mTotal);
                                                int ValorCentavos = (int)(nDecimal * 100);
                                                mCantidadLetras = mTotal.ToWords(System.Globalization.CultureInfo.CreateSpecificCulture("es-GT")).ToUpper() + " CON " + ValorCentavos.ToString() + "/100";

                                            }
                                            log.Debug("--->" + mCantidadLetras);
                                        }
                                        catch (Exception ex)
                                        {
                                            log.Error("No se pudo convertir la cantidad del Total " + docSolicitud.Total + ", a letras " + CatchClass.ExMessage(ex, "DALFel", "ServicioFirmaElectronica()"));
                                            string err = ex.Message;
                                        }
                                        log.Debug("Grabando en mf_gface " + resultadoFirma.FacturaElectronica);
                                        #endregion

                                        #region "Información cobrador y cliente"
                                        //si no hay relación entre el cliente y el vendedor, guardar en la tabla
                                        if (mExisteRelacionVendedor == 0)
                                        {
                                            CLIENTE_VENDEDOR iCLIENTE_VENDEDOR = new CLIENTE_VENDEDOR
                                            {
                                                CLIENTE = ped.cliente.codigo,
                                                VENDEDOR = mCodVendedor,
                                                NoteExistsFlag = 0,
                                                RowPointer = Guid.NewGuid(),
                                                RecordDate = DateTime.Now,
                                                CreatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                                UpdatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                                CreateDate = DateTime.Now
                                            };

                                            db.CLIENTE_VENDEDOR.AddObject(iCLIENTE_VENDEDOR);
                                        }

                                        //obtener el cobrador de la factura (1) o de la nota de crédito|débito (2)

                                        var qCobrador = (from c in db.MF_Cobrador where c.COBRADOR == ped.Tienda
                                                         select c);

                                        if (qCobrador.Count() > 0)
                                        {
                                            cCobrador = qCobrador.FirstOrDefault().COBRADOR;
                                            codJefe = qCobrador.FirstOrDefault().CODIGO_JEFE;
                                            codSupervisor = qCobrador.FirstOrDefault().CODIGO_SUPERVISOR;
                                            NombreSucursal = qCobrador.FirstOrDefault().NOMBRE;
                                            DireccionSucursal = qCobrador.FirstOrDefault().DIRECCION;
                                            DeptoSucursal = qCobrador.FirstOrDefault().DEPARTAMENTO;
                                            MuniSucursal = qCobrador.FirstOrDefault().MUNICIPIO;
                                            nEstablecimiento = qCobrador.FirstOrDefault().ESTABLECIMIENTO ?? 0;

                                        }
                                        //obtener la información del cliente por medio de la factura (en contingencia) o de la nota de crédito|débito
                                        var qCliente = (from c in db.MF_Cliente

                                                        where c.CLIENTE.Equals(ped.cliente.codigo)
                                                        select c
                                                        );

                                        if (qCliente.Count() > 0)
                                        {
                                            cCliente = qCliente.FirstOrDefault().CLIENTE;
                                            NombreCliente = qCliente.FirstOrDefault().PRIMER_NOMBRE.ToUpper();
                                            DirCliente = string.Empty;
                                            DeptoCliente = qCliente.FirstOrDefault().DEPARTAMENTO_FACTURA;
                                            MuniCliente = qCliente.FirstOrDefault().MUNICIPIO_FACTURA;
                                        }
                                        #endregion

                                        #region "mf_gface"
                                        MF_GFace gFace = new MF_GFace
                                        {
                                            ANIO = ped.FechaPedido.Year,
                                            MES = ped.FechaPedido.Month,
                                            COBRADOR = ped.Tienda,
                                            DOCUMENTO = mFactura,
                                            NIT_VENDEDOR = docSolicitud.NITEmisor.PadLeft(12, '0'),
                                            RAZON_SOCIAL = Vendedor,
                                            TIPO = "FC",
                                            FECHA = docSolicitud.Fecha.ToString("yyyy-MM-ddT00:00:00"),
                                            SERIE = resultadoFirma.Serie == null ? string.Empty : resultadoFirma.Serie,
                                            NUMERO_DOCUMENTO = resultadoFirma.Preimpreso == null ? string.Empty : resultadoFirma.Preimpreso
                                   ,
                                            NOMBRE_CLIENTE = docSolicitud.Nombre,
                                            NIT = docSolicitud.NITReceptor.PadLeft(12, '0'),
                                            DIRECCION = DirCliente,
                                            DEPARTAMENTO = DeptoCliente,
                                            MUNICIPIO = MuniCliente,
                                            DESCUENTO = docSolicitud.Descuento,
                                            SUBTOTAL = docSolicitud.Bruto,
                                            IVA = docSolicitud.Iva,
                                            TOTAL = docSolicitud.Total,
                                            ANULADA = "N",
                                            FECHA_RESOLUCION = dtFechaResolucion.ToString("yyyy-MM-dd"),
                                            NUMERO_RESOLUCION = cResolucion,
                                            FOLIO_FINAL = cFolioFin,
                                            FOLIO_INICIAL = cFolioIni,
                                            SUCURSAL = nEstablecimiento.ToString(),
                                            NOMBRE_SUCURSAL = nEstablecimiento > 1 ? "MUEBLES FIESTA" : "MULTIPRODUCTOS",
                                            DIRECCION_SUCURSAL = DireccionSucursal.ToUpper(),
                                            DEPARTAMENTO_SUCURSAL = DeptoSucursal.ToUpper(),
                                            MUNICIPIO_SUCURSAL = MuniSucursal.ToUpper(),
                                            NUMEROS_LETRAS = mCantidadLetras,
                                            TIPO_ACTIVO = GFACE.TIPO_ACTIVO,
                                            MONEDA = docSolicitud.Moneda == MONEDA.GTQ ? "GTQ" : "USD",
                                            TASA_CAMBIO = docSolicitud.Tasa,
                                            CLIENTE = cCliente,
                                            BIEN_SERVICIO = docSolicitud.TipoVenta.Equals("B") ? "BIEN" : "SERVICIO",
                                            XML_DOCUMENTO = resultadoFirma.XmlDocumento == null ? string.Empty : resultadoFirma.XmlDocumento,
                                            XML_FIRMADO = resultadoFirma.Xml == null ? string.Empty : resultadoFirma.Xml,
                                            XML_ERROR = resultadoFirma.XmlError == null ? string.Empty : resultadoFirma.XmlError,
                                            FIRMADO = "S",
                                            FIRMA = resultadoFirma.NumeroAutorizacion == null ? string.Empty : resultadoFirma.NumeroAutorizacion,
                                            FECHA_DOCUMENTO = docSolicitud.Fecha,
                                            DIRECCION_VENDEDOR = DirVendedor,
                                            USUARIO = "VIRTUAL",
                                            FECHA_CREADO = DateTime.Now.Date,
                                            FECHA_HORA_CREADO = DateTime.Now

                                        };
                                        db.MF_GFace.AddObject(gFace);

                                        foreach (var item in docSolicitud.Detalle)
                                        {
                                            MF_GFaceDetalle det = new MF_GFaceDetalle
                                            {
                                                ANIO = DateTime.Now.Year,
                                                MES = DateTime.Now.Month,
                                                COBRADOR = ped.Tienda,//<-------COLOCAMOS EL CÓDIGO DE LA TIENDA
                                                DOCUMENTO = mFactura,
                                                FECHA = docSolicitud.Fecha.ToString("dd/MM/yyyy"),
                                                TIPO = string.Empty,
                                                SERIE = resultadoFirma.Serie == null ? string.Empty : resultadoFirma.Serie,
                                                NUMERO_DOCUMENTO = resultadoFirma.FacturaElectronica == null ? string.Empty : resultadoFirma.FacturaElectronica,
                                                DESCRIPCION = item.Descripcion,
                                                CANTIDAD = (int)item.Cantidad,
                                                PRECIO_UNITARIO = item.Precio,
                                                MONTO_NETO = item.ImpNeto,
                                                DESCUENTO = item.ImpDescuento,
                                                PORCENTAJE_DESCUENTO = item.PorcDesc,
                                                SUBTOTAL = item.ImpBruto,
                                                IVA = item.ImpIva,
                                                TOTAL = item.ImpTotal,
                                                FECHA_DOCUMENTO = docSolicitud.Fecha,
                                                USUARIO = "VIRTUAL",
                                                FECHA_CREADO = DateTime.Now.Date,
                                                FECHA_HORA_CREADO = DateTime.Now

                                            };

                                            db.MF_GFaceDetalle.AddObject(det);


                                        }
                                        #endregion

                                        foreach (var articulo in DataResult.orderDetailData /*docSolicitud.Detalle*/)
                                        {
                                            //if (articulo.CodigoPadre == null)
                                            {
                                                PEDIDO_LINEA iPedidoLinea = new PEDIDO_LINEA()
                                                {
                                                    PEDIDO = mConsecutivoPed,
                                                    PEDIDO_LINEA1 = short.Parse((articulo.line - 1 >= 0 ? articulo.line - 1 : articulo.line).ToString()),
                                                    BODEGA = Const.TIENDA_VIRTUAL.COBRADOR_FACTURACION,
                                                    LOCALIZACION = Const.TIENDA_VIRTUAL.LOCALIZACION_DEFAULT,
                                                    ARTICULO = articulo.item,
                                                    ESTADO = "F",
                                                    FECHA_ENTREGA = DateTime.Now.Date,
                                                    LINEA_USUARIO = short.Parse((articulo.line - 2).ToString()),
                                                    PRECIO_UNITARIO = articulo.basePrice,//articulo.Precio / mIVA,
                                                    CANTIDAD_PEDIDA = articulo.quantity,
                                                    CANTIDAD_A_FACTURA = 0,
                                                    CANTIDAD_FACTURADA = articulo.quantity,
                                                    CANTIDAD_RESERVADA = 0,
                                                    CANTIDAD_BONIFICAD = 0,
                                                    CANTIDAD_CANCELADA = 0,
                                                    TIPO_DESCUENTO = articulo.discountAmount == 0 ? "P" : "F",
                                                    MONTO_DESCUENTO = articulo.discountAmount,
                                                    PORC_DESCUENTO = articulo.percentajeDiscount,
                                                    DESCRIPCION = db.ARTICULO.Where(x => x.ARTICULO1 == articulo.item).FirstOrDefault().DESCRIPCION.ToUpper(),
                                                    COMENTARIO = "",
                                                    FECHA_PROMETIDA = DateTime.Now,
                                                    NoteExistsFlag = 0,
                                                    RecordDate = DateTime.Now,
                                                    RowPointer = Guid.NewGuid(),
                                                    CreatedBy = string.Format("FA/{0}", ped.Usuario),
                                                    UpdatedBy = string.Format("FA/{0}", ped.Usuario),
                                                    CreateDate = DateTime.Now,
                                                    CENTRO_COSTO = mCentroCosto,
                                                    CUENTA_CONTABLE = mCuentaContable,
                                                    U_ARMADOR = "PENDIENTE ASIGNAR"
                                                };

                                                db.PEDIDO_LINEA.AddObject(iPedidoLinea);

                                                MF_Pedido_Linea iMF_PedidoLinea = new MF_Pedido_Linea()
                                                {
                                                    PEDIDO = mConsecutivoPed,
                                                    PEDIDO_LINEA = short.Parse((articulo.line - 1 >= 0 ? articulo.line - 1 : articulo.line).ToString()),
                                                    ARTICULO = articulo.item,
                                                    REQUISICION_ARMADO = DALValidar.RequiereArmado(articulo.item) == true ? "S" : "N",
                                                    OFERTA = "N",
                                                    TIPOVENTA = Const.TIENDA_VIRTUAL.TIPO_VENTA_DEFAULT,
                                                    FINANCIERA = Financiera,
                                                    NIVEL_PRECIO = ped.cliente.FormaDePago,
                                                    PRECIOORIGINAL = 0,
                                                    PRECIO_UNITARIO = articulo.unitPrice,
                                                    CANTIDAD_PEDIDA = articulo.quantity,
                                                    PRECIO_TOTAL = articulo.totalPrice,//(articulo.unitPrice * articulo.quantity),
                                                    FACTURADO = "S",
                                                    DESPACHADO = "N",
                                                    TIPO_OFERTA = "N",
                                                    BODEGA = Const.TIENDA_VIRTUAL.COBRADOR_FACTURACION,
                                                    LOCALIZACION = Const.TIENDA_VIRTUAL.LOCALIZACION_DEFAULT,
                                                    PRECIO_UNITARIO_FACTURAR = articulo.unitPriceToInvoice,
                                                    PRECIO_TOTAL_FACTURAR = articulo.totaltPriceToInvoice, //Math.Round(articulo.unitPriceToInvoice * articulo.quantity, 2, MidpointRounding.AwayFromZero),
                                                    PRECIO_LISTA = articulo.unitPrice,
                                                    PRECIO_UNITARIO_DEBIO_FACTURAR = articulo.unitPrice,
                                                    PRECIO_BASE_LOCAL = articulo.netBasePrice,//Math.Round(articulo.Precio / mIVA, 5, MidpointRounding.AwayFromZero),
                                                    AUTORIZACION = 0,
                                                    FACTURA = mFactura,
                                                    VALE = 0,
                                                    DIFERENCIA_FACTURADA = 0,
                                                    RESPETO_PRECIO = "S",
                                                    PRECIO_SUGERIDO = articulo.suggestedPrice,
                                                    GEL = "N",
                                                    PORCENTAJE_DESCUENTO = articulo.percentajeDiscount,


                                                    TIPO_LINEA = "N"

                                                };

                                                db.MF_Pedido_Linea.AddObject(iMF_PedidoLinea);
                                            }
                                        }

                                        #region "Pedido"
                                        PEDIDO iPedido = new PEDIDO()
                                        {
                                            PEDIDO1 = mConsecutivoPed,
                                            ESTADO = "F", //FACTURADO PORQUE EN ESTA TRANSACCIÓN VAMOS A FACTURAR!
                                            FECHA_PEDIDO = DateTime.Now.Date,
                                            FECHA_PROMETIDA = DateTime.Now.Date,
                                            FECHA_PROX_EMBARQU = DateTime.Now.Date,
                                            FECHA_ULT_CANCELAC = DateTime.Now.Date,
                                            FECHA_ULT_EMBARQUE = DateTime.Now.Date,
                                            FECHA_ORDEN = DateTime.Now.Date,
                                            TARJETA_CREDITO = "",
                                            EMBARCAR_A = ped.cliente.Nombres.ToUpper() + " " + ped.cliente.Apellidos.ToUpper(),
                                            DIREC_EMBARQUE = "ND",
                                            DIRECCION_FACTURA = string.Empty,
                                            OBSERVACIONES = string.Empty,//observaciones pedido
                                            TOTAL_MERCADERIA = DataResult.orderData.netAmount,//Math.Round(mMontoTotal - nTotalImpuesto, 2, MidpointRounding.AwayFromZero),
                                            MONTO_ANTICIPO = 0,
                                            MONTO_FLETE = 0,
                                            MONTO_SEGURO = 0,
                                            TIPO_DOCUMENTO = "P",
                                            MONTO_DOCUMENTACIO = 0,
                                            TIPO_DESCUENTO1 = "P",
                                            TIPO_DESCUENTO2 = "P",
                                            MONTO_DESCUENTO1 = DataResult.orderData.discountAmount1,
                                            MONTO_DESCUENTO2 = 0,
                                            PORC_DESCUENTO1 = 0,
                                            PORC_DESCUENTO2 = 0,
                                            TOTAL_IMPUESTO1 = DataResult.orderData.totalTax1,
                                            TOTAL_IMPUESTO2 = 0,
                                            TOTAL_A_FACTURAR = DataResult.orderData.totalAmountToInvoice,
                                            PORC_COMI_VENDEDOR = 0,
                                            PORC_COMI_COBRADOR = 0,
                                            TOTAL_CANCELADO = 0,
                                            TOTAL_UNIDADES = mUnidades,
                                            IMPRESO = "N",
                                            FECHA_HORA = DateTime.Now,
                                            DESCUENTO_VOLUMEN = 0,
                                            TIPO_PEDIDO = "N",
                                            MONEDA_PEDIDO = "L",
                                            VERSION_NP = 1,
                                            AUTORIZADO = "N",
                                            DOC_A_GENERAR = "F",
                                            CLASE_PEDIDO = "N",
                                            MONEDA = "L",
                                            NIVEL_PRECIO = ped.cliente.FormaDePago,
                                            COBRADOR = ped.Tienda,
                                            RUTA = "ND",
                                            USUARIO = ped.Usuario,
                                            CONDICION_PAGO = "1",
                                            BODEGA = Const.TIENDA_VIRTUAL.COBRADOR_FACTURACION,
                                            ZONA = mCodigoZona,
                                            VENDEDOR = mCodVendedor,
                                            CLIENTE = ped.cliente.codigo,
                                            CLIENTE_DIRECCION = ped.cliente.codigo,
                                            CLIENTE_CORPORAC = ped.cliente.codigo,
                                            CLIENTE_ORIGEN = ped.cliente.codigo,
                                            PAIS = "GUA",
                                            SUBTIPO_DOC_CXC = Const.TIENDA_VIRTUAL.CONTA_SUB_TIPO_DOC,
                                            TIPO_DOC_CXC = "FAC",
                                            BACKORDER = "C",
                                            PORC_INTCTE = 0,
                                            DESCUENTO_CASCADA = "S",
                                            FIJAR_TIPO_CAMBIO = "N",
                                            ORIGEN_PEDIDO = "F",
                                            DESC_DIREC_EMBARQUE = mDireccionEmbarque,
                                            RUBRO1 = string.Empty,//aqui se escribe la garantía de los artículos
                                            NoteExistsFlag = 0,
                                            RecordDate = DateTime.Now,
                                            RowPointer = Guid.NewGuid(),
                                            CreatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                            UpdatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                            CreateDate = DateTime.Now,
                                            NOMBRE_CLIENTE = string.Format("{0}, {1}", ped.cliente.Apellidos.ToUpper(), ped.cliente.Nombres.ToUpper()),
                                            FECHA_PROYECTADA = DateTime.Now.Date,
                                            BASE_IMPUESTO1 = DataResult.orderData.totalTax1// Math.Round(mTotalFacturar - mTotalFacturar / mIVA, 2, MidpointRounding.AwayFromZero)
                                        };
                                        db.PEDIDO.AddObject(iPedido);

                                        MF_Pedido iMF_Pedido = new MF_Pedido()
                                        {
                                            PEDIDO = mConsecutivoPed,
                                            TIPOVENTA = Const.TIENDA_VIRTUAL.TIPO_VENTA_DEFAULT,
                                            FINANCIERA = Financiera,
                                            NIVEL_PRECIO = ped.cliente.FormaDePago,
                                            ENTREGA_AM_PM = string.Empty,
                                            MERCADERIA_SALE = Const.TIENDA_VIRTUAL.COBRADOR_FACTURACION,
                                            DESARMARLA = "N",
                                            NOTAS_TIPOVENTA = string.Empty,
                                            PAGARE = 0,
                                            NOMBRE_AUTORIZACION = string.Empty,
                                            AUTORIZACION = string.Empty,
                                            TOTAL_FACTURAR = DataResult.orderData.totalAmountToInvoice,
                                            ENGANCHE = 0,
                                            SALDO_FINANCIAR = DataResult.orderData.totalAmountToInvoice,
                                            RECARGOS = 0,
                                            MONTO = DataResult.orderData.totalAmount,//docSolicitud.Total,//Math.Round(mMontoTotal, 2, MidpointRounding.AwayFromZero),
                                            SOLICITUD = 0,
                                            CANTIDAD_PAGOS1 = 1,
                                            MONTO_PAGOS1 = 0,
                                            CANTIDAD_PAGOS2 = 1,
                                            MONTO_PAGOS2 = 0,
                                            TIPO_REFERENCIA = Const.TIENDA_VIRTUAL.TIPO_REFERENCIA_PEDIDO,
                                            OBSERVACIONES_REFERENCIA = string.Empty,
                                            RecordDate = DateTime.Now,
                                            CreatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                            UpdatedBy = string.Format("FA/{0}", "VIRTUAL"),
                                            CreateDate = DateTime.Now,
                                            TIPOPEDIDO = "N",
                                            USUARIO = ped.Usuario,
                                            FECHA_REGISTRO = DateTime.Now,
                                            CLIENTE = ped.cliente.codigo,
                                            FECHA_ENTREGA = DateTime.Today.Date.AddDays(DiasEntrega),
                                            FACTURA = mFactura,
                                            NOMBRE_RECIBE = string.Format("{0}, {1}", ped.cliente.Apellidos.ToUpper(), ped.cliente.Nombres.ToUpper()),
                                            PUNTOS = 0,
                                            VALOR_PUNTOS = 0,
                                            AUTORIZACION_EN_LINEA = "N",
                                            LOCAL_F09 = "N",
                                            RE_FACTURACION = "N",
                                            FECHA_PRIMER_PAGO = DateTime.Today,
                                            DESC_ACCESORIOS = "N"
                                        };

                                        db.MF_Pedido.AddObject(iMF_Pedido);

                                        //ACTUALIZAR EL CONSECUTIVO DEL PEDIDO

                                        foreach (var item in qUpdateConsecutivo)
                                        {
                                            item.VALOR_CONSECUTIVO = mConsecutivoPed;
                                        }

                                        #endregion

                                        log.Debug("Iniciando proceso para guardar la factura del cliente " + ped.cliente.codigo);

                                        #region "Contabilidad"


                                        switch (mReciboInt.ToString().Length)
                                        {
                                            case 1:
                                                mCeros = "00000";
                                                break;
                                            case 2:
                                                mCeros = "0000";
                                                break;
                                            case 3:
                                                mCeros = "000";
                                                break;
                                            case 4:
                                                mCeros = "00";
                                                break;
                                            case 5:
                                                mCeros = "0";
                                                break;
                                            default:
                                                mCeros = "";
                                                break;
                                        }
                                        // mConsecutivoPed.ToString().PadLeft(8, '0')
                                        mRecibo = string.Format("PV{0}-{1}{2}", Const.TIENDA_VIRTUAL.COBRADOR_FACTURACION, mCeros, mReciboInt.ToString());



                                        //1) 
                                        AUDIT_TRANS_INV iAudit_Trans_INV = new AUDIT_TRANS_INV
                                        {
                                            USUARIO = ped.Usuario,
                                            FECHA_HORA = DateTime.Now,
                                            MODULO_ORIGEN = "FA",
                                            APLICACION = string.Format("FAC#{0}", mFactura),
                                            REFERENCIA = string.Format("Cliente: {0}", ped.cliente.codigo),
                                            NoteExistsFlag = 0,
                                            RowPointer = Guid.NewGuid(),
                                            RecordDate = DateTime.Now,
                                            CreatedBy = string.Format("FA/{0}", ped.Usuario),
                                            UpdatedBy = string.Format("FA/{0}", ped.Usuario),
                                            CreateDate = DateTime.Now
                                        };

                                        db.AUDIT_TRANS_INV.AddObject(iAudit_Trans_INV);

                                        int mAuditTransINV = iAudit_Trans_INV.AUDIT_TRANS_INV1;



                                        DOCUMENTOS_CC iDocumentos_CC = new DOCUMENTOS_CC
                                        {
                                            DOCUMENTO = mFactura,
                                            TIPO = "FAC",
                                            APLICACION = string.Format("Cargado de factura: {0}", mFactura),
                                            FECHA_DOCUMENTO = DateTime.Now.Date,
                                            FECHA = DateTime.Now.Date,
                                            MONTO = DataResult.accountRegistry.totalAmount, //docSolicitud.Total,
                                            SALDO = 0,
                                            MONTO_LOCAL = DataResult.accountRegistry.localAmount,//docSolicitud.Total,
                                            SALDO_LOCAL = 0,
                                            MONTO_DOLAR = Math.Round((DataResult.accountRegistry.totalAmount / DataResult.accountRegistry.extRateDollar), 2, MidpointRounding.AwayFromZero),
                                            SALDO_DOLAR = 0,
                                            MONTO_CLIENTE = DataResult.accountRegistry.totalAmount,
                                            SALDO_CLIENTE = 0,
                                            TIPO_CAMBIO_MONEDA = 1,
                                            TIPO_CAMBIO_DOLAR = DataResult.accountRegistry.extRateDollar,
                                            TIPO_CAMBIO_CLIENT = 1,
                                            TIPO_CAMB_ACT_LOC = 1,
                                            TIPO_CAMB_ACT_DOL = DataResult.accountRegistry.extRateDollar,
                                            TIPO_CAMB_ACT_CLI = 1,
                                            SUBTOTAL = DataResult.accountRegistry.netAmount,
                                            DESCUENTO = DataResult.accountRegistry.totalDiscount,
                                            IMPUESTO1 = DataResult.accountRegistry.totalTax,
                                            IMPUESTO2 = 0,
                                            RUBRO1 = 0,
                                            RUBRO2 = 0,
                                            MONTO_RETENCION = 0,
                                            SALDO_RETENCION = 0,
                                            DEPENDIENTE = "S",
                                            FECHA_ULT_CREDITO = new DateTime(1980, 1, 1),
                                            CARGADO_DE_FACT = "S",
                                            APROBADO = "S",
                                            ASIENTO_PENDIENTE = "S",
                                            FECHA_ULT_MOD = DateTime.Now,
                                            NOTAS = "",
                                            CLASE_DOCUMENTO = "N",
                                            FECHA_VENCE = DateTime.Now.Date,
                                            NUM_PARCIALIDADES = 0,
                                            COBRADOR = ped.Tienda,
                                            USUARIO_ULT_MOD = ped.Usuario,
                                            CONDICION_PAGO = "1",
                                            MONEDA = "GTQ",
                                            VENDEDOR = mCodVendedor,
                                            CLIENTE_REPORTE = ped.cliente.codigo,
                                            CLIENTE_ORIGEN = ped.cliente.codigo,
                                            CLIENTE = ped.cliente.codigo,
                                            SUBTIPO = Const.TIENDA_VIRTUAL.CONTA_SUB_TIPO_DOC,
                                            PORC_INTCTE = 0,
                                            USUARIO_APROBACION = ped.Usuario,
                                            FECHA_APROBACION = DateTime.Now.Date,
                                            ANULADO = "N",
                                            FACTURADO = "N",
                                            NoteExistsFlag = 0,
                                            RowPointer = Guid.NewGuid(),
                                            RecordDate = DateTime.Now,
                                            CreatedBy = string.Format("FA/{0}", ped.Usuario),
                                            UpdatedBy = string.Format("FA/{0}", ped.Usuario),
                                            CreateDate = DateTime.Now,
                                            PAIS = "GUA",
                                            DEPENDIENTE_GP = "N",
                                            SALDO_TRANS = 0,
                                            SALDO_TRANS_LOCAL = 0,
                                            SALDO_TRANS_DOLAR = 0,
                                            SALDO_TRANS_CLI = 0,
                                            GENERA_DOC_FE = "N",
                                            BASE_IMPUESTO1 = DataResult.accountRegistry.netAmount,
                                            BASE_IMPUESTO2 = 0,
                                            DOCUMENTO_FISCAL = mConsecutivoPed,
                                            DOCUMENTO_GLOBAL = mFactura
                                        };

                                        db.DOCUMENTOS_CC.AddObject(iDocumentos_CC);



                                        MF_Recibo iRecibo = new MF_Recibo
                                        {
                                            DOCUMENTO = mRecibo,
                                            TIPO = "REC",
                                            CLIENTE = ped.cliente.codigo,
                                            FECHA = DateTime.Now.Date,
                                            COBRADOR = ped.Tienda,
                                            VENDEDOR = mCodVendedor,
                                            FACTURA = mFactura,
                                            EFECTIVO = 0,
                                            CHEQUE = 0,
                                            TARJETA = DataResult.accountRegistry.totalAmount,
                                            MONTO = DataResult.accountRegistry.totalAmount,
                                            NUMERO_CHEQUE = string.Empty,
                                            ENTIDAD_FINANCIERA_CHEQUE = "ND",
                                            EMISOR_TARJETA = 0,
                                            ENTIDAD_FINANCIERA_TARJETA = "ND",
                                            ES_ANTICIPO = "N",
                                            DEPOSITO = 0,
                                            ENTIDAD_FINANCIERA_DEPOSITO = "ND",
                                            OBSERVACIONES = "PAGO POR COMPRA DESDE LA TIENDA VIRTUAL Factura " + mFactura,
                                            RecordDate = DateTime.Now,
                                            CreatedBy = string.Format("FA/{0}", ped.Usuario),
                                            UpdatedBy = string.Format("FA/{0}", ped.Usuario),
                                            CreateDate = DateTime.Now,
                                            CONSECUTIVO = mConsecutivoRec,
                                            USUARIO = ped.Usuario,
                                            FECHA_REGISTRO = DateTime.Now,
                                            TIENE_DEPOSITO = "N",
                                            POS = "V",
                                            ANULADO = "N",
                                            EFECTIVO_ORIGINAL = 0,
                                            CHEQUE_ORIGINAL = 0,
                                            TARJETA_ORIGINAL = DataResult.accountRegistry.totalAmount,//mTotalFacturar,
                                            MONTO_ORIGINAL = DataResult.accountRegistry.totalAmount,//Math.Round(mMontoTotal, 2, MidpointRounding.AwayFromZero),
                                            CERRADO = "N",
                                            EFECTIVO_DOLARES = 0,
                                            EFECTIVO_DOLARES_ORIGINAL = 0,
                                            MONEDA = "GTQ",
                                            TASA_CAMBIO = 1,
                                            ESTADO_AUTORIZACION = "N"
                                        };

                                        db.MF_Recibo.AddObject(iRecibo);

                                        //ACTUALIZAR EL CONSECUTIVO DEL RECIBO
                                        var qUpdateConsecutivoRec = from c in db.CONSECUTIVO where c.CONSECUTIVO1 == mConsecutivoRec select c;
                                        foreach (var item in qUpdateConsecutivoRec)
                                        {
                                            item.ULTIMO_VALOR = mRecibo;
                                        }


                                        MF_FacturaRecibo Ifr = new MF_FacturaRecibo
                                        {
                                            FACTURA = mFactura,
                                            RECIBO = mRecibo,
                                            CreateDate = DateTime.Now,
                                            CreatedBy = "VIRTUAL"
                                            ,
                                            RecordDate = DateTime.Now
                                        };

                                        db.MF_FacturaRecibo.AddObject(Ifr);

                                        #endregion

                                        #region "Articulos"

                                        foreach (DiscountBillDetail articulo in DataResult.orderBillDetail/*ped.articulos*/)
                                        {
                                            string mTipoLinea = "N";
                                            decimal mCosto = 0;
                                            decimal mCostoDolar = 0;
                                            decimal mCostoTotalDolar = 0;
                                            decimal mCostoTotal = 0;
                                            decimal mCostoTotalLocal = 0;
                                            decimal mCostoEstimLocal = 0;
                                            decimal mCostoEstimDolar = 0;
                                           

                                            articulo.lineType = db.ARTICULO.Where(x => x.ARTICULO1 == articulo.item).FirstOrDefault().TIPO;

                                            if (articulo.lineType != Const.TIENDA_VIRTUAL.TIPO_LINEA_ARTICULO_SET || articulo.lineType != Const.TIENDA_VIRTUAL.TIPO_LINEA_ARTICULO_NORMAL
                                                || articulo.lineType != Const.TIENDA_VIRTUAL.TIPO_LINEA_ARTICULO_COMPONENTE_SET)
                                                    {
                                                        articulo.lineType = Const.TIENDA_VIRTUAL.TIPO_LINEA_ARTICULO_NORMAL;
                                                    }

                                            if (articulo.lineType == Const.TIENDA_VIRTUAL.TIPO_LINEA_ARTICULO_SET)
                                            {
                                                var qArticuloEnsamble = from a in db.ARTICULO_ENSAMBLE where a.ARTICULO_PADRE == articulo.item select a;
                                                //sumamos los costos de todos sus componentes

                                                foreach (var itemEnsamble in qArticuloEnsamble)
                                                {
                                                    decimal mCantidadPedida = articulo.quantity * itemEnsamble.CANTIDAD;
                                                    mCosto = (from a in db.ARTICULO where a.ARTICULO1 == itemEnsamble.ARTICULO_HIJO select a).First().COSTO_PROM_LOC;
                                                    mCostoDolar = (from a in db.ARTICULO where a.ARTICULO1 == itemEnsamble.ARTICULO_HIJO select a).First().COSTO_PROM_DOL;

                                                    mCostoTotal = mCostoTotal + Math.Round(mCosto * mCantidadPedida, 2, MidpointRounding.AwayFromZero);
                                                    mCostoTotalLocal = mCostoTotalLocal + Math.Round(mCosto * mCantidadPedida, 2, MidpointRounding.AwayFromZero);
                                                    mCostoTotalDolar = mCostoTotalDolar + Math.Round(mCostoDolar * mCantidadPedida, 2, MidpointRounding.AwayFromZero);
                                                    mCostoEstimLocal = mCostoEstimLocal + Math.Round(mCosto * mCantidadPedida, 2, MidpointRounding.AwayFromZero);
                                                    mCostoEstimDolar = mCostoEstimDolar + Math.Round(mCostoDolar * mCantidadPedida, 2, MidpointRounding.AwayFromZero);
                                                }
                                            }
                                            
                                            //FACTURA_LINEA

                                            FACTURA_LINEA iFactura_Linea = new FACTURA_LINEA
                                            {
                                                FACTURA = mFactura,
                                                TIPO_DOCUMENTO = "F",
                                                LINEA = short.Parse((articulo.lineOrder - 1 >= 0 ? articulo.lineOrder - 1 : articulo.lineOrder).ToString()),
                                                BODEGA = Const.TIENDA_VIRTUAL.COBRADOR_FACTURACION,
                                                COSTO_TOTAL_DOLAR = articulo.totalCostDollar,//mCostoTotalDolar,
                                                PEDIDO = mConsecutivoPed,
                                                ARTICULO = articulo.item,
                                                LOCALIZACION = Const.TIENDA_VIRTUAL.LOCALIZACION_DEFAULT,
                                                ANULADA = "N",
                                                FECHA_FACTURA = DateTime.Now.Date,
                                                CANTIDAD = articulo.quantity,
                                                PRECIO_UNITARIO = articulo.unitPrice,//Math.Round(articulo.Precio / mIVA, 2, MidpointRounding.AwayFromZero),
                                                TOTAL_IMPUESTO1 = articulo.mainItem == null ? articulo.totalTax1 : 0,
                                                TOTAL_IMPUESTO2 = 0,
                                                DESC_TOT_LINEA = articulo.totalDiscountLine1,
                                                DESC_TOT_GENERAL = 0,// mDescuentoTotalGeneral,
                                                COSTO_TOTAL = articulo.totalCost,
                                                PRECIO_TOTAL = articulo.mainItem == null ? articulo.totalPrice /*Math.Round((articulo.Precio * articulo.cantidad) / mIVA, 2) */: 0,
                                                DESCRIPCION = db.ARTICULO.Where(x => x.ARTICULO1 == articulo.item).FirstOrDefault().DESCRIPCION.ToUpper(),
                                                COMENTARIO = "",
                                                CANTIDAD_DEVUELT = 0,
                                                DESCUENTO_VOLUMEN = 0,
                                                TIPO_LINEA =  articulo.lineType,
                                                CANTIDAD_ACEPTADA = 0,
                                                CANT_NO_ENTREGADA = 0,
                                                COSTO_TOTAL_LOCAL = articulo.totalCostLocal,//mCostoTotalLocal,
                                                PEDIDO_LINEA = short.Parse((articulo.lineOrder - 1 >= 0 ? articulo.lineOrder - 1 : articulo.lineOrder).ToString()),
                                                MULTIPLICADOR_EV = articulo.lineOrder.Equals(Const.TIENDA_VIRTUAL.TIPO_LINEA_ARTICULO_COMPONENTE_SET) ? short.Parse("0") : short.Parse("1"),
                                                CANT_DESPACHADA = 0,
                                                COSTO_ESTIM_LOCAL = articulo.estimatedTotalCostLocal,//mCostoEstimLocal,
                                                COSTO_ESTIM_DOLAR = articulo.estimatedTotalCostDolar,//mCostoEstimDolar,
                                                CANT_ANUL_PORDESPA = 0,
                                                MONTO_RETENCION = 0,
                                                NoteExistsFlag = 0,
                                                RecordDate = DateTime.Now,
                                                CreatedBy = string.Format("FA/{0}", ped.Usuario),
                                                UpdatedBy = string.Format("FA/{0}", ped.Usuario),
                                                CreateDate = DateTime.Now,
                                                BASE_IMPUESTO1 = articulo.baseTax1,
                                                BASE_IMPUESTO2 = 0,
                                                CENTRO_COSTO = mCentroCosto,
                                                CUENTA_CONTABLE = mCuentaContable,
                                                U_ARMADOR = "PENDIENTE ASIGNAR",
                                                RowPointer = Guid.NewGuid(),
                                            };
                                            db.FACTURA_LINEA.AddObject(iFactura_Linea);

                                            // FACTURA_LINEA_ORIGINAL

                                            MF_Factura_Linea_Original iFactura_Linea_Original = new MF_Factura_Linea_Original
                                            {
                                                FACTURA = mFactura,
                                                TIPO_DOCUMENTO = "F",
                                                LINEA = short.Parse((articulo.lineOrder - 1 >= 0 ? articulo.lineOrder - 1 : articulo.lineOrder).ToString()),
                                                BODEGA = Const.TIENDA_VIRTUAL.COBRADOR_FACTURACION,
                                                COSTO_TOTAL_DOLAR = articulo.totalCostDollar,
                                                ARTICULO = articulo.item,
                                                LOCALIZACION = Const.TIENDA_VIRTUAL.LOCALIZACION_DEFAULT,
                                                CANTIDAD = articulo.quantity,
                                                PRECIO_UNITARIO = articulo.unitPrice,
                                                COSTO_TOTAL = articulo.totalCost,//mCostoTotal,
                                                PRECIO_TOTAL = articulo.totalPrice,//Math.Round((articulo.Precio * articulo.cantidad), 2, MidpointRounding.AwayFromZero),
                                                TIPO_LINEA = mTipoLinea,
                                                COSTO_TOTAL_LOCAL = articulo.totalCostLocal,//mCostoTotalLocal,
                                                COSTO_ESTIM_LOCAL = articulo.estimatedTotalCostLocal,
                                                COSTO_ESTIM_DOLAR = articulo.estimatedTotalCostDolar,//mCostoEstimDolar,
                                                RecordDate = DateTime.Now,
                                                CreatedBy = string.Format("FA/{0}", ped.Usuario),
                                                UpdatedBy = string.Format("FA/{0}", ped.Usuario),
                                                CreateDate = DateTime.Now,
                                                RowPointer = Guid.NewGuid()
                                            };

                                            db.MF_Factura_Linea_Original.AddObject(iFactura_Linea_Original);
                                        }

                                        #endregion

                                        #region "Facturación"
                                        //FACTURA
                                        FACTURA iFactura = new FACTURA
                                        {
                                            TIPO_DOCUMENTO = "F",
                                            FACTURA1 = mFactura,
                                            ESTA_DESPACHADO = "N",
                                            EN_INVESTIGACION = "N",
                                            TRANS_ADICIONALES = "N",
                                            ESTADO_REMISION = "N",
                                            DESCUENTO_VOLUMEN = 0,
                                            MONEDA_FACTURA = "L",
                                            FECHA_DESPACHO = DateTime.Now.Date,
                                            CLASE_DOCUMENTO = "N",
                                            FECHA_RECIBIDO = DateTime.Now.Date,
                                            PEDIDO = mConsecutivoPed,
                                            COMISION_COBRADOR = 0,
                                            TARJETA_CREDITO = "",
                                            TOTAL_VOLUMEN = mUnidades,
                                            TOTAL_PESO = mUnidades,
                                            MONTO_COBRADO = 0,
                                            TOTAL_IMPUESTO1 = DataResult.orderBill.totalTax,
                                            FECHA = DateTime.Now.Date,
                                            FECHA_ENTREGA = DateTime.Now.Date,
                                            TOTAL_IMPUESTO2 = 0,
                                            PORC_DESCUENTO2 = 0,
                                            MONTO_FLETE = 0,
                                            MONTO_SEGURO = 0,
                                            MONTO_DOCUMENTACIO = 0,
                                            TIPO_DESCUENTO1 = "P",
                                            TIPO_DESCUENTO2 = "P",
                                            MONTO_DESCUENTO1 = DataResult.orderBill.totalDiscount,
                                            MONTO_DESCUENTO2 = 0,
                                            PORC_DESCUENTO1 = 0,
                                            TOTAL_FACTURA = DataResult.orderBill.totalAmount,//Math.Round(mMontoTotal, 2, MidpointRounding.AwayFromZero),
                                            FECHA_PEDIDO = DateTime.Now.Date,
                                            FECHA_ORDEN = DateTime.Now.Date,
                                            TOTAL_MERCADERIA = DataResult.orderBill.netAmount,// Math.Round(mMontoTotal - nTotalImpuesto, 2, MidpointRounding.AwayFromZero),
                                            COMISION_VENDEDOR = 0,
                                            FECHA_HORA = DateTime.Now,
                                            TOTAL_UNIDADES = mUnidades,
                                            NUMERO_PAGINAS = 1,
                                            TIPO_CAMBIO = mTipoCambio,
                                            ANULADA = "N",
                                            MODULO = "FA",
                                            CARGADO_CG = "N",
                                            CARGADO_CXC = "S",
                                            EMBARCAR_A = string.Format("{0}, {1}", ped.cliente.Apellidos.ToUpper(), ped.cliente.Nombres.ToUpper()),
                                            DIREC_EMBARQUE = "ND",
                                            DIRECCION_FACTURA = ped.cliente.Casa,
                                            MULTIPLICADOR_EV = 1,
                                            OBSERVACIONES = string.Empty,
                                            VERSION_NP = 1,
                                            MONEDA = "L",
                                            NIVEL_PRECIO = ped.cliente.FormaDePago,
                                            COBRADOR = ped.Tienda,
                                            RUTA = "ND",
                                            USUARIO = ped.Usuario,//"VIRTUAL",
                                            CONDICION_PAGO = "1",
                                            ZONA = mCodigoZona,
                                            VENDEDOR = mCodVendedor,
                                            DOC_CREDITO_CXC = mFactura,
                                            CLIENTE_DIRECCION = ped.cliente.codigo,
                                            CLIENTE_CORPORAC = ped.cliente.codigo,
                                            CLIENTE_ORIGEN = ped.cliente.codigo,
                                            CLIENTE = ped.cliente.codigo,
                                            PAIS = "GUA",
                                            SUBTIPO_DOC_CXC = Const.TIENDA_VIRTUAL.CONTA_SUB_TIPO_DOC,
                                            TIPO_CREDITO_CXC = "FAC",
                                            TIPO_DOC_CXC = "FAC",
                                            MONTO_ANTICIPO = 0,
                                            TOTAL_PESO_NETO = mUnidades,
                                            FECHA_RIGE = DateTime.Now.Date,
                                            PORC_INTCTE = 0,
                                            USA_DESPACHOS = "S",
                                            COBRADA = "S",
                                            DESCUENTO_CASCADA = "S",
                                            DIRECCION_EMBARQUE = ped.cliente.Casa,
                                            CONSECUTIVO = mConsecutivoCnt,
                                            REIMPRESO = 0,
                                            NoteExistsFlag = 0,
                                            RecordDate = DateTime.Now,
                                            CreatedBy = string.Format("FA/{0}", ped.Usuario),
                                            UpdatedBy = string.Format("FA/{0}", ped.Usuario),
                                            CreateDate = DateTime.Now,
                                            NOMBRE_CLIENTE = string.Format("{0}, {1}", ped.cliente.Apellidos.ToUpper(), ped.cliente.Nombres.ToUpper()),
                                            GENERA_DOC_FE = "N",
                                            RowPointer = Guid.NewGuid(),
                                            BASE_IMPUESTO1 = DataResult.orderBill.baseTax1,//Math.Round(mMontoTotal / mIVA, 2, MidpointRounding.AwayFromZero),
                                            RUBRO4 = mConsecutivoFEL,
                                            RUBRO5 = resultadoFirma.NumeroAutorizacion

                                        };

                                        db.FACTURA.AddObject(iFactura);

                                        MF_Factura iMF_Factura = new MF_Factura
                                        {
                                            FACTURA = mFactura,
                                            TIPO = mTipoNivel,
                                            CLIENTE = ped.cliente.codigo,
                                            FECHA = DateTime.Now.Date,
                                            TIPOVENTA = Const.TIENDA_VIRTUAL.TIPO_VENTA_DEFAULT,
                                            FINANCIERA = Financiera ?? 0,
                                            NIVEL_PRECIO = ped.cliente.FormaDePago,
                                            PAGADA = "S",
                                            COBRADOR = ped.Tienda,
                                            JEFE=codJefe,
                                            SUPERVISOR=codSupervisor,
                                            VENDEDOR = mCodVendedor,
                                            MONTO = DataResult.orderBill.totalAmount,//Math.Round(mMontoTotal, 2, MidpointRounding.AwayFromZero),
                                            PCTJ_IVA = (mIVA - 1) * 100,
                                            PEDIDO = mConsecutivoPed,
                                            USUARIO = string.Format("FA/{0}", ped.Usuario),
                                            FECHA_REGISTRO = DateTime.Now,
                                            EXPEDIENTE_BODEGA = "N",
                                            SE_ANULARA = "N",
                                            RECIBO = mRecibo,
                                            FACTURADA_EN_BODEGA = "S",
                                            REVISADA = "N",
                                            RE_FACTURACION = "N",
                                            //campos que devuelve guatefacturas y que deben salir impresos en la factura
                                            NOMBRE_FEL = resultadoFirma.Nombre,
                                            DIRECCION_FEL = resultadoFirma.Direccion
                                        };

                                        db.MF_Factura.AddObject(iMF_Factura);



                                        MF_FacturaAutorizaDespacho iFacturaAutoriza = new MF_FacturaAutorizaDespacho
                                        {
                                            FACTURA = mFactura,
                                            CLIENTE = ped.cliente.codigo,
                                            COBRADOR = ped.Tienda,
                                            VENDEDOR = mCodVendedor,
                                            FECHA_ENTREGA = DateTime.Now.AddDays(DiasEntrega),
                                            OBSERVACIONES = "",
                                            ESTADO = "P",
                                            NOMBRE = "",
                                            TELEFONOS = "",
                                            DIRECCION = "",
                                            FECHA_AUTORIZADA = DateTime.Now.Date,
                                            RecordDate = DateTime.Now,
                                            CreatedBy = string.Format("FA/{0}", ped.Usuario),
                                            UpdatedBy = string.Format("FA/{0}", ped.Usuario),
                                            CreateDate = DateTime.Now
                                        };

                                        db.MF_FacturaAutorizaDespacho.AddObject(iFacturaAutoriza);

                                        //SALDO CLIENTE (SOLO CUANDO NO EXISTA EL REGISTRO YA QUE EL SALDO =0)
                                        Int32 mExisteSaldo = (from sc in db.SALDO_CLIENTE where sc.CLIENTE == ped.cliente.codigo select sc).Count();

                                        if (mExisteSaldo == 0)
                                        {
                                            SALDO_CLIENTE iSaldo_cliente = new SALDO_CLIENTE
                                            {
                                                CLIENTE = ped.cliente.codigo,
                                                MONEDA = "GTQ",
                                                SALDO = 0,
                                                SALDO_CORPORACION = 0,
                                                SALDO_SUCURSALES = 0,
                                                FECHA_ULT_MOV = DateTime.Now.Date,
                                                NoteExistsFlag = 0,
                                                RecordDate = DateTime.Now,
                                                RowPointer = Guid.NewGuid(),
                                                CreatedBy = string.Format("FA/{0}", ped.Usuario),
                                                UpdatedBy = string.Format("FA/{0}", ped.Usuario),
                                                CreateDate = DateTime.Now
                                            };

                                            db.SALDO_CLIENTE.AddObject(iSaldo_cliente);
                                        }

                                        #endregion

                                        db.SaveChanges();
                                        respuesta.Exito = true;
                                        respuesta.Mensaje = "";
                                        respuesta.Objeto = new ResultadoTiendaVirtual { Factura = mFactura, Pedido = mConsecutivoPed };
                                        EnviarNotificacion(mRemitenteNotificacion, lstAdmonNotificar, lstBodegaNotificar, docSolicitud, ped, mFactura);

                                        log.Debug("GENERANDO FACTURA " + mFactura + " desde la tienda en línea");
                                        transactionScope.Complete();
                                        //
                                        //factura y certificado de garantía
                                        //
                                        EnviarCertificadoGarantia(mFactura);
                                    }
                                    else
                                    {
                                        log.Error("Problema al firmar el documento electrónicamente, mensaje: " + resultadoFirma.Error.ToString());
                                        EnviarNotificacion(mRemitenteNotificacion, lstAdmonNotificar, lstBodegaNotificar, docSolicitud, ped, mFactura, resultadoFirma.Error.ToString());
                                        respuesta.Exito = false;
                                        respuesta.Mensaje = mMensajeErrorGral;
                                    }

                                    

                                }
                                catch (Exception ex)
                                {
                                    log.Error("Problema al firmar el documento electrónicamente, mensaje: " + resultadoFirma.Error.ToString()+" "+ex.Message);
                                    EnviarNotificacion(mRemitenteNotificacion, lstAdmonNotificar, lstBodegaNotificar, docSolicitud, ped, mFactura,(mFactura != ""? "Factura generada: "+mFactura+ ", PERO NO SE PUDO GRABAR POR:": "")+ CatchClass.ExMessage(ex, "DALTiendaVirtual", "Facturar"));
                                    respuesta.Exito = false;
                                    respuesta.Mensaje = mMensajeErrorGral;
                                }
                            }
                        
                           
                        }
                        else

                        {
                            log.Error("no se pudo obtener informacion para guardar datos de la facturación");
                            respuesta.Exito = false;
                            respuesta.Mensaje = mMensajeErrorGral;
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error(string.Format("Problema al grabar el pedido del cliente {0} desde la tienda virtual (DAL) ",ped.cliente.codigo),ex);
                    respuesta.Exito = false;
                    respuesta.Mensaje = ex.Message;
                    
                    EnviarNotificacion(mRemitenteNotificacion, lstAdmonNotificar, lstBodegaNotificar,null, ped, "", CatchClass.ExMessage(ex, "DALTiendaVirtual", "Facturar"));

                }


            }

                return respuesta;
        }


        private static async void EnviarCertificadoGarantia(string codigoFactura)
        {
            await System.Threading.Tasks.Task.Run(() => {
                try
                {
                    EmailController email = new EmailController();
                    email.EnviarFacturaGarantia(codigoFactura);
                }
                catch
                {
                   
                }
            });
        }
        private static Respuesta Validaciones(Pedido ped,bool blnValidarExistencias,List<string> Financieras)
        {
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            Respuesta res = new Respuesta();
            int TotalItems = 0;
            decimal TotalPedido = 0;
            res.Exito = true;
            int lineaAnterior = 0;
            try
            {
                
                //validar pedido
                if (ped.TotalItems == 0)
                {
                    res.Exito = false;
                    res.Mensaje = res.Mensaje+"(*) El pedido debe tener al menos un artículo.";
                }
                if (ped.TotalPedido ==0)
                {
                    res.Exito = false;
                    res.Mensaje += "(*) El pedido debe tener un valor total.";
                }
                if (ped.TotalPedido == 0)
                {
                    res.Exito = false;
                    res.Mensaje += "(*) El pedido debe tener un valor total.";
                }
                if (ped.Tienda ==null)
                {
                    res.Exito = false;
                    res.Mensaje += "(*) El pedido debe tener una tienda.";
                }
                if (ped.Tienda == string.Empty)
                {
                    res.Exito = false;
                    res.Mensaje += "(*) El pedido debe tener una tienda.";
                }
                foreach (Articulo articulo in ped.articulos)
                {
                    if (lineaAnterior == 0)
                        lineaAnterior = articulo.Linea;
                    else
                    {
                        if (articulo.Linea <= lineaAnterior)
                        {
                            res.Exito = false;
                            res.Mensaje += "(*) El número de línea de los artículos debe ser diferente.";
                        }
                        lineaAnterior = articulo.Linea;
                    }
                    TotalItems += articulo.cantidad;
                    TotalPedido += articulo.Precio * articulo.cantidad;

                }
                if (TotalItems != ped.TotalItems)
                {
                    res.Exito = false;
                    res.Mensaje += "(*) La cantidad total de artículos en el pedido no concuerda con la suma de los artículos.";
                }
                if (TotalPedido != ped.TotalPedido)
                {
                    res.Exito = false;
                    res.Mensaje += "(*) El valor total del pedido no es igual a la suma de valores de los artículos.";
                }
                //FINANCIERA
                if (ped.cliente.FormaDePago != null)
                {
                    var qFinanciera = (from n in db.MF_NIVEL_PRECIO_COEFICIENTE where n.NIVEL_PADRE == ped.cliente.FormaDePago select n);
                    if (qFinanciera == null)
                    {
                        res.Exito = false;
                        res.Mensaje += "(*) La forma de pago es inválida.";
                    }
                    if (qFinanciera.Count() == 0)
                    {
                        res.Exito = false;
                        res.Mensaje += "(*) La forma de pago es inválida.";
                    }
                    else
                    {
                        var Financiera = qFinanciera.FirstOrDefault().FINANCIERA;
                        if (Financiera.Equals(string.Empty))
                        {
                            res.Exito = false;
                            res.Mensaje += "(*) La forma de pago es inválida.";
                        }
                    }
                }
                //EXISTENCIAS
                if (blnValidarExistencias)
                {
                    DateTime mFechaValidarReservas = (DateTime)(from c in db.MF_Configura select c).First().FECHA_VALIDAR_RESERVAS;

                    var qArticulos = (from p in ped.articulos
                                      where  p.CodigoArticulo.Substring(0, 1) != "0" && p.CodigoArticulo.Substring(0, 1) != "S" && p.CodigoArticulo.Substring(0, 1) != "M"
                                      && p.CodigoArticulo.Substring(0, 3) != "141" && p.CodigoArticulo.Substring(0, 3) != "142" && p.CodigoArticulo.Substring(0, 3) != "149" 
                                      group p by new { p.CodigoArticulo,p.Descripcion } into g
                                      select new
                                      {
                                          Articulo = g.Key.CodigoArticulo,
                                          Descripcion=g.Key.Descripcion,
                                          Cantidad = g.Sum(x => x.cantidad)
                                      });

                    
                    foreach (var item in qArticulos)
                    {
                        if ((from a in db.ARTICULO where a.ARTICULO1 == item.Articulo select a).First().TIPO == "T")
                        {
                            decimal mDisponibles = 0; decimal mExistenciasTotales = 0; decimal mFacturados = 0;
                            var qExistencias = from e in db.EXISTENCIA_BODEGA where e.ARTICULO == item.Articulo && e.BODEGA == "F01" select e;

                            if (qExistencias.Count() > 0) mExistenciasTotales = qExistencias.Sum(x => x.CANT_DISPONIBLE);
                            var qReservas = from fl in db.FACTURA_LINEA
                                            where fl.ANULADA == "N" && fl.CANT_DESPACHADA < fl.CANTIDAD && fl.FECHA_FACTURA >= mFechaValidarReservas && fl.TIPO_DOCUMENTO == "F" && fl.ARTICULO == item.Articulo && fl.BODEGA == "F01"
                                            select fl;

                            if (qReservas.Count() > 0) mFacturados = qReservas.Sum(x => x.CANTIDAD) - qReservas.Sum(x => x.CANT_DESPACHADA);
                            mDisponibles = mExistenciasTotales - mFacturados;
                            if (mDisponibles < 0) mDisponibles = 0;

                            if (item.Cantidad > mDisponibles)
                            {
                              res.Mensaje += string.Format("(*) Del artículo {0} está pidiendo {1} y no hay disponibles en este momento.", item.Descripcion, Convert.ToInt32(item.Cantidad), Convert.ToInt32(mDisponibles));
                                res.Exito = false;
                                return res;
                            }
                        }
                    }
                }

                //ValidarConsecutivosFacturas();
            }
            catch (Exception ex)
            {
                log.Error("Problema al validar el pedido desde la tienda en línea", ex);
                res.Exito = false;
                
            }
            return res;
        }


        private static decimal ObtenerIVA()
        {
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            return 1 + ((from im in db.IMPUESTO where im.IMPUESTO1 == "IVA" select im).First().IMPUESTO11 / 100);
            
        }
        private static bool ValidarEmailCliente(string EMAIL)
        {
            string mAmbiente = WebConfigurationManager.AppSettings["Ambiente"];
            
            WebRequest request = WebRequest.Create(string.Format("~/isEmailValido/?email={0}", EMAIL));

            request.Method = "GET";
            request.ContentType = "application/json";

            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            Clases.RetornaExito mRetornaMail = new Clases.RetornaExito();
            mRetornaMail = JsonConvert.DeserializeObject<Clases.RetornaExito>(responseString);

            if (!mRetornaMail.exito)
            {
                return false;
            }
            return true;
        }

        private static string ObtenerNumeroFactura( string tienda, string tipo)
        {
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            string mRetorna = "No disponible";
            string mNombreConsecutivo = "contado";
            try
            {
                string[] mConsecutivos = (from c in db.COBRADOR where c.COBRADOR1 == tienda select c).First().E_MAIL.ToString().Split(new string[] { "," }, StringSplitOptions.None);

                string mConsecutivo = mConsecutivos[0];

                if (tipo == "CR")
                {
                    mNombreConsecutivo = "crédito";
                    mConsecutivo = mConsecutivos[1];
                }


                if (tienda == "F01") mNombreConsecutivo = tipo;
                if (tipo == "A") mConsecutivo = mConsecutivos[0];
                if (tipo == "CEM") mConsecutivo = mConsecutivos[1];
                if (tipo == "EX")
                {
                    try
                    {
                        mConsecutivo = mConsecutivos[7];
                    }
                    catch
                    {
                        return "No disponible";
                    }
                }
                if (tipo == "F1A")
                {
                    try
                    {
                        mConsecutivo = mConsecutivos[5];
                    }
                    catch
                    {
                        return "No disponible";
                    }
                }

                Int32 mCuantos = (from c in db.CONSECUTIVO_FA where c.CODIGO_CONSECUTIVO == mConsecutivo select c).Count();

                if (mCuantos > 0)
                {
                    var qConsecutivoFAC= (from c in db.CONSECUTIVO_FA where c.CODIGO_CONSECUTIVO == mConsecutivo select c).First();

                    string mUltimoValor = qConsecutivoFAC.VALOR_MAXIMO;
                    string mValorActual = qConsecutivoFAC.VALOR_CONSECUTIVO;
                    string[] mConsecutivoFA = mValorActual.ToString().Split(new string[] { "-" }, StringSplitOptions.None);

                    if (mUltimoValor == mValorActual)
                    {
                        mRetorna = "No hay consecutivos disponibles para facturar en LÍNEA";
                        log.Error("No hay consecutivos disponibles para facturar en LÍNEA");
                    }
                    else
                    {
                        Int32 mNumeroFactura = Convert.ToInt32(mConsecutivoFA[1]);

                        int mSumar = 1; string mCeros = "";
                        int mFolioInicial = (from c in db.CONSECUTIVO_FA join r in db.RESOLUCION_DOC_ELECTRONICO on c.RESOLUCION equals r.RESOLUCION where c.CODIGO_CONSECUTIVO == mConsecutivo select r).First().FOLIO_INICIAL;

                        if (mFolioInicial == mNumeroFactura) mSumar = 0;
                        mNumeroFactura = mNumeroFactura + mSumar;

                        int mNumeroFacturaLength = Convert.ToString(mNumeroFactura).Length;
                        int mConsecutivoFALength = mConsecutivoFA[1].Length;

                        for (int ii = Convert.ToString(mNumeroFactura).Length; ii < mConsecutivoFA[1].Length; ii++)
                        {
                            mCeros = string.Format("0{0}", mCeros);
                        }

                        string mFactura = string.Format("{0}-{1}{2}", mConsecutivoFA[0], mCeros, Convert.ToString(mNumeroFactura));
                        if ((from f in db.FACTURA where f.FACTURA1 == mFactura select f).Count() > 0) mFactura = string.Format("{0}-{1}{2}", mConsecutivoFA[0], mCeros, Convert.ToString(mNumeroFactura + 1));

                        mRetorna = mFactura;
                    }
                }
            } catch (Exception ex)
            {
                log.Error("Error al obtener el correlativo para la facturación de la tienda virtual",ex);
            }
            return mRetorna;
        }

        private static void ValidarConsecutivosFacturas()
        {
            Respuesta mRes = new Respuesta();
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            string mMensajeRespuesta = string.Empty;
            var qConfiguracion = (from conf in db.MF_Catalogo where conf.CODIGO_TABLA == Const.CATALOGO.CATALOGO_FACTURACION_EN_LINEA select conf);
            string[] mCuentaCorreo = qConfiguracion.Where(x => x.CODIGO_CAT.Equals("NOTIFICAR_CONTA")).FirstOrDefault().NOMBRE_CAT.Split('|');
            int ConsecutivoIndex= int.Parse(qConfiguracion.Where(x => x.CODIGO_CAT.Equals("POSICION_CONSECUTIVO_FACTURA")).FirstOrDefault().NOMBRE_CAT.ToString());
            string mTienda = Const.TIENDA_VIRTUAL.COBRADOR_FACTURACION;
            string[] mConsecutivosCnt = (from c in db.COBRADOR where c.COBRADOR1 == Const.TIENDA_VIRTUAL.COBRADOR_FACTURACION select c).First().E_MAIL.ToString().Split(new string[] { "," }, StringSplitOptions.None);

            string mConsecutivoCnt = mConsecutivosCnt[ConsecutivoIndex];

            StringBuilder mBody;
            mBody = new StringBuilder();
            var qConsecutivo= (from c in db.CONSECUTIVO_FA where c.CODIGO_CONSECUTIVO == mConsecutivoCnt select c).First();

            string mUltimoValor = qConsecutivo.VALOR_MAXIMO;
            
            //valor que le corresponde a la factura.
            string mValorConsecutivo= qConsecutivo.VALOR_CONSECUTIVO;

            string mAsunto = string.Empty;
            string mCuerpoCorreo = string.Empty;

            string mTipoPedido = "CO";
            string mConsecutivoContado = mConsecutivosCnt[0];
            string mConsecutivoCredito = mConsecutivosCnt[1];
            string mConsecutivoDespacho = mConsecutivosCnt[2];
            string mConsecutivoContadoBK, mConsecutivoCreditoBK;

            try
            {
                mConsecutivoContadoBK = mConsecutivosCnt[3];
            }
            catch
            {
                mConsecutivoContadoBK = "";
            }
            try
            {
                mConsecutivoCreditoBK = mConsecutivosCnt[4];
            }
            catch
            {
                mConsecutivoCreditoBK = "";
            }
            try
            {
                if (mValorConsecutivo == mUltimoValor)
                {
                    if (mTipoPedido == "CO")
                    {
                        if (mConsecutivoContadoBK == "")
                        {
                            mAsunto = string.Format("Consecutivo facturas de contado {0}", Const.TIENDA_VIRTUAL.COBRADOR_FACTURACION);
                            mCuerpoCorreo = string.Format("Ya se utilizó la última factura de contado.  Es urgente actualizar el consecutivo de facturas de contado en el mantenimiento de cobradores para {0}.", Const.TIENDA_VIRTUAL.COBRADOR_FACTURACION);
                            mBody = new StringBuilder();
                            mBody.Append(mCuerpoCorreo);
                        }
                        else
                        {
                            mAsunto = string.Format("Consecutivo facturas de contado {0}", Const.TIENDA_VIRTUAL.COBRADOR_FACTURACION);
                            mCuerpoCorreo = string.Format("El consecutivo de BackUP para las facturas de contado de {0} ya fue utilizado, por favor agregar un nuevo consecutivo de BackUP en el mantenimiento de cobradores para {0}.", Const.TIENDA_VIRTUAL.COBRADOR_FACTURACION);
                            mBody = new StringBuilder();
                            mBody.Append(mCuerpoCorreo);
                        }
                    }
                }
                else
                {
                    string[] mFacturaActual = mValorConsecutivo.Split(new string[] { "-" }, StringSplitOptions.None);
                    string[] mFacturaUltima = mUltimoValor.Split(new string[] { "-" }, StringSplitOptions.None);

                    Int32 mNumeroFacturaActual = Convert.ToInt32(mFacturaActual[1]);
                    Int32 mNumeroFacturaUltima = Convert.ToInt32(mFacturaUltima[1]);
                    int mDiferencia = mNumeroFacturaUltima - mNumeroFacturaActual;

                    if (mDiferencia <= 25)
                    {
                        if (mTipoPedido == "CO" && mConsecutivoContadoBK == "")
                        {
                            mAsunto = string.Format("Consecutivo facturas de contado {0}", mTienda);
                            mCuerpoCorreo = string.Format("Solamente quedan {0} factura(s) de contado disponibles para {1}, es urgente agregar un consecutivo de BackUP para las facturas de contado.", mDiferencia, mTienda);
                            mBody.Append(mCuerpoCorreo);
                        }
                        else if (mConsecutivoCreditoBK == "")
                        {
                            mAsunto = string.Format("Consecutivo facturas de crédito {0}", mTienda);
                            mCuerpoCorreo = string.Format("Solamente quedan {0} factura(s) de crédito disponibles para {1}, es urgente agregar un consecutivo de BackUP para las facturas de crédito.", mDiferencia, mTienda);
                            mBody.Append(mCuerpoCorreo);
                        }

                    }
                }

                if (mCuerpoCorreo.Length > 0)
                {
                    try
                    {
                        log.Info("---> Validación del correlativo de la factura previo a facturar desde la tienda en línea, Cuerpo Correo> " + mCuerpoCorreo);
                        Utils.Utilitarios utilitarios = new Utils.Utilitarios();
                        if (mCuentaCorreo.ToList().Count() > 0 && !mCuentaCorreo.ToList()[0].Equals(string.Empty))
                            utilitarios.EnviarEmail(WebConfigurationManager.AppSettings["UsrMail"].ToString(), mCuentaCorreo.ToList(), mAsunto, mBody, "Punto de Venta - NOTIFICACIONES-");
                    }
                    catch(Exception ex)
                    {
                        log.Error("Ocurrió un error al intentar enviar el correo, validándo el correlativo de las facturas para la tienda virtual,(DALTiendaVirtual)"+ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(string.Format("Problema al validar el correlativo de la factura {0}, ultimo valor {1}, excepcion {2} ", mValorConsecutivo, mUltimoValor,ex.InnerException));
            }
        }

        private static void EnviarNotificacion(string Remitente,List<string> NotificarConta, List<string> NotificarBodega,Doc pedido,Pedido ped,string mFactura,string msgError="")
        {
            Utils.Utilitarios utilitarios = new Utils.Utilitarios();
            StringBuilder mBody = new StringBuilder();
            //colocar los estilos para el formato del correo y la tabla con la información de los artículos comprados.

            string mCuerpoCorreo = msgError.Equals(string.Empty) ? 
                                    @"<!DOCTYPE html> <html> <title>NUEVA VENTA</title> <meta name='viewport' content='width=device-width, initial-scale=1'> 
                                    <head> <style> #tab { -moz-tab-size: 4; /* Firefox */ -o-tab-size: 4; /* Opera 10.6-12.1 */ tab-size: 4; } pre 
                                    {font-family:Arial;} body { font-family:Arial; } table { border-collapse: collapse; border-spacing: 0; width: 100%;
                                    border: 1px solid #ddd; font-family:Arial; }  th, td { text-align: left; padding: 16px; }  tr:nth-child(even) 
                                    { background-color: #f2f2f2 } #Total { font-size: 120%; }  #TotalPalabra { font-size: 120%; } </style> </head> 
                                    <body>  <div class='w3-container'> <h2>NUEVA FACTURA DESDE LA TIENDA VIRTUAL</h2><br/> <p>A continuación se 
                                    detallan los artículos adquiridos desde la tienda en línea, factura " + mFactura + " </p>  " +
                                    "<table class='w3-table w3-striped'> <tr> <th>Línea</th> <th>Artículo</th> <th>Descripción</th> <th>Cantidad</th> <th>Precio Unitario</th><th>Descuento</th>  <th>Monto Total</th> </tr>"
                                    :
                                    @"<!DOCTYPE html> <html> <title>NUEVA VENTA</title> <meta name='viewport' content='width=device-width, initial-scale=1'> 
                                    <head> <style> #tab { -moz-tab-size: 4; /* Firefox */ -o-tab-size: 4; /* Opera 10.6-12.1 */ tab-size: 4; } pre 
                                    {font-family:Arial;} body { font-family:Arial; } table { border-collapse: collapse; border-spacing: 0; width: 100%;
                                    border: 1px solid #ddd; font-family:Arial; }  th, td { text-align: left; padding: 16px; }  tr:nth-child(even) 
                                    { background-color: #f2f2f2 } #Total { font-size: 120%; }  #TotalPalabra { font-size: 120%; } </style> </head> 
                                    <body>  <div class='w3-container'><br/>NO SE FACTURÓ VENTA DE TIENDA VIRTUAL<BR/>PEDIDO: " + JsonConvert.SerializeObject(ped) + "<BR/> ERROR: " + msgError;
            if (msgError == "")
            {
                foreach (DetalleDoc item in pedido.Detalle)
                {
                    if (item.Linea % 2 == 0)
                        mCuerpoCorreo += "<tr>";
                    else
                        mCuerpoCorreo += "<tr style='background-color: #f2f2f2'>";
                    mCuerpoCorreo += string.Format("<td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td> Q.{4}</td> <td>Q.{5} </td><td>Q.{6} </td> </tr> ", item.Linea, item.Producto, item.Descripcion, item.Cantidad, item.Precio,item.ImpDescuento,item.ImpTotal);

                }
                mCuerpoCorreo += string.Format("<tr> <td></td> <td></td> <td></td> <td></td> <td id='TotalPalabra'> Total</td> <td id='Total'><b> Q.{0} </b></td> </tr> </table> " +
                                         "</div> <br/> <pre id='Tab'>		Saludos cordiales, </pre> </body> </html> ", pedido.Total);
            }

            mBody.Append(mCuerpoCorreo);
            
            if (NotificarConta.Count > 0 && !NotificarConta[0].Equals(string.Empty) && msgError == "" && General.Ambiente.Equals("PRO"))
                utilitarios.EnviarEmail(Remitente, NotificarConta, "NUEVA VENTA DESDE LA TIENDA VIRTUAL", mBody, "PUNTO DE VENTA -NOTIFICACIONES-");

            if (NotificarBodega.Count > 0 && !NotificarBodega[0].Equals(string.Empty))
                utilitarios.EnviarEmail(Remitente, NotificarBodega,(General.Ambiente.Equals("PRO") ? String.Empty : "PRUEBA - ") + (msgError.Equals("")? "NUEVA VENTA DESDE LA TIENDA VIRTUAL":"ERROR EN FACTURACIÓN DE VENTA DESDE LA TIENDA VIRTUAL"), mBody, (General.Ambiente.Equals("PRO") ? String.Empty : "PRUEBA - ") + "PUNTO DE VENTA -NOTIFICACIONES-");
        }
    }
}