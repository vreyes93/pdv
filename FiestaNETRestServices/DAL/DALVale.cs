﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Models;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using WebGrease.Css.Extensions;

namespace FiestaNETRestServices.DAL
{
    public class DALVale : MFDAL
    {

        public Respuesta CanjearVale(List<CanjeVale> infoVale)
        {
            Respuesta resp = new Respuesta();
            resp.Exito = false;
            try
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    infoVale.ForEach(vale =>
                    {
                        var qVale = dbExactus.MF_Vale.Where(x => x.VALE == vale.Codigo);
                        qVale.ForEach(x =>
                        {
                            x.TIENDA = vale.TiendaCanje;
                            x.FACTURA = vale.Factura;
                            x.FACTURADO = "S";
                            x.RecordDate = DateTime.Now;
                            x.UpdatedBy = vale.UsuarioCanje;
                        });
                    });

                    dbExactus.SaveChanges();
                    transactionScope.Complete();
                    resp.Exito = true;
                }
            }
            catch (Exception ex)
            {
                resp.Exito = false;
                resp.Mensaje = ex.Message;
            }
            return resp;
        }
        /// <summary>
        /// Método para grabar el vale de descuento generado por el cliente
        /// </summary>
        /// <param name="infoVale"></param>
        /// <returns></returns>

        public Respuesta GrabarValeDeDescuento(Vale infoVale)
        {
            Respuesta mRespuesta = new Respuesta();
            EXACTUSERPEntities db = new EXACTUSERPEntities();

           db.CommandTimeout = 999999999;
            try
            {
                MF_Vale iVale = new MF_Vale
                {
                    BENEFICIARIO_VENTA = infoVale.Nombres + " " + infoVale.Apellidos,
                    DIRECCION = infoVale.Direccion,
                    EDAD = infoVale.Edad,
                    EMAIL = infoVale.Email,
                    TIENDA = infoVale.Tienda,
                    TIPO_VALE = infoVale.Tipo,
                    VALE = infoVale.Codigo,
                    VALOR = infoVale.Valor,
                    FECHA_VENCE = infoVale.DiaHasta != 0 ? DateTime.Parse(infoVale.AnioHasta.ToString("d4")+"-"+  infoVale.MesHasta.ToString("d2") + "-" + infoVale.DiaHasta.ToString("d2")): DateTime.Now,
                    PORCENTAJE = infoVale.Porcentaje,
                    PROMOCION = infoVale.Promocion,
                    CreateDate = DateTime.Now,
                    RecordDate=DateTime.Now,
                    CreatedBy = "mf.fiestanet",
                    GENERO = infoVale.Genero.Equals("False")? "F": "M",
                    FACTURADO = "N",
                    IDENTIFICACION = infoVale.Identificacion,
                    TELEFONO = infoVale.Telefono,
                    VENCE = infoVale.DiaHasta != 0 ? "S" : "N",
                     IP_SOLICITUD=infoVale.ip_solicitante
                };

                db.MF_Vale.AddObject(iVale);
                db.SaveChanges();
                mRespuesta.Exito = true;

            }
            catch (Exception ex)
            {
                mRespuesta.Exito = false;
                mRespuesta.Mensaje = "Problema al grabar el vale " + infoVale.Codigo;
                log.Error(string.Format("Error al grabar el vale {0} error {1}", infoVale.Codigo, ex.Message));
            }

            mRespuesta.Objeto = infoVale;
            return mRespuesta;
        }

        /// <summary>
        /// Validar la existencia de ese mismo código de vale
        /// </summary>
        /// <param name="Codigo"></param>
        /// <returns></returns>
        public bool ExisteVale(string Codigo)
        {
            bool blnExiste = false;
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            blnExiste = (from s in db.MF_Vale where s.VALE == Codigo select s).Any();
            return blnExiste;
        }

        public Respuesta ObtenerInfoValeDeDescuento(string CodigoVale)
        {
            Respuesta mRespuesta = new Respuesta();
            mRespuesta.Exito = false;
            EXACTUSERPEntities db = new EXACTUSERPEntities();

            //nombres y apellidos
            

            var qVale = (from s in db.MF_Vale where s.VALE == CodigoVale select new Vale {
                Codigo = s.VALE,
                Nombres = s.BENEFICIARIO_VENTA.Substring(0,s.BENEFICIARIO_VENTA.IndexOf("-"))
                , Apellidos = s.BENEFICIARIO_VENTA.Substring(s.BENEFICIARIO_VENTA.IndexOf("-")+1)
                , Direccion= s.DIRECCION,
                    Edad= s.EDAD.Value,
                    Email=s.EMAIL,
                    Genero=s.GENERO,
                    Identificacion=s.IDENTIFICACION,
                    Tienda= s.TIENDA,
                    Tipo=s.TIPO_VALE,
                    Valor=s.VALOR,
                    Telefono=s.TELEFONO,
                    Promocion=s.PROMOCION, 
                    Porcentaje=s.PORCENTAJE ?? 0,
                    DiaHasta=s.VENCE.Equals("S") ? s.FECHA_VENCE.Value.Day:0,
                    MesHasta = s.VENCE.Equals("S") ? s.FECHA_VENCE.Value.Month:0,
                    AnioHasta = s.VENCE.Equals("S") ? s.FECHA_VENCE.Value.Year:0,
            });
            if (qVale.Count() > 0)
            {
                mRespuesta.Objeto = qVale.FirstOrDefault();
                mRespuesta.Exito = true;
            }
            return mRespuesta;
        }

        public Vale BuscarValeporIP(string Ip)
        {
            Respuesta mRespuesta = new Respuesta();
            mRespuesta.Exito = false;
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            var qVale = (from s in db.MF_Vale
                         where s.IP_SOLICITUD == Ip
                             select new Vale {
                                Codigo = s.VALE,
                                Nombres = s.BENEFICIARIO_VENTA.Substring(0, s.BENEFICIARIO_VENTA.IndexOf(" "))
                                , Apellidos = s.BENEFICIARIO_VENTA.Substring(s.BENEFICIARIO_VENTA.IndexOf(" ") + 1, s.BENEFICIARIO_VENTA.Length - s.BENEFICIARIO_VENTA.IndexOf(" "))
                                , Direccion= s.DIRECCION,
                                Edad= s.EDAD.Value,
                                Email=s.EMAIL,
                                Genero=s.GENERO,
                                Identificacion=s.IDENTIFICACION,
                                Tienda= s.TIENDA,
                                Tipo=s.TIPO_VALE,
                                Valor=s.VALOR,
                                Telefono=s.TELEFONO,
                                Promocion=s.PROMOCION,
                                Porcentaje=s.PORCENTAJE ?? 0,
                                DiaHasta=s.VENCE.Equals("S") ? s.FECHA_VENCE.Value.Day:0,
                                MesHasta = s.VENCE.Equals("S") ? s.FECHA_VENCE.Value.Month:0,
                                AnioHasta = s.VENCE.Equals("S") ? s.FECHA_VENCE.Value.Year:0,
                             });

            mRespuesta.Exito = true;
            if (qVale.Count() > 0)
               return qVale.FirstOrDefault();
            else
               
            return null;

        }
       
    }

}