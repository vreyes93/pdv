﻿using FiestaNETRestServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FiestaNETRestServices.DAL
{
    /// <summary>
    /// Clase que permite validar diferentes datos de importancia para el negocio
    /// </summary>
    public static class DALValidar
    {
        /// <summary>
        /// Función pública y estática que evalúa si un artículo requiere armado
        /// </summary>
        /// <param name="codigoArticulo">Código de artículo a evaluar</param>
        /// <returns>Retorna verdadero si el artículo requiere armado, retorna falso en caso contrario</returns>
        public static bool RequiereArmado(string codigoArticulo)
        {
            if (string.IsNullOrEmpty(codigoArticulo) || string.IsNullOrWhiteSpace(codigoArticulo)) return false;

            List<ARTICULO_ESPE> result = ConsultaArmado(codigoArticulo);

            if (result.Count == 0)
                return false;
            else
                return result.FirstOrDefault().VALOR == "S";
        }

        /// <summary>
        /// Función pública y estática que evalúa la compatibilidad entre un producto y una tienda
        /// para armado
        /// </summary>
        /// <param name="codigoTienda">Código de tienda a evaluar</param>
        /// <param name="codigoArticulo">Código de artículo a evaluar</param>
        /// <returns>Retorna verdadero si el producto y la tienda son compatibles para armado, en caso contrario retorna falso</returns>
        public static bool CompatibilidadArmadoEntreTiendaProducto(string codigoTienda, string codigoArticulo)
        {
            bool result = true;

            if ((string.IsNullOrEmpty(codigoArticulo) || string.IsNullOrWhiteSpace(codigoArticulo)) ||
                (string.IsNullOrEmpty(codigoTienda) || string.IsNullOrWhiteSpace(codigoTienda)))
                return false;

            List<ARTICULO_ESPE> dataArticulo = ConsultaCategoriaArticulo(codigoArticulo);

            if (dataArticulo.Count > 0)
            {
                string categoriaProducto = dataArticulo.FirstOrDefault().VALOR;
                switch (ObtenerCategoriaTienda(codigoTienda))
                {
                    case "B":
                        result = !(categoriaProducto == "A");
                        break;
                    case "C":
                        result = !(categoriaProducto == "A" || categoriaProducto == "B");
                        break;
                }
            }

            return result;
        }

        /// <summary>
        /// Método público y estático que evalua la compatibilidad entre las categorias de ambas tiendas y el producto dentro del proceso de traspaso
        /// </summary>
        /// <param name="bodegaOrigen">Código de tienda de donde sale el producto</param>
        /// <param name="bodegaDestino">Código de tienda a donde llega el producto</param>
        /// <param name="codigoArticulo">Código de articulo a traspasar</param>
        /// <param name="descripcionArticulo">Nombre de articulo a traspasar</param>
        public static void CompatibilidadTraspasoTiendaProducto(string bodegaOrigen, string bodegaDestino, string codigoArticulo, string descripcionArticulo)
        {
            string mensaje = "Error: La bodega {0} no soporta la categoría del articulo {1}. Favor comuníquese con el departamento de importaciones.";

            if (bodegaOrigen != "F01")
            {
                if (!CompatibilidadArmadoEntreTiendaProducto(bodegaOrigen, codigoArticulo))
                    throw new Exception(string.Format(mensaje, bodegaOrigen + "(Origen)", descripcionArticulo));
            }

            if (bodegaDestino != "F01")
            {
                if (!CompatibilidadArmadoEntreTiendaProducto(bodegaDestino, codigoArticulo))
                    throw new Exception(string.Format(mensaje, bodegaDestino + "(Destino)", descripcionArticulo));
            }
        }

        /// <summary>
        /// Validar existencias de un artículo, dependiendo de su especificación
        /// </summary>
        /// <param name="codigoArticulo"></param>
        /// <returns></returns>
        public static bool ValidarExistencias(string codigoArticulo)
        {
            try
            {
                List<ARTICULO_ESPE> result = new List<ARTICULO_ESPE>();

                using (EXACTUSERPEntities db = new EXACTUSERPEntities())
                {
                    result = (from e in db.ARTICULO_ESPE
                              where e.ATRIBUTO == "SEGMENTO_TIENDA" && e.ARTICULO == codigoArticulo
                              select e).ToList();
                }

                if (result.FirstOrDefault().VALOR.Equals("OUTLET"))
                    return false;
                else
                    return true;
            }
            catch
            {
                return false;
            }
        }
        #region Privates
        /// <summary>
        /// Función estática y privada que consulta hacia la base de datos la información de un artículo
        /// para armado
        /// </summary>
        /// <param name="codigoArticulo">Código de artículo a consultar</param>
        /// <returns>retorna la información encapsulado en el objeto List<MF_ArticuloArmado></returns>
        private static List<ARTICULO_ESPE> ConsultaArmado(string codigoArticulo)
        {
            List<ARTICULO_ESPE> result = new List<ARTICULO_ESPE>();

            using (EXACTUSERPEntities db = new EXACTUSERPEntities())
            {
                result = (from a in db.ARTICULO_ESPE
                          where a.ARTICULO == codigoArticulo
                          && a.ATRIBUTO == "REQUIERE_ARMADO"
                          select a).ToList();
            }

            return result;
        }

        /// <summary>
        /// Función estática y privada que consulta hacia la base de datos la información de la categoría
        /// que posee un artículo
        /// </summary>
        /// <param name="codigoArticulo">Código de artículo a consultar</param>
        /// <returns>retorna la información encapsulado en el objeto List<ARTICULO_ESPE></returns>
        private static List<ARTICULO_ESPE> ConsultaCategoriaArticulo(string codigoArticulo)
        {
            List<ARTICULO_ESPE> result = new List<ARTICULO_ESPE>();

            using (EXACTUSERPEntities db = new EXACTUSERPEntities())
            {
                result = (from e in db.ARTICULO_ESPE
                          where e.ATRIBUTO == "TIENDA_CATEGORIA"
                             && e.ARTICULO == codigoArticulo
                          select e).ToList();
            }

            return result;
        }

        

        /// <summary>
        /// Función estática y privada que consulta hacia la base de datos la información de la categoría
        /// que posee una tienda
        /// </summary>
        /// <param name="codigoTienda">Código de tienda a consultar</param>
        /// <returns>Retorna el valor de categoría de la tienda</returns>
        private static string ObtenerCategoriaTienda(string codigoTienda)
        {
            string result = "";

            using (EXACTUSERPEntities db = new EXACTUSERPEntities())
            {
                MF_Cobrador store = db.MF_Cobrador.FirstOrDefault(y => y.COBRADOR == codigoTienda);
                if(store != null)
                    result = store.CATEGORIA;
            }

            return result;
        }
        #endregion
    }
}