﻿using FiestaNETRestServices.Content.Abstract;
using FiestaNETRestServices.Models;
using MF.Comun.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web;

namespace FiestaNETRestServices.DAL
{
    public class DALIdentidadCliente : MFDAL
    {
        private new static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        EXACTUSERPEntities Exactus = new EXACTUSERPEntities();

        /// <summary>
        /// Registrar la huella del cliente
        /// </summary>
        /// <param name="cliente">código de cliente</param>
        /// <param name="huella">byte [] huella</param>
        /// <returns>resultado de la operación.</returns>
        public bool RegistrarHuella(string cliente, string huella,string FotoHuella,string Usuario,string TiendaRegistro)
        {
            //Graba la huella del cliente
            bool resultado = false;
            try {
                log.Info(string.Format("Grabando en bdd la huella del cliente {0}", cliente));
                var qCliente = from cl in Exactus.MF_Cliente where cl.CLIENTE == cliente select cl;
                var qClienteF = from cl in Exactus.MF_FotoCliente where cl.Cliente == cliente select cl;

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    foreach (var item in qCliente)
                    {
                        //Formato de la huella como Template para comparar en el lector
                        item.HUELLA = Convert.FromBase64String(huella);
                    }
                    if (qClienteF.Count() > 0)
                    {
                        //cuando ya existe el registro de la foto
                        foreach (var item in qClienteF)
                        {
                            //Formato de la huella como foto
                            item.Huella = Convert.FromBase64String(FotoHuella);
                            item.FechaRegistro = DateTime.Now;
                        }
                    }
                    else
                    {
                        MF_FotoCliente mfFoto = new MF_FotoCliente
                        {
                            Cliente = cliente,
                            Huella = Convert.FromBase64String(FotoHuella),
                            FechaRegistro = DateTime.Now
                        };
                        Exactus.MF_FotoCliente.AddObject(mfFoto);
                    }
                    Exactus.SaveChanges();
                    transactionScope.Complete();
                    resultado = true;
                }

                resultado = true;
            } catch (Exception ex)
            {
                resultado = false;
                log.Error(string.Format("Problema al guardar la huella del cliente {0}, Problema {1}",cliente,ex.Message));
            }
            return resultado;
        }

        public IEnumerable<IdentidadCliente> ObtenerHuella(string _cliente)
        {
            List<IdentidadCliente> oList = new List<IdentidadCliente>();
            IdentidadCliente oHuella = null;
            try
            {
                log.Info(string.Format("Obteniendo huella de bdd"));
                var qCliente = from cl in Exactus.MF_Cliente
                               join ft in Exactus.MF_FotoCliente on cl.CLIENTE equals ft.Cliente
                               where cl.HUELLA != null && _cliente== cl.CLIENTE
                               select new
                               {
                                   cliente = cl.CLIENTE,
                                   nombreCliente = cl.PRIMER_NOMBRE + " " + cl.SEGUNDO_NOMBRE + " " + cl.PRIMER_APELLIDO + " " + cl.SEGUNDO_APELLIDO,
                                   foto = ft.Foto,
                                   fotoHuella = ft.Huella,
                                   huella = cl.HUELLA,
                                   dpi=cl.DPI

                               };
                if (qCliente.Count() > 0)
                {
                    string foto = string.Empty;
                    string fotoHuella = string.Empty;
                    string Huella = string.Empty;
                    foreach (var item in qCliente)
                    {
                        ///validando si hay fotos y huellas grabadas o solo huellas
                        foto = item.foto != null ? Convert.ToBase64String(item.foto) : "";
                        fotoHuella = item.fotoHuella != null ? Convert.ToBase64String(item.fotoHuella) : "";
                        Huella = item.huella != null ? Convert.ToBase64String(item.huella) : "";

                        oHuella=new IdentidadCliente(item.cliente, item.nombreCliente, foto, fotoHuella, Huella);
                        oHuella.NombreCompleto = item.nombreCliente;
                        oHuella.DPI = item.dpi;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(string.Format("Problema al obtener las huellas, Problema {0}", ex.Message));
            }
            if(oHuella!=null)
                oList.Add(oHuella);
            return oList;
        }
        public IEnumerable<IdentidadCliente> ObtenerHuella()
        {
            List<IdentidadCliente> lstHuellas = new List<IdentidadCliente>();
            try
            {
                log.Info(string.Format("Obteniendo huella de bdd"));
                var qCliente = from cl in Exactus.MF_Cliente join ft in Exactus.MF_FotoCliente on cl.CLIENTE equals ft.Cliente where cl.HUELLA != null
                               select new {
                                   cliente = cl.CLIENTE,
                                   nombreCliente=cl.PRIMER_NOMBRE+" "+cl.SEGUNDO_NOMBRE+" "+cl.PRIMER_APELLIDO+" "+cl.SEGUNDO_APELLIDO,
                                   foto=ft.Foto,
                                   fotoHuella=ft.Huella,
                                   huella=cl.HUELLA
                                    
                               };
                if (qCliente.Count() > 0)
                {
                    string foto=string.Empty;
                    string fotoHuella=string.Empty;
                    string Huella=string.Empty;
                    foreach (var item in qCliente)
                    {
                        ///validando si hay fotos y huellas grabadas o solo huellas
                        foto = item.foto != null ? Convert.ToBase64String(item.foto):"";
                        fotoHuella = item.fotoHuella != null ? Convert.ToBase64String(item.fotoHuella) : "";
                        Huella = item.huella != null ? Convert.ToBase64String(item.huella) : "";

                        lstHuellas.Add(new IdentidadCliente(item.cliente,item.nombreCliente, foto,fotoHuella, Huella));
                    }
                }
            }
            catch (Exception ex)
            {
               
                log.Error(string.Format("Problema al obtener las huellas, Problema {0}", ex.Message));
            }
            return lstHuellas.AsEnumerable();
        }

        /// <summary>
        /// Registrar la huella del cliente
        /// </summary>
        /// <param name="cliente">código de cliente</param>
        /// <param name="huella">byte [] huella</param>
        /// <returns>resultado de la operación.</returns>
        public bool RegistrarFoto(string cliente, string foto)
        {
            //Graba la huella del cliente
            bool resultado = false;
            try
            {
                log.Info(string.Format("Grabando en bdd la Foto del cliente {0}", cliente));
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    var qCliente = from cl in Exactus.MF_FotoCliente where cl.Cliente == cliente select cl;
                    if (qCliente.Count() > 0)
                    {
                        foreach (var item in qCliente)
                        {
                            item.Foto = Convert.FromBase64String(foto);
                        }
                    }
                    else
                    {
                        MF_FotoCliente fotoCliente = new MF_FotoCliente
                        {
                            Cliente = cliente,
                            Foto = Convert.FromBase64String(foto),
                            //TiendaRegistro=Tienda,
                            //UsuarioRegistro=Usuario,
                            FechaRegistro=DateTime.Now
                        };

                        Exactus.MF_FotoCliente.AddObject(fotoCliente);
                    }
                    Exactus.SaveChanges();
                    transactionScope.Complete();
                    resultado = true;
                }
               
            }
            catch (Exception ex)
            {
                resultado = false;
                log.Error(string.Format("Problema al guardar la foto del cliente {0}, Problema {1}", cliente, ex.Message));
            }
            return resultado;
        }

        public RegistroFoto ObtenerFoto(string parcliente)
        {
            RegistroFoto FotoCliente = new RegistroFoto();
            try
            {
                log.Info(string.Format("Obteniendo foto de bdd"));
                var qCliente = from cl in Exactus.MF_FotoCliente join c in Exactus.MF_Cliente on cl.Cliente equals c.CLIENTE  where  cl.Cliente == parcliente
                               select new {
                                        Nombre=c.PRIMER_NOMBRE+" "+c.SEGUNDO_NOMBRE+" "+c.PRIMER_APELLIDO+" "+c.SEGUNDO_APELLIDO,
                                        Cliente=cl.Cliente,
                                        Foto=cl.Foto,
                                        fotoHuella=cl.Huella
                                           };
                if (qCliente.Count() > 0)
                {
                    FotoCliente=new RegistroFoto(qCliente.First().Cliente, Convert.ToBase64String(qCliente.First().Foto),qCliente.First().Nombre);
                }
            }
            catch (Exception ex)
            {

                log.Error(string.Format("Problema al obtener la foto, Problema {0}", ex.Message));
            }
            return FotoCliente;
        }
    }
}