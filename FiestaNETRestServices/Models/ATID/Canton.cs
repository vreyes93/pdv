﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FiestaNETRestServices.Models.ATID
{
    public class Canton
    {
        public long Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}