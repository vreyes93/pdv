﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FiestaNETRestServices.Models.ATID
{
    public class Departamento
    {
        public long Codigo { get; set; }

        public string Descripcion { get; set; }

        public Ciudad[] Ciudades { get; set; }
    }
}