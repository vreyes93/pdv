﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FiestaNETRestServices.Models.ATID
{
    public class Pais
    {
        public long Codigo { get; set; }
        public string Descripcion { get; set; }

        public Departamento[] Departamentos { get; set; }
    }
}