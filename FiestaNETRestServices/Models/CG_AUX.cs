//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FiestaNETRestServices.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CG_AUX
    {
        public string GUID_ORIGEN { get; set; }
        public string TABLA_ORIGEN { get; set; }
        public string ASIENTO { get; set; }
        public int LINEA { get; set; }
        public string COMENTARIO { get; set; }
        public Nullable<byte> NoteExistsFlag { get; set; }
        public Nullable<System.DateTime> RecordDate { get; set; }
        public Nullable<System.Guid> RowPointer { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }
}
