//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FiestaNETRestServices.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CONSECUTIVO_CI
    {
        public CONSECUTIVO_CI()
        {
            this.AUDIT_TRANS_INV = new HashSet<AUDIT_TRANS_INV>();
            this.BODEGA = new HashSet<BODEGA>();
        }
    
        public string CONSECUTIVO { get; set; }
        public string ULTIMO_USUARIO { get; set; }
        public string DESCRIPCION { get; set; }
        public string MASCARA { get; set; }
        public string SIGUIENTE_CONSEC { get; set; }
        public string EDITABLE { get; set; }
        public string MULTIPLES_TRANS { get; set; }
        public string FORMATO_IMP { get; set; }
        public System.DateTime ULT_FECHA_HORA { get; set; }
        public string TODAS_TRANS { get; set; }
        public string TIPO { get; set; }
        public Nullable<byte> NoteExistsFlag { get; set; }
        public Nullable<System.DateTime> RecordDate { get; set; }
        public Nullable<System.Guid> RowPointer { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    
        public virtual USUARIO USUARIO { get; set; }
        public virtual ICollection<AUDIT_TRANS_INV> AUDIT_TRANS_INV { get; set; }
        public virtual ICollection<BODEGA> BODEGA { get; set; }
    }
}
