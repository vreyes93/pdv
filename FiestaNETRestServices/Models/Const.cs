﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FiestaNETRestServices.Models
{
    public class Const
    {
        public const string SI = "S";
        public const string NO = "N";

        /// <summary>
        /// Constante que almacena el código de respuesta OK para una 
        /// petición de un servicio REST.
        /// </summary>
        public const bool OK = true;


        /// <summary>
        /// Constante que almacena el valor NULL
        /// </summary>
        public const string _NULL_= "NULL";

        /// <summary>
        /// Constante que almacena el código de respuesta NOT_OK para una 
        /// petición de un servicio REST.
        /// </summary>
        public const bool NOT_OK = false;


        public const int CONFIGURACION_PDV_MODULOS = 11;
       
        //Catálogo de excepciones
        public const int DESEMBOLSOS_EXCLUIDOS = 48;
        /// <summary>
        /// Constante que almacena el código de respuesta NOT_OK para una 
        /// petición de un servicio REST.
        /// </summary>
        /// 

        public class Expediente
        {
            public const string ANULADO = "A";
            public const string DESEMBOLSADO = "D";
            public const string ENVIADO_FINANCIERA = "E";
            public const string INGRESADO_SISTEMA = "I";
            public const string RECHAZADO = "R";
        }

        public class CONSECUTIVO
        {
            public const string TRASPASOS = "TP001";
        }

        /// <summary>
        /// Constantes para el ambiente
        /// </summary>
        public class AMBIENTE
        {
            public const string LOCAL = "LOC";
            public const string DESARROLLO = "DES";
            public const string PRUEBAS = "PRU";
            public const string PRODUCCION = "PRO";
        }

        /// <summary>
        /// Constante que almacena el código de las financieras
        /// </summary>
        public class FINANCIERA
        {
            public const int INTERCONSUMO = 7;
            public const int BANCREDIT = 11;
            public const int CREDIPLUS = 12;
        }

        /// <summary>
        /// Constante que almacena el código de los catalogos de correos
        /// </summary>

        public class LISTA_CORREO
        {
            public const int DESEMBOLSOS = 3;
            public const int ENVIO_EXPEDIENTE = 4;
            public const int RESUMEN_DESEMBOLSO = 5;
            public const int EXPEDIENTES_PENDIENTES = 6;
            public const int PEDIDOS_DEL_DIA_CEMACO = 7;
            public const int VENTAS_DEL_DIA_CEMACO = 8;
            public const int IT_PDV_ERRORES = 10;
            public const int INTERCONSUMO_SOLICITUDES_PENDIENTES = 12;
            public const int INTERCONSUMO_DESEMBOLSOS_PARCIALES = 18;
            public const int INTERCONSUMO_SOLICITUDES_RECHAZADAS = 17;
            public const int REMITENTES_CORREO = 20;
            public const int FINANCIERAS_RECHAZOS_TARDIOS = 22;
            public const int FINANCIERAS_ALERTA_RECHAZOS_TARDIOS = 23;
            public const int MAYOREO_NOTIFICACIONES_SETS_INVALIDOS = 32;
            public const int CREDIPLUS_DESEMBOLSOS_PARCIALES = 18;
        }
        public class REMITENTE_CORREO
        {
            public const int REMITENTE_RESULTADO_SOLICITUD = 1;
            public const int REMITENTE_DESEMBOLSOS_PARCIALES = 2;
            public const int REMITENTE_SOLICITUDES_RECHAZADAS = 3;
            public const int REMITENTE_IT_PDV_ERRORES = 4;
            public const int REMITENTE_DESEMBOLSOS_REALIZADOS = 5;
            public const int REMITENTE_DESEMBOLSOS_PENDIENTES = 6;
            public const int REMITENTE_PEDIDOS_MAYOREO = 7;
            public const int REMITENTE_REPORTE_DESPACHOS_MAYOREO = 8;
            public const int REMITENTE_REPORTE_PEDIDOS_DEL_DIA = 9;
            public const int REMITENTE_REPORTE_VENTAS_TRES_MESES = 10;
            public const int REMITENTE_NOTIFICACION_RECHAZOS_TARDIOS = 11;           
        }

        /// <summary>
        /// Constante que almacena el código de los catalogos 
        /// </summary>

        public class CATALOGO
        {
            public const int REMITENTE_CORREOS_PDV = 20;
            public const int DESTINATARIOS_IT_ERRORES = 10;
            public const int COMISIONES_EXCEPCIONES_VENDEDORES = 14;
            public const int EXISTENCIAS_TOMA_INVENTARIO = 21;
            public const int DESTINATARIOS_ADICIONALES_AUTORIZACION_DESARMADO = 26;
            public const int TIPO_DE_SERVICIO_ARMADO_PEDIDOS = 36;
            public const int TIPO_DE_SERVICIO_ARMADO_TIENDA = 35;
            public const int CATALOGO_TIENDA_VIRTUAL_INVENTARIO_ARTICULOS = 52;
            public const int CATALOGO_TIENDA_VIRTUAL = 34;
            public const int CATALOGO_FINANCIERAS_HUELLA_FOTOGRAFIA = 38;
            public const int CATALOGO_TIENDAS_HUELLA_FOTO = 39;
            public const int CATALOGO_FACTURACION_EN_LINEA = 41;
        }

        public class TIENDA_VIRTUAL
        {
            public const string URL_TIENDA_VIRTUAL = "URL_TIENDA_VIRTUAL";
            public const string VERSION_API_REST_MAGENTO = "VERSION_API_REST_MAGENTO";
            public const string OBTENER_TOKEN_AUTHORIZACION = "OBTENER_TOKEN_AUTHORIZACION";
            public const string PRODUCTOS = "PRODUCTOS";
            public const string USUARIO_TIENDA_VIRTUAL = "USUARIO_TIENDA_VIRTUAL";
            public const string PASSWORD_TIENDA_VIRTUAL = "PASSWORD_TIENDA_VIRTUAL";
            public const string URL_TIENDA_VIRTUAL_PRUEBAS = "URL_TIENDA_VIRTUAL_PRUEBAS";
            public const string CANTIDAD_MINIMA_PRODUCTO = "CANTIDAD_MINIMA_PRODUCTO";

            //CLIENTES Y FACTURACIÓN
            public const string VENDEDOR_FACTURACION = "0003"; //JORGE MATA
            public const string COBRADOR_FACTURACION = "F01";
            public const string CATEGORIA_CLIENTE = "FV";
            public const string COBRADOR = "FV";
            public const string TIPO_VENTA_DEFAULT = "NR";
            public const int CONTA_SUB_TIPO_DOC = 0;
            public const string LOCALIZACION_DEFAULT = "CAJA";
            public const string CREDITO = "CR";
            public const string TIPO_REFERENCIA_PEDIDO = "NO";//NOS VIÓ EN PAGINA WEB
            public const string TIPO_LINEA_ARTICULO_NORMAL = "N";
            public const string TIPO_LINEA_ARTICULO_COMPONENTE_SET = "C";
            public const string TIPO_LINEA_ARTICULO_SET = "K";
        }
        /// <summary>
        /// Constante que almacena el código de los catalogos de correos
        /// </summary>

        public class MODULO
        {
            public const string INTERCONSUMO_PAGARE = "HABILITAR_INTERCONSUMO_PAGARE";
            public const string HABILITAR_REGLA_COMISIONES_FINANCIERAS = "HABILITAR_REGLA_COMISIONES_FINANCIERAS";
            public const string FINANCIERA_DEFAULT = "FINANCIERA_DEFAULT";
        }
        
        /// <summary>
        /// Constante que tipifican los servicios de Interconsumo
        /// </summary>
        public class SERVICIOS_INTERCONSUMO
        {          
            public const string ENVIAR_SOLICITUD_CREDITO = "ENVIAR_SOLICITUD_CREDITO";
            public const string CONSULTAR_SOLICITUD_CREDITO = "CONSULTAR_SOLICITUD_CREDITO";
            public const string CONSULTAR_DESEMBOLSO_EXPEDIENTE = "CONSULTAR_DESEMBOLSO_EXPEDIENTE";
            public const string CONSULTAR_SOLICITUDES_RECHAZADAS = "CONSULTAR_SOLICITUDES_RECHAZADAS";
        }


        /// <summary>
        /// Constante que tipifican los servicios de Interconsumo
        /// </summary>
        public class SERVICIOS_GUATEFAC
        {
            public const string GENERAR_DOCUMENTO = "GENERAR_DOCUMENTO";
            public const string ANULAR_DOCUMENTO = "ANULAR_DOCUMENTO";
        }

        /// <summary>
        /// Constante que tipifican los servicios de Interconsumo
        /// </summary>
        public class DOCUMENTO_GUATEFAC
        {
            public const string FACTURA = "1";
            public const string FACTURA_CAMBIARIA = "2";
            public const string FACTURA_PEQUENO_CONTRIB = "3";
            public const string FACTURA_CAMBIARIA_PEQUENO_CONTRIB = "4";
            public const string FACTURA_ESPECIAL = "5";
            public const string NOTA_ABONO = "6";
            public const string RECIBO_POR_DONACION = "7";
            public const string RECIBO = "8";
            public const string NOTA_DEBITO = "9";
            public const string NOTA_CREDITO = "10";
        }

        /// <summary>
        /// Constante que tipifican el estado de la solicitud de crédito de Interconsumo
        /// </summary>
        public class ESTADO_SOLICITUD_INTERCONSUMO
        {
            public const string DIFERIDO = "DIFERIDA";
            public const string PENDIENTE = "PENDIENTE";
            public const string APROBADO = "APROBADA";
            public const string RECHAZADO = "RECHAZADA";
            public const string PREAUTORIZADO = "PREAUTORIZADA";
        }

        public class CONTABILIDAD
        {
            public const string CONTROL_BANCARIO    = "CB";
            public const string CUENTAS_POR_COBRAR  = "CC";

        }

        public class REPLICATE
        {
            public const string LONGITUD_5 = "D5";
            public const string LONGITUD_6 = "D6";
            public const string LONGITUD_7 = "D7";
            public const string LONGITUD_8 = "D8";
        }

        public class CUENTA_CONTABLE
        {
            public const string CLIENTES                = "1-1-2-001-0001";
            public const string CAJA_MONEDA_NACIONAL    = "1-1-1-001-0001";
        }

        public class CUENTA_BANCARIA
        {
            public const string INTERNACIONAL = "1508-02391-3";
            public const string BAM = "02-0003419-5";
        }

        public class CENTRO_COSTOS
        {
            public const string OTROS_EGRESOS   = "0-000";
         
        }

        public class TIPO_ASIENTO
        {
            public const string RECIBO = "REC";
            public const string TRANSFERENCIA_CREDITO = "TRC";

        }

        public class DESEMBOLSO_INTERCONSUMO {
            public const string DESCRIPCION_PAGO = "Pago de Interconsumo Ptmo.";
            public const string NOMBRE_CONSECUTIVO = "RECINTER01";
            public const string FORMATO_CONSECUTIVO = "INTER-";

        }

        public class DESEMBOLSO_CREDIPLUS
        {
            public const string DESCRIPCION_PAGO = "Pago de Crediplus Ptmo.";
            public const string NOMBRE_CONSECUTIVO = "RECCRPLUS";
            public const string FORMATO_CONSECUTIVO = "CRPLUS-";

        }

        public class TIPO_DOCUMENTO {
            public const string PEDIDO = "PEDIDO";
            public const string COTIZACION = "COTIZACION";
        }
        /*Migrado del Portal para la autenticación de usuarios desde FiestaNetRESTServices*/
        public class ROLES
        {
            public const string RL_TIENDA_VENDEDOR = "RL_TIENDA_VENDEDOR";
            public const string RL_TIENDA_JEFE = "RL_TIENDA_JEFE";
            public const string RL_TIENDA_SUPERVISOR = "RL_TIENDA_SUPERVISOR";


            public const string RL_GERENTE = "RL_GERENTE";
            public const string RL_ADMINISTRADOR = "RL_ADMINISTRADOR";

            public const string RL_CONTABILIDAD_ASISTENTE = "RL_CONTABILIDAD_ASISTENTE";
            public const string RL_CONTABILIDAD_SUBJEFE = "RL_CONTABILIDAD_SUBJEFE";
            public const string RL_CONTABILIDAD_JEFE = "RL_CONTABILIDAD_JEFE";

            public const string RL_RRHH_ASISTENTE = "RL_RRHH_ASISTENTE";
            public const string RL_RRHH_SUBJEFE = "RL_RRHH_SUBJEFE";
            public const string RL_RRHH_JEFE = "RL_RRHH_JEFE";

            public const string RL_IMPORTACIONES_ASISTENTE = "RL_IMPORTACIONES_ASISTENTE";
            public const string RL_IMPORTACIONES_SUBJEFE = "RL_IMPORTACIONES_SUBJEFE";
            public const string RL_IMPORTACIONES_JEFE = "RL_IMPORTACIONES_JEFE";

            public const string RL_BODEGA_ASISTENTE = "RL_BODEGA_ASISTENTE";
            public const string RL_BODEGA_SUBJEFE = "RL_BODEGA_SUBJEFE";
            public const string RL_BODEGA_JEFE = "RL_BODEGA_JEFE";

            public const string RL_ARMADOS_ASISTENTE = "RL_ARMADOS_ASISTENTE";
            public const string RL_ARMADOS_SUBJEFE = "RL_ARMADOS_SUBJEFE";
            public const string RL_ARMADOS_JEFE = "RL_ARMADOS_JEFE";

            public const string RL_MERCADEO_ASISTENTE = "RL_MERCADEO_ASISTENTE";
            public const string RL_MERCADEO_SUBJEFE = "RL_MERCADEO_SUBJEFE";
            public const string RL_MERCADEO_JEFE = "RL_MERCADEO_JEFE";

            public const string RL_ADMINISTRACION_ASISTENTE = "RL_ADMINISTRACION_ASISTENTE";
            public const string RL_ADMINISTRACION_SUBJEFE = "RL_ADMINISTRACION_SUBJEFE";
            public const string RL_ADMINISTRACION_JEFE = "RL_ADMINISTRACION_JEFE";
        }

        public const string ACTIVO = "S";
        public const string INACTIVO = "N";

        /// <summary>
        /// Constante que almacena los conceptos de Friedman
        /// </summary>
        /// 
        public class FRIEDMAN
        {
            public class STATUS_QUETZ
            {
                public const int ESTRELLA = 1;
                public const int NEGRO = 2;
                public const int ROJO = 3;
            }

            public class ESTRATEGIA
            {
                public const string ELOGIO = "ELOGIO";
                public const string CFP = "CFP";
                public const string CAF = "CAF";
                public const string CVP = "CVP";
            }
        }



    }
}