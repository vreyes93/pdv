//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FiestaNETRestServices.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class DESPACHO_DETALLE
    {
        public string DESPACHO { get; set; }
        public short LINEA { get; set; }
        public string ARTICULO { get; set; }
        public string BODEGA { get; set; }
        public decimal CANTIDAD { get; set; }
        public string LOTE { get; set; }
        public string LOCALIZACION { get; set; }
        public decimal COSTO_LOCAL { get; set; }
        public decimal COSTO_DOLAR { get; set; }
        public string DOCUM_ORIG { get; set; }
        public string TIPO_DOCUM_ORIG { get; set; }
        public short LINEA_DOCUM_ORIG { get; set; }
        public Nullable<int> SERIE_CADENA { get; set; }
        public string TIPO_LINEA { get; set; }
        public Nullable<short> LINEA_KIT { get; set; }
        public Nullable<int> CONSEC_TINV_INI { get; set; }
        public Nullable<int> CONSEC_TINV_FIN { get; set; }
        public Nullable<byte> NoteExistsFlag { get; set; }
        public Nullable<System.DateTime> RecordDate { get; set; }
        public Nullable<System.Guid> RowPointer { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public decimal COSTO_LOCAL_COMP { get; set; }
        public decimal COSTO_DOLAR_COMP { get; set; }
    
        public virtual ARTICULO ARTICULO1 { get; set; }
        public virtual BODEGA BODEGA1 { get; set; }
        public virtual DESPACHO DESPACHO1 { get; set; }
        public virtual FACTURA_LINEA FACTURA_LINEA { get; set; }
    }
}
