//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FiestaNETRestServices.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class EXISTENCIA_BODEGA
    {
        public EXISTENCIA_BODEGA()
        {
            this.EXISTENCIA_LOTE = new HashSet<EXISTENCIA_LOTE>();
        }
    
        public string ARTICULO { get; set; }
        public string BODEGA { get; set; }
        public decimal EXISTENCIA_MINIMA { get; set; }
        public decimal EXISTENCIA_MAXIMA { get; set; }
        public decimal PUNTO_DE_REORDEN { get; set; }
        public decimal CANT_DISPONIBLE { get; set; }
        public decimal CANT_RESERVADA { get; set; }
        public decimal CANT_NO_APROBADA { get; set; }
        public decimal CANT_VENCIDA { get; set; }
        public decimal CANT_TRANSITO { get; set; }
        public decimal CANT_PRODUCCION { get; set; }
        public decimal CANT_PEDIDA { get; set; }
        public decimal CANT_REMITIDA { get; set; }
        public string CONGELADO { get; set; }
        public Nullable<System.DateTime> FECHA_CONG { get; set; }
        public string BLOQUEA_TRANS { get; set; }
        public Nullable<System.DateTime> FECHA_DESCONG { get; set; }
        public Nullable<byte> NoteExistsFlag { get; set; }
        public Nullable<System.DateTime> RecordDate { get; set; }
        public Nullable<System.Guid> RowPointer { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public decimal COSTO_UNT_PROMEDIO_LOC { get; set; }
        public decimal COSTO_UNT_PROMEDIO_DOL { get; set; }
        public decimal COSTO_UNT_ESTANDAR_LOC { get; set; }
        public decimal COSTO_UNT_ESTANDAR_DOL { get; set; }
        public decimal COSTO_PROM_COMPARATIVO_LOC { get; set; }
        public decimal COSTO_PROM_COMPARATIVO_DOLAR { get; set; }
    
        public virtual ARTICULO ARTICULO1 { get; set; }
        public virtual BODEGA BODEGA1 { get; set; }
        public virtual ICollection<EXISTENCIA_LOTE> EXISTENCIA_LOTE { get; set; }
    }
}
