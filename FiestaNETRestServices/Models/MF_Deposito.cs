//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FiestaNETRestServices.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MF_Deposito
    {
        public string CUENTA_BANCO { get; set; }
        public string TIPO_DOCUMENTO { get; set; }
        public decimal NUMERO { get; set; }
        public System.DateTime FECHA { get; set; }
        public string COBRADOR { get; set; }
        public decimal MONTO { get; set; }
        public string OBSERVACIONES { get; set; }
        public System.DateTime RecordDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<int> ID_DEPOSITO { get; set; }
        public string CERRADO { get; set; }
        public Nullable<int> CIERRE { get; set; }
    }
}
