//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FiestaNETRestServices.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MF_DesarmadoTienda
    {
        public int DESARMADO { get; set; }
        public string COBRADOR { get; set; }
        public string VENDEDOR { get; set; }
        public System.DateTime FECHA_DESARMADO { get; set; }
        public Nullable<int> NO_REQUISICION { get; set; }
        public string OBSERVACIONES { get; set; }
        public string ESTADO { get; set; }
        public string USUARIO_AUTORIZA { get; set; }
        public Nullable<System.DateTime> FECHA_HORA_AUTORIZA { get; set; }
        public string OBSERVACIONES_AUTORIZA { get; set; }
        public System.DateTime RecordDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string FACTURA { get; set; }
    }
}
