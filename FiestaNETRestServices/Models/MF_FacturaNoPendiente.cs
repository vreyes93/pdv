//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FiestaNETRestServices.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MF_FacturaNoPendiente
    {
        public string FACTURA { get; set; }
        public string COBRADOR { get; set; }
        public string VENDEDOR { get; set; }
        public System.DateTime FECHA_FACTURA { get; set; }
        public System.DateTime FECHA_INICIAL { get; set; }
        public System.DateTime FECHA_FINAL { get; set; }
        public System.DateTime RecordDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public System.DateTime CreateDate { get; set; }
    }
}
