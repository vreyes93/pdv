//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FiestaNETRestServices.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MF_Gerente
    {
        public int GERENTE { get; set; }
        public string NOMBRE { get; set; }
        public string GENERO { get; set; }
        public string USUARIO { get; set; }
        public string EMAIL { get; set; }
        public string AUTORIZA_PRECIO_ESPECIAL { get; set; }
        public string AUTORIZA_DIAS_DESCANSO { get; set; }
        public string INFORMAR_NO_RESPETO_PRECIO { get; set; }
        public string ENVIA_REPORTE_RECIBOS { get; set; }
        public string VER_UTILITARIOS { get; set; }
        public string VER_OPCIONES_GERENCIA { get; set; }
        public string ENVIAR_INVENTARIO { get; set; }
        public string CIERRA_INVENTARIO { get; set; }
        public string ENVIAR_INVENTARIO_TIENDA { get; set; }
        public string ENVIAR_PEDIDO_MAYOREO { get; set; }
        public string ENVIAR_PEDIDO_MAYOREO_BODEGA { get; set; }
        public string VER_OPCIONES_BODEGA { get; set; }
        public string DESPACHOS { get; set; }
        public string ADMINISTRACION { get; set; }
        public string ENVIA_VENTAS_HORA { get; set; }
        public string CONTABILIDAD { get; set; }
        public string AUTORIZA_PRECIO_ESPECIAL_SIN_COSTO { get; set; }
        public string ENVIA_SALDOS_MAYOREO { get; set; }
        public string ENVIA_VENTAS_V2 { get; set; }
        public string ANULA_RECIBOS { get; set; }
        public string VER_OPCIONES_IT { get; set; }
    }
}
