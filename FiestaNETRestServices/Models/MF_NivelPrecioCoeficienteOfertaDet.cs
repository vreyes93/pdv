//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FiestaNETRestServices.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MF_NivelPrecioCoeficienteOfertaDet
    {
        public string Tipo { get; set; }
        public string NivelPrecio { get; set; }
        public System.DateTime FechaOfertaDesde { get; set; }
        public string Articulo { get; set; }
        public decimal PrecioOriginal { get; set; }
    }
}
