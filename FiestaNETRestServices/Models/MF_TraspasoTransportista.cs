//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FiestaNETRestServices.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MF_TraspasoTransportista
    {
        public string TRASPASO { get; set; }
        public int TRANSPORTISTA { get; set; }
        public System.DateTime FECHA { get; set; }
        public System.DateTime RecordDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<int> ARTICULOS_VALIDOS { get; set; }
    }
}
