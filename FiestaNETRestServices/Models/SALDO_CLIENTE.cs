//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FiestaNETRestServices.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SALDO_CLIENTE
    {
        public string CLIENTE { get; set; }
        public string MONEDA { get; set; }
        public decimal SALDO { get; set; }
        public decimal SALDO_CORPORACION { get; set; }
        public decimal SALDO_SUCURSALES { get; set; }
        public System.DateTime FECHA_ULT_MOV { get; set; }
        public Nullable<byte> NoteExistsFlag { get; set; }
        public Nullable<System.DateTime> RecordDate { get; set; }
        public Nullable<System.Guid> RowPointer { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    
        public virtual CLIENTE CLIENTE1 { get; set; }
    }
}
