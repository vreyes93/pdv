//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FiestaNETRestServices.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ZONA
    {
        public ZONA()
        {
            this.CLIENTE = new HashSet<CLIENTE>();
            this.FACTURA = new HashSet<FACTURA>();
            this.PEDIDO = new HashSet<PEDIDO>();
        }
    
        public string ZONA1 { get; set; }
        public string NOMBRE { get; set; }
        public Nullable<byte> NoteExistsFlag { get; set; }
        public Nullable<System.DateTime> RecordDate { get; set; }
        public Nullable<System.Guid> RowPointer { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    
        public virtual ICollection<CLIENTE> CLIENTE { get; set; }
        public virtual ICollection<FACTURA> FACTURA { get; set; }
        public virtual ICollection<PEDIDO> PEDIDO { get; set; }
    }
}
