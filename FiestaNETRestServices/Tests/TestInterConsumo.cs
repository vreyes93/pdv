﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Controllers;
using WebAPI.Models;  
     
 

namespace FiestaNETRestServices.Tests
{
    [TestClass]
    public class TestInterConsumo
    {
       
        [TestMethod]  
        publicvoid EmployeeGetById()  
        {  
            // Set up Prerequisites   
            var controller = newEmployeeController();  
            controller.Request = newHttpRequestMessage();  
            controller.Configuration = newHttpConfiguration();  
            // Act on Test  
            var response = controller.Get(1);  
            // Assert the result  
            Employee employee;  
            Assert.IsTrue(response.TryGetContentValue < Employee > (out employee));  
            Assert.AreEqual("Jignesh", employee.Name);  
        }  
    }  
    }
}