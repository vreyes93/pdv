﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Text;
using System.Xml;

using MF_Clases;
using FiestaNETRestServices.Models;
using System.Web.Configuration;
using System.Net;
using System.Net.Mime;
using System.IO;
using System.Security.Cryptography;
using System.Configuration;


namespace FiestaNETRestServices.Utils
{

    /// <summary>
    /// Esta clase alberga funciones que son utilizadas en la mayoria de validaciones, formatos, etc.
    /// </summary>
    public class Utilitarios
    {
        //PRUEBA de commit

        /// <summary>
        /// Esta variable nos sirve para imprimir en el log la trazabilidad del sistema
        /// </summary>
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función valida la fecha respecto a los parametros dados, es decir valida una fecha como 2017-08-01 la cual
        /// devolveria verdadero ó 2017-45-78 la cual devolveria falso.
        /// </summary>
        /// <param name="Anio">Número correspondiente al año.</param>
        /// <param name="Mes">Número correspondiente al mes.</param>
        /// <param name="Dia">Número correspondiente al día.</param>
        /// <returns>Retorna true cuando la fecha es válida</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public bool FechaValida(string Anio, string Mes, string Dia)
        {
            DateTime Fecha;
            try
            {
                Fecha = new DateTime(Convert.ToInt32(Anio), Convert.ToInt32(Mes), Convert.ToInt32(Dia));
            }
            catch
            {
                return false;
            }

            return true;
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función valida la fecha respecto al parametro dado, es decir valida una fecha como 2017-08-01 la cual
        /// devolveria verdadero ó 2017-45-78 la cual devolveria falso.
        /// </summary>
        /// <param name="pFecha">String correspondiente a una fecha valida.</param>
        /// <returns>Retorna true cuando la fecha es válida</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public bool FechaValida(string pFecha)
        {
            string[] mFecha;
            try
            {
                if (string.IsNullOrEmpty(pFecha) && ((pFecha.Split('-').Length - 1) != 2))
                    return false;

                mFecha = pFecha.Split('-');
                DateTime Fecha = new DateTime(Int32.Parse(mFecha[0]), Int32.Parse(mFecha[1]), Int32.Parse(mFecha[2]));

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta devuelve una fecha valida con su tipo DiteTime
        /// </summary>
        /// <param name="pFecha">String correspondiente a una fecha valida.</param>
        /// <returns>Retorna true cuando la fecha es válida</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public DateTime Fecha(string pFecha)
        {
            string[] mFecha;
            try
            {
                mFecha = pFecha.Split('-');
                DateTime Fecha = new DateTime(Int32.Parse(mFecha[0]), Int32.Parse(mFecha[1]), Int32.Parse(mFecha[2]));

                return Fecha;
            }
            catch (Exception)
            {
                return new DateTime(1900, 1, 1);
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función devuelve el nombre del mes al recibir una fecha
        /// </summary>
        /// <param name="Fecha">Fecha a evaluar.</param>
        /// <returns>Retorna el nombre del mes</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public string Mes(DateTime fecha)
        {
            string mMes = "Enero";
            if (fecha.Month == 2) mMes = "Febrero";
            if (fecha.Month == 3) mMes = "Marzo";
            if (fecha.Month == 4) mMes = "Abril";
            if (fecha.Month == 5) mMes = "Mayo";
            if (fecha.Month == 6) mMes = "Junio";
            if (fecha.Month == 7) mMes = "Julio";
            if (fecha.Month == 8) mMes = "Agosto";
            if (fecha.Month == 9) mMes = "Septiembre";
            if (fecha.Month == 10) mMes = "Octubre";
            if (fecha.Month == 11) mMes = "Noviembre";
            if (fecha.Month == 12) mMes = "Diciembre";

            return mMes;
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función devuelve el nombre del mes al recibir una fecha
        /// </summary>
        /// <param name="Fecha">Fecha a evaluar.</param>
        /// <returns>Retorna el nombre del mes</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public string Mes(int mes)
        {
            string mMes = "";
            switch (mes)
            {
                case 1: mMes = "Enero"; break;
                case 2: mMes = "Febrero"; break;
                case 3: mMes = "Marzo"; break;
                case 4: mMes = "Abril"; break;
                case 5: mMes = "Mayo"; break;
                case 6: mMes = "Junio"; break;
                case 7: mMes = "Julio"; break;
                case 8: mMes = "Agosto"; break;
                case 9: mMes = "Septiembre"; break;
                case 10: mMes = "Octubre"; break;
                case 11: mMes = "Noviembre"; break;
                case 12: mMes = "Diciembre"; break;
                default: mMes = ""; break;

            }
            return mMes;
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función convierte la fecha respecto a los paramentros string, a una variable tipo fecha DateTime.
        /// </summary>
        /// <param name="Anio">Número correspondiente al año.</param>
        /// <param name="Mes">Número correspondiente al mes.</param>
        /// <param name="Dia">Número correspondiente al día.</param>
        /// <returns>Retorna una variable tipo DateTime.</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public DateTime ConvertirFecha(string Anio, string Mes, string Dia)
        {
            DateTime Fecha;
            try
            {
                Fecha = new DateTime(Convert.ToInt32(Anio), Convert.ToInt32(Mes), Convert.ToInt32(Dia));
            }
            catch
            {
                return new DateTime(1900, 1, 1); ;
            }

            return Fecha;
        }


        public bool FechaValida(int Anio, int mes, int dia)
        {
            return false;
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función le da formato DD/MM/YYYY a una fecha dada.
        /// </summary>
        /// <param name="Fecha">Fecha correspondiente a darle formato.</param>
        /// <returns>Retorna una variable tipo string con el formato DD/MM/YYYY.</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public string FormatoDDMMYYYY(DateTime? Fecha1)
        {

            if (Fecha1 != null)
            {
                DateTime Fecha;
                Fecha = ((DateTime)(Fecha1));
                return string.Format("{0}{1}/{2}{3}/{4}", Fecha.Day < 10 ? "0" : "", Fecha.Day.ToString(), Fecha.Month < 10 ? "0" : "", Fecha.Month.ToString(), Fecha.Year.ToString());
            }
            else
                return "---";
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función le da formato DD/MM/YYYY a una fecha dada.
        /// </summary>
        /// <param name="Fecha">Fecha correspondiente a darle formato.</param>
        /// <returns>Retorna una variable tipo string con el formato DD/MM/YYYY.</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public static string FormatoYYYYMMDD(DateTime? pFecha)
        {

            if (pFecha != null)
            {
                DateTime Fecha;
                Fecha = ((DateTime)(pFecha));
                return string.Format("{0}{1}{2}{3}{4}"
                    , Fecha.Year.ToString()
                    , Fecha.Month < 10 ? "0" : "", Fecha.Month.ToString()
                    , Fecha.Day < 10 ? "0" : "", Fecha.Day.ToString()
                    );
            }
            else
                return "---";
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función retorna el encabezado html para mandar el correo en dicho formato.
        /// </summary>
        /// <returns>Retorna una variable tipo string con el formato correspondiente.</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public string CorreoInicioFormatoCafe()
        {
            return "<!DOCTYPE html><html lang='en-us'><head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } .style1 { color: #8C4510; background-color: #FFF7E7; font-size: small; text-align: left; height:35px; } .style2 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: small; height:35px; } .style3 { color: #8C4510; background-color: #FFF7E7; font-size: small; text-align: center; height:35px; } .style4 { color: #8C4510; background-color: #FFF7E7; font-size: small; text-align: left; height:35px; } .style5 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: small; text-align: center; height:35px; } .style6 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: small; text-align: right; height:35px; }  .style7 { color: #8C4510; background-color: #FFF7E7; font-size: small; text-align: right; height:35px; }</style></head><body>";
        }

        public string CorreoInicioFormatoAzul()
        {
            return "<!DOCTYPE html><html lang='en-us'><head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } .style1 { color: #000000; background-color: #FFFFFF; font-size: xx-small; text-align: left; height:35px; } .style2 { font-weight: bold; color: #FFFFFF; background-color: #428BCA; font-size: x-small; height:35px; } .style3 { color: #000000; background-color: #FFFFFF; font-size: x-small; text-align: center; height:35px; } .style4 { color: #8C4510; background-color: #FFF7E7; font-size: small; text-align: left; height:35px; } .style5 { font-weight: bold; color: #FFFFFF; background-color: #428BCA; font-size: x-small; text-align: center; height:35px; } .style6 { font-weight: bold; color: #FFFFFF; background-color: #428BCA; font-size: x-small; text-align: right; height:35px; }  .style7 { color: #000000; background-color: #FFFFFF; font-size: x-small; text-align: right; height:35px; }</style></head><body>";
        }

        public string CorreoInicioFormatoRojo()
        {
            return "<!DOCTYPE html><html lang='en-us'><head><style type='text/css'> table, td { border: thin solid #BDBDBD;} p { font-family: Verdana; font-size: 14px; padding-left:5em}  table { border-collapse: collapse;  font-family: Verdana; align:center; font-size: 12px; padding-left:55em} .style1 { color: #000000; background-color: #efeded;  font-size: 12px; text-align: left; height:35px; font-family: Verdana;} .style2 { font-weight: bold; color: #FFFFFF; background-color: #F44336; font-weight: bold; font-size: 12px; height:35px; font-family: Verdana;} .style3 { color: #000000; background-color: #efeded; font-size: 12px; text-align: center; height:35px;font-family: Verdana; } .style4 { color: #FFFFFF; background-color: #F44336; font-size: 12px; text-align: left; height:35px; padding-left:5px; padding-right:5px; font-family: Verdana;} .style5 {  color: #FFFFFF; background-color: #F44336; font-size: 12px; text-align: center; height:35px; font-family: Verdana;} .style6 { font-weight: bold; color: #FFFFFF; background-color: #F44336; font-size: 12px text-align: right;  height:35px; font-family: Verdana;}  .style7 { color: #000000; background-color: #efeded; font-size: 12px text-align: right; height:35px;  font-family: Verdana;} .FondoBlancoD { color: #000000; background-color: #ffffff; font-size: 12px text-align: right; height:35px; font-family: Verdana;}</style></head><body >";
        }

        public string CorreoInicioFormatoAzulYGris()
        {
            return "<!DOCTYPE html><html lang='en-us'><head><style type='text/css'> table, td { border: thin solid #D3D3D3;} table { border-collapse: collapse; } .style1 { color: #000000; background-color: #FFFFFF; font-size: xx-small; text-align: left; height:35px; } .style2 { font-weight: bold; color: #FFFFFF; background-color: #428BCA; font-size: x-small; height:35px; } .style3 { color: #000000; background-color: #FFFFFF; font-size: x-small; text-align: center; height:35px; } .style4 { color: #8C4510; background-color: #FFF7E7; font-size: small; text-align: left; height:35px; } .style5 { font-weight: bold; color: #FFFFFF; background-color: #428BCA; font-size: small; font-family: Arial, Helvetica, sans-serif; text-align: center; height:35px; } .style6 { font-weight: bold; color: #FFFFFF; background-color: #428BCA; font-size: x-small; text-align: right; height:35px; }  .style7 { color: #000000; background-color: #FFFFFF; font-size: x-small; text-align: right; height:35px; }</style></head><body style='font-family: Arial, Helvetica, sans-serif;'>";
        }
        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función retorna la finalizacion html para mandar el correo en dicho formato.
        /// </summary>
        /// <returns>Retorna una variable tipo string con el formato correspondiente.</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public string CorreoFin()
        {
            return "</body></html>";
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función retorna el formato de un TD, para un titulo a la izquierda.
        /// </summary>
        /// <returns>Retorna una variable tipo string con el formato correspondiente.</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public string FormatoTituloIzquierdo(string texto)
        {
            return string.Format("<td class='style2'>&nbsp;{0}&nbsp;</td>", texto);
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función retorna el formato de un TD, para un titulo al centro
        /// </summary>
        /// <returns>Retorna una variable tipo string con el formato correspondiente.</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public string FormatoTituloCentro(string texto)
        {
            return string.Format("<td class='style5'>&nbsp;{0}&nbsp;</td>", texto);
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función retorna el formato de un TD, para un titulo a la derecha.
        /// </summary>
        /// <returns>Retorna una variable tipo string con el formato correspondiente.</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public string FormatoTituloDerecha(string texto)
        {
            return string.Format("<td class='style6'>&nbsp;{0}&nbsp;</td>", texto);
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función retorna el formato de un TD, para una celda a la izquierda.
        /// </summary>
        /// <returns>Retorna una variable tipo string con el formato correspondiente.</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public string FormatoCuerpoIzquierda(string texto)
        {
            return string.Format("<td class='style1'>&nbsp;{0}&nbsp;</td>", texto);
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función retorna el formato de un TD, para una cekda al centro.
        /// </summary>
        /// <returns>Retorna una variable tipo string con el formato correspondiente.</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public string FormatoCuerpoCentro(string texto)
        {
            return string.Format("<td class='style3'>&nbsp;{0}&nbsp;</td>", texto);
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función retorna el formato de un TD, para una cekda a la derecha.
        /// </summary>
        /// <returns>Retorna una variable tipo string con el formato correspondiente.</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public string FormatoCuerpoDerecha(string texto)
        {
            return string.Format("<td class='style7'>&nbsp;{0}&nbsp;</td>", texto);
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función retorna el formato de un numero decimal a entero.
        /// </summary>
        /// <returns>Retorna una variable tipo string con el formato correspondiente.</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public string FormatoNumeroEntero(decimal numero)
        {
            return string.Format("{0:0}", numero);
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función retorna el formato de un numero decimal a #,###.##
        /// </summary>
        /// <returns>Retorna una variable tipo string con el formato correspondiente.</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public string FormatoNumeroDecimal(decimal numero)
        {
            return string.Format("{0:0,0.00}", numero);
        }


        
        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función envia un correo electrónico del remitente a sus destinatarios con asunto y cuerpo del mensaje.
        /// </summary>
        /// <param name="Remitente">Correo electrónico del remitente.</param>
        /// <param name="CuentaCorreo">Correo electrónico del destinatario.</param>
        /// <param name="Asunto">Asunto del correo electrónico.</param>
        /// <param name="Mensaje">Mensaje o cuerpo del mensaje electrónico.</param>
        /// <param name="DisplayName">Nombre a desplegar en el correo electrónico.</param>
        /// <returns>Retorna una variable tipo Respuesta; 
        ///          Respuesta.exito = true si es existoso
        ///          Respuesta.exito = false si es fallido </returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public Respuesta EnviarEmail(string Remitente, string CuentaCorreo, string Asunto, StringBuilder Mensaje, string DisplayName)
        {
            List<string> ListaCuentaCorreo = new List<string>();
            ListaCuentaCorreo.Add(CuentaCorreo);
            return EnviarEmail(Remitente, ListaCuentaCorreo, Asunto, Mensaje, DisplayName);
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función envia un correo electrónico del remitente a sus destinatarios con asunto y cuerpo del mensaje.
        /// </summary>
        /// <param name="Remitente">Correo electrónico del remitente.</param>
        /// <param name="CuentaCorreo">Listado de correos electrónicos de los destinatarios.</param>
        /// <param name="Asunto">Asunto del correo electrónico.</param>
        /// <param name="Mensaje">Mensaje o cuerpo del mensaje electrónico.</param>
        /// <param name="DisplayName">Nombre a desplegar en el correo electrónico.</param>
        /// <param name="lstImagenes">Lista de imagenes a utilizarse como contenido en el correo, es opcional</param>
        /// <param name="BccTo">destinatario de correo para control</param>
        /// <returns>Retorna una variable tipo Respuesta; 
        ///          Respuesta.exito = true si es existoso
        ///          Respuesta.exito = false si es fallido </returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public Respuesta EnviarEmail(string Remitente, List<string> CuentaCorreo, string Asunto, StringBuilder Mensaje,string DisplayName,List<ImagenAdjunta> lstImagenes=null,List<string> BccTo=null, List<Attachment> adjuntos=null)
        {
            string mBitacora = "";
            string mSlack = "";


            SmtpClient smtp = new SmtpClient();
            MailMessage mail = new MailMessage();
            #region "tratamiento de imagenes"
            if (lstImagenes != null)
            {
                //crear una vista alternativa a partir del mensaje en HTML
                AlternateView av1 = AlternateView.CreateAlternateViewFromString(
                Mensaje.ToString(),
                null, MediaTypeNames.Text.Html);

                //now add the AlternateView

                foreach(var img in lstImagenes)
                av1.LinkedResources.Add(img);

                //now append it to the body of the mail
                mail.AlternateViews.Add(av1);
                mail.IsBodyHtml = true;
            }
            #endregion


            //archivos adjuntos
            if (adjuntos != null)
            {
                adjuntos.ForEach(x=> {
                    mail.Attachments.Add(x);
                });
            }
            
            //-------------------------------------------------------------------------------------------
            //Agregar la lista de correo
            foreach (var item in CuentaCorreo)
            {
                mail.To.Add(item);
            }
            //bcc
            if (BccTo != null)
                BccTo.ForEach(x => mail.Bcc.Add(x));
                

            //-------------------------------------------------------------------------------------------
            //Establecer el remitente y el nombre a mostrar
            mail.From = new MailAddress(Remitente, DisplayName);

            //-------------------------------------------------------------------------------------------
            //Establecer a quien se le responerá
            mail.ReplyToList.Add(Remitente);

            //-------------------------------------------------------------------------------------------
            //Establecer el Asunto del correo
            mail.Subject = Asunto;

            //-------------------------------------------------------------------------------------------
            //Establecer el cuerpo del mensaje como html
            mail.IsBodyHtml = true;
            mail.Body = Mensaje.ToString();

            //-------------------------------------------------------------------------------------------
            //Establecer el host y puerto del servidor de correo
            smtp.Host = WebConfigurationManager.AppSettings["MailHost"];
            smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort"]);
           
            smtp.UseDefaultCredentials = false;
            //-------------------------------------------------------------------------------------------
            //Habilitar el protocolo SSL y el metodo de entrega
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

            mBitacora = string.Format(" {0}; {1}; {2}; {3}; ", "Correo", Remitente, string.Join(",", CuentaCorreo), Asunto);
            mSlack = string.Format("Clase: {0} \nRemitente:{1} \nLista de correo:{2} \nAsunto:{3} ", this.GetType().FullName , Remitente, string.Join(",", CuentaCorreo), Asunto);
            smtp.Timeout = 5000;
            //-------------------------------------------------------------------------------------------
            //Se envia el correo, tras tres intentos fallidos envia por slack un mensaje

            try
            {//Primer intento para enviar correo
                smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail"], WebConfigurationManager.AppSettings["PwdMail"]);
                smtp.Send(mail);
                log.Info(mBitacora);
            }
            catch (Exception exi)
                    {//Envia notificaciones por slack y registra error en log.
                
                    log.Error("CORREO-MF ERRROR: " + exi.Message + " " + mBitacora);
                
                        try
                        {//Intento con la cuenta de contingencia de productos multiples

                            //Establecer el host y puerto del servidor de correo
                            smtp.Host = WebConfigurationManager.AppSettings["MailHost2"];
                            smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort2"]);
                        
                        //-------------------------------------------------------------------------------------------
                        //Habilitar el protocolo SSL y el metodo de entrega
                        smtp.EnableSsl = true;
                            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                            smtp.Timeout = 10000;
                            smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail2"], WebConfigurationManager.AppSettings["PwdMail2"]);
                            smtp.Send(mail);
                            log.Info("ENVIO POR CONTINGENCIA " + mBitacora);
                        }
                        catch (Exception e2)
                        {
                            Respuesta mRespuesta2 = new Respuesta(false, e2, "");
                            log.Error("CORREO CONTINGENCIA ERROR: " + string.Format(mBitacora + " {0};", mRespuesta2.Mensaje));
                            EnviarMensajeSlack(string.Format(mSlack + " \nERROR correo contingencia - {0};", mRespuesta2.Mensaje));
                            return mRespuesta2;
                        }
                        //--------------------------------------------------------------------------------------------------------
                    }
            
            return new Respuesta(true);
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función envia un mensaje por slack al canal de errores del PV
        /// </summary>
        /// <param name="Mensaje">Mensaje a enviar por Slack.</param>
        //-----------------------------------------------------------------------------------------------------------------------
        public static void EnviarMensajeSlack(string Mensaje)
        {
            //----------------------------------------------------------------
            //Validamos si esta habilitado el modulo
            //----------------------------------------------------------------
            string json = "";
            try
            {
                WebRequest request = WebRequest.Create(string.Format("{0}", WebConfigurationManager.AppSettings["ErrorPDVSlackChannel"]));
                json = "{" + string.Format(" \"text\": \"Alerta desde el PDV. \n{0}.\" ", Mensaje.Replace("{","").Replace("}","")) + "}";
                byte[] data = Encoding.ASCII.GetBytes(json);

                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                //var response = (HttpWebResponse)request.GetResponse();
                //var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            }

            catch (Exception e)
            {
                Respuesta mRespuesta = new Respuesta(false, e, "");
                log.Error(string.Format("ERROR {0};", mRespuesta.Mensaje));
            }
        }


        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función envia un correo electrónico del remitente a sus destinatarios con asunto y cuerpo del mensaje.
        /// </summary>
        /// <param name="Remitente">Correo electrónico del remitente.</param>
        /// <param name="CuentaCorreo">Listado de correos electrónicos de los destinatarios.</param>
        /// <param name="Asunto">Asunto del correo electrónico.</param>
        /// <param name="Mensaje">Mensaje o cuerpo del mensaje electrónico.</param>
        /// <param name="DisplayName">Nombre a desplegar en el correo electrónico.</param>
        /// <returns>Retorna una variable tipo Respuesta; 
        ///          Respuesta.exito = true si es existoso
        ///          Respuesta.exito = false si es fallido </returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public void EnviarEmailIT(string Origen,string Mensaje)
        {
            try
            {
                EXACTUSERPEntities dbExactus = new EXACTUSERPEntities();
                string mRemitente =  Utils.Utilitarios.GetRemitenteNotificacion(Const.REMITENTE_CORREO.REMITENTE_IT_PDV_ERRORES, Const.LISTA_CORREO.REMITENTES_CORREO);
                string mDisplayName = "ErrorPDV";

                //---------------------------------------------------------------------------------------------
                //Redactamos el asunto del correo
                //---------------------------------------------------------------------------------------------
                string mAsunto = string.Format("Error - {0} ", Origen);

                //---------------------------------------------------------------------------------------------
                //Agregamos a los destinatario
                //---------------------------------------------------------------------------------------------
                List<string> mCuentaCorreo = new List<string>();

                //---------------------------------------------------------------------------------------------
                //Variable para el cuerpo del correo
                //---------------------------------------------------------------------------------------------
                StringBuilder mMensaje = new StringBuilder();

                mMensaje.Append(CorreoInicioFormatoCafe());

                mMensaje.Append(string.Format("<p>Se ha disparado un error en {0} - Error: {1}</p>", Origen, Mensaje));
                var qListaCorreo =
                                           from mf_c in dbExactus.MF_Catalogo
                                           //Catalogo de correos de IT para soporte
                                           where mf_c.CODIGO_TABLA == Const.LISTA_CORREO.IT_PDV_ERRORES
                                           select mf_c;

                if (qListaCorreo.Count() > 0)
                {
                    foreach (var item in qListaCorreo)
                    {
                        mCuentaCorreo.Add(item.NOMBRE_CAT);
                    }
                }

                mMensaje.Append(CorreoFin());

                //---------------------------------------------------------------------------------------------------------
                //Procedemos a enviar el correo.
                //---------------------------------------------------------------------------------------------------------
                EnviarEmail(mRemitente, mCuentaCorreo, mAsunto, mMensaje, mDisplayName);
            }
            catch
            {
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función tiene como propósito validar si el modulo está habilitado, es decir, la funcionalidad del mismo.
        /// </summary>
        /// <param name="Modulo">Nombre del módulo a validar.</param>
        /// <returns>Retorna una variable booleana
        ///          true  = si esta habilitado
        ///          false = si no esta habilitado</returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public bool ModuloDelSistemaHabilitado(string Modulo)
        {
            try
            {
                EXACTUSERPEntities dbExactus = new EXACTUSERPEntities();

                var qModuloHabilitado =     from MC in dbExactus.MF_Catalogo
                                               //Catalogo de modulos habilitados o no.
                                           where    MC.CODIGO_TABLA == Const.CONFIGURACION_PDV_MODULOS
                                                    && MC.CODIGO_CAT == Modulo
                                           select MC;

                if (qModuloHabilitado.Count() > 0)
                {
                    foreach (var item in qModuloHabilitado)
                    {
                        return item.NOMBRE_CAT == "S" ? true : false;
                    }
                }
            }
            catch
            {
            }
            return false;
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función tiene como objetivo agregar a la variable mCuentaCorreo los correos registrados en la tabla.
        /// </summary>
        /// <param name="ListaCorreo">Parametro para tipificar el listado de correo.</param>
        /// <param name="mCuentaCorreo">Listado de correos electrónicos de los destinatarios.</param>
        //-----------------------------------------------------------------------------------------------------------------------
        public void GetListaCorreo(Int32 ListaCorreo, ref List<string> mCuentaCorreo)
        {
            try
            {
                EXACTUSERPEntities dbExactus = new EXACTUSERPEntities();
                //---------------------------------------------------------------------------------------------
                //Agregamos a los destinatario
                //---------------------------------------------------------------------------------------------
                //List<string> mCuentaCorreo = new List<string>();

                var qListaCorreo = from mf_c in dbExactus.MF_Catalogo
                                        //Catalogo de correos de IT para soporte
                                    where mf_c.CODIGO_TABLA == ListaCorreo
                                    select mf_c;

                if (qListaCorreo.Count() > 0)
                {
                    foreach (var item in qListaCorreo)
                    {
                        mCuentaCorreo.Add(item.NOMBRE_CAT);
                    }
                }

                //return new Respuesta(true, "", mCuentaCorreo);
            }
            catch
            {
                //return new Respuesta(false, "" + e.StackTrace);
            }
        }

        /// <summary>
        /// Función para obtener el remitente de una lista de correos de forma parametrizada
        /// </summary>
        /// <param name="Notificacion_PDV"></param>
        /// <param name="Categoria_Catalogo"></param>
        /// <returns></returns>
        public static string GetRemitenteNotificacion(int Notificacion_PDV,int Categoria_Catalogo)
        {
            string strRemitente = string.Empty;
            try
            {
                EXACTUSERPEntities dbExactus = new EXACTUSERPEntities();
                //---------------------------------------------------------------------------------------------
                //Obtenemos el remitente configurado en la tabla MF_Catalogo
                //---------------------------------------------------------------------------------------------
                //
                string strCODIGO_Cat = Notificacion_PDV.ToString();
                var qRemitenteCorreo = from mf_c in dbExactus.MF_Catalogo
                                   where mf_c.CODIGO_TABLA == Categoria_Catalogo
                                   && mf_c.CODIGO_CAT == strCODIGO_Cat
                                   select new { REMITENTE=mf_c.NOMBRE_CAT };

                if (qRemitenteCorreo.Count() > 0)
                {
                    strRemitente = qRemitenteCorreo.FirstOrDefault().REMITENTE;
                }
            }
            catch
            {
                strRemitente = "puntodeventa@productosmultiples.com";
            }
            return strRemitente;
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Desencripta el texto enviado.
        /// </summary>
        /// <param name="toEncrypt">Mensaje a desencriptar</param>
        /// /// <param name="useHashing">Utilizar metodo Hash</param>
        /// <returns>Mensaje desencriptado</returns>
        public static string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            //-------------------------------------------------------------------
            //Obtenemos la "llave secreta" para poder encriptar el textos con MD5
            string key = ConfigurationManager.AppSettings["DefaultKeySecurity"].ToString();

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
            {
                keyArray = UTF8Encoding.UTF8.GetBytes(key);
            }

            //-------------------------------------------------------------------
            //Utilizamos el alcoritmo Triple DES para desencriptar
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(
                                 toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        /// <summary>
        /// Función para devolver si el usuario es supervisor o no
        /// </summary>
        /// <param name="usuario">Nombre del usuario a evaluar</param>
        /// <returns>bool</returns>
        public bool EsSupervisor(string usuario)
        {
            bool mRetorna = false;
            EXACTUSERPEntities db = new EXACTUSERPEntities();

            var q = from c in db.MF_Cobrador join u in db.USUARIO on c.CODIGO_SUPERVISOR equals u.CELULAR where c.ACTIVO == "S" && u.USUARIO1 == usuario select c;
            if (q.Count() > 0) mRetorna = true;

            return mRetorna;
        }

        /// <summary>
        /// Función para devolver si el usuario es jefe o no
        /// </summary>
        /// <param name="usuario">Nombre del usuario a evaluar</param>
        /// <returns>bool</returns>
        public bool EsJefe(string usuario)
        {
            bool mRetorna = false;
            EXACTUSERPEntities db = new EXACTUSERPEntities();

            var q = from c in db.MF_Cobrador join u in db.USUARIO on c.CODIGO_JEFE equals u.CELULAR where c.ACTIVO == "S" && u.USUARIO1 == usuario select c;
            if (q.Count() > 0) mRetorna = true;

            return mRetorna;
        }

        /// <summary>
        /// Devuelve el valor del iva +1
        /// p.e. 1.12
        /// </summary>
        /// <returns></returns>
        public static decimal ObtenerIva()
        {
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            return 1 + ((from im in db.IMPUESTO where im.IMPUESTO1 == "IVA" select im).First().IMPUESTO11 / 100);
        }

        public static string GetATIDURLServices()
        {
            switch(WebConfigurationManager.AppSettings["Ambiente"])
            {
                case
                    Const.AMBIENTE.LOCAL:       return "http://cpweb1.cp.local/ATIDServices";
                case
                    Const.AMBIENTE.DESARROLLO:  return "http://cpweb1.cp.local/ATIDServices";
                case
                    Const.AMBIENTE.PRUEBAS:     return "http://cpweb1.cp.local/ATIDServices";
                case
                    Const.AMBIENTE.PRODUCCION:  return "http://cpweb1.cp.local/ATIDServices";

                default: return "http://cpweb1.cp.local/ATIDServices";

            }
        }

        public static string FormatoNIT(string nit)
        {
            return nit.Substring(0, nit.Length - 1) + "-" + nit.Substring(nit.Length - 1, 1);
        }
    }
}