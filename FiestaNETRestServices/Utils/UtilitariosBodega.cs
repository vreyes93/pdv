﻿using FiestaNETRestServices.Models;
using MF_Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static MF_Clases.Clases;

namespace FiestaNETRestServices.Utils
{
    public class UtilitariosBodega
    {
        internal static List<ArticuloInventario> FilterData(List<ArticuloInventario> data, List<int> clasificacion)
        {
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            if (clasificacion == null || clasificacion.Count == 0)
                throw new Exception("Filtro es requerido.");

            List<MF_ClasificacionDetalle> dataClass = db.MF_ClasificacionDetalle
                .Where(x => clasificacion.Contains(x.MF_CLASIFICACION))
                .ToList();

            return dataClass
                .Join(data, c => c.CLASIFICACION, d => d.Clasificacion, (c, d) => new { c, d })
                .Select(x => x.d)
                .ToList();
        }

        internal static List<ArticuloInventario> OrderData(List<ArticuloInventario> data, string orderBy)
        {
            EXACTUSERPEntities db = new EXACTUSERPEntities();
            if (string.IsNullOrEmpty(orderBy) || string.IsNullOrWhiteSpace(orderBy))
                throw new Exception("Factor para ordenar datos es requerido.");

            IEnumerable<ArticuloInventario> query = data
                .GroupJoin(db.MF_ClasificacionDetalle, a => a.Clasificacion, d => d.CLASIFICACION, (a, d) => new { a, d = d.DefaultIfEmpty() })
                .GroupJoin(db.MF_Clasificacion, a1 => a1.d.FirstOrDefault() == null ? 18 : a1.d.FirstOrDefault().MF_CLASIFICACION, c => c.MF_CLASIFICACION1, (a1, c) => new { a1, c = c.DefaultIfEmpty() })
                .Select(x => new ArticuloInventario()
                {
                    OrdenClasificacion = x.c.FirstOrDefault() == null ? 160 : (int)x.c.FirstOrDefault().ORDEN,
                    Articulo = x.a1.a.Articulo,
                    Blanco1 = x.a1.a.Blanco1,
                    Blanco2 = x.a1.a.Blanco2,
                    Clasificacion = x.c.FirstOrDefault() == null ? "Otros" : x.c.FirstOrDefault().DESCRIPCION,
                    Conteo = x.a1.a.Conteo,
                    Descripcion = x.a1.a.Descripcion,
                    Diferencia = x.a1.a.Diferencia,
                    ExistenciaFisica = x.a1.a.ExistenciaFisica,
                    Existencias = x.a1.a.Existencias,
                    Observaciones = x.a1.a.Observaciones,
                    Tipo = x.a1.a.Tipo,
                    UltimaCompra = x.a1.a.UltimaCompra,
                });

            switch (orderBy.ToUpper())
            {
                case "CLASIFICACION":
                    query = query.OrderBy(x => x.OrdenClasificacion).ThenBy(x => x.Descripcion);
                    break;
                case "DESCRIPCION":
                    query = query.OrderBy(x => x.Descripcion);
                    break;
            }

            return query.ToList();
        }
        /*
        internal static List<ArticuloInventario> FilterData(List<ArticuloInventario> mInventario, List<int> list)
        {
            throw new NotImplementedException();
        }*/
    }
}