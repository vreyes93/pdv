﻿//using FiestaNETRestServices.Models;
using FiestaNETRestServices.DAL;
using MF_Clases;
using MF_Clases.GuateFacturas.FEL;
using MF_Clases.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Xml;
using static MF_Clases.Clases;

namespace FiestaNETRestServices.Utils
{
    public class UtilitariosGuateFacturas
    {

        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función tiene como propósito consumir el servicio de interconsumo, segun los parametros definidos.
        /// </summary>
        /// <param name="Servicio">Servio a utilizar para consumir el servicio respectivo.</param>
        /// <param name="TipoDocumento">Documento a utilizar.</param>
        /// <param name="XML">XML a enviar al servicio.</param>
        /// <returns>Retorna un XML con la respuesta correspondiente. </returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public Resultado Servicio(string Servicio, Object Documento)
        {
            string mAmbiente = WebConfigurationManager.AppSettings["Ambiente"];

            DALComunes mComunes = new DALComunes();
            Type TYPE_ANULACION = typeof(AnulacionDoc);
            Type TYPE_DOC = typeof(Doc);

            Doc mDocumento = new Doc();
            AnulacionDoc mAnulacion = new AnulacionDoc();

            Resultado mResultado = new Resultado();
            string respuestaGuateFacturas = "";

            try
            {
                mResultado.ErrorComunicacion = "N";
                // - ---------------------------------------------------------------------------------
                // Definimos el protocolo de seguridad TLS 1.2
                // - ---------------------------------------------------------------------------------
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;


                // - ---------------------------------------------------------------------------------
                // Definimos la variable para el servicio
                // - ---------------------------------------------------------------------------------
                guateFacturas.FEL.Guatefac ws = new guateFacturas.FEL.Guatefac();


                // - ---------------------------------------------------------------------------------
                // Definimos la variable del URL con el WSDL
                // - ---------------------------------------------------------------------------------
                //mComunes.ObtenerCatalogo(CONSTANTES.CATALOGOS.GUATEFACTURAS_FEL);

                string mUrl = WebConfigurationManager.AppSettings["URLGuateFacturasWSFEL"];
                string mUsuario = WebConfigurationManager.AppSettings["UsrGuateFacturasBasicAuthFEL"];
                string mPassword = WebConfigurationManager.AppSettings["PwdGuateFacturasBasicAuthFEL"];

                if (mAmbiente == "DES" || mAmbiente == "PRU")
                    mUrl = WebConfigurationManager.AppSettings["URLGuateFacturasWSFELPruebas"];

                ws.Url = mUrl;

                // - ---------------------------------------------------------------------------------
                // Colocamos las credenciales para la auteticacion en la capa de Transporte Basic Authentication
                // - ---------------------------------------------------------------------------------
                System.Net.CredentialCache myCredentials = new System.Net.CredentialCache();
                NetworkCredential netCred = new NetworkCredential(mUsuario, mPassword);
                myCredentials.Add(new Uri(ws.Url), "Basic", netCred);
                ws.Credentials = myCredentials;


                // - ---------------------------------------------------------------------------------
                // Validamos el tipo de documento y la casteamos
                // - ---------------------------------------------------------------------------------
                if (Servicio == CONSTANTES.GUATEFAC.SERVICIOS.GENERAR_DOCUMENTO)
                {
                    if (!TYPE_DOC.Equals(Documento.GetType()))
                    {
                        mResultado.Error = "Error, el TipoDocumento no corresponde al tipo de servicio solicitado.";
                        log.Debug("Resultado: " + JsonConvert.SerializeObject(mResultado));
                        return mResultado;
                    }

                    try
                    {
                        mDocumento = (Doc)Documento;
                    }
                    catch (Exception)
                    {
                        mResultado.Error = "Error, al convertir el documento.";
                        log.Debug("Resultado: " + JsonConvert.SerializeObject(mResultado));
                        return mResultado;
                    }

                    if (!(mDocumento.TipoDocumento >= CONSTANTES.GUATEFAC.DOCUMENTO.FACTURA
                        && mDocumento.TipoDocumento <= CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_CREDITO)
                        )
                    {
                        mResultado.Error = "Error, debe indicar el tipo de documento válido y habilitado.";
                        log.Debug("Resultado: " + JsonConvert.SerializeObject(mResultado));
                        return mResultado;
                    }
                }
                else if (Servicio == CONSTANTES.GUATEFAC.SERVICIOS.ANULAR_DOCUMENTO)
                {
                    if (!TYPE_ANULACION.Equals(Documento.GetType()))
                    {
                        mResultado.Error = "Error, el Objeto enviado no pertenece a la clase GuateFacturas.FEL.AnulacionDoc para poder anular el documento.";
                        log.Debug("Resultado: " + JsonConvert.SerializeObject(mResultado));
                        return mResultado;
                    }
                    try
                    {
                        mAnulacion = (AnulacionDoc)Documento;
                    }
                    catch (Exception)
                    {
                        mResultado.Error = "Error, el Objeto enviado no pertenece a la clase GuateFacturas.FEL.AnulacionDoc para poder anular el documento.";
                        log.Debug("Resultado: " + JsonConvert.SerializeObject(mResultado));
                        return mResultado;
                    }
                }
                else {
                    mResultado.Error = "Error, Debe especificar el tipo de servicio válido y habilitado.";
                    log.Debug("Resultado: " + JsonConvert.SerializeObject(mResultado));
                    return mResultado;
                }

                string pUsuario = WebConfigurationManager.AppSettings["UsrGuateFacturasWSFEL"];
                string pPassword = WebConfigurationManager.AppSettings["PwdGuateFacturasWSFEL"];

                if (mAmbiente == "DES" || mAmbiente == "PRU")
                {
                    pUsuario = WebConfigurationManager.AppSettings["UsrGuateFacturasWSFELPruebas"];
                    pPassword = WebConfigurationManager.AppSettings["PwdGuateFacturasWSFELPruebas"];
                }

                string xml = Xmldocumento(mDocumento.TipoDocumento, mDocumento);
                // - ---------------------------------------------------------------------------------
                // Consumimos el servicio
                // - ---------------------------------------------------------------------------------
                mResultado.XmlDocumento = xml;
                log.Debug(string.Format("Consumiendo WS de GuateFacturas - {0}", Servicio));
                switch (Servicio)
                {
                    case CONSTANTES.GUATEFAC.SERVICIOS.GENERAR_DOCUMENTO:
                        respuestaGuateFacturas = ws.generaDocumento(pUsuario
                            , pPassword
                            , mDocumento.NITEmisor
                            , mDocumento.Establecimiento
                            , mDocumento.TipoDocumento
                            , mDocumento.IdMaquina
                            , CONSTANTES.GUATEFAC.TIPO_RESPUESTA.RESUMEN
                            , xml);

                        //TODO: Descomentar cuando se obtengan las credenciales del Web Service.
                        log.Info(string.Format("Parametros a enviar: {0} - {1} - {2} - {3} - {4} - {5} - {6} - {7} - Respuesta: {8}"
                                            , pUsuario
                                            , pPassword
                                            , mDocumento.NITEmisor
                                            , mDocumento.Establecimiento
                                            , mDocumento.TipoDocumento
                                            , mDocumento.IdMaquina
                                            , CONSTANTES.GUATEFAC.TIPO_RESPUESTA.RESUMEN
                                            , xml
                                            , respuestaGuateFacturas));
                        //respuestaGuateFacturas = TmpSign(TipoDocumento, mDocumento);
                        break;
                    case CONSTANTES.GUATEFAC.SERVICIOS.ANULAR_DOCUMENTO:
                        respuestaGuateFacturas = ws.anulaDocumento(pUsuario
                            , pPassword
                                    , mAnulacion.NITEmisor
                                    , mAnulacion.Serie
                                    , mAnulacion.Preimpreso
                                    , mAnulacion.NitComprador
                                    , Utilitarios.FormatoYYYYMMDD(mAnulacion.FechaAnulacion)
                                    , mAnulacion.MotivoAnulacion);
                        break;
                    default: break;
                }

                // - ---------------------------------------------------------------------------------
                // Cargamos el XML y lo retornamos
                // - ---------------------------------------------------------------------------------
                CastXmlToRespuesta(Servicio,respuestaGuateFacturas, ref mResultado);

            } catch (Exception ex)
            {

                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                string mm = "";
                try
                {
                    mm = ex.InnerException.Message;
                }
                catch
                {
                    mm = "";
                }

                mResultado.ErrorComunicacion = "S";
                mResultado.Error = "Error al firmar " + m + " - " + mm + " - " + ex.Message;
                log.Debug("Resultado: " + JsonConvert.SerializeObject(mResultado));
                return mResultado;
            }
            log.Info("Resultado: " + JsonConvert.SerializeObject(mResultado));
            return mResultado;
        }

        


        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Dummy de prueba.
        /// </summary>
        /// <param name="TipoDocumento">Tipo de documento a convertir.</param>
        /// <param name="Documento">Objeto con la información del documento.</param>
        /// <returns>Retorna el XML construido para ser enviado al sericio correspondiente. </returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public string TmpSign(int TipoDocumento, Doc Documento)
        {
            var bytes = new byte[16];
            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(bytes);
            }

            Random r = new Random();
            int mNumero = r.Next(100);
            string hash = BitConverter.ToString(bytes).Replace("-", "").ToLower();

            return
            "<Resultado><Serie>PRUE-FEL-1</Serie>" +
            "<Preimpreso>00" + hash.Substring(1,10) +
            "</Preimpreso><Nombre>" + Documento.Nombre +
            "</Nombre><Direccion>" + Documento.Direccion +
            "</Direccion><Telefono>33355557" +
            "</Telefono><Firma>" + hash +
            "</Firma><Referencia>" + Documento.Referencia +
            "</Referencia></Resultado>";
        }


        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función tiene como propósito construir el XML de Consulta Solicitud a enviar a los servicios de Interconsumo.
        /// </summary>
        /// <param name="TipoDocumento">Tipo de documento a convertir.</param>
        /// <param name="Documento">Objeto con la información del documento.</param>
        /// <returns>Retorna el XML construido para ser enviado al sericio correspondiente. </returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public string Xmldocumento(int TipoDocumento, Doc Documento)
        {
            Utilitarios mUtil = new Utilitarios();
            StringBuilder mXMLData = new StringBuilder();
            mXMLData.Append("<DocElectronico>");
            mXMLData.Append("<Encabezado>");

            mXMLData.Append("<Receptor>");

            switch (TipoDocumento)
            {
                case CONSTANTES.GUATEFAC.DOCUMENTO.FACTURA_ESPECIAL:
                    mXMLData.Append(string.Format("<NITVendedor>{0}</NITVendedor>", Documento.NITReceptor));
                    mXMLData.Append(string.Format("<NombreVendedor>{0}</NombreVendedor>", Documento.Nombre));
                    mXMLData.Append(string.Format("<Direccion>{0}</Direccion>", Documento.Direccion));
                    break;
                default:
                    mXMLData.Append(string.Format("<NITReceptor>{0}</NITReceptor>", Documento.NITReceptor));
                    mXMLData.Append(string.Format("<Nombre>{0}</Nombre>", Documento.Nombre));
                    mXMLData.Append(string.Format("<Direccion>{0}</Direccion>", Documento.Direccion));
                    break;
            }
            mXMLData.Append("</Receptor>");
            
            mXMLData.Append("<InfoDoc>");
            mXMLData.Append(string.Format("<TipoVenta>{0}</TipoVenta>", Documento.TipoVenta));
            mXMLData.Append(string.Format("<DestinoVenta>{0}</DestinoVenta>", Documento.DestinoVenta));
            mXMLData.Append(string.Format("<Fecha>{0}</Fecha>", mUtil.FormatoDDMMYYYY(Documento.Fecha)));
            mXMLData.Append(string.Format("<Moneda>{0}</Moneda>", Documento.Moneda));

            switch (TipoDocumento)
            {
                case CONSTANTES.GUATEFAC.DOCUMENTO.FACTURA_ESPECIAL:
                    mXMLData.Append(string.Format("<Tasa_Cambio>{0}</Tasa_Cambio>", Documento.Tasa));
                    mXMLData.Append(string.Format("<TipoDocIdentificacion>{0}</TipoDocIdentificacion>", CONSTANTES.GUATEFAC.TIPO_DOCUMENTO_IDENTIFICACION.DPI));
                    mXMLData.Append(string.Format("<NumeroIdentificacion>{0}</NumeroIdentificacion>", Documento.NumeroIdentificacion));
                    mXMLData.Append(string.Format("<PaisEmision>{0}</PaisEmision>", Documento.PaisEmision));
                    mXMLData.Append(string.Format("<DepartamentoEmision>{0}</DepartamentoEmision>", Documento.Departamento));
                    mXMLData.Append(string.Format("<MunicipioEmision>{0}</MunicipioEmision>", Documento.Municipio));
                    mXMLData.Append(string.Format("<Referencia>{0}</Referencia>", Documento.Referencia));
                    mXMLData.Append(string.Format("<PorcISR>{0}</PorcISR>", Documento.PorcISR));
                    if (!string.IsNullOrEmpty(Documento.Reversion)
                        && Documento.Reversion == CONSTANTES.GUATEFAC.REVERSION.SI)
                        mXMLData.Append(string.Format("<Reversion>{0}</Reversion>", CONSTANTES.GUATEFAC.REVERSION.SI));
                    else mXMLData.Append(string.Format("<Reversion>{0}</Reversion>", CONSTANTES.GUATEFAC.REVERSION.NO));

                    mXMLData.Append(string.Format("<NumeroAcceso>{0}</NumeroAcceso>", Documento.NumeroAcceso));
                    mXMLData.Append(string.Format("<SerieAdmin>{0}</SerieAdmin>", Documento.SerieAdmin));
                    mXMLData.Append(string.Format("<NumeroAdmin>{0}</NumeroAdmin>", Documento.NumeroAdmin));
                    break;
                default:
                    mXMLData.Append(string.Format("<Tasa>{0}</Tasa>", Documento.Tasa));
                    mXMLData.Append(string.Format("<Referencia>{0}</Referencia>", Documento.Referencia));
                    mXMLData.Append(string.Format("<NumeroAcceso>{0}</NumeroAcceso>", Documento.NumeroAcceso));
                    mXMLData.Append(string.Format("<SerieAdmin>{0}</SerieAdmin>", Documento.SerieAdmin));
                    mXMLData.Append(string.Format("<NumeroAdmin>{0}</NumeroAdmin>", Documento.NumeroAdmin));

                    if (!string.IsNullOrEmpty(Documento.Reversion)
                        && Documento.Reversion == CONSTANTES.GUATEFAC.REVERSION.SI)
                        mXMLData.Append(string.Format("<Reversion>{0}</Reversion>", CONSTANTES.GUATEFAC.REVERSION.SI));
                    else mXMLData.Append(string.Format("<Reversion>{0}</Reversion>", CONSTANTES.GUATEFAC.REVERSION.NO));
                    break;
            }
            mXMLData.Append("</InfoDoc>");

            mXMLData.Append("<Totales>");
            mXMLData.Append(string.Format("<Bruto>{0}</Bruto>", Documento.Bruto));
            mXMLData.Append(string.Format("<Descuento>{0}</Descuento>", Documento.Descuento));
            mXMLData.Append(string.Format("<Exento>{0}</Exento>", Documento.Exento));
            mXMLData.Append(string.Format("<Otros>{0}</Otros>", Documento.Otros));
            mXMLData.Append(string.Format("<Neto>{0}</Neto>", Documento.Neto));
            mXMLData.Append(string.Format("<Isr>{0}</Isr>", Documento.Isr));
            mXMLData.Append(string.Format("<Iva>{0}</Iva>", Documento.Iva));
            mXMLData.Append(string.Format("<Total>{0}</Total>", Documento.Total));
            mXMLData.Append("</Totales>");

            
            mXMLData.Append("<DatosAdicionales>");
            if (Documento.EsExportacion)
            {
                mXMLData.Append(string.Format("<INCOTERM>{0}</INCOTERM>", Documento.Incoterm));
                mXMLData.Append(string.Format("<DESTINATARIO>>{0}</DESTINATARIO>", Documento.NombreConsignatarioODestinatario));
                mXMLData.Append(string.Format("<CODIGODESTINA>{0}</CODIGODESTINA>", Documento.CodigoConsignatarioODestinatario));
                mXMLData.Append(string.Format("<NOMBCOMPRADOR>>{0}</NOMBCOMPRADOR>", Documento.NombreComprador));
                mXMLData.Append(string.Format("<DIRCOMPRADOR>>{0}</DIRCOMPRADOR>", Documento.DireccionComprador));
                mXMLData.Append(string.Format("<CODCOMPRADOR>{0}</CODCOMPRADOR>", Documento.CodigoComprador));
                mXMLData.Append(string.Format("<OTRAREF>{0}</OTRAREF>", Documento.OtraReferencia));
                mXMLData.Append(string.Format("<CODEXPORT>{0}</CODEXPORT>", Documento.CodigoExportador));
                mXMLData.Append(string.Format("<DIRDEST>{0}</DIRDEST>", Documento.DireccionConsignatarioODestinatario));
            }
            mXMLData.Append(string.Format("<Email>{0}</Email>", Documento.Email));
            mXMLData.Append(string.Format("<Enviar>{0}</Enviar>", Documento.Enviar));
            mXMLData.Append("</DatosAdicionales>");
            

            mXMLData.Append("</Encabezado>");

            mXMLData.Append("<Detalles>");

            if (Documento.Detalle.Count() > 0)
            {
                foreach (DetalleDoc mItem in Documento.Detalle)
                {
                    mXMLData.Append("<Productos>");
                    mXMLData.Append(string.Format("<Producto>{0}</Producto>", mItem.Producto));
                    mXMLData.Append(string.Format("<Descripcion>{0}</Descripcion>", mItem.Descripcion));
                    mXMLData.Append(string.Format("<Medida>{0}</Medida>", mItem.Medida));
                    mXMLData.Append(string.Format("<Cantidad>{0}</Cantidad>", mItem.Cantidad));
                    mXMLData.Append(string.Format("<Precio>{0}</Precio>", mItem.Precio));
                    mXMLData.Append(string.Format("<PorcDesc>{0}</PorcDesc>", mItem.PorcDesc));
                    mXMLData.Append(string.Format("<ImpBruto>{0}</ImpBruto>", mItem.ImpBruto));
                    mXMLData.Append(string.Format("<ImpDescuento>{0}</ImpDescuento>", mItem.ImpDescuento));
                    mXMLData.Append(string.Format("<ImpExento>{0}</ImpExento>", mItem.ImpExento));
                    mXMLData.Append(string.Format("<ImpOtros>{0}</ImpOtros>", mItem.ImpOtros));
                    mXMLData.Append(string.Format("<ImpNeto>{0}</ImpNeto>", mItem.ImpNeto));
                    mXMLData.Append(string.Format("<ImpIsr>{0}</ImpIsr>", mItem.ImpIsr));
                    mXMLData.Append(string.Format("<ImpIva>{0}</ImpIva>", mItem.ImpIva));
                    mXMLData.Append(string.Format("<ImpTotal>{0}</ImpTotal>", mItem.ImpTotal));
                    if (!(TipoDocumento == CONSTANTES.GUATEFAC.DOCUMENTO.FACTURA_ESPECIAL))
                        mXMLData.Append(string.Format("<TipoVentaDet>{0}</TipoVentaDet>", mItem.TipoVentaDet));
                    mXMLData.Append("</Productos>");
                }
            }

            if (TipoDocumento == CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_CREDITO
                || TipoDocumento == CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_DEBITO
                || TipoDocumento == CONSTANTES.GUATEFAC.DOCUMENTO.NOTA_ABONO
                )
            {
                mXMLData.Append("<DocAsociados>");
                mXMLData.Append(string.Format("<DASerie>{0}</DASerie>", Documento.DASerie));
                mXMLData.Append(string.Format("<DAPreimpreso>{0}</DAPreimpreso>", Documento.DAPreimpreso));
                mXMLData.Append("</DocAsociados>");
            }

            mXMLData.Append("</Detalles>");
            mXMLData.Append("</DocElectronico>");

            return mXMLData.ToString();
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Este metodo tiene como propósito mapear la informacion de la solcitud de creditos en los nodos del xml a los atributos de una clase.
        /// </summary>
        /// <param name="xmlMensaje">Mesaje XML a mapear a la clase.</param>
        /// <param name="pResultado">Clase con os datos del resultado pasado por referencia.</param>
        //-----------------------------------------------------------------------------------------------------------------------
        public void CastXmlToRespuesta(string pServicio, string pRespuestaGuateFacturas, ref Resultado pResultado)
        {
            XmlDocument xmlMensaje = new XmlDocument();
            xmlMensaje.PreserveWhitespace = true;

            try
            {
                pResultado.Error = "";
                xmlMensaje.LoadXml(pRespuestaGuateFacturas.Replace("&", "Y"));
                //-- - ---------------------------------------------------------------------------------------------
                //Debido as que el xml de resultado no contiene CData, se lo colocamos para que no devuelva error
                //xmlMensaje.LoadXml(
                //    pRespuestaGuateFacturas
                //    .Replace("<Nombre>", "<Nombre><![CDATA[")
                //    .Replace("</Nombre>", "]]></Nombre>")
                //    .Replace("<Direccion>", "<Direccion><![CDATA[")
                //    .Replace("</Direccion>", "]]></Direccion>")
                //    .Replace("<Telefono>", "<Telefono><![CDATA[")
                //    .Replace("</Telefono>", "]]></Telefono>")
                //    )
                //-- - ---------------------------------------------------------------------------------------------
                //  ;
            }
            catch
            {
                pResultado.Error = pRespuestaGuateFacturas.Replace("<Resultado>", "").Replace("</Resultado>", "").Replace("<", "").Replace(">", "");
            }

            pResultado.Xml = pRespuestaGuateFacturas;
            pResultado.NumeroAutorizacion = "";
            pResultado.Estado = "";
            switch (pServicio)
            {
                case CONSTANTES.GUATEFAC.SERVICIOS.GENERAR_DOCUMENTO:

                    try
                    {
                        pResultado.NumeroAutorizacion = xmlMensaje.SelectNodes("Resultado/NumeroAutorizacion").Item(0).InnerText;

                        pResultado.Firmada = "S";
                        if (pResultado.NumeroAutorizacion.Trim().Length == 0) pResultado.Error = xmlMensaje.SelectNodes("Resultado").Item(0).InnerText;
                    }
                    catch
                    {
                        pResultado.Firmada = "N";
                        pResultado.NumeroAutorizacion = "";

                        try
                        {
                            if (pResultado.Error.Trim().Length == 0) pResultado.Error = xmlMensaje.InnerText;
                            if (pResultado.Error.Contains("NO EXISTE EL NIT")) pResultado.Error = string.Format("{0}, verifique el NIT o bien asigne CF al cliente", pResultado.Error);
                        }
                        catch
                        {
                            //Nothing
                        }
                    }

                    break;
                case CONSTANTES.GUATEFAC.SERVICIOS.ANULAR_DOCUMENTO:

                    try
                    {
                        pResultado.Estado = xmlMensaje.SelectNodes("Resultado/Estado").Item(0).InnerText;
                        if (pResultado.Estado.Trim().Length == 0) pResultado.Error = xmlMensaje.SelectNodes("Resultado").Item(0).InnerText;
                    }
                    catch
                    {
                        pResultado.Estado = "";
                    }

                    break;
                default: break;
            }
            
            try
            {
                pResultado.Serie = xmlMensaje.SelectNodes("Resultado/Serie").Item(0).InnerText;
            }
            catch
            {
                pResultado.Serie = "";
            }
            try
            {
                pResultado.Preimpreso = xmlMensaje.SelectNodes("Resultado/Preimpreso").Item(0).InnerText;
            }
            catch
            {
                pResultado.Preimpreso = "";
            }
            
            if (pResultado.Serie.Trim().Length > 0 && pResultado.Preimpreso.Trim().Length > 0) pResultado.FacturaElectronica = string.Format("{0}-{1}", pResultado.Serie, pResultado.Preimpreso);

            try
            {
                pResultado.Nombre = xmlMensaje.SelectNodes("Resultado/Nombre").Item(0).InnerText;
            }
            catch
            {
                pResultado.Nombre = "";
            }
            try
            {
                pResultado.Direccion = xmlMensaje.SelectNodes("Resultado/Direccion").Item(0).InnerText;
            }
            catch
            {
            }
            try
            {
                pResultado.Telefono = xmlMensaje.SelectNodes("Resultado/Telefono").Item(0).InnerText;
            }
            catch
            {
                pResultado.Telefono = "";
            }
            /*try
            {
                pResultado.Firma = xmlMensaje.SelectNodes("Resultado/Firma").Item(0).InnerText;
            }
            catch
            {
                pResultado.Firma = "";
            }*/

            try
            {
                pResultado.Emisor = xmlMensaje.SelectNodes("Resultado/Emisor").Item(0).InnerText;
            }
            catch
            {
                pResultado.Emisor = "";
            }
            try
            {
                pResultado.Comprador = xmlMensaje.SelectNodes("Resultado/Comprador").Item(0).InnerText;
            }
            catch
            {
                pResultado.Comprador = "";
            }
            try
            {
                pResultado.Referencia = xmlMensaje.SelectNodes("Resultado/Referencia").Item(0).InnerText;
            }
            catch
            {
                pResultado.Referencia = "";
            }
        }

        /// <summary>
        /// Esta función tiene como propósito obtener lotes de números de contingencia
        /// para las tiendas.
        /// </summary>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<Respuesta> SolicitudLoteContingencia(ContingenciaSolicitud solicitud)
        {
            Respuesta mResultado = new Respuesta();
            var mRespuestaGF = string.Empty;
            mResultado.Objeto = null;
            try
            {

                // - ---------------------------------------------------------------------------------
                // Definimos el protocolo de seguridad TLS 1.2
                // - ---------------------------------------------------------------------------------
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;


                // - ---------------------------------------------------------------------------------
                // Definimos la variable para el servicio
                // - ---------------------------------------------------------------------------------
                
                Contingencia.ContingenciaQSService contingencia = new Contingencia.ContingenciaQSService();

                // - ---------------------------------------------------------------------------------
                // Definimos la variable del URL con el WSDL
                // - ---------------------------------------------------------------------------------

                string mAmbiente = WebConfigurationManager.AppSettings["Ambiente"];
                string mUrl = WebConfigurationManager.AppSettings["URLGuateFacturasWSContingencia"];
                string mUser = WebConfigurationManager.AppSettings["UsrGuateFacturasBasicAuthFEL"];
                string mPass = WebConfigurationManager.AppSettings["PwdGuateFacturasBasicAuthFEL"];

                if (mAmbiente == "DES" || mAmbiente == "PRU") mUrl = WebConfigurationManager.AppSettings["URLGuateFacturasWSContingenciaPruebas"];

                contingencia.Url = mUrl;

                // - ---------------------------------------------------------------------------------
                // Colocamos las credenciales para la auteticacion en la capa de Transporte Basic Authentication
                // - ---------------------------------------------------------------------------------
                System.Net.CredentialCache myCredentials = new System.Net.CredentialCache();
                NetworkCredential netCred = new NetworkCredential(mUser, mPass);
                myCredentials.Add(new Uri(contingencia.Url), "Basic", netCred);
                contingencia.Credentials = myCredentials;

                string pUsuario = WebConfigurationManager.AppSettings["UsrGuateFacturasWSFEL"];
                string pPassword = WebConfigurationManager.AppSettings["PwdGuateFacturasWSFEL"];

                if (mAmbiente == "DES" || mAmbiente == "PRU")
                {
                    pUsuario = WebConfigurationManager.AppSettings["UsrGuateFacturasWSFELPruebas"];
                    pPassword = WebConfigurationManager.AppSettings["PwdGuateFacturasWSFELPruebas"];
                }

                // - ---------------------------------------------------------------------------------
                // Se consume el servicio para obtener el lote de contingencias
                // - ---------------------------------------------------------------------------------

                mRespuestaGF = contingencia.contingencia(pUsuario, pPassword, solicitud.NitEmisor, solicitud.Establecimiento, solicitud.Lote, solicitud.Cantidad);
                //-------------------------------------------------------------------------------------
                //transformánto la respuesta en el lote de número de contingencias válido
                mResultado.Objeto = CastXmlContingencia(mRespuestaGF);
                //-------------------------------------------------------------------------------------
                mResultado.Exito = true;
            }
            catch (Exception ex)
            {
               mResultado.Exito = false;
                mResultado.Mensaje = "Ocurrió un problema con la obtención de números de contingencia "+ex.Message;
                log.Error(string.Format("Problema al obtener los números de contingencia para la tienda {0} error {1}",solicitud.Establecimiento,ex.Message));
            }

                return mResultado;
        }

        private Respuesta CastXmlContingencia(string RespuestaContingencia)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                log.Info("Respuesta del GFace para la solicitud de números de contingencia: ****"+RespuestaContingencia+"****");
                XmlDocument xmlMensaje = new XmlDocument();
                xmlMensaje.PreserveWhitespace = true;
                xmlMensaje.LoadXml(RespuestaContingencia);
                
                    if (xmlMensaje.SelectNodes("Resultado").Count > 0)
                    {
                    //todo bien
                    
                    ContingenciaRespuesta contingencia = new ContingenciaRespuesta();
                    foreach (XmlNode node in xmlMensaje.SelectNodes("//NumeroAcceso"))
                        contingencia.LstAccesos.Add(node.InnerText);
    
                        respuesta.Exito = true;
                        respuesta.Objeto = contingencia;
                    }
                    else
                    {
                    //ocurrió un error
                        respuesta.Mensaje = xmlMensaje.SelectNodes("Resultado").Item(0).InnerText;
                        respuesta.Exito = false;
                        
                        log.Error("El servicio de números de contingencia devolvió un mensaje de error, no se obtuvieron los números de contingencia " + respuesta.Mensaje);
                    }

            } catch (Exception ex)
            {
                log.Error("Error al convertir el XML de respuesta de la contingencia, "+ex.Message);
                respuesta.Exito = false;
                respuesta.Mensaje = "Error al convertir el XML de respuesta de la contingencia, " + ex.Message;

            }
            
            return respuesta;
        }

        /// <summary>
        /// Método para deserializar un XML a una clase dada
        /// </summary>
        /// <typeparam name="T">Clase de respuesta</typeparam>
        /// <param name="input"></param>
        /// <returns></returns>
        private T Deserialize<T>(string input) where T : class
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(input))
            {
                return (T)ser.Deserialize(sr);
            }
        }

    }
}