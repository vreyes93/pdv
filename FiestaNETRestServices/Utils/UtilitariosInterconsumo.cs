﻿using FiestaNETRestServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Xml;
using static MF_Clases.Clases;

namespace FiestaNETRestServices.Utils
{
    public class UtilitariosInterconsumo 
    {

        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función tiene como propósito consumir el servicio de interconsumo, segun los parametros definidos.
        /// </summary>
        /// <param name="Servicio">Servio a utilizar para consumir el servicio respectivo.</param>
        /// <param name="XML">XML a enviar al servicio.</param>
        /// <returns>Retorna un XML con la respuesta correspondiente. </returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public XmlDocument Servicio(string Servicio, string XML)
        {
            XmlDocument xmlRespuesta = new XmlDocument();
            string resultado = "";
            string respuesta = "";


            // - ---------------------------------------------------------------------------------
            // Definimos el protocolo de seguridad TLS 1.2
            // - ---------------------------------------------------------------------------------
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            // - ---------------------------------------------------------------------------------
            // Definimos la variable para el servicio
            // - ---------------------------------------------------------------------------------
            FiestaNETRestServices.srvIntegracionInterConsumo.srvIntegracionInteconsumoPuente srvIntegra = new FiestaNETRestServices.srvIntegracionInterConsumo.srvIntegracionInteconsumoPuente();

            // - ---------------------------------------------------------------------------------
            // Definimos la variable de encripcion.
            // - ---------------------------------------------------------------------------------
            InterEncript.Encriptacion dllEncriptacion = new InterEncript.Encriptacion();

            // - ---------------------------------------------------------------------------------
            // Realizamos la encripción del mensaje
            // - ---------------------------------------------------------------------------------
            resultado = dllEncriptacion.Proceso(XML, "solicitud", "1", "xml");

            // - ---------------------------------------------------------------------------------
            // Consumimos el servicio
            // - ---------------------------------------------------------------------------------
            log.Debug(string.Format("Consumiendo WS de consulta de Interconsumo - {0}", Servicio));
            switch (Servicio)
            {
                case Const.SERVICIOS_INTERCONSUMO.CONSULTAR_SOLICITUD_CREDITO:
                    respuesta = srvIntegra.EntradaXmlConsultaSolicitud(resultado);
                    break;
                case Const.SERVICIOS_INTERCONSUMO.CONSULTAR_SOLICITUDES_RECHAZADAS:
                    respuesta = srvIntegra.EntradaXmlConsultaRechazos(resultado);
                    break;
                default: break;

            }

            // - ---------------------------------------------------------------------------------
            // Realizamos la des-encripción del mensaje
            // - ---------------------------------------------------------------------------------
            resultado = dllEncriptacion.Proceso(respuesta.Trim(), "", "2", "1");

            // - ---------------------------------------------------------------------------------
            // Cargamos el XML y lo retornamos
            // - ---------------------------------------------------------------------------------
            xmlRespuesta.PreserveWhitespace = true;
            xmlRespuesta.LoadXml(resultado);
            return xmlRespuesta;
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función tiene como propósito construir el XML de Consulta Solicitud a enviar a los servicios de Interconsumo.
        /// </summary>
        /// <param name="Solicitud">Numero de solicitud, es el numero de referencia en el sistema de Interonsumo para la solicitud de crédito.</param>
        /// <param name="Tienda">Es el codigo de la tienda donde se realizó la solicitud de crédito.</param>
        /// <returns>Retorna el XML construido para ser enviado al sericio correspondiente. </returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public string mensajeEntradaXmlConsultaSolicitud(string Solicitud, string Tienda)
        {
            string xmlData = "<solicitud>";
            xmlData += "<autenticacion>";
            xmlData += "<aplicacion>Muebles Fiesta</aplicacion>";
            xmlData += string.Format("<usuario><![CDATA[{0}]]></usuario>", WebConfigurationManager.AppSettings["UsrInterconsumo"]);
            xmlData += string.Format("<clave><![CDATA[{0}]]></clave>", WebConfigurationManager.AppSettings["PwdInterconsumo"]);
            xmlData += "</autenticacion>";
            xmlData += string.Format("<referencia>{0}</referencia>", Solicitud);
            xmlData += string.Format("<sucursal>{0}</sucursal>", Tienda);
            xmlData += "<idcaja>1</idcaja>";
            xmlData += "</solicitud>";
            return xmlData;
        }

        /// <summary>
        /// Método que devuelve el formato XML para la solicitud de información de Rechazos a InterConsumo
        /// </summary>
        /// <returns></returns>
        public string mensajeEntradaXmlConsultaRechazos()
        {
            //En base a la fecha de hoy, obtenemos la fecha de ayer para la solicitud de rechazos a interconsumo.
           
            var dtHoy = DateTime.Now;
            var dtAyer = dtHoy.AddDays(-1);

            string xmlData = "<solicitud>";
            xmlData += "<autenticacion>";
            xmlData += "<aplicacion>Muebles Fiesta</aplicacion>";
            xmlData += string.Format("<usuario><![CDATA[{0}]]></usuario>", WebConfigurationManager.AppSettings["UsrInterconsumo"]);
            xmlData += string.Format("<clave><![CDATA[{0}]]></clave>", WebConfigurationManager.AppSettings["PwdInterconsumo"]);
            xmlData += "</autenticacion>";
            xmlData += string.Format("<fecha>{0}</fecha>",dtAyer.Year.ToString()+"-"+dtAyer.Month.ToString("00")+"-"+dtAyer.Day.ToString("00"));
            xmlData += "</solicitud>";
            return xmlData;
        }
        /// <summary>
        /// Método que devuelve el formato XML para la solicitud de información de Rechazos a InterConsumo (para el historico)
        /// </summary>
        /// <returns></returns>
        public string mensajeEntradaXmlConsultaRechazos(DateTime dtFecha)
        {
            string xmlData = "<solicitud>";
            xmlData += "<autenticacion>";
            xmlData += "<aplicacion>Muebles Fiesta</aplicacion>";
            xmlData += string.Format("<usuario><![CDATA[{0}]]></usuario>", WebConfigurationManager.AppSettings["UsrInterconsumo"]);
            xmlData += string.Format("<clave><![CDATA[{0}]]></clave>", WebConfigurationManager.AppSettings["PwdInterconsumo"]);
            xmlData += "</autenticacion>";
            xmlData += string.Format("<fecha>{0}</fecha>", dtFecha.Year.ToString() + "-" + dtFecha.Month.ToString("00") + "-" + dtFecha.Day.ToString("00"));
            xmlData += "</solicitud>";
            return xmlData;
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Este metodo tiene como propósito mapear la informacion de la solcitud de creditos en los nodos del xml a los atributos de una clase.
        /// </summary>
        /// <param name="xmlMensaje">Mesaje XML a mapear a la clase.</param>
        /// <param name="mSolicitud">Clase Solicitud de crédito de Interconsumo pasado por referencia.</param>
        //-----------------------------------------------------------------------------------------------------------------------
        public void CastClaseSolicitudCredito(XmlDocument xmlMensaje, ref SolicitudInterconsumo mSolicitud)
        {
            
            try
            {
                mSolicitud.SOLICITUD = long.Parse(xmlMensaje.SelectNodes("respuesta/solicitud").Item(0).InnerText);
            }
            catch
            {

            }
            try
            {
                mSolicitud.NOMBRE = xmlMensaje.SelectNodes("respuesta/nombre").Item(0).InnerText;
            }
            catch
            {
                mSolicitud.NOMBRE = "";
            }

            try
            {
                mSolicitud.DESCRIPCION_ERROR = xmlMensaje.SelectNodes("respuesta/descripcion").Item(0).InnerText;
            }
            catch
            {
                mSolicitud.DESCRIPCION_ERROR = "";
            }

            try
            {
                mSolicitud.VALOR_SOLICITUD = Convert.ToDecimal(xmlMensaje.SelectNodes("respuesta/valor").Item(0).InnerText);
            }
            catch
            {
                mSolicitud.VALOR_SOLICITUD = 0;
            }

            try
            {
                mSolicitud.AUTORIZACION = xmlMensaje.SelectNodes("respuesta/autorizacion").Item(0).InnerText;
            }
            catch
            {
                mSolicitud.AUTORIZACION = "";
            }

            try
            {
                mSolicitud.ENVIAR_CORREO_INTERCONSUMO = string.IsNullOrEmpty(mSolicitud.ESTADO) ? true : false;

                mSolicitud.ESTADO = xmlMensaje.SelectNodes("respuesta/estado").Item(0).InnerText;

                if (mSolicitud.ENVIAR_CORREO_INTERCONSUMO && string.IsNullOrEmpty(mSolicitud.ESTADO))
                    mSolicitud.ENVIAR_CORREO_INTERCONSUMO = false;

            }
            catch
            {
                mSolicitud.ESTADO = "";
                mSolicitud.ENVIAR_CORREO_INTERCONSUMO = false;
            }

            try
            {
                mSolicitud.PRIMER_NOMBRE = xmlMensaje.SelectNodes("respuesta/primer_nombre").Item(0).InnerText;
            }
            catch
            {
                mSolicitud.PRIMER_NOMBRE = "";
            }

            try
            {
                mSolicitud.SEGUNDO_NOMBRE = xmlMensaje.SelectNodes("respuesta/segundo_nombre").Item(0).InnerText;
            }
            catch
            {
                mSolicitud.SEGUNDO_NOMBRE = "";
            }

            try
            {
                mSolicitud.TERCER_NOMBRE = xmlMensaje.SelectNodes("respuesta/tercer_nombre").Item(0).InnerText;
            }
            catch
            {
                mSolicitud.TERCER_NOMBRE = "";
            }

            try
            {
                mSolicitud.PRIMER_APELLIDO = xmlMensaje.SelectNodes("respuesta/primer_apellido").Item(0).InnerText;
            }
            catch
            {
                mSolicitud.PRIMER_APELLIDO = "";
            }

            try
            {
                mSolicitud.SEGUNDO_APELLIDO = xmlMensaje.SelectNodes("respuesta/segundo_apellido").Item(0).InnerText;
            }
            catch
            {
                mSolicitud.SEGUNDO_APELLIDO = "";
            }

            try
            {
                mSolicitud.APELLIDO_CASADA = xmlMensaje.SelectNodes("respuesta/apellido_casada").Item(0).InnerText;
            }
            catch
            {
                mSolicitud.APELLIDO_CASADA = "";
            }
            try
            {
                mSolicitud.PLAZO = Convert.ToInt32(xmlMensaje.SelectNodes("respuesta/plazo").Item(0).InnerText);
            }
            catch
            {
                mSolicitud.PLAZO = 0;
            }


            try
            {
                mSolicitud.CUOTA = Convert.ToDecimal(xmlMensaje.SelectNodes("respuesta/cuota").Item(0).InnerText);
            }
            catch
            {
                mSolicitud.CUOTA = 0;
            }


            try
            {
                mSolicitud.ULTIMA_CUOTA = Convert.ToDecimal(xmlMensaje.SelectNodes("respuesta/ultima_cuota").Item(0).InnerText);
            }
            catch
            {
                mSolicitud.ULTIMA_CUOTA = 0;
            }

            mSolicitud.NOMBRE =  string.Format("{0}{1}{2}{3}{4}, {5}{6}{7}{8}{9}"
                                        , mSolicitud.PRIMER_APELLIDO
                                        , mSolicitud.SEGUNDO_APELLIDO.Trim().Length == 0 ? "" : " ", mSolicitud.SEGUNDO_APELLIDO
                                        , mSolicitud.APELLIDO_CASADA.Trim().Length == 0 ? "" : " ", mSolicitud.APELLIDO_CASADA
                                        , mSolicitud.PRIMER_NOMBRE
                                        , mSolicitud.SEGUNDO_NOMBRE.Trim().Length == 0 ? "" : " ", mSolicitud.SEGUNDO_NOMBRE
                                        , mSolicitud.TERCER_NOMBRE.Trim().Length == 0 ? "" : " ", mSolicitud.TERCER_NOMBRE).ToUpper();

        }

        public static List<SolicitudInterconsumo> ParsearRechazosInterconsumo(XmlDocument doc)
        {
            List<SolicitudInterconsumo> LstRechazo = new List<SolicitudInterconsumo>();

            XmlNode nodeParent = doc.DocumentElement.SelectSingleNode("/respuesta/registros");

            foreach (XmlNode nodeRegistros in nodeParent.ChildNodes)
            {
               

                if (nodeRegistros.HasChildNodes)
                {
                    LstRechazo.Add(
                        new SolicitudInterconsumo
                        {
                            SOLICITUD = (nodeRegistros["solicitud"].InnerText.Equals("") ? 0 : int.Parse(nodeRegistros["solicitud"].InnerText)),
                            CLIENTE = nodeRegistros["cod_cliente"].InnerText,
                            NUMERO_DOCUMENTO = nodeRegistros["factura"]?.InnerText,
                            DESCRIPCION_ERROR = nodeRegistros["motivo"]?.InnerText,
                            PRIMER_NOMBRE = nodeRegistros["primer_nombre"]?.InnerText,
                            SEGUNDO_NOMBRE = nodeRegistros["segundo_nombre"]?.InnerText,
                            TERCER_NOMBRE = nodeRegistros["tercer_nombre"]?.InnerText,
                            PRIMER_APELLIDO = nodeRegistros["primer_apellido"]?.InnerText,
                            SEGUNDO_APELLIDO = nodeRegistros["segundo_apellido"]?.InnerText,
                            APELLIDO_CASADA = nodeRegistros["apellido_casada"].InnerText
                        }
                        );
                }
                       

            }
            
            return LstRechazo;
        }

        
    }
}