﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace FiestaNETRestServices.Contingencia {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3752.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="Contingencia", Namespace="http://dbguatefac/Contingencia.wsdl")]
    public partial class ContingenciaQSService : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback contingenciaOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public ContingenciaQSService() {
            this.Url = global::FiestaNETRestServices.Properties.Settings.Default.FiestaNETRestServices_Contingencia_ContingenciaQSService;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event contingenciaCompletedEventHandler contingenciaCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("http://dbguatefac/Contingencia.wsdl/contingencia", RequestNamespace="http://dbguatefac/Contingencia.wsdl", ResponseNamespace="http://dbguatefac/Contingencia.wsdl", Use=System.Web.Services.Description.SoapBindingUse.Literal)]
        [return: System.Xml.Serialization.XmlElementAttribute("result")]
        public string contingencia(string pUsuario, string pPassword, string pNitEmisor, decimal pIdEstablecimiento, string pLote, decimal pCantidad) {
            object[] results = this.Invoke("contingencia", new object[] {
                        pUsuario,
                        pPassword,
                        pNitEmisor,
                        pIdEstablecimiento,
                        pLote,
                        pCantidad});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void contingenciaAsync(string pUsuario, string pPassword, string pNitEmisor, decimal pIdEstablecimiento, string pLote, decimal pCantidad) {
            this.contingenciaAsync(pUsuario, pPassword, pNitEmisor, pIdEstablecimiento, pLote, pCantidad, null);
        }
        
        /// <remarks/>
        public void contingenciaAsync(string pUsuario, string pPassword, string pNitEmisor, decimal pIdEstablecimiento, string pLote, decimal pCantidad, object userState) {
            if ((this.contingenciaOperationCompleted == null)) {
                this.contingenciaOperationCompleted = new System.Threading.SendOrPostCallback(this.OncontingenciaOperationCompleted);
            }
            this.InvokeAsync("contingencia", new object[] {
                        pUsuario,
                        pPassword,
                        pNitEmisor,
                        pIdEstablecimiento,
                        pLote,
                        pCantidad}, this.contingenciaOperationCompleted, userState);
        }
        
        private void OncontingenciaOperationCompleted(object arg) {
            if ((this.contingenciaCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.contingenciaCompleted(this, new contingenciaCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3752.0")]
    public delegate void contingenciaCompletedEventHandler(object sender, contingenciaCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3752.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class contingenciaCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal contingenciaCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591