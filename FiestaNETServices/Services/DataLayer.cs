﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Transactions;
using FiestaNETServices.Services;
  
public static class DataLayer
{
       
    public static void Sincronizar()
    {
        dbDataContext db = new dbDataContext();
        var qFaltan =
            from a in db.NIVEL_PRECIOs
            join b in db.MF_NIVEL_PRECIO_COEFICIENTEs on new { NIVEL_PRECIO1 = Convert.ToString(a.NIVEL_PRECIO1) } equals new { NIVEL_PRECIO1 = b.NIVEL_PRECIO } into b_join
            from b in b_join.DefaultIfEmpty()
            where
              b.NIVEL_PRECIO == null
            select new
            {
                NivelPrecioFalta = a.NIVEL_PRECIO1
            };

        using (TransactionScope transactionScope = new TransactionScope())
        {
            foreach (var item in qFaltan)
            {
                MF_NIVEL_PRECIO_COEFICIENTE iMF_NIVEL_PRECIO_COEFICIENTEs = new MF_NIVEL_PRECIO_COEFICIENTE
                {
                    NIVEL_PRECIO = item.NivelPrecioFalta,
                    MONEDA = "L",
                    COEFICIENTE_MULR = (from c in db.ARTICULO_PRECIOs where c.NIVEL_PRECIO == item.NivelPrecioFalta select c).Count() == 0 ? 1 : Convert.ToDecimal((from c in db.ARTICULO_PRECIOs where c.NIVEL_PRECIO == item.NivelPrecioFalta select c).First().MARGEN_MULR),
                    DESCRIPCION = "Nuevo Precio",
                    MODIFICADO = Convert.ToChar("N")
                };
                db.MF_NIVEL_PRECIO_COEFICIENTEs.InsertOnSubmit(iMF_NIVEL_PRECIO_COEFICIENTEs);
                db.SubmitChanges();
            }

            transactionScope.Complete();
        }

    }

    public static IQueryable<NivelPrecioCoeficiente> ListarCoeficientes()
    {
        dbDataContext db = new dbDataContext();

        var q =
            from mf_nivel_precio_coeficientes in db.MF_NIVEL_PRECIO_COEFICIENTEs
            orderby
              mf_nivel_precio_coeficientes.NIVEL_PRECIO
            select new NivelPrecioCoeficiente()
            {
                Nivel = mf_nivel_precio_coeficientes.NIVEL_PRECIO,
                Coeficiente = mf_nivel_precio_coeficientes.COEFICIENTE_MULR,
                Descripción = mf_nivel_precio_coeficientes.DESCRIPCION
            };
        return q;
    }

    public static IQueryable<NivelPrecioCoeficiente> ListarCoeficientesPorNivelPrecio(string nivelPrecio)
    {
        dbDataContext db = new dbDataContext();

        var q = ListarCoeficientes();
        q = q.Where(x => x.Nivel.Contains(nivelPrecio));
        return q;
    }

    public static IQueryable<NivelPrecioCoeficiente> ListarCoeficientesPorCriterio(string tipo, string nivelPrecio, string descripcion)
    {
        dbDataContext db = new dbDataContext();

        var q = ListarCoeficientes();

        switch (tipo)
        {
            case "C":
                q = q.Where(x => x.Nivel.Contains(nivelPrecio));
                break;
            case "D":
                q = q.Where(x => x.Descripción.Contains(descripcion));
                break;
            case "A":
                q = q.Where(x => (x.Nivel.Contains(nivelPrecio) && x.Descripción.Contains(descripcion)));
                break;
            case "N":
                q = q.Where(x => x.Nivel.Equals(nivelPrecio));
                break;
        }
        return q;
    }

    public static bool GrabarCoeficientes(string nivel, string coeficiente, string descripcion, ref string mensaje)
    {
        dbDataContext db = new dbDataContext();
        bool mRetorna = true;
         
        try
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                var updateCoeficientes =
                from mf_nivel_precio_coeficientes in db.MF_NIVEL_PRECIO_COEFICIENTEs
                where
                  mf_nivel_precio_coeficientes.NIVEL_PRECIO == nivel
                select mf_nivel_precio_coeficientes;
                foreach (var mf_nivel_precio_coeficientes in updateCoeficientes)
                {
                    mf_nivel_precio_coeficientes.COEFICIENTE_MULR = Convert.ToDecimal(coeficiente);
                    mf_nivel_precio_coeficientes.DESCRIPCION = descripcion;
                    mf_nivel_precio_coeficientes.MODIFICADO = Convert.ToChar("S");
                }
                  
                db.SubmitChanges();
                transactionScope.Complete();

                mensaje = "El Nivel de Precio fue grabado exitosamente.";
            }
        }
        catch (Exception ex)
        {
            mRetorna = false;
            mensaje = string.Format("Error al grabar el coeficiente. {0}", ex.Message);
        }

        return mRetorna;
    }

    public static IQueryable<Articulos> ListarArticulos(dbDataContext db)
    {
        var q =
            from articulos in db.ARTICULOs where articulos.ACTIVO == "S"
            orderby
              articulos.ARTICULO1
            select new Articulos()
            {
                Articulo = articulos.ARTICULO1,
                Descripción = articulos.DESCRIPCION,
                PrecioLista = articulos.PRECIO_BASE_LOCAL
            };
        return q;
    }

    public static IQueryable<Ofertas> ListarOfertas(dbDataContext db)
    {
        var q =
            (
            from MF_ArticuloPrecioOferta in db.MF_ArticuloPrecioOferta
            join articulos in db.ARTICULOs on new { Articulo = MF_ArticuloPrecioOferta.Articulo } equals new { Articulo = Convert.ToString(articulos.ARTICULO1) }
            select new Ofertas()
            {
                Tipo = "Por Arítculo",
                Articulo = MF_ArticuloPrecioOferta.Articulo,
                NombreArticulo = articulos.DESCRIPCION,
                NivelPrecio = "",
                FechaInicial = MF_ArticuloPrecioOferta.FechaOfertaDesde,
                FechaFinal = MF_ArticuloPrecioOferta.FechaOfertaHasta,
                PrecioOriginal = MF_ArticuloPrecioOferta.PrecioOriginal,
                PrecioOferta = MF_ArticuloPrecioOferta.PrecioOferta,
                Estatus = MF_ArticuloPrecioOferta.Estatus == Convert.ToChar("P") ? "Pendiente" : MF_ArticuloPrecioOferta.Estatus == Convert.ToChar("V") ? "Vigente" : MF_ArticuloPrecioOferta.Estatus == Convert.ToChar("C") ? "Canceladas" : "Finalizadas",
                DescripcionOferta = MF_ArticuloPrecioOferta.Descripcion,
                UsuarioCreo = MF_ArticuloPrecioOferta.CreatedBy,
                FechaCreada = MF_ArticuloPrecioOferta.RecordDate
            }
            )
            .Union
            (
                from mf_nivelpreciocoeficienteofertas in db.MF_NivelPrecioCoeficienteOfertas
                select new Ofertas()
                {
                    Tipo = "Por Nivel de Precio",
                    Articulo = "",
                    NombreArticulo = "",
                    NivelPrecio = mf_nivelpreciocoeficienteofertas.NivelPrecio,
                    FechaInicial = mf_nivelpreciocoeficienteofertas.FechaOfertaDesde,
                    FechaFinal = mf_nivelpreciocoeficienteofertas.FechaOfertaHasta,
                    PrecioOriginal = mf_nivelpreciocoeficienteofertas.CoeficienteMulrOriginal,
                    PrecioOferta = mf_nivelpreciocoeficienteofertas.CoeficienteMulrOferta,
                    Estatus = mf_nivelpreciocoeficienteofertas.Estatus == Convert.ToChar("P") ? "Pendiente" : mf_nivelpreciocoeficienteofertas.Estatus == Convert.ToChar("V") ? "Vigente" : mf_nivelpreciocoeficienteofertas.Estatus == Convert.ToChar("C") ? "Canceladas" : "Finalizadas",
                    DescripcionOferta = mf_nivelpreciocoeficienteofertas.Descripcion,
                    UsuarioCreo = mf_nivelpreciocoeficienteofertas.CreatedBy,
                    FechaCreada = mf_nivelpreciocoeficienteofertas.RecordDate
                }
            ).Union
            (
                from mf_articulonivelprecioofertas in db.MF_ArticuloNivelPrecioOfertas
                join articulos in db.ARTICULOs on new { Articulo = mf_articulonivelprecioofertas.Articulo } equals new { Articulo = Convert.ToString(articulos.ARTICULO1) }
                select new Ofertas()
                {
                    Tipo = "Por Artículo + Nivel de Precio",
                    Articulo = mf_articulonivelprecioofertas.Articulo,
                    NombreArticulo = articulos.DESCRIPCION,
                    NivelPrecio = mf_articulonivelprecioofertas.NivelPrecio,
                    FechaInicial = mf_articulonivelprecioofertas.FechaOfertaDesde,
                    FechaFinal = mf_articulonivelprecioofertas.FechaOfertaHasta,
                    PrecioOriginal = mf_articulonivelprecioofertas.PrecioOriginal,
                    PrecioOferta = mf_articulonivelprecioofertas.PrecioOferta,
                    Estatus = mf_articulonivelprecioofertas.Estatus == Convert.ToChar("P") ? "Pendiente" : mf_articulonivelprecioofertas.Estatus == Convert.ToChar("V") ? "Vigente" : mf_articulonivelprecioofertas.Estatus == Convert.ToChar("C") ? "Canceladas" : "Finalizadas",
                    DescripcionOferta = mf_articulonivelprecioofertas.Descripcion,
                    UsuarioCreo = mf_articulonivelprecioofertas.CreatedBy,
                    FechaCreada = mf_articulonivelprecioofertas.RecordDate
                }
            )
            .OrderByDescending(p => p.FechaInicial);

        return q;
    }

    public static IQueryable<Ofertas> ListarOfertasPorCriterio(dbDataContext db, string inicio, string fin, string tipo, string estatus, string articulo, string nivel, string nombreArticulo, string descripcion)
    {
        var q = ListarOfertas(db);

        //if (inicio.Trim().Length > 0 && fin.Trim().Length > 0)
        //{
        //    if (tipo == "T")
        //    {
        //        q = q.Where(x => (x.FechaInicial >= Convert.ToDateTime(inicio) && x.FechaFinal <= Convert.ToDateTime(fin)));
        //    }


        //    return q;
        //}
        //if (inicio.Trim().Length > 0)
        //{
        //    q = q.Where(x => (x.FechaInicial >= Convert.ToDateTime(inicio)));
        //    return q;
        //}
        //if (fin.Trim().Length > 0)
        //{
        //    q = q.Where(x => (x.FechaFinal <= Convert.ToDateTime(fin)));
        //    return q;
        //}


        return q;
    }

    public static IQueryable<Articulos> ListarArticulosPorCriterio(string tipo, string articulo, string descripcion)
    {
        dbDataContext db = new dbDataContext();

        try
        {
            var q = ListarArticulos(db);

            switch (tipo)
            {
                case "C":
                    q = q.Where(x => x.Articulo.Contains(articulo));
                    break;
                case "D":
                    q = q.Where(x => x.Descripción.Contains(descripcion));
                    break;
                case "A":
                    q = q.Where(x => (x.Articulo.Contains(articulo) && x.Descripción.Contains(descripcion)));
                    break;
            }

            return q;
        }
        catch
        {
            var q =
                from articulos in db.ARTICULOs
                where articulos.ARTICULO1 == "010001-000"
                select new Articulos()
                {
                    Articulo = "000000-000",
                    Descripción = "No hay información en el criterio de búsqueda establecido",
                    PrecioLista = 0
                };
            return q;
        }
    }

    public static IQueryable<Articulos> ListarArticulosPorCriterioConPrecio(string tipo, string articulo, string descripcion)
    {
        dbDataContext db = new dbDataContext();
        var q = ListarArticulos(db);

        switch (tipo)
        {
            case "C":
                q = q.Where(x => x.Articulo.Contains(articulo) && x.PrecioLista > 0);
                break;
            case "D":
                q = q.Where(x => x.Descripción.Contains(descripcion) && x.PrecioLista > 0);
                break;
            case "A":
                q = q.Where(x => x.Articulo.Contains(articulo) && x.Descripción.Contains(descripcion) && x.PrecioLista > 0);
                break;
        }

        return q;
    }

    public static bool GrabarOfertaArticulo(ref string mensaje, string articulo, DateTime FechaInicial, DateTime FechaFinal, decimal PrecioOriginal, decimal PrecioOferta, string descripcion)
    {
        dbDataContext db = new dbDataContext();
        bool mRetorna = true;

        try
        {
            var qNivel = (from MF_ArticuloNivelPrecioOfertas in db.MF_ArticuloNivelPrecioOfertas
                     where
                          (MF_ArticuloNivelPrecioOfertas.Estatus == Convert.ToChar("P") || MF_ArticuloNivelPrecioOfertas.Estatus == Convert.ToChar("V")) &&
                          MF_ArticuloNivelPrecioOfertas.Articulo == articulo &&
                          ((FechaInicial >= MF_ArticuloNivelPrecioOfertas.FechaOfertaDesde && FechaInicial <= MF_ArticuloNivelPrecioOfertas.FechaOfertaHasta) ||
                          (FechaFinal >= MF_ArticuloNivelPrecioOfertas.FechaOfertaDesde && FechaFinal <= MF_ArticuloNivelPrecioOfertas.FechaOfertaHasta))
                     select MF_ArticuloNivelPrecioOfertas).Count();

            if (Convert.ToInt32(qNivel) == 0)
            {
                var qqNivel = (from MF_ArticuloNivelPrecioOfertas in db.MF_ArticuloNivelPrecioOfertas
                          where
                               MF_ArticuloNivelPrecioOfertas.Estatus == Convert.ToChar("V") &&
                               MF_ArticuloNivelPrecioOfertas.Articulo == articulo
                          select MF_ArticuloNivelPrecioOfertas).Count();

                if (Convert.ToInt32(qqNivel) > 0)
                {
                    mRetorna = false;
                    mensaje = string.Format("El Artículo {0} tiene una oferta vigente por niveles de precio, no es posible grabar una oferta en este momento.", articulo);
                    return mRetorna;
                }
            }

            if (Convert.ToInt32(qNivel) > 0)
            {
                mRetorna = false;
                mensaje = string.Format("El Artículo {0} tiene una oferta pendiente o vigente por niveles de precio en el rango de fechas especificado.", articulo);
                return mRetorna;
            }

            var q = (from MF_ArticuloPrecioOferta in db.MF_ArticuloPrecioOferta
                      where
                           (MF_ArticuloPrecioOferta.Estatus == Convert.ToChar("P") || MF_ArticuloPrecioOferta.Estatus == Convert.ToChar("V")) &&
                           MF_ArticuloPrecioOferta.Articulo == articulo &&
                           ((FechaInicial >= MF_ArticuloPrecioOferta.FechaOfertaDesde && FechaInicial <= MF_ArticuloPrecioOferta.FechaOfertaHasta) ||
                           (FechaFinal >= MF_ArticuloPrecioOferta.FechaOfertaDesde && FechaFinal <= MF_ArticuloPrecioOferta.FechaOfertaHasta))
                      select MF_ArticuloPrecioOferta).Count();

            if (Convert.ToInt32(q) == 0)
            {
                var qq = (from MF_ArticuloPrecioOferta in db.MF_ArticuloPrecioOferta
                         where
                              MF_ArticuloPrecioOferta.Estatus == Convert.ToChar("V") &&
                              MF_ArticuloPrecioOferta.Articulo == articulo
                         select MF_ArticuloPrecioOferta).Count();

                if (Convert.ToInt32(qq) > 0)
                {
                    mRetorna = false;
                    mensaje = string.Format("El Artículo {0} tiene una oferta vigente, no es posible grabar una oferta en este momento.", articulo);
                    return mRetorna;
                }
            }

            if (Convert.ToInt32(q) > 0)
            {
                mRetorna = false;
                mensaje = string.Format("El Artículo {0} tiene una oferta pendiente o vigente en el rango de fechas especificado.", articulo);
                return mRetorna;
            }
            else
            {
                string mEstatus = "P";

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    MF_ArticuloPrecioOferta iMF_ArticuloPrecioOferta = new MF_ArticuloPrecioOferta
                    {
                        Articulo = articulo,
                        FechaOfertaDesde = FechaInicial,
                        FechaOfertaHasta = FechaFinal,
                        PrecioOriginal = PrecioOriginal,
                        PrecioOferta = PrecioOferta,
                        Estatus = Convert.ToChar(mEstatus),
                        Descripcion = descripcion,
                        RecordDate = DateTime.Now,
                        CreatedBy = System.Environment.UserName,
                        CreateDate = DateTime.Now
                    };

                    db.MF_ArticuloPrecioOferta.InsertOnSubmit(iMF_ArticuloPrecioOferta);
                    db.SubmitChanges();

                    transactionScope.Complete();

                    mensaje = "La oferta del artículo fue grabada exitosamente.";
                }
            }
        }
        catch (Exception ex)
        {
            mRetorna = false;
            mensaje = string.Format("Error al grabar la oferta del artículo. {0}", ex.Message);
        }

        return mRetorna;
    }

    public static bool GrabarOfertaArticuloNormalCondicional(ref string mensaje, string articulo, DateTime FechaInicial, DateTime FechaFinal, decimal PrecioOriginal, decimal PrecioOferta, string descripcion, bool condicional, string[] articulos)
    {
        dbDataContext db = new dbDataContext();
        bool mRetorna = true;

        try
        {
            var qNivel = (from MF_ArticuloNivelPrecioOfertas in db.MF_ArticuloNivelPrecioOfertas
                          where
                               (MF_ArticuloNivelPrecioOfertas.Estatus == Convert.ToChar("P") || MF_ArticuloNivelPrecioOfertas.Estatus == Convert.ToChar("V")) &&
                               MF_ArticuloNivelPrecioOfertas.Articulo == articulo &&
                               ((FechaInicial >= MF_ArticuloNivelPrecioOfertas.FechaOfertaDesde && FechaInicial <= MF_ArticuloNivelPrecioOfertas.FechaOfertaHasta) ||
                               (FechaFinal >= MF_ArticuloNivelPrecioOfertas.FechaOfertaDesde && FechaFinal <= MF_ArticuloNivelPrecioOfertas.FechaOfertaHasta))
                          select MF_ArticuloNivelPrecioOfertas).Count();

            if (Convert.ToInt32(qNivel) == 0)
            {
                var qqNivel = (from MF_ArticuloNivelPrecioOfertas in db.MF_ArticuloNivelPrecioOfertas
                               where
                                    MF_ArticuloNivelPrecioOfertas.Estatus == Convert.ToChar("V") &&
                                    MF_ArticuloNivelPrecioOfertas.Articulo == articulo
                               select MF_ArticuloNivelPrecioOfertas).Count();

                if (Convert.ToInt32(qqNivel) > 0)
                {
                    mRetorna = false;
                    mensaje = string.Format("El Artículo {0} tiene una oferta vigente por niveles de precio, no es posible grabar una oferta en este momento.", articulo);
                    return mRetorna;
                }
            }

            if (Convert.ToInt32(qNivel) > 0)
            {
                mRetorna = false;
                mensaje = string.Format("El Artículo {0} tiene una oferta pendiente o vigente por niveles de precio en el rango de fechas especificado.", articulo);
                return mRetorna;
            }

            var q = (from MF_ArticuloPrecioOferta in db.MF_ArticuloPrecioOferta
                     where
                          (MF_ArticuloPrecioOferta.Estatus == Convert.ToChar("P") || MF_ArticuloPrecioOferta.Estatus == Convert.ToChar("V")) &&
                          MF_ArticuloPrecioOferta.Articulo == articulo &&
                          ((FechaInicial >= MF_ArticuloPrecioOferta.FechaOfertaDesde && FechaInicial <= MF_ArticuloPrecioOferta.FechaOfertaHasta) ||
                          (FechaFinal >= MF_ArticuloPrecioOferta.FechaOfertaDesde && FechaFinal <= MF_ArticuloPrecioOferta.FechaOfertaHasta))
                     select MF_ArticuloPrecioOferta).Count();

            if (Convert.ToInt32(q) == 0)
            {
                var qq = (from MF_ArticuloPrecioOferta in db.MF_ArticuloPrecioOferta
                          where
                               MF_ArticuloPrecioOferta.Estatus == Convert.ToChar("V") &&
                               MF_ArticuloPrecioOferta.Articulo == articulo
                          select MF_ArticuloPrecioOferta).Count();

                if (Convert.ToInt32(qq) > 0)
                {
                    mRetorna = false;
                    mensaje = string.Format("El Artículo {0} tiene una oferta vigente, no es posible grabar una oferta en este momento.", articulo);
                    return mRetorna;
                }
            }

            if (Convert.ToInt32(q) > 0)
            {
                mRetorna = false;
                mensaje = string.Format("El Artículo {0} tiene una oferta pendiente o vigente en el rango de fechas especificado.", articulo);
                return mRetorna;
            }
            else
            {
                string mEstatus = "P"; string mCondicion = "N";

                if (condicional) mCondicion = "S";

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    MF_ArticuloPrecioOferta iMF_ArticuloPrecioOferta = new MF_ArticuloPrecioOferta
                    {
                        Articulo = articulo,
                        FechaOfertaDesde = FechaInicial,
                        FechaOfertaHasta = FechaFinal,
                        PrecioOriginal = PrecioOriginal,
                        PrecioOferta = PrecioOferta,
                        Estatus = Convert.ToChar(mEstatus),
                        Descripcion = descripcion,
                        RecordDate = DateTime.Now,
                        CreatedBy = System.Environment.UserName,
                        CreateDate = DateTime.Now,
                        Condicional = Convert.ToChar(mCondicion)
                    };

                    db.MF_ArticuloPrecioOferta.InsertOnSubmit(iMF_ArticuloPrecioOferta);
                    db.SubmitChanges();

                    if (condicional)
                    {
                        for (int ii = 0; ii < articulos.Length; ii++)
                        {
                            MF_ArticuloPrecioOfertaCondicion iMF_ArticuloPrecioOfertaCondicion = new MF_ArticuloPrecioOfertaCondicion
                            {
                                Articulo = articulo,
                                FechaOfertaDesde = FechaInicial,
                                FechaOfertaHasta = FechaFinal,
                                Estatus = Convert.ToChar(mEstatus),
                                ArticuloCondicion = articulos[ii],
                                RecordDate = DateTime.Now,
                                CreatedBy = System.Environment.UserName,
                                CreateDate = DateTime.Now,
                            };

                            db.MF_ArticuloPrecioOfertaCondicions.InsertOnSubmit(iMF_ArticuloPrecioOfertaCondicion);
                            db.SubmitChanges();
                        }
                    }

                    transactionScope.Complete();
                    mensaje = "La oferta del artículo fue grabada exitosamente.";
                }
            }
        }
        catch (Exception ex)
        {
            mRetorna = false;
            mensaje = string.Format("Error al grabar la oferta del artículo. {0}", ex.Message);
        }

        return mRetorna;
    }

    public static bool GrabarOfertaNivel(ref string mensaje, string nivel, DateTime FechaInicial, DateTime FechaFinal, decimal CoeficienteOriginal, decimal CoeficienteOferta, string descripcion)
    {
        dbDataContext db = new dbDataContext();
        bool mRetorna = true;

        try
        {
            var q = (from MF_NivelPrecioCoeficienteOfertas in db.MF_NivelPrecioCoeficienteOfertas
                     where
                          (MF_NivelPrecioCoeficienteOfertas.Estatus == Convert.ToChar("P") || MF_NivelPrecioCoeficienteOfertas.Estatus == Convert.ToChar("V")) &&
                          MF_NivelPrecioCoeficienteOfertas.NivelPrecio == nivel &&
                          ((FechaInicial >= MF_NivelPrecioCoeficienteOfertas.FechaOfertaDesde && FechaInicial <= MF_NivelPrecioCoeficienteOfertas.FechaOfertaHasta) ||
                          (FechaFinal >= MF_NivelPrecioCoeficienteOfertas.FechaOfertaDesde && FechaFinal <= MF_NivelPrecioCoeficienteOfertas.FechaOfertaHasta))
                     select MF_NivelPrecioCoeficienteOfertas).Count();

            if (Convert.ToInt32(q) == 0)
            {
                var qq = (from MF_NivelPrecioCoeficienteOfertas in db.MF_NivelPrecioCoeficienteOfertas
                         where
                              MF_NivelPrecioCoeficienteOfertas.Estatus == Convert.ToChar("V") &&
                              MF_NivelPrecioCoeficienteOfertas.NivelPrecio == nivel
                         select MF_NivelPrecioCoeficienteOfertas).Count();

                if (Convert.ToInt32(qq) > 0)
                {
                    mRetorna = false;
                    mensaje = string.Format("El Nivel de Precio {0} tiene una oferta vigente, no es posible grabar una oferta en este momento.", nivel);
                    return mRetorna;
                }
            }

            if (Convert.ToInt32(q) > 0)
            {
                mRetorna = false;
                mensaje = string.Format("El Nivel de Precio {0} tiene una oferta pendiente o vigente en el rango de fechas especificado.", nivel);
            }
            else
            {
                string mEstatus = "P";

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    MF_NivelPrecioCoeficienteOferta iMF_NivelPrecioCoeficienteOfertas = new MF_NivelPrecioCoeficienteOferta
                    {
                        NivelPrecio = nivel,
                        FechaOfertaDesde = FechaInicial,
                        FechaOfertaHasta = FechaFinal,
                        CoeficienteMulrOriginal = CoeficienteOriginal,
                        CoeficienteMulrOferta = CoeficienteOferta,
                        Estatus = Convert.ToChar(mEstatus),
                        Descripcion = descripcion,
                        RecordDate = DateTime.Now,
                        CreatedBy = System.Environment.UserName,
                        CreateDate = DateTime.Now
                    };

                    db.MF_NivelPrecioCoeficienteOfertas.InsertOnSubmit(iMF_NivelPrecioCoeficienteOfertas);
                    db.SubmitChanges();

                    transactionScope.Complete();

                    mensaje = "La oferta del Nivel de Precio fue grabada exitosamente.";
                }
            }
        }
        catch (Exception ex)
        {
            mRetorna = false;
            mensaje = string.Format("Error al grabar la oferta del Nivel de precio. {0}", ex.Message);
        }

        return mRetorna;
    }

    public static bool GrabarOfertaArticuloNivel(ref string mensaje, string articulo, string nivel, DateTime FechaInicial, DateTime FechaFinal, decimal PrecioOriginal, decimal PrecioOferta, string descripcion)
    {
        dbDataContext db = new dbDataContext();
        bool mRetorna = true;

        PrecioOriginal = PrecioNivelArticulo(nivel, articulo);

        try
        {
            var q = (from MF_ArticuloNivelPrecioOfertas in db.MF_ArticuloNivelPrecioOfertas
                     where
                          (MF_ArticuloNivelPrecioOfertas.Estatus == Convert.ToChar("P") || MF_ArticuloNivelPrecioOfertas.Estatus == Convert.ToChar("V")) &&
                          MF_ArticuloNivelPrecioOfertas.NivelPrecio == nivel &&
                          MF_ArticuloNivelPrecioOfertas.Articulo == articulo &&
                          ((FechaInicial >= MF_ArticuloNivelPrecioOfertas.FechaOfertaDesde && FechaInicial <= MF_ArticuloNivelPrecioOfertas.FechaOfertaHasta) ||
                          (FechaFinal >= MF_ArticuloNivelPrecioOfertas.FechaOfertaDesde && FechaFinal <= MF_ArticuloNivelPrecioOfertas.FechaOfertaHasta))
                     select MF_ArticuloNivelPrecioOfertas).Count();

            if (Convert.ToInt32(q) == 0)
            {
                var qq = (from MF_ArticuloNivelPrecioOfertas in db.MF_ArticuloNivelPrecioOfertas
                         where
                              MF_ArticuloNivelPrecioOfertas.Estatus == Convert.ToChar("V") &&
                              MF_ArticuloNivelPrecioOfertas.NivelPrecio == nivel &&
                              MF_ArticuloNivelPrecioOfertas.Articulo == articulo
                         select MF_ArticuloNivelPrecioOfertas).Count();

                if (Convert.ToInt32(qq) > 0)
                {
                    mRetorna = false;
                    mensaje = string.Format("El Artículo {0} en el nivel {1} tiene una oferta vigente, no es posible grabar una oferta en este momento.", articulo, nivel);
                    return mRetorna;
                }
            }

            if (Convert.ToInt32(q) > 0)
            {
                mRetorna = false;
                mensaje = string.Format("El Artículo {0} en el nivel {1} tiene una oferta pendiente o vigente en el rango de fechas especificado.", articulo, nivel);
            }
            else
            {
                string mEstatus = "P";

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    MF_ArticuloNivelPrecioOferta iMF_ArticuloNivelPrecioOfertas = new MF_ArticuloNivelPrecioOferta
                    {
                        NivelPrecio = nivel,
                        Articulo = articulo,
                        FechaOfertaDesde = FechaInicial,
                        FechaOfertaHasta = FechaFinal,
                        PrecioOriginal = PrecioOriginal,
                        PrecioOferta = PrecioOferta,
                        Estatus = Convert.ToChar(mEstatus),
                        Descripcion = descripcion,
                        RecordDate = DateTime.Now,
                        CreatedBy = System.Environment.UserName,
                        CreateDate = DateTime.Now
                    };
                    db.MF_ArticuloNivelPrecioOfertas.InsertOnSubmit(iMF_ArticuloNivelPrecioOfertas);
                    db.SubmitChanges();

                    transactionScope.Complete();

                    mensaje = "La oferta del artículo en el nivel fue grabada exitosamente.";
                }
            }
        }
        catch (Exception ex)
        {
            mRetorna = false;
            mensaje = string.Format("Error al grabar la oferta del artículo en el nivel. {0}", ex.Message);
        }

        return mRetorna;
    }

    public static bool GrabarOfertaArticulosNiveles(ref string mensaje, string[] articulos, string[] niveles, DateTime FechaInicial, DateTime FechaFinal, decimal porcentaje, string descripcion, string tipo)
    {
        dbDataContext db = new dbDataContext();
        bool mRetorna = true; string articulo, nivel; int id = 0;

        try
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                var qCuantos = (from MF_ArticuloNivelPrecioOfertas in db.MF_ArticuloNivelPrecioOfertas select MF_ArticuloNivelPrecioOfertas).Count();
                if (Convert.ToInt32(qCuantos) > 0) id = Convert.ToInt32((from MF_ArticuloNivelPrecioOfertas in db.MF_ArticuloNivelPrecioOfertas select MF_ArticuloNivelPrecioOfertas.identificador).Max()) + 1;

                for (int ii = 0; ii < niveles.Length; ii++)
                {
                    nivel = niveles[ii];
                    for (int jj = 0; jj < articulos.Length; jj++)
                    {
                        articulo = articulos[jj];

                        var qArticulo = (from MF_ArticuloPrecioOferta in db.MF_ArticuloPrecioOferta
                                         where
                                              (MF_ArticuloPrecioOferta.Estatus == Convert.ToChar("P") || MF_ArticuloPrecioOferta.Estatus == Convert.ToChar("V")) &&
                                              MF_ArticuloPrecioOferta.Articulo == articulo &&
                                              ((FechaInicial >= MF_ArticuloPrecioOferta.FechaOfertaDesde && FechaInicial <= MF_ArticuloPrecioOferta.FechaOfertaHasta) ||
                                              (FechaFinal >= MF_ArticuloPrecioOferta.FechaOfertaDesde && FechaFinal <= MF_ArticuloPrecioOferta.FechaOfertaHasta))
                                         select MF_ArticuloPrecioOferta).Count();

                        if (Convert.ToInt32(qArticulo) == 0)
                        {
                            var qqArticulo = (from MF_ArticuloPrecioOferta in db.MF_ArticuloPrecioOferta
                                              where
                                                   MF_ArticuloPrecioOferta.Estatus == Convert.ToChar("V") &&
                                                   MF_ArticuloPrecioOferta.Articulo == articulo
                                              select MF_ArticuloPrecioOferta).Count();

                            if (Convert.ToInt32(qqArticulo) > 0)
                            {
                                mRetorna = false;
                                mensaje = string.Format("El Artículo {0} tiene una oferta vigente, no es posible grabar una oferta en este momento.", articulo);
                                return mRetorna;
                            }
                        }

                        if (Convert.ToInt32(qArticulo) > 0)
                        {
                            mRetorna = false;
                            mensaje = string.Format("El Artículo {0} tiene una oferta pendiente o vigente en el rango de fechas especificado.", articulo);
                            return mRetorna;
                        }


                        //var qNivel = (from MF_ArticuloNivelPrecioOfertas in db.MF_ArticuloNivelPrecioOfertas
                        //              where
                        //                   (MF_ArticuloNivelPrecioOfertas.Estatus == Convert.ToChar("P") || MF_ArticuloNivelPrecioOfertas.Estatus == Convert.ToChar("V")) &&
                        //                   MF_ArticuloNivelPrecioOfertas.Articulo == articulo && MF_ArticuloNivelPrecioOfertas.identificador != id &&
                        //                   ((FechaInicial >= MF_ArticuloNivelPrecioOfertas.FechaOfertaDesde && FechaInicial <= MF_ArticuloNivelPrecioOfertas.FechaOfertaHasta) ||
                        //                   (FechaFinal >= MF_ArticuloNivelPrecioOfertas.FechaOfertaDesde && FechaFinal <= MF_ArticuloNivelPrecioOfertas.FechaOfertaHasta))
                        //              select MF_ArticuloNivelPrecioOfertas).Count();

                        //if (Convert.ToInt32(qNivel) == 0)
                        //{
                        //    var qqNivel = (from MF_ArticuloNivelPrecioOfertas in db.MF_ArticuloNivelPrecioOfertas
                        //                   where
                        //                        MF_ArticuloNivelPrecioOfertas.Estatus == Convert.ToChar("V") &&
                        //                        MF_ArticuloNivelPrecioOfertas.Articulo == articulo
                        //                   select MF_ArticuloNivelPrecioOfertas).Count();

                        //    if (Convert.ToInt32(qqNivel) > 0)
                        //    {
                        //        mRetorna = false;
                        //        mensaje = string.Format("El Artículo {0} tiene una oferta vigente por niveles de precio, no es posible grabar una oferta en este momento.", articulo);
                        //        return mRetorna;
                        //    }
                        //}

                        //if (Convert.ToInt32(qNivel) > 0)
                        //{
                        //    mRetorna = false;
                        //    mensaje = string.Format("El Artículo {0} tiene una oferta pendiente o vigente por niveles de precio en el rango de fechas especificado.", articulo);
                        //    return mRetorna;
                        //}


                        var q = (from MF_ArticuloNivelPrecioOfertas in db.MF_ArticuloNivelPrecioOfertas
                                 where
                                      (MF_ArticuloNivelPrecioOfertas.Estatus == Convert.ToChar("P") || MF_ArticuloNivelPrecioOfertas.Estatus == Convert.ToChar("V")) &&
                                      MF_ArticuloNivelPrecioOfertas.NivelPrecio == nivel &&
                                      MF_ArticuloNivelPrecioOfertas.Articulo == articulo &&
                                      ((FechaInicial >= MF_ArticuloNivelPrecioOfertas.FechaOfertaDesde && FechaInicial <= MF_ArticuloNivelPrecioOfertas.FechaOfertaHasta) ||
                                      (FechaFinal >= MF_ArticuloNivelPrecioOfertas.FechaOfertaDesde && FechaFinal <= MF_ArticuloNivelPrecioOfertas.FechaOfertaHasta))
                                 select MF_ArticuloNivelPrecioOfertas).Count();

                        if (Convert.ToInt32(q) == 0)
                        {
                            var qq = (from MF_ArticuloNivelPrecioOfertas in db.MF_ArticuloNivelPrecioOfertas
                                      where
                                           MF_ArticuloNivelPrecioOfertas.Estatus == Convert.ToChar("V") &&
                                           MF_ArticuloNivelPrecioOfertas.NivelPrecio == nivel &&
                                           MF_ArticuloNivelPrecioOfertas.Articulo == articulo
                                      select MF_ArticuloNivelPrecioOfertas).Count();

                            if (Convert.ToInt32(qq) > 0)
                            {
                                mRetorna = false;
                                mensaje = string.Format("El Artículo {0} en el nivel {1} tiene una oferta vigente, no es posible grabar una oferta en este momento.", articulo, nivel);
                                return mRetorna;
                            }
                        }

                        if (Convert.ToInt32(q) > 0)
                        {
                            mRetorna = false;
                            mensaje = string.Format("El Artículo {0} en el nivel {1} tiene una oferta pendiente o vigente en el rango de fechas especificado.", articulo, nivel);
                            return mRetorna;
                        }
                        else
                        {
                            string mEstatus = "P";
                            MF_ArticuloNivelPrecioOferta iMF_ArticuloNivelPrecioOfertas = new MF_ArticuloNivelPrecioOferta
                            {
                                NivelPrecio = nivel,
                                Articulo = articulo,
                                FechaOfertaDesde = FechaInicial,
                                FechaOfertaHasta = FechaFinal,
                                PrecioOriginal = 0,
                                PrecioOferta = 0,
                                Estatus = Convert.ToChar(mEstatus),
                                Descripcion = descripcion,
                                RecordDate = DateTime.Now,
                                CreatedBy = System.Environment.UserName,
                                CreateDate = DateTime.Now,
                                pctjDescuento = porcentaje,
                                identificador = id,
                                tipo = tipo
                            };
                            db.MF_ArticuloNivelPrecioOfertas.InsertOnSubmit(iMF_ArticuloNivelPrecioOfertas);
                            db.SubmitChanges();

                            mensaje = "La oferta de los artículos en los diferentes niveles fue grabada exitosamente.";
                        }
                    }
                }

                transactionScope.Complete();
            }
        }
        catch (Exception ex)
        {
            mRetorna = false;
            mensaje = string.Format("Error al grabar la oferta del artículo en el nivel. {0}", ex.Message);
        }

        return mRetorna;
    }

    public static IQueryable<UsuariosCancelanOfertas> ListarUsuariosCancelan()
    {
        dbDataContext db = new dbDataContext();

        var q =
            from MF_UsuariosCancelanOfertas in db.MF_UsuariosCancelanOfertas
            orderby MF_UsuariosCancelanOfertas.usuario
            select new UsuariosCancelanOfertas()
            {
                usuario = MF_UsuariosCancelanOfertas.usuario
            };
        return q;
    }

    public static bool GrabarUsuariosCancelanOfertas(string[] usuarios, ref string mensaje)
    {
        dbDataContext db = new dbDataContext();
        bool mRetorna = true;

        try
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                var BorrarUsuariosCancelanOfertas =
                from mf_usuarioscancelanofertas in db.MF_UsuariosCancelanOfertas
                select mf_usuarioscancelanofertas;
                foreach (var del in BorrarUsuariosCancelanOfertas)
                {
                    db.MF_UsuariosCancelanOfertas.DeleteOnSubmit(del);
                }
                db.SubmitChanges();

                for (int ii = 0; ii <= (usuarios.Length - 1); ii++)
                {
                    MF_UsuariosCancelanOferta iMF_UsuariosCancelanOfertas = new MF_UsuariosCancelanOferta
                    {
                        usuario = usuarios[ii]
                    };

                    db.MF_UsuariosCancelanOfertas.InsertOnSubmit(iMF_UsuariosCancelanOfertas);
                    db.SubmitChanges();
                }

                transactionScope.Complete();
                mensaje = "Los usuarios con permiso para cancelar ofertas fueron grabados exitosamente.";
            }
        }
        catch (Exception ex)
        {
            mRetorna = false;
            mensaje = string.Format("Error al grabar los usuarios con permisos para cancelar ofertas. {0}", ex.Message);
        }
           
        return mRetorna;
    }

    public static void ActualizarPrecios()
    {
        string mensaje = "";
        dbDataContext db = new dbDataContext();

        try
        {
            var q = from c in db.MF_NIVEL_PRECIO_COEFICIENTEs
                    orderby c.NIVEL_PRECIO
                    select c;
            foreach (var p in q)
            {
                var UpdatePrecios = from x in db.ARTICULO_PRECIOs
                                    where x.NIVEL_PRECIO == p.NIVEL_PRECIO
                                    select x;
                foreach (var PrecioArticulo in UpdatePrecios)
                {
                    PrecioArticulo.PRECIO = (from a in db.ARTICULOs where a.ARTICULO1 == PrecioArticulo.ARTICULO select a).Single().PRECIO_BASE_LOCAL * p.COEFICIENTE_MULR;
                    PrecioArticulo.MARGEN_MULR = p.COEFICIENTE_MULR;
                }
            }
            db.SubmitChanges();

            MF_BitacoraPreciosOferta iMF_BitacoraPreciosOfertas = new MF_BitacoraPreciosOferta
            {
                Tipo = Convert.ToChar("P"),
                Resultado = Convert.ToChar("S"),
                Descripcion = "Los precios fueron actualizados exitosamente.",
                RecordDate = DateTime.Now,
                CreatedBy = Environment.UserName
            };

            db.MF_BitacoraPreciosOfertas.InsertOnSubmit(iMF_BitacoraPreciosOfertas);
            db.SubmitChanges();

            mensaje = "Los usuarios con permiso para cancelar ofertas fueron grabados exitosamente.";
        }
        catch (Exception ex)
        {
            dbDataContext db2 = new dbDataContext();

            MF_BitacoraPreciosOferta iMF_BitacoraPreciosOfertas = new MF_BitacoraPreciosOferta
            {
                Tipo = Convert.ToChar("P"),
                Resultado = Convert.ToChar("F"),
                Descripcion = string.Format("Error al actualizar los precios. - {0} - {1}", ex.Message, mensaje),
                RecordDate = DateTime.Now,
                CreatedBy = Environment.UserName
            };

            db2.MF_BitacoraPreciosOfertas.InsertOnSubmit(iMF_BitacoraPreciosOfertas);
            db2.SubmitChanges();
        }
    }

    public static IQueryable<BitacoraProcesos> ListarBitacoraProcesos(dbDataContext db, string inicio, string fin)
    {
        DateTime mInicial, mFinal;
        char[] Separador = { '/' };
        string[] stringFechaInicial = inicio.Split(Separador);
        string[] stringFechaFinal = fin.Split(Separador);

        if (inicio.Trim().Length == 0)
        {
            mInicial = new DateTime(2012, 3, 1);
        }
        else
        {
            mInicial = new DateTime(Convert.ToInt32(stringFechaInicial[2]), Convert.ToInt32(stringFechaInicial[1]), Convert.ToInt32(stringFechaInicial[0]));
        }
        if (fin.Trim().Length == 0)
        {
            mFinal = new DateTime(3099, 12, 31);
        }
        else
        {
            mFinal = new DateTime(Convert.ToInt32(stringFechaFinal[2]), Convert.ToInt32(stringFechaFinal[1]), Convert.ToInt32(stringFechaFinal[0]));
        }

        mFinal = mFinal.AddDays(1);

        var q =
            from mf_BitacoraPreciosOfertas in db.MF_BitacoraPreciosOfertas
            where
              mf_BitacoraPreciosOfertas.RecordDate >= mInicial &&
              mf_BitacoraPreciosOfertas.RecordDate <= mFinal
            orderby mf_BitacoraPreciosOfertas.RecordDate descending 
            select new BitacoraProcesos()
            {
                Tipo = mf_BitacoraPreciosOfertas.Tipo == Convert.ToChar("P") ? "Actualización de Precios" : mf_BitacoraPreciosOfertas.Tipo == Convert.ToChar("I") ? "Inicio de Ofertas" : "Fin de Ofertas",
                Resultado = mf_BitacoraPreciosOfertas.Resultado == Convert.ToChar("S") ? "Exito" : "Error",
                Descripcion = mf_BitacoraPreciosOfertas.Descripcion,
                FechaRegistro = mf_BitacoraPreciosOfertas.RecordDate,
                UsuarioCreo = mf_BitacoraPreciosOfertas.CreatedBy
            };

        return q;
    }
    
    public static IQueryable<BitacoraProcesos> ListarBitacoraProcesosPorCriterio(string inicio, string fin, string tipo, string resultado)
    {
        dbDataContext db = new dbDataContext();
        var q = ListarBitacoraProcesos(db, inicio, fin);

        if (tipo == "T")
        {
            if (resultado != "T") q = q.Where(x => x.Resultado == resultado);
        }
        else
        {
            if (resultado != "T") q = q.Where(x => x.Tipo == tipo && x.Resultado == resultado);
            else q = q.Where(x => x.Tipo == tipo);
        }

        return q;
    }

    public static bool PuedeCancelarOfertas(string usuario)
    {
        dbDataContext db = new dbDataContext();
        if ((from c in db.MF_UsuariosCancelanOfertas where c.usuario == usuario select c).Count() == 0) return false; else return true;
    }

    public static bool CancelaOfertas(string tipo, string articulo, string nivelPrecio, DateTime fechaInicial, string motivo, string usuario, ref string mensaje)
    {
        dbDataContext db = new dbDataContext();
        bool mRetorna = true; string estatus;

        try
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                switch (tipo)
                {
                    case "Por Artículo":
                        estatus = (from c in db.MF_ArticuloPrecioOferta
                                          where c.Articulo == articulo && c.FechaOfertaDesde == fechaInicial
                                          select c).Single().Estatus.ToString();
                        break;
                    case "Por Nivel de Precio":
                        estatus = (from c in db.MF_NivelPrecioCoeficienteOfertas
                                   where c.NivelPrecio == nivelPrecio && c.FechaOfertaDesde == fechaInicial
                                   select c).Single().Estatus.ToString();
                        break;
                    default:
                        estatus = (from c in db.MF_ArticuloNivelPrecioOfertas
                                        where c.NivelPrecio == nivelPrecio && c.Articulo == articulo && c.FechaOfertaDesde == fechaInicial
                                        select c).Single().Estatus.ToString();
                        break;
                }

                if (estatus == "C")
                {
                    mensaje = "La oferta que seleccionó ya se encuentra cancelada.";
                    mRetorna = false;
                    return mRetorna;
                }
                if (estatus == "F")
                {
                    mensaje = "La oferta que seleccionó se encuentra finalizada.";
                    mRetorna = false;
                    return mRetorna;
                }

                if (estatus == "P")
                {
                    switch (tipo)
                    {
                        case "Por Artículo":
                            var updateArticulo =
                            from c in db.MF_ArticuloPrecioOferta
                            where c.Articulo == articulo && c.FechaOfertaDesde == fechaInicial
                            select c;
                            foreach (var c in updateArticulo)
                            {
                                c.Estatus = Convert.ToChar("C");
                                c.MotivoCancela = motivo;
                                c.UsuarioCancela = usuario;
                                c.FechaCancela = DateTime.Now;
                            }
                  
                            db.SubmitChanges();
                            break;
                        case "Por Nivel de Precio":
                            var updateNivel =
                            from c in db.MF_NivelPrecioCoeficienteOfertas
                            where c.NivelPrecio == nivelPrecio && c.FechaOfertaDesde == fechaInicial
                            select c;
                            foreach (var c in updateNivel)
                            {
                                c.Estatus = Convert.ToChar("C");
                                c.MotivoCancela = motivo;
                                c.UsuarioCancela = usuario;
                                c.FechaCancela = DateTime.Now;
                            }
                  
                            db.SubmitChanges();
                            break;
                        default:
                            var updateArticuloNivel =
                            from c in db.MF_ArticuloNivelPrecioOfertas
                            where c.NivelPrecio == nivelPrecio && c.Articulo == articulo && c.FechaOfertaDesde == fechaInicial
                            select c;
                            foreach (var c in updateArticuloNivel)
                            {
                                c.Estatus = Convert.ToChar("C");
                                c.MotivoCancela = motivo;
                                c.UsuarioCancela = usuario;
                                c.FechaCancela = DateTime.Now;
                            }
                  
                            db.SubmitChanges();
                            break;
                    }                    
                }
                else
                {
                    switch (tipo)
                    {
                        case "Por Artículo":
                            var updateArticulo =
                            from c in db.MF_ArticuloPrecioOferta
                            where c.Articulo == articulo && c.FechaOfertaDesde == fechaInicial
                            select c;
                            foreach (var c in updateArticulo)
                            {
                                c.FechaOfertaHasta = DateTime.Now.Date;
                                c.MotivoCancela = motivo;
                                c.UsuarioCancela = usuario;
                                c.FechaCancela = DateTime.Now;
                            }

                            db.SubmitChanges();
                            break;
                        case "Por Nivel de Precio":
                            var updateNivel =
                            from c in db.MF_NivelPrecioCoeficienteOfertas
                            where c.NivelPrecio == nivelPrecio && c.FechaOfertaDesde == fechaInicial
                            select c;
                            foreach (var c in updateNivel)
                            {
                                c.FechaOfertaHasta = DateTime.Now.Date;
                                c.MotivoCancela = motivo;
                                c.UsuarioCancela = usuario;
                                c.FechaCancela = DateTime.Now;
                            }

                            db.SubmitChanges();
                            break;
                        default:
                            var updateArticuloNivel =
                            from c in db.MF_ArticuloNivelPrecioOfertas
                            where c.NivelPrecio == nivelPrecio && c.Articulo == articulo && c.FechaOfertaDesde == fechaInicial
                            select c;
                            foreach (var c in updateArticuloNivel)
                            {
                                c.FechaOfertaHasta = DateTime.Now.Date;
                                c.MotivoCancela = motivo;
                                c.UsuarioCancela = usuario;
                                c.FechaCancela = DateTime.Now;
                            }

                            db.SubmitChanges();
                            break;
                    }                      
                }

                transactionScope.Complete();
                mensaje = "La oferta fue cancelada exitosamente.";
            }

            if (estatus == "V") db.spFinalizaOfertas();
        }
        catch (Exception ex)
        {
            mRetorna = false;
            mensaje = string.Format("Error al cancelar la oferta. {0}", ex.Message);
        }

        return mRetorna;
    }

    public static decimal PrecioNivelArticulo(string nivel, string articulo)
    {
        dbDataContext db = new dbDataContext();
        return (from c in db.ARTICULO_PRECIOs
                where c.NIVEL_PRECIO == nivel && c.ARTICULO == articulo
                select c).Single().PRECIO;
    }

}
