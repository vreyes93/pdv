﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq.SqlClient;
using System.Web;
using System.Transactions;
using FiestaNETServices.Services;

/// <summary>
/// Summary description for DataLayerActividadVentas
/// </summary>
public static class DataLayerActividadVentas
{
    public static IQueryable<ReporteActividadVentas> ReporteActividadVentas(DateTime FechaInicial, DateTime FechaFinal, string Proveedor, string Articulo, string NombreProveedor, string NombreArticulo, string Tipo)
    {
        dbDataContext db = new dbDataContext();
        
        DateTime mFechaInicialAnterior = FechaInicial.AddYears(-1);
        DateTime mFechaFinalAnterior = FechaFinal.AddYears(-1);

        db.CommandTimeout = 999999999;

        if (Tipo == "N")
        {
            var q =
                from fl in db.FACTURA_LINEAs
                join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO }
                join a in db.ARTICULOs on new { ARTICULO = fl.ARTICULO } equals new { ARTICULO = Convert.ToString(a.ARTICULO1) }
                join p in db.PROVEEDORs on new { PROVEEDOR = a.PROVEEDOR } equals new { PROVEEDOR = Convert.ToString(p.PROVEEDOR1) }
                where f.FECHA >= FechaInicial && f.FECHA <= FechaFinal && SqlMethods.Like(p.PROVEEDOR1, Proveedor) &&
                (Articulo == "%" ? SqlMethods.Like(fl.ARTICULO, Articulo) : fl.ARTICULO.StartsWith(Articulo)) && p.PROVEEDOR1 != "01-0000" && p.PROVEEDOR1 != "01-0001" && (p.PROVEEDOR1.StartsWith("01") || p.PROVEEDOR1.StartsWith("02"))
                orderby p.PROVEEDOR1
                group new { fl, a, p } by new
                {
                    fl.ARTICULO,
                    a.DESCRIPCION,
                    a.PROVEEDOR,
                    p.NOMBRE
                } into g
                select new ReporteActividadVentas()
                {
                    Proveedor = string.Format("{0}  -  {1}", g.Key.PROVEEDOR, g.Key.NOMBRE),
                    CodigoProveedor = g.Key.PROVEEDOR,
                    NombreProveedor = g.Key.NOMBRE,
                    Articulo = g.Key.ARTICULO,
                    NombreArticulo = g.Key.DESCRIPCION.Trim().Length <= 70 ? g.Key.DESCRIPCION : string.Format("{0}  ...", g.Key.DESCRIPCION.Substring(0, 70)),
                    UltimaCompra = (from ar in db.ARTICULOs where ar.ARTICULO1 == g.Key.ARTICULO select ar.ULTIMO_INGRESO).Single().ToShortDateString(),
                    Compras = (from tinv in db.TRANSACCION_INVs where tinv.FECHA >= FechaInicial && tinv.FECHA <= FechaFinal && tinv.BODEGA == "F01" && tinv.AJUSTE_CONFIG == "~OO~" && tinv.NATURALEZA == Convert.ToString('E') && tinv.ARTICULO == g.Key.ARTICULO select tinv).Count() == 0 ? 0 : 
                                (Int32)(from tinv in db.TRANSACCION_INVs where tinv.FECHA >= FechaInicial && tinv.FECHA <= FechaFinal && tinv.BODEGA == "F01" && tinv.AJUSTE_CONFIG == "~OO~" && tinv.NATURALEZA == Convert.ToString('E') && tinv.ARTICULO == g.Key.ARTICULO select tinv.CANTIDAD).Sum(),
                    ExistenciasFecha = 0,
                    Ventas = (Int32)g.Sum(p => p.fl.CANTIDAD), 
                    VentasAnterior = (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.CANTIDAD).Count() == 0 ? 0 :
                                        (Int32)(from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.CANTIDAD).Sum(),
                    CostoVentaUnitario = (Decimal)g.Sum(p => p.fl.COSTO_TOTAL) / (Int32)g.Sum(p => p.fl.CANTIDAD),
                    CostoVentaUnitarioAnterior = ((from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.CANTIDAD).Sum() == 0 ? 0 :
                                            (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.COSTO_TOTAL).Sum()) /
                                            ((from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.CANTIDAD).Sum() == 0 ? 1 :
                                        (Int32)(from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.CANTIDAD).Sum()),
                    CostoVentas = (Decimal)g.Sum(p => p.fl.COSTO_TOTAL),
                    CostoVentasAnterior = (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.CANTIDAD).Count() == 0 ? 0 :
                                            (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.COSTO_TOTAL).Sum(),
                    ValorVentaUnitario = (Decimal)(g.Sum(p => p.fl.PRECIO_TOTAL)) / (Int32)g.Sum(p => p.fl.CANTIDAD),
                    ValorVentaUnitarioAnterior = ((from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.CANTIDAD).Sum() == 0 ? 0 :
                                            (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.PRECIO_TOTAL).Sum()) /
                                            ((from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.CANTIDAD).Sum() == 0 ? 1 :
                                        (Int32)(from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.CANTIDAD).Sum()),
                    ValorVentas = (Decimal)(g.Sum(p => p.fl.PRECIO_TOTAL)),
                    ValorVentasAnterior = (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.CANTIDAD).Count() == 0 ? 0 :
                                            (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.PRECIO_TOTAL).Sum(),
                    MargenVentasMonto = g.Sum(p => p.fl.PRECIO_TOTAL) == 0 ? 0 : (Decimal)(g.Sum(p => p.fl.PRECIO_TOTAL) - g.Sum(p => p.fl.COSTO_TOTAL)),
                    MargenVentasMontoAnterior = (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.CANTIDAD).Count() == 0 ? 0 :
                                                (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.PRECIO_TOTAL).Sum() == 0 ? 0 :
                                                (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.PRECIO_TOTAL).Sum() -
                                                (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.COSTO_TOTAL).Sum(),
                    MargenVentasPorcentaje = (Decimal)(g.Sum(p => p.fl.PRECIO_TOTAL) == 0 ? 0 : ((Decimal)(g.Sum(p => p.fl.PRECIO_TOTAL) - g.Sum(p => p.fl.COSTO_TOTAL)) * 100) / (Decimal)(g.Sum(p => p.fl.PRECIO_TOTAL))),
                    MargenVentasPorcentajeAnterior = (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.CANTIDAD).Count() == 0 ? 0 :
                                                        (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.PRECIO_TOTAL).Sum() == 0 ? 0 :
                                                        (((from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.PRECIO_TOTAL).Sum() -
                                                        (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.COSTO_TOTAL).Sum()) * 100) /
                                                        (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.PRECIO_TOTAL).Sum(),
                    MargenPrecio = (Decimal)g.Sum(p => p.fl.COSTO_TOTAL) == 0 ? 0 : (Decimal)(g.Sum(p => p.fl.PRECIO_TOTAL) / (Decimal)g.Sum(p => p.fl.COSTO_TOTAL)),
                    MargenPrecioAnterior = (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.CANTIDAD).Count() == 0 ? 0 :
                                            (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.COSTO_TOTAL).Sum() == 0 ? 0 :
                                            (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.PRECIO_TOTAL).Sum() /
                                            (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == g.Key.ARTICULO && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" select fl.COSTO_TOTAL).Sum(),
                    EntradasPositivas = (from tinv in db.TRANSACCION_INVs where tinv.FECHA < FechaInicial && tinv.NATURALEZA == Convert.ToString('E') && tinv.ARTICULO == g.Key.ARTICULO && tinv.CANTIDAD > 0 select tinv).Count() == 0 ? 0 : 
                                        (Int32)(from tinv in db.TRANSACCION_INVs where tinv.FECHA < FechaInicial && tinv.NATURALEZA == Convert.ToString('E') && tinv.ARTICULO == g.Key.ARTICULO && tinv.CANTIDAD > 0 select tinv.CANTIDAD).Sum(),
                    EntradasNegativas = (from tinv in db.TRANSACCION_INVs where tinv.FECHA < FechaInicial && tinv.NATURALEZA == Convert.ToString('E') && tinv.ARTICULO == g.Key.ARTICULO && tinv.CANTIDAD < 0 select tinv).Count() == 0 ? 0 : 
                                        (Int32)(from tinv in db.TRANSACCION_INVs where tinv.FECHA < FechaInicial && tinv.NATURALEZA == Convert.ToString('E') && tinv.ARTICULO == g.Key.ARTICULO && tinv.CANTIDAD < 0 select tinv.CANTIDAD).Sum(),
                    SalidasPositivas = (from tinv in db.TRANSACCION_INVs where tinv.FECHA < FechaInicial && tinv.NATURALEZA == Convert.ToString('S') && tinv.ARTICULO == g.Key.ARTICULO && tinv.CANTIDAD > 0 select tinv.CANTIDAD).Count() == 0 ? 0 : 
                                        (Int32)(from tinv in db.TRANSACCION_INVs where tinv.FECHA < FechaInicial && tinv.NATURALEZA == Convert.ToString('S') && tinv.ARTICULO == g.Key.ARTICULO && tinv.CANTIDAD > 0 select tinv.CANTIDAD).Sum(),
                    SalidasNegativas = (from tinv in db.TRANSACCION_INVs where tinv.FECHA < FechaInicial && tinv.NATURALEZA == Convert.ToString('S') && tinv.ARTICULO == g.Key.ARTICULO && tinv.CANTIDAD < 0 select tinv.CANTIDAD).Count() == 0 ? 0 : 
                                        (Int32)(from tinv in db.TRANSACCION_INVs where tinv.FECHA < FechaInicial && tinv.NATURALEZA == Convert.ToString('S') && tinv.ARTICULO == g.Key.ARTICULO && tinv.CANTIDAD < 0 select tinv.CANTIDAD).Sum()
                };

            return q;
        }
        else
        {
            var q =
                from a in db.ARTICULOs
                join p in db.PROVEEDORs on new { PROVEEDOR = a.PROVEEDOR } equals new { PROVEEDOR = Convert.ToString(p.PROVEEDOR1) }
                where SqlMethods.Like(p.PROVEEDOR1, Proveedor) && (Articulo == "%" ? SqlMethods.Like(a.ARTICULO1, Articulo) : a.ARTICULO1.StartsWith(Articulo)) && a.ACTIVO == "S" && 
                p.PROVEEDOR1 != "01-0000" && p.PROVEEDOR1 != "01-0001" && (p.PROVEEDOR1.StartsWith("01") || p.PROVEEDOR1.StartsWith("02"))
                orderby p.PROVEEDOR1, a.ARTICULO1
                select new ReporteActividadVentas()
                {
                    Proveedor = string.Format("{0}  -  {1}", p.PROVEEDOR1, p.NOMBRE),
                    CodigoProveedor = p.PROVEEDOR1,
                    NombreProveedor = p.NOMBRE,
                    Articulo = a.ARTICULO1,
                    NombreArticulo = a.DESCRIPCION.Trim().Length <= 70 ? a.DESCRIPCION : string.Format("{0}  ...", a.DESCRIPCION.Substring(0, 70)),
                    ExistenciasFecha = 0,
                    //Compras = (from tinv in db.TRANSACCION_INVs where tinv.FECHA >= FechaInicial && tinv.FECHA <= FechaFinal && tinv.BODEGA == "F01" && tinv.AJUSTE_CONFIG == "~OO~" && tinv.NATURALEZA == Convert.ToString('E') && tinv.ARTICULO == a.ARTICULO1 select tinv).Count() == 0 ? 0 : (Int32)(from tinv in db.TRANSACCION_INVs where tinv.FECHA >= FechaInicial && tinv.FECHA <= FechaFinal && tinv.BODEGA == "F01" && tinv.AJUSTE_CONFIG == "~OO~" && tinv.NATURALEZA == Convert.ToString('E') && tinv.ARTICULO == a.ARTICULO1 select tinv.CANTIDAD).Sum(),
                    Compras = (from tinv in db.TRANSACCION_INVs where tinv.FECHA >= FechaInicial && tinv.FECHA <= FechaFinal && tinv.AJUSTE_CONFIG == "~OO~" && tinv.NATURALEZA == Convert.ToString('E') && tinv.ARTICULO == a.ARTICULO1 select tinv).Count() == 0 ? 0 : (Int32)(from tinv in db.TRANSACCION_INVs where tinv.FECHA >= FechaInicial && tinv.FECHA <= FechaFinal && tinv.AJUSTE_CONFIG == "~OO~" && tinv.NATURALEZA == Convert.ToString('E') && tinv.ARTICULO == a.ARTICULO1 select tinv.CANTIDAD).Sum(),
                    UltimaCompra = a.ULTIMO_INGRESO.ToShortDateString(),
                    Ventas = (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl).Count(),
                    VentasAnterior = (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.CANTIDAD).Count() == 0 ? 0 :
                                        (Int32)(from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.CANTIDAD).Sum(),
                    CostoVentaUnitario = (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl).Count() == 0 ? 0 :
                                    ((from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl.COSTO_TOTAL).Sum()) /
                                    (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl).Count(),
                    CostoVentaUnitarioAnterior = (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.CANTIDAD).Count() == 0 ? 0 :
                                            ((from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.CANTIDAD).Count() == 0 ? 0 :
                                            (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.COSTO_TOTAL).Sum()) /
                                            (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.CANTIDAD).Count() == 0 ? 0 :
                                        (Int32)(from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.CANTIDAD).Sum(),
                    CostoVentas = (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl).Count() == 0 ? 0 :
                                  (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl.COSTO_TOTAL).Sum(),
                    CostoVentasAnterior = (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.CANTIDAD).Count() == 0 ? 0 :
                                            (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.COSTO_TOTAL).Sum(),

                    ValorVentaUnitario = (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl).Count() == 0 ? 0 :
                                    ((from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl.PRECIO_TOTAL).Sum()) /
                                    (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl).Count(),
                    ValorVentas = (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl).Count() == 0 ? 0 :
                                  (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl.PRECIO_TOTAL).Sum(),


                    ValorVentaUnitarioAnterior = (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl).Count() == 0 ? 0 :
                                  (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl.PRECIO_TOTAL).Sum(),
                    ValorVentasAnterior = (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.CANTIDAD).Count() == 0 ? 0 :
                                            (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.PRECIO_TOTAL).Sum(),
                    MargenVentasMonto = (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl).Count() == 0 ? 0 :
                                        (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl.PRECIO_TOTAL).Sum() == 0 ? 0 :
                                        (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl.PRECIO_TOTAL).Sum() -
                                        (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl.COSTO_TOTAL).Sum(),
                    MargenVentasMontoAnterior = (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.CANTIDAD).Count() == 0 ? 0 :
                                                (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.PRECIO_TOTAL).Sum() == 0 ? 0 :
                                                (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.PRECIO_TOTAL).Sum() -
                                                (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.COSTO_TOTAL).Sum(),
                    MargenVentasPorcentaje = (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl).Count() == 0 ? 0 :
                                             (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl.PRECIO_TOTAL).Sum() == 0 ? 0 :
                                             (((from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl.PRECIO_TOTAL).Sum() -
                                             (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl.COSTO_TOTAL).Sum()) * 100) /
                                             ((from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl.PRECIO_TOTAL).Sum()),
                    MargenVentasPorcentajeAnterior = (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.CANTIDAD).Count() == 0 ? 0 :
                                                        (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.PRECIO_TOTAL).Sum() == 0 ? 0 :
                                                        (((from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.PRECIO_TOTAL).Sum() -
                                                        (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.COSTO_TOTAL).Sum()) * 100) /
                                                        (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.PRECIO_TOTAL).Sum(),
                    MargenPrecio = (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl).Count() == 0 ? 0 :
                                   (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl.COSTO_TOTAL).Sum() == 0 ? 0 :
                                   (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl.PRECIO_TOTAL).Sum() /
                                   (from fl in db.FACTURA_LINEAs where fl.ARTICULO == a.ARTICULO1 && fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.ANULADA == "N" select fl.COSTO_TOTAL).Sum(),
                    MargenPrecioAnterior = (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.CANTIDAD).Count() == 0 ? 0 :
                                            (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.COSTO_TOTAL).Sum() == 0 ? 0 :
                                            (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.PRECIO_TOTAL).Sum() /
                                            (from fl in db.FACTURA_LINEAs where fl.FECHA_FACTURA >= mFechaInicialAnterior && fl.FECHA_FACTURA <= mFechaFinalAnterior && fl.ARTICULO == a.ARTICULO1 select fl.COSTO_TOTAL).Sum(),
                    EntradasPositivas = (from tinv in db.TRANSACCION_INVs where tinv.FECHA < FechaInicial && tinv.NATURALEZA == Convert.ToString('E') && tinv.ARTICULO == a.ARTICULO1 && tinv.CANTIDAD > 0 select tinv).Count() == 0 ? 0 :
                                        (Int32)(from tinv in db.TRANSACCION_INVs where tinv.FECHA < FechaInicial && tinv.NATURALEZA == Convert.ToString('E') && tinv.ARTICULO == a.ARTICULO1 && tinv.CANTIDAD > 0 select tinv.CANTIDAD).Sum(),
                    EntradasNegativas = (from tinv in db.TRANSACCION_INVs where tinv.FECHA < FechaInicial && tinv.NATURALEZA == Convert.ToString('E') && tinv.ARTICULO == a.ARTICULO1 && tinv.CANTIDAD < 0 select tinv).Count() == 0 ? 0 :
                                        (Int32)(from tinv in db.TRANSACCION_INVs where tinv.FECHA < FechaInicial && tinv.NATURALEZA == Convert.ToString('E') && tinv.ARTICULO == a.ARTICULO1 && tinv.CANTIDAD < 0 select tinv.CANTIDAD).Sum(),
                    SalidasPositivas = (from tinv in db.TRANSACCION_INVs where tinv.FECHA < FechaInicial && tinv.NATURALEZA == Convert.ToString('S') && tinv.ARTICULO == a.ARTICULO1 && tinv.CANTIDAD > 0 select tinv.CANTIDAD).Count() == 0 ? 0 :
                                       (Int32)(from tinv in db.TRANSACCION_INVs where tinv.FECHA < FechaInicial && tinv.NATURALEZA == Convert.ToString('S') && tinv.ARTICULO == a.ARTICULO1 && tinv.CANTIDAD > 0 select tinv.CANTIDAD).Sum(),
                    SalidasNegativas = (from tinv in db.TRANSACCION_INVs where tinv.FECHA < FechaInicial && tinv.NATURALEZA == Convert.ToString('S') && tinv.ARTICULO == a.ARTICULO1 && tinv.CANTIDAD < 0 select tinv.CANTIDAD).Count() == 0 ? 0 :
                                       (Int32)(from tinv in db.TRANSACCION_INVs where tinv.FECHA < FechaInicial && tinv.NATURALEZA == Convert.ToString('S') && tinv.ARTICULO == a.ARTICULO1 && tinv.CANTIDAD < 0 select tinv.CANTIDAD).Sum()
                };

            return q;
        }
                  
    }
            
    public static IQueryable<Clasificaciones> ListarClasificacionesCriterio(string clasificacion, string descripcion)
    {
        dbDataContext db = new dbDataContext();
           
        var q =
            from c in db.CLASIFICACIONs
            orderby c.CLASIFICACION1
            select new Clasificaciones()
            {
                Clasificacion = c.CLASIFICACION1,
                Descripcion = c.DESCRIPCION
            };

        if (clasificacion.Trim().Length > 0) q = q.Where(x => x.Clasificacion.StartsWith(clasificacion));
        if (descripcion.Trim().Length > 0) q = q.Where(x => x.Descripcion.ToUpper().Contains(descripcion.ToUpper()));

        return q;
    }

}