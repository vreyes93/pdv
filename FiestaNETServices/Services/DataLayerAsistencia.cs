﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.Linq.SqlClient;
using System.Transactions;
using FiestaNETServices.Services;
using System.Net.Mail;
using System.Net;
using System.IO;
using System.Net.Mime;
using System.Data;

/// <summary>
/// Summary description for DataLayerAsistencia
/// </summary>

public static class DataLayerAsistencia
{
    public static IQueryable<EmpleadoHuella> DevuelveEmpleadoHuella()
    {
        dbDataContext db = new dbDataContext();

        var q = from e in db.MF_EmpleadoHuellas
                select new EmpleadoHuella()
                {
                    Huella = e.HUELLA.ToArray(),
                    Empleado = e.EMPLEADO,
                    Empresa = e.EMPRESA,
                    Fecha = DateTime.Now
                };

        return q;
    }
    
    public static string DevuelveNombreEmpleado(string empleado, string empresa)
    {
        dbDataContext db = new dbDataContext();

        SqlConnection mConexion = new SqlConnection(db.Connection.ConnectionString);
        SqlCommand mCommand = new SqlCommand();

        mConexion.Open();
        mCommand.Connection = mConexion;

        mCommand.CommandText = string.Format("SELECT NOMBRE FROM {0}.EMPLEADO WHERE EMPLEADO = '{1}'", empresa, empleado);
        string mNombreEmpleado = Convert.ToString(mCommand.ExecuteScalar());

        mConexion.Close();

        return mNombreEmpleado;
    }
    
    public static string DevuelveTipo(string empleado, string empresa)
    {
        string mTipo = "";
        dbDataContext db = new dbDataContext();

        if ((from e in db.MF_EmpleadoHuellaAsistencias where e.EMPLEADO == empleado && e.EMPRESA == empresa && e.FECHA == DateTime.Now.Date select e).Count() == 0)
        {
            mTipo = "Entrada";
        }
        else
        {
            if ((from e in db.MF_EmpleadoHuellaAsistencias where e.EMPLEADO == empleado && e.EMPRESA == empresa && e.FECHA == DateTime.Now.Date orderby e.FECHAHORA descending select e).First().TIPO == Convert.ToChar("E"))
            {
                mTipo = "Salida";
            }
            else
            {
                mTipo = "Entrada";
            }
        }

        return mTipo;
    }

    public static DataTable DevuelveHuellas()
    {
        dbDataContext db = new dbDataContext();
        SqlConnection mConexion = new SqlConnection(db.Connection.ConnectionString);
        DataTable dt = new DataTable("huellas");

        System.Data.SqlClient.SqlDataAdapter daHuellas = new System.Data.SqlClient.SqlDataAdapter("SELECT H.HUELLA, H.EMPLEADO, H.EMPRESA, H.FECHA FROM ( SELECT	MEH.HUELLA, MEH.EMPLEADO, MEH.EMPRESA, GetDate() AS FECHA  , CASE	WHEN ISNULL(PE.ACTIVO,'-') = '-' THEN   CASE WHEN ISNULL(AE.ACTIVO,'-') = '-' THEN  CASE WHEN ISNULL(CE.ACTIVO,'-') = '-' THEN 'N' ELSE  CE.ACTIVO END ELSE AE.ACTIVO END ELSE PE.ACTIVO END ACTIVO FROM prodmult.MF_EmpleadoHuella MEH LEFT OUTER JOIN PRODMULT.EMPLEADO PE ON(MEH.EMPRESA = 'prodmult' AND MEH.EMPLEADO = PE.EMPLEADO) LEFT OUTER JOIN ARMADOS.EMPLEADO AE ON(MEH.EMPRESA = 'armados' AND MEH.EMPLEADO = AE.EMPLEADO) LEFT OUTER JOIN CREDIPLUS.EMPLEADO CE ON(MEH.EMPRESA = 'crediplus' AND MEH.EMPLEADO = CE.EMPLEADO) WHERE	1=1 AND HUELLA IS NOT NULL) H WHERE H.ACTIVO = 'S'", mConexion);
        daHuellas.Fill(dt);
        return dt;
    }

    public static DataTable DevuelveEmpleados(string nombre)
    {
        dbDataContext db = new dbDataContext();
        SqlConnection mConexion = new SqlConnection(db.Connection.ConnectionString);
        DataTable dt = new DataTable("empleados");

        System.Data.SqlClient.SqlDataAdapter daEmpleados = new System.Data.SqlClient.SqlDataAdapter(string.Format("SELECT EMPLEADO AS Empleado, NOMBRE AS Nombre, 'prodmult' AS Empresa FROM prodmult.EMPLEADO WHERE NOMBRE LIKE '%{0}%' AND ACTIVO = 'S' UNION SELECT EMPLEADO AS Empleado, NOMBRE AS Nombre, 'armados' AS Empresa FROM armados.EMPLEADO WHERE NOMBRE LIKE '%{0}%' AND ACTIVO = 'S' UNION SELECT EMPLEADO AS Empleado, NOMBRE AS Nombre, 'crediplus' AS Empresa FROM crediplus.EMPLEADO WHERE NOMBRE LIKE '%{0}%' AND ACTIVO = 'S' ORDER BY Nombre", nombre), mConexion);
        daEmpleados.Fill(dt);
        return dt;
    }

    public static bool PermiteCambiarHuella(string pass, ref string mensaje)
    {
        bool mRetorna = false;
        dbDataContext db = new dbDataContext();

        try
        {
            if ((from c in db.MF_AsistenciaConfiguras where c.passwordGrabarHuella == pass.ToLower() select c).Count() == 0)
            {
                mensaje = "Acceso denegado";
                return false;
            }

            mRetorna = true;
        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }

            mensaje = string.Format("Error al validar el permiso para grabar la huella. {0} {1}", ex.Message, m);
        }

        return mRetorna;
    }

    public static bool GrabarAsistencia(ref string mensaje, string empleado, string empresa)
    {
        bool mRetorna = true;
        string mTipo = DevuelveTipo(empleado, empresa);
        dbDataContext db = new dbDataContext();

        try
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                MF_EmpleadoHuellaAsistencia iAsistencia = new MF_EmpleadoHuellaAsistencia()
                {
                    EMPLEADO = empleado,
                    EMPRESA = empresa,
                    FECHA = DateTime.Now.Date,
                    FECHAHORA = DateTime.Now,
                    TIPO = Convert.ToChar(mTipo.Substring(0, 1))
                };

                db.MF_EmpleadoHuellaAsistencias.InsertOnSubmit(iAsistencia);
                db.SubmitChanges();

                transactionScope.Complete();
            }
        }
        catch (Exception ex)
        {
            mRetorna = false;
            mensaje = string.Format("Error al grabar la asistencia WS {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
        }

        return mRetorna;
    }

    public static bool GrabarHuella(ref string mensaje, byte[] huella, string empleado, string empresa)
    {
        bool mRetorna = true;
        dbDataContext db = new dbDataContext();

        try
        {
            db.spRegistraHuella(empleado, empresa, huella, ref mensaje);
        }
        catch (Exception ex)
        {
            mRetorna = false;
            mensaje = string.Format("Error al grabar la huella WS {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
        }

        return mRetorna;
    }

}

