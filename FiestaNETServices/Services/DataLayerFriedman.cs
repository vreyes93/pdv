﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq.SqlClient;
using System.Web;
using System.Transactions;
using FiestaNETServices.Services;

/// <summary>
/// Summary description for DataLayerFriedman
/// </summary>
/// 

public static class DataLayerFriedman
{
    
    public static IQueryable<MetasTiendas> ListarMetasTiendas(int semana, int anio)
    {
        dbFriedmanDataContext db = new dbFriedmanDataContext();
        
        var q =
            from tt in db.Tiendas
            where tt.ID == 1
            select new MetasTiendas()
            {
                F02Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 14 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 14 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F02Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 14 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 14 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F02Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F02" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                        join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                        join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                        join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                        where t.Codigo == "F02" && mt.Semana == semana && mt.Anio == anio
                                                                        select dv.VentaTotal).Sum(),
                F03Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 18 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 18 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F03Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 18 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 18 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F03Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F03" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F03" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F04Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 15 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 15 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F04Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 15 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 15 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F04Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F04" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F04" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F05Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 11 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 11 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F05Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 11 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 11 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F05Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F05" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F05" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F06Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 17 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 17 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F06Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 17 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 17 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F06Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F06" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F06" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F07Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 2 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 2 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F07Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 2 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 2 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F07Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F07" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F07" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F08Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 21 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 21 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F08Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 21 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 21 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F08Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F08" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F08" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F09Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 10 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 10 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F09Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 10 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 10 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F09Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F09" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F09" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F10Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 20 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 20 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F10Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 20 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 20 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F10Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F10" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F10" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F11Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 12 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 12 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F11Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 12 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 12 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F11Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F11" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F11" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F12Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 4 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 4 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F12Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 4 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 4 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F12Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F12" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F12" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F13Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 23 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 23 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F13Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 23 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 23 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F13Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F13" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F13" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F14Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 9 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 9 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F14Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 9 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 9 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F14Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F14" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F14" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F15Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 25 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 25 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F15Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 25 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 25 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F15Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F15" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F15" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F16Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 22 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 22 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F16Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 22 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 22 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F16Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F16" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F16" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F17Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 24 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 24 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F17Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 24 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 24 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F17Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F17" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F17" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F18Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 0 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 0 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F18Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 0 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 0 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F18Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F18" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F18" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F19Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 26 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 26 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F19Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 26 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 26 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F19Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F19" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F19" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F20Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 27 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 27 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F20Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 27 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 27 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F20Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F20" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F20" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F21Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 28 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 28 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F21Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 28 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 28 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F21Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F21" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F21" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F22Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 29 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 29 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F22Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 29 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 29 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F22Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F22" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F22" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F23Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 30 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 30 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F23Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 30 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 30 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F23Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F23" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F23" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F24Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 31 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 31 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F24Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 31 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 31 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F24Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F24" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F24" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F25Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 33 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 33 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F25Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 33 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 33 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F25Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F25" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F25" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F26Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 32 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 32 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F26Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 32 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 32 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F26Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F26" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F26" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F27Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 34 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 34 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F27Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 34 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 34 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F27Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F27" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F27" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum(),
                F28Minimo = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 36 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 36 && mt.Anio == anio && mt.Semana == semana select mt.Minimo).First(),
                F28Meta = (from mt in db.MetaSemanalTiendas where mt.TiendaID == 36 && mt.Anio == anio && mt.Semana == semana select mt.Meta).Count() == 0 ? 0 : (from mt in db.MetaSemanalTiendas where mt.TiendaID == 36 && mt.Anio == anio && mt.Semana == semana select mt.Meta).First(),
                F28Venta = (from dv in db.DesempenioSemanalVendedors
                            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                            where t.Codigo == "F28" && mt.Semana == semana && mt.Anio == anio
                            select dv.VentaTotal).Count() == 0 ? 0 : (from dv in db.DesempenioSemanalVendedors
                                                                      join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
                                                                      join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
                                                                      join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
                                                                      where t.Codigo == "F28" && mt.Semana == semana && mt.Anio == anio
                                                                      select dv.VentaTotal).Sum()

            };

        return q;
    }
    
    public static IQueryable<VentasVendedor> ListarVentasVendedor(int semana, int anio, string tienda)
    {
        dbFriedmanDataContext db = new dbFriedmanDataContext();

        int idTienda = 0;
        if ((from t in db.Tiendas where t.Codigo == tienda select t.ID).Count() > 0) idTienda = (from t in db.Tiendas where t.Codigo == tienda select t.ID).First();

        var q =
            from dv in db.DesempenioSemanalVendedors
            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
            where mt.TiendaID == idTienda && mt.Semana == semana && mt.Anio == anio
            select new VentasVendedor()
            {
                Vendedor = v.NombreInterno,
                Venta = dv.VentaTotal,
                Dias = dv.DiasTrabajados,
                Articulos = dv.CantidadArticulos,
                Facturas = dv.CantidadFacturas,
                Oportunidades = dv.CantidadOportunidades,
                EnVacaciones = dv.EnVacaciones,
                Minimo = mt.Minimo,
                Meta = mt.Meta,
                TotalDias = (from dvv in db.DesempenioSemanalVendedors where dvv.MetaSemanalTiendaID == dv.MetaSemanalTiendaID select dvv.DiasTrabajados).Sum()
            };

        return q;
    }
    
    public static IQueryable<VentasVendedorSemanas> ListarVentasVendedorSemanas(int semana, int anio, string tienda, int semanas, string vendedor)
    {
        dbFriedmanDataContext db = new dbFriedmanDataContext();
        
        int semana1, semana2, semana3, semana4, anio1, anio2;
        int idTienda = (from t in db.Tiendas where t.Codigo == tienda select t.ID).First();

        if ((semana - semanas) > 0)
        {
            semana1 = semana - semanas;
            semana2 = semana - 1;
            semana3 = 0; //semana;
            semana4 = 0; // semana;
            anio1 = anio;
            anio2 = 1900;
        }
        else
        {
            semana1 = 53 + (semana - semanas);
            semana2 = 53;
            semana3 = 1;
            semana4 = semana - 1;
            anio1 = anio - 1;
            anio2 = anio;
        }
             
        var q =
            (from dv in db.DesempenioSemanalVendedors
             join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
             join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
             join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
             where mt.TiendaID == idTienda && mt.Semana >= semana1 && mt.Semana <= semana2 && mt.Anio == anio1 && v.NombreInterno == vendedor
             select new VentasVendedorSemanas()
             {
                 Semana = mt.Semana.ToString(),
                 Venta = dv.VentaTotal,
                 Dias = dv.DiasTrabajados,
                 Articulos = dv.CantidadArticulos,
                 Facturas = dv.CantidadFacturas,
                 Oportunidades = dv.CantidadOportunidades,
                 EnVacaciones = dv.EnVacaciones,
                 Minimo = mt.Minimo,
                 Meta = mt.Meta,
                 TotalDias = (from dvv in db.DesempenioSemanalVendedors where dvv.MetaSemanalTiendaID == dv.MetaSemanalTiendaID select dvv.DiasTrabajados).Sum(),
                 id = dv.ID,
                 anio = mt.Anio,
                 NumeroSemana = mt.Semana
             }).Union(
            from dv in db.DesempenioSemanalVendedors
            join mt in db.MetaSemanalTiendas on new { dv.MetaSemanalTiendaID } equals new { MetaSemanalTiendaID = mt.ID }
            join t in db.Tiendas on new { mt.TiendaID } equals new { TiendaID = t.ID }
            join v in db.Vendedors on new { dv.VendedorID } equals new { VendedorID = v.ID }
            where mt.TiendaID == idTienda && mt.Semana >= semana3 && mt.Semana <= semana4 && mt.Anio == anio2 && v.NombreInterno == vendedor
            select new VentasVendedorSemanas()
            {
                Semana = mt.Semana.ToString(),
                Venta = dv.VentaTotal,
                Dias = dv.DiasTrabajados,
                Articulos = dv.CantidadArticulos,
                Facturas = dv.CantidadFacturas,
                Oportunidades = dv.CantidadOportunidades,
                EnVacaciones = dv.EnVacaciones,
                Minimo = mt.Minimo,
                Meta = mt.Meta,
                TotalDias = (from dvv in db.DesempenioSemanalVendedors where dvv.MetaSemanalTiendaID == dv.MetaSemanalTiendaID select dvv.DiasTrabajados).Sum(),
                id = dv.ID,
                anio = mt.Anio,
                NumeroSemana = mt.Semana
            }).OrderBy(p => p.anio).ThenBy(p => p.NumeroSemana);
        
        return q;
    }
}

