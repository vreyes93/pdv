﻿using System.Collections.Generic;
using System.Linq;

namespace FiestaNETServices.Services
{
    public static class DataLayerGeneral
    {
        public static IQueryable<ArticulosPrecios> productoPorTienda(IQueryable<ArticulosPrecios> data, string codigoTienda)
        {
            using (dbDataContext db = new dbDataContext())
            {
                string specTienda = especificacionTienda(codigoTienda);
                List<string> filtro = productoTienda(specTienda);

                return data.Where(x => specTienda == "OUTLET" ? filtro.Contains(x.Articulo) : !filtro.Contains(x.Articulo));
            }
        }

        public static IQueryable<AdministraOfertas> productoPorTienda(IQueryable<AdministraOfertas> data, string codigoTienda)
        {
            using (dbDataContext db = new dbDataContext())
            {
                string specTienda = especificacionTienda(codigoTienda);
                List<string> filtro = productoTienda(specTienda);

                return data.Where(x => specTienda == "OUTLET" ? filtro.Contains(x.Articulo) : !filtro.Contains(x.Articulo));
            }
        }

        private static List<string> productoTienda(string specTienda)
        {
            using (dbDataContext db = new dbDataContext())
            {
                if (specTienda != "TODO")
                {
                    List<string> result = new List<string>();

                    switch (specTienda)
                    {
                        case "FIESTA":
                            result = db.ARTICULO_ESPEs.Where(x => x.ATRIBUTO == "SEGMENTO_TIENDA" && x.VALOR == "OUTLET")
                                .Select(x => x.ARTICULO).ToList();
                            break;
                        case "OUTLET":
                            result = db.ARTICULO_ESPEs.Where(x => x.ATRIBUTO == "SEGMENTO_TIENDA" && SegmentoTienda().Contains(x.VALOR))
                                .Select(x => x.ARTICULO).ToList();
                            break;
                    }

                    return result;
                }

                return new List<string>();
            }
        }

        private static string especificacionTienda(string codigoTienda)
        {
            using (dbDataContext db = new dbDataContext())
            {
                MF_CobradorEspecificaciones esp = db.MF_CobradorEspecificaciones.Where(x => x.COBRADOR == codigoTienda && x.ATRIBUTO == "VISIBILIDAD").FirstOrDefault();
                return esp == null ? "TODO" : esp.VALOR;
            }
        }

        private static List<string> SegmentoTienda()
        {
            return new List<string>
            {
                "OUTLET",
                "FIESTA_OUTLET"
            };
        }
    }
}