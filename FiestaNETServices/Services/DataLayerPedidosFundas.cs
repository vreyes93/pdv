﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq.SqlClient;
using System.Web;
using System.Transactions;
using FiestaNETServices.Services;

/// <summary>
/// Summary description for DataLayerPedidosFundas
/// </summary>
public static class DataLayerPedidosFundas
{

    public static IQueryable<ArticulosRelacion> ListarArticulosRelacion(string articulo, ref string NombreArticulo, ref string DescripcionRelacion)
    {
        dbDataContext db = new dbDataContext();

        var q =
            from a in db.MF_ArticulosRelacions
            join b in db.ARTICULOs on new { ArticuloRelacion = a.ArticuloRelacion } equals new { ArticuloRelacion = b.ARTICULO1 }
            where a.Articulo == articulo
            select new ArticulosRelacion()
            {
                Articulo = a.ArticuloRelacion,
                Descripcion = b.DESCRIPCION
            };

        NombreArticulo =
            (from a in db.ARTICULOs
             where a.ARTICULO1 == articulo
             select a).Single().DESCRIPCION;

        if ((from a in db.MF_ArticulosRelacions
             join b in db.ARTICULOs on new { Articulo = a.Articulo } equals new { Articulo = b.ARTICULO1 }
             where a.Articulo == articulo
             select a).Count() > 0)
        {
            DescripcionRelacion =
                (from a in db.MF_ArticulosRelacions
                 where a.Articulo == articulo
                 select a).First().Descripcion;
        }

        return q;
    }

    public static IQueryable<EstructuraRelacion> ListarEstructuraRelacion()
    {
        dbDataContext db = new dbDataContext();

        var q =
            from a in db.ARTICULOs
            where a.ARTICULO1.StartsWith("140") && a.ACTIVO == "S"
            orderby a.ARTICULO1
            select new EstructuraRelacion()
            {
                Articulo = a.ARTICULO1,
                Descripcion = a.DESCRIPCION,
                TieneBases = (from b in db.MF_ArticulosRelacions where b.Articulo == a.ARTICULO1 && (b.ArticuloRelacion.StartsWith("142") || b.ArticuloRelacion.StartsWith("M142")) select b).Count() == 0 ? "No" : "Sí",
                TieneColchones = (from c in db.MF_ArticulosRelacions where c.Articulo == a.ARTICULO1 && (c.ArticuloRelacion.StartsWith("141") || c.ArticuloRelacion.StartsWith("M142")) select c).Count() == 0 ? "No" : "Sí"
            };

        return q;
    }

    public static IQueryable<PedidoFundas> SugeridoPedido(string bodega)
    {
        string mBodegas = "";
        dbDataContext db = new dbDataContext();

        if (bodega == "MF") mBodegas = "F%"; else mBodegas = bodega;
        if (bodega == "SI") mBodegas = "S%";

        var q =
            from a in db.ARTICULOs
            where a.ARTICULO1.StartsWith("140") && a.ACTIVO == "S"
            orderby a.ARTICULO1
            select new PedidoFundas()
            {
                Articulo = a.ARTICULO1,
                Descripcion = a.DESCRIPCION.Replace("FUNDA ", "").Replace("DE LUJO", ""),
                Colchon = (int)(from b in db.EXISTENCIA_BODEGAs
                                where SqlMethods.Like(b.BODEGA, mBodegas) && (from c in db.MF_ArticulosRelacions where c.Articulo == a.ARTICULO1 && c.ArticuloRelacion.Substring(0, 3) != "142" && c.ArticuloRelacion.Substring(0, 4) != "M142" select c.ArticuloRelacion).Contains(b.ARTICULO)
                                select b.CANT_DISPONIBLE).Sum() == null ? 0 : (int)(from b in db.EXISTENCIA_BODEGAs
                                                                                    where SqlMethods.Like(b.BODEGA, mBodegas) && (from c in db.MF_ArticulosRelacions
                                                                                                                                  where c.Articulo == a.ARTICULO1 && c.ArticuloRelacion.Substring(0, 3) != "142" && c.ArticuloRelacion.Substring(0, 4) != "M142"
                                                                                                                                  select c.ArticuloRelacion).Contains(b.ARTICULO)
                                                                                    select b.CANT_DISPONIBLE).Sum(),
                Base = (int)(from b in db.EXISTENCIA_BODEGAs
                             where SqlMethods.Like(b.BODEGA, mBodegas) && (from c in db.MF_ArticulosRelacions where c.Articulo == a.ARTICULO1 && (c.ArticuloRelacion.Substring(0, 3) == "142" || c.ArticuloRelacion.Substring(0, 4) == "M142") select c.ArticuloRelacion).Contains(b.ARTICULO)
                             select b.CANT_DISPONIBLE).Sum() == null ? 0 : (int)(from b in db.EXISTENCIA_BODEGAs
                                                                                 where SqlMethods.Like(b.BODEGA, mBodegas) && (from c in db.MF_ArticulosRelacions
                                                                                                                               where c.Articulo == a.ARTICULO1 && (c.ArticuloRelacion.Substring(0, 3) == "142" || c.ArticuloRelacion.Substring(0, 4) == "M142")
                                                                                                                               select c.ArticuloRelacion).Contains(b.ARTICULO)
                                                                                 select b.CANT_DISPONIBLE).Sum(),
                Funda = (int)(from b in db.EXISTENCIA_BODEGAs where SqlMethods.Like(b.BODEGA, mBodegas) && b.ARTICULO == a.ARTICULO1 select b.CANT_DISPONIBLE).Sum(),
                PedidoColchon = 0,
                PedidoFunda = 0,
                CodigoArticulo = a.ARTICULO1
            };

        return q;
    }

    public static IQueryable<PedidoFundas> ListarPedido(int pedido)
    {
        dbDataContext db = new dbDataContext();

        var q =
            from a in db.MF_PedidoCamasDets
            join b in db.ARTICULOs on new { Articulo = a.Articulo } equals new { Articulo = b.ARTICULO1 }
            where a.Pedido == pedido
            orderby a.PedidoDet
            select new PedidoFundas()
            {
                Articulo = a.Articulo,
                Descripcion = b.DESCRIPCION,
                Colchon = a.ExistenciasColchon,
                Base = a.ExistenciasBases,
                Funda = a.ExistenciasFundas,
                PedidoColchon = a.PedidoColchon,
                PedidoFunda = a.PedidoFundas,
                CodigoArticulo = a.Articulo
            };

        return q;
    }

    public static List<ExistenciasArticuloRelacion> ListarExistenciasArticuloRelacionBases(string articulo, string bodega)
    {
        string mBodegas = "";
        dbDataContext db = new dbDataContext();

        if (bodega == "MF") mBodegas = "F%"; else mBodegas = bodega;
        if (bodega == "SI") mBodegas = "S%";

        List<ExistenciasArticuloRelacion> q = new List<ExistenciasArticuloRelacion>();

        var qq =
        from a in db.BODEGAs
        join bb in db.EXISTENCIA_BODEGAs on new { Bodega = a.BODEGA1 } equals new { Bodega = bb.BODEGA }
        join cc in db.MF_ArticulosRelacions on new { Articulo = bb.ARTICULO } equals new { Articulo = cc.ArticuloRelacion }
        join dd in db.ARTICULOs on new { Articulo = cc.ArticuloRelacion } equals new { Articulo = dd.ARTICULO1 }
        where SqlMethods.Like(a.BODEGA1, mBodegas) && cc.Articulo == articulo && (cc.ArticuloRelacion.Substring(0, 3) == "142" || cc.ArticuloRelacion.Substring(0, 4) == "M142")
        select new ExistenciasArticuloRelacion()
        {
            Bodega = a.BODEGA1.Substring(0, 1) == "F" ? a.BODEGA1 : a.NOMBRE,
            Articulo = cc.ArticuloRelacion,
            Descripcion = dd.DESCRIPCION.Replace("COLCHON ", ""),
            Existencias = Convert.ToInt32(bb.CANT_DISPONIBLE),
            Funda = "",
            CodigoBodega = a.BODEGA1
        };
        foreach (var item in qq)
        {
            string mFunda = "";
            if ((from a in db.MF_ArticulosRelacions where a.ArticuloRelacion == item.Articulo && a.Articulo != articulo select a).Count() > 0)
            {
                var qFundas =
                    (from a in db.MF_ArticulosRelacions
                     where a.ArticuloRelacion == item.Articulo && a.Articulo != articulo
                     select new ArticulosRelacion()
                     {
                         Articulo = a.Articulo,
                         Descripcion = ""
                     }).Distinct();

                int ii = 0;
                foreach (var itemFunda in qFundas)
                {
                    if ((from a in db.EXISTENCIA_BODEGAs where a.BODEGA == item.CodigoBodega && a.ARTICULO == itemFunda.Articulo select a).Count() > 0)
                    {
                        ii++;
                        int mExistencias = Convert.ToInt32((from a in db.EXISTENCIA_BODEGAs where a.BODEGA == item.CodigoBodega && a.ARTICULO == itemFunda.Articulo select a).Single().CANT_DISPONIBLE);
                        if (ii == 1) mFunda = string.Format("{0} | {1}", itemFunda.Articulo, mExistencias); else mFunda = string.Format("{0}{1}{2} | {3}", mFunda, Environment.NewLine, itemFunda.Articulo, mExistencias);
                    }

                    string mBase = itemFunda.Articulo.Replace("140", "142");
                    if ((from a in db.EXISTENCIA_BODEGAs where a.BODEGA == item.CodigoBodega && a.ARTICULO == mBase select a).Count() > 0)
                    {
                        ii++;
                        int mExistenciasBase = Convert.ToInt32((from a in db.EXISTENCIA_BODEGAs where a.BODEGA == item.CodigoBodega && a.ARTICULO == mBase select a).Single().CANT_DISPONIBLE);
                        if (ii == 1) mFunda = string.Format("{0} | {1}", mBase, mExistenciasBase); else mFunda = string.Format("{0}{1}{2} | {3}", mFunda, Environment.NewLine, mBase, mExistenciasBase);
                    }
                }
            }

            ExistenciasArticuloRelacion itemR = new ExistenciasArticuloRelacion();
            itemR.Bodega = item.Bodega;
            itemR.Articulo = item.Articulo;
            itemR.Descripcion = item.Descripcion;
            itemR.Existencias = item.Existencias;
            itemR.Funda = mFunda;
            itemR.CodigoBodega = item.CodigoBodega;
            q.Add(itemR);
        }

        return q;
    }

    public static List<ExistenciasArticuloRelacion> ListarExistenciasArticuloRelacion(string articulo, string bodega)
    {
        string mBodegas = "";
        dbDataContext db = new dbDataContext();

        if (bodega == "MF") mBodegas = "F%"; else mBodegas = bodega;
        if (bodega == "SI") mBodegas = "S%";

        List<ExistenciasArticuloRelacion> q = new List<ExistenciasArticuloRelacion>();

        var qq =
        from a in db.BODEGAs
        join bb in db.EXISTENCIA_BODEGAs on new { Bodega = a.BODEGA1 } equals new { Bodega = bb.BODEGA }
        join cc in db.MF_ArticulosRelacions on new { Articulo = bb.ARTICULO } equals new { Articulo = cc.ArticuloRelacion }
        join dd in db.ARTICULOs on new { Articulo = cc.ArticuloRelacion } equals new { Articulo = dd.ARTICULO1 }
        where SqlMethods.Like(a.BODEGA1, mBodegas) && cc.Articulo == articulo && cc.ArticuloRelacion.Substring(0, 3) != "142" && cc.ArticuloRelacion.Substring(0, 4) != "M142"
        select new ExistenciasArticuloRelacion()
        {
            Bodega = a.BODEGA1.Substring(0, 1) == "F" ? a.BODEGA1 : a.NOMBRE,
            Articulo = cc.ArticuloRelacion,
            Descripcion = dd.DESCRIPCION.Replace("COLCHON ", ""),
            Existencias = Convert.ToInt32(bb.CANT_DISPONIBLE),
            Funda = "",
            CodigoBodega = a.BODEGA1
        };
        foreach (var item in qq)
        {
            string mFunda = "";
            if ((from a in db.MF_ArticulosRelacions where a.ArticuloRelacion == item.Articulo && a.Articulo != articulo select a).Count() > 0)
            {
                var qFundas =
                    (from a in db.MF_ArticulosRelacions
                     where a.ArticuloRelacion == item.Articulo && a.Articulo != articulo
                     select new ArticulosRelacion()
                     {
                         Articulo = a.Articulo,
                         Descripcion = ""
                     }).Distinct();

                int ii = 0;
                foreach (var itemFunda in qFundas)
                {
                    if ((from a in db.EXISTENCIA_BODEGAs where a.BODEGA == item.CodigoBodega && a.ARTICULO == itemFunda.Articulo select a).Count() > 0)
                    {
                        ii++;
                        int mExistencias = Convert.ToInt32((from a in db.EXISTENCIA_BODEGAs where a.BODEGA == item.CodigoBodega && a.ARTICULO == itemFunda.Articulo select a).Single().CANT_DISPONIBLE);
                        if (ii == 1) mFunda = string.Format("{0} | {1}", itemFunda.Articulo, mExistencias); else mFunda = string.Format("{0}{1}{2} | {3}", mFunda, Environment.NewLine, itemFunda.Articulo, mExistencias);
                    }

                    string mBase = itemFunda.Articulo.Replace("140", "142");
                    if ((from a in db.EXISTENCIA_BODEGAs where a.BODEGA == item.CodigoBodega && a.ARTICULO == mBase select a).Count() > 0)
                    {
                        ii++;
                        int mExistenciasBase = Convert.ToInt32((from a in db.EXISTENCIA_BODEGAs where a.BODEGA == item.CodigoBodega && a.ARTICULO == mBase select a).Single().CANT_DISPONIBLE);
                        if (ii == 1) mFunda = string.Format("{0} | {1}", mBase, mExistenciasBase); else mFunda = string.Format("{0}{1}{2} | {3}", mFunda, Environment.NewLine, mBase, mExistenciasBase);
                    }
                }
            }

            ExistenciasArticuloRelacion itemR = new ExistenciasArticuloRelacion();
            itemR.Bodega = item.Bodega;
            itemR.Articulo = item.Articulo;
            itemR.Descripcion = item.Descripcion;
            itemR.Existencias = item.Existencias;
            itemR.Funda = mFunda;
            itemR.CodigoBodega = item.CodigoBodega;
            q.Add(itemR);
        }

        return q;
    }

    public static int ExistenciasArticuloRelacion(string articulo, string bodega)
    {
        string mBodegas = "";
        dbDataContext db = new dbDataContext();

        if (bodega == "MF") mBodegas = "F%"; else mBodegas = bodega;
        if (bodega == "SI") mBodegas = "S%";

        int mExistenciasArticuloRelacion = 0; int ii = 0; string mBodegaAnterior = "";

        if (articulo == "140099-083")
        {
            int jj = 0;
            jj += 1;
        }

        var qq =
        from a in db.BODEGAs
        join bb in db.EXISTENCIA_BODEGAs on new { Bodega = a.BODEGA1 } equals new { Bodega = bb.BODEGA }
        join cc in db.MF_ArticulosRelacions on new { Articulo = bb.ARTICULO } equals new { Articulo = cc.ArticuloRelacion }
        join dd in db.ARTICULOs on new { Articulo = cc.ArticuloRelacion } equals new { Articulo = dd.ARTICULO1 }
        where SqlMethods.Like(a.BODEGA1, mBodegas) && cc.Articulo == articulo && cc.ArticuloRelacion.Substring(0, 3) != "142" && cc.ArticuloRelacion.Substring(0, 4) != "M142"
        orderby a.BODEGA1
        select new ExistenciasArticuloRelacion()
        {
            Bodega = a.BODEGA1.Substring(0, 1) == "F" ? a.BODEGA1 : a.NOMBRE,
            Articulo = cc.ArticuloRelacion,
            Descripcion = dd.DESCRIPCION.Replace("COLCHON ", ""),
            Existencias = Convert.ToInt32(bb.CANT_DISPONIBLE),
            Funda = "",
            CodigoBodega = a.BODEGA1
        };
        foreach (var item in qq)
        {
            if (item.CodigoBodega != mBodegaAnterior) ii = 0;

            ii++;
            if ((from a in db.MF_ArticulosRelacions where a.ArticuloRelacion == item.Articulo && a.Articulo != articulo select a).Count() > 0)
            {
                var qFundas =
                    (from a in db.MF_ArticulosRelacions
                     where a.ArticuloRelacion == item.Articulo && a.Articulo != articulo
                     select new ArticulosRelacion()
                     {
                         Articulo = a.Articulo,
                         Descripcion = ""
                     }).Distinct();

                foreach (var itemFunda in qFundas)
                {
                    if ((from a in db.EXISTENCIA_BODEGAs where a.BODEGA == item.CodigoBodega && a.ARTICULO == itemFunda.Articulo select a).Count() > 0)
                    {
                        int mExistencias = Convert.ToInt32((from a in db.EXISTENCIA_BODEGAs where a.BODEGA == item.CodigoBodega && a.ARTICULO == itemFunda.Articulo select a).Single().CANT_DISPONIBLE);
                        if (ii == 1)
                        {
                            mExistenciasArticuloRelacion = mExistenciasArticuloRelacion + mExistencias;
                            //if (item.Descripcion.ToUpper().Contains("KING")) mExistenciasArticuloRelacion = mExistenciasArticuloRelacion * 2;
                        }
                    }

                    string mBase = itemFunda.Articulo.Replace("140", "142");
                    if ((from a in db.EXISTENCIA_BODEGAs where a.BODEGA == item.CodigoBodega && a.ARTICULO == mBase select a).Count() > 0)
                    {
                        int mExistenciasBase = Convert.ToInt32((from a in db.EXISTENCIA_BODEGAs where a.BODEGA == item.CodigoBodega && a.ARTICULO == mBase select a).Single().CANT_DISPONIBLE);
                        if (ii == 1) mExistenciasArticuloRelacion = mExistenciasArticuloRelacion + mExistenciasBase;
                    }
                }
            }

            mBodegaAnterior = item.CodigoBodega;
        }

        return mExistenciasArticuloRelacion;
    }

    public static IQueryable<ExistenciasArticulo> ListarExistenciasArticulo(string articulo, string bodega)
    {
        string mBodegas = "";
        dbDataContext db = new dbDataContext();

        if (bodega == "MF") mBodegas = "F%"; else mBodegas = bodega;
        if (bodega == "SI") mBodegas = "S%";

        var q =
            from a in db.BODEGAs
            where SqlMethods.Like(a.BODEGA1, mBodegas)
            orderby a.BODEGA1
            select new ExistenciasArticulo()
            {
                Bodega = a.BODEGA1.Substring(0, 1) == "F" ? a.BODEGA1 : a.NOMBRE,
                Colchones = (int)(from b in db.EXISTENCIA_BODEGAs
                                  where SqlMethods.Like(b.BODEGA, a.BODEGA1) && (from c in db.MF_ArticulosRelacions where c.Articulo == articulo && c.ArticuloRelacion.Substring(0, 3) != "142" && c.ArticuloRelacion.Substring(0, 4) != "M142" select c.ArticuloRelacion).Contains(b.ARTICULO)
                                  select b.CANT_DISPONIBLE).Sum() == null ? 0 : (int)(from b in db.EXISTENCIA_BODEGAs
                                                                                      where SqlMethods.Like(b.BODEGA, a.BODEGA1) && (from c in db.MF_ArticulosRelacions
                                                                                                                                     where c.Articulo == articulo && c.ArticuloRelacion.Substring(0, 3) != "142" && c.ArticuloRelacion.Substring(0, 4) != "M142"
                                                                                                                                     select c.ArticuloRelacion).Contains(b.ARTICULO)
                                                                                      select b.CANT_DISPONIBLE).Sum(),
                Bases = (int)(from b in db.EXISTENCIA_BODEGAs
                              where SqlMethods.Like(b.BODEGA, a.BODEGA1) && (from c in db.MF_ArticulosRelacions where c.Articulo == articulo && (c.ArticuloRelacion.Substring(0, 3) == "142" || c.ArticuloRelacion.Substring(0, 4) == "M142") select c.ArticuloRelacion).Contains(b.ARTICULO)
                              select b.CANT_DISPONIBLE).Sum() == null ? 0 : (int)(from b in db.EXISTENCIA_BODEGAs
                                                                                  where SqlMethods.Like(b.BODEGA, a.BODEGA1) && (from c in db.MF_ArticulosRelacions
                                                                                                                                 where c.Articulo == articulo && (c.ArticuloRelacion.Substring(0, 3) == "142" || c.ArticuloRelacion.Substring(0, 4) == "M142")
                                                                                                                                 select c.ArticuloRelacion).Contains(b.ARTICULO)
                                                                                  select b.CANT_DISPONIBLE).Sum(),
                Fundas = (int)(from b in db.EXISTENCIA_BODEGAs where SqlMethods.Like(b.BODEGA, a.BODEGA1) && b.ARTICULO == articulo select b.CANT_DISPONIBLE).Sum(),
                ToolTipColchones = "",
                ToolTipBases = "",
                ToolTipFundas = "",
            };

        return q;
    }

    public static bool GrabarSugeridoPedido(int pedido, string descripcion, List<PedidoFundas> q, ref string mensaje, string bodega)
    {
        dbDataContext db = new dbDataContext();
        bool mRetorna = true;

        try
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                if (pedido == 0)
                {
                    pedido = (from p in db.MF_PedidoCamas select p.Pedido).Count() == 0 ? 1 : (from p in db.MF_PedidoCamas select p.Pedido).Max() + 1;
                    MF_PedidoCama iPedido = new MF_PedidoCama
                    {
                        Pedido = pedido,
                        Descripcion = descripcion,
                        Fecha = DateTime.Now.Date,
                        Bodega = bodega,
                        Estatus = Convert.ToChar("P"),
                        RecordDate = DateTime.Now,
                        CreatedBy = System.Environment.UserName
                    };
                    db.MF_PedidoCamas.InsertOnSubmit(iPedido);
                    db.SubmitChanges();
                }
                else
                {
                    var qPedido =
                        from p in db.MF_PedidoCamas where p.Pedido == pedido select p;
                    foreach (var item in qPedido)
                    {
                        item.Descripcion = descripcion;
                    }
                    db.SubmitChanges();
                }

                var qPedidoBorrar =
                    from p in db.MF_PedidoCamasDets where p.Pedido == pedido select p;
                foreach (var del in qPedidoBorrar)
                {
                    db.MF_PedidoCamasDets.DeleteOnSubmit(del);
                }
                db.SubmitChanges();

                int mPedidoDet = 0;
                foreach (PedidoFundas itemPedido in q)
                {
                    mPedidoDet++;

                    MF_PedidoCamasDet iPedidoDet = new MF_PedidoCamasDet
                    {
                        Pedido = pedido,
                        PedidoDet = mPedidoDet,
                        Articulo = itemPedido.Articulo,
                        ExistenciasColchon = (int)itemPedido.Colchon,
                        ExistenciasBases = (int)itemPedido.Base,
                        ExistenciasFundas = (int)itemPedido.Funda,
                        PedidoColchon = (int)itemPedido.PedidoColchon,
                        PedidoFundas = (int)itemPedido.PedidoFunda
                    };

                    db.MF_PedidoCamasDets.InsertOnSubmit(iPedidoDet);
                    db.SubmitChanges();
                }

                transactionScope.Complete();
                mensaje = "El Pedido fue grabado exitosamente.";
            }
        }
        catch (Exception ex)
        {
            mRetorna = false;
            mensaje = string.Format("Error al grabar el pedido. {0}", ex.Message);
        }

        return mRetorna;
    }

    public static IQueryable<Bodegas> ListarBodegas()
    {
        dbDataContext db = new dbDataContext();

        var q =
            from a in db.BODEGAs
            orderby a.BODEGA1
            select new Bodegas()
            {
                Bodega = a.BODEGA1,
                Nombre = a.BODEGA1.Substring(0, 1) == "F" ? a.BODEGA1 : a.NOMBRE
            };

        return q;
    }

    public static IQueryable<Pedidos> ListarPedidos()
    {
        dbDataContext db = new dbDataContext();

        var q =
            from a in db.MF_PedidoCamas
            select new Pedidos()
            {
                Pedido = a.Pedido,
                Fecha = string.Format("{0}/{1}/{2}", a.Fecha.Day < 10 ? string.Format("0{0}", a.Fecha.Day) : a.Fecha.Day.ToString(), a.Fecha.Month < 10 ? string.Format("0{0}", a.Fecha.Month) : a.Fecha.Month.ToString(), a.Fecha.Year),
                Bodega = a.Bodega == "%" ? "Todas las Bodegas" : a.Bodega == "MF" ? "Bodegas MF" : a.Bodega == "S" ? "Bodegas SIMAN y SEARS" : a.Bodega,
                Estatus = a.Estatus == Convert.ToChar("P") ? "Pendiente" : a.Estatus == Convert.ToChar("A") ? "Anulado" : "Autorizado",
                Descripcion = a.Descripcion,
                FechaCreacion = a.RecordDate,
                UsuarioCreo = a.CreatedBy
            };

        q = q.OrderByDescending(x => x.FechaCreacion);

        return q;
    }

    public static bool EliminarPedido(int pedido, ref string mensaje)
    {
        bool mRetorna = true;
        dbDataContext db = new dbDataContext();

        try
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                var qPedidoDet =
                    from a in db.MF_PedidoCamasDets
                    where a.Pedido == pedido
                    select a;
                foreach (var del in qPedidoDet)
                {
                    db.MF_PedidoCamasDets.DeleteOnSubmit(del);
                }
                db.SubmitChanges();

                var qPedido =
                    from a in db.MF_PedidoCamas
                    where a.Pedido == pedido
                    select a;
                foreach (var del in qPedido)
                {
                    db.MF_PedidoCamas.DeleteOnSubmit(del);
                }
                db.SubmitChanges();

                transactionScope.Complete();
                mensaje = string.Format("El pedido {0} fue eliminado exitosamente.", pedido);
            }
        }
        catch (Exception ex)
        {
            mensaje = string.Format("Se produjo el siguiente error al grabar el pedido. {0}", ex.Message);
        }

        return mRetorna;
    }

}
