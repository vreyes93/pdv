﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Linq.SqlClient;
using System.Transactions;
using FiestaNETServices.Services;
using System.Net.Mail;
using System.Net;
using System.IO;
using System.Net.Mime;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using System.Web.Configuration;
using MF_Clases;
using Newtonsoft.Json;
using FiestaNETServices.Util;
using MF_Clases.Utils;
using System.Net.Http;

public static class DataLayerPuntoVentaMayoreo
{
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public static bool LoginExitoso(string usuario, string password, ref string nombre, ref string cliente, ref string NombreCliente, ref string mensaje, ref string cambiaPassword, ref bool usuarioPM)
    {
        bool mRetorna = true;
        dbDataContext db = new dbDataContext();

        try
        {
            string mAmbiente = "DES";

            if (db.Connection.ConnectionString.Contains("sql.fiesta.local")) mAmbiente = "PRO";
            if (db.Connection.ConnectionString.Contains("PRUEBAS")) mAmbiente = "PRU";

            string mServidor = "(local)";
            string mBase = "EXACTUSERP";

            if (mAmbiente == "PRO")
            {
                mBase = "EXACTUSERP";
                mServidor = "sql.fiesta.local";
            }

            if (mAmbiente == "PRU")
            {
                mBase = "PRUEBAS";
                mServidor = "sql.fiesta.local";
            }


            bool mLoginExitoso = false;
            string mStringConexion = string.Format("Data Source={0}; Initial Catalog = {1};User id = {2};password = {3}; Max Pool Size=600;Pooling=false;Application Name=PDV", mServidor, mBase, usuario, password);

            using (SqlConnection conexion = new SqlConnection(mStringConexion))
            {
                try
                {
                    conexion.Open();
                    conexion.Close();

                    mLoginExitoso = true;
                }
                catch
                {
                    if (conexion.State == System.Data.ConnectionState.Open) conexion.Close();
                }
                finally
                {
                    if (conexion.State == System.Data.ConnectionState.Open) conexion.Close();
                }
            }

            if (mLoginExitoso)
            {
                if ((from u in db.MF_UsuarioMayoreos where u.USUARIO == usuario && u.ACTIVO == Convert.ToChar("S") select u).Count() == 0)
                {
                    mRetorna = false;
                    mensaje = "El usuario no está activo en el sistema.";
                }
                else
                {
                    var q = from u in db.MF_UsuarioMayoreos join c in db.CLIENTEs on u.CLIENTE equals c.CLIENTE1 where u.USUARIO == usuario && u.ACTIVO == Convert.ToChar("S") select new { Nombre = u.NOMBRE, Cliente = u.CLIENTE, NombreCli = c.NOMBRE, CambiaPassword = u.CAMBIA_PASSWORD, UsuarioPM = u.USUARIO_PM };
                    foreach (var item in q)
                    {
                        nombre = item.Nombre.Trim();
                        cliente = item.Cliente;
                        NombreCliente = item.NombreCli.Trim();
                        cambiaPassword = item.CambiaPassword.ToString();
                        usuarioPM = item.UsuarioPM == Convert.ToChar("S") ? true : false;
                    }
                }
            }
            else
            {
                mRetorna = false;
                mensaje = "Usuario o contraseña inválidos";
            }
        }
        catch
        {
            mRetorna = false;
            mensaje = "Usuario o contraseña inválidos.";
        }

        return mRetorna;
    }

    public static decimal iva()
    {
        dbDataContext db = new dbDataContext();
        return 1 + ((from im in db.IMPUESTOs where im.IMPUESTO1 == "IVA" select im).First().IMPUESTO11 / 100);
    }

    public static IQueryable<ArticulosMayoreo> DevuelveArticulosMayoreo(string cliente)
    {
        decimal mIVA = iva();
        dbDataContext db = new dbDataContext();

        var q = from a in db.MF_ArticulosMayoreos
                join aa in db.ARTICULOs on a.ARTICULO equals aa.ARTICULO1
                where a.ACTIVO == Convert.ToChar("S") && a.CLIENTE == cliente
                orderby a.DESCRIPCION
                select new ArticulosMayoreo()
                {
                    Articulo = a.ARTICULO,
                    Descripcion = a.DESCRIPCION,
                    PrecioBase = (double)a.PRECIO_BASE_LOCAL - ((double)a.PRECIO_BASE_LOCAL * ((double)a.PORCENTAJE_DESCUENTO / 100)),
                    PrecioLista = Math.Round(((a.PRECIO_BASE_LOCAL - (a.PRECIO_BASE_LOCAL * Convert.ToDecimal((a.PORCENTAJE_DESCUENTO / 100))))) * mIVA, 2, MidpointRounding.AwayFromZero),
                    Cantidad = 0,
                    TieneRegalo = a.TIENE_REGALO.ToString(),
                    Regalo = a.REGALO,
                    CantidadRegalo = a.CANTIDAD_REGALO,
                    Tipo = aa.TIPO

                };

        return q;
    }

    public static bool GrabarPedido(ref string Pedido, DataSet dsInfo, ref string mensaje, string usuario, string cliente, string observaciones, string ordenCompra)
    {
        dbDataContext db = new dbDataContext();

        decimal mIVA = iva(); string mBodega = "F01"; string mOrdenCompra = ordenCompra; bool mNuevo = true;
        decimal mTotalMercaderia = 0; decimal mTotalFacturar = 0; decimal mImpuesto = 0; decimal mUnidades = 0;
        bool mRetorna = true; string mPedido = ""; string mNombreCliente = ""; string mDireccion = ""; string mNombreUsuario = ""; string mMailUsuario = ""; string mMensaje2 = "";

        StringBuilder mBody = new StringBuilder();
        DataTable dtPedidoOriginal = new DataTable("PedidoOriginal");
        DataTable dtUsuariosEnviar = new DataTable("UsuariosEnviar");
        DataTable dtUsuariosEnviarBodega = new DataTable("UsuariosEnviarBodega");

        dtUsuariosEnviar.Columns.Add(new DataColumn("email", typeof(System.String)));
        dtUsuariosEnviarBodega.Columns.Add(new DataColumn("email", typeof(System.String)));

        string mCodigoValidar = Pedido;
        List<DtoLoteBases> lstBases = new List<DtoLoteBases>();
        List<DtoLotePatas> lstPatas = new List<DtoLotePatas>();
        DataTable dtPatasxBase = new DataTable();
        dtPatasxBase.Columns.Add(new DataColumn("Base", typeof(System.String)));
        dtPatasxBase.Columns.Add(new DataColumn("TienePatas", typeof(System.Boolean)));
        String articuloPadre = String.Empty;

        try
        {
           

            for (int ii = 0; ii < dsInfo.Tables["PedidoLinea"].Columns.Count; ii++)
            {
                dtPedidoOriginal.Columns.Add(new DataColumn(dsInfo.Tables["PedidoLinea"].Columns[ii].ColumnName, dsInfo.Tables["PedidoLinea"].Columns[ii].DataType));
            }

            if ((from c in db.MF_Clientes where c.CLIENTE == cliente select c).First().USAR_DIRECC_EN_FACTURA == Convert.ToChar("S"))
            {
                mensaje = "Este cliente no tiene dirección de entrega definida, arregle la dirección del cliente en el Punto de Venta y luego intente grabar el pedido nuevamente.";
                return false;
            }

            var cuantosPedidos = from p in db.PEDIDOs where p.PEDIDO1 == mCodigoValidar select p;
            if (cuantosPedidos.Count() > 0)
            {
                if (cuantosPedidos.First().CLIENTE != cliente)
                {
                    mensaje = string.Format("El pedido {0} no pertenece a este cliente.", mCodigoValidar);
                    return false;
                }
            }

            using (TransactionScope transactionScope = new TransactionScope())
            {
                List<Patas> patasSets = new List<Patas>();
                //***--MAYO 2018 --
                //***Se le asigna el vendedor seleccionado al grabar el pedido para el cálculo de comisiones
                //***Para CEMACO unicamente, para los demás, queda el código de vendedor por defecto.
                string mVendedor = dsInfo.Tables["Pedido"].Rows[0]["VendedorComision"].ToString();
                if (mVendedor.Equals(""))
                    mVendedor = (from c in db.MF_Configura select c).First().VENDEDOR_MAYOREO;
                
                string mDireccionEmbarque = (from d in db.DIRECC_EMBARQUEs where d.CLIENTE == cliente && d.DIRECCION == "ND" select d).First().DESCRIPCION;

                mMailUsuario = (from u in db.MF_UsuarioMayoreos where u.USUARIO == usuario select u).First().E_MAIL;
                mNombreUsuario = (from u in db.MF_UsuarioMayoreos where u.USUARIO == usuario select u).First().NOMBRE;

                var fUsuariosEnviar = db.MF_Gerentes.Where(x => x.ENVIAR_PEDIDO_MAYOREO.Equals("S"));
                var qUsuariosEnviar = fUsuariosEnviar.Where(x => x.CLIENTE_MAYOREO_EXCEPCION.Equals(null) || x.CLIENTE_MAYOREO_EXCEPCION.Equals(string.Empty));
                try
                {
                    var lstExcepciones1 = fUsuariosEnviar.Where(x => x.CLIENTE_MAYOREO_EXCEPCION.Equals(null)==false || x.CLIENTE_MAYOREO_EXCEPCION.Equals(string.Empty) == false);
                   
                    foreach (var item in lstExcepciones1)
                    {
                        List<string> excep = item.CLIENTE_MAYOREO_EXCEPCION.ToString().Split('|').ToList();
                        if (!excep.Contains(cliente))
                        {
                            DataRow RowMail = dtUsuariosEnviar.NewRow();
                            RowMail["email"] = item.EMAIL.Replace("@mueblesfiesta.com", "@productosmultiples.com");
                            dtUsuariosEnviar.Rows.Add(RowMail);
                        }
                    }
                }
                catch
                { }
                //var qUsuariosEnviar = from u in db.MF_Gerentes where u.ENVIAR_PEDIDO_MAYOREO == Convert.ToChar("S") select u;
                foreach (var item in qUsuariosEnviar)
                {
                    DataRow RowMail = dtUsuariosEnviar.NewRow();
                    RowMail["email"] = item.EMAIL.Replace("@mueblesfiesta.com", "@productosmultiples.com");
                    dtUsuariosEnviar.Rows.Add(RowMail);
                }

                //var qUsuariosEnviarBodega = from u in db.MF_Gerentes where u.ENVIAR_PEDIDO_MAYOREO_BODEGA == Convert.ToChar("S") select u;
                var fUsuariosEnviarBodega = db.MF_Gerentes.Where(x => x.ENVIAR_PEDIDO_MAYOREO_BODEGA.Equals("S"));
                var qUsuariosEnviarBodega = fUsuariosEnviarBodega.Where(x =>  x.CLIENTE_MAYOREO_EXCEPCION.Equals(null) || x.CLIENTE_MAYOREO_EXCEPCION.Equals(string.Empty));
                var lstExcepciones = fUsuariosEnviarBodega.Where(x => x.CLIENTE_MAYOREO_EXCEPCION.Equals(null) == false || x.CLIENTE_MAYOREO_EXCEPCION.Equals(string.Empty) == false);
                try
                {
                    foreach (var item in lstExcepciones)
                    {
                        List<string> excep = item.CLIENTE_MAYOREO_EXCEPCION.ToString().Split('|').ToList();
                        if (!excep.Contains(cliente))
                        {
                            DataRow RowMail = dtUsuariosEnviarBodega.NewRow();
                            RowMail["email"] = item.EMAIL.Replace("@mueblesfiesta.com", "@productosmultiples.com");
                            dtUsuariosEnviarBodega.Rows.Add(RowMail);
                        }
                    }
                    
                }
                catch
                { }
                foreach (var item in qUsuariosEnviarBodega)
                {
                    DataRow RowMail = dtUsuariosEnviarBodega.NewRow();
                    RowMail["email"] = item.EMAIL.Replace("@mueblesfiesta.com", "@productosmultiples.com");
                    dtUsuariosEnviarBodega.Rows.Add(RowMail);
                }


                int mExisteRelacionVendedor = (from cv in db.CLIENTE_VENDEDORs where cv.CLIENTE == cliente && cv.VENDEDOR == mVendedor select cv).Count();

                if (mExisteRelacionVendedor == 0)
                {
                    CLIENTE_VENDEDOR iCLIENTE_VENDEDOR = new CLIENTE_VENDEDOR
                    {
                        CLIENTE = cliente,
                        VENDEDOR = mVendedor,
                        NoteExistsFlag = 0,
                        RowPointer = (System.Guid)db.fcNewID(),
                        RecordDate = DateTime.Now,
                        CreatedBy = string.Format("FA/{0}", usuario),
                        UpdatedBy = string.Format("FA/{0}", usuario),
                        CreateDate = DateTime.Now
                    };

                    db.CLIENTE_VENDEDORs.InsertOnSubmit(iCLIENTE_VENDEDOR);
                }

                mNombreCliente = (from c in db.CLIENTEs where c.CLIENTE1 == cliente select c).First().NOMBRE;

                var qCliente = from c in db.MF_Clientes where c.CLIENTE == cliente select c;
                foreach (var item in qCliente)
                {
                    mDireccion = string.Format("Call/Av/Km/Mz: {0} Casa/Lote: {1} Apto/Piso: {2} Zona/Aldea: {3} Col/Barr/Cant: {4} Departamento: {5} Municipio: {6}", item.CALLE_AVENIDA_FACTURA, item.CASA_FACTURA, item.APARTAMENTO_FACTURA, item.ZONA_FACTURA, item.APARTAMENTO_FACTURA, item.COLONIA_FACTURA, item.DEPARTAMENTO_FACTURA, item.MUNICIPIO_FACTURA);
                }

                //Tabla de regalos
                DataTable dtRegalo = new DataTable("PedidoLinea");

                dtRegalo.Columns.Add(new DataColumn("Articulo", typeof(System.String)));
                dtRegalo.Columns.Add(new DataColumn("Descripcion", typeof(System.String)));
                dtRegalo.Columns.Add(new DataColumn("Cantidad", typeof(System.Int32)));
                dtRegalo.Columns.Add(new DataColumn("PrecioBase", typeof(System.Decimal)));
                dtRegalo.Columns.Add(new DataColumn("PrecioLista", typeof(System.Decimal)));
                dtRegalo.Columns.Add(new DataColumn("Tipo", typeof(System.String)));
                dtRegalo.Columns.Add(new DataColumn("IdDescuento", typeof(System.Int32)));

                //Select de las descripciones #cambiar
                //int mCantidadPatas36 = 0;
                //int mCantidadPatas37 = 0;
                //int mCantidadPatas38 = 0;
                //int mCantidadPatas39 = 0;
                //string mDescripcion36 = (from a in db.ARTICULOs where a.ARTICULO1 == "091036-000" select a).First().DESCRIPCION;
                //string mDescripcion37 = (from a in db.ARTICULOs where a.ARTICULO1 == "091037-000" select a).First().DESCRIPCION;
                //string mDescripcion38 = (from a in db.ARTICULOs where a.ARTICULO1 == "091038-000" select a).First().DESCRIPCION;
                //string mDescripcion39 = (from a in db.ARTICULOs where a.ARTICULO1 == "091039-000" select a).First().DESCRIPCION;

                //articuloPadre
                DataRow[] mRowsOrdenadas = cliente == "MA0018" ? dsInfo.Tables["PedidoLinea"].Select() : dsInfo.Tables["PedidoLinea"].Select("", "Articulo");
                

                //Agrego la columna Linea que me servirá para llevar el control de la base que debo modificar para cuadrar el precio del KIT
                dsInfo.Tables["PedidoLinea"].Columns.Add(new DataColumn("Padre", typeof(System.String), string.Empty));
                dsInfo.Tables["PedidoLinea"].Columns.Add(new DataColumn("Dividir", typeof(System.Int32)));
                dsInfo.Tables["PedidoLinea"].Columns.Add(new DataColumn("Descuento", typeof(System.Decimal)));
                dsInfo.Tables["PedidoLinea"].Columns.Add(new DataColumn("DiferenciaPL", typeof(System.Decimal)));

                foreach (DataRow mLinea in dsInfo.Tables["PedidoLinea"].Rows)
                {
                    mLinea["Padre"] = string.Empty;
                    mLinea["Descuento"] = 0;
                    mLinea["DiferenciaPL"] = 0;
                }

                for (int ii = 0; ii < mRowsOrdenadas.Length; ii++)
                {
                    Int32 mCantidad = Convert.ToInt32(mRowsOrdenadas[ii]["Cantidad"]);
                    DataRow mRow = dtPedidoOriginal.NewRow();

                    for (int jj = 0; jj < dtPedidoOriginal.Columns.Count; jj++)
                    {
                        mRow[jj] = mRowsOrdenadas[ii][jj];
                    }

                    dtPedidoOriginal.Rows.Add(mRow);

                    //Regalo
                    if (Convert.ToString(mRowsOrdenadas[ii]["TieneRegalo"]) == "S")
                    {
                        string[] mRegalos = Convert.ToString(mRowsOrdenadas[ii]["Regalo"]).Split(new string[] { "," }, StringSplitOptions.None);

                        for (int rr = 0; rr < mRegalos.Length; rr++)
                        {
                            string mCodigoRegalo = mRegalos[rr];
                            if (Convert.ToInt32(dtRegalo.Compute("COUNT(Articulo)", string.Format("Articulo = '{0}'", mCodigoRegalo))) == 0)
                            {
                                DataRow mRowRegalo = dtRegalo.NewRow();
                                mRowRegalo["Articulo"] = mCodigoRegalo;
                                mRowRegalo["Descripcion"] = (from a in db.ARTICULOs where a.ARTICULO1 == mCodigoRegalo select a).First().DESCRIPCION.Replace("|ORIGINAL", "");
                                mRowRegalo["PrecioBase"] = 0;
                                mRowRegalo["PrecioLista"] = 0;
                                mRowRegalo["Cantidad"] = Convert.ToInt32(mRowsOrdenadas[ii]["Cantidad"]) * Convert.ToInt32(mRowsOrdenadas[ii]["CantidadRegalo"]);
                                mRowRegalo["Tipo"] = "T";
                                mRowRegalo["IdDescuento"] = Convert.ToInt32(mRowsOrdenadas[ii]["IdDescuento"]);
                                dtRegalo.Rows.Add(mRowRegalo);
                            }
                            else
                            {
                                for (int jj = 0; jj < dtRegalo.Rows.Count; jj++)
                                {
                                    if (Convert.ToString(dtRegalo.Rows[jj]["Articulo"]) == mCodigoRegalo)
                                    {
                                        dtRegalo.Rows[jj]["Cantidad"] = Convert.ToInt32(dtRegalo.Rows[jj]["Cantidad"]) + (Convert.ToInt32(mRowsOrdenadas[ii]["Cantidad"]) * Convert.ToInt32(mRowsOrdenadas[ii]["CantidadRegalo"]));
                                    }
                                }
                            }
                        }

                    }

                }
                var qBasesSinPatas = (from t in db.MF_Catalogos where t.CODIGO_TABLA == 74 select t.CODIGO_CAT).ToList();
                for (int ii = 0; ii < dsInfo.Tables["PedidoLinea"].Rows.Count; ii++)
                {
                    string mArticulo = Convert.ToString(dsInfo.Tables["PedidoLinea"].Rows[ii]["Articulo"]);
                    string mTipo = string.Empty;
                    string mDescArticulo = string.Empty;
                    string mClasif3 = string.Empty;

                    var qArticulo = (from a in db.ARTICULOs where a.ARTICULO1 == mArticulo select new { Tipo = a.TIPO, Clasficacion3 = a.CLASIFICACION_3, Descripcion = a.DESCRIPCION }).First();
                    if (qArticulo != null)
                    {
                        mTipo = qArticulo.Tipo;
                        mClasif3 = qArticulo.Clasficacion3 != null ? qArticulo.Clasficacion3.ToString() : "";
                        mDescArticulo = qArticulo.Descripcion.ToString();
                    }
                    Int32 mCantidad = Convert.ToInt32(dsInfo.Tables["PedidoLinea"].Rows[ii]["Cantidad"]);
                    //Cambio si incluye patas.
                    if (mTipo == "T" && mClasif3.Equals("142") && cliente != "MA0018" && !qBasesSinPatas.Any(x => x==mArticulo)) //bases sueltas solamente
                    { 
                        lstBases.Add(new DtoLoteBases { Cantidad = mCantidad, Base = new DtoBaseCama { codArticulo = mArticulo, Descripcion = mDescArticulo } });
                    }
                }

                for (int ii = 0; ii < dsInfo.Tables["PedidoLinea"].Rows.Count; ii++)
                {
                    string mArticulo = Convert.ToString(dsInfo.Tables["PedidoLinea"].Rows[ii]["Articulo"]);
                    string mTipo = string.Empty;
                    string mClasif3 = string.Empty;
                    var qArticulo = (from a in db.ARTICULOs where a.ARTICULO1 == mArticulo select new { Tipo = a.TIPO, Clasficacion3 = a.CLASIFICACION_3 }).First();

                    if (qArticulo != null)
                    {
                        mTipo = qArticulo.Tipo;
                        mClasif3 = qArticulo.Clasficacion3 != null ? qArticulo.Clasficacion3.ToString() : "";
                    }
                    Int32 mCantidad = Convert.ToInt32(dsInfo.Tables["PedidoLinea"].Rows[ii]["Cantidad"]);
                    int IdDescuento = int.Parse(dsInfo.Tables["PedidoLinea"].Rows[ii]["TipoDescuento"].ToString());


                    #region "kiT"

                    if (mTipo == "K")
                    {
                        var qComponentes = from ae in db.ARTICULO_ENSAMBLEs where ae.ARTICULO_PADRE == mArticulo orderby ae.ARTICULO_HIJO select ae;
                        foreach (var item in qComponentes)
                        {
                            string mDescripcion = "";
                            decimal mPrecioLista = 0, mDescuento = 0; double mPrecioBase = 0;
                            string mTieneRegalo = "N"; string mRegalo = ""; Int32 mCantidadRegalo = 1;
                            Patas patas = new Patas();

                            //cambio
                            /*if (cliente != "MA0018")
                            {*/
                            //switch (item.ARTICULO_HIJO)
                            //{
                            //    case "091036-000":
                            //        mDescripcion = mDescripcion36;
                            //        break;
                            //    case "091037-000":
                            //        mDescripcion = mDescripcion37;
                            //        break;
                            //    case "091038-000":
                            //        mDescripcion = mDescripcion38;
                            //        break;
                            //    case "091039-000":
                            //    mDescripcion = mDescripcion38;
                            //    break;
                            //default:

                            //Nueva implementacion de busqueda de patas
                            //patas.todosLosAtributos(db,item.ARTICULO_PADRE, log);
                            //mDescripcion = patas.descripcion;

                                    var qInfo = from a in db.MF_ArticulosMayoreos where a.ARTICULO == item.ARTICULO_HIJO && a.CLIENTE == cliente select a;
                                        foreach (var itemInfo in qInfo)
                                        {
                                            double mDescuentoKit = (double)(from a in db.MF_ArticulosMayoreos where a.ARTICULO == mArticulo && a.CLIENTE == cliente select a).First().PORCENTAJE_DESCUENTO / 100;

                                            mTieneRegalo = itemInfo.TIENE_REGALO.ToString();
                                            mRegalo = itemInfo.REGALO;
                                            mCantidadRegalo = itemInfo.CANTIDAD_REGALO;
                                            mDescripcion = itemInfo.DESCRIPCION;
                                            mPrecioBase = (double)itemInfo.PRECIO_BASE_LOCAL - ((double)itemInfo.PRECIO_BASE_LOCAL * mDescuentoKit);
                                            mPrecioLista = Math.Round((itemInfo.PRECIO_BASE_LOCAL - (itemInfo.PRECIO_BASE_LOCAL * (decimal)mDescuentoKit)) * mIVA, 2, MidpointRounding.AwayFromZero);
                                            mDescuento = decimal.Parse(itemInfo.PORCENTAJE_DESCUENTO.ToString().Equals("") ? "0" : itemInfo.PORCENTAJE_DESCUENTO.ToString());
                                        }

                                        if (qInfo.Count() == 0)
                                        {
                                            var qInfoArticulo = from a in db.ARTICULOs where a.ARTICULO1 == item.ARTICULO_HIJO select a;
                                            foreach (var itemInfo in qInfoArticulo)
                                            {
                                                mDescripcion = itemInfo.DESCRIPCION;
                                                mPrecioBase = (double)itemInfo.PRECIO_BASE_LOCAL;
                                                mPrecioLista = Math.Round(itemInfo.PRECIO_BASE_LOCAL * mIVA, 2, MidpointRounding.AwayFromZero);
                                            }
                                            StringBuilder CuerpoCorreo = new StringBuilder();
                                            CuerpoCorreo.Append(string.Format("El KIT {0} está siendo utilizado por el cliente {1} - {2}, y el componente {3} {4} no existe en su lista de precios.  Se agregó el artículo de FIESTA para grabar el pedido {5}", mArticulo, cliente, mNombreCliente, item.ARTICULO_HIJO, mDescripcion, mPedido));

                                            Mail mail = new Mail();
                                            mail.EnviarEmail(WebConfigurationManager.AppSettings["UsrMail"].ToString(), WebConfigurationManager.AppSettings["ITMailNotification"].ToString(), "Componente de KIT no existe en la lista de precios de Mayoreo", CuerpoCorreo, "Punto de Venta Mayoreo");

                                        }
                                //        break;
                                //}
                            /*}
                            else {
                                var qInfo = from a in db.MF_ArticulosMayoreos where a.ARTICULO == item.ARTICULO_HIJO && a.CLIENTE == cliente select a;
                                foreach (var itemInfo in qInfo)
                                {
                                    double mDescuentoKit = (double)(from a in db.MF_ArticulosMayoreos where a.ARTICULO == mArticulo && a.CLIENTE == cliente select a).First().PORCENTAJE_DESCUENTO / 100;

                                    mTieneRegalo = itemInfo.TIENE_REGALO.ToString();
                                    mRegalo = itemInfo.REGALO;
                                    mCantidadRegalo = itemInfo.CANTIDAD_REGALO;
                                    mDescripcion = itemInfo.DESCRIPCION;
                                    mPrecioBase = (double)itemInfo.PRECIO_BASE_LOCAL - ((double)itemInfo.PRECIO_BASE_LOCAL * mDescuentoKit);
                                    mPrecioLista = Math.Round((itemInfo.PRECIO_BASE_LOCAL - (itemInfo.PRECIO_BASE_LOCAL * (decimal)mDescuentoKit)) * mIVA, 2, MidpointRounding.AwayFromZero);
                                    mDescuento = decimal.Parse(itemInfo.PORCENTAJE_DESCUENTO.ToString().Equals("") ? "0" : itemInfo.PORCENTAJE_DESCUENTO.ToString());
                                }

                                if (qInfo.Count() == 0)
                                {
                                    var qInfoArticulo = from a in db.ARTICULOs where a.ARTICULO1 == item.ARTICULO_HIJO select a;
                                    foreach (var itemInfo in qInfoArticulo)
                                    {
                                        mDescripcion = itemInfo.DESCRIPCION;
                                        mPrecioBase = (double)itemInfo.PRECIO_BASE_LOCAL;
                                        mPrecioLista = Math.Round(itemInfo.PRECIO_BASE_LOCAL * mIVA, 2, MidpointRounding.AwayFromZero);
                                    }
                                    StringBuilder CuerpoCorreo = new StringBuilder();
                                    CuerpoCorreo.Append(string.Format("El KIT {0} está siendo utilizado por el cliente {1} - {2}, y el componente {3} {4} no existe en su lista de precios.  Se agregó el artículo de FIESTA para grabar el pedido {5}", mArticulo, cliente, mNombreCliente, item.ARTICULO_HIJO, mDescripcion, mPedido));

                                    Mail mail = new Mail();
                                    mail.EnviarEmail(WebConfigurationManager.AppSettings["UsrMail"].ToString(), WebConfigurationManager.AppSettings["ITMailNotification"].ToString(), "Componente de KIT no existe en la lista de precios de Mayoreo", CuerpoCorreo, "Punto de Venta Mayoreo");

                                }
                            }*/
                            Int32 mCantidadPedido  = 0;
                            /*if(cliente == "MA0018")
                                mCantidadPedido =  Convert.ToInt32(item.CANTIDAD);
                            else */
                               mCantidadPedido = mCantidad * Convert.ToInt32(item.CANTIDAD);
                            
                            DataRow mRow = dsInfo.Tables["PedidoLinea"].NewRow();
                            mRow["Articulo"] = item.ARTICULO_HIJO;
                            mRow["Descripcion"] = mDescripcion;
                            mRow["Cantidad"] = mCantidadPedido;
                            mRow["PrecioBase"] = mPrecioBase * mCantidadPedido;
                            mRow["PrecioLista"] = mPrecioLista * mCantidadPedido;
                            //if (item.ARTICULO_HIJO.Contains("142")) //Rop032019
                            //{
                            //    mRow["PrecioBase"] = mPrecioBase * mCantidadPedido;
                            //    mRow["PrecioLista"] = mPrecioLista * mCantidadPedido;
                            //}
                            //else
                            //{
                            //    mRow["PrecioBase"] = mPrecioBase;
                            //    mRow["PrecioLista"] = mPrecioLista;
                            //}
                            mRow["TieneRegalo"] = mTieneRegalo;
                            mRow["Regalo"] = mRegalo;
                            mRow["CantidadRegalo"] = mCantidadRegalo;
                            mRow["Tipo"] = "T";
                            mRow["Descuento"] = mDescuento;
                            mRow["DiferenciaPL"] = 0;
                            mRow["TipoDescuento"] = IdDescuento;

                            mRow["Padre"] = mArticulo;

                            mRow["Dividir"] = mCantidadPedido;
                            dsInfo.Tables["PedidoLinea"].Rows.Add(mRow);
                            mensaje = string.Format(" {0} __ ArticuloAA={1} Cantidad={2} mPrecioBase={3} mCantidadPedido{4} mPrecioLista={5} PrecioBase={6} PrecioLista={7} mDescuento={8} IdDescuento={9} PadreAA={10} __ ", mensaje, item.ARTICULO_HIJO, mCantidadPedido.ToString(), mPrecioBase.ToString(), mCantidadPedido.ToString(), mPrecioLista.ToString(), mRow["PrecioBase"].ToString(), mRow["PrecioLista"].ToString(), mDescuento.ToString(), IdDescuento.ToString(), mArticulo);
                        }
                    }
                    #endregion

                    double mPrecioBaseKit = Convert.ToDouble(dsInfo.Tables["PedidoLinea"].Rows[ii]["PrecioBase"]) * mCantidad;
                    decimal mPrecioListaKit = (decimal)dsInfo.Tables["PedidoLinea"].Rows[ii]["PrecioLista"] * mCantidad;
                    //double mPrecioBaseKit = Convert.ToDouble(dsInfo.Tables["PedidoLinea"].Rows[ii]["PrecioBase"]);
                    //decimal mPrecioListaKit = (decimal)dsInfo.Tables["PedidoLinea"].Rows[ii]["PrecioLista"]; //Rop032019

                    double mPrecioBaseKitActual = 0; decimal mPrecioListaKitActual = 0;
                    try
                    {
                        mPrecioBaseKitActual = Convert.ToDouble(dsInfo.Tables["PedidoLinea"].Compute("SUM(PrecioBase)", string.Format("Padre = '{0}'", mArticulo)));
                        mPrecioListaKitActual = (decimal)dsInfo.Tables["PedidoLinea"].Compute("SUM(PrecioLista)", string.Format("Padre = '{0}'", mArticulo));
                    }
                    catch
                    {
                        //Nothing
                    }

                    double mDiferenciaPB = mPrecioBaseKitActual - mPrecioBaseKit;
                    decimal mDiferenciaPL = mPrecioListaKitActual - mPrecioListaKit;

                    for (int jj = 0; jj < dsInfo.Tables["PedidoLinea"].Rows.Count; jj++)
                    {
                        if (Convert.ToString(dsInfo.Tables["PedidoLinea"].Rows[jj]["Padre"]) == mArticulo &&
                            (Convert.ToString(dsInfo.Tables["PedidoLinea"].Rows[jj]["Articulo"]).Contains("142") //descuento en bases
                            ))
                        {
                            dsInfo.Tables["PedidoLinea"].Rows[jj]["PrecioBase"] = Convert.ToDouble(dsInfo.Tables["PedidoLinea"].Rows[jj]["PrecioBase"]) - mDiferenciaPB;
                            dsInfo.Tables["PedidoLinea"].Rows[jj]["PrecioLista"] = Convert.ToDecimal(dsInfo.Tables["PedidoLinea"].Rows[jj]["PrecioLista"]) - mDiferenciaPL;

                        }
                        if (Convert.ToString(dsInfo.Tables["PedidoLinea"].Rows[jj]["Padre"]) == mArticulo &&
                            (Convert.ToString(dsInfo.Tables["PedidoLinea"].Rows[jj]["Articulo"]).Contains("142") //agregar distintivo de descuento (bandera) para separar los colchones y las bases que tienen descuento de las que no
                             || (Convert.ToString(dsInfo.Tables["PedidoLinea"].Rows[jj]["Articulo"]).Contains("141"))))
                        {
                            dsInfo.Tables["PedidoLinea"].Rows[jj]["DiferenciaPL"] = mDiferenciaPL;
                        }
                    }
                }


                DataTable dtPedidoLinea = new DataTable();
                DataTable dtArticulosDistinct = new DataTable();

                string[] mColumns = new string[] { "Articulo", "Descuento", "DiferenciaPL" };
                dtArticulosDistinct = dsInfo.Tables["PedidoLinea"].DefaultView.ToTable(true, mColumns);

                dtPedidoLinea.Columns.Add(new DataColumn("Articulo", typeof(System.String)));
                dtPedidoLinea.Columns.Add(new DataColumn("Descripcion", typeof(System.String)));
                dtPedidoLinea.Columns.Add(new DataColumn("PrecioBase", typeof(System.Decimal)));
                dtPedidoLinea.Columns.Add(new DataColumn("PrecioLista", typeof(System.Decimal)));
                dtPedidoLinea.Columns.Add(new DataColumn("Cantidad", typeof(System.Int32)));
                dtPedidoLinea.Columns.Add(new DataColumn("IdDescuento", typeof(System.Int32)));

                for (int jj = 0; jj < dsInfo.Tables["PedidoLinea"].Rows.Count; jj++)
                {
                    mensaje = string.Format(" {0} PL $$ ArticuloBB={1} PrecioBase={2} PrecioLista={3} Descuento={4} DiferenciaPL={5} CantidadBB={6} $$ ", mensaje, dsInfo.Tables["PedidoLinea"].Rows[jj]["Articulo"].ToString(), dsInfo.Tables["PedidoLinea"].Rows[jj]["PrecioBase"].ToString(), dsInfo.Tables["PedidoLinea"].Rows[jj]["PrecioLista"].ToString(), dsInfo.Tables["PedidoLinea"].Rows[jj]["Descuento"].ToString(), dsInfo.Tables["PedidoLinea"].Rows[jj]["DiferenciaPL"].ToString(), dsInfo.Tables["PedidoLinea"].Rows[jj]["Cantidad"].ToString());
                }

                for (int ii = 0; ii < dtArticulosDistinct.Rows.Count; ii++)
                {
                    mensaje = string.Format(" {0} AD ## ArticuloCC={1} Descuento={2} DiferenciaPLCC={3} ## ", mensaje, dtArticulosDistinct.Rows[ii]["Articulo"].ToString(), dtArticulosDistinct.Rows[ii]["Descuento"].ToString(), dtArticulosDistinct.Rows[ii]["DiferenciaPL"].ToString());
                }


                articuloPadre = dtArticulosDistinct.Rows[0]["Articulo"].ToString();
                for (int ii = 0; ii < dtArticulosDistinct.Rows.Count; ii++)
                
                {
                    string mArticulo = dtArticulosDistinct.Rows[ii]["Articulo"].ToString();
                    

                    double mDescuento = double.Parse(dtArticulosDistinct.Rows[ii]["Descuento"].ToString());
                    double mDiferenciaPL = double.Parse(dtArticulosDistinct.Rows[ii]["DiferenciaPL"].ToString().Equals("") ? "0" : dtArticulosDistinct.Rows[ii]["DiferenciaPL"].ToString());

                    //obtengo el padre de los artículos, para evitar agrupar aquiellos que están sueltos


                    string mTipo = string.Empty;
                    string mClasif3 = string.Empty;
                    var qArticulo = (from a in db.ARTICULOs where a.ARTICULO1 == mArticulo select new { Tipo = a.TIPO, Clasficacion3 = a.CLASIFICACION_3 }).First();
                    if (qArticulo != null)
                    {
                        mTipo = qArticulo.Tipo;
                        mClasif3 = qArticulo.Clasficacion3 != null ? qArticulo.Clasficacion3.ToString() : "";
                    }

                    //En este caso mDescuento para los colchones separados, tendrá 0, no es para efectos de cálculo ya que el precio base y precio lista ya contiene su descuento
                    // es para efectos de agrupamiento y separación con los artículos que vienen de los sets
                    //artículos con el mismo código, pero de diferente set se agruparán
                    // lo importante es sumar las cantidades para el mismo código de artículo que tenga el mismo precio para evitar descuadres por los descuentos

                    //log.Debug(string.Format("Articulo: {0}", dtArticulosDistinct.Rows[ii]["Articulo"].ToString()));
                    //log.Debug(string.Format("Descuento: {0}", dtArticulosDistinct.Rows[ii]["Descuento"].ToString()));
                    //log.Debug(string.Format("DiferenciaPL: {0}", dtArticulosDistinct.Rows[ii]["DiferenciaPL"].ToString()));

                    Int32 mCantidad = mCantidad = Convert.ToInt32(dsInfo.Tables["PedidoLinea"].Compute("SUM(Cantidad)", string.Format("Articulo = '{0}' AND Descuento='{1}' AND DiferenciaPL='{2}'", mArticulo, mDescuento, mDiferenciaPL))); ;

                    int IdDescuento = int.Parse(dsInfo.Tables["PedidoLinea"].Select("Articulo='" + mArticulo + "'").FirstOrDefault()["TipoDescuento"].ToString());
                    
                    if (mTipo == "T" && mArticulo != "091036-000" && mArticulo != "091037-000" && mArticulo != "091038-000" && mArticulo != "091039-000") //validación de códigos de patas
                    {
                        string mDescripcion = "";
                        decimal mPrecioLista = 0; double mPrecioBase = 0;

                        for (int jj = 0; jj < dsInfo.Tables["PedidoLinea"].Rows.Count; jj++)
                        {
                            if (mArticulo == Convert.ToString(dsInfo.Tables["PedidoLinea"].Rows[jj]["Articulo"]))
                            {
                                mDescripcion = Convert.ToString(dsInfo.Tables["PedidoLinea"].Rows[jj]["Descripcion"]);

                                //Cambio en la agrupación de los componentes sueltos y de kit

                                mPrecioBase = Convert.ToDouble(dsInfo.Tables["PedidoLinea"].Compute("SUM(PrecioBase)", string.Format("Articulo = '{0}'  AND Descuento='{1}' AND DiferenciaPL='{2}'", mArticulo, mDescuento, mDiferenciaPL)) ?? "0");
                                mPrecioLista = Convert.ToDecimal(dsInfo.Tables["PedidoLinea"].Compute("SUM(PrecioLista)", string.Format("Articulo = '{0}'  AND Descuento='{1}' AND DiferenciaPL='{2}'", mArticulo, mDescuento, mDiferenciaPL)) ?? "0");
                                //mPrecioBase = Convert.ToDouble(dsInfo.Tables["PedidoLinea"].Rows[jj]["PrecioBase"]); //Rop032019
                                //mPrecioLista = Convert.ToDecimal(dsInfo.Tables["PedidoLinea"].Rows[jj]["PrecioLista"]);

                                mensaje = string.Format(" ?? {0} !! mPrecioBaseADD={1} mArticulo={2} mDescuento={3} mDiferenciaPL={4} mPrecioListaADD={5} $$ ", mensaje, mPrecioBase.ToString(), mArticulo, mDescuento.ToString(), mDiferenciaPL.ToString(), mPrecioLista.ToString());
                            }
                        }


                        var qInfo = from a in db.MF_ArticulosMayoreos where a.ARTICULO == mArticulo && a.CLIENTE == cliente select a;
                        if (qInfo.Count() == 0)
                        {
                            var qInfoArticulo = from a in db.ARTICULOs where a.ARTICULO1 == mArticulo select a;
                            foreach (var itemInfo in qInfoArticulo)
                            {
                                mDescripcion = itemInfo.DESCRIPCION;
                                mPrecioBase = (double)itemInfo.PRECIO_BASE_LOCAL;
                                mPrecioLista = Math.Round(itemInfo.PRECIO_BASE_LOCAL * mIVA, 2, MidpointRounding.AwayFromZero);
                            }
                        }

                        Int32 mCantidadDividir = mCantidad;

                        if (dtArticulosDistinct.Rows.Count == dtPedidoOriginal.Rows.Count)  //<--Cambio para que los artículos que no tienen padre, no los divida
                            mCantidadDividir = 1;

                        if (mArticulo == "M141391-105")
                        {
                            int jj = 0;
                            jj = jj + ii;
                        }

                        try
                        {
                            int mDividir = Convert.ToInt32(dsInfo.Tables["PedidoLinea"].Compute("SUM(Dividir)", string.Format("Articulo = '{0}' AND Descuento='{1}'", mArticulo, mDescuento)));
                            if (mDividir <= 0) mCantidadDividir = 1;
                        }
                        catch
                        {
                            mCantidadDividir = 1;
                        }

                        DataRow mRow = dtPedidoLinea.NewRow();
                        mRow["Articulo"] = mArticulo;
                        mRow["Descripcion"] = mDescripcion;
                        mRow["PrecioBase"] = mPrecioBase / mCantidadDividir;
                        mRow["PrecioLista"] = Math.Round(mPrecioLista / mCantidadDividir, 2, MidpointRounding.AwayFromZero);
                        mRow["Cantidad"] = mCantidad;
                        mRow["IdDescuento"] = IdDescuento;
                        mensaje = string.Format(" ** {0} ArticuloEE={1} PrecioBase={2} PrecioLista={3} Cantidad={4} mPrecioBase={5} mCantidadDividir={6} IdDescuentoEE={7} ++ ", mensaje, mArticulo, mRow["PrecioBase"].ToString(), mRow["PrecioLista"].ToString(), mRow["Cantidad"].ToString(), mPrecioBase.ToString(), mCantidadDividir.ToString(), IdDescuento.ToString());
                        dtPedidoLinea.Rows.Add(mRow);

                    }
                    else
                    {
                        //if (!qBasesSinPatas.Any(x => x == mArticulo))
                        //{
                        //    if (mArticulo == "091036-000" && cliente != "MA0018")
                        //    {
                        //        mCantidadPatas36 += mCantidad;//el set ya tiene configurada la cantidad de patas por cada uno
                        //    }
                        //    else if (mArticulo == "091037-000" && cliente != "MA0018")
                        //    {
                        //        mCantidadPatas37 += mCantidad;
                        //    }
                        //    else if (mArticulo == "091038-000" && cliente != "MA0018")
                        //    {
                        //        mCantidadPatas38 += mCantidad;
                        //    }
                        //    else if (mArticulo == "091039-000" && cliente != "MA0018")
                        //    {
                        //        mCantidadPatas39 += mCantidad;
                        //    }
                        //}
                        //Agregando sets para buscar cantidad de patas.
                        String articulo = mArticulo.Substring(0, 3);
                        String mayArticulo = mArticulo.Substring(0, 4);
                        Patas pats = new Patas();
                        if (articulo.Equals("149") || mayArticulo.Equals("M149")) {


                            int cantidad = mCantidad;
                            Patas patas1 = new Patas();

                            pats.todosLosAtributosSets(db, mArticulo, log);

                            //validando que obtuvo resultado de la consulta
                            if (pats.modelo != null && pats.cantidad != null && pats.descripcion != null)
                            {
                                patas1.modelo = pats.modelo;
                                int patasCantidad = cantidad;
                                int patasCant = int.Parse(pats.cantidad);
                                patas1.cantidad = ((int.Parse(pats.cantidad)) * (cantidad)).ToString();
                                patas1.descripcion = pats.descripcion;

                                //validando si existe en modelo en la lista
                                bool existe = patasSets.Any(item => item.modelo == pats.modelo);

                                //valida si incluye patas
                                if (existe && pats.incluye)
                                {
                                    //buscando el index del modelo para aumentar la cantidad de patas
                                    int index = patasSets.FindIndex(a => a.modelo == pats.modelo);
                                    patasSets[index].cantidad = (int.Parse(patasSets[index].cantidad) + int.Parse(patas1.cantidad)).ToString();
                                }
                                else if (!existe && pats.incluye)
                                {
                                    //agregando un nuevo modelo a la lista
                                    patasSets.Add(patas1);
                                }
                            }


                        }
                        //patasSets
                    }
                }
                #region "Patas"
                ///*****
                /// Obtiene las patas de acuerdo al modelo y número de bases solicitadas
                ///*****

                MF_Clases.Respuesta mRespuesta = new MF_Clases.Respuesta();
                if (lstBases.Count > 0) //bases sueltas
                {
                    try
                    {
                        #region "ambiente"
                        string mAmbiente = General.Ambiente;
                       

                        string mUrlRestServices = "http://localhost:53874/api";
                        if (mAmbiente == "PRO") mUrlRestServices = "http://sql.fiesta.local/RestServices/api";
                        if (mAmbiente == "PRU") mUrlRestServices = "http://sql.fiesta.local/RestServicesPruebas/api";
                        #endregion
                        WebRequest request = WebRequest.Create(string.Format("{0}/ReporteSets/PatasPedido", mUrlRestServices));
                        string json = JsonConvert.SerializeObject(lstBases);
                        byte[] data = Encoding.ASCII.GetBytes(json);
                        request.Method = "POST";
                        request.ContentType = "application/json";
                        request.ContentLength = data.Length;

                        using (var stream = request.GetRequestStream())
                        {
                            stream.Write(data, 0, data.Length);
                        }

                        HttpWebResponse response;
                        //HttpResponseMessage response = new HttpResponseMessage();

                        try 
                        {
                            response = (HttpWebResponse)request.GetResponse();
                        }
                        catch(WebException ex) 
                        {   
                            StreamReader reader;
                            string error;
                            using (var stream = ex.Response.GetResponseStream())
                            using (reader = new StreamReader(stream))
                            {
                                error = reader.ReadToEnd();
                            }
                            throw new Exception(error);
                        }

                         //response = (HttpWebResponse)request.GetResponse();
                        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                        Respuesta res = new Respuesta();
                        res = JsonConvert.DeserializeObject<Respuesta>(responseString);

                        //Select de la informacion del articulo 
                        //Console.WriteLine(lstBases[0].Base.codArticulo);
                        String articulo = "";
                        articulo = lstBases[0].Base.codArticulo;
                        Patas patas = new Patas();
                        //String modeloPatas = (from p in db.ARTICULO_ESPEs where p.ARTICULO == articulo && p.ATRIBUTO == "MODELO_PATAS" select p).First().VALOR;
                        //String cantidadPatas = (from p in db.ARTICULO_ESPEs where p.ARTICULO == articulo && p.ATRIBUTO == "CANTIDAD_PATAS" select p).First().NORMAL).ToString();
                        //String incluyePatas = (from p in db.ARTICULO_ESPEs where p.ARTICULO == articulo && p.ATRIBUTO == "INCLUYE_PATAS" select p).First().VALOR;
                        //patas.todosLosAtributos(db, articulo, log);
                        //String modeloPatas = patas.modelo;
                        //String cantidadPtas = patas.cantidad;

                        if (res.Exito)
                        {
                            lstPatas = JsonConvert.DeserializeObject<List<DtoLotePatas>>(res.Objeto.ToString());

                            if (lstPatas.Count > 0)
                            {
                                //if (mCantidadPatas36 > 0)
                                //    lstPatas.Add(new DtoLotePatas { ModeloPata = new DtoPatas { codArticulo = "091036-000" }, Cantidad = mCantidadPatas36 });
                                //if (mCantidadPatas37 > 0)
                                //    lstPatas.Add(new DtoLotePatas { ModeloPata = new DtoPatas { codArticulo = "091037-000" }, Cantidad = mCantidadPatas37 });
                                //if (mCantidadPatas38 > 0)
                                //    lstPatas.Add(new DtoLotePatas { ModeloPata = new DtoPatas { codArticulo = "091038-000" }, Cantidad = mCantidadPatas38 });
                                //if (mCantidadPatas39 > 0)
                                //    lstPatas.Add(new DtoLotePatas { ModeloPata = new DtoPatas { codArticulo = "091039-000" }, Cantidad = mCantidadPatas39 });
                                //articuloPadre = lstBases[0].Base.codArticulo;
                                //select de la cantidad de patas basado en el modelo de articulo
                                //Patas patas = new Patas();
                                //patas.todosLosAtributos(db, lstBases[0].Base.codArticulo, log);

                                //lstPatas.Add(new DtoLotePatas { ModeloPata = new DtoPatas { codArticulo = patas.modelo }, Cantidad = decimal.Parse(patas.cantidad) });

                                //var nTotalPatasxModelo =
                                // from player in lstPatas
                                // group player by player.ModeloPata.codArticulo into patasGrp
                                // select new
                                // {
                                //     codArticulo = patasGrp.Key,
                                //     cantidad = patasGrp.Sum(x => x.Cantidad),
                                // };

                                for (int i = 0; i < patasSets.Count; i++) {
                                    
                                }

                                //Logica dedicada a la cantidad de patas
                                List<Patas> lstPts = new List<Patas>();
                                //asignando las patas de los sets que se encontraron
                                lstPts = patasSets;
                                if (cliente != "MA0018") {
                                for(int i = 0; i < lstPatas.Count; i++) 
                                {
                                    // *-*-*-*-*-*-* Patas para las bases *-*-*-*-*-*-*
                                    bool existe = lstPts.Any(item => item.modelo == lstPatas[i].ModeloPata.codArticulo);
                                    //bool incluye = patas.incluyePatas
                                    if (existe)
                                    {
                                        //el modelo de patas ya existe en el arreglo solo se aumente su cantidad
                                        int index = lstPts.FindIndex(a => a.modelo == lstPatas[i].ModeloPata.codArticulo);
                                        decimal cantidadNueva = lstPatas[i].Cantidad * lstBases[i].Cantidad;
                                        lstPts[index].cantidad = (decimal.Parse(lstPts[index].cantidad) + cantidadNueva).ToString();
                                    }
                                    else 
                                    {
                                        //Agregando un nuevo modelo de patas al arreglo
                                        Patas pta = new Patas();
                                        pta.cantidad = (lstPatas[i].Cantidad * lstBases[i].Cantidad).ToString();
                                        pta.modelo = lstPatas[i].ModeloPata.codArticulo;
                                        pta.descripcion = lstPatas[i].ModeloPata.Descripcion;
                                        lstPts.Add(pta);
                                    }
                                }
                                }


                                foreach (Patas pata in lstPts)
                                {
                                    DataRow mRowPatas = dtPedidoLinea.NewRow();
                                    mRowPatas["Articulo"] = pata.modelo;
                                    //mRowPatas["Descripcion"] = item.codArticulo.Equals("091036-000") ? mDescripcion36 : item.codArticulo.Equals("091037-000") ? mDescripcion37 : item.codArticulo.Equals("091039-000")? mDescripcion39: mDescripcion38;
                                    mRowPatas["Descripcion"] = pata.descripcion;
                                    mRowPatas["PrecioBase"] = 0;
                                    mRowPatas["PrecioLista"] = 0;
                                    mRowPatas["Cantidad"] = decimal.Parse(pata.cantidad);
                                    mRowPatas["IdDescuento"] = 1;//no maneja descuentos, por defecto las patas van sin precio
                                    dtPedidoLinea.Rows.Add(mRowPatas);

                                    DataRow mRowPatasOriginal = dtPedidoOriginal.NewRow();
                                    mRowPatasOriginal["Articulo"] = pata.modelo;
                                    //mRowPatasOriginal["Descripcion"] = item.codArticulo.Equals("091036-000") ? mDescripcion36 : item.codArticulo.Equals("091037-000") ? mDescripcion37 : item.codArticulo.Equals("091039-000") ? mDescripcion39: mDescripcion38;
                                    mRowPatasOriginal["Descripcion"] = pata.descripcion;
                                    mRowPatasOriginal["PrecioBase"] = 0;
                                    mRowPatasOriginal["PrecioLista"] = 0;
                                    mRowPatasOriginal["Cantidad"] = decimal.Parse(pata.cantidad);
                                    mRowPatasOriginal["TipoDescuento"] = 1; //no maneja descuentos, por defecto las patas van sin precio
                                    dtPedidoOriginal.Rows.Add(mRowPatasOriginal);
                                }

                            }
                        }
                        else
                        {
                            mRetorna = false;
                            mensaje = res.Mensaje;
                        }

                    }
                    catch (Exception ex)
                    {
                        mRetorna = false;
                        mensaje = ex.Message;
                    }
                }
                else
                {
                    //if (mCantidadPatas36 > 0 && cliente != "MA0018")
                    //{
                    //    #region "patas 36"
                    //    DataRow mRowPatas = dtPedidoLinea.NewRow();
                    //    mRowPatas["Articulo"] = "091036-000";
                    //    mRowPatas["Descripcion"] = mDescripcion36;
                    //    mRowPatas["PrecioBase"] = 0;
                    //    mRowPatas["PrecioLista"] = 0;
                    //    mRowPatas["Cantidad"] = mCantidadPatas36;
                    //    mRowPatas["IdDescuento"] = 1;
                    //    dtPedidoLinea.Rows.Add(mRowPatas);

                    //    DataRow mRowPatasOriginal = dtPedidoOriginal.NewRow();
                    //    mRowPatasOriginal["Articulo"] = "091036-000";
                    //    mRowPatasOriginal["Descripcion"] = mDescripcion36;
                    //    mRowPatasOriginal["PrecioBase"] = 0;
                    //    mRowPatasOriginal["PrecioLista"] = 0;
                    //    mRowPatasOriginal["Cantidad"] = mCantidadPatas36;
                    //    dtPedidoOriginal.Rows.Add(mRowPatasOriginal);
                    //    #endregion 
                    //}
                    //
                    // if (mCantidadPatas37 > 0 && cliente != "MA0018")
                    //{
                    //    #region "patas 37"
                    //    DataRow mRowPatas = dtPedidoLinea.NewRow();
                    //    mRowPatas["Articulo"] = "091037-000";
                    //    mRowPatas["Descripcion"] = mDescripcion37;
                    //    mRowPatas["PrecioBase"] = 0;
                    //    mRowPatas["PrecioLista"] = 0;
                    //     mRowPatas["Cantidad"] = mCantidadPatas37;
                    //    mRowPatas["IdDescuento"] = 1;
                    //    dtPedidoLinea.Rows.Add(mRowPatas);
                    //
                    //     DataRow mRowPatasOriginal = dtPedidoOriginal.NewRow();
                    //    mRowPatasOriginal["Articulo"] = "091037-000";
                    //    mRowPatasOriginal["Descripcion"] = mDescripcion37;
                    //    mRowPatasOriginal["PrecioBase"] = 0;
                    //    mRowPatasOriginal["PrecioLista"] = 0;
                    //    mRowPatasOriginal["Cantidad"] = mCantidadPatas37;
                    //    mRowPatasOriginal["TipoDescuento"] = 1;
                    //    dtPedidoOriginal.Rows.Add(mRowPatasOriginal);
                    //    #endregion
                    //}

                    //if (mCantidadPatas38 > 0 && cliente != "MA0018")
                    //{
                    //    #region "patas 38"
                    //    DataRow mRowPatas = dtPedidoLinea.NewRow();
                    //    mRowPatas["Articulo"] = "091038-000";
                    //    mRowPatas["Descripcion"] = mDescripcion38;
                    //    mRowPatas["PrecioBase"] = 0;
                    //    mRowPatas["PrecioLista"] = 0;
                    //    mRowPatas["Cantidad"] = mCantidadPatas38;
                    //    mRowPatas["IdDescuento"] = 1;
                    //    dtPedidoLinea.Rows.Add(mRowPatas);
                    //
                    //    DataRow mRowPatasOriginal = dtPedidoOriginal.NewRow();
                    //    mRowPatasOriginal["Articulo"] = "091038-000";
                    //    mRowPatasOriginal["Descripcion"] = mDescripcion38;
                    //    mRowPatasOriginal["PrecioBase"] = 0;
                    //    mRowPatasOriginal["PrecioLista"] = 0;
                    //    mRowPatasOriginal["Cantidad"] = mCantidadPatas38;
                    //    mRowPatasOriginal["TipoDescuento"] = 1;
                    //    dtPedidoOriginal.Rows.Add(mRowPatasOriginal);
                    //    #endregion
                    //}
                    //if (mCantidadPatas39 > 0 && cliente != "MA0018")
                    //{
                    //    #region "patas 39"
                    //    DataRow mRowPatas = dtPedidoLinea.NewRow();
                    //    mRowPatas["Articulo"] = "091039-000";
                    //    mRowPatas["Descripcion"] = mDescripcion39;
                    //    mRowPatas["PrecioBase"] = 0;
                    //    mRowPatas["PrecioLista"] = 0;
                    //    mRowPatas["Cantidad"] = mCantidadPatas39;
                    //    mRowPatas["IdDescuento"] = 1;
                    //    dtPedidoLinea.Rows.Add(mRowPatas);
                    //
                    //    DataRow mRowPatasOriginal = dtPedidoOriginal.NewRow();
                    //    mRowPatasOriginal["Articulo"] = "091039-000";
                    //    mRowPatasOriginal["Descripcion"] = mDescripcion39;
                    //    mRowPatasOriginal["PrecioBase"] = 0;
                    //    mRowPatasOriginal["PrecioLista"] = 0;
                    //    mRowPatasOriginal["Cantidad"] = mCantidadPatas39;
                    //    mRowPatasOriginal["TipoDescuento"] = 1;
                    //    dtPedidoOriginal.Rows.Add(mRowPatasOriginal);
                    //    #endregion
                    //}

                    List<Patas> patasLista = new List<Patas>();

                    if (cliente != "MA0018"){
                    //cambio
                    Patas patas = new Patas();
                    //patas.todosLosAtributos(db, articuloPadre, log);

                    foreach (DataRow row in dtPedidoOriginal.Rows)
                    {
                        // *-*-*-*-*-*-* Patas para los sets *-*-*-*-*-*-*
                        string pedido = row["Articulo"].ToString();
                        string cantidad = row["Cantidad"].ToString();
                        Patas patas1 = new Patas();

                        patas.todosLosAtributosSets(db,pedido,log);

                            //validando que obtuvo resultado de la consulta
                            if (patas.modelo != null && patas.cantidad != null && patas.descripcion != null) { 
                                patas1.modelo = patas.modelo;
                                int patasCantidad = int.Parse(cantidad);
                                int patasCant = int.Parse(patas.cantidad);
                                patas1.cantidad = ((int.Parse(patas.cantidad)) * (int.Parse(cantidad))).ToString();
                                patas1.descripcion = patas.descripcion;

                                //validando si existe en modelo en la lista
                                bool existe = patasLista.Any(item => item.modelo == patas.modelo);

                                //valida si incluye patas
                                if (existe && patas.incluye)
                                {
                                    //buscando el index del modelo para aumentar la cantidad de patas
                                    int index = patasLista.FindIndex(a => a.modelo == patas.modelo);
                                    patasLista[index].cantidad = (int.Parse(patasLista[index].cantidad) + int.Parse(patas1.cantidad)).ToString();
                                }
                                else if (!existe && patas.incluye)
                                {
                                    //agregando un nuevo modelo a la lista
                                    patasLista.Add(patas1);
                                }
                            }
                        }
                    }

                    foreach(Patas pata in patasLista)
                    {
                        DataRow mRowPatas = dtPedidoLinea.NewRow();
                        mRowPatas["Articulo"] = pata.modelo;
                        mRowPatas["Descripcion"] = pata.descripcion;
                        mRowPatas["PrecioBase"] = 0;
                        mRowPatas["PrecioLista"] = 0;
                        mRowPatas["Cantidad"] = pata.cantidad;
                        mRowPatas["IdDescuento"] = 1;
                        dtPedidoLinea.Rows.Add(mRowPatas);

                        DataRow mRowPatasOriginal = dtPedidoOriginal.NewRow();
                        mRowPatasOriginal["Articulo"] = pata.modelo;
                        mRowPatasOriginal["Descripcion"] = pata.descripcion ;
                        mRowPatasOriginal["PrecioBase"] = 0;
                        mRowPatasOriginal["PrecioLista"] = 0;
                        mRowPatasOriginal["Cantidad"] = pata.cantidad;
                        mRowPatasOriginal["TipoDescuento"] = 1;
                        dtPedidoOriginal.Rows.Add(mRowPatasOriginal);
                    }

                    //cambio
                    //Select de la cantidad de patas basado en el articulo
                    //Patas patas = new Patas();
                    //patas.todosLosAtributos(db, articuloPadre, log);

                    //if (mCantidadPatas39 > 0 && cliente != "MA0018")
                    //if (cliente != "MA0018")
                    //{
                    //    #region "patas 39"
                    //    DataRow mRowPatas = dtPedidoLinea.NewRow();
                    //    mRowPatas["Articulo"] = patas.modelo;
                    //    mRowPatas["Descripcion"] = patas.descripcion;
                    //    mRowPatas["PrecioBase"] = 0;
                    //    mRowPatas["PrecioLista"] = 0;
                    //    mRowPatas["Cantidad"] = int.Parse(patas.cantidad);
                    //    mRowPatas["IdDescuento"] = 1;
                    //    dtPedidoLinea.Rows.Add(mRowPatas);
                    
                    //    DataRow mRowPatasOriginal = dtPedidoOriginal.NewRow();
                    //    mRowPatasOriginal["Articulo"] = patas.modelo;
                    //    mRowPatasOriginal["Descripcion"] = patas.descripcion;
                    //    mRowPatasOriginal["PrecioBase"] = 0;
                    //    mRowPatasOriginal["PrecioLista"] = 0;
                    //    mRowPatasOriginal["Cantidad"] = int.Parse(patas.cantidad);
                    //    mRowPatasOriginal["TipoDescuento"] = 1;
                    //    dtPedidoOriginal.Rows.Add(mRowPatasOriginal);
                    //    #endregion
                    //}
                }

                #endregion  
                if (mRetorna)
                {
                    for (int ii = 0; ii < dtRegalo.Rows.Count; ii++)
                    {
                        DataRow mRowRegalo = dtPedidoLinea.NewRow();
                        mRowRegalo["Articulo"] = dtRegalo.Rows[ii]["Articulo"];
                        mRowRegalo["Descripcion"] = dtRegalo.Rows[ii]["Descripcion"];
                        mRowRegalo["PrecioBase"] = dtRegalo.Rows[ii]["PrecioBase"];
                        mRowRegalo["PrecioLista"] = dtRegalo.Rows[ii]["PrecioLista"];
                        mRowRegalo["Cantidad"] = dtRegalo.Rows[ii]["Cantidad"];
                        dtPedidoLinea.Rows.Add(mRowRegalo);

                        DataRow mRowRegaloOriginal = dtPedidoOriginal.NewRow();
                        mRowRegaloOriginal["Articulo"] = dtRegalo.Rows[ii]["Articulo"];
                        mRowRegaloOriginal["Descripcion"] = dtRegalo.Rows[ii]["Descripcion"];
                        mRowRegaloOriginal["PrecioBase"] = dtRegalo.Rows[ii]["PrecioBase"];
                        mRowRegaloOriginal["PrecioLista"] = dtRegalo.Rows[ii]["PrecioLista"];
                        mRowRegaloOriginal["Cantidad"] = dtRegalo.Rows[ii]["Cantidad"];
                        dtPedidoOriginal.Rows.Add(mRowRegaloOriginal);
                    }

                    DataRow[] mRows = cliente == "MA0018" ? dtPedidoLinea.Select() : dtPedidoLinea.Select("", "Articulo");
                    mensaje = string.Format("{0} --Otro-- ", mensaje);
                    for (int ii = 0; ii < mRows.Count(); ii++)
                    {
                        //mTotalMercaderia = mTotalMercaderia + (Math.Round(Convert.ToDecimal(mRows[ii]["PrecioBase"]), 2, MidpointRounding.AwayFromZero) * Convert.ToInt32(mRows[ii]["Cantidad"]));
                        //mTotalFacturar = mTotalFacturar + (Math.Round(Convert.ToDecimal(mRows[ii]["PrecioLista"]), 2, MidpointRounding.AwayFromZero) * Convert.ToInt32(mRows[ii]["Cantidad"]));
                        mTotalMercaderia = mTotalMercaderia + (Math.Round(Convert.ToDecimal(mRows[ii]["PrecioBase"]), 2, MidpointRounding.AwayFromZero));
                        mTotalFacturar = mTotalFacturar + (Math.Round(Convert.ToDecimal(mRows[ii]["PrecioLista"]), 2, MidpointRounding.AwayFromZero)); //Rop032019
                        mensaje = string.Format(" %% {0} ArticuloFF={1} PrecioBase={2} PrecioLista={3} Cantidad={4} mTotalMercaderia={5} mTotalFacturarFF={6} == ", mensaje, mRows[ii]["Articulo"].ToString(), mRows[ii]["PrecioBase"].ToString(), mRows[ii]["PrecioLista"].ToString(), mRows[ii]["Cantidad"].ToString(), mTotalMercaderia.ToString(), mTotalFacturar.ToString());
                    }

                    mUnidades = Convert.ToInt32(dsInfo.Tables["PedidoLinea"].Compute("SUM(Cantidad)", ""));
                    mTotalMercaderia = Math.Round(mTotalMercaderia, 2, MidpointRounding.AwayFromZero);
                    mTotalFacturar = Math.Round(mTotalFacturar, 2, MidpointRounding.AwayFromZero);
                    mImpuesto = mTotalFacturar - mTotalMercaderia;

                    if (cuantosPedidos.Count() == 0)
                    {
                        string mConsecutivo = (from c in db.CONSECUTIVO_FAs where c.CODIGO_CONSECUTIVO == "PEDIDO" select c).First().VALOR_CONSECUTIVO;
                        Int64 mNumeroPedido = Convert.ToInt64(mConsecutivo.Replace("P", ""));

                        mNumeroPedido = mNumeroPedido + 1;
                        mPedido = Convert.ToString(mNumeroPedido);

                        switch (mPedido.Length)
                        {
                            case 1:
                                mPedido = string.Format("P00000{0}", mPedido);
                                break;
                            case 2:
                                mPedido = string.Format("P0000{0}", mPedido);
                                break;
                            case 3:
                                mPedido = string.Format("P000{0}", mPedido);
                                break;
                            case 4:
                                mPedido = string.Format("P00{0}", mPedido);
                                break;
                            case 5:
                                mPedido = string.Format("P0{0}", mPedido);
                                break;
                            case 6:
                                mPedido = string.Format("P0{0}", mPedido);
                                break;
                            case 7:
                                mPedido = string.Format("P0{0}", mPedido);
                                break;
                            case 8:
                                mPedido = string.Format("P{0}", mPedido);
                                break;
                        }

                        Pedido = mPedido;

                        PEDIDO iPedido = new PEDIDO()
                        {
                            PEDIDO1 = mPedido,
                            ESTADO = "N",
                            FECHA_PEDIDO = DateTime.Now.Date,
                            FECHA_PROMETIDA = DateTime.Now.Date,
                            FECHA_PROX_EMBARQU = DateTime.Now.Date,
                            FECHA_ULT_CANCELAC = new DateTime(1980, 1, 1),
                            FECHA_ORDEN = DateTime.Now.Date,
                            TARJETA_CREDITO = "",
                            EMBARCAR_A = mNombreCliente,
                            DIREC_EMBARQUE = "ND",
                            DIRECCION_FACTURA = mDireccion,
                            OBSERVACIONES = mOrdenCompra,
                            TOTAL_MERCADERIA = mTotalMercaderia,
                            MONTO_ANTICIPO = 0,
                            MONTO_FLETE = 0,
                            MONTO_SEGURO = 0,
                            MONTO_DOCUMENTACIO = 0,
                            TIPO_DESCUENTO1 = "P",
                            TIPO_DESCUENTO2 = "P",
                            MONTO_DESCUENTO1 = 0,
                            MONTO_DESCUENTO2 = 0,
                            PORC_DESCUENTO1 = 0,
                            PORC_DESCUENTO2 = 0,
                            TOTAL_IMPUESTO1 = mImpuesto,
                            TOTAL_IMPUESTO2 = 0,
                            TOTAL_A_FACTURAR = mTotalFacturar,
                            PORC_COMI_VENDEDOR = 0,
                            PORC_COMI_COBRADOR = 0,
                            TOTAL_CANCELADO = 0,
                            TOTAL_UNIDADES = mUnidades,
                            IMPRESO = "N",
                            FECHA_HORA = DateTime.Now,
                            DESCUENTO_VOLUMEN = 0,
                            TIPO_PEDIDO = "N",
                            MONEDA_PEDIDO = "L",
                            VERSION_NP = 1,
                            AUTORIZADO = "N",
                            DOC_A_GENERAR = "F",
                            CLASE_PEDIDO = "N",
                            MONEDA = "L",
                            NIVEL_PRECIO = "ContadoCheq",
                            COBRADOR = "F01",
                            RUTA = "ND",
                            USUARIO = usuario,
                            CONDICION_PAGO = "1",
                            BODEGA = mBodega,
                            ZONA = "0101",
                            VENDEDOR = mVendedor,
                            CLIENTE = cliente,
                            CLIENTE_DIRECCION = cliente,
                            CLIENTE_CORPORAC = cliente,
                            CLIENTE_ORIGEN = cliente,
                            PAIS = "GUA",
                            SUBTIPO_DOC_CXC = 0,
                            TIPO_DOC_CXC = "FAC",
                            BACKORDER = "S",
                            PORC_INTCTE = 0,
                            DESCUENTO_CASCADA = "S",
                            FIJAR_TIPO_CAMBIO = "N",
                            ORIGEN_PEDIDO = "F",
                            DESC_DIREC_EMBARQUE = mDireccionEmbarque,
                            RUBRO1 = "",
                            NoteExistsFlag = 0,
                            RecordDate = DateTime.Now,
                            RowPointer = (System.Guid)db.fcNewID(),
                            CreatedBy = string.Format("FA/{0}", usuario),
                            UpdatedBy = string.Format("FA/{0}", usuario),
                            CreateDate = DateTime.Now,
                            NOMBRE_CLIENTE = mNombreCliente,
                            FECHA_PROYECTADA = DateTime.Now.Date
                        };

                        db.PEDIDOs.InsertOnSubmit(iPedido);

                        MF_Pedido iMF_Pedido = new MF_Pedido()
                        {
                            PEDIDO = mPedido,
                            TIPOVENTA = "NR",
                            FINANCIERA = 1,
                            NIVEL_PRECIO = "ContadoCheq",
                            ENTREGA_AM_PM = "AM",
                            MERCADERIA_SALE = mBodega,
                            DESARMARLA = Convert.ToChar("N"),
                            NOTAS_TIPOVENTA = observaciones,
                            PAGARE = 0,
                            NOMBRE_AUTORIZACION = mOrdenCompra,
                            AUTORIZACION = ordenCompra,
                            TOTAL_FACTURAR = mTotalFacturar,
                            ENGANCHE = 0,
                            SALDO_FINANCIAR = 0,
                            RECARGOS = 0,
                            MONTO = mTotalFacturar,
                            SOLICITUD = 0,
                            CANTIDAD_PAGOS1 = 0,
                            MONTO_PAGOS1 = 0,
                            CANTIDAD_PAGOS2 = 0,
                            MONTO_PAGOS2 = 0,
                            TIPO_REFERENCIA = "O",
                            OBSERVACIONES_REFERENCIA = "",
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("FA/{0}", usuario),
                            UpdatedBy = string.Format("FA/{0}", usuario),
                            CreateDate = DateTime.Now,
                            TIPOPEDIDO = Convert.ToChar("N"),
                            FECHA_ENTREGA = Convert.ToDateTime(dsInfo.Tables["Pedido"].Rows[0]["CemacoFechaEntrega"]),
                            NOMBRE_RECIBE = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteNombre"]).Trim().Length > 0 ? Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteNombre"]) : mNombreCliente,
                            USUARIO = usuario,
                            FECHA_REGISTRO = DateTime.Now,
                            PUNTOS = 0,
                            VALOR_PUNTOS = 0,
                            CLIENTE = cliente,
                            LOCAL_F09 = Convert.ToChar("N"),
                            CEMACO_TIENDA = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoTienda"]),
                            CEMACO_FECHA_VENTA = Convert.ToDateTime(dsInfo.Tables["Pedido"].Rows[0]["CemacoFechaVenta"]),
                            CEMACO_CAJA = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoCaja"]),
                            CEMACO_TRANSACCION = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoTransaccion"]),
                            CEMACO_CLIENTE_NOMBRE = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteNombre"]),
                            CEMACO_CLIENTE_TELEFONO = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteTelefono"]),
                            CEMACO_CLIENTE_DIRECCION = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteDireccion"]),
                            CEMACO_CLIENTE_DEPARTAMENTO = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteDepartamento"]),
                            CEMACO_CLIENTE_MUNICIPIO = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteMunicipio"]),
                            CEMACO_CLIENTE_ZONA = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteZona"]),
                            CEMACO_CLIENTE_INDICACIONES = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteIndicaciones"]),
                            CEMACO_CLIENTE_SEGUNDO_PISO = Convert.ToChar(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteSegundoPiso"]),
                            CEMACO_CLIENTE_ENTRA_CAMION = Convert.ToChar(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteEntraCamion"]),
                            CEMACO_CLIENTE_ENTRA_PICKUP = Convert.ToChar(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteEntraPickup"])
                        };

                        db.MF_Pedido.InsertOnSubmit(iMF_Pedido);

                        var qUpdateConsecutivo =
                        from c in db.CONSECUTIVO_FAs
                        where c.CODIGO_CONSECUTIVO == "PEDIDO"
                        select c;
                        foreach (var item in qUpdateConsecutivo)
                        {
                            item.VALOR_CONSECUTIVO = mPedido;
                        }

                        mensaje = string.Format("El pedido {0} fue grabado y enviado exitosamente", mPedido);
                    }
                    else
                    {
                        mNuevo = false;
                        mPedido = Pedido;

                        string mEstadoPedido = (from p in db.PEDIDOs where p.PEDIDO1 == mPedido select p).First().ESTADO;

                        if (mEstadoPedido != "N")
                        {
                            mRetorna = false;
                            mensaje = string.Format("El Pedido {0} está facturado o cancelado, no es posible hacerle cambios.", mPedido);
                            return false;
                        }

                        var qUpdatePedido = from p in db.PEDIDOs where p.PEDIDO1 == mPedido select p;
                        foreach (var item in qUpdatePedido)
                        {
                            item.TARJETA_CREDITO = "";
                            item.DIRECCION_FACTURA = mDireccion;
                            item.OBSERVACIONES = ordenCompra;
                            item.TOTAL_MERCADERIA = mTotalMercaderia;
                            item.MONTO_ANTICIPO = 0;
                            item.MONTO_FLETE = 0;
                            item.MONTO_SEGURO = 0;
                            item.MONTO_DOCUMENTACIO = 0;
                            item.TIPO_DESCUENTO1 = "P";
                            item.TIPO_DESCUENTO2 = "P";
                            item.MONTO_DESCUENTO1 = 0;
                            item.MONTO_DESCUENTO2 = 0;
                            item.PORC_DESCUENTO1 = 0;
                            item.PORC_DESCUENTO2 = 0;
                            item.TOTAL_IMPUESTO1 = mImpuesto;
                            item.TOTAL_IMPUESTO2 = 0;
                            item.TOTAL_A_FACTURAR = mTotalFacturar;
                            item.PORC_COMI_VENDEDOR = 0;
                            item.PORC_COMI_COBRADOR = 0;
                            item.TOTAL_CANCELADO = 0;
                            item.TOTAL_UNIDADES = mUnidades;
                            item.IMPRESO = "N";
                            item.DESCUENTO_VOLUMEN = 0;
                            item.TIPO_PEDIDO = "N";
                            item.MONEDA_PEDIDO = "L";
                            item.VERSION_NP = 1;
                            item.AUTORIZADO = "N";
                            item.DOC_A_GENERAR = "F";
                            item.CLASE_PEDIDO = "N";
                            item.MONEDA = "L";
                            item.RUTA = "ND";
                            item.CONDICION_PAGO = "1";
                            item.VENDEDOR = mVendedor;
                            item.PAIS = "GUA";
                            item.TIPO_DOC_CXC = "FAC";
                            item.BACKORDER = "C";
                            item.PORC_INTCTE = 0;
                            item.DESCUENTO_CASCADA = "S";
                            item.FIJAR_TIPO_CAMBIO = "N";
                            item.ORIGEN_PEDIDO = "F";
                            item.DESC_DIREC_EMBARQUE = mDireccionEmbarque;
                            item.RecordDate = DateTime.Now;
                            item.UpdatedBy = string.Format("FA/{0}", usuario);
                        }

                        var qUpdateMFPedido = from p in db.MF_Pedido where p.PEDIDO == mPedido select p;
                        foreach (var item in qUpdateMFPedido)
                        {
                            item.TOTAL_FACTURAR = mTotalFacturar;
                            item.MONTO = mTotalFacturar;
                            item.RecordDate = DateTime.Now;
                            item.UpdatedBy = string.Format("FA/{0}", usuario);
                            item.USUARIO_MODIFICA = usuario;
                            item.FECHA_REGISTRO_MODIFICA = DateTime.Now;
                            item.NOTAS_TIPOVENTA = observaciones;
                            item.NOMBRE_AUTORIZACION = mOrdenCompra;
                            item.AUTORIZACION = ordenCompra;
                            item.FECHA_ENTREGA = Convert.ToDateTime(dsInfo.Tables["Pedido"].Rows[0]["CemacoFechaEntrega"]);
                            item.NOMBRE_RECIBE = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteNombre"]).Trim().Length > 0 ? Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteNombre"]) : mNombreCliente;
                            item.CEMACO_TIENDA = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoTienda"]);
                            item.CEMACO_FECHA_VENTA = Convert.ToDateTime(dsInfo.Tables["Pedido"].Rows[0]["CemacoFechaVenta"]);
                            item.CEMACO_CAJA = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoCaja"]);
                            item.CEMACO_TRANSACCION = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoTransaccion"]);
                            item.CEMACO_CLIENTE_NOMBRE = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteNombre"]);
                            item.CEMACO_CLIENTE_TELEFONO = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteTelefono"]);
                            item.CEMACO_CLIENTE_DIRECCION = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteDireccion"]);
                            item.CEMACO_CLIENTE_DEPARTAMENTO = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteDepartamento"]);
                            item.CEMACO_CLIENTE_MUNICIPIO = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteMunicipio"]);
                            item.CEMACO_CLIENTE_ZONA = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteZona"]);
                            item.CEMACO_CLIENTE_INDICACIONES = Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteIndicaciones"]);
                            item.CEMACO_CLIENTE_SEGUNDO_PISO = Convert.ToChar(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteSegundoPiso"]);
                            item.CEMACO_CLIENTE_ENTRA_CAMION = Convert.ToChar(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteEntraCamion"]);
                            item.CEMACO_CLIENTE_ENTRA_PICKUP = Convert.ToChar(dsInfo.Tables["Pedido"].Rows[0]["CemacoClienteEntraPickup"]);
                        }

                        mensaje = string.Format("El pedido {0} fue modificado y enviado exitosamente", mPedido);
                    }

                    try
                    {
                        if (Convert.ToString(dsInfo.Tables["Pedido"].Rows[0]["HayImagen"]) == "S")
                        {
                            var qPedidoImagenBorrar = from p in db.MF_PedidoImagens where p.PEDIDO == mPedido select p;
                            db.MF_PedidoImagens.DeleteAllOnSubmit(qPedidoImagenBorrar);

                            MF_PedidoImagen iPedidoImagen = new MF_PedidoImagen()
                            {
                                PEDIDO = mPedido,
                                CLIENTE = cliente,
                                IMAGEN = (Byte[])dsInfo.Tables["Pedido"].Rows[0]["CemacoImagen"],
                                RecordDate = DateTime.Now,
                                CreatedBy = string.Format("FA/{0}", usuario),
                                UpdatedBy = string.Format("FA/{0}", usuario),
                                CreateDate = DateTime.Now
                            };
                            db.MF_PedidoImagens.InsertOnSubmit(iPedidoImagen);
                        }
                    }
                    catch
                    {
                        //Nothing
                    }

                    var qPedidoBorrar = from pl in db.PEDIDO_LINEAs where pl.PEDIDO == mPedido select pl;
                    db.PEDIDO_LINEAs.DeleteAllOnSubmit(qPedidoBorrar);

                    var qMF_PedidoBorrar = from pl in db.MF_Pedido_Lineas where pl.PEDIDO == mPedido select pl;
                    db.MF_Pedido_Lineas.DeleteAllOnSubmit(qMF_PedidoBorrar);

                    var qMF_PedidoOriginalBorrar = from pl in db.MF_Pedido_Linea_Originals where pl.PEDIDO == mPedido select pl;
                    db.MF_Pedido_Linea_Originals.DeleteAllOnSubmit(qMF_PedidoOriginalBorrar);

                    for (int ii = 0; ii < mRows.Count(); ii++)
                    {
                        if (! (cliente == "MA0008" || cliente == "MA0018"))
                        {
                            PEDIDO_LINEA iPedidoLinea = new PEDIDO_LINEA()
                            {
                                PEDIDO = mPedido,
                                PEDIDO_LINEA1 = (short)(ii),
                                BODEGA = mBodega,
                                LOCALIZACION = "ARMADO",
                                ARTICULO = Convert.ToString(mRows[ii]["Articulo"]),
                                ESTADO = "N",
                                FECHA_ENTREGA = DateTime.Now.Date,
                                LINEA_USUARIO = (short)(ii),
                                PRECIO_UNITARIO = Math.Round(Convert.ToDecimal(mRows[ii]["PrecioBase"]), 2, MidpointRounding.AwayFromZero),
                                CANTIDAD_PEDIDA = Convert.ToInt32(mRows[ii]["Cantidad"]),
                                CANTIDAD_A_FACTURA = Convert.ToInt32(mRows[ii]["Cantidad"]),
                                CANTIDAD_FACTURADA = 0,
                                CANTIDAD_RESERVADA = 0,
                                CANTIDAD_BONIFICAD = 0,
                                CANTIDAD_CANCELADA = 0,
                                TIPO_DESCUENTO = "P",
                                MONTO_DESCUENTO = 0,
                                PORC_DESCUENTO = 0,
                                DESCRIPCION = Convert.ToString(mRows[ii]["Descripcion"]),
                                COMENTARIO = "",
                                FECHA_PROMETIDA = DateTime.Now,
                                NoteExistsFlag = 0,
                                RecordDate = DateTime.Now,
                                RowPointer = (System.Guid)db.fcNewID(),
                                CreatedBy = string.Format("FA/{0}", usuario),
                                UpdatedBy = string.Format("FA/{0}", usuario),
                                CreateDate = DateTime.Now,
                                CENTRO_COSTO = "2-001",
                                CUENTA_CONTABLE = "4-1-1-001-0001",
                                U_ARMADOR = "PENDIENTE ASIGNAR"
                            };

                            db.PEDIDO_LINEAs.InsertOnSubmit(iPedidoLinea);

                            MF_Pedido_Linea iMF_PedidoLinea = new MF_Pedido_Linea()
                            {
                                PEDIDO = mPedido,
                                PEDIDO_LINEA = (short)(ii),
                                ARTICULO = Convert.ToString(mRows[ii]["Articulo"]),
                                REQUISICION_ARMADO = Convert.ToChar("N"),
                                OFERTA = Convert.ToChar("N"),
                                TIPOVENTA = "NR",
                                FINANCIERA = 1,
                                NIVEL_PRECIO = "ContadoCheq",
                                PRECIOORIGINAL = 0,
                                PRECIO_UNITARIO = Math.Round(Convert.ToDecimal(mRows[ii]["PrecioLista"]), 2, MidpointRounding.AwayFromZero),
                                CANTIDAD_PEDIDA = Convert.ToInt32(mRows[ii]["Cantidad"]),
                                PRECIO_TOTAL = Math.Round(Convert.ToDecimal(mRows[ii]["PrecioLista"]) * Convert.ToInt32(mRows[ii]["Cantidad"]), 2, MidpointRounding.AwayFromZero),
                                FACTURADO = Convert.ToChar("N"),
                                DESPACHADO = Convert.ToChar("N"),
                                BODEGA = mBodega,
                                LOCALIZACION = "ARMADO",
                                PRECIO_UNITARIO_FACTURAR = Convert.ToDecimal(mRows[ii]["PrecioLista"]),
                                PRECIO_TOTAL_FACTURAR = Math.Round(Convert.ToDecimal(mRows[ii]["PrecioLista"]) * Convert.ToInt32(mRows[ii]["Cantidad"]), 2, MidpointRounding.AwayFromZero),
                                PRECIO_LISTA = Convert.ToDecimal(mRows[ii]["PrecioLista"]),
                                PRECIO_UNITARIO_DEBIO_FACTURAR = Convert.ToDecimal(mRows[ii]["PrecioLista"]),
                                DIFERENCIA_FACTURADA = 0,
                                RESPETO_PRECIO = Convert.ToChar("S"),
                                PRECIO_SUGERIDO = Convert.ToDecimal(mRows[ii]["PrecioLista"]),
                                PRECIO_BASE_LOCAL = Convert.ToDecimal(mRows[ii]["PrecioBase"]),
                                PORCENTAJE_DESCUENTO = 0,
                                TIPO_LINEA = Convert.ToChar("N"),
                                FECHAOFERTADESDE = new DateTime(1980, 1, 1),
                                TIPO_DESCUENTO = Convert.ToInt32(mRows[ii]["IdDescuento"])
                            };

                            db.MF_Pedido_Lineas.InsertOnSubmit(iMF_PedidoLinea);
                        }

                        Int32 mExistencias = 0;

                        try
                        {
                            mExistencias = Convert.ToInt32((from e in db.EXISTENCIA_BODEGAs where e.BODEGA == "F01" && e.ARTICULO == Convert.ToString(mRows[ii]["Articulo"]) select e).First().CANT_DISPONIBLE);
                        }
                        catch
                        {
                            mExistencias = 0;
                        }

                        string mColor = ""; string mStrong1 = ""; string mStrong2 = "";

                        if (mExistencias == 0 || mExistencias < Convert.ToInt32(mRows[ii]["Cantidad"]))
                        {
                            mColor = " color: #FF0000;";
                            mStrong1 = "<strong>";
                            mStrong2 = "</strong>";
                        }

                        mBody.Append(string.Format("<tr><td style='border: 1px solid #000000; font-size: x-small; text-align: left; height:30px;{1}'>&nbsp;{2}{0}{3}</td>", Convert.ToString(mRows[ii]["Articulo"]), mColor, mStrong1, mStrong2));
                        mBody.Append(string.Format("<td style='border: 1px solid #000000; font-size: xx-small; text-align: left; height:30px;{1}'>&nbsp;{2}{0}&nbsp;&nbsp;&nbsp;&nbsp;{3}</td>", Convert.ToString(mRows[ii]["Descripcion"]), mColor, mStrong1, mStrong2));
                        mBody.Append(string.Format("<td style='border: 1px solid #000000; font-size: x-small; text-align: right; height:30px;{1}'>{2}{0}&nbsp;{3}</td>", Convert.ToString(mRows[ii]["Cantidad"]), mColor, mStrong1, mStrong2));
                        if (cliente == "MA0017")
                        {
                            //mostrando el descuento para cada artículo
                            try
                            {
                                mBody.Append(string.Format("<td style='border: 1px solid #000000; font-size: x-small; text-align: right; height:30px;'>{0}&nbsp;</td>", (from d in db.MF_DescuentoMayoreos where d.IdDescuento == int.Parse(dtPedidoOriginal.Rows[ii]["IdDescuento"].ToString()) select d).First().Descuento.ToUpper()));
                            }
                            catch
                            {
                                mBody.Append(string.Format("<td style='border: 1px solid #000000; font-size: x-small; text-align: right; height:30px;'>{0}&nbsp;</td>", "--"));
                            }
                        }
                        mBody.Append(string.Format("<td style='border: 1px solid #000000; font-size: x-small; text-align: right; height:30px;{1}'>{2}{0}&nbsp;{3}</td></tr>", Convert.ToString(mExistencias), mColor, mStrong1, mStrong2));
                    }

                    for (int ii = 0; ii < dtPedidoOriginal.Rows.Count; ii++)
                    {
                        MF_Pedido_Linea_Original iPedidoLinea = new MF_Pedido_Linea_Original()
                        {
                            PEDIDO = mPedido,
                            PEDIDO_LINEA = (short)(ii),
                            ARTICULO = Convert.ToString(dtPedidoOriginal.Rows[ii]["Articulo"]),
                            CANTIDAD_PEDIDA = Convert.ToInt32(dtPedidoOriginal.Rows[ii]["Cantidad"]),
                            DESCRIPCION = Convert.ToString(dtPedidoOriginal.Rows[ii]["Descripcion"]),
                            PRECIO_UNITARIO = Math.Round(Convert.ToDecimal(dtPedidoOriginal.Rows[ii]["PrecioLista"]), 2, MidpointRounding.AwayFromZero),
                            PRECIO_TOTAL = Math.Round(Convert.ToDecimal(dtPedidoOriginal.Rows[ii]["PrecioLista"]) * Convert.ToInt32(dtPedidoOriginal.Rows[ii]["Cantidad"]), 2, MidpointRounding.AwayFromZero),
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("FA/{0}", usuario),
                            UpdatedBy = string.Format("FA/{0}", usuario),
                            CreateDate = DateTime.Now
                        };

                        db.MF_Pedido_Linea_Originals.InsertOnSubmit(iPedidoLinea);
                    }

                    if (cliente == "MA0008" || cliente == "MA0018")
                    {
                        for (int ii = 0; ii < dtPedidoOriginal.Rows.Count; ii++)
                        {
                            PEDIDO_LINEA iPedidoLinea = new PEDIDO_LINEA()
                            {
                                PEDIDO = mPedido,
                                PEDIDO_LINEA1 = (short)(ii),
                                BODEGA = mBodega,
                                LOCALIZACION = "ARMADO",
                                ARTICULO = Convert.ToString(dtPedidoOriginal.Rows[ii]["Articulo"]),
                                ESTADO = "N",
                                FECHA_ENTREGA = DateTime.Now.Date,
                                LINEA_USUARIO = (short)(ii),
                                PRECIO_UNITARIO = Math.Round(Convert.ToDecimal(dtPedidoOriginal.Rows[ii]["PrecioBase"]), 2, MidpointRounding.AwayFromZero),
                                CANTIDAD_PEDIDA = Convert.ToInt32(dtPedidoOriginal.Rows[ii]["Cantidad"]),
                                CANTIDAD_A_FACTURA = Convert.ToInt32(dtPedidoOriginal.Rows[ii]["Cantidad"]),
                                CANTIDAD_FACTURADA = 0,
                                CANTIDAD_RESERVADA = 0,
                                CANTIDAD_BONIFICAD = 0,
                                CANTIDAD_CANCELADA = 0,
                                TIPO_DESCUENTO = "P",
                                MONTO_DESCUENTO = 0,
                                PORC_DESCUENTO = 0,
                                DESCRIPCION = Convert.ToString(dtPedidoOriginal.Rows[ii]["Descripcion"]),
                                COMENTARIO = "",
                                FECHA_PROMETIDA = DateTime.Now,
                                NoteExistsFlag = 0,
                                RecordDate = DateTime.Now,
                                RowPointer = (System.Guid)db.fcNewID(),
                                CreatedBy = string.Format("FA/{0}", usuario),
                                UpdatedBy = string.Format("FA/{0}", usuario),
                                CreateDate = DateTime.Now,
                                CENTRO_COSTO = "2-001",
                                CUENTA_CONTABLE = "4-1-1-001-0001",
                                U_ARMADOR = "PENDIENTE ASIGNAR"
                            };

                            db.PEDIDO_LINEAs.InsertOnSubmit(iPedidoLinea);

                            MF_Pedido_Linea iMF_PedidoLinea = new MF_Pedido_Linea()
                            {
                                PEDIDO = mPedido,
                                PEDIDO_LINEA = (short)(ii),
                                ARTICULO = Convert.ToString(dtPedidoOriginal.Rows[ii]["Articulo"]),
                                REQUISICION_ARMADO = Convert.ToChar("N"),
                                OFERTA = Convert.ToChar("N"),
                                TIPOVENTA = "NR",
                                FINANCIERA = 1,
                                NIVEL_PRECIO = "ContadoCheq",
                                PRECIOORIGINAL = 0,
                                PRECIO_UNITARIO = Math.Round(Convert.ToDecimal(dtPedidoOriginal.Rows[ii]["PrecioLista"]), 2, MidpointRounding.AwayFromZero),
                                CANTIDAD_PEDIDA = Convert.ToInt32(dtPedidoOriginal.Rows[ii]["Cantidad"]),
                                PRECIO_TOTAL = Math.Round(Convert.ToDecimal(dtPedidoOriginal.Rows[ii]["PrecioLista"]) * Convert.ToInt32(dtPedidoOriginal.Rows[ii]["Cantidad"]), 2, MidpointRounding.AwayFromZero),
                                FACTURADO = Convert.ToChar("N"),
                                DESPACHADO = Convert.ToChar("N"),
                                BODEGA = mBodega,
                                LOCALIZACION = "ARMADO",
                                PRECIO_UNITARIO_FACTURAR = Convert.ToDecimal(dtPedidoOriginal.Rows[ii]["PrecioLista"]),
                                PRECIO_TOTAL_FACTURAR = Math.Round(Convert.ToDecimal(dtPedidoOriginal.Rows[ii]["PrecioLista"]) * Convert.ToInt32(dtPedidoOriginal.Rows[ii]["Cantidad"]), 2, MidpointRounding.AwayFromZero),
                                PRECIO_LISTA = Convert.ToDecimal(dtPedidoOriginal.Rows[ii]["PrecioLista"]),
                                PRECIO_UNITARIO_DEBIO_FACTURAR = Convert.ToDecimal(dtPedidoOriginal.Rows[ii]["PrecioLista"]),
                                DIFERENCIA_FACTURADA = 0,
                                RESPETO_PRECIO = Convert.ToChar("S"),
                                PRECIO_SUGERIDO = Convert.ToDecimal(dtPedidoOriginal.Rows[ii]["PrecioLista"]),
                                PRECIO_BASE_LOCAL = Convert.ToDecimal(dtPedidoOriginal.Rows[ii]["PrecioBase"]),
                                PORCENTAJE_DESCUENTO = 0,
                                TIPO_LINEA = Convert.ToChar("N"),
                                FECHAOFERTADESDE = new DateTime(1980, 1, 1)
                            };

                            db.MF_Pedido_Lineas.InsertOnSubmit(iMF_PedidoLinea);
                        }
                    }
                }
                db.SubmitChanges();
                transactionScope.Complete();
            }
            //por tema de validación de patas de las camas
            //esta variable por default es verdadera, solo cambia a falso en 
            //la parte de las patas de las camas cuando hay algún error en la cantidad
            // de patas solicitadas.
            if (mRetorna)
            {
                mensaje = mMensaje2;
                string mMensaje = "";

                EnviarCorreo(ref mMensaje, dtPedidoOriginal, mNombreCliente, mNombreUsuario, mPedido, observaciones, mMailUsuario, dtUsuariosEnviar, dtUsuariosEnviarBodega, mBody, mOrdenCompra, usuario, mNuevo, cliente);
            }

        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }
            mRetorna = false;
            mensaje = string.Format("Error al grabar el pedido WS. {0} {1}", ex.Message, m);
        }

        return mRetorna;
    }

    public static bool EnviarCorreo(ref string mensaje, DataTable dtPedidoOriginal, string NombreCliente, string NombreUsuario, string NumeroPedido, string Observaciones, string MailUsuario, DataTable dtUsuariosEnviar, DataTable dtUsuariosEnviarBodega, StringBuilder BodyAgrupado, string ordenCompra, string usuario, bool nuevo, string cliente)
    {
        dbDataContext db = new dbDataContext();

        string mAmbiente = "DES";

        if (db.Connection.ConnectionString.Contains("sql.fiesta.local")) mAmbiente = "PRO";
        if (db.Connection.ConnectionString.Contains("PRUEBAS")) mAmbiente = "PRU";

        try
        {
            string mAsunto = "";
            string mNombreCliente = "";
            string mInfoCliente = "";
            bool mAgregarJefeArmados = false;
            string cCliente = string.Empty;
            string mAsunto1 = string.Empty;
            try
            {
                var qCliente = (from p in db.MF_Pedido where p.PEDIDO == NumeroPedido select p);
                mNombreCliente = qCliente.FirstOrDefault().CEMACO_CLIENTE_NOMBRE;
                cCliente = qCliente.First().CLIENTE;

                if (mNombreCliente == null)
                    mNombreCliente = "";
            }
            catch
            {
                mNombreCliente = "";
                cCliente = string.Empty;
            }

            if (mNombreCliente.Trim().Length > 0)
            {
                string mCemacoTienda = (from p in db.MF_Pedido where p.PEDIDO == NumeroPedido select p).First().CEMACO_TIENDA;
                string mCaja = (from p in db.MF_Pedido where p.PEDIDO == NumeroPedido select p).First().CEMACO_CAJA;
                string mTransaccion = (from p in db.MF_Pedido where p.PEDIDO == NumeroPedido select p).First().CEMACO_TRANSACCION;
                string mTelefono = (from p in db.MF_Pedido where p.PEDIDO == NumeroPedido select p).First().CEMACO_CLIENTE_TELEFONO;
                string mDireccion = (from p in db.MF_Pedido where p.PEDIDO == NumeroPedido select p).First().CEMACO_CLIENTE_DIRECCION;
                string mDepartamento = (from p in db.MF_Pedido where p.PEDIDO == NumeroPedido select p).First().CEMACO_CLIENTE_DEPARTAMENTO;
                string mMunicipio = (from p in db.MF_Pedido where p.PEDIDO == NumeroPedido select p).First().CEMACO_CLIENTE_MUNICIPIO;
                string mVendedorComision = (from p in db.PEDIDOs join v in db.MF_Vendedors on p.VENDEDOR equals v.VENDEDOR where p.PEDIDO1 == NumeroPedido select new { vendedor = v.NOMBRE + ' ' + v.APELLIDO }).First().vendedor;

                if ((from a in db.MF_Pedido_Lineas where a.PEDIDO == NumeroPedido && a.ARTICULO.Substring(0, 2) == "51" select a).Count() > 0)
                    mAgregarJefeArmados = true;
                int contadorArmados = 0;
                (from l in db.MF_Pedido_Lineas
                 where l.PEDIDO == NumeroPedido
                 select l).ToList().ForEach(x =>
                 {
                     if (Validar.RequiereArmado(x.ARTICULO))
                         contadorArmados++;
                 });
                mAgregarJefeArmados = (contadorArmados > 0);

                DateTime mFechaEntrega = Convert.ToDateTime((from p in db.MF_Pedido where p.PEDIDO == NumeroPedido select p).First().FECHA_ENTREGA);
                string mFechaEntreString = string.Format("{0}{1}/{2}{3}/{4}", mFechaEntrega.Day < 10 ? "0" : "", mFechaEntrega.Day.ToString(), mFechaEntrega.Month < 10 ? "0" : "", mFechaEntrega.Month.ToString(), mFechaEntrega.Year.ToString());

                mAsunto = string.Format(" - {0}", mNombreCliente);
                mInfoCliente = string.Format("<br /><strong>Tienda: </strong>{8}<br /><strong>Fecha de entrega: </strong>{0}<br /><strong>Nombre del cliente: </strong>{1}<br /><strong>Caja: </strong>{2}<br /><strong>Transacción: </strong>{3}<br /><strong>Teléfono: </strong>{4}<br /><strong>Dirección: </strong>{5}<br /><strong>Departamento: </strong>{6}<br /><strong>Municipio: </strong>{7}<br />", mFechaEntreString, mNombreCliente, mCaja, mTransaccion, mTelefono, mDireccion, mDepartamento, mMunicipio, mCemacoTienda);
                mInfoCliente += string.Format("<strong>Vendedor:&nbsp;</strong>{0}<br/><br/>", mVendedorComision);

            }

            List<string> mCuentaCorreo = new List<string>();
            if (mAmbiente == "PRO")
            {
                string mRemitente = string.Empty;
                mCuentaCorreo.Add(MailUsuario);

                try
                {
                    //clientes que tienen despacho desde F01 hacia sus propios clientes
                    List<string> lstClientesEsp = DevuelveClientesEspeciales();
                    foreach (var item in lstClientesEsp)
                    {
                        if (cCliente.Length > 0 && item.Equals(cCliente))
                        {
                            var CorreoCliente = (from c in db.MF_Catalogos where c.CODIGO_TABLA.Equals(45) && c.CODIGO_CAT.Equals(cCliente) select c).FirstOrDefault().NOMBRE_CAT;
                            mCuentaCorreo.Add(CorreoCliente);
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error al devolver los correos para los clientes especiales mayoreo " + ex.Message);
                }


                if (mAgregarJefeArmados)
                    mCuentaCorreo.Add((from c in db.MF_Configura select c).First().mailJefeArmados);



            }

            //mail.IsBodyHtml = true;
            //mail.From = new MailAddress("puntodeventa2@productosmultiples.com", "Punto de Venta Mayoreo");
            mAsunto1 = string.Format("{0} generó el Pedido No. {1}{2}", NombreCliente, NumeroPedido, mAsunto);

            string mLineaLink = "";
            string mLinea1 = string.Format("<p style='color: #428BCA'>{0} de {1} generó el pedido No. {2} con los siguientes artículos:</p>", NombreUsuario, NombreCliente, NumeroPedido);

            if (!nuevo)
            {
                mAsunto1 = string.Format("{0} MODIFICÓ el Pedido No. {1}{2}", NombreCliente, NumeroPedido, mAsunto);
                mLinea1 = string.Format("<p style='color: #428BCA'>{0} de {1} MODIFICÓ el pedido No. {2} con los siguientes artículos:</p>", NombreUsuario, NombreCliente, NumeroPedido);
            }

            string mLink = string.Format("http://localhost:2643/login.aspx?pe={0}&amb=DES", NumeroPedido);
            if (mAmbiente == "PRO")
                mLink = string.Format("https://extranet.productosmultiples.com/mayoreo/login.aspx?pe={0}&amb=PRO", NumeroPedido);

            mLineaLink = string.Format("<p style='color: #428BCA'>Para modificar el pedido, haga clic <a href='{0}' tabindex='0'>aquí</a>.</p>", mLink);

            StringBuilder mBody1 = new StringBuilder();
            StringBuilder mBody2 = new StringBuilder();
            StringBuilder mBody2Usuario = new StringBuilder();
            StringBuilder mBody3 = new StringBuilder();
            StringBuilder mBodyInicioTabla = new StringBuilder();
            StringBuilder mBodyInicioTablaSets = new StringBuilder();
            StringBuilder mBodyInicioTablaUsuario = new StringBuilder();
            StringBuilder mBodyFinTabla = new StringBuilder();

            mBody1.Append("<!DOCTYPE html><html lang='en-us'>");
            mBody1.Append("<head></head>");
            mBody1.Append("<body>");
            mBody1.Append(mLinea1);
            if (mLineaLink.Trim().Length > 0) mBody1.Append(mLineaLink);
            mBodyInicioTabla.Append(mInfoCliente);
            mBodyInicioTablaUsuario.Append(mInfoCliente);
            mBodyInicioTabla.Append("<table align='center' style='border: thin solid #000000;'");
            mBodyInicioTablaUsuario.Append("<table align='center' style='border: thin solid #000000;'");

            mBodyInicioTabla.Append("<tr><td style='border: 1px solid #000000; color: #FFFFFF; background-color: #428BCA; font-size: small; font-weight: bold;'>&nbsp;Artículo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>");
            mBodyInicioTabla.Append("<td style='border: 1px solid #000000; color: #FFFFFF; background-color: #428BCA; font-size: small; font-weight: bold;'>&nbsp;Descripción</td>");
            mBodyInicioTabla.Append("<td style='border: 1px solid #000000; color: #FFFFFF; background-color: #428BCA; font-size: small; font-weight: bold;'>&nbsp;&nbsp;Cantidad&nbsp;</td>");
            if (cliente == "MA0017")
                mBodyInicioTabla.Append("<td style='border: 1px solid #000000; color: #FFFFFF; background-color: #428BCA; font-size: small; font-weight: bold;'>&nbsp;&nbsp;Tipo de Venta&nbsp;</td>");

            mBodyInicioTablaSets.Append(mBodyInicioTabla.ToString());
            mBodyInicioTablaSets.Append("</tr>");

            mBodyInicioTabla.Append("<td style='border: 1px solid #000000; color: #FFFFFF; background-color: #428BCA; font-size: small; font-weight: bold;'>&nbsp;&nbsp;Existencias&nbsp;</td></tr>");

            mBodyInicioTablaUsuario.Append("<tr><td style='border: 1px solid #000000; color: #FFFFFF; background-color: #428BCA; font-size: small; font-weight: bold;'>&nbsp;Descripción</td>");
            mBodyInicioTablaUsuario.Append("<td style='border: 1px solid #000000; color: #FFFFFF; background-color: #428BCA; font-size: small; font-weight: bold;'>&nbsp;&nbsp;Cantidad&nbsp;</td>");
            if (cliente == "MA0017")
                mBodyInicioTablaUsuario.Append("<td style='border: 1px solid #000000; color: #FFFFFF; background-color: #428BCA; font-size: small; font-weight: bold;'>&nbsp;&nbsp;Tipo de Venta&nbsp;</td></tr>");

            bool mHaySET = false;
            for (int ii = 0; ii < dtPedidoOriginal.Rows.Count; ii++)
            {
                if (Convert.ToInt32(dtPedidoOriginal.Rows[ii]["Cantidad"]) > 0)
                {
                    mBody2.Append(string.Format("<tr><td style='border: 1px solid #000000; font-size: x-small; text-align: left; height:30px;'>&nbsp;{0}</td>", Convert.ToString(dtPedidoOriginal.Rows[ii]["Articulo"])));
                    mBody2.Append(string.Format("<td style='border: 1px solid #000000; font-size: xx-small; text-align: left; height:30px;'>&nbsp;{0}&nbsp;&nbsp;&nbsp;&nbsp;</td>", Convert.ToString(dtPedidoOriginal.Rows[ii]["Descripcion"])));
                    mBody2.Append(string.Format("<td style='border: 1px solid #000000; font-size: x-small; text-align: right; height:30px;'>{0}&nbsp;</td>", Convert.ToString(dtPedidoOriginal.Rows[ii]["Cantidad"])));
                    if (cliente == "MA0017")
                    {
                        //mostrando el descuento para cada artículo
                        try
                        {
                            mBody2.Append(string.Format("<td style='border: 1px solid #000000; font-size: x-small; text-align: right; height:30px;'>{0}&nbsp;</td></tr>", (from d in db.MF_DescuentoMayoreos where d.IdDescuento == int.Parse(dtPedidoOriginal.Rows[ii]["TipoDescuento"].ToString()) select d).First().Descuento.ToUpper()));
                        }
                        catch
                        {
                            mBody2.Append(string.Format("<td style='border: 1px solid #000000; font-size: x-small; text-align: right; height:30px;'>{0}&nbsp;</td></tr>", "--"));
                        }
                    }
                    mBody2Usuario.Append(string.Format("<tr><td style='border: 1px solid #000000; font-size: xx-small; text-align: left; height:30px;'>&nbsp;{0}&nbsp;&nbsp;&nbsp;&nbsp;</td>", Convert.ToString(dtPedidoOriginal.Rows[ii]["Descripcion"])));
                    mBody2Usuario.Append(string.Format("<td style='border: 1px solid #000000; font-size: x-small; text-align: right; height:30px;'>{0}&nbsp;</td>", Convert.ToString(dtPedidoOriginal.Rows[ii]["Cantidad"])));
                    if (cliente == "MA0017")
                    {
                        //mostrando el descuento para cada artículo
                        try
                        {
                            mBody2Usuario.Append(string.Format("<td style='border: 1px solid #000000; font-size: x-small; text-align: right; height:30px;'>{0}&nbsp;</td></tr>", (from d in db.MF_DescuentoMayoreos where d.IdDescuento == int.Parse(dtPedidoOriginal.Rows[ii]["TipoDescuento"].ToString()) select d).First().Descuento.ToUpper()));
                        }
                        catch
                        {
                            mBody2Usuario.Append(string.Format("<td style='border: 1px solid #000000; font-size: x-small; text-align: right; height:30px;'>{0}&nbsp;</td></tr>", "--"));
                        }
                    }
                    if (Convert.ToString(dtPedidoOriginal.Rows[ii]["Descripcion"]).Contains("SET")) mHaySET = true;
                }
            }

            string mOrdenCompra = ordenCompra;
            if (!mOrdenCompra.ToLower().Contains("orden")) mOrdenCompra = string.Format("Orden de compra No. {0}", ordenCompra);

            mBodyFinTabla.Append("</table>");
            mBody3.Append("<br />");
            mBody1.Append("<br />");
            mBody3.Append(string.Format("<p style='color: #428BCA'>Observaciones: {0}</p>", Observaciones));
            if (mOrdenCompra.Trim().Length > 0) mBody3.Append(string.Format("<p style='color: #428BCA'>{0}</p>", mOrdenCompra));
            mBody3.Append("<p style='color: #428BCA'>Punto de Venta Mayoreo - Productos Múltiples, S.A.</p>");
            mBody3.Append("</body>");
            mBody3.Append("</html>");

            StringBuilder mBodyUsuario = new StringBuilder();
            StringBuilder mBodyBodega = new StringBuilder();
            StringBuilder mBodyAdministracion = new StringBuilder();

            mBodyUsuario.Append(mBody1);
            mBodyUsuario.Append(mBodyInicioTablaUsuario);
            mBodyUsuario.Append(mBody2Usuario);
            mBodyUsuario.Append(mBodyFinTabla);
            mBodyUsuario.Append(mBody3);

            mBodyBodega.Append(mBody1);
            mBodyBodega.Append(mBodyInicioTabla);
            mBodyBodega.Append(BodyAgrupado);
            mBodyBodega.Append(mBodyFinTabla);
            mBodyBodega.Append(mBody3);

            mBodyAdministracion.Append(mBody1);
            mBodyAdministracion.Append(mBodyInicioTabla);
            mBodyAdministracion.Append(BodyAgrupado);
            mBodyAdministracion.Append(mBodyFinTabla);
            mBodyAdministracion.Append("<p>");

            if (mHaySET)
            {
                mBodyAdministracion.Append(string.Format("<p style='color: #428BCA'>El usuario de {0} ingresó el pedido de la siguiente forma:</p>", NombreCliente));
                mBodyAdministracion.Append(mBodyInicioTablaSets);
                mBodyAdministracion.Append(mBody2);
                mBodyAdministracion.Append(mBodyFinTabla);
            }

            mBodyAdministracion.Append(mBody3);
            ////----------------------------------------------------------------------------------
            ///Se coloca un parámetro para enviarlo por medio del servidor de gmail
            ///debido a que el link generado para editar este pedido, se ve afectado
            ///// por exchange cuando se envia por medio de este servidor
            ///lo que hace necesario especificar que este correo sea enviado por gmail unicamente.
            ///-------------------------------------------------------------------------------------
            Mail correo = new Mail();
            correo.EnviarEmail(WebConfigurationManager.AppSettings["UsrMail"].ToString(), mCuentaCorreo, mAsunto1, mBodyUsuario, "Punto de Venta Mayoreo", null, null, null, Const.SERVIDOR_CORREO_GMAIL);

           

            //Correo a Bodega
            mCuentaCorreo = new List<string>();
            //mail.Body = mBodyBodega.ToString();

            for (int ii = 0; ii < dtUsuariosEnviarBodega.Rows.Count; ii++)
            {
                mCuentaCorreo.Add(Convert.ToString(dtUsuariosEnviarBodega.Rows[ii]["email"]));
            }

            correo.EnviarEmail(WebConfigurationManager.AppSettings["UsrMail"].ToString(), mCuentaCorreo, mAsunto1, mBodyBodega, "Punto de Venta Mayoreo", null, null, null, Const.SERVIDOR_CORREO_GMAIL);
            #region "Código Anterior"
            //try
            //{
            //    smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail"], WebConfigurationManager.AppSettings["PwdMail"]);
            //    smtp.Send(mail);
            //}
            //catch (Exception ex)
            //{
            //    mensaje = string.Format("Error al enviar el correo al usuario {0}", ex.Message);
            //    return false;
            //}
            #endregion

            //Correo a Administración
            mCuentaCorreo = new List<string>();
            //mail.Body = mBodyAdministracion.ToString();

            for (int ii = 0; ii < dtUsuariosEnviar.Rows.Count; ii++)
            {
                mCuentaCorreo.Add(Convert.ToString(dtUsuariosEnviar.Rows[ii]["email"]));
            }

            correo.EnviarEmail(WebConfigurationManager.AppSettings["UsrMail"].ToString(), mCuentaCorreo, mAsunto1, mBodyAdministracion, "Punto de Venta Mayoreo", null, null, null, Const.SERVIDOR_CORREO_GMAIL);
            #region "Código Anterior"
            //try
            //{
            //    smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail"], WebConfigurationManager.AppSettings["PwdMail"]);
            //    smtp.Send(mail);
            //}
            //catch (Exception ex)
            //{
            //    mensaje = string.Format("Error al enviar el correo al usuario {0}", ex.Message);
            //    return false;
            //}
            #endregion
            mensaje = "Pedido enviado exitosamente";
        }
        catch (Exception ex)
        {
            mensaje = string.Format("Error al enviar el correo {0}", ex.Message);
            return false;
        }

        return true;
    }

    public static string DevuelveCorreoUsuario(string usuario)
    {
        string mRetorna;
        dbDataContext db = new dbDataContext();

        try
        {
            mRetorna = (from u in db.MF_UsuarioMayoreos where u.USUARIO == usuario select u).First().E_MAIL;

            string[] mInfo = mRetorna.ToString().Split(new string[] { "@" }, StringSplitOptions.None);
            mRetorna = string.Format("{0}***********************@{1}", mInfo[0].Substring(0, 3), mInfo[1]);
        }
        catch
        {
            mRetorna = "Usuario inválido";
        }

        return mRetorna;
    }
    public static bool HayArticulosDespachoCentral(string cliente,List<string> lstArticulos)
    {

        if (!TodoDesdeCentral(cliente))
        {
            try
            {
                
                dbDataContext db = new dbDataContext();
                var qArticuloDespacho = db.MF_Catalogos.Where(x => x.CODIGO_TABLA.Equals(61) && lstArticulos.Contains(x.CODIGO_CAT) && x.ACTIVO.Equals("S"));
                if (qArticuloDespacho.Count() > 0)
                    return true;
                else
                    return false;
            }
            catch(Exception ex)
            {
                log.Error("Error al validar los articulos despachados desde F01 " + ex.Message);
                return false;
            }
        }
        else 
        {
            log.Info("Cliente Mayoreo: " + cliente + " Todo desde central");
            return true;
        }
            
    
            }

    public static  bool TodoDesdeCentral(string Cliente)
    {
          try
        {
            dbDataContext db = new dbDataContext();
            return  db.MF_SeriesDespachos.Where(x => x.Cliente.Equals(Cliente) && x.TodoDesdeF01 !=null).FirstOrDefault().TodoDesdeF01.Equals("S");
        }
        catch
        { }
        return false;

    }
    //Devuelve los clientes que tienen despacho desde F01 hacia sus propios clientes por ejemplo CEMACO
    public static List<string> DevuelveClientesEspeciales()
    {
        dbDataContext db = new dbDataContext();
        List<string> lst = new List<string>();
        var qClientes = (
                from cl in db.MF_Catalogos
                where cl.CODIGO_TABLA.Equals(44)
                select cl);
            

        if (qClientes.Count() > 0)
        {

            foreach (var item in qClientes)
                lst.Add(
                   item.NOMBRE_CAT
                    );
        }
        return lst;
    }

    public static bool CambiaPassword(string usuario, string password1, string password2, ref string mensaje)
    {
        bool mRetorna = true;
        dbDataContext db = new dbDataContext();

        try
        {
            if (password1 != password2)
            {
                mensaje = "Las contraseñas no coinciden";
                return false;
            }

            string mAmbiente = "DES";

            if (db.Connection.ConnectionString.Contains("sql.fiesta.local")) mAmbiente = "PRO";
            if (db.Connection.ConnectionString.Contains("PRUEBAS")) mAmbiente = "PRU";

            string mServidor = "(local)";
            string mBase = "EXACTUSERP";

            if (mAmbiente == "PRO")
            {
                mBase = "EXACTUSERP";
                mServidor = "sql.fiesta.local";
            }

            if (mAmbiente == "PRU")
            {
                mBase = "PRUEBAS";
                mServidor = "sql.fiesta.local";
            }

            string mStringConexion = string.Format("Data Source={0}; Initial Catalog = {1};User id = mf.fiestanet;password = 54321; Max Pool Size=600;Pooling=false;Application Name=PDV", mServidor, mBase);

            SqlCommand mCommand = new SqlCommand(); SqlCommand mCommand2 = new SqlCommand(); SqlTransaction mTransaction = null;

            using (SqlConnection conexion = new SqlConnection(mStringConexion))
            {
                try
                {
                    conexion.Open();
                    mTransaction = conexion.BeginTransaction();

                    mCommand.Connection = conexion;
                    mCommand.Transaction = mTransaction;

                    using (SqlConnection conexion2 = new SqlConnection(mStringConexion))
                    {
                        conexion2.Open();
                        mCommand2.Connection = conexion2;

                        mCommand2.CommandText = string.Format("EXEC sp_password null, '{0}', '{1}'", password1, usuario);
                        mCommand2.ExecuteNonQuery();

                        conexion2.Close();
                    }

                    mCommand.CommandText = string.Format("UPDATE prodmult.MF_UsuarioMayoreo SET CAMBIA_PASSWORD = 'N' WHERE USUARIO = '{0}'", usuario);
                    mCommand.ExecuteNonQuery();

                    mTransaction.Commit();
                    conexion.Close();
                }
                catch (Exception ex)
                {
                    mensaje = ex.Message;
                    mTransaction.Rollback();
                    if (conexion.State == System.Data.ConnectionState.Open) conexion.Close();
                    return false;
                }
                finally
                {
                    if (conexion.State == System.Data.ConnectionState.Open) conexion.Close();
                }
            }

            mensaje = "La constraseña fue cambiada exitosamente.";
        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }
            mRetorna = false;
            mensaje = string.Format("Error al cambiar la contraseña WS. {0} {1}", ex.Message, m);
        }

        return mRetorna;
    }

    public static bool ResetPassword(string usuario, ref string mensaje)
    {
        bool mRetorna = true;
        dbDataContext db = new dbDataContext();

        try
        {
            string mAmbiente = "DES";

            if (db.Connection.ConnectionString.Contains("sql.fiesta.local")) mAmbiente = "PRO";
            if (db.Connection.ConnectionString.Contains("PRUEBAS")) mAmbiente = "PRU";

            string mServidor = "(local)";
            string mBase = "EXACTUSERP";

            if (mAmbiente == "PRO")
            {
                mBase = "EXACTUSERP";
                mServidor = "sql.fiesta.local";
            }

            if (mAmbiente == "PRU")
            {
                mBase = "PRUEBAS";
                mServidor = "sql.fiesta.local";
            }

            string mMailUsuario = ""; string mNombre = ""; Int32 mCuantos = 0; string mPassword = "";
            string mStringConexion = string.Format("Data Source={0}; Initial Catalog = {1};User id = mf.fiestanet;password = 54321; Max Pool Size=600;Pooling=false;Application Name=PDV", mServidor, mBase);

            SqlCommand mCommand = new SqlCommand(); SqlCommand mCommand2 = new SqlCommand(); SqlTransaction mTransaction = null;

            using (SqlConnection conexion = new SqlConnection(mStringConexion))
            {
                try
                {
                    conexion.Open();
                    mTransaction = conexion.BeginTransaction();

                    mCommand.Connection = conexion;
                    mCommand.Transaction = mTransaction;

                    mCommand.CommandText = string.Format("SELECT COUNT(*) FROM prodmult.MF_UsuarioMayoreo WHERE USUARIO = '{0}'", usuario);
                    mCuantos = Convert.ToInt32(mCommand.ExecuteScalar());

                    if (mCuantos == 0)
                    {
                        mRetorna = false;
                        mensaje = "El usuario ingresado es inválido";
                    }
                    else
                    {
                        mPassword = System.Web.Security.Membership.GeneratePassword(10, 2);
                        mPassword = mPassword.Replace(";", "1");

                        using (SqlConnection conexion2 = new SqlConnection(mStringConexion))
                        {
                            conexion2.Open();
                            mCommand2.Connection = conexion2;

                            mCommand2.CommandText = string.Format("EXEC sp_password null, '{0}', '{1}'", mPassword, usuario);
                            mCommand2.ExecuteNonQuery();

                            conexion2.Close();
                        }

                        mCommand.CommandText = string.Format("SELECT E_MAIL FROM prodmult.MF_UsuarioMayoreo WHERE USUARIO = '{0}'", usuario);
                        mMailUsuario = mCommand.ExecuteScalar().ToString();

                        mCommand.CommandText = string.Format("SELECT NOMBRE FROM prodmult.MF_UsuarioMayoreo WHERE USUARIO = '{0}'", usuario);
                        mNombre = mCommand.ExecuteScalar().ToString();

                        mCommand.CommandText = string.Format("UPDATE prodmult.MF_UsuarioMayoreo SET CAMBIA_PASSWORD = 'S' WHERE USUARIO = '{0}'", usuario);
                        mCommand.ExecuteNonQuery();
                    }

                    mTransaction.Commit();
                    conexion.Close();
                }
                catch (Exception ex)
                {
                    mensaje = ex.Message;
                    mTransaction.Rollback();
                    if (conexion.State == System.Data.ConnectionState.Open) conexion.Close();
                    return false;
                }
                finally
                {
                    if (conexion.State == System.Data.ConnectionState.Open) conexion.Close();
                }
            }

            //Enviar correo
            List<string> mCuentaCorreo = new List<string>();
            mCuentaCorreo.Add(mMailUsuario);

            StringBuilder mBody = new StringBuilder();

            mBody.Append("<!DOCTYPE html><html lang='en-us'>");
            mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } .style1 { font-size: xx-small; text-align: left; height:30px; } .style2 { font-size: x-small; text-align: left; height:30px; } .style3 { font-size: x-small; text-align: right; height:30px; } .style4 { border: 1px solid #8C4510; } .style5 { font-size: x-small; font-weight: bold; color: #FFFFFF; background-color: #428BCA; text-align: left; height:35px; } </style></head>");
            mBody.Append("<body>");
            mBody.Append(string.Format("<p style='color: #428BCA'>{0}, se le informa que el cambio de contraseña fue exitoso, es necesario que ingrese de nuevo al sistema utilizando la siguiente contraseña:</p>", mNombre));
            mBody.Append("<br />");
            mBody.Append(string.Format("<label style='color: #428BCA'>{0}</label>", mPassword));
            mBody.Append("<br />");
            mBody.Append("<br />");
            mBody.Append("<label style='color: #428BCA'>Punto de Venta Mayoreo - Productos Múltiples, S.A.</label>");
            mBody.Append("</body>");
            mBody.Append("</html>");

            #region "Código Anterior"
            //smtpclient smtp = new smtpclient();
            //mailmessage mail = new mailmessage();

            //mail.To.Add(mMailUsuario);

            //mail.IsBodyHtml = true;
            //mail.From = new MailAddress("puntodeventa2@productosmultiples.com", "Punto de Venta Mayoreo");
            //mail.Subject = string.Format("Cambio de contraseña");

            //StringBuilder mBody = new StringBuilder();

            //mBody.Append("<!DOCTYPE html><html lang='en-us'>");
            //mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } .style1 { font-size: xx-small; text-align: left; height:30px; } .style2 { font-size: x-small; text-align: left; height:30px; } .style3 { font-size: x-small; text-align: right; height:30px; } .style4 { border: 1px solid #8C4510; } .style5 { font-size: x-small; font-weight: bold; color: #FFFFFF; background-color: #428BCA; text-align: left; height:35px; } </style></head>");
            //mBody.Append("<body>");
            //mBody.Append(string.Format("<p style='color: #428BCA'>{0}, se le informa que el cambio de contraseña fue exitoso, es necesario que ingrese de nuevo al sistema utilizando la siguiente contraseña:</p>", mNombre));
            //mBody.Append("<br />");
            //mBody.Append(string.Format("<label style='color: #428BCA'>{0}</label>", mPassword));
            //mBody.Append("<br />");
            //mBody.Append("<br />");
            //mBody.Append("<label style='color: #428BCA'>Punto de Venta Mayoreo - Productos Múltiples, S.A.</label>");
            //mBody.Append("</body>");
            //mBody.Append("</html>");

            //scrumm
            //smtp.Host = WebConfigurationManager.AppSettings["MailHost"];
            //smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort"]);
            //smtp.EnableSsl = true;
            //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

            //mail.Body = mBody.ToString();
            //try
            //{
            //    smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail"], WebConfigurationManager.AppSettings["PwdMail"]);
            //    smtp.Send(mail);
            //}
            //catch (Exception ex)
            //{
            //    mensaje = string.Format("Error al enviar el correo al usuario {0}", ex.Message);
            //    return false;
            //}
            #endregion
            Mail mail = new Mail();
            mail.EnviarEmail(WebConfigurationManager.AppSettings["UsrMail"].ToString(), mCuentaCorreo, "Cambio de contraseña", mBody, "Punto de Venta Mayoreo");

            mensaje = "Favor revisar su correo electrónico para restablecer la contraseña.";
        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }
            mRetorna = false;
            mensaje = string.Format("Error al resetear la contraseña WS. {0} {1}", ex.Message, m);
        }

        return mRetorna;
    }
    /// <summary>
    /// Para obtener los usuarios ACTIVOS según el cliente
    /// para seleccionarlos al crear un pedido
    /// </summary>
    /// <param name="cliente"></param>
    /// <returns></returns>
    public static List<UsuariosMayoreo> ObtenerUsuariosMayoreo(string cliente)
    {
        dbDataContext db = new dbDataContext();
        List<UsuariosMayoreo> lst = new List<UsuariosMayoreo>();
        var qUsuarios = (from ma in db.MF_UsuarioMayoreos
                         join u in db.USUARIOs on new { USUARIO = (string)ma.USUARIO } equals new { USUARIO = u.USUARIO1 }
                         join v in db.MF_Vendedors on new { CELULAR = u.CELULAR } equals new { CELULAR = v.VENDEDOR }
                         where
                           ma.CLIENTE == cliente &&
                           ma.ACTIVO.Equals("S") &&
                           //no incluir a los usuarios administrativos
                           (!db.MF_Catalogos.Any(admin => admin.CODIGO_CAT == v.VENDEDOR && admin.CODIGO_TABLA.Equals(25) && v.VENDEDOR != "0003")
                           
                           )
                           && v.ACTIVO.Equals("S")
                         select new
                         {
                             USUARIO = v.VENDEDOR,
                             NombreUsuario = ma.NOMBRE
                         }).OrderBy(s => s.NombreUsuario);
        if (qUsuarios.Count() > 0)
        {

            foreach (var item in qUsuarios)
                lst.Add(
                    new UsuariosMayoreo
                    {
                        UsuarioId = item.USUARIO,
                        Nombre = item.NombreUsuario
                    }
                    );
        }


        return lst;
    }

    /// <summary>
    /// Método para obtener los diferentes descuentos para mayoreo
    /// si no hay nada, es una venta normal 
    /// </summary>
    /// <returns></returns>
    public static List<TipoDescuento> ObtenerTipoDescuento(string cliente)
    {
        dbDataContext db = new dbDataContext();
        List<TipoDescuento> lst = new List<TipoDescuento>();
        var qTipoVenta = (
                from desc in db.MF_DescuentoMayoreos
                where desc.Activo.Equals("S")
                select desc
            ).OrderBy(D => D.IdDescuento);

        if (qTipoVenta.Count() > 0)
        {

            foreach (var item in qTipoVenta)
                lst.Add(
                    new TipoDescuento
                    {
                        IdDescuento = item.IdDescuento,
                        Descuento = item.Descuento,
                        Porcentaje = item.Porcentaje,
                        ValorFijo = item.ValorFijo,
                        Criterio = item.Criterio,
                        Activo = item.Activo
                    }
                    );
        }
        return lst;
    }
}
