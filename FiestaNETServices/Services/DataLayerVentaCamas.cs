﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq.SqlClient;
using System.Web;
using System.Transactions;
using FiestaNETServices.Services;

/// <summary>
/// Summary description for DataLayerVentaCamas
/// </summary>
public static class DataLayerVentaCamas
{

    public static IQueryable<BodegasVentaCamas> ListarBodegas()
    {
        dbDataContext db = new dbDataContext();

        var q =
            from a in db.BODEGAs
            where a.BODEGA1.StartsWith("F")
            orderby a.BODEGA1
            select new BodegasVentaCamas()
            {
                Bodega = a.BODEGA1,
                Nombre = a.BODEGA1
            };

        return q;
    }

    public static IQueryable<ClientesMayoreo> ListarClientesMayoreo()
    {
        dbDataContext db = new dbDataContext();

        var q =
            from c in db.CLIENTEs
            where c.CLIENTE1.StartsWith("MA")
            orderby c.NOMBRE
            select new ClientesMayoreo()
            {
                Cliente = c.CLIENTE1,
                Nombre = c.NOMBRE
            };

        return q;
    }

    public static IQueryable<Camas> ListarVentaCamasMayoreo(DateTime FechaInicial, DateTime FechaFinal, string Bodega, string Tipo, bool Bases)
    {
        dbDataContext db = new dbDataContext();
        string mBodegas, mCliente; string mBases = "Base%";

        db.CommandTimeout = 999999;

        switch (Tipo)
        {
            case "F":
                mCliente = "0%";
                break;
            case "MA":
                mCliente = "MA%";
                break;
            default:
                mCliente = Tipo;
                break;
        }
         
        if (!Bases) mBases = "Colchones%";
        if (Bodega == "MF") mBodegas = "F%"; else mBodegas = Bodega;

        var q =
             from t in db.MF_ArticuloTamanios
             from b in db.BODEGAs
             where SqlMethods.Like(b.BODEGA1, mBodegas) && SqlMethods.Like(t.Descripcion, mBases)
             orderby b.BODEGA1, t.Orden
             select new Camas()
             {
                 Tienda = b.BODEGA1,
                 Tipo = t.Descripcion.Contains("Bases") ? "Bases" : "Colchones",
                 Tamaño = t.Descripcion,
                 Varios = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, Tipo + t.Codigo + "00" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "00" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()) +
                            (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "01" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "01" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()) +
                            (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "02" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "02" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 FrescoFoam = 0,

                 Wonder = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "03" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "03" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 Deluxe = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "04" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "04" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 Advanced = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "05" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "05" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 DreamSleeper = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "06" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "06" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 KidzSleeper = 0, //(Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "07" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                  //  (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "07" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 TripleCrown = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "08" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "08" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 Luxurious = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "09" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "09" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 WorldClass = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "10" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "10" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 BtyRestExceptionale = 0, //(Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "11" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    //(from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "11" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 DeepSleepPlush = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "12" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "12" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 BackCare = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "13" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "13" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 NaturalTouch = 0, //(Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "14" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                   // (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "14" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 BtyRestBlack = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "15" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "15" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 BtyRestExceptionalePlush = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "16" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "16" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 BeautySleepFirm = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "17" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "17" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 BeautySleepPlush = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "18" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "18" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 BeautyRestNXG = 0, //(Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "19" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    //(from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "19" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 SlumberTime = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "20" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "20" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 TrueEnergyPureEssence = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "21" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "21" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()), 
                 TrueEnergyAdvanced = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "22" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "22" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()), 
                 TrueEnergyElite = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "23" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, "M" + t.Codigo + "23" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 TotalTamanio = 0
             };
           
        return q;
    }

    public static IQueryable<Camas> ListarVentaCamas(DateTime FechaInicial, DateTime FechaFinal, string Bodega, string Tipo, bool Bases)
    {
        dbDataContext db = new dbDataContext();
        string mTipo = ""; string mBodegas, mCliente; string mBases = "Base%";

        db.CommandTimeout = 999999;

        switch (Tipo)
        {
            case "F":
                mCliente = "0%";
                break;
            case "MA":
                mCliente = "MA%";
                break;
            default:
                mCliente = Tipo;
                break;
        }
          
        if (!Bases) mBases = "Colchones%";
        if (Bodega == "MF") mBodegas = "F%"; else mBodegas = Bodega;

        var q =
             from t in db.MF_ArticuloTamanios
             from b in db.BODEGAs
             where SqlMethods.Like(b.BODEGA1, mBodegas) && SqlMethods.Like(t.Descripcion, mBases)
             orderby b.BODEGA1, t.Orden 
             select new Camas()
             {
                 Tienda = b.BODEGA1,
                 Tipo = t.Descripcion.Contains("Bases") ? "Bases" : "Colchones",
                 Tamaño = t.Descripcion,
                 Varios = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "00" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "00" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()) +
                            (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "01" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "01" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()) +
                            (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "02" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "02" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 FrescoFoam = 0,

                 Wonder = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "03" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "03" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 Deluxe = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "04" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "04" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 Advanced = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "05" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "05" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 DreamSleeper = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "06" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "06" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 KidzSleeper = 0, //(Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "07" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                  //  (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "07" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 TripleCrown = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "08" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "08" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 Luxurious = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "09" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "09" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 WorldClass = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "10" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "10" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 BtyRestExceptionale = 0, //(Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "11" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    //(from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "11" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 DeepSleepPlush = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "12" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "12" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 BackCare = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "13" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "13" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 NaturalTouch = 0, //(Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "14" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                   // (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "14" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 BtyRestBlack = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "15" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "15" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 BtyRestExceptionalePlush = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "16" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "16" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 BeautySleepFirm = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "17" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "17" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 BeautySleepPlush = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "18" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "18" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 BeautyRestNXG = 0, //(Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "19" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                   // (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "19" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 SlumberTime = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "20" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "20" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 TrueEnergyPureEssence = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "21" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "21" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()), 
                 TrueEnergyAdvanced = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "22" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "22" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()), 
                 TrueEnergyElite = (Int32)((from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "23" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Count() == 0 ? 0 :
                                    (from fl in db.FACTURA_LINEAs join f in db.FACTURAs on new { fl.FACTURA, fl.TIPO_DOCUMENTO } equals new { FACTURA = Convert.ToString(f.FACTURA1), f.TIPO_DOCUMENTO } where fl.FECHA_FACTURA >= FechaInicial && fl.FECHA_FACTURA <= FechaFinal && fl.TIPO_DOCUMENTO == "F" && fl.ANULADA == "N" && f.COBRADOR == b.BODEGA1 && SqlMethods.Like(fl.ARTICULO, mTipo + t.Codigo + "23" + t.Tamanio + "%") && SqlMethods.Like(f.CLIENTE, mCliente) select fl.CANTIDAD).Sum()),

                 TotalTamanio = 0
             };
        
        return q;
    }

}
