﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Pedidos Camas
[Serializable]
public class ArticulosRelacion
{
    public string Articulo { get; set; }
    public string Descripcion { get; set; }
}

[Serializable]
public class EstructuraRelacion
{
    public string Articulo { get; set; }
    public string Descripcion { get; set; }
    public string TieneBases { get; set; }
    public string TieneColchones { get; set; }
}

[Serializable]
public class PedidoFundas
{
    public string Articulo { get; set; }
    public string Descripcion { get; set; }
    public Nullable<int> Colchon { get; set; }
    public Nullable<int> Base { get; set; }
    public Nullable<int> Funda { get; set; }
    public Nullable<int> PedidoColchon { get; set; }
    public Nullable<int> PedidoFunda { get; set; }
    public string CodigoArticulo { get; set; }
}

[Serializable]
public class Bodegas
{
    public string Bodega { get; set; }
    public string Nombre { get; set; }
}

[Serializable]
public class ExistenciasArticulo
{
    public string Bodega { get; set; }
    public Nullable<int> Colchones { get; set; }
    public Nullable<int> Bases { get; set; }
    public Nullable<int> Fundas { get; set; }
    public string ToolTipColchones { get; set; }
    public string ToolTipBases { get; set; }
    public string ToolTipFundas { get; set; }
    public string CodigoBodega { get; set; }
}

[Serializable]
public class Pedidos
{
    public int Pedido { get; set; }
    public string Fecha { get; set; }
    public string Bodega { get; set; }
    public string Estatus { get; set; }
    public string Descripcion { get; set; }
    public DateTime FechaCreacion { get; set; }
    public string UsuarioCreo { get; set; }
}

[Serializable]
public class ExistenciasArticuloRelacion
{
    public string Bodega { get; set; }
    public string Articulo { get; set; }
    public string Descripcion { get; set; }
    public int Existencias { get; set; }
    public string Funda { get; set; }
    public string CodigoBodega { get; set; }
}



//Friedman
[Serializable]
public class MetasTiendas
{
    public decimal F02Minimo { get; set; }
    public decimal F02Meta { get; set; }
    public decimal F02Venta { get; set; }
    public decimal F03Minimo { get; set; }
    public decimal F03Meta { get; set; }
    public decimal F03Venta { get; set; }
    public decimal F04Minimo { get; set; }
    public decimal F04Meta { get; set; }
    public decimal F04Venta { get; set; }
    public decimal F05Minimo { get; set; }
    public decimal F05Meta { get; set; }
    public decimal F05Venta { get; set; }
    public decimal F06Minimo { get; set; }
    public decimal F06Meta { get; set; }
    public decimal F06Venta { get; set; }
    public decimal F07Minimo { get; set; }
    public decimal F07Meta { get; set; }
    public decimal F07Venta { get; set; }
    public decimal F08Minimo { get; set; }
    public decimal F08Meta { get; set; }
    public decimal F08Venta { get; set; }
    public decimal F09Minimo { get; set; }
    public decimal F09Meta { get; set; }
    public decimal F09Venta { get; set; }
    public decimal F10Minimo { get; set; }
    public decimal F10Meta { get; set; }
    public decimal F10Venta { get; set; }
    public decimal F11Minimo { get; set; }
    public decimal F11Meta { get; set; }
    public decimal F11Venta { get; set; }
    public decimal F12Minimo { get; set; }
    public decimal F12Meta { get; set; }
    public decimal F12Venta { get; set; }
    public decimal F13Minimo { get; set; }
    public decimal F13Meta { get; set; }
    public decimal F13Venta { get; set; }
    public decimal F14Minimo { get; set; }
    public decimal F14Meta { get; set; }
    public decimal F14Venta { get; set; }
    public decimal F15Minimo { get; set; }
    public decimal F15Meta { get; set; }
    public decimal F15Venta { get; set; }
    public decimal F16Minimo { get; set; }
    public decimal F16Meta { get; set; }
    public decimal F16Venta { get; set; }
    public decimal F17Minimo { get; set; }
    public decimal F17Meta { get; set; }
    public decimal F17Venta { get; set; }
    public decimal F18Minimo { get; set; }
    public decimal F18Meta { get; set; }
    public decimal F18Venta { get; set; }
    public decimal F19Minimo { get; set; }
    public decimal F19Meta { get; set; }
    public decimal F19Venta { get; set; }
    public decimal F20Minimo { get; set; }
    public decimal F20Meta { get; set; }
    public decimal F20Venta { get; set; }
    public decimal F21Minimo { get; set; }
    public decimal F21Meta { get; set; }
    public decimal F21Venta { get; set; }
    public decimal F22Minimo { get; set; }
    public decimal F22Meta { get; set; }
    public decimal F22Venta { get; set; }
    public decimal F23Minimo { get; set; }
    public decimal F23Meta { get; set; }
    public decimal F23Venta { get; set; }
    public decimal F24Minimo { get; set; }
    public decimal F24Meta { get; set; }
    public decimal F24Venta { get; set; }
    public decimal F25Minimo { get; set; }
    public decimal F25Meta { get; set; }
    public decimal F25Venta { get; set; }
    public decimal F26Minimo { get; set; }
    public decimal F26Meta { get; set; }
    public decimal F26Venta { get; set; }
    public decimal F27Minimo { get; set; }
    public decimal F27Meta { get; set; }
    public decimal F27Venta { get; set; }
    public decimal F28Minimo { get; set; }
    public decimal F28Meta { get; set; }
    public decimal F28Venta { get; set; }
}

[Serializable]
public class VentasVendedor
{
    public string Vendedor { get; set; }
    public decimal Venta { get; set; }
    public decimal Dias { get; set; }
    public decimal Facturas { get; set; }
    public decimal Articulos { get; set; }
    public decimal Oportunidades { get; set; }
    public bool EnVacaciones { get; set; }
    public decimal Minimo { get; set; }
    public decimal Meta { get; set; }
    public decimal TotalDias { get; set; }
}

[Serializable]
public class VentasVendedorSemanas
{
    public string Semana { get; set; }
    public decimal Venta { get; set; }
    public decimal Dias { get; set; }
    public decimal Facturas { get; set; }
    public decimal Articulos { get; set; }
    public decimal Oportunidades { get; set; }
    public bool EnVacaciones { get; set; }
    public decimal Minimo { get; set; }
    public decimal Meta { get; set; }
    public decimal TotalDias { get; set; }
    public int id { get; set; }
    public int anio { get; set; }
    public int NumeroSemana { get; set; }
}






//Venta Camas
[Serializable]
public class Camas
{
    public string Tienda { get; set; }
    public string Tipo { get; set; }
    public string Tamaño { get; set; }
    public Int32 Varios { get; set; }
    public Int32 FrescoFoam { get; set; }
    public Int32 Wonder { get; set; }
    public Int32 Deluxe { get; set; }
    public Int32 Advanced { get; set; }
    public Int32 DreamSleeper { get; set; }
    public Int32 KidzSleeper { get; set; }
    public Int32 TripleCrown { get; set; }
    public Int32 Luxurious { get; set; }
    public Int32 WorldClass { get; set; }
    public Int32 BtyRestExceptionale { get; set; }
    public Int32 DeepSleepPlush { get; set; }
    public Int32 BackCare { get; set; }
    public Int32 NaturalTouch { get; set; }
    public Int32 BtyRestBlack { get; set; }
    public Int32 BtyRestExceptionalePlush { get; set; }
    public Int32 BeautySleepFirm { get; set; }
    public Int32 BeautySleepPlush { get; set; }
    public Int32 BeautyRestNXG { get; set; }
    public Int32 SlumberTime { get; set; }
    public Int32 TrueEnergyPureEssence { get; set; }
    public Int32 TrueEnergyAdvanced { get; set; }
    public Int32 TrueEnergyElite { get; set; }
    public Int32 TotalTamanio { get; set; }
}

[Serializable]
public class BodegasVentaCamas
{
    public string Bodega { get; set; }
    public string Nombre { get; set; }
}

[Serializable]
public class ClientesMayoreo
{
    public string Cliente { get; set; }
    public string Nombre { get; set; }
}




//Actividad Ventas
[Serializable]
public class ReporteActividadVentas
{
    public string Proveedor { get; set; }
    public string CodigoProveedor { get; set; }
    public string NombreProveedor { get; set; }
    public string Articulo { get; set; }
    public string NombreArticulo { get; set; }
    public string UltimaCompra { get; set; }
    public Nullable<Int32> Compras { get; set; }
    public Nullable<Int32> ExistenciasFecha { get; set; }
    public Nullable<Int32> Ventas { get; set; }
    public Nullable<Int32> VentasAnterior { get; set; }
    public Nullable<decimal> CostoVentaUnitario { get; set; }
    public Nullable<decimal> CostoVentaUnitarioAnterior { get; set; }
    public Nullable<decimal> CostoVentas { get; set; }
    public Nullable<decimal> CostoVentasAnterior { get; set; }
    public Nullable<decimal> ValorVentaUnitario { get; set; }
    public Nullable<decimal> ValorVentaUnitarioAnterior { get; set; }
    public Nullable<decimal> ValorVentas { get; set; }
    public Nullable<decimal> ValorVentasAnterior { get; set; }
    public Nullable<decimal> MargenVentasMonto { get; set; }
    public Nullable<decimal> MargenVentasMontoAnterior { get; set; }
    public Nullable<decimal> MargenVentasPorcentaje { get; set; }
    public Nullable<decimal> MargenVentasPorcentajeAnterior { get; set; }
    public Nullable<decimal> MargenPrecio { get; set; }
    public Nullable<decimal> MargenPrecioAnterior { get; set; }
    public Int32 EntradasPositivas { get; set; }
    public Int32 EntradasNegativas { get; set; }
    public Int32 SalidasPositivas { get; set; }
    public Int32 SalidasNegativas { get; set; }
}

[Serializable]
public class Clasificaciones
{
    public string Clasificacion { get; set; }
    public string Descripcion { get; set; }
}




//Cambio Precios
[Serializable]
public class NivelPrecioCoeficiente
{
    public string Nivel { get; set; }
    public decimal Coeficiente { get; set; }
    public string Descripción { get; set; }
}

[Serializable]
public class Articulos
{
    public string Articulo { get; set; }
    public string Descripción { get; set; }
    public decimal PrecioLista { get; set; }
}

[Serializable]
public class UsuariosCancelanOfertas
{
    public string usuario { get; set; }
}

[Serializable]
public class Ofertas
{
    public string Tipo { get; set; }
    public string Articulo { get; set; }
    public string NombreArticulo { get; set; }
    public string NivelPrecio { get; set; }
    public DateTime FechaInicial { get; set; }
    public DateTime FechaFinal { get; set; }
    public decimal PrecioOriginal { get; set; }
    public decimal PrecioOferta { get; set; }
    public string Estatus { get; set; }
    public string DescripcionOferta { get; set; }
    public string UsuarioCreo { get; set; }
    public DateTime FechaCreada { get; set; }
    public string UsuarioCancela { get; set; }
    public Nullable<DateTime> FechaCancela { get; set; }
    public string MotivoCancela { get; set; }
}

[Serializable]
public class BitacoraProcesos
{
    public string Tipo { get; set; }
    public string Resultado { get; set; }
    public string Descripcion { get; set; }
    public DateTime FechaRegistro { get; set; }
    public string UsuarioCreo { get; set; }
}



//Asistencia
[Serializable]
public class EmpleadoHuella
{
    public byte[] Huella { get; set; }
    public string Empleado { get; set; }
    public string Empresa { get; set; }
    public DateTime Fecha { get; set; }
}





//Punto de Venta
[Serializable]
public class Clientes
{
    public string Cliente { get; set; }
    public string PrimerNombre { get; set; }
    public string SegundoNombre { get; set; }
    public string TercerNombre { get; set; }
    public string PrimerApellido { get; set; }
    public string SegundoApellido { get; set; }
    public string ApellidoCasada { get; set; }
    public string CalleAvenida { get; set; }
    public string CasaNumero { get; set; }
    public string Apartamento { get; set; }
    public string Zona { get; set; }
    public string Colonia { get; set; }
    public string Departamento { get; set; }
    public string Municipio { get; set; }
    public string IndicacionesParaLlegar { get; set; }
    public string Telefono1 { get; set; }
    public string Telefono2 { get; set; }
    public string Celular { get; set; }
    public string Nit { get; set; }
    public string Notas { get; set; }
    public string Email { get; set; }
    public string Empresa { get; set; }
    public string EmpresaDireccion { get; set; }
    public string EmpresaTelefono { get; set; }
    public string Cargo { get; set; }
    public string TiempoLaborar { get; set; }
    public string IngresosMes { get; set; }
    public string GastosMes { get; set; }
    public string ConyugeNombre { get; set; }
    public string ConyugueCedula { get; set; }
    public string ConyugueTelefono { get; set; }
    public string DPI { get; set; }
    public string Genero { get; set; }
    public bool FechaNacimientoValida { get; set; }
    public Nullable<DateTime> FechaNacimiento { get; set; }
    public string FacturarOtroNombre { get; set; }
    public string NitFactura { get; set; }
    public string NombreFactura { get; set; }
    public string EntraCamion { get; set; }
    public string EntraPickup { get; set; }
    public string EsSegundoPiso { get; set; }
    public string UsarDireccEnFactura { get; set; }
    public string CalleAvenidaFacturacion { get; set; }
    public string CasaNumeroFacturacion { get; set; }
    public string ApartamentoFacturacion { get; set; }
    public string ZonaFacturacion { get; set; }
    public string ColoniaFacturacion { get; set; }
    public string DepartamentoFacturacion { get; set; }
    public string MunicipioFacturacion { get; set; }
    public string TipoVenta { get; set; }
    public Int32 Financiera { get; set; }
    public string NivelPrecio { get; set; }
    public string EstadoCivil { get; set; } //S: Soltero(a) ; C: Casado(a) ; U: Unido(a) ; D: Divorciado(a) ; V: Viudo(a)
    public string Nacionalidad { get; set; }
    public string Profesion { get; set; }
    public string TieneVehiculo { get; set; }
    public string MarcaModelo { get; set; }
    public string Placas { get; set; }
    public string TipoCasa { get; set; } //P: Casa Propia ; F: Casa de Familiares ; A: Casa Alquilada
    public string TiempoResidir { get; set; }
    public string PagoMensual { get; set; }
    public string FinancieraPaga { get; set; }
    public string NumeroContador { get; set; }
    public string RefPersonalNombre1 { get; set; }
    public string RefPersonalTelResidencia1 { get; set; }
    public string RefPersonalTelTrabajo1 { get; set; }
    public string RefPersonalCelular1 { get; set; }
    public string RefPersonalNombre2 { get; set; }
    public string RefPersonalTelResidencia2 { get; set; }
    public string RefPersonalTelTrabajo2 { get; set; }
    public string RefPersonalCelular2 { get; set; }
    public string RefComercialNombre1 { get; set; }
    public string RefComercialTelefono1 { get; set; }
    public string RefComercialFax1 { get; set; }
    public string RefComercialCelular1 { get; set; }
    public string RefComercialNombre2 { get; set; }
    public string RefComercialTelefono2 { get; set; }
    public string RefComercialFax2 { get; set; }
    public string RefComercialCelular2 { get; set; }
    public string Nombre { get; set; }
    public string ConsumidorFinal { get; set; }
    public string Extension { get; set; }
    public string CargasFamiliares { get; set; }
    public string Politico { get; set; }
    public string ReferenciasBancarias { get; set; }
    public string ConyugueEmpresa { get; set; }
    public string ConyugueCargo { get; set; }
    public string ConyugueTiempo { get; set; }
    public string ConyugueIngresos { get; set; }
    public bool ConyugueFechaNacimientoValida { get; set; }
    public Nullable<DateTime> ConyugueFechaNacimiento { get; set; }
    public string DepartamentoEmpresa { get; set; }
    public string MunicipioEmpresa { get; set; }
    public decimal TotalFacturar { get; set; }
    public decimal Enganche { get; set; }
    public decimal SaldoFinanciar { get; set; }
    public decimal Recargos { get; set; }
    public decimal Monto { get; set; }
    public string Plazo { get; set; }
    public Int32 CantidadPagos1 { get; set; }
    public decimal MontoPagos1 { get; set; }
    public Int32 CantidadPagos2 { get; set; }
    public decimal MontoPagos2 { get; set; }
    public string ConyugueEmpresaDireccion { get; set; }
    public string ConyugueEmpresaTelefono { get; set; }
    public string PersonaJuridica { get; set; }
    public string Banco { get; set; }
    public bool TrabajoFechaIngresoValida { get; set; }
    public Nullable<DateTime> TrabajoFechaIngreso { get; set; }
    public string CalleAvenidaEmpresa { get; set; }
    public string CasaNumeroEmpresa { get; set; }
    public string ApartamentoEmpresa { get; set; }
    public string ZonaEmpresa { get; set; }
    public string ColoniaEmpresa { get; set; }
    public string VendedorInterconsumo { get; set; }
    public string AutorizadoPor { get; set; }
    public string NoAutorizacion { get; set; }
    public Nullable<DateTime> FechaAutorizacion { get; set; }
    public Nullable<DateTime> FechaInicial { get; set; }
    public Nullable<DateTime> FechaFinal { get; set; }
    public Nullable<DateTime> FechaPedido { get; set; }
    public string Familiar1PrimerNombre { get; set; }
    public string Familiar1SegundoNombre { get; set; }
    public string Familiar1TercerNombre { get; set; }
    public string Familiar1PrimerApellido { get; set; }
    public string Familiar1SegundoApellido { get; set; }
    public string Familiar1ApellidoCasada { get; set; }
    public string Familiar1Celular { get; set; }
    public string Familiar1Tipo { get; set; }
    public string Familiar1Empresa { get; set; }
    public string Familiar1EmpresaDireccion { get; set; }
    public string Familiar1EmpresaDepartamento { get; set; }
    public string Familiar1EmpresaMunicipio { get; set; }
    public string Familiar1EmpresaTelefono { get; set; }
    public string Familiar1CasaDireccion { get; set; }
    public string Familiar1CasaDepartamento { get; set; }
    public string Familiar1CasaMunicipio { get; set; }
    public string Familiar1CasaTelefono { get; set; }
    public string Familiar2PrimerNombre { get; set; }
    public string Familiar2SegundoNombre { get; set; }
    public string Familiar2TercerNombre { get; set; }
    public string Familiar2PrimerApellido { get; set; }
    public string Familiar2SegundoApellido { get; set; }
    public string Familiar2ApellidoCasada { get; set; }
    public string Familiar2Celular { get; set; }
    public string Familiar2Tipo { get; set; }
    public string Familiar2Empresa { get; set; }
    public string Familiar2EmpresaDireccion { get; set; }
    public string Familiar2EmpresaDepartamento { get; set; }
    public string Familiar2EmpresaMunicipio { get; set; }
    public string Familiar2EmpresaTelefono { get; set; }
    public string Familiar2CasaDireccion { get; set; }
    public string Familiar2CasaDepartamento { get; set; }
    public string Familiar2CasaMunicipio { get; set; }
    public string Familiar2CasaTelefono { get; set; }
    public string Personal1PrimerNombre { get; set; }
    public string Personal1SegundoNombre { get; set; }
    public string Personal1TercerNombre { get; set; }
    public string Personal1PrimerApellido { get; set; }
    public string Personal1SegundoApellido { get; set; }
    public string Personal1ApellidoCasada { get; set; }
    public string Personal1Celular { get; set; }
    public string Personal1Tipo { get; set; }
    public string Personal1Empresa { get; set; }
    public string Personal1EmpresaDireccion { get; set; }
    public string Personal1EmpresaDepartamento { get; set; }
    public string Personal1EmpresaMunicipio { get; set; }
    public string Personal1EmpresaTelefono { get; set; }
    public string Personal1CasaDireccion { get; set; }
    public string Personal1CasaDepartamento { get; set; }
    public string Personal1CasaMunicipio { get; set; }
    public string Personal1CasaTelefono { get; set; }
    public string Personal2PrimerNombre { get; set; }
    public string Personal2SegundoNombre { get; set; }
    public string Personal2TercerNombre { get; set; }
    public string Personal2PrimerApellido { get; set; }
    public string Personal2SegundoApellido { get; set; }
    public string Personal2ApellidoCasada { get; set; }
    public string Personal2Celular { get; set; }
    public string Personal2Tipo { get; set; }
    public string Personal2Empresa { get; set; }
    public string Personal2EmpresaDireccion { get; set; }
    public string Personal2EmpresaDepartamento { get; set; }
    public string Personal2EmpresaMunicipio { get; set; }
    public string Personal2EmpresaTelefono { get; set; }
    public string Personal2CasaDireccion { get; set; }
    public string Personal2CasaDepartamento { get; set; }
    public string Personal2CasaMunicipio { get; set; }
    public string Personal2CasaTelefono { get; set; }
    public string JefePrimerNombre { get; set; }
    public string JefeSegundoNombre { get; set; }
    public string JefeTercerNombre { get; set; }
    public string JefePrimerApellido { get; set; }
    public string JefeSegundoApellido { get; set; }
    public string JefeApellidoCasada { get; set; }
    public string JefeCelular { get; set; }
    public string ConyuguePrimerNombre { get; set; }
    public string ConyugueSegundoNombre { get; set; }
    public string ConyugueTercerNombre { get; set; }
    public string ConyuguePrimerApellido { get; set; }
    public string ConyugueSegundoApellido { get; set; }
    public string ConyugueApellidoCasada { get; set; }
    public string ConyugueDPI { get; set; }
    public string ConyugueNIT { get; set; }
    //solo para F01 //S  o  N
    public string Exento { get; set; } 
}

[Serializable]
public class ClientesBusqueda
{
    public string Cliente { get; set; }
    public string Nombre { get; set; }
    public string Nit { get; set; }
    public string TelCasa { get; set; }
    public string TelTrabajo { get; set; }
    public string TelCelular { get; set; }
    public string Tienda { get; set; }
    public string Vendedor { get; set; }
    public DateTime FechaIngreso { get; set; }
}

[Serializable]
public class Zonas
{
    public string Zona { get; set; }
    public string Nombre { get; set; }
}

[Serializable]
public class Departamentos
{
    public string Departamento { get; set; }
}

[Serializable]
public class Municipios
{
    public string Municipio { get; set; }
}
[Serializable]
public class UsuariosMayoreo
{
    public string UsuarioId { get; set; }
    public string Nombre { get; set; }
}
[Serializable]
public class TipoDescuento
{
    public int IdDescuento { get; set; }
    public string Descuento { get; set; }
    public double? Porcentaje { get; set; }
    public double? ValorFijo { get; set; }
    public string Criterio { get; set; }
    public string Activo { get; set; }
}
[Serializable]
public class Vendedores
{
    public string Vendedor { get; set; }
    public string Nombre { get; set; }
    public string Tienda { get; set; }
}

[Serializable]
public class Medios
{
    public string Medio { get; set; }
    public string Descripcion { get; set; }
}

[Serializable]
public class Tiendas
{
    public string Tienda { get; set; }
    public string Nombre { get; set; }
}

[Serializable]
public class CategoriasCliente
{
    public string Categoria { get; set; }
    public string Nombre { get; set; }
    public string Tienda { get; set; }
}

[Serializable]
public class Datos
{
    public string Tienda { get; set; }
    public string Vendedor { get; set; }
    public string NombreVendedor { get; set; }
}

[Serializable]
public class TipoVenta
{
    public string CodigoTipoVenta { get; set; }
    public string Descripcion { get; set; }
    public string Estatus { get; set; }
    public string Observaciones { get; set; }
    public string TodosLosNiveles { get; set; }
    public string Vence { get; set; }
    public DateTime FechaVence { get; set; }
    public string Tipo { get; set; }
    public decimal Valor { get; set; }
    public decimal CompraMinima { get; set; }
    public Int32 Financiera { get; set; }
    public string TextoPromocion { get; set; }
    public decimal DescuentoNormal { get; set; }
    public decimal DescuentoOfertado { get; set; }
    public string MensajeValidacion { get; set; }
    public string Tooltip { get; set; }
    public string ManejaVale { get; set; }
}

[Serializable]
public class TipoVentaDet
{
    public string Articulo { get; set; }
    public string Nombre { get; set; }
    public string Tipo { get; set; }
    public decimal Valor { get; set; }
    public string Observaciones { get; set; }
    public decimal Costo { get; set; }
    public decimal CostoIVA { get; set; }
    public decimal PrecioLista { get; set; }
    public string ArticuloRegalo { get; set; }
}

[Serializable]
public class TipoVentaNivel
{
    public string Nivel { get; set; }
}

[Serializable]
public class Financieras
{
    public Int32 Financiera { get; set; }
    public string Nombre { get; set; }
    public string prefijo { get; set; }
    public string Estatus { get; set; }
}

[Serializable]
public class NivelesPrecio
{
    public string NivelPrecio { get; set; }
    public double Factor { get; set; }
    public string TipoVenta { get; set; }
    public string TipoDeVenta { get; set; }
    public Int32 Financiera { get; set; }
    public string NombreFinanciera { get; set; }
    public string Estatus { get; set; }
    public string Tipo { get; set; }
    public string Pagos { get; set; }
    public string Observaciones { get; set; }
    public string EsCiudad { get; set; }
    public string TextoEncabezado { get; set; }
    public Int32 DiasParaPagar { get; set; }
    public string PrecioContado { get; set; }
}

[Serializable]
public class ArticulosPrecios
{
    public string Articulo { get; set; }
    public string Nombre { get; set; }
    public decimal Precio { get; set; }
    public string NivelPrecio { get; set; }
    public string Oferta { get; set; }
    public decimal PrecioOriginal { get; set; }
    public DateTime FechaVence { get; set; }
    public decimal DescuentoContado { get; set; }
    public decimal DescuentoCredito { get; set; }
    public string Condicional { get; set; }
    public decimal PrecioOferta { get; set; }
    public string Condiciones { get; set; }
    public string Promocion { get; set; }
    public string TipoPromocion { get; set; }
    public decimal ValorPromocion { get; set; }
    public string VencePromocion { get; set; }
    public DateTime FechaVencePromocion { get; set; }
    public string DescripcionPromocion { get; set; }
    public string ObservacionesPromocion { get; set; }
    public string Pagos { get; set; }
    public DateTime FechaDesde { get; set; }
    public string TipoOferta { get; set; }
    public double Factor { get; set; }
    public string PrecioContado { get; set; }
}

[Serializable]
public class AdministraOfertas
{
    public DateTime FechaInicial { get; set; }
    public DateTime FechaFinal { get; set; }
    public string Articulo { get; set; }
    public string Descripcion { get; set; }
    public string Estatus { get; set; }
    public string Tipo { get; set; }
    public Int32 identificador { get; set; }
    public decimal PrecioOriginal { get; set; }
    public decimal PrecioOferta { get; set; }
}

[Serializable]
public class ArticulosLocalizacion
{
    public string Articulo { get; set; }
    public string Descripcion { get; set; }
    public string Localizacion { get; set; }
    public DateTime UltimoIngreso { get; set; }
    public Int32 Existencias { get; set; }
}

[Serializable]
public class ArticulosInventario
{
    public string Articulo { get; set; }
    public string Descripcion { get; set; }
    public string Localizacion { get; set; }
    public DateTime UltimaCompra { get; set; }
    public string Blanco1 { get; set; }
    public Int32 Existencias { get; set; }
    public string Blanco2 { get; set; }
    public Int32 ExistenciaFisica { get; set; }
    public string Conteo { get; set; }
    public string Observaciones { get; set; }
}

[Serializable]
public class ArticulosInventarioGeneral
{
    public string Articulo { get; set; }
    public string Descripcion { get; set; }
    public string Localizacion { get; set; }
    public Int32 F01 { get; set; }
    public Int32 F02 { get; set; }
    public Int32 F03 { get; set; }
    public Int32 F04 { get; set; }
    public Int32 F05 { get; set; }
    public Int32 F06 { get; set; }
    public Int32 F07 { get; set; }
    public Int32 F08 { get; set; }
    public Int32 F09 { get; set; }
    public Int32 F10 { get; set; }
    public Int32 F11 { get; set; }
    public Int32 F12 { get; set; }
    public Int32 F13 { get; set; }
    public Int32 F14 { get; set; }
    public Int32 F15 { get; set; }
    public Int32 F16 { get; set; }
    public Int32 F17 { get; set; }
    public Int32 F18 { get; set; }
    public Int32 F19 { get; set; }
    public Int32 F20 { get; set; }
    public Int32 F21 { get; set; }
    public Int32 F22 { get; set; }
    public Int32 F23 { get; set; }
    public Int32 F24 { get; set; }
    public Int32 F25 { get; set; }
    public Int32 F26 { get; set; }
    public Int32 F27 { get; set; }
    public Int32 F28 { get; set; }
    public Int32 F29 { get; set; }
    public Int32 F30 { get; set; }
    public Int32 F31 { get; set; }
    public Int32 F32 { get; set; }
    public Int32 F33 { get; set; }
    public Int32 F34 { get; set; }
    public Int32 F35 { get; set; }
    public Int32 F36 { get; set; }
    public Int32 F37 { get; set; }
    public Int32 F38 { get; set; }
    public Int32 F39 { get; set; }
    public Int32 F40 { get; set; }
    public Int32 F41 { get; set; }
    public Int32 F42 { get; set; }
    public Int32 F43 { get; set; }
    public Int32 F44 { get; set; }
    public Int32 F45 { get; set; }
    public Int32 F46 { get; set; }
    public Int32 F47 { get; set; }
    public Int32 F48 { get; set; }
    public Int32 F49 { get; set; }
    public Int32 F50 { get; set; }
    public Int32 F51 { get; set; }
    public Int32 F52 { get; set; }
    public Int32 F53 { get; set; }
    public Int32 F54 { get; set; }
    public Int32 F55 { get; set; }
    public Int32 F56 { get; set; }
    public Int32 F57 { get; set; }
    public Int32 F58 { get; set; }
    public Int32 F59 { get; set; }
    public Int32 F60 { get; set; }
}

[Serializable]
public class ArticuloDescripcionPrecio
{
    public string Articulo { get; set; }
    public string Descripcion { get; set; }
    public decimal Precio { get; set; }
}

[Serializable]
public class Pedido
{
    public string NumeroPedido { get; set; }
    public string Cliente { get; set; }
    public DateTime Fecha { get; set; }
    public string Garantia { get; set; } //RUBRO1
    public string Observaciones { get; set; }
    public string ObservacionesTipoVenta { get; set; }
    public decimal TotalFacturar { get; set; }
    public decimal Enganche { get; set; }
    public decimal DescuentoTotal { get; set; }
    public decimal SaldoFinanciar { get; set; }
    public decimal Recargos { get; set; }
    public decimal Monto { get; set; }
    public string TipoVenta { get; set; }
    public Int32 Financiera { get; set; }
    public string NivelPrecio { get; set; }
    public string Tienda { get; set; }
    public string Vendedor { get; set; }
    public string Bodega { get; set; }
    public string NombreCliente { get; set; }
    public string EntregaAMPM { get; set; }
    public string MercaderiaSale { get; set; }
    public string Desarmarla { get; set; }
    public string NotasTipoVenta { get; set; }
    public string Pagare { get; set; }
    public string NombreAutorizacion { get; set; }
    public string Autorizacion { get; set; }
    public string Solicitud { get; set; }
    public Int32 CantidadPagos1 { get; set; }
    public decimal MontoPagos1 { get; set; }
    public Int32 CantidadPagos2 { get; set; }
    public decimal MontoPagos2 { get; set; }
    public string TipoReferencia { get; set; }
    public string ObservacionesReferencia { get; set; }
    public string TipoPedido { get; set; }
    public string Vale { get; set; }
    public Nullable<int> Cotizacion { get; set; }
    public DateTime FechaEntrega { get; set; }
    public bool FechaEntregaValida { get; set; }
    public string NombreRecibe { get; set; }
    public string Puntos { get; set; }
    public string ValorPuntos { get; set; }
    public string AutorizacionPuntos { get; set; }
    public string AutorizacionInterconsumo { get; set; }
    public string NombreAutorizacionInterconsumo { get; set; }
    public string LocalF09 { get; set; }
    public string Refacturacion { get; set; }
    public DateTime PrimerPago { get; set; }
    public string DescuentoAccesorios { get; set; }
    public decimal DescuentoVales { get; set; }
}

[Serializable]
public class PedidoLinea
{
    public string Articulo { get; set; }
    public string Nombre { get; set; }
    public decimal PrecioUnitario { get; set; }
    public decimal CantidadPedida { get; set; }
    public decimal PrecioTotal { get; set; }
    public string Bodega { get; set; }
    public string Localizacion { get; set; }
    public string RequisicionArmado { get; set; }
    public string Descripcion { get; set; }
    public string Comentario { get; set; }
    public string Estado { get; set; }
    public string NumeroPedido { get; set; }
    public string Oferta { get; set; }
    public DateTime FechaOfertaDesde { get; set; }
    public decimal PrecioOriginal { get; set; }
    public string EsDetalleKit { get; set; }
    public decimal PrecioFacturar { get; set; }
    public string TipoOferta { get; set; }
    public Nullable<int> Vale { get; set; }
    public Nullable<Int64> Autorizacion { get; set; }
    public string Condicional { get; set; }
    public string ArticuloCondicion { get; set; }
    public decimal PrecioUnitarioDebioFacturar { get; set; }
    public decimal PrecioSugerido { get; set; }
    public double PrecioBaseLocal { get; set; }
    public Int32 Linea { get; set; }
    public string Gel { get; set; }
    public decimal PorcentajeDescuento { get; set; }
    public string Beneficiario { get; set; }
    public string Tienda { get; set; }
    public DateTime FechaFactura { get; set; }
    public int IdDescuento { get; set; }
    public string DescuentoNombre { get; set; }
    public decimal Descuento { get; set; }
    public decimal NetoFacturar { get; set; }
}

[Serializable]
public class PedidoMayoreo
{
    public string Pedido { get; set; }
    public string OrdenCompra { get; set; }
    public string Observaciones { get; set; }
    public DateTime FechaEntrega { get; set; }
    public string NombreRecibe { get; set; }
    public string CemacoTienda { get; set; }
    public DateTime CemacoFechaVenta { get; set; }
    public string CemacoCaja { get; set; }
    public string CemacoTransaccion { get; set; }
    public string CemacoClienteNombre { get; set; }
    public string CemacoClienteTelefono { get; set; }
    public string CemacoClienteDireccion { get; set; }
    public string CemacoClienteDepartamento { get; set; }
    public string CemacoClienteMunicipio { get; set; }
    public string CemacoClienteZona { get; set; }
    public string CemacoClienteIndicaciones { get; set; }
    public string CemacoClienteSegundoPiso { get; set; }
    public string CemacoClienteEntraCamion { get; set; }
    public string CemacoClienteEntraPickup { get; set; }
    public string Vendedor { get; set; }
}

[Serializable]
public class ValeFechaVenceNombre
{
    public DateTime FechaVence { get; set; }
    public string Nombre { get; set; }
}

[Serializable]
public class BodegaLocalizacion
{
    public string Articulo { get; set; }
    public string Nombre { get; set; }
    public string Bodega { get; set; }
    public string Localizacion { get; set; }
    public Int32 DisponibleAlmacen { get; set; }
    public Int32 ReservadaAlmacen { get; set; }
    public Int32 RemitidaAlmacen { get; set; }
    public Int32 TotalAlmacen { get; set; }
}

[Serializable]
public class Factura
{
    public string NoFactura { get; set; }
    public DateTime Fecha { get; set; }
    public string Cliente { get; set; }
    public string Nombre { get; set; }
    public string Nit { get; set; }
    public string Direccion { get; set; }
    public string Vendedor { get; set; }
    public decimal Monto { get; set; }
    public string MontoLetras { get; set; }
    public string Observaciones { get; set; }
    public string Moneda { get; set; }
}

[Serializable]
public class FacturaLinea
{
    public string NoFactura { get; set; }
    public string Articulo { get; set; }
    public Int32 Cantidad { get; set; }
    public string Descripcion { get; set; }
    public decimal PrecioUnitario { get; set; }
    public decimal PrecioTotal { get; set; }
}

[Serializable]
public class Despacho
{
    public string NoDespacho { get; set; }
    public DateTime Fecha { get; set; }
    public string Cliente { get; set; }
    public string Nombre { get; set; }
    public string Direccion { get; set; }
    public string Indicaciones { get; set; }
    public string Telefonos { get; set; }
    public string FormaPago { get; set; }
    public string Tienda { get; set; }
    public string Vendedor { get; set; }
    public string NombreVendedor { get; set; }
    public string Factura { get; set; }
    public string Observaciones { get; set; }
    public string Notas { get; set; }
    public string Transportista { get; set; }
    public string NombreTransportista { get; set; }
    public string CemacoTienda { get; set; }
    public DateTime CemacoFechaVenta { get; set; }
    public string CemacoCaja { get; set; }
    public string CemacoTransaccion { get; set; }
    public string OrdenCompra { get; set; }
    public string ObservacionesPedido { get; set; }
}

[Serializable]
public class DespachoLinea
{
    public string NoDespacho { get; set; }
    public string Articulo { get; set; }
    public Int32 Cantidad { get; set; }
    public string Descripcion { get; set; }
    public string Bodega { get; set; }
    public string Localizacion { get; set; }
}

[Serializable]
public class Vales
{
    public Int32 Vale { get; set; }
    public DateTime Fecha { get; set; }
    public DateTime FechaVence { get; set; }
    public string Vigente { get; set; }
    public string Canjeado { get; set; }
    public Nullable<DateTime> FechaCanjeado { get; set; }
}

[Serializable]
public class Autorizaciones
{
    public Int64 Autorizacion { get; set; }
    public DateTime Fecha { get; set; }
    public string Cliente { get; set; }
    public string Tipo { get; set; }
    public string ClienteNombre { get; set; }
    public string Articulo { get; set; }
    public string ArticuloNombre { get; set; }
    public string TipoVenta { get; set; }
    public string Financiera { get; set; }
    public string NivelPrecio { get; set; }
    public string TipoVentaNombre { get; set; }
    public string FinancieraNombre { get; set; }
    public Decimal Precio { get; set; }
    public Decimal PrecioLista { get; set; }
    public Decimal PrecioSolicitado { get; set; }
    public Int32 Cantidad { get; set; }
    public Decimal Diferencia { get; set; }
    public string Observaciones { get; set; }
    public string Tienda { get; set; }
    public string Vendedor { get; set; }
    public string VendedorNombre { get; set; }
    public string Gerente { get; set; }
    public string Comentario { get; set; }
    public Decimal PrecioAutorizado { get; set; }
    public Decimal PrecioAutorizadoNivel { get; set; }
    public Decimal Costo { get; set; }
    public Decimal PrecioContado { get; set; }
    public Decimal PctjDescuento { get; set; }
    public Decimal Margen { get; set; }
    public Decimal MargenAutorizar { get; set; }
    public Decimal Factor { get; set; }
    public DateTime FechaVence { get; set; }
    public string Estatus { get; set; }
    public string EstatusDescripcion { get; set; }
    public string Estado { get; set; }
    public string EstadoDescripcion { get; set; }
}

[Serializable]
public class Expediente
{
    public string Cliente { get; set; }
    public string Pedido { get; set; }
    public DateTime Fecha { get; set; }
    public string Vendedor { get; set; }
    public string CodigoVendedor { get; set; }
    public string Tienda { get; set; }
    public string PrimerNombre { get; set; }
    public string SegundoNombre { get; set; }
    public string TercerNombre { get; set; }
    public string PrimerApellido { get; set; }
    public string SegundoApellido { get; set; }
    public string ApellidoCasada { get; set; }
    public string DPI { get; set; }
    public string NIT { get; set; }
    public string Direccion { get; set; }
    public string Colonia { get; set; }
    public string Zona { get; set; }
    public string Departamento { get; set; }
    public string Municipio { get; set; }
    public string TelefonoResidencia { get; set; }
    public string TelefonoCelular { get; set; }
    public string TelefonoTrabajo { get; set; }
    public string Email { get; set; }
    public string EntregarMismaDireccion { get; set; }
    public string NombreEntrega { get; set; }
    public string DireccionEntrega { get; set; }
    public string ColoniaEntrega { get; set; }
    public string ZonaEntrega { get; set; }
    public string DepartamentoEntrega { get; set; }
    public string MunicipioEntrega { get; set; }
    public string EntraCamion { get; set; }
    public string EntraPickup { get; set; }
    public string EsSegundoPiso { get; set; }
    public string Notas { get; set; }
    public string BodegaSale { get; set; }
    public string TiendaSale { get; set; }
    public string ObsVendedor { get; set; }
    public string ObsGerencia { get; set; }
    public string Gerente { get; set; }
    public string Factura { get; set; }
    public string Garantia { get; set; }
    public string Promocion { get; set; }
    public string Financiera { get; set; }
    public string QuienAutorizo { get; set; }
    public string NoAutorizacion { get; set; }
    public decimal Enganche { get; set; }
    public string Plan { get; set; }
    public string NoSolicitud { get; set; }
    public DateTime FechaHora { get; set; }
    public string CantidadPagos1 { get; set; }
    public decimal MontoPagos1 { get; set; }
    public string CantidadPagos2 { get; set; }
    public decimal MontoPagos2 { get; set; }
    public string Despacho { get; set; }
    public DateTime FechaDespacho { get; set; }
    public string Radio { get; set; }
    public string Volante { get; set; }
    public string PrensaLibre { get; set; }
    public string ElQuetzalteco { get; set; }
    public string NuestroDiario { get; set; }
    public string Internet { get; set; }
    public string TV { get; set; }
    public string Otros { get; set; }
    public string ObsOtros { get; set; }
    public string AutorizacionEspecial { get; set; }
    public string ObservacionesAdicionales { get; set; }
    public string IndicacionesLlegar { get; set; }
    public string EntregaAMPM { get; set; }
    public DateTime FechaEntrega { get; set; }
    public string NotasPrecio { get; set; }
    public decimal Factor { get; set; }
    public string Recibos { get; set; }
    public string Requisitos { get; set; }
}

[Serializable]
public class ExpedienteDet
{
    public string Cliente { get; set; }
    public string Pedido { get; set; }
    public string Articulo { get; set; }
    public string Descripcion { get; set; }
    public decimal PrecioLista { get; set; }
    public Int32 Cantidad { get; set; }
    public double Factor { get; set; }
    public decimal PrecioTotal { get; set; }
    public string TipoLinea { get; set; }
    public string Bodega { get; set; }
    public string Armado { get; set; }
}

[Serializable]
public class Kit
{
    public string Articulo { get; set; }
    public string Descripcion { get; set; }
    public Int32 Cantidad { get; set; }
}

[Serializable]
public class Cajeros
{
    public string Cajero { get; set; }
}

[Serializable]
public class Emisores
{
    public Int32 Emisor { get; set; }
    public string Nombre { get; set; }
}

[Serializable]
public class Recibos
{
    public string Documento { get; set; }
    public string Tipo { get; set; }
    public string Cliente { get; set; }
    public DateTime Fecha { get; set; }
    public string Cobrador { get; set; }
    public string Vendedor { get; set; }
    public string Factura { get; set; }
    public decimal Efectivo { get; set; }
    public decimal Cheque { get; set; }
    public decimal Tarjeta { get; set; }
    public decimal Vale_descuento { get; set; }
    public decimal Monto { get; set; }
    public string NumeroCheque { get; set; }
    public string EntidadFinancieraCheque { get; set; }
    public Int32 EmisorTarjeta { get; set; }
    public string EntidadFinancieraTarjeta { get; set; }
    public string EsAnticipo { get; set; }
    public decimal Deposito { get; set; }
    public string EntidadFinancieraDeposito { get; set; }
    public string Observaciones { get; set; }
    public string MontoLetras { get; set; }
    public string Direccion { get; set; }
    public string Telefono { get; set; }
    public string NombreCliente { get; set; }
    public string NombreBancoCheque { get; set; }
    public string Pos { get; set; }
    public string DireccionEmpresa { get; set; }
    public string Usuario { get; set; }
    public DateTime FechaRegistro { get; set; }
    public string Moneda { get; set; }
    public decimal Tasa { get; set; }
    public decimal SaldoFactura { get; set; }
}

[Serializable]
public class Poss
{
    public string Pos { get; set; }
    public string Nombre { get; set; }
}

[Serializable]
public class PromoLaTorre
{
    public string Tipo { get; set; }
    public decimal Valor { get; set; }
    public decimal CompraMinima { get; set; }
    public DateTime FechaVence { get; set; }
    public decimal DescuentoNormal { get; set; }
    public decimal DescuentoOfertado { get; set; }
}

[Serializable]
public class Configura
{
    public string MailOficina1 { get; set; }
    public string MailOficina2 { get; set; }
    public string MailOficina3 { get; set; }
    public string MailBodega1 { get; set; }
    public string MailBodega2 { get; set; }
    public string MailBodega3 { get; set; }
    public string MailArmados1 { get; set; }
    public string MailArmados2 { get; set; }
    public string MailArmados3 { get; set; }
}

[Serializable]
public class Kits
{
    public string Articulo { get; set; }
    public string Descripcion { get; set; }
    public Nullable<Int32> Cantidad { get; set; }
    public string Tipo { get; set; }
}

[Serializable]
public class ModificarPrecios
{
    public string CambiarPrecioHaciaAbajo { get; set; }
    public decimal MaximoPrecioHaciaAbajo { get; set; }
    public string CambiarPrecioHaciaArriba { get; set; }
    public decimal MaximoPrecioHaciaArriba { get; set; }
}

[Serializable]
public class ArticulosRegalo
{
    public string Articulo { get; set; }
    public string Activo { get; set; }
    public string Descripcion { get; set; }
    public DateTime FechaInicial { get; set; }
    public Nullable<DateTime> FechaFinal { get; set; }
    public string Observaciones { get; set; }
    public decimal CompraMinima { get; set; }
    public string IncluyeProductoOfertado { get; set; }
    public string Tipo { get; set; }
    public string ArticuloReemplaza { get; set; }
    public string EliminarSiNoHay { get; set; }
    public decimal CompraMinimaNR { get; set; }
    public decimal Financiera { get; set; }
    public decimal Financiera2 { get; set; }
    public decimal CompraMinima2 { get; set; }
    public string TipoValidacion { get; set; }
}

[Serializable]
public class ArticulosReemplazo
{
    public string Articulo1 { get; set; }
    public string Descripcion1 { get; set; }
    public string Articulo2 { get; set; }
    public string Descripcion2 { get; set; }
}

[Serializable]
public class Bancos
{
    public string Banco { get; set; }
    public string Descripcion { get; set; }
}

[Serializable]
public class FacturasCliente
{
    public string Factura { get; set; }
    public string Descripcion { get; set; }
    public decimal Valor { get; set; }
}

[Serializable]
public class RecibosDepositar
{
    public string Recibo { get; set; }
    public DateTime Fecha { get; set; }
    public decimal Efectivo { get; set; }
    public decimal Cheque { get; set; }
    public decimal Tarjeta { get; set; }
    public string Pos { get; set; }
    public string PosNombre { get; set; }
    public int Archivos { get; set; }
}

[Serializable]
public class DepositosCerrar
{
    public string Cuenta { get; set; }
    public string TipoDocumento { get; set; }
    public decimal Numero { get; set; }
    public string Banco { get; set; }
    public DateTime Fecha { get; set; }
    public decimal Monto { get; set; }
    public int Archivos { get; set; }
}

[Serializable]
public class Cierres
{
    public Int32 Cierre { get; set; }
    public string Fecha { get; set; }
}

[Serializable]
public class CuentasBancarias
{
    public string Cuenta { get; set; }
    public string Nombre { get; set; }
}

[Serializable]
public class AutorizacionCotizacion
{
    public string Vendedor { get; set; }
    public string Tienda { get; set; }
    public string Cliente { get; set; }
    public string Notas { get; set; }
    public Decimal Costo { get; set; }
    public Decimal FacturaActual { get; set; }
    public Decimal Facturar { get; set; }
    public Decimal Diferencia { get; set; }
    public Decimal Margen { get; set; }
    public Decimal MargenAutorizar { get; set; }
    public string Promocion { get; set; }
    public string Financiera { get; set; }
    public string NivelPrecio { get; set; }
    public string Estatus { get; set; }
    public string Usuario { get; set; }
    public string Observaciones { get; set; }
    public string Gerente { get; set; }
}

[Serializable]
public class CotizacionesPendientes
{
    public int Cotizacion { get; set; }
    public Decimal FacturaSolicitada { get; set; }
    public string Estatus { get; set; }
    public string Nombre { get; set; }
    public string ObservacionesSolicitud { get; set; }
    public string Gerente { get; set; }
    public string ObservacionesAutoriza { get; set; }
    public string Cliente { get; set; }
    public string TipoVenta { get; set; }
    public int Financiera { get; set; }
    public string Nivel { get; set; }

}

[Serializable]
public class VendedorGenerales
{
    public string Vendedor { get; set; }
    public string Celular { get; set; }
    public string Correo { get; set; }
    public string TextoCotizacion { get; set; }
    public string CodigoInterconsumo { get; set; }
}

[Serializable]
public class Kardex
{
    public string Articulo { get; set; }
    public string Descripcion { get; set; }
    public DateTime FechaInicial { get; set; }
    public DateTime FechaFinal { get; set; }
    public string Localizacion { get; set; }
    public DateTime Fecha { get; set; }
    public string Documento { get; set; }
    public string Bodega { get; set; }
    public string Tipo { get; set; }
    public int Cantidad { get; set; }
    public int Saldo { get; set; }
}

[Serializable]
public class LoteCierre
{
    public int Lote { get; set; }
    public Decimal Monto { get; set; }
}

[Serializable]
public class BodegasInventario
{
    public string Tienda { get; set; }
    public string Cerrado { get; set; }
}

[Serializable]
public class Gerentes
{
    public string mail { get; set; }
}

[Serializable]
public class Transportistas
{
    public Int32 Transportista { get; set; }
    public string Nombre { get; set; }
}

[Serializable]
public class SeriesDespachos
{
    public string Serie { get; set; }
    public string Nombre { get; set; }
}

[Serializable]
public class Tasa
{
    public decimal TasaCambio { get; set; }
    public DateTime Fecha { get; set; }
    public string Activa { get; set; }
    public string Usuario { get; set; }
}

[Serializable]
public class Profesiones
{
    public string Profesion { get; set; }
}

[Serializable]
public class Puestos
{
    public string Puesto { get; set; }
}

[Serializable]
public class EnvioBodega
{
    public string Envio { get; set; }
    public int Linea { get; set; }
    public string Articulo { get; set; }
    public string Descripcion { get; set; }
    public int Cantidad { get; set; }
    public string BodegaOriginal { get; set; }
    public string BodegaNueva { get; set; }
    public decimal CostoLocal { get; set; }
    public decimal CostoDolar { get; set; }
    public decimal Factura { get; set; }
    public string Cliente { get; set; }
    public string NombreCliente { get; set; }
    public string Localizacion { get; set; }
}

[Serializable]
public class DocumentosDespachar
{
    public string Factura { get; set; }
    public DateTime Fecha { get; set; }
    public string Cliente { get; set; }
    public string NombreCliente { get; set; }
    public decimal Monto { get; set; }
    public string Pedido { get; set; }
    public string Aprobada { get; set; }
    public string Tienda { get; set; }
}

[Serializable]
public class DocumentoDespachar
{
    public string Factura { get; set; }
    public Int32 Linea { get; set; }
    public string Articulo { get; set; }
    public string Articulo2 { get; set; }
    public string Descripcion { get; set; }
    public decimal Cantidad { get; set; }
    public string Bodega { get; set; }
    public string Localizacion { get; set; }
    public string Tooltip { get; set; }
    public Int32 Existencias { get; set; }
    public string Tipo { get; set; }
    public string Cliente { get; set; }
    public string Cobrador { get; set; }
    public decimal CostoTotal { get; set; }
    public decimal CostoTotalDolar { get; set; }
    public Nullable<Int32> LineaOrigen { get; set; }
    public decimal PrecioTotal { get; set; }
    public string Telefonos { get; set; }
    public string Pedido { get; set; }
    public string Vendedor { get; set; }
    public string ObservacionesVendedor { get; set; }
    public DateTime FechaEntrega { get; set; }
    public Int32 PedidoLinea { get; set; }
}

[Serializable]
public class DocumentoFacturar
{
    public string Pedido { get; set; }
    public Int32 Linea { get; set; }
    public string Articulo { get; set; }
    public string Descripcion { get; set; }
    public decimal Cantidad { get; set; }
    public string Bodega { get; set; }
    public string Localizacion { get; set; }
    public string Tooltip { get; set; }
    public Int32 Existencias { get; set; }
    public string Tipo { get; set; }
    public string Cliente { get; set; }
    public DateTime FechaEntrega { get; set; }
    public decimal PrecioUnitario { get; set; }
    public decimal PrecioTotal { get; set; }
    public decimal PrecioBase { get; set; }
    public string Observaciones { get; set; }
    public decimal Impuesto { get; set; }
    public string DescipcionArticulo { get; set; }
    public decimal Monto_Mescuento { get; set; }
}

[Serializable]
public class FacturaRevisar
{
    public string Factura { get; set; }
    public Int32 Linea { get; set; }
    public string Articulo { get; set; }
    public string Descripcion { get; set; }
    public decimal PrecioUnitario { get; set; }
    public decimal Cantidad { get; set; }
    public decimal PrecioTotal { get; set; }
}

[Serializable]
public class ArticulosBuscar
{
    public string Articulo { get; set; }
    public string Descripcion { get; set; }
}

[Serializable]
public class DocumentosDespachados
{
    public string Despacho { get; set; }
    public DateTime Fecha { get; set; }
    public DateTime FechaEntrega { get; set; }
    public Int32 Transportista { get; set; }
    public string NombreTransportista { get; set; }
    public string Cliente { get; set; }
    public string NombreCliente { get; set; }
    public string Tienda { get; set; }
    public string Zona { get; set; }
    public string Municipio { get; set; }
    public string Departamento { get; set; }
    public string Direccion { get; set; }
    public Int32 Articulos { get; set; }
    public string Consecutivo { get; set; }
}

[Serializable]
public class DespachoFactura
{
    public string Despacho { get; set; }
    public string Factura { get; set; }
}

[Serializable]
public class Comisiones
{
    public string NombreVendedor { get; set; }
    public decimal Valor { get; set; }
    public decimal MontoComisionalbe { get; set; }
    public decimal ValorNeto { get; set; }
    public decimal Comision { get; set; }
    public decimal Porcentaje { get; set; }
    public string Vendedor { get; set; }
    public string Tienda { get; set; }
}

[Serializable]
public class ComisionesDocumentos
{
    public string Factura { get; set; }
    public decimal Valor { get; set; }
    public decimal MontoComisionalbe { get; set; }
    public decimal ValorNeto { get; set; }
    public decimal Comision { get; set; }
    public decimal Porcentaje { get; set; }
    public DateTime FechaFactura { get; set; }
    public DateTime FechaEntrega { get; set; }
    public string Vendedor { get; set; }
    public string NombreVendedor { get; set; }
    public string Tienda { get; set; }
    public string Tipo { get; set; }
    public string Pedido { get; set; }
    public string Despachada { get; set; }
}

[Serializable]
public class Empresas
{
    public string Empresa { get; set; }
}

[Serializable]
public class Certificados
{
    public string Vale { get; set; }
    public string Beneficiario { get; set; }
    public string Comprador { get; set; }
    public DateTime FechaCompra { get; set; }
    public DateTime FechaVence { get; set; }
    public decimal Valor { get; set; }
}

