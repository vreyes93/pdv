﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

[Serializable]
public class ArticulosMayoreo
{
    public string Articulo { get; set; }
    public string Descripcion { get; set; }
    public Int32 Cantidad { get; set; }
    public double PrecioBase { get; set; }
    public decimal PrecioLista { get; set; }
    public string TieneRegalo { get; set; }
    public string Regalo { get; set; }
    public Int32 CantidadRegalo { get; set; }
    public string Tipo { get; set; }
}

