﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FiestaNETServices.Services
{
    public class Patas
    {
        public string cantidad { get; set; }
        public string modelo { get; set; }
        public bool incluye { get; set; }
        public string descripcion { get; set; }

        public string modeloPatas(dbDataContext db, string articulo, log4net.ILog log)
        {
            try
            {
                string modelo = string.Empty;
                modelo = (from p in db.ARTICULO_ESPEs where p.ARTICULO == articulo && p.ATRIBUTO == "MODELO_PATAS" select p).First().VALOR;
                return modelo;
            }
            catch (Exception ex)
            {
                log.Error("Ocurrio un error al consultar a la tabla ARTICULO_ESPE: " + ex.ToString());
                throw new Exception("Ocurrio un problema al consultar a la tabla ARTICULO_ESPE");
            }
        }

        public string cantidadPatas(dbDataContext db, string articulo, log4net.ILog log)
        {
            try
            {
                string cantidad = string.Empty;
                cantidad = (from p in db.ARTICULO_ESPEs where p.ARTICULO == articulo && p.ATRIBUTO == "CANTIDAD_PATAS" select p).First().VALOR;
                return cantidad;
            }
            catch (Exception ex)
            {
                log.Error("Ocurrio un error al consultar a la tabla ARTICULO_ESPE: " + ex.ToString());
                throw new Exception("Ocurrio un problema al consultar a la tabla ARTICULO_ESPE");
            }
        }

        public bool incluyePatas(dbDataContext db, string articulo, log4net.ILog log)
        {

            try
            {
                bool incluye = true;
                String xincluye = String.Empty;
                try
                {
                    xincluye = (from p in db.ARTICULO_ESPEs where p.ARTICULO == articulo && p.ATRIBUTO == "INCLUYE_PATAS" select p).First().VALOR;
                }
                catch
                {
                    //si no existe el registro, por defecto incluye patas.                
                }

                if (xincluye.Equals("N"))
                {
                    incluye = false;
                }

                return incluye;
            }
            catch (Exception ex)
            {
                log.Error("Ocurrio un error al consultar a la tabla ARTICULO_ESPE: " + ex.ToString());
                throw new Exception("Ocurrio un problema al consultar a la tabla ARTICULO_ESPE");
            }
        }

        public string descripcionPatas(dbDataContext db, string articuloHijo, log4net.ILog log)
        {
            try
            {
                string descripcion = string.Empty;
                this.descripcion = (from p in db.ARTICULOs where p.ARTICULO1 == articuloHijo select p).First().DESCRIPCION;
                return descripcion;
            }
            catch (Exception ex)
            {
                log.Error("Ocurrio un error al consultar a la tabla ARTICULO_ESPE: " + ex.ToString());
                throw new Exception("Ocurrio un problema al consultar a la tabla ARTICULO_ESPE");
            }
        }

        public void todosLosAtributos(dbDataContext db, string articulo, log4net.ILog log)
        {
            try
            {
                this.modelo = (from p in db.ARTICULO_ESPEs where p.ARTICULO == articulo && p.ATRIBUTO == "MODELO_PATAS" select p).First().VALOR;
                this.cantidad = Convert.ToInt32((from p in db.ARTICULO_ESPEs where p.ARTICULO == articulo && p.ATRIBUTO == "CANTIDAD_PATAS" select p).First().NORMAL).ToString();

                //asignacion de valor incluye patas
                string xIncluye = string.Empty;

                try
                {
                    xIncluye = (from p in db.ARTICULO_ESPEs where p.ARTICULO == articulo && p.ATRIBUTO == "INCLUYE_PATAS" select p).First().VALOR;
                }
                catch (Exception ex)
                {
                    //Si no se encuentra la configuracion en base de datos por defecto incluye patas
                    xIncluye = "S";
                    log.Info("No se encontro la configuracion incluye_patas en base de datos, por defecto incluye exepcio: " + ex.ToString());
                }

                if (!xIncluye.Equals("S"))
                { this.incluye = false; }
                else
                { this.incluye = true; }

                this.descripcion = (from p in db.ARTICULOs where p.ARTICULO1 == modelo select p).First().DESCRIPCION;
            }
            catch (Exception ex)
            {
                log.Error("No se encontró la configuración incluye_patas para el artículo: "+articulo);
                throw new Exception("Articulo no configurado en la tabla ARTICULO_ESPE");
            }
        }
        public void todosLosAtributosSets(dbDataContext db, string articulo, log4net.ILog log)
        {
            String identificadorBase = string.Empty;
            int cantidadBases = 0;

            //buscando el articulo hijo que contiene el identificador de la base
            var resultado = (from p in db.ARTICULO_ENSAMBLEs where p.ARTICULO_PADRE == articulo select p);
            foreach (var item in resultado)
            {
                String xbase = item.ARTICULO_HIJO.Substring(0, 3);
                String xbaseM = item.ARTICULO_HIJO.Substring(0, 4);

                if (xbase.Equals("142") || xbaseM.Equals("M142"))
                {
                    identificadorBase = item.ARTICULO_HIJO;
                    cantidadBases = Decimal.ToInt32(item.CANTIDAD);
                }
            }


            //asignacion de incluye patas
            string xIncluye = string.Empty;
            try
            {
                xIncluye = (from p in db.ARTICULO_ESPEs where p.ARTICULO == identificadorBase && p.ATRIBUTO == "INCLUYE_PATAS" select p).First().VALOR;
                if (xIncluye.Equals("N")) 
                {
                    this.incluye = false;
                }
                else
                {
                    this.incluye = true;
                }
            }
            catch (Exception ex)
            {
                //Si no se encuentra la configuracion incluye_patas en base de datos por defecto incluye patas
                xIncluye = "S";
                this.incluye = true;
                log.Info("No se encontro la configuracion incluye_patas en base de datos, por defecot incluye patas, exeption: " + ex.ToString() + ", para mas informacion comuniquese con el departamento de importaciones.");
            }

            if (xIncluye.Equals("N"))
            {
                //No incluye patas
            }
            else
            {
                try
                {

                    if (resultado.Count() > 0)
                    {

                        try 
                        {
                            this.modelo = (from p in db.ARTICULO_ESPEs where p.ARTICULO == identificadorBase && p.ATRIBUTO == "MODELO_PATAS" select p).First().VALOR;
                        }
                        catch 
                        {
                            String xbase = identificadorBase.Substring(0, 1);
                            if (xbase.Equals("M"))
                            {
                                //quitando la m del inicio
                                identificadorBase = identificadorBase.Replace("M", "");
                                this.modelo = (from p in db.ARTICULO_ESPEs where p.ARTICULO == identificadorBase && p.ATRIBUTO == "MODELO_PATAS" select p).First().VALOR;
                            }
                            else
                            {
                                //colocando una M al incio de la cadena que identifica la base
                                identificadorBase = "M" + identificadorBase;
                                this.modelo = (from p in db.ARTICULO_ESPEs where p.ARTICULO == identificadorBase && p.ATRIBUTO == "MODELO_PATAS" select p).First().VALOR;
                            }
                        }

                        


                        this.cantidad = Convert.ToInt32(((from p in db.ARTICULO_ESPEs where p.ARTICULO == identificadorBase && p.ATRIBUTO == "CANTIDAD_PATAS" select p).First().NORMAL) * cantidadBases).ToString();

                        this.descripcion = (from p in db.ARTICULOs where p.ARTICULO1 == modelo select p).First().DESCRIPCION;
                    }
                }
                catch (Exception ex)
                {
                    //log.Error("Articulo no configurado en la tabla ARTICULO_ESPE: " + ex.ToString());
                    log.Error(" No se encontró la configuración incluye_patas para el artículo: " + identificadorBase);
                    throw new Exception(" No se encontró la configuración incluye_patas para el artículo: "+ identificadorBase);
                }

                //asignacion de incluye patas
                try
                {
                    xIncluye = (from p in db.ARTICULO_ESPEs where p.ARTICULO == identificadorBase && p.ATRIBUTO == "INCLUYE_PATAS" select p).First().VALOR;
                }
                catch (Exception ex)
                {
                    //Si no se encuentra la configuracion incluye_patas en base de datos por defecto incluye patas
                    xIncluye = "S";
                    log.Info("No se encontro la configuracion incluye_patas en base de datos, por defecot incluye patas, exeption: " + ex.ToString());
                }
            }
        }
    }
}