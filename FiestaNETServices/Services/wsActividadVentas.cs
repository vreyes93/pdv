﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using FiestaNETServices.Services;

/// <summary>
/// Summary description for wsActividadVentas
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class wsActividadVentas : System.Web.Services.WebService {

    [WebMethod]
    public ReporteActividadVentas[] ReporteActividadVentas(DateTime FechaInicial, DateTime FechaFinal, string Proveedor, string Articulo, string NombreProveedor, string NombreArticulo, string Tipo)
    {
        return DataLayerActividadVentas.ReporteActividadVentas(FechaInicial, FechaFinal, Proveedor, Articulo, NombreProveedor, NombreArticulo, Tipo).ToArray();
    }

    [WebMethod]
    public Clasificaciones[] ListarClasificacionesCriterio(string clasificacion, string descripcion)
    {
        return DataLayerActividadVentas.ListarClasificacionesCriterio(clasificacion, descripcion).ToArray();
    }
}
