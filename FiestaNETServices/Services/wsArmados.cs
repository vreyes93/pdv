﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Transactions;
using FiestaNETServices.Services;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Text;
   
/// <summary>
/// Summary description for wsArmados
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class wsArmados : System.Web.Services.WebService
{

    [WebMethod]
    public DataSet DevuelveSistemas()
    {
        dbDataContext db = new dbDataContext();

        string mAmbiente = "DES";

        if (db.Connection.ConnectionString.Contains("sql.fiesta.local")) mAmbiente = "PRO";
        if (db.Connection.ConnectionString.Contains("PRUEBAS")) mAmbiente = "PRU";

        string mServidor = "(local)";
        string mBase = "EXACTUSERP";

        if (mAmbiente == "PRO")
        {
            mBase = "EXACTUSERP";
            mServidor = "sql.fiesta.local";
        }

        if (mAmbiente == "PRU")
        {
            mBase = "PRUEBAS";
            mServidor = "sql.fiesta.local";
        }

        string mStringConexion = string.Format("Data Source={0}; Initial Catalog = {1};User id = mf.fiestanet;password = 54321; Max Pool Size=600;Pooling=false;Application Name=PDV", mServidor, mBase);
        SqlConnection mConexion = new SqlConnection(mStringConexion);

        SqlDataAdapter da = new SqlDataAdapter("SELECT SISTEMA FROM prodmult.MF_SistemaArmados WHERE ACTIVO = 'S' ORDER BY SISTEMA", mConexion);
        DataTable dt = new DataTable("sistemas");

        da.Fill(dt);

        DataSet ds = new DataSet();
        ds.Tables.Add(dt);

        return ds;
    }

    [WebMethod]
    public bool DevuelveRequisiciones(string usuario, ref string mensaje, DateTime fechaInicial, DateTime fechaFinal, string tipo, ref DataSet ds, string fechaValidar)
    {
        bool mRetorna = true;
        dbDataContext db = new dbDataContext();
        
        try
        {
            string mAmbiente = "DES";

            if (db.Connection.ConnectionString.Contains("sql.fiesta.local")) mAmbiente = "PRO";
            if (db.Connection.ConnectionString.Contains("PRUEBAS")) mAmbiente = "PRU";

            string mServidor = "(local)";
            string mBase = "EXACTUSERP";

            if (mAmbiente == "PRO")
            {
                mBase = "EXACTUSERP";
                mServidor = "sql.fiesta.local";
            }

            if (mAmbiente == "PRU")
            {
                mBase = "PRUEBAS";
                mServidor = "sql.fiesta.local";
            }

            SqlCommand mCommand = new SqlCommand(); string mFechaValidar = "A";
            string mStringConexion = string.Format("Data Source={0}; Initial Catalog = {1};User id = mf.fiestanet;password = 54321; Max Pool Size=600;Pooling=false;Application Name=PDV", mServidor, mBase);

            //string mEsCiudad = "S";
            //if (tipo == "Departamentos") mEsCiudad = "N";
            if (fechaValidar == "Fecha Transporte") mFechaValidar = "T";

            if (!ds.Tables.Contains("requisiciones")) ds.Tables.Add(new DataTable("requisiciones"));
            if (!ds.Tables.Contains("requisicionesDet")) ds.Tables.Add(new DataTable("requisicionesDet"));
            
            ds.Tables["requisiciones"].Clear();
            ds.Tables["requisicionesDet"].Clear();

            bool mHayRequisiciones = false;
            DataTable dtEnv = new DataTable("requisicionesEnv");
            fechaFinal = new DateTime(fechaFinal.Year, fechaFinal.Month, fechaFinal.Day, 23, 59, 59);
            
            using (SqlConnection conexion = new SqlConnection(mStringConexion))
            {
                conexion.Open();
                mCommand.Connection = conexion;
                mCommand.CommandTimeout = 999999999;

                SqlDataAdapter da = new SqlDataAdapter("EXEC prodmult.spRequisiciones @fechaInicial, @fechaFinal, @Region, @validar, @usuario", conexion);

                da.SelectCommand.Parameters.Add("@fechaInicial", SqlDbType.DateTime).Value = fechaInicial;
                da.SelectCommand.Parameters.Add("@fechaFinal", SqlDbType.DateTime).Value = fechaFinal;
                da.SelectCommand.Parameters.Add("@Region", SqlDbType.VarChar).Value = tipo;
                da.SelectCommand.Parameters.Add("@validar", SqlDbType.VarChar).Value = mFechaValidar;
                da.SelectCommand.Parameters.Add("@usuario", SqlDbType.VarChar).Value = usuario;

                da.SelectCommand.CommandTimeout = 999999999;
                da.Fill(ds, "requisiciones");

                for (int ii = 0; ii < ds.Tables["requisiciones"].Rows.Count; ii++)
                {
                    mHayRequisiciones = true;
                    Int32 mRequisicion = 0;
                    try
                    {
                        mRequisicion = Convert.ToInt32(ds.Tables["requisiciones"].Rows[ii]["Requisicion"]);
                    }
                    catch
                    {
                        mRequisicion = 0;
                    }
                    
                    string mFactura = Convert.ToString(ds.Tables["requisiciones"].Rows[ii]["Factura"]);
                    mCommand.CommandText = string.Format("SELECT ANULADA FROM prodmult.FACTURA WHERE FACTURA = '{0}'", mFactura);
                    string mAnulada = Convert.ToString(mCommand.ExecuteScalar());

                    mCommand.CommandText = string.Format("SELECT SE_ANULARA FROM prodmult.MF_Factura WHERE FACTURA = '{0}'", mFactura);
                    mAnulada = Convert.ToString(mCommand.ExecuteScalar());

                    if (mAnulada == "S") ds.Tables["requisiciones"].Rows[ii]["Tipo"] = "Factura Anulada";

                    if (mRequisicion > 0)
                    {
                        SqlDataAdapter daDet = new SqlDataAdapter(string.Format("SELECT a.NO_REQUISICION AS Requisicion, 'Sí' AS Armado, c.COBRADOR AS Bodega, a.ARTICULO AS Articulo, b.DESCRIPCION AS Descripcion, 1 AS Cantidad, 'Garantía Normal' AS TipoGarantia " +
                            "FROM prodmult.MF_PedidoArmadoLinea a JOIN prodmult.ARTICULO b ON a.ARTICULO = b.ARTICULO JOIN prodmult.MF_PedidoArmado c ON a.PEDIDO = c.PEDIDO " + 
                            "WHERE a.NO_REQUISICION = {0}", ds.Tables["requisiciones"].Rows[ii]["Requisicion"]), conexion);
                        daDet.SelectCommand.CommandTimeout = 999999999;
                        daDet.Fill(ds, "requisicionesDet");

                        SqlDataAdapter daPedido = new SqlDataAdapter(string.Format("SELECT a.NO_REQUISICION AS Requisicion, (CASE b.REQUISICION_ARMADO WHEN 'S' THEN 'Sí' ELSE 'No' END) AS Armado, b.BODEGA AS Bodega, " +
                            "b.ARTICULO AS Articulo, c.DESCRIPCION AS Descripcion, b.CANTIDAD_PEDIDA AS Cantidad, (CASE b.GEL WHEN 'S' THEN 'Garantía Especial Limitada' ELSE 'Garantía Normal' END) AS TipoGarantia " +
                            "FROM prodmult.MF_PedidoArmado a JOIN prodmult.MF_Pedido_Linea b ON a.PEDIDO = b.PEDIDO JOIN prodmult.ARTICULO c ON b.ARTICULO = c.ARTICULO " +
                            "WHERE a.NO_REQUISICION = {0}", ds.Tables["requisiciones"].Rows[ii]["Requisicion"]), conexion);
                        daPedido.SelectCommand.CommandTimeout = 999999999;
                        daPedido.Fill(ds, "requisicionesDet");
                    }
                    
                    if (Convert.ToInt32(ds.Tables["requisiciones"].Rows[ii]["Req"]) < 0 && Convert.ToString(ds.Tables["requisiciones"].Rows[ii]["Envio"]).Substring(0, 2) != "TP")
                    {
                        SqlDataAdapter daEnvio = new SqlDataAdapter(string.Format("SELECT DISTINCT {0} AS Requisicion, 'No' AS Armado, a.BODEGA AS Bodega, a.ARTICULO AS Articulo, b.DESCRIPCION AS Descripcion, a.CANTIDAD AS Cantidad, " +
                            "(CASE d.GEL WHEN 'S' THEN 'Garantía Especial Limitada' ELSE 'Garantía Normal' END) AS TipoGarantia " +
                            "FROM prodmult.DESPACHO_DETALLE a JOIN prodmult.ARTICULO b ON a.ARTICULO = b.ARTICULO JOIN prodmult.DESPACHO e ON a.DESPACHO = e.DESPACHO " +
                            "JOIN prodmult.FACTURA_LINEA c ON a.DOCUM_ORIG = c.FACTURA JOIN prodmult.MF_Pedido_Linea d ON c.PEDIDO = d.PEDIDO AND c.PEDIDO_LINEA = d.PEDIDO_LINEA " +
                            "WHERE a.DESPACHO = '{1}' AND e.ESTADO = 'D' ", ds.Tables["requisiciones"].Rows[ii]["Req"], ds.Tables["requisiciones"].Rows[ii]["Envio"]), conexion);
                        daEnvio.SelectCommand.CommandTimeout = 999999999;
                        daEnvio.Fill(ds, "requisicionesDet");
                    }
                    
                    if (Convert.ToInt32(ds.Tables["requisiciones"].Rows[ii]["Req"]) < 0 && Convert.ToString(ds.Tables["requisiciones"].Rows[ii]["Envio"]).Substring(0, 2) == "TP")
                    {
                        Int32 mAuditTrans = Convert.ToInt32(ds.Tables["requisiciones"].Rows[ii]["Req"]) * -1;
                        SqlDataAdapter daEnvio = new SqlDataAdapter(string.Format("SELECT {0} AS Requisicion, 'No' AS Armado, 'F01' AS Bodega, a.ARTICULO AS Articulo, b.DESCRIPCION AS Descripcion, a.CANTIDAD AS Cantidad, '' AS TipoGarantia " +
                            "FROM prodmult.TRANSACCION_INV a JOIN prodmult.ARTICULO b ON a.ARTICULO = b.ARTICULO WHERE a.AUDIT_TRANS_INV = {1} AND a.NATURALEZA = 'E'", ds.Tables["requisiciones"].Rows[ii]["Req"], mAuditTrans), conexion);
                        daEnvio.SelectCommand.CommandTimeout = 999999999;
                        daEnvio.Fill(ds, "requisicionesDet");
                    }
                    
                    if (Convert.ToString(ds.Tables["requisiciones"].Rows[ii]["Tipo"]) == "Factura Anulada")
                    {
                        SqlDataAdapter daFactura = new SqlDataAdapter(string.Format("SELECT {0} AS Requisicion, 'No' AS Armado, a.BODEGA AS Bodega, a.ARTICULO AS Articulo, b.DESCRIPCION AS Descripcion, a.CANTIDAD AS Cantidad, '' AS TipoGarantia " +
                            "FROM prodmult.FACTURA_LINEA a JOIN prodmult.ARTICULO b ON a.ARTICULO = b.ARTICULO WHERE a.FACTURA = '{1}'", ds.Tables["requisiciones"].Rows[ii]["Req"], ds.Tables["requisiciones"].Rows[ii]["Factura"]), conexion);
                        daFactura.SelectCommand.CommandTimeout = 999999999;
                        daFactura.Fill(ds, "requisicionesDet");
                    }
                }

                conexion.Close();
            }

            if (mHayRequisiciones)
            {
                DataColumn keyColumn = ds.Tables["requisiciones"].Columns["Req"];
                DataColumn foreignKeyColumn = ds.Tables["requisicionesDet"].Columns["Requisicion"];
                ds.Relations.Add("Detalle", keyColumn, foreignKeyColumn);
            }
                
        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }

            mRetorna = false;
            mensaje = string.Format("Error al devolver las requisiciones WS. {0} {1} {2}", ex.Message, m, mensaje);
        }

        return mRetorna;
    }
    
    [WebMethod]
    public bool MarcaGrabadaRequisicion(ref string mensaje, ref string grabada, string requisicion, string envio)
    {
        bool mRetorna = true;
        dbDataContext db = new dbDataContext();
        
        try
        {
            string mAmbiente = "DES";

            if (db.Connection.ConnectionString.Contains("sql.fiesta.local")) mAmbiente = "PRO";
            if (db.Connection.ConnectionString.Contains("PRUEBAS")) mAmbiente = "PRU";

            string mServidor = "(local)";
            string mBase = "EXACTUSERP";

            if (mAmbiente == "PRO")
            {
                mBase = "EXACTUSERP";
                mServidor = "sql.fiesta.local";
            }

            if (mAmbiente == "PRU")
            {
                mBase = "PRUEBAS";
                mServidor = "sql.fiesta.local";
            }

            SqlCommand mCommand = new SqlCommand(); SqlTransaction mTransaction = null;
            string mStringConexion = string.Format("Data Source={0}; Initial Catalog = {1};User id = mf.fiestanet;password = 54321; Max Pool Size=600;Pooling=false;Application Name=PDV", mServidor, mBase);

            using (SqlConnection conexion = new SqlConnection(mStringConexion))
            {
                conexion.Open();
                mTransaction = conexion.BeginTransaction();

                mCommand.Connection = conexion;
                mCommand.Transaction = mTransaction;

                Int32 mRequisicion = Convert.ToInt32(requisicion);
                string mSelect = string.Format("SELECT GRABADA FROM prodmult.MF_PedidoArmado WHERE NO_REQUISICION = {0}", requisicion);

                if (mRequisicion < 0)
                {
                    if (envio.Substring(0, 2) == "TP")
                    {
                        mSelect = string.Format("SELECT REFERENCIA FROM prodmult.AUDIT_TRANS_INV WHERE AUDIT_TRANS_INV = {0}", mRequisicion * -1);
                    }
                    else
                    {
                        mSelect = string.Format("SELECT NOTAS_TRANSPORTE FROM prodmult.DESPACHO WHERE DESPACHO = '{0}'", envio);
                    }
                }

                mCommand.CommandText = mSelect;
                string mGrabada = mCommand.ExecuteScalar().ToString();

                string mNuevaGrabada = "S";
                if (mGrabada == "S") mNuevaGrabada = "N";

                string mUpdate = string.Format("UPDATE prodmult.MF_PedidoArmado SET GRABADA = '{0}' WHERE NO_REQUISICION = {1}", mNuevaGrabada, requisicion);

                if (mRequisicion < 0)
                {
                    if (envio.Substring(0, 2) == "TP")
                    {
                        mUpdate = string.Format("UPDATE prodmult.AUDIT_TRANS_INV SET REFERENCIA = '{0}' WHERE AUDIT_TRANS_INV = {1}", mNuevaGrabada, mRequisicion * -1);
                    }
                    else
                    {
                        mUpdate = string.Format("UPDATE prodmult.DESPACHO SET NOTAS_TRANSPORTE = '{0}' WHERE DESPACHO = '{1}'", mNuevaGrabada, envio);
                    }
                }

                mCommand.CommandText = mUpdate;
                mCommand.ExecuteNonQuery();

                grabada = "Sí";
                if (mNuevaGrabada == "N") grabada = "No";

                mTransaction.Commit();
                conexion.Close();
            }

        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }

            mRetorna = false;
            mensaje = string.Format("Error al marcar como grabada la requisición WS. {0} {1}", ex.Message, m);
        }

        return mRetorna;
    }

    [WebMethod]
    public bool CambiarTipoRequisicion(ref string mensaje, string requisicion, string tipo)
    {
        bool mRetorna = true;
        dbDataContext db = new dbDataContext();

        try
        {
            string mAmbiente = "DES";

            if (db.Connection.ConnectionString.Contains("sql.fiesta.local")) mAmbiente = "PRO";
            if (db.Connection.ConnectionString.Contains("PRUEBAS")) mAmbiente = "PRU";

            string mServidor = "(local)";
            string mBase = "EXACTUSERP";

            if (mAmbiente == "PRO")
            {
                mBase = "EXACTUSERP";
                mServidor = "sql.fiesta.local";
            }

            if (mAmbiente == "PRU")
            {
                mBase = "PRUEBAS";
                mServidor = "sql.fiesta.local";
            }

            SqlCommand mCommand = new SqlCommand(); SqlTransaction mTransaction = null;
            string mStringConexion = string.Format("Data Source={0}; Initial Catalog = {1};User id = mf.fiestanet;password = 54321; Max Pool Size=600;Pooling=false;Application Name=PDV", mServidor, mBase);

            using (SqlConnection conexion = new SqlConnection(mStringConexion))
            {
                conexion.Open();
                mTransaction = conexion.BeginTransaction();

                mCommand.Connection = conexion;
                mCommand.Transaction = mTransaction;

                string mTipo = tipo;
                //if (tipo == "Armado en tienda") mTipo = "A";
                //if (tipo == "Desarmado en tienda") mTipo = "D";
                //if (tipo == "Desarmado y Armado por cobro") mTipo = "P";
                //if (tipo == "Servicio a cliente") mTipo = "S";
                //if (tipo == "Servicio en tienda") mTipo = "T";

                string mUpdate = string.Format("UPDATE prodmult.MF_PedidoArmado SET TIPO = '{0}' WHERE NO_REQUISICION = {1}", mTipo, requisicion);

                mCommand.CommandText = mUpdate;
                mCommand.ExecuteNonQuery();

                mTransaction.Commit();
                conexion.Close();
            }

        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }

            mRetorna = false;
            mensaje = string.Format("Error al cambiar el tipo de la requisición WS. {0} {1}", ex.Message, m);
        }

        return mRetorna;
    }

    [WebMethod]
    public bool GrabarSistemaRequisicion(ref string mensaje, string pedido, string envio, string sistema)
    {
        bool mRetorna = true;
        dbDataContext db = new dbDataContext();

        try
        {
            string mAmbiente = "DES";

            if (db.Connection.ConnectionString.Contains("sql.fiesta.local")) mAmbiente = "PRO";
            if (db.Connection.ConnectionString.Contains("PRUEBAS")) mAmbiente = "PRU";

            string mServidor = "(local)";
            string mBase = "EXACTUSERP";

            if (mAmbiente == "PRO")
            {
                mBase = "EXACTUSERP";
                mServidor = "sql.fiesta.local";
            }

            if (mAmbiente == "PRU")
            {
                mBase = "PRUEBAS";
                mServidor = "sql.fiesta.local";
            }

            SqlCommand mCommand = new SqlCommand(); SqlTransaction mTransaction = null;
            string mStringConexion = string.Format("Data Source={0}; Initial Catalog = {1};User id = mf.fiestanet;password = 54321; Max Pool Size=600;Pooling=false;Application Name=PDV", mServidor, mBase);

            using (SqlConnection conexion = new SqlConnection(mStringConexion))
            {
                conexion.Open();
                mTransaction = conexion.BeginTransaction();

                mCommand.Connection = conexion;
                mCommand.Transaction = mTransaction;

                mCommand.CommandText = string.Format("UPDATE prodmult.MF_Pedido SET SISTEMA_ARMADOS = '{0}' WHERE PEDIDO = '{1}'", sistema, pedido);
                mCommand.ExecuteNonQuery();

                mCommand.CommandText = string.Format("UPDATE prodmult.MF_PedidoArmado SET SISTEMA_ARMADOS = '{0}' WHERE PEDIDO = '{1}'", sistema, pedido);
                mCommand.ExecuteNonQuery();

                mCommand.CommandText = string.Format("UPDATE prodmult.MF_DespachoTransportista SET SISTEMA_ARMADOS = '{0}' WHERE DOCUMENTO = '{1}'", sistema, envio);
                mCommand.ExecuteNonQuery();

                mTransaction.Commit();
                conexion.Close();
            }

        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }

            mRetorna = false;
            mensaje = string.Format("Error al grabar en que sistema fue grabado el envío WS. {0} {1}", ex.Message, m);
        }

        return mRetorna;
    }

}
