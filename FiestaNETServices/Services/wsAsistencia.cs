﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Transactions;
using FiestaNETServices.Services;
using System.Data;

/// <summary>
/// Summary description for wsAsistencia
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class wsAsistencia : System.Web.Services.WebService
{

    [WebMethod]
    public EmpleadoHuella[] DevuelveEmpleadoHuella()
    {
        return DataLayerAsistencia.DevuelveEmpleadoHuella().ToArray();
    }

    [WebMethod]
    public string DevuelveNombreEmpleado(string empleado, string empresa)
    {
        return DataLayerAsistencia.DevuelveNombreEmpleado(empleado, empresa);
    }

    [WebMethod]
    public string DevuelveTipo(string empleado, string empresa)
    {
        return DataLayerAsistencia.DevuelveTipo(empleado, empresa);
    }
    
    [WebMethod]
    public bool GrabarAsistencia(ref string mensaje, string empleado, string empresa)
    {
        return DataLayerAsistencia.GrabarAsistencia(ref mensaje, empleado, empresa);
    }

    [WebMethod]
    public DataTable DevuelveHuellas()
    {
        return DataLayerAsistencia.DevuelveHuellas();
    }

    [WebMethod]
    public DataTable DevuelveEmpleados(string nombre)
    {
        return DataLayerAsistencia.DevuelveEmpleados(nombre);
    }

    [WebMethod]
    public bool GrabarHuella(ref string mensaje, byte[] huella, string empleado, string empresa)
    {
        return DataLayerAsistencia.GrabarHuella(ref mensaje, huella, empleado, empresa);
    }

    [WebMethod]
    public bool PermiteCambiarHuella(string pass, ref string mensaje)
    {
        return DataLayerAsistencia.PermiteCambiarHuella(pass, ref mensaje);
    }

}
