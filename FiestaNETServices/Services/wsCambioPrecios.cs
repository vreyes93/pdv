﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using FiestaNETServices.Services;

/// <summary>
/// Summary description for CambiaPrecios
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.ComponentModel.ToolboxItem(false)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class wsCambioPrecios : System.Web.Services.WebService
{

    [WebMethod]
    public void Sincronizar()
    {
        DataLayer.Sincronizar();
    }

    [WebMethod]
    public NivelPrecioCoeficiente[] ListarCoeficientes()
    {
        return DataLayer.ListarCoeficientes().ToArray();
    }

    [WebMethod]
    public NivelPrecioCoeficiente[] ListarCoeficientesPorNivel(string nivel)
    {
        return DataLayer.ListarCoeficientesPorNivelPrecio(nivel).ToArray();
    }

    [WebMethod]
    public NivelPrecioCoeficiente[] ListarCoeficientesCriterio(string tipo, string nivel, string descripcion)
    {
        return DataLayer.ListarCoeficientesPorCriterio(tipo, nivel, descripcion).ToArray();
    }

    [WebMethod]
    public bool GrabarCoeficientes(string nivel, string coeficiente, string descripcion, ref string mensaje)
    {
        return DataLayer.GrabarCoeficientes(nivel, coeficiente, descripcion, ref mensaje);
    }

    [WebMethod]
    public Articulos[] ListarArticulosCriterio(string tipo, string articulo, string descripcion)
    {
        return DataLayer.ListarArticulosPorCriterio(tipo, articulo, descripcion).ToArray();
    }

    [WebMethod]
    public Articulos[] ListarArticulosCriterioConPrecio(string tipo, string articulo, string descripcion)
    {
        return DataLayer.ListarArticulosPorCriterioConPrecio(tipo, articulo, descripcion).ToArray();
    }

    [WebMethod]
    public bool GrabarOfertaArticulo(ref string mensaje, string articulo, DateTime FechaInicial, DateTime FechaFinal, decimal PrecioOriginal, decimal PrecioOferta, string descripcion)
    {
        return DataLayer.GrabarOfertaArticulo(ref mensaje, articulo, FechaInicial, FechaFinal, PrecioOriginal, PrecioOferta, descripcion);
    }

    [WebMethod]
    public bool GrabarOfertaArticuloNormalCondicional(ref string mensaje, string articulo, DateTime FechaInicial, DateTime FechaFinal, decimal PrecioOriginal, decimal PrecioOferta, string descripcion, bool condicional, string[] articulos)
    {
        return DataLayer.GrabarOfertaArticuloNormalCondicional(ref mensaje, articulo, FechaInicial, FechaFinal, PrecioOriginal, PrecioOferta, descripcion, condicional, articulos);
    }

    [WebMethod]
    public bool GrabarOfertaNivel(ref string mensaje, string nivel, DateTime FechaInicial, DateTime FechaFinal, decimal CoeficienteOriginal, decimal CoeficienteOferta, string descripcion)
    {
        return DataLayer.GrabarOfertaNivel(ref mensaje, nivel, FechaInicial, FechaFinal, CoeficienteOriginal, CoeficienteOferta, descripcion);
    }

    [WebMethod]
    public bool GrabarOfertaArticuloNivel(ref string mensaje, string articulo, string nivel, DateTime FechaInicial, DateTime FechaFinal, decimal PrecioOriginal, decimal PrecioOferta, string descripcion)
    {
        return DataLayer.GrabarOfertaArticuloNivel(ref mensaje, articulo, nivel, FechaInicial, FechaFinal, PrecioOriginal, PrecioOferta, descripcion);
    }
    
    [WebMethod]
    public bool GrabarOfertaArticulosNiveles(ref string mensaje, string[] articulos, string[] niveles, DateTime FechaInicial, DateTime FechaFinal, decimal porcentaje, string descripcion, string tipo)
    {
        return DataLayer.GrabarOfertaArticulosNiveles(ref mensaje, articulos, niveles, FechaInicial, FechaFinal, porcentaje, descripcion, tipo);
    }

    [WebMethod]
    public bool GrabarUsuariosCancelanOfertas(ref string mensaje, string[] usuarios)
    {
        return DataLayer.GrabarUsuariosCancelanOfertas(usuarios, ref mensaje);
    }

    [WebMethod]
    public UsuariosCancelanOfertas[] ListarUsuariosCancelan()
    {
        return DataLayer.ListarUsuariosCancelan().ToArray();
    }

    //[WebMethod]
    //Ofertas()
    //public List<Ofertas> ListarOfertas(string inicio, string fin, string tipo, string estatus, string articulo, string nivel, string nombreArticulo, string descripcion)
    //{
    //    DateTime mInicial, mFinal;
    //    char[] Separador = { '/' };
    //    string[] stringFechaInicial = inicio.Split(Separador);
    //    string[] stringFechaFinal = fin.Split(Separador);

    //    if (inicio.Trim().Length == 0)
    //    {
    //        mInicial = new DateTime(2012, 3, 1);
    //    }
    //    else
    //    {
    //        mInicial = new DateTime(Convert.ToInt32(stringFechaInicial[2]), Convert.ToInt32(stringFechaInicial[1]), Convert.ToInt32(stringFechaInicial[0]));
    //    }
    //    if (fin.Trim().Length == 0)
    //    {
    //        mFinal = new DateTime(3099, 12, 31);
    //    }
    //    else
    //    {
    //        mFinal = new DateTime(Convert.ToInt32(stringFechaFinal[2]), Convert.ToInt32(stringFechaFinal[1]), Convert.ToInt32(stringFechaFinal[0]));
    //    }
          
    //    dbDataContext db = new dbDataContext();
    //    var q = db.spDevuelveOfertas(mInicial, mFinal, Convert.ToChar(tipo), Convert.ToChar(estatus), articulo, nivel, nombreArticulo, descripcion);
    //    List<Ofertas> ListaOfertas = q.GetResult<Ofertas>().ToList();

    //    return ListaOfertas;
    //}

    [WebMethod]
    public BitacoraProcesos[] ListarBitacoraProcesosCriterio(string inicio, string fin, string tipo, string resultado)
    {
        return DataLayer.ListarBitacoraProcesosPorCriterio(inicio, fin, tipo, resultado).ToArray();
    }

    [WebMethod]
    public bool PuedeCancelarOfertas(string usuario)
    {
        return DataLayer.PuedeCancelarOfertas(usuario);
    }

    [WebMethod]
    public bool CancelaOfertas(string tipo, string articulo, string nivelPrecio, DateTime fechaInicial, string motivo, string usuario, ref string mensaje)
    {
        return DataLayer.CancelaOfertas(tipo, articulo, nivelPrecio, fechaInicial, motivo, usuario, ref mensaje);
    }

    [WebMethod]
    public void ActualizarPrecios()
    {
        dbDataContext db = new dbDataContext();
        db.spCambiaPrecios();
        //DataLayer.ActualizarPrecios();
    }
      
    [WebMethod]
    public void InciaOfertas()
    {
        dbDataContext db = new dbDataContext();
        db.spIniciaOfertas();
    }

    [WebMethod]
    public void FinalizaOfertas()
    {
        dbDataContext db = new dbDataContext();
        db.spFinalizaOfertas();
    }

    [WebMethod]
    public decimal PrecioNivelArticulo(string nivel, string articulo)
    {
        return DataLayer.PrecioNivelArticulo(nivel, articulo);
    }

    [WebMethod]
    public decimal iva()
    {
        return DataLayerPuntoVenta.iva();
    }

}
