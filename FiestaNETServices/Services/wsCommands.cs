﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Transactions;
using FiestaNETServices.Services;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Text;

/// <summary>
/// Summary description for wsCommands
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class wsCommands : System.Web.Services.WebService
{

    [WebMethod]
    public bool LoginExitoso(string usuario, string password, ref string nombre, ref string mensaje)
    {
        bool mRetorna = true;
        dbDataContext db = new dbDataContext();
        
        try
        {
            string mAmbiente = "DES";

            if (db.Connection.ConnectionString.Contains("sql.fiesta.local")) mAmbiente = "PRO";
            if (db.Connection.ConnectionString.Contains("PRUEBAS")) mAmbiente = "PRU";

            string mServidor = "(local)";
            string mBase = "EXACTUSERP";

            if (mAmbiente == "PRO")
            {
                mBase = "EXACTUSERP";
                mServidor = "sql.fiesta.local";
            }

            if (mAmbiente == "PRU")
            {
                mBase = "PRUEBAS";
                mServidor = "sql.fiesta.local";
            }
            
            bool mLoginExitoso = false;
            string mStringConexion = string.Format("Data Source={0}; Initial Catalog = {1};User id = {2};password = {3}; Max Pool Size=600;Pooling=false;Application Name=PDV", mServidor, mBase, usuario, password);

            using (SqlConnection conexion = new SqlConnection(mStringConexion))
            {
                try
                {
                    conexion.Open();
                    conexion.Close();

                    mLoginExitoso = true;
                }
                catch
                {
                    if (conexion.State == System.Data.ConnectionState.Open) conexion.Close();
                }
                finally
                {
                    if (conexion.State == System.Data.ConnectionState.Open) conexion.Close();
                }
            }


            if (!mLoginExitoso)
            {
                mRetorna = false;
                mensaje = "Usuario o contraseña inválidos";
            }
        }
        catch
        {
            mRetorna = false;
            mensaje = "Usuario o contraseña inválidos.";
        }

        return mRetorna;
    }

}
