﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Net;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace FiestaNETServices.Services
{
    public class wsConnector
    {

        private static FiestaNETServices.wsMySuite.FactWSFront wsServicio = new FiestaNETServices.wsMySuite.FactWSFront();
        private static FiestaNETServices.wsMySuite.TransactionTag tt = new FiestaNETServices.wsMySuite.TransactionTag();

        // Clase que hara la transaccion en el servidor ws especificado
        //public static string[] wsEnvio(XmlDocument cData1, string cData2, string cData3, string cUser, string cUrl, string cRequestor, string cTransaction, string cCountry, string cNit, bool lPdf, string cRutaPdf)
        public static string[] wsEnvio(string cData1, string cData2, string cData3, string cUser, string cUrl, string cRequestor, string cTransaction, string cCountry, string cNit, bool lPdf, string cRutaPdf)
        {
            //Variables que haran el retorno
            string[] cResultados = new string[13];
            XmlDocument cFirma = new XmlDocument();
            wsServicio.Url = new Uri(cUrl).ToString();
            //wsServicio.Proxy = System.Net.WebProxy.GetDefaultProxy();

            try
            {
                tt = wsServicio.RequestTransaction(cRequestor, cTransaction, cCountry, cNit, cRequestor, cUser, cData1.ToString(), cData2, cData3);
            }
            catch (Exception ex)
            {
                //Variable para controlar Errores
                cResultados[0] = ex.Message.ToString().ToUpper();


                cResultados[1] = "False";
                cResultados[2] = "";
                cResultados[3] = "";
                cResultados[4] = "";
                cResultados[5] = "";
                cResultados[6] = "";
                cResultados[7] = "";
                cResultados[8] = "";
                cResultados[9] = "";
                cResultados[10] = "";
                cResultados[11] = "";
                cResultados[12] = "";
            }
            try
            {
                //Variable para controlar si es true o false
                cResultados[1] = tt.Response.Result.ToString();
                //Es el Response Data
                cResultados[2] = tt.Response.Data.ToString();
            }
            catch (Exception ex)
            {

                cResultados[1] = "False";

                cResultados[2] = "POR FAVOR VERIFICAR SU CONEXION DE INTERNET SI NO COMUNIQUESE CON SU PROVEEDOR DE FACTURA ELECTRONICA " + ex.Message.ToString();
                cResultados[3] = "";
                cResultados[4] = "";
                cResultados[5] = "";
                cResultados[6] = "";
                cResultados[7] = "";
                cResultados[8] = "";
                cResultados[9] = "";
                cResultados[10] = "";
                cResultados[11] = "";
                cResultados[12] = "";

            }

            try
            {

                if (tt.Response.Result == true)
                {
                    //-----------------------------------------------------------------------
                    //TODO: Comentar en otros servicios que no sean GET_DOCUMENT y CONVERT_NATIVE_XML
                    cResultados[3] = tt.Response.Identifier.Batch.ToString();
                    cResultados[4] = tt.Response.Identifier.Serial.ToString();
                    cResultados[5] = Base64String_String(tt.ResponseData.ResponseData1);
                    //-----------------------------------------------------------------------
                    //cResultados[5] = Base64String_String(tt.ResponseData.ResponseData2);
                    //-----------------------------------------------------------------------

                    cFirma.InnerXml = cResultados[5];
                    cResultados[6] = cFirma.SelectSingleNode("//FCAE").FirstChild.NextSibling.InnerText.ToString();



                    XmlNodeList fechaResol = cFirma.GetElementsByTagName("fechaResolucion");
                    XmlNodeList nitGface = cFirma.GetElementsByTagName("NITGFACE");
                    XmlNodeList nAutorizacion = cFirma.GetElementsByTagName("NumeroAutorizacion");
                    XmlNodeList cFace = cFirma.GetElementsByTagName("uniqueCreatorIdentification");
                    XmlNodeList IniAut = cFirma.GetElementsByTagName("rangoInicialAutorizado");
                    XmlNodeList FinAut = cFirma.GetElementsByTagName("rangoFinalAutorizado");

                    try
                    {
                        cResultados[12] = cFirma.SelectSingleNode("//despatchAdvice/contentOwner/additionalPartyIdentification[1]/additionalPartyIdentificationValue").InnerText;
                    }
                    catch
                    {
                        cResultados[12] = "";
                    }

                    cResultados[7] = fechaResol[0].InnerText;
                    cResultados[8] = nitGface[0].InnerText;
                    cResultados[9] = nAutorizacion[0].InnerText;
                    cResultados[10] = IniAut[0].InnerText;
                    cResultados[11] = FinAut[0].InnerText;

                    if (lPdf == true)
                    {
                        try
                        {

                            /*
                            cNombreDocumento = cResultados[3] + "-" + cResultados[4] + ".xml";
                            cRutaXslt = ".\\Dte\\" + cNombreDocumento;
                            System.IO.FileStream oFileStream = new System.IO.FileStream(cRutaXslt, System.IO.FileMode.Create);
                        

                            oFileStream.Write(Base64String_ByteArray(tt.ResponseData.ResponseData1), 0, Base64String_ByteArray(tt.ResponseData.ResponseData1).Length);
                            oFileStream.Close();

                            XPathDocument myXPathDoc = new XPathDocument(cRutaXslt);

                            XslTransform myXslTrans = new XslTransform();
                            myXslTrans.Load(".\\Xslt\\formato.xslt");

                            XmlTextWriter myWriter = new XmlTextWriter(".\\Html\\"+cNombreDocumento.Replace(".xml", ".html"), null);

                            myXslTrans.Transform(myXPathDoc, null, myWriter);
                            */

                            //cNombreDocumento = cResultados[3] + "-" + cResultados[4] + ".pdf";
                            //cNombreDocumento = cFace + ".pdf";
                            //cRutaPdf = cRutaPdf + cNombreDocumento;
                            //System.IO.FileStream oFileStream = new System.IO.FileStream(cRutaPdf, System.IO.FileMode.Create);
                            //oFileStream.Write(Base64String_ByteArray(tt.ResponseData.ResponseData3), 0, Base64String_ByteArray(tt.ResponseData.ResponseData3).Length);
                            //oFileStream.Close();
                        }
                        catch
                        {
                            //System.Windows.Forms.MessageBox.Show(ex.Message);
                        }
                    }
                }
                else
                {
                    cResultados[0] = "";
                    cResultados[1] = "False";
                    cResultados[2] = tt.Response.Data.ToString();
                    cResultados[3] = "";
                    cResultados[4] = "";
                    cResultados[5] = "";
                    cResultados[6] = "";
                    cResultados[7] = "";
                    cResultados[8] = "";
                    cResultados[9] = "";
                    cResultados[10] = "";
                    cResultados[11] = "";
                    cResultados[12] = "";


                }
            }
            catch
            {
            }
            return cResultados;
        }

        public static string[] wsObtener(string cData1, string cData2, string cData3, string cUser, string cUrl, string cRequestor, string cTransaction, string cCountry, string cNit, bool lPdf, string cRutaPdf)
        {
            //Variables que haran el retorno
            string[] cResultados = new string[12];
            XmlDocument cFirma = new XmlDocument();
            wsServicio.Url = new Uri(cUrl).ToString();
            //wsServicio.Proxy = System.Net.WebProxy.GetDefaultProxy();
            string cNombreDocumento = "";

            try
            {
                tt = wsServicio.RequestTransaction(cRequestor, cTransaction, cCountry, cNit, cRequestor, cUser, cData1, cData2, cData3);
            }
            catch (Exception ex)
            {
                //Variable para controlar Errores
                cResultados[0] = ex.Message.ToString().ToUpper();


                cResultados[1] = "False";
                cResultados[2] = "";
                cResultados[3] = "";
                cResultados[4] = "";
                cResultados[5] = "";
                cResultados[6] = "";
                cResultados[7] = "";
                cResultados[8] = "";
                cResultados[9] = "";
                cResultados[10] = "";
                cResultados[11] = "";

            }
            try
            {
                //Variable para controlar si es true o false
                cResultados[1] = tt.Response.Result.ToString();
                //Es el Response Data
                cResultados[2] = tt.Response.Data.ToString();
            }
            catch (Exception ex)
            {

                cResultados[1] = "False";

                cResultados[2] = "POR FAVOR VERIFICAR SU CONEXION DE INTERNET SI NO COMUNIQUESE CON SU PROVEEDOR DE FACTURA ELECTRONICA " + ex.Message.ToString();
                cResultados[3] = "";
                cResultados[4] = "";
                cResultados[5] = "";
                cResultados[6] = "";
                cResultados[7] = "";
                cResultados[8] = "";
                cResultados[9] = "";
                cResultados[10] = "";
                cResultados[11] = "";

            }

            try
            {

                if (tt.Response.Result == true)
                {
                    cResultados[3] = tt.Response.Identifier.Batch.ToString();
                    cResultados[4] = tt.Response.Identifier.Serial.ToString();
                    cResultados[5] = Base64String_String(tt.ResponseData.ResponseData1);
                    cFirma.InnerXml = cResultados[5];
                    cResultados[6] = cFirma.SelectSingleNode("//FCAE").FirstChild.NextSibling.InnerText.ToString();



                    XmlNodeList fechaResol = cFirma.GetElementsByTagName("fechaResolucion");
                    XmlNodeList nitGface = cFirma.GetElementsByTagName("NITGFACE");
                    XmlNodeList nAutorizacion = cFirma.GetElementsByTagName("NumeroAutorizacion");
                    XmlNodeList cFace = cFirma.GetElementsByTagName("uniqueCreatorIdentification");
                    XmlNodeList IniAut = cFirma.GetElementsByTagName("rangoInicialAutorizado");
                    XmlNodeList FinAut = cFirma.GetElementsByTagName("rangoFinalAutorizado");

                    cResultados[7] = fechaResol[0].InnerText;
                    cResultados[8] = nitGface[0].InnerText;
                    cResultados[9] = nAutorizacion[0].InnerText;
                    cResultados[10] = IniAut[0].InnerText;
                    cResultados[11] = FinAut[0].InnerText;

                    //cResultados[3] = cFace[0].InnerText;



                    if (lPdf == true)
                    {
                        try
                        {

                            /*
                                cNombreDocumento = cResultados[3] + "-" + cResultados[4] + ".xml";
                                cRutaXslt = ".\\Dte\\" + cNombreDocumento;
                                System.IO.FileStream oFileStream = new System.IO.FileStream(cRutaXslt, System.IO.FileMode.Create);


                                oFileStream.Write(Base64String_ByteArray(tt.ResponseData.ResponseData1), 0, Base64String_ByteArray(tt.ResponseData.ResponseData1).Length);
                                oFileStream.Close();

                                XPathDocument myXPathDoc = new XPathDocument(cRutaXslt);

                                XslTransform myXslTrans = new XslTransform();
                                myXslTrans.Load(".\\Xslt\\formato.xslt");

                                XmlTextWriter myWriter = new XmlTextWriter(".\\Html\\" + cNombreDocumento.Replace(".xml", ".html"), null);

                                myXslTrans.Transform(myXPathDoc, null, myWriter);
                            */


                            //cNombreDocumento = cResultados[3] + "-" + cResultados[4] + ".pdf";
                            cNombreDocumento = cFace + ".pdf";
                            cRutaPdf = cRutaPdf + cNombreDocumento;
                            System.IO.FileStream oFileStream = new System.IO.FileStream(cRutaPdf, System.IO.FileMode.Create);
                            oFileStream.Write(Base64String_ByteArray(tt.ResponseData.ResponseData3), 0, Base64String_ByteArray(tt.ResponseData.ResponseData3).Length);
                            oFileStream.Close();
                        }
                        catch
                        {
                            //System.Windows.Forms.MessageBox.Show(ex.Message);
                        }
                    }
                }
                else
                {
                    cResultados[0] = "";
                    cResultados[1] = "False";
                    cResultados[2] = tt.Response.Data.ToString();
                    cResultados[3] = "";
                    cResultados[4] = "";
                    cResultados[5] = "";
                    cResultados[6] = "";
                    cResultados[7] = "";
                    cResultados[8] = "";
                    cResultados[9] = "";
                    cResultados[10] = "";
                    cResultados[11] = "";
                    cResultados[12] = "";


                }
            }
            catch
            {
            }
            return cResultados;
        }

        #region Base64
        public static string ByteArray_Base64String(byte[] b)
        {
            return Convert.ToBase64String(b);
        }

        public static byte[] String_ByteArray(string s)
        {
            return System.Text.Encoding.UTF8.GetBytes(s);
        }

        public static string String_Base64String(string s)
        {
            return ByteArray_Base64String(String_ByteArray(s));
        }//String_Base64String

        public static string Base64String_String(string b64)
        {
            try
            {
                return ByteArray_String(Base64String_ByteArray(b64));
            }
            catch
            {
                return b64;
            }

        }//Base64String_String

        public static byte[] Base64String_ByteArray(string s)
        {
            return Convert.FromBase64String(s);
        }//Base64String_ByteArray



        public static string ByteArray_String(byte[] b)
        {
            return new string(System.Text.Encoding.UTF8.GetChars(b));
        }//ByteArray_String
        #endregion  
    
    }
}