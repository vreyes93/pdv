﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Transactions;
using FiestaNETServices.Services;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Configuration;
using FiestaNETServices.Util;

/// <summary>
/// Summary description for wsContabilidad
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class wsContabilidad : System.Web.Services.WebService
{
    

    
    [WebMethod]
    public bool FirmarDocumento(string usuario, ref string mensaje, string empresa, DataSet ds, ref string firmado, ref string firma)
    {
        bool mRetorna = true;
        dbDataContext db = new dbDataContext();

        try
        {

            string xmlData = "<?xml version='1.0'?>";
            xmlData += "<FactDocGT xmlns='http://www.fact.com.mx/schema/gt' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='http://www.fact.com.mx/schema/gt http://www.mysuitemex.com/fact/schema/fx_2012_gt.xsd'>";
            xmlData += "<Version>2</Version>";
            xmlData += "<AsignacionSolicitada>";
            xmlData += string.Format("<Serie>{0}</Serie>", ds.Tables["Encabezado"].Rows[0]["Serie"]);
            xmlData += string.Format("<NumeroDocumento>{0}</NumeroDocumento>", Convert.ToInt64(ds.Tables["Encabezado"].Rows[0]["NumeroDocumento"]));
            xmlData += string.Format("<FechaEmision>{0}</FechaEmision>", ds.Tables["Encabezado"].Rows[0]["Fecha"]);
            xmlData += string.Format("<NumeroAutorizacion>{0}</NumeroAutorizacion>", ds.Tables["Encabezado"].Rows[0]["NumeroResolucion"]);
            xmlData += string.Format("<FechaResolucion>{0}</FechaResolucion>", ds.Tables["Encabezado"].Rows[0]["FechaResolucion"]);
            xmlData += string.Format("<RangoInicialAutorizado>{0}</RangoInicialAutorizado>", ds.Tables["Encabezado"].Rows[0]["FolioInicial"]);
            xmlData += string.Format("<RangoFinalAutorizado>{0}</RangoFinalAutorizado>", ds.Tables["Encabezado"].Rows[0]["FolioFinal"]);
            xmlData += "</AsignacionSolicitada>";
            xmlData += "<Encabezado>";
            xmlData += string.Format("<TipoActivo>{0}</TipoActivo>", ds.Tables["Encabezado"].Rows[0]["TipoActivo"]);
            xmlData += string.Format("<CodigoDeMoneda>{0}</CodigoDeMoneda>", ds.Tables["Encabezado"].Rows[0]["Moneda"]);
            xmlData += string.Format("<TipoDeCambio>{0}</TipoDeCambio>", ds.Tables["Encabezado"].Rows[0]["TasaCambio"]);
            xmlData += "<InformacionDeRegimenIsr>PAGO_TRIMESTRAL</InformacionDeRegimenIsr>";
            xmlData += "<ReferenciaInterna>00000</ReferenciaInterna>";
            xmlData += "</Encabezado>";
            xmlData += "<Vendedor>";
            xmlData += string.Format("<Nit>{0}</Nit>", ds.Tables["Encabezado"].Rows[0]["NitVendedor"]);
            xmlData += string.Format("<NombreComercial>{0}</NombreComercial>", ds.Tables["Encabezado"].Rows[0]["RazonSocial"]);
            xmlData += "<Idioma>es</Idioma>";
            xmlData += "<DireccionDeEmisionDeDocumento>";
            xmlData += string.Format("<NombreDeEstablecimiento>{0}</NombreDeEstablecimiento>", ds.Tables["Encabezado"].Rows[0]["RazonSocial"]);
            xmlData += "<CodigoDeEstablecimiento>1</CodigoDeEstablecimiento>";
            xmlData += "<DispositivoElectronico>001</DispositivoElectronico>";
            xmlData += string.Format("<Direccion1>{0}</Direccion1>", ds.Tables["Encabezado"].Rows[0]["DireccionVendedor"]);
            xmlData += "<Direccion2>.</Direccion2>";
            xmlData += string.Format("<Municipio>{0}</Municipio>", ds.Tables["Encabezado"].Rows[0]["MunicipioSucursal"]);
            xmlData += string.Format("<Departamento>{0}</Departamento>", ds.Tables["Encabezado"].Rows[0]["DepartamentoSucursal"]);
            xmlData += "<CodigoDePais>GT</CodigoDePais>";
            xmlData += "<CodigoPostal>010109</CodigoPostal>";
            xmlData += "</DireccionDeEmisionDeDocumento>";
            xmlData += "</Vendedor>";
            xmlData += "<Comprador>";
            xmlData += string.Format("<Nit>{0}</Nit>", ds.Tables["Encabezado"].Rows[0]["Nit"]);
            xmlData += string.Format("<NombreComercial>{0}</NombreComercial>", ds.Tables["Encabezado"].Rows[0]["Cliente"]);
            xmlData += "<Idioma>es</Idioma>";
            xmlData += "<DireccionComercial>";
            xmlData += string.Format("<Direccion1>{0}</Direccion1>", ds.Tables["Encabezado"].Rows[0]["Direccion"]);
            xmlData += "<Direccion2>.</Direccion2>";
            xmlData += string.Format("<Municipio>{0}</Municipio>", ds.Tables["Encabezado"].Rows[0]["MunicipioSucursal"]);
            xmlData += string.Format("<Departamento>{0}</Departamento>", ds.Tables["Encabezado"].Rows[0]["DepartamentoSucursal"]);
            xmlData += "<CodigoDePais>GT</CodigoDePais>";
            xmlData += "<CodigoPostal>010109</CodigoPostal>";
            xmlData += "<Telefono>-</Telefono>";
            xmlData += "</DireccionComercial>";
            xmlData += "</Comprador>";
            xmlData += "<Detalles>";

            for (int ii = 0; ii < ds.Tables["Detalle"].Rows.Count; ii++)
            {
                xmlData += "<Detalle>";
                xmlData += string.Format("<Descripcion>{0}</Descripcion>", ds.Tables["Detalle"].Rows[ii]["Descripcion"]);
                xmlData += "<CodigoEAN>00000000000000</CodigoEAN>";
                xmlData += "<UnidadDeMedida>PZA</UnidadDeMedida>";
                xmlData += string.Format("<Cantidad>{0}</Cantidad>", ds.Tables["Detalle"].Rows[ii]["Cantidad"]);
                xmlData += "<ValorSinDR>";
                xmlData += string.Format("<Precio>{0}</Precio>", ds.Tables["Detalle"].Rows[ii]["SubTotal"]);
                xmlData += string.Format("<Monto>{0}</Monto>", ds.Tables["Detalle"].Rows[ii]["SubTotal"]);
                xmlData += "</ValorSinDR>";
                xmlData += "<DescuentosYRecargos>";
                xmlData += "<SumaDeDescuentos>0</SumaDeDescuentos>";
                xmlData += "<DescuentoORecargo>";
                xmlData += "<Operacion>DESCUENTO</Operacion>";
                xmlData += "<Servicio>ALLOWANCE_GLOBAL</Servicio>";
                xmlData += string.Format("<Base>{0}</Base>", ds.Tables["Detalle"].Rows[ii]["SubTotal"]);
                xmlData += "<Tasa>0</Tasa>";
                xmlData += "<Monto>0</Monto>";
                xmlData += "</DescuentoORecargo>";
                xmlData += "</DescuentosYRecargos>";
                xmlData += "<ValorConDR>";
                xmlData += string.Format("<Precio>{0}</Precio>", ds.Tables["Detalle"].Rows[ii]["SubTotal"]);
                xmlData += string.Format("<Monto>{0}</Monto>", ds.Tables["Detalle"].Rows[ii]["SubTotal"]);
                xmlData += "</ValorConDR>";
                xmlData += "<Impuestos>";
                xmlData += string.Format("<TotalDeImpuestos>{0}</TotalDeImpuestos>", ds.Tables["Detalle"].Rows[ii]["IVA"]);
                xmlData += string.Format("<IngresosNetosGravados>0</IngresosNetosGravados>", ds.Tables["Detalle"].Rows[ii]["SubTotal"]);
                xmlData += string.Format("<TotalDeIVA>0</TotalDeIVA>", ds.Tables["Detalle"].Rows[ii]["IVA"]);
                xmlData += "<Impuesto>";
                xmlData += "<Tipo>IVA</Tipo>";
                xmlData += string.Format("<Base>{0}</Base>", ds.Tables["Detalle"].Rows[ii]["SubTotal"]);
                xmlData += "<Tasa>12</Tasa>";
                xmlData += string.Format("<Monto>{0}</Monto>", ds.Tables["Detalle"].Rows[ii]["IVA"]);
                xmlData += "</Impuesto>";
                xmlData += "</Impuestos>";
                xmlData += string.Format("<Categoria>{0}</Categoria>", ds.Tables["Encabezado"].Rows[0]["BienServicio"]);
                xmlData += "</Detalle>";
            }

            xmlData += "</Detalles>";
            xmlData += "<Totales>";
            xmlData += string.Format("<SubTotalSinDR>{0}</SubTotalSinDR>", ds.Tables["Encabezado"].Rows[0]["SubTotal"]);
            xmlData += "<DescuentosYRecargos>";
            xmlData += "<SumaDeDescuentos>0.00</SumaDeDescuentos>";
            xmlData += "<DescuentoORecargo>";
            xmlData += "<Operacion>DESCUENTO</Operacion>";
            xmlData += "<Servicio>ALLOWANCE_GLOBAL</Servicio>";
            xmlData += string.Format("<Base>{0}</Base>", ds.Tables["Encabezado"].Rows[0]["SubTotal"]);
            xmlData += "<Tasa>0.000000</Tasa>";
            xmlData += "<Monto>0.00</Monto>";
            xmlData += "</DescuentoORecargo>";
            xmlData += "</DescuentosYRecargos>";
            xmlData += string.Format("<SubTotalConDR>{0}</SubTotalConDR>", ds.Tables["Encabezado"].Rows[0]["SubTotal"]);
            xmlData += "<Impuestos>";
            xmlData += string.Format("<TotalDeImpuestos>{0}</TotalDeImpuestos>", ds.Tables["Encabezado"].Rows[0]["IVA"]);
            xmlData += string.Format("<IngresosNetosGravados>{0}</IngresosNetosGravados>", ds.Tables["Encabezado"].Rows[0]["SubTotal"]);
            xmlData += string.Format("<TotalDeIVA>{0}</TotalDeIVA>", ds.Tables["Encabezado"].Rows[0]["IVA"]);
            xmlData += "<Impuesto>";
            xmlData += "<Tipo>IVA</Tipo>";
            xmlData += string.Format("<Base>3982.14</Base>", ds.Tables["Encabezado"].Rows[0]["SubTotal"]);
            xmlData += "<Tasa>12</Tasa>";
            xmlData += string.Format("<Monto>477.86</Monto>", ds.Tables["Encabezado"].Rows[0]["IVA"]);
            xmlData += "</Impuesto>";
            xmlData += "</Impuestos>";
            xmlData += string.Format("<Total>{0}</Total>", ds.Tables["Encabezado"].Rows[0]["Total"]);
            xmlData += string.Format("<TotalLetras>CUATRO  MIL CUATROCIENTOS SESENTA CON 0/100</TotalLetras>", ds.Tables["Encabezado"].Rows[0]["NumerosLetras"]);
            xmlData += "</Totales>";
            xmlData += "</FactDocGT>";

            if (empresa == "Empresas Concretas, S.A.")
            {
                empresa = "emprconc";
            }
            else
            {
                empresa = "prodmult";
            }

            string mAmbiente = "DES";

            if (db.Connection.ConnectionString.Contains("sql.fiesta.local")) mAmbiente = "PRO";
            if (db.Connection.ConnectionString.Contains("PRUEBAS")) mAmbiente = "PRU";

            string mServidor = "10.10.5.14";
            string mBase = "EXACTUSERP";

            if (mAmbiente == "PRO")
            {
                mBase = "EXACTUSERP";
                mServidor = "sql.fiesta.local";
            }

            if (mAmbiente == "PRU")
            {
                mBase = "PRUEBAS";
                mServidor = "sql.fiesta.local";
            }

            SqlCommand mCommand = new SqlCommand(); SqlDataReader mLector; SqlTransaction mTransaction = null; string[] mFactura = new string[12];
            string mStringConexion = string.Format("Data Source={0}; Initial Catalog = {1};User id = mf.fiestanet;password = 54321; Max Pool Size=600;Pooling=false;Application Name=PDV", mServidor, mBase);

            using (SqlConnection conexion = new SqlConnection(mStringConexion))
            {
                try
                {
                    conexion.Open();
                    mTransaction = conexion.BeginTransaction();

                    mCommand.Connection = conexion;
                    mCommand.Transaction = mTransaction;

                    mCommand.CommandText = string.Format("SELECT USAR_PRUEBAS FROM {0}.MF_GFaceConfigura", empresa);
                    string mPruebas = Convert.ToString(mCommand.ExecuteScalar());

                    if (mPruebas == "S")
                    {
                        mPruebas = "_PRUEBAS";
                    }
                    else
                    {
                        mPruebas = "";
                    }

                    string mNit = ""; string mUsuario = ""; string mRequestor = ""; string mUrl = ""; 

                    mCommand.CommandText = string.Format("SELECT NIT{1}, USUARIO{1}, REQUESTOR{1}, URL{1} FROM {0}.MF_GFaceConfigura", empresa, mPruebas);
                    mLector = mCommand.ExecuteReader();

                    while (mLector.Read())
                    {
                        mNit = Convert.ToString(mLector[0]);
                        mUsuario = Convert.ToString(mLector[1]);
                        mRequestor = Convert.ToString(mLector[2]);
                        mUrl = Convert.ToString(mLector[3]);
                    }
                    mLector.Close();

                    firma = "";
                    firmado = "N";

                    mFactura = wsConnector.wsEnvio(xmlData, "XML", "", mUsuario, mUrl, mRequestor, "CONVERT_NATIVE_XML", "GT", mNit, false, "");

                    mensaje = mFactura[2];
                    if (mFactura[1] == "True")
                    {
                        firmado = "S";
                        firma = mFactura[6];
                    }

                    mCommand.Parameters.Add("@XmlDocumento", SqlDbType.Text).Value = xmlData;
                    mCommand.Parameters.Add("@XmlFirmado", SqlDbType.Text).Value = mFactura[5];
                    mCommand.Parameters.Add("@XmlError", SqlDbType.Text).Value = mFactura[2];
                    mCommand.Parameters.Add("@FechaDocumento", SqlDbType.DateTime).Value = Convert.ToDateTime(ds.Tables["Encabezado"].Rows[0]["FechaDocumento"]);
                    mCommand.Parameters.Add("@FechaCreado", SqlDbType.DateTime).Value = DateTime.Now.Date;
                    mCommand.Parameters.Add("@FechaHoraCreado", SqlDbType.DateTime).Value = DateTime.Now;

                    mCommand.CommandText = string.Format("DELETE FROM {0}.MF_GFaceDetalle WHERE DOCUMENTO = '{1}'", empresa, ds.Tables["Encabezado"].Rows[0]["Documento"]);
                    mCommand.ExecuteNonQuery();

                    mCommand.CommandText = string.Format("DELETE FROM {0}.MF_GFace WHERE DOCUMENTO = '{1}'", empresa, ds.Tables["Encabezado"].Rows[0]["Documento"]);
                    mCommand.ExecuteNonQuery();

                    mCommand.CommandText = string.Format("INSERT INTO {0}.MF_GFace ( ANIO, MES, COBRADOR, DOCUMENTO, NIT_VENDEDOR, RAZON_SOCIAL, TIPO, FECHA, SERIE, NUMERO_DOCUMENTO, NOMBRE_CLIENTE, NIT, DIRECCION, DEPARTAMENTO, MUNICIPIO, SUBTOTAL, " +
                      "IVA, TOTAL, ANULADA, FECHA_RESOLUCION, NUMERO_RESOLUCION, FOLIO_INICIAL, FOLIO_FINAL, SUCURSAL, NOMBRE_SUCURSAL, DIRECCION_SUCURSAL, DEPARTAMENTO_SUCURSAL, " +
                      "MUNICIPIO_SUCURSAL, NUMEROS_LETRAS, TIPO_ACTIVO, MONEDA, TASA_CAMBIO, CLIENTE, BIEN_SERVICIO, XML_DOCUMENTO, XML_FIRMADO, XML_ERROR, FIRMADO, FIRMA, FECHA_DOCUMENTO, DIRECCION_VENDEDOR, USUARIO, FECHA_CREADO, FECHA_HORA_CREADO ) " + 
                      "VALUES ( {1}, {2}, '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', {16}, {17}, {18}, '{19}', '{20}', '{21}', '{22}', '{23}', '{24}', '{25}', '{26}', " +
                      "'{27}', '{28}', '{29}', '{30}', '{31}', '{32}', '{33}', '{34}', @XmlDocumento, @XmlFirmado, @XmlError, '{35}', '{36}', @FechaDocumento, '{37}', '{38}', @FechaCreado, @FechaHoraCreado )", 
                      empresa, ds.Tables["Encabezado"].Rows[0]["Anio"], ds.Tables["Encabezado"].Rows[0]["Mes"], ds.Tables["Encabezado"].Rows[0]["Tienda"], 
                      ds.Tables["Encabezado"].Rows[0]["Documento"], ds.Tables["Encabezado"].Rows[0]["NitVendedor"], ds.Tables["Encabezado"].Rows[0]["RazonSocial"], ds.Tables["Encabezado"].Rows[0]["Tipo"], ds.Tables["Encabezado"].Rows[0]["Fecha"], 
                      ds.Tables["Encabezado"].Rows[0]["Serie"], ds.Tables["Encabezado"].Rows[0]["NumeroDocumento"], ds.Tables["Encabezado"].Rows[0]["NombreCliente"], ds.Tables["Encabezado"].Rows[0]["Nit"], ds.Tables["Encabezado"].Rows[0]["Direccion"],
                      ds.Tables["Encabezado"].Rows[0]["Departamento"], ds.Tables["Encabezado"].Rows[0]["Municipio"], ds.Tables["Encabezado"].Rows[0]["SubTotal"], ds.Tables["Encabezado"].Rows[0]["IVA"], ds.Tables["Encabezado"].Rows[0]["Total"],
                      ds.Tables["Encabezado"].Rows[0]["Anulada"], ds.Tables["Encabezado"].Rows[0]["FechaResolucion"], ds.Tables["Encabezado"].Rows[0]["NumeroResolucion"], ds.Tables["Encabezado"].Rows[0]["FolioInicial"], 
                      ds.Tables["Encabezado"].Rows[0]["FolioFinal"], ds.Tables["Encabezado"].Rows[0]["Sucursal"], ds.Tables["Encabezado"].Rows[0]["NombreSucursal"], ds.Tables["Encabezado"].Rows[0]["DireccionSucursal"],
                      ds.Tables["Encabezado"].Rows[0]["DepartamentoSucursal"], ds.Tables["Encabezado"].Rows[0]["MunicipioSucursal"], ds.Tables["Encabezado"].Rows[0]["NumerosLetras"], ds.Tables["Encabezado"].Rows[0]["TipoActivo"],
                      ds.Tables["Encabezado"].Rows[0]["Moneda"], ds.Tables["Encabezado"].Rows[0]["TasaCambio"], ds.Tables["Encabezado"].Rows[0]["Cliente"], ds.Tables["Encabezado"].Rows[0]["BienServicio"],
                      firmado, firma, ds.Tables["Encabezado"].Rows[0]["DireccionVendedor"], usuario);
                    mCommand.ExecuteNonQuery();
                    
                    for (int ii = 0; ii < ds.Tables["Detalle"].Rows.Count; ii++)
                    {
                        mCommand.Parameters["@FechaDocumento"].Value = Convert.ToDateTime(ds.Tables["Detalle"].Rows[ii]["FechaDocumento"]);

                        mCommand.CommandText = string.Format("INSERT INTO {0}.MF_GFaceDetalle ( ANIO, MES, COBRADOR, DOCUMENTO, TIPO, FECHA, SERIE, NUMERO_DOCUMENTO, DESCRIPCION, CANTIDAD, PRECIO_UNITARIO, SUBTOTAL, IVA, TOTAL, " +
                            "FECHA_DOCUMENTO, USUARIO, FECHA_CREADO, FECHA_HORA_CREADO  ) VALUES ( {1}, {2}, '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', {10}, {11}, {12}, {13}, {14}, @FechaDocumento, '{15}', @FechaCreado, @FechaHoraCreado ) ", empresa,
                            ds.Tables["Detalle"].Rows[ii]["Anio"], ds.Tables["Detalle"].Rows[ii]["Mes"], ds.Tables["Detalle"].Rows[ii]["Tienda"], ds.Tables["Detalle"].Rows[ii]["Documento"], ds.Tables["Detalle"].Rows[ii]["Tipo"],
                            ds.Tables["Detalle"].Rows[ii]["Fecha"], ds.Tables["Detalle"].Rows[ii]["Serie"], ds.Tables["Detalle"].Rows[ii]["NumeroDocumento"], ds.Tables["Detalle"].Rows[ii]["Descripcion"], ds.Tables["Detalle"].Rows[ii]["Cantidad"],
                            ds.Tables["Detalle"].Rows[ii]["PrecioUnitario"], ds.Tables["Detalle"].Rows[ii]["SubTotal"], ds.Tables["Detalle"].Rows[ii]["IVA"], ds.Tables["Detalle"].Rows[ii]["Total"], usuario);
                        mCommand.ExecuteNonQuery();
                    }

                    mTransaction.Commit();
                    conexion.Close();

                }
                catch (Exception ex)
                {
                    string m = "";
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                    }
                    catch
                    {
                        try
                        {
                            m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                        }
                        catch
                        {
                            //Nothing
                        }
                    }

                    if (mFactura[1] == "True")
                    {
                        StringBuilder mBody = new StringBuilder();

                        mBody.Append("<!DOCTYPE html><html lang='en-us'>");
                        mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } </style></head>");
                        mBody.Append("<body>");
                        mBody.Append(mFactura[5]);
                        mBody.Append("<br><br>");
                        mBody.Append(string.Format("Error firmando el documento WS- {0} {1}", ex.Message, m));
                        mBody.Append("</body>");
                        mBody.Append("</html>");

                        Mail mail = new Mail();
                        string mAsunto= string.Format("Error en {0} grabando la firma del documento {1} {2}", empresa, ds.Tables["Encabezado"].Rows[0]["Documento"], firma);
                        mail.EnviarEmail("ir@mueblesfiesta.com", WebConfigurationManager.AppSettings["ITMailNotification"].ToString(), mAsunto, mBody, "Punto de Venta");

                    }

                    mTransaction.Rollback();
                    mensaje = string.Format("Error firmando el documento WS. - {0}{1}{2}", ex.Message, m, mCommand.CommandText);
                    return false;
                }
            }

            if (xmlData.Trim().Length == 0)
            {
                xmlData += "";
            }

        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }

            mRetorna = false;
            mensaje = string.Format("Error al firmar el documento WS. {0} {1}", ex.Message, m);
        }

        return mRetorna;
    }

}
