﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using FiestaNETServices.Services;

/// <summary>
/// Summary description for wsFriedman
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class wsFriedman : System.Web.Services.WebService {

    [WebMethod]
    public MetasTiendas[] ListarMetasTiendas(int semana, int anio)
    {
        return DataLayerFriedman.ListarMetasTiendas(semana, anio).ToArray();
    }
         
    [WebMethod]
    public VentasVendedor[] ListarVentasVendedor(int semana, int anio, string tienda)
    {
        return DataLayerFriedman.ListarVentasVendedor(semana, anio, tienda).ToArray();
    }

    [WebMethod]
    public VentasVendedorSemanas[] ListarVentasVendedorSemanas(int semana, int anio, string tienda, int semanas, string vendedor)
    {
        return DataLayerFriedman.ListarVentasVendedorSemanas(semana, anio, tienda, semanas, vendedor).ToArray();
    }

}
