﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Transactions;
using FiestaNETServices.Services;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Configuration;

/// <summary>
/// Summary description for wsGFace
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class wsGFace : System.Web.Services.WebService
{

    [WebMethod]
    public bool LoginExitoso(string usuario, string password, ref string nombre, ref string mensaje)
    {
        bool mRetorna = true;
        dbDataContext db = new dbDataContext();
        
        try
        {
            string mAmbiente = "DES";

            if (db.Connection.ConnectionString.Contains("sql.fiesta.local")) mAmbiente = "PRO";
            if (db.Connection.ConnectionString.Contains("PRUEBAS")) mAmbiente = "PRU";

            string mServidor = "(local)";
            string mBase = "EXACTUSERP";

            if (mAmbiente == "PRO")
            {
                mBase = "EXACTUSERP";
                mServidor = "sql.fiesta.local";
            }

            if (mAmbiente == "PRU")
            {
                mBase = "PRUEBAS";
                mServidor = "sql.fiesta.local";
            }

            bool mLoginExitoso = false;
            string mStringConexion = string.Format("Data Source={0}; Initial Catalog = {1};User id = {2};password = {3}; Max Pool Size=600;Pooling=false;Application Name=PDV", mServidor, mBase, usuario, password);

            using (SqlConnection conexion = new SqlConnection(mStringConexion))
            {
                try
                {
                    conexion.Open();
                    conexion.Close();

                    mLoginExitoso = true;
                }
                catch
                {
                    if (conexion.State == System.Data.ConnectionState.Open) conexion.Close();
                }
                finally
                {
                    if (conexion.State == System.Data.ConnectionState.Open) conexion.Close();
                }
            }


            if (!mLoginExitoso)
            {
                mRetorna = false;
                mensaje = "Usuario o contraseña inválidos";
            }
        }
        catch
        {
            mRetorna = false;
            mensaje = "Usuario o contraseña inválidos.";
        }

        return mRetorna;
    }

    [WebMethod]
    public bool DevuelveDocumentos(string usuario, ref string mensaje, string empresa, string anio, string mes, ref DataSet ds, bool todos)
    {
        bool mRetorna = true;
        dbDataContext db = new dbDataContext();
        
        try
        {
            int mNumeroMes;

            switch (mes)
            {
                case "Enero":
                    mNumeroMes = 1;
                    break;
                case "Febrero":
                    mNumeroMes = 2;
                    break;
                case "Marzo":
                    mNumeroMes = 3;
                    break;
                case "Abril":
                    mNumeroMes = 4;
                    break;
                case "Mayo":
                    mNumeroMes = 5;
                    break;
                case "Junio":
                    mNumeroMes = 6;
                    break;
                case "Julio":
                    mNumeroMes = 7;
                    break;
                case "Agosto":
                    mNumeroMes = 8;
                    break;
                case "Septiembre":
                    mNumeroMes = 9;
                    break;
                case "Octubre":
                    mNumeroMes = 10;
                    break;
                case "Noviembre":
                    mNumeroMes = 11;
                    break;
                default:
                    mNumeroMes = 12;
                    break;
            }

            int mAnio = 0;
            int mMes = mNumeroMes;

            try
            {
                mAnio = Convert.ToInt32(anio);
            }
            catch
            {
                mensaje = "El año ingresado es inválido";
                return false;
            }

            if (mAnio <= 0)
            {
                mensaje = "El año ingresado es inválido";
                return false;
            }

            DateTime mFechaInicial = new DateTime(mAnio, mMes, 1);
            DateTime mFechaFinal = mFechaInicial.AddMonths(1);

            mFechaFinal = new DateTime(mFechaFinal.Year, mFechaFinal.Month, 1);
            mFechaFinal = mFechaFinal.AddDays(-1);
            mFechaFinal = new DateTime(mFechaFinal.Year, mFechaInicial.Month, mFechaFinal.Day, 23, 59, 59);

            if (empresa == "Empresas Concretas, S.A.")
            {
                empresa = "emprconc";
            }
            else
            {
                empresa = "prodmult";
            }

            string mNit = ""; string mRazonSocial = ""; string mDireccion = "";

            string mAmbiente = "DES";

            if (db.Connection.ConnectionString.Contains("sql.fiesta.local")) mAmbiente = "PRO";
            if (db.Connection.ConnectionString.Contains("PRUEBAS")) mAmbiente = "PRU";

            string mServidor = "(local)";
            string mBase = "EXACTUSERP";

            if (mAmbiente == "PRO")
            {
                mBase = "EXACTUSERP";
                mServidor = "sql.fiesta.local";
            }

            if (mAmbiente == "PRU")
            {
                mBase = "PRUEBAS";
                mServidor = "sql.fiesta.local";
            }

            SqlCommand mCommand = new SqlCommand(); SqlDataReader mLector;
            string mStringConexion = string.Format("Data Source={0}; Initial Catalog = {1};User id = mf.fiestanet;password = 54321; Max Pool Size=600;Pooling=false;Application Name=PDV", mServidor, mBase);

            using (SqlConnection conexion = new SqlConnection(mStringConexion))
            {
                try
                {
                    conexion.Open();
                    mCommand.Connection = conexion;

                    mCommand.CommandText = string.Format("SELECT USAR_PRUEBAS FROM {0}.MF_GFaceConfigura", empresa);
                    string mPruebas = Convert.ToString(mCommand.ExecuteScalar());

                    if (mPruebas == "S")
                    {
                        mPruebas = "_PRUEBAS";
                    }
                    else
                    {
                        mPruebas = "";
                    }

                    mCommand.CommandText = string.Format("SELECT NIT{1}, SUCURSAL{1}, DIRECCION FROM {0}.MF_GFaceConfigura", empresa, mPruebas);
                    mLector = mCommand.ExecuteReader();

                    while (mLector.Read())
                    {
                        mNit = Convert.ToString(mLector[0]);
                        mRazonSocial = Convert.ToString(mLector[1]);
                        mDireccion = Convert.ToString(mLector[2]);
                    }
                    mLector.Close();

                    mCommand.CommandText = string.Format("SELECT COUNT(*) FROM {0}.MF_GFace WHERE anio = {1} AND mes = {2}", empresa, mAnio, mMes);
                    int mCuantos = Convert.ToInt32(mCommand.ExecuteScalar());

                    if (todos) mCuantos = 0;
                    //string mSqlEncabezado = ""; string mSqlDetalle = ""; string mSqlDetalle2 = "";

                    SqlConnection mConexion = new SqlConnection(db.Connection.ConnectionString);

                    SqlDataAdapter daFacturas = new SqlDataAdapter("prodmult.spGFace", mConexion);
                    daFacturas.SelectCommand.CommandType = CommandType.StoredProcedure;

                    daFacturas.SelectCommand.Parameters.Add("@FechaInicial", SqlDbType.DateTime).Value = mFechaInicial;
                    daFacturas.SelectCommand.Parameters.Add("@FechaFinal", SqlDbType.DateTime).Value = mFechaFinal;
                    daFacturas.SelectCommand.Parameters.Add("@Empresa", SqlDbType.VarChar).Value = empresa;

                    daFacturas.SelectCommand.CommandTimeout = 999999999;
                    daFacturas.Fill(ds);

                    ds.Tables[0].TableName = "Encabezado";
                    ds.Tables[1].TableName = "Detalle";

                    if (mCuantos > 0)
                    {
                        for (int ii = 0; ii < ds.Tables["Encabezado"].Rows.Count; ii++)
                        {
                            string mDocumento = Convert.ToString(ds.Tables["Encabezado"].Rows[ii]["Documento"]);

                            mCommand.CommandText = string.Format("SELECT COUNT(*) FROM {0}.MF_GFace WHERE DOCUMENTO = '{1}'", empresa, mDocumento);
                            if (Convert.ToInt32(mCommand.ExecuteScalar()) > 0)
                            {
                                mCommand.CommandText = string.Format("SELECT FIRMADO FROM {0}.MF_GFace WHERE DOCUMENTO = '{1}'", empresa, mDocumento);
                                if (Convert.ToString(mCommand.ExecuteScalar()) == "S")
                                {
                                    mCommand.CommandText = string.Format("SELECT FIRMA FROM {0}.MF_GFace WHERE DOCUMENTO = '{1}'", empresa, mDocumento);
                                    string mFirma = Convert.ToString(mCommand.ExecuteScalar());

                                    ds.Tables["Encabezado"].Rows[ii]["Firmado"] = "S";
                                    ds.Tables["Encabezado"].Rows[ii]["Firma"] = mFirma;
                                }
                            }

                        }

                        //mSqlEncabezado = string.Format("SELECT COBRADOR AS Tienda, ANIO AS Anio, MES AS Mes, DOCUMENTO AS Documento, FIRMADO AS Firmado, FIRMA AS Firma, NIT_VENDEDOR AS NitVendedor, RAZON_SOCIAL AS RazonSocial, TIPO AS Tipo, FECHA AS Fecha, SERIE AS Serie, " +
                        //    "NUMERO_DOCUMENTO AS NumeroDocumento, NOMBRE_CLIENTE AS NombreCliente, NIT AS Nit, DIRECCION AS Direccion, DEPARTAMENTO AS Departamento, MUNICIPIO AS Municipio, SUBTOTAL AS SubTotal, IVA AS IVA, " +
                        //    "TOTAL AS Total, ANULADA AS Anulada, FECHA_RESOLUCION AS FechaResolucion, NUMERO_RESOLUCION AS NumeroResolucion, FOLIO_INICIAL AS FolioInicial, FOLIO_FINAL AS FolioFinal, SUCURSAL AS Sucursal, " +
                        //    "NOMBRE_SUCURSAL AS NombreSucursal, DIRECCION_SUCURSAL AS DireccionSucursal, DEPARTAMENTO_SUCURSAL AS DepartamentoSucursal, MUNICIPIO_SUCURSAL AS MunicipioSucursal, NUMEROS_LETRAS AS NumerosLetras, " +
                        //    "TIPO_ACTIVO AS TipoActivo, MONEDA AS Moneda, TASA_CAMBIO AS TasaCambio, CLIENTE AS Cliente, BIEN_SERVICIO AS BienServicio, XML_DOCUMENTO AS XMLDocumento, XML_FIRMADO AS XML_Firmado, XML_ERROR AS Error, DIRECCION_VENDEDOR AS DireccionVendedor " +
                        //    "FROM {2}.MF_GFace WHERE ANIO = {0} AND MES = {1} ORDER BY FECHA_DOCUMENTO, DOCUMENTO", mAnio, mMes, empresa);

                        //mSqlDetalle = string.Format("SELECT COBRADOR AS Tienda, ANIO AS Anio, MES AS Mes, DOCUMENTO AS Documento, FECHA AS Fecha, TIPO AS Tipo, SERIE AS Serie, NUMERO_DOCUMENTO AS NumeroDocumento, " +
                        //    "DESCRIPCION AS Descripcion, SUBTOTAL AS SubTotal, IVA AS IVA, TOTAL AS Total, CANTIDAD AS Cantidad, PRECIO_UNITARIO AS PrecioUnitario " +
                        //    "FROM {2}.MF_GFaceDetalle WHERE ANIO = {0} AND MES = {1} ORDER BY FECHA_DOCUMENTO, DOCUMENTO", mAnio, mMes, empresa);

                        //mSqlDetalle2 = string.Format("SELECT COBRADOR AS Tienda, ANIO AS Anio, MES AS Mes, DOCUMENTO AS Documento, FECHA AS Fecha, TIPO AS Tipo, SERIE AS Serie, NUMERO_DOCUMENTO AS NumeroDocumento, " +
                        //    "DESCRIPCION AS Descripcion, SUBTOTAL AS SubTotal, IVA AS IVA, TOTAL AS Total, CANTIDAD AS Cantidad, PRECIO_UNITARIO AS PrecioUnitario " +
                        //    "FROM {0}.MF_GFaceDetalle WHERE ANIO = 0 AND MES = 0 ORDER BY FECHA_DOCUMENTO, DOCUMENTO", empresa);

                        //SqlDataAdapter daEncabezado = new SqlDataAdapter(mSqlEncabezado, conexion);
                        //SqlDataAdapter daDetalle = new SqlDataAdapter(mSqlDetalle, conexion);
                        //SqlDataAdapter daDetalle2 = new SqlDataAdapter(mSqlDetalle2, conexion);

                        //daEncabezado.SelectCommand.Parameters.Add("@fechaInicial", SqlDbType.DateTime).Value = mFechaInicial;
                        //daEncabezado.SelectCommand.Parameters.Add("@fechaFinal", SqlDbType.DateTime).Value = mFechaFinal;

                        //daDetalle.SelectCommand.Parameters.Add("@fechaInicial", SqlDbType.DateTime).Value = mFechaInicial;
                        //daDetalle.SelectCommand.Parameters.Add("@fechaFinal", SqlDbType.DateTime).Value = mFechaFinal;

                        //daDetalle2.SelectCommand.Parameters.Add("@fechaInicial", SqlDbType.DateTime).Value = mFechaInicial;
                        //daDetalle2.SelectCommand.Parameters.Add("@fechaFinal", SqlDbType.DateTime).Value = mFechaFinal;

                        //DataTable dtDetalle = new DataTable("Detalle");
                        //DataTable dtEncabezado = new DataTable("Encabezado");

                        //daEncabezado.Fill(dtEncabezado);
                        //daDetalle.Fill(dtDetalle);
                        //daDetalle2.Fill(dtDetalle);

                        //ds.Tables.Add(dtEncabezado);
                        //ds.Tables.Add(dtDetalle);
                    }



                    ds.Tables["Encabezado"].Columns["Firma"].DataType = typeof(System.String);
                    ds.Tables["Encabezado"].Columns["Error"].DataType = typeof(System.String);
                    ds.Tables["Encabezado"].Columns["Firmado"].DataType = typeof(System.String);

                    ds.Tables["Encabezado"].Columns["Firma"].ReadOnly = false;
                    ds.Tables["Encabezado"].Columns["Error"].ReadOnly = false;
                    ds.Tables["Encabezado"].Columns["Firmado"].ReadOnly = false;


                    SqlCommand mCommand2 = new SqlCommand(); SqlTransaction mTransaction2;
                    using (SqlConnection conexion2 = new SqlConnection(mStringConexion))
                    {
                        conexion2.Open();
                        mTransaction2 = conexion2.BeginTransaction();

                        mCommand2.Connection = conexion2;
                        mCommand2.Transaction = mTransaction2;

                        mCommand2.CommandText = "DELETE FROM prodmult.MF_FacturaGface";
                        mCommand2.ExecuteNonQuery();

                        for (int ii = 0; ii < ds.Tables["Encabezado"].Rows.Count; ii++)
                        {
                            mCommand2.CommandText = string.Format("INSERT INTO prodmult.MF_FacturaGface ( FACTURA ) VALUES ( '{0}' )", ds.Tables["Encabezado"].Rows[ii]["Documento"]);
                            mCommand2.ExecuteNonQuery();
                        }

                        mTransaction2.Commit();
                    }
                    
                    DataColumn[] dc = new DataColumn[1];
                    dc[0] = ds.Tables["Encabezado"].Columns["Documento"];
                    ds.Tables["Encabezado"].PrimaryKey = dc;

                    if (todos)
                    {
                        for (int ii = 0; ii < ds.Tables["Encabezado"].Rows.Count; ii++)
                        {
                            mCommand.CommandText = string.Format("SELECT COUNT(*) FROM {0}.MF_GFace WHERE DOCUMENTO = '{1}' AND FIRMADO = 'S'", empresa, ds.Tables["Encabezado"].Rows[ii]["Documento"]);
                            if (Convert.ToInt32(mCommand.ExecuteScalar()) > 0)
                            {
                                ds.Tables["Encabezado"].Rows[ii]["Firmado"] = "S";

                                mCommand.CommandText = string.Format("SELECT FIRMA FROM {0}.MF_GFace WHERE DOCUMENTO = '{1}' AND FIRMADO = 'S'", empresa, ds.Tables["Encabezado"].Rows[ii]["Documento"]);
                                ds.Tables["Encabezado"].Rows[ii]["Firma"] = Convert.ToString(mCommand.ExecuteScalar());
                            }
                        }
                    }

                    conexion.Close();
                }
                catch (Exception ex)
                {
                    string m = "";
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                    }
                    catch
                    {
                        try
                        {
                            m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                        }
                        catch
                        {
                            //Nothing
                        }
                    }

                    mRetorna = false;
                    mensaje = string.Format("Error preparando los documentos WS. {0} {1}", ex.Message, m);
                    if (conexion.State == System.Data.ConnectionState.Open) conexion.Close();
                    return false;
                }
                finally
                {
                    if (conexion.State == System.Data.ConnectionState.Open) conexion.Close();
                }
            }



        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }

            mRetorna = false;
            mensaje = string.Format("Error al devolver los documentos WS. {0} {1}", ex.Message, m);
        }

        return mRetorna;
    }

    
    [WebMethod]
    public bool FirmarDocumento(string usuario, ref string mensaje, string empresa, DataSet ds, ref string firmado, ref string firma, string tipoServicio = "N")
    {
        bool mRetorna = true;
        dbDataContext db = new dbDataContext();

        try
        {
            if (empresa == "Empresas Concretas, S.A.")
            {
                empresa = "emprconc";
            }
            else
            {
                empresa = "prodmult";
            }

            string mAmbiente = "DES";
            
            if (db.Connection.ConnectionString.Contains("sql.fiesta.local")) mAmbiente = "PRO";
            if (db.Connection.ConnectionString.Contains("PRUEBAS")) mAmbiente = "PRU";

            string mServidor = "(local)";
            string mBase = "EXACTUSERP";

            if (mAmbiente == "PRO")
            {
                mBase = "EXACTUSERP";
                mServidor = "sql.fiesta.local";
            }

            if (mAmbiente == "PRU")
            {
                mBase = "PRUEBAS";
                mServidor = "sql.fiesta.local";
            }

            string mEstablecimiento = (from r in db.RESOLUCION_DOC_ELECTRONICOs where r.NUMERO_AUTORIZACION == ds.Tables["Encabezado"].Rows[0]["NumeroResolucion"].ToString() select r).First().SUCURSAL;

            string xmlData = "<?xml version='1.0'?>";
            xmlData += "<FactDocGT xmlns='http://www.fact.com.mx/schema/gt' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='http://www.fact.com.mx/schema/gt http://www.mysuitemex.com/fact/schema/fx_2012_gt.xsd'>";
            xmlData += "<Version>2</Version>";
            xmlData += "<AsignacionSolicitada>";
            xmlData += string.Format("<Serie>{0}</Serie>", ds.Tables["Encabezado"].Rows[0]["Serie"]);
            xmlData += string.Format("<NumeroDocumento>{0}</NumeroDocumento>", Convert.ToInt64(ds.Tables["Encabezado"].Rows[0]["NumeroDocumento"]));
            xmlData += string.Format("<FechaEmision>{0}</FechaEmision>", ds.Tables["Encabezado"].Rows[0]["Fecha"]);
            xmlData += string.Format("<NumeroAutorizacion>{0}</NumeroAutorizacion>", ds.Tables["Encabezado"].Rows[0]["NumeroResolucion"]);
            xmlData += string.Format("<FechaResolucion>{0}</FechaResolucion>", ds.Tables["Encabezado"].Rows[0]["FechaResolucion"]);
            xmlData += string.Format("<RangoInicialAutorizado>{0}</RangoInicialAutorizado>", ds.Tables["Encabezado"].Rows[0]["FolioInicial"]);
            xmlData += string.Format("<RangoFinalAutorizado>{0}</RangoFinalAutorizado>", ds.Tables["Encabezado"].Rows[0]["FolioFinal"]);
            xmlData += "</AsignacionSolicitada>";
            xmlData += "<Encabezado>";
            xmlData += string.Format("<TipoActivo>{0}</TipoActivo>", ds.Tables["Encabezado"].Rows[0]["TipoActivo"]);
            xmlData += string.Format("<CodigoDeMoneda>{0}</CodigoDeMoneda>", ds.Tables["Encabezado"].Rows[0]["Moneda"]);
            xmlData += string.Format("<TipoDeCambio>{0}</TipoDeCambio>", ds.Tables["Encabezado"].Rows[0]["TasaCambio"]);
            xmlData += "<InformacionDeRegimenIsr>PAGO_TRIMESTRAL</InformacionDeRegimenIsr>";
            xmlData += "<ReferenciaInterna>00000</ReferenciaInterna>";
            xmlData += "</Encabezado>";
            xmlData += "<Vendedor>";
            xmlData += string.Format("<Nit>{0}</Nit>", ds.Tables["Encabezado"].Rows[0]["NitVendedor"]);
            xmlData += string.Format("<NombreComercial>{0}</NombreComercial>", ds.Tables["Encabezado"].Rows[0]["RazonSocial"]);
            xmlData += "<Idioma>es</Idioma>";
            xmlData += "<DireccionDeEmisionDeDocumento>";
            xmlData += string.Format("<NombreDeEstablecimiento>{0}</NombreDeEstablecimiento>", ds.Tables["Encabezado"].Rows[0]["RazonSocial"]);
            xmlData += string.Format("<CodigoDeEstablecimiento>{0}</CodigoDeEstablecimiento>", mEstablecimiento);
            xmlData += "<DispositivoElectronico>001</DispositivoElectronico>";
            xmlData += string.Format("<Direccion1>{0}</Direccion1>", ds.Tables["Encabezado"].Rows[0]["DireccionVendedor"].ToString().Replace("&", "").Replace("/", "").Replace("|", ""));
            xmlData += "<Direccion2>.</Direccion2>";
            xmlData += string.Format("<Municipio>{0}</Municipio>", ds.Tables["Encabezado"].Rows[0]["MunicipioSucursal"]);
            xmlData += string.Format("<Departamento>{0}</Departamento>", ds.Tables["Encabezado"].Rows[0]["DepartamentoSucursal"]);
            xmlData += "<CodigoDePais>GT</CodigoDePais>";
            xmlData += "<CodigoPostal>010109</CodigoPostal>";
            xmlData += "</DireccionDeEmisionDeDocumento>";
            xmlData += "</Vendedor>";
            xmlData += "<Comprador>";
            xmlData += string.Format("<Nit>{0}</Nit>", ds.Tables["Encabezado"].Rows[0]["Nit"]);
            xmlData += string.Format("<NombreComercial>{0}</NombreComercial>", ds.Tables["Encabezado"].Rows[0]["Cliente"]);
            xmlData += "<Idioma>es</Idioma>";
            xmlData += "<DireccionComercial>";
            xmlData += string.Format("<Direccion1>{0}</Direccion1>", ds.Tables["Encabezado"].Rows[0]["Direccion"].ToString().Replace("&", "").Replace("/", "").Replace("|", ""));
            xmlData += "<Direccion2>.</Direccion2>";
            xmlData += string.Format("<Municipio>{0}</Municipio>", ds.Tables["Encabezado"].Rows[0]["MunicipioSucursal"]);
            xmlData += string.Format("<Departamento>{0}</Departamento>", ds.Tables["Encabezado"].Rows[0]["DepartamentoSucursal"]);
            xmlData += "<CodigoDePais>GT</CodigoDePais>";
            xmlData += "<CodigoPostal>010109</CodigoPostal>";
            xmlData += "<Telefono>-</Telefono>";
            xmlData += "</DireccionComercial>";
            xmlData += "</Comprador>";
            xmlData += "<Detalles>";

            for (int ii = 0; ii < ds.Tables["Detalle"].Rows.Count; ii++)
            {
                xmlData += "<Detalle>";
                xmlData += string.Format("<Descripcion>{0}</Descripcion>", ds.Tables["Detalle"].Rows[ii]["Descripcion"].ToString().Replace("&", "").Replace("/", "").Replace("|", ""));
                xmlData += "<CodigoEAN>00000000000000</CodigoEAN>";
                xmlData += "<UnidadDeMedida>PZA</UnidadDeMedida>";
                xmlData += string.Format("<Cantidad>{0}</Cantidad>", ds.Tables["Detalle"].Rows[ii]["Cantidad"]);
                xmlData += "<ValorSinDR>";
                xmlData += string.Format("<Precio>{0}</Precio>", ds.Tables["Detalle"].Rows[ii]["SubTotal"]);
                xmlData += string.Format("<Monto>{0}</Monto>", ds.Tables["Detalle"].Rows[ii]["SubTotal"]);
                xmlData += "</ValorSinDR>";
                xmlData += "<DescuentosYRecargos>";
                xmlData += "<SumaDeDescuentos>0</SumaDeDescuentos>";
                xmlData += "<DescuentoORecargo>";
                xmlData += "<Operacion>DESCUENTO</Operacion>";
                xmlData += "<Servicio>ALLOWANCE_GLOBAL</Servicio>";
                xmlData += string.Format("<Base>{0}</Base>", ds.Tables["Detalle"].Rows[ii]["SubTotal"]);
                xmlData += "<Tasa>0</Tasa>";
                xmlData += "<Monto>0</Monto>";
                xmlData += "</DescuentoORecargo>";
                xmlData += "</DescuentosYRecargos>";
                xmlData += "<ValorConDR>";
                xmlData += string.Format("<Precio>{0}</Precio>", ds.Tables["Detalle"].Rows[ii]["SubTotal"]);
                xmlData += string.Format("<Monto>{0}</Monto>", ds.Tables["Detalle"].Rows[ii]["SubTotal"]);
                xmlData += "</ValorConDR>";
                xmlData += "<Impuestos>";
                xmlData += string.Format("<TotalDeImpuestos>{0}</TotalDeImpuestos>", ds.Tables["Detalle"].Rows[ii]["IVA"]);
                xmlData += string.Format("<IngresosNetosGravados>0</IngresosNetosGravados>", ds.Tables["Detalle"].Rows[ii]["SubTotal"]);
                xmlData += string.Format("<TotalDeIVA>0</TotalDeIVA>", ds.Tables["Detalle"].Rows[ii]["IVA"]);
                xmlData += "<Impuesto>";
                xmlData += "<Tipo>IVA</Tipo>";
                xmlData += string.Format("<Base>{0}</Base>", ds.Tables["Detalle"].Rows[ii]["SubTotal"]);
                xmlData += "<Tasa>12</Tasa>";
                xmlData += string.Format("<Monto>{0}</Monto>", ds.Tables["Detalle"].Rows[ii]["IVA"]);
                xmlData += "</Impuesto>";
                xmlData += "</Impuestos>";
                xmlData += string.Format("<Categoria>{0}</Categoria>", ds.Tables["Encabezado"].Rows[0]["BienServicio"]);
                xmlData += "</Detalle>";
            }

            xmlData += "</Detalles>";
            xmlData += "<Totales>";
            xmlData += string.Format("<SubTotalSinDR>{0}</SubTotalSinDR>", ds.Tables["Encabezado"].Rows[0]["SubTotal"]);
            xmlData += "<DescuentosYRecargos>";
            xmlData += "<SumaDeDescuentos>0.00</SumaDeDescuentos>";
            xmlData += "<DescuentoORecargo>";
            xmlData += "<Operacion>DESCUENTO</Operacion>";
            xmlData += "<Servicio>ALLOWANCE_GLOBAL</Servicio>";
            xmlData += string.Format("<Base>{0}</Base>", ds.Tables["Encabezado"].Rows[0]["SubTotal"]);
            xmlData += "<Tasa>0.000000</Tasa>";
            xmlData += "<Monto>0.00</Monto>";
            xmlData += "</DescuentoORecargo>";
            xmlData += "</DescuentosYRecargos>";
            xmlData += string.Format("<SubTotalConDR>{0}</SubTotalConDR>", ds.Tables["Encabezado"].Rows[0]["SubTotal"]);
            xmlData += "<Impuestos>";
            xmlData += string.Format("<TotalDeImpuestos>{0}</TotalDeImpuestos>", ds.Tables["Encabezado"].Rows[0]["IVA"]);
            xmlData += string.Format("<IngresosNetosGravados>{0}</IngresosNetosGravados>", ds.Tables["Encabezado"].Rows[0]["SubTotal"]);
            xmlData += string.Format("<TotalDeIVA>{0}</TotalDeIVA>", ds.Tables["Encabezado"].Rows[0]["IVA"]);
            xmlData += "<Impuesto>";
            xmlData += "<Tipo>IVA</Tipo>";
            xmlData += string.Format("<Base>3982.14</Base>", ds.Tables["Encabezado"].Rows[0]["SubTotal"]);
            xmlData += "<Tasa>12</Tasa>";
            xmlData += string.Format("<Monto>477.86</Monto>", ds.Tables["Encabezado"].Rows[0]["IVA"]);
            xmlData += "</Impuesto>";
            xmlData += "</Impuestos>";
            xmlData += string.Format("<Total>{0}</Total>", ds.Tables["Encabezado"].Rows[0]["Total"]);
            xmlData += string.Format("<TotalLetras>CUATRO  MIL CUATROCIENTOS SESENTA CON 0/100</TotalLetras>", ds.Tables["Encabezado"].Rows[0]["NumerosLetras"]);
            xmlData += "</Totales>";
            xmlData += "</FactDocGT>";

            SqlCommand mCommand = new SqlCommand(); SqlDataReader mLector; SqlTransaction mTransaction = null; string[] mFactura = new string[12];
            string mStringConexion = string.Format("Data Source={0}; Initial Catalog = {1};User id = mf.fiestanet;password = 54321; Max Pool Size=600;Pooling=false;Application Name=PDV", mServidor, mBase);

            using (SqlConnection conexion = new SqlConnection(mStringConexion))
            {
                try
                {
                    conexion.Open();
                    mTransaction = conexion.BeginTransaction();

                    mCommand.Connection = conexion;
                    mCommand.Transaction = mTransaction;

                    mCommand.CommandText = string.Format("SELECT USAR_PRUEBAS FROM {0}.MF_GFaceConfigura", empresa);
                    string mPruebas = Convert.ToString(mCommand.ExecuteScalar());

                    if (mPruebas == "S")
                    {
                        mPruebas = "_PRUEBAS";
                    }
                    else
                    {
                        mPruebas = "";
                    }

                    string mNit = ""; string mUsuario = ""; string mRequestor = ""; string mUrl = ""; 

                    mCommand.CommandText = string.Format("SELECT NIT{1}, USUARIO{1}, REQUESTOR{1}, URL{1} FROM {0}.MF_GFaceConfigura", empresa, mPruebas);
                    mLector = mCommand.ExecuteReader();

                    while (mLector.Read())
                    {
                        mNit = Convert.ToString(mLector[0]);
                        mUsuario = Convert.ToString(mLector[1]);
                        mRequestor = Convert.ToString(mLector[2]);
                        mUrl = Convert.ToString(mLector[3]);
                    }
                    mLector.Close();

                    firma = "";
                    firmado = "N";

                    if (tipoServicio == "RecuperarFirma")
                    {
                        string mSerie = string.Format("{0}", ds.Tables["Encabezado"].Rows[0]["Serie"]);
                        string mFolio = string.Format("{0}", Convert.ToInt64(ds.Tables["Encabezado"].Rows[0]["NumeroDocumento"]));
                        mFactura = wsConnector.wsEnvio(mSerie, mFolio, "XML", mUsuario, mUrl, mRequestor, "GET_DOCUMENT", "GT", mNit, false, "");
                    }
                    else
                    {
                        mFactura = wsConnector.wsEnvio(xmlData, "XML", "", mUsuario, mUrl, mRequestor, "CONVERT_NATIVE_XML", "GT", mNit, false, "");
                    }

                    mensaje = mFactura[2];
                    if (mFactura[1] == "True")
                    {
                        firmado = "S";
                        firma = mFactura[6];
                    }

                    mCommand.Parameters.Add("@XmlDocumento", SqlDbType.Text).Value = xmlData;
                    mCommand.Parameters.Add("@XmlFirmado", SqlDbType.Text).Value = mFactura[5];
                    mCommand.Parameters.Add("@XmlError", SqlDbType.Text).Value = mFactura[2];
                    mCommand.Parameters.Add("@FechaDocumento", SqlDbType.DateTime).Value = Convert.ToDateTime(ds.Tables["Encabezado"].Rows[0]["FechaDocumento"]);
                    mCommand.Parameters.Add("@FechaCreado", SqlDbType.DateTime).Value = DateTime.Now.Date;
                    mCommand.Parameters.Add("@FechaHoraCreado", SqlDbType.DateTime).Value = DateTime.Now;
                    
                    bool mBorrar = true;

                    mCommand.CommandText = string.Format("SELECT COUNT(*) FROM {0}.MF_GFace WHERE DOCUMENTO = '{1}'", empresa, ds.Tables["Encabezado"].Rows[0]["Documento"]);
                    if (Convert.ToInt32(mCommand.ExecuteScalar()) > 0)
                    {
                        mCommand.CommandText = string.Format("SELECT FIRMADO FROM {0}.MF_GFace WHERE DOCUMENTO = '{1}'", empresa, ds.Tables["Encabezado"].Rows[0]["Documento"]);
                        if (Convert.ToString(mCommand.ExecuteScalar()) == "S") mBorrar = false;
                    }
                    
                    if (mBorrar)
                    {
                        mCommand.CommandText = string.Format("DELETE FROM {0}.MF_GFace WHERE DOCUMENTO = '{1}'", empresa, ds.Tables["Encabezado"].Rows[0]["Documento"]);
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = string.Format("INSERT INTO {0}.MF_GFace ( ANIO, MES, COBRADOR, DOCUMENTO, NIT_VENDEDOR, RAZON_SOCIAL, TIPO, FECHA, SERIE, NUMERO_DOCUMENTO, NOMBRE_CLIENTE, NIT, DIRECCION, DEPARTAMENTO, MUNICIPIO, SUBTOTAL, " +
                          "IVA, TOTAL, ANULADA, FECHA_RESOLUCION, NUMERO_RESOLUCION, FOLIO_INICIAL, FOLIO_FINAL, SUCURSAL, NOMBRE_SUCURSAL, DIRECCION_SUCURSAL, DEPARTAMENTO_SUCURSAL, " +
                          "MUNICIPIO_SUCURSAL, NUMEROS_LETRAS, TIPO_ACTIVO, MONEDA, TASA_CAMBIO, CLIENTE, BIEN_SERVICIO, XML_DOCUMENTO, XML_FIRMADO, XML_ERROR, FIRMADO, FIRMA, FECHA_DOCUMENTO, DIRECCION_VENDEDOR, USUARIO, FECHA_CREADO, FECHA_HORA_CREADO ) " +
                          "VALUES ( {1}, {2}, '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', {16}, {17}, {18}, '{19}', '{20}', '{21}', '{22}', '{23}', '{24}', '{25}', '{26}', " +
                          "'{27}', '{28}', '{29}', '{30}', '{31}', '{32}', '{33}', '{34}', @XmlDocumento, @XmlFirmado, @XmlError, '{35}', '{36}', @FechaDocumento, '{37}', '{38}', @FechaCreado, @FechaHoraCreado )",
                          empresa, ds.Tables["Encabezado"].Rows[0]["Anio"], ds.Tables["Encabezado"].Rows[0]["Mes"], ds.Tables["Encabezado"].Rows[0]["Tienda"],
                          ds.Tables["Encabezado"].Rows[0]["Documento"], ds.Tables["Encabezado"].Rows[0]["NitVendedor"], ds.Tables["Encabezado"].Rows[0]["RazonSocial"], ds.Tables["Encabezado"].Rows[0]["Tipo"], ds.Tables["Encabezado"].Rows[0]["Fecha"],
                          ds.Tables["Encabezado"].Rows[0]["Serie"], ds.Tables["Encabezado"].Rows[0]["NumeroDocumento"], ds.Tables["Encabezado"].Rows[0]["NombreCliente"], ds.Tables["Encabezado"].Rows[0]["Nit"], ds.Tables["Encabezado"].Rows[0]["Direccion"],
                          ds.Tables["Encabezado"].Rows[0]["Departamento"], ds.Tables["Encabezado"].Rows[0]["Municipio"], ds.Tables["Encabezado"].Rows[0]["SubTotal"], ds.Tables["Encabezado"].Rows[0]["IVA"], ds.Tables["Encabezado"].Rows[0]["Total"],
                          ds.Tables["Encabezado"].Rows[0]["Anulada"], ds.Tables["Encabezado"].Rows[0]["FechaResolucion"], ds.Tables["Encabezado"].Rows[0]["NumeroResolucion"], ds.Tables["Encabezado"].Rows[0]["FolioInicial"],
                          ds.Tables["Encabezado"].Rows[0]["FolioFinal"], ds.Tables["Encabezado"].Rows[0]["Sucursal"], ds.Tables["Encabezado"].Rows[0]["NombreSucursal"], ds.Tables["Encabezado"].Rows[0]["DireccionSucursal"],
                          ds.Tables["Encabezado"].Rows[0]["DepartamentoSucursal"], ds.Tables["Encabezado"].Rows[0]["MunicipioSucursal"], ds.Tables["Encabezado"].Rows[0]["NumerosLetras"], ds.Tables["Encabezado"].Rows[0]["TipoActivo"],
                          ds.Tables["Encabezado"].Rows[0]["Moneda"], ds.Tables["Encabezado"].Rows[0]["TasaCambio"], ds.Tables["Encabezado"].Rows[0]["Cliente"], ds.Tables["Encabezado"].Rows[0]["BienServicio"],
                          firmado, firma, ds.Tables["Encabezado"].Rows[0]["DireccionVendedor"], usuario);
                        mCommand.ExecuteNonQuery();
                    }

                    mCommand.CommandText = string.Format("DELETE FROM {0}.MF_GFaceDetalle WHERE DOCUMENTO = '{1}'", empresa, ds.Tables["Encabezado"].Rows[0]["Documento"]);
                    mCommand.ExecuteNonQuery();

                    for (int ii = 0; ii < ds.Tables["Detalle"].Rows.Count; ii++)
                    {
                        mCommand.Parameters["@FechaDocumento"].Value = Convert.ToDateTime(ds.Tables["Detalle"].Rows[ii]["FechaDocumento"]);

                        mCommand.CommandText = string.Format("INSERT INTO {0}.MF_GFaceDetalle ( ANIO, MES, COBRADOR, DOCUMENTO, TIPO, FECHA, SERIE, NUMERO_DOCUMENTO, DESCRIPCION, CANTIDAD, PRECIO_UNITARIO, SUBTOTAL, IVA, TOTAL, " +
                            "FECHA_DOCUMENTO, USUARIO, FECHA_CREADO, FECHA_HORA_CREADO  ) VALUES ( {1}, {2}, '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', {10}, {11}, {12}, {13}, {14}, @FechaDocumento, '{15}', @FechaCreado, @FechaHoraCreado ) ", empresa,
                            ds.Tables["Detalle"].Rows[ii]["Anio"], ds.Tables["Detalle"].Rows[ii]["Mes"], ds.Tables["Detalle"].Rows[ii]["Tienda"], ds.Tables["Detalle"].Rows[ii]["Documento"], ds.Tables["Detalle"].Rows[ii]["Tipo"],
                            ds.Tables["Detalle"].Rows[ii]["Fecha"], ds.Tables["Detalle"].Rows[ii]["Serie"], ds.Tables["Detalle"].Rows[ii]["NumeroDocumento"], ds.Tables["Detalle"].Rows[ii]["Descripcion"], ds.Tables["Detalle"].Rows[ii]["Cantidad"],
                            ds.Tables["Detalle"].Rows[ii]["PrecioUnitario"], ds.Tables["Detalle"].Rows[ii]["SubTotal"], ds.Tables["Detalle"].Rows[ii]["IVA"], ds.Tables["Detalle"].Rows[ii]["Total"], usuario);
                        mCommand.ExecuteNonQuery();
                    }

                    mTransaction.Commit();
                    conexion.Close();

                }
                catch (Exception ex)
                {
                    string m = "";
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                    }
                    catch
                    {
                        try
                        {
                            m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                        }
                        catch
                        {
                            //Nothing
                        }
                    }

                    if (mFactura[1] == "True")
                    {
                        using (SmtpClient smtp = new SmtpClient())
                        {
                            using (MailMessage mail = new MailMessage())
                            {
                                mail.Subject = string.Format("Error en {0} grabando la firma del documento {1} {2}", empresa, ds.Tables["Encabezado"].Rows[0]["Documento"], firma);
                                
                                mail.IsBodyHtml = true;
                                mail.From = new MailAddress(WebConfigurationManager.AppSettings["ITMailNotification"].ToString());


                                StringBuilder mBody = new StringBuilder();

                                mBody.Append("<!DOCTYPE html><html lang='en-us'>");
                                mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } </style></head>");
                                mBody.Append("<body>");
                                mBody.Append(mFactura[5]);
                                mBody.Append("<br><br>");
                                mBody.Append(string.Format("Error firmando el documento WS- {0} {1}", ex.Message, m));
                                mBody.Append("</body>");
                                mBody.Append("</html>");

                                mail.Body = mBody.ToString();

                                smtp.Host = WebConfigurationManager.AppSettings["MailHost"];
                                smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort"]);
                                smtp.EnableSsl = true;
                                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                                try
                                {
                                    smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail"], WebConfigurationManager.AppSettings["PwdMail"]);
                                    smtp.Send(mail);
                                }
                                catch (Exception ex2)
                                {
                                    string mm = "";
                                    try
                                    {
                                        mm = ex2.StackTrace.Substring(ex2.StackTrace.IndexOf("línea"));
                                    }
                                    catch
                                    {
                                        try
                                        {
                                            mm = ex2.StackTrace.Substring(ex2.StackTrace.IndexOf("line"));
                                        }
                                        catch
                                        {
                                            //Nothing
                                        }
                                    }
                                }
                            }
                        }                      
                    }

                    mTransaction.Rollback();
                    mensaje = string.Format("Error firmando el documento WS. - {0}{1}{2}", ex.Message, m, mCommand.CommandText);
                    return false;
                }
            }

            if (xmlData.Trim().Length == 0)
            {
                xmlData += "";
            }

        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }

            mRetorna = false;
            mensaje = string.Format("Error al firmar el documento WS. {0} {1}", ex.Message, m);
        }

        return mRetorna;
    }

}
