﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Transactions;
using FiestaNETServices.Services;

/// <summary>
/// Summary description for wsPedidosFundas
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class wsPedidosFundas : System.Web.Services.WebService
{

    [WebMethod]
    public bool GrabarRelacionFundas(string funda, string[] codigos, string descripcion, ref string mensaje)
    {
        bool mRetorna = true;

        try
        {
            dbDataContext db = new dbDataContext();

            using (TransactionScope transactionScope = new TransactionScope())
            {
                var BorrarRelacion =
                    from a in db.MF_ArticulosRelacions
                    where a.Articulo == funda
                    select a;
                foreach (var del in BorrarRelacion)
                {
                    db.MF_ArticulosRelacions.DeleteOnSubmit(del);
                }
                db.SubmitChanges();

                for (int ii = 0; ii < codigos.Length; ii++)
                {
                    MF_ArticulosRelacion iRelacion = new MF_ArticulosRelacion
                    {
                        Articulo = funda,
                        ArticuloRelacion = codigos[ii].ToString(),
                        Descripcion = descripcion,
                        RecordDate = DateTime.Now,
                        CreatedBy = System.Environment.UserName
                    };
                    db.MF_ArticulosRelacions.InsertOnSubmit(iRelacion);
                    db.SubmitChanges();
                }

                transactionScope.Complete();
                mensaje = string.Format("La relación de la funda fue grabada exitosamente.");
            }

        }
        catch (Exception ex)
        {
            mRetorna = false;
            mensaje = string.Format("Se produjo el siguiente error al grabar la relación de la funda.  {0}", ex.Message);
        }

        return mRetorna;
    }

    [WebMethod]
    public ArticulosRelacion[] ListarRelacionFundas(string articulo, ref string NombreArticulo, ref string DescripcionRelacion)
    {
        return DataLayerPedidosFundas.ListarArticulosRelacion(articulo, ref NombreArticulo, ref DescripcionRelacion).ToArray();
    }

    [WebMethod]
    public EstructuraRelacion[] ListarEstructuraRelacion()
    {
        return DataLayerPedidosFundas.ListarEstructuraRelacion().ToArray();
    }

    [WebMethod]
    public PedidoFundas[] SugeridoPedido(string bodega)
    {
        return DataLayerPedidosFundas.SugeridoPedido(bodega).ToArray();
    }

    [WebMethod]
    public bool GrabarSugeridoPedido(int pedido, string descripcion, List<PedidoFundas> q, ref string mensaje, string bodega)
    {
        return DataLayerPedidosFundas.GrabarSugeridoPedido(pedido, descripcion, q, ref mensaje, bodega);
    }

    [WebMethod]
    public Bodegas[] ListarBodegas()
    {
        return DataLayerPedidosFundas.ListarBodegas().ToArray();
    }

    [WebMethod]
    public ExistenciasArticulo[] ListarExistenciasArticulo(string articulo, string bodega)
    {
        return DataLayerPedidosFundas.ListarExistenciasArticulo(articulo, bodega).ToArray();
    }

    [WebMethod]
    public List<ExistenciasArticuloRelacion> ListarExistenciasArticuloRelacion(string articulo, string bodega)
    {
        return DataLayerPedidosFundas.ListarExistenciasArticuloRelacion(articulo, bodega);
    }
       
    [WebMethod]
    public List<ExistenciasArticuloRelacion> ListarExistenciasArticuloRelacionBases(string articulo, string bodega)
    {
        return DataLayerPedidosFundas.ListarExistenciasArticuloRelacionBases(articulo, bodega);
    }

    [WebMethod]
    public Pedidos[] ListarPedidos()
    {
        return DataLayerPedidosFundas.ListarPedidos().ToArray();
    }

    [WebMethod]
    public int ExistenciasArticuloRelacion(string articulo, string bodega)
    {
        return DataLayerPedidosFundas.ExistenciasArticuloRelacion(articulo, bodega);
    }

    [WebMethod]
    public bool EliminarPedido(int pedido, ref string mensaje)
    {
        return DataLayerPedidosFundas.EliminarPedido(pedido, ref mensaje);
    }

    [WebMethod]
    public PedidoFundas[] ListarPedido(int pedido)
    {
        return DataLayerPedidosFundas.ListarPedido(pedido).ToArray();
    }

}
