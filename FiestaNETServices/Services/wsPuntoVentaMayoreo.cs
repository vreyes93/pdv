﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Transactions;
using FiestaNETServices.Services;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Security.Cryptography;
using Newtonsoft.Json;

/// <summary>
/// Summary description for wsPuntoVentaMayoreo
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class wsPuntoVentaMayoreo : System.Web.Services.WebService
{
    [WebMethod]
    public ArticulosMayoreo[] DevuelveArticulosMayoreo(string cliente)
    {
        return DataLayerPuntoVentaMayoreo.DevuelveArticulosMayoreo(cliente).ToArray();
    }

    [WebMethod]
    public bool LoginExitoso(string usuario, string password, ref string nombre, ref string cliente, ref string NombreCliente, ref string mensaje, ref string cambiaPassword, ref bool usuarioPM)
    {
        return DataLayerPuntoVentaMayoreo.LoginExitoso(usuario, password, ref nombre, ref cliente, ref NombreCliente, ref mensaje, ref cambiaPassword, ref usuarioPM);
    }

    [WebMethod]
    public bool GrabarPedido(ref string Pedido, DataSet dsInfo, ref string mensaje, string usuario, string cliente, string observaciones, string ordenCompra)
    {
        return DataLayerPuntoVentaMayoreo.GrabarPedido(ref Pedido, dsInfo, ref mensaje, usuario, cliente, observaciones, ordenCompra);
    }

    [WebMethod]
    public string DevuelveCorreoUsuario(string usuario)
    {
        return DataLayerPuntoVentaMayoreo.DevuelveCorreoUsuario(usuario);
    }

    [WebMethod]
    public bool ResetPassword(string usuario, ref string mensaje)
    {
        return DataLayerPuntoVentaMayoreo.ResetPassword(usuario, ref mensaje);
    }

    [WebMethod]
    public bool CambiaPassword(string usuario, string password1, string password2, ref string mensaje)
    {
        return DataLayerPuntoVentaMayoreo.CambiaPassword(usuario, password1, password2, ref mensaje);
    }

    [WebMethod]
    public ClientesMayoreo[] DevuelveClientesMayoreo(string usuario, ref bool mostrar)
    {
        dbDataContext db = new dbDataContext();

        var q = (from a in db.MF_ArticulosMayoreos
                 join c in db.CLIENTEs on a.CLIENTE equals c.CLIENTE1
                 where c.ACTIVO == "S"
                 select new ClientesMayoreo()
                 {
                     Cliente = a.CLIENTE,
                     Nombre = c.NOMBRE
                 }).Distinct().OrderBy(x => x.Nombre);

        int mCuantos = (from g in db.MF_Gerentes where g.USUARIO == usuario select g).Count();

        mostrar = false;
        if (mCuantos > 0) mostrar = true;

        return q.ToArray();
    }

    [WebMethod]
    public bool CambiarCliente(string usuario, string cliente, ref string mensaje, ref string nombre)
    {
        bool mRetorna = true;
        dbDataContext db = new dbDataContext();

        try
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                var q = from u in db.MF_UsuarioMayoreos where u.USUARIO == usuario select u;
                foreach (var item in q)
                {
                    item.CLIENTE = cliente;
                }
                db.SubmitChanges();

                nombre = (from c in db.CLIENTEs where c.CLIENTE1 == cliente select c).First().NOMBRE;

                transactionScope.Complete();
                mensaje = "Listo!";
            }
        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }
            mRetorna = false;
            mensaje = string.Format("Error WS {0} {1}", ex.Message, m);
        }

        return mRetorna;
    }

    [WebMethod]
    public List<Departamentos> DevuelveDepartamentos()
    {
        return DataLayerPuntoVenta.DevuelveDepartamentos();
    }

    [WebMethod]
    public List<Municipios> DevuelveMunicipios(string departamento)
    {
        return DataLayerPuntoVenta.DevuelveMunicipios(departamento);
    }

    [WebMethod]
    public PedidoMayoreo[] DevuelvePedido(string pedido)
    {
        dbDataContext db = new dbDataContext();

        var q = from p in db.MF_Pedido join pe in db.PEDIDOs on p.PEDIDO equals pe.PEDIDO1
                where p.PEDIDO == pedido
                select new PedidoMayoreo()
                {
                    Pedido = p.PEDIDO,
                    FechaEntrega = Convert.ToDateTime(p.FECHA_ENTREGA),
                    NombreRecibe = p.NOMBRE_RECIBE,
                    OrdenCompra = p.AUTORIZACION,
                    Observaciones = p.NOTAS_TIPOVENTA,
                    CemacoTienda = p.CEMACO_TIENDA,
                    CemacoFechaVenta = Convert.ToDateTime(p.CEMACO_FECHA_VENTA),
                    CemacoCaja = p.CEMACO_CAJA,
                    CemacoTransaccion = p.CEMACO_TRANSACCION,
                    CemacoClienteNombre = p.CEMACO_CLIENTE_NOMBRE,
                    CemacoClienteTelefono = p.CEMACO_CLIENTE_TELEFONO,
                    CemacoClienteDireccion = p.CEMACO_CLIENTE_DIRECCION,
                    CemacoClienteDepartamento = p.CEMACO_CLIENTE_DEPARTAMENTO,
                    CemacoClienteMunicipio = p.CEMACO_CLIENTE_MUNICIPIO,
                    CemacoClienteZona = p.CEMACO_CLIENTE_ZONA,
                    CemacoClienteIndicaciones = p.CEMACO_CLIENTE_INDICACIONES,
                    CemacoClienteSegundoPiso = Convert.ToString(p.CEMACO_CLIENTE_SEGUNDO_PISO),
                    CemacoClienteEntraCamion = Convert.ToString(p.CEMACO_CLIENTE_ENTRA_CAMION),
                    CemacoClienteEntraPickup = Convert.ToString(p.CEMACO_CLIENTE_ENTRA_PICKUP),
                    Vendedor=pe.VENDEDOR
                };

        return q.ToArray();
    }
    
    [WebMethod]
    public bool ExistePedido(string pedido, ref string mensaje)
    {
        bool mRetorna = true;
        dbDataContext db = new dbDataContext();

        var q = from p in db.PEDIDOs where p.PEDIDO1 == pedido select p;

        if (q.Count() == 0)
        {
            mRetorna = false;
            mensaje = "El pedido ingresado no existe.";
        }
        else
        {
            if (q.First().ESTADO != "N")
            {
                mRetorna = false;
                mensaje = "El pedido está cancelado o facturado, no se puede modificar.";
            }
        }

        return mRetorna;
    }
    
    [WebMethod]
    public List<UsuariosMayoreo> ObtenerUsuariosMayoreo(string cliente)
    {
        return DataLayerPuntoVentaMayoreo.ObtenerUsuariosMayoreo(cliente);
    }

    [WebMethod]
    public List<TipoDescuento> ObtenerTipoDescuentoMayoreo(string cliente)
    {
        return DataLayerPuntoVentaMayoreo.ObtenerTipoDescuento(cliente);
    }
    [WebMethod]
    public List<string> DevuelveClientesEspeciales()
    {
        return DataLayerPuntoVentaMayoreo.DevuelveClientesEspeciales();
    }
    [WebMethod]
    public bool HayArticulosDespachoCentral(string cliente, List<string> lstArticulos)
    {
        return DataLayerPuntoVentaMayoreo.HayArticulosDespachoCentral(cliente, lstArticulos);
    }
}
