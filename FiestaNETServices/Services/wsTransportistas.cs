﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Transactions;
using FiestaNETServices.Services;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for wsTransportistas
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class wsTransportistas : System.Web.Services.WebService
{

    [WebMethod]
    public bool LoginExitoso(string usuario, string password, ref string nombre, ref string mensaje)
    {
        bool mRetorna = true;
        dbDataContext db = new dbDataContext();

        try
        {
            string mAmbiente = "DES";

            if (db.Connection.ConnectionString.Contains("sql.fiesta.local")) mAmbiente = "PRO";
            if (db.Connection.ConnectionString.Contains("PRUEBAS")) mAmbiente = "PRU";

            string mServidor = "(local)";
            string mBase = "EXACTUSERP";

            if (mAmbiente == "PRO")
            {
                mBase = "EXACTUSERP";
                mServidor = "sql.fiesta.local";
            }

            if (mAmbiente == "PRU")
            {
                mBase = "PRUEBAS";
                mServidor = "sql.fiesta.local";
            }

            bool mLoginExitoso = false;
            string mStringConexion = string.Format("Data Source={0}; Initial Catalog = {1};User id = {2};password = {3}; Max Pool Size=600;Pooling=false;Application Name=PDV", mServidor, mBase, usuario, password);

            using (SqlConnection conexion = new SqlConnection(mStringConexion))
            {
                try
                {
                    conexion.Open();
                    conexion.Close();

                    mLoginExitoso = true;
                }
                catch
                {
                    if (conexion.State == System.Data.ConnectionState.Open) conexion.Close();
                }
                finally
                {
                    if (conexion.State == System.Data.ConnectionState.Open) conexion.Close();
                }
            }


            if (mLoginExitoso)
            {
                if ((from u in db.USUARIOs where u.USUARIO1 == usuario select u).First().ACTIVO == "N")
                {
                    mRetorna = false;
                    mensaje = "El usuario no está activo en el sistema.";
                }
                else
                {
                    nombre = (from u in db.USUARIOs where u.USUARIO1 == usuario select u).First().NOMBRE;
                }
            }
            else
            {
                mRetorna = false;
                mensaje = "Usuario o contraseña inválidos";
            }
        }
        catch
        {
            mRetorna = false;
            mensaje = "Usuario o contraseña inválidos.";
        }

        return mRetorna;
    }

    [WebMethod]
    public bool DocumentoValido(string tipo, string documento, ref DataTable dtArticulos, ref string mensaje)
    {
        bool mRetorna;
        dbDataContext db = new dbDataContext();
        //Traspaso
        try
        {
            DataTable dt = new DataTable("transportistas");
            
            dt.Columns.Add(new DataColumn("Tipo", typeof(System.String)));
            dt.Columns.Add(new DataColumn("Documento", typeof(System.String)));
            dt.Columns.Add(new DataColumn("Linea", typeof(System.Int32)));
            dt.Columns.Add(new DataColumn("Articulo", typeof(System.String)));
            dt.Columns.Add(new DataColumn("Descripcion", typeof(System.String)));
            dt.Columns.Add(new DataColumn("Transportista", typeof(System.Int32)));
            dt.Columns.Add(new DataColumn("Fecha", typeof(System.DateTime)));

            if (tipo == "Envío")
            {
                int mCuantos = (from d in db.DESPACHOs where d.DESPACHO1 == documento select d).Count();
                if (mCuantos == 0)
                {
                    mensaje = "El número de envío ingresado es inválido.";
                    return false;
                }

                string mEstado = (from d in db.DESPACHOs where d.DESPACHO1 == documento select d).First().ESTADO;
                if (mEstado == "A")
                {
                    mensaje = "El número de envío ingresado está anulado.";
                    return false;
                }

                if ((from d in db.MF_DespachoTransportistas where d.TIPO == Convert.ToChar("D") && d.DOCUMENTO == documento select d).Count() == 0)
                {
                    var q = from d in db.DESPACHO_DETALLEs join a in db.ARTICULOs on d.ARTICULO equals a.ARTICULO1 where d.DESPACHO == documento orderby d.LINEA select new { Linea = d.LINEA, Articulo = d.ARTICULO, Descripcion = a.DESCRIPCION };
                    foreach (var item in q)
                    {
                        DataRow row = dt.NewRow();
                        row["Tipo"] = "D";
                        row["Documento"] = documento;
                        row["Linea"] = item.Linea;
                        row["Articulo"] = item.Articulo;
                        row["Descripcion"] = item.Descripcion;
                        row["Transportista"] = 1;
                        row["Fecha"] = DateTime.Now.Date;
                        dt.Rows.Add(row);
                    }
                }
                else
                {
                    var q = from d in db.MF_DespachoTransportistas join a in db.ARTICULOs on d.ARTICULO equals a.ARTICULO1 where d.TIPO == Convert.ToChar("D") && d.DOCUMENTO == documento orderby d.LINEA select new { Linea = d.LINEA, Articulo = d.ARTICULO, Descripcion = a.DESCRIPCION, Transportista = d.TRANSPORTISTA, Fecha = d.FECHA };
                    foreach (var item in q)
                    {
                        DataRow row = dt.NewRow();
                        row["Tipo"] = "D";
                        row["Documento"] = documento;
                        row["Linea"] = item.Linea;
                        row["Articulo"] = item.Articulo;
                        row["Descripcion"] = item.Descripcion;
                        row["Transportista"] = item.Transportista;
                        row["Fecha"] = item.Fecha;
                        dt.Rows.Add(row);
                    }
                }
            }
            else
            {
                int mCuantos = (from a in db.AUDIT_TRANS_INVs where a.APLICACION == documento select a).Count();
                if (mCuantos == 0)
                {
                    mensaje = "El número de traspaso es inválido.";
                    return false;
                }

                if ((from d in db.MF_DespachoTransportistas where d.TIPO == Convert.ToChar("T") && d.DOCUMENTO == documento select d).Count() == 0)
                {
                    int mAudit = (from a in db.AUDIT_TRANS_INVs where a.APLICACION == documento select a).First().AUDIT_TRANS_INV1;

                    var q = from t in db.TRANSACCION_INVs join a in db.ARTICULOs on t.ARTICULO equals a.ARTICULO1 where t.AUDIT_TRANS_INV == mAudit orderby t.CONSECUTIVO select new { Linea = t.CONSECUTIVO, Articulo = t.ARTICULO, Descripcion = a.DESCRIPCION };
                    foreach (var item in q)
                    {
                        DataRow row = dt.NewRow();
                        row["Tipo"] = "T";
                        row["Documento"] = documento;
                        row["Linea"] = item.Linea;
                        row["Articulo"] = item.Articulo;
                        row["Descripcion"] = item.Descripcion;
                        row["Transportista"] = 1;
                        row["Fecha"] = DateTime.Now.Date;
                        dt.Rows.Add(row);
                    }
                }
                else
                {
                    var q = from d in db.MF_DespachoTransportistas join a in db.ARTICULOs on d.ARTICULO equals a.ARTICULO1 where d.TIPO == Convert.ToChar("T") && d.DOCUMENTO == documento orderby d.LINEA select new { Linea = d.LINEA, Articulo = d.ARTICULO, Descripcion = a.DESCRIPCION, Transportista = d.TRANSPORTISTA, Fecha = d.FECHA };
                    foreach (var item in q)
                    {
                        DataRow row = dt.NewRow();
                        row["Tipo"] = "T";
                        row["Documento"] = documento;
                        row["Linea"] = item.Linea;
                        row["Articulo"] = item.Articulo;
                        row["Descripcion"] = item.Descripcion;
                        row["Transportista"] = item.Transportista;
                        row["Fecha"] = item.Fecha;
                        dt.Rows.Add(row);
                    }
                }
            }

            dtArticulos = dt;
            mRetorna = true;
        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }
            mRetorna = false;
            mensaje = string.Format("Error al validar el documento WS. {0} {1}", ex.Message, m);
        }

        return mRetorna;
    }
    
    [WebMethod]
    public bool GrabarTransportistaDocumento(string usuario, ref string mensaje, DataTable dt)
    {
        dbDataContext db = new dbDataContext();
        bool mRetorna = true;
        
        try
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                for (int ii = 0; ii < dt.Rows.Count; ii++)
                {
                    if ((from d in db.MF_DespachoTransportistas where d.TIPO == Convert.ToChar(dt.Rows[ii]["Tipo"]) && d.DOCUMENTO == Convert.ToString(dt.Rows[ii]["Documento"]) && d.LINEA == Convert.ToInt16(dt.Rows[ii]["Linea"]) select d).Count() == 0)
                    {
                        MF_DespachoTransportista iDespachoTrans = new MF_DespachoTransportista
                        {
                            TIPO = Convert.ToChar(dt.Rows[ii]["Tipo"]),
                            DOCUMENTO = Convert.ToString(dt.Rows[ii]["Documento"]),
                            LINEA = Convert.ToInt16(dt.Rows[ii]["Linea"]),
                            ARTICULO = Convert.ToString(dt.Rows[ii]["Articulo"]),
                            TRANSPORTISTA = Convert.ToInt32(dt.Rows[ii]["Transportista"]),
                            FECHA = Convert.ToDateTime(dt.Rows[ii]["Fecha"]),
                            RecordDate = DateTime.Now,
                            CreatedBy = string.Format("FA/{0}", usuario),
                            UpdatedBy = string.Format("FA/{0}", usuario),
                            CreateDate = DateTime.Now
                        };

                        db.MF_DespachoTransportistas.InsertOnSubmit(iDespachoTrans);
                    }
                    else
                    {
                        var q = from d in db.MF_DespachoTransportistas where d.TIPO == Convert.ToChar(dt.Rows[ii]["Tipo"]) && d.DOCUMENTO == Convert.ToString(dt.Rows[ii]["Documento"]) && d.LINEA == Convert.ToInt16(dt.Rows[ii]["Linea"]) select d;
                        foreach (var item in q)
                        {
                            item.TRANSPORTISTA = Convert.ToInt32(dt.Rows[ii]["Transportista"]);
                            item.FECHA = Convert.ToDateTime(dt.Rows[ii]["Fecha"]);
                            item.RecordDate = DateTime.Now;
                            item.UpdatedBy = string.Format("FA/{0}", usuario);
                        }
                    }

                    var qDespacho = from d in db.MF_Despachos where d.DESPACHO == Convert.ToString(dt.Rows[ii]["Documento"]) select d;
                    foreach (var item in qDespacho)
                    {
                        item.TRANSPORTISTA = Convert.ToInt32(dt.Rows[ii]["Transportista"]);
                        item.FECHA_ENTREGA = Convert.ToDateTime(dt.Rows[ii]["Fecha"]);
                    }
                }
                
                db.SubmitChanges();
                transactionScope.Complete();
                mensaje = string.Format("El documento fue grabado exitosamente.");
            }
        }
        catch (Exception ex)
        {
            mRetorna = false;
            mensaje = string.Format("Error al grabar el documento WS. {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
        }

        return mRetorna;
    }
    


}
