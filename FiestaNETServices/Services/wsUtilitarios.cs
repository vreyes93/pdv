﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Transactions;
using FiestaNETServices.Services;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for wsUtilitarios
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class wsUtilitarios : System.Web.Services.WebService
{
    
    [WebMethod]
    public bool LocalizacionFactura(string factura, ref string mensaje)
    {
        bool mRetorna = true;
        dbDataContext db = new dbDataContext();
        
        try
        {
            SqlCommand mCommand = new SqlCommand();
            SqlTransaction mTransacion = null;
            string mStringConexion = db.Connection.ConnectionString;

            using (SqlConnection conexion = new SqlConnection(mStringConexion))
            {
                conexion.Open();

                mTransacion = conexion.BeginTransaction();
                mCommand.Connection = conexion;
                mCommand.Transaction = mTransacion;

                mCommand.Parameters.Add("@factura", SqlDbType.VarChar).Value = factura;

                mCommand.CommandText = "SELECT COUNT(*) FROM prodmult.FACTURA WHERE factura = @factura";
                if (Convert.ToInt32(mCommand.ExecuteScalar()) == 0)
                {
                    mensaje = "El número de factura ingresado no existe.";
                    mTransacion.Rollback();
                    conexion.Close();
                    return false;
                }

                mCommand.CommandText = "SELECT ANULADA FROM prodmult.FACTURA WHERE factura = @factura";
                if (Convert.ToString(mCommand.ExecuteScalar()) == "S")
                {
                    mensaje = "La factura ingresada está anulada.";
                    mTransacion.Rollback();
                    conexion.Close();
                    return false;
                }

                mCommand.CommandText = "UPDATE a SET a.LOCALIZACION = b.LOCALIZACION FROM prodmult.FACTURA_LINEA a JOIN prodmult.PEDIDO_LINEA b ON a.PEDIDO = b.PEDIDO AND a.PEDIDO_LINEA = b.PEDIDO_LINEA WHERE a.FACTURA = @factura";
                mCommand.ExecuteNonQuery();

                mensaje = "La factura fue actualizada exitosamente.";

                mTransacion.Commit();
                conexion.Close();
            }


        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }
            mRetorna = false;
            mensaje = string.Format("Error al grabar la localización WS. {0} {1}", ex.Message, m);
        }

        return mRetorna;
    }


    [WebMethod]
    public bool ArticuloFactura(string factura, string articulo, string articuloNuevo, ref string mensaje)
    {
        bool mRetorna = true;
        dbDataContext db = new dbDataContext();

        try
        {
            SqlCommand mCommand = new SqlCommand();
            SqlTransaction mTransacion = null;
            string mStringConexion = db.Connection.ConnectionString;

            using (SqlConnection conexion = new SqlConnection(mStringConexion))
            {
                conexion.Open();

                mTransacion = conexion.BeginTransaction();
                mCommand.Connection = conexion;
                mCommand.Transaction = mTransacion;

                mCommand.Parameters.Add("@factura", SqlDbType.VarChar).Value = factura;
                mCommand.Parameters.Add("@articulo", SqlDbType.VarChar).Value = articulo;
                mCommand.Parameters.Add("@articuloNuevo", SqlDbType.VarChar).Value = articuloNuevo;

                mCommand.CommandText = "SELECT COUNT(*) FROM prodmult.FACTURA WHERE FACTURA = @factura";
                if (Convert.ToInt32(mCommand.ExecuteScalar()) == 0)
                {
                    mensaje = "El número de factura ingresado no existe.";
                    mTransacion.Rollback();
                    conexion.Close();
                    return false;
                }

                mCommand.CommandText = "SELECT ANULADA FROM prodmult.FACTURA WHERE FACTURA = @factura";
                if (Convert.ToString(mCommand.ExecuteScalar()) == "S")
                {
                    mensaje = "La factura ingresada está anulada.";
                    mTransacion.Rollback();
                    conexion.Close();
                    return false;
                }

                mCommand.CommandText = "SELECT COUNT(*) FROM prodmult.FACTURA_LINEA WHERE FACTURA = @factura AND ARTICULO = @articulo";
                if (Convert.ToInt32(mCommand.ExecuteScalar()) == 0)
                {
                    mensaje = string.Format("El artículo {0} no existe en la factura {1}.", articulo, factura);
                    mTransacion.Rollback();
                    conexion.Close();
                    return false;
                }

                mCommand.CommandText = "SELECT COUNT(*) FROM prodmult.ARTICULO WHERE ARTICULO = @articuloNuevo";
                if (Convert.ToInt32(mCommand.ExecuteScalar()) == 0)
                {
                    mensaje = string.Format("El artículo {0} es inválido.", articuloNuevo);
                    mTransacion.Rollback();
                    conexion.Close();
                    return false;
                }

                mCommand.CommandText = "UPDATE a SET a.ARTICULO = @articuloNuevo, a.DESCRIPCION = b.DESCRIPCION FROM prodmult.FACTURA_LINEA a JOIN prodmult.ARTICULO b ON b.ARTICULO = @articuloNuevo WHERE a.FACTURA = @factura AND a.ARTICULO = @articulo";
                mCommand.ExecuteNonQuery();

                mensaje = "El artículo fue actualizdo exitosamente.";

                mTransacion.Commit();
                conexion.Close();
            }
        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }
            mRetorna = false;
            mensaje = string.Format("Error al grabar el artículo WS. {0} {1}", ex.Message, m);
        }

        return mRetorna;
    }


}
