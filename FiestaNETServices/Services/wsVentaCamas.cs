﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Transactions;
using FiestaNETServices.Services;

/// <summary>
/// Summary description for wsPedidosFundas
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class wsVentaCamas : System.Web.Services.WebService
{
      
    [WebMethod]
    public Camas[] ListarVentaCamas(DateTime FechaInicial, DateTime FechaFinal, string Bodega, string Tipo, bool Bases)
    {
        return DataLayerVentaCamas.ListarVentaCamas(FechaInicial, FechaFinal, Bodega, Tipo, Bases).ToArray();
    }

    [WebMethod]
    public Camas[] ListarVentaCamasMayoreo(DateTime FechaInicial, DateTime FechaFinal, string Bodega, string Tipo, bool Bases)
    {
        return DataLayerVentaCamas.ListarVentaCamasMayoreo(FechaInicial, FechaFinal, Bodega, Tipo, Bases).ToArray();
    }

    [WebMethod]
    public ClientesMayoreo[] ListarClientesMayoreo()
    {
        return DataLayerVentaCamas.ListarClientesMayoreo().ToArray();
    }

    [WebMethod]
    public BodegasVentaCamas[] ListarBodegas()
    {
        return DataLayerVentaCamas.ListarBodegas().ToArray();
    }

}
