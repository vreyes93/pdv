﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace FiestaNETServices.Util
{
    public class Mail
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función envia un correo electrónico del remitente a sus destinatarios con asunto y cuerpo del mensaje.
        /// </summary>
        /// <param name="Remitente">Correo electrónico del remitente.</param>
        /// <param name="CuentaCorreo">Correo electrónico del destinatario.</param>
        /// <param name="Asunto">Asunto del correo electrónico.</param>
        /// <param name="Mensaje">Mensaje o cuerpo del mensaje electrónico.</param>
        /// <param name="DisplayName">Nombre a desplegar en el correo electrónico.</param>
        /// <returns>Retorna una variable tipo Respuesta; 
        ///          Respuesta.exito = true si es existoso
        ///          Respuesta.exito = false si es fallido </returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public bool EnviarEmail(string Remitente, string CuentaCorreo, string Asunto, StringBuilder Mensaje, string DisplayName)
        {
            List<string> ListaCuentaCorreo = new List<string>();
            ListaCuentaCorreo.Add(CuentaCorreo);
            return EnviarEmail(Remitente, ListaCuentaCorreo, Asunto, Mensaje, DisplayName);
        }

       

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función envia un correo electrónico del remitente a sus destinatarios con asunto y cuerpo del mensaje.
        /// </summary>
        /// <param name="Remitente">Correo electrónico del remitente.</param>
        /// <param name="CuentaCorreo">Listado de correos electrónicos de los destinatarios.</param>
        /// <param name="Asunto">Asunto del correo electrónico.</param>
        /// <param name="Mensaje">Mensaje o cuerpo del mensaje electrónico.</param>
        /// <param name="DisplayName">Nombre a desplegar en el correo electrónico.</param>
        /// <param name="lstImagenes">Lista de imagenes a utilizarse como contenido en el correo, es opcional</param>
        /// <param name="BccTo">destinatario de correo para control</param>
        /// <returns>Retorna una variable tipo Respuesta; 
        ///          Respuesta.exito = true si es existoso
        ///          Respuesta.exito = false si es fallido </returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public bool EnviarEmail(string Remitente, List<string> CuentaCorreo, string Asunto, StringBuilder Mensaje, string DisplayName, string BccTo = null,List<Attachment> attachments=null,List<string> ReplyTo=null,int OpcionCorreo=0)
        {
            string mBitacora = "";
            string mSlack = "";
            string strMailHost = string.Empty;
            string strMailPort = string.Empty;
            string strUsrMail = string.Empty;
            string strUsrPwd = string.Empty;


            SmtpClient smtp = new SmtpClient();
            MailMessage mail = new MailMessage();
            
            //-------------------------------------------------------------------------------------------
            //Agregar la lista de correo

            foreach (var item in CuentaCorreo)
            {
                mail.To.Add(item);
            }
            //bcc
            if (BccTo != null && BccTo!=string.Empty)
                mail.Bcc.Add(BccTo);

            if (attachments != null)
                foreach (Attachment item in attachments)
                    mail.Attachments.Add(item);

            //-------------------------------------------------------------------------------------------
            //Responder a
            if (ReplyTo != null)
                foreach (string item in ReplyTo)
                    mail.ReplyToList.Add(item);

                try
            {
                //no responder a puntoDeventa
                if (mail.ReplyToList.Contains(new System.Net.Mail.MailAddress(Remitente)))
                    mail.ReplyToList.Remove(new System.Net.Mail.MailAddress(Remitente));
            }
            catch
            { }
            //-------------------------------------------------------------------------------------------
            //Establecer el remitente y el nombre a mostrar
            mail.From = new MailAddress(Remitente, DisplayName);

            //-------------------------------------------------------------------------------------------
            //Establecer a quien se le responerá
            mail.ReplyToList.Add(Remitente);

            //-------------------------------------------------------------------------------------------
            //Establecer el Asunto del correo
            mail.Subject = Asunto;

            //-------------------------------------------------------------------------------------------
            //Establecer el cuerpo del mensaje como html
            mail.IsBodyHtml = true;
            mail.Body = Mensaje.ToString();

            //-------------------------------------------------------------------------------------------
            //Establecer el host y puerto del servidor de correo
            strMailHost = OpcionCorreo == 0 ? "MailHost2" : "MailHost";
            strMailPort= OpcionCorreo == 0 ? "MailPort2" : "MailPort";

            smtp.Host = WebConfigurationManager.AppSettings[strMailHost];
            smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings[strMailPort]);

            smtp.Timeout = 5000;
            smtp.UseDefaultCredentials = false;
            //-------------------------------------------------------------------------------------------
            //Habilitar el protocolo SSL y el metodo de entrega
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

            mBitacora = string.Format(" {0}; {1}; {2}; {3}; ", "Correo", Remitente, string.Join(",", CuentaCorreo), Asunto);
            mSlack = string.Format("Clase: {0} \nRemitente:{1} \nLista de correo:{2} \nAsunto:{3} ", this.GetType().FullName, Remitente, string.Join(",", CuentaCorreo), Asunto);

            //-------------------------------------------------------------------------------------------
            //Se envia el correo, tras tres intentos fallidos envia por slack un mensaje
           
            try
            {//Primer intento para enviar correo
                strUsrMail = OpcionCorreo == 0 ? "UsrMail2" : "UsrMail";
                strUsrPwd= OpcionCorreo == 0 ? "PwdMail2" : "PwdMail";

                smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings[strUsrMail], WebConfigurationManager.AppSettings[strUsrPwd]);
                            smtp.Send(mail);
                            log.Info(mBitacora);
                       
                
            }
            catch(Exception exi)
            {
                try
                {
                    log.Error("ERROR CORREO MF " + exi.Message + " " + exi.InnerException +" "+mBitacora);
                }
                catch
                {
                    log.Error("ERROR CORREO MF " + exi.Message + " " + mBitacora);
                }
                    try
                    {//Intento con la cuenta de contingencia de productos multiples

                    //Establecer el host y puerto del servidor de correo
                    strMailHost = OpcionCorreo == 0 ? "MailHost" : "MailHost2";
                    strMailPort = OpcionCorreo == 0 ? "MailPort" : "MailPort2";

                        smtp.Host = WebConfigurationManager.AppSettings[strMailHost];
                        smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings[strMailPort]);
                        smtp.Timeout = 10000;
                        //-------------------------------------------------------------------------------------------
                        //Habilitar el protocolo SSL y el metodo de entrega
                        smtp.EnableSsl = true;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                        strUsrMail = OpcionCorreo == 0 ? "UsrMail" : "UsrMail2";
                        strUsrPwd = OpcionCorreo == 0 ? "PwdMail" : "PwdMail2";

                        smtp.Credentials = new System.Net.NetworkCredential( WebConfigurationManager.AppSettings[strUsrMail] , WebConfigurationManager.AppSettings[strUsrPwd]);
                        smtp.Send(mail);
                        log.Info("Envio por Contingencia" + mBitacora);
                    }
                    catch (Exception e2)
                    {
                       
                        log.Error("Error correo de Contingencia" + string.Format(mBitacora + " {0};", e2.Message));
                        EnviarMensajeSlack(string.Format(mSlack + " \nERROR correo contingencia - {0};",e2.Message));
                        return false;
                    }
                    //--------------------------------------------------------------------------------------------------------
                
            }
            return true;
        }


        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función envia un mensaje por slack al canal de errores del PV
        /// </summary>
        /// <param name="Mensaje">Mensaje a enviar por Slack.</param>
        //-----------------------------------------------------------------------------------------------------------------------
        public static void EnviarMensajeSlack(string Mensaje)
        {
            //----------------------------------------------------------------
            //Validamos si esta habilitado el modulo
            //----------------------------------------------------------------
            string json = "";
            try
            {
                WebRequest request = WebRequest.Create(string.Format("{0}", WebConfigurationManager.AppSettings["ErrorPDVSlackChannel"]));
                json = "{" + string.Format(" \"text\": \"Alerta desde el PDV. \n{0}.\" ", Mensaje.Replace("{", "").Replace("}", "")) + "}";
                byte[] data = Encoding.ASCII.GetBytes(json);

                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

            }

            catch (Exception e)
            {
                log.Error(string.Format("ERROR {0};", e.Message));
            }
        }
    }
}