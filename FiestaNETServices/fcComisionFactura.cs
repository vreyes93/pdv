﻿//USE [EXACTUSERP]
//GO

///****** Object:  UserDefinedFunction [prodmult].[fcComisionFactura]    Script Date: 07/21/2016 07:54:00 ******/
//SET ANSI_NULLS ON
//GO

//SET QUOTED_IDENTIFIER ON
//GO


//ALTER FUNCTION [prodmult].[fcComisionFactura] (@Factura AS VARCHAR(20)) RETURNS DECIMAL(12,2) AS
//BEGIN
	
//    IF ((SELECT COUNT(*) FROM prodmult.FACTURA WHERE FACTURA = @Factura AND TIPO_DOCUMENTO = 'F') = 0) RETURN 0
//    IF ((SELECT ANULADA FROM prodmult.FACTURA WHERE FACTURA = @Factura AND TIPO_DOCUMENTO = 'F') = 'S') RETURN 0	
	
//    DECLARE @MontoComisionable AS DECIMAL(12,2) = 0
//    DECLARE @MontoComisionableLinea AS DECIMAL(12,2) = 0
//    DECLARE @IvaConfigura AS DECIMAL(12, 2) =  1 + ((SELECT IMPUESTO1 FROM prodmult.IMPUESTO WHERE IMPUESTO = 'IVA') / 100)
//    DECLARE @Iva AS DECIMAL(12, 2) = @IvaConfigura
//    DECLARE @PrecioUnitario AS DECIMAL(12, 2)
//    DECLARE @PrecioTotal AS DECIMAL(12, 2)
//    DECLARE @PrecioLista AS DECIMAL(12, 2)
//    DECLARE @PrecioEvaluar AS DECIMAL(12, 2)
//    DECLARE @PreciosBase AS DECIMAL(28, 8)
//    DECLARE @Multiplicar AS DECIMAL(12, 2)
//    DECLARE @PrecioBaseSumar AS DECIMAL(28, 8)
//    DECLARE @PrecioListaEvaluar AS DECIMAL(12, 2)
//    DECLARE @PrecioListaEvaluarString AS VARCHAR(50)
//    DECLARE @TipoVenta AS VARCHAR(4)
//    DECLARE @NivelPrecio AS VARCHAR(20)
//    DECLARE @PorcentajeDescuento AS DECIMAL(12, 2)
//    DECLARE @PorcentajeDescuentoCama AS DECIMAL(12, 2)
//    DECLARE @PrecioBaseLocal AS DECIMAL(28, 8)
//    DECLARE @Oferta AS CHAR(1)
//    DECLARE @Articulo AS VARCHAR(20)
//    DECLARE @FechaOfertaDesde AS DATETIME
//    DECLARE @PrecioBaseArticulo AS DECIMAL(28, 8)
//    DECLARE @Valor AS DECIMAL(12, 2) = (SELECT TOTAL_FACTURA FROM prodmult.FACTURA WHERE FACTURA = @Factura AND TIPO_DOCUMENTO = 'F')
//    DECLARE @YaCalculado AS INT
//    DECLARE @EsDescuentoAutorizado AS CHAR(1)
//    DECLARE @Financiera AS INT 
//    DECLARE @PrecioTotalFacturar AS DECIMAL(12, 2)

//    IF ((SELECT COUNT(*) FROM prodmult.MF_Factura WHERE FACTURA = @Factura) > 0) SET @Iva = 1 + ((SELECT PCTJ_IVA FROM prodmult.MF_Factura WHERE FACTURA = @Factura) / 100)
	
//    IF ((SELECT COUNT(*) FROM prodmult.MF_ComisionExcepcion WHERE FACTURA = @Factura) = 0)
//    BEGIN
	
//        IF ((SELECT COALESCE(p.COTIZACION, 0) FROM prodmult.MF_Pedido p WHERE p.FACTURA = @Factura) > 0)
//        BEGIN
//            IF ((SELECT c.ESTATUS FROM prodmult.MF_Pedido p JOIN prodmult.MF_Cotizacion c ON p.COTIZACION = c.COTIZACION WHERE p.FACTURA = @Factura) = 'A') SET @EsDescuentoAutorizado = 'S'
//        END
	
//        DECLARE #CursorLineas CURSOR LOCAL FOR
//        SELECT p.PRECIO_UNITARIO, p.PRECIO_TOTAL, p.PRECIO_LISTA, p.PORCENTAJE_DESCUENTO, p.PRECIO_BASE_LOCAL, p.OFERTA, p.ARTICULO, p.FECHAOFERTADESDE, p.TIPOVENTA, p.NIVEL_PRECIO, p.FINANCIERA, p.PRECIO_TOTAL_FACTURAR 
//        FROM prodmult.MF_Pedido_Linea p WHERE p.FACTURA = @Factura

//        OPEN #CursorLineas
//        FETCH NEXT FROM #CursorLineas INTO @PrecioUnitario, @PrecioTotal, @PrecioLista, @PorcentajeDescuento, @PrecioBaseLocal, @Oferta, @Articulo, @FechaOfertaDesde, @TipoVenta, @NivelPrecio, @Financiera, @PrecioTotalFacturar

//        WHILE @@FETCH_STATUS = 0
//        BEGIN
//            SET @YaCalculado = 0
//            SET @MontoComisionableLinea = 0
//            SET @PrecioEvaluar = 0
			
//            IF (@Financiera = 7)
//            BEGIN
//                SET @MontoComisionableLinea = @PrecioTotalFacturar
//            END
//            ELSE
//            BEGIN
//                IF (@Oferta = 'S')
//                BEGIN
//                    IF ((SELECT COUNT(*) FROM prodmult.MF_ArticuloNivelPrecioOferta WHERE NivelPrecio = @NivelPrecio AND ARTICULO = @Articulo AND FechaOfertaDesde = @FechaOfertaDesde) > 0)
//                    BEGIN
//                        SET @YaCalculado = 1
//                        SET @PorcentajeDescuentoCama = @PorcentajeDescuento
//                        IF (@PorcentajeDescuento = 0) SET @PorcentajeDescuentoCama = (SELECT pctjDescuento FROM prodmult.MF_ArticuloNivelPrecioOferta WHERE NivelPrecio = @NivelPrecio AND ARTICULO = @Articulo AND FechaOfertaDesde = @FechaOfertaDesde)
						
//                        SET @MontoComisionableLinea = (@PrecioBaseLocal * @Iva) * (1 - (@PorcentajeDescuentoCama / 100))
//                    END
//                END
				
//                IF (@YaCalculado = 0)
//                BEGIN
//                    SET @Multiplicar = @PorcentajeDescuento
//                    IF (@PorcentajeDescuento = 0) SET @Multiplicar = 1
//                    IF (@PorcentajeDescuento > 1) SET @Multiplicar = 1 - (@PorcentajeDescuento / 100)
					
//                    SET @PrecioEvaluar = @PrecioLista
					
//                    SET @MontoComisionableLinea = @PrecioEvaluar * @Multiplicar * 0.88
									
//                    IF (@EsDescuentoAutorizado = 'S') SET @MontoComisionableLinea = @PrecioTotal * 0.88
//                END			
//            END
			
//            SET @MontoComisionable = @MontoComisionable + @MontoComisionableLinea
			
//            FETCH NEXT FROM #CursorLineas INTO @PrecioUnitario, @PrecioTotal, @PrecioLista, @PorcentajeDescuento, @PrecioBaseLocal, @Oferta, @Articulo, @FechaOfertaDesde, @TipoVenta, @NivelPrecio, @Financiera, @PrecioTotalFacturar
//        END
//        CLOSE #CursorLineas
//        DEALLOCATE #CursorLineas
//    END 
//    ELSE
//    BEGIN
//        IF ((SELECT TIPO FROM prodmult.MF_ComisionExcepcion WHERE FACTURA = @Factura) = 'N')
//        BEGIN
//            SET @MontoComisionable = 0
//        END
//        ELSE
//        BEGIN
//            SET @MontoComisionable = (SELECT MONTO_COMISION FROM prodmult.MF_ComisionExcepcion WHERE FACTURA = @Factura)
//        END
//    END

//    RETURN @MontoComisionable
//END



//GO


