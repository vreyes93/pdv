﻿//USE [EXACTUSERP]
//GO

///****** Object:  StoredProcedure [prodmult].[spComisiones]    Script Date: 07/21/2016 07:52:12 ******/
//SET ANSI_NULLS ON
//GO

//SET QUOTED_IDENTIFIER ON
//GO


//ALTER PROCEDURE [prodmult].[spComisiones] @FechaInicial AS DATETIME, @FechaFinal AS DATETIME, @CodigoTienda AS VARCHAR(4) AS
//BEGIN

//BEGIN TRY
//    DROP TABLE #temp
//END TRY
//BEGIN CATCH
//    --Nothing
//END CATCH

//BEGIN TRY	
//    CREATE TABLE #temp ( Factura VARCHAR(20), Valor DECIMAL(12,2), MontoComisionable DECIMAL(12,2), ValorNeto DECIMAL(12,2), Comision DECIMAL(12,2), Porcentaje DECIMAL(12,2), 
//        FechaFactura DATETIME, FechaEntrega DATETIME, Vendedor VARCHAR(4), NombreVendedor VARCHAR(100), Tienda VARCHAR(4), Titulo VARCHAR(20) ) 
//END TRY
//BEGIN CATCH 	
//    --Nothing 
//END CATCH

//DELETE FROM #temp

//DECLARE @Factura AS VARCHAR(20)
//DECLARE @Valor AS DECIMAL(12, 2)
//DECLARE @MontoComisionable AS DECIMAL(12, 2)
//DECLARE @ValorNeto AS DECIMAL(12, 2)
//DECLARE @FechaFactura AS DATETIME
//DECLARE @FechaEntrega AS DATETIME
//DECLARE @Vendedor AS VARCHAR(4)
//DECLARE @NombreVendedor AS VARCHAR(100)
//DECLARE @Tienda AS VARCHAR(4)

//DECLARE @Iva AS DECIMAL(12,2)
//DECLARE @IvaConfigura AS DECIMAL(12, 2) =  1 + ((SELECT IMPUESTO1 FROM prodmult.IMPUESTO WHERE IMPUESTO = 'IVA') / 100)

//IF (@CodigoTienda = 'T')
//BEGIN
//    --Cursor de Facturas Todas
//    DECLARE #CursorFacturasTodas CURSOR LOCAL FOR
//    SELECT d.FACTURA, f.TOTAL_FACTURA AS Valor, f.FECHA AS FechaFactura, d.FECHA_ENTREGA AS FechaEntrega, d.VENDEDOR AS Vendedor, v.NOMBRE AS NombreVendedor, d.COBRADOR AS Tienda 
//    FROM prodmult.MF_Despacho d JOIN prodmult.DESPACHO de ON d.DESPACHO = de.DESPACHO JOIN prodmult.FACTURA f on d.FACTURA = f.FACTURA JOIN prodmult.VENDEDOR v ON d.VENDEDOR = v.VENDEDOR 
//    WHERE d.FECHA_ENTREGA >= @FechaInicial AND d.FECHA_ENTREGA <= @FechaFinal AND de.ESTADO = 'D' AND f.ANULADA = 'N' AND f.COBRADOR <> 'F01'
//    ORDER BY d.COBRADOR, d.VENDEDOR, f.FACTURA

//    OPEN #CursorFacturasTodas
//    FETCH NEXT FROM #CursorFacturasTodas INTO @Factura, @Valor, @FechaFactura, @FechaEntrega, @Vendedor, @NombreVendedor, @Tienda

//    WHILE @@FETCH_STATUS = 0
//    BEGIN

//        SET @Iva = @IvaConfigura
//        SET @MontoComisionable = 0
//        SET @ValorNeto = 0
		
//        IF ((SELECT COUNT(*) FROM prodmult.MF_Factura WHERE FACTURA = @Factura) > 0) SET @Iva = 1 + ((SELECT PCTJ_IVA FROM prodmult.MF_Factura WHERE FACTURA = @Factura) / 100)

//        SET @MontoComisionable = prodmult.fcComisionFactura(@Factura)
//        SET @ValorNeto = @MontoComisionable / @Iva
		
//        IF ((SELECT COUNT(*) FROM #temp WHERE Factura = @Factura) = 0) INSERT INTO #temp VALUES ( @Factura, @Valor, @MontoComisionable, @ValorNeto, 0, 0, @FechaFactura, @FechaEntrega, @Vendedor, @NombreVendedor, @Tienda, 'Facturas' )
		
//        --Fetch Cursor de Facturas
//        FETCH NEXT FROM #CursorFacturasTodas INTO @Factura, @Valor, @FechaFactura, @FechaEntrega, @Vendedor, @NombreVendedor, @Tienda
//    END
//    CLOSE #CursorFacturasTodas
//    DEALLOCATE #CursorFacturasTodas
//END
//ELSE
//BEGIN
//    --Cursor de Facturas
//    DECLARE #CursorFacturas CURSOR LOCAL FOR
//    SELECT d.FACTURA, f.TOTAL_FACTURA AS Valor, f.FECHA AS FechaFactura, d.FECHA_ENTREGA AS FechaEntrega, d.VENDEDOR AS Vendedor, v.NOMBRE AS NombreVendedor, d.COBRADOR AS Tienda 
//    FROM prodmult.MF_Despacho d JOIN prodmult.DESPACHO de ON d.DESPACHO = de.DESPACHO JOIN prodmult.FACTURA f on d.FACTURA = f.FACTURA JOIN prodmult.VENDEDOR v ON d.VENDEDOR = v.VENDEDOR 
//    WHERE d.FECHA_ENTREGA >= @FechaInicial AND d.FECHA_ENTREGA <= @FechaFinal AND de.ESTADO = 'D' AND f.ANULADA = 'N' AND f.COBRADOR = @CodigoTienda
//    ORDER BY d.COBRADOR, d.VENDEDOR, f.FACTURA

//    OPEN #CursorFacturas
//    FETCH NEXT FROM #CursorFacturas INTO @Factura, @Valor, @FechaFactura, @FechaEntrega, @Vendedor, @NombreVendedor, @Tienda

//    WHILE @@FETCH_STATUS = 0
//    BEGIN

//        SET @Iva = @IvaConfigura
//        SET @MontoComisionable = 0
//        SET @ValorNeto = 0
		
//        IF ((SELECT COUNT(*) FROM prodmult.MF_Factura WHERE FACTURA = @Factura) > 0) SET @Iva = 1 + ((SELECT PCTJ_IVA FROM prodmult.MF_Factura WHERE FACTURA = @Factura) / 100)

//        SET @MontoComisionable = prodmult.fcComisionFactura(@Factura)
//        SET @ValorNeto = @MontoComisionable / @Iva
		
//        IF ((SELECT COUNT(*) FROM #temp WHERE Factura = @Factura) = 0) INSERT INTO #temp VALUES ( @Factura, @Valor, @MontoComisionable, @ValorNeto, 0, 0, @FechaFactura, @FechaEntrega, @Vendedor, @NombreVendedor, @Tienda, 'Facturas' )
		
//        --Fetch Cursor de Facturas
//        FETCH NEXT FROM #CursorFacturas INTO @Factura, @Valor, @FechaFactura, @FechaEntrega, @Vendedor, @NombreVendedor, @Tienda
//    END
//    CLOSE #CursorFacturas
//    DEALLOCATE #CursorFacturas
//END

//SELECT * FROM #temp

//END


//GO


