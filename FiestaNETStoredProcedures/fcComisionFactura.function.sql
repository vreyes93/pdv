﻿

CREATE FUNCTION [prodmult].[fcComisionFactura] (@Factura AS VARCHAR(20)) RETURNS DECIMAL(12,2) AS
BEGIN
	
	IF ((SELECT COUNT(*) FROM prodmult.FACTURA WHERE FACTURA = @Factura AND TIPO_DOCUMENTO = 'F') = 0) RETURN 0
	IF ((SELECT ANULADA FROM prodmult.FACTURA WHERE FACTURA = @Factura AND TIPO_DOCUMENTO = 'F') = 'S') RETURN 0	
	
	DECLARE @MontoComisionable AS DECIMAL(12,2) = 0
	DECLARE @NivelPrecio AS VARCHAR(20) = (SELECT NIVEL_PRECIO FROM prodmult.MF_Factura WHERE FACTURA = @Factura)
	DECLARE @Valor AS DECIMAL(12, 2) = (SELECT TOTAL_FACTURA FROM prodmult.FACTURA WHERE FACTURA = @Factura AND TIPO_DOCUMENTO = 'F')
		
	IF ((SELECT COUNT(*) FROM prodmult.MF_ComisionExcepcion WHERE FACTURA = @Factura) = 0)
		SET @MontoComisionable = @Valor * (SELECT COMISION FROM prodmult.MF_NIVEL_PRECIO_COEFICIENTE WHERE NIVEL_PRECIO = @NivelPrecio)

	IF (@MontoComisionable > @Valor) SET @MontoComisionable = @Valor
	
	IF (@Valor < 300) SET @MontoComisionable = @Valor

	IF ((SELECT COUNT(*) FROM prodmult.MF_ComisionExcepcion WHERE FACTURA = @Factura) > 0)
	BEGIN
		IF ((SELECT TIPO FROM prodmult.MF_ComisionExcepcion WHERE FACTURA = @Factura) = 'N')
		BEGIN
			SET @MontoComisionable = 0
		END
		ELSE
		BEGIN
			SET @MontoComisionable = (SELECT MONTO_COMISION FROM prodmult.MF_ComisionExcepcion WHERE FACTURA = @Factura)
		END
	END

	RETURN @MontoComisionable
END


GO


