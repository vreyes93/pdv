﻿CREATE PROCEDURE [prodmult].[spAntiguedadSaldos]
	@TipoBuscar AS CHAR(1), @CodigoBuscar AS VARCHAR(20), @FechaBuscar AS DATETIME, @Empresa AS VARCHAR(20)
AS
BEGIN

--DECLARE @TipoBuscar AS CHAR(1) = 'T'
--DECLARE @CodigoBuscar AS VARCHAR(20) = '0013826'
--DECLARE @FechaBuscar AS DATETIME = '2016-12-31'
--DECLARE @Empresa AS VARCHAR(20) = 'prodmult'

DECLARE @Where AS VARCHAR(200) = ' '

IF (@TipoBuscar = 'C') SET @Where = ' WHERE a.CLIENTE = ''' + @CodigoBuscar + ''' '
IF (@TipoBuscar = 'F') SET @Where = ' WHERE SUBSTRING(a.CLIENTE, 1, 1) <> '' M '' '
IF (@TipoBuscar = 'M') SET @Where = ' WHERE SUBSTRING(a.CLIENTE, 1, 1) = '' M '' '

DECLARE @FechaString AS VARCHAR(20) = CONVERT(VARCHAR(4), YEAR(@FechaBuscar)) + '-' + CONVERT(VARCHAR(2), MONTH(@FechaBuscar)) + '-' + CONVERT(VARCHAR(2), DAY(@FechaBuscar)) + ' 23:59:59 ' 

EXEC (
'DECLARE @Fecha AS DATETIME = ''' + @FechaString + ''' ' + 
'DECLARE @Cargos AS DECIMAL(12,2) ' + 
'DECLARE @Abonos AS DECIMAL(12,2) ' +
'DECLARE @Saldo AS DECIMAL(12,2) ' +
'DECLARE @CargosCalculo AS DECIMAL(12,2) ' +
'DECLARE @AbonosCalculo AS DECIMAL(12,2) ' +
'DECLARE @SaldoDocumento AS DECIMAL(12,2) ' +
'DECLARE @Linea AS INT ' +
'DECLARE @TipoCuenta AS VARCHAR(20) ' +
'DECLARE @DocumentoCuenta AS VARCHAR(20) ' +
'DECLARE @CargoCuenta AS DECIMAL(12,2) ' +
'DECLARE @AbonoCuenta AS DECIMAL(12,2) ' +
'DECLARE @SaldoCuenta AS DECIMAL(12,2) ' +
'DECLARE @Documento AS VARCHAR(20) ' +
'DECLARE @FechaCargo AS DATETIME ' +
'DECLARE @MontoCargo AS DECIMAL(12,2) ' +
'DECLARE @FechaAbono AS DATETIME ' +
'DECLARE @FechaCuenta AS DATETIME ' +
'DECLARE @MontoAbono AS DECIMAL(12,2) ' +
'DECLARE @Dias AS INT ' +
'DECLARE @SaldoActual AS DECIMAL(12,2) ' +
'DECLARE @De1a30 AS DECIMAL(12,2) ' +
'DECLARE @De31a60 AS DECIMAL(12,2) ' +
'DECLARE @De61a90 AS DECIMAL(12,2) ' +
'DECLARE @De91a120 AS DECIMAL(12,2) ' +
'DECLARE @De121a150 AS DECIMAL(12,2) ' +
'DECLARE @De151Mas AS DECIMAL(12,2) ' +
'DECLARE @ii AS INT ' +
'DECLARE @Empresa AS VARCHAR(20) = ''' + @Empresa + ''' ' +
' ' +
'BEGIN TRY DROP TABLE #antiguedad END TRY BEGIN CATCH SET @ii = 0 END CATCH ' +
'BEGIN TRY DROP TABLE #documentos END TRY BEGIN CATCH SET @ii = 0 END CATCH ' +
'BEGIN TRY DROP TABLE #cargos END TRY BEGIN CATCH SET @ii = 0 END CATCH ' +
'BEGIN TRY DROP TABLE #abonos END TRY BEGIN CATCH SET @ii = 0 END CATCH ' +
'BEGIN TRY DROP TABLE #cuenta END TRY BEGIN CATCH SET @ii = 0 END CATCH ' +
' ' +
'BEGIN TRY CREATE TABLE #antiguedad ( CodigoCliente VARCHAR(20), Nombre VARCHAR(200), SaldoActual DECIMAL(12,2), De1a30 DECIMAL(12,2), De31a60 DECIMAL(12,2), De61a90 DECIMAL(12,2), De91a120 DECIMAL(12,2), De121a150 DECIMAL(12,2), De151Mas DECIMAL(12,2), UltimoMovimiento DATETIME ) END TRY BEGIN CATCH SET @ii = 0 END CATCH ' +
'BEGIN TRY CREATE TABLE #documentos ( CodigoCliente VARCHAR(20), Nombre VARCHAR(200), Documento VARCHAR(20), Tipo VARCHAR(20), Fecha DATETIME, Monto DECIMAL(12,2), Saldo DECIMAL(12,2), Dias INT ) END TRY BEGIN CATCH SET @ii = 0 END CATCH ' +
'BEGIN TRY CREATE TABLE #cuenta ( CodigoCliente VARCHAR(20), Nombre VARCHAR(200), Fecha DATETIME, Tipo VARCHAR(20), Documento VARCHAR(20), Cargos DECIMAL(12,2), Abonos DECIMAL(12,2), Saldo DECIMAL(12,2), Linea INT, Coincidencia1 VARCHAR(20), Coincidencia2 VARCHAR(20), Coincidencia3 VARCHAR(20), Coincidencia4 VARCHAR(20), Coincidencia5 VARCHAR(20), Aplicacion1 VARCHAR(20), Aplicacion2 VARCHAR(20), Aplicacion3 VARCHAR(20), Descripcion VARCHAR(1000), Tienda VARCHAR(20), Vendedor VARCHAR(200), Nit VARCHAR(20), Telefono1 VARCHAR(100), Telefono2 VARCHAR(100), Telefono3 VARCHAR(100), TelefonoCoincidencia1 VARCHAR(20), TelefonoCoincidencia2 VARCHAR(20), TelefonoCoincidencia3 VARCHAR(20), Direccion VARCHAR(200) ) END TRY BEGIN CATCH SET @ii = 0 END CATCH ' +
' ' +
'DELETE FROM #antiguedad ' +
'DELETE FROM #documentos ' +
'DELETE FROM #cuenta ' +
' ' +
'BEGIN TRY CREATE TABLE #cargos ( Documento VARCHAR(20), Tipo VARCHAR(20), Fecha DATETIME, Monto DECIMAL(12,2), Saldo DECIMAL(12,2), Dias INT, Descripcion VARCHAR(1000) ) END TRY BEGIN CATCH SET @ii = 0 END CATCH ' +
'BEGIN TRY CREATE TABLE #abonos ( Documento VARCHAR(20), Tipo VARCHAR(20), Fecha DATETIME, Monto DECIMAL(12,2), Saldo DECIMAL(12,2), Dias INT, Descripcion VARCHAR(1000) ) END TRY BEGIN CATCH SET @ii = 0 END CATCH ' +
' ' +
'DECLARE #Clientes CURSOR FOR ' +
'SELECT a.CLIENTE AS Cliente, a.NOMBRE AS Nombre, a.COBRADOR, b.NOMBRE, COALESCE(a.TELEFONO1, ''''), COALESCE(a.TELEFONO2, ''''), COALESCE(a.FAX, '''') FROM ' + @Empresa + '.CLIENTE a JOIN ' + @Empresa + '.VENDEDOR b ON a.VENDEDOR = b.VENDEDOR ' + @Where + ' ORDER BY a.Cliente ' +
' ' +
'DECLARE @Cliente AS VARCHAR(20) ' + 
'DECLARE @Nombre AS VARCHAR(200) ' +
'DECLARE @Cobrador AS VARCHAR(20) ' + 
'DECLARE @Vendedor AS VARCHAR(200) ' +
'DECLARE @Nit AS VARCHAR(20) ' + 
'DECLARE @Telefono1 AS VARCHAR(100) ' +
'DECLARE @Telefono2 AS VARCHAR(100) ' +
'DECLARE @Telefono3 AS VARCHAR(100) ' +
'DECLARE @Direccion AS VARCHAR(200) ' +
' ' +
'OPEN #Clientes ' + 
'FETCH NEXT FROM #Clientes INTO @Cliente, @Nombre, @Cobrador, @Vendedor, @Telefono1, @Telefono2, @Telefono3 ' +
' ' +
'WHILE @@FETCH_STATUS = 0 BEGIN ' +
'	SET @Dias = 0 ' +
'	SET @SaldoActual = 0 ' +
'	SET @De1a30 = 0 ' +
'	SET @De31a60 = 0 ' +
'	SET @De61a90 = 0 ' +
'	SET @De91a120 = 0 ' +
'	SET @De121a150 = 0 ' +
'	SET @De151Mas = 0 ' +
' ' +	
'	SET @Cargos = 0 ' +
'	SET @Abonos = 0 ' +
'	SET @Saldo = 0 ' +
'	SET @CargosCalculo = 0 ' +
'	SET @AbonosCalculo = 0 ' +
'	SET @SaldoDocumento = 0 ' +
' '	+
'	SET @Cargos = (SELECT COALESCE(SUM(MONTO_LOCAL), 0) FROM ' + @Empresa + '.DOCUMENTOS_CC WHERE FECHA <= @Fecha AND ANULADO = ''N'' AND TIPO IN ( ''FAC'', ''N/D'', ''O/D'' ) AND CLIENTE = @Cliente) ' +
'	SET @Abonos = (SELECT COALESCE(SUM(MONTO_LOCAL), 0) FROM ' + @Empresa + '.DOCUMENTOS_CC WHERE FECHA <= @Fecha AND ANULADO = ''N'' AND TIPO IN ( ''DEP'', ''N/C'', ''O/C'', ''REC'', ''RET'', ''TEF'' ) AND CLIENTE = @Cliente) ' +
'	SET @Saldo = @Cargos - @Abonos ' +
' ' +	
'	SET @CargosCalculo = @Cargos ' +
'	SET @AbonosCalculo = @Abonos ' +
' ' +
'	DELETE FROM #cargos ' +
'	DELETE FROM #abonos ' +
' ' +
'	IF (@Saldo <> 0) ' +
'	BEGIN ' +
'		INSERT INTO #cargos ' +
'		SELECT DOCUMENTO, TIPO, FECHA, MONTO_LOCAL, MONTO_LOCAL, 0, APLICACION FROM ' + @Empresa + '.DOCUMENTOS_CC WHERE FECHA <= @Fecha AND ANULADO = ''N'' AND TIPO IN ( ''FAC'', ''N/D'', ''O/D'' ) AND CLIENTE = @Cliente ORDER BY FECHA ' +
' ' +		
'		INSERT INTO #abonos ' +
'		SELECT DOCUMENTO, TIPO, FECHA, MONTO_LOCAL, MONTO_LOCAL, 0, APLICACION FROM ' + @Empresa + '.DOCUMENTOS_CC WHERE FECHA <= @Fecha AND ANULADO = ''N'' AND TIPO IN ( ''DEP'', ''N/C'', ''O/C'', ''REC'', ''RET'', ''TEF'' ) AND CLIENTE = @Cliente ORDER BY FECHA ' +
' ' +			
'		IF (@Saldo > 0) ' +
'		BEGIN ' +
'			UPDATE #abonos SET Saldo = 0 ' +
' ' +
'			DECLARE #CursorCargos CURSOR FOR ' +
'			SELECT Documento, Fecha, Monto FROM #cargos ' +
' ' +			
'			OPEN #CursorCargos ' +
'			FETCH NEXT FROM #CursorCargos INTO @Documento, @FechaCargo, @MontoCargo ' +
' ' +			
'			WHILE @@FETCH_STATUS = 0 ' +
'			BEGIN ' +
'				IF (@AbonosCalculo > 0) ' +
'				BEGIN ' +
'					IF (@AbonosCalculo > @MontoCargo) ' +
'					BEGIN ' +
'						UPDATE #cargos SET Saldo = 0 WHERE Documento = @Documento ' +
'					END ' +
'					ELSE ' +
'					BEGIN ' +
'						UPDATE #cargos SET Saldo = Saldo - @AbonosCalculo WHERE Documento = @Documento ' +
'					END ' +
' ' +
'					SET @AbonosCalculo = @AbonosCalculo - @MontoCargo ' +
'				END ' +
' ' +							
'				FETCH NEXT FROM #CursorCargos INTO @Documento, @FechaCargo, @MontoCargo ' +
'			END ' +
'			CLOSE #CursorCargos ' +
'			DEALLOCATE #CursorCargos ' +
' ' +			
'			UPDATE #cargos SET Dias = DATEDIFF(DAY, Fecha, @Fecha) WHERE Saldo > 0 ' +
' ' +
'			DECLARE #CursorCargos2 CURSOR FOR ' +
'			SELECT Documento, Fecha, Monto, Saldo, Dias FROM #cargos WHERE Saldo > 0 ' +
' ' +
'			OPEN #CursorCargos2 ' +
'			FETCH NEXT FROM #CursorCargos2 INTO @Documento, @FechaCargo, @MontoCargo, @SaldoDocumento, @Dias ' +
' ' +			
'			WHILE @@FETCH_STATUS = 0 ' +
'			BEGIN ' +
'				IF (@Dias <= 30) SET @De1a30 = @De1a30 + @SaldoDocumento ' +
'				IF (@Dias >= 31 AND @Dias <= 60) SET @De31a60 = @De31a60 + @SaldoDocumento ' +
'				IF (@Dias >= 61 AND @Dias <= 90) SET @De61a90 = @De61a90 + @SaldoDocumento ' +
'				IF (@Dias >= 91 AND @Dias <= 120) SET @De91a120 = @De91a120 + @SaldoDocumento ' +
'				IF (@Dias >= 121 AND @Dias <= 150) SET @De121a150 = @De121a150 + @SaldoDocumento ' +
'				IF (@Dias >= 151) SET @De151Mas = @De151Mas + @SaldoDocumento ' +
' ' +
'				FETCH NEXT FROM #CursorCargos2 INTO @Documento, @FechaCargo, @MontoCargo, @SaldoDocumento, @Dias ' +
'			END ' +
'			CLOSE #CursorCargos2 ' +
'			DEALLOCATE #CursorCargos2 ' +
' ' +			
'			SET @SaldoActual = @De1a30 + @De31a60 + @De61a90 + @De91a120 + @De121a150 + @De151Mas ' +
'			INSERT INTO #antiguedad VALUES ( @Cliente, @Nombre, @SaldoActual, @De1a30, @De31a60, @De61a90, @De91a120, @De121a150, @De151Mas, NULL ) ' +
' ' +
'			INSERT INTO #documentos ' +
'			SELECT @Cliente, @Nombre, Documento, Tipo, Fecha, Monto, Saldo, Dias FROM #cargos WHERE Saldo > 0 ' +
'		END ' +
' ' +
'		IF (@Saldo < 0) ' +
'		BEGIN ' +
'			UPDATE #cargos SET Saldo = 0 ' +
' ' +
'			DECLARE #CursorAbonos CURSOR FOR ' +
'			SELECT Documento, Fecha, Monto FROM #abonos ' +
' ' +
'			OPEN #CursorAbonos ' +
'			FETCH NEXT FROM #CursorAbonos INTO @Documento, @FechaAbono, @MontoAbono ' +
' ' +
'			WHILE @@FETCH_STATUS = 0 ' +
'			BEGIN ' +
'				IF (@CargosCalculo > 0) ' +
'				BEGIN ' +
'					IF (@CargosCalculo > @MontoAbono) ' +
'					BEGIN ' +
'						UPDATE #abonos SET Saldo = 0 WHERE Documento = @Documento ' +
'					END ' +
'					ELSE ' +
'					BEGIN ' +
'						UPDATE #abonos SET Saldo = Saldo - @CargosCalculo WHERE Documento = @Documento ' +
'					END ' +
' ' +
'					SET @CargosCalculo = @CargosCalculo - @MontoAbono ' +
'				END ' +
' ' +							
'				FETCH NEXT FROM #CursorAbonos INTO @Documento, @FechaAbono, @MontoAbono ' +
'			END ' +
'			CLOSE #CursorAbonos ' +
'			DEALLOCATE #CursorAbonos ' +
' ' +
'			UPDATE #abonos SET Dias = DATEDIFF(DAY, Fecha, @Fecha) WHERE Saldo > 0 ' +
' ' +
'			DECLARE #CursorAbonos2 CURSOR FOR ' +
'			SELECT Documento, Fecha, Monto, Saldo * -1, Dias FROM #abonos WHERE Saldo > 0 ' +
' ' +			
'			OPEN #CursorAbonos2 ' +
'			FETCH NEXT FROM #CursorAbonos2 INTO @Documento, @FechaAbono, @MontoAbono, @SaldoDocumento, @Dias ' +
' ' +
'			WHILE @@FETCH_STATUS = 0 ' +
'			BEGIN ' +
'				IF (@Dias <= 30) SET @De1a30 = @De1a30 + @SaldoDocumento ' +
'				IF (@Dias >= 31 AND @Dias <= 60) SET @De31a60 = @De31a60 + @SaldoDocumento ' +
'				IF (@Dias >= 61 AND @Dias <= 90) SET @De61a90 = @De61a90 + @SaldoDocumento ' +
'				IF (@Dias >= 91 AND @Dias <= 120) SET @De91a120 = @De91a120 + @SaldoDocumento ' +
'				IF (@Dias >= 121 AND @Dias <= 150) SET @De121a150 = @De121a150 + @SaldoDocumento ' +
'				IF (@Dias >= 151) SET @De151Mas = @De151Mas + @SaldoDocumento ' +
' ' +
'				FETCH NEXT FROM #CursorAbonos2 INTO @Documento, @FechaAbono, @MontoAbono, @SaldoDocumento, @Dias ' +
'			END ' +
'			CLOSE #CursorAbonos2 ' +
'			DEALLOCATE #CursorAbonos2 ' +
' ' +			
'			SET @SaldoActual = @De1a30 + @De31a60 + @De61a90 + @De91a120 + @De121a150 + @De151Mas ' +
'			INSERT INTO #antiguedad VALUES ( @Cliente, @Nombre, @SaldoActual, @De1a30, @De31a60, @De61a90, @De91a120, @De121a150, @De151Mas, NULL ) ' +
' ' +
'			INSERT INTO #documentos ' +
'			SELECT @Cliente, @Nombre, Documento, Tipo, Fecha, Monto, Saldo * -1, Dias FROM #abonos WHERE Saldo > 0 ' +
'		END ' +
' ' +
'		INSERT INTO #cuenta ' +
'		SELECT @Cliente, @Nombre, Fecha AS Fecha, Tipo, Documento, Monto, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, Descripcion, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL FROM #cargos ' +
'		UNION ' +
'		SELECT @Cliente, @Nombre, Fecha AS Fecha, Tipo, Documento, 0, Monto, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, Descripcion, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL FROM #abonos ' +
'		ORDER BY Fecha ' +
' ' +
'		DECLARE #CursorCuenta CURSOR FOR ' +
'		SELECT Tipo, Documento, Cargos, Abonos, Fecha FROM #cuenta WHERE CodigoCliente = @Cliente ORDER BY Fecha ' +
' ' +
'		SET @Linea = 0 ' +
'		SET @SaldoCuenta = 0 ' +
' ' +
'		OPEN #CursorCuenta ' +
'		FETCH NEXT FROM #CursorCuenta INTO @TipoCuenta, @DocumentoCuenta, @CargoCuenta, @AbonoCuenta, @FechaCuenta ' +
' ' +
'		WHILE @@FETCH_STATUS = 0 ' +
'		BEGIN ' +
'			SET @Linea = @Linea + 1 ' +
'			SET @SaldoCuenta = @SaldoCuenta + @CargoCuenta - @AbonoCuenta ' +
'			UPDATE #cuenta SET Saldo = @SaldoCuenta, Linea = @Linea WHERE CodigoCliente = @Cliente AND Tipo = @TipoCuenta AND Documento = @DocumentoCuenta ' +
' ' +
'			FETCH NEXT FROM #CursorCuenta INTO @TipoCuenta, @DocumentoCuenta, @CargoCuenta, @AbonoCuenta, @FechaCuenta ' +
'		END ' +
'		CLOSE #CursorCuenta ' +
'		DEALLOCATE #CursorCuenta ' +
' ' +
'		SET @Nit = ''' +
'		SET @Direccion = ''' + 
' ' +
'		IF (@Empresa = ''prodmult'') ' +
'		BEGIN ' +
'			SET @Nit = (SELECT NIT_FACTURA FROM prodmult.MF_Cliente WHERE CLIENTE = @Cliente) ' +
'			SET @Direccion = (SELECT CALLE_AVENIDA_FACTURA + '' '' + CASA_FACTURA + '' '' + ''Zona '' + ZONA_FACTURA + '' '' + APARTAMENTO_FACTURA + '' '' + COLONIA_FACTURA + '' '' + MUNICIPIO_FACTURA + '', '' + DEPARTAMENTO_FACTURA FROM prodmult.MF_Cliente WHERE CLIENTE = @Cliente) ' + 
'		END ' +
' ' +
'		INSERT INTO #cuenta VALUES ( @Cliente, @Nombre, @FechaCuenta, ''---'', ''SALDO'', NULL, NULL, @SaldoCuenta, @Linea + 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @Cobrador, @Vendedor, @Nit, @Telefono1, @Telefono2, @Telefono3, NULL, NULL, NULL, @Direccion) ' +
'		INSERT INTO #cuenta VALUES ( @Cliente, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @Linea + 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL ) ' +
'		INSERT INTO #cuenta VALUES ( @Cliente, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @Linea + 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL ) ' +
'	END ' +
' ' +
'	FETCH NEXT FROM #Clientes INTO @Cliente, @Nombre, @Cobrador, @Vendedor, @Telefono1, @Telefono2, @Telefono3 ' +
'END ' +
'CLOSE #Clientes ' +
'DEALLOCATE #Clientes ' +
' ' +
'IF ((SELECT COUNT(*) FROM #antiguedad) > 0) ' +
'BEGIN ' +
'	UPDATE a SET a.UltimoMovimiento = (SELECT b.Fecha FROM #cuenta b WHERE b.CodigoCliente = a.CodigoCliente AND b.Documento = ''Saldo'') FROM #antiguedad a ' +
' ' +
'	DECLARE @TotalSaldoActual AS DECIMAL(12,2) = (SELECT SUM(SaldoActual) FROM #antiguedad) ' +
'	DECLARE @TotalDe1a30 AS DECIMAL(12,2) = (SELECT SUM(De1a30) FROM #antiguedad) ' +
'	DECLARE @TotalDe31a60 AS DECIMAL(12,2) = (SELECT SUM(De31a60) FROM #antiguedad) ' +
'	DECLARE @TotalDe61a90 AS DECIMAL(12,2) = (SELECT SUM(De61a90) FROM #antiguedad) ' +
'	DECLARE @TotalDe91a120 AS DECIMAL(12,2) = (SELECT SUM(De91a120) FROM #antiguedad) ' +
'	DECLARE @TotalDe121a150 AS DECIMAL(12,2) = (SELECT SUM(De121a150) FROM #antiguedad) ' +
'	DECLARE @TotalDe151Mas AS DECIMAL(12,2) = (SELECT SUM(De151Mas) FROM #antiguedad) ' +
' ' +
'	INSERT INTO #antiguedad VALUES ( ''ZZZZZZ'', ''SALDO TOTAL'', @TotalSaldoActual, @TotalDe1a30, @TotalDe31a60, @TotalDe61a90, @TotalDe91a120, @TotalDe121a150, @TotalDe151Mas, NULL ) ' +
' ' +
'	DECLARE @TotalSaldoDocumentos AS DECIMAL(12,2) = (SELECT SUM(Saldo) FROM #documentos) ' +
'	INSERT INTO #documentos VALUES ( ''ZZZZZZ'', NULL, ''SALDO TOTAL'', ''---'', @Fecha, NULL, @TotalSaldoDocumentos, NULL ) ' +
' ' +
'	DECLARE @SaldoTotal AS DECIMAL(12,2) = (SELECT SUM(Saldo) FROM #cuenta WHERE Tipo = ''---'') ' +
'	INSERT INTO #cuenta VALUES ( ''ZZZZZZ'', NULL, @Fecha, ''---'', ''SALDO TOTAL'', NULL, NULL, @SaldoTotal, 1000000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL ) ' +
' ' +
'	UPDATE a SET a.Coincidencia1 = (SELECT TOP 1 b.CodigoCliente FROM #cuenta b WHERE b.Documento = ''SALDO'' AND b.Saldo = a.Saldo * -1) FROM #cuenta a WHERE a.Documento = ''SALDO'' AND a.Saldo > 0 ' +
'	UPDATE a SET a.Coincidencia1 = (SELECT TOP 1 b.CodigoCliente FROM #cuenta b WHERE b.Documento = ''SALDO'' AND b.Saldo = a.Saldo * -1) FROM #cuenta a WHERE a.Documento = ''SALDO'' AND a.Saldo < 0 ' +
' ' +
'	UPDATE a SET a.Coincidencia2 = (SELECT TOP 1 b.CodigoCliente FROM #cuenta b WHERE b.Documento = ''SALDO'' AND b.Saldo = a.Saldo * -1 AND b.CodigoCliente <> a.Coincidencia1) FROM #cuenta a WHERE a.Documento = ''SALDO'' AND a.Saldo > 0 ' +
'	UPDATE a SET a.Coincidencia2 = (SELECT TOP 1 b.CodigoCliente FROM #cuenta b WHERE b.Documento = ''SALDO'' AND b.Saldo = a.Saldo * -1 AND b.CodigoCliente <> a.Coincidencia1) FROM #cuenta a WHERE a.Documento = ''SALDO'' AND a.Saldo < 0 ' +
' ' +
'	UPDATE a SET a.Coincidencia3 = (SELECT TOP 1 b.CodigoCliente FROM #cuenta b WHERE b.Documento = ''SALDO'' AND b.Saldo = a.Saldo * -1 AND b.CodigoCliente <> a.Coincidencia1 AND b.CodigoCliente <> a.Coincidencia2) FROM #cuenta a WHERE a.Documento = ''SALDO'' AND a.Saldo > 0 ' +
'	UPDATE a SET a.Coincidencia3 = (SELECT TOP 1 b.CodigoCliente FROM #cuenta b WHERE b.Documento = ''SALDO'' AND b.Saldo = a.Saldo * -1 AND b.CodigoCliente <> a.Coincidencia1 AND b.CodigoCliente <> a.Coincidencia2) FROM #cuenta a WHERE a.Documento = ''SALDO'' AND a.Saldo < 0 ' +
' ' +
'	UPDATE a SET a.Coincidencia4 = (SELECT TOP 1 b.CodigoCliente FROM #cuenta b WHERE b.Documento = ''SALDO'' AND b.Saldo = a.Saldo * -1 AND b.CodigoCliente <> a.Coincidencia1 AND b.CodigoCliente <> a.Coincidencia2 AND b.CodigoCliente <> a.Coincidencia3) FROM #cuenta a WHERE a.Documento = ''SALDO'' AND a.Saldo > 0 ' +
'	UPDATE a SET a.Coincidencia4 = (SELECT TOP 1 b.CodigoCliente FROM #cuenta b WHERE b.Documento = ''SALDO'' AND b.Saldo = a.Saldo * -1 AND b.CodigoCliente <> a.Coincidencia1 AND b.CodigoCliente <> a.Coincidencia2 AND b.CodigoCliente <> a.Coincidencia3) FROM #cuenta a WHERE a.Documento = ''SALDO'' AND a.Saldo < 0 ' +
' ' +
'	UPDATE a SET a.Coincidencia5 = (SELECT TOP 1 b.CodigoCliente FROM #cuenta b WHERE b.Documento = ''SALDO'' AND b.Saldo = a.Saldo * -1 AND b.CodigoCliente <> a.Coincidencia1 AND b.CodigoCliente <> a.Coincidencia2 AND b.CodigoCliente <> a.Coincidencia3 AND b.CodigoCliente <> a.Coincidencia4) FROM #cuenta a WHERE a.Documento = ''SALDO'' AND a.Saldo > 0 ' +
'	UPDATE a SET a.Coincidencia5 = (SELECT TOP 1 b.CodigoCliente FROM #cuenta b WHERE b.Documento = ''SALDO'' AND b.Saldo = a.Saldo * -1 AND b.CodigoCliente <> a.Coincidencia1 AND b.CodigoCliente <> a.Coincidencia2 AND b.CodigoCliente <> a.Coincidencia3 AND b.CodigoCliente <> a.Coincidencia4) FROM #cuenta a WHERE a.Documento = ''SALDO'' AND a.Saldo < 0 ' +
' ' +
'	UPDATE a SET a.Aplicacion1 = (SELECT TOP 1 cc.CLI_DOC_CREDIT FROM prodmult.AUXILIAR_CC cc WHERE cc.CLI_DOC_DEBITO COLLATE Modern_Spanish_CI_AS = a.CodigoCliente COLLATE Modern_Spanish_CI_AS AND cc.CLI_DOC_CREDIT COLLATE Modern_Spanish_CI_AS <> a.CodigoCliente COLLATE Modern_Spanish_CI_AS) FROM #cuenta a WHERE a.Documento = ''SALDO'' AND a.Saldo > 0 ' +
'	UPDATE a SET a.Aplicacion1 = (SELECT TOP 1 cc.CLI_DOC_DEBITO FROM prodmult.AUXILIAR_CC cc WHERE cc.CLI_DOC_CREDIT COLLATE Modern_Spanish_CI_AS = a.CodigoCliente COLLATE Modern_Spanish_CI_AS AND cc.CLI_DOC_DEBITO COLLATE Modern_Spanish_CI_AS <> a.CodigoCliente COLLATE Modern_Spanish_CI_AS) FROM #cuenta a WHERE a.Documento = ''SALDO'' AND a.Saldo < 0 ' +
' ' +
'	UPDATE a SET a.Aplicacion2 = (SELECT TOP 1 cc.CLI_DOC_CREDIT FROM prodmult.AUXILIAR_CC cc WHERE cc.CLI_DOC_DEBITO COLLATE Modern_Spanish_CI_AS = a.CodigoCliente COLLATE Modern_Spanish_CI_AS AND cc.CLI_DOC_CREDIT COLLATE Modern_Spanish_CI_AS <> a.CodigoCliente COLLATE Modern_Spanish_CI_AS AND cc.CLI_DOC_CREDIT COLLATE Modern_Spanish_CI_AS <> a.Aplicacion1 COLLATE Modern_Spanish_CI_AS) FROM #cuenta a WHERE a.Documento = ''SALDO'' AND a.Saldo > 0 ' +
'	UPDATE a SET a.Aplicacion2 = (SELECT TOP 1 cc.CLI_DOC_DEBITO FROM prodmult.AUXILIAR_CC cc WHERE cc.CLI_DOC_CREDIT COLLATE Modern_Spanish_CI_AS = a.CodigoCliente COLLATE Modern_Spanish_CI_AS AND cc.CLI_DOC_DEBITO COLLATE Modern_Spanish_CI_AS <> a.CodigoCliente COLLATE Modern_Spanish_CI_AS AND cc.CLI_DOC_DEBITO COLLATE Modern_Spanish_CI_AS <> a.Aplicacion1 COLLATE Modern_Spanish_CI_AS) FROM #cuenta a WHERE a.Documento = ''SALDO'' AND a.Saldo < 0 ' +
' ' +
'	UPDATE a SET a.Aplicacion3 = (SELECT TOP 1 cc.CLI_DOC_CREDIT FROM prodmult.AUXILIAR_CC cc WHERE cc.CLI_DOC_DEBITO COLLATE Modern_Spanish_CI_AS = a.CodigoCliente COLLATE Modern_Spanish_CI_AS AND cc.CLI_DOC_CREDIT COLLATE Modern_Spanish_CI_AS <> a.CodigoCliente COLLATE Modern_Spanish_CI_AS AND cc.CLI_DOC_CREDIT COLLATE Modern_Spanish_CI_AS <> a.Aplicacion1 COLLATE Modern_Spanish_CI_AS AND cc.CLI_DOC_CREDIT COLLATE Modern_Spanish_CI_AS <> a.Aplicacion2 COLLATE Modern_Spanish_CI_AS) FROM #cuenta a WHERE a.Documento = ''SALDO'' AND a.Saldo > 0 ' +
'	UPDATE a SET a.Aplicacion3 = (SELECT TOP 1 cc.CLI_DOC_DEBITO FROM prodmult.AUXILIAR_CC cc WHERE cc.CLI_DOC_CREDIT COLLATE Modern_Spanish_CI_AS = a.CodigoCliente COLLATE Modern_Spanish_CI_AS AND cc.CLI_DOC_DEBITO COLLATE Modern_Spanish_CI_AS <> a.CodigoCliente COLLATE Modern_Spanish_CI_AS AND cc.CLI_DOC_DEBITO COLLATE Modern_Spanish_CI_AS <> a.Aplicacion1 COLLATE Modern_Spanish_CI_AS AND cc.CLI_DOC_DEBITO COLLATE Modern_Spanish_CI_AS <> a.Aplicacion2 COLLATE Modern_Spanish_CI_AS) FROM #cuenta a WHERE a.Documento = ''SALDO'' AND a.Saldo < 0 ' +
' ' +
'	UPDATE a SET a.TelefonoCoincidencia1 = (SELECT TOP 1 b.CodigoCliente FROM #cuenta b WHERE b.Documento = ''SALDO'' AND (a.Telefono1 = b.Telefono1 OR a.Telefono1 = b.Telefono2 OR a.Telefono1 = b.Telefono3) AND a.CodigoCliente <> b.CodigoCliente) FROM #cuenta a WHERE a.Documento = ''SALDO'' AND a.Saldo > 0 AND LEN(a.Telefono1) > 0 ' +
'	UPDATE a SET a.TelefonoCoincidencia2 = (SELECT TOP 1 b.CodigoCliente FROM #cuenta b WHERE b.Documento = ''SALDO'' AND (a.Telefono2 = b.Telefono1 OR a.Telefono2 = b.Telefono2 OR a.Telefono2 = b.Telefono3) AND a.CodigoCliente <> b.CodigoCliente) FROM #cuenta a WHERE a.Documento = ''SALDO'' AND a.Saldo > 0 AND LEN(a.Telefono2) > 0 ' +
'	UPDATE a SET a.TelefonoCoincidencia3 = (SELECT TOP 1 b.CodigoCliente FROM #cuenta b WHERE b.Documento = ''SALDO'' AND (a.Telefono3 = b.Telefono1 OR a.Telefono3 = b.Telefono2 OR a.Telefono3 = b.Telefono3) AND a.CodigoCliente <> b.CodigoCliente) FROM #cuenta a WHERE a.Documento = ''SALDO'' AND a.Saldo > 0 AND LEN(a.Telefono3) > 0 ' +
' ' +
'END ' +
' ' +
'SELECT (CASE WHEN CodigoCliente = ''ZZZZZZ'' THEN NULL ELSE CodigoCliente END) AS Cliente, Nombre, SaldoActual, De1a30, De31a60, De61a90, De91a120, De121a150, De151Mas, UltimoMovimiento FROM #antiguedad ORDER BY CodigoCliente ' +
'SELECT (CASE WHEN Nombre IS NULL THEN NULL ELSE CodigoCliente END) AS Cliente, Nombre, Fecha, (CASE Tipo WHEN ''---'' THEN NULL ELSE Tipo END) AS Tipo, Documento, Monto, Saldo, Dias FROM #documentos ORDER BY CodigoCliente, Documento ' +
'SELECT (CASE WHEN Nombre IS NULL THEN NULL ELSE CodigoCliente END) AS Cliente, Nombre, Fecha, (CASE Tipo WHEN ''---'' THEN NULL ELSE Tipo END) AS Tipo, Documento, Cargos, Abonos, Saldo FROM #cuenta ORDER BY CodigoCliente, Linea ' +
'SELECT (CASE WHEN Nombre IS NULL THEN NULL ELSE CodigoCliente END) AS Cliente, Nombre, Fecha, (CASE Tipo WHEN ''---'' THEN NULL ELSE Tipo END) AS Tipo, Documento, Cargos, Abonos, Saldo, Coincidencia1, Coincidencia2, Coincidencia3, Coincidencia4, Coincidencia5, Aplicacion1, Aplicacion2, Aplicacion3, Descripcion, Nit, Tienda, Vendedor, Telefono1, Telefono2, Telefono3, TelefonoCoincidencia1, TelefonoCoincidencia2, TelefonoCoincidencia3, Direccion FROM #cuenta WHERE Documento IS NOT NULL ORDER BY CodigoCliente, Linea ' +
'SELECT CodigoCliente AS ClienteDebito, CodigoCliente AS ClienteCredito, Fecha, Monto FROM #documentos WHERE 1 = 2 ' 
)

END



GO
