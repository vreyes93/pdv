﻿

CREATE PROCEDURE [prodmult].[spComisiones] @FechaInicial AS DATETIME, @FechaFinal AS DATETIME, @CodigoVendedor AS VARCHAR(4) AS
BEGIN

--DECLARE @CodigoVendedor AS VARCHAR(4) = ''
--DECLARE @FechaInicial AS DATETIME = '2016-11-26'
--DECLARE @FechaFinal AS DATETIME = '2016-12-25'

BEGIN TRY
	DROP TABLE #temp
END TRY
BEGIN CATCH
	--Nothing
END CATCH

BEGIN TRY	
	CREATE TABLE #temp ( Factura VARCHAR(20), Valor DECIMAL(12,2), MontoComisionable DECIMAL(12,2), ValorNeto DECIMAL(12,2), Comision DECIMAL(12,2), Porcentaje DECIMAL(12,2), 
		FechaFactura DATETIME, FechaEntrega DATETIME, Vendedor VARCHAR(4), NombreVendedor VARCHAR(100), Tienda VARCHAR(4), Titulo VARCHAR(20), NivelPrecio VARCHAR(20), PorcentajeTarjeta DECIMAL(12,2) ) 
END TRY
BEGIN CATCH 	
	--Nothing 
END CATCH

DELETE FROM #temp

DECLARE @Factura AS VARCHAR(20)
DECLARE @Valor AS DECIMAL(12, 2)
DECLARE @MontoComisionable AS DECIMAL(12, 2)
DECLARE @ValorNeto AS DECIMAL(12, 2)
DECLARE @FechaFactura AS DATETIME
DECLARE @FechaEntrega AS DATETIME
DECLARE @Vendedor AS VARCHAR(4)
DECLARE @NombreVendedor AS VARCHAR(100)
DECLARE @Tienda AS VARCHAR(4)
DECLARE @NivelPrecio AS VARCHAR(20)
DECLARE @PorcentajeTarjeta AS DECIMAL(12, 2)
DECLARE @Financiera AS INT

DECLARE @Iva AS DECIMAL(12,2)
DECLARE @IvaConfigura AS DECIMAL(12, 2) =  1 + ((SELECT IMPUESTO1 FROM prodmult.IMPUESTO WHERE IMPUESTO = 'IVA') / 100)

IF (@CodigoVendedor = '')
BEGIN
	--Cursor de Facturas Todas
	DECLARE #CursorFacturasTodas CURSOR LOCAL FOR
	SELECT d.FACTURA, f.TOTAL_FACTURA AS Valor, f.FECHA AS FechaFactura, d.FECHA_ENTREGA AS FechaEntrega, f.VENDEDOR AS Vendedor, v.NOMBRE AS NombreVendedor, f.COBRADOR AS Tienda 
	FROM prodmult.MF_Despacho d JOIN prodmult.DESPACHO de ON d.DESPACHO = de.DESPACHO JOIN prodmult.FACTURA f on d.FACTURA = f.FACTURA JOIN prodmult.VENDEDOR v ON f.VENDEDOR = v.VENDEDOR 
	WHERE d.FECHA_ENTREGA >= @FechaInicial AND d.FECHA_ENTREGA <= @FechaFinal AND de.ESTADO = 'D' AND f.ANULADA = 'N' AND f.COBRADOR <> 'F01' AND d.ACEPTADO_CLIENTE = 'S'
	ORDER BY d.COBRADOR, f.VENDEDOR, f.FACTURA

	OPEN #CursorFacturasTodas
	FETCH NEXT FROM #CursorFacturasTodas INTO @Factura, @Valor, @FechaFactura, @FechaEntrega, @Vendedor, @NombreVendedor, @Tienda

	WHILE @@FETCH_STATUS = 0
	BEGIN

		SET @Iva = @IvaConfigura
		SET @MontoComisionable = 0
		SET @ValorNeto = 0
		SET @NivelPrecio = ''
		SET @PorcentajeTarjeta = NULL
		SET @Financiera = 1
		
		IF ((SELECT COUNT(*) FROM prodmult.MF_Factura WHERE FACTURA = @Factura) > 0) 
		BEGIN
			SET @Iva = 1 + ((SELECT PCTJ_IVA FROM prodmult.MF_Factura WHERE FACTURA = @Factura) / 100)
			SET @NivelPrecio = (SELECT NIVEL_PRECIO FROM prodmult.MF_Factura WHERE FACTURA = @Factura)
			SET @Financiera = (SELECT FINANCIERA FROM prodmult.MF_Factura WHERE FACTURA = @Factura)
		END

		IF ((SELECT COMISION FROM prodmult.MF_NIVEL_PRECIO_COEFICIENTE WHERE NIVEL_PRECIO = @NivelPrecio) <> 1) SET @PorcentajeTarjeta = (SELECT COMISION FROM prodmult.MF_NIVEL_PRECIO_COEFICIENTE WHERE NIVEL_PRECIO = @NivelPrecio)

		SET @MontoComisionable = prodmult.fcComisionFactura(@Factura)
		SET @ValorNeto = @MontoComisionable / @Iva
		
		IF ((SELECT COUNT(*) FROM #temp WHERE Factura = @Factura) = 0) INSERT INTO #temp VALUES ( @Factura, @Valor, @MontoComisionable, @ValorNeto, 0, 0, @FechaFactura, @FechaEntrega, @Vendedor, @NombreVendedor, @Tienda, 'Facturas', @NivelPrecio, @PorcentajeTarjeta )
		
		--Fetch Cursor de Facturas
		FETCH NEXT FROM #CursorFacturasTodas INTO @Factura, @Valor, @FechaFactura, @FechaEntrega, @Vendedor, @NombreVendedor, @Tienda
	END
	CLOSE #CursorFacturasTodas
	DEALLOCATE #CursorFacturasTodas
END
ELSE
BEGIN
	--Cursor de Facturas
	DECLARE #CursorFacturas CURSOR LOCAL FOR
	SELECT d.FACTURA, f.TOTAL_FACTURA AS Valor, f.FECHA AS FechaFactura, d.FECHA_ENTREGA AS FechaEntrega, f.VENDEDOR AS Vendedor, v.NOMBRE AS NombreVendedor, f.COBRADOR AS Tienda 
	FROM prodmult.MF_Despacho d JOIN prodmult.DESPACHO de ON d.DESPACHO = de.DESPACHO JOIN prodmult.FACTURA f on d.FACTURA = f.FACTURA JOIN prodmult.VENDEDOR v ON f.VENDEDOR = v.VENDEDOR 
	WHERE d.FECHA_ENTREGA >= @FechaInicial AND d.FECHA_ENTREGA <= @FechaFinal AND de.ESTADO = 'D' AND f.ANULADA = 'N' AND f.VENDEDOR = @CodigoVendedor AND d.ACEPTADO_CLIENTE = 'S'
	ORDER BY d.COBRADOR, f.VENDEDOR, f.FACTURA

	OPEN #CursorFacturas
	FETCH NEXT FROM #CursorFacturas INTO @Factura, @Valor, @FechaFactura, @FechaEntrega, @Vendedor, @NombreVendedor, @Tienda

	WHILE @@FETCH_STATUS = 0
	BEGIN

		SET @Iva = @IvaConfigura
		SET @MontoComisionable = 0
		SET @ValorNeto = 0
		SET @NivelPrecio = ''
		SET @PorcentajeTarjeta = NULL
		SET @Financiera = 1
		
		IF ((SELECT COUNT(*) FROM prodmult.MF_Factura WHERE FACTURA = @Factura) > 0) 
		BEGIN
			SET @Iva = 1 + ((SELECT PCTJ_IVA FROM prodmult.MF_Factura WHERE FACTURA = @Factura) / 100)
			SET @NivelPrecio = (SELECT NIVEL_PRECIO FROM prodmult.MF_Factura WHERE FACTURA = @Factura)
			SET @Financiera = (SELECT FINANCIERA FROM prodmult.MF_Factura WHERE FACTURA = @Factura)
		END

		IF ((SELECT COMISION FROM prodmult.MF_NIVEL_PRECIO_COEFICIENTE WHERE NIVEL_PRECIO = @NivelPrecio) <> 1) SET @PorcentajeTarjeta = (SELECT COMISION FROM prodmult.MF_NIVEL_PRECIO_COEFICIENTE WHERE NIVEL_PRECIO = @NivelPrecio)

		SET @MontoComisionable = prodmult.fcComisionFactura(@Factura)
		SET @ValorNeto = @MontoComisionable / @Iva
		
		IF ((SELECT COUNT(*) FROM #temp WHERE Factura = @Factura) = 0) INSERT INTO #temp VALUES ( @Factura, @Valor, @MontoComisionable, @ValorNeto, 0, 0, @FechaFactura, @FechaEntrega, @Vendedor, @NombreVendedor, @Tienda, 'Facturas', @NivelPrecio, @PorcentajeTarjeta )
		
		--Fetch Cursor de Facturas
		FETCH NEXT FROM #CursorFacturas INTO @Factura, @Valor, @FechaFactura, @FechaEntrega, @Vendedor, @NombreVendedor, @Tienda
	END
	CLOSE #CursorFacturas
	DEALLOCATE #CursorFacturas
END

SELECT * FROM #temp

END





GO


