﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FiestaNet.Startup))]
namespace FiestaNet
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
