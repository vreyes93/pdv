<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="es-ES"> 

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<title>Muebles Fiesta | Ubicaciones</title>
<meta http-equiv="content" content="Guatemala: Muebles Fiesta, Camas Guatemala, Camas, Camas Indufoam, Closets, Cocinas, Comedores, Dormitorios, Escritorios, Infantil, Salas, Muebles, Muebles en Guatemala, M�dulos, Centros de Entretenimiento, Ubicaciones">
<meta http-equiv="description" content="Guatemala: Muebles Fiesta, Camas Guatemala, Camas, Camas Indufoam, Closets, Cocinas, Comedores, Dormitorios, Escritorios, Infantil, Salas, Muebles, Muebles en Guatemala, M�dulos, Centros de Entretenimiento, Ubicaciones">
<meta http-equiv="keywords" content="Guatemala: Muebles Fiesta, Camas Guatemala, Camas, Camas Indufoam, Closets, Cocinas, Comedores, Dormitorios, Escritorios, Infantil, Salas, Muebles, Muebles en Guatemala, M�dulos, Centros de Entretenimiento, Ubicaciones">

<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
<link rel="icon" href="favicon.png" type="image/vnd.microsoft.icon">


<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<link rel="stylesheet" type="text/css" href="js/gmap/gmap.css" media="all"/>
<script type="text/javascript" src="js/gmap/jquery-1.7.min.js"></script>
<script type="text/javascript" src="js/gmap/jquery-ui-1.8.9.custom.min.js"></script>
<script type="text/javascript" src="js/gmap/googlemaps.js"></script>
<script type="text/javascript" src="js/gmap/locations.js"></script>

<style type="text/css">@import url(style.css);</style>
<style type="text/css">
.fluid_container {
	margin:0;
	height:465px;
	width: 950px;
	overflow:hidden;
	padding:0px;
}
</style>

</head>


<body class="bodygen">

<div class="fullpage">

<div class="social">Vis&iacute;tanos en: <a href="http://www.facebook.com/home.php#!/pages/Muebles-Fiesta/175200984104" target="_blank"><img src="images/facebook.png" height="25" align="absmiddle"></a>

</div>



	<div class="header">
	
		<div class="header1">
			<a href="main.asp"><img src="imgs/logo-MF.png" alt="Muebles Fiesta" title="Muebles Fiesta"></a>
		</div>
		
		<div class="header2">
		
	      	<form method="post" action="login.asp">
	      	<ul>
	      		<li>&nbsp;usuario: <input type="text" name="cuenta" size="11" maxlength=40></li>
	      		<li>&nbsp;clave: <input type="password" size="11" name="pin"></li>
	      		<li><input type="submit" value="login" id="submitlogin"></li>
	      		<p><a href="nuevacuenta.asp">�A&uacute;n no tienes cuenta ?</a>&nbsp;&nbsp;<a href="registro.asp"><span>Reg&iacute;strate</span></a></p>
	      	</ul>
	      	</form>
	      
		</div>
		
		<div class="redes">
			<a href="http://www.facebook.com/home.php#!/pages/Muebles-Fiesta/175200984104" target="_blank"><img src="imgs/facebook.png"></a>
            
		</div>
        
        <div class="newsletter"><a href="newsletter.asp">Subscr�bete al bolet�n de ofertas</a></div>
	
		<ul class="menutop">
			<li ><a href="main.asp">Inicio</a></li>
			<li ><a href="suplementos.asp">Suplementos</a></li>
            <li><a href="docs/MueblesFiesta-CatalogoJun2015web.pdf">Cat&aacute;logo 2015</a></li>
			<li ><a href="ubicaciones.asp">Ubicaciones</a></li>
			<li ><a href="quienes_somos.asp">Qui&eacute;nes Somos</a></li>
			<li ><a href="empleos.asp">Empleos</a></li>
			<li ><a href="contactenos.asp">Contacto</a></li>
		</ul>
		
		<ul class="searchheader">
			<li><form action="busquedas.asp" method="POST"><input type="text" name="buscar" id="inputsearchh"><input type="submit" value="buscar producto" id="submitsearchh"></form></li>
			<li><a href="ubicaciones.asp"><img src="imgs/botonencuentra.png"></a></li>
		</ul>
		
	</div>



<div class="content">

<div class="content2">
<h1>Ubicaciones</h1>


<div id="mapWrapper">
	<div id="googlemap"></div>
 
	<div id="sucursales-list">
		<div class="block-title">Nuestras Tiendas</div>
			<div class="block-content">
				<ul>
					
					<li id="sucursal_95">
					<h2><span>C.C. Montserrat</span></h2>
					<p>Calzada San Juan 14-06 zona 4</p>
					</li>
					
					<li id="sucursal_96">
					<h2><span>C.C. Peri Roosevelt</span></h2>
					<p>Calz. Roosevelt 25-50 Z.7 Local 119 y 120</p>
					</li>
					
					<li id="sucursal_97">
					<h2><span>C.C. Metronorte</span></h2>
					<p>Km. 5 Car. Al Atl\u00e1ntico Z.17 Local 86B</p>
					</li>
					
					<li id="sucursal_98">
					<h2><span>C.C. Pradera Xela</span></h2>
					<p>Ave. Las Americas 7-12 Z.3 Local 94B</p>
					</li>
					
					<li id="sucursal_99">
					<h2><span>Plaza del Ahorro Boca del Monte</span></h2>
					<p>1era ave 5-48 zona 1 Boca del Monte</p>
					</li>
					
					<li id="sucursal_100">
					<h2><span>C.C. Santa Clara</span></h2>
					<p>Km. 17.5 Car. Al Pacifico, Villa Nueva Local 111</p>
					</li>
					
					<li id="sucursal_101">
					<h2><span>C.C. Pacific Villa Hermosa</span></h2>
					<p>23 Calle 20-00 Zona 7 Villa Hermosa I San Miguel Petapa Local 1027</p>
					</li>
					
					<li id="sucursal_102">
					<h2><span>C.C. Pradera Chiquimula</span></h2>
					<p>Km. 167.5 Ruta a Esquipulas Local 252.</p>
					</li>
					
					<li id="sucursal_103">
					<h2><span>C.C. La Trinidad Malacat�n</span></h2>
					<p>5ta calle 9-55 zona 2 Canton Morazan, Malacatan</p>
					</li>
					
					<li id="sucursal_104">
					<h2><span>C.C. La Trinidad Coatepeque</span></h2>
					<p>6ta calle, 12-124 Z.1 Lotificaci�n La Felicidad, Calzada Alvaro Arz�, Coatepeque</p>
					</li>
					
					<li id="sucursal_105">
					<h2><span>C.C. Pradera Chimaltenango</span></h2>
					<p>7 Calle 2-02 Z.2 Local 42</p>
					</li>
					
					<li id="sucursal_106">
					<h2><span>C.C. Plaza Am�ricas</span></h2>
					<p>C.C. Plaza Am�ricas, Mazatenango. Local 11 y 23</p>
					</li>
					
					<li id="sucursal_107">
					<h2><span>C.C. Pradera Escuintla</span></h2>
					<p>1 Ave. 1-40 Z. 3 Escuintla Local 69.</p>
					</li>
					
					<li id="sucursal_108">
					<h2><span>C.C. Pradera Puerto Barrios</span></h2>
					<p>Km. 292.5 Ruta al Atl�ntico, Puerto Barrios</p>
					</li>
					
					<li id="sucursal_109">
					<h2><span>Plaza Magdalena, Cob�n</span></h2>
					<p>1 calle 15-20 Zona 2, Plaza Magdalena en Coban</p>
					</li>
					
					<li id="sucursal_110">
					<h2><span>C.C. Flores del Lago, Amatitl�n</span></h2>
					<p>Carretera al Pacifico Km. 29.5. Plaza Las Lilas, local 19.</p>
					</li>
					
					<li id="sucursal_111">
					<h2><span>C.C. Pradera Huehuetenango</span></h2>
					<p>Km. 259 cambote zona 11, Calzada Kaibil Balam, Huehuetenango.</p>
					</li>
					
					<li id="sucursal_112">
					<h2><span>Plaza San Nicol�s</span></h2>
					<p>41 Av. 3-10, Zona 4 de Mixco Bosques de San Nicol�s, Guatemala.</p>
					</li>
					
					<li id="sucursal_113">
					<h2><span>C.C. Metroplaza Mundo Maya Pet�n</span></h2>
					<p>Km. 1 (Frente al Aeropuerto Internacional), sobre la carretera que conduce de Flores a Guatemala.</p>
					</li>
					
					<li id="sucursal_114">
					<h2><span>C.C. Metroplaza Jutiapa</span></h2>
					<p>km. 116 Carretera Interamericana, Jutiapa.</p>
					</li>
					
					<li id="sucursal_115">
					<h2><span>C.C. El Frutal</span></h2>
					<p>Bulevar El Frutal 14-00 zona 5 Villa Nueva C.C. El Frutal</p>
					</li>
					
					<li id="sucursal_116">
					<h2><span>Tiquisate</span></h2>
					<p>Calzada Principal 1-0 zona 4 Tiquisate Pueblo Nuevo de Tiquisate, este colinda con la Municipalidad y el Parque.</p>
					</li>
					
					
				</ul>
				<div class="gradient-top"></div>
				<div class="gradient-bot"></div>
			</div>
		</div>
		<script type="text/javascript">
			
        	jQuery("#sucursal_" + 95).data("extra", {"store_id":"95","store_name":"C.C. Montserrat","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3043-6592, 3043-6622","store_fax":"","description":"","status":"1","address":"Calzada San Juan 14-06 zona 4","address_2":"","state":"Guatemala","suburb":"","city":"Calzada San Juan 14-06 zona 4","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"14.642189","store_longitude":"-90.568264","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"http://www.mueblesfiesta.com/uploadimg/tiendas/95.jpg"});
        	
        	jQuery("#sucursal_" + 96).data("extra", {"store_id":"96","store_name":"C.C. Peri Roosevelt","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3043-6913, 3043-6856","store_fax":"","description":"","status":"1","address":"Calz. Roosevelt 25-50 Z.7 Local 119 y 120","address_2":"","state":"Guatemala","suburb":"","city":"Calz. Roosevelt 25-50 Z.7 Local 119 y 120","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"14.626851","store_longitude":"-90.555658","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"\/images\/tiendas\/default.png"});
        	
        	jQuery("#sucursal_" + 97).data("extra", {"store_id":"97","store_name":"C.C. Metronorte","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3043-6721, 3043-6696","store_fax":"","description":"","status":"1","address":"Km. 5 Car. Al Atl\u00e1ntico Z.17 Local 86B","address_2":"","state":"Guatemala","suburb":"","city":"Km. 5 Car. Al Atl\u00e1ntico Z.17 Local 86B","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"14.647275","store_longitude":"-90.477746","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"http://www.mueblesfiesta.com/uploadimg/tiendas/97.jpg"});
        	
        	jQuery("#sucursal_" + 98).data("extra", {"store_id":"98","store_name":"C.C. Pradera Xela","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3043-6733, 3043-6765","store_fax":"","description":"","status":"1","address":"Ave. Las Americas 7-12 Z.3 Local 94B","address_2":"","state":"Guatemala","suburb":"","city":"Ave. Las Americas 7-12 Z.3 Local 94B","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"14.850336","store_longitude":"-91.534466","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"http://www.mueblesfiesta.com/uploadimg/tiendas/98.jpg"});
        	
        	jQuery("#sucursal_" + 99).data("extra", {"store_id":"99","store_name":"Plaza del Ahorro Boca del Monte","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3043-6803, 3043-6824","store_fax":"","description":"","status":"1","address":"1era ave 5-48 zona 1 Boca del Monte","address_2":"","state":"Guatemala","suburb":"","city":"1era ave 5-48 zona 1 Boca del Monte","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"14.546416","store_longitude":"-90.527341","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"\/images\/tiendas\/default.png"});
        	
        	jQuery("#sucursal_" + 100).data("extra", {"store_id":"100","store_name":"C.C. Santa Clara","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3043-6791, 3043-6794","store_fax":"","description":"","status":"1","address":"Km. 17.5 Car. Al Pacifico, Villa Nueva Local 111","address_2":"","state":"Guatemala","suburb":"","city":"Km. 17.5 Car. Al Pacifico, Villa Nueva Local 111","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"14.530291","store_longitude":"-90.597645","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"http://www.mueblesfiesta.com/uploadimg/tiendas/100.jpg"});
        	
        	jQuery("#sucursal_" + 101).data("extra", {"store_id":"101","store_name":"C.C. Pacific Villa Hermosa","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3043-7009, 3043-6952","store_fax":"","description":"","status":"1","address":"23 Calle 20-00 Zona 7 Villa Hermosa I San Miguel Petapa Local 1027","address_2":"","state":"Guatemala","suburb":"","city":"23 Calle 20-00 Zona 7 Villa Hermosa I San Miguel Petapa Local 1027","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"14.525064","store_longitude":"-90.553107","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"http://www.mueblesfiesta.com/uploadimg/tiendas/101.jpg"});
        	
        	jQuery("#sucursal_" + 102).data("extra", {"store_id":"102","store_name":"C.C. Pradera Chiquimula","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3043-7302, 3043-7249","store_fax":"","description":"","status":"1","address":"Km. 167.5 Ruta a Esquipulas Local 252.","address_2":"","state":"Guatemala","suburb":"","city":"Km. 167.5 Ruta a Esquipulas Local 252.","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"14.807615","store_longitude":"-89.532867","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"http://www.mueblesfiesta.com/uploadimg/tiendas/102.jpg"});
        	
        	jQuery("#sucursal_" + 103).data("extra", {"store_id":"103","store_name":"C.C. La Trinidad Malacat�n","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3043-7054, 3043-7093","store_fax":"","description":"","status":"1","address":"5ta calle 9-55 zona 2 Canton Morazan, Malacatan","address_2":"","state":"Guatemala","suburb":"","city":"5ta calle 9-55 zona 2 Canton Morazan, Malacatan","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"14.907692","store_longitude":"-91.063498","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"http://www.mueblesfiesta.com/uploadimg/tiendas/103.jpg"});
        	
        	jQuery("#sucursal_" + 104).data("extra", {"store_id":"104","store_name":"C.C. La Trinidad Coatepeque","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3043-7383, 3043-7314","store_fax":"","description":"","status":"1","address":"6ta calle, 12-124 Z.1 Lotificaci�n La Felicidad, Calzada Alvaro Arz�, Coatepeque","address_2":"","state":"Guatemala","suburb":"","city":"6ta calle, 12-124 Z.1 Lotificaci�n La Felicidad, Calzada Alvaro Arz�, Coatepeque","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"14.701195","store_longitude":"-91.851495","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"http://www.mueblesfiesta.com/uploadimg/tiendas/104.jpg"});
        	
        	jQuery("#sucursal_" + 105).data("extra", {"store_id":"105","store_name":"C.C. Pradera Chimaltenango","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3043-7040, 3043-7030","store_fax":"","description":"","status":"1","address":"7 Calle 2-02 Z.2 Local 42","address_2":"","state":"Guatemala","suburb":"","city":"7 Calle 2-02 Z.2 Local 42","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"14.660623","store_longitude":"-90.810838","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"http://www.mueblesfiesta.com/uploadimg/tiendas/105.jpg"});
        	
        	jQuery("#sucursal_" + 106).data("extra", {"store_id":"106","store_name":"C.C. Plaza Am�ricas","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3043-7465, 3043-7435","store_fax":"","description":"","status":"1","address":"C.C. Plaza Am�ricas, Mazatenango. Local 11 y 23","address_2":"","state":"Guatemala","suburb":"","city":"C.C. Plaza Am�ricas, Mazatenango. Local 11 y 23","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"14.534728","store_longitude":"-91.492762","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"http://www.mueblesfiesta.com/uploadimg/tiendas/106.jpg"});
        	
        	jQuery("#sucursal_" + 107).data("extra", {"store_id":"107","store_name":"C.C. Pradera Escuintla","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3043-7196, 3043-7117","store_fax":"","description":"","status":"1","address":"1 Ave. 1-40 Z. 3 Escuintla Local 69.","address_2":"","state":"Guatemala","suburb":"","city":"1 Ave. 1-40 Z. 3 Escuintla Local 69.","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"14.289537","store_longitude":"-90.78508","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"http://www.mueblesfiesta.com/uploadimg/tiendas/107.jpg"});
        	
        	jQuery("#sucursal_" + 108).data("extra", {"store_id":"108","store_name":"C.C. Pradera Puerto Barrios","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3043-7412, 3043-7417","store_fax":"","description":"","status":"1","address":"Km. 292.5 Ruta al Atl�ntico, Puerto Barrios","address_2":"","state":"Guatemala","suburb":"","city":"Km. 292.5 Ruta al Atl�ntico, Puerto Barrios","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"15.697065","store_longitude":"-88.587011","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"http://www.mueblesfiesta.com/uploadimg/tiendas/108.jpg"});
        	
        	jQuery("#sucursal_" + 109).data("extra", {"store_id":"109","store_name":"Plaza Magdalena, Cob�n","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3043-7225, 3043-7231","store_fax":"","description":"","status":"1","address":"1 calle 15-20 Zona 2, Plaza Magdalena en Coban","address_2":"","state":"Guatemala","suburb":"","city":"1 calle 15-20 Zona 2, Plaza Magdalena en Coban","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"15.697065","store_longitude":"-88.587011","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"http://www.mueblesfiesta.com/uploadimg/tiendas/109.jpg"});
        	
        	jQuery("#sucursal_" + 110).data("extra", {"store_id":"110","store_name":"C.C. Flores del Lago, Amatitl�n","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3043-4573, 3043-4496","store_fax":"","description":"","status":"1","address":"Carretera al Pacifico Km. 29.5. Plaza Las Lilas, local 19.","address_2":"","state":"Guatemala","suburb":"","city":"Carretera al Pacifico Km. 29.5. Plaza Las Lilas, local 19.","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"14.471148","store_longitude":"-90.638151","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"http://www.mueblesfiesta.com/uploadimg/tiendas/110.jpg"});
        	
        	jQuery("#sucursal_" + 111).data("extra", {"store_id":"111","store_name":"C.C. Pradera Huehuetenango","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 30437471, 66261322","store_fax":"","description":"","status":"1","address":"Km. 259 cambote zona 11, Calzada Kaibil Balam, Huehuetenango.","address_2":"","state":"Guatemala","suburb":"","city":"Km. 259 cambote zona 11, Calzada Kaibil Balam, Huehuetenango.","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"15.311126","store_longitude":"-91.491756","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"\/images\/tiendas\/default.png"});
        	
        	jQuery("#sucursal_" + 112).data("extra", {"store_id":"112","store_name":"Plaza San Nicol�s","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 30434609, 66261323","store_fax":"","description":"","status":"1","address":"41 Av. 3-10, Zona 4 de Mixco Bosques de San Nicol�s, Guatemala.","address_2":"","state":"Guatemala","suburb":"","city":"41 Av. 3-10, Zona 4 de Mixco Bosques de San Nicol�s, Guatemala.","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"14.654141","store_longitude":"-90.568388","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"\/images\/tiendas\/default.png"});
        	
        	jQuery("#sucursal_" + 113).data("extra", {"store_id":"113","store_name":"C.C. Metroplaza Mundo Maya Pet�n","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3056-0862","store_fax":"","description":"","status":"1","address":"Km. 1 (Frente al Aeropuerto Internacional), sobre la carretera que conduce de Flores a Guatemala.","address_2":"","state":"Guatemala","suburb":"","city":"Km. 1 (Frente al Aeropuerto Internacional), sobre la carretera que conduce de Flores a Guatemala.","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"16.912620","store_longitude":"-89.875623","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"\/images\/tiendas\/default.png"});
        	
        	jQuery("#sucursal_" + 114).data("extra", {"store_id":"114","store_name":"C.C. Metroplaza Jutiapa","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3164-1117, 3164-1185","store_fax":"","description":"","status":"1","address":"km. 116 Carretera Interamericana, Jutiapa.","address_2":"","state":"Guatemala","suburb":"","city":"km. 116 Carretera Interamericana, Jutiapa.","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"14.292549","store_longitude":"-89.910986","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"\/images\/tiendas\/default.png"});
        	
        	jQuery("#sucursal_" + 115).data("extra", {"store_id":"115","store_name":"C.C. El Frutal","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3164-1117","store_fax":"","description":"","status":"1","address":"Bulevar El Frutal 14-00 zona 5 Villa Nueva C.C. El Frutal","address_2":"","state":"Guatemala","suburb":"","city":"Bulevar El Frutal 14-00 zona 5 Villa Nueva C.C. El Frutal","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"14.521453","store_longitude":"-90.564399","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"http://www.mueblesfiesta.com/uploadimg/tiendas/115..jpg"});
        	
        	jQuery("#sucursal_" + 116).data("extra", {"store_id":"116","store_name":"Tiquisate","store_manager":"Muebles Fiesta","store_email":"@mueblesfiesta.com","store_phone":"(502) 3164-1185","store_fax":"","description":"","status":"1","address":"Calzada Principal 1-0 zona 4 Tiquisate Pueblo Nuevo de Tiquisate, este colinda con la Municipalidad y el Parque.","address_2":"","state":"Guatemala","suburb":"","city":"Calzada Principal 1-0 zona 4 Tiquisate Pueblo Nuevo de Tiquisate, este colinda con la Municipalidad y el Parque.","region_id":"0","city_id":"0","suburb_id":"0","zipcode":"00502","state_id":"5532","country":"GT","store_latitude":"14.2851374","store_longitude":"-91.3638040","monday_open":"","monday_close":"","tuesday_open":"","tuesday_close":"","wednesday_open":"","wednesday_close":"","thursday_open":"","thursday_close":"","friday_open":"","friday_close":"","saturday_open":"","saturday_close":"","sunday_open":"","sunday_close":"","minimum_gap":"60","store_image":"\/images\/tiendas\/default.png"});
        		    
		</script>
	</div>
</div>

</div>

<div class="banners">
<ul>

</ul>
</div>

</div>

</div>

<div class="footer">
	
	<ul>
		<li>
			<a href="ubicaciones.asp">Ubicaciones</a>
			<a href="suplementos.asp">Suplementos</a>
			<a href="empleos.asp">Empleos</a>
			<a href="quienes_somos.asp">Qui&eacute;nes Somos</a>
		</li>
		<li>
			<a href="ofertas.asp">Ofertas</a>
			<a href="certificados.asp">Certificados de Regalo</a>
			<a href="boletin.asp">Bolet�n de Ofertas</a>
			<a href="contactenos.asp">Contacto</a>
		</li>
		<li>
			<a name="prod"><b>Categor&iacute;as</b></a>
			<div class="footersub">
				
				<a href="categorias.asp?ct=24">Bares</a>
				
				<a href="categorias.asp?ct=3">Centros de Entretenimiento</a>
				
				<a href="categorias.asp?ct=2">Closets</a>
				
				<a href="categorias.asp?ct=15">Cocinas</a>
				
				<a href="categorias.asp?ct=8">Comedores</a>
				
				<a href="categorias.asp?ct=5">Dormitorios</a>
				
				<a href="categorias.asp?ct=11">Escritorios</a>
				
				<a href="categorias.asp?ct=4">Infantil</a>
				
				<a href="categorias.asp?ct=7">Salas</a>
				
			</div>
		</li>
		<li id="liwidth">
			<a name="prod"><b>Camas</b></a>
			<div class="footersub">
				
				<a href=productos_calidad_n.asp?ct=14&id=2>INDUFOAM: Deluxe</a>
				
				<a href=productos_calidad_n.asp?ct=14&id=5>INDUFOAM: Dream Sleeper</a>
				
				<a href=productos_calidad_n.asp?ct=14&id=14>SIMMONS: Beauty Sleep Plush</a>
				
				<a href=productos_calidad_n.asp?ct=14&id=10>SIMMONS: Back Care</a>
				
				<a href=productos_calidad_n.asp?ct=14&id=15>SIMMONS: Recharge</a>
				
				<a href=productos_calidad_n.asp?ct=14&id=6>INDUFOAM: Triple Crown</a>
				
				<a href=productos_calidad_n.asp?ct=14&id=3>SIMMONS: Pure Essence</a>
				
				<a href=productos_calidad_n.asp?ct=14&id=7>INDUFOAM: Luxurious</a>
				
			</div>
		</li>
		
		<div class="redesfooter">
		<a href="http://www.facebook.com/home.php#!/pages/Muebles-Fiesta/175200984104" target="_blank"><img src="imgs/facebook.png"></a>
         
		</div>
	
	</ul>
	
	</div>

<div class="grupo"><a href="http://grupo.centramerica.com" target="_blank">grupo.centramerica.com</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>

<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-3401323-1";
urchinTracker();
</script>
    
</body>


</html>