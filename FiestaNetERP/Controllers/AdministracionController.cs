﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using MF.Comun.Dto;
using Microsoft.AspNetCore.Mvc;
using RestSharp;

namespace FiestaNetERP.Controllers
{
    public class AdministracionController : Controller
    {
        public IActionResult Index()
        {
           //al inicio el grid debe estar vació
            return View("GridView");
        }

        [System.Web.Mvc.ValidateInput(false)]
        public ActionResult GridView(string accion,string textoBusqueda)
        {
            IRestResponse<List<FacturaDetallada>> response = GetData2(criterio:accion,txtTextoBusqueda:textoBusqueda);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                ViewBag.ErrorMessage = response.ErrorMessage;
            }

            List<FacturaDetallada> objF = response.Data;
            return View("ConsultaIntegrada", objF);
        }

        public ActionResult ConsultaIntegrada(string accion, string textoBusqueda)
        {
            IRestResponse<List<FacturaDetallada>> response = GetData2(criterio: accion, txtTextoBusqueda: textoBusqueda);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                ViewBag.ErrorMessage = response.ErrorMessage;
            }

            List<FacturaDetallada> objF = response.Data;
            return PartialView("ConsultaIntegradaParcial", objF);
        }

        [HttpPost, System.Web.Mvc.ValidateInput(false)]
        public ActionResult ConsultaIntegradaParcialAddNew(FacturaDetallada item)
        {
            List<FacturaDetallada> objF = new List<FacturaDetallada>();
            if (ModelState.IsValid)
            {
                try
                {
                    IRestResponse<List<FacturaDetallada>> response = GetData2();
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        ViewBag.ErrorMessage = response.ErrorMessage;
                    }

                    objF = response.Data;

                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("ConsultaIntegradaParcial", objF);
        }
        [HttpPost, System.Web.Mvc.ValidateInput(false)]
        public ActionResult ConsultaIntegradaParcialUpdate(FacturaDetallada item)
        {
            List<FacturaDetallada> objF = new List<FacturaDetallada>();
            if (ModelState.IsValid)
            {
                try
                {
                    IRestResponse<List<FacturaDetallada>> response = GetData2();
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        ViewBag.ErrorMessage = response.ErrorMessage;
                    }

                    objF = response.Data;

                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("ConsultaIntegradaParcial", objF);


        }
        [HttpPost, System.Web.Mvc.ValidateInput(false)]
        public ActionResult ConsultaIntegradaParcialDelete(System.String cliente)
        {
            List<FacturaDetallada> objF = new List<FacturaDetallada>();
            if (cliente != null)
            {
                try
                {
                    IRestResponse<List<FacturaDetallada>> response = GetData2();
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        ViewBag.ErrorMessage = response.ErrorMessage;
                    }

                    objF = response.Data;
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("ConsultaIntegradaParcial", objF);
        }

        private IRestResponse<List<Factura>> GetData(string criterio="",string txtTextoBusqueda="")
        {
            
            var client = new RestSharp.RestClient("http://localhost:53874");
          
            string strBusqueda=string.Empty;
            if (!(txtTextoBusqueda == null || txtTextoBusqueda.Equals("")))
            {
                strBusqueda = txtTextoBusqueda;
                var request = new RestRequest("/api/ConsultaIntegrada/" + criterio + "/" + strBusqueda, Method.GET);
                IRestResponse<List<Factura>> response = client.Execute<List<Factura>>(request);
                return response;


            }
            else
            {
                var request = new RestRequest("/api/ConsultaIntegrada/", Method.GET);
                IRestResponse<List<Factura>> response = client.Execute<List<Factura>>(request);
                return response;

            }

        }

        private IRestResponse<List<FacturaDetallada>> GetData2(string criterio = "", string txtTextoBusqueda = "")
        {
            List<FacturaDetallada> lmd = new List<FacturaDetallada>();  // creating list of model.
            var client = new RestSharp.RestClient("http://localhost:53874");

            string strBusqueda = string.Empty;
            if (!(txtTextoBusqueda == null || txtTextoBusqueda.Equals("")))
            {
                strBusqueda = txtTextoBusqueda;
                var request = new RestRequest("/api/ConsultaIntegrada/" + criterio + "/" + strBusqueda, Method.GET);
                IRestResponse<List<FacturaDetallada>> response = client.Execute<List<FacturaDetallada>>(request);
                return response;

            }
            else
            {
                var request = new RestRequest("/api/ConsultaIntegrada/", Method.GET);
                IRestResponse<List<FacturaDetallada>> response = client.Execute<List<FacturaDetallada>>(request);
                return response;

            }

        }

        public ActionResult Factura(string accion, string textoBusqueda)
        {
            if (accion == null)
                accion = "cliente";
            IRestResponse<List<Factura> > response = GetData(criterio: accion, txtTextoBusqueda: textoBusqueda);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                ViewBag.ErrorMessage = response.ErrorMessage;
            }

            LoteFacturas objF = new LoteFacturas( response.Data);
            return View("Factura", objF);
        }
        
    


private static List<DetalleFactura> DetalleFac(string Factura)
        {
            List<DetalleFactura> lmd = new List<DetalleFactura>();  // creating list of model.
            var client = new RestSharp.RestClient("http://localhost:53874");

            var request = new RestRequest("/api/ConsultaIntegrada/Factura/" + Factura + "/detalle", Method.GET);
            IRestResponse<List<DetalleFactura>> response = client.Execute<List<DetalleFactura>>(request);
            if (response.Data != null)
            {
                lmd = response.Data;
            }
            return lmd;
        }
        public ActionResult Detalle(string Factura)
        {
            List<DetalleFactura> lstd = new List<DetalleFactura>();
            lstd = DetalleFac(Factura);
            return PartialView("Detalle", lstd);

        }

    }

}