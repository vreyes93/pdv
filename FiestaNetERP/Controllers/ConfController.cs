﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MF_Clases;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FiestaNetERP.Controllers
{
    [Produces("application/json")]
    [Route("api/Conf")]
    public class ConfController : Controller
    {
        // GET: api/Conf
        [HttpGet]
        public Config Get()
        {
            appSettings Settings = new appSettings();
            return Settings.SConfig;

        }

       
    }
}
