﻿using DevExpress.AspNetCore.Bootstrap;
using DevExpress.AspNetCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using MF.Comun.Dto;
using Microsoft.AspNetCore.Mvc;
using RestSharp;

namespace FiestaNetERP.Controllers
{
    public class FacturaDetalladaController : Controller
    {
        public IActionResult Index()
        {
            IRestResponse<List<Factura>> response = GetData();
            if (response.StatusCode != HttpStatusCode.OK)
            {
                ViewBag.ErrorMessage = response.ErrorMessage;
            }

            List<Factura> objF = response.Data;
            return PartialView("FacturaDetallada", objF);
        }

        public ActionResult FacturaDetallada(string accion, string textoBusqueda)
        {
            IRestResponse<List<Factura>> response = GetData2(criterio: accion, txtTextoBusqueda: textoBusqueda);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                ViewBag.ErrorMessage = response.ErrorMessage;
            }

            List<Factura> objF = response.Data;
            return PartialView("FacturaDetallada", objF);
        }
        public ActionResult GetFacturasPorCliente(string codCliente)
        {
            List<Factura> objF = new List<Factura>();
            
                try
                {
                    IRestResponse<List<Factura>> response = GetData("cliente",codCliente);
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        ViewBag.ErrorMessage = response.ErrorMessage;
                    }

                    objF = response.Data;

                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
           
            return View("FacturaDetallada", objF);
        }
        [HttpPost, System.Web.Mvc.ValidateInput(false)]
        public ActionResult FacturaDetalladaAddNew(DetalleFactura item)
        {
            List<Factura> objF = new List<Factura>();
            if (ModelState.IsValid)
            {
                try
                {
                    IRestResponse<List<Factura>> response = GetData();
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        ViewBag.ErrorMessage = response.ErrorMessage;
                    }

                    objF = response.Data;

                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return View("FacturaDetallada", objF);
        }
        [HttpPost, System.Web.Mvc.ValidateInput(false)]
        public ActionResult FacturaDetalladaUpdate(DetalleFactura item)
        {
            List<Factura> objF = new List<Factura>();
            if (ModelState.IsValid)
            {
                try
                {
                    IRestResponse<List<Factura>> response = GetData();
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        ViewBag.ErrorMessage = response.ErrorMessage;
                    }

                    objF = response.Data;

                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return View("FacturaDetallada", objF);


        }
        [HttpPost, System.Web.Mvc.ValidateInput(false)]
        public ActionResult FacturaDetalladaDelete(System.String cliente)
        {
            List<Factura> objF = new List<Factura>();
            if (cliente != null)
            {
                try
                {
                    IRestResponse<List<Factura>> response = GetData();
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        ViewBag.ErrorMessage = response.ErrorMessage;
                    }

                    objF = response.Data;
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return View("FacturaDetallada", objF);
        }

        private IRestResponse<List<Factura>> GetData(string criterio = "", string txtTextoBusqueda = "")
        {
            List<Factura> lmd = new List<Factura>();  // creating list of model.
            var client = new RestSharp.RestClient("http://localhost:53874");

            string strBusqueda = string.Empty;
            if (!(txtTextoBusqueda == null || txtTextoBusqueda.Equals("")))
            {
                strBusqueda = txtTextoBusqueda;
                var request = new RestRequest("/api/ConsultaIntegrada/" + criterio + "/" + strBusqueda, Method.GET);
                IRestResponse<List<Factura>> response = client.Execute<List<Factura>>(request);
                return response;

            }
            else
            {
                var request = new RestRequest("/api/ConsultaIntegrada/", Method.GET);
                IRestResponse<List<Factura>> response = client.Execute<List<Factura>>(request);
                return response;

            }

        }

        private IRestResponse<List<Factura>> GetData2(string criterio = "", string txtTextoBusqueda = "")
        {
            List<Factura> lmd = new List<Factura>();  // creating list of model.
            var client = new RestSharp.RestClient("http://localhost:53874");

            string strBusqueda = string.Empty;
            if (!(txtTextoBusqueda == null || txtTextoBusqueda.Equals("")))
            {
                strBusqueda = txtTextoBusqueda;
                var request = new RestRequest("/api/ConsultaIntegrada/cliente2/" + criterio + "/" + strBusqueda, Method.GET);
                IRestResponse<List<Factura>> response = client.Execute<List<Factura>>(request);
                return response;

            }
            else
            {
                var request = new RestRequest("/api/ConsultaIntegrada/cliente2/" + txtTextoBusqueda, Method.GET);
                IRestResponse<List<Factura>> response = client.Execute<List<Factura>>(request);
                return response;

            }

        }

        
        public IActionResult Detalle()
        {
           
            return PartialView("Administracion/Detalle");
        }
        public IActionResult GridViewPartialAddNew(MF.Comun.Dto.DetalleFactura item)
        {
            var model = new object[0];
            try
            {
                if (ModelState.IsValid)
                {
                    // Insert here a code to insert the new item in your model
                }
            }
            catch (Exception e)
            {
                ViewData["error"] = e.Message;
            }
            return PartialView("~/Views/Administracion/GridViewPartial.cshtml", model);
        }
        public IActionResult GridViewPartialUpdate(MF.Comun.Dto.DetalleFactura item)
        {
            var model = new object[0];
            try
            {
                if (ModelState.IsValid)
                {
                    // Insert here a code to update the item in your model
                }
            }
            catch (Exception e)
            {
                ViewData["error"] = e.Message;
            }
            return PartialView("~/Views/Administracion/GridViewPartial.cshtml", model);
        }
        public IActionResult GridViewPartialDelete(System.String NoFactura)
        {
            var model = new object[0];
            try
            {
                // Insert here a code to delete the item from your model
            }
            catch (Exception e)
            {
                ViewData["error"] = e.Message;
            }
            return PartialView("~/Views/Administracion/GridViewPartial.cshtml", model);
        }
    }
}