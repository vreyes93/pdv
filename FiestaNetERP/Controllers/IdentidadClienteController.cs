﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using MF.Comun.Dto;
using MF_Clases;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace FiestaNetERP.Controllers
{
    public class IdentidadClienteController : Controller
    {
        IdentidadCliente cliente = new IdentidadCliente();

        appSettings _appSettings;
        public IActionResult Index()
        {
            
            return View("IdentidadCliente",cliente);
        }

        [HttpGet]
        [Route("IdentidadCliente/{Pedido}/{ubicacion}")]
        public async Task<IActionResult> IdentidadCliente(string Pedido,string ubicacion)
        {
            #region "info solicitud del cliente"
            _appSettings = new appSettings();
            IdentidadCliente oIdentidad = new IdentidadCliente();
            
            ViewData["RestServicesUrl"] = _appSettings.SConfig.RestServicesUrl;
            ViewData["PagesUrl"] = _appSettings.SConfig.PagesUrl;
            ViewData["DefaultEnvironment"] = _appSettings.SConfig.Ambiente;
            ViewBag.pedido = Pedido;
            ViewBag.ubicacion = ubicacion;
            var cancellationTokenSource = new CancellationTokenSource();
            string urlRest = ViewData["RestServicesUrl"].ToString();
            if (urlRest != string.Empty)
            {
                var client = new RestClient(urlRest);
                var restRequest = new RestRequest("api/SolicitudCredito/" + Pedido, Method.GET)
                {
                    RequestFormat = DataFormat.Json
                };

                var result = await client.ExecuteTaskAsync(restRequest, cancellationTokenSource.Token);
                if (result != null)
                {
                    var resp = JsonConvert.DeserializeObject<Respuesta>(result.Content);
                    if (resp.Exito)
                        oIdentidad = JsonConvert.DeserializeObject<IdentidadCliente>(resp.Objeto.ToString());
                }
            }
            if (oIdentidad.Cliente == null)
            {
                //para cotizaciones que no tienen cliente asignado
                string url = ViewData["PagesUrl"] + "/cotizaciones.aspx?ErrorMsg=cliente&P=" + Pedido;
                Response.Redirect(url);
            }

            #endregion

            
            return View("IdentidadCliente", oIdentidad);
        }

        private async Task<IdentidadCliente> ObtenerInfoCliente(string cliente)
        { 
            IdentidadCliente oIdentidad = new IdentidadCliente();
            string urlRest = ViewData["RestServicesUrl"].ToString();//Ambiente.Equals("DES") ? "http://localhost:53874" : Ambiente.Equals("PRU") ? "http://sql.fiesta.local/RestServicesPruebas" : "http://sql.fiesta.local/RestServices";
            var client = new RestClient(urlRest);
            var cancellationTokenSource = new CancellationTokenSource();
            var restRequest = new RestRequest("api/Identidad/"+cliente, Method.GET)
            {
                RequestFormat = DataFormat.Json
            };

            var restResponse = await client.ExecuteTaskAsync(restRequest, cancellationTokenSource.Token);
            if (restResponse != null)
            {
               var lstIdentidad = JsonConvert.DeserializeObject<IEnumerable<IdentidadCliente>>(restResponse.Content).ToList();
                if (lstIdentidad.Count > 0)
                    oIdentidad = lstIdentidad[0];
            }
            return oIdentidad;
           
        }
    }
}