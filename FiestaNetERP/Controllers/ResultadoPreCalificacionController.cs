﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MF_Clases;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace FiestaNetERP.Controllers
{
    public class ResultadoPreCalificacionController : Controller
    {
        appSettings _appSettings;

        // GET: ResultadoPreCalificacion
        public ActionResult Index()
        {
            return View();
        }

        
        [HttpGet]
        [Route("ResultadoPreCalificacion/{id}")]
        public ActionResult ResultadoPreCalificacion(string id)
        {
            RespuestaSolicitud oRespuesta = null;
            if (id != null)
            {
                try
                {
                    oRespuesta= new RespuestaSolicitud();
                    _appSettings = new appSettings();
                    ViewData["RestServicesUrl"] = _appSettings.SConfig.RestServicesUrl;

                    string urlRest = ViewData["RestServicesUrl"].ToString();
                    if (urlRest != string.Empty)
                    {
                        var client = new RestClient(urlRest);
                        var restRequest = new RestRequest("api/RespuestaSolicitud/" + id, Method.GET)
                        {
                            RequestFormat = DataFormat.Json
                        };

                        var result = client.Get(restRequest);
                        if (result != null)
                        {
                            if (result.Content != null)
                                if (result.Content != "null")
                                { oRespuesta = JsonConvert.DeserializeObject<RespuestaSolicitud>(result.Content);
                                    return View("ResultadoPreCalificacion", oRespuesta);
                                }

                        }
                    }

                }
                catch (Exception ex)
                {
                    oRespuesta.MensajeError = "Error de la página: " + ex.Message;
                }
            }
            return View("ResultadoPreCalificacion", null);

        }
        
    }
}