﻿using MF.Comun.Dto;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FiestaNetERP.Models
{
    public static class Detalle
    {

        private static List<DetalleFactura> DetalleFac(string Factura)
        {
            List<DetalleFactura> lmd = new List<DetalleFactura>();  // creating list of model.
            var client = new RestSharp.RestClient("http://localhost:53874");

            var request = new RestRequest("/api/ConsultaIntegrada/Factura/" + Factura + "/detalle", Method.GET);
            IRestResponse<List<DetalleFactura>> response = client.Execute<List<DetalleFactura>>(request);
            if (response.Data != null)
            {
                lmd = response.Data;
            }
            return lmd;
        }
        public static Object GetDetalleFac(string Factura)
        {
            List<DetalleFactura> lstd = new List<DetalleFactura>();
            lstd = DetalleFac(Factura);
            return lstd;

        }
    }
}
