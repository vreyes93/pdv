﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Claims;
using System.Security.Principal;

namespace FiestaNetERP.Models.Extensions
{
    public static class IdentityExtensions
    {
        public static string GetTienda(this IIdentity identity)
        {
            var cValue = string.Empty;
            var claim = ((ClaimsIdentity)identity).FindFirst("Tienda");

            return (claim != null) ? claim.Value : string.Empty;
        }
        public static string GetNombre(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("Nombre");
            // Test for null to avoid issues during local testing
            return (claim != null) ? claim.Value : string.Empty;
        }

        public static string GetVendedor(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("CodigoVendedor");
            // Test for null to avoid issues during local testing
            return (claim != null) ? claim.Value : string.Empty;
        }
    }
}