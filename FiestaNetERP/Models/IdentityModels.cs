﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace FiestaNetERP.Models
{
    // You can add User data for the user by adding more properties to your User class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public ClaimsIdentity GenerateUserIdentity(ApplicationUserManager manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = manager.CreateIdentity(this, DefaultAuthenticationTypes.ApplicationCookie);

            //IdentityUserClaim role = new IdentityUserClaim();
            //role.ClaimType = ClaimTypes.Role;
            //role.ClaimValue = "RL_RRHH";
            //userIdentity.Claims.Add(role);

            userIdentity.AddClaim(new Claim(ClaimTypes.Role, "RL_RRHH"));

            // Add custom user claims here
            return userIdentity;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUserManager manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            userIdentity.AddClaim(new Claim("Tienda", this.Tienda.ToString()));
            userIdentity.AddClaim(new Claim("Nombre", this.Nombre.ToString()));
            userIdentity.AddClaim(new Claim("CodigoVendedor", this.CodigoVendedor.ToString()));
            return userIdentity;
            //return Task.FromResult(GenerateUserIdentity(manager));
        }

        public string Nombre { get; set; }
        public string Tienda { get; set; }
        public string CodigoVendedor { get; set; }

        //public virtual ICollection<UserLog> UserLogs { get; set; }
        //public virtual ICollection<UserMenu> UserMenus { get; set; }
    }

    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base() { }
        public ApplicationRole(string name) : base(name) { }
        public string Description { get; set; }

    }

    //public class UserLog
    //{
    //    [Key]
    //    public Guid UserLogID { get; set; }

    //    public string IPAD { get; set; }
    //    public DateTime LoginDate { get; set; }
    //    public string UserId { get; set; }

    //    [ForeignKey("UserId")]
    //    public virtual ApplicationUser User { get; set; }
    //}

    //public class UserMenu
    //{
    //    [Key]
    //    public Guid ID { get; set; }

    //    public string MenuID { get; set; }

    //    public string MenuName { get; set; }

    //    public string ParentMenuID { get; set; }

    //    public string MenuFile { get; set; }

    //    public string MenuURL { get; set; }

    //    public string Active { get; set; }

    //    public string CreatedBy { get; set; }
    //    public string UpdatedBy { get; set; }
    //    public DateTime CreateDate{ get; set; }
    //    public DateTime RecordDate { get; set; }
    //    public string RoleId { get; set; }
    //    public string UserId { get; set; }
    //    [ForeignKey("UserId")]
    //    public virtual ApplicationUser User { get; set; }
    //}

    //public System.Data.Entity.DbSet<UserLog> UsersLogs { get; set; }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("ApplicationServices", throwIfV1Schema: false)
        {
        }

        static ApplicationDbContext()
        {
            Database.SetInitializer<ApplicationDbContext>(new ApplicationDbInitializer());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }



        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("prodmult");
            modelBuilder.Entity<ApplicationUser>().ToTable("MF_Usuario").Property(p => p.Id).HasColumnName("IdUsuario");
            modelBuilder.Entity<IdentityUser>().ToTable("MF_Usuario");
            modelBuilder.Entity<IdentityUser>().ToTable("MF_Usuario").Property(u => u.UserName).HasColumnName("Usuario");
            modelBuilder.Entity<IdentityUser>().ToTable("MF_Usuario").Property(u => u.AccessFailedCount).HasColumnName("IntentosFallidos");
            modelBuilder.Entity<IdentityUser>().ToTable("MF_Usuario").Property(u => u.Id).HasColumnName("IdUsuario");
            modelBuilder.Entity<IdentityUser>().ToTable("MF_Usuario").Property(u => u.EmailConfirmed).HasColumnName("EmailConfirmado");
            modelBuilder.Entity<IdentityUser>().ToTable("MF_Usuario").Property(u => u.LockoutEnabled).HasColumnName("BloqueoHabilitado");
            modelBuilder.Entity<IdentityUser>().ToTable("MF_Usuario").Property(u => u.PhoneNumber).HasColumnName("Telefono");
            modelBuilder.Entity<IdentityUser>().ToTable("MF_Usuario").Property(u => u.PhoneNumberConfirmed).HasColumnName("TelefonoConfirmado");
            modelBuilder.Entity<IdentityUser>().ToTable("MF_Usuario").Property(u => u.SecurityStamp).HasColumnName("SelloSeguridad");
            modelBuilder.Entity<IdentityUser>().ToTable("MF_Usuario").Property(u => u.TwoFactorEnabled).HasColumnName("FactorDobleSeguridadHabilitado");
            modelBuilder.Entity<IdentityUser>().ToTable("MF_Usuario").Property(u => u.LockoutEndDateUtc).HasColumnName("FechaBloqueoFinUTC");

            modelBuilder.Entity<IdentityRole>().ToTable("MF_Rol");
            modelBuilder.Entity<IdentityRole>().ToTable("MF_Rol").Property(u => u.Name).HasColumnName("Nombre");
            modelBuilder.Entity<IdentityRole>().ToTable("MF_Rol").Property(u => u.Id).HasColumnName("IdRol");

            modelBuilder.Entity<IdentityUserRole>().ToTable("MF_UsuarioRol");
            modelBuilder.Entity<IdentityUserRole>().ToTable("MF_UsuarioRol").Property(u => u.RoleId).HasColumnName("IdRol");
            modelBuilder.Entity<IdentityUserRole>().ToTable("MF_UsuarioRol").Property(u => u.UserId).HasColumnName("IdUsuario");
            modelBuilder.Entity<IdentityUserRole>().ToTable("MF_UsuarioRol").HasKey(key => new { key.UserId, key.RoleId });

            modelBuilder.Entity<IdentityUserClaim>().ToTable("MF_AtribucionUsuario");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("MF_AtribucionUsuario").Property(u => u.ClaimType).HasColumnName("AtributoUsuario");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("MF_AtribucionUsuario").Property(u => u.ClaimValue).HasColumnName("Valor");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("MF_AtribucionUsuario").Property(u => u.UserId).HasColumnName("IdUsuario");


            modelBuilder.Entity<IdentityUserLogin>().ToTable("MF_LoginUsuario");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("MF_LoginUsuario").Property(u => u.LoginProvider).HasColumnName("ProveedorDeLogueo");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("MF_LoginUsuario").Property(u => u.ProviderKey).HasColumnName("Llave");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("MF_LoginUsuario").Property(u => u.UserId).HasColumnName("IdUsuario");

        }



    }
}

#region Helpers
//namespace FiestaNetERP
//{
//    public static class IdentityHelper
//    {
//        // Used for XSRF when linking external logins
//        public const string XsrfKey = "XsrfId";

//        public const string ProviderNameKey = "providerName";
//        public static string GetProviderNameFromRequest(System.Web.HttpRequest request)
//        {
//            return request.QueryString[ProviderNameKey];
//        }

//        public const string CodeKey = "code";
//        public static string GetCodeFromRequest(HttpRequest request)
//        {
//            return request.QueryString[CodeKey];
//        }

//        public const string UserIdKey = "userId";
//        public static string GetUserIdFromRequest(HttpRequest request)
//        {
//            return HttpUtility.UrlDecode(request.QueryString[UserIdKey]);
//        }

//        public static string GetResetPasswordRedirectUrl(string code, HttpRequest request)
//        {
//            var absoluteUri = "/Account/ResetPassword?" + CodeKey + "=" + HttpUtility.UrlEncode(code);
//            return new Uri(request.Url, absoluteUri).AbsoluteUri.ToString();
//        }

//        public static string GetUserConfirmationRedirectUrl(string code, string userId, HttpRequest request)
//        {
//            var absoluteUri = "/Account/Confirm?" + CodeKey + "=" + HttpUtility.UrlEncode(code) + "&" + UserIdKey + "=" + HttpUtility.UrlEncode(userId);
//            return new Uri(request.Url, absoluteUri).AbsoluteUri.ToString();
//        }

//        private static bool IsLocalUrl(string url)
//        {
//            return !string.IsNullOrEmpty(url) && ((url[0] == '/' && (url.Length == 1 || (url[1] != '/' && url[1] != '\\'))) || (url.Length > 1 && url[0] == '~' && url[1] == '/'));
//        }

//        public static void RedirectToReturnUrl(string returnUrl, HttpResponse response)
//        {
//            if (!String.IsNullOrEmpty(returnUrl) && IsLocalUrl(returnUrl))
//            {
//                response.Redirect(returnUrl);
//            }
//            else
//            {
//                response.Redirect("~/");
//            }
//        }
//    }
//}
#endregion