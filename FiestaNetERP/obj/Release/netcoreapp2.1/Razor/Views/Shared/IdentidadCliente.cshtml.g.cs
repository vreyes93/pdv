#pragma checksum "C:\desarrollo\migracion\pdv-migracion\Desarrollo\FiestaNetERP\Views\Shared\IdentidadCliente.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b8bad1cfc03466a41d80e17423f56df869813dec"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_IdentidadCliente), @"mvc.1.0.view", @"/Views/Shared/IdentidadCliente.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/IdentidadCliente.cshtml", typeof(AspNetCore.Views_Shared_IdentidadCliente))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\desarrollo\migracion\pdv-migracion\Desarrollo\FiestaNetERP\Views\_ViewImports.cshtml"
using FiestaNetERP;

#line default
#line hidden
#line 2 "C:\desarrollo\migracion\pdv-migracion\Desarrollo\FiestaNetERP\Views\_ViewImports.cshtml"
using FiestaNetERP.Models;

#line default
#line hidden
#line 3 "C:\desarrollo\migracion\pdv-migracion\Desarrollo\FiestaNetERP\Views\_ViewImports.cshtml"
using DevExpress.AspNetCore;

#line default
#line hidden
#line 4 "C:\desarrollo\migracion\pdv-migracion\Desarrollo\FiestaNetERP\Views\_ViewImports.cshtml"
using DevExpress.AspNetCore.Bootstrap;

#line default
#line hidden
#line 1 "C:\desarrollo\migracion\pdv-migracion\Desarrollo\FiestaNetERP\Views\Shared\IdentidadCliente.cshtml"
using FiestaNetERP.Models.Extensions;

#line default
#line hidden
#line 2 "C:\desarrollo\migracion\pdv-migracion\Desarrollo\FiestaNetERP\Views\Shared\IdentidadCliente.cshtml"
using System.Web.Mvc;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b8bad1cfc03466a41d80e17423f56df869813dec", @"/Views/Shared/IdentidadCliente.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0d24b7558b6dbb4cf57b211367ebb897aea90962", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_IdentidadCliente : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<MF.Comun.Dto.IdentidadCliente>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(64, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 5 "C:\desarrollo\migracion\pdv-migracion\Desarrollo\FiestaNetERP\Views\Shared\IdentidadCliente.cshtml"
  
    ViewData["Title"] = "IdentidadCliente";
    Layout = "~/Views/Shared/_Layout.cshtml";


#line default
#line hidden
            BeginContext(205, 319, true);
            WriteLiteral(@"
    <div style=""padding-left:25px; padding-right:15px; padding-bottom:10px; padding-top:5px;"">
        <div id=""wizard"" class=""aiia-wizard"" style=""display: none;"">

            <div class=""aiia-wizard-step"">
                <h1>Huella Digital</h1>
                <div class=""step-content"">
                    ");
            EndContext();
            BeginContext(525, 42, false);
#line 17 "C:\desarrollo\migracion\pdv-migracion\Desarrollo\FiestaNetERP\Views\Shared\IdentidadCliente.cshtml"
               Write(await Html.PartialAsync("pwHuella", Model));

#line default
#line hidden
            EndContext();
            BeginContext(567, 195, true);
            WriteLiteral("\r\n\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"aiia-wizard-step\">\r\n                <h1>Fotografía</h1>\r\n                <div class=\"step-content\">\r\n                    ");
            EndContext();
            BeginContext(763, 40, false);
#line 25 "C:\desarrollo\migracion\pdv-migracion\Desarrollo\FiestaNetERP\Views\Shared\IdentidadCliente.cshtml"
               Write(await Html.PartialAsync("pwFoto", Model));

#line default
#line hidden
            EndContext();
            BeginContext(803, 189, true);
            WriteLiteral("\r\n                </div>\r\n            </div>\r\n          \r\n        <div class=\"aiia-wizard-step\">\r\n            <h1>¡Finalizado!</h1>\r\n            <div class=\"step-content\">\r\n                ");
            EndContext();
            BeginContext(993, 45, false);
#line 32 "C:\desarrollo\migracion\pdv-migracion\Desarrollo\FiestaNetERP\Views\Shared\IdentidadCliente.cshtml"
           Write(await Html.PartialAsync("pwFinWizard", Model));

#line default
#line hidden
            EndContext();
            BeginContext(1038, 76, true);
            WriteLiteral("\r\n\r\n            </div>\r\n        </div>\r\n\r\n\r\n\r\n\r\n        </div>\r\n    </div>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<MF.Comun.Dto.IdentidadCliente> Html { get; private set; }
    }
}
#pragma warning restore 1591
