﻿// Write your JavaScript code.
//Code for example B
$("input.buttonBslidedown").click(function () { $("div.contentToChange").find("p.fourthparagraph:hidden").slideDown("slow"); });
$("input.buttonBslideup").click(function () { $("div.contentToChange").find("p.fourthparagraph:visible").slideUp("slow"); });
//show code example B
$("a.codeButtonB").click(function () { $("pre.codeB").toggle() });


$(document).ready(function () {
    $("#flip").click(function () {
        $("#panel").slideDown("slow");
    });
});
$(document).ready(function () {
    $("#flop").click(function () {
        $("#panel").slideUp("slow");
    });
});

function myFunctionHuella(_cliente,_ambiente,_monto,_cuotas1,_cuotas2,_enganche,_saldoFinanciar) {
    var control = document.getElementById('MyActiveX');
    control.Cliente = _cliente;
    control.Ambiente = _ambiente;
    NumbersFormatted(_monto, _cuotas1, _cuotas2, _enganche, _saldoFinanciar);
}

function myFunctionFoto(_cliente, _ambiente, _monto, _cuotas1, _cuotas2, _enganche, _saldoFinanciar) {
    
    var control = document.getElementById('MyActiveXFoto');
    control.Cliente = _cliente;
    control.Ambiente = _ambiente;
    NumbersFormattedFoto(_monto, _cuotas1, _cuotas2, _enganche, _saldoFinanciar);
}

function myFunctionFinWizard(_monto, _cuotas1, _cuotas2, _enganche, _saldoFinanciar) {
    NumbersFormattedFinWizzard(_monto, _cuotas1, _cuotas2, _enganche, _saldoFinanciar);
   
}

function myFunctionEnviarSolicitud() {   
    self.close();
        //document.location.href = 'https://fiestanet.mueblesfiesta.com/PDVIdentityPruebas/pedidos.aspx?EnviarSol=si';
        //'http://localhost:65502/pedidos.aspx?EnviarSol=si';
   
}

function FormatNumber(num) {
   return parseFloat(Math.round(num * 100) / 100).toFixed(2);
}
function NumbersFormatted(monto, cuotas1, cuotas2, enganche, saldoFinanciar) {
   
    document.getElementById('monto').innerHTML =" Monto a Facturar: Q."+ parseFloat(Math.round(monto * 100) / 100).toFixed(2);
    document.getElementById('cuotas1').innerHTML = "Q."+parseFloat(Math.round(cuotas1 * 100) / 100).toFixed(2);
    document.getElementById('cuotas2').innerHTML =  "Q."+parseFloat(Math.round(cuotas2 * 100) / 100).toFixed(2); 
    document.getElementById('enganche').innerHTML = " Enganche: Q." + parseFloat(Math.round(enganche * 100) / 100).toFixed(2);
    document.getElementById('saldoFinanciar').innerHTML = " Saldo a financiar: Q." + parseFloat(Math.round(saldoFinanciar * 100) / 100).toFixed(2);
}

function NumbersFormattedFoto(monto, cuotas1, cuotas2, enganche, saldoFinanciar) {
   
    document.getElementById('montoFoto').innerHTML = " Monto a Facturar: Q." + parseFloat(Math.round(monto * 100) / 100).toFixed(2);
    document.getElementById('cuotas1Foto').innerHTML = "Q." + parseFloat(Math.round(cuotas1 * 100) / 100).toFixed(2);
    document.getElementById('cuotas2Foto').innerHTML = "Q." + parseFloat(Math.round(cuotas2 * 100) / 100).toFixed(2);
    document.getElementById('engancheFoto').innerHTML = " Enganche: Q." + parseFloat(Math.round(enganche * 100) / 100).toFixed(2);
    document.getElementById('saldoFinanciarFoto').innerHTML = " Saldo a financiar: Q." + parseFloat(Math.round(saldoFinanciar * 100) / 100).toFixed(2);
}

function NumbersFormattedFinWizzard(monto, cuotas1, cuotas2, enganche, saldoFinanciar) {
   
    document.getElementById('montoFW').innerHTML = " Monto a Facturar: Q." + parseFloat(Math.round(monto * 100) / 100).toFixed(2);
    document.getElementById('cuotas1FW').innerHTML = "Q." + parseFloat(Math.round(cuotas1 * 100) / 100).toFixed(2);
    document.getElementById('cuotas2FW').innerHTML = "Q." + parseFloat(Math.round(cuotas2 * 100) / 100).toFixed(2);
    document.getElementById('engancheFW').innerHTML = " Enganche: Q." + parseFloat(Math.round(enganche * 100) / 100).toFixed(2);
    document.getElementById('saldoFinanciarFW').innerHTML = " Saldo a financiar: Q." + parseFloat(Math.round(saldoFinanciar * 100) / 100).toFixed(2);
}
