﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FiestaNetExternalServices.Models;
using MF_Clases;
using MF_Clases.GuateFacturas.FEL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace FiestaNetExternalServices.Controllers
{
    
    public class DocumentosContaController : Controller
    {
        [HttpGet]
        public IActionResult DocumentosConta()
        {
            Models.appSettings settings = new Models.appSettings();
            var client = new RestClient(string.Format("{0}/api/" + Const.SERVICIO_OBTENER_NOTAS_CREDITO_DEBITO_SIN_FIRMAR, settings.UrlRestServices()));
            var request = new RestRequest("NoFirmadas", Method.GET);

            request.ReadWriteTimeout = 36000000;
            try
            {
                var response = client.Get(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Respuesta mRespuesta=  JsonConvert.DeserializeObject<Respuesta>(response.Content);
                    List <Doc> lstdocumentos= JsonConvert.DeserializeObject<List<Doc>>(mRespuesta.Objeto.ToString());
                    List<ErrorMessage> lstErrores = new List<ErrorMessage>();
                    ViewData["lstDocumentos"] = lstdocumentos;
                    
                    var MsgErrores= ((mRespuesta.Mensaje.Length > 0 && mRespuesta.Mensaje.Contains("|")) ? mRespuesta.Mensaje.Split("|").ToList() : new List<string>());

                    foreach (string s in MsgErrores)
                        lstErrores.Add(new ErrorMessage { Mensaje = s });

                    ViewData["lstErrores"] = lstErrores;
                    return View("../Contabilidad/DocumentosConta",mRespuesta);
                }
            }
            catch
            {
                return View("Error");
            }
            return View("Error");
            
        }

        [HttpGet]
        public ActionResult BindGrid()
        {
            //Models.appSettings settings = new Models.appSettings();
            //var client = new RestClient(string.Format("{0}/api/" + Const.SERVICIO_OBTENER_NOTAS_CREDITO_DEBITO_SIN_FIRMAR, settings.UrlRestServices()));
            //var request = new RestRequest("NoFirmadas", Method.GET);

            //request.ReadWriteTimeout = 36000000;
            //try
            //{
            //    var response = client.Get(request);
            //    if (response.StatusCode == System.Net.HttpStatusCode.OK)
            //    {
            //        List<Doc> documentos = JsonConvert.DeserializeObject<List<Doc>>(response.Content);
            //        return View("../Contabilidad/pvDocumentosSinFirmar", documentos.AsEnumerable());
            //    }
            //}
            //catch
            //{
            //    return View("Error");
            //}
            return View("Error");
        }
    }
}