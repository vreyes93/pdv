﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FiestaNetExternalServices.Models;
using MF_Clases;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace FiestaNetExternalServices.Controllers.Facturacion
{
    public class ConsultaController : Controller
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: Consulta
        public ActionResult Index(MF_Clases.ConsultaFacturaMayoreo consulta=null)
        {
            log.Debug(string.Format("Iniciando consulta de orden de compra Mayoreo parámetros: Orden de compra {0}, cliente {1}, Fecha {2},Tienda {3}", consulta.OrdenDeCompra, consulta.Cliente, consulta.FechaIni,consulta.Cobrador));

            List<FacturaMayoreo> lstdocumentos = new List<FacturaMayoreo>();
            //consulta.Fecha = consulta.Fecha!=null? consulta.Fecha.Equals("Ingrese Fecha de la factura") ? null : consulta.Fecha:null;
            consulta.Cliente = consulta.Cliente != null ? consulta.Cliente.Equals("Código del Cliente") ? null : consulta.Cliente : null;
            consulta.OrdenDeCompra = consulta.OrdenDeCompra != null ? consulta.OrdenDeCompra.Equals("Orden de Compra") ? null : consulta.OrdenDeCompra : null;
            consulta.NombreCliente = consulta.NombreCliente != null ? consulta.NombreCliente.Equals("Nombre del Cliente") ? null : consulta.NombreCliente : null;

            

            if (consulta.OrdenDeCompra != null || (consulta.FechaIni!=null && consulta.FechaFin !=null) || consulta.Cliente != null || consulta.NombreCliente!=null || consulta.Cobrador !=null)
            {
                consulta.Cliente = consulta.Cliente != null ? consulta.Cliente.Replace("'", "''").Replace("%", "").Replace("@", ""):null;
                consulta.NombreCliente = consulta.NombreCliente != null ? consulta.NombreCliente.Replace("'", "''").Replace("%", "").Replace("@", "") : null;
                consulta.OrdenDeCompra = consulta.OrdenDeCompra != null ? consulta.OrdenDeCompra.Replace("'", "''").Replace("%", "").Replace("@", "") : null;
                consulta.Cobrador = consulta.Cobrador != null ? consulta.Cobrador.Replace("'", "''").Replace("%", "").Replace("@", "") : null;

                Models.appSettings settings = new Models.appSettings();
                var client = new RestClient(string.Format("{0}/api/" + Const.SERVICIO_ORDEN_DE_COMPRA_MAYOREO, settings.UrlRestServices()));
                var request = new RestRequest(string.Format("?OrdenDeCompra={0}&FechaIni={1}&FechaFin={2}&Cliente={3}&NombreCliente={4}&Tienda={5}", consulta.OrdenDeCompra, consulta.FechaIni.HasValue? consulta.FechaIni.Value.ToString("dd/MM/yyyy"):null, consulta.FechaFin.HasValue ? consulta.FechaFin.Value.ToString("dd/MM/yyyy") : null, consulta.Cliente,consulta.NombreCliente,consulta.Cobrador), Method.GET);

                log.Debug("Consultado a: " + client.BaseUrl.AbsoluteUri + string.Format("?OrdenDeCompra={0}&FechaIni={1}&FechaFin={2}&Cliente={3}&NombreCliente={4}&Tienda={5}", consulta.OrdenDeCompra, consulta.FechaIni.HasValue ? consulta.FechaIni.Value.ToString("dd/MM/yyyy") : null, consulta.FechaFin.HasValue ? consulta.FechaFin.Value.ToString("dd/MM/yyyy") : null, consulta.Cliente, consulta.NombreCliente,consulta.Cobrador));

                request.ReadWriteTimeout = 36000000;
                try
                {
                    var response = client.Get(request);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        log.Debug(response.Content);
                        lstdocumentos = JsonConvert.DeserializeObject<List<FacturaMayoreo>>(response.Content);
                        if (lstdocumentos.Count > 0)
                            foreach (var fac in lstdocumentos)
                            {
                                fac.linkFactura = fac.Estado.Equals("Vigente")? settings.UrlPortalEnviromnents() + "/" + fac.linkFactura:string.Empty;
                                //fac.FechaPedido = DateTime.Parse(fac.FechaPedido).ToString("dd/MM/yyyy");
                            }
                        log.Debug("Devolvió "+lstdocumentos.Count+" resultados.");
                        ViewData["lstOrdenes"] = lstdocumentos;
                        log.Debug("Cantidad de documentos devueltos :" + lstdocumentos.Count);

                        if (ViewData["Tiendas"] == null)
                        {

                            List<Tiendas> lstTiendas1 = GetTiendas();
                            if (lstTiendas1.Count > 0)
                                ViewData["Tiendas"] = lstTiendas1;
                        }




                        return View("../Facturacion/Consulta",consulta);
                    }
                    else
                    {
                        ViewBag.ErrorMessage = response.ErrorMessage;
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Ocurrió un error al consultar la ordén de compra " + ex.Message+" "+ex.InnerException);
                    ViewBag.ErrorMessage = "Ocurrió un error al consultar la ordén de compra " + ex.Message+" "+ex.InnerException;
                }
                return View("Error");
            }
            if (ViewData["Tiendas"] == null)
            {

                List<Tiendas> lstTiendas1 = GetTiendas();
                if (lstTiendas1.Count > 0)
                    ViewData["Tiendas"] = lstTiendas1;
            }
            if (ViewData["lstOrdenes"] == null)
                ViewData["lstOrdenes"] = new List<FacturaMayoreo>();

                return View("../Facturacion/Consulta");
        }

        // GET: Consulta/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        private List<Tiendas> GetTiendas()
        {
            Models.appSettings settings = new Models.appSettings();
            var client = new RestClient(string.Format("{0}/api/Tiendas", settings.UrlRestServices()));
            var request = new RestRequest("", Method.GET);

            request.ReadWriteTimeout = 36000000;

            var response = client.Get(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var lstTiendas = JsonConvert.DeserializeObject<IEnumerable<Tiendas>>(response.Content);
                Tiendas F01 = lstTiendas.ToList<Tiendas>().Find(x => x.Tienda == "F01");
                List<Tiendas> Lst = lstTiendas.ToList();
               
                return Lst;
            }
            return null;
        }
    }
}