﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FiestaNetExternalServices.Models;
using MF_Clases;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace FiestaNetExternalServices.Controllers.Facturacion
{
    [Route("OrdenesDeCompra")]
    public class OrdenesDeCompraController : Controller
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [HttpPost]
        public async Task<IActionResult> OrdenesDeCompra(ConsultaFacturaMayoreo consulta)
        {
            log.Debug(string.Format("Iniciando consulta de orden de compra Mayoreo parámetros: Orden de compra {0}, cliente {1}, Fecha {2}", consulta.OrdenDeCompra, consulta.Cliente, consulta.FechaIni));

            List<FacturaMayoreo> lstdocumentos = new List<FacturaMayoreo>();
            //consulta.Fecha = consulta.Fecha!=null? consulta.Fecha.Equals("Ingrese Fecha de la factura") ? null : consulta.Fecha:null;
            consulta.Cliente =consulta.Cliente!=null? consulta.Cliente.Equals("Nombre del Cliente") ? null : consulta.Cliente:null;
            consulta.OrdenDeCompra = consulta.OrdenDeCompra !=null? consulta.OrdenDeCompra.Equals("Orden de Compra") ? null : consulta.OrdenDeCompra:null;

            if (consulta.OrdenDeCompra != null || consulta.FechaIni != null || consulta.Cliente != null)
            {
                Models.appSettings settings = new Models.appSettings();
                var client = new RestClient(string.Format("{0}/api/" + Const.SERVICIO_ORDEN_DE_COMPRA_MAYOREO, settings.UrlRestServices()));
                var request = new RestRequest(string.Format("?OrdenDeCompra={0}&Fecha={1}&Cliente={2}", consulta.OrdenDeCompra, consulta.FechaIni.Value.ToString("dd/MM/yyyy"), consulta.Cliente), Method.GET);

                log.Debug("Consultado a: "+client.BaseUrl.AbsoluteUri+ string.Format("?OrdenDeCompra={0}&Fecha={1}&Cliente={2}", consulta.OrdenDeCompra, consulta.FechaIni.Value.ToString("dd/MM/yyyy"), consulta.Cliente));
                
                request.ReadWriteTimeout = 36000000;
                try
                {
                    var response = client.Get(request);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        lstdocumentos = JsonConvert.DeserializeObject<List<FacturaMayoreo>>(response.Content);
                        if (lstdocumentos.Count > 0)
                            foreach (var fac in lstdocumentos)
                            {
                                fac.linkFactura = settings.UrlPortalEnviromnents() + "/" + fac.linkFactura;
                                fac.FechaPedido = Utilitarios.dateParseFormatDMY(fac.FechaPedido).ToString("dd/MM/yyyy");
                            }
                        ViewData["lstOrdenes"] = lstdocumentos;
                        log.Debug("Cantidad de documentos devueltos :"+lstdocumentos.Count());
                        return View("../Facturacion/Consulta");
                    }
                }
                catch(Exception ex)
                {
                    log.Error("Ocurrió un error al consultar la ordén de compra "+ex.Message);
                    ViewBag["ErrorMessage"] = "Ocurrió un error al consultar la ordén de compra " + ex.Message;
                }
                return View("Error");
            }

            PartialViewResult result = new PartialViewResult()
            {
                ViewName = "../Facturacion/Consulta",
            };

            return result;

        }


        [HttpGet]
        public IActionResult Binding()
        {

            return PartialView("OrdenesDeCompra");

        }
    }
}