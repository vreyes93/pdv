﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using FiestaNetExternalServices.Models;
using MF_Clases;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;
using appSettings = FiestaNetExternalServices.Models.appSettings;

namespace FiestaNetExternalServices.Controllers
{
    [Route("api/Facturacion")]
    public class FacturacionController : Controller
    {
        [HttpPost]
        public ActionResult Post([FromBody] Pedido pedido)
        {
            string mensaje = string.Empty;

            if (pedido != null)
            {
                appSettings appsettings = new appSettings();
                var client = new RestClient(string.Format("{0}", appsettings.UrlRestServices()));
                var request = new RestRequest("/api/Facturacion", Method.POST);
                Respuesta infoResp = new Respuesta();
                request.ReadWriteTimeout = 36000000;

                request.AddHeader("Content-type", "application/json");
                request.AddJsonBody(pedido); // AddJsonBody serializes the object automatically

                var response = client.Post(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    infoResp = JsonConvert.DeserializeObject<Respuesta>(response.Content);
                    return Ok(infoResp);
                }
                else
                    return BadRequest(response.ErrorMessage);
                #region "anterior"
                //WebRequest GuardarRequest = WebRequest.Create("http://sql.fiesta.local/RestServicesPruebas/api/Facturacion");

                //string json = JsonConvert.SerializeObject(pedido);
                //byte[] data = Encoding.ASCII.GetBytes(json);

                //GuardarRequest.Method = "POST";
                //GuardarRequest.ContentType = "application/json";
                //GuardarRequest.ContentLength = data.Length;

                //using (var stream = GuardarRequest.GetRequestStream())
                //{
                //    stream.Write(data, 0, data.Length);
                //}

                //var GuardarResponse = (HttpWebResponse)GuardarRequest.GetResponse();

                //if (GuardarResponse.StatusCode == HttpStatusCode.OK)
                //{
                //    return Ok();

                //}
                #endregion  
            }
            return BadRequest();
        }
    }

    public class Pedido
    {
        public string Codigo { get; set; }
        public string Usuario { get; set; }
        public DateTime FechaPedido { get; set; }
        public DateTime FechaEntrega { get; set; }
        public decimal TotalPedido { get; set; }
        public int TotalItems { get; set; }
        public string Tienda { get; set; }
        public decimal DescuentoGeneral { get; set; }
        public Cliente cliente { get; set; }
        public List<Articulo> articulos { get; set; }

    }

    public class Cliente
    {
        public string codigo { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Email { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Genero { get; set; }
        public bool RecibirInfo { get; set; }
        public string NIT { get; set; }
        public string Identificacion { get; set; }
        public string Municipio { get; set; }
        public string Departamento { get; set; }
        public string Telefono { get; set; }
        public string Indicaciones { get; set; }
        public string Zona { get; set; }
        public string Casa { get; set; }
        public string Apartamento { get; set; }
        public string Calle_avenida { get; set; }
        public string Colonia { get; set; }
        public string FormaDePago { get; set; }

        public Cliente()
        {
            RecibirInfo = false;
        }
    }

    public class Articulo
    {
        public int Linea { get; set; }
        public string Descripcion { get; set; }
        public string CodigoArticulo { get; set; }
        public decimal Descuento { get; set; }
        public decimal Precio { get; set; }
        public int cantidad { get; set; }
        public string CodigoPadre { get; set; }
        public string TipoLinea { get; set; }
        public Articulo()
        {
            TipoLinea = "N";

        }
    }
}
         
                          