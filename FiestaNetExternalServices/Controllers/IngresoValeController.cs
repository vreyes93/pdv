﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using reCAPTCHA.AspNetCore;
using FiestaNetExternalServices.Models;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;
using Microsoft.AspNetCore.Http;
using System.Net.Mail;
using System.Net.Mime;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting;
using System.Net.Http;

namespace FiestaNetExternalServices.Controllers
{
    public class IngresoValeController : Controller
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IRecaptchaService _recaptcha;
        private readonly IHostingEnvironment _env;

        public IngresoValeController(IRecaptchaService recaptcha)
        {
            _recaptcha = recaptcha;
        }

        
        /// <summary>
        /// Método para grabar el vale de descuento con las distintas características:
        ///  (1) Validándo la ip del solicitante para evitar que genere más de un vale
        ///  (2) Validando la ubicación del solicitante para evitar que personas que no han visto el anuncio generen el vale.
        ///  (3) Validando que no sea un bot (con re-captcha)
        ///  (4) El vale puede ser por porcentaje o por un valor único (debe tener uno de los dos por default)
        ///  (5) En appsettings.json se configuran las tiendas para cajear el vale, el tipo de vale y el valor o porcentaje de vale
        ///  (6) Se valida que el usuario no este realizando intentos seleccionando una tienda al azar, utilizando cookies. //Deshabilitado
        ///  (7) Por promoción está pendiente (febrero 2019)
        /// </summary>
        /// <param name="infoVale"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Vale(Vale infoVale)
        {

            if (ModelState.IsValid)
            {
                appSettings settings = new appSettings();
                var lstTiendasVale = settings.SConfig.TiendasVale;


                log.Info("Entro");
                var recaptcha = await Validate(Request, true);


                if (!recaptcha.success)
                    {
                    log.Error("Error con el captcha");
                        ModelState.AddModelError("Recaptcha", "Ocurrió un error al validar el reCaptcha, por favor intente de nuevo!");
                        return View("../Mercadeo/IngresoVale", infoVale);
                    }
                #region "tests"
                //string path = Directory.GetCurrentDirectory();
                //string target = @"c:\temp";
                //Console.WriteLine("The current directory is {0}", path);
                //if (Directory.Exists(target))
                #endregion
                {
                    //validar ip del solicitante
                    Vale valeAux = BuscarValePorIP(infoVale.ip_solicitante);
                    if (valeAux == null) //No ha hecho una solicitud de vale
                    {
                        log.Info("Ya validó que no hay vale creado");
                        ///Validar la ubicación del solicitante
                        var blnValidacion = infoVale.Latitude != null ? blnValidarUbicacionUsuarioVale(float.Parse(infoVale.Latitude), float.Parse(infoVale.Longitude)) : true;
                        if (blnValidacion)
                        {
                            if (lstTiendasVale.Find(x => x.Id == infoVale.Tienda) != null)
                            {

                                //Obtener código del vale
                                var client = new RestClient(string.Format("{0}/api/" + Const.SERVICIO_OBTENER_CODIGO_VALE, settings.UrlRestServices()));
                                var request = new RestRequest("/N/", Method.GET);

                                request.ReadWriteTimeout = 36000000;

                                var response = client.Get(request);
                                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    infoVale.Codigo = JsonConvert.DeserializeObject<string>(response.Content);
                                    log.Debug("Vale generado " + infoVale.Codigo);

                                    //
                                    //Grabar el vale con la infomación grabada por el usuario
                                    //
                                    //Valor por defecto
                                    DateTime dtFechaVencimiento = DateTime.Now.AddDays(settings.SConfig.DiasValidezVale);
                                    infoVale.Valor = settings.SConfig.DescuentoDefault;
                                    infoVale.Tipo = settings.SConfig.TipoDescuento;
                                    infoVale.Porcentaje = settings.SConfig.PorcentajeDefault;
                                    infoVale.Promocion = settings.SConfig.PromocionDefault;
                                    //Definiendo el tiempo de validez del vale
                                    infoVale.DiaHasta = settings.SConfig.TieneVencimiento.Equals("S") ? dtFechaVencimiento.Day : 0;
                                    infoVale.MesHasta = settings.SConfig.TieneVencimiento.Equals("S") ? dtFechaVencimiento.Month : 0;
                                    infoVale.AnioHasta = settings.SConfig.TieneVencimiento.Equals("S") ? dtFechaVencimiento.Year : 0;

                                    infoVale.Nombres = infoVale.Nombres.Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú","u")+"-";
                                    infoVale.Apellidos = infoVale.Apellidos.Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u");

                                    #region "Grabar Vale"
                                    WebRequest GuardarRequest = WebRequest.Create(string.Format("{0}/api/" + Const.SERVICIO_GRABAR_VALE, settings.UrlRestServices()));

                                    string json = JsonConvert.SerializeObject(infoVale);
                                    byte[] data = Encoding.ASCII.GetBytes(json);

                                    GuardarRequest.Method = "POST";
                                    GuardarRequest.ContentType = "application/json";
                                    GuardarRequest.ContentLength = data.Length;

                                    using (var stream = GuardarRequest.GetRequestStream())
                                    {
                                        stream.Write(data, 0, data.Length);
                                    }

                                    var GuardarResponse = (HttpWebResponse)GuardarRequest.GetResponse();
                                    #endregion
                                    if (GuardarResponse.StatusCode == HttpStatusCode.OK)
                                    {

                                        var report = new Rotativa.AspNetCore.ViewAsPdf("../Mercadeo/ResultadoVale", infoVale)
                                        {
                                            PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                                            PageSize = Rotativa.AspNetCore.Options.Size.A5,
                                        };
                                        
                                        EnviarEmail(infoVale);
                                        return report;
                                       
                                    }
                                    else
                                        ViewBag.ErrorMessage = "¡El vale no se pudo generar!";
                                    return View("Error");
                                }
                                else
                                {
                                    ViewBag.ErrorMessage = "¡El vale no se pudo generar!";
                                    log.Error("Vale no generado " + response.ErrorMessage);
                                    return View("Error");
                                }
                            }
                            else
                            {
                                //Tienda no es de las que esta en la promoción
                                ViewBag.ErrorMessage = "¡Gracias por participar! Pero esa tienda no se encuentra con Vale de descuento.";
                                return View("Error");
                            }
                        }
                        else
                        {
                            //Mostrar el vale anteriormente guardado
                            ViewBag.ErrorMessage = "¡Gracias por participar! Pero no se encuentra en el perímetro cercano a las tiendas que tienen esta promoción.";
                            return View("Error");
                        }
                    }
                    else
                    {
                        //Mostrar el vale anteriormente guardado
                        appSettings settings1 = new appSettings();
                        List<Tiendas> lstTiendas1 = GetTiendas();
                        ViewData["Message"] = "Usted ya ha generado un vale previamente. No se generó uno nuevo.";
                        valeAux.Tienda = lstTiendas1.Find(x => x.Tienda == valeAux.Tienda).Nombre;
                        var report = new Rotativa.AspNetCore.ViewAsPdf("../Mercadeo/ResultadoVale", valeAux)
                        {
                            PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                            PageSize = Rotativa.AspNetCore.Options.Size.A5
                        };
                        return report;
                        
                    }
                }
                

            }

            List<Tiendas> lstTiendas = GetTiendas();
            ViewData["Tiendas"] = lstTiendas.AsEnumerable();
            return View("../Mercadeo/IngresoVale", infoVale);

        }


        
        [HttpGet]
        ///Tests: IngresoVale/Vale/YSOBHWY2
        public IActionResult Vale(string codigoVale)
        {
            if (codigoVale != null)
            {

                Vale infoVale = new Vale();
                //obtener la información del vale
                //********
                //
                appSettings settings = new appSettings();
                var client = new RestClient(string.Format("{0}/api/" + Const.SERVICIO_OBTENER_INFO_VALE, settings.UrlRestServices()));
                var request = new RestRequest("/"+codigoVale+"/", Method.GET);

                request.ReadWriteTimeout = 36000000;

                var response = client.Get(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    infoVale = JsonConvert.DeserializeObject<Vale>(response.Content);
                    appSettings settings1 = new appSettings();
                    List<Tiendas> lstTiendas1 = GetTiendas();
                    ViewData["Message"] = "Usted ya ha generado un vale previamente. No se generó uno nuevo.";
                    infoVale.Tienda = lstTiendas1.Find(x => x.Tienda == infoVale.Tienda).Nombre;
                    
                    var report = new Rotativa.AspNetCore.ViewAsPdf("../Mercadeo/ResultadoVale", infoVale)
                    {
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                            PageSize = Rotativa.AspNetCore.Options.Size.A5
                        };
                    return report;
                }
                else
                {
                    ViewBag.ErrorMessage = "Código de Vale no existe.";
                    return View("Error");
                }
               
            } else
            {
                //Obtener las tiendas
                //******
                appSettings settings = new appSettings();
                List<Tiendas> lstTiendas = GetTiendas();
                ViewData["Tiendas"] = lstTiendas.AsEnumerable();
                List<string> TiendasVale = settings.GetTiendasVale();
                //para validar las tiendas de la promoción
                ViewData["TiendasVale"] = TiendasVale;
                return View("../Mercadeo/IngresoVale");
            }
        }

        private string CuerpoCorreo(Vale vale)
        {
            string strCuerpo = string.Empty;
            appSettings settings = new appSettings();
            strCuerpo = "<head>  <style> img { max-width: 100%; height: auto; } .table1 { border-collapse: separate; }  .container { position: relative; text-align: center; color: white; }"
                + "  .bottom-left { position: absolute; bottom: 220px; color: black; left: 26px; font-family: Arial, Helvetica, sans-serif; }  "
                + ".top-left { position: absolute; top: 25px; left: 26px; font-family: Arial, Helvetica, sans-serif; color: black; }  .top-right " +
                "{ position: absolute; top: 95px; right: 16px; font-family: Arial, Helvetica, sans-serif; color: black; } " +
                ".bottom-right { position: absolute; bottom: 20px; right: 16px; font-family: Arial, Helvetica, sans-serif; } " +
                ".centered { position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); } " +
                "#rcorners2 { border-radius: 25px; border: 1px solid red; padding: 8px;  } " +
                ".spacer { margin: 15px; } </style> </head>  <body>  <div class='contentform'>" +
                "<div class='row'> <div class='col-sm-6'> <img  src =cid:Logo_vale " +
                "width='50%' height='50%'><br/><br/> Estimado: " + vale.Nombres.ToUpper().Replace("-","") + " " + vale.Apellidos.ToUpper() + "<br/><br/><div align='center'> Gracias por preferirnos <a href='"+settings.SConfig.UrlExtServices + "/" + vale.Codigo+ "'> Haga click aqui para acceder al vale de descuento generado. </a></div> <br/>"
                + "</body>";
            return strCuerpo;
        }
        private void EnviarEmail(Vale vale)
        {
            try
            {
                appSettings settings = new appSettings();
                WebRequest MailRequest = WebRequest.Create(string.Format("{0}/api/" + Const.SERVICIO_ENVIAR_CORREO, settings.UrlRestServices()));
                StringBuilder mCuerpoCorreo = new StringBuilder();
                string mBody = "";
                //armar el cuerpo del correo
                mBody = CuerpoCorreo(vale);

                mCuerpoCorreo.Append(mBody);
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "");
                path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Replace(@"bin\Release\netcoreapp2.1\", ""));
                List<ImagenAdjunta> imagenAdjuntas = new List<ImagenAdjunta>();
                imagenAdjuntas.Add(new ImagenAdjunta(path + "wwwroot\\images\\giftcard.jpg", MediaTypeNames.Image.Jpeg, "Background_vale"));
                imagenAdjuntas.Add(new ImagenAdjunta(path + "wwwroot\\images\\LogoMF.png", MediaTypeNames.Image.Jpeg, "Logo_vale"));
                List<string> mCuentaCorreo = new List<string>();
                mCuentaCorreo.Add(vale.Email);

                EnviarEmail(Const.EMAIL_USR+"@mueblesfiesta.com", mCuentaCorreo,"MUEBLES FIESTA PROMOCIONES",mCuerpoCorreo,"MUEBLES FIESTA",imagenAdjuntas,"patricia.rodriguez@mueblesfiesta.com");
            }
            catch (Exception ex)
            {
                log.Error("Problema al enviar el correo ",ex);
            }
        }


        /// <summary>
        /// Método para obtener las tiendas activas
        /// </summary>
        /// <returns></returns>
        private List<Tiendas> GetTiendas()
        {
            appSettings settings = new appSettings();
            var client = new RestClient(string.Format("{0}/api/Tiendas", settings.UrlRestServices()));
            var request = new RestRequest("", Method.GET);

            request.ReadWriteTimeout = 36000000;

            var response = client.Get(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var lstTiendas = JsonConvert.DeserializeObject<IEnumerable<Tiendas>>(response.Content);
                Tiendas F01 = lstTiendas.ToList<Tiendas>().Find(x => x.Tienda == "F01");
                List <Tiendas> Lst= lstTiendas.ToList();
                Lst.Remove(F01);
                return Lst;
            }
            return null;
        }

        /// <summary>
        /// Método interno para verificar si un usuario no ha grabado ya un vale
        /// </summary>
        /// <param name="ipSolicitante"></param>
        /// <returns></returns>
        private Vale BuscarValePorIP(string ipSolicitante)
        {
            appSettings settings = new appSettings();
            var client = new RestClient(string.Format("{0}/api/IPVale", settings.UrlRestServices()));
            var request = new RestRequest("/GEt?Ip="+ipSolicitante, Method.GET);

            request.ReadWriteTimeout = 36000000;

            var response = client.Get(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var Vale = JsonConvert.DeserializeObject<Vale>(response.Content);
                return Vale;
            }
            //si no hay ningún vale con esa ip, responderá nulo
            return null;
        }

        /// <summary>
        /// Método para validar con los servicios de Google, la ubicación
        /// actual del usuario, para no permitir que guarden vales desde lugares
        /// donde no han visto la promoción
        /// Este método devolverá true si la llave API KEY de google está vacia
        /// Es decir, si está vacía, se omite esta validación
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lng"></param>
        /// <returns></returns>
        private bool blnValidarUbicacionUsuarioVale(float lat, float lng)
        {
            try
            {
               
                if (Const.GOOGLE_API_KEY.Length > 0)
                {
                    appSettings settings = new appSettings();

                    //lista de los nombres de las tiendas que tienen vale de descuento
                    var TiendasVale = settings.GetTiendasVale();

                    //conjunto de todas las tiendas
                    var TiendasTodas = GetTiendas();


                    //Proceso para seleccionar las tiendas restringidas por el vale de descuento
                    // y tomar la informacion adicional que viene en el servicio GetTiendas()

                    List<Tiendas> TiendasValeInfo = new List<Tiendas>();
                    foreach (Tiendas tienda in TiendasTodas)
                    {
                       tienda.Nombre= tienda.Nombre.Replace("á", "a");
                        tienda.Nombre = tienda.Nombre.Replace("é", "e");
                        tienda.Nombre = tienda.Nombre.Replace("í", "i");
                        tienda.Nombre = tienda.Nombre.Replace("ó", "o");
                        tienda.Nombre = tienda.Nombre.Replace("ú", "u");
                        tienda.Nombre = tienda.Nombre.Replace("ü","u");
                        foreach (string tiendaVale in TiendasVale)
                            if (tienda.Nombre.Equals(tiendaVale))
                                TiendasValeInfo.Add(tienda);
                    }

                    var client = new RestClient(Const.SERVICIO_OBTENER_UBICACION_USUARIO);
                    var request = new RestRequest(string.Format("?latlng={0},{1}&sensor=true&key={2}", lat, lng, Const.GOOGLE_API_KEY), Method.GET);

                    request.ReadWriteTimeout = 36000000;

                    var response = client.Get(request);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        GeolocatioinResults results = JsonConvert.DeserializeObject<GeolocatioinResults>(response.Content);
                        if (results != null)
                            foreach (var item in results.Results)
                                if (item.AddressComponents.Count()>0)
                                {
                                    foreach (var component in item.AddressComponents)
                                    {
                                        component.LongName = component.LongName.Replace("á", "a");
                                        component.LongName = component.LongName.Replace("é", "e");
                                        component.LongName = component.LongName.Replace("í", "i");
                                        component.LongName = component.LongName.Replace("ó", "o");
                                        component.LongName = component.LongName.Replace("ú", "u");
                                        component.LongName = component.LongName.Replace("ü", "u");
                                        if (TiendasValeInfo.Find(x => x.Departamento.ToUpper().Contains(component.LongName.ToUpper())) != null
                                            && component.Types[0]!="country")
                                        {
                                            return true;
                                        }
                                    }
                                }

                        return false;
                    }
                }
                else
                    return true;
            } catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public bool EnviarEmail(string Remitente, List<string> CuentaCorreo, string Asunto, StringBuilder Mensaje, string DisplayName, List<ImagenAdjunta> lstImagenes = null, string BccTo = null, Attachment adjuntos = null)
        {
            string mBitacora = "";
            string mSlack = "";


            SmtpClient smtp = new SmtpClient();
            MailMessage mail = new MailMessage();
            #region "tratamiento de imagenes"
            if (lstImagenes != null)
            {
                //crear una vista alternativa a partir del mensaje en HTML
                AlternateView av1 = AlternateView.CreateAlternateViewFromString(
                Mensaje.ToString(),
                null, MediaTypeNames.Text.Html);

                //now add the AlternateView

                foreach (var img in lstImagenes)
                    av1.LinkedResources.Add(img);

                //now append it to the body of the mail
                mail.AlternateViews.Add(av1);
                mail.IsBodyHtml = true;
            }
            #endregion


            //archivos adjuntos
            if (adjuntos != null)
                mail.Attachments.Add(adjuntos);

            //-------------------------------------------------------------------------------------------
            //Agregar la lista de correo
            foreach (var item in CuentaCorreo)
            {
                mail.To.Add(item);
            }
            //bcc
            if (BccTo != null)
                mail.Bcc.Add(BccTo);


            

            //-------------------------------------------------------------------------------------------
            //Establecer el remitente y el nombre a mostrar
            mail.From = new MailAddress(Remitente, DisplayName);

            //-------------------------------------------------------------------------------------------
            //Establecer a quien se le responerá
            mail.ReplyToList.Add(Remitente);

            //-------------------------------------------------------------------------------------------
            //Establecer el Asunto del correo
            mail.Subject = Asunto;

            //-------------------------------------------------------------------------------------------
            //Establecer el cuerpo del mensaje como html
            mail.IsBodyHtml = true;
            mail.Body = Mensaje.ToString();

            //-------------------------------------------------------------------------------------------
            //Establecer el host y puerto del servidor de correo
            smtp.Host = Const.EMAIL_HOST;
            smtp.Port = Int32.Parse(Const.EMAIL_PORT);

            smtp.UseDefaultCredentials = false;
            //-------------------------------------------------------------------------------------------
            //Habilitar el protocolo SSL y el metodo de entrega
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

            mBitacora = string.Format(" {0}; {1}; {2}; {3}; ", "Correo", Remitente, string.Join(",", CuentaCorreo), Asunto);
            mSlack = string.Format("Clase: {0} \nRemitente:{1} \nLista de correo:{2} \nAsunto:{3} ", this.GetType().FullName, Remitente, string.Join(",", CuentaCorreo), Asunto);
            smtp.Timeout = 5000;
            //-------------------------------------------------------------------------------------------
            //Se envia el correo, tras tres intentos fallidos envia por slack un mensaje

            try
            {//Primer intento para enviar correo
                smtp.Credentials = new System.Net.NetworkCredential(Const.EMAIL_USR, Const.EMAIL_PWD);
                smtp.Send(mail);
                log.Info(mBitacora);
            }
            catch(Exception ex)
            {
                log.Error("No se pudo enviar el correo con el vale",ex.InnerException);
            }

            return true;
        }

        public async Task<RecaptchaResponse> Validate(HttpRequest request, bool antiForgery = true)
        {
            //if (!request.Form.ContainsKey("g-recaptcha-response")) // error if no reason to do anything, this is to alert developers they are calling it without reason.
            //    throw new ValidationException("Google recaptcha response not found in form. Did you forget to include it?");
            HttpClient Client = new HttpClient();
            reCaSettings caSettings = new reCaSettings();
            var response = request.Form["g-recaptcha-response"];
            var result = await Client.GetStringAsync(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", caSettings.SecretKey, response));
            var captchaResponse = JsonConvert.DeserializeObject<RecaptchaResponse>(result);

            return captchaResponse;
        }
    }
}