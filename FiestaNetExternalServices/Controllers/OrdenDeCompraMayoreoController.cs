﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FiestaNetExternalServices.Models;
using MF_Clases;
using MF_Clases.GuateFacturas.FEL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace FiestaNetExternalServices.Controllers
{
   
   [Route("OrdenDeCompraMayoreo")]
    public class OrdenDeCompraMayoreoController : Controller
    {
        /// <summary>
        /// Front End
        /// </summary>
        /// <param name="OrdenDeCompra"></param>
        /// <param name="Fecha"></param>
        /// <param name="Cliente"></param>
        /// <returns></returns>


        [HttpGet]
        public IActionResult Get()
        {
            return View("../Facturacion/OrdenDeCompraMayoreo", new ConsultaFacturaMayoreo());
        }

        


    }
}