﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FiestaNetExternalServices.Models;
using MF_Clases;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace FiestaNetExternalServices.Controllers
{
   
    [ApiController]
    public class ValidarExistenciasController : ControllerBase
    {
       
       
            private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Servicio para válidar la existencia de un artículo
        /// para la compra en línea
        /// </summary>
        /// <param name="CodArticulo"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/ValidarExistencias/{CodArticulo}")]
        public ActionResult Get(string CodArticulo)
            {
                log.Debug("validando existencias");
            Models.appSettings appsettings = new Models.appSettings();
            var client = new RestClient(string.Format("{0}", appsettings.UrlRestServices()));
            var request = new RestRequest("/api/ValidarExistencias?CodArticulo="+CodArticulo, Method.GET);
            dtoArticulo infoResp = new dtoArticulo();
            request.ReadWriteTimeout = 36000000;
            
            var response = client.Get(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                infoResp = JsonConvert.DeserializeObject<MF_Clases.dtoArticulo>(response.Content);
                return Ok(infoResp);
            }
            return BadRequest(response.ErrorMessage);
        }


        
    }
}