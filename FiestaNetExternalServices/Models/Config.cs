﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FiestaNetExternalServices.Models
{
    public class Config
    {
        public string Ambiente { get; set; }
        public string TipoDescuento { get; set; }
        public int DiasValidezVale { get; set; }
        public decimal DescuentoDefault { get; set; }
        public decimal PorcentajeDefault { get; set; }
        public string PromocionDefault { get; set; }
        public string TieneVencimiento { get; set; }
        public string ValidarUbicacionVale { get; set; }
        public string UrlExtServices { get; set; }

        public List<Values> PortalEnvironments { get; set; }
        public List<Values> RestServicesEnvironments { get; set; }
        public List<Values> ValoresDescuento { get; set; }
        public List<Values> TiendasVale { get; set; }
        public List<Values> CalendarioTareas { get; set; }
        /// <summary>
        /// Devuelve el valor de la URL según el ambiente dado
        /// </summary>
        /// <param name="Ambiente"></param>
        /// <returns></returns>
        public string getRestUrl(string Ambiente)
        {
            return RestServicesEnvironments.Find(x => x.Id.Equals(Ambiente)).Value;
        }
        
    }
    public class Values
    {
        public string Id { get; set; }
        public string Value { get; set; }
    }
}
