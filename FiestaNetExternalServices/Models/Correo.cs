﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace FiestaNetExternalServices.Models
{
    public class Correo
    {
        public List<string> Destinatarios { get; set; }
        public string Remitente { get; set; }
        public List<string> ResponderA { get; set; }
        public StringBuilder CuerpoCorreo { get; set; }
        public string Asunto { get; set; }
        public string NombreAMostrar { get; set; } //DisplayName
        public List<ImagenAdjunta> lstImagenes { get; set; }
        public string ConCopiaOcultaA { get; set; }
       
    }

    public class ImagenAdjunta : LinkedResource
    {

        public ImagenAdjunta(string fileName, string mediaType, string _ContentId) : base(fileName, mediaType)
        {
            this.ContentId = _ContentId;
        }
    }
}
