﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FiestaNetExternalServices.Models
{
    public class Tiendas
    {
        public string Tienda { get; set; }
        public decimal Valor { get; set; }
        public decimal MontoComisionable { get; set; }
        public decimal ValorNeto { get; set; }
        public decimal Comision { get; set; }
        public string Titulo { get; set; }
        public string Nombre { get; set; }
        public string Departamento { get; set; }
        public string Municipio { get; set; }
    }
}
