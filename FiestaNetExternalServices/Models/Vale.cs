﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FiestaNetExternalServices.Models
{
    public class Vale
    {
        public string Codigo { get; set; }
        [Required]
        [StringLength(100)]
        public string Nombres { get; set; }
        [Required]
        [StringLength(100)]
        public string Apellidos { get; set; }
        [Required]
        public string Tienda { get; set; }
        [Required]
        public string Genero { get; set; }
        [Required]
        [StringLength(100)]
        public string Email { get; set; }
        [Required]
        [Range(1, 99)]
        public int Edad { get; set; }
        [Required]
        [StringLength(50)]
        public string Identificacion { get; set; }
        [Required]
        [StringLength(200)]
        public string Direccion { get; set; }
        [Required]
        [StringLength(100)]
        public string Telefono { get; set; }
        public string Tipo { get; set; }
        public decimal Valor { get; set; }
        public decimal Porcentaje { get; set; }
        public int DiaHasta { get; set; }
        public int MesHasta { get; set; }
        public int AnioHasta { get; set; }
        public string Promocion { get; set; }
       /// <summary>
       /// Seguridad
       /// </summary>
        public string ip_solicitante { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        
        public Vale()
        {
            
        }
    }
}
