﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FiestaNetExternalServices.Models
{
    /// <summary>
    /// Clase para obtener la configuración general, del 
    /// archivo appSettings.json
    /// </summary>
    public class appSettings
    {
        public Config SConfig { get; set; }

        public appSettings()
        {
            try
            {
                
                ///--------------NO debe usar tildes en lo que ingrese en el appsettings.json-----------------------
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "appsettings.json");

                if (File.Exists(path))
                {
                    string obj = JObject.Parse(File.ReadAllText(path)).ToString();
                    SConfig = JsonConvert.DeserializeObject<Config>(obj);

                }
                else
                {
                    try
                    {
                        path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Replace(@"bin\Release\netcoreapp2.1\", "appsettings.json"));
                        if (File.Exists(path))
                        {
                            string obj = JObject.Parse(File.ReadAllText(path)).ToString();
                            SConfig = JsonConvert.DeserializeObject<Config>(obj);

                        }
                    }
                    catch
                    { }
                }
            }
            catch (Exception e)
            {
                try
                {
                    string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Replace(@"bin\Release\netcoreapp2.1\", "appsettings.json"));
                    if (File.Exists(path))
                    {
                        string obj = JObject.Parse(File.ReadAllText(path)).ToString();
                        SConfig = JsonConvert.DeserializeObject<Config>(obj);

                    }
                }
                catch
                { }
            }
        }

        public string UrlRestServices()
        {
           return SConfig.RestServicesEnvironments.Find(x => x.Id == SConfig.Ambiente).Value;
        }
        public string UrlPortalEnviromnents()
        {
            return SConfig.PortalEnvironments.Find(x => x.Id == SConfig.Ambiente).Value;
        }
        public List<string> GetTiendasVale()
        {
            List<string> LstTiendas = new List<string>();
            foreach (var item in SConfig.TiendasVale)
                LstTiendas.Add(item.Value);
            return LstTiendas;
        }
    }
}
