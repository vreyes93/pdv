﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FiestaNetExternalServices.Code.Cron;

namespace FiestaNetExternalServices.Code.Scheduling
{
    public class SchedulerHostedService : HostedService
    {
        public event EventHandler<UnobservedTaskExceptionEventArgs> UnobservedTaskException;
        bool i = true;
        private readonly List<SchedulerTaskWrapper> _scheduledTasks = new List<SchedulerTaskWrapper>();

        public SchedulerHostedService(IEnumerable<IScheduledTask> scheduledTasks)
        {
            var referenceTime = DateTime.UtcNow.AddHours(-6);//GTM-6 
            
            foreach (var scheduledTask in scheduledTasks)
            {
                _scheduledTasks.Add(new SchedulerTaskWrapper
                {
                    Schedule = CrontabSchedule.Parse(scheduledTask.Schedule),
                    Task = scheduledTask,
                    NextRunTime = referenceTime
                     , IsFirstTime = true
                });
            }
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
               
                    await ExecuteOnceAsync(cancellationToken);
                
                await Task.Delay(TimeSpan.FromMinutes(1), cancellationToken);
            }
        }
        
        private async Task ExecuteOnceAsync(CancellationToken cancellationToken)
        {
            var taskFactory = new TaskFactory(TaskScheduler.Current);
            var referenceTime = DateTime.UtcNow.AddHours(-6);///GTM-6
            
            var tasksThatShouldRun = _scheduledTasks.Where(t => t.ShouldRun(referenceTime)).ToList();

            foreach (var taskThatShouldRun in tasksThatShouldRun)
            {
                
                taskThatShouldRun.Increment();
                if (!taskThatShouldRun.IsFirstTime)
                {


                    await taskFactory.StartNew(
                        async () =>
                        {
                            try
                            {
                                await taskThatShouldRun.Task.ExecuteAsync(cancellationToken);
                            }
                            catch (Exception ex)
                            {
                                var args = new UnobservedTaskExceptionEventArgs(
                                    ex as AggregateException ?? new AggregateException(ex));

                                UnobservedTaskException?.Invoke(this, args);

                                if (!args.Observed)
                                {
                                    throw;
                                }
                            }
                        },
                        cancellationToken);
                }
                else
                    taskThatShouldRun.IsFirstTime = false;
                
            }
        }

        private class SchedulerTaskWrapper
        {
            public CrontabSchedule Schedule { get; set; }
            public IScheduledTask Task { get; set; }
            public bool IsFirstTime { get; set; }
            public DateTime LastRunTime { get; set; }
            public DateTime NextRunTime { get; set; }

            public void Increment()
            {
                LastRunTime = NextRunTime;
                NextRunTime = Schedule.GetNextOccurrence(NextRunTime);
            }

            public bool ShouldRun(DateTime currentTime)
            {
                var blnResult = NextRunTime < currentTime && LastRunTime != NextRunTime ;

                return blnResult;
            }
        }
    }
}