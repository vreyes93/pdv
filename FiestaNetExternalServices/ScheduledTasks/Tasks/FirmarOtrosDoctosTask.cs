﻿using FiestaNetExternalServices.Code;
using FiestaNetExternalServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace FiestaNetExternalServices.ScheduledTasks.Tasks
{
    public class FirmarOtrosDoctosTask : IScheduledTask
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public string Schedule
        {
            ///////////////////////////////////////////////////
            ///
            //Programación de tarea se ejecutará cada 5 minutos
            //el Schedule debe devolver algo asi: "0/5 * * * *";
            //                         [minuto] [hora] [dia] [mes] [dia de la semana]
            //son 5 campos
            //referencia de formato (formato unicamente) http://www.quartz-scheduler.org/documentation/quartz-2.x/tutorials/crontrigger.html#format
            //
            //////////////////////////////////////////////////
            get
            {
                appSettings settings = new appSettings();
                return settings.SConfig.CalendarioTareas.Find(x => x.Id == Const.SCHEDULING_FIRMAR_OTROS_DOCTOS).Value;
            }
        }

        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            log.Debug("COMENZANDO LA TAREA DE  FIRMAR OTROS DOCTOS FEL");
            appSettings settings = new appSettings();
            string URLNotificacionService = settings.SConfig.getRestUrl(settings.SConfig.Ambiente) + Const.SERVICIO_FIRMAR_OTROS_DOCUMENTOS_ELECTRONICOS_FEL;
            var httpClient = new HttpClient();
            await httpClient.GetStringAsync(URLNotificacionService);
        }
    }
}
