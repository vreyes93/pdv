﻿using FiestaNetExternalServices.Code;
using FiestaNetExternalServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace FiestaNetExternalServices.ScheduledTasks.Tasks
{
    public class NotificarContingenciasUtilizadasTask : IScheduledTask
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string Schedule
        {
            ///////////////////////////////////////////////////
            ///
            //Programación de tarea se ejecutará cada 5 minutos
            //el Schedule debe devolver algo asi: "0/5 * * * *";
            //                         [minuto] [hora] [dia] [mes] [dia de la semana]
            //son 5 campos
            //referencia de formato (formato unicamente) http://www.quartz-scheduler.org/documentation/quartz-2.x/tutorials/crontrigger.html#format
            //
            //////////////////////////////////////////////////
            get
            {
                appSettings settings = new appSettings();
                var schd= settings.SConfig.CalendarioTareas.Find(x => x.Id == Const.SCHEDULING_NOTIFICAR_CONTINGENCIAS_UTILIZADAS).Value;
                return schd;
            }
        }

        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            log.Debug("COMENZANDO LA TAREA DE NOTIFICAR CONTINGENCIAS UTILIZADAS");
            appSettings settings = new appSettings();
            string URLNotificacionService = settings.SConfig.getRestUrl(settings.SConfig.Ambiente) + Const.SERVICIO_NOTIFICAR_CONTINGENCIAS_UTILIZADAS_FEL;
            var httpClient = new HttpClient();
            await httpClient.GetStringAsync(URLNotificacionService);
        }
    }
}
