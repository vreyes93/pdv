﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using DevExpress.AspNetCore;
using DevExpress.AspNetCore.Bootstrap;
using log4net.Repository;
using log4net.Config;
using log4net;
using System.IO;
using System.Reflection;
using Rotativa.AspNetCore;
using reCAPTCHA.AspNetCore;
using FiestaNetExternalServices.Code.Scheduling;
using FiestaNetExternalServices.Code;
using FiestaNetExternalServices.ScheduledTasks.Tasks;

namespace FiestaNetExternalServices
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;   
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<RecaptchaSettings>(Configuration.GetSection("RecaptchaSettings"));
            services.AddTransient<IRecaptchaService, RecaptchaService>();

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDevExpressControls(options => {
                options.Bootstrap(bootstrap => {
                    bootstrap.Mode = BootstrapMode.Bootstrap3;
                });
                options.Resources =  ResourcesType.ThirdParty;
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddMemoryCache(); // Adds a default in-memory implementation of 
                                       // IDistributedCache

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(20);
            });

            ILoggerRepository logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromSeconds(10);
                options.Cookie.HttpOnly = true;
            });

            ///---
            ///Servicio para firmar las notas de crédito y débito creadas en exactus
            //----
            services.AddSingleton<IScheduledTask, FirmarOtrosDoctosTask>();
            ///---
            ///Servicio para validar los números de acceso o contingencia para cada tienda Y obtener más si se necesitaran
            ///---
            services.AddSingleton<IScheduledTask, LoteDeContingenciaTask>();
            ///---
            ///Servicio para notificar a la conta sobre las contingencias utilizadas para ingresarlas al portal de la SAT
            ///---
            services.AddSingleton<IScheduledTask, NotificarContingenciasUtilizadasTask>();
            ///---
            services.AddScheduler((sender, args) =>
            {
                Console.Write(args.Exception.Message);
                args.SetObserved();
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseDevExpressControls();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseSession();

            app.UseMvc(routes =>
            {
                //routes.MapRoute(
                //    name: "default",
                //    template: "{controller=Home}/{action=Vale}/{id?}");
                routes.MapRoute(
                  name: "pvConsulta",
                    template: "{controller=Consulta}/{action=Index}");
                routes.MapRoute(
            name: "Vale",
              template: "{controller=IngresoVale}/{action=Vale}/{CodigoVale?}");
                routes.MapRoute(
                name: "OrdenDeCompra",
                template: "{controller=OrdenDeCompraMayoreo}/{action=OrdenDeCompraMayoreo}/{OrdenDeCompra?}");
                routes.MapRoute(
              name: "Conta",
                template: "{controller=DocumentosConta}/{action=DocumentosConta}");
                routes.MapRoute(
              name: "pvConta",
                template: "{controller=DocumentosConta}/{action=BindGrid}");
                


            });
           
            RotativaConfiguration.Setup(env);
        }
    }
}
