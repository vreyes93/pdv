#pragma checksum "C:\desarrollo\migracion\pdv-migracion\Desarrollo\FiestaNetExternalServices\Views\Facturacion\pvOrdenesDeCompra.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "dc8c7c9e373ccab08d1f2bcfdbc4e6db07a0d94b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Facturacion_pvOrdenesDeCompra), @"mvc.1.0.view", @"/Views/Facturacion/pvOrdenesDeCompra.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Facturacion/pvOrdenesDeCompra.cshtml", typeof(AspNetCore.Views_Facturacion_pvOrdenesDeCompra))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\desarrollo\migracion\pdv-migracion\Desarrollo\FiestaNetExternalServices\Views\_ViewImports.cshtml"
using FiestaNetExternalServices;

#line default
#line hidden
#line 2 "C:\desarrollo\migracion\pdv-migracion\Desarrollo\FiestaNetExternalServices\Views\_ViewImports.cshtml"
using FiestaNetExternalServices.Models;

#line default
#line hidden
#line 3 "C:\desarrollo\migracion\pdv-migracion\Desarrollo\FiestaNetExternalServices\Views\_ViewImports.cshtml"
using DevExpress.AspNetCore;

#line default
#line hidden
#line 4 "C:\desarrollo\migracion\pdv-migracion\Desarrollo\FiestaNetExternalServices\Views\_ViewImports.cshtml"
using DevExpress.AspNetCore.Bootstrap;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"dc8c7c9e373ccab08d1f2bcfdbc4e6db07a0d94b", @"/Views/Facturacion/pvOrdenesDeCompra.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"27b09c15056d937d27a407bb38552cc9ad077f2f", @"/Views/_ViewImports.cshtml")]
    public class Views_Facturacion_pvOrdenesDeCompra : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<MF_Clases.FacturaMayoreo>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(46, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(50, 1645, false);
#line 3 "C:\desarrollo\migracion\pdv-migracion\Desarrollo\FiestaNetExternalServices\Views\Facturacion\pvOrdenesDeCompra.cshtml"
Write(Html.DevExpress()
                    .BootstrapGridView<MF_Clases.FacturaMayoreo>("OrdenDeCompra")
                    //.KeyFieldName(m => m.ProductID)
                    //.KeyFieldName("Factura")
                    //.SettingsBehavior(settings => settings.AllowEllipsisInText(true))
                    .SettingsResizing(settings => settings.ColumnResizeMode(ColumnResizeMode.Control))
                    .Columns(columns =>
                    {
                        columns.Add(m => m.Factura).Width(70);
                        columns.Add(m => m.Pedido).Width(70);
                        columns.Add(m => m.NombreCliente).Width(150);
                        columns.Add(m => m.FechaPedido).Caption("Fecha").Width(50);
                        columns.Add(m => m.OrdenDeCompra).Caption("Orden de Compra").Width(350);
                        columns.AddHyperLinkColumn(m => m.linkFactura).Width(350);
            //columns.Add()
            //    .FieldName("UnitPrice");
            //columns.Add()
            //    .FieldName("UnitsOnOrder");
            //columns.AddTextColumn()
            //    .FieldName("Total")
            //    .UnboundType(UnboundColumnType.Decimal)
            //    .UnboundExpression("UnitsOnOrder * UnitPrice")
            //    .PropertiesTextEdit(properties => properties
            //        .DisplayFormatString("c"));
        })
                    //.Routes(routes => routes
                    //    .MapRoute(r => r
                    //        .Controller("OrdenDeCompraMayoreo")
                    //        .Action("Get")))
                    .Bind(Model));

#line default
#line hidden
            EndContext();
            BeginContext(1696, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<MF_Clases.FacturaMayoreo>> Html { get; private set; }
    }
}
#pragma warning restore 1591
