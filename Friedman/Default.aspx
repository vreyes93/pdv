﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Friedman.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            text-decoration: underline;
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table align="left">
        <tr>
            <td class="style1">
                <strong>MENU DE FRIEDMAN</strong></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnDesempenioSemanal" runat="server" 
                    Text="Resumen de Desempeño Semanal" onclick="btnDesempenioSemanal_Click" 
                    Width="303px" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnRegresar" runat="server" 
                    Text="Regresar" onclick="btnRegresar_Click" width="303px" />
            </td>
        </tr>
    </table>
</asp:Content>
