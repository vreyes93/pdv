﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DesempenioSemanal.aspx.cs" Inherits="Friedman.DesempenioSemanal" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style4
        {
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table align="left" id="tablaTitulos" runat="server">
        <tr>
            <td class="style4" colspan="4">
                <strong style="text-align: center; font-weight: bold;">RESUMEN DE DESEMPEÑO SEMANAL</strong></td>
        </tr>
        <tr>
            <td style="text-align: center" colspan="4">
                <asp:Label ID="lbInfo" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                Semana:</td>
            <td style="text-align: center">
                <asp:DropDownList ID="semana" runat="server">
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                    <asp:ListItem>32</asp:ListItem>
                    <asp:ListItem>33</asp:ListItem>
                    <asp:ListItem>34</asp:ListItem>
                    <asp:ListItem>35</asp:ListItem>
                    <asp:ListItem>36</asp:ListItem>
                    <asp:ListItem>37</asp:ListItem>
                    <asp:ListItem>38</asp:ListItem>
                    <asp:ListItem>39</asp:ListItem>
                    <asp:ListItem>40</asp:ListItem>
                    <asp:ListItem>41</asp:ListItem>
                    <asp:ListItem>42</asp:ListItem>
                    <asp:ListItem>43</asp:ListItem>
                    <asp:ListItem>44</asp:ListItem>
                    <asp:ListItem>45</asp:ListItem>
                    <asp:ListItem>46</asp:ListItem>
                    <asp:ListItem>47</asp:ListItem>
                    <asp:ListItem>48</asp:ListItem>
                    <asp:ListItem>49</asp:ListItem>
                    <asp:ListItem>50</asp:ListItem>
                    <asp:ListItem>51</asp:ListItem>
                    <asp:ListItem>52</asp:ListItem>
                    <asp:ListItem>53</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td style="text-align: center">
                <asp:DropDownList ID="anio" runat="server" Width="55px">
                    <asp:ListItem>2006</asp:ListItem>
                    <asp:ListItem>2007</asp:ListItem>
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                    <asp:ListItem>2011</asp:ListItem>
                    <asp:ListItem>2012</asp:ListItem>
                    <asp:ListItem>2013</asp:ListItem>
                    <asp:ListItem>2014</asp:ListItem>
                    <asp:ListItem>2015</asp:ListItem>
                    <asp:ListItem>2016</asp:ListItem>
                    <asp:ListItem>2017</asp:ListItem>
                    <asp:ListItem>2018</asp:ListItem>
                    <asp:ListItem>2019</asp:ListItem>
                    <asp:ListItem>2020</asp:ListItem>
                    <asp:ListItem>2021</asp:ListItem>
                    <asp:ListItem>2022</asp:ListItem>
                    <asp:ListItem>2023</asp:ListItem>
                    <asp:ListItem>2024</asp:ListItem>
                    <asp:ListItem>2025</asp:ListItem>
                    <asp:ListItem>2026</asp:ListItem>
                    <asp:ListItem>2027</asp:ListItem>
                    <asp:ListItem>2028</asp:ListItem>
                    <asp:ListItem>2029</asp:ListItem>
                    <asp:ListItem>2030</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td style="text-align: center">
                <asp:LinkButton ID="lkSeleccionar" runat="server" onclick="lkSeleccionar_Click">Seleccionar</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td style="text-align: center" colspan="2">
                Consulta Semanas:</td>
            <td style="text-align: center">
                <asp:TextBox ID="txtSemanas" runat="server" Width="45px"></asp:TextBox>
            </td>
            <td style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center" colspan="4">
                <asp:Label ID="lbError" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />

    <table align="left">
        <tr>
            <td>
                <asp:GridView ID="gridTiendas" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                    onrowdatabound="gridTiendas_RowDataBound">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="F02">
                            <HeaderTemplate>
                                 F-02<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF02Minimo" runat="server" Text='<%# Bind("F02Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF02Meta" runat="server" Text='<%# Bind("F02Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF02Minimo" runat="server" Text='<%# Bind("F02Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF02Meta" runat="server" Text='<%# Bind("F02Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpiF02" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F02Venta" HeaderText="F02Venta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF02Venta" runat="server" Text='<%# Bind("F02Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF02Venta" runat="server" Text='<%# Bind("F02Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F03" Visible="false">
                            <HeaderTemplate>
                                 F-03<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF03Minimo" runat="server" Text='<%# Bind("F03Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF03Meta" runat="server" Text='<%# Bind("F03Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF03Minimo" runat="server" Text='<%# Bind("F03Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF03Meta" runat="server" Text='<%# Bind("F03Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" Visible="false">
                            <ItemTemplate>
                                <asp:Image id="kpiF03" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F03Venta" HeaderText="F03Venta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF03Venta" runat="server" Text='<%# Bind("F03Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF03Venta" runat="server" Text='<%# Bind("F03Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F04">
                            <HeaderTemplate>
                                 F-04<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF04Minimo" runat="server" Text='<%# Bind("F04Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF04Meta" runat="server" Text='<%# Bind("F04Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF04Minimo" runat="server" Text='<%# Bind("F04Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF04Meta" runat="server" Text='<%# Bind("F04Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpiF04" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F04Venta" HeaderText="F04Venta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF04Venta" runat="server" Text='<%# Bind("F04Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF04Venta" runat="server" Text='<%# Bind("F04Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F05">
                            <HeaderTemplate>
                                 F-05<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF05Minimo" runat="server" Text='<%# Bind("F05Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF05Meta" runat="server" Text='<%# Bind("F05Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF05Minimo" runat="server" Text='<%# Bind("F05Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF05Meta" runat="server" Text='<%# Bind("F05Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpiF05" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F05Venta" HeaderText="F05Venta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF05Venta" runat="server" Text='<%# Bind("F05Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF05Venta" runat="server" Text='<%# Bind("F05Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F06" Visible="False">
                            <HeaderTemplate>
                                 F-06<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF06Minimo" runat="server" Text='<%# Bind("F06Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF06Meta" runat="server" Text='<%# Bind("F06Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF06Minimo" runat="server" Text='<%# Bind("F06Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF06Meta" runat="server" Text='<%# Bind("F06Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" Visible="False">
                            <ItemTemplate>
                                <asp:Image id="kpiF06" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F06Venta" HeaderText="F06Venta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF06Venta" runat="server" Text='<%# Bind("F06Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF06Venta" runat="server" Text='<%# Bind("F06Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F07">
                            <HeaderTemplate>
                                 F-07<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF07Minimo" runat="server" Text='<%# Bind("F07Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF07Meta" runat="server" Text='<%# Bind("F07Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF07Minimo" runat="server" Text='<%# Bind("F07Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF07Meta" runat="server" Text='<%# Bind("F07Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpiF07" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F07Venta" HeaderText="F07Venta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF07Venta" runat="server" Text='<%# Bind("F07Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF07Venta" runat="server" Text='<%# Bind("F07Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F08"  Visible="False">
                            <HeaderTemplate>
                                 F-08<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF08Minimo" runat="server" Text='<%# Bind("F08Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF08Meta" runat="server" Text='<%# Bind("F08Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF08Minimo" runat="server" Text='<%# Bind("F08Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF08Meta" runat="server" Text='<%# Bind("F08Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText=""  Visible="False">
                            <ItemTemplate>
                                <asp:Image id="kpiF08" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F08Venta" HeaderText="F08Venta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF08Venta" runat="server" Text='<%# Bind("F08Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF08Venta" runat="server" Text='<%# Bind("F08Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F09">
                            <HeaderTemplate>
                                 F-09<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF09Minimo" runat="server" Text='<%# Bind("F09Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF09Meta" runat="server" Text='<%# Bind("F09Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF09Minimo" runat="server" Text='<%# Bind("F09Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF09Meta" runat="server" Text='<%# Bind("F09Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpiF09" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F09Venta" HeaderText="F09Venta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF09Venta" runat="server" Text='<%# Bind("F09Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF09Venta" runat="server" Text='<%# Bind("F09Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F10" Visible="False">
                            <HeaderTemplate>
                                 F-10<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF10Minimo" runat="server" Text='<%# Bind("F10Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF10Meta" runat="server" Text='<%# Bind("F10Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF10Minimo" runat="server" Text='<%# Bind("F10Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF10Meta" runat="server" Text='<%# Bind("F10Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" Visible="False">
                            <ItemTemplate>
                                <asp:Image id="kpiF10" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F10Venta" HeaderText="F10Venta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF10Venta" runat="server" Text='<%# Bind("F10Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF10Venta" runat="server" Text='<%# Bind("F10Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F11">
                            <HeaderTemplate>
                                 F-11<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF11Minimo" runat="server" Text='<%# Bind("F11Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF11Meta" runat="server" Text='<%# Bind("F11Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF11Minimo" runat="server" Text='<%# Bind("F11Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF11Meta" runat="server" Text='<%# Bind("F11Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpiF11" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F11Venta" HeaderText="F11Venta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF11Venta" runat="server" Text='<%# Bind("F11Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF11Venta" runat="server" Text='<%# Bind("F11Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F12">
                            <HeaderTemplate>
                                 F-12<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF12Minimo" runat="server" Text='<%# Bind("F12Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF12Meta" runat="server" Text='<%# Bind("F12Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF12Minimo" runat="server" Text='<%# Bind("F12Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF12Meta" runat="server" Text='<%# Bind("F12Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpiF12" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F12Venta" HeaderText="F12Venta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF12Venta" runat="server" Text='<%# Bind("F12Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF12Venta" runat="server" Text='<%# Bind("F12Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F13" Visible="true">
                            <HeaderTemplate>
                                 F-13<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF13Minimo" runat="server" Text='<%# Bind("F13Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF13Meta" runat="server" Text='<%# Bind("F13Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF13Minimo" runat="server" Text='<%# Bind("F13Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF13Meta" runat="server" Text='<%# Bind("F13Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" Visible="true">
                            <ItemTemplate>
                                <asp:Image id="kpiF13" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F13Venta" HeaderText="F13Venta" Visible="false">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF13Venta" runat="server" Text='<%# Bind("F13Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF13Venta" runat="server" Text='<%# Bind("F13Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F14">
                            <HeaderTemplate>
                                 F-14<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF14Minimo" runat="server" Text='<%# Bind("F14Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF14Meta" runat="server" Text='<%# Bind("F14Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF14Minimo" runat="server" Text='<%# Bind("F14Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF14Meta" runat="server" Text='<%# Bind("F14Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpiF14" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F14Venta" HeaderText="F14Venta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF14Venta" runat="server" Text='<%# Bind("F14Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF14Venta" runat="server" Text='<%# Bind("F14Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F15">
                            <HeaderTemplate>
                                 F-15<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF15Minimo" runat="server" Text='<%# Bind("F15Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF15Meta" runat="server" Text='<%# Bind("F15Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF15Minimo" runat="server" Text='<%# Bind("F15Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF15Meta" runat="server" Text='<%# Bind("F15Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpiF15" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F15Venta" HeaderText="F15Venta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF15Venta" runat="server" Text='<%# Bind("F15Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF15Venta" runat="server" Text='<%# Bind("F15Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F16">
                            <HeaderTemplate>
                                 F-16<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF16Minimo" runat="server" Text='<%# Bind("F16Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF16Meta" runat="server" Text='<%# Bind("F16Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF16Minimo" runat="server" Text='<%# Bind("F16Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF16Meta" runat="server" Text='<%# Bind("F16Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpiF16" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F16Venta" HeaderText="F16Venta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF16Venta" runat="server" Text='<%# Bind("F16Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF16Venta" runat="server" Text='<%# Bind("F16Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F17" Visible="true">
                            <HeaderTemplate>
                                 F-17<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF17Minimo" runat="server" Text='<%# Bind("F17Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF17Meta" runat="server" Text='<%# Bind("F17Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF17Minimo" runat="server" Text='<%# Bind("F17Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF17Meta" runat="server" Text='<%# Bind("F17Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" Visible="true">
                            <ItemTemplate>
                                <asp:Image id="kpiF17" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F17Venta" HeaderText="F17Venta" Visible="false">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF17Venta" runat="server" Text='<%# Bind("F17Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF17Venta" runat="server" Text='<%# Bind("F17Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F18" Visible="false">
                            <HeaderTemplate>
                                 F-18<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF18Minimo" runat="server" Text='<%# Bind("F18Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF18Meta" runat="server" Text='<%# Bind("F18Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF18Minimo" runat="server" Text='<%# Bind("F18Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF18Meta" runat="server" Text='<%# Bind("F18Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" Visible="false">
                            <ItemTemplate>
                                <asp:Image id="kpiF18" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F18Venta" HeaderText="F18Venta" Visible="false">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF18Venta" runat="server" Text='<%# Bind("F18Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF18Venta" runat="server" Text='<%# Bind("F18Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F19" Visible="true">
                            <HeaderTemplate>
                                 F-19<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF19Minimo" runat="server" Text='<%# Bind("F19Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF19Meta" runat="server" Text='<%# Bind("F19Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF19Minimo" runat="server" Text='<%# Bind("F19Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF19Meta" runat="server" Text='<%# Bind("F19Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" Visible="true">
                            <ItemTemplate>
                                <asp:Image id="kpiF19" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F19Venta" HeaderText="F19Venta" Visible="false">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF19Venta" runat="server" Text='<%# Bind("F19Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF19Venta" runat="server" Text='<%# Bind("F19Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F20" Visible="true">
                            <HeaderTemplate>
                                 F-20<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF20Minimo" runat="server" Text='<%# Bind("F20Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF20Meta" runat="server" Text='<%# Bind("F20Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF20Minimo" runat="server" Text='<%# Bind("F20Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF20Meta" runat="server" Text='<%# Bind("F20Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" Visible="true">
                            <ItemTemplate>
                                <asp:Image id="kpiF20" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F20Venta" HeaderText="F20Venta" Visible="false">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF20Venta" runat="server" Text='<%# Bind("F20Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF20Venta" runat="server" Text='<%# Bind("F20Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F21" Visible="true">
                            <HeaderTemplate>
                                 F-21<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF21Minimo" runat="server" Text='<%# Bind("F21Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF21Meta" runat="server" Text='<%# Bind("F21Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF21Minimo" runat="server" Text='<%# Bind("F21Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF21Meta" runat="server" Text='<%# Bind("F21Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" Visible="true">
                            <ItemTemplate>
                                <asp:Image id="kpiF21" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F21Venta" HeaderText="F21Venta" Visible="false">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF21Venta" runat="server" Text='<%# Bind("F21Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF21Venta" runat="server" Text='<%# Bind("F21Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F22" Visible="true">
                            <HeaderTemplate>
                                 F-22<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF22Minimo" runat="server" Text='<%# Bind("F22Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF22Meta" runat="server" Text='<%# Bind("F22Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF22Minimo" runat="server" Text='<%# Bind("F22Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF22Meta" runat="server" Text='<%# Bind("F22Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" Visible="true">
                            <ItemTemplate>
                                <asp:Image id="kpiF22" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F22Venta" HeaderText="F22Venta" Visible="false">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF22Venta" runat="server" Text='<%# Bind("F22Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF22Venta" runat="server" Text='<%# Bind("F22Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F23" Visible="true">
                            <HeaderTemplate>
                                 F-23<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF23Minimo" runat="server" Text='<%# Bind("F23Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF23Meta" runat="server" Text='<%# Bind("F23Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF23Minimo" runat="server" Text='<%# Bind("F23Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF23Meta" runat="server" Text='<%# Bind("F23Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" Visible="true">
                            <ItemTemplate>
                                <asp:Image id="kpiF23" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F23Venta" HeaderText="F23Venta" Visible="false">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF23Venta" runat="server" Text='<%# Bind("F23Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF23Venta" runat="server" Text='<%# Bind("F23Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F24" Visible="true">
                            <HeaderTemplate>
                                 F-24<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF24Minimo" runat="server" Text='<%# Bind("F24Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF24Meta" runat="server" Text='<%# Bind("F24Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF24Minimo" runat="server" Text='<%# Bind("F24Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF24Meta" runat="server" Text='<%# Bind("F24Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" Visible="true">
                            <ItemTemplate>
                                <asp:Image id="kpiF24" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F24Venta" HeaderText="F24Venta" Visible="false">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF24Venta" runat="server" Text='<%# Bind("F24Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF24Venta" runat="server" Text='<%# Bind("F24Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F25" Visible="true">
                            <HeaderTemplate>
                                 F-25<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF25Minimo" runat="server" Text='<%# Bind("F25Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF25Meta" runat="server" Text='<%# Bind("F25Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF25Minimo" runat="server" Text='<%# Bind("F25Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF25Meta" runat="server" Text='<%# Bind("F25Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" Visible="true">
                            <ItemTemplate>
                                <asp:Image id="kpiF25" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F25Venta" HeaderText="F25Venta" Visible="false">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF25Venta" runat="server" Text='<%# Bind("F25Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF25Venta" runat="server" Text='<%# Bind("F25Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F26" Visible="true">
                            <HeaderTemplate>
                                 F-26<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF26Minimo" runat="server" Text='<%# Bind("F26Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF26Meta" runat="server" Text='<%# Bind("F26Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF26Minimo" runat="server" Text='<%# Bind("F26Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF26Meta" runat="server" Text='<%# Bind("F26Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" Visible="true">
                            <ItemTemplate>
                                <asp:Image id="kpiF26" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F26Venta" HeaderText="F26Venta" Visible="false">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF26Venta" runat="server" Text='<%# Bind("F26Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF26Venta" runat="server" Text='<%# Bind("F26Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F27" Visible="true">
                            <HeaderTemplate>
                                 F-27<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF27Minimo" runat="server" Text='<%# Bind("F27Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF27Meta" runat="server" Text='<%# Bind("F27Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF27Minimo" runat="server" Text='<%# Bind("F27Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF27Meta" runat="server" Text='<%# Bind("F27Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" Visible="true">
                            <ItemTemplate>
                                <asp:Image id="kpiF27" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F27Venta" HeaderText="F27Venta" Visible="false">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF27Venta" runat="server" Text='<%# Bind("F27Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF27Venta" runat="server" Text='<%# Bind("F27Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F28">
                            <HeaderTemplate>
                                 F-28<br />Mínimo&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Meta
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF28Minimo" runat="server" Text='<%# Bind("F28Minimo", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtF28Meta" runat="server" Text='<%# Bind("F28Meta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF28Minimo" runat="server" Text='<%# Bind("F28Minimo", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                                &nbsp;
                                <asp:Label ID="lbF28Meta" runat="server" Text='<%# Bind("F28Meta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="180px" />
                            <ItemStyle HorizontalAlign="Center" Width="180px" Font-Size="Small" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpiF28" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="18px" />
                            <ItemStyle Width="18px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="F28Venta" HeaderText="F28Venta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtF28Venta" runat="server" Text='<%# Bind("F28Venta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbF28Venta" runat="server" Text='<%# Bind("F28Venta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
        </tr>
    </table>

    <table align="left" cellpadding="0" cellspacing="0" id="totales" runat="server" visible="false">
        <tr>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;&nbsp;VENTA F02:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF02" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F04:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF04" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F05:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF05" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F07:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF07" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F09:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF09" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F11:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF11" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F12:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF12" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F13:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF13" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F14:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF14" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F15:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF15" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F16:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF16" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F17:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF17" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F19:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF19" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F20:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF20" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F21:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF21" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F22:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF22" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F23:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF23" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F24:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF24" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F25:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF25" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F26:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF26" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F27:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF27" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;</td>
            <td style='width:155'><strong>&nbsp;&nbsp;&nbsp;VENTA F28:&nbsp;</strong></td>
            <td style='width:146'><asp:TextBox ID="txtTotalF28" runat="server" Width="90px" ReadOnly="True" style='text-align:right' Font-Bold="True"></asp:TextBox></td>
            <td style='width:37'>&nbsp;&nbsp;</td>
        </tr>
    </table>
    <br />

    <table align="left" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top">
                <asp:GridView ID="gridF02" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                    onrowdatabound="gridF02_RowDataBound" onselectedindexchanged="gridF02_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF04" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                    onrowdatabound="gridF04_RowDataBound" onselectedindexchanged="gridF04_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF05" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                    onrowdatabound="gridF05_RowDataBound" onselectedindexchanged="gridF05_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF06" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                    onrowdatabound="gridF06_RowDataBound" 
                    onselectedindexchanged="gridF06_SelectedIndexChanged" Visible="False">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF07" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                    onrowdatabound="gridF07_RowDataBound" onselectedindexchanged="gridF07_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF08" runat="server" AutoGenerateColumns="False" Visible="false"
                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                    onrowdatabound="gridF08_RowDataBound" onselectedindexchanged="gridF08_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF09" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                    onrowdatabound="gridF09_RowDataBound" onselectedindexchanged="gridF09_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px"  />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF10" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                    onrowdatabound="gridF10_RowDataBound" 
                    onselectedindexchanged="gridF10_SelectedIndexChanged" Visible="False">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF11" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                    onrowdatabound="gridF11_RowDataBound" onselectedindexchanged="gridF11_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF12" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                    onrowdatabound="gridF12_RowDataBound" onselectedindexchanged="gridF12_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF13" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                    onrowdatabound="gridF13_RowDataBound" onselectedindexchanged="gridF13_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF14" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                    onrowdatabound="gridF14_RowDataBound" onselectedindexchanged="gridF14_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                    <asp:GridView ID="gridF15" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                    onrowdatabound="gridF15_RowDataBound" onselectedindexchanged="gridF15_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>

            <td valign="top">
                <asp:GridView ID="gridF16" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                    onrowdatabound="gridF16_RowDataBound" onselectedindexchanged="gridF16_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF17" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                    onrowdatabound="gridF17_RowDataBound" onselectedindexchanged="gridF17_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF18" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" visible="false" 
                    onrowdatabound="gridF18_RowDataBound" onselectedindexchanged="gridF18_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF19" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" visible="true" 
                    onrowdatabound="gridF19_RowDataBound" onselectedindexchanged="gridF19_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF20" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" visible="true" 
                    onrowdatabound="gridF20_RowDataBound" onselectedindexchanged="gridF20_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF21" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" visible="true" 
                    onrowdatabound="gridF21_RowDataBound" onselectedindexchanged="gridF21_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF22" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" visible="true" 
                    onrowdatabound="gridF22_RowDataBound" onselectedindexchanged="gridF22_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF23" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" visible="true" 
                    onrowdatabound="gridF23_RowDataBound" onselectedindexchanged="gridF23_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF24" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" visible="true" 
                    onrowdatabound="gridF24_RowDataBound" onselectedindexchanged="gridF24_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF25" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" visible="true" 
                    onrowdatabound="gridF25_RowDataBound" onselectedindexchanged="gridF25_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF26" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" visible="true" 
                    onrowdatabound="gridF26_RowDataBound" onselectedindexchanged="gridF26_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF27" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" visible="true" 
                    onrowdatabound="gridF27_RowDataBound" onselectedindexchanged="gridF27_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gridF28" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" visible="true" 
                    onrowdatabound="gridF28_RowDataBound" onselectedindexchanged="gridF28_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendedor" AccessibleHeaderText="Vendedor">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVendedor" runat="server" CausesValidation="false" CommandName="Select" onclick="lbVendedor_Click" Text='<%# Bind("Vendedor") %>' ToolTip="Haga clic aquí para ver el detalle del vendedor y clic nuevamente para ocultarlo"></asp:LinkButton>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridVendedor" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" OnItemDataBound="gridVendedor_ItemDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="false" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Semana" HeaderText="#" HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="true"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Venta">
                                                                                    <ItemTemplate><asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn Visible="false">
                                                                                    <ItemTemplate><asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label></ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate><asp:Image runat="server" ID="kpi" AlternateText="kpi" Visible="true" /></ItemTemplate>
                                                                                </asp:TemplateColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px"  BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Venta" AccessibleHeaderText="Venta">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbVenta" runat="server" Text='<%# Bind("Venta", "{0:###,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" Width="68px" />
                            <ItemStyle HorizontalAlign="Right" Width="68px"  BackColor="#FFFFCC" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Image id="kpi" imageurl="~/imagenes/blanco.jpg" runat="server"/>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle Width="20px"  BackColor="#FFFFCC"  />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDias" runat="server" Text='<%# Bind("Dias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDias" runat="server" Text='<%# Bind("Dias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Facturas" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFacturas" runat="server" Text='<%# Bind("Facturas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Articulos" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulos" runat="server" Text='<%# Bind("Articulos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oportunidades" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbOportunidades" runat="server" Text='<%# Bind("Oportunidades") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnVacaciones" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbEnVacaciones" runat="server" Text='<%# Bind("EnVacaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMinimo" runat="server" Text='<%# Bind("Minimo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meta" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMeta" runat="server" Text='<%# Bind("Meta") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalDias" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTotalDias" runat="server" Text='<%# Bind("TotalDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
        </tr>
    </table>

</asp:Content>
