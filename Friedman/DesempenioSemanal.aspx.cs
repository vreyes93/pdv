﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Friedman
{
    public partial class DesempenioSemanal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateTime dt = DateTime.Now;
                
                txtSemanas.Text = "10";
                anio.SelectedValue = Convert.ToString(dt.Year);

                int Restar = 1; DateTime PrimeroEnero = new DateTime(dt.Year, 1, 1);
                int NumeroSemana = System.Globalization.CultureInfo.CurrentUICulture.Calendar.GetWeekOfYear(dt, System.Globalization.CalendarWeekRule.FirstDay, dt.DayOfWeek);

                if (PrimeroEnero.DayOfWeek == DayOfWeek.Thursday) Restar = 0;

                switch (NumeroSemana)
                {
                    case 2:
                        semana.SelectedValue = "53";
                        anio.SelectedValue = Convert.ToString(dt.Year - 1);
                        break;
                    default:
                        semana.SelectedValue = Convert.ToString(NumeroSemana - Restar);
                        break;
                }
                
                ViewState["url"] = string.Format("http://integra/services/wsFriedman.asmx", Request.Url.Host);
            }
        }
        
        protected void lkSeleccionar_Click(object sender, EventArgs e)
        {
            try
            {
                lbError.Text = "";
                
                var ws = new wsFriedman.wsFriedman();
                ws.Url = Convert.ToString(ViewState["url"]);
                ws.Timeout = 999999999;

                var q = ws.ListarMetasTiendas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue)).ToArray();

                gridTiendas.DataSource = q;
                gridTiendas.DataBind();

                var qF02 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F02").ToArray();
                var qF04 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F04").ToArray();
                var qF05 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F05").ToArray();
                var qF06 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F06").ToArray();
                var qF07 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F07").ToArray();
                var qF08 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F08").ToArray();
                var qF09 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F09").ToArray();
                var qF10 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F10").ToArray();
                var qF11 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F11").ToArray();
                var qF12 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F12").ToArray();
                var qF13 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F13").ToArray();
                var qF14 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F14").ToArray();
                var qF15 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F15").ToArray();
                var qF16 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F16").ToArray();
                var qF17 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F17").ToArray();
                var qF18 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F18").ToArray();
                var qF19 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F19").ToArray();
                var qF20 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F20").ToArray();
                var qF21 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F21").ToArray();
                var qF22 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F22").ToArray();
                var qF23 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F23").ToArray();
                var qF24 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F24").ToArray();
                var qF25 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F25").ToArray();
                var qF26 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F26").ToArray();
                var qF27 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F27").ToArray();
                var qF28 = ws.ListarVentasVendedor(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F28").ToArray();
                
                
                try
                {
                    gridF02.DataSource = qF02;
                    gridF02.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F02.   {0}", ex.Message);
                }

                try
                {
                    gridF04.DataSource = qF04;
                    gridF04.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F04.   {0}", ex.Message);
                }

                try
                {
                    gridF05.DataSource = qF05;
                    gridF05.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F05.   {0}", ex.Message);
                }

                try
                {
                    gridF06.DataSource = qF06;
                    gridF06.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F06.   {0}", ex.Message);
                }

                try
                {
                    gridF07.DataSource = qF07;
                    gridF07.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F07.   {0}", ex.Message);
                }

                try
                {
                    gridF08.DataSource = qF08;
                    gridF08.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F08.   {0}", ex.Message);
                }

                try
                {
                    gridF09.DataSource = qF09;
                    gridF09.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F09.   {0}", ex.Message);
                }

                try
                {
                    gridF10.DataSource = qF10;
                    gridF10.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F10.   {0}", ex.Message);
                }

                try
                {
                    gridF11.DataSource = qF11;
                    gridF11.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F11.   {0}", ex.Message);
                }

                try
                {
                    gridF12.DataSource = qF12;
                    gridF12.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F12.   {0}", ex.Message);
                }

                try
                {
                    gridF13.DataSource = qF13;
                    gridF13.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F13.   {0}", ex.Message);
                }

                try
                {
                    gridF14.DataSource = qF14;
                    gridF14.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F14.   {0}", ex.Message);
                }

                try
                {
                    gridF15.DataSource = qF15;
                    gridF15.DataBind();
                }
                catch (Exception ex)
                {
                    string m = "";
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                    }
                    catch
                    {
                        try
                        {
                            m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                        }
                        catch
                        {
                            //Nothing
                        }
                    }
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F15. {0} {1}", ex.Message, m);
                }

                try
                {
                    gridF16.DataSource = qF16;
                    gridF16.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F16.   {0}", ex.Message);
                }

                try
                {
                    gridF17.DataSource = qF17;
                    gridF17.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F17.   {0}", ex.Message);
                }

                try
                {
                    gridF19.DataSource = qF19;
                    gridF19.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F19.   {0}", ex.Message);
                }

                try
                {
                    gridF20.DataSource = qF20;
                    gridF20.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F20.   {0}", ex.Message);
                }
                
                try
                {
                    gridF21.DataSource = qF21;
                    gridF21.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F21.   {0}", ex.Message);
                }

                try
                {
                    gridF22.DataSource = qF22;
                    gridF22.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F22.   {0}", ex.Message);
                }

                try
                {
                    gridF23.DataSource = qF23;
                    gridF23.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F23.   {0}", ex.Message);
                }

                try
                {
                    gridF24.DataSource = qF24;
                    gridF24.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F24.   {0}", ex.Message);
                }

                try
                {
                    gridF25.DataSource = qF25;
                    gridF25.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F25.   {0}", ex.Message);
                }

                try
                {
                    gridF26.DataSource = qF26;
                    gridF26.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F26.   {0}", ex.Message);
                }

                try
                {
                    gridF27.DataSource = qF27;
                    gridF27.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F27.   {0}", ex.Message);
                }
                try
                {
                    gridF28.DataSource = qF28;
                    gridF28.DataBind();
                }
                catch (Exception ex)
                {
                    lbError.Text = string.Format("Error al generar las metas.  NO HAY DIAS EN F28.   {0}", ex.Message);
                }

                //aplicarTotales();

                txtTotalF02.Text = String.Format("{0:0,0.00}", q[0].F02Venta);
                txtTotalF04.Text = String.Format("{0:0,0.00}", q[0].F04Venta);
                txtTotalF05.Text = String.Format("{0:0,0.00}", q[0].F05Venta);
                txtTotalF07.Text = String.Format("{0:0,0.00}", q[0].F07Venta);
                txtTotalF09.Text = String.Format("{0:0,0.00}", q[0].F09Venta);
                txtTotalF11.Text = String.Format("{0:0,0.00}", q[0].F11Venta);
                txtTotalF12.Text = String.Format("{0:0,0.00}", q[0].F12Venta);
                txtTotalF13.Text = String.Format("{0:0,0.00}", q[0].F13Venta);
                txtTotalF14.Text = String.Format("{0:0,0.00}", q[0].F14Venta);
                txtTotalF15.Text = String.Format("{0:0,0.00}", q[0].F15Venta);
                txtTotalF16.Text = String.Format("{0:0,0.00}", q[0].F16Venta);
                txtTotalF17.Text = String.Format("{0:0,0.00}", q[0].F17Venta);
                txtTotalF19.Text = String.Format("{0:0,0.00}", q[0].F19Venta);
                txtTotalF20.Text = String.Format("{0:0,0.00}", q[0].F20Venta);
                txtTotalF21.Text = String.Format("{0:0,0.00}", q[0].F21Venta);
                txtTotalF22.Text = String.Format("{0:0,0.00}", q[0].F22Venta);
                txtTotalF23.Text = String.Format("{0:0,0.00}", q[0].F23Venta);
                txtTotalF24.Text = String.Format("{0:0,0.00}", q[0].F24Venta);
                txtTotalF25.Text = String.Format("{0:0,0.00}", q[0].F25Venta);
                txtTotalF26.Text = String.Format("{0:0,0.00}", q[0].F26Venta);
                txtTotalF27.Text = String.Format("{0:0,0.00}", q[0].F27Venta);
                txtTotalF28.Text = String.Format("{0:0,0.00}", q[0].F28Venta);
                
                totales.Visible = true;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al generar las metas. {0} {1}", ex.Message, m);
            }
        }
        
        void aplicarTotales()
        {
            try
            {
                GridViewHelper helperF02 = new GridViewHelper(this.gridF02);
                helperF02.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF02.GeneralSummary += new FooterEvent(helperF02_GeneralSummary);
                gridF02.DataBind();

                GridViewHelper helperF04 = new GridViewHelper(this.gridF04);
                helperF04.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF04.GeneralSummary += new FooterEvent(helperF04_GeneralSummary);
                gridF04.DataBind();

                GridViewHelper helperF05 = new GridViewHelper(this.gridF05);
                helperF05.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF05.GeneralSummary += new FooterEvent(helperF05_GeneralSummary);
                gridF05.DataBind();

                GridViewHelper helperF06 = new GridViewHelper(this.gridF06);
                helperF06.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF06.GeneralSummary += new FooterEvent(helperF06_GeneralSummary);
                gridF06.DataBind();

                GridViewHelper helperF07 = new GridViewHelper(this.gridF07);
                helperF07.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF07.GeneralSummary += new FooterEvent(helperF07_GeneralSummary);
                gridF07.DataBind();

                GridViewHelper helperF08 = new GridViewHelper(this.gridF08);
                helperF08.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF08.GeneralSummary += new FooterEvent(helperF08_GeneralSummary);
                gridF08.DataBind();

                GridViewHelper helperF09 = new GridViewHelper(this.gridF09);
                helperF09.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF09.GeneralSummary += new FooterEvent(helperF09_GeneralSummary);
                gridF09.DataBind();

                GridViewHelper helperF10 = new GridViewHelper(this.gridF10);
                helperF10.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF10.GeneralSummary += new FooterEvent(helperF10_GeneralSummary);
                gridF10.DataBind();

                GridViewHelper helperF11 = new GridViewHelper(this.gridF11);
                helperF11.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF11.GeneralSummary += new FooterEvent(helperF11_GeneralSummary);
                gridF11.DataBind();

                GridViewHelper helperF12 = new GridViewHelper(this.gridF12);
                helperF12.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF12.GeneralSummary += new FooterEvent(helperF12_GeneralSummary);
                gridF12.DataBind();

                GridViewHelper helperF13 = new GridViewHelper(this.gridF13);
                helperF13.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF13.GeneralSummary += new FooterEvent(helperF13_GeneralSummary);
                gridF13.DataBind();

                GridViewHelper helperF14 = new GridViewHelper(this.gridF14);
                helperF14.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF14.GeneralSummary += new FooterEvent(helperF14_GeneralSummary);
                gridF14.DataBind();

                GridViewHelper helperF15 = new GridViewHelper(this.gridF15);
                helperF15.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF15.GeneralSummary += new FooterEvent(helperF15_GeneralSummary);
                gridF15.DataBind();

                GridViewHelper helperF16 = new GridViewHelper(this.gridF16);
                helperF16.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF16.GeneralSummary += new FooterEvent(helperF16_GeneralSummary);
                gridF16.DataBind();

                GridViewHelper helperF17 = new GridViewHelper(this.gridF17);
                helperF17.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF17.GeneralSummary += new FooterEvent(helperF17_GeneralSummary);
                gridF17.DataBind();

                GridViewHelper helperF19 = new GridViewHelper(this.gridF19);
                helperF19.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF19.GeneralSummary += new FooterEvent(helperF19_GeneralSummary);
                gridF19.DataBind();

                GridViewHelper helperF20 = new GridViewHelper(this.gridF20);
                helperF20.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF20.GeneralSummary += new FooterEvent(helperF20_GeneralSummary);
                gridF20.DataBind();

                GridViewHelper helperF21 = new GridViewHelper(this.gridF21);
                helperF21.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF21.GeneralSummary += new FooterEvent(helperF21_GeneralSummary);
                gridF21.DataBind();

                GridViewHelper helperF22 = new GridViewHelper(this.gridF22);
                helperF22.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF22.GeneralSummary += new FooterEvent(helperF22_GeneralSummary);
                gridF22.DataBind();

                GridViewHelper helperF23 = new GridViewHelper(this.gridF23);
                helperF23.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF23.GeneralSummary += new FooterEvent(helperF23_GeneralSummary);
                gridF23.DataBind();

                GridViewHelper helperF24 = new GridViewHelper(this.gridF24);
                helperF24.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF24.GeneralSummary += new FooterEvent(helperF24_GeneralSummary);
                gridF24.DataBind();

                GridViewHelper helperF25 = new GridViewHelper(this.gridF25);
                helperF25.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF25.GeneralSummary += new FooterEvent(helperF25_GeneralSummary);
                gridF25.DataBind();

                GridViewHelper helperF26 = new GridViewHelper(this.gridF26);
                helperF26.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF26.GeneralSummary += new FooterEvent(helperF26_GeneralSummary);
                gridF26.DataBind();

                GridViewHelper helperF27 = new GridViewHelper(this.gridF27);
                helperF27.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF27.GeneralSummary += new FooterEvent(helperF27_GeneralSummary);
                gridF27.DataBind();

                GridViewHelper helperF28 = new GridViewHelper(this.gridF28);
                helperF28.RegisterSummary("Venta", "{0:###,###,###,###,###.##}", SummaryOperation.Sum);

                helperF28.GeneralSummary += new FooterEvent(helperF28_GeneralSummary);
                gridF28.DataBind();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al aplicar los totales. {0} {1}", ex.Message, m);
            }
        }

        private void helperF02_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F02:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF04_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F04:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF05_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F05:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF06_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F06:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF07_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F07:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF08_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F08:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF09_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F09:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF10_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F10:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF11_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F11:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF12_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F12:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF13_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F13:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF14_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F14:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF15_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F15:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF16_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F16:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF17_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F17:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF19_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F19:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF20_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F20:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF21_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F21:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF22_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F22:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF23_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F23:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF24_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F24:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF25_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F25:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF26_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F26:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helperF27_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F27:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }
        private void helperF28_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL F28:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        protected void gridTiendas_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var kpiF02 = e.Row.FindControl("kpiF02") as Image;
                Label lbF02Venta = (Label)e.Row.FindControl("lbF02Venta");
                Label lbF02Meta = (Label)e.Row.FindControl("lbF02Meta");
                Label lbF02Minimo = (Label)e.Row.FindControl("lbF02Minimo");

                if (Convert.ToDecimal(lbF02Venta.Text) < Convert.ToDecimal(lbF02Minimo.Text)) kpiF02.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF02Venta.Text) >= Convert.ToDecimal(lbF02Minimo.Text)) && (Convert.ToDecimal(lbF02Venta.Text) < Convert.ToDecimal(lbF02Meta.Text))) kpiF02.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF02Venta.Text) >= Convert.ToDecimal(lbF02Meta.Text)) kpiF02.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF03 = e.Row.FindControl("kpiF03") as Image;
                Label lbF03Venta = (Label)e.Row.FindControl("lbF03Venta");
                Label lbF03Meta = (Label)e.Row.FindControl("lbF03Meta");
                Label lbF03Minimo = (Label)e.Row.FindControl("lbF03Minimo");

                if (Convert.ToDecimal(lbF03Venta.Text) < Convert.ToDecimal(lbF03Minimo.Text)) kpiF03.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF03Venta.Text) >= Convert.ToDecimal(lbF03Minimo.Text)) && (Convert.ToDecimal(lbF03Venta.Text) < Convert.ToDecimal(lbF03Meta.Text))) kpiF03.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF03Venta.Text) >= Convert.ToDecimal(lbF03Meta.Text)) kpiF03.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF04 = e.Row.FindControl("kpiF04") as Image;
                Label lbF04Venta = (Label)e.Row.FindControl("lbF04Venta");
                Label lbF04Meta = (Label)e.Row.FindControl("lbF04Meta");
                Label lbF04Minimo = (Label)e.Row.FindControl("lbF04Minimo");

                if (Convert.ToDecimal(lbF04Venta.Text) < Convert.ToDecimal(lbF04Minimo.Text)) kpiF04.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF04Venta.Text) >= Convert.ToDecimal(lbF04Minimo.Text)) && (Convert.ToDecimal(lbF04Venta.Text) < Convert.ToDecimal(lbF04Meta.Text))) kpiF04.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF04Venta.Text) >= Convert.ToDecimal(lbF04Meta.Text)) kpiF04.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF05 = e.Row.FindControl("kpiF05") as Image;
                Label lbF05Venta = (Label)e.Row.FindControl("lbF05Venta");
                Label lbF05Meta = (Label)e.Row.FindControl("lbF05Meta");
                Label lbF05Minimo = (Label)e.Row.FindControl("lbF05Minimo");

                if (Convert.ToDecimal(lbF05Venta.Text) < Convert.ToDecimal(lbF05Minimo.Text)) kpiF05.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF05Venta.Text) >= Convert.ToDecimal(lbF05Minimo.Text)) && (Convert.ToDecimal(lbF05Venta.Text) < Convert.ToDecimal(lbF05Meta.Text))) kpiF05.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF05Venta.Text) >= Convert.ToDecimal(lbF05Meta.Text)) kpiF05.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF06 = e.Row.FindControl("kpiF06") as Image;
                Label lbF06Venta = (Label)e.Row.FindControl("lbF06Venta");
                Label lbF06Meta = (Label)e.Row.FindControl("lbF06Meta");
                Label lbF06Minimo = (Label)e.Row.FindControl("lbF06Minimo");

                if (Convert.ToDecimal(lbF06Venta.Text) < Convert.ToDecimal(lbF06Minimo.Text)) kpiF06.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF06Venta.Text) >= Convert.ToDecimal(lbF06Minimo.Text)) && (Convert.ToDecimal(lbF06Venta.Text) < Convert.ToDecimal(lbF06Meta.Text))) kpiF06.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF06Venta.Text) >= Convert.ToDecimal(lbF06Meta.Text)) kpiF06.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF07 = e.Row.FindControl("kpiF07") as Image;
                Label lbF07Venta = (Label)e.Row.FindControl("lbF07Venta");
                Label lbF07Meta = (Label)e.Row.FindControl("lbF07Meta");
                Label lbF07Minimo = (Label)e.Row.FindControl("lbF07Minimo");

                if (Convert.ToDecimal(lbF07Venta.Text) < Convert.ToDecimal(lbF07Minimo.Text)) kpiF07.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF07Venta.Text) >= Convert.ToDecimal(lbF07Minimo.Text)) && (Convert.ToDecimal(lbF07Venta.Text) < Convert.ToDecimal(lbF07Meta.Text))) kpiF07.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF07Venta.Text) >= Convert.ToDecimal(lbF07Meta.Text)) kpiF07.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF08 = e.Row.FindControl("kpiF08") as Image;
                Label lbF08Venta = (Label)e.Row.FindControl("lbF08Venta");
                Label lbF08Meta = (Label)e.Row.FindControl("lbF08Meta");
                Label lbF08Minimo = (Label)e.Row.FindControl("lbF08Minimo");

                if (Convert.ToDecimal(lbF08Venta.Text) < Convert.ToDecimal(lbF08Minimo.Text)) kpiF08.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF08Venta.Text) >= Convert.ToDecimal(lbF08Minimo.Text)) && (Convert.ToDecimal(lbF08Venta.Text) < Convert.ToDecimal(lbF08Meta.Text))) kpiF08.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF08Venta.Text) >= Convert.ToDecimal(lbF08Meta.Text)) kpiF08.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF09 = e.Row.FindControl("kpiF09") as Image;
                Label lbF09Venta = (Label)e.Row.FindControl("lbF09Venta");
                Label lbF09Meta = (Label)e.Row.FindControl("lbF09Meta");
                Label lbF09Minimo = (Label)e.Row.FindControl("lbF09Minimo");

                if (Convert.ToDecimal(lbF09Venta.Text) < Convert.ToDecimal(lbF09Minimo.Text)) kpiF09.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF09Venta.Text) >= Convert.ToDecimal(lbF09Minimo.Text)) && (Convert.ToDecimal(lbF09Venta.Text) < Convert.ToDecimal(lbF09Meta.Text))) kpiF09.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF09Venta.Text) >= Convert.ToDecimal(lbF09Meta.Text)) kpiF09.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF10 = e.Row.FindControl("kpiF10") as Image;
                Label lbF10Venta = (Label)e.Row.FindControl("lbF10Venta");
                Label lbF10Meta = (Label)e.Row.FindControl("lbF10Meta");
                Label lbF10Minimo = (Label)e.Row.FindControl("lbF10Minimo");

                if (Convert.ToDecimal(lbF10Venta.Text) < Convert.ToDecimal(lbF10Minimo.Text)) kpiF10.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF10Venta.Text) >= Convert.ToDecimal(lbF10Minimo.Text)) && (Convert.ToDecimal(lbF10Venta.Text) < Convert.ToDecimal(lbF10Meta.Text))) kpiF10.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF10Venta.Text) >= Convert.ToDecimal(lbF10Meta.Text)) kpiF10.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF11 = e.Row.FindControl("kpiF11") as Image;
                Label lbF11Venta = (Label)e.Row.FindControl("lbF11Venta");
                Label lbF11Meta = (Label)e.Row.FindControl("lbF11Meta");
                Label lbF11Minimo = (Label)e.Row.FindControl("lbF11Minimo");

                if (Convert.ToDecimal(lbF11Venta.Text) < Convert.ToDecimal(lbF11Minimo.Text)) kpiF11.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF11Venta.Text) >= Convert.ToDecimal(lbF11Minimo.Text)) && (Convert.ToDecimal(lbF11Venta.Text) < Convert.ToDecimal(lbF11Meta.Text))) kpiF11.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF11Venta.Text) >= Convert.ToDecimal(lbF11Meta.Text)) kpiF11.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF12 = e.Row.FindControl("kpiF12") as Image;
                Label lbF12Venta = (Label)e.Row.FindControl("lbF12Venta");
                Label lbF12Meta = (Label)e.Row.FindControl("lbF12Meta");
                Label lbF12Minimo = (Label)e.Row.FindControl("lbF12Minimo");

                if (Convert.ToDecimal(lbF12Venta.Text) < Convert.ToDecimal(lbF12Minimo.Text)) kpiF12.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF12Venta.Text) >= Convert.ToDecimal(lbF12Minimo.Text)) && (Convert.ToDecimal(lbF12Venta.Text) < Convert.ToDecimal(lbF12Meta.Text))) kpiF12.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF12Venta.Text) >= Convert.ToDecimal(lbF12Meta.Text)) kpiF12.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF13 = e.Row.FindControl("kpiF13") as Image;
                Label lbF13Venta = (Label)e.Row.FindControl("lbF13Venta");
                Label lbF13Meta = (Label)e.Row.FindControl("lbF13Meta");
                Label lbF13Minimo = (Label)e.Row.FindControl("lbF13Minimo");

                if (Convert.ToDecimal(lbF13Venta.Text) < Convert.ToDecimal(lbF13Minimo.Text)) kpiF13.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF13Venta.Text) >= Convert.ToDecimal(lbF13Minimo.Text)) && (Convert.ToDecimal(lbF13Venta.Text) < Convert.ToDecimal(lbF13Meta.Text))) kpiF13.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF13Venta.Text) >= Convert.ToDecimal(lbF13Meta.Text)) kpiF13.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF14 = e.Row.FindControl("kpiF14") as Image;
                Label lbF14Venta = (Label)e.Row.FindControl("lbF14Venta");
                Label lbF14Meta = (Label)e.Row.FindControl("lbF14Meta");
                Label lbF14Minimo = (Label)e.Row.FindControl("lbF14Minimo");

                if (Convert.ToDecimal(lbF14Venta.Text) < Convert.ToDecimal(lbF14Minimo.Text)) kpiF14.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF14Venta.Text) >= Convert.ToDecimal(lbF14Minimo.Text)) && (Convert.ToDecimal(lbF14Venta.Text) < Convert.ToDecimal(lbF14Meta.Text))) kpiF14.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF14Venta.Text) >= Convert.ToDecimal(lbF14Meta.Text)) kpiF14.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF15 = e.Row.FindControl("kpiF15") as Image;
                Label lbF15Venta = (Label)e.Row.FindControl("lbF15Venta");
                Label lbF15Meta = (Label)e.Row.FindControl("lbF15Meta");
                Label lbF15Minimo = (Label)e.Row.FindControl("lbF15Minimo");

                if (Convert.ToDecimal(lbF15Venta.Text) < Convert.ToDecimal(lbF15Minimo.Text)) kpiF15.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF15Venta.Text) >= Convert.ToDecimal(lbF15Minimo.Text)) && (Convert.ToDecimal(lbF15Venta.Text) < Convert.ToDecimal(lbF15Meta.Text))) kpiF15.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF15Venta.Text) >= Convert.ToDecimal(lbF15Meta.Text)) kpiF15.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF16 = e.Row.FindControl("kpiF16") as Image;
                Label lbF16Venta = (Label)e.Row.FindControl("lbF16Venta");
                Label lbF16Meta = (Label)e.Row.FindControl("lbF16Meta");
                Label lbF16Minimo = (Label)e.Row.FindControl("lbF16Minimo");

                if (Convert.ToDecimal(lbF16Venta.Text) < Convert.ToDecimal(lbF16Minimo.Text)) kpiF16.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF16Venta.Text) >= Convert.ToDecimal(lbF16Minimo.Text)) && (Convert.ToDecimal(lbF16Venta.Text) < Convert.ToDecimal(lbF16Meta.Text))) kpiF16.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF16Venta.Text) >= Convert.ToDecimal(lbF16Meta.Text)) kpiF16.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF17 = e.Row.FindControl("kpiF17") as Image;
                Label lbF17Venta = (Label)e.Row.FindControl("lbF17Venta");
                Label lbF17Meta = (Label)e.Row.FindControl("lbF17Meta");
                Label lbF17Minimo = (Label)e.Row.FindControl("lbF17Minimo");

                if (Convert.ToDecimal(lbF17Venta.Text) < Convert.ToDecimal(lbF17Minimo.Text)) kpiF17.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF17Venta.Text) >= Convert.ToDecimal(lbF17Minimo.Text)) && (Convert.ToDecimal(lbF17Venta.Text) < Convert.ToDecimal(lbF17Meta.Text))) kpiF17.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF17Venta.Text) >= Convert.ToDecimal(lbF17Meta.Text)) kpiF17.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF19 = e.Row.FindControl("kpiF19") as Image;
                Label lbF19Venta = (Label)e.Row.FindControl("lbF19Venta");
                Label lbF19Meta = (Label)e.Row.FindControl("lbF19Meta");
                Label lbF19Minimo = (Label)e.Row.FindControl("lbF19Minimo");

                if (Convert.ToDecimal(lbF19Venta.Text) < Convert.ToDecimal(lbF19Minimo.Text)) kpiF19.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF19Venta.Text) >= Convert.ToDecimal(lbF19Minimo.Text)) && (Convert.ToDecimal(lbF19Venta.Text) < Convert.ToDecimal(lbF19Meta.Text))) kpiF19.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF19Venta.Text) >= Convert.ToDecimal(lbF19Meta.Text)) kpiF19.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF20 = e.Row.FindControl("kpiF20") as Image;
                Label lbF20Venta = (Label)e.Row.FindControl("lbF20Venta");
                Label lbF20Meta = (Label)e.Row.FindControl("lbF20Meta");
                Label lbF20Minimo = (Label)e.Row.FindControl("lbF20Minimo");

                if (Convert.ToDecimal(lbF20Venta.Text) < Convert.ToDecimal(lbF20Minimo.Text)) kpiF20.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF20Venta.Text) >= Convert.ToDecimal(lbF20Minimo.Text)) && (Convert.ToDecimal(lbF20Venta.Text) < Convert.ToDecimal(lbF20Meta.Text))) kpiF20.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF20Venta.Text) >= Convert.ToDecimal(lbF20Meta.Text)) kpiF20.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF21 = e.Row.FindControl("kpiF21") as Image;
                Label lbF21Venta = (Label)e.Row.FindControl("lbF21Venta");
                Label lbF21Meta = (Label)e.Row.FindControl("lbF21Meta");
                Label lbF21Minimo = (Label)e.Row.FindControl("lbF21Minimo");

                if (Convert.ToDecimal(lbF21Venta.Text) < Convert.ToDecimal(lbF21Minimo.Text)) kpiF21.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF21Venta.Text) >= Convert.ToDecimal(lbF21Minimo.Text)) && (Convert.ToDecimal(lbF21Venta.Text) < Convert.ToDecimal(lbF21Meta.Text))) kpiF21.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF21Venta.Text) >= Convert.ToDecimal(lbF21Meta.Text)) kpiF21.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF22 = e.Row.FindControl("kpiF22") as Image;
                Label lbF22Venta = (Label)e.Row.FindControl("lbF22Venta");
                Label lbF22Meta = (Label)e.Row.FindControl("lbF22Meta");
                Label lbF22Minimo = (Label)e.Row.FindControl("lbF22Minimo");

                if (Convert.ToDecimal(lbF22Venta.Text) < Convert.ToDecimal(lbF22Minimo.Text)) kpiF22.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF22Venta.Text) >= Convert.ToDecimal(lbF22Minimo.Text)) && (Convert.ToDecimal(lbF22Venta.Text) < Convert.ToDecimal(lbF22Meta.Text))) kpiF22.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF22Venta.Text) >= Convert.ToDecimal(lbF22Meta.Text)) kpiF22.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF23 = e.Row.FindControl("kpiF23") as Image;
                Label lbF23Venta = (Label)e.Row.FindControl("lbF23Venta");
                Label lbF23Meta = (Label)e.Row.FindControl("lbF23Meta");
                Label lbF23Minimo = (Label)e.Row.FindControl("lbF23Minimo");

                if (Convert.ToDecimal(lbF23Venta.Text) < Convert.ToDecimal(lbF23Minimo.Text)) kpiF23.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF23Venta.Text) >= Convert.ToDecimal(lbF23Minimo.Text)) && (Convert.ToDecimal(lbF23Venta.Text) < Convert.ToDecimal(lbF23Meta.Text))) kpiF23.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF23Venta.Text) >= Convert.ToDecimal(lbF23Meta.Text)) kpiF23.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF24 = e.Row.FindControl("kpiF24") as Image;
                Label lbF24Venta = (Label)e.Row.FindControl("lbF24Venta");
                Label lbF24Meta = (Label)e.Row.FindControl("lbF24Meta");
                Label lbF24Minimo = (Label)e.Row.FindControl("lbF24Minimo");

                if (Convert.ToDecimal(lbF24Venta.Text) < Convert.ToDecimal(lbF24Minimo.Text)) kpiF24.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF24Venta.Text) >= Convert.ToDecimal(lbF24Minimo.Text)) && (Convert.ToDecimal(lbF24Venta.Text) < Convert.ToDecimal(lbF24Meta.Text))) kpiF24.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF24Venta.Text) >= Convert.ToDecimal(lbF24Meta.Text)) kpiF24.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF25 = e.Row.FindControl("kpiF25") as Image;
                Label lbF25Venta = (Label)e.Row.FindControl("lbF25Venta");
                Label lbF25Meta = (Label)e.Row.FindControl("lbF25Meta");
                Label lbF25Minimo = (Label)e.Row.FindControl("lbF25Minimo");

                if (Convert.ToDecimal(lbF25Venta.Text) < Convert.ToDecimal(lbF25Minimo.Text)) kpiF25.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF25Venta.Text) >= Convert.ToDecimal(lbF25Minimo.Text)) && (Convert.ToDecimal(lbF25Venta.Text) < Convert.ToDecimal(lbF25Meta.Text))) kpiF25.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF25Venta.Text) >= Convert.ToDecimal(lbF25Meta.Text)) kpiF25.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF26 = e.Row.FindControl("kpiF26") as Image;
                Label lbF26Venta = (Label)e.Row.FindControl("lbF26Venta");
                Label lbF26Meta = (Label)e.Row.FindControl("lbF26Meta");
                Label lbF26Minimo = (Label)e.Row.FindControl("lbF26Minimo");

                if (Convert.ToDecimal(lbF26Venta.Text) < Convert.ToDecimal(lbF26Minimo.Text)) kpiF26.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF26Venta.Text) >= Convert.ToDecimal(lbF26Minimo.Text)) && (Convert.ToDecimal(lbF26Venta.Text) < Convert.ToDecimal(lbF26Meta.Text))) kpiF26.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF26Venta.Text) >= Convert.ToDecimal(lbF26Meta.Text)) kpiF26.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF27 = e.Row.FindControl("kpiF27") as Image;
                Label lbF27Venta = (Label)e.Row.FindControl("lbF27Venta");
                Label lbF27Meta = (Label)e.Row.FindControl("lbF27Meta");
                Label lbF27Minimo = (Label)e.Row.FindControl("lbF27Minimo");

                if (Convert.ToDecimal(lbF27Venta.Text) < Convert.ToDecimal(lbF27Minimo.Text)) kpiF27.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF27Venta.Text) >= Convert.ToDecimal(lbF27Minimo.Text)) && (Convert.ToDecimal(lbF27Venta.Text) < Convert.ToDecimal(lbF27Meta.Text))) kpiF27.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF27Venta.Text) >= Convert.ToDecimal(lbF27Meta.Text)) kpiF27.ImageUrl = "~/imagenes/estrella.jpg";

                var kpiF28 = e.Row.FindControl("kpiF28") as Image;
                Label lbF28Venta = (Label)e.Row.FindControl("lbF28Venta");
                Label lbF28Meta = (Label)e.Row.FindControl("lbF28Meta");
                Label lbF28Minimo = (Label)e.Row.FindControl("lbF28Minimo");

                if (Convert.ToDecimal(lbF28Venta.Text) < Convert.ToDecimal(lbF28Minimo.Text)) kpiF28.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((Convert.ToDecimal(lbF28Venta.Text) >= Convert.ToDecimal(lbF28Minimo.Text)) && (Convert.ToDecimal(lbF28Venta.Text) < Convert.ToDecimal(lbF28Meta.Text))) kpiF28.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (Convert.ToDecimal(lbF28Venta.Text) >= Convert.ToDecimal(lbF28Meta.Text)) kpiF28.ImageUrl = "~/imagenes/estrella.jpg";

            }
        }

        protected void gridF02_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var kpi = e.Row.FindControl("kpi") as Image;
                Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                Label lbDias = (Label)e.Row.FindControl("lbDias");
                Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                Decimal minimo = 0;
                Decimal meta = 0;
                Decimal venta = Convert.ToDecimal(lbVenta.Text);

                if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                {
                    minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                }

                if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
            }
        }

        protected void gridF04_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var kpi = e.Row.FindControl("kpi") as Image;
                Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                Label lbDias = (Label)e.Row.FindControl("lbDias");
                Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                Decimal minimo = 0;
                Decimal meta = 0;
                Decimal venta = Convert.ToDecimal(lbVenta.Text);

                if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                {
                    minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                }

                if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
            }
        }

        protected void gridF05_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var kpi = e.Row.FindControl("kpi") as Image;
                Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                Label lbDias = (Label)e.Row.FindControl("lbDias");
                Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                Decimal minimo = 0;
                Decimal meta = 0;
                Decimal venta = Convert.ToDecimal(lbVenta.Text);

                if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                {
                    minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                }

                if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
            }
        }

        protected void gridF06_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var kpi = e.Row.FindControl("kpi") as Image;
                Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                Label lbDias = (Label)e.Row.FindControl("lbDias");
                Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                Decimal minimo = 0;
                Decimal meta = 0;
                Decimal venta = Convert.ToDecimal(lbVenta.Text);

                if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                {
                    minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                }

                if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
            }
        }

        protected void gridF07_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var kpi = e.Row.FindControl("kpi") as Image;
                Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                Label lbDias = (Label)e.Row.FindControl("lbDias");
                Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                Decimal minimo = 0;
                Decimal meta = 0;
                Decimal venta = Convert.ToDecimal(lbVenta.Text);

                if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                {
                    minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                }

                if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
            }
        }

        protected void gridF08_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var kpi = e.Row.FindControl("kpi") as Image;
                Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                Label lbDias = (Label)e.Row.FindControl("lbDias");
                Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                Decimal minimo = 0;
                Decimal meta = 0;
                Decimal venta = Convert.ToDecimal(lbVenta.Text);

                if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                {
                    minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                }

                if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
            }
        }

        protected void gridF09_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var kpi = e.Row.FindControl("kpi") as Image;
                Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                Label lbDias = (Label)e.Row.FindControl("lbDias");
                Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                Decimal minimo = 0;
                Decimal meta = 0;
                Decimal venta = Convert.ToDecimal(lbVenta.Text);

                if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                {
                    minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                }

                if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
            }
        }

        protected void gridF10_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var kpi = e.Row.FindControl("kpi") as Image;
                Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                Label lbDias = (Label)e.Row.FindControl("lbDias");
                Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                Decimal minimo = 0;
                Decimal meta = 0;
                Decimal venta = Convert.ToDecimal(lbVenta.Text);

                if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                {
                    minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                }


                if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
            }
        }

        protected void gridF11_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var kpi = e.Row.FindControl("kpi") as Image;
                Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                Label lbDias = (Label)e.Row.FindControl("lbDias");
                Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                Decimal minimo = 0;
                Decimal meta = 0;
                Decimal venta = Convert.ToDecimal(lbVenta.Text);

                if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                {
                    minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                }

                if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
            }
        }

        protected void gridF12_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var kpi = e.Row.FindControl("kpi") as Image;
                Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                Label lbDias = (Label)e.Row.FindControl("lbDias");
                Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                Decimal minimo = 0;
                Decimal meta = 0;
                Decimal venta = Convert.ToDecimal(lbVenta.Text);

                if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                {
                    minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                }

                if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
            }
        }

        protected void gridF13_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var kpi = e.Row.FindControl("kpi") as Image;
                Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                Label lbDias = (Label)e.Row.FindControl("lbDias");
                Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                Decimal minimo = 0;
                Decimal meta = 0;
                Decimal venta = Convert.ToDecimal(lbVenta.Text);

                if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                {
                    minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                }

                if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
            }
        }

        protected void gridF14_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var kpi = e.Row.FindControl("kpi") as Image;
                Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                Label lbDias = (Label)e.Row.FindControl("lbDias");
                Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                Decimal minimo = 0;
                Decimal meta = 0;
                Decimal venta = Convert.ToDecimal(lbVenta.Text);

                if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                {
                    minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                }

                if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
            }
        }

        protected void gridF16_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var kpi = e.Row.FindControl("kpi") as Image;
                Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                Label lbDias = (Label)e.Row.FindControl("lbDias");
                Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                Decimal minimo = 0;
                Decimal meta = 0;
                Decimal venta = Convert.ToDecimal(lbVenta.Text);

                if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                {
                    minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                }

                if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
            }
        }

        protected void gridF17_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var kpi = e.Row.FindControl("kpi") as Image;
                Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                Label lbDias = (Label)e.Row.FindControl("lbDias");
                Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                Decimal minimo = 0;
                Decimal meta = 0;
                Decimal venta = Convert.ToDecimal(lbVenta.Text);

                if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                {
                    minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                }

                if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
            }
        }

        protected void gridF18_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
            }
        }

        protected void gridF19_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var kpi = e.Row.FindControl("kpi") as Image;
                Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                Label lbDias = (Label)e.Row.FindControl("lbDias");
                Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                Decimal minimo = 0;
                Decimal meta = 0;
                Decimal venta = Convert.ToDecimal(lbVenta.Text);

                if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                {
                    minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                }

                if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
            }
        }

        protected void gridF20_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var kpi = e.Row.FindControl("kpi") as Image;
                    Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                    Label lbDias = (Label)e.Row.FindControl("lbDias");
                    Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                    Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                    Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                    Decimal minimo = 0;
                    Decimal meta = 0;
                    Decimal venta = Convert.ToDecimal(lbVenta.Text);

                    if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                    {
                        minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                        meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    }

                    if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                    if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                    if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                    if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
                }
            }
        }

        protected void gridF21_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var kpi = e.Row.FindControl("kpi") as Image;
                    Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                    Label lbDias = (Label)e.Row.FindControl("lbDias");
                    Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                    Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                    Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                    Decimal minimo = 0;
                    Decimal meta = 0;
                    Decimal venta = Convert.ToDecimal(lbVenta.Text);

                    if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                    {
                        minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                        meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    }

                    if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                    if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                    if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                    if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
                }
            }
        }

        protected void gridF22_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var kpi = e.Row.FindControl("kpi") as Image;
                    Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                    Label lbDias = (Label)e.Row.FindControl("lbDias");
                    Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                    Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                    Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                    Decimal minimo = 0;
                    Decimal meta = 0;
                    Decimal venta = Convert.ToDecimal(lbVenta.Text);

                    if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                    {
                        minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                        meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    }

                    if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                    if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                    if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                    if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
                }
            }
        }

        protected void gridF23_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var kpi = e.Row.FindControl("kpi") as Image;
                    Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                    Label lbDias = (Label)e.Row.FindControl("lbDias");
                    Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                    Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                    Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                    Decimal minimo = 0;
                    Decimal meta = 0;
                    Decimal venta = Convert.ToDecimal(lbVenta.Text);

                    if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                    {
                        minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                        meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    }

                    if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                    if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                    if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                    if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
                }
            }
        }

        protected void gridF24_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var kpi = e.Row.FindControl("kpi") as Image;
                    Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                    Label lbDias = (Label)e.Row.FindControl("lbDias");
                    Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                    Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                    Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                    Decimal minimo = 0;
                    Decimal meta = 0;
                    Decimal venta = Convert.ToDecimal(lbVenta.Text);

                    if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                    {
                        minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                        meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    }

                    if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                    if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                    if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                    if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
                }
            }
        }

        protected void gridF25_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var kpi = e.Row.FindControl("kpi") as Image;
                    Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                    Label lbDias = (Label)e.Row.FindControl("lbDias");
                    Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                    Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                    Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                    Decimal minimo = 0;
                    Decimal meta = 0;
                    Decimal venta = Convert.ToDecimal(lbVenta.Text);

                    if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                    {
                        minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                        meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    }

                    if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                    if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                    if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                    if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
                }
            }
        }

        protected void gridF26_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var kpi = e.Row.FindControl("kpi") as Image;
                    Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                    Label lbDias = (Label)e.Row.FindControl("lbDias");
                    Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                    Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                    Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                    Decimal minimo = 0;
                    Decimal meta = 0;
                    Decimal venta = Convert.ToDecimal(lbVenta.Text);

                    if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                    {
                        minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                        meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    }

                    if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                    if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                    if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                    if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
                }
            }
        }

        protected void gridF27_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var kpi = e.Row.FindControl("kpi") as Image;
                    Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                    Label lbDias = (Label)e.Row.FindControl("lbDias");
                    Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                    Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                    Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                    Decimal minimo = 0;
                    Decimal meta = 0;
                    Decimal venta = Convert.ToDecimal(lbVenta.Text);

                    if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                    {
                        minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                        meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    }

                    if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                    if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                    if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                    if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
                }
            }
        }

        protected void gridF28_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var kpi = e.Row.FindControl("kpi") as Image;
                    Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                    Label lbDias = (Label)e.Row.FindControl("lbDias");
                    Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                    Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                    Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                    Decimal minimo = 0;
                    Decimal meta = 0;
                    Decimal venta = Convert.ToDecimal(lbVenta.Text);

                    if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                    {
                        minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                        meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    }

                    if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                    if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                    if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                    if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
                }
            }
        }
        
        protected void lbVendedor_Click(object sender, EventArgs e)
        {
            ViewState["Boton"] = "Detalle";
        }

        protected void gridF02_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF02.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF02.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F02", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();
                            
                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridVendedor_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                var kpi = e.Item.FindControl("kpi") as Image;
                Label lbVenta = (Label)e.Item.FindControl("lbVenta");
                Label lbDias = (Label)e.Item.FindControl("lbDias");
                Label lbMeta = (Label)e.Item.FindControl("lbMeta");
                Label lbMinimo = (Label)e.Item.FindControl("lbMinimo");
                Label lbTotalDias = (Label)e.Item.FindControl("lbTotalDias");

                Decimal minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                Decimal meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                Decimal venta = Convert.ToDecimal(lbVenta.Text);

                if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojoPeq.jpg";
                if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegroPeq.jpg";
                if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrellaPeq.jpg";
                if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blancoPeq.jpg";
            }
        }

        protected void gridF04_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF04.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF04.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F04", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF05_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF05.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF05.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F05", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF06_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF06.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF06.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F06", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF07_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF07.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF07.SelectedRow.FindControl("gridVendedor");
                    
                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F07", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF08_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF08.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF08.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F08", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF09_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF09.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF09.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F09", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF10_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF10.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF10.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F10", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF11_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF11.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF11.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F11", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF12_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF12.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF12.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F12", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF13_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF13.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF13.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F13", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF14_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF14.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF14.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F14", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF15_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var kpi = e.Row.FindControl("kpi") as Image;
                Label lbVenta = (Label)e.Row.FindControl("lbVenta");
                Label lbDias = (Label)e.Row.FindControl("lbDias");
                Label lbMeta = (Label)e.Row.FindControl("lbMeta");
                Label lbMinimo = (Label)e.Row.FindControl("lbMinimo");
                Label lbTotalDias = (Label)e.Row.FindControl("lbTotalDias");

                Decimal minimo = 0;
                Decimal meta = 0;
                Decimal venta = Convert.ToDecimal(lbVenta.Text);

                if (Convert.ToDecimal(lbTotalDias.Text) > 0)
                {
                    minimo = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMinimo.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                    meta = (Convert.ToDecimal(lbDias.Text) * Convert.ToDecimal(lbMeta.Text)) / Convert.ToDecimal(lbTotalDias.Text);
                }

                if (venta < minimo) kpi.ImageUrl = "~/imagenes/puntoRojo.jpg";
                if ((venta >= minimo) && (venta < meta)) kpi.ImageUrl = "~/imagenes/puntoNegro.jpg";
                if (venta >= meta) kpi.ImageUrl = "~/imagenes/estrella.jpg";
                if (Convert.ToDecimal(lbDias.Text) == 0) kpi.ImageUrl = "~/imagenes/blanco.jpg";
            }
        }

        protected void gridF15_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF15.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF15.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F15", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }
        protected void gridF16_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF16.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF16.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F16", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF17_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF17.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF17.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F17", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF18_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void gridF19_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF19.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF19.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F19", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF20_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF20.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF20.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F20", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF21_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF21.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF21.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F21", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF22_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF22.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF22.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F22", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF23_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF23.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF23.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F23", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF24_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF24.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF24.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F24", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF25_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF25.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF25.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F25", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF26_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF26.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF26.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F26", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }
        
        protected void gridF27_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF27.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF27.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F27", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }

        protected void gridF28_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridF28.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Detalle":
                    LinkButton lbVendedor = (LinkButton)rowSeleccionada.FindControl("lbVendedor");
                    Control ctl = gridF28.SelectedRow.FindControl("gridVendedor");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;

                        if (fileGrid.Items.Count > 0)
                        {
                            fileGrid.DataSource = null;
                            fileGrid.DataBind();
                        }
                        else
                        {
                            var ws = new wsFriedman.wsFriedman();
                            ws.Url = Convert.ToString(ViewState["url"]);
                            ws.Timeout = 999999999;

                            var q = ws.ListarVentasVendedorSemanas(Convert.ToInt32(semana.SelectedValue), Convert.ToInt32(anio.SelectedValue), "F28", Convert.ToInt32(txtSemanas.Text), lbVendedor.Text).ToArray();

                            fileGrid.DataSource = q.ToArray();
                            fileGrid.DataBind();
                        }
                    }

                    break;
            }
        }
    }
}