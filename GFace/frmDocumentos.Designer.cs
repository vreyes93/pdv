﻿namespace Transportistas
{
    partial class frmDocumentos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDocumentos));
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip16 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem16 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem16 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip17 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem17 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem17 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip18 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem18 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem18 = new DevExpress.Utils.ToolTipItem();
            this.gridEncabezado = new DevExpress.XtraGrid.GridControl();
            this.gridViewEncabezado = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridDetalle = new DevExpress.XtraGrid.GridControl();
            this.gridViewDetalle = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.imagenes = new System.Windows.Forms.ImageList();
            this.anio = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.mes = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.btnCargar = new DevExpress.XtraBars.BarButtonItem();
            this.btnFirmar = new DevExpress.XtraBars.BarButtonItem();
            this.empresa = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.agregarDocumento = new DevExpress.XtraBars.BarButtonItem();
            this.eliminarDocumento = new DevExpress.XtraBars.BarButtonItem();
            this.exportarEncabezado = new DevExpress.XtraBars.BarButtonItem();
            this.exportarDetalle = new DevExpress.XtraBars.BarButtonItem();
            this.descargarFirmas = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.cargarTodos = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.bk = new System.ComponentModel.BackgroundWorker();
            this.bkRecuperarFirmas = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.gridEncabezado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEncabezado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDetalle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetalle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // gridEncabezado
            // 
            this.gridEncabezado.Dock = System.Windows.Forms.DockStyle.Left;
            this.gridEncabezado.Location = new System.Drawing.Point(0, 95);
            this.gridEncabezado.MainView = this.gridViewEncabezado;
            this.gridEncabezado.Name = "gridEncabezado";
            this.gridEncabezado.Size = new System.Drawing.Size(960, 866);
            this.gridEncabezado.TabIndex = 0;
            this.gridEncabezado.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewEncabezado});
            // 
            // gridViewEncabezado
            // 
            this.gridViewEncabezado.GridControl = this.gridEncabezado;
            this.gridViewEncabezado.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridViewEncabezado.Name = "gridViewEncabezado";
            this.gridViewEncabezado.OptionsFind.SearchInPreview = true;
            this.gridViewEncabezado.PaintStyleName = "Web";
            this.gridViewEncabezado.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewEncabezado_FocusedRowChanged);
            // 
            // gridDetalle
            // 
            this.gridDetalle.Dock = System.Windows.Forms.DockStyle.Right;
            this.gridDetalle.Location = new System.Drawing.Point(924, 95);
            this.gridDetalle.MainView = this.gridViewDetalle;
            this.gridDetalle.Name = "gridDetalle";
            this.gridDetalle.Size = new System.Drawing.Size(960, 866);
            this.gridDetalle.TabIndex = 1;
            this.gridDetalle.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDetalle});
            // 
            // gridViewDetalle
            // 
            this.gridViewDetalle.GridControl = this.gridDetalle;
            this.gridViewDetalle.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridViewDetalle.Name = "gridViewDetalle";
            this.gridViewDetalle.PaintStyleName = "Web";
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Images = this.imagenes;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.anio,
            this.mes,
            this.btnCargar,
            this.btnFirmar,
            this.empresa,
            this.agregarDocumento,
            this.eliminarDocumento,
            this.exportarEncabezado,
            this.exportarDetalle,
            this.descargarFirmas,
            this.barButtonItem1,
            this.cargarTodos});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 14;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2});
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbonControl1.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.ShowOnMultiplePages;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(1884, 95);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // imagenes
            // 
            this.imagenes.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imagenes.ImageStream")));
            this.imagenes.TransparentColor = System.Drawing.Color.Transparent;
            this.imagenes.Images.SetKeyName(0, "Download.ico");
            this.imagenes.Images.SetKeyName(1, "Compose Email.ico");
            this.imagenes.Images.SetKeyName(2, "tratamiento.ico");
            this.imagenes.Images.SetKeyName(3, "menos.ico");
            this.imagenes.Images.SetKeyName(4, "excel3.ICO");
            this.imagenes.Images.SetKeyName(5, "excel.ICO");
            this.imagenes.Images.SetKeyName(6, "download.ico");
            this.imagenes.Images.SetKeyName(7, "door.png");
            this.imagenes.Images.SetKeyName(8, "refresh.ico");
            // 
            // anio
            // 
            this.anio.Caption = "Año:";
            this.anio.Edit = this.repositoryItemSpinEdit1;
            this.anio.EditValue = 2014;
            this.anio.EditWidth = 75;
            this.anio.Id = 1;
            this.anio.Name = "anio";
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.repositoryItemSpinEdit1.Mask.EditMask = "####";
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // mes
            // 
            this.mes.Caption = "Mes:";
            this.mes.Edit = this.repositoryItemComboBox1;
            this.mes.EditWidth = 150;
            this.mes.Id = 3;
            this.mes.Name = "mes";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.DropDownRows = 12;
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            this.repositoryItemComboBox1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // btnCargar
            // 
            this.btnCargar.Caption = "Cargar datos";
            this.btnCargar.Id = 4;
            this.btnCargar.ImageOptions.ImageIndex = 0;
            this.btnCargar.ImageOptions.LargeImageIndex = 0;
            this.btnCargar.Name = "btnCargar";
            this.btnCargar.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            toolTipTitleItem10.Text = "Cargar Datos";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Muestra los documentos correspondientes al año y mes asignados anteriormente";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.btnCargar.SuperTip = superToolTip10;
            this.btnCargar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCargar_ItemClick);
            // 
            // btnFirmar
            // 
            this.btnFirmar.Caption = "Firmar documentos";
            this.btnFirmar.Id = 5;
            this.btnFirmar.ImageOptions.ImageIndex = 1;
            this.btnFirmar.ImageOptions.LargeImageIndex = 1;
            this.btnFirmar.Name = "btnFirmar";
            this.btnFirmar.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            toolTipTitleItem11.Text = "Firmar documentos";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Genera las firmas de los documentos que no estén firmados";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.btnFirmar.SuperTip = superToolTip11;
            this.btnFirmar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnFirmar_ItemClick);
            // 
            // empresa
            // 
            this.empresa.Caption = "Cía.:";
            this.empresa.Edit = this.repositoryItemComboBox2;
            this.empresa.EditWidth = 150;
            this.empresa.Id = 6;
            this.empresa.Name = "empresa";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Items.AddRange(new object[] {
            "Empresas Concretas, S.A.",
            "Productos Múltiples, S.A."});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            this.repositoryItemComboBox2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // agregarDocumento
            // 
            this.agregarDocumento.Caption = "Agregar documento";
            this.agregarDocumento.Id = 7;
            this.agregarDocumento.ImageOptions.ImageIndex = 2;
            this.agregarDocumento.ImageOptions.LargeImageIndex = 2;
            this.agregarDocumento.Name = "agregarDocumento";
            this.agregarDocumento.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            toolTipTitleItem12.Text = "Agregar documento";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Agrega un documento (Factura o Nota de Crédito al listado para subir al GFACE)";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            this.agregarDocumento.SuperTip = superToolTip12;
            this.agregarDocumento.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.agregarDocumento_ItemClick);
            // 
            // eliminarDocumento
            // 
            this.eliminarDocumento.Caption = "Eliminar documento";
            this.eliminarDocumento.Id = 8;
            this.eliminarDocumento.ImageOptions.ImageIndex = 3;
            this.eliminarDocumento.ImageOptions.LargeImageIndex = 3;
            this.eliminarDocumento.Name = "eliminarDocumento";
            this.eliminarDocumento.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            toolTipTitleItem13.Text = "Eliminar documento";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "Eliminar el documento seleccionado";
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem13);
            this.eliminarDocumento.SuperTip = superToolTip13;
            this.eliminarDocumento.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.eliminarDocumento_ItemClick);
            // 
            // exportarEncabezado
            // 
            this.exportarEncabezado.Caption = "Exportar encabezado";
            this.exportarEncabezado.Id = 9;
            this.exportarEncabezado.ImageOptions.ImageIndex = 4;
            this.exportarEncabezado.ImageOptions.LargeImageIndex = 4;
            this.exportarEncabezado.Name = "exportarEncabezado";
            this.exportarEncabezado.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            toolTipTitleItem14.Text = "Exportar Encabezado";
            toolTipItem14.LeftIndent = 6;
            toolTipItem14.Text = "Exportar a Excel el encabezdo de los documentos";
            superToolTip14.Items.Add(toolTipTitleItem14);
            superToolTip14.Items.Add(toolTipItem14);
            this.exportarEncabezado.SuperTip = superToolTip14;
            this.exportarEncabezado.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.exportarEncabezado_ItemClick);
            // 
            // exportarDetalle
            // 
            this.exportarDetalle.Caption = "Exportar detalle";
            this.exportarDetalle.Id = 10;
            this.exportarDetalle.ImageOptions.ImageIndex = 5;
            this.exportarDetalle.ImageOptions.LargeImageIndex = 5;
            this.exportarDetalle.Name = "exportarDetalle";
            this.exportarDetalle.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            toolTipTitleItem15.Text = "Exportar detalle";
            toolTipItem15.LeftIndent = 6;
            toolTipItem15.Text = "Exportar a Excel el detalle de los documentos";
            superToolTip15.Items.Add(toolTipTitleItem15);
            superToolTip15.Items.Add(toolTipItem15);
            this.exportarDetalle.SuperTip = superToolTip15;
            this.exportarDetalle.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.exportarDetalle_ItemClick);
            // 
            // descargarFirmas
            // 
            this.descargarFirmas.Caption = "Descargar firmas de documentos";
            this.descargarFirmas.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.descargarFirmas.Id = 11;
            this.descargarFirmas.ImageOptions.ImageIndex = 6;
            this.descargarFirmas.ImageOptions.LargeImageIndex = 6;
            this.descargarFirmas.Name = "descargarFirmas";
            this.descargarFirmas.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            toolTipTitleItem16.Text = "Descargar firmas";
            toolTipItem16.LeftIndent = 6;
            toolTipItem16.Text = "Utilice esta opción para descargar las firmas de los documentos listados";
            superToolTip16.Items.Add(toolTipTitleItem16);
            superToolTip16.Items.Add(toolTipItem16);
            this.descargarFirmas.SuperTip = superToolTip16;
            this.descargarFirmas.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.descargarFirmas_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Salir del sistema";
            this.barButtonItem1.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItem1.Id = 12;
            this.barButtonItem1.ImageOptions.ImageIndex = 7;
            this.barButtonItem1.ImageOptions.LargeImageIndex = 7;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            toolTipTitleItem17.Text = "Salir";
            toolTipItem17.LeftIndent = 6;
            toolTipItem17.Text = "Haga clic aquí para salir del sistema";
            superToolTip17.Items.Add(toolTipTitleItem17);
            superToolTip17.Items.Add(toolTipItem17);
            this.barButtonItem1.SuperTip = superToolTip17;
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // cargarTodos
            // 
            this.cargarTodos.Caption = "Cargar todos los documentos";
            this.cargarTodos.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.cargarTodos.Id = 13;
            this.cargarTodos.ImageOptions.ImageIndex = 8;
            this.cargarTodos.ImageOptions.LargeImageIndex = 8;
            this.cargarTodos.Name = "cargarTodos";
            this.cargarTodos.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            toolTipTitleItem18.Text = "Cargar todos";
            toolTipItem18.LeftIndent = 6;
            toolTipItem18.Text = resources.GetString("toolTipItem18.Text");
            superToolTip18.Items.Add(toolTipTitleItem18);
            superToolTip18.Items.Add(toolTipItem18);
            this.cargarTodos.SuperTip = superToolTip18;
            this.cargarTodos.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.cargarTodos_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.ribbonPageGroup3,
            this.ribbonPageGroup7,
            this.ribbonPageGroup4,
            this.ribbonPageGroup5,
            this.ribbonPageGroup6});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Opciones";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.anio);
            this.ribbonPageGroup1.ItemLinks.Add(this.mes);
            this.ribbonPageGroup1.ItemLinks.Add(this.empresa);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Fechas y Empresa";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btnCargar);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnFirmar);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Procesar";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.agregarDocumento);
            this.ribbonPageGroup3.ItemLinks.Add(this.eliminarDocumento);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Documentos";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.ItemLinks.Add(this.cargarTodos);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.Text = "Seleccionar";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.exportarEncabezado);
            this.ribbonPageGroup4.ItemLinks.Add(this.exportarDetalle);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "Exportar a Excel";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.descargarFirmas);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "Descargar";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.Text = "Salir";
            // 
            // bk
            // 
            this.bk.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bk_DoWork);
            this.bk.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bk_RunWorkerCompleted);
            // 
            // bkRecuperarFirmas
            // 
            this.bkRecuperarFirmas.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bkRecuperarFirmas_DoWork);
            this.bkRecuperarFirmas.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bkRecuperarFirmas_RunWorkerCompleted);
            // 
            // frmDocumentos
            // 
            this.AccessibleDescription = "xxx";
            this.AccessibleName = "xxx";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1884, 961);
            this.Controls.Add(this.gridEncabezado);
            this.Controls.Add(this.gridDetalle);
            this.Controls.Add(this.ribbonControl1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmDocumentos";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "xxx";
            this.Text = "Documentos para GFACE";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmDocumentos_Load);
            this.Shown += new System.EventHandler(this.frmDocumentos_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.gridEncabezado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEncabezado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDetalle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetalle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridEncabezado;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewEncabezado;
        private DevExpress.XtraGrid.GridControl gridDetalle;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDetalle;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.BarEditItem anio;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarEditItem mes;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraBars.BarButtonItem btnCargar;
        private DevExpress.XtraBars.BarButtonItem btnFirmar;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarEditItem empresa;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private System.Windows.Forms.ImageList imagenes;
        private DevExpress.XtraBars.BarButtonItem agregarDocumento;
        private DevExpress.XtraBars.BarButtonItem eliminarDocumento;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem exportarEncabezado;
        private DevExpress.XtraBars.BarButtonItem exportarDetalle;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private DevExpress.XtraBars.BarButtonItem descargarFirmas;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.BarButtonItem cargarTodos;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private System.ComponentModel.BackgroundWorker bk;
        private System.ComponentModel.BackgroundWorker bkRecuperarFirmas;
    }
}