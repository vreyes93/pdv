﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.Utils;
using DevExpress.Data;
using DevExpress.XtraGrid;
using DevExpress.XtraEditors;
using System.Collections;

namespace Transportistas
{
    public partial class frmDocumentos : Form
    {
        bool vYaCargo = false;
        DataSet ds = new DataSet();
        public frmDocumentos()
        {
            InitializeComponent();
        }

        private void frmDocumentos_Load(object sender, EventArgs e)
        {
        }

        
        void CargarDocumentos(bool todos)
        {
            try
            {
                vYaCargo = false;
                string mMensaje = "";
                DataSet dsBlanco = new DataSet();
                
                wsGFace.wsGFace ws = new wsGFace.wsGFace();
                ws.Url = Globales.pUrl;

                ds = dsBlanco;
                ws.Timeout = 999999999;
                if (!ws.DevuelveDocumentos(this.AccessibleName, ref mMensaje, empresa.EditValue.ToString(), anio.EditValue.ToString(), mes.EditValue.ToString(), ref ds, todos))
                {
                    MessageBox.Show(mMensaje, Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                gridDetalle.DataSource = ds.Tables["Detalle"];
                gridEncabezado.DataSource = ds.Tables["Encabezado"];

                gridViewDetalle.OptionsView.ShowAutoFilterRow = true;
                gridViewEncabezado.OptionsView.ShowAutoFilterRow = true;

                gridViewDetalle.OptionsView.ShowFooter = true;
                gridViewEncabezado.OptionsView.ShowFooter = true;

                foreach (GridColumn column in gridViewEncabezado.Columns)
                {
                    column.MinWidth = column.GetBestWidth();
                    column.OptionsColumn.ReadOnly = true;

                    if (column.Name == "colDocumento")
                    {
                        column.SummaryItem.Tag = column.Name;
                        column.SummaryItem.FieldName = column.FieldName;
                        column.SummaryItem.DisplayFormat = "Cantidad = {0}";
                        column.SummaryItem.SummaryType = SummaryItemType.Count;

                        GridGroupSummaryItem total = new GridGroupSummaryItem
                        {
                            FieldName = column.Name,
                            SummaryType = SummaryItemType.Count,
                            DisplayFormat = "Cantidad = {0}",
                            ShowInGroupColumnFooter = column,
                            Tag = string.Format("Cantidad de {0}", column.Name.Replace("col", ""))
                        };
                        gridViewEncabezado.GroupSummary.Add(total);
                    }

                    if (column.Name == "colSubTotal" || column.Name == "colIVA" || column.Name == "colTotal")
                    {
                        column.SummaryItem.Tag = column.Name;
                        column.SummaryItem.FieldName = column.FieldName;
                        column.SummaryItem.DisplayFormat = "{0:###,###,###,###,##0.##}";
                        column.SummaryItem.SummaryType = SummaryItemType.Sum;

                        GridGroupSummaryItem total = new GridGroupSummaryItem
                        {
                            FieldName = column.Name,
                            SummaryType = SummaryItemType.Sum,
                            DisplayFormat = "{0:###,###,###,###,##0.##}",
                            ShowInGroupColumnFooter = column,
                            Tag = string.Format("Totales de {0}", column.Name.Replace("col", ""))
                        };
                        gridViewEncabezado.GroupSummary.Add(total);
                    }
                }

                foreach (GridColumn column in gridViewDetalle.Columns)
                {
                    column.MinWidth = column.GetBestWidth();
                    column.OptionsColumn.ReadOnly = true;

                    if (column.Name == "colDocumento")
                    {
                        column.SummaryItem.Tag = column.Name;
                        column.SummaryItem.FieldName = column.FieldName;
                        column.SummaryItem.DisplayFormat = "Cantidad = {0}";
                        column.SummaryItem.SummaryType = SummaryItemType.Count;

                        GridGroupSummaryItem total = new GridGroupSummaryItem
                        {
                            FieldName = column.Name,
                            SummaryType = SummaryItemType.Count,
                            DisplayFormat = "Cantidad = {0}",
                            ShowInGroupColumnFooter = column,
                            Tag = string.Format("Cantidad de {0}", column.Name.Replace("col", ""))
                        };
                        gridViewDetalle.GroupSummary.Add(total);
                    }

                    if (column.Name == "colSubTotal" || column.Name == "colIVA" || column.Name == "colTotal")
                    {
                        column.SummaryItem.Tag = column.Name;
                        column.SummaryItem.FieldName = column.FieldName;
                        column.SummaryItem.DisplayFormat = "{0:###,###,###,###,##0.##}";
                        column.SummaryItem.SummaryType = SummaryItemType.Sum;

                        GridGroupSummaryItem total = new GridGroupSummaryItem
                        {
                            FieldName = column.Name,
                            SummaryType = SummaryItemType.Sum,
                            DisplayFormat = "{0:###,###,###,###,##0.##}",
                            ShowInGroupColumnFooter = column,
                            Tag = string.Format("Totales de {0}", column.Name.Replace("col", ""))
                        };
                        gridViewDetalle.GroupSummary.Add(total);
                    }

                }

                vYaCargo = true;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al cargar los datos. {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCargar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CargarDocumentos(false);
        }

        private void btnFirmar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MessageBox.Show("¿Está seguro que desea firmar los documentos?", Globales.pSistema, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No) return;
            bk.RunWorkerAsync();
        }

        private void frmDocumentos_Shown(object sender, EventArgs e)
        {
            try
            {
                DateTime mFecha = DateTime.Now.Date.AddMonths(-1);

                anio.EditValue = mFecha.Year;
                empresa.EditValue = "Productos Múltiples, S.A.";

                switch (mFecha.Month)
                {
                    case 1:
                        mes.EditValue = "Enero";
                        break;
                    case 2:
                        mes.EditValue = "Febrero";
                        break;
                    case 3:
                        mes.EditValue = "Marzo";
                        break;
                    case 4:
                        mes.EditValue = "Abril";
                        break;
                    case 5:
                        mes.EditValue = "Mayo";
                        break;
                    case 6:
                        mes.EditValue = "Junio";
                        break;
                    case 7:
                        mes.EditValue = "Julio";
                        break;
                    case 8:
                        mes.EditValue = "Agosto";
                        break;
                    case 9:
                        mes.EditValue = "Septiembre";
                        break;
                    case 10:
                        mes.EditValue = "Octubre";
                        break;
                    case 11:
                        mes.EditValue = "Noviembre";
                        break;
                    default:
                        mes.EditValue = "Diciembre";
                        break;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al inicializar la opción. {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void agregarDocumento_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void eliminarDocumento_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void exportarEncabezado_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (gridViewEncabezado.DataRowCount == 0)
                {
                    MessageBox.Show("No hay datos para exportar en el encabezado.", Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                saveFileDialog1.Title = "Exportar encabezado de documentos";

                saveFileDialog1.InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString();
                saveFileDialog1.Filter = "xlsx files (*.xlsx)|*.xlsx";
                saveFileDialog1.FilterIndex = 1;
                saveFileDialog1.RestoreDirectory = true;

                if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string mArchivo = saveFileDialog1.FileName;
                    if (File.Exists(mArchivo)) File.Delete(mArchivo);

                    gridViewEncabezado.ExportToXlsx(mArchivo);

                    Process prExcel = new Process();
                    prExcel.StartInfo.FileName = "excel.exe";
                    prExcel.StartInfo.Arguments = "\"" + mArchivo + "\"";
                    prExcel.Start();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al exportar el encabezado a excel. {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void exportarDetalle_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (gridViewDetalle.DataRowCount == 0)
                {
                    MessageBox.Show("No hay datos para exportar en el detalle.", Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                saveFileDialog1.Title = "Exportar detalle de documentos";

                saveFileDialog1.InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString();
                saveFileDialog1.Filter = "xlsx files (*.xlsx)|*.xlsx";
                saveFileDialog1.FilterIndex = 1;
                saveFileDialog1.RestoreDirectory = true;

                if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string mArchivo = saveFileDialog1.FileName;
                    if (File.Exists(mArchivo)) File.Delete(mArchivo);

                    gridViewDetalle.ExportToXlsx(mArchivo);

                    Process prExcel = new Process();
                    prExcel.StartInfo.FileName = "excel.exe";
                    prExcel.StartInfo.Arguments = "\"" + mArchivo + "\"";
                    prExcel.Start();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al exportar el detalle a excel. {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void gridViewEncabezado_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            try
            {
                if (!vYaCargo) return;
                gridViewDetalle.ActiveFilterString = string.Format("[Documento] = '{0}'", gridViewEncabezado.GetFocusedRowCellValue("Documento"));
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al filtrar el detalle del documento actual. {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cargarTodos_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CargarDocumentos(true);
        }

        private void bk_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void bk_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (gridViewEncabezado.DataRowCount == 0)
                {
                    MessageBox.Show("No hay documentos para firmar.", Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                string mMensaje = ""; int ss = 0; int nn = 0; int tt = 0;
                wsGFace.wsGFace ws = new wsGFace.wsGFace();
                ws.Url = Globales.pUrl;

                for (int kk = 0; kk < gridViewEncabezado.DataRowCount; kk++)
                {
                    tt += 1;
                    gridViewEncabezado.FocusedRowHandle = kk;

                    string mYaFirmado = gridViewEncabezado.GetFocusedRowCellValue("Firmado").ToString();
                    string mDoc = gridViewEncabezado.GetFocusedRowCellValue("Documento").ToString();

                    if (mYaFirmado == "N")
                    {
                        DataSet dsFirma = new DataSet();
                        DataTable dtDetalle = new DataTable("Detalle");
                        DataTable dtEncabezado = new DataTable("Encabezado");

                        for (int ii = 0; ii < ds.Tables["Encabezado"].Columns.Count; ii++)
                        {
                            dtEncabezado.Columns.Add(new DataColumn(ds.Tables["Encabezado"].Columns[ii].ColumnName, ds.Tables["Encabezado"].Columns[ii].DataType));
                        }

                        for (int ii = 0; ii < ds.Tables["Detalle"].Columns.Count; ii++)
                        {
                            dtDetalle.Columns.Add(new DataColumn(ds.Tables["Detalle"].Columns[ii].ColumnName, ds.Tables["Detalle"].Columns[ii].DataType));
                        }

                        string mDocumento = gridViewEncabezado.GetFocusedRowCellValue("Documento").ToString();
                       
                        DataRow[] mRowsDetalle = ds.Tables["Detalle"].Select(string.Format("Documento = '{0}'", mDocumento, ""));
                        DataRow[] mRowsEncabezado = ds.Tables["Encabezado"].Select(string.Format("Documento = '{0}'", mDocumento, ""));

                        DataRow mRowEncabezado = dtEncabezado.NewRow();
                        for (int ii = 0; ii < dtEncabezado.Columns.Count; ii++)
                        {
                            mRowEncabezado[ii] = mRowsEncabezado[0][ii];
                        }
                        dtEncabezado.Rows.Add(mRowEncabezado);

                        for (int jj = 0; jj < mRowsDetalle.Length; jj++)
                        {
                            DataRow mRowDetalle = dtDetalle.NewRow();
                            for (int ii = 0; ii < dtDetalle.Columns.Count; ii++)
                            {
                                mRowDetalle[ii] = mRowsDetalle[0][ii];
                            }
                            dtDetalle.Rows.Add(mRowDetalle);
                        }

                        dsFirma.Tables.Add(dtDetalle);
                        dsFirma.Tables.Add(dtEncabezado);

                        string mFirmado = ""; string mFirma = "";
                        if (!ws.FirmarDocumento(this.AccessibleName, ref mMensaje, empresa.EditValue.ToString(), dsFirma, ref mFirmado, ref mFirma,"Firmar"))
                        {
                            //MessageBox.Show(mMensaje, Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            //return;
                        }

                        if (mFirmado == "S")
                        {
                            ss += 1;
                        }
                        else
                        {
                            nn += 1;
                        }

                        gridViewEncabezado.SetFocusedRowCellValue("Firma", mFirma);
                        gridViewEncabezado.SetFocusedRowCellValue("Error", mMensaje);
                        gridViewEncabezado.SetFocusedRowCellValue("Firmado", mFirmado);

                        gridViewEncabezado.UpdateCurrentRow();
                    }
                    else
                    {
                        nn += 1;
                    }
                }

                MessageBox.Show(string.Format("Proceso terminado.{0}{0}Total de documentos: {1}{0}Firmados: {2}{0}No firmados: {3}", System.Environment.NewLine, tt, ss, nn), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al generar la firma. {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Application.Exit();
        }

        private void descargarFirmas_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MessageBox.Show("¿Está seguro que desea recuperar las firmas de los documentos?", Globales.pSistema, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No) return;
            bkRecuperarFirmas.RunWorkerAsync();
        }

        private void bkRecuperarFirmas_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void bkRecuperarFirmas_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (gridViewEncabezado.DataRowCount == 0)
                {
                    MessageBox.Show("No hay documentos para recuperar firmas.", Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                string mMensaje = ""; int ss = 0; int nn = 0; int tt = 0;
                wsGFace.wsGFace ws = new wsGFace.wsGFace();
                ws.Url = Globales.pUrl;

                for (int kk = 0; kk < gridViewEncabezado.DataRowCount; kk++)
                {
                    
                    gridViewEncabezado.FocusedRowHandle = kk;

                    string mYaFirmado = gridViewEncabezado.GetFocusedRowCellValue("Firmado").ToString();
                    string mDoc = gridViewEncabezado.GetFocusedRowCellValue("Documento").ToString();

                    if (mYaFirmado == "N")
                    {
                        tt += 1;

                        DataSet dsFirma = new DataSet();
                        DataTable dtDetalle = new DataTable("Detalle");
                        DataTable dtEncabezado = new DataTable("Encabezado");

                        for (int ii = 0; ii < ds.Tables["Encabezado"].Columns.Count; ii++)
                        {
                            dtEncabezado.Columns.Add(new DataColumn(ds.Tables["Encabezado"].Columns[ii].ColumnName, ds.Tables["Encabezado"].Columns[ii].DataType));
                        }

                        for (int ii = 0; ii < ds.Tables["Detalle"].Columns.Count; ii++)
                        {
                            dtDetalle.Columns.Add(new DataColumn(ds.Tables["Detalle"].Columns[ii].ColumnName, ds.Tables["Detalle"].Columns[ii].DataType));
                        }

                        string mDocumento = gridViewEncabezado.GetFocusedRowCellValue("Documento").ToString();

                        DataRow[] mRowsDetalle = ds.Tables["Detalle"].Select(string.Format("Documento = '{0}'", mDocumento, ""));
                        DataRow[] mRowsEncabezado = ds.Tables["Encabezado"].Select(string.Format("Documento = '{0}'", mDocumento, ""));

                        DataRow mRowEncabezado = dtEncabezado.NewRow();
                        for (int ii = 0; ii < dtEncabezado.Columns.Count; ii++)
                        {
                            mRowEncabezado[ii] = mRowsEncabezado[0][ii];
                        }
                        dtEncabezado.Rows.Add(mRowEncabezado);

                        for (int jj = 0; jj < mRowsDetalle.Length; jj++)
                        {
                            DataRow mRowDetalle = dtDetalle.NewRow();
                            for (int ii = 0; ii < dtDetalle.Columns.Count; ii++)
                            {
                                mRowDetalle[ii] = mRowsDetalle[0][ii];
                            }
                            dtDetalle.Rows.Add(mRowDetalle);
                        }

                        dsFirma.Tables.Add(dtDetalle);
                        dsFirma.Tables.Add(dtEncabezado);

                        string mFirmado = ""; string mFirma = "";
                        if (!ws.FirmarDocumento(this.AccessibleName, ref mMensaje, empresa.EditValue.ToString(), dsFirma, ref mFirmado, ref mFirma,"RecuperarFirma"))
                        {
                            //MessageBox.Show(mMensaje, Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            //return;
                        }

                        if (mFirmado == "S")
                        {
                            ss += 1;
                        }
                        else
                        {
                            nn += 1;
                        }

                        gridViewEncabezado.SetFocusedRowCellValue("Firma", mFirma);
                        gridViewEncabezado.SetFocusedRowCellValue("Error", mMensaje);
                        gridViewEncabezado.SetFocusedRowCellValue("Firmado", mFirmado);

                        gridViewEncabezado.UpdateCurrentRow();
                    }
                }

                MessageBox.Show(string.Format("Proceso terminado.{0}{0}Total de documentos a recuperar firma: {1}{0}Firmas recuperadas: {2}{0}Firmas no recuperadas: {3}", System.Environment.NewLine, tt, ss, nn), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al generar la firma. {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
