﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LocalizacionSets
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.AcceptButton = btnGrabar;
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtFactura.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Debe ingresar un número de factura.", "Localización", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                
                string mMensaje = "";
                wsUtilitarios.wsUtilitarios ws = new wsUtilitarios.wsUtilitarios();
                ws.Url = "http://sql.fiesta.local/services/wsUtilitarios.asmx";
                
                if (!ws.LocalizacionFactura(txtFactura.Text.Trim(), ref mMensaje))
                {
                    MessageBox.Show(mMensaje, "Localización", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                MessageBox.Show(mMensaje, "Localización", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtFactura.Text = "";
                txtFactura.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                MessageBox.Show(string.Format("Error al grabar. {0}{1}{0}{2}", System.Environment.NewLine, ex.Message, m), "Localización", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            txtFactura.Text = "";
            txtFactura.Focus();
        }

    }
}
