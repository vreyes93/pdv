﻿namespace MF.Comun.Dto.Comision
{
    public class SincronizacionComisionDetalle
    {
        public long Sincronizacion { get; set; }
        public string Cobrador { get; set; }
        public string Vendedor { get; set; }
        public string Empleado { get; set; }
        public decimal Comision { get; set; }
        public string EstadoEmpleado { get; set; }
        public decimal? Monto { get; set; }
        public decimal? Total { get; set; }
        public bool Actualizado { get; set; }
    }
}
