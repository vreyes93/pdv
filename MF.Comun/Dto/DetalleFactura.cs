﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF.Comun.Dto
{
    public class DetalleFactura
    {
        public string NoFactura { get; set; }
        public string ArticuloId { get; set; }
        public string Articulo { get; set; }
        public string Despacho { get; set; }
        public DateTime FechaEntrega { get; set; }
    }
}
