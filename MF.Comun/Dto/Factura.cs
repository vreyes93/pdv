﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MF.Comun.Dto
{
    public class Factura
    {
        public string NoFactura { get; set; }
        public decimal Valor { get; set; }
        public decimal ValorNeto { get; set; }
        public DateTime FechaFactura { get; set; }
        public DateTime FechaEntrega { get; set; }

        //public string CodigoVendedor { get; set; }
        //public string NombreVendedor { get; set; }
        public Vendedor Vendedor { get; set; }
        public Tienda Tienda { get; set; }
        //public string Tienda { get; set; }
        public string NivelPrecio { get; set; }
        public string Envio { get; set; }
        public string CodigoCliente { get; set; }
    }


}
