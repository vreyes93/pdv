﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF.Comun.Dto
{

    /* Herencia de objetos
    public class FacturaConLineas : Factura
    {
        public List<FacturaLinea> Lineas { get; set; }
    }
    */

    /*Composicion de objetos
     * utilizar la segunda*/
    public class FacturaConLineas 
    {
        public Factura Factura { get; set; }
        public List<FacturaLinea> Lineas { get; set; }
    }
}
