﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF.Comun.Dto
{
   public class FacturaDetallada
    {
            public string cliente { get; set; }
            public string primer_nombre { get; set; }
            public string segundo_nombre { get; set; }
            public string primer_apellido { get; set; }
            public string segundo_apellido { get; set; }
            public string genero { get; set; }
            public string dpi { get; set; }
            public string telefono { get; set; }
            public string departamento { get; set; }
            public string municipio { get; set; }
            public string factura { get; set; }
            public DateTime fecha_ultima_factura { get; set; }
            public int ultima_cotizacion { get; set; }
            public DateTime fecha_ultima_cotizacion { get; set; }
            public string tienda { get; set; }
            public string vendedor { get; set; }
            public DateTime fecha_factura { get; set; }
            public decimal total_factura { get; set; }
            public string TipoVenta { get; set; }
            public string Financiera { get; set; }
            public string Nivel_Precio { get; set; }
            public bool pagada { get; set; }
            public int Transportista { get; set; }
            public string Vehiculo { get; set; }
            public string codArticulo { get; set; }
            public string NombreArticulo { get; set; }
            public string Pedido { get; set; }
            public bool factura_activa { get; set; }
            public DateTime fecha_entrega { get; set; }
        
    }
}
