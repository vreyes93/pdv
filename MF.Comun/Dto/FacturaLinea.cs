﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF.Comun.Dto
{
    public class FacturaLinea
    {
        public string NoFactura { get; set; }
        public int Linea { get; set; }
        public string Descripcion { get; set; }
        public string CodigoArticulo { get; set; }
    }
}
