﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF.Comun.Dto.Facturacion
{
    public class ArticuloClasificacion
    {
        public string Articulo { get; set; }
        public int C { get; set; }
        public string Clasificacion { get; set; }
    }
}
