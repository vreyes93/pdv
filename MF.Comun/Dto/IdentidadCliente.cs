﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF.Comun.Dto
{
   public  class IdentidadCliente
    {
        public string NombreCompleto { get; set; }
        public string Foto { get; set; }
        public string FotoHuella { get; set; }
        public string Huella { get; set; }
        public string Cliente { get; set; }
        public string DPI { get; set; }
        public PreSolicitud preSolicitud { get; set; }
        public IdentidadCliente(string cliente, string nombre,string foto,string fotoHuella,string huellaAcomparar)
        {
            Cliente = cliente;
            NombreCompleto = nombre;
            Foto = foto;
            FotoHuella = fotoHuella;
            Huella = huellaAcomparar;
            
        }
        public IdentidadCliente()
        { }

        public IdentidadCliente(string codCliente)
        {
            Cliente = codCliente;

        }

    }
}
