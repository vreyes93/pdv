﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF.Comun.Dto
{
  public  class LoteFacturas
    {
        
        public IEnumerable<Factura> lstFacturas { get; set; }
        public int Count() { return ((List<Factura>)lstFacturas).Count; }
       
        public LoteFacturas(List<Factura> lstFact)
        {
            lstFacturas = lstFact;
        }
    }
}
