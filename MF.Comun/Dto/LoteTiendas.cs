﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF.Comun.Dto
{
   public class LoteTiendas
    {
        public List<Tienda> LstTiendas { get; set; }
        public LoteTiendas()
        {
            LstTiendas = new List<Tienda>();
        }
    }
}
