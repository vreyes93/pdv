﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF.Comun.Dto
{
    public class PreSolicitud
    {
        public decimal? MontoTotal { get; set; }
        public DateTime? FechaSolicitud { get; set; }
        public decimal? Enganche { get; set; }
        public decimal? SaldoFinanciar { get; set; } //monto a financiar
        public int? CantPagos1 { get; set; }
        public int? CantPagos2 { get; set; }
        public decimal? MontoPagos1 { get; set; }
        public decimal? MontoPagos2 { get; set; }
        public string ObservacionesCredito { get; set; }
        public string NombreFinanciera { get; set; }
        public string NivelPrecio { get; set; }

        public PreSolicitud()
        { }
    }
}
