﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF.Comun.Dto
{
    public class RegistroFoto
    {
        public string Cliente { get; set; }
        public string Foto { get; set; }
        public string NombreCliente { get; set; }
       

        public RegistroFoto(string cliente, string foto)
        {
            Cliente = cliente;
            Foto = foto;
            
        }

        public RegistroFoto(string cliente, string foto, string nombre)
        {
            Cliente = cliente;
            Foto = foto;
            NombreCliente = nombre;
        }
        public RegistroFoto()
        { }
    }
}
