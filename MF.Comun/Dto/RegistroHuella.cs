﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF.Comun.Dto
{
    public class RegistroHuella
    {
        public string Cliente { get; set; }
        public string Huella { get; set; }
        public string FotoHuella { get; set; }
        public string NombreCliente { get; set; }
        public string UsuarioRegistro { get; set; }
        public string Tienda { get; set; }

        public RegistroHuella(string cliente, string huella,string nombreCompleto)
        {
            //Constructor del DTO para la huella digital
            Cliente = cliente;
            Huella = huella;
            NombreCliente = nombreCompleto;
        }
        public RegistroHuella(string cliente, string huella)
        {
            //Constructor del DTO para la huella digital
            Cliente = cliente;
            Huella = huella;
        }

        public RegistroHuella()
        {



        }
        
    }
}
