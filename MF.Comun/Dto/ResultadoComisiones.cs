﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF.Comun.Dto
{
   public class ResultadoComisiones
    {
        public string Resultado { get; set; }
        public string Motivo { get; set; }
        public decimal MontoComision { get; set; }

        public string Nombre { get; set; }
        public string Vendedor { get; set; }
        public string Empleado { get; set; }
        public string Tienda { get; set; }
        public ResultadoComisiones()
        { }
    }
}
