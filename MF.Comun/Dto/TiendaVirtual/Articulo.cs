﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF.Comun.Dto.TiendaVirtual
{
   public class Articulo
    {
        public int Linea { get; set; }
        public string Descripcion { get; set; }
        public string CodigoArticulo { get; set; }
        public decimal Descuento { get; set; }
        public decimal Precio { get; set; }
        public int cantidad { get; set; }
        public string CodigoPadre { get; set; }
        public string TipoLinea { get; set; }

        public Articulo()
        {
            TipoLinea = "N";
        }
    }
}
