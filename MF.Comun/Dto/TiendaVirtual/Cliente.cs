﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF.Comun.Dto
{
    /// <summary>
    /// Esta estructura es para el intercambio de información desde la tienda virtual hacia los registros
    /// de los pedidos y la facturación
    /// Contiene la información básica de un cliente para validar si existe o crear uno nuevo
    /// </summary>
    public class Cliente
    {
        public string codigo { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Email { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Genero { get; set; }
        public bool RecibirInfo { get; set; }
        public string NIT { get; set; }
        public string Identificacion { get; set; }
        public string Municipio { get; set; }
        public string Departamento { get; set; }
        public string Telefono { get; set; }
        public string Indicaciones { get; set; }
        public string Zona { get; set; }
        public string Casa { get; set; }
        public string Apartamento { get; set; }
        public string Calle_avenida { get; set; }
        public string Colonia { get; set; }
        public string FormaDePago { get; set; }

        public Cliente()
        {
            RecibirInfo = false;
        }
    }
}
