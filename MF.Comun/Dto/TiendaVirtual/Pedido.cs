﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF.Comun.Dto.TiendaVirtual
{
    public class Pedido
    {
        public string Codigo { get; set; }
        public string Usuario { get; set; }
        public DateTime FechaPedido { get; set; }
        public DateTime FechaEntrega { get; set; }
        public decimal TotalPedido { get; set; }
        public int TotalItems { get; set; }
        public string Tienda { get; set; }
        public decimal DescuentoGeneral { get; set; }
        public Cliente cliente { get; set; }
        public List<Articulo> articulos { get; set; }

    }
}
