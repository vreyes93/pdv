﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF.Comun.Dto
{
    public class Vendedor
    {
        public string VendedorId { get; set; }
        public string Nombre { get; set; }
        public string Telefono { get; set; }
    }

}
