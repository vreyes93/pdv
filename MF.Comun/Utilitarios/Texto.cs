﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF.Comun.Utilitarios
{
    public class Texto
    {

        public static string Formato(string cadena, string formato)
        {
            return string.Format("{0} {1}", cadena, formato);
        }
    }
}
