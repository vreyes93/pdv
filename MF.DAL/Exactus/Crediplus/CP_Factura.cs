//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MF.DAL.Exactus.Crediplus
{
    using System;
    using System.Collections.Generic;
    
    public partial class CP_Factura
    {
        public string FACTURA { get; set; }
        public string FACTURA_ELECTRONICA { get; set; }
        public string CLIENTE { get; set; }
        public System.DateTime FECHA { get; set; }
        public string FIRMADA { get; set; }
        public string FIRMA { get; set; }
        public string SERIE { get; set; }
        public string NUMERO { get; set; }
        public string IDENTIFICADOR { get; set; }
        public string MAQUINA { get; set; }
        public string NOMBRE { get; set; }
        public string DIRECCION { get; set; }
        public string TELEFONO { get; set; }
        public string ERROR { get; set; }
        public string XML_DOCUMENTO { get; set; }
        public string XML_REVERSION { get; set; }
        public string XML_RESPUESTA { get; set; }
        public System.DateTime FECHA_REGISTRO { get; set; }
        public int ID_CREDIPLUS { get; set; }
        public string CLIENTE_CREDIPLUS { get; set; }
        public string CREDITO_CREDIPLUS { get; set; }
        public string NOMBRE_CREDIPLUS { get; set; }
        public decimal VALOR_CREDIPLUS { get; set; }
        public decimal IVA_CREDIPLUS { get; set; }
        public decimal TOTAL_CREDIPLUS { get; set; }
        public System.DateTime FECHA_REGISTRO_CREDIPLUS { get; set; }
        public string ANULADA_ELECTRONICA { get; set; }
        public string XML_RESPUESTA_ANULACION { get; set; }
        public Nullable<System.DateTime> FECHA_HORA_ANULACION { get; set; }
    }
}
