//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MF.DAL.Exactus.Crediplus
{
    using System;
    using System.Collections.Generic;
    
    public partial class FACTURA
    {
        public FACTURA()
        {
            this.FACTURA_LINEA = new HashSet<FACTURA_LINEA>();
        }
    
        public string TIPO_DOCUMENTO { get; set; }
        public string FACTURA1 { get; set; }
        public Nullable<int> AUDIT_TRANS_INV { get; set; }
        public string ESTA_DESPACHADO { get; set; }
        public string EN_INVESTIGACION { get; set; }
        public string TRANS_ADICIONALES { get; set; }
        public string ESTADO_REMISION { get; set; }
        public string ASIENTO_DOCUMENTO { get; set; }
        public decimal DESCUENTO_VOLUMEN { get; set; }
        public string MONEDA_FACTURA { get; set; }
        public string COMENTARIO_CXC { get; set; }
        public System.DateTime FECHA_DESPACHO { get; set; }
        public string CLASE_DOCUMENTO { get; set; }
        public System.DateTime FECHA_RECIBIDO { get; set; }
        public string PEDIDO { get; set; }
        public string FACTURA_ORIGINAL { get; set; }
        public string TIPO_ORIGINAL { get; set; }
        public decimal COMISION_COBRADOR { get; set; }
        public string TARJETA_CREDITO { get; set; }
        public decimal TOTAL_VOLUMEN { get; set; }
        public string NUMERO_AUTORIZA { get; set; }
        public decimal TOTAL_PESO { get; set; }
        public decimal MONTO_COBRADO { get; set; }
        public decimal TOTAL_IMPUESTO1 { get; set; }
        public System.DateTime FECHA { get; set; }
        public System.DateTime FECHA_ENTREGA { get; set; }
        public decimal TOTAL_IMPUESTO2 { get; set; }
        public decimal PORC_DESCUENTO2 { get; set; }
        public decimal MONTO_FLETE { get; set; }
        public decimal MONTO_SEGURO { get; set; }
        public decimal MONTO_DOCUMENTACIO { get; set; }
        public string TIPO_DESCUENTO1 { get; set; }
        public string TIPO_DESCUENTO2 { get; set; }
        public decimal MONTO_DESCUENTO1 { get; set; }
        public decimal MONTO_DESCUENTO2 { get; set; }
        public decimal PORC_DESCUENTO1 { get; set; }
        public decimal TOTAL_FACTURA { get; set; }
        public System.DateTime FECHA_PEDIDO { get; set; }
        public Nullable<System.DateTime> FECHA_HORA_ANULA { get; set; }
        public Nullable<System.DateTime> FECHA_ORDEN { get; set; }
        public decimal TOTAL_MERCADERIA { get; set; }
        public decimal COMISION_VENDEDOR { get; set; }
        public string ORDEN_COMPRA { get; set; }
        public System.DateTime FECHA_HORA { get; set; }
        public decimal TOTAL_UNIDADES { get; set; }
        public short NUMERO_PAGINAS { get; set; }
        public decimal TIPO_CAMBIO { get; set; }
        public string ANULADA { get; set; }
        public string MODULO { get; set; }
        public string CARGADO_CG { get; set; }
        public string CARGADO_CXC { get; set; }
        public string EMBARCAR_A { get; set; }
        public string DIREC_EMBARQUE { get; set; }
        public string DIRECCION_FACTURA { get; set; }
        public short MULTIPLICADOR_EV { get; set; }
        public string OBSERVACIONES { get; set; }
        public string RUBRO1 { get; set; }
        public string RUBRO2 { get; set; }
        public string RUBRO3 { get; set; }
        public string RUBRO4 { get; set; }
        public string RUBRO5 { get; set; }
        public int VERSION_NP { get; set; }
        public string MONEDA { get; set; }
        public string NIVEL_PRECIO { get; set; }
        public string COBRADOR { get; set; }
        public string RUTA { get; set; }
        public string USUARIO { get; set; }
        public string USUARIO_ANULA { get; set; }
        public string CONDICION_PAGO { get; set; }
        public string ZONA { get; set; }
        public string VENDEDOR { get; set; }
        public string DOC_CREDITO_CXC { get; set; }
        public string CLIENTE_DIRECCION { get; set; }
        public string CLIENTE_CORPORAC { get; set; }
        public string CLIENTE_ORIGEN { get; set; }
        public string CLIENTE { get; set; }
        public string PAIS { get; set; }
        public Nullable<short> SUBTIPO_DOC_CXC { get; set; }
        public string TIPO_CREDITO_CXC { get; set; }
        public string TIPO_DOC_CXC { get; set; }
        public Nullable<decimal> MONTO_ANTICIPO { get; set; }
        public Nullable<decimal> TOTAL_PESO_NETO { get; set; }
        public System.DateTime FECHA_RIGE { get; set; }
        public string CONTRATO { get; set; }
        public Nullable<decimal> PORC_INTCTE { get; set; }
        public string USA_DESPACHOS { get; set; }
        public string COBRADA { get; set; }
        public string DESCUENTO_CASCADA { get; set; }
        public string DIRECCION_EMBARQUE { get; set; }
        public string CONSECUTIVO { get; set; }
        public int REIMPRESO { get; set; }
        public string DIVISION_GEOGRAFICA1 { get; set; }
        public string DIVISION_GEOGRAFICA2 { get; set; }
        public Nullable<decimal> BASE_IMPUESTO1 { get; set; }
        public Nullable<decimal> BASE_IMPUESTO2 { get; set; }
        public string NOMBRE_CLIENTE { get; set; }
        public string DOC_FISCAL { get; set; }
        public string NOMBREMAQUINA { get; set; }
        public string SERIE_RESOLUCION { get; set; }
        public Nullable<int> CONSEC_RESOLUCION { get; set; }
        public string GENERA_DOC_FE { get; set; }
        public string TASA_IMPOSITIVA { get; set; }
        public Nullable<decimal> TASA_IMPOSITIVA_PORC { get; set; }
        public string TASA_CREE1 { get; set; }
        public Nullable<decimal> TASA_CREE1_PORC { get; set; }
        public string TASA_CREE2 { get; set; }
        public Nullable<decimal> TASA_CREE2_PORC { get; set; }
        public Nullable<decimal> TASA_GAN_OCASIONAL_PORC { get; set; }
        public string CONTRATO_AC { get; set; }
        public byte NoteExistsFlag { get; set; }
        public System.DateTime RecordDate { get; set; }
        public System.Guid RowPointer { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public System.DateTime CreateDate { get; set; }
    
        public virtual CLIENTE CLIENTE1 { get; set; }
        public virtual CLIENTE CLIENTE2 { get; set; }
        public virtual CLIENTE CLIENTE3 { get; set; }
        public virtual CLIENTE CLIENTE4 { get; set; }
        public virtual COBRADOR COBRADOR1 { get; set; }
        public virtual DOCUMENTOS_CC DOCUMENTOS_CC { get; set; }
        public virtual ICollection<FACTURA_LINEA> FACTURA_LINEA { get; set; }
    }
}
