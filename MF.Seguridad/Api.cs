﻿using System;
using RestSharp;
using System.Net;
using System.Threading.Tasks;
using System.Web;

namespace MF.Seguridad
{
    [Serializable]
    internal class Api
    {
        private string UrlApi { get; set; }
        private RestClient Client { get; set; }
        private RestRequest Request { get; set; }
        private HttpContext Context = HttpContext.Current;

        public Api(string urlapi)
        {
            this.UrlApi = urlapi;
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, errors) => true;
        }

        public IRestResponse Process(Method requestmethod, string apipart, object body)
        {
            this.Client = new RestClient(this.UrlApi);
            this.Request = new RestRequest(apipart, requestmethod);
            this.Request.Timeout = 0;
            this.Request.AddHeader("Token", Context.Session["Token"].ToString());

            if (requestmethod != Method.GET)
                this.Request.AddJsonBody(body);

            return Task.Run(() => this.Client.Execute(this.Request)).Result;
        }
    }
}
