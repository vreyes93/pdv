﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Configuration;
using System.Net;
using System.Text;
using System.Web;

namespace MF.Seguridad
{
    [Serializable]
    public class Autenticacion
    {
        private HttpContext Context = HttpContext.Current;
        private string Servidor = ConfigurationManager.AppSettings["ServidorAutenticacion"];
        private string Api = ConfigurationManager.AppSettings["ApiAutenticacion"];
        private string Appkey = ConfigurationManager.AppSettings["AppkeyAutenticacion"];
        private string Endpoint = ConfigurationManager.AppSettings["EndpointAutenticacion"];
        private string EndpointExpira = ConfigurationManager.AppSettings["EndpointExpira"];
        private string AppCapturador = ConfigurationManager.AppSettings["AppCapturador"];
        private string AppBase = ConfigurationManager.AppSettings["AppBase"];
        private static string CookieName = ConfigurationManager.AppSettings["CookieName"];

        public void login()
        {
            Uri uri = new Uri(Context.Request.Url.AbsoluteUri);
            Uri appUri = new Uri(new Uri(AppBase), uri.AbsolutePath);

            string url = ToUrlParameter(appUri.AbsoluteUri);
            System.Web.HttpCookie cookie = Context.Request.Cookies[CookieName];
            string virtualUrl = VirtualPathUtility.ToAbsolute("~/");
            string responseUrl = ToUrlParameter(AppCapturador);

            if ((string.IsNullOrEmpty(Servidor) || string.IsNullOrWhiteSpace(Servidor)) ||
                (string.IsNullOrEmpty(Api) || string.IsNullOrWhiteSpace(Api)) ||
                (string.IsNullOrEmpty(Endpoint) || string.IsNullOrWhiteSpace(Endpoint)) ||
                (string.IsNullOrEmpty(Appkey) || string.IsNullOrWhiteSpace(Appkey)) ||
                (string.IsNullOrEmpty(CookieName) || string.IsNullOrWhiteSpace(CookieName)))
                throw new Exception("Servidor de autenticación no configurado para esta aplicación");

            if (cookie == null)
                Context.Response.Redirect(string.Format(Servidor, Appkey, url, responseUrl));
            else
            {
                Context.Session.Add("Token", cookie.Value);
                IRestResponse response = new Api(Api).Process(Method.POST, Endpoint, null);
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception(response.Content);

                UserInfo userinfo = JsonConvert.DeserializeObject<UserInfo>(response.Content);

                Context.Session.Add("UserInfo", response.Content);
                Context.Session.Add("Tienda", userinfo.Store);
            }
        }

        public void Cookie(string token, bool toDelete)
        {
            if (string.IsNullOrEmpty(EndpointExpira) || string.IsNullOrWhiteSpace(EndpointExpira))
                throw new Exception("Servidor de autenticación no configurado para esta aplicación");

            Context.Session.Add("Token", token);
            AuthResult data = new AuthResult();

            if (!toDelete)
            {
                IRestResponse response = new Api(Api).Process(Method.POST, EndpointExpira, null);
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception(response.Content);

                data = JsonConvert.DeserializeObject<AuthResult>(response.Content);
            }

            Context.Session["Expired"] = data.Expire;

            System.Web.HttpCookie cookie = new System.Web.HttpCookie(CookieName);
            cookie.Value = token;
            cookie.Expires = toDelete ? DateTime.Today.AddDays(-2) : data.Expire;
            Context.Response.Cookies.Add(cookie);

            if (!toDelete)
            {
                CookieTienda(false, data.StoreCode);
                Context.Response.Redirect(data.Redirect);
            }
        }

        public static void CookieTienda(bool toDelete, string storeCode = null)
        {
            HttpContext Context = HttpContext.Current;
            DateTime expired = (DateTime?)Context.Session["Expired"] ?? new DateTime();

            System.Web.HttpCookie cookie = new System.Web.HttpCookie("Tienda");
            cookie.Value = storeCode == null ? Context.Session["Tienda"].ToString() : storeCode;
            cookie.Expires = toDelete ? DateTime.Today.AddDays(-2) : expired;
            Context.Response.Cookies.Add(cookie);
        }

        public static bool CheckCookie()
        {
            HttpContext Context = HttpContext.Current;

            return Context.Request.Cookies.Get(CookieName) != null;
        }

        public void UpdateCookie()
        {
            try
            {
                System.Web.HttpCookie cookieMueblesFiesta = Context.Request.Cookies.Get(CookieName);
                System.Web.HttpCookie cookieTienda = Context.Request.Cookies.Get("Tienda");
                DateTime renewTime = DateTime.Now.AddMinutes(MF_Clases.General.RenewCookieExpire);
                Context.Response.Cookies.Add(cookieMueblesFiesta);
                cookieMueblesFiesta.Expires = renewTime;

                if (cookieTienda != null)
                {
                    cookieTienda.Expires = renewTime;
                    Context.Response.Cookies.Add(cookieTienda);
                }
            }
            catch { }
        }

        private static string ToUrlParameter(string value)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(value);
            return Convert.ToBase64String(bytes).Replace("+", "-").Replace("/", "_");
        }
    }
}