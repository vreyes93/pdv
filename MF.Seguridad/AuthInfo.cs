﻿namespace MF.Seguridad
{
    public class AuthInfo
    {
        public string AppKey { get; set; }
        public string UrlRedirect { get; set; }
        public string UrlResponse { get; set; }
    }
}
