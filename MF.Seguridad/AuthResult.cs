﻿using System;

namespace MF.Seguridad
{
    [Serializable]
    public class AuthResult
    {
        public string Username { get; set; }
        public string Sellercode { get; set; }
        public string StoreCode { get; set; }
        public string CompleteName { get; set; }
        public string Redirect { get; set; }
        public DateTime Expire { get; set; }
        public Guid AccessToken { get; set; }
    }
}
