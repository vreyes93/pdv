﻿using System;

namespace MF.Seguridad
{
    [Serializable]
    public class UserInfo
    {
        public string ExactusUsername { get; set; }
        public string Store { get; set; }
        public string StoreName { get; set; }
        public string Seller { get; set; }
        public string CompleteName { get; set; }
        public string Email { get; set; }
    }
}
