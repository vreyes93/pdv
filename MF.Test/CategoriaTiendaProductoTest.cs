﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MF_Clases.Utils;
using System;

namespace MF.Test
{
    [TestClass]
    public class CategoriaTiendaProductoTest
    {
        private string ProductoRequiereArmado = "111455-028";
        private string ProductoNoRequiereArmado = "511447-007";
        private string ProductoNoConfArmado = "A001030023001-011";

        private string TiendaA = "F05";
        private string TiendaB = "F02";
        private string TiendaC = "F39";
        private string TiendaNoConf = "B05";

        private string TiendaA_1 = "F09";
        private string TiendaB_1 = "F07";
        private string TiendaC_1 = "F17";
        private string TiendaNoConf_1 = "B19";

        private string ProductoA = "153403-057";
        private string ProductoB = "511447-007";
        private string ProductoC = "511450-068";
        private string ProductoNoConf = "153612-062";

        [TestMethod]
        public void RequiereArmado()
        {
            Assert.IsTrue(Validar.RequiereArmado(ProductoRequiereArmado));
        }

        [TestMethod]
        public void NoRequiereArmado()
        {
            Assert.IsFalse(Validar.RequiereArmado(ProductoNoRequiereArmado));
        }

        [TestMethod]
        public void ArmadoNoConfigurado()
        {
            Assert.IsFalse(Validar.RequiereArmado(ProductoNoConfArmado));
        }

        [TestMethod]
        public void TiendaAproductoA()
        {
            Assert.IsTrue(Validar.CompatibilidadArmadoEntreTiendaProducto(TiendaA, ProductoA));
        }

        [TestMethod]
        public void TiendaAproductoB()
        {
            Assert.IsTrue(Validar.CompatibilidadArmadoEntreTiendaProducto(TiendaA, ProductoB));
        }

        [TestMethod]
        public void TiendaAproductoC()
        {
            Assert.IsTrue(Validar.CompatibilidadArmadoEntreTiendaProducto(TiendaA, ProductoC));
        }

        [TestMethod]
        public void TiendaBproductoA()
        {
            Assert.IsFalse(Validar.CompatibilidadArmadoEntreTiendaProducto(TiendaB, ProductoA));
        }

        [TestMethod]
        public void TiendaBproductoB()
        {
            Assert.IsTrue(Validar.CompatibilidadArmadoEntreTiendaProducto(TiendaB, ProductoB));
        }

        [TestMethod]
        public void TiendaBproductoC()
        {
            Assert.IsTrue(Validar.CompatibilidadArmadoEntreTiendaProducto(TiendaB, ProductoC));
        }

        [TestMethod]
        public void TiendaCproductoA()
        {
            Assert.IsFalse(Validar.CompatibilidadArmadoEntreTiendaProducto(TiendaC, ProductoA));
        }

        [TestMethod]
        public void TiendaCproductoB()
        {
            Assert.IsFalse(Validar.CompatibilidadArmadoEntreTiendaProducto(TiendaC, ProductoB));
        }

        [TestMethod]
        public void TiendaCproductoC()
        {
            Assert.IsTrue(Validar.CompatibilidadArmadoEntreTiendaProducto(TiendaC, ProductoC));
        }

        [TestMethod]
        public void TraspasoBodegaA_A_ProductoA()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaA, TiendaA_1, ProductoA, "");
        }

        [TestMethod]
        public void TraspasoBodegaA_A_ProductoB()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaA, TiendaA_1, ProductoB, "");
        }

        [TestMethod]
        public void TraspasoBodegaA_A_ProductoC()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaA, TiendaA_1, ProductoC, "");
        }

        [TestMethod]
        public void TraspasoBodegaA_B_ProductoA()
        {
            Assert.ThrowsException<Exception>(() =>
            {
                Validar.CompatibilidadTraspasoTiendaProducto(TiendaA, TiendaB, ProductoA, "");
            });
        }

        [TestMethod]
        public void TraspasoBodegaA_B_ProductoB()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaA, TiendaB, ProductoB, "");
        }

        [TestMethod]
        public void TraspasoBodegaA_B_ProductoC()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaA, TiendaB, ProductoC, "");
        }

        [TestMethod]
        public void TraspasoBodegaA_C_ProductoA()
        {
            Assert.ThrowsException<Exception>(() =>
            {
                Validar.CompatibilidadTraspasoTiendaProducto(TiendaA, TiendaC, ProductoA, "");
            });
        }

        [TestMethod]
        public void TraspasoBodegaA_C_ProductoB()
        {
            Assert.ThrowsException<Exception>(() =>
            {
                Validar.CompatibilidadTraspasoTiendaProducto(TiendaA, TiendaC, ProductoB, "");
            });
        }

        [TestMethod]
        public void TraspasoBodegaA_C_ProductoC()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaA, TiendaC, ProductoC, "");
        }

        [TestMethod]
        public void TraspasoBodegaB_B_ProductoA()
        {
            Assert.ThrowsException<Exception>(() =>
            {
                Validar.CompatibilidadTraspasoTiendaProducto(TiendaB, TiendaB_1, ProductoA, "");
            });
        }

        [TestMethod]
        public void TraspasoBodegaB_B_ProductoB()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaB, TiendaB_1, ProductoB, "");
        }

        [TestMethod]
        public void TraspasoBodegaB_B_ProductoC()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaB, TiendaB_1, ProductoC, "");
        }

        [TestMethod]
        public void TraspasoBodegaB_C_ProductoA()
        {
            Assert.ThrowsException<Exception>(() =>
            {
                Validar.CompatibilidadTraspasoTiendaProducto(TiendaB, TiendaC, ProductoA, "");
            });
        }

        [TestMethod]
        public void TraspasoBodegaB_C_ProductoB()
        {
            Assert.ThrowsException<Exception>(() =>
            {
                Validar.CompatibilidadTraspasoTiendaProducto(TiendaB, TiendaC, ProductoB, "");
            });
        }

        [TestMethod]
        public void TraspasoBodegaB_C_ProductoC()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaB, TiendaC, ProductoC, "");
        }

        [TestMethod]
        public void TraspasoBodegaC_C_ProductoA()
        {
            Assert.ThrowsException<Exception>(() =>
            {
                Validar.CompatibilidadTraspasoTiendaProducto(TiendaC, TiendaC_1, ProductoA, "");
            });
        }

        [TestMethod]
        public void TraspasoBodegaC_C_ProductoB()
        {
            Assert.ThrowsException<Exception>(() =>
            {
                Validar.CompatibilidadTraspasoTiendaProducto(TiendaC, TiendaC_1, ProductoB, "");
            });
        }

        [TestMethod]
        public void TraspasoBodegaC_C_ProductoC()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaC, TiendaC_1, ProductoC, "");
        }

        [TestMethod]
        public void TraspasoBodegaA_A_ProductoNoConf()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaA, TiendaA_1, ProductoNoConf, "");
        }

        [TestMethod]
        public void TraspasoBodegaA_B_ProductoNoConf()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaA, TiendaB, ProductoNoConf, "");
        }

        [TestMethod]
        public void TraspasoBodegaA_C_ProductoNoConf()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaA, TiendaC, ProductoNoConf, "");
        }

        [TestMethod]
        public void TraspasoBodegaB_B_ProductoNoConf()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaB, TiendaB_1, ProductoNoConf, "");
        }

        [TestMethod]
        public void TraspasoBodegaB_C_ProductoNoConf()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaB, TiendaC, ProductoNoConf, "");
        }

        [TestMethod]
        public void TraspasoBodegaC_C_ProductoNoConf()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaC, TiendaC_1, ProductoNoConf, "");
        }

        [TestMethod]
        public void TraspasoF01_A_ProductoA()
        {
            Validar.CompatibilidadTraspasoTiendaProducto("F01", TiendaA, ProductoA, "");
        }

        [TestMethod]
        public void TraspasoF01_A_ProductoB()
        {
            Validar.CompatibilidadTraspasoTiendaProducto("F01", TiendaA, ProductoB, "");
        }

        [TestMethod]
        public void TraspasoF01_A_ProductoC()
        {
            Validar.CompatibilidadTraspasoTiendaProducto("F01", TiendaA, ProductoC, "");
        }

        [TestMethod]
        public void TraspasoF01_B_ProductoA()
        {
            Assert.ThrowsException<Exception>(() => {
                Validar.CompatibilidadTraspasoTiendaProducto("F01", TiendaB, ProductoA, "");
            });
        }

        [TestMethod]
        public void TraspasoF01_B_ProductoB()
        {
            Validar.CompatibilidadTraspasoTiendaProducto("F01", TiendaB, ProductoB, "");
        }

        [TestMethod]
        public void TraspasoF01_B_ProductoC()
        {
            Validar.CompatibilidadTraspasoTiendaProducto("F01", TiendaB, ProductoC, "");
        }

        [TestMethod]
        public void TraspasoF01_C_ProductoA()
        {
            Assert.ThrowsException<Exception>(() => {
                Validar.CompatibilidadTraspasoTiendaProducto("F01", TiendaC, ProductoA, "");
            });
        }

        [TestMethod]
        public void TraspasoF01_C_ProductoB()
        {
            Assert.ThrowsException<Exception>(() => {
                Validar.CompatibilidadTraspasoTiendaProducto("F01", TiendaC, ProductoB, "");
            });
        }

        [TestMethod]
        public void TraspasoF01_C_ProductoC()
        {
            Validar.CompatibilidadTraspasoTiendaProducto("F01", TiendaC, ProductoC, "");
        }

        [TestMethod]
        public void TraspasoA_F01_ProductoA()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaA, "F01", ProductoA, "");
        }

        [TestMethod]
        public void TraspasoA_F01_ProductoB()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaA, "F01", ProductoB, "");
        }

        [TestMethod]
        public void TraspasoA_F01_ProductoC()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaA, "F01", ProductoC, "");
        }

        [TestMethod]
        public void TraspasoB_F01_ProductoA()
        {
            Assert.ThrowsException<Exception>(() => {
                Validar.CompatibilidadTraspasoTiendaProducto(TiendaB, "F01", ProductoA, "");
            });
        }

        [TestMethod]
        public void TraspasoB_F01_ProductoB()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaB, "F01", ProductoB, "");
        }

        [TestMethod]
        public void TraspasoB_F01_ProductoC()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaB, "F01", ProductoC, "");
        }

        [TestMethod]
        public void TraspasoC_F01_ProductoA()
        {
            Assert.ThrowsException<Exception>(() => {
                Validar.CompatibilidadTraspasoTiendaProducto(TiendaC, "F01", ProductoA, "");
            });
        }

        [TestMethod]
        public void TraspasoC_F01_ProductoB()
        {
            Assert.ThrowsException<Exception>(() => {
                Validar.CompatibilidadTraspasoTiendaProducto(TiendaC, "F01", ProductoB, "");
            });
        }

        [TestMethod]
        public void TraspasoC_F01_ProductoC()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaC, "F01", ProductoC, "");
        }

        [TestMethod]
        public void TraspasoNCF_CF_ProductoA()
        {
            Assert.ThrowsException<Exception>(() => {
                Validar.CompatibilidadTraspasoTiendaProducto(TiendaNoConf, TiendaC, ProductoA, "");
            });
        }

        [TestMethod]
        public void TraspasoNCF_CF_ProductoB()
        {
            Assert.ThrowsException<Exception>(() => {
                Validar.CompatibilidadTraspasoTiendaProducto(TiendaNoConf, TiendaC, ProductoB, "");
            });
        }

        [TestMethod]
        public void TraspasoNCF_CF_ProductoC()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaNoConf, TiendaC, ProductoC, "");
        }

        [TestMethod]
        public void TraspasoCF_NCF_ProductoA()
        {
            Assert.ThrowsException<Exception>(() => {
                Validar.CompatibilidadTraspasoTiendaProducto(TiendaC, TiendaNoConf, ProductoA, "");
            });
        }

        [TestMethod]
        public void TraspasoCF_NCF_ProductoB()
        {
            Assert.ThrowsException<Exception>(() => {
                Validar.CompatibilidadTraspasoTiendaProducto(TiendaC, TiendaNoConf, ProductoB, "");
            });
        }

        [TestMethod]
        public void TraspasoCF_NCF_ProductoC()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaC, TiendaNoConf, ProductoC, "");
        }

        [TestMethod]
        public void TraspasoNCF_NCF_ProductoA()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaNoConf, TiendaNoConf_1, ProductoA, "");
        }

        [TestMethod]
        public void TraspasoNCF_NCF_ProductoB()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaNoConf, TiendaNoConf_1, ProductoB, "");
        }

        [TestMethod]
        public void TraspasoNCF_NCF_ProductoC()
        {
            Validar.CompatibilidadTraspasoTiendaProducto(TiendaNoConf, TiendaNoConf_1, ProductoC, "");
        }
    }
}
