﻿using MF_Clases.Comun;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;

namespace MF_Clases
{
    [Serializable]
    public static class Clases
    {
        [Serializable]
        public class Articulos
        {
            public string Articulo { get; set; }
            public string Descripcion { get; set; }
            public decimal PrecioBase { get; set; }
            public decimal PrecioLista { get; set; }
            public string Tooltip { get; set; }
            public int Existencias { get; set; }
            public string Ubicaciones { get; set; }
        }

        [Serializable]
        public class ExistenciaBodega
        {
            public string Articulo { get; set; }
            public string Bodega { get; set; }
            public int Cantidad { get; set; }
        }

        [Serializable]
        public class DespachosMayoreo
        {
            public string Despachos { get; set; }
            public string Marca { get; set; }
            public string Tipo { get; set; }
            public string Modelo { get; set; }
            public int MA0008_Imperial { get; set; }
            public int MA0008_Semi { get; set; }
            public int MA0008_Matri { get; set; }
            public int MA0008_Queen { get; set; }
            public int MA0008_King { get; set; }
            public int MA0008_Total { get; set; }
            public int MA0007_Imperial { get; set; }
            public int MA0007_Semi { get; set; }
            public int MA0007_Matri { get; set; }
            public int MA0007_Queen { get; set; }
            public int MA0007_King { get; set; }
            public int MA0007_Total { get; set; }
            public int MA0011_Imperial { get; set; }
            public int MA0011_Semi { get; set; }
            public int MA0011_Matri { get; set; }
            public int MA0011_Queen { get; set; }
            public int MA0011_King { get; set; }
            public int MA0011_Total { get; set; }
            public int MA0012_Imperial { get; set; }
            public int MA0012_Semi { get; set; }
            public int MA0012_Matri { get; set; }
            public int MA0012_Queen { get; set; }
            public int MA0012_King { get; set; }
            public int MA0012_Total { get; set; }
            public int MA0005_Imperial { get; set; }
            public int MA0005_Semi { get; set; }
            public int MA0005_Matri { get; set; }
            public int MA0005_Queen { get; set; }
            public int MA0005_King { get; set; }
            public int MA0005_Total { get; set; }
            public int MA0016_Imperial { get; set; }
            public int MA0016_Semi { get; set; }
            public int MA0016_Matri { get; set; }
            public int MA0016_Queen { get; set; }
            public int MA0016_King { get; set; }
            public int MA0016_Total { get; set; }
            public int MA0017_Imperial { get; set; }
            public int MA0017_Semi { get; set; }
            public int MA0017_Matri { get; set; }
            public int MA0017_Queen { get; set; }
            public int MA0017_King { get; set; }
            public int MA0017_Total { get; set; }
            public int Total { get; set; }
            public int pctjTotal { get; set; }
            public int porcentaje { get; set; }
        }

        [Serializable]
        public class PolizasAtid
        {
            public string Poliza { get; set; }
            public DateTime FechaOperacion { get; set; }
            public string NumeroCredito { get; set; }
            public string CuentaContable { get; set; }
            public string NombreCuentaContable { get; set; }
            public string Descripcion { get; set; }
            public string MovimientoTransaccion { get; set; }
            public decimal Valor { get; set; }
            public string AgenciaTransaccion { get; set; }
            public string TipoTransaccion { get; set; }
        }

        [Serializable]
        public class DepositosAtid
        {
            public int id { get; set; }
            public DateTime fechaIngreso { get; set; }
            public DateTime fechaAplicacion { get; set; }
            public string cuentaEmision { get; set; }
            public string numeroBoleta { get; set; }
            public string descripcion { get; set; }
            public string descripcionTransaccion { get; set; }
            public decimal valor { get; set; }
        }

        [Serializable]
        public class DesembolsosAtid
        {
            public int id { get; set; }
            public DateTime fechaDesembolso { get; set; }
            public string cuentaEmision { get; set; }
            public string cuentaDestino { get; set; }
            public string numeroTransferencia { get; set; }
            public decimal valorDesembolso { get; set; }
        }

        [Serializable]
        public class ContabilidadAtid
        {
            public List<PolizasAtid> contabilidad { get; set; }
            public List<DepositosAtid> depositos { get; set; }
            public List<DesembolsosAtid> desembolsos { get; set; }

            public ContabilidadAtid()
            {
                contabilidad = new List<PolizasAtid>();
                depositos = new List<DepositosAtid>();
                desembolsos = new List<DesembolsosAtid>();
            }
        }

        [Serializable]
        public class DetalleFacturaAtid
        {
            public int id { get; set; }
            public int linea { get; set; }
            public int idFactura { get; set; }
            public string producto { get; set; }
            public string descripcion { get; set; }
            public string descripcionFechaCubierta { get; set; }
            public decimal valor { get; set; }
            public decimal iva { get; set; }
            public decimal total { get; set; }
        }

        [Serializable]
        public class FacturasAtid_MF : FacturasAtid
        {
            public List<DetalleFacturaConCosto> detalleFacturaCosto { get; set; }
            public FacturasAtid_MF()
            {
                detalleFacturaCosto = new List<DetalleFacturaConCosto>();
            }
        }

        [Serializable]
        public class DetalleFacturaConCosto : DetalleFacturaAtid
        {
            public decimal costo { get; set; }
            public string productoMF { get; set; }
        }

        [Serializable]
        public class FacturasAtid
        {
            public int id { get; set; }
            public string cliente { get; set; }
            public string dpi { get; set; }
            public string nit { get; set; }
            public string credito { get; set; }
            public DateTime fechaNacimiento { get; set; }
            public string sexo { get; set; }
            public string estadoCivil { get; set; }
            public string eMail { get; set; }
            public string nombrecliente { get; set; }
            public string primerNombre { get; set; }
            public string segundoNombre { get; set; }
            public string tercerNombre { get; set; }
            public string primerApellido { get; set; }
            public string segundoApellido { get; set; }
            public string apellidoCasada { get; set; }
            public string direccion { get; set; }
            public string municipio { get; set; }
            public string departamento { get; set; }
            public DateTime fechaOperacion { get; set; }
            public DateTime fechaRegistro { get; set; }
            public string anulada { get; set; }
            public DateTime? fechaAnulacion { get; set; }
            public List<DetalleFacturaAtid> detalleFactura { get; set; }
        }

        [Serializable]
        public class RespuestaGuateFacturas
        {
            public string firmada { get; set; }
            public string firma { get; set; }
            public string serie { get; set; }
            public string numero { get; set; }
            public string identificador { get; set; }
            public string maquina { get; set; }
            public string factura { get; set; }
            public string facturaElectronica { get; set; }
            public string nombre { get; set; }
            public string direccion { get; set; }
            public string telefono { get; set; }
            public string error { get; set; }
            public string xml { get; set; }
        }

        [Serializable]
        public class ImpresionFacturaDetalle
        {
            public string NoFactura { get; set; }
            public string Articulo { get; set; }
            public int Cantidad { get; set; }
            public string Descripcion { get; set; }
            public decimal PrecioUnitario { get; set; }
            public decimal PrecioTotal { get; set; }
        }

        [Serializable]
        public class ImpresionFactura
        {
            public string NoFactura { get; set; }
            public DateTime Fecha { get; set; }
            public string Cliente { get; set; }
            public string Nombre { get; set; }
            public string Nit { get; set; }
            public string Direccion { get; set; }
            public string Vendedor { get; set; }
            public decimal Monto { get; set; }
            public string MontoLetras { get; set; }
            public string Observaciones { get; set; }
            public string Moneda { get; set; }
            public List<ImpresionFacturaDetalle> detalleFactura { get; set; }

            public ImpresionFactura()
            {
                detalleFactura = new List<ImpresionFacturaDetalle>();
            }
        }

        [Serializable]
        public class TiposReferencia
        {
            public string Codigo { get; set; }
            public string Descripcion { get; set; }
        }

        public class Solicitud
        {
            public DateTime? FechaSolicitud { get; set; }
            public string ClienteMF { get; set; }
            public Int64 NumeroSolicitud { get; set; }
            public Int64 NumeroPagare { get; set; }
            public string SolicitudTexto { get; set; }
            public string PagareTexto { get; set; }
            public string PrimerNombre { get; set; }
            public string SegundoNombre { get; set; }
            public string TercerNombre { get; set; }
            public string PrimerApellido { get; set; }
            public string SegundoApellido { get; set; }
            public string ApellidoCasada { get; set; }
            public string DPI { get; set; }
            public string Nit { get; set; }
            public string FechaNacimiento { get; set; }
            public string Genero { get; set; }
            public string Profesion { get; set; }
            public string Nacionalidad { get; set; }
            public string EstadoCivil { get; set; }
            public string Email { get; set; }
            public string Direccion { get; set; }
            public string Municipio { get; set; }
            public string Departamento { get; set; }
            public string Telefono { get; set; }
            public string Celular { get; set; }
            public string TipoVivienda { get; set; }
            public string Empresa { get; set; }
            public string Puesto { get; set; }
            public string FechaIngreso { get; set; }
            public string JefePrimerNombre { get; set; }
            public string JefeSegundoNombre { get; set; }
            public string JefeTercerNombre { get; set; }
            public string JefePrimerApellido { get; set; }
            public string JefeSegundoApellido { get; set; }
            public string JefeApellidoCasada { get; set; }
            public string Ingresos { get; set; }
            public string Egresos { get; set; }
            public string EmpresaTelefono { get; set; }
            public string EmpresaExtension { get; set; }
            public string EmpresaDireccion { get; set; }
            public string EmpresaMunicipio { get; set; }
            public string EmpresaDepartamento { get; set; }
            public string Familiar1PrimerNombre { get; set; }
            public string Familiar1SegundoNombre { get; set; }
            public string Familiar1TercerNombre { get; set; }
            public string Familiar1PrimerApellido { get; set; }
            public string Familiar1SegundoApellido { get; set; }
            public string Familiar1ApellidoCasada { get; set; }
            public string Familiar1Celular { get; set; }
            public string Familiar1Tipo { get; set; }
            public string Familiar1TipoDescripcion { get; set; }
            public string Familiar1Empresa { get; set; }
            public string Familiar1EmpresaDireccion { get; set; }
            public string Familiar1EmpresaDepartamento { get; set; }
            public int Familiar1EmpresaDepartamentoCodigo { get; set; }
            public string Familiar1EmpresaMunicipio { get; set; }
            public int Familiar1EmpresaMunicipioCodigo { get; set; }
            public string Familiar1EmpresaTelefono { get; set; }
            public string Familiar1CasaDireccion { get; set; }
            public string Familiar1CasaDepartamento { get; set; }
            public int Familiar1CasaDepartamentoCodigo { get; set; }
            public string Familiar1CasaMunicipio { get; set; }
            public int Familiar1CasaMunicipioCodigo { get; set; }
            public string Familiar1CasaTelefono { get; set; }
            public string Familiar2PrimerNombre { get; set; }
            public string Familiar2SegundoNombre { get; set; }
            public string Familiar2TercerNombre { get; set; }
            public string Familiar2PrimerApellido { get; set; }
            public string Familiar2SegundoApellido { get; set; }
            public string Familiar2ApellidoCasada { get; set; }
            public string Familiar2Celular { get; set; }
            public string Familiar2Tipo { get; set; }
            public string Familiar2TipoDescripcion { get; set; }
            public string Familiar2Empresa { get; set; }
            public string Familiar2EmpresaDireccion { get; set; }
            public string Familiar2EmpresaDepartamento { get; set; }
            public int Familiar2EmpresaDepartamentoCodigo { get; set; }
            public string Familiar2EmpresaMunicipio { get; set; }
            public int Familiar2EmpresaMunicipioCodigo { get; set; }
            public string Familiar2EmpresaTelefono { get; set; }
            public string Familiar2CasaDireccion { get; set; }
            public string Familiar2CasaDepartamento { get; set; }
            public int Familiar2CasaDepartamentoCodigo { get; set; }
            public string Familiar2CasaMunicipio { get; set; }
            public int Familiar2CasaMunicipioCodigo { get; set; }
            public string Familiar2CasaTelefono { get; set; }
            public string Personal1PrimerNombre { get; set; }
            public string Personal1SegundoNombre { get; set; }
            public string Personal1TercerNombre { get; set; }
            public string Personal1PrimerApellido { get; set; }
            public string Personal1SegundoApellido { get; set; }
            public string Personal1ApellidoCasada { get; set; }
            public string Personal1Celular { get; set; }
            public string Personal1Tipo { get; set; }
            public string Personal1TipoDescripcion { get; set; }
            public string Personal1Empresa { get; set; }
            public string Personal1EmpresaDireccion { get; set; }
            public string Personal1EmpresaDepartamento { get; set; }
            public int Personal1EmpresaDepartamentoCodigo { get; set; }
            public string Personal1EmpresaMunicipio { get; set; }
            public int Personal1EmpresaMunicipioCodigo { get; set; }
            public string Personal1EmpresaTelefono { get; set; }
            public string Personal1CasaDireccion { get; set; }
            public string Personal1CasaDepartamento { get; set; }
            public int Personal1CasaDepartamentoCodigo { get; set; }
            public string Personal1CasaMunicipio { get; set; }
            public int Personal1CasaMunicipioCodigo { get; set; }
            public string Personal1CasaTelefono { get; set; }
            public string Personal2PrimerNombre { get; set; }
            public string Personal2SegundoNombre { get; set; }
            public string Personal2TercerNombre { get; set; }
            public string Personal2PrimerApellido { get; set; }
            public string Personal2SegundoApellido { get; set; }
            public string Personal2ApellidoCasada { get; set; }
            public string Personal2Celular { get; set; }
            public string Personal2Tipo { get; set; }
            public string Personal2TipoDescripcion { get; set; }
            public string Personal2Empresa { get; set; }
            public string Personal2EmpresaDireccion { get; set; }
            public string Personal2EmpresaDepartamento { get; set; }
            public int Personal2EmpresaDepartamentoCodigo { get; set; }
            public string Personal2EmpresaMunicipio { get; set; }
            public int Personal2EmpresaMunicipioCodigo { get; set; }
            public string Personal2EmpresaTelefono { get; set; }
            public string Personal2CasaDireccion { get; set; }
            public string Personal2CasaDepartamento { get; set; }
            public int Personal2CasaDepartamentoCodigo { get; set; }
            public string Personal2CasaMunicipio { get; set; }
            public int Personal2CasaMunicipioCodigo { get; set; }
            public string Personal2CasaTelefono { get; set; }
            public decimal PrecioDelBien { get; set; }
            public decimal Enganche { get; set; }
            public decimal SaldoFinanciar { get; set; }
            public decimal Intereses { get; set; }
            public decimal Monto { get; set; }
            public string Plazo { get; set; }
            public decimal Cuota { get; set; }
            public decimal UltimaCuota { get; set; }
            public string Cuotas { get; set; }
            public string CuotaFormato { get; set; }
            public string UltimaCuotaFormato { get; set; }
            public string Articulo { get; set; }
            public string Sucursal { get; set; }
            public string Vendedor { get; set; }
            public string Fecha { get; set; }
            public string ConyuguePrimerNombre { get; set; }
            public string ConyugueSegundoNombre { get; set; }
            public string ConyugueTercerNombre { get; set; }
            public string ConyuguePrimerApellido { get; set; }
            public string ConyugueSegundoApellido { get; set; }
            public string ConyugueApellidoCasada { get; set; }
            public string ConyugueDPI { get; set; }
            public string ConyugueNIT { get; set; }
            public string ConyugueFechaNacimiento { get; set; }
            public string ConyugueCelular { get; set; }
            public string ConyugueEmpresa { get; set; }
            public string ConyuguePuesto { get; set; }
            public string ConyugueIngresos { get; set; }
            public string ConyugueEgresos { get; set; }
            public string ConyugueTelefono { get; set; }
            public string NombreCompleto { get; set; }
            public string Edad { get; set; }
            public string SaldoFinanciarLetras { get; set; }
            public string DireccionLetras { get; set; }
            public string PlazoLetras { get; set; }
            public string FechaVence { get; set; }
            public string FechaVenceLetras { get; set; }
            public string CuotasLetras { get; set; }
            public string UltimaCuotaLetras { get; set; }
            public string FechaInicialLetras { get; set; }
            public string Tasa { get; set; }
            public string TasaLetras { get; set; }
            public string DireccionFinanciera { get; set; }
            public string SaldoFinanciarFormato { get; set; }
            public string CuotasMenosUno { get; set; }
            public string CuotasMenosUnoLetras { get; set; }
            public int CodigoProfesion { get; set; }
            public int CodigoPuesto { get; set; }
            public int CodigoMunicipio { get; set; }
            public int CodigoDepartamento { get; set; }
            public int CodigoCanton { get; set; }
            public int CodigoMunicipioEmpresa { get; set; }
            public int CodigoDepartamentoEmpresa { get; set; }
            public int CodigoCantonEmpresa { get; set; }
            public int Dependientes { get; set; }
            public string Politico { get; set; }
            public string NoCreditoATID { get; set; }
            public string DireccionATID { get; set; }
            public string Usuario { get; set; }
            public string Tienda { get; set; }
            public DateTime FechaPagare { get; set; }
            public DateTime FechaPrimerPago { get; set; }
            public string FechaInicioLabores { get; set; }
            public string SueldoLetras { get; set; }
            public string FechaLetras { get; set; }
            public string Documento { get; set; }
            public DateTime FechaDocumento { get; set; }
            public string NumeroCredito { get; set; }
            public Byte[] Foto { get; set; }
            public Byte[] Huella { get; set; }
        }

        [Serializable]
        public class EstadoSolicitudATID
        {
            public string Solicitud { get; set; }
            public string Cliente { get; set; }
            public string Documento { get; set; }
            public string Factura { get; set; }
            public string Nombre { get; set; }
            public string Descripcion { get; set; }
            public decimal Valor { get; set; }
            public decimal Enganche { get; set; }
            public decimal ValorBien { get; set; }
            public string Autorizacion { get; set; }
            public string UsuarioAutorizo { get; set; }
            public string Estado { get; set; }
            public string PrimerNombre { get; set; }
            public string SegundoNombre { get; set; }
            public string TercerNombre { get; set; }
            public string PrimerApellido { get; set; }
            public string SegundoApellido { get; set; }
            public string ApellidoCasada { get; set; }
            public string Plazo { get; set; }
            public decimal Cuota { get; set; }
            public decimal UltimaCuota { get; set; }
            public string NombreEjecutivoIniciador { get; set; }
            public string EmailEjecutivoIniciador { get; set; }
        }

        [Serializable]
        public class FacturaAnular
        {
            public string Tienda { get; set; }
            public string Factura { get; set; }
            public DateTime Fecha { get; set; }
            public string Cliente { get; set; }
            public string Nombre { get; set; }
            public decimal Monto { get; set; }
            public string Recibos { get; set; }
            public string Despachos { get; set; }
            public string Observaciones { get; set; }
            public string Usuario { get; set; }
            public bool SoloDespachos { get; set; }
        }

        [Serializable]
        public class DocumentoCC
        {
            public DateTime Fecha { get; set; }
            public string Tipo { get; set; }
            public string Cliente { get; set; }
            public string Documento { get; set; }
            public string Descripcion { get; set; }
            public decimal Cargos { get; set; }
            public decimal Abonos { get; set; }
            public decimal Saldo { get; set; }
            public string Recibos { get; set; }
            public string Despachos { get; set; }
        }

        [Serializable]
        public class FacturasAnular
        {
            public List<FacturaAnular> Anular { get; set; }
            public List<DespachosAnular> CC { get; set; }

            public FacturasAnular()
            {
                Anular = new List<FacturaAnular>();
                CC = new List<DespachosAnular>();
            }
        }

        [Serializable]
        public class DespachosAnular
        {
            public string Factura { get; set; }
            public DateTime Fecha_Despacho { get; set; }
            public DateTime Fecha_entrega { get; set; }
            public string Despacho { get; set; }
            public string Direccion { get; set; }
            public string DeTienda { get; set; }
            public string Usuario { get; set; }
            public string Articulos { get; set; }
        }

        [Serializable]
        public class DiasDepartamentos
        {
            public int Dia { get; set; }
            public string Tipo { get; set; }
            public string Departamento { get; set; }
            public bool Lunes { get; set; }
            public bool Martes { get; set; }
            public bool Miercoles { get; set; }
            public bool Jueves { get; set; }
            public bool Viernes { get; set; }
            public bool Sabado { get; set; }
            public bool Domingo { get; set; }
            public string Tooltip { get; set; }
        }

        [Serializable]
        public class DiasMunicipios
        {
            public int Dia { get; set; }
            public string Tipo { get; set; }
            public string Departamento { get; set; }
            public string Municipio { get; set; }
            public bool Lunes { get; set; }
            public bool Martes { get; set; }
            public bool Miercoles { get; set; }
            public bool Jueves { get; set; }
            public bool Viernes { get; set; }
            public bool Sabado { get; set; }
            public bool Domingo { get; set; }
            public string Tooltip { get; set; }
        }

        [Serializable]
        public class DiasDespachos
        {
            public string Usuario { get; set; }
            public List<DiasDepartamentos> Departamentos { get; set; }
            public List<DiasMunicipios> Municipios { get; set; }

            public DiasDespachos()
            {
                Departamentos = new List<DiasDepartamentos>();
                Municipios = new List<DiasMunicipios>();
            }
        }

        [Serializable]
        public class DevolucionDetalle
        {
            public string Factura { get; set; }
            public int Linea { get; set; }
            public string Articulo { get; set; }
            public string Descripcion { get; set; }
            public string Bodega { get; set; }
            public int Cantidad { get; set; }
            public int CantidadDespachada { get; set; }
            public int CantidadDevolver { get; set; }
        }

        [Serializable]
        public class Devolucion
        {
            public string Tienda { get; set; }
            public string Factura { get; set; }
            public DateTime Fecha { get; set; }
            public string Cliente { get; set; }
            public string Nombre { get; set; }
            public decimal Monto { get; set; }
            public string Observaciones { get; set; }
            public string Usuario { get; set; }
            public string Mensaje { get; set; }
            public List<DevolucionDetalle> Detalle { get; set; }

            public Devolucion()
            {
                Detalle = new List<DevolucionDetalle>();
            }
        }

        [Serializable]
        public class Bodegas
        {
            public string Bodega { get; set; }
            public string Nombre { get; set; }
        }

        [Serializable]
        public class Usuarios
        {
            public string Usuario { get; set; }
            public string Nombre { get; set; }
        }

        [Serializable]
        public class BoletaConfirmar
        {
            public Int64 Boleta { get; set; }
            public DateTime Fecha { get; set; }
            public string Banco { get; set; }
            public decimal Monto { get; set; }
            public string Vendedor { get; set; }
        }

        [Serializable]
        public class ArticuloInventario
        {
            public int OrdenClasificacion { get; set; }
            public string Clasificacion { get; set; }
            public string Articulo { get; set; }
            public string Descripcion { get; set; }
            public DateTime UltimaCompra { get; set; }
            public string Blanco1 { get; set; }
            public Int32 Existencias { get; set; }
            public string Blanco2 { get; set; }
            public Int32 ExistenciaFisica { get; set; }
            public string Conteo { get; set; }
            public string Observaciones { get; set; }
            public string Tipo { get; set; }
            public int Diferencia { get; set; }
            public string AgrupacionEspecial { get; set; }
        }

        [Serializable]
        public class TomaInventario
        {
            public Int32 Mes { get; set; }
            public Int32 Anio { get; set; }
            public string Tipo { get; set; }
            public string Bodega { get; set; }
            public string Usuario { get; set; }
            public string Mensaje { get; set; }
            public string Html { get; set; }
            public string Asunto { get; set; }
            public bool ConsultaCierre { get; set; }
            public string CorreoUsuario { get; set; }
            public DateTime Fecha { get; set; }
            public List<string> Destinatarios { get; set; }
            public List<ArticuloInventario> Articulos { get; set; }
            public List<ArticuloInventario> ArticulosCodigo { get; set; }
            public List<ArticuloInventario> ArticulosClasificacion { get; set; }
            public List<ArticuloInventario> Diferencias { get; set; }

            public TomaInventario()
            {
                Mes = 0;
                Anio = 0;
                Tipo = "";
                Bodega = "";
                Usuario = "";
                Mensaje = "";
                Html = "";
                Asunto = "";
                ConsultaCierre = false;
                CorreoUsuario = "";
                Destinatarios = new List<string>();
                Articulos = new List<ArticuloInventario>();
                Diferencias = new List<ArticuloInventario>();
            }
        }

        [Serializable]
        public class AjustesInventario
        {
            public string Bodega { get; set; }
            public string Nombre { get; set; }
            public string Estado { get; set; }
        }

        [Serializable]
        public class RetornaUsuarioBodega
        {
            public List<UsuarioBodega> UsuarioBodega { get; set; }
            public List<RetornaExito> Exito { get; set; }
            public List<Bodegas> Bodegas { get; set; }
            public List<Usuarios> Usuarios { get; set; }

            public RetornaUsuarioBodega()
            {
                UsuarioBodega = new List<UsuarioBodega>();
                Exito = new List<RetornaExito>();
                Bodegas = new List<Bodegas>();
                Usuarios = new List<Usuarios>();
            }
        }

        [Serializable]
        public class LibroCompras
        {
            public string Documento { get; set; }
            public string Serie { get; set; }
            public string Numero { get; set; }
            public DateTime Fecha { get; set; }
            public string NIT { get; set; }
            public string Nombre { get; set; }
            public string Columna { get; set; }
            public decimal IVA { get; set; }
            public decimal TotalDocumento { get; set; }
            public string Modulo { get; set; }
            public string BienServicio { get; set; }
            public string Proveedor { get; set; }
            public string CajaChica { get; set; }
            public int ValeCH { get; set; }
            public int Vale { get; set; }
            public string Doc { get; set; }
        }

        [Serializable]
        public class LibroComprasMod
        {
            public string Usuario { get; set; }
            public List<LibroCompras> Borrar { get; set; }
            public List<LibroCompras> BienServicio { get; set; }
            public List<LibroCompras> PC { get; set; }
            public List<LibroCompras> FE { get; set; }
            public List<LibroCompras> Editar { get; set; }

            public LibroComprasMod()
            {
                Usuario = "";
                Borrar = new List<LibroCompras>();
                BienServicio = new List<LibroCompras>();
                PC = new List<LibroCompras>();
                FE = new List<LibroCompras>();
                Editar = new List<LibroCompras>();
            }
        }

        [Serializable]
        public class UsuarioBodega
        {
            public string ID { get; set; }
            public string Usuario { get; set; }
            public string Nombre { get; set; }
            public string Bodega { get; set; }
        }

        [Serializable]
        public class Traspasos
        {
            public string Traspaso { get; set; }
            public DateTime Fecha { get; set; }
            public int Linea { get; set; }
            public string Articulo { get; set; }
            public string Descripcion { get; set; }
            public string Localizacion { get; set; }
            public string BodegaOrigen { get; set; }
            public string BodegaDestino { get; set; }
            public Int32 Cantidad { get; set; }
            public string Usuario { get; set; }
            public string NombreUsuario { get; set; }
            public DateTime CreateDate { get; set; }
            public string NombreRecibe { get; set; }
            public string Consecutivo { get; set; }
            public string Tooltip { get; set; }
            public string Mayoreo { get; set; }
            public string SKU { get; set; }
        }

        [Serializable]
        public class TraspasoImpresion
        {
            public List<Traspasos> Traspaso { get; set; }

            public TraspasoImpresion()
            {
                Traspaso = new List<Traspasos>();
            }
        }

        [Serializable]
        public class MinimosMetas
        {
            public string Tienda { get; set; }
            public string Vendedor { get; set; }
            public string NombreVendedor { get; set; }
            public DateTime FechaInicial { get; set; }
            public DateTime FechaFinal { get; set; }
            public Int32 Periodo { get; set; }
            public decimal Minimo { get; set; }
            public decimal Meta { get; set; }
            public string Usuario { get; set; }
            public decimal PctjMinimo { get; set; }
            public decimal PctjMeta { get; set; }
            public decimal MinimoTienda { get; set; }
            public decimal MetaTienda { get; set; }
            public decimal Dias { get; set; }
            public decimal DiasPeriodo { get; set; }
            public decimal MinimoDia { get; set; }
            public decimal MetaDia { get; set; }
            public decimal PctjMinimoDia { get; set; }
            public decimal PctjMetaDia { get; set; }
        }

        [Serializable]
        public class Periodos
        {
            public Int32 Periodo { get; set; }
            public string Fechas { get; set; }
            public Int32 Anio { get; set; }
        }

        [Serializable]
        public class Desarmado
        {
            public string Tienda { get; set; }
            public string Vendedor { get; set; }
            public string NombreVendedor { get; set; }
            public DateTime FechaDesarmado { get; set; }
            public int NoRequisicion { get; set; }
            public string Observaciones { get; set; }
            public string Usuario { get; set; }
            public string Cliente { get; set; }
            public string NombreCliente { get; set; }
            public string Mensaje { get; set; }
            public string UsuarioAutoriza { get; set; }
            public string ObservacionesAutoriza { get; set; }
            public int NoDesarmado { get; set; }
            public string Tipo { get; set; }
            public string Pedido { get; set; }
            public bool YaEnviada { get; set; }
            public string Estado { get; set; }
            public string Factura { get; set; }
            public List<Articulos> Articulo { get; set; }

            public Desarmado()
            {
                Articulo = new List<Articulos>();
            }
        }

        [Serializable]
        public class RetornaComisionesJefesSupervisores
        {
            public List<ComisionesJefesSupervisores> ComisionesJefesSupervisores { get; set; }
            public List<ComisionesJefesSupervisoresDetalle> JefeSupervisorDetalle { get; set; }
            public List<DetalleComisionJefe> DetalleComisionesJefe { get; set; }
            public List<ResumenComisionJefe> ResumenJefe { get; set; }
            public List<DetalleComisionSupervisor> DetalleComisionesSupervisor { get; set; }
            public List<ResumenComisionSupervisor> ResumenSupervisor { get; set; }
            public List<SupervisoresResumen> SupervisoresResumen { get; set; }
            public List<Info> Info { get; set; }

            public RetornaComisionesJefesSupervisores()
            {
                ComisionesJefesSupervisores = new List<ComisionesJefesSupervisores>();
                SupervisoresResumen = new List<SupervisoresResumen>();
                Info = new List<Info>();
            }
        }

        [Serializable]
        public class SupervisoresResumen
        {
            public DateTime FechaInicial { get; set; }
            public DateTime FechaFinal { get; set; }
            public string Codigo_Supervisor { get; set; }
            public string Supervisor { get; set; }
            public string Categoria { get; set; }
            public decimal Comision { get; set; }
            public string Info1 { get; set; }
            public string Info2 { get; set; }
            public string Rubro { get; set; }
            public decimal Porcentaje { get; set; }
            public string Usuario { get; set; }
        }

        [Serializable]
        public class ComisionesJefesSupervisores
        {
            public DateTime FechaInicial { get; set; }
            public DateTime FechaFinal { get; set; }
            public string Supervisor { get; set; }
            public string Categoria { get; set; }
            public string Tienda { get; set; }
            public string Ubicacion { get; set; }
            public string Jefe { get; set; }
            public string CodigoSupervisor { get; set; }
            public string Categoria_Supervisor { get; set; }
            public string CodigoJefe { get; set; }
            public decimal VentasSinIVA { get; set; }
            public decimal Comision { get; set; }
            public decimal VentasTiendaSinIVA { get; set; }
            public decimal Minimo { get; set; }
            public decimal Meta { get; set; }
            public string ResultadoTienda { get; set; }
            public decimal VentaComisionarJefe { get; set; }
            public decimal PctjJefe { get; set; }
            public decimal ComisionJefe { get; set; }
            public decimal ComisionJefeTotal { get; set; }
            public decimal PctjSupervisor { get; set; }
            public decimal ComisionSupervisor { get; set; }
            public decimal PctjSupervisor2 { get; set; }
            public decimal ComisionSupervisor2 { get; set; }
            public string Titulo { get; set; }
            public string Usuario { get; set; }
            public string Agrupado { get; set; }
        }

        [Serializable]
        public class ComisionesJefesSupervisoresDetalle
        {
            public string CodigoSupervisor { get; set; }
            public string NombreSupervisor { get; set; }
            public string Categoria { get; set; }
            public string Tienda { get; set; }
            public string Ubicacion { get; set; }
            public string CodigoJefe { get; set; }
            public string NombreJefe { get; set; }
            public decimal VentasSinIVA { get; set; }
            public decimal Comision { get; set; }
            public decimal VentasTiendaSinIVA { get; set; }
            public decimal Minimo { get; set; }
            public decimal Meta { get; set; }
            public string ResultadoTienda { get; set; }
            public decimal VentaComisionarJefe { get; set; }
            public decimal PctDividido { get; set; }
            public decimal PctTiendaJefe { get; set; }
            public decimal ComisionJefe { get; set; }
            public decimal ComisionJefeTotal { get; set; }
            public decimal PctTiendaS1 { get; set; }
            public decimal ComisionS1 { get; set; }
            public decimal PctTiendaS2 { get; set; }
            public decimal ComisionS2 { get; set; }
        }

        [Serializable]
        public class DetalleComisionJefe
        {
            public string Categoria { get; set; }
            public string Tienda { get; set; }
            public string Ubicacion { get; set; }
            public string CodigoJefe { get; set; }
            public string NombreJefe { get; set; }
            public decimal PctDividido { get; set; }
            public decimal PctTiendaJefe { get; set; }
            public decimal ComisionJefe { get; set; }
            public decimal ComisionJefeTotal { get; set; }
        }

        [Serializable]
        public class ResumenComisionJefe
        {
            public string CodigoJefe { get; set; }
            public string NombreJefe { get; set; }
            public decimal Comision { get; set; }
        }

        [Serializable]
        public class DetalleComisionSupervisor
        {
            public string Categoria { get; set; }
            public string Tienda { get; set; }
            public string Ubicacion { get; set; }
            public string CodigoSupervisor { get; set; }
            public string NombreSupervisor { get; set; }
            public decimal PctDividido { get; set; }
            public decimal PctTiendaS1 { get; set; }
            public decimal ComisionS1 { get; set; }
            public decimal PctTiendaS2 { get; set; }
            public decimal ComisionS2 { get; set; }
        }

        [Serializable]
        public class ResumenComisionSupervisor
        {
            public string CodigoSupervisor { get; set; }
            public string NombreSupervisor { get; set; }
            public decimal Comision { get; set; }
            public string Titulo { get; set; }
            public decimal Porcentaje { get; set; }
        }

        [Serializable]
        public class MontosJefesSupervisores
        {
            public string Tienda { get; set; }
            public string CodigoJefe { get; set; }
            public string CodigoSupervisor { get; set; }
            public decimal ValorFactura { get; set; }
            public decimal MontoComisionable { get; set; }
            public decimal ValorNeto { get; set; }
        }

        [Serializable]
        public class RetornaComisiones
        {
            public List<Comisiones> Comisiones { get; set; }
            public List<Tiendas> Tiendas { get; set; }
            public List<Facturas> Facturas { get; set; }
            public List<Pendientes> Pendientes { get; set; }
            public List<Excepciones> Excepciones { get; set; }
            public List<Anuladas> Anuladas { get; set; }
            public List<Info> Info { get; set; }

            public RetornaComisiones()
            {
                Comisiones = new List<Comisiones>();
                Tiendas = new List<Tiendas>();
                Facturas = new List<Facturas>();
                Pendientes = new List<Pendientes>();
                Excepciones = new List<Excepciones>();
                Anuladas = new List<Anuladas>();
                Info = new List<Info>();
            }
        }

        [Serializable]
        public class Info
        {
            public bool Exito { get; set; }
            public string Mensaje { get; set; }
            public DateTime FechaInicial { get; set; }
            public DateTime FechaFinal { get; set; }
            public string Usuario { get; set; }
            public bool YaGrabadas { get; set; }
            public string Documento { get; set; }
        }

        [Serializable]
        public class Comisiones
        {
            public string NombreVendedor { get; set; }
            public decimal Valor { get; set; }
            public decimal MontoComisionable { get; set; }
            public decimal ValorNeto { get; set; }
            public decimal Comision { get; set; }
            public decimal Porcentaje { get; set; }
            public string Vendedor { get; set; }
            public string Tienda { get; set; }
            public string Titulo { get; set; }
            public string Tooltip { get; set; }
        }

        [Serializable]
        public class Tiendas
        {
            public string Tienda { get; set; }
            public decimal Valor { get; set; }
            public decimal MontoComisionable { get; set; }
            public decimal ValorNeto { get; set; }
            public decimal Comision { get; set; }
            public string Titulo { get; set; }
            public string Nombre { get; set; }
            public string Departamento { get; set; }
            public string Municipio { get; set; }
        }

        [Serializable]
        public class FacturasCliente
        {
            public string Factura { get; set; }
            public string Texto { get; set; }
        }

        [Serializable]
        public class Facturas
        {
            public string Factura { get; set; }
            public decimal Valor { get; set; }
            public decimal MontoComisionable { get; set; }
            public decimal ValorNeto { get; set; }
            public decimal Comision { get; set; }
            public decimal Porcentaje { get; set; }
            public DateTime FechaFactura { get; set; }
            public DateTime FechaEntrega { get; set; }
            public string CodigoVendedor { get; set; }
            public string NombreVendedor { get; set; }
            public string CodigoJefe { get; set; }
            public string NombreJefe { get; set; }
            public string CodigoSupervisor { get; set; }
            public string NombreSupervisor { get; set; }
            public string Tienda { get; set; }
            public string Titulo { get; set; }
            public string NivelPrecio { get; set; }
            public string FormaDePago { get; set; }
            public decimal PorcentajeTarjeta { get; set; }
            public string Envio { get; set; }
        }

        [Serializable]
        public class Factura : RetornaExito
        {
            public string NoFactura { get; set; }
            public decimal Valor { get; set; }
            public decimal ValorNeto { get; set; }
            public DateTime FechaFactura { get; set; }
            public DateTime FechaEntrega { get; set; }
            public string Vendedor { get; set; }
            public string NombreVendedor { get; set; }
            public string Tienda { get; set; }
            public string NivelPrecio { get; set; }
            public string Envio { get; set; }
        }

        [Serializable]
        public class FacturasFinanciera : Factura
        {
            public int ID { get; set; }
            public string Cliente { get; set; }
            public string NombreCliente { get; set; }
            public string Solicitud { get; set; }
            //public int Solicitud { get; set; }
            //TODO: refactorizar duplicidad de informacion
            public string Identificacion { get; set; }
            public string OtraIdentificacion { get; set; }
            public string ConstanciaIngresos { get; set; }
            public string DocumentoServiciosPublicos { get; set; }
            public int? EstadosDeCuenta { get; set; }
            public string Pagare { get; set; }
            public string Observaciones { get; set; }
            public string EstadoActual { get; set; }
            public int Financiera { get; set; }
            public string NombreFinanciera { get; set; }
        }

        [Serializable]
        public class ExpedienteInterconsumo : Factura
        {
            public string Cliente { get; set; }
            public string NombreCliente { get; set; }
            public string NumeroSolicitud { get; set; }
            public string Identificacion { get; set; }
            public string OtraIdentificacion { get; set; }
            public string ConstanciaIngresos { get; set; }
            public string DocumentoServiciosPublicos { get; set; }
            public int? EstadosDeCuenta { get; set; }
            public string Pagare { get; set; }
            public string Observaciones { get; set; }

        }

        [Serializable]
        public class SaldoDesembolso
        {
            public string Factura { get; set; }
            public DateTime FechaPedido { get; set; }
            public DateTime FechaFactura { get; set; }
            public DateTime FechaEnvioInterconsumo { get; set; }
            public DateTime FechaDesembolsoInterconsumo { get; set; }
            public string Cliente { get; set; }
            public decimal TotalFactura { get; set; }
            public decimal? Enganche { get; set; }
            public decimal? MontoFinanciado { get; set; }
            public decimal? MontoDesembolso { get; set; }

        }

        [Serializable]
        public class SaldosDesembolso
        {
            public List<SaldoDesembolso> LoteSaldosDesembolso { get; set; }
            public SaldosDesembolso()
            {
                LoteSaldosDesembolso = new List<SaldoDesembolso>();
            }
        }

        [Serializable]
        public class AnulaRecibo
        {
            public string Recibo { get; set; }
            public string Usuario { get; set; }
            public string Observaciones { get; set; }
            public string Vendedor { get; set; }
            public string Cobrador { get; set; }
            public string Accion { get; set; }
            public string Ambiente { get; set; }
        }

        [Serializable]
        public class Recibos
        {
            public string Documento { get; set; }
            public string Tipo { get; set; }
            public string Cliente { get; set; }
            public DateTime Fecha { get; set; }
            public string Cobrador { get; set; }
            public string Vendedor { get; set; }
            public string Factura { get; set; }
            public decimal Efectivo { get; set; }
            public decimal Cheque { get; set; }
            public decimal Tarjeta { get; set; }
            public decimal Monto { get; set; }
            public string NumeroCheque { get; set; }
            public string EntidadFinancieraCheque { get; set; }
            public Int32 EmisorTarjeta { get; set; }
            public string EntidadFinancieraTarjeta { get; set; }
            public string EsAnticipo { get; set; }
            public decimal Deposito { get; set; }
            public string EntidadFinancieraDeposito { get; set; }
            public string Observaciones { get; set; }
            public string MontoLetras { get; set; }
            public string Direccion { get; set; }
            public string Telefono { get; set; }
            public string NombreCliente { get; set; }
            public string NombreBancoCheque { get; set; }
            public string Pos { get; set; }
            public string DireccionEmpresa { get; set; }
            public string Usuario { get; set; }
            public DateTime FechaRegistro { get; set; }
            public string Moneda { get; set; }
            public decimal Tasa { get; set; }
        }

        [Serializable]
        public class RecibosAsociar
        {
            public string Recibo { get; set; }
            public string FacturaAsociar { get; set; }
            public decimal MontoAsociar { get; set; }
            public string Usuario { get; set; }
        }

        [Serializable]
        public class DocumentoConSaldo
        {
            public string Documento { get; set; }
            public string Descripcion { get; set; }
            public decimal Saldo { get; set; }
        }

        [Serializable]
        public class RecibosDepositar
        {
            public string Recibo { get; set; }
            public DateTime Fecha { get; set; }
            public decimal Efectivo { get; set; }
            public decimal Cheque { get; set; }
            public decimal Tarjeta { get; set; }
            public string Pos { get; set; }
            public string PosNombre { get; set; }
        }

        [Serializable]
        public class Desembolso
        {
            public DateTime Fecha { get; set; }
            public string Prestamo { get; set; }
            public string Solicitud { get; set; }
            public string Cliente { get; set; }
            public string ClienteMF { get; set; }
            public string Cobrador { get; set; }
            public string EmailJefeTienda { get; set; }
            public string EmailSupervisor { get; set; }
            public string EmailVendedor { get; set; }
            public string NombreCliente { get; set; }
            public string Factura { get; set; }
            public decimal Valor { get; set; }
            public decimal ValorMF { get; set; }
            public decimal? EngancheMF { get; set; }
            public string FacturaMF { get; set; }
            public string Criterio { get; set; }
            public string Vendedor { get; set; }
            public bool ReadOnly { get; set; }
            public string NombreFinanciera { get; set; }
            public List<FacturasFinanciera> FacturasSugerencia { get; set; }

            public Desembolso()
            {
                FacturasSugerencia = new List<FacturasFinanciera>();
            }
        }

        [Serializable]
        public class LoteDesombolsosFinanciera : RetornaExito
        {
            public List<Desembolso> Desembolsos { get; set; }
            public DateTime FechaDesembolsos { get; set; }
            public DateTime FechaRigeDesembolso { get; set; }
            public string Operacion { get; set; }
            public string Usuario { get; set; }
            public int Financiera { get; set; }

            public LoteDesombolsosFinanciera()

            {
                Desembolsos = new List<Desembolso>();
            }
            /// <summary>
            /// Constructor con lista de desembolsos
            /// </summary>
            /// <param name="lst">Lista de desembolsos</param>
            public LoteDesombolsosFinanciera(List<Desembolso> lst)
            {
                Desembolsos = new List<Desembolso>();
                Desembolsos = lst;
            }
        }

        [Serializable]
        public class LoteFacturasFinanciera : RetornaExito
        {
            public List<FacturasFinanciera> Facturas { get; set; }
            public List<Info> Info { get; set; }
            public string Operacion { get; set; }
            public string Usuario { get; set; }
            public int Financiera { get; set; }
            public LoteFacturasFinanciera()
            {
                Facturas = new List<FacturasFinanciera>();
                Info = new List<Info>();
            }
        }

        [Serializable]
        public class Pendientes
        {
            public string Factura { get; set; }
            public string Tipo { get; set; }
            public string Estado { get; set; }
            public decimal Valor { get; set; }
            public decimal MontoComisionable { get; set; }
            public decimal ValorNeto { get; set; }
            public decimal Comision { get; set; }
            public decimal Porcentaje { get; set; }
            public DateTime FechaFactura { get; set; }
            public string FechaEntrega { get; set; }
            public string Vendedor { get; set; }
            public string NombreVendedor { get; set; }
            public string EstaDespachado { get; set; }
            public string Tienda { get; set; }
            public string Titulo { get; set; }
        }

        [Serializable]
        public class Excepciones
        {
            public string Factura { get; set; }
            public decimal Valor { get; set; }
            public decimal MontoComisionable { get; set; }
            public decimal ValorNeto { get; set; }
            public decimal Comision { get; set; }
            public decimal Porcentaje { get; set; }
            public DateTime FechaFactura { get; set; }
            public DateTime FechaEntrega { get; set; }
            public string Vendedor { get; set; }
            public string NombreVendedor { get; set; }
            public string Tienda { get; set; }
            public string Tipo { get; set; }
            public string Titulo { get; set; }
        }

        [Serializable]
        public class Anuladas
        {
            public string Factura { get; set; }
            public decimal Valor { get; set; }
            public decimal MontoComisionable { get; set; }
            public decimal ValorNeto { get; set; }
            public decimal Comision { get; set; }
            public decimal Porcentaje { get; set; }
            public DateTime FechaFactura { get; set; }
            public DateTime FechaEntrega { get; set; }
            public string Vendedor { get; set; }
            public string NombreVendedor { get; set; }
            public string Tienda { get; set; }
            public string Titulo { get; set; }
        }

        [Serializable]
        public class RetornaExito
        {
            public bool exito { get; set; }
            public string mensaje { get; set; }
            public string usuario { get; set; }
        }

        [Serializable]
        public class FacturaModificaComision
        {
            public string Factura { get; set; }
            public decimal MontoComision { get; set; }
            public string Tipo { get; set; }
            public string Anio { get; set; }
            public string Mes { get; set; }
            public string Usuario { get; set; }
        }

        [Serializable]
        public class Email
        {
            public string Correo { get; set; }
        }

        [Serializable]
        public class Existencias
        {
            public int id { get; set; }
            public string Articulo { get; set; }
            public string Descripcion { get; set; }
            public Int32 Existencia { get; set; }
            public Int32 Reserva { get; set; }
            public Int32 Disponible { get; set; }
            public Int32 DisponibleTiendas { get; set; }
            public string Tooltip { get; set; }
            public string TooltipReservas { get; set; }
            public string ArticuloDescripcion { get; set; }
        }

        [Serializable]
        public class ExistenciasTiendas : Existencias
        {
            public string Tienda { get; set; }
            public string Localizacion { get; set; }
        }

        [Serializable]
        public class RetornaExistencias
        {
            public List<Existencias> Existencias { get; set; }
            public List<ExistenciasTiendas> ExistenciasTiendas { get; set; }
            public List<RetornaExito> RetornaExito { get; set; }

            public RetornaExistencias()
            {
                Existencias = new List<Existencias>();
                ExistenciasTiendas = new List<ExistenciasTiendas>();
                RetornaExito = new List<RetornaExito>();
            }
        }

        [Serializable]
        public class Informacion
        {
            public Informacion()
            {
                this.Exito = false;
                this.Mensaje = "";
            }
            public Informacion(bool pExito, string pMensaje)
            {
                this.Exito = pExito;
                this.Mensaje = pMensaje;
            }

            public bool Exito { get; set; }
            public string Mensaje { get; set; }
        }

        [Serializable]
        public class ArticuloPedido
        {
            public string Pedido { get; set; }
            public string Cliente { get; set; }
            public string Articulo { get; set; }
            public int Linea { get; set; }
            public string Tipo { get; set; }
            public decimal PrecioBase { get; set; }
            public string ArticuloNuevo { get; set; }
            public string DescripcionNueva { get; set; }
            public decimal PrecioBaseNuevo { get; set; }
            public decimal PrecioNuevo { get; set; }
            public int Cantidad { get; set; }
            public decimal PrecioTotal { get; set; }
            public string Tooltip { get; set; }
            public bool Exito { get; set; }
            public string Mensaje { get; set; }
        }

        [Serializable]
        public class CuotasInterconsumo
        {
            public decimal SaldoFinanciar { get; set; }
            public decimal Primera { get; set; }
            public decimal Ultima { get; set; }
            public decimal Intereses { get; set; }
            public decimal MontoTotal { get; set; }
            public DateTime PrimerPago { get; set; }
            public DateTime UltimoPago { get; set; }
            public bool Exito { get; set; }
            public string Mensaje { get; set; }
        }

        [Serializable]
        public class Precio
        {
            public string Articulo { get; set; }
            public string Nombre { get; set; }
            public decimal SaldoFinanciar { get; set; }
            public decimal Enganche { get; set; }
            public decimal Primera { get; set; }
            public decimal Ultima { get; set; }
            public decimal Intereses { get; set; }
            public decimal MontoTotal { get; set; }
            public DateTime PrimerPago { get; set; }
            public DateTime UltimoPago { get; set; }
            public string NivelPrecio { get; set; }
            public decimal Factor { get; set; }
            public string Tooltip { get; set; }
            public string Oferta { get; set; }
            public decimal PrecioOriginal { get; set; }
            public DateTime FechaVence { get; set; }
            public decimal PrecioOferta { get; set; }
            public string Pagos { get; set; }
            public DateTime FechaDesde { get; set; }
        }

        [Serializable]
        public class Precios
        {
            public List<Precio> Precio { get; set; }
            public List<RetornaExito> RetornaExito { get; set; }

            public Precios()
            {
                Precio = new List<Precio>();
                RetornaExito = new List<RetornaExito>();
            }
        }

        [Serializable]
        public class Cotizacion
        {
            public string CLIENTE { get; set; }
            public string GRABADA_EN_LINEA { get; set; }
            public DateTime FECHA_VENCE { get; set; }
            public decimal? TOTAL_FACTURAR { get; set; }
            public string COBRADOR { get; set; }
            public Int64? SOLICITUD { get; set; }
            public decimal? ENGANCHE { get; set; }
        }

        [Serializable]
        public class Pedido
        {
            public string CLIENTE { get; set; }
            public string GRABADA_EN_LINEA { get; set; }
            public decimal? TOTAL_FACTURAR { get; set; }
            public string COBRADOR { get; set; }
            public Int64? SOLICITUD { get; set; }
            public decimal? ENGANCHE { get; set; }
        }

        [Serializable]
        public class SolicitudInterconsumo
        {
            public string TIPO_OPERACION { get; set; }
            public string NUMERO_DOCUMENTO { get; set; }
            public Int64? SOLICITUD { get; set; }
            public string CLIENTE { get; set; }
            public string NOMBRE_CLIENTE { get; set; }
            public string VENDEDOR { get; set; }
            public string NOMBRE_VENDEDOR { get; set; }
            public DateTime? FECHA_SOLICITUD { get; set; }
            public DateTime? FECHA_IMPRESION_PAGARE { get; set; }
            public DateTime? FECHA_APROBACION_SOLICITUD { get; set; }
            public DateTime? FECHA_RECHAZO_SOLICITUD { get; set; }
            public DateTime? FECHA_FACTURA { get; set; }
            public string COBRADOR { get; set; }
            public string EMAIL_VENDEDOR { get; set; }
            public string EMAIL_JEFE { get; set; }
            public string EMAIL_SUPERVISOR { get; set; }
            public string USUARIO_AUTORIZO { get; set; }
            public string NOMBRE { get; set; }
            public string DESCRIPCION_ERROR { get; set; }
            public decimal VALOR_SOLICITUD { get; set; }
            public decimal VALOR_FACTURA { get; set; }
            public decimal? VALOR_ENGANCHE { get; set; }
            public decimal VALOR_SALDO { get; set; }
            public string OBSERVACIONES { get; set; }
            public string AUTORIZACION { get; set; }
            public string ESTADO { get; set; }
            public string PRIMER_NOMBRE { get; set; }
            public string SEGUNDO_NOMBRE { get; set; }
            public string TERCER_NOMBRE { get; set; }
            public string PRIMER_APELLIDO { get; set; }
            public string SEGUNDO_APELLIDO { get; set; }
            public string APELLIDO_CASADA { get; set; }
            public Int32 PLAZO { get; set; }
            public decimal CUOTA { get; set; }
            public decimal ULTIMA_CUOTA { get; set; }
            public string XML_CONSULTA { get; set; }
            public string XML_CONSULTA_RESPUESTA { get; set; }
            public bool ENVIAR_CORREO_INTERCONSUMO { get; set; }
            public string NOMBRE_FINANCIERA { get; set; }
            public int FINANCIERA { get; set; }
        }

        [Serializable]
        public class Financiera
        {
            public Int64? ID { get; set; }
            public string Nombre { get; set; }
            public string Activa { get; set; }
            public string EsBanco { get; set; }
            public string CuotasIguales { get; set; }
            public string RptSolicitud { get; set; }
            public string RptPagare { get; set; }
            public string Prefijo { get; set; }
            public decimal? PcjRestar { get; set; }
        }

        [Serializable]
        public class SolicitudCredito
        {
            public string NombreCompleto { get; set; }
            public string PrimerNombre { get; set; }
            public string SegundoNombre { get; set; }
            public string TercerNombre { get; set; }
            public string PrimerApellido { get; set; }
            public string SegundoApellido { get; set; }
            public string ApellidoDeCasada { get; set; }
            public string Edad { get; set; }
            public string EstadoCivil { get; set; }
            public string Profesión { get; set; }
            public string Nacionalidad { get; set; }
            public string DPILetras { get; set; }
            public string Departamento { get; set; }
            public string Municipio { get; set; }
            public string MontoTotalLetras { get; set; }
            public string MontoCuotaLetras { get; set; }
            public string FechaFinLetras { get; set; }
            public string FechaPrimerPagoLetras { get; set; }
            public string FechaPrimerPagoMesLetras { get; set; }
            public string FechaCorteDiaLetras { get; set; }
            public string Domicilio { get; set; }
            public string FechaPedidoDiaLetras { get; set; }
            public string FechaPedidoMesLetras { get; set; }
            public string FechaPedidoAnoLetras { get; set; }


        }

        [Serializable]
        public class Catalogo
        {
            public string Codigo { get; set; }
            public string Descripcion { get; set; }
        }
        //------------------------------------- FRIEDMAN

        [Serializable]
        public class FriedmanResumenSemanal
        {
            public string Cobrador { get; set; }
            public int Anio { get; set; }
            public int Semana { get; set; }
            public string Vendedor { get; set; }
            public string NombreVendedor { get; set; }
            public decimal Dias { get; set; }
            public decimal Meta { get; set; }
            public decimal VentasReales { get; set; }
            public decimal Diferencia { get; set; }
            public int CantFacturas { get; set; }
            public decimal CantArticulos { get; set; }
            public decimal Minimo { get; set; }
            public decimal FacturaPromedio { get; set; }
            public decimal ArticulosPorFactura { get; set; }
            public decimal VentasPorDia { get; set; }
            public decimal QuetVTAvrsMeta { get; set; }
            public int StatusQuetz { get; set; }
            public string Estrategia { get; set; }
            public string CodigoJefe { get; set; }
            public int Orden { get; set; }
            public int Mes { get; set; }
        }

        [Serializable]
        public class FriedmanResumenTrimestral
        {
            public string Cobrador { get; set; }
            public int Anio { get; set; }
            public int Semana { get; set; }
            public string Vendedor { get; set; }
            public string NombreVendedor { get; set; }
            public decimal Meta { get; set; }
            public decimal VentasReales { get; set; }
            public decimal Minimo { get; set; }
            public int StatusQuetz { get; set; }
            public int Orden { get; set; }
        }

        [Serializable]
        public class FriedmanResumenTrimestralDto
        {
            public List<string> Mes { get; set; }
            public List<string> Periodo { get; set; }
            public string mPeriodo { get; set; }
            public string Cobrador { get; set; }
            public string Trimestre { get; set; }
            public string Mes01 { get; set; }
            public string Mes02 { get; set; }
            public string Mes03 { get; set; }
            public string Vendedor { get; set; }
            public string Orden { get; set; }
            public string NombreVendedor { get; set; }
            public string Valor01 { get; set; }
            public string Valor02 { get; set; }
            public string Valor03 { get; set; }
            public string Valor04 { get; set; }
            public string Valor05 { get; set; }
            public string Valor06 { get; set; }
            public string Valor07 { get; set; }
            public string Valor08 { get; set; }
            public string Valor09 { get; set; }
            public string Valor10 { get; set; }
            public string Valor11 { get; set; }
            public string Valor12 { get; set; }
            public string Valor13 { get; set; }
            public string Valor14 { get; set; }
            public string Valor15 { get; set; }
            public string Valor16 { get; set; }
            public string Valor17 { get; set; }
            public string Valor18 { get; set; }
            public string Valor19 { get; set; }
            public string Valor20 { get; set; }
            public string Valor21 { get; set; }

            public string Periodo01 { get; set; }
            public string Periodo02 { get; set; }
            public string Periodo03 { get; set; }
            public string Periodo04 { get; set; }
            public string Periodo05 { get; set; }
            public string Periodo06 { get; set; }
            public string Periodo07 { get; set; }
            public string Periodo08 { get; set; }
            public string Periodo09 { get; set; }
            public string Periodo10 { get; set; }
            public string Periodo11 { get; set; }
            public string Periodo12 { get; set; }
            public string Periodo13 { get; set; }
            public string Periodo14 { get; set; }
            public string Periodo15 { get; set; }
            public string Periodo16 { get; set; }
            public string Periodo17 { get; set; }
            public string Periodo18 { get; set; }

            public decimal Dias01 { get; set; }
            public decimal Dias02 { get; set; }
            public decimal Dias03 { get; set; }
            public decimal Dias04 { get; set; }
            public decimal Dias05 { get; set; }
            public decimal Dias06 { get; set; }
            public decimal Dias07 { get; set; }
            public decimal Dias08 { get; set; }
            public decimal Dias09 { get; set; }
            public decimal Dias10 { get; set; }
            public decimal Dias11 { get; set; }
            public decimal Dias12 { get; set; }
            public decimal Dias13 { get; set; }
            public decimal Dias14 { get; set; }
            public decimal Dias15 { get; set; }
            public decimal Dias16 { get; set; }
            public decimal Dias17 { get; set; }
            public decimal Dias18 { get; set; }

            public decimal VentaReal01 { get; set; }
            public decimal VentaReal02 { get; set; }
            public decimal VentaReal03 { get; set; }
            public decimal VentaReal04 { get; set; }
            public decimal VentaReal05 { get; set; }
            public decimal VentaReal06 { get; set; }
            public decimal VentaReal07 { get; set; }
            public decimal VentaReal08 { get; set; }
            public decimal VentaReal09 { get; set; }
            public decimal VentaReal10 { get; set; }
            public decimal VentaReal11 { get; set; }
            public decimal VentaReal12 { get; set; }
            public decimal VentaReal13 { get; set; }
            public decimal VentaReal14 { get; set; }
            public decimal VentaReal15 { get; set; }
            public decimal VentaReal16 { get; set; }
            public decimal VentaReal17 { get; set; }
            public decimal VentaReal18 { get; set; }
        }
        //------------------------------------- FRIEDMAN

        [Serializable]
        public class ResultadoOperacion
        {
            public string Mensaje { get; set; }
        }
    }

    [Serializable]
    public class ReporteRecibos
    {
        public ReporteRecibos()
        {
            EfectivoGTQ = 0;
            EfectivoUSD = 0;
            Dolares = 0;
            Cheque = 0;
            TarjetaCredomatic = 0;
            TarjetaVisa = 0;
        }
        public ReporteRecibos(string pCobrador)
        {
            COBRADOR = pCobrador;
            EfectivoGTQ = 0;
            EfectivoUSD = 0;
            Dolares = 0;
            Cheque = 0;
            TarjetaCredomatic = 0;
            TarjetaVisa = 0;
        }

        public void Sumar(ReporteRecibos ReciboAdicionar)
        {
            EfectivoGTQ = EfectivoGTQ + ReciboAdicionar.EfectivoGTQ;
            EfectivoUSD = EfectivoUSD + ReciboAdicionar.EfectivoUSD;
            Dolares = Dolares + ReciboAdicionar.Dolares;
            Cheque = Cheque + ReciboAdicionar.Cheque;
            TarjetaCredomatic = TarjetaCredomatic + ReciboAdicionar.TarjetaCredomatic;
            TarjetaVisa = TarjetaVisa + ReciboAdicionar.TarjetaVisa;
        }

        public decimal Total()
        {
            return EfectivoGTQ + EfectivoUSD + Cheque + TarjetaCredomatic + TarjetaVisa;
        }
        public string COBRADOR { get; set; }
        public decimal EfectivoGTQ { get; set; }
        public decimal EfectivoUSD { get; set; }
        public decimal Dolares { get; set; }
        public decimal Cheque { get; set; }
        public decimal TarjetaCredomatic { get; set; }
        public decimal TarjetaVisa { get; set; }
    }

    [Serializable]
    public class Respuesta
    {

        /// <summary>
        /// Constructor que tiene por parametro el Codigo de respuesta y
        /// el Mensaje de respuesta, este último es opcional.
        /// </summary>
        public Respuesta(int CodigoRespuesta, string Mensaje = "")
        {
            this.CodigoRespuesta = CodigoRespuesta;
            this.Mensaje = Mensaje;
            Exito = (CodigoRespuesta < 20000) ? true : false;
        }

        /// <summary>
        /// Constructor que tiene por parametro el tipo de respuesta y
        /// el Mensaje de respuesta, este último es opcional.
        /// </summary>
        public Respuesta(bool Exito, string Mensaje = "")
        {
            this.Exito = Exito;
            this.Mensaje = Mensaje;
        }

        /// <summary>
        /// Constructor que tiene por parametro el tipo de respuesta, la excepción y
        /// el Mensaje de respuesta, este último es opcional.
        /// </summary>
        public Respuesta(bool Exito, Exception ex, string Mensaje = "")
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }

            string mm = "";
            try
            {
                mm = ex.InnerException.Message;
            }
            catch
            {
                mm = "";
            }

            this.Exito = Exito;
            this.Mensaje = string.Format("{0} {1} {2} {3}", Mensaje, ex.Message, m, mm);
        }


        /// <summary>
        /// Constructor que tiene por parametro el tipo de respuesta y
        /// el Mensaje de respuesta, este último es opcional.
        /// </summary>
        public Respuesta(bool Exito, string Mensaje, Object Objeto)
        {
            this.Exito = Exito;
            this.Mensaje = Mensaje;
            this.Objeto = Objeto;
        }

        /// <summary>
        /// Constructor simple que crea el objeto respuesta 
        /// con el tipo OK sin ningun mensaje detallado.
        /// </summary>
        public Respuesta()
        {
            this.Exito = false;
            this.Mensaje = "";
        }

        //---------------------------------------------------------------------------------------------------
        /// <summary>
        ///     Variable que almacena el código de respuesta.
        /// </summary>
        public int CodigoRespuesta { get; set; }

        //---------------------------------------------------------------------------------------------------
        /// <summary>
        ///     Variable que define si el mensaje de respuesta es OK o NOT_OK
        ///     es decir, correcta o no correcta. 
        ///     Los únicos valores disponibles para este campo son:
        ///     Const.OK
        ///     Const.NOT_OK
        /// </summary>
        public bool Exito { get; set; }

        //---------------------------------------------------------------------------------------------------
        /// <summary>
        ///     Variable que almacena la información, el mensaje personalizado 
        ///     asociada a la respuesta.
        /// </summary>
        public string Mensaje { get; set; }

        //---------------------------------------------------------------------------------------------------
        /// <summary>
        ///     Variable que almacena el mensaje de error detallado que lanza alguna
        ///     Excepcion del sistema.
        /// </summary>
        public string StackError { get; set; }


        //---------------------------------------------------------------------------------------------------
        /// <summary>
        ///     Variable que almacena el objeto de respuesta, este puede ser de cualquier tipo.
        ///     El casteo se realiza en el servicio cliente que consuma este servicio.
        /// </summary>
        public Object Objeto { get; set; }
    }

    [Serializable]
    public class ImagenAdjunta : LinkedResource
    {

        public ImagenAdjunta(string fileName, string mediaType, string _ContentId) : base(fileName, mediaType)
        {
            this.ContentId = _ContentId;
        }
    }

    /// <summary>
    /// Clase utilitaria con información necesaria para el envio
    /// del correo para notificar el rechazo de interconsumo
    /// </summary>
    [Serializable]
    public class infoInterconsumoMensajeRechazo
    {
        public string EmailVendedor;
        public string EmailJefeTienda;
        public DateTime FechaFact;
        public string Factura;
        public string Tienda;
        public long? solicitud;
        public string ClienteMF;
        public string nombreCliente;
        public DateTime? FechaSolicitud;
        public string EmailSupervisor;
        public infoInterconsumoMensajeRechazo()
        {
            FechaSolicitud = null;
        }
    }

    [Serializable]
    public class SetCamas
    {
        public string codArticulo { get; set; }
        public string DescripcionSet { get; set; }
    }

    [Serializable]
    public class dtoArticulo
    {
        public string sku { get; set; }
        public int cantidad { get; set; }
        public decimal PrecioNormal { get; set; }
        public decimal? PrecioOferta { get; set; }
        public bool EstaEnOferta { get; set; }
        public string OfertaDesde { get; set; }
        public string OfertaHasta { get; set; }
        public string Nombre { get; set; }
        public bool Activo { get; set; }
    }

    [Serializable]
    public class DtoBaseCama
    {
        public string codArticulo { get; set; }
        public string Descripcion { get; set; }
    }

    [Serializable]
    public class DtoLoteBases
    {
        public int Cantidad { get; set; }
        public DtoBaseCama Base { get; set; }
    }

    [Serializable]
    public class DtoPatas
    {
        public string codArticulo { get; set; }
        public string Descripcion { get; set; }
    }

    [Serializable]
    public class DtoLotePatas
    {
        public decimal Cantidad { get; set; }
        public DtoPatas ModeloPata { get; set; }
    }

    [Serializable]
    public class ProductoPrecioRegular
    {
        public string sku { get; set; }
        public string price { get; set; }
        public int status { get; set; }
        public Extension_attributes extension_attributes { get; set; }
        public List<custom_attributes> custom_attributes { get; set; }
    }

    [Serializable]
    public class Producto_Ofertado : ProductoPrecioRegular
    {

    }

    [Serializable]
    public class Extension_attributes
    {
        public Stock_item stock_item { get; set; }
        public Extension_attributes(int qty)
        {
            stock_item = new Stock_item(qty);
        }
    }

    [Serializable]
    public class Stock_item
    {
        public int qty { get; set; }
        public bool is_in_stock { get; set; }
        public Stock_item(int _q)
        {
            qty = _q;
            is_in_stock = _q > 0 ? true : false;
        }
    }

    [Serializable]
    public class custom_attributes
    {
        public string attribute_code { get; set; }
        public string value { get; set; }
        public custom_attributes(string atcode, string val)
        {
            attribute_code = atcode;
            value = val;
        }
    }

    #region "Clases migradas del Portal"
    [Serializable]
    public class DataDirectory
    {
        //=================================================================================================================================================================
        //Variable que nos sirve para grabar la trazabilidad del sistema en archivos de texto.
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Contiene la configuracion actual del sistema.
        /// </summary>
        public ServicesConfig SConfig { get; set; }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Constructor por defecto para cargar la configuracion del ambiente para la aplicación.
        /// </summary>
        public DataDirectory()
        {
            try
            {
                log.Debug("Init");
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Const_Utils.SERVICES_CONFIG_PATH);
                if (File.Exists(path))
                {
                    string obj = JObject.Parse(File.ReadAllText(path)).ToString();
                    SConfig = JsonConvert.DeserializeObject<ServicesConfig>(obj);
                    log.Debug("Finish");
                }
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.ToString());
                log.Error(string.Format("{0} - {1}", e.Message, e.StackTrace));
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Recupera el nombre del ambiente por defecto.
        /// </summary>
        public string DefaultEnvironment()
        {
            string mEnvironment = this.SConfig._DefaultEnvironment;

            return string.IsNullOrEmpty(mEnvironment) ? Const_Utils.DEFAULT_ENVIRONMENT : mEnvironment;
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Recupera el nombre del servidor por defecto de los servicios.
        /// </summary>
        public string Server()
        {
            AppEnvironment mEnvironment = this.SConfig.ServicesEnvironments.Find(e => e.Id == DefaultEnvironment());

            if (mEnvironment == null)
                return Const_Utils.DEFAULT_SERVER;
            else
                return string.IsNullOrEmpty(mEnvironment.Server) ? Const_Utils.DEFAULT_SERVER : mEnvironment.Server;
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Recupera el nombre del servidor por defecto para las paginas.
        /// </summary>
        public string PageServer()
        {
            AppEnvironment mPageEnvironment = this.SConfig.PagesEnvironments.Find(e => e.Id == DefaultEnvironment());

            if (mPageEnvironment == null)
                return Const_Utils.DEFAULT_SERVER;
            else
                return string.IsNullOrEmpty(mPageEnvironment.Server) ? Const_Utils.DEFAULT_SERVER : mPageEnvironment.Server;
        }
        public string PortalEnvironment()
        {
            AppEnvironment mPortalEnvironment = this.SConfig.PortalEnvironments.Find(e => e.Id == DefaultEnvironment());
            if (mPortalEnvironment == null)
                return Const_Utils.DEFAULT_SERVER;
            else
                return string.IsNullOrEmpty(mPortalEnvironment.Server) ? Const_Utils.DEFAULT_SERVER : mPortalEnvironment.Server;
        }
        public string ExtServicesEnvironment()
        {
            AppEnvironment mExtServicesEnvrt = this.SConfig.ExtServicesEnvoronments.Find(e => e.Id == DefaultEnvironment());
            if (mExtServicesEnvrt == null)
                return Const_Utils.DEFAULT_SERVER;
            else
                return string.IsNullOrEmpty(mExtServicesEnvrt.Server) ? Const_Utils.DEFAULT_SERVER : mExtServicesEnvrt.Server;
        }
        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Recupera el nombre del servidor para el servicio.
        /// </summary>
        /// <param name="IdService">Identificador del servicio</param>
        /// <returns>Nombre del servidor</returns>
        public string Server(string IdService)
        {
            AppService mService = this.SConfig.Services.Find(s => s.Id == IdService);

            if (mService == null)
                return Const_Utils.DEFAULT_SERVER;
            else
            {
                if (string.IsNullOrEmpty(mService.Server))
                    return Server();
                else
                    return mService.Server;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Recupera el nombre del servidor para la pagina.
        /// </summary>
        /// <param name="IdPage">Identificador de la pagina</param>
        /// <returns>Nombre del servidor</returns>
        public string PageServer(string IdPage)
        {
            AppService mPage = this.SConfig.Pages.Find(s => s.Id == IdPage);

            if (mPage == null)
                return Const_Utils.DEFAULT_SERVER;
            else
            {
                if (string.IsNullOrEmpty(mPage.Server))
                    return PageServer();
                else
                    return mPage.Server;
            }
        }

        public string ExtServicesServer(string IdPage)
        {
            AppService mPage = this.SConfig.Pages.Find(s => s.Id == IdPage);
            string env = string.Empty;
            return env = this.SConfig.ExtServicesEnvoronments.Find(x => x.Id == DefaultEnvironment()).Server;

        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Recupera el nombre del contexto por defecto del servicio.
        /// </summary>
        public string DefaultContext()
        {
            string mContext = this.SConfig._DefaultContext;

            return string.IsNullOrEmpty(mContext) ? Const_Utils.DEFAULT_CONTEXT : mContext;
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Recupera el nombre del contexto por defecto de la pagina.
        /// </summary>
        public string DefaultPageContext()
        {
            string mPageContext = this.SConfig._DefaultPageContext;

            return string.IsNullOrEmpty(mPageContext) ? Const_Utils.DEFAULT_PAGE_CONTEXT : mPageContext;
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Recupera el contexto del servicio.
        /// </summary>
        /// <param name="IdService">Identificador del servicio</param>
        /// <returns>Nombre del contexto</returns>
        public string Context(string IdService)
        {
            AppService mService = this.SConfig.Services.Find(s => s.Id == IdService);

            if (mService == null)
                return Const_Utils.DEFAULT_CONTEXT;
            else
            {
                if (string.IsNullOrEmpty(mService.Context))
                    return DefaultContext();
                else
                    return mService.Context;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Recupera el contexto de la pagina.
        /// </summary>
        /// <param name="IdPage">Identificador de la pagina.</param>
        /// <returns>Nombre del contexto</returns>
        public string PageContext(string IdPage)
        {
            AppService mPage = this.SConfig.Pages.Find(s => s.Id == IdPage);

            if (mPage == null)
                return Const_Utils.DEFAULT_PAGE_CONTEXT;
            else
            {
                if (string.IsNullOrEmpty(mPage.Context))
                    return DefaultPageContext();
                else
                    return mPage.Context;
            }
        }
        public string ExtServicesContext(string IdPage)
        {
            AppService mPage = this.SConfig.Pages.Find(s => s.Id == IdPage);

            if (mPage == null)
                return Const_Utils.DEFAULT_PAGE_CONTEXT;
            else
            {
                if (string.IsNullOrEmpty(mPage.Context))
                    return DefaultPageContext();
                else
                    return mPage.Context;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Recupera la direccion del servidor añadiendo su contexto.
        /// </summary>
        /// <param name="IdService">Identificador del servicio</param>
        /// <returns>Nombre del contexto</returns>
        public string ServerAndContext(string IdService)
        {
            return String.Format("{0}/{1}", Server(IdService), Context(IdService));
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Recupera la direccion del servidor añadiendo su contexto.
        /// </summary>
        /// <param name="IdPage">Identificador de la pagina</param>
        /// <returns>Nombre del contexto</returns>
        public string ServerAndContextPage(string IdPage)
        {
            return String.Format("{0}/{1}", PageServer(IdPage), PageContext(IdPage));
        }


        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Recupera la direccion del servidor añadiendo su contexto.
        /// </summary>
        /// <param name="IdPage">Identificador de la pagina</param>
        /// <returns>Nombre del contexto</returns>
        public string ServerContextAndNamePage(string IdPage)
        {

            if (!string.IsNullOrEmpty(PageContext(IdPage)))
            {
                return String.Format("{0}/{1}/{2}", PageServer(IdPage), PageContext(IdPage), PageName(IdPage));
            }
            return String.Format("{0}/{1}", PageServer(IdPage), PageName(IdPage));
        }
        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Recupera la direccion del servidor añadiendo su contexto.
        /// </summary>
        /// <param name="IdPage">Identificador de la pagina</param>
        /// <returns>Nombre del contexto</returns>
        public string ServerContextAndNamePagePortal(string PageName)
        {
            DataDirectory mRestServiceDD = new DataDirectory();
            string LocalEnv = mRestServiceDD.DefaultEnvironment().Equals("Produccion") ? "Portal" : mRestServiceDD.DefaultEnvironment().Equals("Pruebas") ? "PortalPruebas" : "";
            string url = PageName;

            return url;
        }
        public string ServerContextAndNamePageExtServices(string IdPage)
        {
            DataDirectory mRestServiceDD = new DataDirectory();
            if (!string.IsNullOrEmpty(PageContext(IdPage)))
            {
                return String.Format("{0}/{1}/{2}", ExtServicesServer(IdPage), ExtServicesContext(IdPage), PageName(IdPage));
            }
            return String.Format("{0}/{1}", ExtServicesServer(IdPage), PageName(IdPage));
        }
        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Recupera el nombre del servicio.
        /// </summary>
        /// <param name="IdService">Identificador del servicio</param>
        /// <returns>Nombre del servicio</returns>
        public string Name(string IdService)
        {
            AppService mService = this.SConfig.Services.Find(s => s.Id == IdService);

            if (mService == null)
                return IdService;
            else
                return string.IsNullOrEmpty(mService.Name) ? IdService : mService.Name;
        }


        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Recupera el nombre de la pagina.
        /// </summary>
        /// <param name="IdPage">Identificador de la pagina</param>
        /// <returns>Nombre de la pagina </returns>
        public string PageName(string IdPage)
        {
            AppService mPage = this.SConfig.Pages.Find(s => s.Id == IdPage);

            if (mPage == null)
                return IdPage;
            else
                return string.IsNullOrEmpty(mPage.Name) ? IdPage : mPage.Name;
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Encripta el texto enviado.
        /// </summary>
        /// <param name="toEncrypt">Mensaje a encriptar</param>
        /// /// <param name="useHashing">Utilizar metodo Hash</param>
        /// <returns>Mensaje encriptado</returns>
        public string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            //-------------------------------------------------------------------
            //Obtenemos la "llave secreta" para poder encriptar el textos con MD5
            string key = this.SConfig._DefaultKeySecurity;

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));

                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            //-------------------------------------------------------------------
            //Utilizamos el alcoritmo Triple DES para encriptar
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0,
                                 toEncryptArray.Length);
            tdes.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Desencripta el texto enviado.
        /// </summary>
        /// <param name="toEncrypt">Mensaje a desencriptar</param>
        /// /// <param name="useHashing">Utilizar metodo Hash</param>
        /// <returns>Mensaje desencriptado</returns>
        public string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            //-------------------------------------------------------------------
            //Obtenemos la "llave secreta" para poder encriptar el textos con MD5
            string key = this.SConfig._DefaultKeySecurity;

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
            {
                keyArray = UTF8Encoding.UTF8.GetBytes(key);
            }

            //-------------------------------------------------------------------
            //Utilizamos el alcoritmo Triple DES para desencriptar
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(
                                 toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

    }

    [Serializable]
    public class DtoVendedor
    {
        public string VENDEDOR { get; set; }
        public string NOMBRE { get; set; }
        public string APELLIDO { get; set; }
        public string CELULAR { get; set; }
        public string EMAIL { get; set; }
        public string COBRADOR { get; set; }
    }
    #region "Redmine"
    /// <summary>
    /// Estructura (json) que devuelve el api rest de redmine al hacer consultas de tickets
    /// </summary>
    [Serializable]
    public class RedmineQueryResponse
    {
        public int total_count { get; set; }
        public int limit { get; set; }
        public List<Issue> issues { get; set; }
        public int offset { get; set; }
    }

    /// <summary>
    /// Clase que contendrá la información proveniente de la creación del ticket
    /// en redmine
    /// </summary>
    [Serializable]
    public class RedmineResponse
    {
        public Issue issue { get; set; }
    }

    [Serializable]
    public class RedmineUpdate
    {
        public IssueUpdate issue { get; set; }
    }

    [Serializable]
    public class Issue
    {
        public string subject { get; set; }
        public Estado_Ticket status { get; set; }
        public DateTime start_date { get; set; }
        public DateTime updated_on { get; set; }
        public int id { get; set; }
        public string notes { get; set; }
        //public List<CustomField> custom_fields { get; set; }
        public List<Journal> journals { get; set; }
        public string description { get; set; }
        public DateTime created_on { get; set; }
    }

    [Serializable]
    public class CustomField
    {
        public int id { get; set; }

        public string value { get; set; }
    }

    [Serializable]
    public class User
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    [Serializable]
    public class Detail
    {
        public string old_value { get; set; }
        public string new_value { get; set; }
        public string property { get; set; }
        public string name { get; set; }
    }

    [Serializable]
    public class Journal
    {
        public int id { get; set; }
        public User user { get; set; }
        public string notes { get; set; }
        public List<Detail> details { get; set; }
        public DateTime created_on { get; set; }
    }

    [Serializable]
    public class IssueUpdate
    {
        public int status_id { get; set; }
        public string notes { get; set; }
    }

    [Serializable]
    public class Estado_Ticket
    {
        public string name { get; set; }
        public int id { get; set; }
    }
    #endregion

    [Serializable]
    public class UsuarioPDVDto : Respuesta
    {
        public string UsuarioExactus { get; set; }
        public string Tienda { get; set; }
        public string Vendedor { get; set; }
        public string Nombre { get; set; }
        public string LoginActiveDirectory { get; set; }
        public string Email { get; set; }
        public string Puesto { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string UsuarioTransaccion { get; set; }

        public List<String> Roles { get; set; }
    }

    [Serializable]
    public class AppEnvironment
    {
        /// <summary>
        /// Identificador del ambiente.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Contiene la dirección del servidor.
        /// </summary>
        public string Server { get; set; }
    }

    [Serializable]
    public class AppService
    {
        /// <summary>
        /// Identificador del servicio.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Servidor específico del servicio.
        /// </summary>
        public string Server { get; set; }

        /// <summary>
        /// Contexto específico del servicios.
        /// </summary>
        public string Context { get; set; }

        /// <summary>
        /// Nombre del servicio.
        /// </summary>
        public string Name { get; set; }
    }

    [Serializable]
    public class ServicesConfig
    {
        /// <summary>
        /// Identificador del ambiente.
        /// </summary>
        public string _DefaultEnvironment { get; set; }

        /// <summary>
        /// Identificador del contexto para el servicio.
        /// </summary>
        public string _DefaultContext { get; set; }

        /// <summary>
        /// Identificador del contexto para la pagina.
        /// </summary>
        public string _DefaultPageContext { get; set; }

        /// <summary>
        /// Identificador del contexto para la pagina.
        /// </summary>
        public string _DefaultKeySecurity { get; set; }

        /// <summary>
        /// Listado de ambientes.
        /// </summary>
        public List<AppEnvironment> ServicesEnvironments { get; set; }

        /// <summary>
        /// Listado de ambientes.
        /// </summary>
        public List<AppEnvironment> PagesEnvironments { get; set; }

        /// <summary>
        /// Listado de ambientes (Portal).
        /// </summary>
        public List<AppEnvironment> PortalEnvironments { get; set; }
        /// <summary>
        /// LIstado de ambinentes (ExtServices)
        /// </summary>
        public List<AppEnvironment> ExtServicesEnvoronments { get; set; }
        /// <summary>
        /// Listado de servicios.
        /// </summary>
        public List<AppService> Services { get; set; }


        /// <summary>
        /// Listado de servicios.
        /// </summary>
        public List<AppService> Pages { get; set; }

    }

    [Serializable]
    public class Const_Utils
    {
        public const string DEFAULT_SERVER = @"http://localhost";
        public const string DEFAULT_ENVIRONMENT = "Pruebas";
        public const string DEFAULT_CONTEXT = "api";
        public const string DEFAULT_PAGE_CONTEXT = "";
        public const string CONFIG_PATH = @"C:\Config\";

        public const string SERVICES_CONFIG_PATH = @"servicesDataDirectory.json";
        public const string APPSETTINGS_CONFIG_PATH = @"appsettings.json";

        public class IDSERVICE
        {
            public const string MF_SEGURIDAD_AUTENTICA_ACTIVE_DIRECTORY = "MF.Seguridad.Autentica.ActiveDirectory";
            public const string MF_SEGURIDAD_AUTENTICA_EXACTUS = "MF.Seguridad.Autentica.Exactus";
            public const string MF_REPORTE_FRIEDMAN_TRIMESTRAL = "MF.Friedman.Trimestral";
            public const string MF_REPORTE_FRIEDMAN_SEMANAL = "MF.Friedman.Semanal";
            public const string MF_CONFIGURACION_CAMBIAR_TIENDA = "MF.Configuracion.CambiarTienda";
            public const string MF_CONFIGURACION_TIENDAS = "MF.Configuracion.Tiendas";
            public const string MF_REPORTE_SOLICITUD = "MF.Financiera.Solicitud";
            public const string MF_CALIFICACION_CLIENTE = "MF.Cliente.CalificacionCliente";
            public const string MF_DESPACHOS_MAYOREO = "MF.Bodega.DespachoMayoreo";
            //FEL
            public const string MF_FEL_DOCTO_ELECTRONICO = "MF.FEL.DocumentoElectronico";
            public const string MF_FEL_NOTAS_CREDITO_DEBITO_PARA_FIRMAR = "MF.FEL.NotasCreditoDebito";

        }
    }
    #endregion

    #region ClasesBuro
    [Serializable]
    public class BuroCalificacion
    {
        public int Calificacion { get; set; }
        public string Error { get; set; }
        public decimal LimiteCredito { get; set; }
        public decimal MontoCompras { get; set; }
        public int NumeroCompras { get; set; }
        public decimal PromedioCompras { get; set; }
    }
    #endregion

    #region "FiestaNetERP"
    [Serializable]
    public class appSettings
    {

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Contiene la configuracion actual del sistema.
        /// </summary>
        public Config SConfig { get; set; }
        public appSettings()
        {
            try
            {



                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Const_Utils.APPSETTINGS_CONFIG_PATH);
                //Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Replace(@"bin\Release\netcoreapp2.1\",Const_Utils.APPSETTINGS_CONFIG_PATH));
                if (File.Exists(path))
                {
                    string obj = JObject.Parse(File.ReadAllText(path)).ToString();
                    SConfig = JsonConvert.DeserializeObject<Config>(obj);

                }
                else
                {
                    try
                    {
                        path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Replace(@"bin\Release\netcoreapp2.1\", Const_Utils.APPSETTINGS_CONFIG_PATH));
                        if (File.Exists(path))
                        {
                            string obj = JObject.Parse(File.ReadAllText(path)).ToString();
                            SConfig = JsonConvert.DeserializeObject<Config>(obj);

                        }
                    }
                    catch
                    { }
                }
            }
            catch
            {
                try
                {
                    string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Replace(@"bin\Release\netcoreapp2.1\", Const_Utils.APPSETTINGS_CONFIG_PATH));
                    if (File.Exists(path))
                    {
                        string obj = JObject.Parse(File.ReadAllText(path)).ToString();
                        SConfig = JsonConvert.DeserializeObject<Config>(obj);

                    }
                }
                catch
                { }
            }
        }

    }

    [Serializable]
    public class Config
    {
        public string Ambiente { get; set; }
        public List<RestServicesEnvironments> RestServicesEnvironments { get; set; }
        public List<PagesEnvironments> PagesEnvironments { get; set; }
        public string RestServicesUrl
        {

            set { RestServicesUrl = value; }
            get
            {
                string url = string.Empty;
                switch (Ambiente)
                {
                    case "DES": url = RestServicesEnvironments.Find(x => x.Id == "DES").Url; break;
                    case "PRU": url = RestServicesEnvironments.Find(x => x.Id == "PRU").Url; break;
                    case "PRO": url = RestServicesEnvironments.Find(x => x.Id == "PRO").Url; break;
                    default: url = RestServicesEnvironments.Find(x => x.Id == "PRO").Url; break;
                }
                return url;
            }

        }
        public string PagesUrl
        {
            set { PagesUrl = value; }
            get
            {
                string url = string.Empty;
                switch (Ambiente)
                {
                    case "DES": url = PagesEnvironments.Find(x => x.Id == "DES").Url; break;
                    case "PRU": url = PagesEnvironments.Find(x => x.Id == "PRU").Url; break;
                    case "PRO": url = PagesEnvironments.Find(x => x.Id == "PRO").Url; break;
                    default: url = PagesEnvironments.Find(x => x.Id == "PRO").Url; break;
                }
                return url;
            }
        }
    }

    [Serializable]
    public class RestServicesEnvironments
    {
        public string Id { get; set; }
        public string Url { get; set; }
    }

    [Serializable]
    public class PagesEnvironments
    {
        public string Id { get; set; }
        public string Url { get; set; }
    }
    #endregion

    #region "ATID"

    /// <summary>
    /// Respuesta a la solicitud de crédito a ATID
    /// </summary>
    [Serializable]
    public class RespuestaSolicitud
    {
        public string CodigoCliente { get; set; }

        public string Error { get; set; }

        public string MensajeError { get; set; }

        public string NivelEndeudamiento { get; set; }

        public string NumeroCredito { get; set; }

        public string NumeroSolicitud { get; set; }

        public string PunteoPrecalificacion { get; set; }

        public string ResultadoPrecalificacion { get; set; }

        public decimal Cuota { get; set; }

        public decimal UltimaCuota { get; set; }

        public RespuestaSolicitud()
        { }
    }

    [Serializable]
    public class FacturaSolicitudATID
    {
        public string Solicitud { get; set; }
        public string Cliente { get; set; }
        public string Documento { get; set; }
        public string Factura { get; set; }
    }

    [Serializable]
    public class DesembolsoATID
    {
        public string solicitud { get; set; }
        public string Credito { get; set; }
        public DateTime? FechaDesembolso { get; set; }
        public string Factura { get; set; }
        public string Cliente { get; set; }
        public string Documento { get; set; }
        public string NombreCliente { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string TercerNombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string ApellidoCasada { get; set; }
        public decimal ValorDesembolso { get; set; }

    }

    [Serializable]
    public class DatosFacturacion
    {
        public string cliente { get; set; }
        public string NitCliente { get; set; }
        public string NombreCliente { get; set; }
        public string DirCliente { get; set; }

        //DETALLE
        public string Producto { get; set; }
        public string Descripcion { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Precio { get; set; }
        public decimal TotalProducto { get; set; }
        public string Moneda { get; set; }

    }

    [Serializable]
    public class FacturacionArticulos
    {
        public DateTime FechaFactura { get; set; }
        public string cliente { get; set; }
        public string NitCliente { get; set; }
        public string FormaDePago { get; set; }
        public string NombreCliente { get; set; }
        public string ApellidosCliente { get; set; }
        public string DirCliente { get; set; }
        public List<DetalleFacturacionArticulos> Articulos { get; set; }
        public decimal TotalBruto { get; set; }
        public decimal TotalNeto { get; set; }
        public decimal TotalImpuesto { get; set; }
        public string Email { get; set; }
    }

    [Serializable]
    public class DetalleFacturacionArticulos
    {
        public int Linea { get; set; }
        public string Producto { get; set; }
        public string Descripcion { get; set; }
        public decimal iva { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Precio { get; set; }
        public decimal TotalProductoBruto { get; set; }
        public decimal TotalProductoNeto { get; set; }
        public string Moneda { get; set; }
    }
    #endregion

    #region "canje vales"
    public class CANJE
    {
        public const string RESPUESTA_SIN_CANJEAR = "1";
        public const string RESPUESTA_CANJE_EXITO = "1";
    }
    #endregion
    public class CanjeVale
    {
        public string Codigo { get; set; }
        public string Factura { get; set; }
        public string Recibo { get; set; }
        public DateTime FechaCanje { get; set; }
        public string UsuarioCanje { get; set; }
        public string TiendaCanje { get; set; }

    }
    [Serializable]
    public class Vale
    {
        public string Codigo { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Tienda { get; set; }
        public string Genero { get; set; }
        public string Email { get; set; }
        public int Edad { get; set; }
        public string Identificacion { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Tipo { get; set; }
        public decimal Valor { get; set; }
        public decimal Porcentaje { get; set; }
        public int DiaHasta { get; set; }
        public int MesHasta { get; set; }
        public int AnioHasta { get; set; }
        public string Promocion { get; set; }
        public string Empresa_promotora { get; set; }
        public string Factura { get; set; }
        public string Recibo { get;set; }
        public string ip_solicitante { get; set; }

    }

    [Serializable]
    public class Correo
    {
        public List<string> Destinatarios { get; set; }
        public string Remitente { get; set; }
        public List<string> ResponderA { get; set; }
        public StringBuilder CuerpoCorreo { get; set; }
        public string Asunto { get; set; }
        public string NombreAMostrar { get; set; } //DisplayName
        public List<ImagenAdjunta> lstImagenes { get; set; }
        public List<string> ConCopiaOcultaA { get; set; }

    }

    [Serializable]
    public class ResultadoTiendaVirtual
    {
        public string Factura { get; set; }
        public string Pedido { get; set; }
    }

    //consulta clientes crediplus
    [Serializable]
    public class dtoDireccionCreditos
    {
        public string CLIENTE { get; set; }
        public string DIRECCION { get; set; }
        public string INDICACIONES { get; set; }
        public string ESTADO { get; set; }
        public string FACTURA { get; set; }
        public List<ArticuloCredito> ARTICULOS { get; set; }
    }

    [Serializable]
    public class FacturaMayoreo
    {
        public string OrdenDeCompra { get; set; }
        public string Factura { get; set; }
        public string Pedido { get; set; }
        public string Cliente { get; set; }
        public string NombreCliente { get; set; }
        public string FechaPedido { get; set; }
        public decimal CantArtículos { get; set; }
        public decimal TotalFactura { get; set; }
        public string Estado { get; set; }
        public string SolAnulacion { get; set; }
        public string linkFactura { get; set; }
    }

    [Serializable]
    public class ConsultaFacturaMayoreo
    {
        public string OrdenDeCompra { get; set; }
        public DateTime? FechaIni { get; set; }
        public DateTime? FechaFin { get; set; }
        public string Cliente { get; set; }
        public string NombreCliente { get; set; }
        public string Cobrador { get; set; }
    }

    //CUPON CREDIPLUS JULIO 2019
    [Serializable]
    public class dtoCuponCrediplus
    {
        public string NombreCliente { get; set; }
        public string CreditoCrediplus { get; set; }
    }
}
