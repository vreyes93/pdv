﻿using System;

namespace MF_Clases.Comun
{
    [Serializable]
    public class ArticuloCrediplus
    {
        public int Cantidad { get; set; }
        public string CodigoMF { get; set; }
        public string CodigoCP { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }
        public bool HayExistencia { get; set; }
        public bool Existe { get; set; }
        public decimal PrecioMF { get; set; }
        public decimal PrecioSinIvaMF { get; set; }
        public decimal IvaMF { get; set; }
        public decimal MargenMF { get; set; }
        public decimal PrecioCP { get; set; }
        public decimal PrecioSinIvaCP { get; set; }
        public decimal IvaCP
        {
            get
            {
                return PrecioCP - PrecioSinIvaCP;
            }
        }
    }
}
