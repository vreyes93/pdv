﻿using System;

namespace MF_Clases.Comun
{
    [Serializable]
    public class ArticuloCredito
    {
        public string Articulo { get; set; }
        public string Descripcion { get; set; }
        public decimal cantidad { get; set; }
        public decimal Precio { get; set; }
    }
}
