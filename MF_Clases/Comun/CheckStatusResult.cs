﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF_Clases.Comun
{
   public static class CheckStatusResult
    {
       public static Dictionary<int, string> changeResultOptions = new Dictionary<int, string>()
                                            {
                                                {201,"El codigo coincide con el vale y el canje es exitoso"},
                                                {403, "El código no coincide con el vale o hay otro porblema que no permite el proceso de canje"}
                                                
                                            };
        public static Dictionary<int, string> statusResultOptions = new Dictionary<int, string>()
                                            {
                                                {1,"Sin Canjear"},
                                                {2, "OKY no posee saldo para canje"},
                                                {3,"Código inválido porque pertenece a otra empresa"},
                                                {4, "El vale ya ha expirado"},
                                                {5,"Canjeado, este vale ya fué canjeado en alguna sucursal"},
                                                {6, "El código no es correcto o no coincide en la base de datos"}

                                            };
    }
}
