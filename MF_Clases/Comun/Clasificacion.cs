﻿using System;

namespace MF_Clases.Comun
{
    [Serializable]
    public class Clasificacion
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? Order { get; set; }
    }
}
