﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MF_Clases.Comun
{
   public class CuponResult
    {
            public DataResult ArrResult { get; set; }
            public int Code { get; set; }
            public string MsgCode { get; set; }
            public string Msg { get; set; }
            public bool StatusService { get; set; }
            public bool OperationResult { get; set; }

    }
}
