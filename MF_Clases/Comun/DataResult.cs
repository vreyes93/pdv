﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF_Clases.Comun
{
    /// <summary>
    /// Estructura de la información que devuelve la promotora de cupones al momento de emitir una respuesta
    /// </summary>
  public  class DataResult
    {
        public string id { get; set; }
        public string name { get; set; }
        public string amountLocalCurrency { get; set; }
        public string brand { get; set; }
        public string latamBranch { get; set; }
        public string latamuser { get; set; }
        public string confirmationCode { get; set; }
        public string status { get; set; }
        public string statuscode { get; set; }
        public string product { get; set; }
    }
    
}
