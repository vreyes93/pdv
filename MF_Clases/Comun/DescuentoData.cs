﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MF_Clases.Comun
{
    public class DescuentoData
    {
        public string Pedido { get; set; }
        public decimal DescuentoVale { get; set; }
        public DescuentoData()
        {
            DescuentoVale = 0;
        }
    }
}
