﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF_Clases.Comun
{
    [Serializable]
   public class ExchangeInfoApiRequest
    {
        public string CouponCode { get; set; }
        public string advertisingCompany { get; set; }
        public decimal CouponValue { get; set; }
        public string invoiceAmount { get; set; }
        public string exchangeReferente { get; set; }
        public string invoiceNumber { get; set; }
        public string UserName { get; set; }

    }
}
