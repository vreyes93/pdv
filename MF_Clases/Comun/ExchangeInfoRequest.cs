﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF_Clases.Comun
{
    /// <summary>
    /// Clase para compartir/obtener información de la empresa
    /// promotora de cupones
    /// </summary>
    public class ExchangeInfoRequest
    {

        public string method { get; set; }

        public List<DataExchange> data { get; set; }
        public ExchangeInfoRequest()
        {
            data = new List<DataExchange>();
        }


    }

}
