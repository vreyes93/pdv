﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF_Clases.Comun
{
    /// <summary>
    /// Estructura de la información que devuelve la promotora de cupones al momento de emitir una respuesta
    /// </summary>
    public class ExchangeInfoResult
    {
        public DataResult ArrResult { get; set; }
        public int Code { get; set; }
        public string MsgCode { get; set; }
        public string Msg { get; set; }
        public bool StatusService { get; set; }
        public bool OperationResult { get; set; }

        public string CouponCode { get; set; }
    }
}

