﻿using System;

namespace MF_Clases.Comun
{
    [Serializable]
    public class ExistenciaProductos
    {
        public string CodigoArticulo { get; set; }
        public decimal CantidadDisponible { get; set; }
    }
}
