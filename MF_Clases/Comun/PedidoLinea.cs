﻿using System;
using System.Collections.Generic;
using System.Text;
namespace MF_Clases.Comun
{
    /// <summary>
    /// clase extraída del proyecto de services
    /// para utilizar las promociones desde un api y mantener la estructura que se tiene
    /// actualmente.
    /// </summary>
    
    public class PedidoLinea
    {
        public string Articulo { get; set; }
        public string Nombre { get; set; }
        public decimal PrecioUnitario { get; set; }
        public decimal CantidadPedida { get; set; }
        public decimal PrecioTotal { get; set; }
        public string Bodega { get; set; }
        public string Localizacion { get; set; }
        public string RequisicionArmado { get; set; }
        public string Descripcion { get; set; }
        public string Comentario { get; set; }
        public string Estado { get; set; }
        public string NumeroPedido { get; set; }
        public string Oferta { get; set; }
        public DateTime FechaOfertaDesde { get; set; }
        public decimal PrecioOriginal { get; set; }
        public string EsDetalleKit { get; set; }
        public decimal PrecioFacturar { get; set; }
        public string TipoOferta { get; set; }
        public Nullable<int> Vale { get; set; }
        public Nullable<Int64> Autorizacion { get; set; }
        public string Condicional { get; set; }
        public string ArticuloCondicion { get; set; }
        public decimal PrecioUnitarioDebioFacturar { get; set; }
        public decimal PrecioSugerido { get; set; }
        public double PrecioBaseLocal { get; set; }
        public Int32 Linea { get; set; }
        public string Gel { get; set; }
        public decimal PorcentajeDescuento { get; set; }
        public string Beneficiario { get; set; }
        public string Tienda { get; set; }
        public DateTime FechaFactura { get; set; }
        public int IdDescuento { get; set; }
        public string DescuentoNombre { get; set; }
        public decimal Descuento { get; set; }
        public decimal NetoFacturar { get; set; }
    }
}
