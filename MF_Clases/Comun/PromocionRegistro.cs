﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MF_Clases.Comun
{
    
    public class PromocionRegistro
    {
        public string TipoVenta { get; set; }
        public decimal DsctoVales { get; set; }
        public List<PedidoLinea> articulos { get; set; }

        public decimal TotalFacturar { get; set; }
        public decimal Enganche { get; set; }
        public decimal SaldoFinanciar { get; set; }
        public decimal Monto { get; set; }
        public decimal Descuento { get; set; }
        public string nivelPrecio { get; set; }
        public string mensaje { get; set; }
        public string notas { get; set; }
        public int Financiera { get; set; } 
        public bool EsBanco { get; set; }
        public List<string> lstVales { get; set; }
        public string cliente { get; set; }

        public PromocionRegistro()
        {
            notas = string.Empty;
            Descuento = 0;
            lstVales = new List<string>();
        }
        

    }
    
}
