﻿using System;

namespace MF_Clases.Comun
{
    [Serializable]
    public class TiendaEspecificacion
    {
        public string CodigoTienda { get; set; }
        public string Atributo { get; set; }
        public string Valor { get; set; }
    }
}
