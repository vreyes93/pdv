﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MF_Clases.Comun
{
    
   public class pedido
    {
        public List<valePedidoLinea> articulos { get; set; }
        public decimal TotalFacturar { get; set; }
        public decimal DsctoVales { get; set; }
        public string nivelPrecio { get; set; }
        public bool EsBanco { get; set; }
        public decimal Descuento { get; set; }
        public decimal Monto { get; set; }
        public int Financiera { get; set; }
    }
}
