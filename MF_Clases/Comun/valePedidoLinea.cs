﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MF_Clases.Comun
{

    public class valePedidoLinea
    {
        public int Linea { get; set; }
        public string Articulo { get; set; }
        public decimal CantidadPedida { get; set; }
        public decimal PrecioTotal { get; set; }
        public decimal PrecioFacturar { get; set; }
        public decimal Descuento { get; set; }
        public decimal NetoFacturar { get; set; }
        public decimal DescuentoVales { get; set; }
        public decimal PorcentajeDescuento { get; set; }
        public decimal PrecioUnitario { get; set; }
    }
}
