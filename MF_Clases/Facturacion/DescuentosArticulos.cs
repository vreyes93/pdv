﻿using System;
using System.Collections.Generic;

namespace MF_Clases.Facturacion
{
    [Serializable]
    public class DescuentosArticulos
    {
        public List<NuevoPrecioProducto> articulosActuales { get; set; }
        public NuevoPrecioProducto ArticuloAgregar { get; set; }
        public decimal factor { get; set; }
    }
}
