﻿using System;
using System.Collections.Generic;

namespace MF_Clases.Facturacion
{
    [Serializable]
    public class DescuentosFacturacionResult
    {
        public DiscountOrder orderData { get; set; }
        public List<DiscountOrderDetail> orderDetailData { get; set; }
        public DiscountBill orderBill { get; set; }
        public List<DiscountBillDetail> orderBillDetail { get; set; }

        public DiscountAccountRegistry accountRegistry { get; set; }
    }
}
