﻿using System;

namespace MF_Clases.Facturacion
{
    [Serializable]
    public class DiscountAccountRegistry
    {
        public decimal totalAmount { get; set; }
        public decimal totalBalance { get; set; }
        public decimal balanceClient { get; set; }
        public decimal balanceLocal { get; set; }
        public decimal localAmount { get; set; }
        public decimal clientAmount { get; set; }
        public decimal totalDollar { get; set; }
        public decimal balanceDollar { get; set; }
        public decimal extRateDollar { get; set; }
        public decimal netAmount { get; set; }
        public decimal totalTax { get; set; }
        public decimal totalDiscount { get; set; }
        public decimal baseTax { get; set; }
    }
}
