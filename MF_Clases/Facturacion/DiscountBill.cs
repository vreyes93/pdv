﻿using System;

namespace MF_Clases.Facturacion
{
    [Serializable]
    public class DiscountBill
    {
        public decimal totalTax { get; set; }
        public decimal totalDiscount { get; set; }
        public decimal totalAmount { get; set; }
        public decimal netAmount { get; set; }
        public decimal baseTax1 { get; set; }
        public decimal financialAmount { get; set; }
    }
}
