﻿using System;

namespace MF_Clases.Facturacion
{
    [Serializable]
    public class DiscountBillDetail
    {
        public int lineOrder { get; set; }
        public string item { get; set; }
        public decimal totalCostDollar { get; set; }
        public int quantity { get; set; }
        public decimal unitPrice { get; set; }
        public decimal totalTax1 { get; set; }
        public decimal totalDiscountLine1 { get; set; }
        public decimal totalCost { get; set; }
        public decimal totalPrice { get; set; }
        public string description { get; set; }
        public decimal totalCostLocal { get; set; }
        public decimal estimatedTotalCostLocal { get; set; }
        public decimal estimatedTotalCostDolar { get; set; }
        public decimal baseTax1 { get; set; } 
        public string lineType { get; set; }
        //código padre
        public string mainItem { get; set; }

    }
}
