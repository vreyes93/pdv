﻿using System;

namespace MF_Clases.Facturacion
{
    [Serializable]
    public class DiscountOrder
    {
        // total_mercaderia,MONTO_DESCUENTO1,TIPO_DESCUENTO1,TOTAL_A_FACTURAR,TOTAL_IMPUESTO1,BASE_IMPUESTO1
        public decimal netAmount { get; set; }
        public decimal discountAmount1 { get; set; }
        public decimal discountType { get; set; }
        public decimal totalAmount { get; set; }
        public decimal totalTax1 { get; set; }
        public decimal baseTax1 { get; set; }
        public decimal totalAmountToInvoice { get; set; }
    }
}
