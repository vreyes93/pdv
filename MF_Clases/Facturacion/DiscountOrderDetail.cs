﻿using System;

namespace MF_Clases.Facturacion
{
    [Serializable]
    public class DiscountOrderDetail
    {
        //pedido,pedido_linea,articulo,financiera,preciooriginal,precio_unitario,CANTIDAD_PEDIDA,precio_total,precio_unitario,PRECIO_UNITARIO_FACTURAR,precio_sugerido,PRECIO_BASE_LOCAL,PORCENTAJE_DESCUENTO,TIPO_LINEA
        public int line { get; set; }
        public string lineType { get; set; }
        public string item { get; set; }
        public int financial { get; set; }
        public decimal originalPrice { get; set; }
        public decimal basePrice { get; set; }
        public decimal quantity { get; set; }
        public decimal unitPrice { get; set; }
        public decimal netBasePrice { get; set; }
        public string description { get; set; }
        public decimal totalPrice
        {
            get
            {
                return (financial == 1 || financial == 9) ?
                 unitPrice * quantity - discountAmount : unitPrice * quantity - discountAmount;
            }
        }
        public decimal unitPriceToInvoice { get; set; }
        public decimal totaltPriceToInvoice { get; set; }
        public decimal suggestedPrice { get; set; }
        public decimal percentajeDiscount
        {
            get
            {
                return Math.Round(unitPrice > 0 ? discountAmount / unitPrice * 100 : 0, 6, MidpointRounding.AwayFromZero);
            }
        }
        public decimal discountAmount { get; set; }

    }
}
