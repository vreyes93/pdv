﻿using System;

namespace MF_Clases.Facturacion
{
    [Serializable]
    public class Garantia
    {
        public string CodigoTienda { get; set; }
        public string Visibilidad { get; set; }
        public string Factura { get; set; }
        public string CodigoCliente { get; set; }
        public string Nombre { get; set; }
        public DateTime Fecha { get; set; }
        public string Email { get; set; }
    }
}
