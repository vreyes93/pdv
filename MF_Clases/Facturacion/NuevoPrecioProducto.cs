﻿using System;
using Newtonsoft.Json;

namespace MF_Clases.Facturacion
{
    [Serializable]
    public class NuevoPrecioProducto
    {
        public string itemCode { get; set; }
        public int payment { get; set; }
        public decimal price { get; set; }
        public decimal originalPrice { get; set; }
        public decimal discountAmount => Math.Round(Math.Abs(originalPrice - price), 2);
        public decimal billingPrice => factor == null ? 0 : Math.Round(price / (decimal)factor, 2);
        public decimal originalDiscountPrice { get; set; }
        public decimal regularPayment => (price == 0 || payment == 0) ? 0 : Math.Round((price / payment) + 1, 0);
        public decimal lastPayment => price - (regularPayment * (payment - 1));
        public decimal? factor { get; set; }
        public decimal charge => factor == null ? 0 : Math.Round(Math.Abs(price - billingPrice), 2);
        public bool EsDecoracion { get; set; }
        public int Index { get; set; } = -1;
        
    }
    
}
