﻿using System;
using System.Collections.Generic;

namespace MF_Clases.Facturacion
{
    [Serializable]
    public class OrderData
    {
        public string store { get; set; }
        public decimal total_invoice { get; set; } //total a facturar
        public decimal down_payment { get; set; } // enganche
        public decimal balance_finance { get; set; } //saldo a financiar
        public decimal surchargues { get; set; } //recargos
        public decimal amount { get; set; } //monto
        public string paymentType { get; set; }// FormaDePago
        public decimal surcharguesFactor { get; set; }//factor
        public List<OrderDataDetail> details { get; set; }

    }
}
