﻿using System;

namespace MF_Clases.Facturacion
{
    [Serializable]
    public class OrderDataDetail
    {
        public int line { get; set; }
        public string lineType { get; set; }

        public string description { get; set; }
        public string item { get; set; }
        public decimal unitPrice { get; set; }
        public int orderQty { get; set; }
        public decimal totalPrice { get; set; }
        public decimal percentajeDiscount { get; set; }
        public decimal discount { get; set; }
        public string mainItem { get; set; }

    }
}
