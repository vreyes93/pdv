﻿using System;

namespace MF_Clases
{
    [Serializable]
    public class FechaComisionesRespuesta
    {
        public bool exito { get; set; }
        public string mensaje { get; set; }
        public DateTime FechaInicial { get; set; }
        public DateTime FechaFinal { get; set; }
    }
}
