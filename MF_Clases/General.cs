﻿using System;
using System.Web.Configuration;

namespace MF_Clases
{
    [Serializable]
    public static class General
    {
        public static string Ambiente = WebConfigurationManager.AppSettings["Ambiente"].ToString();

        public static string PrecioLista
        {
            get
            {
                string url = "";
                switch (Ambiente)
                {
                    case "DES":
                        url = GetAppSetting("PrecioLista");
                        break;
                    case "PRU":
                        url = GetAppSetting("PrecioListaPRU");
                        break;
                    case "PRO":
                        url = GetAppSetting("PrecioListaPRO");
                        break;
                }

                return url;
            }
        }

        public static string Etiquetas
        {
            get
            {
                string url = "";
                switch (Ambiente)
                {
                    case "DES":
                        url = GetAppSetting("Etiquetas");
                        break;
                    case "PRU":
                        url = GetAppSetting("EtiquetasPRU");
                        break;
                    case "PRO":
                        url = GetAppSetting("EtiquetasPRO");
                        break;
                }

                return url;
            }
        }

        public static string Jefes
        {
            get
            {
                string url = "";
                switch (Ambiente)
                {
                    case "DES":
                        url = GetAppSetting("Jefes");
                        break;
                    case "PRU":
                        url = GetAppSetting("JefesPRU");
                        break;
                    case "PRO":
                        url = GetAppSetting("JefesPRO");
                        break;
                }

                return url;
            }
        }

        public static string Supervisores
        {
            get
            {
                string url = "";
                switch (Ambiente)
                {
                    case "DES":
                        url = GetAppSetting("Supervisores");
                        break;
                    case "PRU":
                        url = GetAppSetting("SupervisoresPRU");
                        break;
                    case "PRO":
                        url = GetAppSetting("SupervisoresPRO");
                        break;
                }

                return url;
            }
        }

        public static string Recursos
        {
            get
            {
                string url = "";
                switch (Ambiente)
                {
                    case "DES":
                        url = GetAppSetting("Recursos");
                        break;
                    case "PRU":
                        url = GetAppSetting("RecursosPRU");
                        break;
                    case "PRO":
                        url = GetAppSetting("RecursosPRO");
                        break;
                }

                return url;
            }
        }

        public static string Contabilidad
        {
            get
            {
                string url = "";
                switch (Ambiente)
                {
                    case "DES":
                        url = GetAppSetting("Contabilidad");
                        break;
                    case "PRU":
                        url = GetAppSetting("ContabilidadPRU");
                        break;
                    case "PRO":
                        url = GetAppSetting("ContabilidadPRO");
                        break;
                }

                return url;
            }
        }

        public static string Mercadeo
        {
            get
            {
                string url = "";
                switch (Ambiente)
                {
                    case "DES":
                        url = GetAppSetting("Mercadeo");
                        break;
                    case "PRU":
                        url = GetAppSetting("MercadeoPRU");
                        break;
                    case "PRO":
                        url = GetAppSetting("MercadeoPRO");
                        break;
                }

                return url;
            }
        }

        public static string RecibosAsociar
        {
            get
            {
                string url = "";
                switch (Ambiente)
                {
                    case "DES":
                        url = GetAppSetting("RecibosAsociar");
                        break;
                    case "PRU":
                        url = GetAppSetting("RecibosAsociarPRU");
                        break;
                    case "PRO":
                        url = GetAppSetting("RecibosAsociarPRO");
                        break;
                }

                return url;
            }
        }

        public static string FiestaNetRestService
        {
            get
            {
                string url = "";
                switch (Ambiente)
                {
                    case "DES":
                        url = GetAppSetting("FiestaNETRestService");
                        break;
                    case "PRU":
                        url = GetAppSetting("FiestaNETRestServicePRU");
                        break;
                    case "PRO":
                        url = GetAppSetting("FiestaNETRestServicePRO");
                        break;
                }

                return url;
            }
        }

        public static string FiestaNETService
        {
            get
            {
                string url = "";
                switch (Ambiente)
                {
                    case "DES":
                        url = GetAppSetting("FiestaNETService");
                        break;
                    case "PRU":
                        url = GetAppSetting("FiestaNETServicePRU");
                        break;
                    case "PRO":
                        url = GetAppSetting("FiestaNETServicePRO");
                        break;
                }

                return url;
            }
        }

        public static int VigenciaCotizaciones
        {
            get
            {
                int vigencia;
                bool converted = int.TryParse(GetAppSetting("VigenciaCotizaciones"), out vigencia);

                if (!converted)
                    throw new Exception("Error al tratar de convertir en entero la vigencia de cotizaciones desde las configuraciones");

                return vigencia;
            }
        }

        public static string PortalService
        {
            get
            {
                string url = "";
                switch (Ambiente)
                {
                    case "DES":
                        url = GetAppSetting("Portal");
                        break;
                    case "PRU":
                        url = GetAppSetting("PortalPRU");
                        break;
                    case "PRO":
                        url = GetAppSetting("PortalPRO");
                        break;
                }

                return url;
            }
        }

        public static string URLPrincipal
        {
            get
            {
                string url = "";
                switch (Ambiente)
                {
                    case "DES":
                        url = GetAppSetting("UrlPrincipalDES");
                        break;
                    case "PRU":
                        url = GetAppSetting("UrlPrincipalPRU");
                        break;
                    case "PRO":
                        url = GetAppSetting("UrlPrincipalPRO");
                        break;
                }

                return url;
            }
        }

        public static string CookieName
        {
            get
            {
                return GetAppSetting("CookieName");
            }
        }

        public static double RenewCookieExpire
        {
            get
            {
                return double.Parse(GetAppSetting("RenewCookieExpire"));
            }
        }

        public static string ComisionesNomina
        {
            get
            {
                return GetAppSetting("ComisionesNomina");
            }
        }

        public static int ComisionesConcepto
        {
            get
            {
                return int.Parse(GetAppSetting("ComisionesConcepto"));
            }
        }

        public static string ZohoServicesDeptId
        {
            get {
                return GetAppSetting("ZohoServicesDeptId");
            }
        }
        public static string TextoAgrupacionEspecial
        {
            get
            {
                return GetAppSetting("TextoAgrupacionEspecial");
            }
        }

        public static string NetunimService
        {
            get
            {
                string url = "";
                switch (Ambiente)
                {
                    case "DES":
                        url = GetAppSetting("NetunimDev");
                        break;
                    case "PRU":
                        url = GetAppSetting("NetunimPru");
                        break;
                    case "PRO":
                        url = GetAppSetting("NetunimPro");
                        break;
                }
                return url;
            }
        }

        public static string ApiAutenticacion => GetAppSetting("ApiAutenticacion");

        public static string EndpointValidarPermiso => GetAppSetting("validarPermiso");
        public static string EndpointValidarSincronizarComision => GetAppSetting("sincronizarComision");
        public static string S3Recibo => GetAppSetting("s3Recibos");
        public static string S3Depositos => GetAppSetting("s3Depositos");
        public static string S3Cierres => GetAppSetting("s3Cierres");

        

        public static Guid AppkeyAutenticacion => Guid.Parse(GetAppSetting("AppkeyAutenticacion"));

        private static string GetAppSetting(string key)
        {
            return WebConfigurationManager.AppSettings[key].ToString();
        }
    }
}
