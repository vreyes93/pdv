﻿using System;

namespace MF_Clases.GuateFacturas.FEL
{
    [Serializable]
    public class AnulacionDoc
    {

        /// <value>NIT del emisor.</value>
        public string NITEmisor { get; set; }

        /// <value>Serie del documento FEL.</value>
        public string Serie { get; set; }

        /// <value>.</value>
        public string Preimpreso { get; set; }

        /// <value>.</value>
        public string NitComprador { get; set; }

        /// <value>.</value>
        public DateTime FechaAnulacion { get; set; }

        /// <value>.</value>
        public string MotivoAnulacion { get; set; }

    }
}
