﻿using System;

namespace MF_Clases.GuateFacturas.FEL
{
    [Serializable]
    public class ContingenciaDto
    {
        public string Cobrador { get; set; }
        public string Lote { get; set; }
        public string NumAcceso { get; set; }
        public string FacturaElectronica { get; set; }
        public DateTime FechaIni { get; set; }
        public DateTime FechaFin { get; set; }
        public bool Utilizado { get; set; }

    }
}
