﻿using System;
using System.Collections.Generic;

namespace MF_Clases.GuateFacturas.FEL
{
    [Serializable]
    public class ContingenciaRespuesta
    {
        public List<string> LstAccesos { get; set; }

        public ContingenciaRespuesta()
        {
            LstAccesos = new List<string>();
        }
    }

}
