﻿using System;

namespace MF_Clases.GuateFacturas.FEL
{
    [Serializable]
    public class ContingenciaSolicitud
    {
        public string NitEmisor { get; set; }
        public int Establecimiento { get; set; }
        public string Lote { get; set; }
        public int Cantidad { get; set; }
    }
}
