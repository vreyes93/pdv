﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MF_Clases.GuateFacturas.FEL.Contingencia
{
    /// <summary>
    /// Clase que servirá para devolver la información de los números de contingencia
    /// que se han utilizado en lugar de las facturas electrónicas, las cuales debe
    /// buscarse  la información de las facturas para posteriormente firmarse y reemplazarse
    /// por números de factura electrónica válidos
    /// 
    /// </summary>
  public class UsadosContingencia
    {
        public string Cobrador { get; set; }
        public List<string> NumAccesoUsados { get; set; }
    }
}
