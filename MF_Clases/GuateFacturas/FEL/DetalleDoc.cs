﻿using MF_Clases.Utils;
using System;

namespace MF_Clases.GuateFacturas.FEL
{
    [Serializable]
    public class DetalleDoc
    {
        public int Linea { get; set; }
        /// <value>Código del producto a ser cargado.</value>
        public string Producto { get; set; }

        /// <value>Descripción del producto a cargar.</value>
        public string Descripcion { get; set; }

        /// <value>Identificador único de unidad de medida (1=Unidad).</value>
        public string Medida{ get; set; }

        /// <value>Cantidad del producto que se está cargando.</value>
        public decimal Cantidad { get; set; }

        /// <value>Precio unitario del producto, IVA incluido.</value>
        public decimal Precio { get; set; }

        /// <value>Porcentaje de descuento aplicado al producto.</value>
        public decimal PorcDesc { get; set; }

        /// <value>Importe Bruto resultado de precio por cantidad.</value>
        public decimal ImpBruto { get; set; }

        /// <value>Importe Descuento aplicado al producto.</value>
        public decimal ImpDescuento { get; set; }

        /// <value>Importe Exento de cálculo de IVA.</value>
        public decimal ImpExento { get; set; }

        /// <value>Importe de Otros impuestos.</value>
        public decimal ImpOtros { get; set; }

        /// <value>Importe Neto luego de restar descuentos y que está sujeto de IVA.</value>
        public decimal ImpNeto { get; set; }

        /// <value>Importe ISR aplicado al documento.</value>
        public decimal ImpIsr { get; set; }

        /// <value>Importe IVA aplicado sobre importe neto.</value>
        public decimal ImpIva { get; set; }

        /// <value>Importe total del producto.</value>
        public decimal ImpTotal { get; set; }

        /// <value>Indica si el tipo de venta corresponde a B=Bienes o S=Servicios.</value>
        public string TipoVentaDet{ get; set; }

        public DetalleDoc()
        {
            this.Medida = CONSTANTES.GUATEFAC.MEDIDA.UNIDAD;
            this.TipoVentaDet = CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES;
        }
    }
}
