﻿using MF_Clases.Utils;
using System;
using System.Collections.Generic;

namespace MF_Clases.GuateFacturas.FEL
{
    [Serializable]
    public class Doc
    {

        //------------------------------------------------------------------------------------------------------------------------------------
        //--Receptor
        //------------------------------------------------------------------------------------------------------------------------------------        
        #region Receptor
        /// <value>NIT del cliente comprador.</value>
        public string NITReceptor { get; set; }

        /// <value>Nombre del cliente comprador (Opcional, debemandarse si es Consumidor Final).</value>
        public string Nombre { get; set; }

        /// <value>Direccion del cliente comprador (Opcional, debemandarse si es Consumidor Final).</value>
        public string Direccion { get; set; }
        //------------------------------------------------------------------------------------------------------------------------------------
        /// <value>Numero de documento de identificacion DPI para Factura Especial.</value>
        public string NumeroIdentificacion { get; set; }

        /// <value>País de emisión del documento para Factura Especial.</value>
        public int PaisEmision { get; set; }

        /// <value>Codigo del departamento de emisión del documento para Factura Especial.</value>
        public int Departamento { get; set; }

        /// <value>Codigo del municipio de emisión del documento para Factura Especial.</value>
        public int Municipio { get; set; }

        /// <value>Porcentaje ISR aplicado al documento para Factura Especial.</value>
        public decimal PorcISR  { get; set; }
        
        //------------------------------------------------------------------------------------------------------------------------------------
        #endregion

        //------------------------------------------------------------------------------------------------------------------------------------
        //--Emisor
        //------------------------------------------------------------------------------------------------------------------------------------        
        #region Emisor
        /// <value>NIT del emisor.</value>
        public string NITEmisor { get; set; }

        /// <value>Id de la maáquina para facturar.</value>
        public string IdMaquina { get; set; }

        /// <value>Estableciniento donde se va a generar la factura</value>
        public int Establecimiento { get; set; }

        /// <value>Tipo de documento a utilizar.</value>
        ///public int TipoDoc { get; set; }

        //------------------------------------------------------------------------------------------------------------------------------------
        #endregion

        //------------------------------------------------------------------------------------------------------------------------------------
        //--Informacion del documento
        //------------------------------------------------------------------------------------------------------------------------------------
        #region InfoDoc
        /// <value>Indica si el tipo de venta corresponde a B=Bienes o S=Servicios.</value>
        public string TipoVenta { get; set; }

        /// <value>Indica el país de destino de la venta. (1=Guatemala).</value>
        public string DestinoVenta { get; set; }

        /// <value>Fecha del documento en formato DD/MM/YYYY.</value>
        public DateTime Fecha { get; set;}

        /// <value>Código de moneda siendo 1=Quetzal, 2= Dólar.</value>
        public string Moneda { get; set; }

        /// <value>Tasa de cambio según mooneda (1 cuando la modena es quetzal).</value>
        public decimal Tasa { get; set; }

        /// <value>Identificador único en el sistema original, datos obligatorioa para realizar reversiones.</value>
        public string Referencia { get; set; }

        /// <value>Número aleatorio, se utiliza como referencia para una operación que haya sido realizada en contingencia..</value>
        public string NumeroAcceso { get; set; }

        /// <value>Serie administrativa para facilitar el contro de los documentos en FEL.</value>
        public string SerieAdmin { get; set; }

        /// <value>Número administrativo para facilitar el control de los documentos en FEL.</value>
        public string NumeroAdmin { get; set; }

        /// <value>Opcional, indica si debe anular el documento anteriormente generado, "S" para revertir la operación, 
        /// "N" o vacío para no hacer reversión, tambienún puede no incluirse el TAG.</value>
        public string Reversion { get; set; }

        /// <value>Indica el tipo de documento a firmar FACTURA=1, FACTURA_CAMBIARIA=2, FACTURA_PEQUENO_CONTRIB=3, FACTURA_CAMBIARIA_PEQUENO_CONTRIB=4, FACTURA_ESPECIAL=5, NOTA_ABONO=6, RECIBO_POR_DONACION=7, RECIBO=8, NOTA_DEBITO=9, NOTA_CREDITO=10</value>
        public int TipoDocumento { get; set; }

        /// <value>Tipo de servicio a consumir GENERAR_DOCUMENTO o bien ANULAR_DOCUMENTO</value>
        public string Servicio { get; set; }
        //------------------------------------------------------------------------------------------------------------------------------------
        #endregion

        //------------------------------------------------------------------------------------------------------------------------------------
        //Totales
        //------------------------------------------------------------------------------------------------------------------------------------
        #region Totales
        /// <value>Importe bruto luego de calcular precio por cantidad.</value>
        public decimal Bruto { get; set; }

        /// <value>Importe descuento aplicado a cada documento.</value>
        public decimal Descuento { get; set; }

        /// <value>Exento de cálculo de IVA.</value>
        public decimal Exento { get; set; }

        /// <value>Importe otros impuestos no afectos al IVA.</value>
        public decimal Otros { get; set; }

        /// <value>Importe neto sujeto a cálculos de IVA.</value>
        public decimal Neto { get; set; }

        /// <value>Importe ISR aplicado al documento .</value>
        public decimal Isr { get; set; }

        /// <value>Importe IVA aplicado sobre importe neto.</value>
        public decimal Iva { get; set; }

        /// <value>Importe total del documento.</value>
        public decimal Total { get; set; }
        //------------------------------------------------------------------------------------------------------------------------------------
        #endregion

        //------------------------------------------------------------------------------------------------------------------------------------
        //Datos adicionales
        //------------------------------------------------------------------------------------------------------------------------------------
        #region DatosAdicionales
        /// <value>Cuenta de correo del cliente para entrega de la factura electrónica.</value>
        public string Email { get; set; }

        /// <value>Campo que indica si se envia o no el documento por correo.</value>
        public string Enviar { get; set; }

        /// <value>Campo que indica el INCOTERM
        /// EXW | En fábrica En fábrica
        /// FCA | Libre transportista
        /// FAS | Libre al costado del buque
        /// FOB | Libre a bordo
        /// CFR | Costo y flete
        /// CIF | Costo, seguro y flete
        /// CPT | Flete pagado hasta
        /// CIP | Flete y seguro pagado hasta
        /// DDP | Entregado en destino con derechos pagados
        /// DAP | Entregada en lugar
        /// DAT | Entregada en terminal
        /// ZZZ | Otros
        /// </value>
        public string Incoterm { get; set; }
        /// <value>Campo que indica el codigo de consignatario o destinatario.</value>
        public string CodigoConsignatarioODestinatario { get; set; }
        /// <value>Campo que indica el nombre de consignatario o destinatario.</value>
        public string NombreConsignatarioODestinatario { get; set; }
        /// <value>Campo que indica la direccion de consignatario o destinatario.</value>
        public string DireccionConsignatarioODestinatario { get; set; }
        /// <value>Campo que indica el codigo del comprador.</value>
        public string CodigoComprador { get; set; }
        /// <value>Campo que indica el nombre del comprador.</value>
        public string NombreComprador { get; set; }
        /// <value>Campo que indica la direccion del comprador.</value>
        public string DireccionComprador { get; set; }
        /// <value>Campo que indica otra referencia.</value>
        public string OtraReferencia { get; set; }
        /// <value>Campo que indica el codigo de exportador.</value>
        public string CodigoExportador { get; set; }

        public bool EsExportacion { get; set; }

        //------------------------------------------------------------------------------------------------------------------------------------
        #endregion

        //------------------------------------------------------------------------------------------------------------------------------------
        //Solo aplica para Facturas Cambiarias
        //------------------------------------------------------------------------------------------------------------------------------------
        #region AbonosFacturaCambiaria
        /// <value>Numero correlativo del Abono.</value>
        public string NumeroAbono { get; set; }

        /// <value>Fecha del Vencimiento del Abono.</value>
        public DateTime FechaVencimiento { get; set; }

        /// <value>Monto del Abono a realizar.</value>
        public decimal MontoAbono { get; set; }
        //------------------------------------------------------------------------------------------------------------------------------------
        #endregion

        //------------------------------------------------------------------------------------------------------------------------------------
        //Doc Asociados: Opcional solo para Notas de crédito y débito
        //------------------------------------------------------------------------------------------------------------------------------------
        #region DocAsociados
        /// <value>Serie del documento asociado a la Nota de Débito o Crédito.</value>
        public string DASerie{ get; set; }
        
        /// <value>Preimpreso del documento asociado a la Nota de Débito o Crédito.</value>
        public string DAPreimpreso { get; set; }
        //------------------------------------------------------------------------------------------------------------------------------------
        #endregion

        /// <value>Lista de productos del documento.</value>
        public List<DetalleDoc> Detalle { get; set; }

        public Doc()
        {
            this.TipoVenta = CONSTANTES.GUATEFAC.TIPO_VENTA.BIENES;
            this.DestinoVenta = CONSTANTES.GUATEFAC.DESTINO_VENTA.GT;
            this.Moneda = CONSTANTES.GUATEFAC.MONEDA.GTQ;
            this.Tasa = CONSTANTES.GUATEFAC.TASA.GTQ;
            this.Reversion = CONSTANTES.GUATEFAC.REVERSION.NO;
            this.EsExportacion = false;
            this.Enviar = "N";
            this.Detalle = new List<DetalleDoc>();

        }
    }
}
