﻿using System;

namespace MF_Clases.GuateFacturas.FEL.ImpresionFEL
{
    [Serializable]
    public class ImpDoc
    {
        //Datos del encabezado de la factura
        #region "encabezado factura"
        public string RazonSocial { get; set; }
        public string NombreEmpresa { get; set; }
        public string DireccionEmpresa { get; set; }
        public string TelefonoEmpresa { get; set; }
        //nit con formato ######-# de la empresa
        public string NitEmpresa { get; set; }
        public string SerieFEL { get; set; }
        public string NumeroFEL { get; set; }
        public string FirmaFEl { get; set; }
        public DateTime FechaFactura { get; set; }
        public string CantidadEnLetras { get; set; }
        public decimal GranTotal { get; set; }
        public int TipoDoc { get; set; }
        public string Tienda { get; set; }
        //a imprimirse en el documento p.e. FACTURA/NOTA DE CRÉDITO/NOTA DE DÉBITO
        public string OrdenDeCompra { get; set; }
        public string Cliente { get; set; }
        public string Anulado { get; set; }
        #endregion
        #region "Datos Cliente"
        // nit con formato #######-#
        public string NitCliente { get; set; }
        public string NombreCliente { get; set; }
        public string DirCliente { get; set; }
        #endregion
        public string NombreCertificadorFEL { get; set; }
        public string NItCertificadorFEL { get; set; }
        public string MesajeEnContingencia { get; set; }

        //DETALLE
        public string Producto { get; set; }
        public string Descripcion { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Precio { get; set; }
        public decimal TotalProducto { get; set; }
        public string Moneda { get; set; }
        //para notas de crédito y débito
        public string SeriePreimpreso { get; set; }
        public string NumeroPreimpreso { get; set; }
        //si estuvo en contingencia
        public string ValorDeAcceso { get; set; }
        public decimal Descuento { get; set; }
    }
}
