﻿using System;

namespace MF_Clases.GuateFacturas.FEL
{
    [Serializable]
    public class Resultado
    {

        #region GenerarDocumento
        /// <value>Identificador que corresponde a la parte inicial del numero de factura, diferenciador de actividades de la empresa. Generado por SAT.</value>
        public string Serie { get; set; }

        /// <value>Identificador que corresponde al correlativo del numero de factura. Generado aleatoriamente por SAT.</value>
        public string Preimpreso { get; set; }

        /// <value>numero de Autorizacion (UUID) Universal Unique Identifier generado por la SAT .</value>
        public string NumeroAutorizacion { get; set; }

        /// <value>Nombre del contribuyente de la factura.</value>
        public string Nombre { get; set; }

        /// <value>Domicilio fiscal del contribuyente asociado a la factura.</value>
        public string Direccion { get; set; }

        /// <value>Telefono del contribuyente asociado a la factura.</value>
        public string Telefono { get; set; }

        /// <value>.</value>
        //public string Firma { get; set; }
        #endregion

        #region Anulacion

        /// <value>Estado del documento tras finalizar la transacción.</value>
        public string Estado { get; set; }

        /// <value>NIT del emisor.</value>
        public string Emisor { get; set; }

        /// <value>NIT del comprador.</value>
        public string Comprador { get; set; }

        /// <value>Idetificador unico del sistema local.</value>
        public string Referencia { get; set; }
        #endregion

        #region Anulacion

        /// <value>Mensaje de error devuelto por GuateFacturas.</value>
        public string Error { get; set; }
        #endregion

        #region Comunes
        /// <value>Xml devuelto por GuateFacturas.</value>
        public string Xml { get; set; }
        #endregion

        /// <value>.</value>
        public string Firmada { get; set; }

        /// <value>.</value>
        public string Identificador { get; set; }

        /// <value>.</value>
        public string Maquina { get; set; }

        /// <value>.</value>
        public string Factura { get; set; }

        /// <value>.</value>
        public string FacturaElectronica { get; set; }

        /// <value>.</value>
        public string XmlError { get; set; }

        /// <value>.</value>
        public string XmlDocumento { get; set; }

        /// <value>.</value>
        public string ErrorComunicacion { get; set; }
    }
}
