﻿using System;

namespace MF_Clases.Reporte
{
    [Serializable]
    public enum DevexpressOpciones
    {
        PDF,
        BYTE
    }
}
