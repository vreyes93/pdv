﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MF_Clases.Restful
{
    [Serializable]
    public class Api
    {
        private string UrlApi { get; set; }
        private RestClient Client { get; set; }
        private RestRequest Request { get; set; }

        public Api(string urlapi)
        {
            this.UrlApi = urlapi;
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, errors) => true;
        }

        public string Process(Method requestMethod, string apiPart, object body)
        {
            this.Client = new RestClient(this.UrlApi);
            this.Request = new RestRequest(apiPart, requestMethod);
            this.Request.Timeout = 999999999;

            if (requestMethod != Method.GET)
                this.Request.AddJsonBody(body);

            IRestResponse WSR = Task.Run(() => this.Client.Execute(this.Request)).Result;
            if (WSR.StatusCode == HttpStatusCode.OK)
                return WSR.Content;
            else
                throw new Exception(WSR.StatusCode.ToString() + ": " + WSR.Content);
        }

        public IRestResponse Process(Method requestMethod, string apiPart, object body, Dictionary<string, string> headers = null, List<string> postedFiles = null)
        {
            this.Client = new RestClient(this.UrlApi);
            this.Request = new RestRequest(apiPart, requestMethod);
            this.Request.Timeout = 999999999;

            headers = headers ?? new Dictionary<string, string>();
            headers.ToList().ForEach(x =>
            {
                this.Request.AddHeader(x.Key, x.Value);
            });

            if (requestMethod != Method.GET)
                this.Request.AddJsonBody(body);

            if (postedFiles != null && postedFiles.Count > 0)
                postedFiles.ForEach(x => this.Request.AddFile(Path.GetFileNameWithoutExtension(x), x));

            return Task.Run(() => this.Client.Execute(this.Request)).Result;
        }
    }
}
