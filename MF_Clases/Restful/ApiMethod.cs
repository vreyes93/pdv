﻿using System;
using RestSharp;

namespace MF_Clases.Restful
{
    [Serializable]
    public enum ApiMethod
    {
        Get = Method.GET,
        Post = Method.POST,
        Put = Method.PUT,
        Delete = Method.DELETE
    }
}
