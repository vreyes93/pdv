﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MF_Clases.Utils
{
    [Serializable]
    public class CONSTANTES
    {
        [Serializable]
        public class GUATEFAC
        {
            [Serializable]
            public class SERVICIOS
            {
                public const string GENERAR_DOCUMENTO = "GENERAR_DOCUMENTO";
                public const string ANULAR_DOCUMENTO = "ANULAR_DOCUMENTO";
            }

            [Serializable]
            public class TIPO_RESPUESTA
            {
                public const string RESUMEN = "R";
                public const string DETALLE = "D";
            }

            [Serializable]
            public class DOCUMENTO
            {
                public const int FACTURA = 1;
                public const int FACTURA_CAMBIARIA = 2;
                public const int FACTURA_PEQUENO_CONTRIB = 3;
                public const int FACTURA_CAMBIARIA_PEQUENO_CONTRIB = 4;
                public const int FACTURA_ESPECIAL = 5;
                public const int NOTA_ABONO = 6;
                public const int RECIBO_POR_DONACION = 7;
                public const int RECIBO = 8;
                public const int NOTA_DEBITO = 9;
                public const int NOTA_CREDITO = 10;
            }

            [Serializable]
            public class TIPO_DOCUMENTO_IDENTIFICACION
            {
                public const int DPI = 2;
            }

            [Serializable]
            public class TIPO_VENTA
            {
                public const string BIENES = "B";
                public const string SERVICIOS = "S";
            }

            [Serializable]
            public class DESTINO_VENTA
            {
                public const string GT = "1";
            }

            [Serializable]
            public class MONEDA
            {
                public const string GTQ = "1";
                public const string DOL = "2";
            }

            [Serializable]
            public class TASA
            {
                public const decimal GTQ = 1;
            }

            [Serializable]
            public class MEDIDA
            {
                public const string UNIDAD = "1";
            }

            [Serializable]
            public class REVERSION
            {
                public const string SI = "S";
                public const string NO = "N";
            }

            [Serializable]
            public class CLIENTE
            {
                public const string MF = "0000004560";
            }

            [Serializable]
            public class EMISOR
            {
                public const string NIT= "5440998";
            }

            [Serializable]
            public class MODALIDAD_FEL
            {
                public static DateTime FECHA_INICIO_MODALIDAD_FEL = new DateTime(2019,6, 25);
                public const int NUMERO_DIAS_MAX_FIRMA_DOCUMENTOS = 5;
                public const string CODIGO_CONSECUTIVO_FEL = "FEL";
            }

            [Serializable]
            public class EXPORTACION
            {
                public const string CLIENTE_EXPORTACION = "0027165";
            }

            [Serializable]
            public class ERRORES
            {
                public const int NO_EXISTE_EL_NIT_DEL_CONTRIBUYENTE = 2;
            }

            [Serializable]
            public class RESOLUCION_DOC
            {
                public const String CONSECUTIVO_RESOLUCION_NOTA_CREDITO = "F01NC";
                public const String CONSECUTIVO_RESOLUCION_NOTA_DEBITO = "F01ND";
                public const String CONSECUTIVO_RESOLUCION_NOTA_ABONO = "F01NA";
                public const String CONSECUTIVO_RESOLUCION_FACTURA_CREDITO = "CRE";
                public const String CONSECUTIVO_RESOLUCION_FACTURA_CONTADO = "CON";
            }

            [Serializable]
            public class GFACE
            {
                public const string TIPO_ACTIVO= "CNCE5";
            }

            [Serializable]
            public class MAQUINA
            {
                public const string FACTURACION_DEFAULT = "001";
                public const string FACTURACION_TIENDA_VIRTUAL_CRED = "009";
            }

            [Serializable]
            public class MAQUINA_CREDIPLUS
            {
                public const string FACTURACION_DEFAULT = "1";
                public const string FACTURACION_PHOENIX = "2";
            }
        }

        [Serializable]
        public class CONTINGENCIA
        {
            public const int TAMANIO_LOTE_SOLICITUD = 10;
            
            //Este número de factura sirve para actualizar los registros que tienen
            //un número de contingencia o de Acceso en lugar de su factura electrónica
            //para colocarles el verdadero número de factura se pasan los registros
            //primero con este número de factura
            public const string FACTURA_CONTINGENCIA_TRASPASO_FEL = "A-000000";
            public const string NOTA_CREDITO_CONTINGENCIA_TRASPASO_FEL = "MT-000000";
        }

        [Serializable]
        public class IMPRESION
        {
            public const string NOMBRE_EMPRESA = "PRODUCTOS MULTIPLES SOCIEDAD ANÓNIMA";
            public const string RAZON_SOCIAL = "MULTIPRODUCTOS";
            public const string CERTIFICADOR_FEL = "GUATEFACTURAS SOCIEDAD ANONIMA";
            public const string NIT_CERTIFICADOR_FEL = "5640773-4";
            public const string MENSAJE_CONTINGENCIA = "DOCUMENTO TEMPORAL";
            public const string URL_IMPRESION_FIESTA_DEFAULT = "Report/FacturaMF_FEL/";
            public const string URL_IMPRESION_MAYOREO_DEFAULT = "Report/FacturaMayoreo_FEL/";
        }

        [Serializable]
        public class CATALOGOS
        {
            public const int GUATEFACTURAS_FEL =59;
            public const string MODULO_FEL = "HABILITAR_FEL";

            public const string COPIA_CORREO_FIRMA_NC_ND = "COPIA ENVIO NOTAS NC/ND";
            public const string DESTINATARIOS_CORREO_FIRMA_NC_ND = "DESTINATARIOS FIRMA N/C Y N/D FEL";
            public const int NOTIFICACIONES_DESARROLLO_PRUEBAS = 10;

            public const int CATEGORIAS_SUPERVISORES = 47;

        }

        [Serializable]
        public class OUTLET
        {
            public const string VALOR_OUTLET = "OUTLET";
            public const string ATRIBUTO_OUTLET = "SEGMENTO_TIENDA";
        }

        [Serializable]
        public class FINANCIERA
        {
            public const int CREDIPLUS = 12;
        }

        [Serializable]
        public class CATEGORIA_SUPERVISORES
        {
            public const string SupervisorTipo1 = "S1";
            public const string SupervisorTipo2 = "S2";
        }

        [Serializable]
        public class CLASIFICACION_ARTICULOS
        {
            public const int DECORACION = 19;
        }

        [Serializable]
        public class CREDIPLUS
        {
            public const int LONGITUD_CLIENTE = 10;
            public const string VENDEDOR = "ND";
            public const string COBRADOR = "ND";
            public const string CONDICION_PAGO = "0";
            public const string MUNICIPIO_DEFAULT = "Guatemala";
            public const string DEPARTAMENTO_DEFAULT = "Villa Nueva";
            public const string ZONA_DEFAULT = "1001";
            public const string IDENT_DEFAULT = "000000000000";
        }
    }
}

