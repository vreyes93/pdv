﻿using MF_Clases.Restful;
using System;

namespace MF_Clases.Utils
{
    [Serializable]
    public static class Validar
    {
        public static bool RequiereArmado(string codigoArticulo)
        {
            try
            {
                if (string.IsNullOrEmpty(codigoArticulo) || string.IsNullOrWhiteSpace(codigoArticulo))
                    throw new Exception("El código del artículo es requerido");

                Api api = new Api(General.FiestaNetRestService);
                string result = api.Process(RestSharp.Method.GET, "/Validar/RequiereArmado?codigoArticulo=" + codigoArticulo, null);

                return Convert.ToBoolean(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool CompatibilidadArmadoEntreTiendaProducto(string codigoTienda, string codigoArticulo)
        {
            try
            {
                if (string.IsNullOrEmpty(codigoArticulo) || string.IsNullOrWhiteSpace(codigoArticulo))
                    throw new Exception("El código del artículo es requerido");

                Api api = new Api(General.FiestaNetRestService);
                string result = api.Process(RestSharp.Method.GET, "/Validar/CompatibilidadArmadoEntreTiendaProducto?codigoTienda=" + codigoTienda + "&codigoArticulo=" + codigoArticulo, null);

                return Convert.ToBoolean(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool CompatibilidadTraspasoTiendaProducto(string bodegaOrigen, string bodegaDestino, string codigoArticulo, string descripcionArticulo)
        {
            try
            {
                Api api = new Api(General.FiestaNetRestService);
                string result = api.Process(RestSharp.Method.GET, "/Validar/CompatibilidadTraspasoTiendaProducto?bodegaOrigen=" + bodegaOrigen 
                    + "&bodegaDestino=" + bodegaDestino + "&codigoArticulo=" + codigoArticulo + "&descripcionArticulo=" + descripcionArticulo, null);

                return Convert.ToBoolean(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
