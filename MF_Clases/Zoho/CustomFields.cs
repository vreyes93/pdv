﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MF_Clases.Zoho
{
    [Serializable]
    public class CamposPersonalizados
    {
        public string cf_numero_de_factura { get; set; }
        public string cf_solicitud { get; set; }
        public string cf_origen { get; set; }
        public string cf_tipo_de_garantia { get; set; }
        public string cf_nombre_del_distribuidor { get; set; }

        public string cf_nombre_del_comprador { get; set; }

        public string cf_direccion_de_entrega { get; set; }
        public string cf_nombre_del_armador { get; set; }

        public string cf_nombre_transportista { get; set; }
        public string cf_usuario { get; set; }
        public string cf_descripcion { get; set; }
        public string cf_numero_de_envio { get; set; }
        public string cf_fecha_de_entrega { get; set; }
        public string cf_fecha { get; set; }
    }
}
