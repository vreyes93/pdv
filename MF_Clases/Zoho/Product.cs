﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MF_Clases.Zoho
{
    public class Product
    {
        public string productName { get; set; }
        public string productCode { get; set; }
        public decimal unitPrice { get; set; }
        public string id { get; set; }
        public List<string> departmentIds { get; set; }
    }
}
