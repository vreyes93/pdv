﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MF_Clases.Zoho
{
    [Serializable]
    public class Ticket
    {
        public string email { get; set; }
        public string phone { get; set; }
        public string subject { get; set; }
        public string status { get; set; }
        public string statusType { get; set; }
        public string departmentId { get; set; }
       
        public string productId { get; set; }
        public string assigneeId { get; set; }
        public string teamId { get; set; }
        public Contacto contact { get; set; }
        public CamposPersonalizados cf { get; set; }
    }
}
