﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MF_Clases.Zoho
{
    public class TicketRequest
    {
        public Ticket ticket { get; set; }
        public Product product { get; set; }
    }
}
