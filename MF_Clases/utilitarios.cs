﻿using System;

namespace MF_Clases
{
    [Serializable]
    public class Utilitarios
    {
        public static string FormatoDDMMYYYY(DateTime Fecha)
        {
            return string.Format("{0}{1}/{2}{3}/{4}", Fecha.Day < 10 ? "0" : "", Fecha.Day.ToString(), Fecha.Month < 10 ? "0" : "", Fecha.Month.ToString(), Fecha.Year.ToString());
        }

        public static string FormatoYYYYMMDD(DateTime Fecha)
        {
            return string.Format("{0}-{1}{2}-{3}{4}", Fecha.Year.ToString(), Fecha.Month < 10 ? "0" : "", Fecha.Month.ToString(), Fecha.Day < 10 ? "0" : "", Fecha.Day.ToString());
        }

        public static string NitGFace(string nit)
        {
            string mCeros = ""; string mNitSinGuion = nit.Replace("-", "");

            switch (mNitSinGuion.Length)
            {
                case 1:
                    mCeros = "00000000000";
                    break;
                case 2:
                    mCeros = "0000000000";
                    break;
                case 3:
                    mCeros = "000000000";
                    break;
                case 4:
                    mCeros = "00000000";
                    break;
                case 5:
                    mCeros = "0000000";
                    break;
                case 6:
                    mCeros = "000000";
                    break;
                case 7:
                    mCeros = "00000";
                    break;
                case 8:
                    mCeros = "0000";
                    break;
                case 9:
                    mCeros = "000";
                    break;
                case 10:
                    mCeros = "00";
                    break;
                case 11:
                    mCeros = "0";
                    break;
                default:
                    mCeros = "";
                    break;
            }

            return string.Format("{0}{1}", mCeros, mNitSinGuion);
        }

        /// <summary>
        /// Devuelve un DateTime a partir de una cadena
        /// </summary>
        /// <param name="Fecha">string fecha</param>
        /// <returns></returns>
        public static DateTime dateParseFormatDMY(string Fecha)
        {
            string dia = Fecha.Substring(0, Fecha.IndexOf("/"));
            string mes = Fecha.Substring(Fecha.IndexOf("/") + 1, Fecha.IndexOf("/"));
            string anio = Fecha.Substring(Fecha.LastIndexOf("/") + 1, Fecha.Length - (Fecha.LastIndexOf("/") + 1));
            return new DateTime(int.Parse(anio), int.Parse(mes), int.Parse(dia));
        }

        /// <summary>
        /// Método para codificar una cadena
        /// </summary>
        /// <param name="cadena"></param>
        /// <returns></returns>
        public static string EncodeString(string cadena)
        {
            byte[] toEncodeAsBytes = System.Text.Encoding.UTF8.GetBytes(cadena);
            string returnValue64 = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue64;
        }

        /// <summary>
        /// Método para Decodificar una cadena 
        /// </summary>
        /// <param name="cadena64"></param>
        /// <returns></returns>
        public static string DecodeStringFrom64(string cadena64)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(cadena64);
            string returnValue = System.Text.Encoding.UTF8.GetString(encodedDataAsBytes);
            return returnValue;
        }
    }
}
