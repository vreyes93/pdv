﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RootProject.Startup))]
namespace RootProject
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
