﻿<%@ Page Title="Menú de Pedidos de Camas" Language="C#" MasterPageFile="~/SimpleMaster.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="PedidosCamas._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="menu">
    <style type="text/css">
        .style1
        {
            text-align: center;
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="content">
    <div id="divContenido">
    &nbsp;&nbsp;&nbsp;
    <table align="center">
        <tr>
            <td class="style1">
                <strong>MENU DE SISTEMA DE PEDIDOS DE CAMAS</strong></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Button ID="btnRelacionar" runat="server" onclick="btnRelacionar_Click" 
                    Text="Relacionar Fundas con Bases y Colchones" 
                    
                    
                    ToolTip="Esta opción le permitirá relacionar un código de funda con uno o varios marcos y/ colchones" 
                    Width="300px" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:TextBox ID="FundaEspecifica" runat="server" TabIndex="10" Width="105px"></asp:TextBox>
                <asp:Button ID="btnFundaEspecifica" runat="server" 
                    onclick="btnFundaEspecifica_Click" TabIndex="20" Text="Ver Funda Específica" 
                    Width="185px" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Button ID="btnEstructuraRelacion" runat="server" 
                    onclick="btnEstructuraRelacion_Click" 
                    Text="Relación Fundas vrs Bases y Colchones" Width="300px" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSugeridoPedido" runat="server" 
                    onclick="btnSugeridoPedido_Click" 
                    Text="Generar Sugerido de Pedido" Width="300px" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnPedidos" runat="server" 
                    onclick="btnPedidos_Click" 
                    Text="Ver Listado de Pedidos" Width="300px" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Button ID="btnRegresar" runat="server" 
                    onclick="btnRegresar_Click" 
                    Text="Regresar" Width="300px" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</div>
</asp:Content>
