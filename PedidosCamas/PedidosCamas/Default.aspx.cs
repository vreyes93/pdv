﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PedidosCamas
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Session["Ambiente"] = "DES"; //DESARROLLO
                Session["Ambiente"] = "PRO"; //PRODUCCION
                //Session["Ambiente"] = "PRU"; //PRUEBAS

                Session["UrlPrecios"] = string.Format("http://sql.fiesta.local/services/wsCambioPrecios.asmx", Request.Url.Host);
            }
        }

        protected void btnRelacionar_Click(object sender, EventArgs e)
        {
            Session["ArticuloRelacion"] = null;
            Response.Redirect("RelacionarCodigos.aspx");
        }

        protected void btnFundaEspecifica_Click(object sender, EventArgs e)
        {
            if (FundaEspecifica.Text.Trim().Length < 10)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('El código de funda ingresado es inválido.');", true);
                FundaEspecifica.Text = "";
                FundaEspecifica.Focus();
                return;
            }

            Session["ArticuloRelacion"] = FundaEspecifica.Text;
            Response.Redirect("RelacionarCodigos.aspx");
        }

        protected void btnEstructuraRelacion_Click(object sender, EventArgs e)
        {
            Response.Redirect("EstructuraRelacion.aspx");
        }

        protected void btnSugeridoPedido_Click(object sender, EventArgs e)
        {
            Response.Redirect("SugeridoPedido.aspx");
        }

        protected void btnPedidos_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pedidos.aspx");
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            //Response.Redirect(string.Format("http://{0}/FiestaNet/Default.aspx", Request.Url.Host));
        }

    }
}
