﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SimpleMaster.Master" AutoEventWireup="true" CodeBehind="EstructuraRelacion.aspx.cs" Inherits="PedidosCamas.EstructuraRelacion" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="menu" runat="server">
    <style type="text/css">
        .style1
        {
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="divContenido">
    <table align="center">
        <tr>
            <td>
                &nbsp;</td>
            <td style="text-align: center" class="style1">
                <strong>ESTRUCTURA DE RELACION DE FUNDAS VRS. BASES Y/O COLCHONES</strong></td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td style="text-align: center">
                <asp:LinkButton ID="lbVerTienenBase" runat="server" 
                    onclick="lbVerTienenBase_Click" 
                    ToolTip="Filtra el listado por las fundas que SI tienen base relacionada">Sí Base</asp:LinkButton>
&nbsp;<asp:LinkButton ID="lbVerNoTienenBase" runat="server" onclick="lbVerNoTienenBase_Click" 
                    ToolTip="Filtra el listado por las fundas que NO tienen base relacionada">No Base</asp:LinkButton>
&nbsp;<asp:LinkButton ID="lbVerTienenColchones" runat="server" 
                    onclick="lbVerTienenColchones_Click" 
                    ToolTip="Filtra el listado por las fundas que SI tienen colchón relacionado">Sí Colchón</asp:LinkButton>
&nbsp;<asp:LinkButton ID="lbVerNoTienenColchones" runat="server" 
                    onclick="lbVerNoTienenColchones_Click" 
                    ToolTip="Filtra el listado por las fundas que NO tienen colchón relacionado">No Colchón</asp:LinkButton>
&nbsp;<asp:LinkButton ID="lbVerTienenBasesNoColchones" runat="server" 
                    onclick="lbVerTienenBasesNoColchones_Click" 
                    
                    ToolTip="Filtra el listado por las fundas que SI tienen base relacionada y NO tienen colchón relacionado">Sí Base y No Colchón</asp:LinkButton>
&nbsp;<asp:LinkButton ID="lbVerNoTienenBasesSiColchones" runat="server" 
                    onclick="lbVerNoTienenBasesSiColchones_Click" 
                    
                    ToolTip="Filtra el listado por las fundas que NO tienen base relacionada y SI tienen colchón relacionado">No Base y Sí Colchón</asp:LinkButton>
&nbsp;<asp:LinkButton ID="lbVerSiTienenBasesSiColchones" runat="server" 
                    onclick="lbVerSiTienenBasesSiColchones_Click" 
                    
                    ToolTip="Filtra el listado por las fundas que SI tienen base relacionada y SI tienen colchón relacionado">Sí Base y Sí Colchón</asp:LinkButton>
&nbsp;<asp:LinkButton ID="lbVerNoTienenBasesNoColchones" runat="server" 
                    onclick="lbVerNoTienenBasesNoColchones_Click" 
                    
                    ToolTip="Filtra el listado por las fundas que NO tienen base relacionada y NO tienen colchón relacionado">No Base y No Colchón</asp:LinkButton>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td style="text-align: center">
                Código:
                <asp:TextBox ID="txtCodigo" runat="server" AutoPostBack="True" 
                    ontextchanged="txtCodigo_TextChanged" Width="100px"></asp:TextBox>
&nbsp;<asp:Button ID="btnBuscarCodigo" runat="server" onclick="btnBuscarCodigo_Click" 
                    Text="Buscar por Código" />
&nbsp; Nombre:
                <asp:TextBox ID="txtNombre" runat="server" AutoPostBack="True" 
                    ontextchanged="txtNombre_TextChanged" Width="235px"></asp:TextBox>
&nbsp;<asp:Button ID="btnBuscarNombre" runat="server" onclick="btnBuscarNombre_Click" 
                    Text="Buscar por Nombre" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td style="text-align: center">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="Label1" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:GridView ID="gridRelacion" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                    onselectedindexchanged="gridRelacion_SelectedIndexChanged" 
                    AllowPaging="True" onpageindexchanging="gridRelacion_PageIndexChanging" 
                    onrowdatabound="gridRelacion_RowDataBound" PageSize="20">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbModificar" runat="server" CausesValidation="False" 
                                    CommandName="Select" onclick="lbModificar_Click" Text="Modificar" 
                                    ToolTip="Le permite modificar la relación de la funda versus base y/o colchón"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbVerRelacion" runat="server" CausesValidation="False" 
                                    CommandName="Select" onclick="lbVerRelacion_Click" Text="Ver Relación" 
                                    ToolTip="Le permite ver en esta misma página la relación de la funda versus bases y/o colchones"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Funda" HeaderStyle-Width="80" ItemStyle-Width="80">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Articulo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server"  Text='<%# Bind("Articulo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nombre de la Funda" HeaderStyle-Width="360" ItemStyle-Width="360">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                            <asp:Label ID="Label3" runat="server"  Text='<%# Bind("Descripcion") %>'></asp:Label>
					<table width="100%" border="0">
						<tr>
							<td width="70%">
								<asp:DataGrid id="gridRelacionDet" runat="server" AutoGenerateColumns="False" BorderColor="#CC9966"
									BorderStyle="None" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small">
									<FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									<SelectedItemStyle Font-Bold="True" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
									<ItemStyle ForeColor="#330099" BackColor="White"></ItemStyle>
									<HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									<Columns>
										<asp:BoundColumn DataField="Articulo" HeaderText="Artículo" HeaderStyle-Width="80" ItemStyle-Width="80"></asp:BoundColumn>
										<asp:BoundColumn DataField="Descripcion" HeaderText="Nombre del Artículo" HeaderStyle-Width="280" ItemStyle-Width="280"></asp:BoundColumn>
									</Columns>
									<PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								</asp:DataGrid>
							</td>
						</tr>
					</table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="TieneBases" HeaderText="Tiene Bases?" />
                        <asp:BoundField DataField="TieneColchones" HeaderText="Tiene Colchones?" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td style="text-align: center">
                <asp:Button ID="btnActualizar" runat="server" onclick="btnActualizar_Click" 
                    Text="Actualizar listado de fundas" />
&nbsp;<asp:Button ID="btnSalir" runat="server" onclick="btnSalir_Click" 
                    Text="Salir" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</div>
</asp:Content>
