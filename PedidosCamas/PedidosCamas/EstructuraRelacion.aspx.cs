﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PedidosCamas
{
    public partial class EstructuraRelacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["urlPrecios"] = Convert.ToString(Session["UrlPrecios"]);
                ViewState["url"] = string.Format("http://sql.fiesta.local/services/wsPedidosFundas.asmx", Request.Url.Host);
                cargarEstructuraFundas();
            }
        }
         
        void cargarEstructuraFundas()
        {
            var ws = new wsPedidosFundas.wsPedidosFundas();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarEstructuraRelacion();
            
            int mTieneBases = q.Where(x => x.TieneBases.Equals("Sí")).Count();
            int mTieneColchones = q.Where(x => x.TieneColchones.Equals("Sí")).Count();

            gridRelacion.DataSource = q;
            gridRelacion.DataBind();

            txtCodigo.Text = "";
            txtNombre.Text = "";
              
            ViewState["q"] = q;
            Label1.Text = string.Format("Se encontraron {0} Fundas, habiendo {1} con base relacionada y {2} con colchón relacionado.", q.Length, mTieneBases, mTieneColchones);
        }

        protected void lbModificar_Click(object sender, EventArgs e)
        {
            ViewState["Boton"] = "Modificar";
        }

        protected void lbVerRelacion_Click(object sender, EventArgs e)
        {
            ViewState["Boton"] = "Relacion";
        }

        protected void gridRelacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = gridRelacion.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Modificar":
                    Label lbArticulo = (Label)row.FindControl("Label2");
                    Session["ArticuloRelacion"] = lbArticulo.Text;
                    Response.Redirect("RelacionarCodigos.aspx");
                    break;
                case "Relacion":
                    string mNombreArticulo = ""; string mDescRelacion = "";
                    Label lbArticuloRelacion = (Label)row.FindControl("Label2");

                    var ws = new wsPedidosFundas.wsPedidosFundas();
                    if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                    var q = ws.ListarRelacionFundas(lbArticuloRelacion.Text, ref mNombreArticulo, ref mDescRelacion);

                    Control ctl = gridRelacion.SelectedRow.FindControl("gridRelacionDet");

                    if (ctl is DataGrid)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;
                        fileGrid.DataSource = q.ToArray();
                        fileGrid.DataBind();
                    }

                    break;
            }
        }

        protected void gridRelacion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }

            foreach (TableCell cel in e.Row.Cells)
            {
                cel.Attributes.Add("style", "text-align: left");
            }
        }

        protected void gridRelacion_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            var q = ViewState["q"];
            
            gridRelacion.DataSource = q;
            gridRelacion.PageIndex = e.NewPageIndex;
            gridRelacion.DataBind();
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void lbVerTienenBase_Click(object sender, EventArgs e)
        {
            var ws = new wsPedidosFundas.wsPedidosFundas();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarEstructuraRelacion();

            q = q.Where(x => x.TieneBases.Equals("Sí")).ToArray();

            int mTieneBases = q.Where(x => x.TieneBases.Equals("Sí")).Count();
            int mTieneColchones = q.Where(x => x.TieneColchones.Equals("Sí")).Count();

            gridRelacion.DataSource = q;
            gridRelacion.DataBind();

            ViewState["q"] = q;
            Label1.Text = string.Format("Se encontraron {0} Fundas, habiendo {1} con base relacionada y {2} con colchón relacionado.", q.Length, mTieneBases, mTieneColchones);
        }

        protected void lbVerNoTienenBase_Click(object sender, EventArgs e)
        {
            var ws = new wsPedidosFundas.wsPedidosFundas();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarEstructuraRelacion();

            q = q.Where(x => x.TieneBases.Equals("No")).ToArray();

            int mTieneBases = q.Where(x => x.TieneBases.Equals("Sí")).Count();
            int mTieneColchones = q.Where(x => x.TieneColchones.Equals("Sí")).Count();

            gridRelacion.DataSource = q;
            gridRelacion.DataBind();

            ViewState["q"] = q;
            Label1.Text = string.Format("Se encontraron {0} Fundas, habiendo {1} con base relacionada y {2} con colchón relacionado.", q.Length, mTieneBases, mTieneColchones);
        }

        protected void lbVerTienenColchones_Click(object sender, EventArgs e)
        {
            var ws = new wsPedidosFundas.wsPedidosFundas();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarEstructuraRelacion();

            q = q.Where(x => x.TieneColchones.Equals("Sí")).ToArray();

            int mTieneBases = q.Where(x => x.TieneBases.Equals("Sí")).Count();
            int mTieneColchones = q.Where(x => x.TieneColchones.Equals("Sí")).Count();

            gridRelacion.DataSource = q;
            gridRelacion.DataBind();

            ViewState["q"] = q;
            Label1.Text = string.Format("Se encontraron {0} Fundas, habiendo {1} con base relacionada y {2} con colchón relacionado.", q.Length, mTieneBases, mTieneColchones);
        }

        protected void lbVerNoTienenColchones_Click(object sender, EventArgs e)
        {
            var ws = new wsPedidosFundas.wsPedidosFundas();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarEstructuraRelacion();

            q = q.Where(x => x.TieneColchones.Equals("No")).ToArray();

            int mTieneBases = q.Where(x => x.TieneBases.Equals("Sí")).Count();
            int mTieneColchones = q.Where(x => x.TieneColchones.Equals("Sí")).Count();

            gridRelacion.DataSource = q;
            gridRelacion.DataBind();

            ViewState["q"] = q;
            Label1.Text = string.Format("Se encontraron {0} Fundas, habiendo {1} con base relacionada y {2} con colchón relacionado.", q.Length, mTieneBases, mTieneColchones);
        }

        protected void lbVerTienenBasesNoColchones_Click(object sender, EventArgs e)
        {
            var ws = new wsPedidosFundas.wsPedidosFundas();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarEstructuraRelacion();

            q = q.Where(x => x.TieneBases.Equals("Sí") && x.TieneColchones.Equals("No")).ToArray();

            int mTieneBases = q.Where(x => x.TieneBases.Equals("Sí")).Count();
            int mTieneColchones = q.Where(x => x.TieneColchones.Equals("Sí")).Count();

            gridRelacion.DataSource = q;
            gridRelacion.DataBind();

            ViewState["q"] = q;
            Label1.Text = string.Format("Se encontraron {0} Fundas, habiendo {1} con base relacionada y {2} con colchón relacionado.", q.Length, mTieneBases, mTieneColchones);
        }

        protected void lbVerNoTienenBasesSiColchones_Click(object sender, EventArgs e)
        {
            var ws = new wsPedidosFundas.wsPedidosFundas();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarEstructuraRelacion();

            q = q.Where(x => x.TieneBases.Equals("No") && x.TieneColchones.Equals("Sí")).ToArray();

            int mTieneBases = q.Where(x => x.TieneBases.Equals("Sí")).Count();
            int mTieneColchones = q.Where(x => x.TieneColchones.Equals("Sí")).Count();

            gridRelacion.DataSource = q;
            gridRelacion.DataBind();

            ViewState["q"] = q;
            Label1.Text = string.Format("Se encontraron {0} Fundas, habiendo {1} con base relacionada y {2} con colchón relacionado.", q.Length, mTieneBases, mTieneColchones);
        }

        protected void lbVerSiTienenBasesSiColchones_Click(object sender, EventArgs e)
        {
            var ws = new wsPedidosFundas.wsPedidosFundas();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarEstructuraRelacion();

            q = q.Where(x => x.TieneBases.Equals("Sí") && x.TieneColchones.Equals("Sí")).ToArray();

            int mTieneBases = q.Where(x => x.TieneBases.Equals("Sí")).Count();
            int mTieneColchones = q.Where(x => x.TieneColchones.Equals("Sí")).Count();

            gridRelacion.DataSource = q;
            gridRelacion.DataBind();

            ViewState["q"] = q;
            Label1.Text = string.Format("Se encontraron {0} Fundas, habiendo {1} con base relacionada y {2} con colchón relacionado.", q.Length, mTieneBases, mTieneColchones);
        }

        protected void lbVerNoTienenBasesNoColchones_Click(object sender, EventArgs e)
        {
            var ws = new wsPedidosFundas.wsPedidosFundas();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarEstructuraRelacion();

            q = q.Where(x => x.TieneBases.Equals("No") && x.TieneColchones.Equals("No")).ToArray();

            int mTieneBases = q.Where(x => x.TieneBases.Equals("Sí")).Count();
            int mTieneColchones = q.Where(x => x.TieneColchones.Equals("Sí")).Count();

            gridRelacion.DataSource = q;
            gridRelacion.DataBind();

            ViewState["q"] = q;
            Label1.Text = string.Format("Se encontraron {0} Fundas, habiendo {1} con base relacionada y {2} con colchón relacionado.", q.Length, mTieneBases, mTieneColchones);
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            cargarEstructuraFundas();
        }

        protected void btnBuscarCodigo_Click(object sender, EventArgs e)
        {
            buscarCodigo();
        }

        void buscarCodigo()
        {
            if (txtCodigo.Text.Trim().Length == 0) return;

            var ws = new wsPedidosFundas.wsPedidosFundas();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarEstructuraRelacion();

            q = q.Where(x => x.Articulo.Contains(txtCodigo.Text.Trim())).ToArray();

            int mTieneBases = q.Where(x => x.TieneBases.Equals("Sí")).Count();
            int mTieneColchones = q.Where(x => x.TieneColchones.Equals("Sí")).Count();

            gridRelacion.DataSource = q;
            gridRelacion.DataBind();

            ViewState["q"] = q;
            Label1.Text = string.Format("Se encontraron {0} Fundas, habiendo {1} con base relacionada y {2} con colchón relacionado.", q.Length, mTieneBases, mTieneColchones);
        }

        protected void btnBuscarNombre_Click(object sender, EventArgs e)
        {
            buscarNombre();
        }

        void buscarNombre()
        {
            if (txtNombre.Text.Trim().Length == 0) return;

            var ws = new wsPedidosFundas.wsPedidosFundas();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarEstructuraRelacion();

            q = q.Where(x => x.Descripcion.ToUpper().Contains(txtNombre.Text.Trim().ToUpper())).ToArray();

            int mTieneBases = q.Where(x => x.TieneBases.Equals("Sí")).Count();
            int mTieneColchones = q.Where(x => x.TieneColchones.Equals("Sí")).Count();

            gridRelacion.DataSource = q;
            gridRelacion.DataBind();

            ViewState["q"] = q;
            Label1.Text = string.Format("Se encontraron {0} Fundas, habiendo {1} con base relacionada y {2} con colchón relacionado.", q.Length, mTieneBases, mTieneColchones);
        }

        protected void txtCodigo_TextChanged(object sender, EventArgs e)
        {
            buscarCodigo();
        }

        protected void txtNombre_TextChanged(object sender, EventArgs e)
        {
            buscarNombre();
        }

    }
}