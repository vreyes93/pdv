﻿<%@ Page Title="Pedido" Language="C#" MasterPageFile="~/SimpleMaster.Master" AutoEventWireup="true" CodeBehind="Pedido.aspx.cs" Inherits="PedidosCamas.Pedido" %>
<asp:Content ID="Content1" ContentPlaceHolderID="menu" runat="server">
    <style type="text/css">
        .style2
        {
            height: 33px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="divContenido">
    <table align="center">
        <tr>
            <td style="text-align: center" colspan="6">
                <asp:Label ID="lbPedido" runat="server" Font-Bold="True" Font-Underline="True"></asp:Label>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="6">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center" class="style2" colspan="6">
                <asp:Button ID="btnSalir0" runat="server" onclick="btnSalir_Click" 
                    Text="Salir" />
            </td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:LinkButton ID="lbBodega" runat="server" Font-Underline="False" 
                    ForeColor="#846969" >Bodega:</asp:LinkButton>
            </td>
            <td style="text-align: left">
                <asp:TextBox ID="bodega" runat="server" ReadOnly="True" Width="260px"></asp:TextBox>
            </td>
            <td style="text-align: left">
                <asp:Label ID="lbDescripcion" runat="server" Text="Descripción del Pedido:"></asp:Label>
            </td>
            <td style="text-align: left" colspan="3">
                <asp:TextBox ID="descripcion" runat="server" Width="356px" ReadOnly="True"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: left" colspan="4">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                <asp:Label ID="Label8" runat="server"></asp:Label>
            </td>
            <td style="text-align: right" colspan="2">
                <asp:Label ID="lbHora" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:GridView ID="gridPedido" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" BorderStyle="Solid" 
                    BorderColor="#284775" BorderWidth="1px" 
                    PageSize="20" >
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField Visible="False">
                            <ItemTemplate>
                                <asp:CheckBox ID="cbFunda" runat="server" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" />
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False" Visible="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbModificar" runat="server" CausesValidation="False" 
                                    CommandName="Select" Text="Modificar" 
                                    ToolTip="Modifica los valores sugeridos para pedir Colchones y/o Fundas"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False" Visible="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbEliminar" runat="server" CausesValidation="False" 
                                    CommandName="Select"  
                                    Text="Eliminar" ToolTip="Le permite eliminar el registro seleccionado."></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False" Visible="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbDetalle" runat="server" CausesValidation="False" 
                                    CommandName="Select" Text="Detalle" 
                                    ToolTip="Le permite ver el detalle de la distribución de las existencias entre las bodegas" ></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Funda">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Articulo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Articulo") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" BackColor="#A9C3E1" ForeColor="White" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nombre de la Funda">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" Width="500px" />
                            <ItemStyle HorizontalAlign="Left" Width="500px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Colchones">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Colchon") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("Colchon") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="40px" />
                            <ItemStyle HorizontalAlign="Center" Width="40px" BackColor="White" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Bases">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Base") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("Base") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="35px" />
                            <ItemStyle HorizontalAlign="Center" Width="35px" BackColor="#a9c3e1" ForeColor="White" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fundas">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Funda") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Bind("Funda") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="35px" />
                            <ItemStyle HorizontalAlign="Center" Width="35px" BackColor="#a9c3e1" ForeColor="White" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pedido Colchones">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("PedidoColchon") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Bind("PedidoColchon") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="60px" />
                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pedido Fundas">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("PedidoFunda") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Text='<%# Bind("PedidoFunda") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="45px" />
                            <ItemStyle HorizontalAlign="Center" Width="45px" BackColor="#a9c3e1" ForeColor="White" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Código Artículo" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text='<%# Bind("CodigoArticulo") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("CodigoArticulo") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="CodigoFunda" HeaderText="CodigoFunda" 
                            Visible="False" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                <asp:Label ID="Label9" runat="server"></asp:Label>
            </td>
            <td style="text-align: right">
                <asp:Label ID="lbHora0" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: center" colspan="6">
                <asp:Button ID="btnSalir" runat="server" onclick="btnSalir_Click" 
                    Text="Salir" />
            </td>
        </tr>
    </table>
</div>
</asp:Content>
