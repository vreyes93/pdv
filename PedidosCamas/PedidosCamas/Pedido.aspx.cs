﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PedidosCamas
{
    public partial class Pedido : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["urlPrecios"] = Convert.ToString(Session["UrlPrecios"]);
                ViewState["url"] = string.Format("http://sql.fiesta.local/services/wsPedidosFundas.asmx", Request.Url.Host);
                cargarPedido();
            }
        }

        void cargarPedido()
        {
            int mPedido = Convert.ToInt32(Session["PedidoNumero"]);

            lbPedido.Text = string.Format("PEDIDO No. {0}", Session["PedidoNumero"].ToString());
            bodega.Text = Session["PedidoBodega"].ToString();
            descripcion.Text = Session["PedidoDescripcion"].ToString();
            lbHora.Text = string.Format("Fecha y Hora de Creación:  {0}", Session["PedidoFechaHora"].ToString());
            lbHora0.Text = string.Format("Fecha y Hora de Creación:  {0}", Session["PedidoFechaHora"].ToString());

            var ws = new wsPedidosFundas.wsPedidosFundas();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarPedido(mPedido);

            gridPedido.DataSource = q;
            gridPedido.DataBind();

            Label8.Text = string.Format("El pedido consta de {0} artículos.", gridPedido.Rows.Count);
            Label9.Text = string.Format("El pedido consta de {0} artículos.", gridPedido.Rows.Count);
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
    }
}