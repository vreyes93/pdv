﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PedidosCamas
{
    public partial class Pedidos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["urlPrecios"] = Convert.ToString(Session["UrlPrecios"]);
                ViewState["url"] = string.Format("http://sql.fiesta.local/services/wsPedidosFundas.asmx", Request.Url.Host);
                CargarPedidos();
            }
        }

        void CargarPedidos()
        {
            var ws = new wsPedidosFundas.wsPedidosFundas();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarPedidos();

            Label2.Text = "";
            Label1.Text = string.Format("Existen {0} Pedidos en el sistema.", q.Length);

            gridPedidos.DataSource = q;
            gridPedidos.DataBind();
              
            gridPedidos.SelectedIndex = -1;
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void lbSeleccionar_Click(object sender, EventArgs e)
        {
            ViewState["Boton"] = "Seleccionar";
        }

        protected void lbEliminar_Click(object sender, EventArgs e)
        {
            ViewState["Boton"] = "Eliminar";
        }

        protected void gridPedidos_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridPedidos.SelectedRow;
            Label lbPedido = (Label)rowSeleccionada.FindControl("Label1");

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Seleccionar":
                    Session["PedidoNumero"] = lbPedido.Text;
                    Session["PedidoBodega"] = rowSeleccionada.Cells[4].Text;
                    Session["PedidoDescripcion"] = rowSeleccionada.Cells[6].Text;
                    Session["PedidoFechaHora"] = rowSeleccionada.Cells[7].Text;

                    Response.Redirect("Pedido.aspx");

                    break;
                case "Eliminar":

                    var ws = new wsPedidosFundas.wsPedidosFundas(); string mMensaje = "";
                    if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                    if (!ws.EliminarPedido(Convert.ToInt32(lbPedido.Text), ref mMensaje))
                    {
                        Label1.Text = "";
                        Label2.Text = mMensaje;

                        return;
                    }

                    CargarPedidos();
                    break;
            }
        }

        protected void btnActualizarPedidos_Click(object sender, EventArgs e)
        {
            CargarPedidos();
        }

    }
}