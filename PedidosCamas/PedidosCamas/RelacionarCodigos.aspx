﻿<%@ Page Title="Relacionar Códigos" Language="C#" MasterPageFile="~/SimpleMaster.Master" AutoEventWireup="true" CodeBehind="RelacionarCodigos.aspx.cs" Inherits="PedidosCamas.RelacionarCodigos" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="menu" runat="server">
    <style type="text/css">
        .style1
        {
            text-align: center;
        }
        .style2
        {
            border-style: solid;
            border-width: 2px;
        }
        .style3
        {
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="divContenido">
    <table align="center">
        <tr>
            <td class="style3" colspan="3" style="text-align: center">
                <strong>RELACION DE CODIGOS DE FUNDAS CON BASES Y COLCHONES</strong></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2">
                <asp:Label ID="Label2" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Código
                Funda:</td>
            <td>
                <asp:TextBox ID="articulo" runat="server" Width="118px" AutoPostBack="True" 
                    ontextchanged="articulo_TextChanged"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="descripcion" runat="server" Width="340px" 
                    BackColor="AliceBlue" Font-Bold="True" ReadOnly="True" TabIndex="5" 
                    ToolTip="Nothing"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:LinkButton ID="lbBuscarArticulo" runat="server" 
                    onclick="lbBuscarArticulo_Click" TabIndex="10">Buscar Funda</asp:LinkButton>
            </td>
        </tr>
        </table>
    <table id="tablaBusqueda" runat="server" align="center" class="style2">
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2">
                <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                <asp:Label ID="Label5" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                Funda:</td>
            <td>
                <asp:TextBox ID="codigoArticulo" runat="server" TabIndex="20"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                Nombre:</td>
            <td>
                <asp:TextBox ID="descripcionArticulo" runat="server" TabIndex="30" 
                    MaxLength="100"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                <asp:Button ID="btnBuscar" runat="server" Text="Buscar" 
                    ToolTip="Busca artículos según el criterio de búsqueda ingresado" 
                    onclick="btnBuscar_Click" TabIndex="40" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td class="style1" colspan="2">
                <asp:GridView ID="gridArticulos" runat="server" CellPadding="4" 
                    ForeColor="#333333" GridLines="None" 
                    onselectedindexchanged="gridArticulos_SelectedIndexChanged" TabIndex="50" 
                    onrowdatabound="gridArticulos_RowDataBound" AutoGenerateColumns="False" 
                    style="text-align: left">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                                    CommandName="Select" 
                                    Text="Seleccionar"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Articulo" HeaderText="Funda" />
                        <asp:BoundField DataField="Descripción" HeaderText="Nombre de la Funda" />
                        <asp:BoundField DataField="PrecioLista" HeaderText="PrecioLista" 
                            Visible="False" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                <asp:LinkButton ID="lbCerrarBusqueda" runat="server" 
                    ToolTip="Cierra la ventana de búsqueda" onclick="lbCerrarBusqueda_Click" 
                    TabIndex="60">Cerrar Búsqueda</asp:LinkButton>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <table align="center">
        <tr>
            <td>
                Descripción de la Relación:</td>
            <td colspan="3">
                <asp:TextBox ID="descripcionRelacion" runat="server" Width="350px" TabIndex="90" 
                    MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Bases y/o Colchones Relacionados:</td>
            <td>
                &nbsp;<asp:LinkButton ID="lbAgregarArticulo" runat="server" 
                    onclick="lbAgregarArticulo_Click">Agregar Base y/o Colchón</asp:LinkButton>
            </td>
            <td style="text-align: left">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center">
                &nbsp;</td>
        </tr>
        </table>
    <table align="center">
        <tr>
            <td>
                <asp:GridView ID="gridRelacion" runat="server" CellPadding="4" ForeColor="#333333" 
                    GridLines="None" AutoGenerateColumns="False" 
                    onrowdatabound="gridRelacion_RowDataBound" 
                    style="text-align: left" onrowdeleting="gridRelacion_RowDeleting">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                                    CommandName="Delete" Text="Eliminar" 
                                    onclientclick="javascript:return confirm(&quot;Desea eliminar el registro?&quot;)"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Articulo" HeaderText="Artículo" />
                        <asp:BoundField DataField="Descripcion" HeaderText="Nombre del Artículo" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
    </table>
    <table id="tablaBusquedaDet" runat="server" align="center" class="style2">
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2">
                <asp:Label ID="Label6" runat="server" ForeColor="Red"></asp:Label>
                <asp:Label ID="Label7" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                Artículo:</td>
            <td>
                <asp:TextBox ID="codigoArticuloDet" runat="server" TabIndex="20"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                Nombre:</td>
            <td>
                <asp:TextBox ID="descripcionArticuloDet" runat="server" TabIndex="30" 
                    MaxLength="100"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                <asp:Button ID="btnBuscarDet" runat="server" Text="Buscar" 
                    ToolTip="Busca artículos según el criterio de búsqueda ingresado" 
                    onclick="btnBuscarDet_Click" TabIndex="40" />
                <asp:CheckBox ID="cbCerrarBusqueda" runat="server" Text="Cerrar al seleccionar" 
                    ToolTip="Al activar esta casilla la búsqueda se cerrará al seleccionar un artículo.  Si está desmarcada podrá seleccionar varios artículos y luego hacer clic abajo en &quot;Cerrar Búsqueda&quot;." />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td class="style1" colspan="2">
                <asp:GridView ID="gridArticulosDet" runat="server" CellPadding="4" 
                    ForeColor="#333333" GridLines="None" TabIndex="50" 
                    AutoGenerateColumns="False" onrowdatabound="gridArticulosDet_RowDataBound" 
                    onselectedindexchanged="gridArticulosDet_SelectedIndexChanged" 
                    style="text-align: left">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1Det" runat="server" CausesValidation="False" 
                                    CommandName="Select" 
                                    Text="Seleccionar"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Articulo" HeaderText="Artículo" />
                        <asp:BoundField DataField="Descripción" HeaderText="Nombre del Artículo" />
                        <asp:BoundField DataField="PrecioLista" HeaderText="PrecioLista" 
                            Visible="False" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                <asp:LinkButton ID="lbCerrarBusquedaDet" runat="server" 
                    ToolTip="Cierra la ventana de búsqueda" onclick="lbCerrarBusquedaDet_Click" 
                    TabIndex="60">Cerrar Búsqueda</asp:LinkButton>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: center">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        <table align="center">
            <tr>
                <td>
                <asp:Button ID="btnGrabar" runat="server" onclick="btnGrabar_Click" 
                    TabIndex="160" Text="Grabar Relación de Códigos" 
                    UseSubmitBehavior="False" />
                    <asp:Button ID="btnNueva" runat="server" onclick="btnNueva_Click" 
                        Text="Nueva Relacion" />
                <asp:Button ID="btnSalir" runat="server" onclick="btnSalir_Click" 
                    Text="Salir" />
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                <asp:Label ID="Label3" runat="server" ForeColor="Red"></asp:Label>
                <asp:Label ID="Label4" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
</div>
</asp:Content>
