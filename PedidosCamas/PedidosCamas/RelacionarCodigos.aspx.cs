﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PedidosCamas
{
    public partial class RelacionarCodigos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable("codigos");

                dt.Columns.Add(new DataColumn("Articulo", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Descripcion", typeof(System.String)));

                DataColumn[] llave = new DataColumn[1];
                llave[0] = dt.Columns["Articulo"];
                dt.PrimaryKey = llave;

                ds.Tables.Add(dt);
                ViewState["ds"] = ds;

                Label1.Text = "";
                Label2.Text = "";
                Label5.Text = "";

                cbCerrarBusqueda.Checked = true;
                descripcionRelacion.Text = "";

                articulo.Focus();
                tablaBusqueda.Visible = false;
                tablaBusquedaDet.Visible = false;

                ViewState["urlPrecios"] = Convert.ToString(Session["UrlPrecios"]);
                ViewState["url"] = string.Format("http://sql.fiesta.local/services/wsPedidosFundas.asmx", Request.Url.Host);

                if (Session["ArticuloRelacion"] != null) cargarRelacion();
            }
        }

        protected void lbBuscarArticulo_Click(object sender, EventArgs e)
        {
            Label1.Text = "";
            Label5.Text = "";
            codigoArticulo.Text = "";
            descripcionArticulo.Text = "";
            tablaBusqueda.Visible = true;
            descripcionArticulo.Focus();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            string mTipo = "";

            Label1.Text = "";
            Label5.Text = "";

            if (codigoArticulo.Text.Trim().Length == 0 && descripcionArticulo.Text.Trim().Length == 0)
            {
                Label1.Text = "Debe ingresar un criterio de búsqueda ya sea el código, la descripción o ambos.";
                codigoArticulo.Focus();
                return;
            }

            if (codigoArticulo.Text.Trim().Length > 0) mTipo = "C";
            if (descripcionArticulo.Text.Trim().Length > 0) mTipo = "D";
            if (codigoArticulo.Text.Trim().Length > 0 && descripcionArticulo.Text.Trim().Length > 0) mTipo = "A";

            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["urlPrecios"]);

            var q = ws.ListarArticulosCriterio(mTipo, codigoArticulo.Text, descripcionArticulo.Text);
            var articulos = q.ToArray();

            gridArticulos.DataSource = articulos;
            gridArticulos.DataBind();

            descripcionArticulo.Focus();
            if (q.Length == 0)
            {
                Label1.Text = "No se encontraron artículos con el criterio de búsqueda ingresado.";
            }
            else
            {
                Label5.Text = string.Format("Se encontraron {0} artículos con el criterio de búsqueda ingresado.", q.Length);
            }
        }

        protected void lbCerrarBusqueda_Click(object sender, EventArgs e)
        {
            tablaBusqueda.Visible = false;
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void gridArticulos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }

            foreach (TableCell cel in e.Row.Cells)
            {
                cel.Attributes.Add("style", "text-align: left");
            }
        }

        protected void gridArticulos_SelectedIndexChanged(object sender, EventArgs e)
        {
            articulo.Text = gridArticulos.SelectedRow.Cells[1].Text;
            descripcion.Text = gridArticulos.SelectedRow.Cells[2].Text;

            if (descripcion.Text == "&nbsp;") descripcion.Text = "";

            descripcion.Text = descripcion.Text.Replace("&#39;", "'");
            descripcion.Text = descripcion.Text.Replace("&#225;", "á");
            descripcion.Text = descripcion.Text.Replace("&#233;", "é");
            descripcion.Text = descripcion.Text.Replace("&#237;", "í");
            descripcion.Text = descripcion.Text.Replace("&#243;", "ó");
            descripcion.Text = descripcion.Text.Replace("&#250;", "ú");

            Label1.Text = "";
            Label2.Text = "";
            Label5.Text = "";

            Session["ArticuloRelacion"] = articulo.Text;

            descripcionRelacion.Focus();
            tablaBusqueda.Visible = false;

            cargarRelacion();
        }

        protected void articulo_TextChanged(object sender, EventArgs e)
        {
            if (articulo.Text.Trim().Length == 0)
            {
                Label1.Text = "";
                Label5.Text = "";
                descripcion.Text = "";
                return;
            }

            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["urlPrecios"]);

            var q = ws.ListarArticulosCriterio("C", articulo.Text, "");
            var articulos = q.ToArray();

            gridArticulos.DataSource = articulos;
            gridArticulos.DataBind();

            if (gridArticulos.Rows.Count == 0)
            {
                articulo.Text = "";
                descripcion.Text = "";
                Label1.Text = "El código de artículo ingresado no existe.";
                articulo.Focus();
                return;
            }

            Session["ArticuloRelacion"] = articulo.Text;
            descripcion.Text = gridArticulos.Rows[0].Cells[2].Text;

            Label1.Text = "";
            Label2.Text = "";
            Label5.Text = "";

            descripcionRelacion.Focus();
            tablaBusqueda.Visible = false;

            cargarRelacion();
            Session["ArticuloRelacion"] = null;
        }

        protected void gridRelacion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }

            foreach (TableCell cel in e.Row.Cells)
            {
                cel.Attributes.Add("style", "text-align: left");
            }
        }

        protected void lbCerrarBusquedaDet_Click(object sender, EventArgs e)
        {
            tablaBusquedaDet.Visible = false;
        }
          
        protected void btnBuscarDet_Click(object sender, EventArgs e)
        {
            string mTipo = "";

            Label6.Text = "";
            Label7.Text = "";

            if (codigoArticuloDet.Text.Trim().Length == 0 && descripcionArticuloDet.Text.Trim().Length == 0)
            {
                Label6.Text = "Debe ingresar un criterio de búsqueda ya sea el código, la descripción o ambos.";
                codigoArticuloDet.Focus();
                return;
            }
               
            if (codigoArticuloDet.Text.Trim().Length > 0) mTipo = "C";
            if (descripcionArticuloDet.Text.Trim().Length > 0) mTipo = "D";
            if (codigoArticuloDet.Text.Trim().Length > 0 && descripcionArticuloDet.Text.Trim().Length > 0) mTipo = "A";

            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["urlPrecios"]);

            var q = ws.ListarArticulosCriterio(mTipo, codigoArticuloDet.Text, descripcionArticuloDet.Text);
            var articulos = q.ToArray();

            gridArticulosDet.DataSource = articulos;
            gridArticulosDet.DataBind();

            descripcionArticuloDet.Focus();
            if (q.Length == 0)
            {
                Label6.Text = "No se encontraron artículos con el criterio de búsqueda ingresado.";
            }
            else
            {
                Label7.Text = string.Format("Se encontraron {0} artículos con el criterio de búsqueda ingresado.", q.Length);
            }
        }

        protected void lbAgregarArticulo_Click(object sender, EventArgs e)
        {
            Label6.Text = "";
            Label7.Text = "";
            codigoArticuloDet.Text = "";
            descripcionArticuloDet.Text = "";
            tablaBusquedaDet.Visible = true;
            descripcionArticuloDet.Focus();
        }

        protected void gridArticulosDet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }

            foreach (TableCell cel in e.Row.Cells)
            {
                cel.Attributes.Add("style", "text-align: left");
            }
        }

        protected void gridArticulosDet_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            ds = (DataSet)ViewState["ds"];

            if (Convert.ToInt32(ds.Tables["codigos"].Compute("COUNT(Articulo)", "Articulo = '" + gridArticulosDet.SelectedRow.Cells[1].Text + "'")) > 0)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('La Base ó Colchón seleccionado ya se encuentra relacionado a la funda.');", true);
                return;
            }

            DataRow row = ds.Tables["codigos"].NewRow();

            row["Articulo"] = gridArticulosDet.SelectedRow.Cells[1].Text;
            row["Descripcion"] = gridArticulosDet.SelectedRow.Cells[2].Text;

            ds.Tables["codigos"].Rows.Add(row);

            gridRelacion.DataSource = ds;
            gridRelacion.DataMember = "codigos";
            gridRelacion.DataBind();

            ViewState["ds"] = ds;

            Label6.Text = "";
            Label7.Text = "";

            descripcionRelacion.Focus();
            if (cbCerrarBusqueda.Checked) tablaBusquedaDet.Visible = false;
        }

        protected void btnGrabar_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            ds = (DataSet)ViewState["ds"];

            if (articulo.Text.Trim().Length == 0)
            {
                Label2.Text = "";
                Label4.Text = "";
                articulo.Focus();
                Label3.Text = "Debe ingresar el código de la funda.";
                return;
            }
            if (ds.Tables["codigos"].Rows.Count == 0)
            {
                Label2.Text = "";
                Label4.Text = "";
                lbAgregarArticulo.Focus();
                Label3.Text = "Debe ingresar al menos un código de base ó colchón.";
                return;
            }

            string[] codigos = new string[1];

            for (int ii = 0; ii < ds.Tables["codigos"].Rows.Count; ii++)
            {
                if (ii > 0) Array.Resize(ref codigos, ii + 1);
                codigos[ii] = ds.Tables["codigos"].Rows[ii]["Articulo"].ToString();
            }

            var ws = new wsPedidosFundas.wsPedidosFundas(); string mMensaje = "";
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            if (!ws.GrabarRelacionFundas(articulo.Text, codigos, descripcionRelacion.Text, ref mMensaje))
            {
                Label2.Text = "";
                Label4.Text = "";
                Label3.Text = mMensaje;
                return;
            }

            Label4.Text = mMensaje;
            limpiar();
        }

        protected void gridRelacion_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string articulo = e.Values[0].ToString();

            DataSet ds = new DataSet();
            ds = (DataSet)ViewState["ds"];
              
            ds.Tables["codigos"].Rows.Find(articulo).Delete();

            gridRelacion.DataSource = ds;
            gridRelacion.DataMember = "codigos";
            gridRelacion.DataBind();

            ViewState["ds"] = ds;
        }
        
        void cargarRelacion()
        {
            string mNombreArticulo = ""; string mDescRelacion = "";
            var ws = new wsPedidosFundas.wsPedidosFundas();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarRelacionFundas(Convert.ToString(Session["ArticuloRelacion"]), ref mNombreArticulo, ref mDescRelacion);

            articulo.Text = Convert.ToString(Session["ArticuloRelacion"]);
            descripcion.Text = mNombreArticulo;
            descripcionRelacion.Text = mDescRelacion;

            Label2.Text = "";
            Label3.Text = "";
            Label4.Text = "";

            tablaBusqueda.Visible = false;
            tablaBusquedaDet.Visible = false;

            descripcionRelacion.Focus();

            if (q.Length > 0)
            {
                DataSet ds = new DataSet();
                ds = (DataSet)ViewState["ds"];

                ds.Tables["codigos"].Clear();

                for (int ii = 0; ii < q.Length; ii++)
                {
                    DataRow row = ds.Tables["codigos"].NewRow();
                    row["Articulo"] = q[ii].Articulo;
                    row["Descripcion"] = q[ii].Descripcion;
                    ds.Tables["codigos"].Rows.Add(row);
                }

                gridRelacion.DataSource = ds;
                gridRelacion.DataMember = "codigos";
                gridRelacion.DataBind();

                ViewState["ds"] = ds;
            }

            Session["ArticuloRelacion"] = null;
        }

        protected void btnNueva_Click(object sender, EventArgs e)
        {
            limpiar(); 
        }

        void limpiar()
        {
            DataSet ds = new DataSet();
            ds = (DataSet)ViewState["ds"];

            ds.Tables["codigos"].Clear();

            gridRelacion.DataSource = ds;
            gridRelacion.DataMember = "codigos";
            gridRelacion.DataBind();

            ViewState["ds"] = ds;
            Session["ArticuloRelacion"] = null;

            articulo.Text = "";
            descripcion.Text = "";
            descripcionRelacion.Text = "";

            Label2.Text = "";
            Label3.Text = "";
  
            articulo.Focus();
            tablaBusqueda.Visible = false;
            tablaBusquedaDet.Visible = false;
        }

    }
}