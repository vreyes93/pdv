﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SimpleMaster.Master" AutoEventWireup="true" CodeBehind="RenglonPedido.aspx.cs" Inherits="PedidosCamas.RenglonPedido" %>
<asp:Content ID="Content1" ContentPlaceHolderID="menu" runat="server">
    <style type="text/css">
        .style1
        {
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <table align="center">
        <tr>
            <td class="style1" colspan="2" style="text-align: center">
                <strong>MODIFICACION DE RENGLON DE PEDIDO</strong></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Código:</td>
            <td>
                <asp:TextBox ID="Codigo" runat="server" ReadOnly="True" Width="150px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Descripción:</td>
            <td>
                <asp:TextBox ID="Descripcion" runat="server" Height="50px" ReadOnly="True" 
                    TextMode="MultiLine" Width="150px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Colchones:</td>
            <td>
                <asp:TextBox ID="Colchones" runat="server" ReadOnly="True" Width="150px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Bases:</td>
            <td>
                <asp:TextBox ID="Bases" runat="server" ReadOnly="True" Width="150px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Fundas:</td>
            <td>
                <asp:TextBox ID="Fundas" runat="server" ReadOnly="True" Width="150px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Pedido Colchones:</td>
            <td>
                <asp:TextBox ID="PedidoColchones" runat="server" Width="150px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Pedido Fundas:</td>
            <td>
                <asp:TextBox ID="PedidoFundas" runat="server" Width="150px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <asp:Button ID="btnGrabar" runat="server" onclick="btnGrabar_Click" 
                    Text="Grabar" />
&nbsp;<asp:Button ID="btnSalir" runat="server" onclick="btnSalir_Click" Text="Salir" />
            </td>
        </tr>
    </table>
</asp:Content>
