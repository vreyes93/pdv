﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PedidosCamas
{
    public partial class RenglonPedido : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["urlPrecios"] = Convert.ToString(Session["UrlPrecios"]);
                ViewState["url"] = string.Format("http://sql.fiesta.local/services/wsPedidosFundas.asmx", Request.Url.Host);

                CargarDatos();
            }
        }
        
        void CargarDatos()
        {
            wsPedidosFundas.PedidoFundas item = new wsPedidosFundas.PedidoFundas();
            item = (wsPedidosFundas.PedidoFundas)Session["qSeleccionada"];

            Codigo.Text = item.Articulo;
            Descripcion.Text = item.Descripcion;
            Colchones.Text = item.Colchon.ToString();
            Bases.Text = item.Base.ToString();
            Fundas.Text = item.Funda.ToString();
            PedidoColchones.Text = item.PedidoColchon.ToString();
            PedidoFundas.Text = item.PedidoFunda.ToString();

            PedidoColchones.Focus();
        }

        protected void btnGrabar_Click(object sender, EventArgs e)
        {
            int mPedidoColchones, mPedidoFundas;

            try
            {
                mPedidoColchones = Convert.ToInt32(PedidoColchones.Text);
            }
            catch
            {
                Label1.Text = "Debe ingresar un número válido para el Pedido de Colchones.";
                PedidoColchones.Focus();
                return;
            }
            try
            {
                mPedidoFundas = Convert.ToInt32(PedidoFundas.Text);
            }
            catch
            {
                Label1.Text = "Debe ingresar un número válido para el Pedido de Fundas.";
                PedidoFundas.Focus();
                return;
            }

            if (mPedidoColchones < 0)
            {
                Label1.Text = "Debe ingresar un número válido para el Pedido de Colchones.";
                PedidoColchones.Focus();
                return;
            }
            if (mPedidoFundas < 0)
            {
                Label1.Text = "Debe ingresar un número válido para el Pedido de Fundas.";
                PedidoFundas.Focus();
                return;
            }

            wsPedidosFundas.PedidoFundas[] q;
            q = (wsPedidosFundas.PedidoFundas[])Session["q"];
              
            q[(int)Session["qModificada"]].PedidoColchon = Convert.ToInt32(PedidoColchones.Text);
            q[(int)Session["qModificada"]].PedidoFunda = Convert.ToInt32(PedidoFundas.Text);

            Session["q"] = q;
            Response.Redirect("SugeridoPedido.aspx");
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("SugeridoPedido.aspx");
        }
    }
}