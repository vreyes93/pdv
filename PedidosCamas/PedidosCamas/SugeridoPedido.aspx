﻿<%@ Page Title="Sugerido de Pedido" Language="C#" MasterPageFile="~/SimpleMaster.Master" AutoEventWireup="true" CodeBehind="SugeridoPedido.aspx.cs" Inherits="PedidosCamas.SugeridoPedido" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="menu" runat="server">
    <style type="text/css">
        .style1
        {
            text-decoration: underline;
        }
        .style2
        {
            height: 33px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="divContenido">
    <table align="center">
        <tr>
            <td style="text-align: center" colspan="6">
                <span class="style1"><strong>SUGERIDO DE PEDIDO DE COLCHONES Y FUNDAS</strong></span>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="6">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center" class="style2" colspan="6">
                <asp:Button ID="btnImprimir" runat="server" onclick="btnImprimir_Click" 
                    Text="Imprimir" 
                    ToolTip="Este botón prepara la página para que pueda imprimirla desde su navegador.  AL TERMINAR DE IMPRIMIR HAGA CLIC EN LA PALABRA &quot;Bodega&quot;." />
                <asp:Button ID="btnMarcarTodos" runat="server" onclick="btnMarcarTodos_Click" 
                    Text="Marcar Todos" />
                <asp:Button ID="btnDesmarcarTodos" runat="server" onclick="btnDesmarcarTodos_Click" 
                    Text="Desmarcar Todos" />
                <asp:Button ID="btnInvertirSeleccion" runat="server" onclick="btnInvertirSeleccion_Click" 
                    Text="Invertir Selección" />
                <asp:Button ID="btnEliminarMarcados" runat="server" onclick="btnEliminarMarcados_Click" 
                    Text="Elim. Marcados" 
                    onclientclick="javascript:return confirm(&quot;Desea eliminar los registros marcados?&quot;)" />
                <asp:Button ID="btnEliminarDesMarcados" runat="server" onclick="btnEliminarDesMarcados_Click" 
                    Text="Elim. Des-Marcados" 
                    
                    onclientclick="javascript:return confirm(&quot;Desea eliminar los registros desmarcados?&quot;)" />
                <asp:Button ID="btnGrabar" runat="server" onclick="btnGrabar_Click" 
                    Text="Grabar" />
                <asp:Button ID="btnActualizar" runat="server" onclick="btnActualizar_Click" 
                    Text="Actualizar" 
                    
                    ToolTip="Genera de nuevo el sugerido de pedido en base a la bodega seleccionada" />
                <asp:Button ID="btnEliminarCeros" runat="server" onclick="btnEliminarCeros_Click" 
                    Text="Elim. Ceros" 
                    onclientclick="javascript:return confirm(&quot;Desea eliminar los registros en cero?&quot;)" 
                    
                    ToolTip="Elimina todos los registros que tengan cero en todas sus columnas" />
                <asp:Button ID="btnSalir0" runat="server" onclick="btnSalir_Click" 
                    Text="Salir" />
            </td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:LinkButton ID="lbBodega" runat="server" Font-Underline="False" 
                    ForeColor="#846969" onclick="lbBodega_Click">Bodega:</asp:LinkButton>
            </td>
            <td style="text-align: left">
                <asp:DropDownList ID="bodega" runat="server" Width="285px" AutoPostBack="True" 
                    onselectedindexchanged="bodega_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td style="text-align: left">
                <asp:Label ID="lbDescripcion" runat="server" Text="Descripción para Pedido:"></asp:Label>
            </td>
            <td style="text-align: left" colspan="3">
                <asp:TextBox ID="descripcion" runat="server" Width="356px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: left" colspan="4">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                <asp:Label ID="Label8" runat="server"></asp:Label>
            </td>
            <td style="text-align: right" colspan="2">
                <asp:Label ID="lbHora" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:GridView ID="gridPedido" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                    onrowdatabound="gridPedido_RowDataBound" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px"
                    onselectedindexchanged="gridPedido_SelectedIndexChanged" onpageindexchanging="gridPedido_PageIndexChanging" 
                    PageSize="20" >
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="cbFunda" runat="server" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" />
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbModificar" runat="server" CausesValidation="False" 
                                    CommandName="Select" onclick="lbModificar_Click" Text="Modificar" 
                                    ToolTip="Modifica los valores sugeridos para pedir Colchones y/o Fundas" 
                                    Font-Size="Small"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbEliminar" runat="server" CausesValidation="False" 
                                    CommandName="Select" onclick="lbEliminar_Click" 
                                    onclientclick="javascript:return confirm(&quot;Desea eliminar el registro?&quot;)" 
                                    Text="Eliminar" ToolTip="Le permite eliminar el registro seleccionado." 
                                    Font-Size="Small"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbDetalle" runat="server" CausesValidation="False" 
                                    CommandName="Select" Text="Detalle" 
                                    ToolTip="Le permite ver el detalle de la distribución de las existencias entre las bodegas" 
                                    onclick="lbDetalle_Click" Font-Size="Small"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Funda">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Font-Size="Small" Text='<%# Bind("Articulo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Font-Size="Small" Text='<%# Bind("Articulo") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="90px" />
                            <ItemStyle HorizontalAlign="Center" Width="90px" BackColor="#A9C3E1" ForeColor="White" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nombre de la Funda">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
					            <table width="100%" border="0">
						            <tr>
							            <td width="70%">
								            <asp:DataGrid id="gridPedidoDet" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									            BorderStyle="Solid" BorderWidth="1px" BackColor="LightBlue" CellPadding="4" Font-Size="X-Small" onrowdatabound="gridPedidoDet_RowDataBound" onselectedindexchanged="gridPedidoDet_SelectedIndexChanged" GridLines="Both">
									            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									            <SelectedItemStyle Font-Bold="True" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									            <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									            <Columns>
                                                    <asp:TemplateColumn>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lbDetalleBodega" runat="server" CausesValidation="false" 
                                                                CommandName="Select" onclick="lbDetalleBodega_Click" Text="Detalle"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
										            <asp:TemplateColumn HeaderText="Bodega">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="TextBox19" runat="server" 
                                                                Text='<%# DataBinder.Eval(Container, "DataItem.Bodega") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label19" runat="server" 
                                                                Text='<%# DataBinder.Eval(Container, "DataItem.Bodega") %>'></asp:Label>
					                                        <table width="100%" border="0">
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridPedidoDetDet" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" onrowdatabound="gridPedidoDetDet_RowDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="True" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Bodega" HeaderText="Bodega" HeaderStyle-Width="45" ItemStyle-Width="45" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="false"></asp:BoundColumn>
										                                        <asp:BoundColumn DataField="Articulo" HeaderText="Colchón" HeaderStyle-Width="80" ItemStyle-Width="80" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
										                                        <asp:BoundColumn DataField="Descripcion" HeaderText="Nombre del Colchón" HeaderStyle-Width="125" ItemStyle-Width="125" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
										                                        <asp:BoundColumn DataField="Existencias" HeaderText="Exis." HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:###,###,###,###,###}"></asp:BoundColumn>
										                                        <asp:BoundColumn DataField="Funda" HeaderText="Ver También" HeaderStyle-Width="105" ItemStyle-Width="105" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-BackColor="#a9c3e1" ItemStyle-ForeColor="White" Visible="false"></asp:BoundColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
						                                        <tr>
							                                        <td width="70%">
								                                        <asp:DataGrid id="gridPedidoDetDetBases" runat="server" AutoGenerateColumns="False" BorderColor="#284775"
									                                        BorderStyle="Solid" BorderWidth="1px" BackColor="White" CellPadding="4" Font-Size="X-Small" onrowdatabound="gridPedidoDetDetBases_RowDataBound" GridLines="Both">
									                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
									                                        <SelectedItemStyle Font-Bold="True" ForeColor="#663399" BackColor="#dceafd"></SelectedItemStyle>
									                                        <ItemStyle ForeColor="#330099" BackColor="White" BorderColor="#284775" BorderStyle="Solid" BorderWidth="1px" ></ItemStyle>
									                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
									                                        <Columns>
										                                        <asp:BoundColumn DataField="Bodega" HeaderText="Bodega" HeaderStyle-Width="45" ItemStyle-Width="45" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="false"></asp:BoundColumn>
										                                        <asp:BoundColumn DataField="Articulo" HeaderText="Base" HeaderStyle-Width="80" ItemStyle-Width="80" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
										                                        <asp:BoundColumn DataField="Descripcion" HeaderText="Nombre de la Base" HeaderStyle-Width="125" ItemStyle-Width="125" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
										                                        <asp:BoundColumn DataField="Existencias" HeaderText="Exis." HeaderStyle-Width="20" ItemStyle-Width="20" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:###,###,###,###,###}"></asp:BoundColumn>
										                                        <asp:BoundColumn DataField="Funda" HeaderText="Ver También" HeaderStyle-Width="105" ItemStyle-Width="105" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-BackColor="#a9c3e1" ItemStyle-ForeColor="White" Visible="false"></asp:BoundColumn>
									                                        </Columns>
									                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								                                        </asp:DataGrid>
							                                        </td>
						                                        </tr>
					                                        </table>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Left" Width="330px" />
                                                        <ItemStyle HorizontalAlign="Left" Width="330px" />
                                                    </asp:TemplateColumn>
										            <asp:BoundColumn DataField="Colchones" HeaderText="Colchones" HeaderStyle-Width="45" ItemStyle-Width="45" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-BackColor="White" DataFormatString="{0:###,###,###,###,###}"></asp:BoundColumn>
										            <asp:BoundColumn DataField="Bases" HeaderText="Bases" HeaderStyle-Width="27" ItemStyle-Width="27" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-BackColor="#a9c3e1" ItemStyle-ForeColor="White" DataFormatString="{0:###,###,###,###,###}"></asp:BoundColumn>
										            <asp:BoundColumn DataField="Fundas" HeaderText="Fundas" HeaderStyle-Width="30" ItemStyle-Width="30" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-BackColor="#a9c3e1" ItemStyle-ForeColor="White" DataFormatString="{0:###,###,###,###,###}"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CodigoBodega" HeaderText="CodigoBodega" HeaderStyle-Width="45" ItemStyle-Width="45" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="false"></asp:BoundColumn>
									            </Columns>
									            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								            </asp:DataGrid>
							            </td>
						            </tr>
					            </table>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" Width="500px" />
                            <ItemStyle HorizontalAlign="Left" Width="500px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Colchones">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Colchon", "{0:###,###,###,###,###}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("Colchon", "{0:###,###,###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="40px" />
                            <ItemStyle HorizontalAlign="Center" Width="40px" BackColor="White" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Bases">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Base", "{0:###,###,###,###,###}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("Base", "{0:###,###,###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="35px" />
                            <ItemStyle HorizontalAlign="Center" Width="35px" BackColor="#a9c3e1" ForeColor="White" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fundas">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Funda", "{0:###,###,###,###,###}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Bind("Funda", "{0:###,###,###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="35px" />
                            <ItemStyle HorizontalAlign="Center" Width="35px" BackColor="#a9c3e1" ForeColor="White" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pedido Colchones">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("PedidoColchon", "{0:###,###,###,###,###}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Bind("PedidoColchon", "{0:###,###,###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="60px" />
                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pedido Fundas">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("PedidoFunda", "{0:###,###,###,###,###}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Text='<%# Bind("PedidoFunda", "{0:###,###,###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="45px" />
                            <ItemStyle HorizontalAlign="Center" Width="45px" BackColor="#a9c3e1" ForeColor="White" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Código Artículo" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text='<%# Bind("CodigoArticulo") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("CodigoArticulo") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="CodigoFunda" HeaderText="CodigoFunda" 
                            Visible="False" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderColor="#284775" BorderWidth="1px" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                <asp:Label ID="Label9" runat="server"></asp:Label>
            </td>
            <td style="text-align: right">
                <asp:Label ID="lbHora0" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: center" colspan="6">
                <asp:Button ID="btnImprimir0" runat="server" onclick="btnImprimir_Click" 
                    Text="Imprimir" 
                    ToolTip="Este botón prepara la página para que pueda imprimirla desde su navegador.  AL TERMINAR DE IMPRIMIR HAGA CLIC EN LA PALABRA &quot;Bodega&quot;." />
                <asp:Button ID="btnMarcarTodos0" runat="server" onclick="btnMarcarTodos_Click" 
                    Text="Marcar Todos" />
                <asp:Button ID="btnDesmarcarTodos0" runat="server" onclick="btnDesmarcarTodos_Click" 
                    Text="Desmarcar Todos" />
                <asp:Button ID="btnInvertirSeleccion0" runat="server" onclick="btnInvertirSeleccion_Click" 
                    Text="Invertir Selección" />
                <asp:Button ID="btnEliminarMarcados0" runat="server" onclick="btnEliminarMarcados_Click" 
                    Text="Elim. Marcados" 
                    onclientclick="javascript:return confirm(&quot;Desea eliminar los registros marcados?&quot;)" />
                <asp:Button ID="btnEliminarDesMarcados0" runat="server" onclick="btnEliminarDesMarcados_Click" 
                    Text="Elim. Des-Marcados" 
                    
                    onclientclick="javascript:return confirm(&quot;Desea eliminar los registros desmarcados?&quot;)" />
                <asp:Button ID="btnGrabar0" runat="server" onclick="btnGrabar_Click" 
                    Text="Grabar" />
                <asp:Button ID="btnActualizar0" runat="server" onclick="btnActualizar_Click" 
                    Text="Actualizar" 
                    
                    ToolTip="Genera de nuevo el sugerido de pedido en base a la bodega seleccionada" />
                <asp:Button ID="btnEliminarCeros0" runat="server" onclick="btnEliminarCeros_Click" 
                    Text="Elim. Ceros" 
                    onclientclick="javascript:return confirm(&quot;Desea eliminar los registros en cero?&quot;)" 
                    
                    ToolTip="Elimina todos los registros que tengan cero en todas sus columnas" />
                <asp:Button ID="btnSalir" runat="server" onclick="btnSalir_Click" 
                    Text="Salir" />
                <asp:GridView ID="gridPedidoBK" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" Visible="False">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbModificar0" runat="server" CausesValidation="False" 
                                    CommandName="Select" onclick="lbModificar_Click" Text="Modificar" 
                                    ToolTip="Modifica los valores sugeridos para pedir Colchones y/o Fundas"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbEliminar0" runat="server" CausesValidation="False" 
                                    CommandName="Select" onclick="lbEliminar_Click" 
                                    onclientclick="javascript:return confirm(&quot;Desea eliminar el registro?&quot;)" 
                                    Text="Eliminar" ToolTip="Le permite eliminar el registro seleccionado."></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbDetalle0" runat="server" CausesValidation="False" 
                                    CommandName="Select" Text="Detalle" 
                                    
                                    ToolTip="Le permite ver el detalle de la distribución de las existencias entre las bodegas" 
                                    onclick="lbDetalle_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Código">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("Articulo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label10" runat="server" Text='<%# Bind("Articulo") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="65px" />
                            <ItemStyle HorizontalAlign="Center" Width="65px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Descripción">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label11" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" Width="355px" />
                            <ItemStyle HorizontalAlign="Left" Width="355px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Colchones">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("Colchon") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label12" runat="server" Text='<%# Bind("Colchon") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="60px" />
                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Bases">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox12" runat="server" Text='<%# Bind("Base") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label13" runat="server" Text='<%# Bind("Base") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fundas">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox13" runat="server" Text='<%# Bind("Funda") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label14" runat="server" Text='<%# Bind("Funda") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pedido Colchones">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox14" runat="server" Text='<%# Bind("PedidoColchon") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label15" runat="server" Text='<%# Bind("PedidoColchon") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="75px" />
                            <ItemStyle HorizontalAlign="Center" Width="75px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pedido Fundas">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox15" runat="server" Text='<%# Bind("PedidoFunda") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label16" runat="server" Text='<%# Bind("PedidoFunda") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="65px" />
                            <ItemStyle HorizontalAlign="Center" Width="65px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Código Artículo" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label17" runat="server" Text='<%# Bind("CodigoArticulo") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox16" runat="server" Text='<%# Bind("CodigoArticulo") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="CodigoFunda" HeaderText="CodigoFunda" 
                            Visible="False" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</div>
</asp:Content>
