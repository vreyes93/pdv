﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PedidosCamas
{
    public partial class SugeridoPedido : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["urlPrecios"] = Convert.ToString(Session["UrlPrecios"]);
                ViewState["url"] = string.Format("http://sql.fiesta.local/services/wsPedidosFundas.asmx", Request.Url.Host);

                CargarBodegas();
                if (Session["qModificada"] == null) GenerarSugerido(); else CargarModificacion();
            }

            lbHora.Text = DateTime.Now.ToString();
            lbHora0.Text = DateTime.Now.ToString();
        }
        
        void CargarBodegas()
        {
            var ws = new wsPedidosFundas.wsPedidosFundas();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarBodegas().ToList();
            
            wsPedidosFundas.Bodegas item = new wsPedidosFundas.Bodegas();
            item.Bodega = "%";
            item.Nombre = "Ver Todas las Bodegas";
            q.Add(item);

            wsPedidosFundas.Bodegas item2 = new wsPedidosFundas.Bodegas();
            item2.Bodega = "MF";
            item2.Nombre = "Ver Todas las Bodegas de MF";
            q.Add(item2);

            wsPedidosFundas.Bodegas item3 = new wsPedidosFundas.Bodegas();
            item3.Bodega = "SI";
            item3.Nombre = "Ver Todas las Bodegas de SIMAN y SEARS";
            q.Add(item3);

            bodega.DataSource = q;
            bodega.DataTextField = "Nombre";
            bodega.DataValueField = "Bodega";
            bodega.DataBind();

            if (Session["bodega"] == null) bodega.SelectedValue = "F01"; else bodega.SelectedValue = (string)Session["bodega"];
        }

        void GenerarSugerido()
        {
            var ws = new wsPedidosFundas.wsPedidosFundas();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            ws.Timeout = 999999999;
            var q = ws.SugeridoPedido(bodega.SelectedValue);

            for (int ii = 0; ii < q.Length; ii++)
            {
                if (q[ii].Colchon == null) q[ii].Colchon = 0;
                if (q[ii].Base == null) q[ii].Base = 0;
                if (q[ii].Funda == null) q[ii].Funda = 0;

                if (q[ii].Articulo == "140099-083")
                {
                    int jj = 0;
                    jj += 1;
                }

                int mExistenciasArticuloRelacion = ws.ExistenciasArticuloRelacion(q[ii].Articulo, bodega.SelectedValue);

                int mFactorFunda = (int)(q[ii].Base + q[ii].Funda + mExistenciasArticuloRelacion);
                int mFactorColchon = (int)(q[ii].Descripcion.ToUpper().Contains("KING") ? (q[ii].Colchon * 2) : q[ii].Colchon);

                int mPedidoColchon = mFactorFunda - mFactorColchon;
                int mPedidoFunda = mFactorColchon - mFactorFunda;

                if (q[ii].Descripcion.ToUpper().Contains("KING")) mPedidoColchon = mPedidoColchon / 2;

                if (mPedidoColchon > 0) q[ii].PedidoColchon = mPedidoColchon;
                if (mPedidoFunda > 0) q[ii].PedidoFunda = mPedidoFunda;
            }

            Label1.Text = "";
            Label2.Text = "";
              
            Label8.Text = string.Format("El pedido consta de {0} artículos.", q.Length);
            Label9.Text = string.Format("El pedido consta de {0} artículos.", q.Length);

            Session["q"] = q;

            gridPedido.DataSource = q;
            gridPedido.DataBind();

            gridPedidoBK.DataSource = q;
            gridPedidoBK.DataBind();

            SetToolTips();
        }

        void SetToolTips()
        {
            for (int ii = 0; ii < gridPedido.Rows.Count; ii++)
            {
                gridPedido.Rows[ii].Cells[6].ToolTip = "Existencias de colchones";
                gridPedido.Rows[ii].Cells[7].ToolTip = "Existencias de bases";
                gridPedido.Rows[ii].Cells[8].ToolTip = "Existencias de fundas";
                gridPedido.Rows[ii].Cells[9].ToolTip = "Pedido de colchones";
                gridPedido.Rows[ii].Cells[10].ToolTip = "Pedido de fundas";
            }
        }

        void CargarModificacion()
        {
            gridPedido.DataSource = Session["q"];
            gridPedido.DataBind();

            gridPedidoBK.DataSource = Session["q"];
            gridPedidoBK.DataBind();

            Label1.Text = "";
            Label2.Text = "";

            Label8.Text = string.Format("El pedido consta de {0} artículos.", gridPedidoBK.Rows.Count);
            Label9.Text = string.Format("El pedido consta de {0} artículos.", gridPedidoBK.Rows.Count);

            Session["qModificada"] = null;
            if (Session["bodega"] != null) bodega.SelectedValue = (string)Session["bodega"];

            SetToolTips();
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void gridPedido_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            GenerarSugerido();
        }

        protected void btnGrabar_Click(object sender, EventArgs e)
        {
            if (descripcion.Text.Trim().Length == 0)
            {
                Label1.Text = "Debe ingresar la descripción del pedido.";
                Label2.Text = "Debe ingresar la descripción del pedido.";

                descripcion.Focus();
                return;
            }

            CargarSession();

            wsPedidosFundas.PedidoFundas[] q;
            q = (wsPedidosFundas.PedidoFundas[])Session["q"];

            var ws = new wsPedidosFundas.wsPedidosFundas(); string mMensaje = "";
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            if (!ws.GrabarSugeridoPedido(0, descripcion.Text, q, ref mMensaje, bodega.SelectedValue))
            {
                Label1.Text = string.Format("Error al grabar el pedido: {0}", mMensaje);
                Label2.Text = string.Format("Error al grabar el pedido: {0}", mMensaje);

                return;
            }

            Label1.Text = "";
            Label2.Text = "";

            Label8.Text = "El pedido fue grabado exitosamente.";
            Label9.Text = "El pedido fue grabado exitosamente.";
            Response.Redirect("Default.aspx");
        }

        protected void lbModificar_Click(object sender, EventArgs e)
        {
            ViewState["Boton"] = "Modificar";
        }

        protected void lbEliminar_Click(object sender, EventArgs e)
        {
            ViewState["Boton"] = "Eliminar";
        }

        protected void lbDetalle_Click(object sender, EventArgs e)
        {
            ViewState["Boton"] = "Detalle";
        }

        protected void gridPedido_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow rowSeleccionada = gridPedido.SelectedRow;

            switch (Convert.ToString(ViewState["Boton"]))
            {
                case "Modificar":
                    CargarSession();

                    Label lbCodigoSeleccionada = (Label)rowSeleccionada.FindControl("Label1");
                    Label lbDescripcionSeleccionada = (Label)rowSeleccionada.FindControl("Label2");
                    Label lbColchonesSeleccionada = (Label)rowSeleccionada.FindControl("Label3");
                    Label lbBasesSeleccionada = (Label)rowSeleccionada.FindControl("Label4");
                    Label lbFundasSeleccionada = (Label)rowSeleccionada.FindControl("Label5");
                    Label lbPedidoColchonesSeleccionada = (Label)rowSeleccionada.FindControl("Label6");
                    Label lbPedidoFundasSeleccionada = (Label)rowSeleccionada.FindControl("Label7");

                    wsPedidosFundas.PedidoFundas itemSeleccionada = new wsPedidosFundas.PedidoFundas();
                    itemSeleccionada.Articulo = lbCodigoSeleccionada.Text;
                    itemSeleccionada.Descripcion = lbDescripcionSeleccionada.Text;
                    itemSeleccionada.Colchon = lbColchonesSeleccionada.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbColchonesSeleccionada.Text);
                    itemSeleccionada.Base = lbBasesSeleccionada.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbBasesSeleccionada.Text);
                    itemSeleccionada.Funda = lbFundasSeleccionada.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbFundasSeleccionada.Text);
                    itemSeleccionada.PedidoColchon = lbPedidoColchonesSeleccionada.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbPedidoColchonesSeleccionada.Text);
                    itemSeleccionada.PedidoFunda = lbPedidoFundasSeleccionada.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbPedidoFundasSeleccionada.Text);

                    Session["qModificada"] = null;
                    Session["qModificada"] = rowSeleccionada.RowIndex;
                    Session["qSeleccionada"] = itemSeleccionada;
                    Session["bodega"] = bodega.SelectedValue;

                    Response.Redirect("RenglonPedido.aspx");

                    break;
                case "Eliminar":
                    wsPedidosFundas.PedidoFundas[] q = new wsPedidosFundas.PedidoFundas[1];

                    var qBK = Session["q"];

                    gridPedidoBK.DataSource = qBK;
                    gridPedidoBK.DataBind();

                    int jj = 0;
                    for (int ii = 0; ii < gridPedidoBK.Rows.Count; ii++)
                    {
                        if (ii != rowSeleccionada.RowIndex)
                        {
                            jj++;
                            GridViewRow row = gridPedidoBK.Rows[ii];

                            Label lbCodigo = (Label)row.FindControl("Label10");
                            Label lbDescripcion = (Label)row.FindControl("Label11");
                            Label lbColchones = (Label)row.FindControl("Label12");
                            Label lbBases = (Label)row.FindControl("Label13");
                            Label lbFundas = (Label)row.FindControl("Label14");
                            Label lbPedidoColchones = (Label)row.FindControl("Label15");
                            Label lbPedidoFundas = (Label)row.FindControl("Label16");
                            Label lbCodigoArticulo = (Label)row.FindControl("Label17");

                            wsPedidosFundas.PedidoFundas item = new wsPedidosFundas.PedidoFundas();
                            item.Articulo = lbCodigo.Text;
                            item.Descripcion = lbDescripcion.Text;
                            item.Colchon = lbColchones.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbColchones.Text);
                            item.Base = lbBases.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbBases.Text);
                            item.Funda = lbFundas.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbFundas.Text);
                            item.PedidoColchon = lbPedidoColchones.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbPedidoColchones.Text);
                            item.PedidoFunda = lbPedidoFundas.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbPedidoFundas.Text);
                            item.CodigoArticulo = lbCodigoArticulo.Text;

                            if (jj > 1) Array.Resize(ref q, jj);
                            q[jj - 1] = item;
                        }
                    }

                    Label1.Text = "";
                    Label2.Text = "";

                    Label8.Text = string.Format("El pedido consta de {0} artículos.", q.Length);
                    Label9.Text = string.Format("El pedido consta de {0} artículos.", q.Length);

                    gridPedido.DataSource = q;
                    gridPedido.DataBind();

                    Session["q"] = q;
                    gridPedido.SelectedIndex = -1;

                    SetToolTips();
                    break;
                case "Detalle":
                    Label lbCodigoArticuloBuscar = (Label)rowSeleccionada.FindControl("Label8");
                    ViewState["ArticuloBuscar"] = lbCodigoArticuloBuscar.Text;

                    var ws = new wsPedidosFundas.wsPedidosFundas();
                    if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                    var qExistencias = ws.ListarExistenciasArticulo(lbCodigoArticuloBuscar.Text, bodega.SelectedValue);
                    wsPedidosFundas.ExistenciasArticulo[] qDet = new wsPedidosFundas.ExistenciasArticulo[1];

                    int kk = 0;
                    for (int ii = 0; ii < qExistencias.Length; ii++)
                    {
                        if (qExistencias[ii].Colchones == null) qExistencias[ii].Colchones = 0;
                        if (qExistencias[ii].Bases == null) qExistencias[ii].Bases = 0;
                        if (qExistencias[ii].Fundas == null) qExistencias[ii].Fundas = 0;

                        kk++;
                        wsPedidosFundas.ExistenciasArticulo itemExistencias = new wsPedidosFundas.ExistenciasArticulo();
                        itemExistencias.Bodega = qExistencias[ii].Bodega;
                        itemExistencias.Colchones = qExistencias[ii].Colchones;
                        itemExistencias.Bases = qExistencias[ii].Bases;
                        itemExistencias.Fundas = qExistencias[ii].Fundas;
                        itemExistencias.ToolTipColchones = qExistencias[ii].ToolTipColchones;
                        itemExistencias.ToolTipBases = qExistencias[ii].ToolTipBases;
                        itemExistencias.ToolTipFundas = qExistencias[ii].ToolTipFundas;
                        itemExistencias.CodigoBodega = qExistencias[ii].CodigoBodega;

                        if (kk > 1) Array.Resize(ref qDet, kk);
                        qDet[kk - 1] = itemExistencias;
                    }

                    Control ctl = gridPedido.SelectedRow.FindControl("gridPedidoDet");

                    if (ctl is DataGrid && kk > 0)
                    {
                        DataGrid fileGrid = (DataGrid)ctl;
                        fileGrid.DataSource = qDet.ToArray();
                        fileGrid.DataBind();

                        for (int ii = 0; ii < fileGrid.Items.Count; ii++)
                        {
                            fileGrid.Items[ii].Cells[2].ToolTip = string.Format("Estas son las existencias de colchones en {0}", qExistencias[ii].Bodega);
                            fileGrid.Items[ii].Cells[3].ToolTip = string.Format("Estas son las existencias de bases en {0}", qExistencias[ii].Bodega);
                            fileGrid.Items[ii].Cells[4].ToolTip = string.Format("Estas son las existencias de fundas en {0}", qExistencias[ii].Bodega);
                        }
                    }

                    break;
            }
        }

        protected void gridPedidoDet_SelectedIndexChanged(object sender, EventArgs e)
        {
            Control ctl, ctlBases;

            try
            {
                ctl = gridPedido.SelectedRow.FindControl("gridPedidoDet");
            }
            catch
            {
                Label1.Text = "Por favor haga clic en 'Detalle' para seleccionar la fila (3era columna de izquierda a derecha).";
                Label2.Text = "Por favor haga clic en 'Detalle' para seleccionar la fila (3era columna de izquierda a derecha).";
                return;
            }

            if (ctl is DataGrid)
            {
                Control ctlDet;
                DataGrid ctlGrid = (DataGrid)ctl;

                try
                {
                     ctlDet = ctlGrid.SelectedItem.FindControl("gridPedidoDetDet");
                }
                catch
                {
                    Label1.Text = "Por favor haga clic en las columnas 'Detalle' para seleccionar la fila (3era. y 5ta. columnas de izquierda a derecha).";
                    Label2.Text = "Por favor haga clic en las columnas 'Detalle' para seleccionar la fila (3era. y 5ta. columnas de izquierda a derecha).";
                    return;
                }

                Label1.Text = "";
                Label2.Text = "";
                Label lbBodega = (Label)ctlGrid.Items[ctlGrid.SelectedIndex].FindControl("Label19");

                string mArticulo = (string)ViewState["ArticuloBuscar"];
                string mBodega = lbBodega.Text;

                var ws = new wsPedidosFundas.wsPedidosFundas();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);
                   
                var q = ws.ListarExistenciasArticuloRelacion((string)ViewState["ArticuloBuscar"], lbBodega.Text);

                DataGrid fileGrid = (DataGrid)ctlDet;
                fileGrid.DataSource = q.ToArray();
                fileGrid.DataBind();

                bool mHayFundas = false;
                for (int ii = 0; ii < fileGrid.Items.Count; ii++)
                {
                    fileGrid.Items[ii].Cells[3].ToolTip = string.Format("Estas existencias corresponden al Colchón {0}.", fileGrid.Items[ii].Cells[1].Text);

                    if (fileGrid.Items[ii].Cells[4].Text != "&nbsp;")
                    {
                        mHayFundas = true;
                        fileGrid.Items[ii].Cells[4].ToolTip = string.Format("El Colchón {0} se encuentra también relacionado con la o las fundas desplegadas en esta casilla junto con sus respectivas bases.", fileGrid.Items[ii].Cells[1].Text);
                    }
                }

                if (mHayFundas)
                {
                    fileGrid.Columns[4].Visible = true;
                }
                else
                {
                    fileGrid.Columns[3].ItemStyle.Width = 125;
                    fileGrid.Columns[3].HeaderStyle.Width = 125;
                }
            }

        
        //Bases
            try
            {
                ctlBases = gridPedido.SelectedRow.FindControl("gridPedidoDet");
            }
            catch
            {
                Label1.Text = "Por favor haga clic en 'Detalle' para seleccionar la fila (3era columna de izquierda a derecha).";
                Label2.Text = "Por favor haga clic en 'Detalle' para seleccionar la fila (3era columna de izquierda a derecha).";
                return;
            }

            if (ctlBases is DataGrid)
            {
                Control ctlDet;
                DataGrid ctlGrid = (DataGrid)ctlBases;

                try
                {
                    ctlDet = ctlGrid.SelectedItem.FindControl("gridPedidoDetDetBases");
                }
                catch
                {
                    Label1.Text = "Por favor haga clic en las columnas 'Detalle' para seleccionar la fila (3era. y 5ta. columnas de izquierda a derecha).";
                    Label2.Text = "Por favor haga clic en las columnas 'Detalle' para seleccionar la fila (3era. y 5ta. columnas de izquierda a derecha).";
                    return;
                }

                Label1.Text = "";
                Label2.Text = "";
                Label lbBodega = (Label)ctlGrid.Items[ctlGrid.SelectedIndex].FindControl("Label19");

                string mArticulo = (string)ViewState["ArticuloBuscar"];
                string mBodega = lbBodega.Text;

                var ws = new wsPedidosFundas.wsPedidosFundas();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var q = ws.ListarExistenciasArticuloRelacionBases((string)ViewState["ArticuloBuscar"], lbBodega.Text);

                DataGrid fileGrid = (DataGrid)ctlDet;
                fileGrid.DataSource = q.ToArray();
                fileGrid.DataBind();

                bool mHayFundas = false;
                for (int ii = 0; ii < fileGrid.Items.Count; ii++)
                {
                    fileGrid.Items[ii].Cells[3].ToolTip = string.Format("Estas existencias corresponden a la Base {0}.", fileGrid.Items[ii].Cells[1].Text);

                    if (fileGrid.Items[ii].Cells[4].Text != "&nbsp;")
                    {
                        mHayFundas = true;
                        fileGrid.Items[ii].Cells[4].ToolTip = string.Format("El Colchón {0} se encuentra también relacionado con la o las fundas desplegadas en esta casilla junto con sus respectivas bases.", fileGrid.Items[ii].Cells[1].Text);
                    }
                }

                if (mHayFundas)
                {
                    fileGrid.Columns[4].Visible = true;
                }
                else
                {
                    fileGrid.Columns[3].ItemStyle.Width = 125;
                    fileGrid.Columns[3].HeaderStyle.Width = 125;
                }
            }

        }

        protected void gridPedidoDet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridPedidoDetDet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        void CargarSession()
        {
            wsPedidosFundas.PedidoFundas[] q = new wsPedidosFundas.PedidoFundas[1];

            var qBK = Session["q"];

            gridPedidoBK.DataSource = qBK;
            gridPedidoBK.DataBind();

            for (int ii = 0; ii < gridPedidoBK.Rows.Count; ii++)
            {
                GridViewRow row = gridPedidoBK.Rows[ii];

                Label lbCodigo = (Label)row.FindControl("Label10");
                Label lbDescripcion = (Label)row.FindControl("Label11");
                Label lbColchones = (Label)row.FindControl("Label12");
                Label lbBases = (Label)row.FindControl("Label13");
                Label lbFundas = (Label)row.FindControl("Label14");
                Label lbPedidoColchones = (Label)row.FindControl("Label15");
                Label lbPedidoFundas = (Label)row.FindControl("Label16");

                wsPedidosFundas.PedidoFundas item = new wsPedidosFundas.PedidoFundas();
                item.Articulo = lbCodigo.Text;
                item.Descripcion = lbDescripcion.Text;
                item.Colchon = lbColchones.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbColchones.Text);
                item.Base = lbBases.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbBases.Text);
                item.Funda = lbFundas.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbFundas.Text);
                item.PedidoColchon = lbPedidoColchones.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbPedidoColchones.Text);
                item.PedidoFunda = lbPedidoFundas.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbPedidoFundas.Text);

                if (ii > 0) Array.Resize(ref q, ii + 1);
                q[ii] = item;
            }

            Session["q"] = q;
        }

        protected void bodega_SelectedIndexChanged(object sender, EventArgs e)
        {
            GenerarSugerido();
        }

        protected void btnEliminarCeros_Click(object sender, EventArgs e)
        {
            wsPedidosFundas.PedidoFundas[] q = new wsPedidosFundas.PedidoFundas[1];

            var qBK = Session["q"];

            gridPedidoBK.DataSource = qBK;
            gridPedidoBK.DataBind();

            int jj = 0;
            for (int ii = 0; ii < gridPedidoBK.Rows.Count; ii++)
            {
                GridViewRow row = gridPedidoBK.Rows[ii];

                Label lbCodigo = (Label)row.FindControl("Label10");
                Label lbDescripcion = (Label)row.FindControl("Label11");
                Label lbColchones = (Label)row.FindControl("Label12");
                Label lbBases = (Label)row.FindControl("Label13");
                Label lbFundas = (Label)row.FindControl("Label14");
                Label lbPedidoColchones = (Label)row.FindControl("Label15");
                Label lbPedidoFundas = (Label)row.FindControl("Label16");
                Label lbCodigoArticulo = (Label)row.FindControl("Label17");

                if (lbColchones.Text != "0" || lbBases.Text != "0" || lbFundas.Text != "0" || lbPedidoColchones.Text != "0" || lbPedidoFundas.Text != "0")
                {
                    jj++;

                    wsPedidosFundas.PedidoFundas item = new wsPedidosFundas.PedidoFundas();
                    item.Articulo = lbCodigo.Text;
                    item.Descripcion = lbDescripcion.Text;
                    item.Colchon = Convert.ToInt32(lbColchones.Text);
                    item.Base = Convert.ToInt32(lbBases.Text);
                    item.Funda = Convert.ToInt32(lbFundas.Text);
                    item.PedidoColchon = Convert.ToInt32(lbPedidoColchones.Text);
                    item.PedidoFunda = Convert.ToInt32(lbPedidoFundas.Text);
                    item.CodigoArticulo = lbCodigoArticulo.Text;

                    if (jj > 1) Array.Resize(ref q, jj);
                    q[jj - 1] = item;
                }
            }

            Label1.Text = "";
            Label2.Text = "";

            Label8.Text = string.Format("El pedido consta de {0} artículos.", q.Length);
            Label9.Text = string.Format("El pedido consta de {0} artículos.", q.Length);

            gridPedido.DataSource = q;
            gridPedido.DataBind();

            SetToolTips();

            Session["q"] = q;
            gridPedido.SelectedIndex = -1;
        }

        protected void gridPedido_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            var q = Session["q"];

            gridPedido.DataSource = q;
            gridPedido.PageIndex = e.NewPageIndex;
            gridPedido.DataBind();

            SetToolTips();
            gridPedido.SelectedIndex = -1;
        }

        protected void lbDetalleBodega_Click(object sender, EventArgs e)
        {

        }

        protected void btnMarcarTodos_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow gvr in gridPedido.Rows)
            {
                CheckBox cb = (CheckBox)gvr.FindControl("cbFunda");
                cb.Checked = true;
            }
        }

        protected void btnDesmarcarTodos_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow gvr in gridPedido.Rows)
            {
                CheckBox cb = (CheckBox)gvr.FindControl("cbFunda");
                cb.Checked = false;
            }
        }

        protected void btnEliminarMarcados_Click(object sender, EventArgs e)
        {
            int jj = 0;
            wsPedidosFundas.PedidoFundas[] q = new wsPedidosFundas.PedidoFundas[1];

            foreach (GridViewRow row in gridPedido.Rows)
            {
                CheckBox cb = (CheckBox)row.FindControl("cbFunda");

                if (!cb.Checked)
                {
                    jj++;
                    Label lbCodigo = (Label)row.FindControl("Label1");
                    Label lbDescripcion = (Label)row.FindControl("Label2");
                    Label lbColchones = (Label)row.FindControl("Label3");
                    Label lbBases = (Label)row.FindControl("Label4");
                    Label lbFundas = (Label)row.FindControl("Label5");
                    Label lbPedidoColchones = (Label)row.FindControl("Label6");
                    Label lbPedidoFundas = (Label)row.FindControl("Label7");
                    Label lbCodigoArticulo = (Label)row.FindControl("Label8");

                    wsPedidosFundas.PedidoFundas item = new wsPedidosFundas.PedidoFundas();
                    item.Articulo = lbCodigo.Text;
                    item.Descripcion = lbDescripcion.Text;
                    item.Colchon = lbColchones.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbColchones.Text);
                    item.Base = lbBases.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbBases.Text);
                    item.Funda = lbFundas.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbFundas.Text);
                    item.PedidoColchon = lbPedidoColchones.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbPedidoColchones.Text);
                    item.PedidoFunda = lbPedidoFundas.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbPedidoFundas.Text);
                    item.CodigoArticulo = lbCodigoArticulo.Text;

                    if (jj > 1) Array.Resize(ref q, jj);
                    q[jj - 1] = item;

                    Label1.Text = "";
                    Label2.Text = "";

                    Label8.Text = string.Format("El pedido consta de {0} artículos.", q.Length);
                    Label9.Text = string.Format("El pedido consta de {0} artículos.", q.Length);

                    gridPedido.DataSource = q;
                    gridPedido.DataBind();

                    Session["q"] = q;
                    gridPedido.SelectedIndex = -1;

                    SetToolTips();
                }
            }
        }

        protected void btnImprimir_Click(object sender, EventArgs e)
        {
            gridPedido.GridLines = GridLines.Both;
            gridPedido.HeaderStyle.ForeColor = System.Drawing.Color.Black;

            gridPedido.Columns[0].Visible = false;
            gridPedido.Columns[1].Visible = false;
            gridPedido.Columns[2].Visible = false;
            gridPedido.Columns[3].Visible = false;

            gridPedido.Columns[4].ItemStyle.ForeColor = System.Drawing.Color.Black;
            gridPedido.Columns[7].ItemStyle.ForeColor = System.Drawing.Color.Black;
            gridPedido.Columns[8].ItemStyle.ForeColor = System.Drawing.Color.Black;
            gridPedido.Columns[10].ItemStyle.ForeColor = System.Drawing.Color.Black;

            lbDescripcion.Visible = false;
            descripcion.Visible = false;

            btnActualizar.Visible = false;
            btnDesmarcarTodos.Visible = false;
            btnEliminarCeros.Visible = false;
            btnEliminarMarcados.Visible = false;
            btnGrabar.Visible = false;
            btnImprimir.Visible = false;
            btnMarcarTodos.Visible = false;
            btnSalir.Visible = false;

            btnActualizar0.Visible = false;
            btnDesmarcarTodos0.Visible = false;
            btnEliminarCeros0.Visible = false;
            btnEliminarMarcados0.Visible = false;
            btnGrabar0.Visible = false;
            btnImprimir0.Visible = false;
            btnMarcarTodos0.Visible = false;
            btnSalir0.Visible = false;

            Label1.Visible = false;
            Label2.Visible = false;
            Label8.Visible = false;
            Label9.Visible = false;

            foreach (GridViewRow row in gridPedido.Rows)
            {
                Control ctl = row.FindControl("gridPedidoDet");

                if (ctl is DataGrid)
                {
                    DataGrid fileGrid = (DataGrid)ctl;

                    fileGrid.SelectedIndex = -1;
                    fileGrid.Columns[0].Visible = false;
                    fileGrid.HeaderStyle.ForeColor = System.Drawing.Color.Black;

                    fileGrid.Columns[1].ItemStyle.ForeColor = System.Drawing.Color.Black;
                    fileGrid.Columns[2].ItemStyle.ForeColor = System.Drawing.Color.Black;
                    fileGrid.Columns[3].ItemStyle.ForeColor = System.Drawing.Color.Black;
                    fileGrid.Columns[4].ItemStyle.ForeColor = System.Drawing.Color.Black;

                    foreach (DataGridItem rowDet in fileGrid.Items)
                    {
                        Control ctlDet = rowDet.FindControl("gridPedidoDetDet");

                        if (ctlDet is DataGrid)
                        {
                            DataGrid fileGridDet = (DataGrid)ctlDet;

                            fileGridDet.SelectedIndex = -1;
                            fileGridDet.HeaderStyle.ForeColor = System.Drawing.Color.Black;

                            fileGridDet.Columns[1].ItemStyle.ForeColor = System.Drawing.Color.Black;
                            fileGridDet.Columns[2].ItemStyle.ForeColor = System.Drawing.Color.Black;
                            fileGridDet.Columns[3].ItemStyle.ForeColor = System.Drawing.Color.Black;
                            fileGridDet.Columns[4].ItemStyle.ForeColor = System.Drawing.Color.Black;
                        }
                    }
                }
            }

            gridPedido.SelectedIndex = -1;
            lbHora.Text = DateTime.Now.ToString();
            lbHora0.Text = DateTime.Now.ToString();
        }

        protected void lbBodega_Click(object sender, EventArgs e)
        {
            gridPedido.GridLines = GridLines.None;
            gridPedido.HeaderStyle.ForeColor = System.Drawing.Color.White;

            gridPedido.Columns[0].Visible = true;
            gridPedido.Columns[1].Visible = true;
            gridPedido.Columns[2].Visible = true;
            gridPedido.Columns[3].Visible = true;

            gridPedido.Columns[4].ItemStyle.ForeColor = System.Drawing.Color.White;
            gridPedido.Columns[7].ItemStyle.ForeColor = System.Drawing.Color.White;
            gridPedido.Columns[8].ItemStyle.ForeColor = System.Drawing.Color.White;
            gridPedido.Columns[10].ItemStyle.ForeColor = System.Drawing.Color.White;

            lbDescripcion.Visible = true;
            descripcion.Visible = true;

            btnActualizar.Visible = true;
            btnDesmarcarTodos.Visible = true;
            btnEliminarCeros.Visible = true;
            btnEliminarMarcados.Visible = true;
            btnGrabar.Visible = true;
            btnImprimir.Visible = true;
            btnMarcarTodos.Visible = true;
            btnSalir.Visible = true;

            btnActualizar0.Visible = true;
            btnDesmarcarTodos0.Visible = true;
            btnEliminarCeros0.Visible = true;
            btnEliminarMarcados0.Visible = true;
            btnGrabar0.Visible = true;
            btnImprimir0.Visible = true;
            btnMarcarTodos0.Visible = true;
            btnSalir0.Visible = true;

            Label1.Visible = true;
            Label2.Visible = true;
            Label8.Visible = true;
            Label9.Visible = true;

            foreach (GridViewRow row in gridPedido.Rows)
            {
                Control ctl = row.FindControl("gridPedidoDet");

                if (ctl is DataGrid)
                {
                    DataGrid fileGrid = (DataGrid)ctl;

                    fileGrid.Columns[0].Visible = true;
                    fileGrid.HeaderStyle.ForeColor = System.Drawing.Color.White;

                    fileGrid.Columns[1].ItemStyle.ForeColor = System.Drawing.Color.FromName("#330099");
                    fileGrid.Columns[2].ItemStyle.ForeColor = System.Drawing.Color.FromName("#330099");
                    fileGrid.Columns[3].ItemStyle.ForeColor = System.Drawing.Color.White;
                    fileGrid.Columns[4].ItemStyle.ForeColor = System.Drawing.Color.White;

                    fileGrid.Columns[0].ItemStyle.BackColor = System.Drawing.Color.FromName("#DCEAFD");
                    fileGrid.Columns[1].ItemStyle.BackColor = System.Drawing.Color.FromName("#DCEAFD");                    

                    foreach (DataGridItem rowDet in fileGrid.Items)
                    {
                        Control ctlDet = rowDet.FindControl("gridPedidoDetDet");

                        if (ctlDet is DataGrid)
                        {
                            DataGrid fileGridDet = (DataGrid)ctlDet;

                            fileGridDet.HeaderStyle.ForeColor = System.Drawing.Color.White;

                            fileGridDet.Columns[1].ItemStyle.ForeColor = System.Drawing.Color.FromName("#330099");
                            fileGridDet.Columns[2].ItemStyle.ForeColor = System.Drawing.Color.FromName("#330099");
                            fileGridDet.Columns[3].ItemStyle.ForeColor = System.Drawing.Color.FromName("#330099");
                            fileGridDet.Columns[4].ItemStyle.ForeColor = System.Drawing.Color.White;
                        }
                    }
                }
            }

            lbHora.Text = DateTime.Now.ToString();
            lbHora0.Text = DateTime.Now.ToString();
        }

        protected void btnInvertirSeleccion_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow gvr in gridPedido.Rows)
            {
                CheckBox cb = (CheckBox)gvr.FindControl("cbFunda");
                if (cb.Checked)
                {
                    cb.Checked = false;
                }
                else
                {
                    cb.Checked = true;
                }
            }

        }

        protected void btnEliminarDesMarcados_Click(object sender, EventArgs e)
        {
            int jj = 0;
            wsPedidosFundas.PedidoFundas[] q = new wsPedidosFundas.PedidoFundas[1];

            foreach (GridViewRow row in gridPedido.Rows)
            {
                CheckBox cb = (CheckBox)row.FindControl("cbFunda");

                if (cb.Checked)
                {
                    jj++;
                    Label lbCodigo = (Label)row.FindControl("Label1");
                    Label lbDescripcion = (Label)row.FindControl("Label2");
                    Label lbColchones = (Label)row.FindControl("Label3");
                    Label lbBases = (Label)row.FindControl("Label4");
                    Label lbFundas = (Label)row.FindControl("Label5");
                    Label lbPedidoColchones = (Label)row.FindControl("Label6");
                    Label lbPedidoFundas = (Label)row.FindControl("Label7");
                    Label lbCodigoArticulo = (Label)row.FindControl("Label8");

                    wsPedidosFundas.PedidoFundas item = new wsPedidosFundas.PedidoFundas();
                    item.Articulo = lbCodigo.Text;
                    item.Descripcion = lbDescripcion.Text;
                    item.Colchon = lbColchones.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbColchones.Text);
                    item.Base = lbBases.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbBases.Text);
                    item.Funda = lbFundas.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbFundas.Text);
                    item.PedidoColchon = lbPedidoColchones.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbPedidoColchones.Text);
                    item.PedidoFunda = lbPedidoFundas.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(lbPedidoFundas.Text);
                    item.CodigoArticulo = lbCodigoArticulo.Text;

                    if (jj > 1) Array.Resize(ref q, jj);
                    q[jj - 1] = item;

                    Label1.Text = "";
                    Label2.Text = "";

                    Label8.Text = string.Format("El pedido consta de {0} artículos.", q.Length);
                    Label9.Text = string.Format("El pedido consta de {0} artículos.", q.Length);

                    gridPedido.DataSource = q;
                    gridPedido.DataBind();

                    Session["q"] = q;
                    gridPedido.SelectedIndex = -1;

                    SetToolTips();
                }
            }
        }

    }

}