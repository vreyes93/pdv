USE EXACTUSERP
GO

-- Esta tabla contendr� la relaci�n entre los art�culos de las fundas con los c�digos de los marcos y los colchones.
CREATE TABLE prodmult.MF_ArticulosRelacion (
	Articulo			VARCHAR(20)				NOT NULL,
	ArticuloRelacion	VARCHAR(20)				NOT NULL,
	Descripcion			VARCHAR(100)				NULL,
	RecordDate			CurrentDateType			NOT NULL,
	CreatedBy			UsernameType			NOT NULL
)
GO

ALTER TABLE prodmult.MF_ArticulosRelacion ADD PRIMARY KEY ( Articulo, ArticuloRelacion )
GO


-- Esta tabla almacenar� el encabezado del sugerido de pedido
CREATE TABLE prodmult.MF_PedidoCamas (
	Pedido				INT						NOT NULL,
	Descripcion			VARCHAR(100)				NULL,
	Fecha				DATETIME				NOT NULL,
	Bodega				VARCHAR(4)				NOT NULL,
	Estatus				CHAR(1)					NOT NULL,
	RecordDate			CurrentDateType			NOT NULL,
	CreatedBy			UsernameType			NOT NULL
)
GO

ALTER TABLE prodmult.MF_PedidoCamas ADD PRIMARY KEY ( Pedido )
GO

-- Esta tabla almacenar� el detalle del sugerido de pedido
CREATE TABLE prodmult.MF_PedidoCamasDet (
	Pedido				INT						NOT NULL,
	PedidoDet			INT						NOT NULL,
	Articulo			VARCHAR(20)				NOT NULL,
	ExistenciasColchon	INT						NOT NULL,
	ExistenciasBases	INT						NOT NULL,
	ExistenciasFundas	INT						NOT NULL,
	PedidoColchon		INT						NOT NULL,
	PedidoFundas		INT						NOT NULL
)
GO

ALTER TABLE prodmult.MF_PedidoCamasDet ADD PRIMARY KEY ( Pedido, PedidoDet )
GO

ALTER TABLE [prodmult].[MF_PedidoCamasDet]  WITH CHECK ADD  CONSTRAINT [FK_MF_PedidoCamasDet_MF_PedidoCamas] FOREIGN KEY([Pedido])
REFERENCES [prodmult].[MF_PedidoCamas] ([Pedido])
GO
ALTER TABLE [prodmult].[MF_PedidoCamasDet] CHECK CONSTRAINT [FK_MF_PedidoCamasDet_MF_PedidoCamas]
GO


--Cursor de Fundas
--La funci�n de este cursor es crear una relaci�n autom�tica entre las fundas y su respectiva base.
DECLARE @Funda AS VARCHAR(20)
DECLARE @Base AS VARCHAR(20)

DELETE FROM prodmult.MF_ArticulosRelacion
WHERE ArticuloRelacion LIKE '142%'

DECLARE #CursorFundas CURSOR LOCAL FOR
SELECT ARTICULO FROM prodmult.ARTICULO WHERE ARTICULO LIKE '140%' ORDER BY ARTICULO 

OPEN #CursorFundas
FETCH NEXT FROM #CursorFundas INTO @Funda
	
WHILE @@FETCH_STATUS = 0
BEGIN
	SET @Base = '142' + SUBSTRING(@Funda, 4, 7)
	
	IF (SELECT COUNT(*) FROM prodmult.ARTICULO WHERE ARTICULO = @Base) > 0
	BEGIN
		INSERT INTO prodmult.MF_ArticulosRelacion ( Articulo, ArticuloRelacion, Descripcion, CreatedBy, RecordDate )
		VALUES ( @Funda, @Base, 'Relaci�n Autom�tica Funda-Base', 'rolando.pineda', GETDATE() )
	END
	
	--Fetch Cursor de Fundas
	FETCH NEXT FROM #CursorFundas INTO @Funda
END
CLOSE #CursorFundas
DEALLOCATE #CursorFundas
GO


