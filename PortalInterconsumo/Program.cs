﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace PortalInterconsumo
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] file)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());

            string mLinea; string mUrl = "";
            StreamReader streamFile = new StreamReader(file[0]);
            
            while ((mLinea = streamFile.ReadLine()) != null)
            {
                string[] mData = mLinea.Split(new string[] { " " }, StringSplitOptions.None);
                if (mUrl.Trim().Length == 0) mUrl = mData[0];
            }
            streamFile.Close();
            
            Process p = new Process();
            ProcessStartInfo psi = new ProcessStartInfo("iexplore.exe");
            psi.Arguments = mUrl;
            p.StartInfo = psi;
            p.Start();

            Application.Exit();
        }
    }
}
