﻿using MF_Clases;
using System;
using System.Web.Services;
using MF.Seguridad;

namespace PuntoDeVenta
{
    public partial class Authenticate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string token = Request.QueryString["Access"];
            Autenticacion auth = new Autenticacion();

            if (string.IsNullOrEmpty(token) || string.IsNullOrWhiteSpace(token))
            {
                /*System.Web.HttpCookie mf = Request.Cookies[General.CookieName],
                                        tienda = Request.Cookies["Tienda"];

                if (mf != null || (tienda == null || (string.IsNullOrEmpty(tienda.Value) || string.IsNullOrWhiteSpace(tienda.Value))))
                {
                    mf.Expires = DateTime.Now.AddDays(-2);
                    tienda.Expires = DateTime.Now.AddDays(-2);
                    Response.Cookies.Add(mf);
                    Response.Cookies.Add(tienda);

                    Session.Abandon();
                }*/
                auth.Cookie(token, true);
                Autenticacion.CookieTienda(true);
                Session.Clear();

                Response.Redirect("~/");
            }
            else
                auth.Cookie(token, false);
        }

        [WebMethod]
        public static bool CheckCookie()
        {
            return MF.Seguridad.Autenticacion.CheckCookie();
        }
    }
}