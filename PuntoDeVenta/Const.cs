﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuntoDeVenta
{
    public class Const
    {

        public const string Ambiente = "Ambiente";
        public const string Usuario = "Usuario";
        public const string Tienda = "Tienda";
        public const string Vendedor = "Vendedor";
        public const string NombreVendedor = "NombreVendedor";
        public const string LogHabilitado = "LogHabilitado";
    }
}