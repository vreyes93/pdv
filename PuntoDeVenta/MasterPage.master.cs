using MF_Clases;
using PuntoDeVenta;
using System;

public partial class MasterPage : System.Web.UI.MasterPage
{
    public string UrlPrecio { get; } = General.PrecioLista;
    public string UrlEtiqueta { get; } = General.Etiquetas;
    public string UrlJefes { get; } = General.Jefes;
    public string UrlSupervisores { get; } = General.Supervisores;
    public string UrlRecursos { get; } = General.Recursos;
    public string UrlRecibosAsociar { get; } = General.RecibosAsociar;
    public string UrlContabilidad { get; } = General.Contabilidad;

    public string UrlMercadeo { get; }= General.Mercadeo;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["Tienda"] == null)
                Session["Tienda"] = Request.Cookies["Tienda"].Value;
        }
        catch {
            Session["Tienda"] = "F40";
        }
        Tools.IconHandler(this, Session["Tienda"].ToString());

        if ((Request.FilePath.Contains("administracion") || Request.FilePath.Contains("bodega") || Request.FilePath.Contains("contabilidad") ||
            Request.FilePath.Contains("crediplus") || Request.FilePath.Contains("despachos") || Request.FilePath.Contains("friedman") ||
            Request.FilePath.Contains("gerencia") || Request.FilePath.Contains("it") || Request.FilePath.Contains("jefe") ||
            Request.FilePath.Contains("perfil") || Request.FilePath.Contains("recibos") || Request.FilePath.Contains("utilitarios")) &&
            !((bool)Session["Invitado"]))
            Tools.SubmenuHandler(this);
    }
}
