﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using DevExpress.Web;

public enum DemoExportFormat { Pdf, Xls, Xlsx, Rtf, Csv, Com, Tien, Fac, Exc, Anu, Pend, All, Art, SinPre, Mues, Pre, PlantOfe, Ofe, OfeVig, OfePend, UndoOfe, ArtMay, May, LVPlantilla, LVCargarNotas, LVExcel, LVAsl, UndoNotas, GFace, PlantExencion, CargaExencion, PlantAnul, CargaAnul, AntSaldos, CargarAntSaldos, AntMay ,LIXls,LIPdf}

public partial class ToolbarExport : UserControl
{
    Dictionary<DemoExportFormat, string> itemIcons;
    DemoExportFormat[] exportItemTypes;
    static readonly object EventItemClick = new object();
    public delegate void ExportItemClickEventHandler(object source, ExportItemClickEventArgs e);
    
    [TypeConverter(typeof(EnumConverter))]
    public DemoExportFormat[] ExportItemTypes
    {
        get
        {
            if (exportItemTypes == null)
                exportItemTypes = Enum.GetValues(typeof(DemoExportFormat)).Cast<DemoExportFormat>().ToArray();
            return exportItemTypes;
        }
        set { exportItemTypes = value; }
    }
    public bool IsDataAwareXls { get; set; }
    public bool IsDataAwareXlsx { get; set; }
    public event ExportItemClickEventHandler ItemClick
    {
        add { Events.AddHandler(EventItemClick, value); }
        remove { Events.RemoveHandler(EventItemClick, value); }
    }
    Dictionary<DemoExportFormat, string> ItemIcons
    {
        get
        {
            if (itemIcons == null)
            {
                itemIcons = new Dictionary<DemoExportFormat, string>();
                FillItemIcons();
            }
            return itemIcons;
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        foreach (var type in ExportItemTypes)
            CreateMenuItem(type);

            //if (type == DemoExportFormat.Pdf || type == DemoExportFormat.Xlsx || type == DemoExportFormat.Csv || type == DemoExportFormat.Xls) CreateMenuItem(type);
    }
    void CreateMenuItem(DemoExportFormat type)
    {
        var item = new MenuItem(string.Empty, type.ToString());
        MenuExportButtons.Items.Add(item);
        if (ItemIcons.ContainsKey(type))
            item.Image.IconID = ItemIcons[type];
        item.ToolTip = GetItemToolTip(type);
    }
    string GetItemToolTip(DemoExportFormat type)
    {
        var result = "Exportar a " + type.ToString();
        if ((IsDataAwareXls && type == DemoExportFormat.Xls) || (IsDataAwareXlsx && type == DemoExportFormat.Xlsx))
            result += " (DataAware)";

        if (type == DemoExportFormat.Xls) result = "Enviar correo a tiendas";
        if (type == DemoExportFormat.Com) result = "Exportar a Excel las comisiones";
        if (type == DemoExportFormat.Tien) result = "Exportar a Excel el resumen por tienda";
        if (type == DemoExportFormat.Fac) result = "Exporar a Excel las facturas que integran las comisiones";
        if (type == DemoExportFormat.Exc) result = "Exportar a Excel las excepciones";
        if (type == DemoExportFormat.Anu) result = "Exportar a Excel las facturas anuladas";
        if (type == DemoExportFormat.Pend) result = "Exporar a Excel las facturas pendientes de comisión";
        if (type == DemoExportFormat.All) result = "Exportar toda la información en un único archivo de Excel";
        if (type == DemoExportFormat.Art) result = "Descargar reporte de artículos";
        if (type == DemoExportFormat.SinPre) result = "Descargar artículos sin precios base";
        if (type == DemoExportFormat.Pre) result = "Cargar precios base";
        if (type == DemoExportFormat.Ofe) result = "Cargar ofertas";
        if (type == DemoExportFormat.OfeVig) result = "Descargar ofertas vigentes";
        if (type == DemoExportFormat.OfePend) result = "Descargar ofertas pendientes";
        if (type == DemoExportFormat.ArtMay) result = "Descargar artículos de mayoreo";
        if (type == DemoExportFormat.May) result = "Cargar precios de mayoreo";
        if (type == DemoExportFormat.Mues) result = "Descargar plantilla de precios";
        if (type == DemoExportFormat.PlantOfe) result = "Descargar plantilla de ofertas";
        if (type == DemoExportFormat.UndoOfe) result = "Cancelar ofertas";
        if (type == DemoExportFormat.LVPlantilla) result = "Descargar plantilla de notas de crédito y abono";
        if (type == DemoExportFormat.LVCargarNotas) result = "Cargar notas de crédito y abono";
        if (type == DemoExportFormat.LVExcel) result = "Descargar libro de ventas en EXCEL";
        if (type == DemoExportFormat.LVAsl) result = "Descargar libro de ventas en ASL";
        if (type == DemoExportFormat.UndoNotas) result = "Revertir carga de notas";
        if (type == DemoExportFormat.GFace) result = "Descargar documentos para validar GFace";
        if (type == DemoExportFormat.PlantAnul) result = "Descargar plantilla de facturas anuladas";
        if (type == DemoExportFormat.CargaAnul) result = "Cargar facturas anuladas";
        if (type == DemoExportFormat.PlantExencion) result = "Descargar plantilla de exenciones";
        if (type == DemoExportFormat.CargaExencion) result = "Cargar exenciones";
        if (type == DemoExportFormat.AntSaldos) result = "Descargar antigüedad de saldos";
        if (type == DemoExportFormat.CargarAntSaldos) result = "Cargar documentos únicamente al módulo de cuentas por cobrar";
        if (type == DemoExportFormat.AntMay) result = "Antigüedad de saldos de Mayoreo";

        return result;
    }
    void FillItemIcons()
    {
        ItemIcons[DemoExportFormat.Pdf] = "export_exporttopdf_32x32";
        ItemIcons[DemoExportFormat.Xls ] = "mail_sendpdf_32x32";
        ItemIcons[DemoExportFormat.Xlsx] = "export_exporttoxlsx_32x32";
        ItemIcons[DemoExportFormat.Rtf] = "export_exporttortf_32x32";
        ItemIcons[DemoExportFormat.Csv] = "export_exporttocsv_32x32";
        ItemIcons[DemoExportFormat.Com] = "analysis_errorbarspercentage_32x32";
        ItemIcons[DemoExportFormat.Tien] = "actions_expandcollapse_32x32devav";
        ItemIcons[DemoExportFormat.Fac] = "export_exporttoxlsx_32x32gray";
        ItemIcons[DemoExportFormat.Exc] = "export_exporttoxlsx_32x32office2013";
        ItemIcons[DemoExportFormat.Anu] = "export_exporttoxlsx_32x32";
        ItemIcons[DemoExportFormat.Pend] = "save_saveto_32x32office2013";
        ItemIcons[DemoExportFormat.All] = "actions_printsortdesc_32x32devav";
        ItemIcons[DemoExportFormat.Art] = "actions_download_32x32office2013";
        ItemIcons[DemoExportFormat.SinPre] = "arrows_movedown_32x32office2013";
        ItemIcons[DemoExportFormat.Pre] = "businessobjects_bosale_32x32";
        ItemIcons[DemoExportFormat.Ofe] = "actions_costanalysis_32x32devav";
        ItemIcons[DemoExportFormat.PlantOfe] = "arrows_movedown_32x32";
        ItemIcons[DemoExportFormat.OfeVig] = "chart_drilldownonarguments_chart_32x32office2013";
        ItemIcons[DemoExportFormat.OfePend] = "chart_drilldownonseries_pie_32x32";
        ItemIcons[DemoExportFormat.ArtMay] = "actions_download_32x32";
        ItemIcons[DemoExportFormat.May] = "businessobjects_bosaleitem_32x32";
        itemIcons[DemoExportFormat.Mues] = "chart_drilldownonseries_chart_32x32";
        itemIcons[DemoExportFormat.UndoOfe] = "actions_cancel_32x32";
        itemIcons[DemoExportFormat.LVPlantilla] = "actions_download_32x32office2013";
        itemIcons[DemoExportFormat.LVCargarNotas] = "navigation_up_32x32";
        itemIcons[DemoExportFormat.LVExcel] = "arrows_movedown_32x32office2013";
        itemIcons[DemoExportFormat.LVAsl] = "chart_drilldownonseries_chart_32x32";
        itemIcons[DemoExportFormat.UndoNotas] = "actions_cancel_32x32";
        itemIcons[DemoExportFormat.GFace] = "actions_download_32x32";
        itemIcons[DemoExportFormat.PlantAnul] = "arrows_movedown_32x32";
        itemIcons[DemoExportFormat.CargaAnul] = "reports_deletegroupheader_32x32";
        ItemIcons[DemoExportFormat.PlantExencion] = "actions_printsortdesc_32x32devav";
        ItemIcons[DemoExportFormat.CargaExencion] = "actions_printsortasc_32x32devav";
        ItemIcons[DemoExportFormat.AntSaldos] = "actions_printsortdesc_32x32devav";
        ItemIcons[DemoExportFormat.CargarAntSaldos] = "actions_printsortasc_32x32devav";
        ItemIcons[DemoExportFormat.AntMay] = "analysis_errorbarspercentage_32x32";
    }
    protected void MenuExportButtons_ItemClick(object source, MenuItemEventArgs e)
    {
        var handler = (ExportItemClickEventHandler)Events[EventItemClick];
        if (handler != null)
            handler(this, new ExportItemClickEventArgs((DemoExportFormat)Enum.Parse(typeof(DemoExportFormat), e.Item.Name)));
    }
}
public class ItemTooltips : Collection<ItemTooltip>
{
    public ItemTooltips()
        : base()
    {
    }
}
public class ItemTooltip : CollectionItem
{
    public ItemTooltip()
    {
    }
    public ItemTooltip(DemoExportFormat type, string toolTip)
    {
        Type = type;
        ToolTip = toolTip;
    }
    public DemoExportFormat Type { get; set; }
    public string ToolTip { get; set; }
}

public class ExportItemClickEventArgs : EventArgs
{
    public ExportItemClickEventArgs(DemoExportFormat type)
    {
        ExportType = type;
    }
    public DemoExportFormat ExportType { get; set; }
}
public class EnumConverter : StringToObjectTypeConverter
{
    public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
    {
        var items = value.ToString().Split(',');
        var result = new DemoExportFormat[items.Length];
        for (var i = 0; i < items.Length; ++i)
            Enum.TryParse(items[i], out result[i]);
        return result;
    }
}

