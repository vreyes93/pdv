﻿using MF_Clases;
using MF_Clases.Comun;
using MF_Clases.Restful;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using PuntoDeVenta.Util;

namespace PuntoDeVenta
{
    public static class Tools
    {
        public static void IconHandler(System.Web.UI.MasterPage master, string tienda)
        {
            if (string.IsNullOrEmpty(tienda) || string.IsNullOrWhiteSpace(tienda))
                HttpContext.Current.Response.Redirect("~/Authenticate.aspx");

            HttpContext context = HttpContext.Current;

            HtmlImage logo = (HtmlImage)master.FindControl("logo"), logoPruebas = (HtmlImage)master.FindControl("logoPruebas"),
                logoOutlet = (HtmlImage)master.FindControl("logoOutlet");
            HtmlAnchor piePagina = (HtmlAnchor)master.FindControl("piePagina");

            Api api = new Api(General.FiestaNetRestService);
            string apiResponse = api.Process(RestSharp.Method.GET, string.Format("/TiendaEspecificacion/Obtener/{0}/{1}", Convert.ToString(tienda), "VISIBILIDAD"), null);
            TiendaEspecificacion spec = JsonConvert.DeserializeObject<TiendaEspecificacion>(apiResponse);

            bool esPruebas = General.Ambiente == "PRU",
                esOutlet = spec.Valor == "OUTLET";

            logoPruebas.Visible = esPruebas;
            piePagina.Visible = esPruebas;

            logo.Visible = !esPruebas && !esOutlet;
            logoOutlet.Visible = !esPruebas && esOutlet;

            Label lbUsuario = (Label)master.FindControl("lblUsuario");
            if (Convert.ToString(context.Session["NombreVendedor"]) == "Alerta") context.Session["Vendedor"] = null;
            lbUsuario.Text = $"{Convert.ToString(context.Session["NombreVendedor"])}  -  {Convert.ToString(context.Session["Tienda"])}";
        }

        public static void SubmenuHandler(System.Web.UI.MasterPage master)
        {
            HtmlGenericControl submenu = (HtmlGenericControl)master.FindControl("submenu");
            submenu.Visible = true;

            HttpContext context = HttpContext.Current;
            var ws = new wsPuntoVenta.wsPuntoVenta();    
            ws.Url = General.FiestaNETService;

            string mUsuario = Convert.ToString(context.Session["Usuario"]);
            if (ws.AccedeUtilitarios(mUsuario))
            {
                ((HtmlGenericControl)master.FindControl("lkTiposVenta")).Visible = true;
                ((HtmlGenericControl)master.FindControl("lkFinancieras")).Visible = true;
                ((HtmlGenericControl)master.FindControl("lkNivelesPrecio")).Visible = true;
                ((HtmlGenericControl)master.FindControl("lkCrearOfertas")).Visible = true;
                ((HtmlGenericControl)master.FindControl("lkEnviarMails")).Visible = true;
            }

            if (ws.AccedeOpcionesGerencia(mUsuario))
                ((HtmlGenericControl)master.FindControl("lkGerencia")).Visible = true;

            if (ws.AccedeOpcionesBodega(mUsuario))
                ((HtmlGenericControl)master.FindControl("lkBodega")).Visible = true;

            if (ws.AccedeDespachos(mUsuario))
                ((HtmlGenericControl)master.FindControl("lkDespachos")).Visible = true;

            if (ws.AccedeAdministracion(mUsuario))
            {
                ((HtmlGenericControl)master.FindControl("lkAdministracion")).Visible = true;
                ((HtmlGenericControl)master.FindControl("lkEtiqueta")).Visible = true;
            }

            if (ws.AccedeContabilidad(mUsuario))
                ((HtmlGenericControl)master.FindControl("lkContabilidad")).Visible = true;

            if (ws.AccedeIT(mUsuario))
            {
                ((HtmlGenericControl)master.FindControl("lkIT")).Visible = true;
                ((HtmlGenericControl)master.FindControl("lkFriedman")).Visible = true;
                ((HtmlGenericControl)master.FindControl("lkJefes")).Visible = true;
            }

            if (ws.AccedeFriedman(mUsuario))
                ((HtmlGenericControl)master.FindControl("lkFriedman")).Visible = true;

            if (ws.AccedeJefes(mUsuario))
                ((HtmlGenericControl)master.FindControl("lkJefes")).Visible = true;

            if (ws.AccedeCrediplus(mUsuario))
                ((HtmlGenericControl)master.FindControl("lkCrediplus")).Visible = true;

            ((HtmlGenericControl)master.FindControl("lkRecursos")).Visible = Permiso.Validar("TrasladoVendedor");
            ((HtmlGenericControl)master.FindControl("lkVistaContable")).Visible = Permiso.Validar("VistaContabilidad");

            if (ws.AccedeContabilidad(mUsuario))
                ((HtmlGenericControl)master.FindControl("lkAsociarRecibos")).Visible = true;
            ((HtmlGenericControl)master.FindControl("lkMercadeo")).Visible = Permiso.Validar("Mercadeo");
        }
    }
}