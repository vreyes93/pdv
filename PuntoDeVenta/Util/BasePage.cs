﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Net;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using MF.Seguridad;
using MF_Clases;
using Newtonsoft.Json.Linq;

namespace PuntoDeVenta
{
    public abstract class BasePage : Page
    {
        //-----------------------------------------------------------------------------------------------------------------------------------
        #region Variables
        //-----------------------------------------------------------------------------------------------------------------------------------
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected UserInfo MFUser = null;
        #endregion
        //-----------------------------------------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------------------------------------
        #region Métodos de las variables de sesion
        //-----------------------------------------------------------------------------------------------------------------------------------

        private string GetStringValue(string NombreVariable)
        {
            var valor = Convert.ToString(Session[NombreVariable]);
            return (String.IsNullOrEmpty(valor) ? "" : valor);
        }

        //---------------------------------------------------------------------------------------------------
        private Boolean GetBooleanValue(string NombreVariable)
        {
            var valor = Convert.ToString(Session[NombreVariable]);
            return (String.IsNullOrEmpty(valor) ? false : Convert.ToBoolean(valor));
        }

        //---------------------------------------------------------------------------------------------------
        /// <summary>
        /// Obtiene el valor del ambiente donde se está ejecutando la aplicación.
        /// </summary>
        /// <returns>Retorna un string con el nombre del ambiente.</returns>
        public string GetAmbiente()
        {
            return GetStringValue(Const.Ambiente);
        }

        //---------------------------------------------------------------------------------------------------
        /// <summary>
        /// Obtiene el usuario que está ejecuntado la aplicación.
        /// </summary>
        /// <returns>Retorna un string con el usuario.</returns>
        public string GetUsuario()
        {
            return GetStringValue(Const.Usuario);
        }

        //---------------------------------------------------------------------------------------------------
        /// <summary>
        /// Obtiene el código de la tienda del usuario que está ejecutando la aplicación.
        /// </summary>
        /// <returns>Retorna un string con el código de la tienda.</returns>
        public string GetTienda()
        {
            return GetStringValue(Const.Tienda);
        }

        //---------------------------------------------------------------------------------------------------
        /// <summary>
        /// Obtiene el código del vendedor que está ejecutando la aplicación.
        /// </summary>
        /// <returns>Retorna un string con el código del vendedor.</returns>
        public string GetVendedor()
        {
            return GetStringValue(Const.Vendedor);
        }

        //---------------------------------------------------------------------------------------------------
        /// <summary>
        /// Obtiene el nombre del vendedor que está ejecutando la aplicación.
        /// </summary>
        /// <returns>Retorna un string con nombre del vendedor.</returns>
        public string GetNombreVendedor()
        {
            return GetStringValue(Const.NombreVendedor);
        }

        //---------------------------------------------------------------------------------------------------
        /// <summary>
        /// Obtiene el valor del log, si está habilitado o no.
        /// </summary>
        /// <returns>Retorna true si está habilitado el log.</returns>
        public Boolean IsLogEnabled()
        {
            return GetBooleanValue(Const.LogHabilitado);
        }

        public string GetUrlRestService()
        {
            return Convert.ToString(Session["UrlRestServices"]);
        }

        #endregion
        //-----------------------------------------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------------------------------------
        #region Métodos comunes de log
        //-----------------------------------------------------------------------------------------------------------------------------------

        //------------------------------------------------------------------
        /// <summary>
        /// Obtiene el Id del control que realiza el postback
        /// </summary>
        /// <returns>Retorna un string con el nombre del Id.</returns>
        public string GetControlID()
        {
            string ControlID = "";
            Control control = null;

            string controlName = this.Request.Params.Get("__EVENTTARGET");
            if (controlName != null && controlName != string.Empty)
            {
                control = this.FindControl(controlName);
            }
            else
            {
                foreach (string controlButton in this.Request.Form)
                {
                    Control c = this.FindControl(controlButton);
                    if (c is System.Web.UI.WebControls.Button)
                    {
                        control = c;
                        break;
                    }
                }
            }
            if (control != null)
                ControlID = control.ID;

            return ControlID;
        }


        //---------------------------------------------------------------------------------------------------
        /// <summary>
        /// Registra la actividad en el log. La actividad se presenta cuando se hace una petición al servidor.
        /// </summary>
        //---------------------------------------------------------------------------------------------------
        protected void LogActivity(string Method = null)
        {
            // Verificamos si está habilitado el log.
            if (!this.IsLogEnabled()) return;

            string ComponentID = "";
            string Category = "";

            if (!IsPostBack)
            {
                Category = Const.LogCategory.PageAcceded;
                ComponentID = Const.LogCategory.PageAcceded;
            }
            else
            {
                if (!String.IsNullOrEmpty(Method))
                {
                    Category = Const.LogCategory.Method;
                    ComponentID = Method;
                }
                else
                {
                    Category = Const.LogCategory.Control;
                    ComponentID = GetControlID();
                }
            }


            //Ambiente {0}; Tienda: {1}; Usuario: {2}; Vendedor: {3}; Nombre vendedor: {4}; Pagina: {5}; Category: {6}; ComponentId: {7};
            log.Info(string.Format(" {0}; {1}; {2}; {3}; {4}; {5}; {6}; {7};"
                    , GetAmbiente(), GetTienda(), GetUsuario(), GetVendedor(), GetNombreVendedor(), this.Page.ToString(), Category, ComponentID));
        }


        #endregion
        //-----------------------------------------------------------------------------------------------------------------------------------

        #region Servicios
        //-----------------------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------------
        /// <summary>
        /// Obtiene el valor del ambiente donde se está ejecutando la aplicación.
        /// </summary>
        /// <returns>Retorna un string con el nombre del ambiente.</returns>
        public WebRequest GetRequest(string Servicio, string Parametros = "")
        {
            if (!String.IsNullOrEmpty(Parametros))
                return WebRequest.Create(string.Format("{0}/{1}/?{2}", GetUrlRestService(), Servicio, Parametros));

            return WebRequest.Create(string.Format("{0}/{1}", GetUrlRestService(), Servicio));
        }

        public string getService(string Servicio)
        {
            return string.Format("{0}/{1}", GetUrlRestService(), Servicio);
        }


        public Object ConsumiendoServicio(string Servicio, string Method, string id = "", Object Objeto = null, string parameter = "")
        {
            log.Info("Init");
            log.Debug(string.Format("Servicio: {0} Metodo: {1} Id: {2}", Servicio, Method, id));

            string member = "";
            Object result = "";

            //Verificamos si contiene un id para concatenar en la llamada del servicio.
            if (String.IsNullOrEmpty(id)) member = "";
            else member = "/" + id;

            if (Method == Const.ServiceMethod.GET && !String.IsNullOrEmpty(parameter)) member = "?" + parameter;

            //Verificamos el servicio a llamar
            WebRequest request = WebRequest.Create(getService(Servicio) + member);
            //Seteamos el metodo a llamar
            request.Method = Method;
            request.ContentType = "application/json";

            //Verificamos si el objeto es nulo para pasarlo como parametro
            if (Objeto != null)
            {
                string requestData = JsonConvert.SerializeObject(Objeto);
                byte[] data = Encoding.UTF8.GetBytes(requestData);
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(data, 0, data.Length);

                dataStream.Close();
            }


            try
            {

                log.Debug("Calling HhtpWebResponse");
                HttpWebResponse resp = request.GetResponse() as HttpWebResponse;

                if (Method.Equals(Const.ServiceMethod.GET))
                {
                    if (resp.StatusCode == HttpStatusCode.OK)
                    {
                        using (System.IO.Stream respStream = resp.GetResponseStream())
                        {
                            //log.Debug("Calling StremReader");
                            StreamReader reader = new StreamReader(respStream, Encoding.UTF8);
                            //log.Debug("Calling ReadToEnd");
                            result = reader.ReadToEnd();
                            log.Debug("Objeto obtenido exitosamente.");
                        }
                    }
                    else
                    {
                        log.Error("Verificar el error al momento de crear o eliminar el objeto.");
                    }
                }
                if (Method.Equals(Const.ServiceMethod.DELETE))
                {
                    if (resp.StatusCode == HttpStatusCode.OK)
                    {

                        result = Const.ServiceResult.ELIMINADO;
                        log.Debug("Objeto eliminado exitosamente.");

                    }
                    else
                    {
                        result = Const.ServiceResult.ERROR;
                        log.Error("Verificar el error al momento de crear o eliminar el objeto.");
                    }
                }
                if (Method.Equals(Const.ServiceMethod.PUT))
                {
                    if (resp.StatusCode == HttpStatusCode.NoContent || resp.StatusCode == HttpStatusCode.OK)
                    {

                        result = Const.ServiceResult.MODIFICADO;
                        log.Debug("Objeto modificado exitosamente.");

                    }
                    else
                    {
                        result = Const.ServiceResult.ERROR;
                        log.Error("Verificar el error al momento de modificar el objeto.");
                    }
                }
                if (Method.Equals(Const.ServiceMethod.POST))
                {
                    if (resp.StatusCode == HttpStatusCode.Created || resp.StatusCode == HttpStatusCode.OK)
                    {

                        result = Const.ServiceResult.CREADO;
                        log.Debug("Objeto creado exitosamente.");
                    }
                    else
                    {
                        result = Const.ServiceResult.ERROR;
                        log.Error("Verificar el error al momento de crear el objeto.");
                    }
                }

            }
            catch (WebException e)
            {
                using (WebResponse response2 = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response2;
                    log.Error(string.Format("Error code: {0}", httpResponse.StatusCode));
                    using (Stream data = response2.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {
                        string text = reader.ReadToEnd();
                        log.Error(text);
                        if (httpResponse.StatusCode == HttpStatusCode.BadRequest)
                        {
                            result = text;
                            return result;
                        }
                    }
                }
            }
            log.Info("Finalize");
            return result;
        }


        #endregion

        protected override void OnPreInit(EventArgs e)
        {
            Autenticacion auth = new Autenticacion();

            if (Request.Cookies[General.CookieName] == null || Session["UserInfo"] == null)
            {
                try
                {
                    if (excludedPages(Context.Request.Url))
                    {
                        Session["Invitado"] = true;
                    }
                    else
                    {
                        Session["Invitado"] = false;
                        auth.login();
                        MFUser = Session["UserInfo"] == null ? null : JsonConvert.DeserializeObject<UserInfo>(Session["UserInfo"].ToString());

                        InitLoad();

                        Session["Usuario"] = MFUser.ExactusUsername.ToUpper();

                        Session["Tienda"] = MFUser.Store;
                        Autenticacion.CookieTienda(false);

                        Session["Vendedor"] = MFUser.Seller;
                        Session["NombreVendedor"] = MFUser.CompleteName;
                        Session["Cliente"] = null;
                        Session["Pedido"] = null;
                        Session["InfoCambio"] = null;
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message == "Token Expiró")
                        auth.login();
                }
            }
            else
            {
                auth.UpdateCookie();
            }
            
            base.OnPreInit(e);
        }

        private void InitLoad()
        {
            Session["Ambiente"] = General.Ambiente;
            Session["Usuario"] = null; Session["Tienda"] = null; Session["Vendedor"] = null; Session["NombreVendedor"] = null; Session["Cliente"] = null;
            Session["Pedido"] = null; Session["TipoVenta"] = null; Session["Financiera"] = null; Session["NivelPrecio"] = null; Session["dsArticulos"] = null;
            Session["dsFacturas"] = null; Session["dsRutas"] = null; Session["dsComisiones"] = null; Session["ExistenciasReservas"] = null;

            Session["UrlFiestaNetERP"] = "http://localhost:50585";
            if (Convert.ToString(Session["Ambiente"]) == "PRO") Session["UrlFiestaNetERP"] = "https://fiestanet.mueblesfiesta.com/mfERP";
            if (Convert.ToString(Session["Ambiente"]) == "PRU") Session["UrlFiestaNetERP"] = "https://fiestanet.mueblesfiesta.com/mfERP";

            Session["Url"] = string.Format("http://sql.fiesta.local/services/wsPuntoVenta.asmx", Request.Url.Host);
            Session["UrlPrecios"] = string.Format("http://sql.fiesta.local/services/wsCambioPrecios.asmx", Request.Url.Host);

            if (Convert.ToString(Session["Ambiente"]) == "PRU") Session["Url"] = string.Format("http://sql.fiesta.local/servicesPruebas/wsPuntoVenta.asmx", Request.Url.Host);
            if (Convert.ToString(Session["Ambiente"]) == "PRU") Session["UrlPrecios"] = string.Format("http://sql.fiesta.local/servicesPruebas/wsCambioPrecios.asmx", Request.Url.Host);

            Session["UrlRestServices"] = "http://localhost:53874/api";
            if (Convert.ToString(Session["Ambiente"]) == "PRO") Session["UrlRestServices"] = string.Format("http://sql.fiesta.local/RestServices/api", Request.Url.Host);
            if (Convert.ToString(Session["Ambiente"]) == "PRU") Session["UrlRestServices"] = string.Format("http://sql.fiesta.local/RestServicesPruebas/api", Request.Url.Host);


            WebRequest request = WebRequest.Create(string.Format("{0}/ActivarLog4Net/", Convert.ToString(Session["UrlRestServices"])));

            request.Method = "GET";
            request.ContentType = "application/json";

            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            dynamic mRespuesta = JObject.Parse(responseString);
            Session["LogHabilitado"] = Convert.ToBoolean(mRespuesta.log4net);

            ViewState["url"] = Convert.ToString(Session["Url"]);
        }

        private bool excludedPages(Uri url)
        {
            List<string> urls = new List<string>() { "/gerencia.aspx", "/solicitudDesarmado.aspx" };
            List<string> parameters = Context.Request.Params.AllKeys.ToList();

            if (url.AbsolutePath == "/solicitudDesarmado.aspx" ||
                url.AbsolutePath == "/solicitudCoti.aspx")
            {
                return parameters.Contains("proc") &&
                       parameters.Contains("sol") &&
                       parameters.Contains("g") &&
                       parameters.Contains("amb");
            }

            return urls.Contains(url.AbsolutePath);
        }
    }
}