﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuntoDeVenta
{
    ///<summary>
    ///Constantes de ambiente, log.
    ///</summary>
    ///<remarks>
    ///Una clase donde están todas las variables a utilizar y que no cambian.
    ///</remarks>
    public class Const
    {

        public const string Ambiente = "Ambiente";
        public const string Usuario = "Usuario";
        public const string Tienda = "Tienda";
        public const string Vendedor = "Vendedor";
        public const string NombreVendedor = "NombreVendedor";
        public const string LogHabilitado = "LogHabilitado";

        ///<summary>
        ///Clase de las constantes del ambiente
        ///</summary>
        ///<remarks>
        ///Los ambientes se utilizan para las configuraciones del sistema.
        ///</remarks>
        public class Environment
        {
            public const string PRODUCCION = "PRO";
            public const string PRUEBAS = "PRU";
            public const string DESARROLLO = "DEV";
            public const string LOCAL = "LOC";
        }

        ///<summary>
        ///Clase de las constantes del ambiente
        ///</summary>
        ///<remarks>
        ///Los ambientes se utilizan para las configuraciones del sistema.
        ///</remarks>
        public class CATALOGO
        {
            public const int TIPO_DE_SERVICIO_ARMADO_TIENDA = 35;
            public const int TIPO_DE_SERVICIO_ARMADO_PEDIDOS = 36;
        }

        ///<summary>
        ///Clase de las constantes de la categoria del Log.
        ///</summary>
        ///<remarks>
        ///Las categorias pueden ser de acceso a pagina, metodo o control.
        ///</remarks>
        public class LogCategory
        {
            public const string PageAcceded = "PageAcceded";
            public const string Method = "Method";
            public const string Control = "Control";

        }

        public class Servicios
        {

            public const string FacturasComisiones = "FacturasComisiones";
        }

        public class ServiceMethod
        {
            public const string GET = "GET";
            public const string PUT = "PUT";
            public const string POST = "POST";
            public const string DELETE = "DELETE";
        }

        public class ServiceResult
        {
            public const string CREADO = "CREADO";
            public const string MODIFICADO = "MODIFICADO";
            public const string ELIMINADO = "ELIMINADO";
            public const string ERROR = "ERROR";
        }

        public class ExpedienteFinanciera
        {
            public const string ANULADO = "A";
            public const string DESEMBOLSADO = "D";
            public const string ENVIADO_FINANCIERA = "E";
            public const string INGRESADO_SISTEMA = "I";
            public const string RECHAZADO = "R";
            public const string ELIMINADO = "D";
        }
        public class DocumentoInterconsumo
        {
            public const string PAGARE = "PAGARE";
            public const string SOLICITUD = "SOLICITUD";
        }

        public class Comisiones
        {
            public const string FRIEDMAN = "F";
            public const string CALENDARIO = "C";
        }


        public class MODULO
        {
            public const string INTERCONSUMO_PAGARE = "HABILITAR_INTERCONSUMO_PAGARE";
            public const string BANCREDIT_PAGARE = "HABILITAR_BANCREDIT_PAGARE";
        }

        public class FINANCIERA
        {
            public const string INTERCONSUMO = "7";
            public const string BANCREDIT = "11";
        }
        public class CUPON
        {
            public const string VALIDAR_CUPON = "Coupon/CheckStatus";
            public const string CANJEAR_CUPON = "Coupon/ChangeStatus";
            public const string RESPUESTA_SIN_CANJEAR = "1";
            public const string RESPUESTA_CANJEO_EXITOSO = "1";

        }

    }
}