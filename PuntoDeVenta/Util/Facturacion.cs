﻿using MF.Comun.Dto.Facturacion;
using MF_Clases;
using MF_Clases.Comun;
using MF_Clases.Facturacion;
using MF_Clases.Restful;
using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using PuntoDeVenta.wsPuntoVenta;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI.WebControls;

namespace PuntoDeVenta.Util
{
    public static class Facturacion
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static Respuesta ObtenerDatosFacturacion(string Cobrador, string CodArticulo, int NivelPrecio, string Financiera, decimal PrecioVendedor)
        {
            Api api = new Api(General.NetunimService);
            Respuesta resp = new Respuesta();
            resp.Exito = true;

            Dictionary<string, string> header = new Dictionary<string, string>();
            header.Add("AppKey", General.AppkeyAutenticacion.ToString());
            header.Add("Token", HttpContext.Current.Request.Cookies.Get(General.CookieName)?.Value);

            try
            {
                if (Financiera == "5" || Financiera == "9")
                    Financiera = "1";
                string endpoint = $"/PrecioLista/Individual/Financiera/{Financiera}/Plazos/{NivelPrecio}/Producto/{CodArticulo}/Tienda/{Cobrador}/Monto/{PrecioVendedor}";
                log.Info(endpoint);

                IRestResponse response = api.Process(Method.GET, endpoint, null, header);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    NuevoPrecioProducto data = JsonConvert.DeserializeObject<NuevoPrecioProducto>(response.Content);
                    data.price = data.originalPrice;
                    resp.Objeto = data;
                    resp.Exito = true;
                }
                else
                {
                    resp.Exito = false;
                    resp.Mensaje = response.Content;
                }
            }
            catch (Exception ex)
            {
                resp.Exito = false;
                resp.Mensaje = ex.Message;
            }

            return resp;
        }

        public static bool EsDecoracion(string codArticulo)
        {
            bool EsDecoracion = false;
            string mUrlRestServices = MF_Clases.General.FiestaNetRestService;
            WebRequest request2 = WebRequest.Create(string.Format("{0}/Clasificacion/{1}", mUrlRestServices, codArticulo));
            request2.Method = "GET";
            request2.ContentType = "application/json";
            var response2 = (HttpWebResponse)request2.GetResponse();
            var responseString2 = new StreamReader(response2.GetResponseStream()).ReadToEnd();
            EsDecoracion = JsonConvert.DeserializeObject<bool>(responseString2);
            return EsDecoracion;
        }

        public static bool ValidarArticuloRegaloAccesorio(List<MF_Clases.Facturacion.NuevoPrecioProducto> particulosActuales, MF_Clases.Facturacion.NuevoPrecioProducto pArticuloAgregar, decimal pfactor)
        {
            bool blOk = true;
            DescuentosArticulos descuentosArticulos = null;
            #region "anterior"
            //bool ArticulosActualesOferta = false;
            //bool HayArticuloEnOferta =  articulosActuales.Count()>0 ? (articulosActuales.Any(x =>  x.originalDiscountPrice != x.originalPrice) || ArticuloAgregar.originalDiscountPrice != ArticuloAgregar.originalPrice)
            //                                            : (ArticuloAgregar.originalDiscountPrice != ArticuloAgregar.originalPrice);

            //ArticulosActualesOferta = articulosActuales.Count() > 0 && (articulosActuales.Any(x => x.originalDiscountPrice != x.originalPrice && x.originalDiscountPrice <= x.price));


            //if (HayArticuloEnOferta && (ArticuloAgregar.price != ArticuloAgregar.originalPrice)) //solo si se está aplicando a un descuento
            //{
            //    decimal TotalDescuento = articulosActuales.Count() > 0 && ArticulosActualesOferta ? articulosActuales.Sum(x => ((x.originalPrice / factor) - (x.originalDiscountPrice / factor))) + (ArticuloAgregar.originalPrice / factor) - (ArticuloAgregar.originalDiscountPrice / factor)
            //                                            : (ArticuloAgregar.originalPrice / factor) - (ArticuloAgregar.originalDiscountPrice / factor);

            //    decimal DescuentoUtilizado = articulosActuales.Count() > 0 && ArticulosActualesOferta ? articulosActuales.Sum(x => (x.originalPrice / factor - (x.price / factor))) + (ArticuloAgregar.originalPrice / factor) - ((ArticuloAgregar.price / factor))
            //                              : (ArticuloAgregar.originalPrice / factor) - ((ArticuloAgregar.price / factor));



            //    //decimal TotalAccesorios = articulosActuales.Where(x => x.EsDecoracion).Sum(x => x.originalPrice / factor);
            //    if ((TotalDescuento) - DescuentoUtilizado < 0)
            //        blOk = false;
            //}
            #endregion
            try
            {
                Api api = new Api(General.FiestaNetRestService);
                descuentosArticulos = new DescuentosArticulos { articulosActuales = particulosActuales, ArticuloAgregar = pArticuloAgregar, factor = pfactor };
                blOk = JsonConvert.DeserializeObject<bool>(api.Process(Method.POST, "/DescuentosArticulos/ValidarDescuentoArticulo", descuentosArticulos));

            }
            catch (Exception ex)
            {
                string json = JsonConvert.SerializeObject(descuentosArticulos);
                log.Debug("JSON ARTICULOS VALIDAR: " + json);
                log.Error("Error al validar los descuentos " + ex.Message + " " + ex.InnerException);
                blOk = false;
            }
            return blOk;
        }
        public static PromocionRegistro CalcularDescuentoVales(PromocionRegistro pedido,ref string msgError)
        {
            PromocionRegistro respuesta = new PromocionRegistro();
           
            PromocionRegistro resp = new PromocionRegistro();
            try
            {
               
                Api api = new Api(General.FiestaNetRestService);
                string json = JsonConvert.SerializeObject(pedido);
                respuesta = JsonConvert.DeserializeObject<PromocionRegistro>(api.Process(Method.POST, "/ValidarDescuentos/", pedido));
                
            }
            catch (Exception ex)
            {
                msgError = "PROBLEMA AL VALIDAR EL DESCUENTO PARA APLICAR LOS VALES error:" + ex.Message + "  " + ex.InnerException;
                log.Error(msgError);
            }
            return respuesta;
        }

        public static MF_Clases.Facturacion.NuevoPrecioProducto ValidarPorcentajeDescuento(MF_Clases.Facturacion.NuevoPrecioProducto pArticuloAgregar)
        {
            decimal porcDescuento = (pArticuloAgregar.originalPrice - pArticuloAgregar.price) * 100 / pArticuloAgregar.originalPrice;
            decimal factor = (decimal)pArticuloAgregar.factor;
            porcDescuento = Math.Round(porcDescuento, 6, MidpointRounding.AwayFromZero);
            if ((pArticuloAgregar.originalPrice * porcDescuento / 100) > (pArticuloAgregar.originalPrice - pArticuloAgregar.price))
                pArticuloAgregar.price = Math.Round(pArticuloAgregar.originalPrice * ((1 - porcDescuento / 100)), 2, MidpointRounding.AwayFromZero);
            return pArticuloAgregar;
        }
        public static decimal ObtenerSaldoDescuento(List<MF_Clases.Facturacion.NuevoPrecioProducto> articulosActuales)
        {
            decimal SaldoDescuento = 0;
            if (articulosActuales != null)
            {
                decimal precio = articulosActuales.Sum(x => x.price);
                decimal TotalDescuento = articulosActuales.Sum(x => (x.originalPrice) - ((x.price)));
                SaldoDescuento = Math.Round(articulosActuales.Sum(x => (x.originalPrice / (decimal)x.factor) - (x.originalDiscountPrice / (decimal)x.factor)) - TotalDescuento, 2, MidpointRounding.ToEven);
            }
           
            return SaldoDescuento;
        }
        public static bool AplicaPromocion(ref List<MF_Clases.Comun.PedidoLinea> qLinea, ref string mPrecioBien,
            ref string mMonto, ref string mDescuento, string NivelPrecio
            , ref string mMensaje, ref string mNotas, string mTipoVenta,int pFinanciera, string ValePromocion, string Cobrador, string Vendedor,string Cliente)
        {
            Api api = new Api(General.FiestaNetRestService);
            PromocionRegistro promocion = new PromocionRegistro
            {
                TipoVenta = mTipoVenta,
                articulos = qLinea,
                nivelPrecio = NivelPrecio,
                Financiera = pFinanciera,
                notas = mNotas,
                cliente=Cliente
            };
            try
            {
                PromocionRegistro promocionesAplicadas = JsonConvert.DeserializeObject<PromocionRegistro>(api.Process(Method.POST, "/Promociones", promocion));
                mPrecioBien = String.Format("{0:0,0.00}", promocionesAplicadas.TotalFacturar);
                mMonto = String.Format("{0:0,0.00}", promocionesAplicadas.Monto);
                mDescuento = String.Format("{0:0,0.00}", promocionesAplicadas.Descuento);
                mMensaje = promocionesAplicadas.mensaje;
                mNotas = promocionesAplicadas.notas;
                qLinea = promocionesAplicadas.articulos;
                if (promocionesAplicadas.Monto == 0 && mMensaje != "")
                    return false;
            }
            catch (Exception ex)
            {
                mMensaje = ex.Message;
                return false;
            }
            return true;
        }

    }
}