﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
using MF_Clases;
using MF_Clases.Restful;
using Newtonsoft.Json;
using RestSharp;

namespace PuntoDeVenta.Util
{
    public static class Permiso
    {
        private static Api Api => new Api(General.ApiAutenticacion);

        private static string Token => HttpContext.Current.Request.Cookies.Get(General.CookieName)?.Value;

        public static bool Validar(string opcion)
        {
            Dictionary<string, string> header = new Dictionary<string, string>();
            header.Add("AppKey", General.AppkeyAutenticacion.ToString());
            header.Add("Token", Token);

            string endpoint = General.EndpointValidarPermiso.Replace("{0}", opcion);
            IRestResponse response = Api.Process(Method.GET, endpoint, null, header);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new Exception(response.Content);

            return JsonConvert.DeserializeObject<bool>(response.Content);
        }
    }
}