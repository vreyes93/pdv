﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;

namespace PuntoDeVenta
{

        public class Bancredit
        {


            public int getFechaCorte(DateTime Fecha)
            {
                return (Fecha.Day < 15) ? 15 : 30;
            }

            public DateTime getFechaPrimerPago(DateTime Fecha)
            {
                DateTime PrimerPago = new DateTime();

                if (Fecha.Day < 15)
                    PrimerPago = new DateTime(Fecha.Year, Fecha.Month, 15).AddMonths(1);
                else
                {
                    if (Fecha.Month == 1)
                        PrimerPago = new DateTime(Fecha.Year, Fecha.Month, 1).AddMonths(2).AddDays(-1);
                    else
                        PrimerPago = new DateTime(Fecha.AddMonths(1).Year, Fecha.AddMonths(1).Month, 30);
                }

                return PrimerPago;
            }

            public DateTime getFechaSegundoPago(DateTime Fecha)
            {
                DateTime PrimerPago = new DateTime();

                if (Fecha.Day < 15)
                    PrimerPago = new DateTime(Fecha.Year, Fecha.Month, 15).AddMonths(2);
                else
                {
                    if (Fecha.Month == 12)
                        PrimerPago = new DateTime(Fecha.Year, Fecha.Month, 1).AddMonths(3).AddDays(-1);
                    else
                        PrimerPago = new DateTime(Fecha.AddMonths(2).Year, Fecha.AddMonths(2).Month, 30);
                }

                return PrimerPago;
            }



            public DateTime getFechaMesPago(DateTime Fecha, int mes)
            {
                DateTime PrimerPago = new DateTime();

                if (Fecha.Day < 15)
                    PrimerPago = new DateTime(Fecha.Year, Fecha.Month, 15).AddMonths(mes);
                else
                {
                    //Caso especial para febrero
                    if (Fecha.AddMonths(mes).Month == 2)
                        PrimerPago = new DateTime(Fecha.Year, Fecha.Month, 1).AddMonths(mes + 1).AddDays(-1);
                    else
                        PrimerPago = new DateTime(Fecha.AddMonths(mes).Year, Fecha.AddMonths(mes).Month, 30);
                }

                return PrimerPago;
            }

        public DateTime getUltimaFecha(DateTime Fecha, int Cuotas)
        {

            DateTime UltimoPago = new DateTime();

            if (Fecha.Day < 15)
                UltimoPago = new DateTime(Fecha.Year, Fecha.Month, 15).AddMonths(Cuotas);
            else
            {
                //Caso especial para febrero
                if (Fecha.AddMonths(Cuotas).Month == 2)
                    UltimoPago = new DateTime(Fecha.Year, Fecha.Month, 1).AddMonths(Cuotas + 1).AddDays(-1);
                else
                    UltimoPago = new DateTime(Fecha.AddMonths(Cuotas).Year, Fecha.AddMonths(Cuotas).Month, 30);
            }

            return UltimoPago;

        }

    }

        public class Cast
        {
        public Numalet NumeroLetras = new Numalet();

        public Cast()
        {
            Numalet NumeroLetras = new Numalet();
            NumeroLetras.ApocoparUnoParteEntera = true;
            NumeroLetras.ConvertirDecimales = false;
        }

        private String[] UNIDADES = { "", "uno ", "dos ", "tres ", "cuatro ", "cinco ", "seis ", "siete ", "ocho ", "nueve " };
        private String[] DECENAS = 
                {"diez ", "once ", "doce ", "trece ", "catorce ", "quince ", "dieciséis ","diecisiete ", "dieciocho ", "diecinueve "
                , "veinte " , "veintiuno ", "veintidós ", "veintitrés ", "veinticuatro ", "veinticinco ", "veintiséis ", "veintisiete ", "veintiocho ", "veintinueve "
                , "treinta ", "cuarenta ","cincuenta ", "sesenta ", "setenta ", "ochenta ", "noventa "};
        private String[] CENTENAS = {"", "ciento ", "doscientos ", "trecientos ", "cuatrocientos ", "quinientos ", "seiscientos ",
            "setecientos ", "ochocientos ", "novecientos "};

        private Regex r;

        public String ConvertirNumLetra(String numero, bool mayusculas)
        {

            String literal = "";
            String parte_decimal;
            //si el numero utiliza (.) en lugar de (,) -> se reemplaza
            numero = numero.Replace(".", ",");

            //si el numero no tiene parte decimal, se le agrega ,00
            if (numero.IndexOf(",") == -1)
            {
                numero = numero + ",00";
            }
            //se valida formato de entrada -> 0,00 y 999 999 999,00
            r = new Regex(@"\d{1,9},\d{1,2}");
            MatchCollection mc = r.Matches(numero);
            if (mc.Count > 0)
            {
                //se divide el numero 0000000,00 -> entero y decimal
                String[] Num = numero.Split(',');

                //de da formato al numero decimal
                parte_decimal = Num[1] + "/100 ";
                //se convierte el numero a literal
                if (int.Parse(Num[0]) == 0)
                {//si el valor es cero                
                    literal = "cero ";
                }
                else if (int.Parse(Num[0]) > 999999)
                {//si es millon
                    literal = getMillones(Num[0]);
                }
                else if (int.Parse(Num[0]) > 999)
                {//si es miles
                    literal = getMiles(Num[0]);
                }
                else if (int.Parse(Num[0]) > 99)
                {//si es centena
                    literal = getCentenas(Num[0]);
                }
                else if (int.Parse(Num[0]) > 9)
                {//si es decena
                    literal = getDecenas(Num[0]);
                }
                else
                {//sino unidades -> 9
                    literal = getUnidades(Num[0]);
                }
                //devuelve el resultado en mayusculas o minusculas
                if (mayusculas)
                {
                    return (literal /*+ parte_decimal*/).ToUpper();
                }
                else
                {
                    return (literal /*+ parte_decimal*/);
                }
            }
            else
            {//error, no se puede convertir
                return literal = null;
            }
        }

        /* funciones para convertir los numeros a literales */

        private String getUnidades(String numero)
        {   // 1 - 9            
            //si tuviera algun 0 antes se lo quita -> 09 = 9 o 009=9
            String num = numero.Substring(numero.Length - 1);
            return UNIDADES[int.Parse(num)];
        }

        private String getDecenas(String num)
        {// 99                        
            int n = int.Parse(num);
            if (n < 10)
            {//para casos como -> 01 - 09
                return getUnidades(num);
            }
            else if (n > 29)
            {//para 20...99
                String u = getUnidades(num);
                if (u.Equals(""))
                { //para 20,30,40,50,60,70,80,90
                    return DECENAS[int.Parse(num.Substring(0, 1)) + 17];
                }
                else
                {
                    return DECENAS[int.Parse(num.Substring(0, 1)) + 17] + "y " + u;
                }
            }
            else
            {//numeros entre 11 y 19
                return DECENAS[n - 10];
            }
        }

        private String getCentenas(String num)
        {// 999 o 099
            if (int.Parse(num) > 99)
            {//es centena
                if (int.Parse(num) == 100)
                {//caso especial
                    return " cien ";
                }
                else
                {
                    return CENTENAS[int.Parse(num.Substring(0, 1))] + getDecenas(num.Substring(1));
                }
            }
            else
            {//por Ej. 099 
             //se quita el 0 antes de convertir a decenas
                return getDecenas(int.Parse(num) + "");
            }
        }

        private String getMiles(String numero)
        {// 999 999
         //obtiene las centenas
            String c = numero.Substring(numero.Length - 3);
            //obtiene los miles
            String m = numero.Substring(0, numero.Length - 3);
            String n = "";
            //se comprueba que miles tenga valor entero
            if (int.Parse(m) > 0)
            {
                n = getCentenas(m);
                return n + "mil " + getCentenas(c);
            }
            else
            {
                return "" + getCentenas(c);
            }

        }

        private String getMillones(String numero)
        { //000 000 000        
          //se obtiene los miles
            String miles = numero.Substring(numero.Length - 6);
            //se obtiene los millones
            String millon = numero.Substring(0, numero.Length - 6);
            String n = "";
            if (millon.Length > 1)
            {
                n = getCentenas(millon) + "millones ";
            }
            else
            {
                n = getUnidades(millon) + "millon ";
            }
            return n + getMiles(miles);
        }

        public String ConvertirMesLetra(int mes)
        {
            string mMes = "";

            switch (mes)
            {
                case 1: mMes = "ENERO"; break;
                case 2: mMes = "FEBRERO"; break;
                case 3: mMes = "MARZO"; break;
                case 4: mMes = "ABRIL"; break;
                case 5: mMes = "MAYO"; break;
                case 6: mMes = "JUNIO"; break;
                case 7: mMes = "JULIO"; break;
                case 8: mMes = "AGOSTO"; break;
                case 9: mMes = "SEPTIEMBRE"; break;
                case 10: mMes = "OCTUBRE"; break;
                case 11: mMes = "NOVIEMBRE"; break;
                case 12: mMes = "DICIEMBRE"; break;
                default: mMes = ""; break;
            }
            return mMes;
        }
        public String ConvertirFechaLetra(DateTime Fecha)
        {
            return "" +this.ConvertirNumLetra(Fecha.Day.ToString(),true) + " DE " + ConvertirMesLetra(Fecha.Month) + " DEL AÑO " + this.ConvertirNumLetra(Fecha.Year.ToString(),true);
        }

        public String ConvertirDPILetra(String numero, bool mayusculas)
        {
            int PrimerBloque = Int32.Parse(numero.Substring(0,4));
            int SegundoBloque = Int32.Parse(numero.Substring(4, 5));
            int TercerBloque = Int32.Parse(numero.Substring(9, 4));

            string txtPrimerBloque = "";
            string txtSegundoBloque = "";
            string txtTercerBloque = "";


            if (PrimerBloque.ToString().Length < 4)
            {
                switch (PrimerBloque.ToString().Length)
                {
                    case 1:
                        txtPrimerBloque = "CERO CERO CERO ";
                        break;
                    case 2:
                        txtPrimerBloque = "CERO CERO ";
                        break;
                    case 3:
                        txtPrimerBloque = "CERO ";
                        break;
                }
            }

            txtPrimerBloque = txtPrimerBloque + NumeroLetras.ToCustomLetter(PrimerBloque).ToUpper();

            if (SegundoBloque.ToString().Length < 5)
            {
                switch (SegundoBloque.ToString().Length)
                {
                    case 1:
                        txtSegundoBloque = "CERO CERO CERO CERO ";
                        break;
                    case 2:
                        txtSegundoBloque = "CERO CERO CERO ";
                        break;
                    case 3:
                        txtSegundoBloque = "CERO CERO ";
                        break;
                    case 4:
                        txtSegundoBloque = "CERO ";
                        break;
                }
            }
            txtSegundoBloque = txtSegundoBloque + NumeroLetras.ToCustomLetter(SegundoBloque).ToUpper();

            if (TercerBloque.ToString().Length < 4)
            {
                switch (TercerBloque.ToString().Length)
                {
                    case 1:
                        txtTercerBloque = "CERO CERO CERO ";
                        break;
                    case 2:
                        txtTercerBloque = "CERO CERO ";
                        break;
                    case 3:
                        txtTercerBloque = "CERO ";
                        break;
                }
            }
            txtTercerBloque = txtTercerBloque + NumeroLetras.ToCustomLetter(TercerBloque).ToUpper();


            return txtPrimerBloque + " ESPACIO " + txtSegundoBloque + " ESPACIO " + txtTercerBloque;
        }

        public string ConvertirNumeroDireccionLetras(string Cadena, bool Ordinal = false)
        {

            string mNumero = Regex.Match(Cadena, @"\d+").Value;
            string mNuevacadena = "";

            if (String.IsNullOrEmpty(mNumero))
                return Cadena;
            else
            {
                string mPrimeraParte = "";
                string mSegundaParte = "";

                mPrimeraParte = Cadena.Substring(0, Cadena.IndexOf(mNumero));
                mSegundaParte = Cadena.Substring((Cadena.IndexOf(mNumero) + mNumero.Length), (Cadena.Length - mNumero.Length - Cadena.IndexOf(mNumero)));
                if (Ordinal)
                {
                    this.NumeroLetras.ApocoparUnoParteEntera = true;
                    this.NumeroLetras.ConvertirDecimales = false;
                    this.NumeroLetras._Ordinal = true;
                    mNuevacadena = mPrimeraParte + this.NumeroLetras.ToCustomLetter(mNumero).Trim() + mSegundaParte;

                }
                else
                {
                    this.NumeroLetras.ApocoparUnoParteEntera = true;
                    this.NumeroLetras.ConvertirDecimales = false;
                    this.NumeroLetras._Ordinal = false;
                    mNuevacadena = mPrimeraParte + this.NumeroLetras.ToCustomLetter(mNumero).Trim() + mSegundaParte;
                 }

                return ConvertirNumeroDireccionLetras(mNuevacadena);
            }
        }


        public string CardinalEnLetras(string num)
        {
            string res, dec = "";
            Int64 entero;
            int decimales;
            double nro;

            try
            {
                nro = Convert.ToDouble(num);
            }
            catch
            {
                return "";
            }

            entero = Convert.ToInt64(Math.Truncate(nro));
            decimales = Convert.ToInt32(Math.Round((nro - entero) * 100, 2));
            if (decimales > 0)
            {
                dec = " CON " + decimales.ToString() + "/100";
            }

            res = toCardinalText(Convert.ToDouble(entero)) //+ dec
                ;
            return res;
        }

        public string OrdinalEnLetras(string num)
        {
            string res, dec = "";
            Int64 entero;
            int decimales;
            double nro;

            try
            {
                nro = Convert.ToDouble(num);
            }
            catch
            {
                return "";
            }

            entero = Convert.ToInt64(Math.Truncate(nro));
            decimales = Convert.ToInt32(Math.Round((nro - entero) * 100, 2));
            if (decimales > 0)
            {
                dec = " CON " + decimales.ToString() + "/100";
            }

            res = toOrdinalText(Convert.ToDouble(entero)) + dec;
            return res;
        }

        private string toCardinalText(double value, bool ordinal = false)
        {
            string Num2Text = "";
            value = Math.Truncate(value);
            if (value == 0) Num2Text = "CERO";
            else if (value == 1) Num2Text = "UNO";
            else if (value == 2) Num2Text = "DOS";
            else if (value == 3) Num2Text = "TRES";
            else if (value == 4) Num2Text = "CUATRO";
            else if (value == 5) Num2Text = "CINCO";
            else if (value == 6) Num2Text = "SEIS";
            else if (value == 7) Num2Text = "SIETE";
            else if (value == 8) Num2Text = "OCHO";
            else if (value == 9) Num2Text = "NUEVE";
            else if (value == 10) Num2Text = "DIEZ";
            else if (value == 11) Num2Text = "ONCE";
            else if (value == 12) Num2Text = "DOCE";
            else if (value == 13) Num2Text = "TRECE";
            else if (value == 14) Num2Text = "CATORCE";
            else if (value == 15) Num2Text = "QUINCE";
            else if (value == 16) Num2Text = "DIECISÉIS";
            else if (value < 20) Num2Text = "DIECI" + toCardinalText(value - 10);
            else if (value == 20 && !ordinal) Num2Text = "VEINTE";
            else if (value == 22) Num2Text = "VEINTIDÓS";
            else if (value == 23) Num2Text = "VEINTITRÉS";
            else if (value == 26) Num2Text = "VEINTISÉIS";
            else if (value < 30) Num2Text = "VEINTI" + toCardinalText(value - 20);
            else if (value == 30) Num2Text = "TREINTA";
            else if (value == 40) Num2Text = "CUARENTA";
            else if (value == 50) Num2Text = "CINCUENTA";
            else if (value == 60) Num2Text = "SESENTA";
            else if (value == 70) Num2Text = "SETENTA";
            else if (value == 80) Num2Text = "OCHENTA";
            else if (value == 90) Num2Text = "NOVENTA";
            else if (value < 100) Num2Text = toCardinalText(Math.Truncate(value / 10) * 10) + " Y " + toCardinalText(value % 10);
            else if (value == 100) Num2Text = "CIEN";
            else if (value < 200) Num2Text = "CIENTO " + toCardinalText(value - 100);
            else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800)) Num2Text = toCardinalText(Math.Truncate(value / 100)) + "CIENTOS";
            else if (value == 500) Num2Text = "QUINIENTOS";
            else if (value == 700) Num2Text = "SETECIENTOS";
            else if (value == 900) Num2Text = "NOVECIENTOS";
            else if (value < 1000) Num2Text = toCardinalText(Math.Truncate(value / 100) * 100) + " " + toCardinalText(value % 100);
            else if (value == 1000) Num2Text = "MIL";
            else if (value < 2000) Num2Text = "MIL " + toCardinalText(value % 1000);
            else if (value < 1000000)
            {
                Num2Text = toCardinalText(Math.Truncate(value / 1000)) + " MIL";
                if ((value % 1000) > 0) Num2Text = Num2Text + " " + toCardinalText(value % 1000);
            }

            else if (value == 1000000) Num2Text = "UN MILLON";
            else if (value < 2000000) Num2Text = "UN MILLON " + toCardinalText(value % 1000000);
            else if (value < 1000000000000)
            {
                Num2Text = toCardinalText(Math.Truncate(value / 1000000)) + " MILLONES ";
                if ((value - Math.Truncate(value / 1000000) * 1000000) > 0) Num2Text = Num2Text + " " + toCardinalText(value - Math.Truncate(value / 1000000) * 1000000);
            }

            else if (value == 1000000000000) Num2Text = "UN BILLON";
            else if (value < 2000000000000) Num2Text = "UN BILLON " + toCardinalText(value - Math.Truncate(value / 1000000000000) * 1000000000000);

            else
            {
                Num2Text = toCardinalText(Math.Truncate(value / 1000000000000)) + " BILLONES";
                if ((value - Math.Truncate(value / 1000000000000) * 1000000000000) > 0) Num2Text = Num2Text + " " + toCardinalText(value - Math.Truncate(value / 1000000000000) * 1000000000000);
            }
            return Num2Text;

        }


        private string toOrdinalText(double value, bool ordinal = false)
        {
            string Num2Text = "";
            value = Math.Truncate(value);
            if (value == 0) Num2Text = "CERO";
            else if (value == 1) Num2Text = "PRIMERA";
            else if (value == 2) Num2Text = "SEGUNDA";
            else if (value == 3) Num2Text = "TERCERA";
            else if (value == 4) Num2Text = "CUARTA";
            else if (value == 5) Num2Text = "QUINTA";
            else if (value == 6) Num2Text = "SEXTA";
            else if (value == 7) Num2Text = "SEPTIMA";
            else if (value == 8) Num2Text = "OCTAVA";
            else if (value == 9) Num2Text = "NOVENA";
            else if (value == 10) Num2Text = "DECIMA";
            else if (value == 11) Num2Text = "ONCE";
            else if (value == 12) Num2Text = "DOCE";
            else if (value == 13) Num2Text = "TRECE";
            else if (value == 14) Num2Text = "CATORCE";
            else if (value == 15) Num2Text = "QUINCE";
            else if (value == 16) Num2Text = "DIECISÉIS";
            else if (value < 20) Num2Text = "DIECI" + toCardinalText(value - 10);
            else if (value == 20 && !ordinal) Num2Text = "VEINTE";
            else if (value == 22) Num2Text = "VEINTIDÓS";
            else if (value == 23) Num2Text = "VEINTITRÉS";
            else if (value == 26) Num2Text = "VEINTISÉIS";
            else if (value < 30) Num2Text = "VEINTI" + toCardinalText(value - 20);
            else if (value == 30) Num2Text = "TREINTA";
            else if (value == 40) Num2Text = "CUARENTA";
            else if (value == 50) Num2Text = "CINCUENTA";
            else if (value == 60) Num2Text = "SESENTA";
            else if (value == 70) Num2Text = "SETENTA";
            else if (value == 80) Num2Text = "OCHENTA";
            else if (value == 90) Num2Text = "NOVENTA";
            else if (value < 100) Num2Text = toCardinalText(Math.Truncate(value / 10) * 10) + " Y " + toCardinalText(value % 10);
            else if (value == 100) Num2Text = "CIEN";
            else if (value < 200) Num2Text = "CIENTO " + toCardinalText(value - 100);
            else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800)) Num2Text = toCardinalText(Math.Truncate(value / 100)) + "CIENTOS";
            else if (value == 500) Num2Text = "QUINIENTOS";
            else if (value == 700) Num2Text = "SETECIENTOS";
            else if (value == 900) Num2Text = "NOVECIENTOS";
            else if (value < 1000) Num2Text = toCardinalText(Math.Truncate(value / 100) * 100) + " " + toCardinalText(value % 100);
            else if (value == 1000) Num2Text = "MIL";
            else if (value < 2000) Num2Text = "MIL " + toCardinalText(value % 1000);
            else if (value < 1000000)
            {
                Num2Text = toCardinalText(Math.Truncate(value / 1000)) + " MIL";
                if ((value % 1000) > 0) Num2Text = Num2Text + " " + toCardinalText(value % 1000);
            }

            else if (value == 1000000) Num2Text = "UN MILLON";
            else if (value < 2000000) Num2Text = "UN MILLON " + toCardinalText(value % 1000000);
            else if (value < 1000000000000)
            {
                Num2Text = toCardinalText(Math.Truncate(value / 1000000)) + " MILLONES ";
                if ((value - Math.Truncate(value / 1000000) * 1000000) > 0) Num2Text = Num2Text + " " + toCardinalText(value - Math.Truncate(value / 1000000) * 1000000);
            }

            else if (value == 1000000000000) Num2Text = "UN BILLON";
            else if (value < 2000000000000) Num2Text = "UN BILLON " + toCardinalText(value - Math.Truncate(value / 1000000000000) * 1000000000000);

            else
            {
                Num2Text = toCardinalText(Math.Truncate(value / 1000000000000)) + " BILLONES";
                if ((value - Math.Truncate(value / 1000000000000) * 1000000000000) > 0) Num2Text = Num2Text + " " + toCardinalText(value - Math.Truncate(value / 1000000000000) * 1000000000000);
            }
            return Num2Text;
        }


        public String ConvertirDireccionLetras(string Direccion)
        {
            return ConvertirNumeroDireccionLetras(
                Direccion.Replace("-", " GUION ")
                  );
        }
    }


    /// <summary>
    /// Convierte números en su expresión numérica a su numeral cardinal
    /// </summary>
    public sealed class Numalet
    {
        #region Miembros estáticos

        private const int UNI = 0, DIECI = 1, DECENA = 2, CENTENA = 3;
        private static string[,] _matrizCardinal = new string[CENTENA + 1, 10]
            {
                {null," uno", " dos", " tres", " cuatro", " cinco", " seis", " siete", " ocho", " nueve"},
                {" diez"," once"," doce"," trece"," catorce"," quince"," dieciséis"," diecisiete"," dieciocho"," diecinueve"},
                {null,null,null," treinta"," cuarenta"," cincuenta"," sesenta"," setenta"," ochenta"," noventa"},
                {null,null,null,null,null," quinientos",null," setecientos",null," novecientos"}
            };
        private static string[,] _matrizOrdinal = new string[CENTENA + 1, 10]
            {
                {null," primera", " segunda", " tercera", " cuarta", " quinta", " sexta", " séptima", " octava", " novena"},
                {" décima"," once"," doce"," trece"," catorce"," quince"," dieciséis"," diecisiete"," dieciocho"," diecinueve"},
                {null,null,null," treinta"," cuarenta"," cincuenta"," sesenta"," setenta"," ochenta"," noventa"},
                {null,null,null,null,null," quinientos",null," setecientos",null," novecientos"}
            };


        private const Char sub = (Char)26;
        //Cambiar acá si se quiere otro comportamiento en los métodos de clase
        public const String SeparadorDecimalSalidaDefault = "con";
        public const String MascaraSalidaDecimalDefault = "00'/100.-'";
        public const Int32 DecimalesDefault = 2;
        public const Boolean LetraCapitalDefault = false;
        public const Boolean ConvertirDecimalesDefault = false;
        public const Boolean ApocoparUnoParteEnteraDefault = false;
        public const Boolean ApocoparUnoParteDecimalDefault = false;
        public const Boolean _OrdinalDefault = false;
        #endregion

        #region Propiedades

        private Int32 _decimales = DecimalesDefault;
        private CultureInfo _cultureInfo = CultureInfo.CurrentCulture;
        private String _separadorDecimalSalida = SeparadorDecimalSalidaDefault;
        private Int32 _posiciones = DecimalesDefault;
        private String _mascaraSalidaDecimal, _mascaraSalidaDecimalInterna = MascaraSalidaDecimalDefault;
        private Boolean _esMascaraNumerica = true;
        private Boolean _letraCapital = LetraCapitalDefault;
        private Boolean _convertirDecimales = ConvertirDecimalesDefault;
        private Boolean _apocoparUnoParteEntera = false;
        private Boolean _apocoparUnoParteDecimal;
        public Boolean _Ordinal = false;

        /// <summary>
        /// Indica la cantidad de decimales que se pasarán a entero para la conversión
        /// </summary>
        /// <remarks>Esta propiedad cambia al cambiar MascaraDecimal por un valor que empieze con '0'</remarks>
        public Int32 Decimales
        {
            get { return _decimales; }
            set
            {
                if (value > 10) throw new ArgumentException(value.ToString() + " excede el número máximo de decimales admitidos, solo se admiten hasta 10.");
                _decimales = value;
            }
        }

        /// <summary>
        /// Objeto CultureInfo utilizado para convertir las cadenas de entrada en números
        /// </summary>
        public CultureInfo CultureInfo
        {
            get { return _cultureInfo; }
            set { _cultureInfo = value; }
        }

        /// <summary>
        /// Indica la cadena a intercalar entre la parte entera y la decimal del número
        /// </summary>
        public String SeparadorDecimalSalida
        {
            get { return _separadorDecimalSalida; }
            set
            {
                _separadorDecimalSalida = value;
                //Si el separador decimal es compuesto, infiero que estoy cuantificando algo,
                //por lo que apocopo el "uno" convirtiéndolo en "un"
                if (value.Trim().IndexOf(" ") > 0)
                    _apocoparUnoParteEntera = true;
                else _apocoparUnoParteEntera = false;
            }
        }

        /// <summary>
        /// Indica el formato que se le dara a la parte decimal del número
        /// </summary>
        public String MascaraSalidaDecimal
        {
            get
            {
                if (!String.IsNullOrEmpty(_mascaraSalidaDecimal))
                    return _mascaraSalidaDecimal;
                else return "";
            }
            set
            {
                //determino la cantidad de cifras a redondear a partir de la cantidad de '0' o '#' 
                //que haya al principio de la cadena, y también si es una máscara numérica
                int i = 0;
                while (i < value.Length
                    && (value[i] == '0')
                        | value[i] == '#')
                    i++;
                _posiciones = i;
                if (i > 0)
                {
                    _decimales = i;
                    _esMascaraNumerica = true;
                }
                else _esMascaraNumerica = false;
                _mascaraSalidaDecimal = value;
                if (_esMascaraNumerica)
                    _mascaraSalidaDecimalInterna = value.Substring(0, _posiciones) + "'"
                        + value.Substring(_posiciones)
                        .Replace("''", sub.ToString())
                        .Replace("'", String.Empty)
                        .Replace(sub.ToString(), "'") + "'";
                else
                    _mascaraSalidaDecimalInterna = value
                        .Replace("''", sub.ToString())
                        .Replace("'", String.Empty)
                        .Replace(sub.ToString(), "'");
            }
        }

        /// <summary>
        /// Indica si la primera letra del resultado debe estár en mayúscula
        /// </summary>
        public Boolean LetraCapital
        {
            get { return _letraCapital; }
            set { _letraCapital = value; }
        }

        /// <summary>
        /// Indica si se deben convertir los decimales a su expresión nominal
        /// </summary>
        public Boolean ConvertirDecimales
        {
            get { return _convertirDecimales; }
            set
            {
                _convertirDecimales = value;
                _apocoparUnoParteDecimal = value;
                if (value)
                {// Si la máscara es la default, la borro
                    if (_mascaraSalidaDecimal == MascaraSalidaDecimalDefault)
                        MascaraSalidaDecimal = "";
                }
                else if (String.IsNullOrEmpty(_mascaraSalidaDecimal))
                    //Si no hay máscara dejo la default
                    MascaraSalidaDecimal = MascaraSalidaDecimalDefault;
            }
        }

        /// <summary>
        /// Indica si de debe cambiar "uno" por "un" en las unidades.
        /// </summary>
        public Boolean ApocoparUnoParteEntera
        {
            get { return _apocoparUnoParteEntera; }
            set { _apocoparUnoParteEntera = value; }
        }

        /// <summary>
        /// Determina si se debe apococopar el "uno" en la parte decimal
        /// </summary>
        /// <remarks>El valor de esta propiedad cambia al setear ConvertirDecimales</remarks>
        public Boolean ApocoparUnoParteDecimal
        {
            get { return _apocoparUnoParteDecimal; }
            set { _apocoparUnoParteDecimal = value; }
        }

        #endregion

        #region Constructores

        public Numalet(Boolean OrdinalNumber = false)
        {
            _Ordinal = OrdinalNumber;
            MascaraSalidaDecimal = MascaraSalidaDecimalDefault;
            SeparadorDecimalSalida = SeparadorDecimalSalidaDefault;
            LetraCapital = LetraCapitalDefault;
            ConvertirDecimales = _convertirDecimales;
        }

        public Numalet(Boolean ConvertirDecimales, String MascaraSalidaDecimal, String SeparadorDecimalSalida, Boolean LetraCapital)
        {
            if (!String.IsNullOrEmpty(MascaraSalidaDecimal))
                this.MascaraSalidaDecimal = MascaraSalidaDecimal;
            if (!String.IsNullOrEmpty(SeparadorDecimalSalida))
                _separadorDecimalSalida = SeparadorDecimalSalida;
            _letraCapital = LetraCapital;
            _convertirDecimales = ConvertirDecimales;
        }
        #endregion

        #region Conversores de instancia

        public String ToCustomLetter(Double Numero)
        { return Convertir((Decimal)Numero, _decimales, _separadorDecimalSalida, _mascaraSalidaDecimalInterna, _esMascaraNumerica, _letraCapital, _convertirDecimales, _apocoparUnoParteEntera, _apocoparUnoParteDecimal, _Ordinal); }

        public String ToCustomLetter(String Numero)
        {
            Double dNumero;
            if (Double.TryParse(Numero, NumberStyles.Float, _cultureInfo, out dNumero))
                return ToCustomLetter(dNumero);
            else throw new ArgumentException("'" + Numero + "' no es un número válido.");
        }

        public String ToCustomLetter(Decimal Numero)
        { return ToLetter((Numero)); }

        public String ToCustomLetter(Int32 Numero)
        { return Convertir((Decimal)Numero, 0, _separadorDecimalSalida, _mascaraSalidaDecimalInterna, _esMascaraNumerica, _letraCapital, _convertirDecimales, _apocoparUnoParteEntera, false, _Ordinal); }

        #endregion

        #region Conversores estáticos

        public static String ToLetter(Int32 Numero)
        {
            return Convertir((Decimal)Numero, 0, null, null, true, LetraCapitalDefault, ConvertirDecimalesDefault, ApocoparUnoParteEnteraDefault, ApocoparUnoParteDecimalDefault, _OrdinalDefault);
        }

        public static String ToLetter(Double Numero)
        {
            return ToLetter((Decimal)Numero);
        }

        public static String ToLetter(String Numero, CultureInfo ReferenciaCultural)
        {
            Double dNumero;
            if (Double.TryParse(Numero, NumberStyles.Float, ReferenciaCultural, out dNumero))
                return ToLetter(dNumero);
            else throw new ArgumentException("'" + Numero + "' no es un número válido.");
        }

        public static String ToLetter(String Numero)
        {
            return Numalet.ToLetter(Numero, CultureInfo.CurrentCulture);
        }

        public static String ToLetter(Decimal Numero)
        {
            return Convertir(Numero, DecimalesDefault, SeparadorDecimalSalidaDefault, MascaraSalidaDecimalDefault, true, LetraCapitalDefault, ConvertirDecimalesDefault, ApocoparUnoParteEnteraDefault, ApocoparUnoParteDecimalDefault, _OrdinalDefault);
        }

        #endregion

        private static String Convertir(Decimal Numero, Int32 Decimales, String SeparadorDecimalSalida, String MascaraSalidaDecimal, Boolean EsMascaraNumerica, Boolean LetraCapital, Boolean ConvertirDecimales, Boolean ApocoparUnoParteEntera, Boolean ApocoparUnoParteDecimal, Boolean _Ordinal)
        {
            Int64 Num;
            Int32 terna, centenaTerna, decenaTerna, unidadTerna, iTerna;
            String cadTerna;
            StringBuilder Resultado = new StringBuilder();

            Num = (Int64)Math.Abs(Numero);

            if (Num >= 1000000000000 || Num < 0) throw new ArgumentException("El número '" + Numero.ToString() + "' excedió los límites del conversor: [0;1.000.000.000.000)");
            if (Num == 0)
                Resultado.Append(" cero");
            else
            {
                iTerna = 0;
                while (Num > 0)
                {
                    iTerna++;
                    cadTerna = String.Empty;
                    terna = (Int32)(Num % 1000);

                    centenaTerna = (Int32)(terna / 100);
                    decenaTerna = terna % 100;
                    unidadTerna = terna % 10;

                    if ((decenaTerna > 0) && (decenaTerna < 10) && !_Ordinal)
                        cadTerna = _matrizCardinal[UNI, unidadTerna] + cadTerna;
                    else if ((decenaTerna > 0) && (decenaTerna < 10) && _Ordinal)
                        cadTerna = _matrizOrdinal[UNI, unidadTerna] + cadTerna;
                    else if (((decenaTerna >= 10) && (decenaTerna < 20)) && !_Ordinal)
                        cadTerna = cadTerna + _matrizCardinal[DIECI, unidadTerna];
                    else if (((decenaTerna >= 10) && (decenaTerna < 20)) && _Ordinal)
                        cadTerna = cadTerna + _matrizOrdinal[DIECI, unidadTerna];
                    else if (decenaTerna == 20)
                        cadTerna = cadTerna + " veinte";
                    else if ((decenaTerna > 20) && (decenaTerna < 30))
                        cadTerna = " veinti" + _matrizCardinal[UNI, unidadTerna].Substring(1);
                    else if ((decenaTerna >= 30) && (decenaTerna < 100))
                        if (unidadTerna != 0)
                            cadTerna = _matrizCardinal[DECENA, (Int32)(decenaTerna / 10)] + " y" + _matrizCardinal[UNI, unidadTerna] + cadTerna;
                        else
                            cadTerna += _matrizCardinal[DECENA, (Int32)(decenaTerna / 10)];

                    switch (centenaTerna)
                    {
                        case 1:
                            if (decenaTerna > 0) cadTerna = " ciento" + cadTerna;
                            else cadTerna = " cien" + cadTerna;
                            break;
                        case 5:
                        case 7:
                        case 9:
                            cadTerna = _matrizCardinal[CENTENA, (Int32)(terna / 100)] + cadTerna;
                            break;
                        default:
                            if ((Int32)(terna / 100) > 1) cadTerna = _matrizCardinal[UNI, (Int32)(terna / 100)] + "cientos" + cadTerna;
                            break;
                    }
                    //Reemplazo el 'uno' por 'un' si no es en las únidades o si se solicító apocopar
                    if ((iTerna > 1 | ApocoparUnoParteEntera) && decenaTerna == 21)
                        cadTerna = cadTerna.Replace("veintiuno", "veintiún");
                    else if ((iTerna > 1 | ApocoparUnoParteEntera) && unidadTerna == 1 && decenaTerna != 11)
                        cadTerna = cadTerna.Substring(0, cadTerna.Length - 1);
                    //Acentúo 'veintidós', 'veintitrés' y 'veintiséis'
                    else if (decenaTerna == 22) cadTerna = cadTerna.Replace("veintidos", "veintidós");
                    else if (decenaTerna == 23) cadTerna = cadTerna.Replace("veintitres", "veintitrés");
                    else if (decenaTerna == 26) cadTerna = cadTerna.Replace("veintiseis", "veintiséis");

                    //Completo miles y millones
                    switch (iTerna)
                    {
                        case 3:
                            if (Numero < 2000000) cadTerna += " millón";
                            else cadTerna += " millones";
                            break;
                        case 2:
                        case 4:
                            if (terna > 0) cadTerna += " mil";
                            break;
                    }
                    Resultado.Insert(0, cadTerna);
                    Num = (Int32)(Num / 1000);
                } //while
            }

            //Se agregan los decimales si corresponde
            //if (Decimales > 0)
            //{
            //    Resultado.Append(" " + SeparadorDecimalSalida + " ");
            //    Int32 EnteroDecimal = (Int32)Math.Round((Double)(Numero - (Int64)Numero) * Math.Pow(10, Decimales), 0);
            //    if (ConvertirDecimales)
            //    {
            //        Boolean esMascaraDecimalDefault = MascaraSalidaDecimal == MascaraSalidaDecimalDefault;
            //        Resultado.Append(Convertir((Decimal)EnteroDecimal, 0, null, null, EsMascaraNumerica, false, false, (ApocoparUnoParteDecimal && !EsMascaraNumerica/*&& !esMascaraDecimalDefault*/), false, _Ordinal) + " "
            //            + (EsMascaraNumerica ? "" : MascaraSalidaDecimal));
            //    }
            //    else
            //        if (EsMascaraNumerica) Resultado.Append(EnteroDecimal.ToString(MascaraSalidaDecimal));
            //    else Resultado.Append(EnteroDecimal.ToString() + " " + MascaraSalidaDecimal);
            //}
            //Se pone la primer letra en mayúscula si corresponde y se retorna el resultado
            if (LetraCapital)
                return Resultado[1].ToString().ToUpper() + Resultado.ToString(2, Resultado.Length - 2);
            else
                return Resultado.ToString().Substring(1);
        }

        

    }
    public class Cripto
    {
        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Encripta el texto enviado.
        /// </summary>
        /// <param name="toEncrypt">Mensaje a encriptar</param>
        /// /// <param name="useHashing">Utilizar metodo Hash</param>
        /// <returns>Mensaje encriptado</returns>
        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            //-------------------------------------------------------------------
            //Obtenemos la "llave secreta" para poder encriptar el textos con MD5
            string key = ConfigurationManager.AppSettings["DefaultKeySecurity"].ToString();

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));

                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            //-------------------------------------------------------------------
            //Utilizamos el alcoritmo Triple DES para encriptar
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0,
                                 toEncryptArray.Length);
            tdes.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Desencripta el texto enviado.
        /// </summary>
        /// <param name="toEncrypt">Mensaje a desencriptar</param>
        /// /// <param name="useHashing">Utilizar metodo Hash</param>
        /// <returns>Mensaje desencriptado</returns>
        public static string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            //-------------------------------------------------------------------
            //Obtenemos la "llave secreta" para poder encriptar el textos con MD5
            string key = ConfigurationManager.AppSettings["MFSecurity2017"].ToString();

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
            {
                keyArray = UTF8Encoding.UTF8.GetBytes(key);
            }

            //-------------------------------------------------------------------
            //Utilizamos el alcoritmo Triple DES para desencriptar
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(
                                 toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
    }

    public static class Mail
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función envia un correo electrónico del remitente a sus destinatarios con asunto y cuerpo del mensaje.
        /// </summary>
        /// <param name="Remitente">Correo electrónico del remitente.</param>
        /// <param name="CuentaCorreo">Correo electrónico del destinatario.</param>
        /// <param name="Asunto">Asunto del correo electrónico.</param>
        /// <param name="Mensaje">Mensaje o cuerpo del mensaje electrónico.</param>
        /// <param name="DisplayName">Nombre a desplegar en el correo electrónico.</param>
        /// <returns>Retorna una variable tipo Respuesta; 
        ///          Respuesta.exito = true si es existoso
        ///          Respuesta.exito = false si es fallido </returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public static bool EnviarEmail(string Remitente, string CuentaCorreo, string Asunto, StringBuilder Mensaje, string DisplayName)
        {
            List<string> ListaCuentaCorreo = new List<string>();
            ListaCuentaCorreo.Add(CuentaCorreo);
            return EnviarEmail(Remitente, ListaCuentaCorreo, Asunto, Mensaje, DisplayName);
        }

        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función envia un correo electrónico del remitente a sus destinatarios con asunto y cuerpo del mensaje.
        /// </summary>
        /// <param name="Remitente">Correo electrónico del remitente.</param>
        /// <param name="CuentaCorreo">Listado de correos electrónicos de los destinatarios.</param>
        /// <param name="Asunto">Asunto del correo electrónico.</param>
        /// <param name="Mensaje">Mensaje o cuerpo del mensaje electrónico.</param>
        /// <param name="DisplayName">Nombre a desplegar en el correo electrónico.</param>
        /// <param name="lstImagenes">Lista de imagenes a utilizarse como contenido en el correo, es opcional</param>
        /// <param name="BccTo">destinatario de correo para control</param>
        /// <returns>Retorna una variable tipo Respuesta; 
        ///          Respuesta.exito = true si es existoso
        ///          Respuesta.exito = false si es fallido </returns>
        //-----------------------------------------------------------------------------------------------------------------------
        public static bool EnviarEmail(string Remitente, List<string> CuentaCorreo, string Asunto, StringBuilder Mensaje, string DisplayName, string BccTo = null,List<Attachment> attachments=null,List<string> ReplyTo=null)
        {
            string mBitacora = "";
            string mSlack = "";


            SmtpClient smtp = new SmtpClient();
            MailMessage mail = new MailMessage();

            //-------------------------------------------------------------------------------------------
            //Agregar la lista de correo

            foreach (var item in CuentaCorreo)
            {
                mail.To.Add(item);
            }
            //bcc
            if (BccTo != null & BccTo != string.Empty)
                mail.Bcc.Add(BccTo);
            //-------------------------------------------------------------------------------------------
            //Responder a
            if (ReplyTo != null)
                foreach (string item in ReplyTo)
                    mail.ReplyToList.Add(item);
            //-------------------------------------------------------------------------------------------
            //Establecer el remitente y el nombre a mostrar
            mail.From = new MailAddress(Remitente, DisplayName);

            //-------------------------------------------------------------------------------------------
            //Establecer a quien se le responerá
            mail.ReplyToList.Add(Remitente);

            //-------------------------------------------------------------------------------------------
            //Establecer el Asunto del correo
            mail.Subject = Asunto;

            //-------------------------------------------------------------------------------------------
            //Establecer el cuerpo del mensaje como html
            mail.IsBodyHtml = true;
            mail.Body = Mensaje.ToString();

            //adjuntos
            if (attachments != null)
                foreach (Attachment item in attachments)
                    mail.Attachments.Add(item);
            //-------------------------------------------------------------------------------------------
            //Establecer el host y puerto del servidor de correo
            smtp.Host = WebConfigurationManager.AppSettings["MailHost2"];
            smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort2"]);
            
            smtp.UseDefaultCredentials = false;
            //-------------------------------------------------------------------------------------------
            //Habilitar el protocolo SSL y el metodo de entrega
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

            mBitacora = string.Format(" {0}; {1}; {2}; {3}; ", "Correo", Remitente, string.Join(",", CuentaCorreo), Asunto);
            mSlack = string.Format("Clase: {0} \nRemitente:{1} \nLista de correo:{2} \nAsunto:{3} ","Utileria.cs", Remitente, string.Join(",", CuentaCorreo), Asunto);
            smtp.Timeout = 5000;
            //-------------------------------------------------------------------------------------------
            //Se envia el correo, tras tres intentos fallidos envia por slack un mensaje

            try
            {//Primer intento para enviar correo
                smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail2"], WebConfigurationManager.AppSettings["PwdMail2"]);
                smtp.Send(mail);
                log.Info(mBitacora);
            }
            catch
            {
                
                    
                    try
                    {//Intento con la cuenta de contingencia de productos multiples

                        //Establecer el host y puerto del servidor de correo
                        smtp.Host = WebConfigurationManager.AppSettings["MailHost"];
                        smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort"]);

                        //-------------------------------------------------------------------------------------------
                        //Habilitar el protocolo SSL y el metodo de entrega
                        smtp.EnableSsl = true;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtp.Timeout = 10000;
                        smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail"], WebConfigurationManager.AppSettings["PwdMail"]);
                        smtp.Send(mail);
                        log.Info("Contingencia" + mBitacora);
                    }
                    catch (Exception e2)
                    {

                        log.Error("Contingencia" + string.Format(mBitacora + " {0};", e2.Message));
                        EnviarMensajeSlack(string.Format(mSlack + " \nERROR correo contingencia - {0};", e2.Message));
                        return false;
                    }
                    //--------------------------------------------------------------------------------------------------------
                }
            
            return true;
        }


        //-----------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Esta función envia un mensaje por slack al canal de errores del PV
        /// </summary>
        /// <param name="Mensaje">Mensaje a enviar por Slack.</param>
        //-----------------------------------------------------------------------------------------------------------------------
        public static void EnviarMensajeSlack(string Mensaje)
        {
            //----------------------------------------------------------------
            //Validamos si esta habilitado el modulo
            //----------------------------------------------------------------
            string json = "";
            try
            {
                WebRequest request = WebRequest.Create(string.Format("{0}", WebConfigurationManager.AppSettings["ErrorPDVSlackChannel"]));
                json = "{" + string.Format(" \"text\": \"Alerta desde el PDV. \n{0}.\" ", Mensaje.Replace("{", "").Replace("}", "")) + "}";
                byte[] data = Encoding.ASCII.GetBytes(json);

                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

            }

            catch (Exception e)
            {
                log.Error(string.Format("ERROR {0};", e.Message));
            }
        }
    }
}