﻿using MF_Clases;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace PuntoDeVenta.Util
	{
    /// <summary>
    /// Cliente para consumir los servicios de las consultas de los desembolsos/solicitudes de crédito a las financieras
    /// </summary>
    public class WebRestClient
    {
        /// <summary>
        /// Obtiene los desembolsos realizados en una fecha determinada
        /// </summary>
        /// <param name="dtFecha"></param>
        /// <param name="urlRestServices"></param>
        /// <returns></returns>
        public static IEnumerable<MF_Clases.Clases.Desembolso> ConsultarDesembolsosRealizados(DateTime dtFecha, string urlRestServices,string Financiera)
        {
            MF_Clases.Clases.Desembolso[] mDesembolsos = null;
            try
            {
                WebRequest request = WebRequest.Create(string.Format("{0}/ConsultasDesembolsos/Realizados?FechaDesembolso={1}&nFinanciera={2}", Convert.ToString(urlRestServices), Utilitarios.FormatoDDMMYYYY(dtFecha),Financiera));
                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                MF_Clases.Respuesta mRespuesta = new MF_Clases.Respuesta();

                mRespuesta = JsonConvert.DeserializeObject<MF_Clases.Respuesta>(responseString);
                if (mRespuesta.Exito)
                    mDesembolsos = JsonConvert.DeserializeObject<MF_Clases.Clases.Desembolso[]>(mRespuesta.Objeto.ToString());
            }
            catch
            {

            }
            return mDesembolsos;
        }

        /// <summary>
        /// Obtiene los rechazos pendientes de resolver, por una financiera o -1 para todas las financieras
        /// </summary>
        /// <param name="urlRestServices">Dirección del servicio en FiestaNETRestservices</param>
        /// <param name="nFinanciera"># financiera o -1 para todas</param>
        /// <returns></returns>
        public static IEnumerable<MF_Clases.Clases.SolicitudInterconsumo> ConsultarSolicitudesRechazadas(string urlRestServices, int nFinanciera)
        {
            MF_Clases.Clases.SolicitudInterconsumo[] mRechazos = null;
            try
            {
                WebRequest request = WebRequest.Create(string.Format("{0}/ConsultasDesembolsos/Rechazados?nFinanciera={1}", Convert.ToString(urlRestServices), nFinanciera));
                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                MF_Clases.Respuesta mRespuesta = new MF_Clases.Respuesta();

                mRespuesta = JsonConvert.DeserializeObject<MF_Clases.Respuesta>(responseString);
                if (mRespuesta.Exito)
                    mRechazos = JsonConvert.DeserializeObject<MF_Clases.Clases.SolicitudInterconsumo[]>(mRespuesta.Objeto.ToString());
            }
            catch
            {
            }
            return mRechazos;
        }

        /// <summary>
        /// Obtiene los desembolsos pendientes de aprobación/rechazo por parte de la financiera
        /// </summary>
        /// <param name="urlRestServices"></param>
        /// <param name="blnTodas"></param>
        /// <returns></returns>
        public static IEnumerable<MF_Clases.Clases.SolicitudInterconsumo> ConsultarDesembolsosPendientes(string urlRestServices, bool blnTodas,int nFinanciera)
        {
            MF_Clases.Clases.SolicitudInterconsumo[] mPendientes = null;
            try
            {
                WebRequest request = WebRequest.Create(string.Format("{0}/ConsultasDesembolsos/Pendientes?blnTodas={1}&nFinanciera={2}", Convert.ToString(urlRestServices), blnTodas,nFinanciera));
                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                MF_Clases.Respuesta mRespuesta = new MF_Clases.Respuesta();

                mRespuesta = JsonConvert.DeserializeObject<MF_Clases.Respuesta>(responseString);
                if (mRespuesta.Exito)
                    mPendientes = JsonConvert.DeserializeObject<MF_Clases.Clases.SolicitudInterconsumo[]>(mRespuesta.Objeto.ToString());
            }
            catch
            {
            }
            return mPendientes;
        }

        /// <summary>
        /// Obtiene los desembolsos pendientes de envio desde las tiendas
        /// </summary>
        /// <param name="urlRestServices"></param>
        /// <returns></returns>
        public static IEnumerable<MF_Clases.Clases.SolicitudInterconsumo> ConsultarDesembolsosPendientesTiendas(string urlRestServices, int nFinanciera)
        {
            MF_Clases.Clases.SolicitudInterconsumo[] mPendientes = null;
            try
            {
                WebRequest request = WebRequest.Create(string.Format("{0}/ConsultasDesembolsos/ObtenerSolicitudesPendientesEnTienda?F={1}", Convert.ToString(urlRestServices), nFinanciera));
                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                MF_Clases.Respuesta mRespuesta = new MF_Clases.Respuesta();

                mRespuesta = JsonConvert.DeserializeObject<MF_Clases.Respuesta>(responseString);
                if (mRespuesta.Exito)
                    mPendientes = JsonConvert.DeserializeObject<MF_Clases.Clases.SolicitudInterconsumo[]>(mRespuesta.Objeto.ToString());
            }
            catch
            {
            }
            return mPendientes;
        }

        /// <summary>
        /// Obtener la lista de las financieras activas
        /// restricción: Activo=S Es_Banco=S
        /// Para debuguear: http://localhost:53874/api/Financiera/GetFinancieras
        /// </summary>
        /// <param name="urlRestServices"></param>
        /// <returns></returns>
        public static IEnumerable<MF_Clases.Clases.Financiera> ObtenerFinancieras(string urlRestServices)
        {
            MF_Clases.Clases.Financiera[] mFinanciera = null;
            try
            {
                WebRequest request = WebRequest.Create(string.Format("{0}/Financiera/GetFinancieras", urlRestServices));
                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                MF_Clases.Respuesta mRespuesta = new MF_Clases.Respuesta();

                mRespuesta = JsonConvert.DeserializeObject<MF_Clases.Respuesta>(responseString);
                if (mRespuesta.Exito)
                    mFinanciera = JsonConvert.DeserializeObject<MF_Clases.Clases.Financiera[]>(mRespuesta.Objeto.ToString());
            }
            catch
            {
            }
            return mFinanciera;
        }
        /// <summary>
        /// Obtener los niveles de precio dentro de la forma de cálculo del precio del bien
        /// para la facturación (junio 2018)
        /// </summary>
        /// <param name="urlRestServices"></param>
        /// <returns></returns>
        public static List<string> ObtenerNivelPrecioFacturacion(string urlRestServices)
        {
            List<string> lstResult = new List<string>();
            try
            {
                WebRequest request = WebRequest.Create(string.Format("{0}/FormaDePagoValidacion", urlRestServices));
                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                MF_Clases.Respuesta mRespuesta = new MF_Clases.Respuesta();

                List<string> strRespuesta = JsonConvert.DeserializeObject<List<string>>(responseString);

                return strRespuesta;
            }
            catch
            {
            }
            return lstResult;
        }
        /// <summary>
        /// Catalogo de financieras a las que se está guardando la huella digital y la foto del cliente
        /// </summary>
        /// <param name="urlRestServices"></param>
        /// <returns></returns>
        public static List<Clases.Catalogo> ObtenerFinancierasConHuellaYFoto(string urlRestServices)
        {
            List<Clases.Catalogo> lstResult = new List<Clases.Catalogo>();
            try
            {
                WebRequest request = WebRequest.Create(string.Format("{0}/BiometricosHabilitados", urlRestServices));
                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                MF_Clases.Respuesta mRespuesta = new MF_Clases.Respuesta();

                mRespuesta = JsonConvert.DeserializeObject<Respuesta>(responseString);

                return JsonConvert.DeserializeObject<IEnumerable<Clases.Catalogo>>(mRespuesta.Objeto.ToString()).ToList();
            }
            catch (Exception EX)
            {
                var i = EX.Message;
            }
            return lstResult;
        }

        public static List<Clases.Catalogo> ObtenerTiendasConHuellaYFoto(string urlRestServices)
        {
            List<Clases.Catalogo> lstResult = new List<Clases.Catalogo>();
            try
            {
                WebRequest request = WebRequest.Create(string.Format("{0}/TiendasHabilitadas", urlRestServices));
                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                MF_Clases.Respuesta mRespuesta = new MF_Clases.Respuesta();

                mRespuesta = JsonConvert.DeserializeObject<Respuesta>(responseString);

                return JsonConvert.DeserializeObject<IEnumerable<Clases.Catalogo>>(mRespuesta.Objeto.ToString()).ToList();
            }
            catch (Exception EX)
            {
                var i = EX.Message;
            }
            return lstResult;
        }
    }
        
	}