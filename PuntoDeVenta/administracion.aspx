﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="administracion.aspx.cs" Inherits="PuntoDeVenta.administracion" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="ToolbarExport.ascx" TagName="ToolbarExport" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v16.2" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #middle {
            vertical-align: middle;
        }

        a:-webkit-any-link {
            text-decoration: none;
        }

        .flex-container {
            display: inline-block;
        }

            .flex-container > div {
                margin: 10px;
                padding: 20px;
                font-size: 30px;
            }

        .auto-style2 {
            column-span: all;
            height: 20px;
        }

        .auto-style4 {
            width: 86px;
        }

        .auto-style5 {
            width: 251px;
        }
    </style>

    <script type="text/javascript">
        function OnBatchEditEndEditing(s, e) {
            setTimeout(function () {
                s.UpdateEdit();
            }, 0);
        }
        function HidePopup() {
            popupArticulos.Hide();
        }
        function onDescriptionClick(s, e, key) {
            txtLinea.SetText(key);
            popupArticulos.Show();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>&nbsp;&nbsp;Opciones de Administración</h3>
    <div class="content2">
        <div class="clientes2">

            <ul>
                <li>

                    <table>
                        <tr>
                            <td colspan="3">
                                <a>Actualizar despachos</a></td>
                            <td colspan="3" style="text-align: right">
                                <asp:Label ID="lbErrorTransportista" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                <asp:Label ID="lbInfoTransportista" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>Rango fechas de entrega:</td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaInicialT" runat="server" Theme="SoftOrange" DisplayFormatString="dd/MM/yyyy"
                                    CssClass="DateEditStyle" Width="140px" TabIndex="10">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true"
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true"
                                        ShowWeekNumbers="False">
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                            <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaFinalT" runat="server" Theme="SoftOrange" DisplayFormatString="dd/MM/yyyy"
                                    CssClass="DateEditStyle" Width="140px" TabIndex="20">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true"
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true"
                                        ShowWeekNumbers="False">
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                            <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkFechasEnvio" runat="server"
                                    ToolTip="Haga clic aquí para seleccionar envíos según la fecha del envío"
                                    OnClick="lkSeleccionarFechasEnvio_Click" TabIndex="30">Por fecha de envío</asp:LinkButton>
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Tienda que desea consultar:</td>
                            <td>
                                <dx:ASPxComboBox ID="tiendaTransporte" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle"
                                    ValueType="System.String" Width="75px" DropDownStyle="DropDownList"
                                    DropDownWidth="40" DropDownRows="25" TabIndex="40">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkSeleccionarTiendaT" runat="server"
                                    ToolTip="Haga clic aquí para seleccionar documentos según la tienda seleccionada"
                                    OnClick="lkSeleccionarTiendaT_Click" TabIndex="50" Visible="False">Seleccionar tienda</asp:LinkButton>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkFechasTransporte" runat="server" ToolTip="Haga clic aquí para seleccionar envíos según la fecha de entrega de los mismos"
                                    OnClick="lkSeleccionarFechasT_Click" TabIndex="30" Visible="False">Por fechas de entrega</asp:LinkButton>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Transportista que lo llevaba:</td>
                            <td colspan="2">
                                <dx:ASPxComboBox ID="transportistaT" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle"
                                    ValueType="System.String" Width="295px" DropDownStyle="DropDownList"
                                    DropDownWidth="40" DropDownRows="25" TabIndex="60">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkTransportistaT" runat="server"
                                    ToolTip="Haga clic aquí para seleccionar envíos según el transportista y las fechas seleccionadas"
                                    OnClick="lkTransportistaT_Click" TabIndex="70">Por transportista</asp:LinkButton>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td></td>
                            <td colspan="2" style="text-align: center">
                                <asp:LinkButton ID="lkActualizarTransportista" runat="server"
                                    ToolTip="Haga clic aquí para grabar los cambios en los transportistas y aceptación de clientes"
                                    OnClick="lkActualizarTransportista_Click" TabIndex="80">Grabar</asp:LinkButton>
                            </td>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="6">

                                <dx:ASPxGridView ID="gridDespachos" EnableTheming="True" Theme="SoftOrange" Visible="False"
                                    runat="server" KeyFieldName="Despacho" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid"
                                    OnBatchUpdate="gridDespachos_BatchUpdate"
                                    OnRowUpdating="gridDespachos_RowUpdating"
                                    OnCommandButtonInitialize="gridDespachos_CommandButtonInitialize"
                                    OnCellEditorInitialize="gridDespachos_CellEditorInitialize"
                                    OnHtmlDataCellPrepared="gridDespachos_HtmlDataCellPrepared"
                                    OnHtmlRowPrepared="gridDespachos_HtmlRowPrepared" TabIndex="90">
                                    <Styles>
                                        <StatusBar>
                                            <border borderstyle="None" />
                                        </StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                        <GroupRow Font-Bold="true" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AllowSort="false"></SettingsBehavior>

                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>

                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>

                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro"
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar"
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Despacho" ReadOnly="true" Caption="Despacho"
                                            Visible="true" Width="105px" VisibleIndex="1">
                                            <EditFormSettings Visible="false" />
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataDateColumn FieldName="Fecha" ReadOnly="true" Caption="Fecha" Visible="false">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaEntrega" ReadOnly="false"
                                            Caption="Fecha Envío" Visible="true" Width="80px" VisibleIndex="2">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="Transportista" ReadOnly="false"
                                            Caption="Transportista" Visible="true" Width="300px" VisibleIndex="3">
                                            <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="300" DropDownRows="25" ValueField="Transportista" ValueType="System.Int32" TextField="Nombre" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Left">
                                                <border borderstyle="None"></border>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Articulos" ReadOnly="false"
                                            Caption="# Art." Visible="true" Width="20px"
                                            ToolTip="Ingrese aquí el número total de artículos válidos de cada envío"
                                            VisibleIndex="4">
                                            <PropertiesSpinEdit MinValue="0" MaxValue="100"></PropertiesSpinEdit>
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataCheckColumn FieldName="Cliente" ReadOnly="false"
                                            Caption="Cliente" Visible="true" Width="30px"
                                            ToolTip="Marque esta casilla si el cliente confirmó de recibida su mercadería"
                                            VisibleIndex="5">
                                            <PropertiesCheckEdit ValueType="System.String" ValueChecked="S" ValueUnchecked="N"></PropertiesCheckEdit>
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataTextColumn FieldName="NombreCliente" ReadOnly="true"
                                            Caption="Nombre Cliente / Fecha de entrega" Visible="true" Width="365px" VisibleIndex="6">
                                            <EditFormSettings Visible="false" />
                                            <HeaderStyle BackColor="#ff8a3f" />
                                            <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                            <CellStyle Font-Size="X-Small" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda"
                                            Visible="false" Width="40px" VisibleIndex="0">
                                            <EditFormSettings Visible="false" />
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsEditing Mode="Batch" />
                                    <ClientSideEvents BatchEditEndEditing="OnBatchEditEndEditing" />
                                    <SettingsBehavior EnableRowHotTrack="true" />
                                    <SettingsPager PageSize="20" Visible="False">
                                    </SettingsPager>
                                    <SettingsText SearchPanelEditorNullText="Ingrese aquí su criterio de búsqueda" CommandBatchEditUpdate="Aplicar cambios" CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" />
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>

                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>

                </li>
            </ul>

            <ul>
                <li>

                    <table class="dxflInternalEditorTable_SoftOrange">
                        <tr>
                            <td colspan="3">
                                <a>Modificar facturas para comisiones</a></td>
                            <td colspan="4" style="text-align: right">
                                <asp:Label ID="lbErrorModificaFactura" runat="server" Font-Bold="True"
                                    ForeColor="Red"></asp:Label>
                                <asp:Label ID="lbInfoModificaFactura" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>No. de factura:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtFacturaModifica" runat="server" Width="110px"
                                    CssClass="textBoxStyle" TabIndex="110"
                                    ToolTip="Ingrese el número de factura" AutoPostBack="True"
                                    OnTextChanged="txtFacturaModifica_TextChanged">
                                </dx:ASPxTextBox>
                            </td>
                            <td>Tipo de excepción:</td>
                            <td colspan="2">
                                <dx:ASPxComboBox ID="tipoExcepcion" runat="server" Theme="SoftOrange"
                                    CssClass="ComboBoxStyle" Width="200px"
                                    DropDownWidth="40px" DropDownRows="25" TabIndex="120" AutoPostBack="True"
                                    OnSelectedIndexChanged="tipoExcepcion_SelectedIndexChanged">
                                    <Items>
                                        <dx:ListEditItem Text="Modificar monto a comisionar" Value="M" />
                                        <dx:ListEditItem Text="No pagar comisión" Value="N" />
                                        <dx:ListEditItem Text="Eliminar excepción" Value="E" />
                                        <dx:ListEditItem Text="Pagar comisión" Value="P" />
                                        <dx:ListEditItem Text="Eliminar pago de comisión" Value="C" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Monto factura:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtMontoFactura" runat="server" Width="110px"
                                    CssClass="textBoxStyle" TabIndex="9999"
                                    ToolTip="Este es el monto de la factura con IVA incluído" ReadOnly="True">
                                </dx:ASPxTextBox>
                            </td>
                            <td>Monto a comisionar:</td>
                            <td colspan="2">
                                <dx:ASPxTextBox ID="txtMontoComision" runat="server" Width="200px"
                                    CssClass="textBoxStyle" TabIndex="130"
                                    ToolTip="Ingrese el a comisionar con IVA incluído">
                                </dx:ASPxTextBox>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Comisionable:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtMontoComisionable" runat="server" Width="110px"
                                    CssClass="textBoxStyle" TabIndex="9999"
                                    ToolTip="Este es el monto comisionable calculado por el sistema"
                                    ReadOnly="True">
                                </dx:ASPxTextBox>
                            </td>
                            <td>Mes y año:</td>
                            <td>
                                <dx:ASPxComboBox ID="mesPago" runat="server" Theme="SoftOrange"
                                    CssClass="ComboBoxStyle" Width="110px"
                                    DropDownWidth="40px" DropDownRows="25" TabIndex="132">
                                    <Items>
                                        <dx:ListEditItem Text="Enero" Value="1" />
                                        <dx:ListEditItem Text="Febrero" Value="2" />
                                        <dx:ListEditItem Text="Marzo" Value="3" />
                                        <dx:ListEditItem Text="Abril" Value="4" />
                                        <dx:ListEditItem Text="Mayo" Value="5" />
                                        <dx:ListEditItem Text="Junio" Value="6" />
                                        <dx:ListEditItem Text="Julio" Value="7" />
                                        <dx:ListEditItem Text="Agosto" Value="8" />
                                        <dx:ListEditItem Text="Septiembre" Value="9" />
                                        <dx:ListEditItem Text="Octubre" Value="10" />
                                        <dx:ListEditItem Text="Noviembre" Value="11" />
                                        <dx:ListEditItem Text="Diciembre" Value="12" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <dx:ASPxComboBox ID="anioPago" runat="server" Theme="SoftOrange"
                                    CssClass="ComboBoxStyle" Width="70px"
                                    DropDownWidth="40px" DropDownRows="20" TabIndex="134">
                                    <Items>
                                        <dx:ListEditItem Text="2016" Value="2016" />
                                        <dx:ListEditItem Text="2017" Value="2017" />
                                        <dx:ListEditItem Text="2018" Value="2018" />
                                        <dx:ListEditItem Text="2019" Value="2019" />
                                        <dx:ListEditItem Text="2020" Value="2020" />
                                        <dx:ListEditItem Text="2021" Value="2021" />
                                        <dx:ListEditItem Text="2022" Value="2022" />
                                        <dx:ListEditItem Text="2023" Value="2023" />
                                        <dx:ListEditItem Text="2024" Value="2024" />
                                        <dx:ListEditItem Text="2025" Value="2025" />
                                        <dx:ListEditItem Text="2026" Value="2026" />
                                        <dx:ListEditItem Text="2027" Value="2027" />
                                        <dx:ListEditItem Text="2028" Value="2028" />
                                        <dx:ListEditItem Text="2029" Value="2029" />
                                        <dx:ListEditItem Text="2030" Value="2030" />
                                        <dx:ListEditItem Text="2031" Value="2031" />
                                        <dx:ListEditItem Text="2032" Value="2032" />
                                        <dx:ListEditItem Text="2033" Value="2033" />
                                        <dx:ListEditItem Text="2034" Value="2034" />
                                        <dx:ListEditItem Text="2035" Value="2035" />
                                        <dx:ListEditItem Text="2036" Value="2036" />
                                        <dx:ListEditItem Text="2037" Value="2037" />
                                        <dx:ListEditItem Text="2038" Value="2038" />
                                        <dx:ListEditItem Text="2039" Value="2039" />
                                        <dx:ListEditItem Text="2040" Value="2040" />
                                        <dx:ListEditItem Text="2041" Value="2041" />
                                        <dx:ListEditItem Text="2042" Value="2042" />
                                        <dx:ListEditItem Text="2043" Value="2043" />
                                        <dx:ListEditItem Text="2044" Value="2044" />
                                        <dx:ListEditItem Text="2045" Value="2045" />
                                        <dx:ListEditItem Text="2046" Value="2046" />
                                        <dx:ListEditItem Text="2047" Value="2047" />
                                        <dx:ListEditItem Text="2048" Value="2048" />
                                        <dx:ListEditItem Text="2049" Value="2049" />
                                        <dx:ListEditItem Text="2050" Value="2050" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkGrabarModificaFactura" runat="server"
                                    ToolTip="Haga clic aquí para grabar los cambios en la factura"
                                    OnClick="lkGrabarModificaFactura_Click" TabIndex="150">Grabar</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" />
                        </tr>
                    </table>

                </li>
            </ul>

            <ul>
                <li>

                    <table>
                        <tr>
                            <td colspan="2">
                                <a>Comisiones de vendedores</a></td>
                            <td colspan="4" style="text-align: right">
                                <asp:Label ID="lbErrorComisiones" runat="server" Font-Bold="True"
                                    ForeColor="Red"></asp:Label>
                                <asp:Label ID="lbInfoComisiones" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>Mes:</td>
                            <td>
                                <dx:ASPxComboBox ID="mesComision" runat="server" Theme="SoftOrange"
                                    CssClass="ComboBoxStyle" Width="120px"
                                    DropDownWidth="40px" DropDownRows="25" TabIndex="210" AutoPostBack="True" OnSelectedIndexChanged="mesComision_SelectedIndexChanged">
                                    <Items>
                                        <dx:ListEditItem Text="Enero" Value="1" />
                                        <dx:ListEditItem Text="Febrero" Value="2" />
                                        <dx:ListEditItem Text="Marzo" Value="3" />
                                        <dx:ListEditItem Text="Abril" Value="4" />
                                        <dx:ListEditItem Text="Mayo" Value="5" />
                                        <dx:ListEditItem Text="Junio" Value="6" />
                                        <dx:ListEditItem Text="Julio" Value="7" />
                                        <dx:ListEditItem Text="Agosto" Value="8" />
                                        <dx:ListEditItem Text="Septiembre" Value="9" />
                                        <dx:ListEditItem Text="Octubre" Value="10" />
                                        <dx:ListEditItem Text="Noviembre" Value="11" />
                                        <dx:ListEditItem Text="Diciembre" Value="12" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <dx:ASPxComboBox ID="anioComision" runat="server" Theme="SoftOrange"
                                    CssClass="ComboBoxStyle" Width="70px"
                                    DropDownWidth="40px" DropDownRows="20" TabIndex="220" AutoPostBack="True" OnSelectedIndexChanged="anioComision_SelectedIndexChanged">
                                    <Items>
                                        <dx:ListEditItem Text="2016" Value="2016" />
                                        <dx:ListEditItem Text="2017" Value="2017" />
                                        <dx:ListEditItem Text="2018" Value="2018" />
                                        <dx:ListEditItem Text="2019" Value="2019" />
                                        <dx:ListEditItem Text="2020" Value="2020" />
                                        <dx:ListEditItem Text="2021" Value="2021" />
                                        <dx:ListEditItem Text="2022" Value="2022" />
                                        <dx:ListEditItem Text="2023" Value="2023" />
                                        <dx:ListEditItem Text="2024" Value="2024" />
                                        <dx:ListEditItem Text="2025" Value="2025" />
                                        <dx:ListEditItem Text="2026" Value="2026" />
                                        <dx:ListEditItem Text="2027" Value="2027" />
                                        <dx:ListEditItem Text="2028" Value="2028" />
                                        <dx:ListEditItem Text="2029" Value="2029" />
                                        <dx:ListEditItem Text="2030" Value="2030" />
                                        <dx:ListEditItem Text="2031" Value="2031" />
                                        <dx:ListEditItem Text="2032" Value="2032" />
                                        <dx:ListEditItem Text="2033" Value="2033" />
                                        <dx:ListEditItem Text="2034" Value="2034" />
                                        <dx:ListEditItem Text="2035" Value="2035" />
                                        <dx:ListEditItem Text="2036" Value="2036" />
                                        <dx:ListEditItem Text="2037" Value="2037" />
                                        <dx:ListEditItem Text="2038" Value="2038" />
                                        <dx:ListEditItem Text="2039" Value="2039" />
                                        <dx:ListEditItem Text="2040" Value="2040" />
                                        <dx:ListEditItem Text="2041" Value="2041" />
                                        <dx:ListEditItem Text="2042" Value="2042" />
                                        <dx:ListEditItem Text="2043" Value="2043" />
                                        <dx:ListEditItem Text="2044" Value="2044" />
                                        <dx:ListEditItem Text="2045" Value="2045" />
                                        <dx:ListEditItem Text="2046" Value="2046" />
                                        <dx:ListEditItem Text="2047" Value="2047" />
                                        <dx:ListEditItem Text="2048" Value="2048" />
                                        <dx:ListEditItem Text="2049" Value="2049" />
                                        <dx:ListEditItem Text="2050" Value="2050" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btConsultarComisionesCalendario" runat="server" Text="Comisiones mes calendario" AutoPostBack="false"
                                    RenderMode="Link"
                                    ToolTip="Haga clic aquí para consultar las comisiones mes calendario."
                                    ImagePosition="Left"
                                    OnClick="lkComisionesMesCalendario_Click" Visible="False">
                                    <Image IconID="actions_search_16x16devav"></Image>
                                </dx:ASPxButton>
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="4" style="text-align: center">
                                <dx:ASPxComboBox ID="tiendaComisiones" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle"
                                    ValueType="System.String" Width="5px" DropDownStyle="DropDownList"
                                    DropDownWidth="40" DropDownRows="25" TabIndex="9999" Visible="False">
                                </dx:ASPxComboBox>
                                <dx:ASPxComboBox ID="vendedorComisiones" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle"
                                    ValueType="System.String" Width="10px" DropDownStyle="DropDownList"
                                    DropDownWidth="40" DropDownRows="25" TabIndex="9999" Visible="False">
                                </dx:ASPxComboBox>
                                <dx:ASPxDateEdit ID="fechaInicialComisiones" runat="server" Theme="SoftOrange" DisplayFormatString="dd/MM/yyyy"
                                    CssClass="DateEditStyle" Width="140px" TabIndex="9999" Visible="False">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true"
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true"
                                        ShowWeekNumbers="False">
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                            <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                                <dx:ASPxDateEdit ID="fechaFinalComisiones" runat="server" Theme="SoftOrange" DisplayFormatString="dd/MM/yyyy"
                                    CssClass="DateEditStyle" Width="140px" TabIndex="9999" Visible="False">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true"
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true"
                                        ShowWeekNumbers="False">
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                            <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                                <div style="width: 100%; padding: 10px; text-align: left;">
                                    <dx:ASPxButton ID="btnGenerar" runat="server" Height="8px"
                                        Text="Generar" ToolTip="Generar información de comisiones de ventas" Width="60px" OnClick="btnGenerar_Click">
                                        <Image Url="~/images/GenerarComision.png" Width="20px" Height="20px">
                                        </Image>
                                    </dx:ASPxButton>
                                    <dx:ASPxButton ID="btnGrabar" runat="server" Height="8px"
                                        Text="Grabar" ToolTip="Grabar el cierre de comisiones" Width="60px" OnClick="btnGrabar_Click" Visible="false">
                                        <Image Url="~/images/Guardar.png" Width="20px" Height="20px">
                                        </Image>
                                    </dx:ASPxButton>
                                    <dx:ASPxButton ID="btnAplicar" runat="server" Height="8px"
                                        Text="Aplicar" ToolTip="Aplicar comisiones a nómina" Width="60px" OnClick="btnAplicar_Click" Visible="false">
                                        <Image Url="~/images/Comision.png" Width="20px" Height="20px">
                                        </Image>
                                    </dx:ASPxButton>
                                    <dx:ASPxButton ID="btnBorrarComision" runat="server" Height="8px"
                                        Text="Borrar" ToolTip="Borrar las comisiones" Width="60px" OnClick="btnBorrarComision_Click" Visible="false">
                                        <Image Url="~/images/Eliminar.png" Width="20px" Height="20px">
                                        </Image>
                                    </dx:ASPxButton>
                                    <dx:ASPxButton ID="btnResumen" runat="server" Height="8px"
                                        Text="Enviar" ToolTip="Enviar resumen de proceso de nómina" Width="60px" OnClick="btnResumen_Click" Visible="false">
                                        <Image Url="~/images/Email.png" Width="20px" Height="20px">
                                        </Image>
                                    </dx:ASPxButton>
                                </div>
                                <asp:LinkButton ID="lkSeleccionarTiendaComisiones" runat="server"
                                    ToolTip="Haga clic aquí para seleccionar documentos según la tienda seleccionada"
                                    OnClick="lkSeleccionarTiendaComisiones_Click" TabIndex="9999"
                                    Visible="False">St</asp:LinkButton>
                                <asp:LinkButton ID="lkVendedorComisiones" runat="server"
                                    ToolTip="Haga clic aquí para seleccionar las comisiones del vendedor seleccionado"
                                    TabIndex="9999" OnClick="lkVendedorComisiones_Click" Visible="False">PV</asp:LinkButton>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="4" style="text-align: center">
                                <dx:ToolbarExport runat="server" ID="ToolbarExport" ExportItemTypes="Com,Tien,Fac,Exc,Anu,Pend,All" OnItemClick="ToolbarExport_ItemClick" />
                                <dx:ASPxGridViewExporter ID="xprComisiones" runat="server" GridViewID="gridComisiones" OnRenderBrick="RenderBrick"></dx:ASPxGridViewExporter>
                                <dx:ASPxGridViewExporter ID="xprFacturas" runat="server" GridViewID="gridFacturas" OnRenderBrick="RenderBrick"></dx:ASPxGridViewExporter>
                                <dx:ASPxGridViewExporter ID="xprExcepciones" runat="server" GridViewID="gridExcepciones" OnRenderBrick="RenderBrick"></dx:ASPxGridViewExporter>
                                <dx:ASPxGridViewExporter ID="xprAnuladas" runat="server" GridViewID="gridAnuladas" OnRenderBrick="RenderBrick"></dx:ASPxGridViewExporter>
                                <dx:ASPxGridViewExporter ID="xprPendientes" runat="server" GridViewID="gridPendientes" OnRenderBrick="RenderBrick"></dx:ASPxGridViewExporter>
                                <dx:ASPxGridViewExporter ID="xprTiendas" runat="server" GridViewID="gridTiendas" OnRenderBrick="RenderBrick"></dx:ASPxGridViewExporter>
                                <dx:ASPxGridViewExporter ID="xprTiendaJefeSupervisor" runat="server" GridViewID="gridJefesSupervisores" OnRenderBrick="RenderBrick"></dx:ASPxGridViewExporter>
                                <dx:ASPxGridViewExporter ID="xprDetalleJefe" runat="server" GridViewID="gridDetalleComisionJefe" OnRenderBrick="RenderBrick"></dx:ASPxGridViewExporter>
                                <dx:ASPxGridViewExporter ID="xprResumenJefe" runat="server" GridViewID="gridResumenComisionJefe" OnRenderBrick="RenderBrick"></dx:ASPxGridViewExporter>
                                <dx:ASPxGridViewExporter ID="xprDetalleSupervisor" runat="server" GridViewID="gridDetalleComisionSupervisor" OnRenderBrick="RenderBrick"></dx:ASPxGridViewExporter>
                                <dx:ASPxGridViewExporter ID="xprResumenSupervisor" runat="server" GridViewID="gridResumenComisionSupervisor" OnRenderBrick="RenderBrick"></dx:ASPxGridViewExporter>
                                <dx:ASPxGridViewExporter ID="xprDesgloseComision" runat="server" GridViewID="gridDesgloceJefeSupervisor" OnRenderBrick="RenderBrick"></dx:ASPxGridViewExporter>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridComisiones" EnableTheming="True" Theme="SoftOrange"
                                    runat="server" KeyFieldName="Vendedor" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="280"
                                    Visible="False" OnHtmlRowPrepared="gridComisiones_HtmlRowPrepared"
                                    OnHtmlDataCellPrepared="gridComisiones_HtmlDataCellPrepared">
                                    <Styles>
                                        <StatusBar>
                                            <border borderstyle="None" />
                                        </StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro"
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar"
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="true" Width="75px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Vendedor" ReadOnly="true" Caption="Vendedor" Visible="true" Width="50px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreVendedor" ReadOnly="true" Caption="Nombre del vendedor" Visible="true" Width="260px">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" Caption="Valor" Visible="true" Width="110px" ToolTip="Valor total de las facturas">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MontoComisionable" ReadOnly="true" Caption="Comisionable" Visible="true" Width="110px" ToolTip="Monto comisionable">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ValorNeto" ReadOnly="true" Caption="Valor Neto" Visible="true" Width="110px" ToolTip="Valor neto del monto comisionable">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="true" Width="110px" ToolTip="Comisión a recibir">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Porcentaje" ReadOnly="true" Caption="Porcentaje" Visible="true" Width="75px" ToolTip="Porcentaje">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Titulo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Tooltip" Visible="false" Caption="Información">
                                            <CellStyle Wrap="False"></CellStyle>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                    <SettingsPager PageSize="500" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                    <%--                                    <TotalSummary>
                                        <dx:ASPxSummaryItem FieldName="NombreVendedor" SummaryType="Count" DisplayFormat="Total general: {0}" />
                                        <dx:ASPxSummaryItem FieldName="Valor" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="MontoComisionable" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ValorNeto" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </TotalSummary>--%>
                                    <GroupSummary>
                                        <dx:ASPxSummaryItem FieldName="NombreVendedor" ShowInGroupFooterColumn="NombreVendedor" SummaryType="Count" DisplayFormat="Cantidad vendedores: {0}" />
                                        <%--                                        <dx:ASPxSummaryItem FieldName="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="MontoComisionable" ShowInGroupFooterColumn="MontoComisionable" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ValorNeto" ShowInGroupFooterColumn="ValorNeto" SummaryType="Sum" DisplayFormat="{0:n2}" />--%>
                                        <dx:ASPxSummaryItem FieldName="Comision" ShowInGroupFooterColumn="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </GroupSummary>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridTiendas" EnableTheming="True" Theme="SoftOrange"
                                    runat="server" KeyFieldName="Tienda" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="285"
                                    Visible="False" OnHtmlRowPrepared="gridTiendas_HtmlRowPrepared">
                                    <Styles>
                                        <StatusBar>
                                            <border borderstyle="None" />
                                        </StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro"
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar"
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="true" Width="100px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" Caption="Valor" Visible="true" Width="200px" ToolTip="Valor total de las facturas">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MontoComisionable" ReadOnly="true" Caption="Comisionable" Visible="true" Width="200px" ToolTip="Monto comisionable">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ValorNeto" ReadOnly="true" Caption="Valor Neto" Visible="true" Width="200px" ToolTip="Valor neto del monto comisionable">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="true" Width="200px" ToolTip="Comisión a recibir">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Titulo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                    <SettingsPager PageSize="500" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                    <%--                                    <TotalSummary>
                                        <dx:ASPxSummaryItem FieldName="NombreVendedor" SummaryType="Count" DisplayFormat="Total general: {0}" />
                                        <dx:ASPxSummaryItem FieldName="Valor" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="MontoComisionable" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ValorNeto" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </TotalSummary>--%>
                                    <GroupSummary>
                                        <dx:ASPxSummaryItem FieldName="Tienda" ShowInGroupFooterColumn="Tienda" SummaryType="Count" DisplayFormat="Cantidad tiendas: {0}" />
                                        <dx:ASPxSummaryItem FieldName="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="MontoComisionable" ShowInGroupFooterColumn="MontoComisionable" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ValorNeto" ShowInGroupFooterColumn="ValorNeto" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="Comision" ShowInGroupFooterColumn="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </GroupSummary>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridFacturas" EnableTheming="True" Theme="SoftOrange"
                                    runat="server" KeyFieldName="Factura" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="290"
                                    Visible="False" OnHtmlRowPrepared="gridFacturas_HtmlRowPrepared">
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                    <SettingsPager PageSize="15000" Visible="False">
                                    </SettingsPager>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" />
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="false"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro"
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar"
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="CodigoSupervisor" ReadOnly="true" Caption="Código supervisor" Visible="False">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreSupervisor" ReadOnly="true" Caption="Nombre supervisor" Visible="False" VisibleIndex="0">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="CodigoJefe" ReadOnly="true" Caption="Código jefe" Visible="False">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreJefe" ReadOnly="true" Caption="Nombre jefe" Visible="False" VisibleIndex="1">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="CodigoVendedor" ReadOnly="true" Caption="Código vendedor" Visible="False">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreVendedor" ReadOnly="true" Caption="Nombre vendedor" Visible="False" VisibleIndex="2">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="False" VisibleIndex="3">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="NivelPrecio" Visible="False" VisibleIndex="4">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PorcentajeTarjeta" Visible="False" VisibleIndex="5">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="FormaDePago" Visible="False" VisibleIndex="6">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura" Visible="True" VisibleIndex="7" Width="240px">
                                            <DataItemTemplate>
                                                <dx:ASPxButton ID="lkFacturaComision" EncodeHtml="false" runat="server" AutoPostBack="false" Text='<%# Bind("Factura") %>' RenderMode="Link" OnClick="lkFacturaComision_Click" ToolTip="Haga clic aquí para seleccionar la factura y modificarle la comisión"></dx:ASPxButton>
                                            </DataItemTemplate>
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" Caption="Valor" Visible="False" Width="10px" ToolTip="Valor total de las facturas">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MontoComisionable" ReadOnly="true" Caption="Comisionable" Visible="True" Width="100px" ToolTip="Monto comisionable" VisibleIndex="8">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ValorNeto" ReadOnly="true" Caption="Valor Neto" Visible="True" Width="100px" ToolTip="Valor neto del monto comisionable" VisibleIndex="9">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="True" Width="100px" ToolTip="Comisión a recibir" VisibleIndex="10">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Porcentaje" ReadOnly="true" Caption="Porcentaje" Visible="True" Width="60px" ToolTip="Porcentaje" VisibleIndex="11">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaFactura" ReadOnly="true" Caption="Fecha Factura" Visible="True" Width="70px" VisibleIndex="12">
                                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaEntrega" ReadOnly="true" Caption="Fecha Entrega" Visible="True" Width="70px" VisibleIndex="13">
                                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn FieldName="Titulo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <GroupSummary>
                                        <dx:ASPxSummaryItem FieldName="Factura" ShowInGroupFooterColumn="Factura" SummaryType="Count" DisplayFormat="Cantidad facturas: {0}" />
                                        <dx:ASPxSummaryItem FieldName="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="MontoComisionable" ShowInGroupFooterColumn="MontoComisionable" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ValorNeto" ShowInGroupFooterColumn="ValorNeto" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="Comision" ShowInGroupFooterColumn="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </GroupSummary>
                                    <Styles>
                                        <StatusBar>
                                            <border borderstyle="None" />
                                        </StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridExcepciones" EnableTheming="True" Theme="SoftOrange"
                                    runat="server" KeyFieldName="Factura" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="300"
                                    Visible="False" OnHtmlRowPrepared="gridExcepciones_HtmlRowPrepared">
                                    <Styles>
                                        <StatusBar>
                                            <border borderstyle="None" />
                                        </StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="false"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro"
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar"
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="false">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Vendedor" ReadOnly="true" Caption="Vendedor" Visible="true" Width="50px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreVendedor" ReadOnly="true" Caption="Nombre del vendedor" Visible="true" Width="140px">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura" Visible="true" Width="50px">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" Caption="Valor" Visible="true" Width="90px" ToolTip="Valor total de las facturas">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MontoComisionable" ReadOnly="true" Caption="Comisionable" Visible="true" Width="90px" ToolTip="Monto comisionable">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ValorNeto" ReadOnly="true" Caption="Valor Neto" Visible="true" Width="90px" ToolTip="Valor neto del monto comisionable">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="false" ToolTip="Comisión a recibir">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Porcentaje" ReadOnly="true" Caption="Porcentaje" Visible="false" ToolTip="Porcentaje">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaFactura" ReadOnly="true" Caption="Fecha Factura" Visible="false">
                                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaEntrega" ReadOnly="true" Caption="Fecha Entrega" Visible="false">
                                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn FieldName="Tipo" Visible="true" Width="390px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle Wrap="False"></CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Titulo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                    <SettingsPager PageSize="15000" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                    <GroupSummary>
                                        <dx:ASPxSummaryItem FieldName="Factura" ShowInGroupFooterColumn="Factura" SummaryType="Count" DisplayFormat="Cantidad facturas: {0}" />
                                        <dx:ASPxSummaryItem FieldName="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="MontoComisionable" ShowInGroupFooterColumn="MontoComisionable" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ValorNeto" ShowInGroupFooterColumn="ValorNeto" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="Comision" ShowInGroupFooterColumn="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </GroupSummary>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridAnuladas" EnableTheming="True" Theme="SoftOrange"
                                    runat="server" KeyFieldName="Factura" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="310"
                                    Visible="False" OnHtmlRowPrepared="gridAnuladas_HtmlRowPrepared">
                                    <Styles>
                                        <StatusBar>
                                            <border borderstyle="None" />
                                        </StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="false"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro"
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar"
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="Hidden" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="false">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Vendedor" ReadOnly="true" Caption="Vendedor" Visible="true" Width="50px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreVendedor" ReadOnly="true" Caption="Nombre del vendedor" Visible="true" Width="430px">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura" Visible="true" Width="270px">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" Caption="Valor" Visible="true" Width="100px" ToolTip="Valor total de las facturas">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MontoComisionable" ReadOnly="true" Caption="Comisionable" Visible="false" ToolTip="Monto comisionable">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ValorNeto" ReadOnly="true" Caption="Valor Neto" Visible="false" ToolTip="Valor neto del monto comisionable">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="false" ToolTip="Comisión a recibir">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Porcentaje" ReadOnly="true" Caption="Porcentaje" Visible="false" ToolTip="Porcentaje">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaFactura" ReadOnly="true" Caption="Fecha Factura" Visible="true" Width="70px">
                                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaEntrega" ReadOnly="true" Caption="Fecha Entrega" Visible="false">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn FieldName="Titulo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                    <SettingsPager PageSize="15000" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridPendientes" EnableTheming="True" Theme="SoftOrange"
                                    runat="server" KeyFieldName="Factura" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="320"
                                    Visible="False" OnHtmlRowPrepared="gridPendientes_HtmlRowPrepared">
                                    <Styles>
                                        <StatusBar>
                                            <border borderstyle="None" />
                                        </StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="false"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro"
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar"
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="false">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Vendedor" ReadOnly="true" Caption="Vendedor"
                                            Visible="false" Width="50px" VisibleIndex="0">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreVendedor" ReadOnly="true"
                                            Caption="Nombre del vendedor" Visible="false" VisibleIndex="1">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura"
                                            Visible="true" Width="160px" VisibleIndex="5">
                                            <DataItemTemplate>
                                                <dx:ASPxButton ID="lkFacturaPendiente" EncodeHtml="false" runat="server" AutoPostBack="false" Text='<%# Bind("Factura") %>' RenderMode="Link" OnClick="lkFacturaPendiente_Click" ToolTip="Haga clic aquí para excluir la factura de las pendientes"></dx:ASPxButton>
                                            </DataItemTemplate>
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Tipo" ReadOnly="true" Caption="Tipo" Visible="true" VisibleIndex="5">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Estado" ReadOnly="true" Caption="Estado" Visible="true" VisibleIndex="5">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" Caption="Valor" Visible="false" Width="100px" ToolTip="Valor total de las facturas" VisibleIndex="6">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MontoComisionable" ReadOnly="true"
                                            Caption="Comisionable" Visible="true" Width="80px"
                                            ToolTip="Monto comisionable" VisibleIndex="7">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ValorNeto" ReadOnly="true"
                                            Caption="Valor Neto" Visible="true" Width="80px"
                                            ToolTip="Valor neto del monto comisionable" VisibleIndex="8">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true"
                                            Caption="Comisión" Visible="false" ToolTip="Comisión a recibir"
                                            VisibleIndex="2">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Porcentaje" ReadOnly="true"
                                            Caption="Porcentaje" Visible="false" ToolTip="Porcentaje" VisibleIndex="3">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaFactura" ReadOnly="true"
                                            Caption="Fecha Factura" Visible="true" Width="70px" VisibleIndex="9">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaEntrega" ReadOnly="true"
                                            Caption="Fecha Entrega" Visible="true" Width="70px" VisibleIndex="10">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn FieldName="Titulo" Visible="false" VisibleIndex="4">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                    <SettingsPager PageSize="15000" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                    <GroupSummary>
                                        <dx:ASPxSummaryItem FieldName="Factura" ShowInGroupFooterColumn="Factura" SummaryType="Count" DisplayFormat="Cantidad facturas: {0}" />
                                        <dx:ASPxSummaryItem FieldName="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="MontoComisionable" ShowInGroupFooterColumn="MontoComisionable" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ValorNeto" ShowInGroupFooterColumn="ValorNeto" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="Comision" ShowInGroupFooterColumn="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </GroupSummary>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridJefesSupervisores" EnableTheming="True" Theme="SoftOrange"
                                    runat="server" KeyFieldName="Tienda" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="320"
                                    Visible="False" OnHtmlRowPrepared="gridJefesSupervisores_HtmlRowPrepared">
                                    <Styles>
                                        <StatusBar>
                                            <border borderstyle="None" />
                                        </StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro"
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar"
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="true" ShowGroupFooter="VisibleIfExpanded" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Supervisor" ReadOnly="true" Caption="Supervisor" Visible="false" Width="80px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Left" />
                                            <CellStyle HorizontalAlign="Left" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Categoria" ReadOnly="true" Caption="Categoría" Visible="true" VisibleIndex="1" Width="70px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="true" VisibleIndex="2" Width="60px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Ubicacion" ReadOnly="true" Caption="Ubicación" Visible="true" VisibleIndex="3" Width="180px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Left" />
                                            <CellStyle HorizontalAlign="Left" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Jefe" ReadOnly="true" Caption="Jefe" Visible="false" Width="130px">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="CodigoSupervisor" ReadOnly="true" Caption="CodigoSupervisor" Visible="false">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="CodigoJefe" ReadOnly="true" Caption="CodigoJefe" Visible="false">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="VentasSinIVA" ReadOnly="true" Caption="Ventas Sin IVA" Visible="true" Width="100px" ToolTip="Ventas sin IVA" VisibleIndex="4">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                            <FooterCellStyle ForeColor="Black" Font-Bold="true" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="true" Width="100px" ToolTip="Comisión del jefe como vendedor" VisibleIndex="5">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                            <FooterCellStyle ForeColor="Black" Font-Bold="true" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="VentasTiendaSinIVA" ReadOnly="true" Caption="Ventas Tienda Sin IVA" Visible="true" ToolTip="Ventas de la tienda sin IVA" VisibleIndex="6" Width="150px">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                            <FooterCellStyle ForeColor="Black" Font-Bold="true" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Minimo" ReadOnly="true" Caption="Mínimo" Visible="true" ToolTip="Mínimo de la tienda" VisibleIndex="7">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Meta" ReadOnly="true" Caption="Meta" Visible="true" ToolTip="Meta de la tienda" VisibleIndex="8">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="ResultadoTienda" ReadOnly="true" Caption="Resultado Tienda" Visible="true" VisibleIndex="9" Width="120px" ExportWidth="110">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="VentaComisionarJefe" ReadOnly="true" Caption="Venta Comisionar Jefe" Visible="false" ToolTip="Venta a comisionar jefe" Width="140px">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                            <FooterCellStyle ForeColor="Black" Font-Bold="true" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PctjJefe" ReadOnly="true" Caption="% Jefe" Visible="true" ToolTip="% a comisionar del jefe" VisibleIndex="10" Width="60px">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ComisionJefe" ReadOnly="true" Caption="Comisión Jefe" Visible="false" ToolTip="Comisión jefe" Width="100px">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                            <FooterCellStyle ForeColor="Black" Font-Bold="true" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ComisionJefeTotal" ReadOnly="true" Caption="Comisión Jefe Total" Visible="false" ToolTip="Comisión jefe total" Width="120px">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                            <FooterCellStyle ForeColor="Black" Font-Bold="true" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PctjSupervisor" ReadOnly="true" Caption="% Supervisor" Visible="true" ToolTip="% a comisionar del supervisor" VisibleIndex="11" Width="90px">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ComisionSupervisor" ReadOnly="true" Caption="Comisión Supervisor" Visible="false" ToolTip="Comisión supervisor" Width="130px">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                            <FooterCellStyle ForeColor="Black" Font-Bold="true" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PctjSupervisor2" ReadOnly="true" Caption="% Supervisor 2" Visible="true" ToolTip="% a comisionar del supervisor 2" VisibleIndex="12" Width="90px">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Titulo" Visible="false" VisibleIndex="32">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <Settings HorizontalScrollBarMode="Visible"></Settings>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                    <SettingsPager PageSize="15000" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                    <TotalSummary>
                                        <dx:ASPxSummaryItem FieldName="VentasSinIVA" ShowInColumn="VentasSinIVA" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="Comision" ShowInColumn="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="VentasTiendaSinIVA" ShowInColumn="VentasTiendaSinIVA" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="VentaComisionarJefe" ShowInColumn="VentaComisionarJefe" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <%--<dx:ASPxSummaryItem FieldName="ComisionJefe" ShowInColumn="ComisionJefe" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ComisionJefeTotal" ShowInColumn="ComisionJefeTotal" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ComisionSupervisor" ShowInColumn="ComisionSupervisor" SummaryType="Sum" DisplayFormat="{0:n2}" />--%>
                                    </TotalSummary>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridDesgloceJefeSupervisor" EnableTheming="True" Theme="SoftOrange"
                                    runat="server" KeyFieldName="Tienda" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="320"
                                    Visible="False" OnHtmlRowPrepared="gridDesgloceJefeSupervisor_HtmlRowPrepared">
                                    <Styles>
                                        <StatusBar>
                                            <border borderstyle="None" />
                                        </StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro"
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar"
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="true" ShowGroupFooter="VisibleIfExpanded" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="CodigoSupervisor" ReadOnly="true" Caption="Código supervisor" Visible="False">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreSupervisor" ReadOnly="true" Caption="Nombre supervisor" Visible="True" VisibleIndex="0">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Categoria" ReadOnly="true" Caption="Categoría" Visible="True" VisibleIndex="1">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="True" VisibleIndex="2">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Ubicacion" ReadOnly="true" Caption="Ubicación" Visible="True" VisibleIndex="3">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Left" />
                                            <CellStyle HorizontalAlign="Left" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="CodigoJefe" ReadOnly="true" Caption="Código jefe"  Visible="False">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreJefe" ReadOnly="true" Caption="Nombre jefe" Visible="True" VisibleIndex="4">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="VentasSinIVA" ReadOnly="true" Caption="Ventas Sin IVA" Visible="True" VisibleIndex="5">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                            <FooterCellStyle ForeColor="Black" Font-Bold="true" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="True" VisibleIndex="6">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                            <FooterCellStyle ForeColor="Black" Font-Bold="true" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="VentasTiendaSinIVA" ReadOnly="true" Caption="Ventas Tienda Sin IVA" Visible="True" VisibleIndex="7">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                            <FooterCellStyle ForeColor="Black" Font-Bold="true" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Minimo" ReadOnly="true" Caption="Mínimo" Visible="True" VisibleIndex="8">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Meta" ReadOnly="true" Caption="Meta" Visible="True" VisibleIndex="9">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="ResultadoTienda" ReadOnly="true" Caption="Resultado Tienda" Visible="True" VisibleIndex="10">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="VentaComisionarJefe" ReadOnly="true" Caption="Venta Comisionar Jefe" Visible="True" VisibleIndex="11">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                            <FooterCellStyle ForeColor="Black" Font-Bold="true" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PctDividido" ReadOnly="true" Caption="% Dividido" Visible="True" VisibleIndex="12">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PctTiendaJefe" ReadOnly="true" Caption="% Tienda jefe" Visible="True" VisibleIndex="13">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ComisionJefe" ReadOnly="true" Caption="Comisión jefe" Visible="True" VisibleIndex="14">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                            <FooterCellStyle ForeColor="Black" Font-Bold="true" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ComisionJefeTotal" ReadOnly="true" Caption="Comisión jefe total" Visible="True" VisibleIndex="15">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                            <FooterCellStyle ForeColor="Black" Font-Bold="true" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PctTiendaS1" ReadOnly="true" Caption="% Tienda supervisor"  Visible="True" VisibleIndex="16">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ComisionS1" ReadOnly="true" Caption="Comisión supervisor"  Visible="True" VisibleIndex="17">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                            <FooterCellStyle ForeColor="Black" Font-Bold="true" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PctTiendaS2" ReadOnly="true" Caption="% Tienda supervisor 2"  Visible="True" VisibleIndex="18">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ComisionS2" ReadOnly="true" Caption="Comisión supervisor 2"  Visible="True" VisibleIndex="19">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                            <FooterCellStyle ForeColor="Black" Font-Bold="true" />
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <Settings HorizontalScrollBarMode="Visible"></Settings>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                    <SettingsPager PageSize="15000" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                    <TotalSummary>
                                        <dx:ASPxSummaryItem FieldName="VentasSinIVA" ShowInColumn="VentasSinIVA" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="Comision" ShowInColumn="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="VentasTiendaSinIVA" ShowInColumn="VentasTiendaSinIVA" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="VentaComisionarJefe" ShowInColumn="VentaComisionarJefe" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ComisionJefe" ShowInColumn="ComisionJefe" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ComisionJefeTotal" ShowInColumn="ComisionJefeTotal" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ComisionS1" ShowInColumn="ComisionS1" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ComisionS2" ShowInColumn="ComisionS2" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </TotalSummary>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridDetalleComisionJefe" EnableTheming="True" Theme="SoftOrange"
                                    runat="server" KeyFieldName="Tienda" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="320"
                                    Visible="False" OnHtmlRowPrepared="gridDetalleComisionJefe_HtmlRowPrepared">
                                    <Styles>
                                        <StatusBar>
                                            <border borderstyle="None" />
                                        </StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro"
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar"
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="true" ShowGroupFooter="VisibleIfExpanded" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="True" VisibleIndex="0">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="CodigoJefe" ReadOnly="true" Caption="Código jefe" Visible="False">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreJefe" ReadOnly="true" Caption="Nombre jefe" Visible="True" VisibleIndex="1">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="PctDividido" ReadOnly="true" Caption="% Dividido" Visible="True" VisibleIndex="2">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PctTiendaJefe" ReadOnly="true" Caption="% Tienda Jefe" Visible="True" VisibleIndex="3">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ComisionJefe" ReadOnly="true" Caption="Comisión jefe" Visible="True" VisibleIndex="4">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ComisionJefeTotal" ReadOnly="true" Caption="Comisión jefe total" Visible="True" VisibleIndex="5">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <Settings HorizontalScrollBarMode="Visible"></Settings>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                    <SettingsPager PageSize="15000" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                    <TotalSummary>
                                        <dx:ASPxSummaryItem FieldName="ComisionJefe" ShowInColumn="ComisionJefe" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ComisionJefeTotal" ShowInColumn="ComisionJefeTotal" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </TotalSummary>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridResumenComisionJefe" EnableTheming="True" Theme="SoftOrange"
                                    runat="server" KeyFieldName="Tienda" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="320"
                                    Visible="False" OnHtmlRowPrepared="gridResumenComisionJefe_HtmlRowPrepared">
                                    <Styles>
                                        <StatusBar>
                                            <border borderstyle="None" />
                                        </StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro"
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar"
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="true" ShowGroupFooter="VisibleIfExpanded" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="CodigoJefe" ReadOnly="true" Caption="Código jefe" Visible="False">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreJefe" ReadOnly="true" Caption="Nombre jefe" Visible="True" VisibleIndex="1">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="True" VisibleIndex="2">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <Settings HorizontalScrollBarMode="Visible"></Settings>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                    <SettingsPager PageSize="15000" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                    <TotalSummary>
                                        <dx:ASPxSummaryItem FieldName="Comision" ShowInColumn="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </TotalSummary>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridDetalleComisionSupervisor" EnableTheming="True" Theme="SoftOrange"
                                    runat="server" KeyFieldName="Tienda" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="320"
                                    Visible="False" OnHtmlRowPrepared="gridDetalleComisionSupervisor_HtmlRowPrepared">
                                    <Styles>
                                        <StatusBar>
                                            <border borderstyle="None" />
                                        </StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro"
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar"
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="true" ShowGroupFooter="VisibleIfExpanded" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="True" VisibleIndex="0">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="CodigoSupervisor" ReadOnly="true" Caption="Código supervisor" Visible="False">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreSupervisor" ReadOnly="true" Caption="Nombre supervisor" Visible="True" VisibleIndex="1">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="PctDividido" ReadOnly="true" Caption="% Dividido" Visible="True" VisibleIndex="2">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PctTiendaS1" ReadOnly="true" Caption="% Tienda supervisor"  Visible="True" VisibleIndex="3">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ComisionS1" ReadOnly="true" Caption="Comisión supervisor" Visible="True" VisibleIndex="4">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PctTiendaS2" ReadOnly="true" Caption="% Tienda supervisor 2" Visible="True" VisibleIndex="5">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ComisionS2" ReadOnly="true" Caption="Comisión supervisor 2" Visible="True" VisibleIndex="6">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <Settings HorizontalScrollBarMode="Visible"></Settings>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                    <SettingsPager PageSize="15000" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                    <TotalSummary>
                                        <dx:ASPxSummaryItem FieldName="ComisionS1" ShowInColumn="ComisionS1" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ComisionS2" ShowInColumn="ComisionS2" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </TotalSummary>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridResumenComisionSupervisor" EnableTheming="True" Theme="SoftOrange"
                                    runat="server" KeyFieldName="Tienda" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="320"
                                    Visible="False" OnHtmlRowPrepared="gridResumenComisionSupervisor_HtmlRowPrepared">
                                    <Styles>
                                        <StatusBar>
                                            <border borderstyle="None" />
                                        </StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro"
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar"
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="true" ShowGroupFooter="VisibleIfExpanded" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="CodigoSupervisor" ReadOnly="true" Caption="Código supervisor" Visible="False">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreSupervisor" ReadOnly="true" Caption="Nombre supervisor" Visible="True" VisibleIndex="1">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="True" VisibleIndex="4">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="Titulo" ReadOnly="true" Caption=" " Visible="True" VisibleIndex="1">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Porcentaje" ReadOnly="true" Caption="Porcentaje" Visible="True" VisibleIndex="2">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <Settings HorizontalScrollBarMode="Visible"></Settings>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                    <SettingsPager PageSize="15000" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                    <TotalSummary>
                                        <dx:ASPxSummaryItem FieldName="Comision" ShowInColumn="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </TotalSummary>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right" colspan="6">
                                <asp:LinkButton ID="lkExportarTodo" runat="server"
                                    ToolTip="Haga clic aquí para exportar todas las comisiones a EXCEL"
                                    OnClick="lkExportarTodo_Click" TabIndex="352">Exportar todo a EXCEL</asp:LinkButton>
                            </td>
                        </tr>
                    </table>

                </li>
            </ul>

            <ul>
                <li>
                    <table>
                        <tr>
                            <td colspan="4">
                                <a>Facturar pedidos</a>
                            </td>
                            <td colspan="5" style="text-align: right">
                                <asp:Label ID="lbErrorFacturar" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                <asp:Label ID="lbInfoFacturar" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>Rango de fechas de entrega:
                            </td>
                            <td colspan="2">
                                <dx:ASPxDateEdit ID="fechaInicialPedidos" runat="server" Theme="SoftOrange" DisplayFormatString="dd/MM/yyyy"
                                    CssClass="DateEditStyle" Width="140px" TabIndex="340">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true"
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true"
                                        ShowWeekNumbers="False">
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                            <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaFinalPedidos" runat="server" Theme="SoftOrange" DisplayFormatString="dd/MM/yyyy"
                                    CssClass="DateEditStyle" Width="110px" TabIndex="343">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true"
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true"
                                        ShowWeekNumbers="False">
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                            <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td colspan="3">
                                <asp:LinkButton ID="lkSeleccionarFechasPedidos" runat="server"
                                    ToolTip="Haga clic aquí para seleccionar pedidos según la fecha de entrega de los mismos"
                                    OnClick="lkSeleccionarFechasPedidos_Click" TabIndex="346">Por fechas de entrega</asp:LinkButton>
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Código o nombre del cliente:</td>
                            <td colspan="2">
                                <dx:ASPxTextBox ID="txtClientePedido" runat="server" Width="140px" Style="text-transform: uppercase;"
                                    CssClass="textBoxStyle" TabIndex="349" AutoPostBack="True"
                                    ToolTip="Ingrese parte del código o parte del nombre para buscar"
                                    OnTextChanged="txtClientePedido_TextChanged">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkSeleccionarClientePedido" runat="server"
                                    ToolTip="Haga clic aquí para seleccionar pedidos según el cliente ingresado"
                                    OnClick="lkSeleccionarClientePedido_Click" TabIndex="352">Seleccionar cliente</asp:LinkButton>
                            </td>
                            <td colspan="3">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Número de pedido a buscar:</td>
                            <td colspan="2">
                                <dx:ASPxTextBox ID="txtPedidoFactura" runat="server" Width="140px"
                                    CssClass="textBoxStyle" TabIndex="355"
                                    ToolTip="Ingrese el número de pedido">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkCargarPedido" runat="server"
                                    ToolTip="Haga clic aquí para cargar los artículos del pedido"
                                    OnClick="lkCargarPedido_Click" TabIndex="358">Seleccionar pedido</asp:LinkButton>
                            </td>
                            <td colspan="3"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td colspan="2">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td colspan="3">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Código y nombre del cliente:</td>
                            <td colspan="6">
                                <dx:ASPxTextBox ID="txtNombreClientePedido" runat="server" Width="430px" TabIndex="9999"
                                    ToolTip="Este es el código y el nombre del cliente" ReadOnly="True">
                                </dx:ASPxTextBox>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Teléfonos del cliente:</td>
                            <td colspan="2">
                                <dx:ASPxTextBox ID="txtTelefonosClientePedido" runat="server" Width="140px"
                                    TabIndex="9999"
                                    ToolTip="Este es el código y el nombre del cliente" ReadOnly="True"
                                    Font-Size="X-Small">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <strong>Fecha de entrega:</strong></td>
                            <td colspan="3">
                                <dx:ASPxDateEdit ID="fechaEntregaPedido" runat="server" ReadOnly="false" Theme="SoftOrange" DisplayFormatString="dd/MM/yyyy"
                                    Width="145px" TabIndex="361" Font-Bold="True">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true"
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true"
                                        ShowWeekNumbers="False">
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                            <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Dirección del cliente:</td>
                            <td colspan="6">
                                <dx:ASPxTextBox ID="txtDireccionPedido" runat="server" Width="430px"
                                    CssClass="textBoxStyle" TabIndex="9999"
                                    ToolTip="Esta es la dirección que saldrá impresa en el despacho"
                                    ReadOnly="True" Font-Size="X-Small">
                                </dx:ASPxTextBox>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Monto del flete:</td>
                            <td>
                                <dx:ASPxSpinEdit ID="txtFlete" runat="server" DecimalPlaces="2" CssClass="textBoxStyle" Width="95px" TabIndex="362">
                                    <SpinButtons ShowIncrementButtons="false" />
                                </dx:ASPxSpinEdit>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnFlete" runat="server" Width="8px" Height="8px"
                                    ToolTip="Haga clic aquí para agregar el flete" OnClick="btnFlete_Click" TabIndex="363">
                                    <Image IconID="maps_transit_16x16">
                                    </Image>
                                </dx:ASPxButton>
                            </td>
                            <td>Monto del seguro:</td>
                            <td>
                                <dx:ASPxSpinEdit ID="txtSeguro" runat="server" DecimalPlaces="2" CssClass="textBoxStyle" Width="105px" TabIndex="365">
                                    <SpinButtons ShowIncrementButtons="false" />
                                </dx:ASPxSpinEdit>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnSeguro" runat="server" Width="8px" Height="8px"
                                    ToolTip="Haga clic aquí para agregar el seguro" OnClick="btnSeguro_Click" TabIndex="366">
                                    <Image IconID="actions_add_16x16">
                                    </Image>
                                </dx:ASPxButton>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Orden de compra:</td>
                            <td colspan="6">
                                <dx:ASPxTextBox ID="txtOrdenCompra" runat="server" Width="430px" TabIndex="367"
                                    ToolTip="Ingrese el número de órden de compra" ReadOnly="false">
                                </dx:ASPxTextBox>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="9">
                                <dx:ASPxGridView ID="gridPedidoDet" EnableTheming="True" Theme="SoftOrange" Visible="False"
                                    runat="server" KeyFieldName="Linea" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="940px" CssClass="dxGrid"
                                    OnBatchUpdate="gridPedidoDet_BatchUpdate"
                                    OnRowUpdating="gridPedidoDet_RowUpdating"
                                    OnCommandButtonInitialize="gridPedidoDet_CommandButtonInitialize"
                                    OnCellEditorInitialize="gridPedidoDet_CellEditorInitialize"
                                    OnHtmlDataCellPrepared="gridPedidoDet_HtmlDataCellPrepared"
                                    OnHtmlRowPrepared="gridPedidoDet_HtmlRowPrepared" TabIndex="368">
                                    <Styles>
                                        <StatusBar>
                                            <border borderstyle="None" />
                                        </StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AllowSort="false"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro"
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar"
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowFooter="true" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Pedido" ReadOnly="true" Caption="Pedido"
                                            Visible="false" Width="70px">
                                            <EditFormSettings Visible="false" />
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Linea" ReadOnly="true" Caption="Línea" Visible="false" Width="25px">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Articulo" ReadOnly="false" Caption="Artículo" Visible="true" Width="110px" VisibleIndex="9">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Descripcion" ReadOnly="true" Caption="Descripción" Visible="true" Width="510px" VisibleIndex="10">
                                            <EditFormSettings Visible="False" />
                                            <DataItemTemplate>
                                                <dx:ASPxLabel ID="lbDescripcion" EncodeHtml="false" runat="server" AutoPostBack="false" Text='<%# Bind("Descripcion") %>' Font-Size="X-Small">
                                                </dx:ASPxLabel>
                                            </DataItemTemplate>
                                            <HeaderStyle BackColor="#ff8a3f" />
                                            <CellStyle Font-Size="X-Small" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PrecioBase" ReadOnly="false" Caption="P Unit sin IVA" Visible="true" Width="90px" VisibleIndex="14">
                                            <PropertiesTextEdit DisplayFormatString="n6" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PrecioUnitario" ReadOnly="true" Caption="Precio Unitario" Visible="true" Width="75px" VisibleIndex="16">
                                            <PropertiesTextEdit DisplayFormatString="n6" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="Cantidad" ReadOnly="false" Caption="Cantidad" Visible="true" Width="15px" VisibleIndex="18">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="PrecioTotal" ReadOnly="true" Caption="Precio Total" Visible="true" Width="70px" VisibleIndex="20">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="Bodega" ReadOnly="false"
                                            Caption="Bodega" Visible="true" Width="20px" VisibleIndex="22">
                                            <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="40" DropDownRows="25" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center">
                                                <border borderstyle="None"></border>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="Localizacion" ReadOnly="false"
                                            Caption="Localización" Visible="true" Width="50px" VisibleIndex="24">
                                            <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="40" DropDownRows="5" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center">
                                                <border borderstyle="None"></border>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataTextColumn FieldName="Tooltip" Visible="false" VisibleIndex="1">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Existencias" Visible="false" VisibleIndex="2">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Tipo" Visible="false" VisibleIndex="3">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="NombreCliente" Visible="false" VisibleIndex="4">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Telefonos" Visible="false" VisibleIndex="5">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Direccion" Visible="false" VisibleIndex="6">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="FechaEntrega" Visible="false" VisibleIndex="7">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PrecioBase" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Iva" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                         <dx:GridViewDataTextColumn FieldName="Monto_Descuento" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Cliente" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <TotalSummary>
                                        <dx:ASPxSummaryItem FieldName="PrecioTotal" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </TotalSummary>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsEditing Mode="Batch" />
                                    <ClientSideEvents BatchEditEndEditing="OnBatchEditEndEditing" />
                                    <SettingsBehavior EnableRowHotTrack="true" />
                                    <SettingsPager PageSize="20" Visible="False">
                                    </SettingsPager>
                                    <SettingsText SearchPanelEditorNullText="Ingrese aquí su criterio de búsqueda" CommandBatchEditUpdate="Aplicar cambios" CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" />
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Fecha de la factura:</td>
                            <td colspan="2">
                                <dx:ASPxDateEdit ID="fechaFactura" runat="server" ReadOnly="false" Theme="SoftOrange" DisplayFormatString="dd/MM/yyyy"
                                    Width="140px" TabIndex="373" Font-Bold="True">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true"
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true"
                                        ShowWeekNumbers="False">
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                            <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td colspan="3">&nbsp;</td>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td style="text-align: center" colspan="2">
                                <dx:ASPxComboBox ID="cbSerie" runat="server" Theme="SoftOrange"
                                    CssClass="ComboBoxStyle" Width="140px"
                                    DropDownWidth="40px" DropDownRows="25" TabIndex="375" SelectedIndex="0"
                                    AutoPostBack="True" OnSelectedIndexChanged="cbSerie_SelectedIndexChanged">
                                    <Items>
                                        <dx:ListEditItem Selected="True" Text="Seleccione serie" Value="" />
                                        <dx:ListEditItem Text="Serie A" Value="A" />
                                        <dx:ListEditItem Text="Serie Cem" Value="CEM" />
                                        <dx:ListEditItem Text="Serie EX" Value="EX" />
                                        <dx:ListEditItem Text="Serie F1A" Value="F1A" />
                                        <dx:ListEditItem Text="Serie SI" Value="SI" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>
                            <td style="text-align: right" colspan="3">
                                <dx:ASPxTextBox ID="txtNumeroFactura" runat="server" Width="285px"
                                    CssClass="textBoxStyle" TabIndex="376"
                                    ToolTip="Ingrese el número de factura">
                                </dx:ASPxTextBox>
                            </td>
                            <td colspan="2" style="text-align: center">
                                <dx:ASPxButton ID="btnFacturar" runat="server" Width="8px" Height="8px"
                                    ToolTip="Generar la factura" OnClick="btnFacturar_Click" TabIndex="377">
                                    <Image IconID="save_saveall_16x16office2013">
                                    </Image>
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="btnImprimirFactura" runat="server" Width="8px" Height="8px"
                                    ToolTip="Imprimir la factura" OnClick="btnImprimirFactura_Click"
                                    TabIndex="378">
                                    <Image IconID="print_print_16x16office2013">
                                    </Image>
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="btnLimpiarPedido" runat="server" Height="8px"
                                    OnClick="btnLimpiarPedido_Click" TabIndex="379"
                                    ToolTip="Limpiar la pantalla de pedidos" Width="8px">
                                    <Image IconID="actions_clear_16x16">
                                    </Image>
                                </dx:ASPxButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="9">
                                <dx:ASPxGridView ID="gridPedidos" EnableTheming="True" Theme="SoftOrange"
                                    runat="server" KeyFieldName="Pedido" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid"
                                    OnFocusedRowChanged="gridPedidos_FocusedRowChanged" TabIndex="382"
                                    Visible="False"
                                    OnCustomButtonInitialize="gridPedidos_CustomButtonInitialize">
                                    <Styles>
                                        <StatusBar>
                                            <border borderstyle="None" />
                                        </StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro"
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar"
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Pedido" ReadOnly="true" Caption="Pedido" Visible="true" Width="85px">
                                            <DataItemTemplate>
                                                <asp:LinkButton ID="lkPedidoCliente" runat="server"
                                                    Text='<%# Bind("Pedido") %>' CausesValidation="true" CommandName="Select"
                                                    ToolTip="Haga clic aquí para seleccionar el pedido"></asp:LinkButton>
                                            </DataItemTemplate>
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Fecha" ReadOnly="true" Caption="Fecha" Visible="false">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaEntrega" ReadOnly="true" Caption="Fecha Entrega" Visible="true" Width="90px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataColumn FieldName="Cliente" ReadOnly="true" Caption="Cliente" Visible="true" Width="60px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreCliente" ReadOnly="true" Caption="Nombre Cliente" Visible="true" Width="350px">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                            <CellStyle Font-Size="X-Small" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Monto" ReadOnly="true" Caption="Monto" Visible="true" Width="75px">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                    <SettingsPager PageSize="100" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                    </table>
                </li>
            </ul>

            <ul>
                <li>
                    <table>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>Documento a re-imprimir:
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txtFacturaImprimir" runat="server" Width="260px"
                                    CssClass="textBoxStyle" TabIndex="385"
                                    ToolTip="Ingrese el número de factura" Visible="true">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkReimprimirFactura" runat="server"
                                    ToolTip="Haga clic aquí para re-imprimir la factura"
                                    TabIndex="388" OnClick="lkReimprimirFactura_Click">Re imprimir factura</asp:LinkButton>
                                <br />
                                <asp:LinkButton ID="lnkREimprimirNotaCreditoDeb" runat="server" OnClick="lnkREimprimirNotaCreditoDeb_Click">Re imprimir Notas de Crédito</asp:LinkButton>
                            </td>

                        </tr>
                    </table>
                </li>
            </ul>

            <ul>
                <li>
                    <table>
                        <tr>
                            <td colspan="3">
                                <a>Anulación de facturas</a>
                            </td>
                            <td colspan="5" style="text-align: right">
                                <asp:Label ID="lbErrorAnulacionFacturas" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                <asp:Label ID="lbInfoAnulacionFacturas" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Tienda:
                            </td>
                            <td>
                                <dx:ASPxComboBox ID="tiendaAnulacion" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle"
                                    ValueType="System.String" Width="65px" DropDownStyle="DropDownList"
                                    DropDownWidth="40" DropDownRows="25" TabIndex="401">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnFacturasAnular" runat="server" Width="8px" Height="8px"
                                    ToolTip="Mostrar facturas para anular" OnClick="btnFacturasAnular_Click" TabIndex="403">
                                    <Image IconID="actions_download_16x16office2013">
                                    </Image>
                                </dx:ASPxButton>
                            </td>
                            <td>No. Factura:
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txtFacturaAnular" runat="server" Width="153px" TabIndex="404" ToolTip="Ingrese el número de factura a anular" ReadOnly="false"></dx:ASPxTextBox>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnFacturaAnular" runat="server" Width="8px" Height="8px"
                                    ToolTip="Mostrar la factura a anular" OnClick="btnFacturaAnular_Click" TabIndex="405">
                                    <Image IconID="programming_showtestreport_16x16">
                                    </Image>
                                </dx:ASPxButton>
                            </td>
                            <td>Observaciones:
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txtObservacionesAnulacion" runat="server" Width="400px" TabIndex="406" ToolTip="Ingrese las observaciones de la anulación" ReadOnly="false"></dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="8">
                                <dx:ASPxGridView ID="gridFacturasAnular" EnableTheming="True" Theme="SoftOrange"
                                    runat="server" KeyFieldName="Factura" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" v Width="930px" CssClass="dxGrid"
                                    OnFocusedRowChanged="gridFacturasAnular_FocusedRowChanged" TabIndex="408"
                                    Visible="False" ClientInstanceName="gridFacturasAnular"
                                    OnCustomButtonInitialize="gridPedidos_CustomButtonInitialize" OnRowCommand="gridFacturasAnular_RowCommand">
                                   
                                    <SettingsBehavior EnableRowHotTrack="True"></SettingsBehavior>
                                    <Styles>
                                        <StatusBar>
                                            <border borderstyle="None" />
                                        </StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsDetail ShowDetailRow="true" />
                                    <Templates>
                                        <DetailRow>
                                            <dx:ASPxGridView ID="gridFacturasAnularDet" runat="server" AutoGenerateColumns="false" Theme="SoftOrange" CssClass="dxGrid" Border-BorderStyle="None" Width="890px"
                                                 OnRowCommand="gridFacturasAnularDet_RowCommand" KeyFieldName="Despacho"
                                                OnLoad="gridFacturasAnularDet_Load" OnHtmlRowPrepared="gridFacturasAnularDet_HtmlRowPrepared">
                                                <Styles>
                                                    <StatusBar>
                                                        <border borderstyle="None" />
                                                    </StatusBar>
                                                    <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                                    <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                                    <AlternatingRow BackColor="#fde4cf" />
                                                </Styles>
                                                <SettingsBehavior EnableRowHotTrack="True"></SettingsBehavior>
                                                <Columns>
                                                    <dx:GridViewDataDateColumn FieldName="Fecha_Despacho" ReadOnly="true" Caption="Fecha despacho" Visible="true" Width="80px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                                                    </dx:GridViewDataDateColumn>
                                                    <dx:GridViewDataColumn FieldName="Despacho" ReadOnly="true" Caption="Despacho" Visible="true" Width="80px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                   
                                                    <dx:GridViewDataColumn FieldName="Direccion" ReadOnly="true" Caption="Dirección" Visible="true" Width="220px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Left" />
                                                        <CellStyle HorizontalAlign="Left" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="DeTienda" ReadOnly="true" Caption="De Tienda" Visible="true" Width="80px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Left" />
                                                        <CellStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataDateColumn FieldName="Fecha_entrega" ReadOnly="true" Caption="Fecha entrega" Visible="true" Width="80px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                                                    </dx:GridViewDataDateColumn>
                                                    <dx:GridViewDataColumn FieldName="Usuario" ReadOnly="true" Caption="Usuario" Visible="true" Width="120px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Left" />
                                                        <CellStyle HorizontalAlign="Left" />
                                                    </dx:GridViewDataColumn>
                                                     <dx:GridViewDataTextColumn FieldName="Articulos" ReadOnly="true" Caption="Articulos" Visible="true" Width="120px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Left" />
                                                        <CellStyle HorizontalAlign="Left"  Font-Size="X-Small" />
                                                        <PropertiesTextEdit EncodeHtml="False" />  
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataColumn Caption="" Visible="true" Width="40px">
                                            <DataItemTemplate>
                                                 <dx:GridViewDataTextColumn  ReadOnly="true" Caption="Eliminar"  Visible="true" Width="75px">
	                                                    <EditFormSettings Visible="False" />
	                                                    <DataItemTemplate>
		                                                    <dx:ASPxButton ID="btnEliminarExpediente" EncodeHtml="false" runat="server" AutoPostBack="false" Text="Anular" 
                                                            ToolTip="Haga clic aquí para anular este depacho" OnClick="btnEliminar_Click" Font-Size="XX-Small"
                                                             ClientInstanceName="btnEliminar">
                                                              <ClientSideEvents Click="function(s, e) {
                                                                    e.processOnServer = confirm('¿Desea anular este despacho?');
                                                                        }" />
		                                                    </dx:ASPxButton>
	                                                    </DataItemTemplate>
	                                                    <HeaderStyle BackColor="#ff8a3f"  />
	                                                    <CellStyle Font-Size="XX-Small" />
                                                    </dx:GridViewDataTextColumn>
                                            </DataItemTemplate>
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                                </Columns>
                                            </dx:ASPxGridView>
                                        </DetailRow>
                                    </Templates>
                                   
                                    <SettingsPager PageSize="100" Visible="False">
                                    </SettingsPager>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro"
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar"
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Columns>
                                        <dx:GridViewDataColumn Caption="" Visible="true" Width="40px">
                                            <DataItemTemplate>
                                                <dx:ASPxButton ID="btnAnularFactura" EncodeHtml="false" runat="server" AutoPostBack="false" Text="Anular Factura"
                                                    ToolTip="Haga clic aquí para anular la factura." Font-Size="XX-Small" ClientInstanceName="btnAnularFactura">
                                                    <ClientSideEvents Click="function(s, e) {
                                                        gridFacturasAnular.Caption = 'Factura';
                                                        e.processOnServer = confirm('¿Desea anular la factura seleccionada?');
                                                            }" />
                                                </dx:ASPxButton>
                                            </DataItemTemplate>
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="true" Width="25px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura" Visible="true" Width="195px">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataDateColumn FieldName="Fecha" ReadOnly="true" Caption="Fecha" Visible="true" Width="70px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataColumn FieldName="Cliente" ReadOnly="true" Caption="Cliente" Visible="true" Width="50px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Nombre" ReadOnly="true" Caption="Nombre Cliente" Visible="true" Width="100px">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                            <CellStyle Font-Size="X-Small" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Monto" ReadOnly="true" Caption="Monto" Visible="true" Width="70px">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="Recibos" ReadOnly="true" Caption="Recibos" Visible="true" Width="130px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Left" />
                                            <CellStyle HorizontalAlign="Left" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Despachos" ReadOnly="true" Caption="Despachos" Visible="true" Width="90px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Left" />
                                            <CellStyle HorizontalAlign="Left" />
                                        </dx:GridViewDataColumn>
                                        
                                       
                                        
                                    </Columns>
                                   
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>

<AlternatingRow BackColor="#FDE4CF"></AlternatingRow>

<FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>

<StatusBar Border-BorderStyle="None"></StatusBar>
                                    </Styles>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                    </table>
                </li>
            </ul>

            <ul>
                <li>
                    <table>
                        <tr>
                            <td colspan="3">
                                <a>Revisión de facturas</a>
                            </td>
                            <td colspan="3" style="text-align: right">
                                <asp:Label ID="lbErrorFacturasRevisar" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                <asp:Label ID="lbInfoFacturasRevisar" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>
                                <asp:LinkButton ID="lkFacturasRevisar" runat="server"
                                    ToolTip="Haga clic aquí para cargar las facturas por revisar"
                                    TabIndex="330" OnClick="lkFacturasRevisar_Click">Cargar facturas por revisar</asp:LinkButton>
                                &nbsp;&nbsp;
                            </td>
                            <td>Factura a revisar:
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txtFacturaRevisar" runat="server" Width="110px"
                                    CssClass="textBoxStyle" TabIndex="440"
                                    ToolTip="Ingrese el número de factura a revisar" AutoPostBack="True"
                                    OnTextChanged="txtFacturaRevisar_TextChanged">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkCargarFacturaRevisar" runat="server"
                                    ToolTip="Haga clic aquí para cargar la factura para revisión"
                                    TabIndex="445" OnClick="lkCargarFacturaRevisar_Click">Cargar factura</asp:LinkButton>
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridFacturaRevisarDet" EnableTheming="True"
                                    Theme="SoftOrange" Visible="False"
                                    runat="server" KeyFieldName="Linea" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid"
                                    OnBatchUpdate="gridFacturaRevisarDet_BatchUpdate"
                                    OnRowUpdating="gridFacturaRevisarDet_RowUpdating"
                                    OnCommandButtonInitialize="gridFacturaRevisarDet_CommandButtonInitialize"
                                    OnCellEditorInitialize="gridFacturaRevisarDet_CellEditorInitialize"
                                    OnHtmlDataCellPrepared="gridFacturaRevisarDet_HtmlDataCellPrepared"
                                    OnHtmlRowPrepared="gridFacturaRevisarDet_HtmlRowPrepared" TabIndex="450">
                                    <Styles>
                                        <StatusBar>
                                            <border borderstyle="None" />
                                        </StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AllowSort="false"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro"
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar"
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowFooter="True" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura"
                                            Visible="true" Width="100px" VisibleIndex="8">
                                            <EditFormSettings Visible="false" />
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Linea" ReadOnly="true" Caption="Línea" Visible="false" Width="25px">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Articulo" ReadOnly="true"
                                            Caption="Artículo" Visible="true" Width="100px" VisibleIndex="9">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Descripcion" ReadOnly="true" Caption="Descripción" Visible="true" Width="350px" VisibleIndex="10">
                                            <EditFormSettings Visible="False" />
                                            <DataItemTemplate>
                                                <dx:ASPxLabel ID="lbDescripcion" EncodeHtml="false" runat="server" AutoPostBack="false" Text='<%# Bind("Descripcion") %>' Font-Size="X-Small">
                                                </dx:ASPxLabel>
                                            </DataItemTemplate>
                                            <HeaderStyle BackColor="#ff8a3f" />
                                            <CellStyle Font-Size="X-Small" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PrecioUnitario" ReadOnly="false" Caption="PrecioUnitario" Visible="true" Width="110px" VisibleIndex="12">
                                            <PropertiesTextEdit DisplayFormatString="n6" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="Cantidad" ReadOnly="true" Caption="Cantidad" Visible="true" Width="50px" VisibleIndex="14">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="PrecioTotal" ReadOnly="true" Caption="PrecioTotal" Visible="true" Width="110px" VisibleIndex="15">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <TotalSummary>
                                        <dx:ASPxSummaryItem FieldName="PrecioTotal" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </TotalSummary>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsEditing Mode="Batch" />
                                    <ClientSideEvents BatchEditEndEditing="OnBatchEditEndEditing" />
                                    <SettingsBehavior EnableRowHotTrack="true" />
                                    <SettingsPager PageSize="20" Visible="False">
                                    </SettingsPager>
                                    <SettingsText SearchPanelEditorNullText="Ingrese aquí su criterio de búsqueda" CommandBatchEditUpdate="Aplicar cambios" CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" />
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="text-align: center">
                                <dx:ASPxButton ID="btnFacturaRevisar" runat="server" Width="8px" Height="8px"
                                    ToolTip="Actualizar la factura" OnClick="btnFacturaRevisar_Click"
                                    TabIndex="455">
                                    <Image IconID="save_saveall_16x16office2013">
                                    </Image>
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="btnLimpiarFacturaRevisar" runat="server" Height="8px"
                                    OnClick="btnLimpiarFacturaRevisar_Click" TabIndex="460"
                                    ToolTip="Limpiar la pantalla de revisión de facturas" Width="8px">
                                    <Image IconID="actions_clear_16x16">
                                    </Image>
                                </dx:ASPxButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridFacturasRevisar" EnableTheming="True" Theme="SoftOrange"
                                    runat="server" KeyFieldName="Factura" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid"
                                    OnBatchUpdate="gridFacturasRevisar_BatchUpdate"
                                    OnRowUpdating="gridFacturasRevisar_RowUpdating"
                                    OnCommandButtonInitialize="gridFacturasRevisar_CommandButtonInitialize"
                                    OnCellEditorInitialize="gridFacturasRevisar_CellEditorInitialize"
                                    OnFocusedRowChanged="gridFacturasRevisar_FocusedRowChanged" TabIndex="465"
                                    Visible="False" OnHtmlRowPrepared="gridFacturasRevisar_HtmlRowPrepared">
                                    <Styles>
                                        <StatusBar>
                                            <border borderstyle="None" />
                                        </StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro"
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar"
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura" Visible="true" Width="85px">
                                            <DataItemTemplate>
                                                <asp:LinkButton ID="lkFacturaRevisar" runat="server"
                                                    Text='<%# Bind("Factura") %>' CausesValidation="true" CommandName="Select"
                                                    ToolTip="Haga clic aquí para seleccionar la factura y revisarla"></asp:LinkButton>
                                            </DataItemTemplate>
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Fecha" ReadOnly="true" Caption="Fecha" Visible="false">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Cliente" ReadOnly="true" Caption="Cliente" Visible="true" Width="60px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreCliente" ReadOnly="true" Caption="Nombre Cliente" Visible="true" Width="350px">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                            <CellStyle Font-Size="X-Small" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Monto" ReadOnly="true" Caption="Monto" Visible="true" Width="75px">
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="Anulada" ReadOnly="true" Caption="Anulada" Visible="true" Width="40px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                    <SettingsPager PageSize="100" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                    </table>
                </li>
            </ul>
            <ul>
                <li>
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <table style="width: 100%">
                                <tr>
                                    <td class="auto-style2" colspan="7"><a>Cambiar tienda a Vendedor</a></td>
                                </tr>
                                <tr style="column-span: all;">
                                    <td class="auto-style1">Buscar tienda:</td>
                                    <td>
                                        <dx:ASPxComboBox ID="cmbTiendaActual" runat="server" Theme="SoftOrange"
                                            CssClass="ComboBoxStyle" Width="90px"
                                            DropDownWidth="40px" DropDownRows="25" TabIndex="369" SelectedIndex="0"
                                            AutoPostBack="True" OnSelectedIndexChanged="cmbTiendaActual_SelectedIndexChanged">
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td>Vendedor:</td>
                                    <td>
                                        <dx:ASPxComboBox ID="cmbVendedorTienda" runat="server" Theme="SoftOrange"
                                            CssClass="ComboBoxStyle" Width="270px"
                                            DropDownWidth="40px" DropDownRows="25" TabIndex="369" SelectedIndex="0"
                                            AutoPostBack="True" OnSelectedIndexChanged="cbSerie_SelectedIndexChanged">
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td style="width: 110px">Cambiar a tienda:</td>
                                    <td>
                                        <dx:ASPxComboBox ID="cmbTiendaNueva" runat="server" Theme="SoftOrange"
                                            CssClass="ComboBoxStyle" Width="90px"
                                            DropDownWidth="40px" DropDownRows="25" TabIndex="369" SelectedIndex="0"
                                            OnSelectedIndexChanged="cbSerie_SelectedIndexChanged">
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btnCambiarVendedorTienda" runat="server" Text="Aplicar Cambio de Tienda" AutoPostBack="false"
                                            RenderMode="Link"
                                            ToolTip="Click para cambiar al vendedor de tienda"
                                            ImagePosition="Left" OnClick="btnCambiarVendedorTienda_Click">
                                            <Image IconID="content_checkbox_16x16office2013"></Image>
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                            <div style="align-items: right; position: relative; text-align: right; margin-top: 10px; margin-right: 10px;">
                                <asp:Label ID="lblErrorCTV" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>




                </li>
            </ul>
        </div>
        <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server" TabIndex="8888"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red"
                        TabIndex="8888"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
