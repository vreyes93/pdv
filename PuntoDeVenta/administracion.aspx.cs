﻿using System;
using System.Data;
using System.Text;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CrystalDecisions.CrystalReports.Engine;
using System.Net;
using DevExpress.Web;
using DevExpress.Web.Data;
using DevExpress.Export;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using Newtonsoft.Json;
using MF_Clases;
using System.Reflection;
using RestSharp;
using System.Web.Configuration;
using MF_Clases.Restful;
using System.Web;

namespace PuntoDeVenta
{
    public partial class administracion : BasePage
    {
        private bool IsSaved = false;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                lblErrorCTV.ForeColor = System.Drawing.Color.Red;
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU")
                    ws.Url = Convert.ToString(Session["url"]);

                var q = ws.DevuelveTransportistasTodos();
                (gridDespachos.Columns["Transportista"] as GridViewDataComboBoxColumn).PropertiesComboBox.DataSource = q;

                DataSet dsDespachos = new DataSet();
                dsDespachos = (DataSet)Session["dsDespachos"];

                gridDespachos.DataSource = dsDespachos;
                gridDespachos.DataMember = "documentos";
                gridDespachos.DataBind();
                gridDespachos.SettingsPager.PageSize = 500;
                gridDespachos.DataColumns["NombreCliente"].Settings.AutoFilterCondition = AutoFilterCondition.Contains;

                Clases.RetornaComisiones mRetorna = new Clases.RetornaComisiones();
                mRetorna = (Clases.RetornaComisiones)Session["Comisiones"];

                gridComisiones.DataSource = mRetorna.Comisiones.AsEnumerable();
                gridComisiones.DataMember = "comisiones";
                gridComisiones.DataBind();
                gridComisiones.FocusedRowIndex = -1;
                gridComisiones.SettingsPager.PageSize = 500;

                gridTiendas.DataSource = mRetorna.Tiendas.AsEnumerable();
                gridTiendas.DataMember = "tiendas";
                gridTiendas.DataBind();
                gridTiendas.FocusedRowIndex = -1;
                gridTiendas.SettingsPager.PageSize = 500;

                gridFacturas.DataSource = mRetorna.Facturas.AsEnumerable();
                gridFacturas.DataMember = "facturas";
                gridFacturas.DataBind();
                gridFacturas.FocusedRowIndex = -1;
                gridFacturas.SettingsPager.PageSize = 15000;

                gridExcepciones.DataSource = mRetorna.Excepciones.AsEnumerable();
                gridExcepciones.DataMember = "excepciones";
                gridExcepciones.DataBind();
                gridExcepciones.FocusedRowIndex = -1;
                gridExcepciones.SettingsPager.PageSize = 500;

                gridAnuladas.DataSource = mRetorna.Anuladas.AsEnumerable();
                gridAnuladas.DataMember = "anuladas";
                gridAnuladas.DataBind();
                gridAnuladas.FocusedRowIndex = -1;
                gridAnuladas.SettingsPager.PageSize = 500;

                gridPendientes.DataSource = mRetorna.Pendientes.AsEnumerable();
                gridPendientes.DataMember = "pendientes";
                gridPendientes.DataBind();
                gridPendientes.FocusedRowIndex = -1;
                gridPendientes.SettingsPager.PageSize = 15000;

                Clases.RetornaComisionesJefesSupervisores mRetornaJefesSupervisores = new Clases.RetornaComisionesJefesSupervisores();
                mRetornaJefesSupervisores = (Clases.RetornaComisionesJefesSupervisores)Session["JefesSupervisores"];

                gridJefesSupervisores.Visible = true;
                gridJefesSupervisores.DataSource = mRetornaJefesSupervisores.ComisionesJefesSupervisores.AsEnumerable();
                gridJefesSupervisores.DataMember = "ComisionesJefesSupervisores";
                gridJefesSupervisores.DataBind();
                gridJefesSupervisores.FocusedRowIndex = -1;
                gridJefesSupervisores.SettingsPager.PageSize = 500;

                gridDetalleComisionJefe.Visible = true;
                gridDetalleComisionJefe.DataSource = mRetornaJefesSupervisores.DetalleComisionesJefe.AsEnumerable();
                gridDetalleComisionJefe.DataMember = "DetalleComisionJefe";
                gridDetalleComisionJefe.DataBind();
                gridDetalleComisionJefe.FocusedRowIndex = -1;
                gridDetalleComisionJefe.SettingsPager.PageSize = 500;

                gridResumenComisionJefe.Visible = true;
                gridResumenComisionJefe.DataSource = mRetornaJefesSupervisores.ResumenJefe.AsEnumerable();
                gridResumenComisionJefe.DataMember = "ResumenComisionJefe";
                gridResumenComisionJefe.DataBind();
                gridResumenComisionJefe.FocusedRowIndex = -1;
                gridResumenComisionJefe.SettingsPager.PageSize = 500;

                gridDetalleComisionSupervisor.Visible = true;
                gridDetalleComisionSupervisor.DataSource = mRetornaJefesSupervisores.DetalleComisionesSupervisor.AsEnumerable();
                gridDetalleComisionSupervisor.DataMember = "DetalleComisionSupervisor";
                gridDetalleComisionSupervisor.DataBind();
                gridDetalleComisionSupervisor.FocusedRowIndex = -1;
                gridDetalleComisionSupervisor.SettingsPager.PageSize = 500;

                gridResumenComisionSupervisor.Visible = true;
                gridResumenComisionSupervisor.DataSource = mRetornaJefesSupervisores.ResumenSupervisor.AsEnumerable();
                gridResumenComisionSupervisor.DataMember = "ResumenComisionSupervisor";
                gridResumenComisionSupervisor.DataBind();
                gridResumenComisionSupervisor.FocusedRowIndex = -1;
                gridResumenComisionSupervisor.SettingsPager.PageSize = 500;

                gridDesgloceJefeSupervisor.Visible = true;
                gridDesgloceJefeSupervisor.DataSource = mRetornaJefesSupervisores.JefeSupervisorDetalle.AsEnumerable();
                gridDesgloceJefeSupervisor.DataMember = "DetalleJefeSupervisor";
                gridDesgloceJefeSupervisor.DataBind();
                gridDesgloceJefeSupervisor.FocusedRowIndex = -1;
                gridDesgloceJefeSupervisor.SettingsPager.PageSize = 500;

                DataSet dsFacturasRevisar = new DataSet();
                dsFacturasRevisar = (DataSet)Session["dsFacturasRevisar"];

                gridFacturasRevisar.DataSource = dsFacturasRevisar;
                gridFacturasRevisar.DataMember = "documentos";
                gridFacturasRevisar.DataBind();
                gridFacturasRevisar.SettingsPager.PageSize = 500;
            }
            catch { }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                HookOnFocus(this.Page as Control);

                Session["dsArticulos"] = null;
                Session["dsDespachos"] = null;
                Session["Comisiones"] = null;
                Session["dsPedidos"] = null;
                Session["JefesSupervisores"] = null;
                ViewState["TipoComision"] = Const.Comisiones.FRIEDMAN;

                CargarBodegas();
                CargarTransportistas();
                LimpiarControlesDespacho();
                LimpiarControlesModificaFactura();
                LimpiarControlesComisiones();

                fechaInicialPedidos.Value = DateTime.Now.Date.AddDays(-1);
                fechaFinalPedidos.Value = DateTime.Now.Date.AddDays(1);
                fechaFactura.Value = DateTime.Now.Date;

                txtOrdenCompra.Text = "";
                txtObservacionesAnulacion.Text = "";
                txtFacturaModifica.Focus();

                if (Session["Pedido"] != null)
                {
                    txtPedidoFactura.Text = Convert.ToString(Session["Pedido"]);
                    CargarPedido();
                }

                VerifyDeleteCommission();
                VerifySendResume();

                int mes = int.Parse(mesComision.Value.ToString());
                int anio = int.Parse(anioComision.Value.ToString());
                DateTime fechaInicio = Convert.ToDateTime($"{anio}-{mes}-01");
                DateTime fechaFin = Convert.ToDateTime($"{anio}-{mes}-{DateTime.DaysInMonth(anio, mes)}");
                VerifyIsSaved(fechaInicio, fechaFin);
            }

            Page.ClientScript.RegisterStartupScript(
            typeof(administracion),
            "ScriptDoFocus",
            SCRIPT_DOFOCUS.Replace("REQUEST_LASTFOCUS", Request["__LASTFOCUS"]),
            true);
        }

        private const string SCRIPT_DOFOCUS = @"window.setTimeout('DoFocus()', 1); function DoFocus() {
            try {
                document.getElementById('REQUEST_LASTFOCUS').focus();
            } catch (ex) {}
        }";

        private void HookOnFocus(Control CurrentControl)
        {
            //checks if control is one of TextBox, DropDownList, ListBox or Button
            if ((CurrentControl is TextBox) ||
                (CurrentControl is DropDownList) ||
                (CurrentControl is ListBox) ||
                (CurrentControl is Button))
                //adds a script which saves active control on receiving focus 
                //in the hidden field __LASTFOCUS.
                (CurrentControl as WebControl).Attributes.Add(
                   "onfocus",
                   "try{document.getElementById('__LASTFOCUS').value=this.id} catch(e) {}");
            //checks if the control has children
            if (CurrentControl.HasControls())
                //if yes do them all recursively
                foreach (Control CurrentChildControl in CurrentControl.Controls)
                    HookOnFocus(CurrentChildControl);
        }

        void CargaVendedores()
        {
            cmbVendedorTienda.SelectedIndex = -1;

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]) == "" ? Convert.ToString(Session["Url"]) : Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveVendedoresTienda(cmbTiendaActual.SelectedItem.Value.ToString()).ToList();

            cmbVendedorTienda.DataSource = q;
            cmbVendedorTienda.TextField = "Nombre";
            cmbVendedorTienda.ValueField = "Vendedor";
            cmbVendedorTienda.DataBind();
            cmbVendedorTienda.Items.Insert(0, new ListEditItem("Seleccione", "0"));
            cmbVendedorTienda.SelectedIndex = 0;
        }

        void CargarBodegas()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveTiendas();

            tiendaTransporte.DataSource = q;
            tiendaTransporte.ValueField = "Tienda";
            tiendaTransporte.ValueType = typeof(System.String);
            tiendaTransporte.TextField = "Tienda";
            tiendaTransporte.DataBindItems();

            tiendaComisiones.DataSource = q;
            tiendaComisiones.ValueField = "Tienda";
            tiendaComisiones.ValueType = typeof(System.String);
            tiendaComisiones.TextField = "Tienda";
            tiendaComisiones.DataBindItems();

            //cambio de tienda a vendedor
            cmbTiendaActual.DataSource = q.Where(x => x.Tienda.Substring(0, 1) != "B");
            cmbTiendaActual.ValueField = "Tienda";
            cmbTiendaActual.ValueType = typeof(System.String);
            cmbTiendaActual.TextField = "Tienda";
            cmbTiendaActual.DataBindItems();
            cmbTiendaActual.Items.Insert(0, new ListEditItem("Seleccione", "0"));
            cmbTiendaActual.SelectedIndex = 0;

            cmbTiendaNueva.DataSource = q.Where(x => x.Tienda.Substring(0, 1) != "B");
            cmbTiendaNueva.ValueField = "Tienda";
            cmbTiendaNueva.ValueType = typeof(System.String);
            cmbTiendaNueva.TextField = "Tienda";
            cmbTiendaNueva.DataBindItems();
            cmbTiendaNueva.Items.Insert(0, new ListEditItem("Seleccione", "0"));
            cmbTiendaNueva.SelectedIndex = 0;

            //fin cambio tienda a vendedor
            WebRequest request = WebRequest.Create(string.Format("{0}/tiendasacceso/{1}", Convert.ToString(Session["UrlRestServices"]), Convert.ToString(Session["Usuario"])));

            request.Method = "GET";
            request.ContentType = "application/json";

            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            List<Clases.Tiendas> mTiendas = new List<Clases.Tiendas>();
            mTiendas = JsonConvert.DeserializeObject<List<Clases.Tiendas>>(responseString);

            tiendaAnulacion.DataSource = mTiendas;
            tiendaAnulacion.ValueField = "Tienda";
            tiendaAnulacion.ValueType = typeof(System.String);
            tiendaAnulacion.TextField = "Tienda";
            tiendaAnulacion.DataBindItems();

            tiendaAnulacion.SelectedIndex = 0;
        }

        void CargarTransportistas()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveTransportistasTodos();

            transportistaT.DataSource = q;
            transportistaT.ValueField = "Transportista";
            transportistaT.ValueType = typeof(System.Int32);
            transportistaT.TextField = "Nombre";
            transportistaT.DataBindItems();

            transportistaT.Value = 1;
        }

        void LimpiarControlesDespacho()
        {
            try
            {
                lbInfoTransportista.Text = "";
                lbErrorTransportista.Text = "";

                DateTime mFechaFinal = DateTime.Now.Date.AddDays(-1);
                DateTime mFechaInicial = mFechaFinal.AddDays(-1);

                fechaInicialT.Value = mFechaInicial;
                fechaFinalT.Value = mFechaFinal;

                tiendaTransporte.Value = "F02";
                fechaInicialT.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorTransportista.Text = string.Format("Error al inicializar los despachos. {0} {1}", ex.Message, m);
            }
        }

        void UpdateDespacho(string despacho, DateTime fechaEntrega, Int32 transportista, string cliente, Int32 articulos)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsDespachos"];

                ds.Tables["documentos"].Rows.Find(despacho)["FechaEntrega"] = fechaEntrega;
                ds.Tables["documentos"].Rows.Find(despacho)["Transportista"] = transportista;
                ds.Tables["documentos"].Rows.Find(despacho)["Cliente"] = cliente;
                ds.Tables["documentos"].Rows.Find(despacho)["Articulos"] = articulos;
                Session["dsArticulos"] = ds;

                gridDespachos.DataSource = ds;
                gridDespachos.DataMember = "documentos";
                gridDespachos.DataBind();
                gridDespachos.FocusedRowIndex = -1;
                gridDespachos.SettingsPager.PageSize = 500;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorTransportista.Text = string.Format("Error al actualizar los datos UPDATE {0} {1}", ex.Message, m);
            }
        }

        protected void gridDespachos_BatchUpdate(object sender, ASPxDataBatchUpdateEventArgs e)
        {
            foreach (var args in e.UpdateValues)
                UpdateDespacho(args.Keys["Despacho"].ToString(), Convert.ToDateTime(args.NewValues["FechaEntrega"]), Convert.ToInt32(args.NewValues["Transportista"]), args.NewValues["Cliente"].ToString(), Convert.ToInt32(args.NewValues["Articulos"]));

            e.Handled = true;
        }

        protected void gridDespachos_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {

        }

        protected void gridDespachos_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            UpdateDespacho(e.Keys["Despacho"].ToString(), Convert.ToDateTime(e.NewValues["FechaEntrega"]), Convert.ToInt32(e.NewValues["Cantidad"]), e.NewValues["Cliente"].ToString(), Convert.ToInt32(e.NewValues["Articulos"]));
            CancelEditingDespacho(e);
        }

        protected void gridDespachos_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == ColumnCommandButtonType.Update || e.ButtonType == ColumnCommandButtonType.Cancel) e.Visible = false;
        }

        protected void gridDespachos_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.Name == "Cliente") e.Cell.ToolTip = "Marque esta casilla si el cliente confirmó de recibida su mercadería";
        }

        protected void gridDespachos_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {

        }

        protected void CancelEditingDespacho(CancelEventArgs e)
        {
            e.Cancel = true;
            gridDespachos.CancelEdit();
        }

        protected void lkSeleccionarFechasT_Click(object sender, EventArgs e)
        {
            CargarDespachados("F");
        }

        protected void lkSeleccionarTiendaT_Click(object sender, EventArgs e)
        {
            CargarDespachados("T");
        }

        protected void lkTransportistaT_Click(object sender, EventArgs e)
        {
            CargarDespachados("R");
        }

        protected void lkSeleccionarFechasEnvio_Click(object sender, EventArgs e)
        {
            CargarDespachados("E");
        }

        void CargarDespachados(string tipo)
        {
            try
            {
                lbInfoTransportista.Text = "";
                lbErrorTransportista.Text = "";

                gridDespachos.Visible = false;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DateTime mFechaInicial = Convert.ToDateTime(fechaInicialT.Value);
                DateTime mFechaFinal = Convert.ToDateTime(fechaFinalT.Value);

                DataSet ds = new DataSet(); string mMensaje = "";
                ds = ws.DocumentosDespachadosModificar(ref mMensaje, mFechaInicial, mFechaFinal, Convert.ToString(tiendaTransporte.Value), tipo, Convert.ToString(transportistaT.Value), "S", Convert.ToString(Session["Usuario"]), "");

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorTransportista.Text = mMensaje;
                    return;
                }

                if (ds.Tables["documentos"].Rows.Count > 0)
                {
                    gridDespachos.Visible = true;
                    gridDespachos.DataSource = ds;
                    gridDespachos.DataMember = "documentos";
                    gridDespachos.DataBind();
                    gridDespachos.FocusedRowIndex = -1;
                    gridDespachos.SettingsPager.PageSize = 500;
                    gridDespachos.DataColumns["NombreCliente"].Settings.AutoFilterCondition = AutoFilterCondition.Contains;

                    if (tipo == "P")
                    {
                        gridDespachos.DataColumns["Despacho"].Caption = "Traspaso";
                        gridDespachos.DataColumns["Cliente"].ReadOnly = true;
                        gridDespachos.DataColumns["Articulos"].ReadOnly = true;
                    }
                    else
                    {
                        gridDespachos.Columns["Despacho"].Caption = "Despacho";
                        gridDespachos.DataColumns["Cliente"].ReadOnly = false;
                        gridDespachos.DataColumns["Articulos"].ReadOnly = false;
                    }

                    Session["dsDespachos"] = ds;
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorTransportista.Text = string.Format("Error al mostrar los documentos despachados. {0} {1}", ex.Message, m);
            }

        }

        protected void lkActualizarTransportista_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoTransportista.Text = "";
                lbErrorTransportista.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsDespachos"];

                if (!ws.ActualizarTransportista(ref mMensaje, Convert.ToString(Session["Usuario"]), ds))
                {
                    lbErrorTransportista.Text = mMensaje;
                    return;
                }

                gridDespachos.Visible = false;
                lbInfoTransportista.Text = mMensaje;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorTransportista.Text = string.Format("Error al actualizar los transportistas y aceptación de clientes. {0} {1}", ex.Message, m);
            }
        }

        void LimpiarControlesModificaFactura()
        {
            lbErrorModificaFactura.Text = "";
            lbInfoModificaFactura.Text = "";

            txtFacturaModifica.Text = "";
            tipoExcepcion.Value = "M";
            txtMontoFactura.Text = "";
            txtMontoComision.Text = "";
            txtMontoComisionable.Text = "";

            mesPago.Text = "";
            mesPago.Value = null;

            anioPago.Text = "";
            anioPago.Value = null;

            mesPago.ReadOnly = true;
            anioPago.ReadOnly = true;
        }

        protected void lkGrabarModificaFactura_Click(object sender, EventArgs e)
        {
            try
            {
                lbErrorModificaFactura.Text = "";
                lbInfoModificaFactura.Text = "";

                if (txtFacturaModifica.Text.Trim().Length == 0) return;

                decimal mMontoComision = 0;
                if (tipoExcepcion.Value.ToString() == "M" || tipoExcepcion.Value.ToString() == "P")
                {
                    try
                    {
                        mMontoComision = Convert.ToDecimal(txtMontoComision.Text.Trim().Replace(",", "").Replace(" ", "").Replace("Q.", "").Replace("Q", ""));
                    }
                    catch
                    {
                        lbErrorModificaFactura.Text = "El monto a comisionar es inválido.";
                        txtMontoComision.Focus();
                        return;
                    }

                    if (mMontoComision < 0)
                    {
                        lbErrorModificaFactura.Text = "El monto a comisionar no puede ser menor que cero.";
                        txtMontoComision.Focus();
                        return;
                    }

                    decimal mMontoFactura = Convert.ToDecimal(txtMontoFactura.Text.Trim().Replace(",", "").Replace(" ", "").Replace("Q.", "").Replace("Q", ""));

                    if (mMontoComision > mMontoFactura)
                    {
                        lbErrorModificaFactura.Text = "El monto a comisionar no puede ser mayor al monto de la factura.";
                        txtMontoComision.Focus();
                        return;
                    }
                }

                string mMes = ""; string mAnio = "";

                if (Convert.ToString(tipoExcepcion.Value) == "P")
                {
                    try
                    {
                        mMes = Convert.ToString(mesPago.Value);
                        if (mMes == null) mMes = "";
                    }
                    catch
                    {
                        mMes = "";
                    }

                    try
                    {
                        mAnio = Convert.ToString(anioPago.Value);
                        if (mAnio == null) mAnio = "";
                    }
                    catch
                    {
                        mAnio = "";
                    }

                    if (mMes.Trim().Length == 0)
                    {
                        lbErrorModificaFactura.Text = "Debe seleccionar el mes.";
                        mesPago.Focus();
                        return;
                    }

                    if (mAnio.Trim().Length == 0)
                    {
                        lbErrorModificaFactura.Text = "Debe seleccionar el año.";
                        anioPago.Focus();
                        return;
                    }
                }

                Clases.FacturaModificaComision mFactura = new Clases.FacturaModificaComision();

                mFactura.Mes = mMes;
                mFactura.Anio = mAnio;
                mFactura.Factura = txtFacturaModifica.Text.Trim().ToUpper();
                mFactura.MontoComision = mMontoComision;
                mFactura.Tipo = tipoExcepcion.Value.ToString();
                mFactura.Usuario = Convert.ToString(Session["Usuario"]);

                WebRequest request = WebRequest.Create(string.Format("{0}/ModificarFacturaComision/", Convert.ToString(Session["UrlRestServices"])));

                string json = JsonConvert.SerializeObject(mFactura);
                byte[] data = Encoding.ASCII.GetBytes(json);

                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                Retorna mRetorna = new Retorna();
                mRetorna = JsonConvert.DeserializeObject<Retorna>(responseString);

                if (!mRetorna.exito)
                {
                    lbErrorModificaFactura.Text = mRetorna.mensaje;
                    return;
                }

                LimpiarControlesModificaFactura();
                lbInfoModificaFactura.Text = mRetorna.mensaje;
                txtFacturaModifica.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorModificaFactura.Text = string.Format("Error al modificar la factura. {0} {1}", ex.Message, m);
            }
        }

        protected void txtFacturaModifica_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lbErrorModificaFactura.Text = "";
                lbInfoModificaFactura.Text = "";

                txtMontoComision.Text = "";

                if (txtFacturaModifica.Text.Trim().Length == 0)
                {
                    txtMontoFactura.Text = "";
                    return;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta(); string mFactura = ""; string mComisionable = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                mFactura = ws.devuelveMontoFactura(txtFacturaModifica.Text.Trim(), ref mComisionable);

                if (mFactura.Length > 20)
                {
                    txtMontoFactura.Text = "";
                    txtMontoComisionable.Text = "";
                    lbErrorModificaFactura.Text = mFactura;
                    return;
                }
                else
                {
                    txtMontoFactura.Text = mFactura;
                    txtMontoComisionable.Text = mComisionable;
                }

                txtMontoComision.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorModificaFactura.Text = string.Format("Error al validar la factura. {0} {1}", ex.Message, m);
            }
        }

        void LimpiarControlesComisiones()
        {
            lbInfoComisiones.Text = "";
            lbErrorTransportista.Text = "";

            Int16 mRestarInicial = -1;
            Int16 mRestarFinal = 0;

            if (DateTime.Now.Date.Day <= 10)
            {
                mRestarInicial = -2;
                mRestarFinal = -1;
            }

            DateTime mFechaInicial = DateTime.Now.Date.AddMonths(mRestarInicial);
            DateTime mFechaFinal = DateTime.Now.Date.AddMonths(mRestarFinal);

            fechaInicialComisiones.Value = new DateTime(mFechaInicial.Year, mFechaInicial.Month, 26);
            fechaFinalComisiones.Value = new DateTime(mFechaFinal.Year, mFechaFinal.Month, 25);

            mesComision.Value = mFechaFinal.Month.ToString();
            anioComision.Value = mFechaFinal.Year.ToString();

            tiendaComisiones.Value = "F02";
            CargarVendedoresComisiones();
        }

        protected void tiendaComisiones_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarVendedoresComisiones();
        }

        void CargarVendedoresComisiones()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveVendedoresTienda(tiendaComisiones.Value.ToString()).ToList();

            vendedorComisiones.DataSource = q;
            vendedorComisiones.ValueField = "Vendedor";
            vendedorComisiones.ValueType = typeof(System.String);
            vendedorComisiones.TextField = "Nombre";
            vendedorComisiones.DataBindItems();

            if (q.Count > 0)
                vendedorComisiones.Value = q[0].Vendedor;
        }

        protected void lkSeleccionarTiendaComisiones_Click(object sender, EventArgs e)
        {
            CargarComisiones("T", Const.Comisiones.FRIEDMAN);
        }

        protected void lkVendedorComisiones_Click(object sender, EventArgs e)
        {
            CargarComisiones("V", Const.Comisiones.FRIEDMAN);
        }

        protected void lkComisionesMesCalendario_Click(object sender, EventArgs e)
        {
            CargarComisiones("F", Const.Comisiones.CALENDARIO);
        }


        void CargarComisiones(string tipo, string TipoFecha)
        {
            try
            {
                lbInfoComisiones.Text = "";
                lbErrorComisiones.Text = "";

                btnGrabar.Visible = false;
                gridComisiones.Visible = false;
                gridTiendas.Visible = false;
                gridFacturas.Visible = false;
                gridExcepciones.Visible = false;
                gridAnuladas.Visible = false;
                gridPendientes.Visible = false;

                DateTime mFechaFinal;
                DateTime mFechaInicial;

                if (TipoFecha.Equals(Const.Comisiones.CALENDARIO))
                {
                    mFechaInicial = new DateTime(Convert.ToInt32(anioComision.Value), Convert.ToInt32(mesComision.Value), 1);
                    mFechaFinal = new DateTime(Convert.ToInt32(anioComision.Value), Convert.ToInt32(mesComision.Value), mFechaInicial.AddMonths(1).AddDays(-1).Day);
                }
                else
                {
                    mFechaFinal = new DateTime(Convert.ToInt32(anioComision.Value), Convert.ToInt32(mesComision.Value), 25);
                    mFechaInicial = new DateTime(mFechaFinal.AddMonths(-1).Year, mFechaFinal.AddMonths(-1).Month, 26);
                }

                //Cambio para que sea del 1 al último día de cada mes
                if (Convert.ToInt32(anioComision.Value) >= 2018)
                {
                    mFechaInicial = new DateTime(Convert.ToInt32(anioComision.Value), Convert.ToInt32(mesComision.Value), 1);
                    mFechaFinal = new DateTime(Convert.ToInt32(anioComision.Value), Convert.ToInt32(mesComision.Value), mFechaInicial.AddMonths(1).AddDays(-1).Day);
                }
                else
                {
                    if (Convert.ToInt32(anioComision.Value) == 2017 && Convert.ToInt32(mesComision.Value) == 12)
                    {
                        mFechaInicial = new DateTime(Convert.ToInt32(anioComision.Value), 11, 26);
                        mFechaFinal = new DateTime(Convert.ToInt32(anioComision.Value), 12, 31);
                    }
                    else
                    {
                        mFechaFinal = new DateTime(Convert.ToInt32(anioComision.Value), Convert.ToInt32(mesComision.Value), 25);
                        mFechaInicial = new DateTime(mFechaFinal.AddMonths(-1).Year, mFechaFinal.AddMonths(-1).Month, 26);
                    }
                }

                fechaInicialComisiones.Value = mFechaInicial;
                fechaFinalComisiones.Value = mFechaFinal;

                Api api = new Api(General.FiestaNetRestService);
                Clases.RetornaComisiones mRetorna = JsonConvert
                    .DeserializeObject<Clases.RetornaComisiones>(api.Process(Method.GET, $"/comisiones/?anio1={mFechaInicial.Year}&mes1={mFechaInicial.Month}&dia1={mFechaInicial.Day}&anio2={mFechaFinal.Year}&mes2={mFechaFinal.Month}&dia2={mFechaFinal.Day}&vendedor=&tienda=", null));

                if (!mRetorna.Info[0].Exito)
                {
                    Session["Comisiones"] = null;
                    lbErrorComisiones.Text = mRetorna.Info[0].Mensaje;
                    return;
                }

                lbInfoComisiones.Text = mRetorna.Info[0].Mensaje;

                if (TipoFecha.Equals(Const.Comisiones.CALENDARIO))
                {
                    lbInfoComisiones.Text = "Comisiones calendario, generadas exitosamente";
                    ViewState["TipoComision"] = Const.Comisiones.CALENDARIO;
                }

                mRetorna.Info[0].Usuario = Convert.ToString(Session["Usuario"]);

                gridComisiones.Visible = true;
                gridComisiones.DataSource = mRetorna.Comisiones.AsEnumerable();
                gridComisiones.DataMember = "comisiones";
                gridComisiones.DataBind();
                gridComisiones.FocusedRowIndex = -1;
                gridComisiones.SettingsPager.PageSize = 500;

                gridTiendas.Visible = true;
                gridTiendas.DataSource = mRetorna.Tiendas.AsEnumerable();
                gridTiendas.DataMember = "tiendas";
                gridTiendas.DataBind();
                gridTiendas.FocusedRowIndex = -1;
                gridTiendas.SettingsPager.PageSize = 500;

                gridFacturas.Visible = true;
                gridFacturas.DataSource = mRetorna.Facturas.AsEnumerable();
                gridFacturas.DataMember = "facturas";
                gridFacturas.DataBind();
                gridFacturas.FocusedRowIndex = -1;
                gridFacturas.SettingsPager.PageSize = 15000;

                gridExcepciones.Visible = true;
                gridExcepciones.DataSource = mRetorna.Excepciones.AsEnumerable();
                gridExcepciones.DataMember = "excepciones";
                gridExcepciones.DataBind();
                gridExcepciones.FocusedRowIndex = -1;
                gridExcepciones.SettingsPager.PageSize = 500;

                gridAnuladas.Visible = true;
                gridAnuladas.DataSource = mRetorna.Anuladas.AsEnumerable();
                gridAnuladas.DataMember = "anuladas";
                gridAnuladas.DataBind();
                gridAnuladas.FocusedRowIndex = -1;
                gridAnuladas.SettingsPager.PageSize = 500;

                gridPendientes.Visible = true;
                gridPendientes.DataSource = mRetorna.Pendientes.AsEnumerable();
                gridPendientes.DataMember = "pendientes";
                gridPendientes.DataBind();
                gridPendientes.FocusedRowIndex = -1;
                gridPendientes.SettingsPager.PageSize = 15000;

                foreach (GridViewDataColumn item in gridComisiones.GetGroupedColumns())
                    item.UnGroup();

                foreach (GridViewDataColumn item in gridTiendas.GetGroupedColumns())
                    item.UnGroup();

                foreach (GridViewDataColumn item in gridFacturas.GetGroupedColumns())
                    item.UnGroup();

                foreach (GridViewDataColumn item in gridExcepciones.GetGroupedColumns())
                    item.UnGroup();

                foreach (GridViewDataColumn item in gridAnuladas.GetGroupedColumns())
                    item.UnGroup();

                foreach (GridViewDataColumn item in gridPendientes.GetGroupedColumns())
                    item.UnGroup();

                ((GridViewDataColumn)gridComisiones.Columns["Tienda"]).SortIndex = 0;
                ((GridViewDataColumn)gridComisiones.Columns["Vendedor"]).SortIndex = 1;

                ((GridViewDataColumn)gridComisiones.Columns["Tienda"]).SortAscending();
                ((GridViewDataColumn)gridComisiones.Columns["Vendedor"]).SortAscending();

                gridComisiones.GroupBy(gridComisiones.Columns["Titulo"], 0);
                gridComisiones.GroupBy(gridComisiones.Columns["Tienda"], 1);

                gridTiendas.GroupBy(gridTiendas.Columns["Titulo"], 0);

                gridFacturas.GroupBy(gridFacturas.Columns["Titulo"], 0);
                gridFacturas.GroupBy(gridFacturas.Columns["Tienda"], 1);
                gridFacturas.GroupBy(gridFacturas.Columns["NombreVendedor"], 2);

                gridExcepciones.GroupBy(gridExcepciones.Columns["Titulo"], 0);
                gridExcepciones.GroupBy(gridExcepciones.Columns["Tienda"], 1);

                gridAnuladas.GroupBy(gridAnuladas.Columns["Titulo"], 0);
                gridAnuladas.GroupBy(gridAnuladas.Columns["Tienda"], 1);

                gridPendientes.GroupBy(gridPendientes.Columns["Titulo"], 0);
                gridPendientes.GroupBy(gridPendientes.Columns["Tienda"], 1);
                gridPendientes.GroupBy(gridPendientes.Columns["NombreVendedor"], 2);

                if (mRetorna.Excepciones.Count() == 0) gridExcepciones.Visible = false;
                if (mRetorna.Anuladas.Count() == 0) gridAnuladas.Visible = false;
                if (mRetorna.Pendientes.Count() == 0) gridPendientes.Visible = false;

                Session["Comisiones"] = mRetorna;
                if (!mRetorna.Info[0].YaGrabadas && TipoFecha.Equals(Const.Comisiones.FRIEDMAN)) btnGrabar.Visible = true;

                btnGrabar.Visible = true;

                Clases.RetornaComisionesJefesSupervisores mRetornaJefesSupervisores = JsonConvert
                    .DeserializeObject<Clases.RetornaComisionesJefesSupervisores>(api.Process(Method.GET, $"/ComisionesJefesSupervisores/?anio1={mFechaInicial.Year}&mes1={mFechaInicial.Month}&dia1={mFechaInicial.Day}&anio2={mFechaFinal.Year}&mes2={mFechaFinal.Month}&dia2={mFechaFinal.Day}&vendedor=&usuario={Session["Usuario"].ToString()}&tienda=", null));

                if (!mRetornaJefesSupervisores.Info[0].Exito)
                {
                    lbErrorComisiones.Text = mRetornaJefesSupervisores.Info[0].Mensaje;
                    return;
                }

                gridJefesSupervisores.Visible = true;
                gridJefesSupervisores.DataSource = mRetornaJefesSupervisores.ComisionesJefesSupervisores.AsEnumerable().Where(x => x.Categoria != string.Empty);
                gridJefesSupervisores.DataMember = "ComisionesJefesSupervisores";
                gridJefesSupervisores.DataBind();
                gridJefesSupervisores.FocusedRowIndex = -1;
                gridJefesSupervisores.SettingsPager.PageSize = 500;

                gridDesgloceJefeSupervisor.Visible = true;
                gridDesgloceJefeSupervisor.DataSource = mRetornaJefesSupervisores.JefeSupervisorDetalle.AsEnumerable();
                gridDesgloceJefeSupervisor.DataMember = "DetalleJefeSupervisor";
                gridDesgloceJefeSupervisor.DataBind();
                gridDesgloceJefeSupervisor.FocusedRowIndex = -1;
                gridDesgloceJefeSupervisor.SettingsPager.PageSize = 500;

                gridDetalleComisionJefe.Visible = true;
                gridDetalleComisionJefe.DataSource = mRetornaJefesSupervisores.DetalleComisionesJefe.AsEnumerable();
                gridDetalleComisionJefe.DataMember = "DetalleComisionJefe";
                gridDetalleComisionJefe.DataBind();
                gridDetalleComisionJefe.FocusedRowIndex = -1;
                gridDetalleComisionJefe.SettingsPager.PageSize = 500;

                gridResumenComisionJefe.Visible = true;
                gridResumenComisionJefe.DataSource = mRetornaJefesSupervisores.ResumenJefe.AsEnumerable();
                gridResumenComisionJefe.DataMember = "ResumenComisionJefe";
                gridResumenComisionJefe.DataBind();
                gridResumenComisionJefe.FocusedRowIndex = -1;
                gridResumenComisionJefe.SettingsPager.PageSize = 500;

                gridDetalleComisionSupervisor.Visible = true;
                gridDetalleComisionSupervisor.DataSource = mRetornaJefesSupervisores.DetalleComisionesSupervisor.AsEnumerable();
                gridDetalleComisionSupervisor.DataMember = "DetalleComisionSupervisor";
                gridDetalleComisionSupervisor.DataBind();
                gridDetalleComisionSupervisor.FocusedRowIndex = -1;
                gridDetalleComisionSupervisor.SettingsPager.PageSize = 500;

                gridResumenComisionSupervisor.Visible = true;
                gridResumenComisionSupervisor.DataSource = mRetornaJefesSupervisores.ResumenSupervisor.AsEnumerable();
                gridResumenComisionSupervisor.DataMember = "ResumenComisionSupervisor";
                gridResumenComisionSupervisor.DataBind();
                gridResumenComisionSupervisor.FocusedRowIndex = -1;
                gridResumenComisionSupervisor.SettingsPager.PageSize = 500;

                if (mRetornaJefesSupervisores.ComisionesJefesSupervisores.Count() == 0) gridJefesSupervisores.Visible = false;

                Session["JefesSupervisores"] = mRetornaJefesSupervisores;
                VerifyIsSaved(mFechaInicial, mFechaFinal);
                VerifyDeleteCommission();
                VerifySendResume();
            }
            catch (Exception ex)
            {
                lbErrorComisiones.Text = CatchClass.ExMessage(ex, "administracion", "CargarComisiones");
            }
        }

        private void VerifyIsSaved(DateTime fechainicial, DateTime fechafinal)
        {
            Api api = new Api(General.FiestaNetRestService);

            this.IsSaved = JsonConvert
                    .DeserializeObject<bool>(api.Process(Method.GET, $"Comisiones/evaluar/?anio1={fechainicial.Year}&mes1={fechainicial.Month}&dia1={fechainicial.Day}&anio2={fechafinal.Year}&mes2={fechafinal.Month}&dia2={fechafinal.Day}", null));

            btnAplicar.Visible = this.IsSaved;
        }

        private void VerifyDeleteCommission()
        {
            Api api = new Api(General.FiestaNetRestService);

            this.IsSaved = JsonConvert
                    .DeserializeObject<bool>(api.Process(Method.PUT, $"Comisiones/EvaluarBoton?mes={mesComision.Value}&anio={anioComision.Value}&boton=BorrarComision", null));

            btnBorrarComision.Visible = this.IsSaved;
        }

        private void VerifySendResume()
        {
            Api api = new Api(General.FiestaNetRestService);

            this.IsSaved = JsonConvert
                    .DeserializeObject<bool>(api.Process(Method.PUT, $"Comisiones/EvaluarBoton?mes={mesComision.Value}&anio={anioComision.Value}&boton=EnviarResumen", null));

            btnResumen.Visible = this.IsSaved;
        }

        protected void gridComisiones_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.VisibleIndex >= 0 && e.GetValue("Tooltip").ToString().Trim().Length > 0) e.Cell.ToolTip = e.GetValue("Tooltip").ToString();
        }

        protected void gridComisiones_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
            if (Convert.ToString(e.GetValue("Tooltip")).Trim().Length > 0) e.Row.ForeColor = System.Drawing.Color.FromArgb(227, 89, 0);
        }

        protected void gridTiendas_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void gridFacturas_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void gridExcepciones_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void gridAnuladas_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void gridPendientes_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void gridJefesSupervisores_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void gridSupervisoresResumen_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        void ExportarTodo()
        {
            try
            {
                foreach (GridViewDataColumn item in gridComisiones.GetGroupedColumns())
                    item.UnGroup();

                foreach (GridViewDataColumn item in gridTiendas.GetGroupedColumns())
                    item.UnGroup();

                foreach (GridViewDataColumn item in gridFacturas.GetGroupedColumns())
                    item.UnGroup();

                foreach (GridViewDataColumn item in gridExcepciones.GetGroupedColumns())
                    item.UnGroup();

                foreach (GridViewDataColumn item in gridAnuladas.GetGroupedColumns())
                    item.UnGroup();

                foreach (GridViewDataColumn item in gridPendientes.GetGroupedColumns())
                    item.UnGroup();

                //foreach (GridViewDataColumn item in gridJefesSupervisores.GetGroupedColumns())
                //    item.UnGroup();

                ((GridViewDataColumn)gridComisiones.Columns["Tienda"]).SortIndex = 0;
                ((GridViewDataColumn)gridComisiones.Columns["Vendedor"]).SortIndex = 1;

                ((GridViewDataColumn)gridComisiones.Columns["Tienda"]).SortAscending();
                ((GridViewDataColumn)gridComisiones.Columns["Vendedor"]).SortAscending();

                ((GridViewDataColumn)gridTiendas.Columns["Tienda"]).SortIndex = 0;
                ((GridViewDataColumn)gridTiendas.Columns["Tienda"]).SortAscending();

                ((GridViewDataColumn)gridFacturas.Columns["Tienda"]).SortIndex = 0;
                ((GridViewDataColumn)gridFacturas.Columns["CodigoVendedor"]).SortIndex = 1;

                ((GridViewDataColumn)gridFacturas.Columns["Tienda"]).SortAscending();
                ((GridViewDataColumn)gridFacturas.Columns["CodigoVendedor"]).SortAscending();

                ((GridViewDataColumn)gridExcepciones.Columns["Tienda"]).SortIndex = 0;
                ((GridViewDataColumn)gridExcepciones.Columns["Vendedor"]).SortIndex = 1;

                ((GridViewDataColumn)gridExcepciones.Columns["Tienda"]).SortAscending();
                ((GridViewDataColumn)gridExcepciones.Columns["Vendedor"]).SortAscending();

                ((GridViewDataColumn)gridAnuladas.Columns["Tienda"]).SortIndex = 0;
                ((GridViewDataColumn)gridAnuladas.Columns["Vendedor"]).SortIndex = 1;

                ((GridViewDataColumn)gridAnuladas.Columns["Tienda"]).SortAscending();
                ((GridViewDataColumn)gridAnuladas.Columns["Vendedor"]).SortAscending();

                ((GridViewDataColumn)gridPendientes.Columns["Tienda"]).SortIndex = 0;
                ((GridViewDataColumn)gridPendientes.Columns["Vendedor"]).SortIndex = 1;

                ((GridViewDataColumn)gridPendientes.Columns["Tienda"]).SortAscending();
                ((GridViewDataColumn)gridPendientes.Columns["Vendedor"]).SortAscending();

                //((GridViewDataColumn)gridJefesSupervisores.Columns["Tienda"]).SortIndex = 0;
                //((GridViewDataColumn)gridJefesSupervisores.Columns["Tienda"]).SortAscending();

                gridComisiones.Columns["Tooltip"].Width = 300;
                gridComisiones.Columns["Tooltip"].Visible = true;

                gridFacturas.Columns["Tienda"].Visible = true;
                gridFacturas.Columns["CodigoVendedor"].Visible = true;
                gridFacturas.Columns["NombreVendedor"].Visible = true;
                gridFacturas.Columns["CodigoJefe"].Visible = true;
                gridFacturas.Columns["NombreJefe"].Visible = true;
                gridFacturas.Columns["CodigoSupervisor"].Visible = true;
                gridFacturas.Columns["NombreSupervisor"].Visible = true;
                gridFacturas.Columns["NivelPrecio"].Visible = true;
                gridFacturas.Columns["PorcentajeTarjeta"].Visible = true;
                gridFacturas.Columns["FormaDePago"].Visible = true;

                gridExcepciones.Columns["Tienda"].Visible = true;
                gridExcepciones.Columns["FechaFactura"].Visible = true;

                gridAnuladas.Columns["Tienda"].Visible = true;
                gridAnuladas.Columns["NombreVendedor"].Visible = true;

                gridPendientes.Columns["Tienda"].Visible = true;
                gridPendientes.Columns["Vendedor"].Visible = true;
                gridPendientes.Columns["NombreVendedor"].Visible = true;

                PrintingSystemBase ps = new PrintingSystemBase();
                ps.XlSheetCreated += ps_XlSheetCreated;

                PrintableComponentLinkBase lkComisiones = new PrintableComponentLinkBase(ps) { PaperName = "Comisiones", Component = xprComisiones },
                    lkFacturas = new PrintableComponentLinkBase(ps) { PaperName = "Facturas", Component = xprFacturas },
                    lkExcepciones = new PrintableComponentLinkBase(ps) { PaperName = "Excepciones", Component = xprExcepciones },
                    lkAnuladas = new PrintableComponentLinkBase(ps) { PaperName = "Anuladas", Component = xprAnuladas},
                    lkPendientes = new PrintableComponentLinkBase(ps){PaperName = "Pendientes", Component = xprPendientes},
                    lkTiendas = new PrintableComponentLinkBase(ps){PaperName = "Tiendas", Component = xprTiendas},
                    lkTiendaJefeSupervisor = new PrintableComponentLinkBase(ps){PaperName = "JefesSupervisores", Component = xprTiendaJefeSupervisor},
                    lkDetalleJefe = new PrintableComponentLinkBase(ps){PaperName = "DetalleJefe", Component = xprDetalleJefe},
                    lkResumenJefe = new PrintableComponentLinkBase(ps){PaperName = "ResumenJefe",Component = xprResumenJefe},
                    lkDetalleSupervisor = new PrintableComponentLinkBase(ps){PaperName = "DetalleSupervisor", Component = xprDetalleSupervisor},
                    lkResumenSupervisor = new PrintableComponentLinkBase(ps){PaperName = "ResumenSupervisor", Component = xprResumenSupervisor},
                    lkDesgloseComision = new PrintableComponentLinkBase(ps){PaperName = "DesgloseComisiones", Component = xprDesgloseComision};


                CompositeLinkBase compositeLink = new CompositeLinkBase(ps);
                compositeLink.Links.AddRange(new object[]
                {
                    lkTiendaJefeSupervisor,
                    lkDesgloseComision,
                    lkDetalleJefe,
                    lkResumenJefe,
                    lkDetalleSupervisor,
                    lkResumenSupervisor,
                    lkTiendas,
                    lkComisiones,
                    lkFacturas,
                    lkExcepciones,
                    lkAnuladas,
                    lkPendientes
                });

                compositeLink.CreatePageForEachLink();

                string mMes = "Enero";
                if (Convert.ToDateTime(fechaFinalComisiones.Value).Month == 2) mMes = "Febrero";
                if (Convert.ToDateTime(fechaFinalComisiones.Value).Month == 3) mMes = "Marzo";
                if (Convert.ToDateTime(fechaFinalComisiones.Value).Month == 4) mMes = "Abril";
                if (Convert.ToDateTime(fechaFinalComisiones.Value).Month == 5) mMes = "Mayo";
                if (Convert.ToDateTime(fechaFinalComisiones.Value).Month == 6) mMes = "Junio";
                if (Convert.ToDateTime(fechaFinalComisiones.Value).Month == 7) mMes = "Julio";
                if (Convert.ToDateTime(fechaFinalComisiones.Value).Month == 8) mMes = "Agosto";
                if (Convert.ToDateTime(fechaFinalComisiones.Value).Month == 9) mMes = "Septiembre";
                if (Convert.ToDateTime(fechaFinalComisiones.Value).Month == 10) mMes = "Octubre";
                if (Convert.ToDateTime(fechaFinalComisiones.Value).Month == 11) mMes = "Noviembre";
                if (Convert.ToDateTime(fechaFinalComisiones.Value).Month == 12) mMes = "Diciembre";

                string mName = "";
                if (ViewState["TipoComision"].Equals(Const.Comisiones.CALENDARIO))
                {
                    mName = "ComisionesCalendario";
                }
                else mName = "ComisionesFriedman";



                using (MemoryStream stream = new MemoryStream())
                {
                    XlsxExportOptions options = new XlsxExportOptions();
                    options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                    compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                    Response.Clear();
                    Response.Buffer = false;
                    Response.AppendHeader("Content-Type", "application/xlsx");
                    Response.AppendHeader("Content-Transfer-Encoding", "binary");
                    Response.AppendHeader("Content-Disposition", string.Format("attachment; filename={0}{1}{2}.xlsx", mName, mMes, Convert.ToDateTime(fechaFinalComisiones.Value).Year.ToString()));
                    Response.BinaryWrite(stream.ToArray());
                    Response.End();
                }
                ps.Dispose();

                gridComisiones.Columns["Tooltip"].Width = 0;
                gridComisiones.Columns["Tooltip"].Visible = false;

                gridFacturas.Columns["Tienda"].Visible = false;
                gridFacturas.Columns["CodigoVendedor"].Visible = false;
                gridFacturas.Columns["NombreVendedor"].Visible = false;
                gridFacturas.Columns["CodigoJefe"].Visible = false;
                gridFacturas.Columns["NombreJefe"].Visible = false;
                gridFacturas.Columns["CodigoSupervisor"].Visible = false;
                gridFacturas.Columns["NombreSupervisor"].Visible = false;
                gridFacturas.Columns["NivelPrecio"].Visible = false;
                gridFacturas.Columns["PorcentajeTarjeta"].Visible = false;

                gridExcepciones.Columns["Tienda"].Visible = false;
                gridExcepciones.Columns["FechaFactura"].Visible = false;

                gridAnuladas.Columns["Tienda"].Visible = false;
                gridAnuladas.Columns["NombreVendedor"].Visible = false;

                gridPendientes.Columns["Tienda"].Visible = false;
                gridPendientes.Columns["Vendedor"].Visible = false;
                gridPendientes.Columns["NombreVendedor"].Visible = false;

                gridComisiones.GroupBy(gridComisiones.Columns["Titulo"], 0);
                gridComisiones.GroupBy(gridComisiones.Columns["Tienda"], 1);

                gridTiendas.GroupBy(gridTiendas.Columns["Titulo"], 0);

                gridFacturas.GroupBy(gridFacturas.Columns["Titulo"], 0);
                gridFacturas.GroupBy(gridFacturas.Columns["Tienda"], 1);
                gridFacturas.GroupBy(gridFacturas.Columns["NombreVendedor"], 2);

                gridExcepciones.GroupBy(gridExcepciones.Columns["Titulo"], 0);
                gridExcepciones.GroupBy(gridExcepciones.Columns["Tienda"], 1);

                gridAnuladas.GroupBy(gridAnuladas.Columns["Titulo"], 0);
                gridAnuladas.GroupBy(gridAnuladas.Columns["Tienda"], 1);

                gridPendientes.GroupBy(gridPendientes.Columns["Titulo"], 0);
                gridPendientes.GroupBy(gridPendientes.Columns["Tienda"], 1);
                gridPendientes.GroupBy(gridPendientes.Columns["NombreVendedor"], 2);

                //gridJefesSupervisores.GroupBy(gridTiendas.Columns["Titulo"], 0);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorComisiones.Text = string.Format("Error al exportar las comisiones. {0} {1}", ex.Message, m);
            }
        }

        protected void lkExportarTodo_Click(object sender, EventArgs e)
        {
            ExportarTodo();
        }

        protected void ToolbarExport_ItemClick(object source, ExportItemClickEventArgs e)
        {
            try
            {
                lbInfoComisiones.Text = "";
                lbErrorComisiones.Text = "";

                if (Session["Comisiones"] == null)
                {
                    lbErrorComisiones.Text = "Debe cargar documentos de comisiones.";
                    return;
                }

                Clases.RetornaComisiones mRetorna = new Clases.RetornaComisiones();
                mRetorna = (Clases.RetornaComisiones)Session["Comisiones"];

                if (mRetorna.Comisiones.Count() == 0)
                {
                    lbErrorComisiones.Text = "Debe seleccionar un rango de fechas.";
                    return;
                }

                switch (e.ExportType)
                {
                    case DemoExportFormat.Com:
                        foreach (GridViewDataColumn item in gridComisiones.GetGroupedColumns())
                            item.UnGroup();

                        ((GridViewDataColumn)gridComisiones.Columns["Tienda"]).SortIndex = 0;
                        ((GridViewDataColumn)gridComisiones.Columns["Vendedor"]).SortIndex = 1;

                        ((GridViewDataColumn)gridComisiones.Columns["Tienda"]).SortAscending();
                        ((GridViewDataColumn)gridComisiones.Columns["Vendedor"]).SortAscending();

                        gridComisiones.Columns["Tooltip"].Width = 300;
                        gridComisiones.Columns["Tooltip"].Visible = true;


                        if (ViewState["TipoComision"].Equals(Const.Comisiones.CALENDARIO))
                        {
                            xprComisiones.FileName = "ComisionesCalendario";
                        }
                        else
                        {
                            xprComisiones.FileName = "ComisionesFriedman";
                        }



                        xprComisiones.GridViewID = "gridComisiones";
                        xprComisiones.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });

                        gridComisiones.Columns["Tooltip"].Width = 0;
                        gridComisiones.Columns["Tooltip"].Visible = false;

                        gridComisiones.GroupBy(gridComisiones.Columns["Titulo"], 0);
                        gridComisiones.GroupBy(gridComisiones.Columns["Tienda"], 1);

                        break;
                    case DemoExportFormat.Tien:
                        foreach (GridViewDataColumn item in gridTiendas.GetGroupedColumns())
                            item.UnGroup();

                        ((GridViewDataColumn)gridTiendas.Columns["Tienda"]).SortIndex = 0;
                        ((GridViewDataColumn)gridTiendas.Columns["Tienda"]).SortAscending();

                        if (ViewState["TipoComision"].Equals(Const.Comisiones.CALENDARIO))
                        {
                            xprComisiones.FileName = "TiendasCalendario";
                        }
                        else
                        {
                            xprComisiones.FileName = "TiendasFriedman";
                        }

                        xprComisiones.GridViewID = "gridTiendas";
                        xprComisiones.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });

                        gridTiendas.GroupBy(gridTiendas.Columns["Titulo"], 0);

                        break;
                    case DemoExportFormat.Fac:
                        if (mRetorna.Facturas.Count() == 0)
                        {
                            lbErrorComisiones.Text = "No hay facturas para exportar.";
                            return;
                        }

                        foreach (GridViewDataColumn item in gridFacturas.GetGroupedColumns())
                            item.UnGroup();

                        ((GridViewDataColumn)gridFacturas.Columns["Tienda"]).SortIndex = 0;
                        ((GridViewDataColumn)gridFacturas.Columns["Vendedor"]).SortIndex = 1;

                        ((GridViewDataColumn)gridFacturas.Columns["Tienda"]).SortAscending();
                        ((GridViewDataColumn)gridFacturas.Columns["Vendedor"]).SortAscending();

                        gridFacturas.Columns["Tienda"].Visible = true;
                        gridFacturas.Columns["Vendedor"].Visible = true;
                        gridFacturas.Columns["NombreVendedor"].Visible = true;
                        gridFacturas.Columns["NivelPrecio"].Visible = true;
                        gridFacturas.Columns["PorcentajeTarjeta"].Visible = true;
                        gridFacturas.Columns["FormaDePago"].Visible = true;

                        if (ViewState["TipoComision"].Equals(Const.Comisiones.CALENDARIO))
                        {
                            xprComisiones.FileName = "FacturasCalendario";
                        }
                        else
                        {
                            xprComisiones.FileName = "FacturasFriedman";
                        }

                        xprComisiones.GridViewID = "gridFacturas";
                        xprComisiones.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });

                        gridFacturas.Columns["Tienda"].Visible = false;
                        gridFacturas.Columns["Vendedor"].Visible = false;
                        gridFacturas.Columns["NombreVendedor"].Visible = false;
                        gridFacturas.Columns["NivelPrecio"].Visible = false;
                        gridFacturas.Columns["PorcentajeTarjeta"].Visible = false;

                        gridFacturas.GroupBy(gridFacturas.Columns["Titulo"], 0);
                        gridFacturas.GroupBy(gridFacturas.Columns["Tienda"], 1);
                        gridFacturas.GroupBy(gridFacturas.Columns["NombreVendedor"], 2);

                        break;
                    case DemoExportFormat.Exc:
                        if (mRetorna.Excepciones.Count() == 0)
                        {
                            lbErrorComisiones.Text = "No hay excepciones para exportar.";
                            return;
                        }

                        foreach (GridViewDataColumn item in gridExcepciones.GetGroupedColumns())
                            item.UnGroup();

                        ((GridViewDataColumn)gridExcepciones.Columns["Tienda"]).SortIndex = 0;
                        ((GridViewDataColumn)gridExcepciones.Columns["Vendedor"]).SortIndex = 1;

                        ((GridViewDataColumn)gridExcepciones.Columns["Tienda"]).SortAscending();
                        ((GridViewDataColumn)gridExcepciones.Columns["Vendedor"]).SortAscending();

                        gridExcepciones.Columns["Tienda"].Visible = true;
                        gridExcepciones.Columns["FechaFactura"].Visible = true;

                        if (ViewState["TipoComision"].Equals(Const.Comisiones.CALENDARIO))
                        {
                            xprComisiones.FileName = "ExcepcionesCalendario";
                        }
                        else
                        {
                            xprComisiones.FileName = "ExcepcionesFriedman";
                        }

                        xprComisiones.GridViewID = "gridExcepciones";
                        xprComisiones.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });

                        gridExcepciones.Columns["Tienda"].Visible = false;
                        gridExcepciones.Columns["FechaFactura"].Visible = false;

                        gridExcepciones.GroupBy(gridExcepciones.Columns["Titulo"], 0);
                        gridExcepciones.GroupBy(gridExcepciones.Columns["Tienda"], 1);

                        break;
                    case DemoExportFormat.Anu:
                        if (mRetorna.Anuladas.Count() == 0)
                        {
                            lbErrorComisiones.Text = "No hay facturas anuladas para exportar.";
                            return;
                        }

                        foreach (GridViewDataColumn item in gridAnuladas.GetGroupedColumns())
                            item.UnGroup();

                        ((GridViewDataColumn)gridAnuladas.Columns["Tienda"]).SortIndex = 0;
                        ((GridViewDataColumn)gridAnuladas.Columns["Vendedor"]).SortIndex = 1;

                        ((GridViewDataColumn)gridAnuladas.Columns["Tienda"]).SortAscending();
                        ((GridViewDataColumn)gridAnuladas.Columns["Vendedor"]).SortAscending();

                        gridAnuladas.Columns["Tienda"].Visible = true;
                        gridAnuladas.Columns["NombreVendedor"].Visible = true;

                        if (ViewState["TipoComision"].Equals(Const.Comisiones.CALENDARIO))
                        {
                            xprComisiones.FileName = "AnuladasCalendario";
                        }
                        else
                        {
                            xprComisiones.FileName = "AnuladasFriedman";
                        }

                        xprComisiones.GridViewID = "gridAnuladas";
                        xprComisiones.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });

                        gridAnuladas.Columns["Tienda"].Visible = false;
                        gridAnuladas.Columns["NombreVendedor"].Visible = false;

                        gridAnuladas.GroupBy(gridAnuladas.Columns["Titulo"], 0);
                        gridAnuladas.GroupBy(gridAnuladas.Columns["Tienda"], 1);

                        break;
                    case DemoExportFormat.Pend:
                        if (mRetorna.Pendientes.Count() == 0)
                        {
                            lbErrorComisiones.Text = "No hay facturas pendientes para exportar.";
                            return;
                        }

                        foreach (GridViewDataColumn item in gridPendientes.GetGroupedColumns())
                            item.UnGroup();

                        ((GridViewDataColumn)gridPendientes.Columns["Tienda"]).SortIndex = 0;
                        ((GridViewDataColumn)gridPendientes.Columns["Vendedor"]).SortIndex = 1;

                        ((GridViewDataColumn)gridPendientes.Columns["Tienda"]).SortAscending();
                        ((GridViewDataColumn)gridPendientes.Columns["Vendedor"]).SortAscending();

                        gridPendientes.Columns["Tienda"].Visible = true;
                        gridPendientes.Columns["Vendedor"].Visible = true;
                        gridPendientes.Columns["NombreVendedor"].Visible = true;

                        if (ViewState["TipoComision"].Equals(Const.Comisiones.CALENDARIO))
                        {
                            xprComisiones.FileName = "PendientesCalendario";
                        }
                        else
                        {
                            xprComisiones.FileName = "PendientesFriedman";
                        }

                        xprComisiones.GridViewID = "gridPendientes";
                        xprComisiones.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });

                        gridPendientes.Columns["Tienda"].Visible = false;
                        gridPendientes.Columns["Vendedor"].Visible = false;
                        gridPendientes.Columns["NombreVendedor"].Visible = false;

                        gridPendientes.GroupBy(gridPendientes.Columns["Titulo"], 0);
                        gridPendientes.GroupBy(gridPendientes.Columns["Tienda"], 1);
                        gridPendientes.GroupBy(gridPendientes.Columns["NombreVendedor"], 2);

                        break;
                    case DemoExportFormat.All:
                        ExportarTodo();
                        break;
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorComisiones.Text = string.Format("Error al exportar las comisiones. {0} {1}", ex.Message, m);
            }

        }

        void ps_XlSheetCreated(object sender, XlSheetCreatedEventArgs e)
        {
            if (e.Index == 0) e.SheetName = "% Tienda";
            if (e.Index == 1) e.SheetName = "Desglose";
            if (e.Index == 2) e.SheetName = "DetalleJefe";
            if (e.Index == 3) e.SheetName = "ResumenJefe";
            if (e.Index == 4) e.SheetName = "DetalleSupervisor";
            if (e.Index == 5) e.SheetName = "ResumenSupervisor";
            if (e.Index == 6) e.SheetName = "Tienda";
            if (e.Index == 7) e.SheetName = "Comisión";
            if (e.Index == 8) e.SheetName = "Factura";
            if (e.Index == 9) e.SheetName = "Excepción";
            if (e.Index == 10) e.SheetName = "Anulada";
            if (e.Index == 11) e.SheetName = "Pendiente";
        }

        protected void lkFacturaComision_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoComisiones.Text = "";
                lbErrorComisiones.Text = "";

                var btn = (sender as ASPxButton);
                var nc = btn.NamingContainer as GridViewDataItemTemplateContainer;

                string mFactura = DataBinder.Eval(nc.DataItem, "Factura").ToString();

                txtFacturaModifica.Text = mFactura;

                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMontoFactura = ""; string mComisionable = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                mMontoFactura = ws.devuelveMontoFactura(mFactura, ref mComisionable);

                if (mMontoFactura.Length > 20)
                {
                    txtMontoFactura.Text = "";
                    txtMontoComisionable.Text = "";
                    lbErrorModificaFactura.Text = mMontoFactura;
                }
                else
                {
                    txtMontoFactura.Text = mMontoFactura;
                    txtMontoComisionable.Text = mComisionable;
                }

                txtMontoComision.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorComisiones.Text = string.Format("Error al seleccionar la factura. {0} {1}", ex.Message, m);
            }
        }

        protected void lkFacturaPendiente_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoComisiones.Text = "";
                lbErrorComisiones.Text = "";

                var btn = (sender as ASPxButton);
                var nc = btn.NamingContainer as GridViewDataItemTemplateContainer;

                string mFactura = DataBinder.Eval(nc.DataItem, "Factura").ToString();
                string mVendedor = DataBinder.Eval(nc.DataItem, "Vendedor").ToString();
                string mCobrador = DataBinder.Eval(nc.DataItem, "Tienda").ToString();

                DateTime mFechaInicial = Convert.ToDateTime(fechaInicialComisiones.Value);
                DateTime mFechaFinal = Convert.ToDateTime(fechaFinalComisiones.Value);
                DateTime mFechaFactura = Convert.ToDateTime(DataBinder.Eval(nc.DataItem, "FechaFactura"));

                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                ws.Timeout = 999999999;
                if (!ws.QuitarFacturaPendiente(mFactura, mCobrador, mVendedor, mFechaFactura, mFechaInicial, mFechaFinal, ref mMensaje, Convert.ToString(Session["Usuario"])))
                {
                    lbErrorComisiones.Text = mMensaje;
                    return;
                }

                Clases.RetornaComisiones mRetorna = new Clases.RetornaComisiones();
                mRetorna = (Clases.RetornaComisiones)Session["Comisiones"];

                mRetorna.Pendientes.Remove(mRetorna.Pendientes.Where(x => x.Factura.Equals(mFactura)).First());

                gridPendientes.DataSource = mRetorna.Pendientes.AsEnumerable();
                gridPendientes.DataMember = "pendientes";
                gridPendientes.DataBind();
                gridPendientes.FocusedRowIndex = -1;
                gridPendientes.SettingsPager.PageSize = 15000;

                foreach (GridViewDataColumn item in gridPendientes.GetGroupedColumns())
                    item.UnGroup();

                gridPendientes.GroupBy(gridPendientes.Columns["Titulo"], 0);
                gridPendientes.GroupBy(gridPendientes.Columns["Tienda"], 1);
                gridPendientes.GroupBy(gridPendientes.Columns["NombreVendedor"], 2);

                if (mRetorna.Pendientes.Count() == 0) gridPendientes.Visible = false;

                Session["Comisiones"] = mRetorna;
                lbInfoComisiones.Text = mMensaje;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorComisiones.Text = string.Format("Error al seleccionar la factura. {0} {1}", ex.Message, m);
            }
        }

        void CargarFacturasRevisar()
        {
            try
            {
                lbErrorFacturasRevisar.Text = "";
                lbInfoFacturasRevisar.Text = "";

                gridFacturasRevisar.Visible = false;
                gridFacturaRevisarDet.Visible = false;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DataSet ds = new DataSet(); string mMensaje = "";
                ds = ws.FacturasRevisar(ref mMensaje);

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorFacturasRevisar.Text = mMensaje;
                    return;
                }

                if (ds.Tables["documentos"].Rows.Count > 0)
                {
                    gridFacturasRevisar.Visible = true;
                    gridFacturasRevisar.DataSource = ds;
                    gridFacturasRevisar.DataMember = "documentos";
                    gridFacturasRevisar.DataBind();
                    gridFacturasRevisar.FocusedRowIndex = -1;
                    gridFacturasRevisar.SettingsPager.PageSize = 500;

                    Session["dsFacturasRevisar"] = ds;
                    txtFacturaRevisar.Text = "";
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorFacturasRevisar.Text = string.Format("Error cargando facturas. {0} {1}", ex.Message, m);
            }
        }

        protected void lkFacturasRevisar_Click(object sender, EventArgs e)
        {
            CargarFacturasRevisar();
        }

        protected void txtFacturaRevisar_TextChanged(object sender, EventArgs e)
        {
            CargarFacturaRevisar();
        }

        protected void lkCargarFacturaRevisar_Click(object sender, EventArgs e)
        {
            CargarFacturaRevisar();
        }

        protected void CancelEditingFacturaRevisarDet(CancelEventArgs e)
        {
            e.Cancel = true;
            gridFacturaRevisarDet.CancelEdit();
        }

        void UpdateFacturaRevisar(string linea, decimal PrecioUnitario)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsFacturaRevisar"];

                ds.Tables["documento"].Rows.Find(linea)["PrecioUnitario"] = PrecioUnitario;
                ds.Tables["documento"].Rows.Find(linea)["PrecioTotal"] = Math.Round(PrecioUnitario * Convert.ToDecimal(ds.Tables["documento"].Rows.Find(linea)["Cantidad"]), 2, MidpointRounding.AwayFromZero);
                Session["dsArticulosPedido"] = ds;

                gridFacturaRevisarDet.DataSource = ds;
                gridFacturaRevisarDet.DataMember = "documento";
                gridFacturaRevisarDet.DataBind();
                gridFacturaRevisarDet.FocusedRowIndex = -1;
                gridFacturaRevisarDet.SettingsPager.PageSize = 100;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorFacturasRevisar.Text = string.Format("Error al actualizar los datos UPDATE {0} {1}", ex.Message, m);
            }
        }

        protected void gridFacturaRevisarDet_BatchUpdate(object sender, ASPxDataBatchUpdateEventArgs e)
        {
            foreach (var args in e.UpdateValues)
                UpdateFacturaRevisar(args.Keys["Linea"].ToString(), Convert.ToDecimal(args.NewValues["PrecioUnitario"]));

            e.Handled = true;
        }

        protected void gridFacturaRevisarDet_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {

        }

        protected void gridFacturaRevisarDet_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == ColumnCommandButtonType.Update || e.ButtonType == ColumnCommandButtonType.Cancel) e.Visible = false;
        }

        protected void gridFacturaRevisarDet_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {

        }

        protected void gridFacturaRevisarDet_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {

        }

        protected void gridFacturaRevisarDet_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            UpdateFacturaRevisar(e.Keys["Linea"].ToString(), Convert.ToDecimal(e.NewValues["PrecioUnitario"]));
            CancelEditingFacturaRevisarDet(e);
        }

        protected void gridFacturasRevisar_BatchUpdate(object sender, ASPxDataBatchUpdateEventArgs e)
        {

        }

        protected void gridFacturasRevisar_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {

        }

        protected void gridFacturasRevisar_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {

        }

        protected void gridFacturasRevisar_FocusedRowChanged(object sender, EventArgs e)
        {
            if (!IsPostBack) return;
            if (gridFacturasRevisar.FocusedRowIndex == -1) return;
            if (txtFacturaRevisar.Text.Trim().Length > 0) return;
            if (Convert.ToString(gridFacturasRevisar.GetRowValues(gridFacturasRevisar.FocusedRowIndex, "Factura")).Trim().Length == 0) return;
            if (txtFacturaRevisar.Text == Convert.ToString(gridFacturasRevisar.GetRowValues(gridFacturasRevisar.FocusedRowIndex, "Factura"))) return;

            txtFacturaRevisar.Text = Convert.ToString(gridFacturasRevisar.GetRowValues(gridFacturasRevisar.FocusedRowIndex, "Factura"));
            CargarFacturaRevisar();
        }

        void CargarFacturaRevisar()
        {
            try
            {
                lbErrorFacturasRevisar.Text = "";
                lbInfoFacturasRevisar.Text = "";

                if (txtFacturaRevisar.Text.Trim().Length == 0) return;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DataSet ds = new DataSet(); string mMensaje = "";
                ds = ws.FacturaRevisar(ref mMensaje, txtFacturaRevisar.Text.Trim().ToUpper());

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorFacturasRevisar.Text = mMensaje;
                    return;
                }

                if (ds.Tables["documento"].Rows.Count > 0)
                {
                    gridFacturaRevisarDet.Visible = true;
                    gridFacturaRevisarDet.DataSource = ds;
                    gridFacturaRevisarDet.DataMember = "documento";
                    gridFacturaRevisarDet.DataBind();
                    gridFacturaRevisarDet.FocusedRowIndex = -1;
                    gridFacturaRevisarDet.SettingsPager.PageSize = 100;
                }

                Session["dsFacturaRevisar"] = ds;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorFacturasRevisar.Text = string.Format("Error al mostrar la factura. {0} {1}", ex.Message, m);
            }
        }

        protected void gridFacturasRevisar_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {

        }

        protected void gridFacturasRevisar_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {

        }

        protected void btnFacturaRevisar_Click(object sender, EventArgs e)
        {
            try
            {
                lbErrorFacturasRevisar.Text = "";
                lbInfoFacturasRevisar.Text = "";

                if (txtFacturaRevisar.Text.Trim().Length == 0) return;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DataSet ds = new DataSet(); string mMensaje = "";
                ds = (DataSet)Session["dsFacturaRevisar"];

                if (!ws.ActualizarFactura(ref mMensaje, Convert.ToString(Session["Usuario"]), txtFacturaRevisar.Text.Trim().ToUpper(), ds))
                {
                    lbErrorFacturasRevisar.Text = mMensaje;
                    return;
                }

                gridFacturaRevisarDet.Visible = false;

                DataSet dsFacturasRevisar = new DataSet();
                dsFacturasRevisar = (DataSet)Session["dsFacturasRevisar"];

                int mSiguiente = 0;

                try
                {
                    for (int ii = 0; ii < dsFacturasRevisar.Tables["documentos"].Rows.Count; ii++)
                    {
                        if (txtFacturaRevisar.Text.Trim().ToUpper() == Convert.ToString(dsFacturasRevisar.Tables["documentos"].Rows[ii]["Factura"])) mSiguiente = ii;
                    }

                    dsFacturasRevisar.Tables["documentos"].Rows.Find(txtFacturaRevisar.Text.Trim().ToUpper()).Delete();
                    dsFacturasRevisar.AcceptChanges();

                    Session["dsFacturasRevisar"] = dsFacturasRevisar;
                }
                catch
                {
                    //Nothing
                }

                txtFacturaRevisar.Text = "";

                gridFacturasRevisar.DataSource = dsFacturasRevisar;
                gridFacturasRevisar.DataMember = "documentos";
                gridFacturasRevisar.DataBind();
                gridFacturasRevisar.SettingsPager.PageSize = 500;
                gridFacturasRevisar.FocusedRowIndex = -1;

                try
                {
                    txtFacturaRevisar.Text = Convert.ToString(dsFacturasRevisar.Tables["documentos"].Rows[mSiguiente]["Factura"]);
                    CargarFacturaRevisar();
                }
                catch
                {
                    //Nothing
                }

                lbInfoFacturasRevisar.Text = mMensaje;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorFacturasRevisar.Text = string.Format("Error actualizando la factura. {0} {1}", ex.Message, m);
            }
        }

        protected void btnLimpiarFacturaRevisar_Click(object sender, EventArgs e)
        {
            txtFacturaRevisar.Text = "";
            gridFacturaRevisarDet.Visible = false;
        }

        public class Retorna
        {
            public bool exito { get; set; }
            public string mensaje { get; set; }
        }

        protected void tipoExcepcion_SelectedIndexChanged(object sender, EventArgs e)
        {
            mesPago.Text = "";
            mesPago.Value = null;

            anioPago.Text = "";
            anioPago.Value = null;

            mesPago.ReadOnly = true;
            anioPago.ReadOnly = true;

            txtMontoComision.Text = "";

            if (Convert.ToString(tipoExcepcion.Value) == "P")
            {
                mesPago.ReadOnly = false;
                anioPago.ReadOnly = false;

                txtMontoComision.Text = txtMontoComisionable.Text;
                mesPago.Focus();
            }
        }

        protected void btnFacturar_Click(object sender, EventArgs e)
        {
            try
            {
                lbErrorFacturar.Text = "";
                lbInfoFacturar.Text = "";

                if (txtPedidoFactura.Text.Trim().Length == 0) return;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU")
                    ws.Url = Convert.ToString(Session["url"]);

                DataSet ds = new DataSet(); string mMensaje = "";
                ds = (DataSet)Session["dsArticulosPedido"];

                string mFactura = txtNumeroFactura.Text.Trim().ToUpper();

                ws.Timeout = 999999999;
                if (!ws.FacturarPedido(ref mMensaje, Convert.ToString(Session["Usuario"]), txtPedidoFactura.Text.Trim().ToUpper(), Convert.ToDateTime(fechaEntregaPedido.Value), ds, ref mFactura, Convert.ToString(cbSerie.Value), txtOrdenCompra.Text.Trim().ToUpper(), Convert.ToDateTime(fechaFactura.Value)))
                {
                    lbErrorFacturar.Text = mMensaje;
                    return;
                }

                gridPedidoDet.Visible = false;

                DataSet dsPedidos = new DataSet();
                dsPedidos = (DataSet)Session["dsPedidos"];

                int mSiguiente = 0;

                try
                {
                    for (int ii = 0; ii < dsPedidos.Tables["documentos"].Rows.Count; ii++)
                    {
                        if (txtPedidoFactura.Text.Trim().ToUpper() == Convert.ToString(dsPedidos.Tables["documentos"].Rows[ii]["Pedido"])) mSiguiente = ii;
                    }

                    dsPedidos.Tables["documentos"].Rows.Find(txtPedidoFactura.Text.Trim().ToUpper()).Delete();
                    dsPedidos.AcceptChanges();

                    Session["dsPedidos"] = dsPedidos;
                }
                catch
                {
                    //Nothing
                }

                txtPedidoFactura.Text = "";
                txtNombreClientePedido.Text = "";
                txtTelefonosClientePedido.Text = "";
                txtDireccionPedido.Text = "";
                txtOrdenCompra.Text = "";

                gridPedidos.DataSource = dsPedidos;
                gridPedidos.DataMember = "documentos";
                gridPedidos.DataBind();
                gridPedidos.SettingsPager.PageSize = 500;
                gridPedidos.FocusedRowIndex = -1;

                try
                {
                    txtPedidoFactura.Text = Convert.ToString(dsPedidos.Tables["documentos"].Rows[mSiguiente]["Pedido"]);
                    CargarPedido();
                }
                catch
                {
                    //Nothing
                }

                NumeroFactura();
                lbInfoFacturar.Text = mMensaje;
                txtFacturaImprimir.Text = mFactura;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorFacturar.Text = string.Format("Error facturando. {0} {1}", ex.Message, m);
            }
        }

        protected void btnLimpiarPedido_Click(object sender, EventArgs e)
        {
            txtPedidoFactura.Text = "";
            txtNombreClientePedido.Text = "";
            txtTelefonosClientePedido.Text = "";
            txtDireccionPedido.Text = "";
            txtOrdenCompra.Text = "";
            gridPedidos.FocusedRowIndex = -1;
            gridPedidoDet.Visible = false;
        }

        void CargarPedidos(string tipo)
        {
            try
            {
                lbErrorFacturar.Text = "";
                lbInfoFacturar.Text = "";

                gridPedidos.Visible = false;
                gridPedidoDet.Visible = false;

                txtNombreClientePedido.Text = "";
                txtTelefonosClientePedido.Text = "";
                txtDireccionPedido.Text = "";


                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DateTime mFechaInicial = Convert.ToDateTime(fechaInicialPedidos.Value);
                DateTime mFechaFinal = Convert.ToDateTime(fechaFinalPedidos.Value);

                if (tipo == "E")
                {
                    mFechaInicial = Convert.ToDateTime(fechaInicialPedidos.Value);
                    mFechaFinal = Convert.ToDateTime(fechaFinalPedidos.Value);
                }

                DataSet ds = new DataSet(); string mMensaje = "";
                ds = ws.PedidosPorFacturar(ref mMensaje, mFechaInicial, mFechaFinal, txtClientePedido.Text.Trim().ToUpper(), tipo);

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorFacturar.Text = mMensaje;
                    return;
                }

                if (ds.Tables["documentos"].Rows.Count > 0)
                {
                    gridPedidos.Visible = true;
                    gridPedidos.DataSource = ds;
                    gridPedidos.DataMember = "documentos";
                    gridPedidos.DataBind();
                    gridPedidos.FocusedRowIndex = -1;
                    gridPedidos.SettingsPager.PageSize = 500;

                    Session["dsPedidos"] = ds;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorFacturar.Text = string.Format("Error cargando pedidos. {0} {1}", ex.Message, m);
            }
        }

        protected void lkSeleccionarFechasPedidos_Click(object sender, EventArgs e)
        {
            CargarPedidos("E");
        }

        protected void lkSeleccionarClientePedido_Click(object sender, EventArgs e)
        {
            CargarPedidos("C");
        }

        protected void lkCargarPedido_Click(object sender, EventArgs e)
        {
            CargarPedido();
        }

        protected void txtClientePedido_TextChanged(object sender, EventArgs e)
        {
            CargarPedidos("C");
        }

        protected void CancelEditingPedidoDet(CancelEventArgs e)
        {
            e.Cancel = true;
            gridPedidoDet.CancelEdit();
        }

        void UpdatePedido(string linea, string bodega, string cantidad, string localizacion, decimal PrecioBase, string ArticuloAnterior, string ArticuloNuevo)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsArticulosPedido"];

                if (ArticuloAnterior != ArticuloNuevo)
                {
                    if (ds.Tables["documento"].Rows.Find(linea)["PrecioBase"].ToString() == "K")
                    {
                        lbErrorFacturar.Text = "No es posible cambiar el SET, para eliminarlo de la factura, coloque 'cero' en la cantidad.";

                        gridPedidoDet.DataSource = ds;
                        gridPedidoDet.DataMember = "documento";
                        gridPedidoDet.DataBind();

                        return;
                    }

                    Clases.ArticuloPedido mInfo = new Clases.ArticuloPedido();
                    mInfo.Pedido = ds.Tables["documento"].Rows.Find(linea)["Pedido"].ToString();
                    mInfo.Cliente = ds.Tables["documento"].Rows.Find(linea)["Cliente"].ToString();
                    mInfo.Articulo = ArticuloAnterior;
                    mInfo.Linea = Convert.ToInt32(linea);
                    mInfo.Tipo = ds.Tables["documento"].Rows.Find(linea)["Tipo"].ToString();
                    mInfo.PrecioBase = Convert.ToDecimal(ds.Tables["documento"].Rows.Find(linea)["PrecioBase"]);
                    mInfo.ArticuloNuevo = ArticuloNuevo;
                    mInfo.Cantidad = Convert.ToInt32(ds.Tables["documento"].Rows.Find(linea)["Cantidad"]);

                    string json = JsonConvert.SerializeObject(mInfo);
                    byte[] data = Encoding.ASCII.GetBytes(json);

                    WebRequest request = WebRequest.Create(string.Format("{0}/CambiaArticuloPedido", Convert.ToString(Session["UrlRestServices"])));

                    request.Method = "PUT";
                    request.ContentType = "application/json";
                    request.ContentLength = data.Length;

                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }

                    var response = (HttpWebResponse)request.GetResponse();
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    Clases.ArticuloPedido mRetorna = new Clases.ArticuloPedido();
                    mRetorna = JsonConvert.DeserializeObject<Clases.ArticuloPedido>(responseString);

                    if (!mRetorna.Exito)
                    {
                        lbErrorFacturar.Text = mRetorna.Mensaje;

                        gridPedidoDet.DataSource = ds;
                        gridPedidoDet.DataMember = "documento";
                        gridPedidoDet.DataBind();

                        return;
                    }

                    ds.Tables["documento"].Rows.Find(linea)["Articulo"] = mRetorna.ArticuloNuevo;
                    ds.Tables["documento"].Rows.Find(linea)["Descripcion"] = mRetorna.DescripcionNueva;
                    ds.Tables["documento"].Rows.Find(linea)["PrecioBase"] = mRetorna.PrecioBaseNuevo;
                    ds.Tables["documento"].Rows.Find(linea)["PrecioUnitario"] = mRetorna.PrecioNuevo;
                    ds.Tables["documento"].Rows.Find(linea)["PrecioTotal"] = mRetorna.PrecioTotal;
                    ds.Tables["documento"].Rows.Find(linea)["Tooltip"] = mRetorna.Tooltip;
                }
                else
                {
                    if (ds.Tables["documento"].Rows.Find(linea)["Tipo"].ToString() != "C")
                    {
                        decimal mPrecioUnitario = Math.Round(PrecioBase * Convert.ToDecimal(ds.Tables["documento"].Rows.Find(linea)["Iva"]), 2, MidpointRounding.AwayFromZero);

                        ds.Tables["documento"].Rows.Find(linea)["Bodega"] = bodega;
                        ds.Tables["documento"].Rows.Find(linea)["Cantidad"] = Convert.ToInt32(cantidad);
                        ds.Tables["documento"].Rows.Find(linea)["Localizacion"] = localizacion;



                        ds.Tables["documento"].Rows.Find(linea)["PrecioBase"] = PrecioBase;
                        ds.Tables["documento"].Rows.Find(linea)["PrecioUnitario"] = mPrecioUnitario;
                        ds.Tables["documento"].Rows.Find(linea)["PrecioTotal"] = Math.Round(mPrecioUnitario * Convert.ToDecimal(ds.Tables["documento"].Rows.Find(linea)["Cantidad"]), 2, MidpointRounding.AwayFromZero);
                    }
                }

                Session["dsArticulosPedido"] = ds;

                gridPedidoDet.DataSource = ds;
                gridPedidoDet.DataMember = "documento";
                gridPedidoDet.DataBind();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorFacturar.Text = string.Format("Error al actualizar los datos UPDATE {0} {1}", ex.Message, m);
            }
        }

        protected void gridPedidoDet_BatchUpdate(object sender, ASPxDataBatchUpdateEventArgs e)
        {
            //foreach (var args in e.UpdateValues)
            //    UpdatePedido(args.Keys["Linea"].ToString(), args.NewValues["Bodega"].ToString(), args.NewValues["Cantidad"].ToString(), args.NewValues["Localizacion"].ToString(), Convert.ToDecimal(args.NewValues["PrecioBase"]));

            //e.Handled = true;
        }

        protected void gridPedidoDet_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            if (e.Column.FieldName == "Bodega")
            {
                var q = ws.DevuelveTiendasTodas();

                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                cmb.DataSource = q;
                cmb.ValueField = "Tienda";
                cmb.ValueType = typeof(System.String);
                cmb.TextField = "Tienda";
                cmb.DataBindItems();
            }

            if (e.Column.FieldName == "Localizacion")
            {
                DataSet ds = new DataSet();
                ds = ws.Localizaciones();

                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                cmb.DataSource = ds.Tables["Localizaciones"];
                cmb.ValueField = "Localizacion";
                cmb.ValueType = typeof(System.String);
                cmb.TextField = "Localizacion";
                cmb.DataBindItems();
            }
        }

        protected void gridPedidoDet_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == ColumnCommandButtonType.Update || e.ButtonType == ColumnCommandButtonType.Cancel) e.Visible = false;
        }

        protected void gridPedidoDet_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.VisibleIndex >= 0) e.Cell.ToolTip = e.GetValue("Tooltip").ToString();
        }

        protected void gridPedidoDet_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            // && Convert.ToString(e.GetValue("Articulo")).Substring(0, 1) != "S"
            try
            {
                if (Convert.ToInt32(e.GetValue("Existencias")) == 0 && Convert.ToString(e.GetValue("Tipo")) != "K" && Convert.ToString(e.GetValue("Articulo")).Substring(0, 1) != "S") e.Row.ForeColor = System.Drawing.Color.Red;
                if (!Convert.ToString(e.GetValue("Tooltip")).Contains(Convert.ToString(e.GetValue("Bodega"))) && Convert.ToString(e.GetValue("Tipo")) != "K" && Convert.ToString(e.GetValue("Articulo")).Substring(0, 1) != "S")
                    e.Row.ForeColor = System.Drawing.Color.Red;
            }
            catch
            {
                //Nothing
            }
        }

        protected void gridPedidoDet_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            UpdatePedido(e.Keys["Linea"].ToString(), e.NewValues["Bodega"].ToString(), e.NewValues["Cantidad"].ToString(), e.NewValues["Localizacion"].ToString(), Convert.ToDecimal(e.NewValues["PrecioBase"]), e.OldValues["Articulo"].ToString(), e.NewValues["Articulo"].ToString());
            CancelEditingPedidoDet(e);
        }

        protected void gridPedidos_FocusedRowChanged(object sender, EventArgs e)
        {
            if (!IsPostBack) return;
            if (gridPedidos.FocusedRowIndex == -1) return;
            if (txtPedidoFactura.Text.Trim().Length > 0) return;
            if (Convert.ToString(gridPedidos.GetRowValues(gridPedidos.FocusedRowIndex, "Pedido")).Trim().Length == 0) return;
            if (txtPedidoFactura.Text == Convert.ToString(gridPedidos.GetRowValues(gridPedidos.FocusedRowIndex, "Pedido"))) return;

            txtPedidoFactura.Text = Convert.ToString(gridPedidos.GetRowValues(gridPedidos.FocusedRowIndex, "Pedido"));
            CargarPedido();
        }

        void CargarPedido()
        {
            try
            {
                lbErrorFacturar.Text = "";
                lbInfoFacturar.Text = "";

                if (txtPedidoFactura.Text.Trim().Length == 0) return;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DataSet ds = new DataSet(); string mMensaje = "";
                ds = ws.PedidoPorFacturar(ref mMensaje, txtPedidoFactura.Text.Trim().ToUpper());

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorFacturar.Text = mMensaje;
                    return;
                }

                if (ds.Tables["documento"].Rows.Count > 0)
                {
                    gridPedidoDet.Visible = true;
                    gridPedidoDet.DataSource = ds;
                    gridPedidoDet.DataMember = "documento";
                    gridPedidoDet.DataBind();
                    gridPedidoDet.FocusedRowIndex = -1;
                    gridPedidoDet.SettingsPager.PageSize = 100;

                    txtNombreClientePedido.Text = string.Format("{0} - {1}", Convert.ToString(ds.Tables["documento"].Rows[0]["Cliente"]), Convert.ToString(ds.Tables["documento"].Rows[0]["NombreCliente"]));
                    txtTelefonosClientePedido.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Telefonos"]);
                    txtDireccionPedido.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Direccion"]);
                    fechaEntregaPedido.Value = Convert.ToDateTime(ds.Tables["documento"].Rows[0]["FechaEntrega"]);
                    cbSerie.Value = Convert.ToString(ds.Tables["documento"].Rows[0]["Serie"]);
                    txtOrdenCompra.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Observaciones"]);

                    fechaFactura.Value = DateTime.Now.Date;
                    NumeroFactura();
                }

                Session["dsArticulosPedido"] = ds;
                txtOrdenCompra.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorFacturar.Text = string.Format("Error al mostrar el pedido. {0} {1}", ex.Message, m);
            }
        }

        protected void gridPedidos_CustomButtonInitialize(object sender, ASPxGridViewCustomButtonEventArgs e)
        {

        }

        protected void btnFlete_Click(object sender, EventArgs e)
        {
            try
            {
                lbErrorFacturar.Text = "";
                lbInfoFacturar.Text = "";

                if (txtFlete.Text.Trim().Length == 0) return;

                decimal mFlete = 0;

                try
                {
                    mFlete = Convert.ToDecimal(txtFlete.Text);
                }
                catch
                {
                    lbErrorFacturar.Text = "El flete ingresado es inválido.";
                    return;
                }

                if (mFlete <= 0)
                {
                    lbErrorFacturar.Text = "El flete ingresado es inválido.";
                    return;
                }

                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsArticulosPedido"];

                DataRow row = ds.Tables["documento"].NewRow();
                row["Pedido"] = ds.Tables["documento"].Rows[0]["Pedido"];
                row["Linea"] = ds.Tables["documento"].Rows.Count;
                row["Articulo"] = "S00002-000";
                row["Descripcion"] = "Flete";
                row["Cantidad"] = 1;
                row["Bodega"] = ds.Tables["documento"].Rows[0]["Bodega"];
                row["Localizacion"] = ds.Tables["documento"].Rows[0]["Localizacion"];
                row["Tooltip"] = "Esta línea es el flete";
                row["Existencias"] = 1;
                row["Tipo"] = "N";
                row["Cliente"] = ds.Tables["documento"].Rows[0]["Cliente"];
                row["NombreCliente"] = ds.Tables["documento"].Rows[0]["NombreCliente"];
                row["Direccion"] = ds.Tables["documento"].Rows[0]["Direccion"];
                row["Telefonos"] = ds.Tables["documento"].Rows[0]["Telefonos"];
                row["FechaEntrega"] = ds.Tables["documento"].Rows[0]["FechaEntrega"];
                row["PrecioUnitario"] = mFlete;
                row["PrecioTotal"] = mFlete;
                row["Serie"] = ds.Tables["documento"].Rows[0]["Serie"];
                row["PrecioBase"] = mFlete / Convert.ToDecimal(ds.Tables["documento"].Rows[0]["Iva"]);
                row["Iva"] = ds.Tables["documento"].Rows[0]["Iva"];
                ds.Tables["documento"].Rows.Add(row);

                gridPedidoDet.Visible = true;
                gridPedidoDet.DataSource = ds;
                gridPedidoDet.DataMember = "documento";
                gridPedidoDet.DataBind();
                gridPedidoDet.FocusedRowIndex = -1;
                gridPedidoDet.SettingsPager.PageSize = 100;

                Session["dsArticulosPedido"] = ds;
                txtFlete.Text = "";
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorFacturar.Text = string.Format("Error al agregar el flete. {0} {1}", ex.Message, m);
            }
        }

        protected void btnSeguro_Click(object sender, EventArgs e)
        {
            try
            {
                lbErrorFacturar.Text = "";
                lbInfoFacturar.Text = "";

                if (txtSeguro.Text.Trim().Length == 0) return;

                decimal mSeguro = 0;

                try
                {
                    mSeguro = Convert.ToDecimal(txtSeguro.Text);
                }
                catch
                {
                    lbErrorFacturar.Text = "El seguro ingresado es inválido.";
                    return;
                }

                if (mSeguro <= 0)
                {
                    lbErrorFacturar.Text = "El seguro ingresado es inválido.";
                    return;
                }

                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsArticulosPedido"];

                DataRow row = ds.Tables["documento"].NewRow();
                row["Pedido"] = ds.Tables["documento"].Rows[0]["Pedido"];
                row["Linea"] = ds.Tables["documento"].Rows.Count;
                row["Articulo"] = "S00002-000";
                row["Descripcion"] = "Seguro";
                row["Cantidad"] = 1;
                row["Bodega"] = ds.Tables["documento"].Rows[0]["Bodega"];
                row["Localizacion"] = ds.Tables["documento"].Rows[0]["Localizacion"];
                row["Tooltip"] = "Esta línea es el seguro";
                row["Existencias"] = 1;
                row["Tipo"] = "N";
                row["Cliente"] = ds.Tables["documento"].Rows[0]["Cliente"];
                row["NombreCliente"] = ds.Tables["documento"].Rows[0]["NombreCliente"];
                row["Direccion"] = ds.Tables["documento"].Rows[0]["Direccion"];
                row["Telefonos"] = ds.Tables["documento"].Rows[0]["Telefonos"];
                row["FechaEntrega"] = ds.Tables["documento"].Rows[0]["FechaEntrega"];
                row["PrecioUnitario"] = mSeguro;
                row["PrecioTotal"] = mSeguro;
                row["Serie"] = ds.Tables["documento"].Rows[0]["Serie"];
                row["PrecioBase"] = mSeguro / Convert.ToDecimal(ds.Tables["documento"].Rows[0]["Iva"]);
                row["Iva"] = ds.Tables["documento"].Rows[0]["Iva"];
                ds.Tables["documento"].Rows.Add(row);

                gridPedidoDet.Visible = true;
                gridPedidoDet.DataSource = ds;
                gridPedidoDet.DataMember = "documento";
                gridPedidoDet.DataBind();
                gridPedidoDet.FocusedRowIndex = -1;
                gridPedidoDet.SettingsPager.PageSize = 100;

                Session["dsArticulosPedido"] = ds;
                txtFlete.Text = "";
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorFacturar.Text = string.Format("Error al agregar el seguro. {0} {1}", ex.Message, m);
            }
        }

        protected void cbSerie_SelectedIndexChanged(object sender, EventArgs e)
        {
            NumeroFactura();
        }

        void NumeroFactura()
        {
            try
            {
                txtNumeroFactura.Text = "";

                lbInfoFacturar.Text = "";
                lbErrorFacturar.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                string mTienda = "F01";
                if (mTienda.Length > 4) mTienda = "F01";

                //string mTipo = "CO";
                //if (Convert.ToString(cbSerie.Value) == "C") mTipo = "CR";
                //if (Convert.ToString(cbSerie.Value) == "F") mTipo = "F01A";
                string mFactura = ws.NumeroFactura(mTienda, Convert.ToString(cbSerie.Value));

                txtNumeroFactura.Text = mFactura;

                WebRequest request = WebRequest.Create(string.Format("{0}/felactivado/", Convert.ToString(Session["UrlRestServices"])));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string mFelActivado = Convert.ToString(responseString).Replace("\"", "");
                if (mFelActivado == "S")
                {
                    txtNumeroFactura.Text = "Se generará Factura Electrónica.";
                }
                else
                {
                    if (mFactura == "No disponible")
                    {
                        lbErrorFacturar.Text = "No hay facturas disponibles.";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lbErrorFacturar.Text = CatchClass.ExMessage(ex, "administracion", "cbSerie_SelectedIndexChanged");
            }
        }

        protected void btnImprimirFactura_Click(object sender, EventArgs e)
        {
            ReimprimirFactura();
        }

        protected void lkReimprimirFactura_Click(object sender, EventArgs e)
        {
            ReimprimirFactura();
        }

        void ReimprimirFactura()
        {
            try
            {
                if (txtFacturaImprimir.Text.Trim().Length == 0) return;

                lbErrorFacturar.Text = "";

                WebRequest request2 = WebRequest.Create(string.Format("{0}/felactivado/", Convert.ToString(Session["UrlRestServices"])));

                request2.Method = "GET";
                request2.ContentType = "application/json";

                var response2 = (HttpWebResponse)request2.GetResponse();
                var responseString2 = new StreamReader(response2.GetResponseStream()).ReadToEnd();

                string mFelActivado = Convert.ToString(responseString2).Replace("\"", "");
                if (mFelActivado == "S" && txtFacturaImprimir.Text.Trim().Length > 15)
                {
                    string mReportServer = WebConfigurationManager.AppSettings["ReportServer"].ToString();
                    if (Convert.ToString(Session["Ambiente"]) == "DES" || Convert.ToString(Session["Ambiente"]) == "PRU") mReportServer = WebConfigurationManager.AppSettings["ReportServerPruebas"].ToString();
                    Response.Redirect(string.Format("{0}/Report/FacturaMayoreo_FEL/{1}", mReportServer, txtFacturaImprimir.Text.ToUpper().Trim()), false);
                }
                else
                {
                    var ws = new wsPuntoVenta.wsPuntoVenta();
                    if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                    wsPuntoVenta.Factura[] q = new wsPuntoVenta.Factura[1];
                    wsPuntoVenta.FacturaLinea[] qLinea = new wsPuntoVenta.FacturaLinea[1];

                    string mMensaje = "";
                    if (!ws.DevuelveImpresionFactura(txtFacturaImprimir.Text.ToUpper().Trim(), ref mMensaje, ref q, ref qLinea))
                    {
                        lbErrorFacturar.Text = mMensaje;
                        return;
                    }

                    dsPuntoVenta ds = new dsPuntoVenta();

                    for (int jj = 1; jj <= 3; jj++)
                    {
                        DataRow row = ds.Factura.NewRow();
                        row["NoFactura"] = q[0].NoFactura;
                        row["Fecha"] = q[0].Fecha;
                        row["Cliente"] = q[0].Cliente;
                        row["Nombre"] = q[0].Nombre;
                        row["Nit"] = q[0].Nit;
                        row["Direccion"] = q[0].Direccion;
                        row["Vendedor"] = q[0].Vendedor;
                        row["Monto"] = q[0].Monto;
                        row["MontoLetras"] = q[0].MontoLetras;
                        row["Observaciones"] = q[0].Observaciones;
                        row["Numero"] = jj;
                        row["Moneda"] = q[0].Moneda;
                        ds.Factura.Rows.Add(row);

                        for (int ii = 0; ii < qLinea.Count(); ii++)
                        {
                            DataRow rowDet = ds.FacturaLinea.NewRow();
                            rowDet["NoFactura"] = qLinea[ii].NoFactura;
                            rowDet["Articulo"] = qLinea[ii].Articulo;
                            rowDet["Cantidad"] = qLinea[ii].Cantidad;
                            rowDet["Descripcion"] = qLinea[ii].Descripcion;
                            rowDet["PrecioUnitario"] = qLinea[ii].PrecioUnitario;
                            rowDet["PrecioTotal"] = qLinea[ii].PrecioTotal;
                            rowDet["Numero"] = jj;
                            ds.FacturaLinea.Rows.Add(rowDet);
                        }
                    }

                    WebRequest request = WebRequest.Create(string.Format("{0}/formatomayoreo/{1}", Convert.ToString(Session["UrlRestServices"]), txtFacturaImprimir.Text.ToUpper().Trim()));

                    request.Method = "GET";
                    request.ContentType = "application/json";

                    var response = (HttpWebResponse)request.GetResponse();
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    string mFormatoFactura = Convert.ToString(responseString).Replace("\"", "");
                    if (mFormatoFactura.ToLower().Contains("error"))
                    {
                        lbErrorFacturar.Text = mFormatoFactura;
                        return;
                    }

                    using (ReportDocument reporte = new ReportDocument())
                    {
                        string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mFormatoFactura);
                        reporte.Load(p);

                        string mNombreDocumento = string.Format("{0}.pdf", txtFacturaImprimir.Text.ToUpper().Trim());
                        if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                        if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                        reporte.SetDataSource(ds);
                        reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                        Response.Clear();
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                        Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                        Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                lbErrorFacturar.Text = CatchClass.ExMessage(ex, "administracion", "btnImprimirFactura_Click");
            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            var x = e;
        }

        protected void btnFacturasAnular_Click(object sender, EventArgs e)
        {
            CargarFacturasAnular(tiendaAnulacion.Value.ToString());
        }

        protected void btnFacturaAnular_Click(object sender, EventArgs e)
        {
            CargarFacturasAnular(txtFacturaAnular.Text.Trim().ToUpper());
        }

        void CargarFacturasAnular(string valor)
        {
            try
            {
                lbErrorAnulacionFacturas.Text = "";
                lbInfoAnulacionFacturas.Text = "";

                WebRequest request = WebRequest.Create(string.Format("{0}/anulacionfacturas/{1}", Convert.ToString(Session["UrlRestServices"]), valor));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                Clases.FacturasAnular mFacturas = new Clases.FacturasAnular();
                mFacturas = JsonConvert.DeserializeObject<Clases.FacturasAnular>(responseString);

                if (mFacturas.Anular.Count() == 0)
                {
                    lbErrorAnulacionFacturas.Text = "No se encontraron facturas o bien la factura ingresada no cumple las condiciones para ser anulada";
                    gridFacturasAnular.Visible = true;
                    return;
                }

                gridFacturasAnular.Visible = true;
                gridFacturasAnular.DataSource = mFacturas.Anular.AsEnumerable();
                gridFacturasAnular.DataMember = "Anular";
                gridFacturasAnular.DataBind();
                gridFacturasAnular.FocusedRowIndex = -1;
                gridFacturasAnular.SettingsPager.PageSize = 500;
                if (mFacturas.CC.Count > 0)
                    gridFacturasAnular.DetailRows.ExpandRow(0);

                Session["FacturasAnular"] = mFacturas;
            }
            catch (Exception ex)
            {
                lbErrorAnulacionFacturas.Text = CatchClass.ExMessage(ex, "administracion", "CargarFacturasAnular");
            }
        }

        protected void gridFacturasAnular_FocusedRowChanged(object sender, EventArgs e)
        {
            //if (!IsPostBack) return;
            //if (gridFacturasAnular.FocusedRowIndex == -1) return;
            //if (Convert.ToString(gridFacturasAnular.GetRowValues(gridFacturasAnular.FocusedRowIndex, "Factura")).Trim().Length == 0) return;

            //string mFactura = Convert.ToString(gridFacturasAnular.GetRowValues(gridFacturasAnular.FocusedRowIndex, "Factura"));
            //txtObservacionesAnulacion.Text = mFactura;
        }



        protected void gridFacturasAnularDet_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
        {
            var x = e.CommandSource;
            try
            {
                lbErrorAnulacionFacturas.Text = "";
                lbInfoAnulacionFacturas.Text = "";


                string mDespacho = e.KeyValue.ToString();

                Clases.FacturaAnular mAnular = new Clases.FacturaAnular();
                mAnular.Factura = "";
                mAnular.Usuario = Convert.ToString(Session["Usuario"]);
                mAnular.Observaciones = txtObservacionesAnulacion.Text;
                mAnular.SoloDespachos = true;
                mAnular.Despachos = mDespacho;
                string json = JsonConvert.SerializeObject(mAnular);
                byte[] data = Encoding.ASCII.GetBytes(json);

                WebRequest request = WebRequest.Create(string.Format("{0}/anulacionfacturas/", Convert.ToString(Session["UrlRestServices"])));

                request.Timeout = 999999999;
                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string mMensaje = responseString.ToString().Replace("\"", "");

                if (mMensaje.ToLower().Contains("error"))
                {
                    lbErrorAnulacionFacturas.Text = mMensaje;
                    return;
                }

                var q = ((Clases.FacturasAnular)Session["FacturasAnular"]);
                q.CC.RemoveAt(e.VisibleIndex);
                Session["FacturasAnular"] = q;
                ((DevExpress.Web.ASPxGridView)(sender)).DataSource = q.CC;
                ((DevExpress.Web.ASPxGridView)(sender)).DataBind();
                if (q.CC.Count == 0)
                    gridFacturasAnular.DetailRows.CollapseRow(0);

                lbInfoAnulacionFacturas.Text = mMensaje;
            }
            catch (Exception ex)
            {
                lbErrorAnulacionFacturas.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gridFacturasAnular_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
        {
            try
            {
                lbErrorAnulacionFacturas.Text = "";
                lbInfoAnulacionFacturas.Text = "";

                bool mSoloDespachos = false;
                string mFactura = e.KeyValue.ToString();

                ASPxButton btn = (ASPxButton)e.CommandSource;
                if (btn.ID == "btnAnularDespacho")
                    mSoloDespachos = true;

                Clases.FacturaAnular mAnular = new Clases.FacturaAnular();
                mAnular.Factura = mFactura;
                mAnular.Usuario = Convert.ToString(Session["Usuario"]);
                mAnular.Observaciones = txtObservacionesAnulacion.Text;
                mAnular.SoloDespachos = mSoloDespachos;

                string json = JsonConvert.SerializeObject(mAnular);
                byte[] data = Encoding.ASCII.GetBytes(json);

                WebRequest request = WebRequest.Create(string.Format("{0}/anulacionfacturas/", Convert.ToString(Session["UrlRestServices"])));

                request.Timeout = 999999999;
                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string mMensaje = responseString.ToString().Replace("\"", "");

                if (mMensaje.ToLower().Contains("error"))
                {
                    lbErrorAnulacionFacturas.Text = mMensaje;
                    return;
                }

                //CargarFacturasAnular();
                lbInfoAnulacionFacturas.Text = mMensaje;
            }
            catch (Exception ex)
            {
                lbErrorAnulacionFacturas.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void gridFacturasAnularDet_Load(object sender, EventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            var mFactura = grid.GetMasterRowKeyValue();

            Clases.FacturasAnular mFacturas = new Clases.FacturasAnular();
            mFacturas = (Clases.FacturasAnular)Session["FacturasAnular"];

            string factura = (from f in mFacturas.Anular where f.Factura == mFactura.ToString() select f).First().Factura;
            var q = (from c in mFacturas.CC where c.Factura == factura orderby c.Fecha_Despacho select c).ToList();

            grid.DataSource = q;
            grid.DataBind();
        }

        protected void gridFacturasAnularDet_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            //if (Convert.ToString(e.GetValue("Descripcion")).Contains("SALDO FINAL:") || Convert.ToString(e.GetValue("Descripcion")).Contains("SALDO INICIAL:")) e.Row.Font.Bold = true;

            //if (Convert.ToDecimal(e.GetValue("Saldo")) < 0 && (Convert.ToString(e.GetValue("Descripcion")).Contains("SALDO FINAL:") || Convert.ToString(e.GetValue("Descripcion")).Contains("SALDO INICIAL:")))
            //    e.Row.ForeColor = System.Drawing.Color.Red;
        }

        protected void btnCambiarVendedorTienda_Click(object sender, EventArgs e)
        {
            if (cmbTiendaNueva.SelectedIndex > 0)
            {
                try
                {

                    var client = new RestClient(string.Format("{0}/Configuracion/", Convert.ToString(Session["UrlRestServices"])));
                    var request = new RestRequest("CambiarTiendaVendedor/", Method.POST);
                    request.AddParameter("vendedor", Cripto.Encrypt(cmbVendedorTienda.SelectedItem.Value.ToString(), true), RestSharp.ParameterType.QueryString);
                    request.AddParameter("nuevaTienda", Cripto.Encrypt(cmbTiendaNueva.SelectedItem.Value.ToString(), true), RestSharp.ParameterType.QueryString);

                    ////----------------------------------------------------------------------
                    ////Respuesta de parte del servicio devolviendo un objeto UsuarioPDVDto e
                    ////en el atributo response.Data
                    log.Info(string.Format("tienda: {0}", cmbTiendaNueva.SelectedItem.Value.ToString()));

                    IRestResponse<bool> response = client.Execute<bool>(request);
                    ////----------------------------------------------------------------------

                    ////----------------------------------------------------------------------
                    ////Validar si la respuesta de autenticación es válida o no.
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        lblErrorCTV.Text = response.Content.Replace("{\"Message\":\"}", "").Replace("}", "");
                        return;
                    }
                    else
                    {
                        lblErrorCTV.ForeColor = System.Drawing.Color.Black;
                        lblErrorCTV.Text = "¡Cambio realizado!";
                    }

                }
                catch (Exception ex)
                {
                    lblErrorCTV.Text = "Problema al cambiar de tienda " + ex.Message;
                }
            }
        }

        protected void cmbTiendaActual_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblErrorCTV.ForeColor = System.Drawing.Color.Red;
            lblErrorCTV.Text = string.Empty;

            if (cmbTiendaActual.SelectedIndex > 0)
                CargaVendedores();
        }

        protected void lnkREimprimirNotaCreditoDeb_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtFacturaImprimir.Text.Trim().Length > 15)
                {
                    string mReportServer = WebConfigurationManager.AppSettings["ReportServer"].ToString();
                    if (Convert.ToString(Session["Ambiente"]) == "DES" || Convert.ToString(Session["Ambiente"]) == "PRU") mReportServer = WebConfigurationManager.AppSettings["ReportServerPruebas"].ToString();
                    Response.Redirect(string.Format("{0}/Report/FacturaMF_FEL/{1}", mReportServer, txtFacturaImprimir.Text.Trim().ToUpper()), false);
                }
            }
            catch
            {
            }
        }

        protected void gridPedidoDet_ClientLayout(object sender, ASPxClientLayoutArgs e)
        {

        }

        protected void btnAplicar_Click(object sender, EventArgs e)
        {
            try
            {
                lbErrorComisiones.Text = "";
                lbInfoComisiones.Text = "";

                Dictionary<string, string> header = new Dictionary<string, string>();
                header.Add("AppKey", General.AppkeyAutenticacion.ToString());
                header.Add("Token", HttpContext.Current.Request.Cookies.Get(General.CookieName)?.Value);

                Api api = new Api(General.NetunimService);
                string endpoint = General.EndpointValidarSincronizarComision.Replace("{0}", anioComision.Value.ToString()).Replace("{1}", mesComision.Value.ToString());
                IRestResponse response = api.Process(Method.PUT, endpoint, null, header);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    if (JsonConvert.DeserializeObject<bool>(response.Content))
                    {
                        lbInfoComisiones.Text = "Proceso de actualización de comisiones a nómina finalizado correctamente.";
                        btnResumen_Click(sender, e);
                    }
                    else
                        lbErrorComisiones.Text = "Ocurrió un error desconocido, por favor comuníquese con el departament de informatica.";
                }
                else
                {
                    lbErrorComisiones.Text = "Error: " + response.Content;
                }

                VerifySendResume();
                VerifyDeleteCommission();
            }
            catch (Exception ex)
            {
                string error = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                lbErrorComisiones.Text = error.Replace("BadRequest", "").Replace("Message", "").Replace(":", "").Replace("{", "").Replace("}", "").Replace("\"", "");
            }
        }

        protected void btnGenerar_Click(object sender, EventArgs e)
        {
            CargarComisiones("F", Const.Comisiones.FRIEDMAN);
        }

        protected void btnGrabar_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoComisiones.Text = "";
                lbErrorComisiones.Text = "";

                DateTime mFechaInicial = Convert.ToDateTime(fechaInicialComisiones.Value).Date;
                DateTime mFechaFinal = Convert.ToDateTime(fechaFinalComisiones.Value).Date;

                Clases.RetornaComisiones mComisiones = new Clases.RetornaComisiones();
                mComisiones = (Clases.RetornaComisiones)Session["Comisiones"];

                string json = JsonConvert.SerializeObject(mComisiones);
                byte[] data = Encoding.ASCII.GetBytes(json);

                WebRequest request = WebRequest.Create(string.Format("{0}/grabarcomisiones/", Convert.ToString(Session["UrlRestServices"])));

                request.Timeout = 999999999;
                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                Retorna mRetorna = new Retorna();
                mRetorna = JsonConvert.DeserializeObject<Retorna>(responseString);

                if (!mRetorna.exito)
                {
                    lbErrorComisiones.Text = mRetorna.mensaje;
                    return;
                }

                lbInfoComisiones.Text = mRetorna.mensaje;

                Clases.RetornaComisionesJefesSupervisores mRetornaJefesSupervisores = new Clases.RetornaComisionesJefesSupervisores();
                mRetornaJefesSupervisores = (Clases.RetornaComisionesJefesSupervisores)Session["JefesSupervisores"];

                string json2 = JsonConvert.SerializeObject(mRetornaJefesSupervisores);
                byte[] data2 = Encoding.ASCII.GetBytes(json2);

                WebRequest request2 = WebRequest.Create(string.Format("{0}/ComisionesJefesSupervisores/", Convert.ToString(Session["UrlRestServices"])));

                request2.Timeout = 999999999;
                request2.Method = "POST";
                request2.ContentType = "application/json";
                request2.ContentLength = data2.Length;

                using (var stream = request2.GetRequestStream())
                {
                    stream.Write(data2, 0, data2.Length);
                }

                var response2 = (HttpWebResponse)request2.GetResponse();
                var responseString2 = new StreamReader(response2.GetResponseStream()).ReadToEnd();

                lbInfoComisiones.Text = responseString2;
                if (responseString2.Contains("error")) lbErrorComisiones.Text = responseString2;
                btnAplicar.Visible = true;
                VerifySendResume();
                VerifyDeleteCommission();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorComisiones.Text = string.Format("Error al grabar las comisiones. {0} {1}", ex.Message, m);
            }
        }

        protected void btnBorrarComision_Click(object sender, EventArgs e)
        {
            try
            {
                lbErrorComisiones.Text = "";
                lbInfoComisiones.Text = "";

                Api api = new Api(General.FiestaNetRestService);
                bool result = JsonConvert
                    .DeserializeObject<bool>(api.Process(Method.DELETE, $"/Comisiones/Eliminar?mes={mesComision.Value}&anio={anioComision.Value}", null));

                if (result)
                    lbInfoComisiones.Text = "Se eliminaron correctamente los registros de comisión para el mes seleccionado.";
                else
                    lbErrorComisiones.Text = "Ocurrió un error desconocido, por favor comuníquese con el departament de informatica.";

                DateChangeHandler();
            }
            catch (Exception ex)
            {
                string error = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                lbErrorComisiones.Text = error.Replace("BadRequest", "").Replace("Message", "").Replace(":", "").Replace("{", "").Replace("}", "").Replace("\"", "");
            }
        }

        protected void btnResumen_Click(object sender, EventArgs e)
        {
            try
            {
                lbErrorComisiones.Text = "";
                lbInfoComisiones.Text = "";
                DateTime fechaInicial = Convert.ToDateTime($"{anioComision.Value}-{mesComision.Value}-1");
                DateTime fechaFinal = Convert.ToDateTime($"{anioComision.Value}-{mesComision.Value}-{DateTime.DaysInMonth(int.Parse(anioComision.Value.ToString()), int.Parse(mesComision.Value.ToString()))}");

                Api api = new Api(General.FiestaNetRestService);
                api.Process(Method.GET, $"/Email/EnviarResultadoComisiones?pFechaInicio={fechaInicial.ToString("yyyy-MM-dd")}&pFechaFin={fechaFinal.ToString("yyyy-MM-dd")}", null);

                lbInfoComisiones.Text = "Se reenvió el resumen a los correos configurados.";
            }
            catch (Exception ex)
            {
                string error = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                lbErrorComisiones.Text = error.Replace("BadRequest", "").Replace("Message", "").Replace(":", "").Replace("{", "").Replace("}", "").Replace("\"", "");
            }
        }

        protected void mesComision_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateChangeHandler();
        }

        protected void anioComision_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateChangeHandler();

        }

        private void DateChangeHandler()
        {
            Session.Remove("JefesSupervisores");
            Session.Remove("Comisiones");

            btnGenerar.Visible = true;
            btnGrabar.Visible = false;
            btnAplicar.Visible = false;
            btnBorrarComision.Visible = false;
            btnResumen.Visible = false;

            gridComisiones.DataSource = null;
            gridTiendas.DataSource = null;
            gridFacturas.DataSource = null;
            gridExcepciones.DataSource = null;
            gridAnuladas.DataSource = null;
            gridPendientes.DataSource = null;
            gridJefesSupervisores.DataSource = null;
            gridDetalleComisionJefe.DataSource = null;
            gridResumenComisionJefe.DataSource = null;
            gridDetalleComisionSupervisor.DataSource = null;
            gridResumenComisionSupervisor.DataSource = null;
            gridDesgloceJefeSupervisor.DataSource = null;

            gridComisiones.Visible = false;
            gridTiendas.Visible = false;
            gridFacturas.Visible = false;
            gridExcepciones.Visible = false;
            gridAnuladas.Visible = false;
            gridPendientes.Visible = false;
            gridJefesSupervisores.Visible = false;
            gridDetalleComisionJefe.Visible = false;
            gridResumenComisionJefe.Visible = false;
            gridDetalleComisionSupervisor.Visible = false;
            gridResumenComisionSupervisor.Visible = false;
            gridDesgloceJefeSupervisor.Visible = false;

            VerifyDeleteCommission();
            VerifySendResume();
        }

        protected void gridDetalleComisionJefe_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void gridDetalleComisionSupervisor_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void gridDesgloceJefeSupervisor_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void gridResumenComisionJefe_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void gridResumenComisionSupervisor_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void RenderBrick(object sender, ASPxGridViewExportRenderingEventArgs e)
        {
            if (e.RowType != GridViewRowType.Header) e.BrickStyle.BorderWidth = 0;
        }
    }
}