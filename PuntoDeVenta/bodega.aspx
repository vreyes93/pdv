﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="bodega.aspx.cs" Inherits="PuntoDeVenta.bodega" MaintainScrollPositionOnPostback="true" %>
<%@ Register assembly="DevExpress.Web.v16.2, Version=16.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<%@ Register Src="ToolbarExport.ascx" TagName="ToolbarExport" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v16.2" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #middle {
            vertical-align: middle;
        }
        a:-webkit-any-link{
            text-decoration: none;
        }        
        .auto-style1 {
            height: 27px;
        }
    </style>

    <script type="text/javascript">
        function OnBatchEditEndEditing(s, e) {
            setTimeout(function () {
                s.UpdateEdit();
            }, 0);
        }
        function HidePopup() {
            popupArticulos.Hide();
        }
        function onDescriptionClick(s, e, key) {
            txtLinea.SetText(key);
            popupArticulos.Show();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h3>&nbsp;&nbsp;Opciones de Bodega</h3>
    <h3>&nbsp;&nbsp;Opciones de Bodega</h3>
    <div class="content2">
        <div class="clientes2">

            <dx:ASPxPopupControl ID="popupArticulos" runat="server" ClientInstanceName="popupArticulos" HeaderText="Buscar Artículo" HeaderStyle-BackColor="#ff8a3f" HeaderStyle-ForeColor="White"
            Modal="true" Width="500px" AllowDragging="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
            <HeaderStyle BackColor="#FF8A3F" ForeColor="White" />
                <ContentCollection>
                    <dx:PopupControlContentControl>

                        <dx:ASPxTextBox ID="txtObservacionesCambio" runat="server" Width="650px" style="text-transform: uppercase;" CssClass="textBoxStyle" TabIndex="9999" ToolTip="Ingrese las observaciones para enviar en la solicitud de cambio de artículo" >
                        </dx:ASPxTextBox>
                        
                        <dx:ASPxGridView ID="gridArticulosBuscar" EnableTheming="True" Theme="SoftOrange" visible="true"
                            runat="server" KeyFieldName="Linea" AutoGenerateColumns="False" OnHtmlDataCellPrepared="gridArticulosBuscar_HtmlDataCellPrepared" 
                            Border-BorderStyle="None" Width="650px" CssClass="dxGrid" >
                            <Styles>
                                <StatusBar><Border BorderStyle="None" />
                                    <Border BorderStyle="None"></Border>
                                </StatusBar>
                                <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                <RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>
                                <FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                            </Styles>
                            <Settings ShowStatusBar="Hidden"></Settings>
                            <SettingsBehavior EnableRowHotTrack="True" AllowSort="false"></SettingsBehavior>
                            <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                            CommandBatchEditUpdate="Aplicar cambios"
                            ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                            CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                            SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                            <Settings ShowStatusBar="Hidden" />
                            <SettingsSearchPanel Visible="true" ShowApplyButton="true" ShowClearButton="true" />
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="Articulo" ReadOnly="true" Caption="Artículo" Visible="true" Width="100px">
                                    <DataItemTemplate>
                                        <dx:ASPxButton ID="btnArticulo" runat="server" AutoPostBack="false" Text='<%# Bind("Articulo") %>' RenderMode="Link" OnClick="btnArticuloBuscar_Click" ToolTip="Haga clic aquí para seleccionar el artículo y solicitar el cambio"></dx:ASPxButton>
                                    </DataItemTemplate>
                                    <HeaderStyle BackColor="#ff8a3f"  />
                                    <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Descripcion" ReadOnly="true" Caption="Descripción" Visible="true" Width="550px">
                                    <HeaderStyle BackColor="#ff8a3f"  />
                                    <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                    <CellStyle Font-Size="X-Small"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Existencias" ReadOnly="true" Caption="Existencias" Visible="false" Width="20px">
                                    <HeaderStyle BackColor="#ff8a3f"  />
                                    <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                    <CellStyle Font-Size="X-Small"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Tooltip" ReadOnly="true" Caption="Tooltip" Visible="false" Width="20px">
                                    <HeaderStyle BackColor="#ff8a3f"  />
                                    <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                    <CellStyle Font-Size="X-Small"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <Styles>
                                <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                            </Styles>
                            <SettingsBehavior EnableRowHotTrack="true" />
                            <SettingsPager NumericButtonCount="3">
                                <Summary Visible="False" />
                            </SettingsPager>
                            <SettingsText SearchPanelEditorNullText="Ingrese aquí su criterio de búsqueda" CommandBatchEditUpdate="Aplicar cambios" CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"/>
                            <Border BorderStyle="None"></Border>
                        </dx:ASPxGridView>
                    </dx:PopupControlContentControl>
                </ContentCollection>
            </dx:ASPxPopupControl>


            <ul>
                <li>
                    
                    <table >
                        <tr>
                            <td colspan="3">
                                <a>Modificar bodega en envíos</a></td>
                            <td colspan="3" style="text-align: right">
                                <asp:Label ID="lbErrorEnvio" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                <asp:Label ID="lbInfoEnvio" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr id="middle">
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                Envío No.:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtEnvio" runat="server" Width="120px" CssClass="textBoxStyle" TabIndex="10" AutoPostBack="false" ToolTip="Ingrese el número de envío a buscar" >
                                    <ClientSideEvents TextChanged="function(s, e) {
	                                    }" KeyUp="function(s, e) {
	                                    s.SetText(s.GetText().toUpperCase());
                                    }" />
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                &nbsp;
                                <asp:LinkButton ID="lkConsultarEnvio" runat="server" Text="Buscar" 
                                    ToolTip="Haga clic aquí para buscar los artículos del envío ingresado" 
                                    onclick="lkConsultarEnvio_Click" TabIndex="20"></asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;
                                <asp:LinkButton ID="lkGrabarEnvio" runat="server" Text="Grabar" 
                                    ToolTip="Haga clic aquí para grabar las modificaciones en el envío" 
                                    onclick="lkGrabarEnvio_Click" TabIndex="30"></asp:LinkButton>
                                &nbsp;
                                <asp:LinkButton ID="lkImprimirAjuste" runat="server" Text="Grabar" 
                                    ToolTip="Haga clic aquí para imprimir el ajuste de inventario" 
                                    onclick="lkImprimirAjuste_Click" TabIndex="40"></asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridEnvio" EnableTheming="True" Theme="SoftOrange" 
                                    runat="server" KeyFieldName="Linea" AutoGenerateColumns="False" 
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" 
                                    onbatchupdate="gridEnvio_BatchUpdate" 
                                    onrowupdating="gridEnvio_RowUpdating" 
                                    oncommandbuttoninitialize="gridEnvio_CommandButtonInitialize" 
                                    oncelleditorinitialize="gridEnvio_CellEditorInitialize" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                    </Styles>
                                        <SettingsBehavior EnableRowHotTrack="True"></SettingsBehavior>
                                        <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                        CommandBatchEditUpdate="Aplicar cambios" 
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Envio" ReadOnly="true" Caption="Envio" Visible="false" Width="0px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Linea" ReadOnly="true" Caption="Linea" 
                                            Visible="false" Width="0px" VisibleIndex="0">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Articulo" ReadOnly="true" Caption="Artículo" 
                                            Visible="true" Width="75px" VisibleIndex="7">
                                            <HeaderStyle BackColor="#ff8a3f" Font-Bold="true" Border-BorderStyle="None" >
<Border BorderStyle="None"></Border>
                                            </HeaderStyle>
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Descripcion" ReadOnly="true" 
                                            Caption="Descripción" Width="420px" VisibleIndex="8" >
                                            <HeaderStyle BackColor="#ff8a3f" Font-Bold="true" >
                                                <Border BorderStyle="None"></Border>
                                            </HeaderStyle>
                                            <CellStyle Font-Size="Small">
                                            </CellStyle>
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="BodegaOriginal" ReadOnly="true" 
                                            Caption="Bodega Original" Visible="true" Width="20px" VisibleIndex="9" >
                                            <HeaderStyle BackColor="#ff8a3f" Font-Bold="true" Border-BorderStyle="None" 
                                                HorizontalAlign="Center" >
                                                <Border BorderStyle="None"></Border>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="BodegaNueva" ReadOnly="false" 
                                            Caption="Bodega Nueva" Visible="true" Width="20px" VisibleIndex="10" >
                                            <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="40" DropDownRows="25" />
                                            <HeaderStyle BackColor="#ff8a3f" Font-Bold="true" Border-BorderStyle="None" 
                                                HorizontalAlign="Center" >
                                                <Border BorderStyle="None"></Border>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataColumn FieldName="CostoLocal" ReadOnly="true" 
                                            Caption="CostoLocal" Visible="false" Width="0px" VisibleIndex="1">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="CostoDolar" ReadOnly="true" 
                                            Caption="CostoDolar" Visible="false" Width="0px" VisibleIndex="2">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura" 
                                            Visible="false" Width="0px" VisibleIndex="3">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Cliente" ReadOnly="true" Caption="Cliente" 
                                            Visible="false" Width="0px" VisibleIndex="4">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreCliente" ReadOnly="true" 
                                            Caption="NombreCliente" Visible="false" Width="0px" VisibleIndex="5">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Localizacion" ReadOnly="true" 
                                            Caption="Localizacion" Visible="false" Width="0px" VisibleIndex="6">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" />
                                        <SettingsPager PageSize="20" Visible="False">
                                        </SettingsPager>
                                    <SettingsEditing Mode="Batch" />
                                    <ClientSideEvents BatchEditEndEditing="OnBatchEditEndEditing" />
                                    <SettingsText SearchPanelEditorNullText="Ingrese aquí su criterio de búsqueda" CommandBatchEditUpdate="Aplicar cambios"
                                     CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" CommandApplySearchPanelFilter="Buscar" 
                                     CommandClearSearchPanelFilter="Limpiar" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"/>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        </table>
                </li>
            </ul>

            <ul runat="server" id="ulFacturar" visible="false">
                <li>
                    <table>
                        <tr>
                            <td colspan="3">
                                <a>Facturar pedidos</a>
                            </td>
                            <td colspan="3" style="text-align: right">
                                <asp:Label ID="lbErrorFacturar" runat="server" Font-Bold="True" ForeColor="Red" ></asp:Label>
                                <asp:Label ID="lbInfoFacturar" runat="server" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>
                                Rango de fechas de entrega:
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaInicialPedidos" runat="server" Theme="SoftOrange"  DisplayFormatString="dd/MM/yyyy"
                                    CssClass="DateEditStyle" Width="140px" TabIndex="52">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaFinalPedidos" runat="server" Theme="SoftOrange" DisplayFormatString="dd/MM/yyyy"
                                    CssClass="DateEditStyle" Width="140px" TabIndex="54">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkSeleccionarFechasPedidos" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar pedidos según la fecha de entrega de los mismos" 
                                    onclick="lkSeleccionarFechasPedidos_Click" TabIndex="56">Por fechas de entrega</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Código o parte del nombre cliente:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtClientePedido" runat="server" Width="140px" style="text-transform: uppercase;"
                                    CssClass="textBoxStyle" TabIndex="58" AutoPostBack="True" 
                                    ToolTip="Ingrese parte del código o parte del nombre para buscar" 
                                    ontextchanged="txtClientePedido_TextChanged" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkSeleccionarClientePedido" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar pedidos según el cliente ingresado" 
                                    onclick="lkSeleccionarClientePedido_Click" TabIndex="60">Seleccionar cliente</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Número de pedido a buscar:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtPedidoFactura" runat="server" Width="140px" 
                                    CssClass="textBoxStyle" TabIndex="62" 
                                    ToolTip="Ingrese el número de factura" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkCargarPedido" runat="server" 
                                    ToolTip="Haga clic aquí para cargar los artículos del pedido" 
                                    onclick="lkCargarPedido_Click" TabIndex="64">Seleccionar pedido</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Código y nombre del cliente:</td>
                            <td colspan="3">
                                <dx:ASPxTextBox ID="txtNombreClientePedido" runat="server" Width="455px" TabIndex="9999" 
                                    ToolTip="Este es el código y el nombre del cliente" ReadOnly="True" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Teléfonos del cliente:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtTelefonosClientePedido" runat="server" Width="140px" 
                                    TabIndex="9999" 
                                    ToolTip="Este es el código y el nombre del cliente" ReadOnly="True" 
                                    Font-Size="X-Small" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <strong>Fecha de entrega:</strong></td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaEntregaPedido" runat="server" ReadOnly="false" Theme="SoftOrange"  DisplayFormatString="dd/MM/yyyy"
                                    Width="140px" TabIndex="66" Font-Bold="True">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Dirección del cliente:</td>
                            <td colspan="3">
                                <dx:ASPxTextBox ID="txtDireccionPedido" runat="server" Width="455px" 
                                    CssClass="textBoxStyle" TabIndex="9999" 
                                    ToolTip="Esta es la dirección que saldrá impresa en el despacho" 
                                    ReadOnly="True" Font-Size="X-Small" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridPedidoDet" EnableTheming="True" Theme="SoftOrange" visible="False"
                                    runat="server" KeyFieldName="Linea" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" 
                                    onbatchupdate="gridPedidoDet_BatchUpdate" 
                                    onrowupdating="gridPedidoDet_RowUpdating" 
                                    oncommandbuttoninitialize="gridPedidoDet_CommandButtonInitialize" 
                                    oncelleditorinitialize="gridPedidoDet_CellEditorInitialize" 
                                    onhtmldatacellprepared="gridPedidoDet_HtmlDataCellPrepared" 
                                    onhtmlrowprepared="gridPedidoDet_HtmlRowPrepared" TabIndex="68" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                        <SettingsBehavior EnableRowHotTrack="True" AllowSort="false"></SettingsBehavior>
                                        <SettingsCommandButton>
                                            <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                            <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                        </SettingsCommandButton>
                                        <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Pedido" ReadOnly="true" Caption="Pedido" 
                                            Visible="true" Width="100px" VisibleIndex="8">
                                            <EditFormSettings Visible="false"/>
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Linea" ReadOnly="true" Caption="Línea" Visible="false" Width="25px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Articulo" ReadOnly="true" 
                                            Caption="Artículo" Visible="true" Width="100px" VisibleIndex="9">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Descripcion" ReadOnly="true" Caption="Descripción" Visible="true" Width="350px" VisibleIndex="10">
                                            <EditFormSettings Visible="False" />
                                            <DataItemTemplate>
                                                <dx:ASPxLabel ID="lbDescripcion" EncodeHtml="false" runat="server" AutoPostBack="false" Text='<%# Bind("Descripcion") %>' Font-Size="X-Small">
                                                </dx:ASPxLabel>
                                            </DataItemTemplate>
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                            <CellStyle Font-Size="X-Small" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="Cantidad" ReadOnly="false" Caption="Cantidad" 
                                            Visible="true" Width="50px" VisibleIndex="11">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="Bodega" ReadOnly="false" 
                                            Caption="Bodega" Visible="true" Width="20px" VisibleIndex="12" >
                                            <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="40" DropDownRows="25" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" >
                                                <Border BorderStyle="None"></Border>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="Localizacion" ReadOnly="false" 
                                            Caption="Localización" Visible="true" Width="80px" VisibleIndex="13">
                                            <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="40" DropDownRows="5" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" >
                                                <Border BorderStyle="None"></Border>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataTextColumn FieldName="Tooltip" Visible="false" VisibleIndex="1">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Existencias" Visible="false" 
                                            VisibleIndex="2">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Tipo" Visible="false" VisibleIndex="3">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="NombreCliente" Visible="false" 
                                            VisibleIndex="4">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Telefonos" Visible="false" 
                                            VisibleIndex="5">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Direccion" Visible="false" 
                                            VisibleIndex="6">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="FechaEntrega" Visible="false" 
                                            VisibleIndex="7">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PrecioTotal" Visible="false" 
                                            VisibleIndex="8">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsEditing Mode="Batch" />
                                    <ClientSideEvents BatchEditEndEditing="OnBatchEditEndEditing" />
                                    <SettingsBehavior EnableRowHotTrack="true" />
                                    <SettingsPager PageSize="20" Visible="False">
                                    </SettingsPager>
                                    <SettingsText SearchPanelEditorNullText="Ingrese aquí su criterio de búsqueda" CommandBatchEditUpdate="Aplicar cambios" CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"/>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>                            
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="text-align: center">
                                <dx:ASPxButton ID="btnFacturar" runat="server" Width="8px" Height="8px"
                                    ToolTip="Generar la factura" onclick="btnFacturar_Click" TabIndex="70" >
                                    <Image IconID="save_saveall_16x16office2013">
                                    </Image>
                                </dx:ASPxButton>
                                &nbsp;&nbsp;
                                <dx:ASPxButton ID="btnLimpiarPedido" runat="server" Height="8px" 
                                    onclick="btnLimpiarPedido_Click" TabIndex="72" ToolTip="Limpiar la pantalla de pedidos" Width="8px">
                                    <Image IconID="actions_clear_16x16">
                                    </Image>
                                </dx:ASPxButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridPedidos" EnableTheming="True" Theme="SoftOrange" 
                                    runat="server" KeyFieldName="Pedido" AutoGenerateColumns="False" 
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" 
                                    onfocusedrowchanged="gridPedidos_FocusedRowChanged" TabIndex="74" 
                                    Visible="False" 
                                    oncustombuttoninitialize="gridPedidos_CustomButtonInitialize" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                        <SettingsBehavior EnableRowHotTrack="True"></SettingsBehavior>
                                        <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                        CommandBatchEditUpdate="Aplicar cambios" 
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Pedido" ReadOnly="true" Caption="Pedido" Visible="true" Width="85px">
                                            <DataItemTemplate>
                                                <asp:LinkButton ID="lkPedidoCliente" runat="server" 
                                                    Text='<%# Bind("Pedido") %>' CausesValidation="true" CommandName="Select" 
                                                    ToolTip="Haga clic aquí para seleccionar el pedido" ></asp:LinkButton>
                                            </DataItemTemplate>
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Fecha" ReadOnly="true" Caption="Fecha" Visible="false" >
                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="FechaEntrega" ReadOnly="true" Caption="Fecha Entrega" Visible="true" Width="90px">
                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="Cliente" ReadOnly="true" Caption="Cliente" Visible="true" Width="60px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreCliente" ReadOnly="true" Caption="Nombre Cliente" Visible="true" Width="350px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                            <CellStyle Font-Size="X-Small" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Monto" ReadOnly="true" Caption="Monto" Visible="true" Width="75px" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="100" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        </table>
                </li>
            </ul>

            <ul>
                <li>
                    
                    <table >
                        <tr>
                            <td colspan="3">
                                <a>Despachos</a></td>
                            <td colspan="4" style="text-align: right">
                                <asp:Label ID="lbErrorDespachos" runat="server" Font-Bold="True" ForeColor="Red" ></asp:Label>
                                <asp:Label ID="lbInfoDespachos" runat="server" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                Rango de fechas de entrega:</td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaInicialE" runat="server" Theme="SoftOrange"  DisplayFormatString="dd/MM/yyyy"
                                    CssClass="DateEditStyle" Width="140px" TabIndex="110">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaFinalE" runat="server" Theme="SoftOrange" DisplayFormatString="dd/MM/yyyy"
                                    CssClass="DateEditStyle" Width="140px" TabIndex="120">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td colspan="2">
                                <asp:LinkButton ID="lkSeleccionarFechasE" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar facturas según la fecha de entrega de las mismas" 
                                    onclick="lkSeleccionarFechasE_Click" TabIndex="130">Por fechas de entrega</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxDateEdit ID="fechaInicial" runat="server" Theme="SoftOrange"  DisplayFormatString="dd/MM/yyyy"
                                    CssClass="DateEditStyle" Width="10px" TabIndex="9999" Visible="False">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                                <dx:ASPxDateEdit ID="fechaFinal" runat="server" Theme="SoftOrange"  DisplayFormatString="dd/MM/yyyy"
                                    CssClass="DateEditStyle" Width="10px" TabIndex="9999" Visible="False">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                Código o parte del nombre:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtCliente" runat="server" Width="140px" style="text-transform: uppercase;"
                                    CssClass="textBoxStyle" TabIndex="140" AutoPostBack="True" 
                                    ToolTip="Ingrese parte del código o parte del nombre para buscar" 
                                    ontextchanged="txtCliente_TextChanged" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkSeleccionarCliente" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar documentos según el cliente ingresado" 
                                    onclick="lkSeleccionarCliente_Click" TabIndex="150">Seleccionar cliente</asp:LinkButton>
                            </td>
                            <td colspan="2">
                                <asp:LinkButton ID="lkSeleccionarAprobadas" runat="server" 
                                    ToolTip="Haga clic aquí para ver las facturas aprobadas que aún no se han despachado" 
                                    onclick="lkSeleccionarAprobadas_Click" TabIndex="999" Visible="False">Ver aprobadas</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Tienda que desea consultar:</td>
                            <td>
                                <dx:ASPxComboBox ID="tienda" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle" 
                                    ValueType="System.String" Width="75px" DropDownStyle="DropDownList" 
                                    DropDownWidth="40" DropDownRows="25" TabIndex="160">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkSeleccionarTienda" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar documentos según la tienda seleccionada" 
                                    onclick="lkSeleccionarTienda_Click" TabIndex="170">Seleccionar tienda</asp:LinkButton>
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtPedido" runat="server" Visible="False" Width="20px" TabIndex="9999"></asp:TextBox>
                                <asp:TextBox ID="txtTienda" runat="server" Visible="False" Width="20px" TabIndex="9999"></asp:TextBox>
                                <dx:ASPxTextBox ID="txtLinea" runat="server" ClientInstanceName="txtLinea" CssClass="textBoxStyleHide" Width="40px" TabIndex="9999" Visible="true" ReadOnly="true" ForeColor="White">
                                    <Border BorderColor="White" />
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Número de factura a buscar:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtFactura" runat="server" Width="140px" 
                                    CssClass="textBoxStyle" TabIndex="180" 
                                    ToolTip="Ingrese el número de factura" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkCargarFactura" runat="server" 
                                    ToolTip="Haga clic aquí para cargar los artículos de la factura" 
                                    onclick="lkCargarFactura_Click" TabIndex="190">Seleccionar factura</asp:LinkButton>
                            </td>
                            <td colspan="2">
                                <dx:ASPxLabel ID="lbPedido" runat="server" TabIndex="9999">
                                </dx:ASPxLabel>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td colspan="2">
                                </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Código y nombre del cliente:</td>
                            <td colspan="4">
                                <dx:ASPxTextBox ID="txtNombreCliente" runat="server" Width="440px" 
                                    TabIndex="200" 
                                    ToolTip="Este es el código y el nombre del cliente" ReadOnly="True" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkExpediente" runat="server" 
                                    ToolTip="Haga clic aquí para ver el expediente de esta factura" 
                                    onclick="lkExpediente_Click" TabIndex="210">Expediente</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Teléfonos del cliente:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtTelefonos" runat="server" Width="140px" 
                                    TabIndex="220" 
                                    ToolTip="Este es el código y el nombre del cliente" ReadOnly="True" 
                                    Font-Size="X-Small" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Fecha de entrega:</strong></td>
                            <td colspan="2">
                                <dx:ASPxDateEdit ID="fechaEntrega" runat="server" ReadOnly="false" Theme="SoftOrange"  DisplayFormatString="dd/MM/yyyy"
                                    Width="140px" TabIndex="230" Font-Bold="True">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkVerFactura" runat="server" 
                                    ToolTip="Haga clic aquí para ver la factura en pantalla" 
                                    onclick="lkVerFactura_Click" TabIndex="240">Factura</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Dirección del cliente:</td>
                            <td colspan="4">
                                <dx:ASPxTextBox ID="txtDireccion" runat="server" Width="440px" 
                                    CssClass="textBoxStyle" TabIndex="250" 
                                    ToolTip="Esta es la dirección que saldrá impresa en el despacho" 
                                    ReadOnly="True" Font-Size="X-Small" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkCliente" runat="server" 
                                    ToolTip="Haga clic aquí para ver los datos del cliente" 
                                    onclick="lkCliente_Click" TabIndex="260">Cliente</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Persona recibirá dónde cliente:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtPersonaRecibe" runat="server" Width="140px" 
                                    CssClass="textBoxStyle" TabIndex="270" 
                                    ToolTip="Este es el nombre de la persona que recibirá en casa de cliente" 
                                    ReadOnly="True" Font-Size="X-Small" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;Nombre del vendedor:</td>
                            <td colspan="2">
                                <dx:ASPxTextBox ID="txtVendedor" runat="server" Width="140px" 
                                    CssClass="textBoxStyle" TabIndex="280" 
                                    ToolTip="Este es el nombre del vendedor" 
                                    ReadOnly="True" Font-Size="X-Small" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkPedidos" runat="server" 
                                    ToolTip="Haga clic aquí para ver los pedidos del cliente" 
                                    onclick="lkPedidos_Click" TabIndex="290">Pedidos</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">
                                </td>
                            <td class="auto-style1">
                                Observaciones del vendedor:</td>
                            <td colspan="4" class="auto-style1">
                                <dx:ASPxTextBox ID="txtObservacionesVendedor" runat="server" Width="440px" 
                                    CssClass="textBoxStyle" TabIndex="300" Font-Size="X-Small" 
                                    ToolTip="Estas son las observaciones que ingresó el vendedor" 
                                    ReadOnly="True" >
                                </dx:ASPxTextBox>
                            </td>
                            <td class="auto-style1">
                                </td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <dx:ASPxGridView ID="gridFacturaDet" EnableTheming="True" Theme="SoftOrange" visible="false"
                                    runat="server" KeyFieldName="Linea" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" 
                                    onbatchupdate="gridFacturaDet_BatchUpdate" 
                                    onrowupdating="gridFacturaDet_RowUpdating" 
                                    oncommandbuttoninitialize="gridFacturaDet_CommandButtonInitialize" 
                                    oncelleditorinitialize="gridFacturaDet_CellEditorInitialize" 
                                    onhtmldatacellprepared="gridFacturaDet_HtmlDataCellPrepared" 
                                    onhtmlrowprepared="gridFacturaDet_HtmlRowPrepared" TabIndex="110" 
                                    onafterperformcallback="gridFacturaDet_AfterPerformCallback" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                        <SettingsBehavior EnableRowHotTrack="True" AllowSort="false"></SettingsBehavior>
                                        <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura" Visible="true" Width="140px">
                                            <EditFormSettings Visible="false"/>
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Linea" ReadOnly="true" Caption="Línea" Visible="false" Width="25px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Articulo" ReadOnly="true" Caption="Artículo" Visible="false" Width="100px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Articulo2" ReadOnly="true" Caption="Artículo" Visible="true" Width="100px">
                                            <DataItemTemplate>
                                                <dx:ASPxButton ID="btnArticulo2" EncodeHtml="false" runat="server" AutoPostBack="false" Text='<%# Bind("Articulo2") %>' RenderMode="Link" OnClick="btnArticulo2_Click" ToolTip="Haga clic aquí para cambiar el color del artículo"></dx:ASPxButton>
                                            </DataItemTemplate>
                                            <EditFormSettings Visible="false"/>
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Descripcion" ReadOnly="true" Caption="Descripción" Visible="true" Width="350px">
                                            <EditFormSettings Visible="False" />
                                            <DataItemTemplate>
                                                <dx:ASPxButton ID="btnDescripcion" EncodeHtml="false" runat="server" AutoPostBack="false" Text='<%# Bind("Descripcion") %>' RenderMode="Link" ToolTip="Haga clic aquí para cambiar el artículo" OnInit="btnDescripcion_Init" Font-Size="X-Small">
                                                </dx:ASPxButton>
                                            </DataItemTemplate>
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                            <CellStyle Font-Size="X-Small" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="Cantidad" ReadOnly="false" Caption="Cantidad" Visible="true" Width="50px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="Bodega" ReadOnly="false" Caption="Bodega" Visible="true" Width="20px" >
                                            <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="40" DropDownRows="25" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" >
                                                <Border BorderStyle="None"></Border>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="Localizacion" ReadOnly="false" Caption="Localización" Visible="true" Width="80px">
                                            <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="40" DropDownRows="5" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" >
                                                <Border BorderStyle="None"></Border>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataTextColumn FieldName="Tooltip" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Existencias" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Tipo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Cliente" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Consecutivo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Despacho" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Cobrador" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="NombreCliente" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Direccion" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="CostoTotal" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="CostoTotalDolar" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="LineaOrigen" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PrecioTotal" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Telefonos" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Pedido" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="NombreRecibe" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Vendedor" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ObservacionesVendedor" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="FechaEntrega" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Observaciones" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsEditing Mode="Batch" />
                                    <ClientSideEvents BatchEditEndEditing="OnBatchEditEndEditing" />
                                    <SettingsBehavior EnableRowHotTrack="true" />
                                    <SettingsPager PageSize="100" Visible="False">
                                    </SettingsPager>
                                    <SettingsText SearchPanelEditorNullText="Ingrese aquí su criterio de búsqueda" CommandBatchEditUpdate="Aplicar cambios" CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"/>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Observaciones del despacho:</td>
                            <td colspan="4">
                                <dx:ASPxTextBox ID="txtObservaciones" runat="server" Width="457px" 
                                    style="text-transform: uppercase;" CssClass="textBoxStyle" TabIndex="320" 
                                    ToolTip="Ingrese las observaciones del envío" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Transportista que lo llevará:</td>
                            <td colspan="2">
                                <dx:ASPxComboBox ID="transportista" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle" 
                                    ValueType="System.String" Width="295px" DropDownStyle="DropDownList" 
                                    DropDownWidth="40" DropDownRows="25" TabIndex="325">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                Artículos válidos:</td>
                            <td>
                                <dx:ASPxSpinEdit ID="articulosValdios" runat="server" Number="0"  
                                    TabIndex="328" MaxValue="100" Width="50px" Height="26px" 
                                    ToolTip="Ingrese aquí el número de artículos válidos que tiene el envío">
                                </dx:ASPxSpinEdit>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="7" style="text-align: center">
                                <dx:ASPxLabel ID="lbAyudaTransportista" runat="server" Text="" 
                                    ForeColor="White" BackColor="#E35904" Height="30px"></dx:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Número de despacho:</td>
                            <td>
                                <dx:ASPxComboBox ID="cbSerie" runat="server" Theme="SoftOrange" 
                                    CssClass="ComboBoxStyle" Width="140px" 
                                    DropDownWidth="40px" DropDownRows="25" TabIndex="330" AutoPostBack="True" 
                                    onselectedindexchanged="cbSerie_SelectedIndexChanged">
                                </dx:ASPxComboBox>                            
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txtDespacho" runat="server" Width="140px" 
                                    CssClass="textBoxStyle" TabIndex="340" 
                                    ToolTip="Este es el número de envío a generar">
                                </dx:ASPxTextBox>
                            </td>
                            <td colspan="3">
                                <asp:LinkButton ID="lkGrabarDespacho" runat="server" 
                                    ToolTip="Haga clic aquí para grabar el despacho" 
                                    onclick="lkGrabarDespacho_Click" TabIndex="350">Grabar despacho</asp:LinkButton>
                                    &nbsp;&nbsp;
                                <dx:ASPxButton ID="btnImprimirDespacho" runat="server" Width="8px" Height="8px"
                                    ToolTip="Imprimir el envío" onclick="btnImprimirDespacho_Click" 
                                    TabIndex="360" >
                                    <Image IconID="print_print_16x16office2013">
                                    </Image>
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="ASPxButton1" runat="server" Height="8px" 
                                    onclick="ASPxButton1_Click" TabIndex="361" ToolTip="Limpiar la pantalla" 
                                    Width="8px">
                                    <Image IconID="actions_clear_16x16">
                                    </Image>
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="btnImprimirDespachosHoy" runat="server" Width="8px" Height="8px"
                                    ToolTip="Imprimir los envíos de hoy" onclick="btnImprimirDespachosHoy_Click" 
                                    TabIndex="362" >
                                    <Image IconID="print_printarea_16x16">
                                    </Image>
                                </dx:ASPxButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <dx:ASPxGridView ID="gridFacturas" EnableTheming="True" Theme="SoftOrange" 
                                    runat="server" KeyFieldName="Factura" AutoGenerateColumns="False" 
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" 
                                    onbatchupdate="gridFacturas_BatchUpdate" 
                                    onrowupdating="gridFacturas_RowUpdating" 
                                    oncommandbuttoninitialize="gridFacturas_CommandButtonInitialize" 
                                    oncelleditorinitialize="gridFacturas_CellEditorInitialize" 
                                    onfocusedrowchanged="gridFacturas_FocusedRowChanged" TabIndex="370" 
                                    Visible="False" onhtmlrowprepared="gridFacturas_HtmlRowPrepared" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                        <SettingsBehavior EnableRowHotTrack="True"></SettingsBehavior>
                                        <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                        CommandBatchEditUpdate="Aplicar cambios" 
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="true" Width="40px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura" Visible="true" Width="150px">
                                            <DataItemTemplate>
                                                <asp:LinkButton ID="lkFacturaCliente" runat="server" 
                                                    Text='<%# Bind("Factura") %>' CausesValidation="true" CommandName="Select" 
                                                    ToolTip="Haga clic aquí para seleccionar la factura" ></asp:LinkButton>
                                            </DataItemTemplate>
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Fecha" ReadOnly="true" Caption="Fecha" Visible="false">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="FechaEntrega" ReadOnly="true" Caption="Fecha Entrega" Visible="true" Width="70px">
                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="Cliente" ReadOnly="true" Caption="Cliente" Visible="true" Width="50px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreCliente" ReadOnly="true" Caption="Nombre Cliente" Visible="true" Width="350px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                            <CellStyle Font-Size="X-Small" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Monto" ReadOnly="true" Caption="Monto" Visible="true" Width="70px" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Aprobada" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="100" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>                                
                            </td>
                        </tr>
                        </table>
                    
                </li>
            </ul>

            <ul>
                <li>
                    
                    <table class="dxflInternalEditorTable">
                        <tr>
                            <td colspan="3">
                                <a>Rutas</a></td>
                            <td colspan="3" style="text-align: right">
                                <asp:Label ID="lbErrorRutas" runat="server" Font-Bold="True" ForeColor="Red" ></asp:Label>
                                <asp:Label ID="lbInfoRutas" runat="server" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                Rango de fechas de entrega:</td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaInicialR" runat="server" Theme="SoftOrange"  DisplayFormatString="dd/MM/yyyy"
                                    CssClass="DateEditStyle" Width="140px" TabIndex="380">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaFinalR" runat="server" Theme="SoftOrange"  DisplayFormatString="dd/MM/yyyy"
                                    CssClass="DateEditStyle" Width="140px" TabIndex="385">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkFechasRutas" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar envíos según la fecha de entrega de los mismos" 
                                    onclick="lkSeleccionarFechasR_Click" TabIndex="390">Por fechas de entrega</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Tienda que desea consultar:</td>
                            <td>
                                <dx:ASPxComboBox ID="tiendaRutas" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle" 
                                    ValueType="System.String" Width="75px" DropDownStyle="DropDownList" 
                                    DropDownWidth="40" DropDownRows="25" TabIndex="395">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkSeleccionarTiendaR" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar documentos según la tienda y fechas seleccionada" 
                                    onclick="lkSeleccionarTiendaR_Click" TabIndex="400">Seleccionar tienda</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Transportista:</td>
                            <td colspan="2">
                                <dx:ASPxComboBox ID="transportistaRutas" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle" 
                                    ValueType="System.String" Width="295px" DropDownStyle="DropDownList" 
                                    DropDownWidth="40" DropDownRows="25" TabIndex="405">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkTransportistaRutas" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar envíos según el transportista y las fechas seleccionadas" 
                                    onclick="lkTransportistaRutas_Click" TabIndex="410">Por transportista</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Despacho o traspaso:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtDespachoTraspaso" runat="server" Width="110px" style="text-transform: uppercase;"
                                    CssClass="textBoxStyle" TabIndex="412" ToolTip="Ingrese el número de despacho o traspaso a buscar" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkDespachoTraspaso" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar el envío o traspaso seleccionado" 
                                    onclick="lkDespachoTraspaso_Click" TabIndex="414">Despacho o traspaso</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td colspan="2">
                                
                                <dx:ToolbarExport runat="server" ID="ToolbarExport" ExportItemTypes="Pdf,Xls,Xlsx,Csv" OnItemClick="ToolbarExport_ItemClick"  />
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridRutas" 
                                    onrenderbrick="gridExport_RenderBrick"></dx:ASPxGridViewExporter>
                                <dx:ASPxGridView ID="gridRutas" EnableTheming="True" Theme="SoftOrange" visible="false"
                                    runat="server" KeyFieldName="Despacho" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" 
                                    oncommandbuttoninitialize="gridRutas_CommandButtonInitialize" 
                                    oncelleditorinitialize="gridRutas_CellEditorInitialize" 
                                    onhtmldatacellprepared="gridRutas_HtmlDataCellPrepared" 
                                    onhtmlrowprepared="gridRutas_HtmlRowPrepared" TabIndex="420" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                        <GroupRow Font-Bold="true" />
                                    </Styles>
                                        <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                        <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="true" ShowFilterRow="false" ShowFilterRowMenu="false" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Despacho" ReadOnly="true" Caption="Despacho" Visible="true" Width="80px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataDateColumn FieldName="Fecha" ReadOnly="true" Caption="Fecha" Visible="false" Width="65px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaEntrega" ReadOnly="true" Caption="Entrega" Visible="true" Width="65px">
                                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="Transportista" ReadOnly="false" Caption="Transportista" Visible="false" Width="300px" >
                                            <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="300" DropDownRows="25" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" >
                                                <Border BorderStyle="None"></Border>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataColumn FieldName="NombreTransportista" ReadOnly="true" Caption="Transportista" Visible="true" Width="195px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataCheckColumn FieldName="Cliente" ReadOnly="true" Caption="Cliente" Visible="false" Width="60px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataTextColumn FieldName="NombreCliente" ReadOnly="true" Caption="Nombre Cliente" Visible="true" Width="290px" ExportWidth="350">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                            <Settings AutoFilterCondition="Contains" />
                                            <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="true" Width="20px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                    </Columns>
                                    <Templates>
                                        <DetailRow>
                                            <dx:ASPxGridView id="gridRutasDet" runat="server" AutoGenerateColumns="false" Theme="SoftOrange" CssClass="dxGrid" Border-BorderStyle="None" OnLoad="gridRutasDet_Load" Width="760px" >
                                                <Columns>
                                                    <dx:GridViewDataColumn FieldName="Despacho" ReadOnly="true" Caption="Despacho" Visible="false">
                                                        <HeaderStyle BackColor="#ff8a3f"  />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="Linea" ReadOnly="true" Caption="Línea" Visible="false">
                                                        <HeaderStyle BackColor="#ff8a3f"  />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataTextColumn FieldName="Articulo" ReadOnly="true" Caption="Artículo" Visible="false">
                                                        <HeaderStyle BackColor="#ff8a3f"  />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="Articulo2" ReadOnly="true" Caption="Artículo" Visible="true" Width="100px">
                                                        <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                                        <HeaderStyle BackColor="#ff8a3f"  />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="Descripcion" ReadOnly="true" Caption="Descripción" Visible="true" Width="250px">
                                                        <EditFormSettings Visible="False" />
                                                        <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                                        <HeaderStyle BackColor="#ff8a3f"  />
                                                        <CellStyle Font-Size="X-Small" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataColumn FieldName="Cantidad" ReadOnly="false" Caption="Cantidad" Visible="true" Width="30px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataComboBoxColumn FieldName="Bodega" ReadOnly="false" Caption="Bodega" Visible="true" Width="20px" >
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" >
                                                            <Border BorderStyle="None"></Border>
                                                        </HeaderStyle>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataComboBoxColumn>
                                                    <dx:GridViewDataComboBoxColumn FieldName="Localizacion" Visible="false">
                                                    </dx:GridViewDataComboBoxColumn>
                                                    <dx:GridViewDataTextColumn FieldName="Tipo" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="Cliente" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="Cobrador" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="Factura" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="Vendedor" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="EmailVendedor" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="FechaEntrega" Visible="false">
                                                    </dx:GridViewDataTextColumn>                                                    
                                                </Columns>
                                            </dx:ASPxGridView>
                                        </DetailRow>
                                    </Templates>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsDetail ShowDetailRow="true" />
                                    <SettingsBehavior EnableRowHotTrack="true" />
                                    <SettingsPager NumericButtonCount="3">
                                        <Summary Visible="False" />
                                    </SettingsPager>
                                    <SettingsText GroupPanel="Arrastre una columna hacia aquí para agrupar por esa columna" SearchPanelEditorNullText="Ingrese aquí su criterio de búsqueda" CommandBatchEditUpdate="Aplicar cambios" CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"/>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                    
                </li>
            </ul>

            <ul>
                <li>
                    
                    <table >
                        <tr>
                            <td colspan="3">
                                <a>Actualizar transportista en despachos</a></td>
                            <td colspan="3" style="text-align: right">
                                <asp:Label ID="lbErrorTransportista" runat="server" Font-Bold="True" ForeColor="Red" ></asp:Label>
                                <asp:Label ID="lbInfoTransportista" runat="server" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                Rango de fechas de entrega:</td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaInicialT" runat="server" Theme="SoftOrange"  DisplayFormatString="dd/MM/yyyy"
                                    CssClass="DateEditStyle" Width="140px" TabIndex="430">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaFinalT" runat="server" Theme="SoftOrange" DisplayFormatString="dd/MM/yyyy"
                                    CssClass="DateEditStyle" Width="140px" TabIndex="440">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkFechasTransporte" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar envíos según la fecha de entrega de los mismos" 
                                    onclick="lkSeleccionarFechasT_Click" TabIndex="450">Por fechas de entrega</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Tienda que desea consultar:</td>
                            <td>
                                <dx:ASPxComboBox ID="tiendaTransporte" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle" 
                                    ValueType="System.String" Width="75px" DropDownStyle="DropDownList" 
                                    DropDownWidth="40" DropDownRows="25" TabIndex="460">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkSeleccionarTiendaT" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar documentos según la tienda seleccionada" 
                                    onclick="lkSeleccionarTiendaT_Click" TabIndex="470">Seleccionar tienda</asp:LinkButton>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkTraspasos" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar los traspasos según la fecha de entrega de los mismos" 
                                    onclick="lkTraspasos_Click" TabIndex="475">Seleccionar traspasos</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Transportista que lo llevaba:</td>
                            <td colspan="2">
                                <dx:ASPxComboBox ID="transportistaT" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle" 
                                    ValueType="System.String" Width="295px" DropDownStyle="DropDownList" 
                                    DropDownWidth="40" DropDownRows="25" TabIndex="480">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkTransportistaT" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar envíos según el transportista y las fechas seleccionadas" 
                                    onclick="lkTransportistaT_Click" TabIndex="482">Por transportista</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Despacho o traspaso:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtDespachoTraspasoT" runat="server" Width="110px" style="text-transform: uppercase;"
                                    CssClass="textBoxStyle" TabIndex="484" ToolTip="Ingrese el número de despacho o traspaso a buscar" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkDespachoTraspasoT" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar el envío o traspaso seleccionado" 
                                    onclick="lkDespachoTraspasoT_Click" TabIndex="486">Despacho o traspaso</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                </td>
                            <td colspan="2" style="text-align: center">
                                <asp:LinkButton ID="lkActualizarTransportista" runat="server" 
                                    ToolTip="Haga clic aquí para grabar los cambios en los transportistas y aceptación de clientes" 
                                    onclick="lkActualizarTransportista_Click" TabIndex="487">Grabar</asp:LinkButton>
                            </td>
                            <td>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="6">

                                <dx:ASPxGridView ID="gridDespachos" EnableTheming="True" Theme="SoftOrange" visible="false"
                                    runat="server" KeyFieldName="Despacho" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" 
                                    onbatchupdate="gridDespachos_BatchUpdate" 
                                    onrowupdating="gridDespachos_RowUpdating" 
                                    oncommandbuttoninitialize="gridDespachos_CommandButtonInitialize" 
                                    oncelleditorinitialize="gridDespachos_CellEditorInitialize" 
                                    onhtmldatacellprepared="gridDespachos_HtmlDataCellPrepared" 
                                    onhtmlrowprepared="gridDespachos_HtmlRowPrepared" TabIndex="490" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                        <GroupRow Font-Bold="true" />
                                    </Styles>
                                        <SettingsBehavior EnableRowHotTrack="True" AllowSort="false"></SettingsBehavior>
                                        <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Despacho" ReadOnly="true" Caption="Despacho" Visible="true" Width="135px">
                                            <EditFormSettings Visible="false"/>
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataDateColumn FieldName="Fecha" ReadOnly="true" Caption="Fecha" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaEntrega" ReadOnly="false" Caption="Fecha Entrega" Visible="true" Width="80px">
                                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="Transportista" ReadOnly="false" Caption="Transportista" Visible="true" Width="270px" >
                                            <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="300" DropDownRows="25" ValueField="Transportista" ValueType="System.Int32" TextField="Nombre"/>
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Left" >
                                                <Border BorderStyle="None"></Border>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" Font-Size="Small" />
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Articulos" ReadOnly="false" Caption="Artículos" Visible="true" Width="30px" ToolTip="Ingrese aquí el número total de artículos válidos de cada envío">
                                            <PropertiesSpinEdit MinValue="0" MaxValue="100"></PropertiesSpinEdit>
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataCheckColumn FieldName="Cliente" ReadOnly="false" Caption="Cliente" Visible="true" Width="30px" ToolTip="Marque esta casilla si el cliente confirmó de recibida su mercadería">
                                            <PropertiesCheckEdit ValueType="System.String" ValueChecked="S" ValueUnchecked="N"></PropertiesCheckEdit>
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataTextColumn FieldName="NombreCliente" ReadOnly="true" Caption="Nombre Cliente / Fecha del envío" Visible="true" Width="355px">
                                            <EditFormSettings Visible="false"/>
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                            <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                            <CellStyle Font-Size="X-Small" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="false" Width="40px">
                                            <EditFormSettings Visible="false"/>
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsEditing Mode="Batch" />
                                    <ClientSideEvents BatchEditEndEditing="OnBatchEditEndEditing" />
                                    <SettingsBehavior EnableRowHotTrack="true" />
                                    <SettingsPager PageSize="20" Visible="False">
                                    </SettingsPager>
                                    <SettingsText SearchPanelEditorNullText="Ingrese aquí su criterio de búsqueda" CommandBatchEditUpdate="Aplicar cambios" CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"/>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                </td>
                            <td>
                            </td>
                            <td>
                                </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        </table>
                    
                </li>
            </ul>

            <ul>
                <li>

                    <table>
                        <tr>
                            <td colspan="3">
                                <a>Traspasos</a></td>
                            <td colspan="6" style="text-align: right">
                                <asp:Label ID="lbErrorTraspasos" runat="server" Font-Bold="True" ForeColor="Red" ></asp:Label>
                                <asp:Label ID="lbInfoTraspasos" runat="server" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>Artículo:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtArticulo" runat="server" Width="110px" style="text-transform: uppercase;"
                                    CssClass="textBoxStyle" TabIndex="510" AutoPostBack="True" 
                                    ToolTip="Ingrese parte del código o parte de la descripción para buscar" 
                                    ontextchanged="txtArticulo_TextChanged" >
                                </dx:ASPxTextBox>
                            </td>
                            <td colspan="5">
                                <dx:ASPxTextBox ID="txtArticuloDescripcion" runat="server" Width="375px" style="text-transform: uppercase;" CssClass="textBoxStyle" TabIndex="9999" ReadOnly="true" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnAyudaArticulos" runat="server" Width="8px" Height="8px" 
                                    ToolTip="Ayuda de artículos" TabIndex="512" AutoPostBack="False" >
                                    <ClientSideEvents Click="function(s, e) {
                                        txtLinea.SetText('Traspaso');
	                                    popupArticulos.Show();
                                    }" />
                                    <Image IconID="sales_salesanalysis_16x16devav" />
                                </dx:ASPxButton>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Bodega origen:</td>
                            <td>
                                <dx:ASPxComboBox ID="cbBodegaOrigen" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle" 
                                    ValueType="System.String" Width="110px" DropDownStyle="DropDownList" 
                                    DropDownWidth="40" DropDownRows="25" TabIndex="515">
                                </dx:ASPxComboBox>
                            </td>
                            <td colspan="2" style="text-align: right">Bodega destino:</td>
                            <td colspan="2" style="text-align: right">
                                <dx:ASPxComboBox ID="cbBodegaDestino" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle" 
                                    ValueType="System.String" Width="110px" DropDownStyle="DropDownList" 
                                    DropDownWidth="40" DropDownRows="25" TabIndex="520">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txtTraspaso" runat="server" Width="110px" style="text-transform: uppercase;" CssClass="textBoxStyle" TabIndex="550" ToolTip="Ingrese el número de traspaso a imprimir" />
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnImprimirTraspaso" runat="server" Width="8px" Height="8px"
                                    ToolTip="Impresión de traspaso" onclick="btnImprimirTraspaso_Click" TabIndex="555" >
                                    <Image IconID="print_print_16x16office2013" />
                                </dx:ASPxButton>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Cantidad:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtCantidad" runat="server" Width="110px" style="text-transform: uppercase;" CssClass="textBoxStyle" TabIndex="525" />
                            </td>
                            <td colspan="2" style="text-align: right">Localización:</td>
                            <td colspan="2" style="text-align: right">
                                <dx:ASPxComboBox runat="server" DropDownWidth="80px" DropDownRows="12" Width="110px" Theme="SoftOrange" CssClass="ComboBoxStyle" TabIndex="530" ToolTip="Seleccione el año" ID="cbLocalizacion" >
                                    <Items>
                                        <dx:ListEditItem Text="ARMADO" Value="ARMADO" />
                                        <dx:ListEditItem Text="CAJA" Value="CAJA" />
                                        <dx:ListEditItem Text="DESARMDO" Value="DESARMDO" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>
                            <td></td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnAgregarArticulo" runat="server" Width="8px" Height="8px"
                                    ToolTip="Agregar el artículo al traspaso" onclick="btnAgregarArticulo_Click" TabIndex="535" >
                                    <Image IconID="reports_addfooter_16x16" />
                                </dx:ASPxButton>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnGrabarTraspaso" runat="server" Width="8px" Height="8px"
                                    ToolTip="Grabar traspaso" onclick="btnGrabarTraspaso_Click" TabIndex="540" >
                                    <Image IconID="actions_save_16x16devav" />
                                </dx:ASPxButton>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="9">
                                <dx:ASPxGridView ID="gridTraspaso" EnableTheming="True" Theme="SoftOrange" 
                                    runat="server" KeyFieldName="Linea" AutoGenerateColumns="False" 
                                    Border-BorderStyle="None" Width="930px" CssClass="dxGrid" TabIndex="545" Visible="False" 
                                    OnRowCommand="gridTraspaso_RowCommand" onhtmldatacellprepared="gridTraspaso_HtmlDataCellPrepared"
                                    oncelleditorinitialize="gridTraspaso_CellEditorInitialize" onbatchupdate="gridTraspaso_BatchUpdate" onrowupdating="gridTraspaso_RowUpdating" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                    CommandBatchEditUpdate="Aplicar cambios" 
                                    ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                    CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                    SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" />
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="Eliminar" ReadOnly="true" Caption="" Visible="true" Width="50px" >
	                                        <EditFormSettings Visible="False" />
	                                        <DataItemTemplate>
		                                        <dx:ASPxButton ID="btnEliminarArticulo" EncodeHtml="false" runat="server" AutoPostBack="false" Text="Eliminar" 
                                                ToolTip="Haga clic aquí para eliminar la fila." Font-Size="XX-Small"
                                                    ClientInstanceName="btnEliminar">
                                                    <ClientSideEvents Click="function(s, e) {
                                                        e.processOnServer = confirm('¿Desea eliminar la fila seleccionada?');
                                                            }" />
		                                        </dx:ASPxButton>
	                                        </DataItemTemplate>
	                                        <HeaderStyle BackColor="#ff8a3f"  />
	                                        <CellStyle Font-Size="XX-Small" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="Traspaso" ReadOnly="true" Caption="Traspaso" Visible="false" Width="10px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Fecha" ReadOnly="true" Caption="Fecha" Visible="false" Width="10px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Linea" ReadOnly="true" Caption="Línea" Visible="false" Width="10px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Articulo" ReadOnly="true" Caption="Artículo" Visible="true" Width="110px" VisibleIndex="1">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Descripcion" ReadOnly="true" Caption="Descripción del artículo" Visible="true" Width="515px" VisibleIndex="2">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="Localizacion" ReadOnly="false" Caption="Localización" Visible="true" Width="60px" VisibleIndex="3">
                                            <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="40" DropDownRows="5" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Left" >
                                                <Border BorderStyle="None"></Border>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" />
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="BodegaOrigen" ReadOnly="false" Caption="Bodega Origen" Visible="true" Width="75px" VisibleIndex="4">
                                            <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="40" DropDownRows="12" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" >
                                                <Border BorderStyle="None"></Border>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="BodegaDestino" ReadOnly="false" Caption="Bodega Destino" Visible="true" Width="75px" VisibleIndex="5">
                                            <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="40" DropDownRows="12" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" >
                                                <Border BorderStyle="None"></Border>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataColumn FieldName="Cantidad" ReadOnly="false" Caption="Cantidad" Visible="true" Width="45px" VisibleIndex="6">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Usuario" ReadOnly="true" Caption="Usuario" Visible="false" Width="10px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreUsuario" ReadOnly="true" Caption="NombreUsuario" Visible="false" Width="10px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="CreateDate" ReadOnly="true" Caption="CreateDate" Visible="false" Width="10px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreRecibe" ReadOnly="true" Caption="NombreRecibe" Visible="false" Width="10px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Consecutivo" ReadOnly="true" Caption="Consecutivo" Visible="false" Width="10px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Tooltip" ReadOnly="true" Caption="Tooltip" Visible="false" Width="10px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsEditing Mode="Batch" />
                                    <ClientSideEvents BatchEditEndEditing="OnBatchEditEndEditing" />
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                    <SettingsPager PageSize="100" Visible="False" />
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                    </table>

                </li>
            </ul>

            <ul>
                <li>
                    <table>
                        <tr>
                            <td>
                                <asp:LinkButton ID="lkDiasDespachos" runat="server" ToolTip="Haga clic aquí para consultar y modificar los días de despacho asignados a cada región" 
                                    onclick="lkDiasDespachos_Click" TabIndex="555">Consultar días de despachos</asp:LinkButton>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:LinkButton ID="lkGrabarDias" runat="server" ToolTip="Haga clic aquí para grabar los cambios de días" 
                                    onclick="lkGrabarDias_Click" TabIndex="557" Visible="false">Grabar</asp:LinkButton>
                            </td>
                            <td style="text-align: right">
                                <asp:Label ID="lbErrorDias" runat="server" Font-Bold="True" ForeColor="Red" ></asp:Label>
                                <asp:Label ID="lbInfoDias" runat="server" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <dx:ASPxGridView ID="gridDias" EnableTheming="True" Theme="SoftOrange"
                                    runat="server" KeyFieldName="Departamento" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid"
                                    onbatchupdate="gridDias_BatchUpdate" 
                                    onrowupdating="gridDias_RowUpdating" 
                                    onhtmldatacellprepared="gridDias_HtmlDataCellPrepared"
                                    TabIndex="560" Visible="False">
                                    <Styles>
                                        <StatusBar><border borderstyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                        <GroupRow Font-Bold="true" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro"
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar"
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="DiaDespacho" ReadOnly="true" Caption="DiaDespacho" Visible="false" Width="5px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Tipo" ReadOnly="true" Caption="Tipo" Visible="false" Width="10px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Departamento" ReadOnly="true" Caption="Departamento" Visible="true" Width="170px">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataCheckColumn FieldName="Lunes" ReadOnly="false" Caption="Lunes" Visible="true" Width="100px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataCheckColumn FieldName="Martes" ReadOnly="false" Caption="Martes" Visible="true" Width="100px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataCheckColumn FieldName="Miercoles" ReadOnly="false" Caption="Miercoles" Visible="true" Width="100px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataCheckColumn FieldName="Jueves" ReadOnly="false" Caption="Jueves" Visible="true" Width="100px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataCheckColumn FieldName="Viernes" ReadOnly="false" Caption="Viernes" Visible="true" Width="100px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataCheckColumn FieldName="Sabado" ReadOnly="false" Caption="Sábado" Visible="true" Width="100px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataCheckColumn FieldName="Domingo" ReadOnly="false" Caption="Domingo" Visible="true" Width="100px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataColumn FieldName="Tooltip" ReadOnly="false" Caption="Tooltip" Visible="false" Width="5px">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                    </Columns>
                                    <Templates>
                                        <DetailRow>
                                            <dx:ASPxGridView ID="gridDiasDet" runat="server" AutoGenerateColumns="false" Theme="SoftOrange" CssClass="dxGrid" Border-BorderStyle="None" OnLoad="gridDiasDet_Load" 
                                                onbatchupdate="gridDiasDet_BatchUpdate" 
                                                onrowupdating="gridDiasDet_RowUpdating"
                                                onhtmldatacellprepared="gridDiasDet_HtmlDataCellPrepared"
                                                KeyFieldName="Municipio" Width="870px">
                                                <Styles>
                                                    <StatusBar>
                                                        <border borderstyle="None" />
                                                    </StatusBar>
                                                    <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                                    <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                                    <AlternatingRow BackColor="#fde4cf" />
                                                </Styles>
                                                <SettingsBehavior EnableRowHotTrack="True"></SettingsBehavior>
                                                <Settings ShowStatusBar="Hidden" />
                                                <Columns>
                                                    <dx:GridViewDataColumn FieldName="DiaDespacho" ReadOnly="true" Caption="DiaDespacho" Visible="false" Width="5px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="Tipo" ReadOnly="true" Caption="Tipo" Visible="false" Width="10px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="Municipio" ReadOnly="true" Caption="Municipio" Visible="true" Width="170px">
                                                        <HeaderStyle BackColor="#ff8a3f" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataCheckColumn FieldName="Lunes" ReadOnly="false" Caption="Lunes" Visible="true" Width="100px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataCheckColumn>
                                                    <dx:GridViewDataCheckColumn FieldName="Martes" ReadOnly="false" Caption="Martes" Visible="true" Width="100px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataCheckColumn>
                                                    <dx:GridViewDataCheckColumn FieldName="Miercoles" ReadOnly="false" Caption="Miercoles" Visible="true" Width="100px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataCheckColumn>
                                                    <dx:GridViewDataCheckColumn FieldName="Jueves" ReadOnly="false" Caption="Jueves" Visible="true" Width="100px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataCheckColumn>
                                                    <dx:GridViewDataCheckColumn FieldName="Viernes" ReadOnly="false" Caption="Viernes" Visible="true" Width="100px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataCheckColumn>
                                                    <dx:GridViewDataCheckColumn FieldName="Sabado" ReadOnly="false" Caption="Sábado" Visible="true" Width="100px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataCheckColumn>
                                                    <dx:GridViewDataCheckColumn FieldName="Domingo" ReadOnly="false" Caption="Domingo" Visible="true" Width="100px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataCheckColumn>
                                                    <dx:GridViewDataColumn FieldName="Tooltip" ReadOnly="false" Caption="Tooltip" Visible="false" Width="5px">
                                                        <HeaderStyle BackColor="#ff8a3f" />
                                                    </dx:GridViewDataColumn>
                                                </Columns>
                                                <SettingsEditing Mode="Batch" />
                                                <ClientSideEvents BatchEditEndEditing="OnBatchEditEndEditing" />
                                            </dx:ASPxGridView>
                                        </DetailRow>
                                    </Templates>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsEditing Mode="Batch" />
                                    <ClientSideEvents BatchEditEndEditing="OnBatchEditEndEditing" />
                                    <SettingsDetail ShowDetailRow="true" />
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                    <SettingsPager PageSize="100" Visible="False"></SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                    </table>
                </li>
            </ul>

        </div>
        <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server" TabIndex="8888"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" 
                        TabIndex="8888"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
