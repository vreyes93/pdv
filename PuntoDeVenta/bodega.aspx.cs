﻿using System;
using System.Data;
using System.Text;
using System.IO;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CrystalDecisions.CrystalReports.Engine;
using System.Net;
using DevExpress.Web;
using DevExpress.Web.Data;
using DevExpress.Export;
using DevExpress.XtraPrinting;
using MF_Clases;
using Newtonsoft.Json;

namespace PuntoDeVenta
{
    public partial class bodega : BasePage
    {

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                var q = ws.DevuelveTransportistas("F01", Convert.ToString(Session["Usuario"])).OrderBy(t => t.Nombre);
                (gridDespachos.Columns["Transportista"] as GridViewDataComboBoxColumn).PropertiesComboBox.DataSource = q;

                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsFacturas"];

                gridFacturas.DataSource = ds;
                gridFacturas.DataMember = "documentos";
                gridFacturas.DataBind();
                gridFacturas.SettingsPager.PageSize = 500;

                DataSet dsRutas = new DataSet();
                dsRutas = (DataSet)Session["dsRutas"];

                gridRutas.DataSource = dsRutas;
                gridRutas.DataMember = "documentos";
                gridRutas.DataBind();
                gridRutas.SettingsPager.PageSize = 500;
                gridRutas.DataColumns["NombreCliente"].Settings.AutoFilterCondition = AutoFilterCondition.Contains;

                DataSet dsDespachos = new DataSet();
                dsDespachos = (DataSet)Session["dsDespachos"];

                gridDespachos.DataSource = dsDespachos;
                gridDespachos.DataMember = "documentos";
                gridDespachos.DataBind();
                gridDespachos.SettingsPager.PageSize = 500;
                gridDespachos.DataColumns["NombreCliente"].Settings.AutoFilterCondition = AutoFilterCondition.Contains;

                DataSet dsPedidos = new DataSet();
                dsPedidos = (DataSet)Session["dsPedidos"];

                gridPedidos.DataSource = dsPedidos;
                gridPedidos.DataMember = "documentos";
                gridPedidos.DataBind();
                gridPedidos.SettingsPager.PageSize = 500;

                List<Clases.Traspasos> mTraspaso = new List<Clases.Traspasos>();
                mTraspaso = (List<Clases.Traspasos>)Session["Traspaso"];

                gridTraspaso.DataSource = mTraspaso.AsEnumerable();
                gridTraspaso.DataMember = "Traspasos";
                gridTraspaso.DataBind();
                gridTraspaso.FocusedRowIndex = -1;
                gridTraspaso.SettingsPager.PageSize = 500;

                Clases.DiasDespachos mDiasDespachos = new Clases.DiasDespachos();
                mDiasDespachos = (Clases.DiasDespachos)Session["DiasDespachos"];

                gridDias.DataSource = mDiasDespachos.Departamentos.AsEnumerable();
                gridDias.DataMember = "Departamentos";
                gridDias.DataBind();
                gridDias.FocusedRowIndex = -1;
                gridDias.SettingsPager.PageSize = 500;
            }
            catch{}
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                HookOnFocus(this.Page as Control);
                
                gridEnvio.Visible = false;
                lkImprimirAjuste.Visible = false;
                Session["dsArticulos"] = null;

                ViewState["Accion"] = null;
                Session["dsFacturas"] = null;
                Session["dsRutas"] = null;
                Session["dsDespachos"] = null;
                Session["dsPedidos"] = null;

                List<Clases.Traspasos> mTraspaso = new List<Clases.Traspasos>();
                Session["Traspaso"] = mTraspaso;

                DataSet ds = new DataSet();
                DataTable dt = new DataTable("Bodegas");
                dt.Columns.Add(new DataColumn("Bodega", typeof(System.String)));

                ds.Tables.Add(dt);
                Session["Bodegas"] = ds;

                CargarSeries();
               
                    CargarBodegas();
                CargarTransportistas();
                if (Session["Ambiente"].ToString() != "DES")
                    CargarArticulosBusqueda();
                LimpiarControlesDespacho();
            }
            else
            {
                try
                {
                    DataSet dsTodos = new DataSet();
                    dsTodos = (DataSet)Session["dsTodos"];

                    gridArticulosBuscar.DataSource = dsTodos;
                    gridArticulosBuscar.DataMember = "articulos";
                    gridArticulosBuscar.DataBind();
                    gridArticulosBuscar.FocusedRowIndex = -1;
                    gridArticulosBuscar.SettingsPager.PageSize = 35;
                }
                catch
                {
                    lbErrorDespachos.Text = "Sesión expirada, por favor salga del sistema y vuelva a ingresar.";
                }
            }

            Page.ClientScript.RegisterStartupScript(
            typeof(bodega),
            "ScriptDoFocus",
            SCRIPT_DOFOCUS.Replace("REQUEST_LASTFOCUS", Request["__LASTFOCUS"]),
            true);
        }

        private const string SCRIPT_DOFOCUS = @"window.setTimeout('DoFocus()', 1); function DoFocus() {
            try {
                document.getElementById('REQUEST_LASTFOCUS').focus();
            } catch (ex) {}
        }";

        private void HookOnFocus(Control CurrentControl)
        {
            //checks if control is one of TextBox, DropDownList, ListBox or Button
            if ((CurrentControl is TextBox) ||
                (CurrentControl is DropDownList) ||
                (CurrentControl is ListBox) ||
                (CurrentControl is Button))
                //adds a script which saves active control on receiving focus 
                //in the hidden field __LASTFOCUS.
                (CurrentControl as WebControl).Attributes.Add(
                   "onfocus",
                   "try{document.getElementById('__LASTFOCUS').value=this.id} catch(e) {}");
            //checks if the control has children
            if (CurrentControl.HasControls())
                //if yes do them all recursively
                foreach (Control CurrentChildControl in CurrentControl.Controls)
                    HookOnFocus(CurrentChildControl);
        }

        void CargarEnvio()
        {
            try
            {
                lbInfoEnvio.Text = "";
                lbErrorEnvio.Text = "";
                gridEnvio.Visible = false;

                if (txtEnvio.Text.Trim().Length == 0)
                {
                    lbErrorEnvio.Text = "Debe ingresar un número de envío.";
                    return;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                string mMensaje = "";
                //var qEnvio = ws.DevuelveEnvioBodega(ref mMensaje, txtEnvio.Text.ToUpper());

                DataSet ds = new DataSet();
                ds = ws.DevuelveEnvioBodegaDs(ref mMensaje, txtEnvio.Text.ToUpper());

                if (mMensaje.Trim().Length > 0) lbErrorEnvio.Text = mMensaje;

                if (ds.Tables["articulos"].Rows.Count > 0)
                {
                    gridEnvio.Visible = true;
                    gridEnvio.DataSource = ds;
                    gridEnvio.DataMember = "articulos";
                    gridEnvio.DataBind();
                    gridEnvio.SettingsPager.PageSize = 500;

                    lkGrabarEnvio.Visible = true;
                    lkImprimirAjuste.Visible = false;
                }

                Session["dsArticulos"] = ds;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorEnvio.Text = string.Format("Error al buscar el envío {0} {1}", ex.Message, m);
            }
        }

        void GrabarEnvio()
        {
            try
            {
                lbInfoEnvio.Text = "";
                lbErrorEnvio.Text = "";

                if (txtEnvio.Text.Trim().Length == 0)
                {
                    lbErrorEnvio.Text = "Debe ingresar un número de envío.";
                    return;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                string mMensaje = "";
                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsArticulos"];

                string[] campos = new string[] { "Linea", "BodegaNueva" };
                for (int ii = 0; ii < gridEnvio.VisibleRowCount; ii++)
                {

                    object row = gridEnvio.GetRowValues(ii, campos);
                    object[] datos = (object[])row;

                    int jj = 0;
                    int mLinea = 0;
                    string mBodega = "";

                    foreach (object dato in datos)
                    {
                        if (jj == 0)
                        {
                            mLinea = Convert.ToInt32(dato.ToString());
                        }
                        else
                        {
                            mBodega = dato.ToString().ToUpper();
                        }
                        jj += 1;
                    }

                    ds.Tables["articulos"].Rows.Find(mLinea)["BodegaNueva"] = mBodega;
                }

                if (!ws.GrabarEnvioBodega(ref mMensaje, ref ds, Convert.ToString(Session["usuario"])))
                {
                    lbErrorEnvio.Text = mMensaje;
                    return;
                }

                Session["dsArticulos"] = ds;

                lbInfoEnvio.Text = mMensaje;
                gridEnvio.Visible = false;
                lkGrabarEnvio.Visible = false;
                lkImprimirAjuste.Visible = true;
                txtEnvio.Text = "";
                txtEnvio.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorEnvio.Text = string.Format("Error al grabar el envío {0} {1}", ex.Message, m);
            }
        }

        protected void lkConsultarEnvio_Click(object sender, EventArgs e)
        {
            CargarEnvio();
        }

        protected void lkGrabarEnvio_Click(object sender, EventArgs e)
        {
            GrabarEnvio();
        }

        protected void CancelEditing(CancelEventArgs e)
        {
            e.Cancel = true;
            gridEnvio.CancelEdit();
        }

        void UpdateEnvio(string linea, string bodega)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsArticulos"];

                ds.Tables["articulos"].Rows.Find(linea)["BodegaNueva"] = bodega;
                Session["dsArticulos"] = ds;

                gridEnvio.DataSource = ds;
                gridEnvio.DataMember = "articulos";
                gridEnvio.DataBind();

                DataSet ds2 = new DataSet();
                ds2 = (DataSet)Session["dsArticulos"];

                int ii = 0;
                ii += 1;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al actualizar los datos UPDATE {0} {1}", ex.Message, m);
            }
        }

        protected void gridEnvio_BatchUpdate(object sender, DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs e)
        {
            foreach (var args in e.UpdateValues)
                UpdateEnvio(args.Keys["Linea"].ToString(), args.NewValues["BodegaNueva"].ToString());

            e.Handled = true;
        }

        protected void gridEnvio_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            UpdateEnvio(e.Keys["Linea"].ToString(), e.NewValues["BodegaNueva"].ToString());
            CancelEditing(e);
        }

        protected void gridEnvio_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == ColumnCommandButtonType.Update || e.ButtonType == ColumnCommandButtonType.Cancel) e.Visible = false;
        }

        protected void gridEnvio_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            if (e.Column.FieldName == "BodegaNueva")
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                var q = ws.DevuelveTiendasTodas();

                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                cmb.DataSource = q;
                cmb.ValueField = "Tienda";
                cmb.ValueType = typeof(System.String);
                cmb.TextField = "Tienda";
                cmb.DataBindItems();
            }
        }

        protected void lkImprimirAjuste_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsArticulos"];

                using (ReportDocument reporte = new ReportDocument())
                {
                    string mNombreDocumento = string.Format("Ajuste_{0}.pdf", ds.Tables["Ajustes"].Rows[0]["Documento"]);
                    string mFormato = "rptAjusteInventario.rpt";
                    string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mFormato);

                    reporte.Load(p);
                    reporte.SetDataSource(ds);

                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";

                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al imprimir el ajuste {0} {1}", ex.Message, m);
            }
        }



        //Despachos
        void CargarSeries()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") 
                ws.Url = Convert.ToString(Session["url"]);

            var q = ws.SeriesDespachos();

            cbSerie.DataSource = q;
            cbSerie.ValueField = "Serie";
            cbSerie.ValueType = typeof(System.String);
            cbSerie.TextField = "Nombre";
            cbSerie.DataBindItems();
        }

        void CargarBodegas()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveTiendasUsuario(Convert.ToString(Session["Usuario"]));

            tienda.DataSource = q;
            tienda.ValueField = "Tienda";
            tienda.ValueType = typeof(System.String);
            tienda.TextField = "Tienda";
            tienda.DataBindItems();

            tiendaRutas.DataSource = q;
            tiendaRutas.ValueField = "Tienda";
            tiendaRutas.ValueType = typeof(System.String);
            tiendaRutas.TextField = "Tienda";
            tiendaRutas.DataBindItems();

            tiendaTransporte.DataSource = q;
            tiendaTransporte.ValueField = "Tienda";
            tiendaTransporte.ValueType = typeof(System.String);
            tiendaTransporte.TextField = "Tienda";
            tiendaTransporte.DataBindItems();

            bool mTodas = false;
            ViewState["dias"] = 0;

            foreach (var item in q)
            {
                if (item.Tienda == "F01")
                {
                    mTodas = true;
                    ViewState["dias"] = 1;
                }
            }

            if (mTodas) q = ws.DevuelveTiendasTodas();

            cbBodegaOrigen.DataSource = q;
            cbBodegaOrigen.ValueField = "Tienda";
            cbBodegaOrigen.ValueType = typeof(System.String);
            cbBodegaOrigen.TextField = "Nombre";
            cbBodegaOrigen.DataBindItems();

            cbBodegaDestino.DataSource = q;
            cbBodegaDestino.ValueField = "Tienda";
            cbBodegaDestino.ValueType = typeof(System.String);
            cbBodegaDestino.TextField = "Nombre";
            cbBodegaDestino.DataBindItems();

            DataSet ds = new DataSet();
            ds = (DataSet)Session["Bodegas"];

            foreach (var item in q)
            {
                DataRow mRow = ds.Tables["Bodegas"].NewRow();
                mRow["Bodega"] = item.Tienda;
                ds.Tables["Bodegas"].Rows.Add(mRow);
            }

            Session["Bodegas"] = ds;
        }

        void CargarTransportistas()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveTransportistas("F01", Convert.ToString(Session["Usuario"])).OrderBy(t => t.Nombre);

            transportista.DataSource = q;
            transportista.ValueField = "Transportista";
            transportista.ValueType = typeof(System.Int32);
            transportista.TextField = "Nombre";
            transportista.DataBindItems();

            transportistaRutas.DataSource = q;
            transportistaRutas.ValueField = "Transportista";
            transportistaRutas.ValueType = typeof(System.Int32);
            transportistaRutas.TextField = "Nombre";
            transportistaRutas.DataBindItems();

            transportistaT.DataSource = q;
            transportistaT.ValueField = "Transportista";
            transportistaT.ValueType = typeof(System.Int32);
            transportistaT.TextField = "Nombre";
            transportistaT.DataBindItems();

            transportista.Value = 0;
            transportistaRutas.Value = 1;
            transportistaT.Value = 1;
        }

        void CargarArticulosBusqueda()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            DataSet dsTodos = new DataSet();

            ws.Timeout = 999999999;
            dsTodos = ws.DevuelveArticulosTodos();

            gridArticulosBuscar.DataSource = dsTodos;
            gridArticulosBuscar.DataMember = "articulos";
            gridArticulosBuscar.DataBind();
            gridArticulosBuscar.FocusedRowIndex = -1;
            gridArticulosBuscar.SettingsPager.PageSize = 35;

            Session["dsTodos"] = dsTodos;
        }

        void LimpiarControlesDespacho()
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                DateTime mFechaFinal = DateTime.Now.Date;
                DateTime mFechaInicial = mFechaFinal.AddDays(-7);

                fechaInicial.Value = mFechaInicial;
                fechaFinal.Value = mFechaFinal;

                int mDias = 0;
                DateTime mFechaInicialE = DateTime.Now.Date.AddDays(1);
                if (DateTime.Now.Date.DayOfWeek == DayOfWeek.Saturday) mDias = 1;

                fechaInicialE.Value = mFechaInicialE;
                fechaFinalE.Value = mFechaInicialE.AddDays(mDias);

                fechaInicialR.Value = mFechaInicialE;
                fechaFinalR.Value = mFechaInicialE.AddDays(mDias);

                fechaInicialT.Value = mFechaInicialE;
                fechaFinalT.Value = mFechaInicialE.AddDays(mDias);

                fechaInicialPedidos.Value = mFechaInicialE;
                fechaFinalPedidos.Value = mFechaInicialE.AddDays(mDias);

                txtCliente.Text = "";
                txtDespachoTraspaso.Text = "";
                txtDespachoTraspasoT.Text = "";

                tienda.SelectedIndex = 0;
                tiendaRutas.SelectedIndex = 0;
                tiendaTransporte.SelectedIndex = 0;

                cbBodegaOrigen.SelectedIndex = 0;
                cbBodegaDestino.SelectedIndex = 1;
                cbLocalizacion.SelectedIndex = 0;
                txtCantidad.Text = "1";

                txtFactura.Text = "";
                txtFactura.ToolTip = "Ingrese el número de factura a buscar";
                lbPedido.Text = "";
                lbAyudaTransportista.Text = "";
                articulosValdios.Value = 0;

                fechaInicial.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al inicializar los despachos. {0} {1}", ex.Message, m);
            }
        }

        protected void gridFacturas_BatchUpdate(object sender, ASPxDataBatchUpdateEventArgs e)
        {

        }

        protected void gridFacturas_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {

        }

        protected void gridFacturas_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {

        }

        protected void gridFacturas_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {

        }

        protected void lkSeleccionarFechasE_Click(object sender, EventArgs e)
        {
            CargarFacturas("E");
        }

        protected void lkSeleccionarCliente_Click(object sender, EventArgs e)
        {
            CargarFacturas("C");
        }

        protected void lkSeleccionarTienda_Click(object sender, EventArgs e)
        {
            CargarFacturas("T");
        }

        protected void lkSeleccionarAprobadas_Click(object sender, EventArgs e)
        {
            CargarFacturas("A");
        }

        void CargarFacturas(string tipo)
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                gridFacturas.Visible = false;
                gridFacturaDet.Visible = false;

                txtFactura.Text = "";
                txtPedido.Text = "";
                txtTienda.Text = "";
                txtNombreCliente.Text = "";
                txtDireccion.Text = "";
                txtTelefonos.Text = "";
                txtObservaciones.Text = "";
                txtPersonaRecibe.Text = "";
                txtVendedor.Text = "";
                txtObservacionesVendedor.Text = "";

                int mDias = Convert.ToInt32(ViewState["dias"]);
                fechaEntrega.Value = DateTime.Now.Date.AddDays(mDias);

                txtFactura.ToolTip = "Ingrese el número de factura a buscar";
                lbPedido.Text = "";
                transportista.Value = 0;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DateTime mFechaInicial = Convert.ToDateTime(fechaInicial.Value);
                DateTime mFechaFinal = Convert.ToDateTime(fechaFinal.Value);

                if (tipo == "E")
                {
                    mFechaInicial = Convert.ToDateTime(fechaInicialE.Value);
                    mFechaFinal = Convert.ToDateTime(fechaFinalE.Value);
                }

                DataSet ds = new DataSet(); string mMensaje = "";
                ds = ws.DocumentosPorDespacharAprobados(ref mMensaje, mFechaInicial, mFechaFinal, txtCliente.Text.Trim().ToUpper(), Convert.ToString(tienda.Value), tipo, Convert.ToString(Session["Usuario"]), false);

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorDespachos.Text = mMensaje;
                    return;
                }

                if (ds.Tables["documentos"].Rows.Count > 0)
                {
                    gridFacturas.Visible = true;
                    gridFacturas.DataSource = ds;
                    gridFacturas.DataMember = "documentos";
                    gridFacturas.DataBind();
                    gridFacturas.FocusedRowIndex = -1;
                    gridFacturas.SettingsPager.PageSize = 500;

                    Session["dsFacturas"] = ds;
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al mostrar los documentos. {0} {1}", ex.Message, m);
            }

        }

        void CargarFactura()
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                if (txtFactura.Text.Trim().Length == 0) return;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DataSet ds = new DataSet(); string mMensaje = "";
                ds = ws.DocumentoPorDespachar(ref mMensaje, txtFactura.Text.Trim().ToUpper(), Convert.ToString(Session["Usuario"]));

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorDespachos.Text = mMensaje;
                    return;
                }

                if (ds.Tables["documento"].Rows.Count > 0)
                {
                    //gridFacturas.Visible = false;
                    gridFacturaDet.Visible = true;
                    gridFacturaDet.DataSource = ds;
                    gridFacturaDet.DataMember = "documento";
                    gridFacturaDet.DataBind();
                    gridFacturaDet.FocusedRowIndex = -1;
                    gridFacturaDet.SettingsPager.PageSize = 100;

                    Session["Cliente"] = Convert.ToString(ds.Tables["documento"].Rows[0]["Cliente"]);
                    txtNombreCliente.Text = string.Format("{0} - {1}", Convert.ToString(ds.Tables["documento"].Rows[0]["Cliente"]), Convert.ToString(ds.Tables["documento"].Rows[0]["NombreCliente"]));
                    txtTelefonos.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Telefonos"]);
                    txtDireccion.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Direccion"]);
                    txtTienda.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Cobrador"]);
                    txtPedido.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Pedido"]);
                    txtPersonaRecibe.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["NombreRecibe"]);
                    txtVendedor.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Vendedor"]);
                    txtObservacionesVendedor.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["ObservacionesVendedor"]);

                    fechaEntrega.Value = Convert.ToDateTime(ds.Tables["documento"].Rows[0]["fechaEntrega"]);

                   
                    txtObservaciones.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Observaciones"]);
                    txtFactura.ToolTip = string.Format("Esta factura tiene el Pedido No. {0}", Convert.ToString(ds.Tables["documento"].Rows[0]["Pedido"]));
                    lbPedido.Text = string.Format("Pedido No. {0}", Convert.ToString(ds.Tables["documento"].Rows[0]["Pedido"]));

                    //lbAyudaTransportista.Font.Size = FontUnit.Small;
                    lbAyudaTransportista.Text = ws.AyudaTransportista(Convert.ToString(ds.Tables["documento"].Rows[0]["Cliente"]), Convert.ToDateTime(ds.Tables["documento"].Rows[0]["FechaEntrega"]));

                    txtTelefonos.ReadOnly = true;
                    txtDireccion.ReadOnly = true;
                    txtNombreCliente.ReadOnly = true;


                    string mSerie = ds.Tables["documento"].Rows[0]["SerieDespacho"].ToString();

                    cbSerie.Value = mSerie;
                    txtDespacho.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Despacho"]);

                    if (Convert.ToString(ds.Tables["documento"].Rows[0]["Consecutivo"]) == "DES SUENIA")
                    {
                        txtTelefonos.ReadOnly = false;
                        txtDireccion.ReadOnly = false;
                        txtNombreCliente.ReadOnly = false;

                        txtNombreCliente.Focus();
                    }
                    else
                    {
                        txtObservaciones.Focus();
                    }
                }

                Session["dsArticulos"] = ds;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al mostrar el documento. {0} {1}", ex.Message, m);
            }
        }

        protected void CancelEditingFacturaDet(CancelEventArgs e)
        {
            e.Cancel = true;
            gridFacturaDet.CancelEdit();
        }

        void UpdateFactura(string linea, string bodega, string cantidad, string localizacion)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsArticulos"];

                ds.Tables["documento"].Rows.Find(linea)["Bodega"] = bodega;
                ds.Tables["documento"].Rows.Find(linea)["Cantidad"] = Convert.ToInt32(cantidad);
                ds.Tables["documento"].Rows.Find(linea)["Localizacion"] = localizacion;
                Session["dsArticulos"] = ds;

                gridFacturaDet.DataSource = ds;
                gridFacturaDet.DataMember = "documento";
                gridFacturaDet.DataBind();
                gridFacturaDet.FocusedRowIndex = -1;
                gridFacturaDet.SettingsPager.PageSize = 100;

                DataSet ds2 = new DataSet();
                ds2 = (DataSet)Session["dsArticulos"];

                int ii = 0;
                ii += 1;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al actualizar los datos UPDATE {0} {1}", ex.Message, m);
            }
        }

        protected void gridFacturaDet_BatchUpdate(object sender, ASPxDataBatchUpdateEventArgs e)
        {
            foreach (var args in e.UpdateValues)
                UpdateFactura(args.Keys["Linea"].ToString(), args.NewValues["Bodega"].ToString(), args.NewValues["Cantidad"].ToString(), args.NewValues["Localizacion"].ToString());

            e.Handled = true;
        }

        protected void gridFacturaDet_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            UpdateFactura(e.Keys["Linea"].ToString(), e.NewValues["Bodega"].ToString(), e.NewValues["Cantidad"].ToString(), e.NewValues["Localizacion"].ToString());
            CancelEditingFacturaDet(e);
        }

        protected void gridFacturaDet_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            if (e.Column.FieldName == "Bodega")
            {
                var q = ws.DevuelveTiendasTodas();

                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                cmb.DataSource = q;
                cmb.ValueField = "Tienda";
                cmb.ValueType = typeof(System.String);
                cmb.TextField = "Tienda";
                cmb.DataBindItems();
            }

            if (e.Column.FieldName == "Localizacion")
            {
                DataSet ds = new DataSet();
                ds = ws.Localizaciones();

                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                cmb.DataSource = ds.Tables["Localizaciones"];
                cmb.ValueField = "Localizacion";
                cmb.ValueType = typeof(System.String);
                cmb.TextField = "Localizacion";
                cmb.DataBindItems();
            }
        }

        protected void gridFacturaDet_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == ColumnCommandButtonType.Update || e.ButtonType == ColumnCommandButtonType.Cancel) e.Visible = false;
        }

        protected void lkCargarFactura_Click(object sender, EventArgs e)
        {
            CargarFactura();
        }

        protected void txtCliente_TextChanged(object sender, EventArgs e)
        {
            CargarFacturas("C");
        }

        protected void lkGrabarDespacho_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsArticulos"];

                if (!ws.GrabarDespachoF01(txtDespacho.Text.Trim().ToUpper(), ref mMensaje, Convert.ToString(Session["Usuario"]), txtObservaciones.Text, ds, txtNombreCliente.Text, txtTelefonos.Text, txtDireccion.Text, transportista.Value.ToString(), Convert.ToDateTime(fechaEntrega.Value), articulosValdios.Value.ToString(), cbSerie.Value.ToString()))
                {
                    lbErrorDespachos.Text = mMensaje;
                    return;
                }

                //gridFacturas.Visible = false;
                gridFacturaDet.Visible = false;

                DataSet dsFacturas = new DataSet();
                dsFacturas = (DataSet)Session["dsFacturas"];

                int mSiguiente = 0;

                try
                {
                    for (int ii = 0; ii < dsFacturas.Tables["documentos"].Rows.Count; ii++)
                    {
                        if (txtFactura.Text.Trim().ToUpper() == Convert.ToString(dsFacturas.Tables["documentos"].Rows[ii]["Factura"])) mSiguiente = ii;
                    }

                    dsFacturas.Tables["documentos"].Rows.Find(txtFactura.Text.Trim().ToUpper()).Delete();
                    Session["dsFacturas"] = dsFacturas;
                }
                catch
                {
                    //Nothing
                }

                txtFactura.Text = "";
                txtPedido.Text = "";
                txtTienda.Text = "";
                txtNombreCliente.Text = "";
                txtDireccion.Text = "";
                txtTelefonos.Text = "";
                txtObservaciones.Text = "";
                txtPersonaRecibe.Text = "";
                txtVendedor.Text = "";
                txtObservacionesVendedor.Text = "";

                int mDias = Convert.ToInt32(ViewState["dias"]);
                fechaEntrega.Value = DateTime.Now.Date.AddDays(mDias);

                txtFactura.ToolTip = "Ingrese el número de factura a buscar";
                lbPedido.Text = "";
                transportista.Value = 0;
                lbAyudaTransportista.Text = "";
                articulosValdios.Value = 0;

                gridFacturas.DataSource = dsFacturas;
                gridFacturas.DataMember = "documentos";
                gridFacturas.DataBind();
                gridFacturas.SettingsPager.PageSize = 500;
                gridFacturas.FocusedRowIndex = -1;

                try
                {
                    txtFactura.Text = Convert.ToString(dsFacturas.Tables["documentos"].Rows[mSiguiente]["Factura"]);
                    CargarFactura();
                }
                catch
                {
                    //Nothing
                }

                lbInfoDespachos.Text = mMensaje;
                //btnImprimirDespacho.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al grabar el despacho. {0} {1}", ex.Message, m);
            }
        }

        protected void gridFacturaDet_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.VisibleIndex >= 0) e.Cell.ToolTip = e.GetValue("Tooltip").ToString();
        }

        protected void gridFacturas_FocusedRowChanged(object sender, EventArgs e)
        {
            if (!IsPostBack) return;
            if (gridFacturas.FocusedRowIndex == -1) return;
            if (txtFactura.Text.Trim().Length > 0) return;
            if (Convert.ToString(gridFacturas.GetRowValues(gridFacturas.FocusedRowIndex, "Factura")).Trim().Length == 0) return;
            if (txtFactura.Text == Convert.ToString(gridFacturas.GetRowValues(gridFacturas.FocusedRowIndex, "Factura"))) return;

            txtFactura.Text = Convert.ToString(gridFacturas.GetRowValues(gridFacturas.FocusedRowIndex, "Factura"));
            CargarFactura();
        }

        protected void gridFacturas_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            //if (Convert.ToString(e.GetValue("Aprobada")) == "S") e.Row.BackColor = System.Drawing.Color.LightGreen;
        }

        protected void gridFacturaDet_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (Convert.ToInt32(e.GetValue("Existencias")) == 0 && Convert.ToString(e.GetValue("Tipo")) != "K") e.Row.ForeColor = System.Drawing.Color.Red;
            if (!Convert.ToString(e.GetValue("Tooltip")).Contains(Convert.ToString(e.GetValue("Bodega"))) && Convert.ToString(e.GetValue("Tipo")) != "K") e.Row.ForeColor = System.Drawing.Color.Red;
        }

        void ImprimirExpediente(string pedido)
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                if (!ws.PedidoFacturado(pedido))
                {
                    lbErrorDespachos.Text = "Este pedido NO se encuentra facturado, no es posible continuar";
                    return;
                }

                dsPuntoVenta ds = new dsPuntoVenta();
                var qExpediente = ws.DevuelveExpediente(pedido, "");
                var qExpedienteDet = ws.DevuelveExpedienteDet(pedido, "");

                DataRow mRowExpediente = ds.Expediente.NewRow();
                mRowExpediente["Cliente"] = qExpediente[0].Cliente;
                mRowExpediente["Pedido"] = qExpediente[0].Pedido;
                mRowExpediente["Fecha"] = qExpediente[0].Fecha;
                mRowExpediente["Vendedor"] = qExpediente[0].Vendedor;
                mRowExpediente["CodigoVendedor"] = qExpediente[0].CodigoVendedor;
                mRowExpediente["Tienda"] = qExpediente[0].Tienda;
                mRowExpediente["PrimerNombre"] = qExpediente[0].PrimerNombre;
                mRowExpediente["SegundoNombre"] = qExpediente[0].SegundoNombre;
                mRowExpediente["PrimerApellido"] = qExpediente[0].PrimerApellido;
                mRowExpediente["SegundoApellido"] = qExpediente[0].SegundoApellido;
                mRowExpediente["ApellidoCasada"] = qExpediente[0].ApellidoCasada;
                mRowExpediente["DPI"] = qExpediente[0].DPI;
                mRowExpediente["NIT"] = qExpediente[0].NIT;
                mRowExpediente["Direccion"] = qExpediente[0].Direccion;
                mRowExpediente["Colonia"] = qExpediente[0].Colonia;
                mRowExpediente["Zona"] = qExpediente[0].Zona;
                mRowExpediente["Departamento"] = qExpediente[0].Departamento;
                mRowExpediente["Municipio"] = qExpediente[0].Municipio;
                mRowExpediente["TelefonoResidencia"] = qExpediente[0].TelefonoResidencia;
                mRowExpediente["TelefonoCelular"] = qExpediente[0].TelefonoCelular;
                mRowExpediente["TelefonoTrabajo"] = qExpediente[0].TelefonoTrabajo;
                mRowExpediente["Email"] = qExpediente[0].Email;
                mRowExpediente["EntregarMismaDireccion"] = qExpediente[0].EntregarMismaDireccion;
                mRowExpediente["NombreEntrega"] = qExpediente[0].NombreEntrega;
                mRowExpediente["DireccionEntrega"] = qExpediente[0].DireccionEntrega;
                mRowExpediente["ColoniaEntrega"] = qExpediente[0].ColoniaEntrega;
                mRowExpediente["ZonaEntrega"] = qExpediente[0].ZonaEntrega;
                mRowExpediente["DepartamentoEntrega"] = qExpediente[0].DepartamentoEntrega;
                mRowExpediente["MunicipioEntrega"] = qExpediente[0].MunicipioEntrega;
                mRowExpediente["EntraCamion"] = qExpediente[0].EntraCamion;
                mRowExpediente["EntraPickup"] = qExpediente[0].EntraPickup;
                mRowExpediente["EsSegundoPiso"] = qExpediente[0].EsSegundoPiso;
                mRowExpediente["Notas"] = qExpediente[0].Notas;
                mRowExpediente["BodegaSale"] = qExpediente[0].BodegaSale;
                mRowExpediente["TiendaSale"] = qExpediente[0].TiendaSale;
                mRowExpediente["ObsVendedor"] = qExpediente[0].ObsVendedor;
                mRowExpediente["ObsGerencia"] = qExpediente[0].ObsGerencia;
                mRowExpediente["Gerente"] = qExpediente[0].Gerente;
                mRowExpediente["Factura"] = qExpediente[0].Factura;
                mRowExpediente["Garantia"] = qExpediente[0].Garantia;
                mRowExpediente["Promocion"] = qExpediente[0].Promocion;
                mRowExpediente["Financiera"] = qExpediente[0].Financiera;
                mRowExpediente["QuienAutorizo"] = qExpediente[0].QuienAutorizo;
                mRowExpediente["NoAutorizacion"] = qExpediente[0].NoAutorizacion;
                mRowExpediente["Enganche"] = qExpediente[0].Enganche;
                mRowExpediente["Plan"] = qExpediente[0].Plan;
                mRowExpediente["NoSolicitud"] = qExpediente[0].NoSolicitud;
                mRowExpediente["FechaHora"] = qExpediente[0].FechaHora;
                mRowExpediente["CantidadPagos1"] = qExpediente[0].CantidadPagos1;
                mRowExpediente["MontoPagos1"] = qExpediente[0].MontoPagos1;
                mRowExpediente["CantidadPagos2"] = qExpediente[0].CantidadPagos2;
                mRowExpediente["MontoPagos2"] = qExpediente[0].MontoPagos2;
                mRowExpediente["Despacho"] = qExpediente[0].Despacho;
                mRowExpediente["Radio"] = qExpediente[0].Radio;
                mRowExpediente["Volante"] = qExpediente[0].Volante;
                mRowExpediente["PrensaLibre"] = qExpediente[0].PrensaLibre;
                mRowExpediente["ElQuetzalteco"] = qExpediente[0].ElQuetzalteco;
                mRowExpediente["NuestroDiario"] = qExpediente[0].NuestroDiario;
                mRowExpediente["Internet"] = qExpediente[0].Internet;
                mRowExpediente["TV"] = qExpediente[0].TV;
                mRowExpediente["Otros"] = qExpediente[0].Otros;
                mRowExpediente["ObsOtros"] = qExpediente[0].ObsOtros;
                mRowExpediente["AutorizacionEspecial"] = qExpediente[0].AutorizacionEspecial;
                mRowExpediente["ObservacionesAdicionales"] = qExpediente[0].ObservacionesAdicionales;
                mRowExpediente["IndicacionesLlegar"] = qExpediente[0].IndicacionesLlegar;
                mRowExpediente["EntregaAMPM"] = qExpediente[0].EntregaAMPM;
                mRowExpediente["FechaEntrega"] = qExpediente[0].FechaEntrega;
                mRowExpediente["NotasPrecio"] = qExpediente[0].NotasPrecio;
                mRowExpediente["Factor"] = qExpediente[0].Factor;
                mRowExpediente["Recibos"] = qExpediente[0].Recibos;
                ds.Expediente.Rows.Add(mRowExpediente);

                for (int ii = 0; ii < qExpedienteDet.Length; ii++)
                {
                    DataRow mRowExpedienteDet = ds.ExpedienteDet.NewRow();
                    mRowExpedienteDet["Cliente"] = qExpedienteDet[ii].Cliente;
                    mRowExpedienteDet["Pedido"] = qExpedienteDet[ii].Pedido;
                    mRowExpedienteDet["Articulo"] = qExpedienteDet[ii].Articulo;
                    mRowExpedienteDet["Descripcion"] = qExpedienteDet[ii].Descripcion;
                    mRowExpedienteDet["PrecioLista"] = qExpedienteDet[ii].PrecioLista;
                    mRowExpedienteDet["Cantidad"] = qExpedienteDet[ii].Cantidad;
                    mRowExpedienteDet["Factor"] = qExpedienteDet[ii].Factor;
                    mRowExpedienteDet["PrecioTotal"] = qExpedienteDet[ii].PrecioTotal;
                    mRowExpedienteDet["Bodega"] = qExpedienteDet[ii].Bodega;
                    ds.ExpedienteDet.Rows.Add(mRowExpedienteDet);
                }

                int mRows = ds.ExpedienteDet.Rows.Count;
                for (int ii = mRows; ii < 16; ii++)
                {
                    DataRow mRowExpedienteDet = ds.ExpedienteDet.NewRow();
                    mRowExpedienteDet["Cliente"] = qExpediente[0].Cliente;
                    mRowExpedienteDet["Pedido"] = qExpediente[0].Pedido;
                    mRowExpedienteDet["Articulo"] = "";
                    mRowExpedienteDet["Descripcion"] = "";
                    mRowExpedienteDet["PrecioLista"] = 0;
                    mRowExpedienteDet["Cantidad"] = 0;
                    mRowExpedienteDet["Factor"] = 0;
                    mRowExpedienteDet["PrecioTotal"] = 0;
                    mRowExpedienteDet["Bodega"] = "";
                    ds.ExpedienteDet.Rows.Add(mRowExpedienteDet);
                }

                using (ReportDocument ReporteExpediente = new ReportDocument())
                {
                    string m = (Request.PhysicalApplicationPath + "reportes/rptExpediente.rpt");
                    ReporteExpediente.Load(m);

                    string mExpediente = string.Format("Expediente_{0}.pdf", pedido);
                    ReporteExpediente.SetDataSource(ds);

                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");

                    try
                    {
                        if (File.Exists(@"C:\reportes\" + mExpediente)) File.Delete(@"C:\reportes\" + mExpediente);
                    }
                    catch
                    {
                        mExpediente = mExpediente.Replace(".pdf", "");
                        mExpediente = string.Format("{0}_2.pdf", mExpediente);

                        try
                        {
                            if (File.Exists(@"C:\reportes\" + mExpediente)) File.Delete(@"C:\reportes\" + mExpediente);
                        }
                        catch
                        {
                            mExpediente = mExpediente.Replace(".pdf", "");
                            mExpediente = string.Format("{0}_2.pdf", mExpediente);

                            try
                            {
                                if (File.Exists(@"C:\reportes\" + mExpediente)) File.Delete(@"C:\reportes\" + mExpediente);
                            }
                            catch
                            {
                                mExpediente = mExpediente.Replace(".pdf", "");
                                mExpediente = string.Format("{0}_2.pdf", mExpediente);
                                if (File.Exists(@"C:\reportes\" + mExpediente)) File.Delete(@"C:\reportes\" + mExpediente);
                            }
                        }
                    }

                    ReporteExpediente.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mExpediente);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mExpediente);
                    Response.WriteFile(@"C:\reportes\" + mExpediente);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorDespachos.Text = string.Format("Error al imprimir el expediente {0} {1}", ex.Message, m);
            }
        }

        protected void cbSerie_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                if (cbSerie.Value.ToString() == "")
                {
                    txtDespacho.Text = "";
                    return;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") 
                    ws.Url = Convert.ToString(Session["url"]);

                txtDespacho.Text = ws.NumeroDespachoF01(cbSerie.Value.ToString());
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al seleccionar el consecutivo. {0} {1}", ex.Message, m);
            }
        }

        protected void btnImprimirDespachosHoy_Click(object sender, EventArgs e)
        {
            string mMensaje = "";

            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                dsPuntoVenta ds = new dsPuntoVenta();
                var qDespachos = ws.DevuelveDespachoFactura(Convert.ToString(Session["Usuario"]));

                foreach (var item in qDespachos)
                {
                    DataSet dsInfo = new DataSet();
                    wsPuntoVenta.Despacho[] q = new wsPuntoVenta.Despacho[1];
                    wsPuntoVenta.DespachoLinea[] qLinea = new wsPuntoVenta.DespachoLinea[1];

                    mMensaje = ""; string mFactura = "";
                    if (!ws.DevuelveImpresionDespacho(item.Despacho, mFactura, ref mMensaje, ref q, ref qLinea, ref dsInfo))
                    {
                        lbErrorDespachos.Text = mMensaje;
                        return;
                    }

                    DataRow row = ds.Despacho.NewRow();
                    row["NoDespacho"] = q[0].NoDespacho;
                    row["Fecha"] = q[0].Fecha;
                    row["Cliente"] = q[0].Cliente;
                    row["Nombre"] = q[0].Nombre;
                    row["Direccion"] = q[0].Direccion;
                    row["Indicaciones"] = q[0].Indicaciones;
                    row["Telefonos"] = q[0].Telefonos;
                    row["FormaPago"] = q[0].FormaPago;
                    row["Tienda"] = q[0].Tienda;
                    row["Vendedor"] = q[0].Vendedor;
                    row["NombreVendedor"] = q[0].NombreVendedor;
                    row["Factura"] = q[0].Factura;
                    row["Observaciones"] = q[0].Observaciones;
                    row["Notas"] = q[0].Notas;
                    row["Transportista"] = q[0].Transportista;
                    row["NombreTransportista"] = q[0].NombreTransportista;
                    row["CemacoTienda"] = q[0].CemacoTienda;
                    row["CemacoFechaVenta"] = q[0].CemacoFechaVenta;
                    row["CemacoCaja"] = q[0].CemacoCaja;
                    row["CemacoTransaccion"] = q[0].CemacoTransaccion;
                    row["OrdenCompra"] = q[0].OrdenCompra;
                    row["ObservacionesPedido"] = q[0].ObservacionesPedido;

                    if (dsInfo.Tables["Despacho"].Rows.Count > 0)
                        row["CemacoImagen"] = dsInfo.Tables["Despacho"].Rows[0]["CemacoImagen"];

                    ds.Despacho.Rows.Add(row);

                    for (int ii = 0; ii < qLinea.Count(); ii++)
                    {
                        DataRow rowDet = ds.DespachoLinea.NewRow();
                        rowDet["NoDespacho"] = qLinea[ii].NoDespacho;
                        rowDet["Articulo"] = qLinea[ii].Articulo;
                        rowDet["Cantidad"] = qLinea[ii].Cantidad;
                        rowDet["Descripcion"] = qLinea[ii].Descripcion;
                        rowDet["Bodega"] = qLinea[ii].Bodega;
                        rowDet["Localizacion"] = qLinea[ii].Localizacion;
                        ds.DespachoLinea.Rows.Add(rowDet);
                    }

                }

                using (ReportDocument reporte = new ReportDocument())
                {
                    string p = (Request.PhysicalApplicationPath + "reportes/rptDespachoPVF01Lote.rpt");

                    reporte.Load(p);
                    string mNombreDocumento = string.Format("Envios{0}{1}{2}{3}{4}.pdf", DateTime.Now.Date.Day < 10 ? "0" : "", DateTime.Now.Date.Day, DateTime.Now.Date.Month < 10 ? "0" : "", DateTime.Now.Date.Month, DateTime.Now.Date.Year);

                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                    reporte.SetDataSource(ds);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al imprimir los envíos de hoy. {0} {1} {2}", ex.Message, mMensaje, m);
                return;
            }
        }

        protected void btnImprimirDespacho_Click(object sender, EventArgs e)
        {
            string mMensaje = "";

            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DataSet dsInfo = new DataSet();
                wsPuntoVenta.Despacho[] q = new wsPuntoVenta.Despacho[1];
                wsPuntoVenta.DespachoLinea[] qLinea = new wsPuntoVenta.DespachoLinea[1];

                mMensaje = ""; string mFactura = "";
                if (!ws.DevuelveImpresionDespacho(txtDespacho.Text.Trim().ToUpper(), mFactura, ref mMensaje, ref q, ref qLinea, ref dsInfo))
                {
                    lbErrorDespachos.Text = mMensaje;
                    return;
                }

                dsPuntoVenta ds = new dsPuntoVenta();

                DataRow row = ds.Despacho.NewRow();
                row["NoDespacho"] = q[0].NoDespacho;
                row["Fecha"] = q[0].Fecha;
                row["Cliente"] = q[0].Cliente;
                row["Nombre"] = q[0].Nombre;
                row["Direccion"] = q[0].Direccion;
                row["Indicaciones"] = q[0].Indicaciones;
                row["Telefonos"] = q[0].Telefonos;
                row["FormaPago"] = q[0].FormaPago;
                row["Tienda"] = q[0].Tienda;
                row["Vendedor"] = q[0].Vendedor;
                row["NombreVendedor"] = q[0].NombreVendedor;
                row["Factura"] = q[0].Factura;
                row["Observaciones"] = q[0].Observaciones;
                row["Notas"] = q[0].Notas;
                row["Transportista"] = q[0].Transportista;
                row["NombreTransportista"] = q[0].NombreTransportista;
                row["CemacoTienda"] = q[0].CemacoTienda;
                row["CemacoFechaVenta"] = q[0].CemacoFechaVenta;
                row["CemacoCaja"] = q[0].CemacoCaja;
                row["CemacoTransaccion"] = q[0].CemacoTransaccion;
                row["OrdenCompra"] = q[0].OrdenCompra;
                row["ObservacionesPedido"] = q[0].ObservacionesPedido;

                if (dsInfo.Tables["Despacho"].Rows.Count > 0)
                    row["CemacoImagen"] = dsInfo.Tables["Despacho"].Rows[0]["CemacoImagen"];

                ds.Despacho.Rows.Add(row);

                for (int ii = 0; ii < qLinea.Count(); ii++)
                {
                    DataRow rowDet = ds.DespachoLinea.NewRow();
                    rowDet["NoDespacho"] = qLinea[ii].NoDespacho;
                    rowDet["Articulo"] = qLinea[ii].Articulo;
                    rowDet["Cantidad"] = qLinea[ii].Cantidad;
                    rowDet["Descripcion"] = qLinea[ii].Descripcion;
                    rowDet["Bodega"] = qLinea[ii].Bodega;
                    rowDet["Localizacion"] = qLinea[ii].Localizacion;
                    ds.DespachoLinea.Rows.Add(rowDet);
                }

                using (ReportDocument reporte = new ReportDocument())
                {
                    string p = (Request.PhysicalApplicationPath + "reportes/rptDespachoPVF01.rpt");
                    reporte.Load(p);

                    string mNombreDocumento = string.Format("{0}.pdf", txtDespacho.Text);

                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                    reporte.SetDataSource(ds);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al imprimir el envío. {0} {1} {2}", ex.Message, mMensaje, m);
                return;
            }
        }

        protected void lkExpediente_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                if (txtPedido.Text.Trim().Length == 0)
                {
                    lbErrorDespachos.Text = "Debe seleccionar una factura";
                    return;
                }

                if (txtTienda.Text == "F01")
                {
                    lbErrorDespachos.Text = "El expediente sólo está disponible para facturas de tiendas";
                    return;
                }

                ImprimirExpediente(txtPedido.Text);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al imprimir el expediente. {0} {1}", ex.Message, m);
                return;
            }
        }

        protected void lkVerFactura_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                if (txtPedido.Text.Trim().Length == 0)
                {
                    lbErrorDespachos.Text = "Debe seleccionar una factura";
                    return;
                }

                if (txtTienda.Text == "F01")
                {
                    lbErrorDespachos.Text = "Esta opción sólo está disponible para facturas de tiendas";
                    return;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                wsPuntoVenta.Factura[] q = new wsPuntoVenta.Factura[1];
                wsPuntoVenta.FacturaLinea[] qLinea = new wsPuntoVenta.FacturaLinea[1];

                string mMensaje = "";
                if (!ws.DevuelveImpresionFactura(txtFactura.Text, ref mMensaje, ref q, ref qLinea))
                {
                    lbErrorDespachos.Text = mMensaje;
                    return;
                }

                dsPuntoVenta ds = new dsPuntoVenta();

                DataRow row = ds.Factura.NewRow();
                row["NoFactura"] = q[0].NoFactura;
                row["Fecha"] = q[0].Fecha;
                row["Cliente"] = q[0].Cliente;
                row["Nombre"] = q[0].Nombre;
                row["Nit"] = q[0].Nit;
                row["Direccion"] = q[0].Direccion;
                row["Vendedor"] = q[0].Vendedor;
                row["Monto"] = q[0].Monto;
                row["MontoLetras"] = q[0].MontoLetras;
                ds.Factura.Rows.Add(row);

                for (int ii = 0; ii < qLinea.Count(); ii++)
                {
                    DataRow rowDet = ds.FacturaLinea.NewRow();
                    rowDet["NoFactura"] = qLinea[ii].NoFactura;
                    rowDet["Articulo"] = qLinea[ii].Articulo;
                    rowDet["Cantidad"] = qLinea[ii].Cantidad;
                    rowDet["Descripcion"] = qLinea[ii].Descripcion;
                    rowDet["PrecioUnitario"] = qLinea[ii].PrecioUnitario;
                    rowDet["PrecioTotal"] = qLinea[ii].PrecioTotal;
                    ds.FacturaLinea.Rows.Add(rowDet);
                }

                using (ReportDocument reporte = new ReportDocument())
                {
                    string mFormatoFactura = "rptFacturaPV.rpt";
                    if (Convert.ToString(Session["Tienda"]).Length == 3) mFormatoFactura = ws.DevuelveFormatoFactura(Convert.ToString(Session["Tienda"]), txtFactura.Text);

                    string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mFormatoFactura);
                    reporte.Load(p);

                    string mNombreDocumento = string.Format("{0}.pdf", txtFactura.Text);
                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                    reporte.SetDataSource(ds);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al re-imprimir la factura {0} {1}", ex.Message, m);
            }
        }

        protected void lkCliente_Click(object sender, EventArgs e)
        {
            if (txtPedido.Text.Trim().Length == 0)
            {
                lbErrorDespachos.Text = "Debe seleccionar una factura";
                return;
            }

            if (txtTienda.Text == "F01")
            {
                lbErrorDespachos.Text = "Esta opción sólo está disponible para facturas de tiendas";
                return;
            }

            string _open = "window.open('clientes.aspx', '_newtab');";
            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), _open, true);
        }

        protected void lkPedidos_Click(object sender, EventArgs e)
        {
            if (txtPedido.Text.Trim().Length == 0)
            {
                lbErrorDespachos.Text = "Debe seleccionar una factura";
                return;
            }

            if (txtTienda.Text == "F01")
            {
                lbErrorDespachos.Text = "Esta opción sólo está disponible para facturas de tiendas";
                return;
            }

            string _open = "window.open('pedidos.aspx', '_newtab');";
            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), _open, true);
        }

        protected void lkArticulo2_Click(object sender, EventArgs e)
        {

            lbPedido.Text = string.Format("RowIndex = {0}", gridFacturaDet.FocusedRowIndex.ToString());
            txtDireccion.Text = string.Format("{0} - {1}", Convert.ToString(gridFacturaDet.GetRowValues(gridFacturaDet.FocusedRowIndex, "Articulo")).Trim(), Convert.ToString(gridFacturaDet.GetRowValues(gridFacturaDet.FocusedRowIndex, "Descripcion")).Trim());
        }

        protected void hypArticulo2_Init(object sender, EventArgs e)
        {
            //ASPxHyperLink link = (ASPxHyperLink)sender;
            //GridViewDataItemTemplateContainer templateContainer = (GridViewDataItemTemplateContainer)link.NamingContainer;

            //int rowVisibleIndex = templateContainer.VisibleIndex;
            //string mArticulo = templateContainer.Grid.GetRowValues(rowVisibleIndex, "Articulo").ToString();
            //string mDescripcion = templateContainer.Grid.GetRowValues(rowVisibleIndex, "Descripcion").ToString();
            //string mLinea = templateContainer.Grid.GetRowValues(rowVisibleIndex, "Linea").ToString();

            //txtDireccion.Text = string.Format("{0} - {1} - Linea: {2}", mArticulo, mDescripcion, mLinea);
        }

        void CargarDespachos(string tipo)
        {
            try
            {
                lbInfoTransportista.Text = "";
                lbErrorTransportista.Text = "";

                gridDespachos.Visible = false;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DateTime mFechaInicial = Convert.ToDateTime(fechaInicialT.Value);
                DateTime mFechaFinal = Convert.ToDateTime(fechaFinalT.Value);

                DataSet ds = new DataSet(); string mMensaje = "";
                ds = ws.DocumentosPorDespacharAprobados(ref mMensaje, mFechaInicial, mFechaFinal, txtCliente.Text.Trim().ToUpper(), Convert.ToString(tienda.Value), tipo, Convert.ToString(Session["Usuario"]), false);

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorDespachos.Text = mMensaje;
                    return;
                }

                if (ds.Tables["documentos"].Rows.Count > 0)
                {
                    gridFacturas.Visible = true;
                    gridFacturas.DataSource = ds;
                    gridFacturas.DataMember = "documentos";
                    gridFacturas.DataBind();
                    gridFacturas.FocusedRowIndex = -1;
                    gridFacturas.SettingsPager.PageSize = 500;

                    Session["dsFacturas"] = ds;
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al mostrar los documentos. {0} {1}", ex.Message, m);
            }
        }

        void UpdateDespacho(string despacho, DateTime fechaEntrega, Int32 transportista, string cliente, Int32 articulos)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsDespachos"];

                ds.Tables["documentos"].Rows.Find(despacho)["FechaEntrega"] = fechaEntrega;
                ds.Tables["documentos"].Rows.Find(despacho)["Transportista"] = transportista;
                ds.Tables["documentos"].Rows.Find(despacho)["Cliente"] = cliente;
                ds.Tables["documentos"].Rows.Find(despacho)["Articulos"] = articulos;
                Session["dsArticulos"] = ds;

                gridDespachos.DataSource = ds;
                gridDespachos.DataMember = "documentos";
                gridDespachos.DataBind();
                gridDespachos.FocusedRowIndex = -1;
                gridDespachos.SettingsPager.PageSize = 500;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorTransportista.Text = string.Format("Error al actualizar los datos UPDATE {0} {1}", ex.Message, m);
            }
        }

        protected void gridDespachos_BatchUpdate(object sender, ASPxDataBatchUpdateEventArgs e)
        {
            foreach (var args in e.UpdateValues)
                UpdateDespacho(args.Keys["Despacho"].ToString()
                    , Convert.ToDateTime(args.NewValues["FechaEntrega"])
                    , Convert.ToInt32(args.NewValues["Transportista"])
                    , args.NewValues["Cliente"].ToString()
                    , Convert.ToInt32(args.NewValues["Articulos"]));

            e.Handled = true;
        }

        protected void gridDespachos_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {

        }

        protected void gridDespachos_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            UpdateDespacho(e.Keys["Despacho"].ToString()
                , Convert.ToDateTime(e.NewValues["FechaEntrega"])
                , Convert.ToInt32(e.NewValues["Cantidad"])
                , e.NewValues["Cliente"].ToString()
                , Convert.ToInt32(e.NewValues["Articulos"]));
            CancelEditingDespacho(e);
        }

        protected void gridDespachos_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == ColumnCommandButtonType.Update || e.ButtonType == ColumnCommandButtonType.Cancel) e.Visible = false;
        }

        protected void gridDespachos_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.Name == "Cliente") e.Cell.ToolTip = "Marque esta casilla si el cliente confirmó de recibida su mercadería";
        }

        protected void gridDespachos_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {

        }

        protected void CancelEditingDespacho(CancelEventArgs e)
        {
            e.Cancel = true;
            gridDespachos.CancelEdit();
        }

        protected void btnArticulo2_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                var btn = (sender as ASPxButton);
                var nc = btn.NamingContainer as GridViewDataItemTemplateContainer;

                string mPedido = DataBinder.Eval(nc.DataItem, "Pedido").ToString();
                string mLinea = DataBinder.Eval(nc.DataItem, "Linea").ToString();
                string mFactura = DataBinder.Eval(nc.DataItem, "Factura").ToString();
                string mArticulo = DataBinder.Eval(nc.DataItem, "Articulo").ToString();
                string mTipo = DataBinder.Eval(nc.DataItem, "Tipo").ToString();

                string mArticuloNuevo = ""; string mDescripcionNueva = ""; string mTooltip = "";
                if (!ws.CambiarColorDespachoF01(ref mMensaje, mPedido, mFactura, mLinea, mArticulo, ref mArticuloNuevo, ref mDescripcionNueva, ref mTooltip))
                {
                    lbErrorDespachos.Text = mMensaje;
                    return;
                }

                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsArticulos"];

                ds.Tables["documento"].Rows.Find(mLinea)["Articulo"] = mArticuloNuevo;
                ds.Tables["documento"].Rows.Find(mLinea)["Articulo2"] = mArticuloNuevo;
                ds.Tables["documento"].Rows.Find(mLinea)["Descripcion"] = mDescripcionNueva;
                ds.Tables["documento"].Rows.Find(mLinea)["Tooltip"] = mTooltip;

                if (mTipo == "C")
                {
                    ds.Tables["documento"].Rows.Find(mLinea)["Articulo2"] = string.Format("&nbsp;&nbsp;&nbsp;&nbsp;{0}", mArticuloNuevo);
                    ds.Tables["documento"].Rows.Find(mLinea)["Descripcion"] = string.Format("&nbsp;&nbsp;&nbsp;&nbsp;{0}", mDescripcionNueva);
                }

                Session["dsArticulos"] = ds;

                gridFacturaDet.DataSource = ds;
                gridFacturaDet.DataMember = "documento";
                gridFacturaDet.DataBind();
                gridFacturaDet.FocusedRowIndex = -1;
                gridFacturaDet.SettingsPager.PageSize = 100;

                lbInfoDespachos.Text = mMensaje;
                fechaInicial.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al cambiar el color. {0} {1}", ex.Message, m);
            }
        }

        protected void btnArticuloBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                var btn = (sender as ASPxButton);
                var nc = btn.NamingContainer as GridViewDataItemTemplateContainer;

                string mLinea = txtLinea.Text;
                string mArticulo = DataBinder.Eval(nc.DataItem, "Articulo").ToString();
                string mDescripcion = DataBinder.Eval(nc.DataItem, "Descripcion").ToString();
                string mTooltip = DataBinder.Eval(nc.DataItem, "Tooltip").ToString();

                if (mLinea == "Traspaso")
                {
                    lbInfoTraspasos.Text = "";
                    lbErrorTraspasos.Text = "";

                    txtArticulo.Text = mArticulo;
                    txtArticuloDescripcion.Text = mDescripcion;
                    txtArticuloDescripcion.ToolTip = mTooltip;
                    cbBodegaOrigen.Focus();
                }
                else
                {
                    if (!ws.SolicitudCambiarArticuloDespachoF01(ref mMensaje, txtPedido.Text, txtFactura.Text, txtLinea.Text, mArticulo, mDescripcion, Convert.ToString(Session["Usuario"]), txtObservacionesCambio.Text))
                    {
                        lbErrorDespachos.Text = mMensaje;
                        return;
                    }

                    lbInfoDespachos.Text = mMensaje;
                    fechaInicial.Focus();
                }

                popupArticulos.ShowOnPageLoad = false;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al seleccionar el artículo del listado para cambiar. {0} {1}", ex.Message, m);
            }
        }

        protected void btnDescripcion_Init(object sender, EventArgs e)
        {
            var btn = sender as ASPxButton;
            var container = btn.NamingContainer as GridViewDataItemTemplateContainer;
            btn.ClientSideEvents.Click = string.Format("function(s,e){{ onDescriptionClick(s,e,{0}); }}", container.KeyValue ?? -1);
        }

        protected void gridArticulosBuscar_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.VisibleIndex >= 0) e.Cell.ToolTip = e.GetValue("Tooltip").ToString();
        }

        /// <summary>
        /// Este método recibe el tipo de envíos a cargar
        /// </summary>
        /// <param name="tipo">"F" por fechas, "T" por tienda, "R" por transportista, "D" por despacho específico</param>
        void CargarEnvios(string tipo)
        {
            try
            {
                lbInfoRutas.Text = "";
                lbErrorRutas.Text = "";

                if (tipo == "D" && txtDespachoTraspaso.Text.Trim().Length == 0)
                {
                    lbErrorRutas.Text = "Debe ingresar un número de despacho o traspaso";
                    txtDespachoTraspaso.Focus();
                    return;
                }

                gridRutas.Visible = false;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DateTime mFechaInicial = Convert.ToDateTime(fechaInicialR.Value);
                DateTime mFechaFinal = Convert.ToDateTime(fechaFinalR.Value);

                DataSet ds = new DataSet(); string mMensaje = "";
                ds = ws.DocumentosDespachados(ref mMensaje, mFechaInicial, mFechaFinal, Convert.ToString(tienda.Value), tipo, Convert.ToString(transportistaRutas.Value), Convert.ToString(Session["Usuario"]), txtDespachoTraspaso.Text);

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorRutas.Text = mMensaje;
                    return;
                }

                if (ds.Tables["documentos"].Rows.Count > 0)
                {
                    gridRutas.Visible = true;
                    gridRutas.DataSource = ds;
                    gridRutas.DataMember = "documentos";
                    gridRutas.DataBind();
                    gridRutas.FocusedRowIndex = -1;
                    gridRutas.SettingsPager.PageSize = 500;
                    gridRutas.DataColumns["NombreCliente"].Settings.AutoFilterCondition = AutoFilterCondition.Contains;

                    foreach (GridViewDataColumn item in gridRutas.GetGroupedColumns())
                        item.UnGroup();

                    gridRutas.GroupBy(gridRutas.Columns["NombreTransportista"], 0);

                    Session["dsRutas"] = ds;
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al mostrar los documentos de ruta. {0} {1}", ex.Message, m);
            }
        }

        protected void gridRutasDet_Load(object sender, EventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            var mDespacho = grid.GetMasterRowKeyValue();

            DataSet dsRutas = new DataSet();
            dsRutas = (DataSet)Session["dsRutas"];

            DataView dv = new DataView(dsRutas.Tables["documentosDet"]);

            dv.RowFilter = string.Format("Despacho = '{0}'", mDespacho.ToString());

            grid.DataSource = dv;
            grid.DataBind();
        }

        protected void lkSeleccionarFechasR_Click(object sender, EventArgs e)
        {
            CargarEnvios("F");
        }

        protected void lkSeleccionarTiendaR_Click(object sender, EventArgs e)
        {
            CargarEnvios("T");
        }

        protected void lkTransportistaRutas_Click(object sender, EventArgs e)
        {
            CargarEnvios("R");
        }

        protected void lkDespachoTraspaso_Click(object sender, EventArgs e)
        {
            CargarEnvios("D");
        }

        protected void gridRutas_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {

        }

        protected void gridRutas_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {

        }

        protected void gridRutas_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {

        }

        protected void gridRutas_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {

        }

        protected void ToolbarExport_ItemClick(object source, ExportItemClickEventArgs e)
        {
            try
            {
                lbInfoRutas.Text = "";
                lbErrorRutas.Text = "";

                if (Session["dsRutas"] == null)
                {
                    lbErrorRutas.Text = "Debe seleccionar documentos de ruta.";
                    return;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DataSet dsRutas = new DataSet();
                dsRutas = (DataSet)Session["dsRutas"];

                if (dsRutas.Tables["documentos"].Rows.Count == 0)
                {
                    lbErrorRutas.Text = "Debe seleccionar documentos de ruta.";
                    return;
                }

                gridExport.PageHeader.Center = string.Format("Ruta de transporte del {0}{1}/{2}{3}/{4} al {5}{6}/{7}{8}/{9}", Convert.ToDateTime(fechaInicialR.Value).Day < 10 ? "0" : "", Convert.ToDateTime(fechaInicialR.Value).Day, Convert.ToDateTime(fechaInicialR.Value).Month < 10 ? "0" : "", Convert.ToDateTime(fechaInicialR.Value).Month, Convert.ToDateTime(fechaInicialR.Value).Year, Convert.ToDateTime(fechaFinalR.Value).Day < 10 ? "0" : "", Convert.ToDateTime(fechaFinalR.Value).Day, Convert.ToDateTime(fechaFinalR.Value).Month < 10 ? "0" : "", Convert.ToDateTime(fechaFinalR.Value).Month, Convert.ToDateTime(fechaFinalR.Value).Year);
                //gridExport.PageHeader.Center = string.Format("Ruta de transporte del {0}{1}/{2}{3}/{4} al {5}{6}/{7}{8}/{9} {10}Transportista:{11}", Convert.ToDateTime(fechaInicialR.Value).Day < 10 ? "0" : "", Convert.ToDateTime(fechaInicialR.Value).Day, Convert.ToDateTime(fechaInicialR.Value).Month < 10 ? "0" : "", Convert.ToDateTime(fechaInicialR.Value).Month, Convert.ToDateTime(fechaInicialR.Value).Year, Convert.ToDateTime(fechaFinalR.Value).Day < 10 ? "0" : "", Convert.ToDateTime(fechaFinalR.Value).Day, Convert.ToDateTime(fechaFinalR.Value).Month < 10 ? "0" : "", Convert.ToDateTime(fechaFinalR.Value).Month, Convert.ToDateTime(fechaFinalR.Value).Year, System.Environment.NewLine, transportistaRutas.Value.ToString());
                //gridExport.ReportFooter = string.Format("{0}{0}{0}Firma:________________________________________", System.Environment.NewLine);

                switch (e.ExportType)
                {
                    case DemoExportFormat.Pdf:
                        DataSet ds = new DataSet();
                        ds = (DataSet)Session["dsRutas"];

                        DataSet dsRuta = new DataSet();
                        DataTable dtRuta = new DataTable("Ruta");
                        DataTable dtRutaDet = new DataTable("RutaDet");

                        dtRuta = ds.Tables["documentos"].Copy();
                        dtRutaDet = ds.Tables["documentosDet"].Copy();

                        dsRuta.Tables.Add(dtRuta);
                        dsRuta.Tables.Add(dtRutaDet);

                        dsRuta.Tables["documentos"].TableName = "Ruta";
                        dsRuta.Tables["documentosDet"].TableName = "RutaDet";

                        dsRuta.Tables["RutaDet"].Clear();

                        for (int ii = 0; ii < ds.Tables["documentosDet"].Rows.Count; ii++)
                        {
                            if (Convert.ToInt32(ds.Tables["documentosDet"].Rows[ii]["Cantidad"]) >= 0)
                            {
                                DataRow row = dsRuta.Tables["RutaDet"].NewRow();
                                for (int jj = 0; jj < ds.Tables["documentosDet"].Columns.Count; jj++)
                                {
                                    row[jj] = ds.Tables["documentosDet"].Rows[ii][jj];
                                }
                                dsRuta.Tables["RutaDet"].Rows.Add(row);
                            }
                        }

                        using (ReportDocument reporte = new ReportDocument())
                        {
                            string mNombreDocumento = string.Format("Ruta_{0}{1}{2}{3}{4}.pdf", Convert.ToDateTime(fechaInicialR.Value).Day < 10 ? "0" : "", Convert.ToDateTime(fechaInicialR.Value).Day, Convert.ToDateTime(fechaInicialR.Value).Month < 10 ? "0" : "", Convert.ToDateTime(fechaInicialR.Value).Month, Convert.ToDateTime(fechaInicialR.Value).Year);
                            string mFormato = "rptRuta.rpt";
                            string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mFormato);

                            reporte.Load(p);
                            reporte.SetDataSource(dsRuta);
                            reporte.SetParameterValue("titulo", string.Format("Fecha: {0}{1}/{2}{3}/{4}", Convert.ToDateTime(fechaInicialR.Value).Day < 10 ? "0" : "", Convert.ToDateTime(fechaInicialR.Value).Day, Convert.ToDateTime(fechaInicialR.Value).Month < 10 ? "0" : "", Convert.ToDateTime(fechaInicialR.Value).Month, Convert.ToDateTime(fechaInicialR.Value).Year));
                            reporte.SetParameterValue("usuario", Convert.ToString(Session["Usuario"]));

                            if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");

                            try
                            {
                                if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                            }
                            catch
                            {
                                try
                                {
                                    mNombreDocumento = mNombreDocumento.Replace(".pdf", "_1.pdf");
                                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                                }
                                catch
                                {
                                    try
                                    {
                                        mNombreDocumento = mNombreDocumento.Replace(".pdf", "_1.pdf");
                                        if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                                    }
                                    catch
                                    {
                                        mNombreDocumento = mNombreDocumento.Replace(".pdf", "_1.pdf");
                                        if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                                    }
                                }
                            }

                            reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                            Response.Clear();
                            Response.ContentType = "application/pdf";

                            Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                            Response.WriteFile(@"C:\reportes\" + mNombreDocumento);

                            HttpContext.Current.ApplicationInstance.CompleteRequest();
                        }

                        break;
                    case DemoExportFormat.Xls:
                        //gridExport.WriteXlsToResponse(new XlsExportOptionsEx { ExportType = ExportType.WYSIWYG });

                        if (!ws.EnviarCorreoRutaTiendas(ref mMensaje, Convert.ToString(Session["Usuario"]), dsRutas))
                        {
                            lbErrorRutas.Text = mMensaje;
                            return;
                        }

                        lbInfoRutas.Text = mMensaje;

                        break;
                    case DemoExportFormat.Xlsx:
                        gridExport.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
                        break;
                    case DemoExportFormat.Rtf:
                        gridExport.WriteRtfToResponse();
                        break;
                    case DemoExportFormat.Csv:
                        gridExport.WriteCsvToResponse(new CsvExportOptionsEx() { ExportType = ExportType.WYSIWYG });
                        break;
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorRutas.Text = string.Format("Error al exportar los documentos de ruta. {0} {1}", ex.Message, m);
            }

        }

        protected void WriteToResponse(string fileName, bool saveAsFile, string fileFormat, System.IO.MemoryStream stream)
        {
            if (Page == null || Page.Response == null) return;
            string disposition = saveAsFile ? "attachment" : "inline";
            Page.Response.Clear();
            Page.Response.Buffer = false;
            Page.Response.AppendHeader("Content-Type", string.Format("application/{0}", fileFormat));
            Page.Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Page.Response.AppendHeader("Content-Disposition", string.Format("{0}; filename={1}.{2}", disposition, fileName, fileFormat));
            Page.Response.BinaryWrite(stream.GetBuffer());
            Page.Response.End();
        }

        private object[] FindGroupValues(ASPxGridView grid)
        {
            grid.DataBind();
            string column = grid.GetGroupedColumns()[0].FieldName;
            ArrayList list = new ArrayList();
            for (int i = 0; i < grid.VisibleRowCount; i++)
            {
                if (grid.GetRowLevel(i) == 0)
                {
                    list.Add(grid.GetRowValues(i, column));
                }
            }
            return list.ToArray();
        }

        protected void gridExport_RenderBrick(object sender, ASPxGridViewExportRenderingEventArgs e)
        {
            if (e.RowType != GridViewRowType.Header) e.BrickStyle.BorderWidth = 0;
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            txtFactura.Text = "";
            txtPedido.Text = "";
            txtTienda.Text = "";
            txtNombreCliente.Text = "";
            txtDireccion.Text = "";
            txtTelefonos.Text = "";
            txtObservaciones.Text = "";
            txtPersonaRecibe.Text = "";
            txtVendedor.Text = "";
            txtObservacionesVendedor.Text = "";

            int mDias = Convert.ToInt32(ViewState["dias"]);
            fechaEntrega.Value = DateTime.Now.Date.AddDays(mDias);

            txtFactura.ToolTip = "Ingrese el número de factura a buscar";
            lbPedido.Text = "";
            transportista.Value = 0;
            lbAyudaTransportista.Text = "";
            gridFacturaDet.Visible = false;
        }

        protected void lkSeleccionarFechasT_Click(object sender, EventArgs e)
        {
            CargarDespachados("F");
        }

        protected void lkSeleccionarTiendaT_Click(object sender, EventArgs e)
        {
            CargarDespachados("T");
        }

        protected void lkTransportistaT_Click(object sender, EventArgs e)
        {
            CargarDespachados("R");
        }

        protected void lkDespachoTraspasoT_Click(object sender, EventArgs e)
        {
            CargarDespachados("D");
        }

        void CargarDespachados(string tipo)
        {
            try
            {
                lbInfoTransportista.Text = "";
                lbErrorTransportista.Text = "";

                if (tipo == "D" && txtDespachoTraspasoT.Text.Trim().Length == 0)
                {
                    lbErrorTransportista.Text = "Debe ingresar un número de despacho o traspaso";
                    txtDespachoTraspasoT.Focus();
                    return;
                }

                gridDespachos.Visible = false;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DateTime mFechaInicial = Convert.ToDateTime(fechaInicialT.Value);
                DateTime mFechaFinal = Convert.ToDateTime(fechaFinalT.Value);

                DataSet ds = new DataSet(); string mMensaje = "";
                ds = ws.DocumentosDespachadosModificar(ref mMensaje, mFechaInicial, mFechaFinal, Convert.ToString(tiendaTransporte.Value), tipo, Convert.ToString(transportistaT.Value), "N", Convert.ToString(Session["Usuario"]), txtDespachoTraspasoT.Text.Trim());

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorTransportista.Text = mMensaje;
                    return;
                }

                if (ds.Tables["documentos"].Rows.Count > 0)
                {
                    gridDespachos.Visible = true;
                    gridDespachos.DataSource = ds;
                    gridDespachos.DataMember = "documentos";
                    gridDespachos.DataBind();
                    gridDespachos.FocusedRowIndex = -1;
                    gridDespachos.SettingsPager.PageSize = 500;
                    gridDespachos.DataColumns["NombreCliente"].Settings.AutoFilterCondition = AutoFilterCondition.Contains;

                    if (tipo == "P")
                    {
                        gridDespachos.DataColumns["Despacho"].Caption = "Traspaso";
                        gridDespachos.DataColumns["Cliente"].ReadOnly = true;
                        gridDespachos.DataColumns["Articulos"].ReadOnly = true;
                    }
                    else
                    {
                        gridDespachos.Columns["Despacho"].Caption = "Despacho";
                        gridDespachos.DataColumns["Cliente"].ReadOnly = false;
                        gridDespachos.DataColumns["Articulos"].ReadOnly = false;
                    }

                    Session["dsDespachos"] = ds;
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorTransportista.Text = string.Format("Error al mostrar los documentos despachados. {0} {1}", ex.Message, m);
            }

        }

        protected void lkTraspasos_Click(object sender, EventArgs e)
        {
            CargarDespachados("P");
        }

        protected void lkActualizarTransportista_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoTransportista.Text = "";
                lbErrorTransportista.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsDespachos"];

                if (!ws.ActualizarTransportista(ref mMensaje, Convert.ToString(Session["Usuario"]), ds))
                {
                    lbErrorTransportista.Text = mMensaje;
                    return;
                }

                gridDespachos.Visible = false;
                lbInfoTransportista.Text = mMensaje;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorTransportista.Text = string.Format("Error al actualizar los transportistas y aceptación de clientes. {0} {1}", ex.Message, m);
            }
        }

        void CargarPedidos(string tipo)
        {
            try
            {
                lbErrorFacturar.Text = "";
                lbInfoFacturar.Text = "";

                gridPedidos.Visible = false;
                gridPedidoDet.Visible = false;

                txtNombreClientePedido.Text = "";
                txtTelefonosClientePedido.Text = "";
                txtDireccionPedido.Text = "";


                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DateTime mFechaInicial = Convert.ToDateTime(fechaInicial.Value);
                DateTime mFechaFinal = Convert.ToDateTime(fechaFinal.Value);

                if (tipo == "E")
                {
                    mFechaInicial = Convert.ToDateTime(fechaInicialPedidos.Value);
                    mFechaFinal = Convert.ToDateTime(fechaFinalPedidos.Value);
                }

                DataSet ds = new DataSet(); string mMensaje = "";
                ds = ws.PedidosPorFacturar(ref mMensaje, mFechaInicial, mFechaFinal, txtClientePedido.Text.Trim().ToUpper(), tipo);

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorFacturar.Text = mMensaje;
                    return;
                }

                if (ds.Tables["documentos"].Rows.Count > 0)
                {
                    gridPedidos.Visible = true;
                    gridPedidos.DataSource = ds;
                    gridPedidos.DataMember = "documentos";
                    gridPedidos.DataBind();
                    gridPedidos.FocusedRowIndex = -1;
                    gridPedidos.SettingsPager.PageSize = 500;

                    Session["dsPedidos"] = ds;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorFacturar.Text = string.Format("Error cargando pedidos. {0} {1}", ex.Message, m);
            }
        }

        protected void lkSeleccionarFechasPedidos_Click(object sender, EventArgs e)
        {
            CargarPedidos("E");
        }

        protected void lkSeleccionarClientePedido_Click(object sender, EventArgs e)
        {
            CargarPedidos("C");
        }

        protected void lkCargarPedido_Click(object sender, EventArgs e)
        {
            CargarPedido();
        }

        protected void txtClientePedido_TextChanged(object sender, EventArgs e)
        {
            CargarPedidos("C");
        }

        protected void CancelEditingPedidoDet(CancelEventArgs e)
        {
            e.Cancel = true;
            gridPedidoDet.CancelEdit();
        }

        void UpdatePedido(string linea, string bodega, string cantidad, string localizacion)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsArticulosPedido"];

                ds.Tables["documento"].Rows.Find(linea)["Bodega"] = bodega;
                ds.Tables["documento"].Rows.Find(linea)["Cantidad"] = Convert.ToInt32(cantidad);
                ds.Tables["documento"].Rows.Find(linea)["Localizacion"] = localizacion;
                Session["dsArticulosPedido"] = ds;

                gridPedidoDet.DataSource = ds;
                gridPedidoDet.DataMember = "documento";
                gridPedidoDet.DataBind();
                gridPedidoDet.FocusedRowIndex = -1;
                gridPedidoDet.SettingsPager.PageSize = 100;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorFacturar.Text = string.Format("Error al actualizar los datos UPDATE {0} {1}", ex.Message, m);
            }
        }

        protected void gridPedidoDet_BatchUpdate(object sender, ASPxDataBatchUpdateEventArgs e)
        {
            foreach (var args in e.UpdateValues)
                UpdatePedido(args.Keys["Linea"].ToString(), args.NewValues["Bodega"].ToString(), args.NewValues["Cantidad"].ToString(), args.NewValues["Localizacion"].ToString());

            e.Handled = true;
        }

        protected void gridPedidoDet_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            if (e.Column.FieldName == "Bodega")
            {
                var q = ws.DevuelveTiendasTodas();

                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                cmb.DataSource = q;
                cmb.ValueField = "Tienda";
                cmb.ValueType = typeof(System.String);
                cmb.TextField = "Tienda";
                cmb.DataBindItems();
            }

            if (e.Column.FieldName == "Localizacion")
            {
                DataSet ds = new DataSet();
                ds = ws.Localizaciones();

                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                cmb.DataSource = ds.Tables["Localizaciones"];
                cmb.ValueField = "Localizacion";
                cmb.ValueType = typeof(System.String);
                cmb.TextField = "Localizacion";
                cmb.DataBindItems();
            }
        }

        protected void gridPedidoDet_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == ColumnCommandButtonType.Update || e.ButtonType == ColumnCommandButtonType.Cancel) e.Visible = false;
        }

        protected void gridPedidoDet_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.VisibleIndex >= 0) e.Cell.ToolTip = e.GetValue("Tooltip").ToString();
        }

        protected void gridPedidoDet_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (Convert.ToInt32(e.GetValue("Existencias")) == 0 && Convert.ToString(e.GetValue("Tipo")) != "K") e.Row.ForeColor = System.Drawing.Color.Red;
            if (!Convert.ToString(e.GetValue("Tooltip")).Contains(Convert.ToString(e.GetValue("Bodega"))) && Convert.ToString(e.GetValue("Tipo")) != "K") e.Row.ForeColor = System.Drawing.Color.Red;
        }

        protected void gridPedidoDet_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            UpdatePedido(e.Keys["Linea"].ToString(), e.NewValues["Bodega"].ToString(), e.NewValues["Cantidad"].ToString(), e.NewValues["Localizacion"].ToString());
            CancelEditingFacturaDet(e);
        }

        protected void gridPedidos_FocusedRowChanged(object sender, EventArgs e)
        {
            if (!IsPostBack) return;
            if (gridPedidos.FocusedRowIndex == -1) return;
            if (txtPedidoFactura.Text.Trim().Length > 0) return;
            if (Convert.ToString(gridPedidos.GetRowValues(gridPedidos.FocusedRowIndex, "Pedido")).Trim().Length == 0) return;
            if (txtPedidoFactura.Text == Convert.ToString(gridPedidos.GetRowValues(gridPedidos.FocusedRowIndex, "Pedido"))) return;

            txtPedidoFactura.Text = Convert.ToString(gridPedidos.GetRowValues(gridPedidos.FocusedRowIndex, "Pedido"));
            CargarPedido();
        }

        void CargarPedido()
        {
            try
            {
                lbErrorFacturar.Text = "";
                lbInfoFacturar.Text = "";

                if (txtPedidoFactura.Text.Trim().Length == 0) return;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DataSet ds = new DataSet(); string mMensaje = "";
                ds = ws.PedidoPorFacturar(ref mMensaje, txtPedidoFactura.Text.Trim().ToUpper());

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorFacturar.Text = mMensaje;
                    return;
                }

                if (ds.Tables["documento"].Rows.Count > 0)
                {
                    gridPedidoDet.Visible = true;
                    gridPedidoDet.DataSource = ds;
                    gridPedidoDet.DataMember = "documento";
                    gridPedidoDet.DataBind();
                    gridPedidoDet.FocusedRowIndex = -1;
                    gridPedidoDet.SettingsPager.PageSize = 100;

                    txtNombreClientePedido.Text = string.Format("{0} - {1}", Convert.ToString(ds.Tables["documento"].Rows[0]["Cliente"]), Convert.ToString(ds.Tables["documento"].Rows[0]["NombreCliente"]));
                    txtTelefonosClientePedido.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Telefonos"]);
                    txtDireccionPedido.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Direccion"]);
                    fechaEntregaPedido.Value = Convert.ToDateTime(ds.Tables["documento"].Rows[0]["FechaEntrega"]);
                }

                Session["dsArticulosPedido"] = ds;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorFacturar.Text = string.Format("Error al mostrar el pedido. {0} {1}", ex.Message, m);
            }
        }

        protected void btnFacturar_Click(object sender, EventArgs e)
        {
            try
            {
                lbErrorFacturar.Text = "";
                lbInfoFacturar.Text = "";

                if (txtPedidoFactura.Text.Trim().Length == 0) return;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DataSet ds = new DataSet(); string mMensaje = "";
                ds = (DataSet)Session["dsArticulosPedido"];

                string mFactura = "";
                if (!ws.FacturarPedido(ref mMensaje, Convert.ToString(Session["Usuario"]), txtPedidoFactura.Text.Trim().ToUpper(), Convert.ToDateTime(fechaEntregaPedido.Value), ds, ref mFactura, "A", "", DateTime.Now.Date))
                {
                    lbErrorFacturar.Text = mMensaje;
                    return;
                }

                gridPedidoDet.Visible = false;

                DataSet dsPedidos = new DataSet();
                dsPedidos = (DataSet)Session["dsPedidos"];

                int mSiguiente = 0;

                try
                {
                    for (int ii = 0; ii < dsPedidos.Tables["documentos"].Rows.Count; ii++)
                    {
                        if (txtPedidoFactura.Text.Trim().ToUpper() == Convert.ToString(dsPedidos.Tables["documentos"].Rows[ii]["Pedido"])) mSiguiente = ii;
                    }
                }
                catch
                {
                    //Nothing
                }

                dsPedidos.Tables["documentos"].Rows.Find(txtPedidoFactura.Text.Trim().ToUpper()).Delete();
                dsPedidos.AcceptChanges();

                Session["dsPedidos"] = dsPedidos;

                txtPedidoFactura.Text = "";
                txtNombreClientePedido.Text = "";
                txtTelefonosClientePedido.Text = "";
                txtDireccionPedido.Text = "";

                gridPedidos.DataSource = dsPedidos;
                gridPedidos.DataMember = "documentos";
                gridPedidos.DataBind();
                gridPedidos.SettingsPager.PageSize = 500;
                gridPedidos.FocusedRowIndex = -1;

                try
                {
                    txtPedidoFactura.Text = Convert.ToString(dsPedidos.Tables["documentos"].Rows[mSiguiente]["Pedido"]);
                    CargarPedido();
                }
                catch
                {
                    //Nothing
                }

                lbInfoFacturar.Text = mMensaje;

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorFacturar.Text = string.Format("Error facturando. {0} {1}", ex.Message, m);
            }
        }

        protected void btnLimpiarPedido_Click(object sender, EventArgs e)
        {
            txtPedidoFactura.Text = "";
            txtNombreClientePedido.Text = "";
            txtTelefonosClientePedido.Text = "";
            txtDireccionPedido.Text = "";

            int mDias = Convert.ToInt32(ViewState["dias"]);
            fechaEntrega.Value = DateTime.Now.Date.AddDays(mDias);

            gridPedidoDet.Visible = false;
        }

        protected void gridPedidos_CustomButtonInitialize(object sender, ASPxGridViewCustomButtonEventArgs e)
        {

        }

        protected void gridFacturaDet_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
        {

        }

        List<Clases.Articulos> ConsultaArticulo(string articulo)
        {
            WebRequest request = WebRequest.Create(string.Format("{0}/articulo/{1}", Convert.ToString(Session["UrlRestServices"]), articulo.Trim().ToUpper()));

            request.Method = "GET";
            request.ContentType = "application/json";

            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            List<Clases.Articulos> mArticulos = new List<Clases.Articulos>();
            mArticulos = JsonConvert.DeserializeObject<List<Clases.Articulos>>(responseString);

            return mArticulos;
        }

        protected void txtArticulo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lbInfoTraspasos.Text = "";
                lbErrorTraspasos.Text = "";

                txtArticuloDescripcion.Text = "";
                if (txtArticulo.Text.Trim().Length <= 2) return;

                List<Clases.Articulos> mArticulos = ConsultaArticulo(txtArticulo.Text);

                if (mArticulos.Count() == 0)
                {
                    lbErrorTraspasos.Text = "No se encontraron artículos";
                    txtArticulo.Text = "";
                    txtArticulo.Focus();
                    return;
                }

                if (mArticulos.Count() == 1)
                {
                    txtArticulo.Text = mArticulos[0].Articulo;
                    txtArticuloDescripcion.Text = mArticulos[0].Descripcion;
                    txtArticuloDescripcion.ToolTip = mArticulos[0].Tooltip;
                    cbBodegaOrigen.Focus();
                }
                else
                {
                    txtArticulo.Text = "";
                    lbErrorTraspasos.Text = "Utilice la ayuda de articulos";
                    btnAyudaArticulos.Focus();
                }
            }
            catch (Exception ex)
            {
                lbErrorTraspasos.Text = CatchClass.ExMessage(ex, "bodega", "txtArticulo_TextChanged");
            }
        }

        protected void btnAgregarArticulo_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoTraspasos.Text = "";
                lbErrorTraspasos.Text = "";

                if (txtArticulo.Text.Trim().Length == 0)
                {
                    lbErrorTraspasos.Text = "Debe seleccionar un artículo";
                    txtArticulo.Text = "";
                    txtArticulo.Focus();
                    return;
                }
                if (txtArticuloDescripcion.Text.Trim().Length == 0)
                {
                    lbErrorTraspasos.Text = "Debe ingresar un artículo válido";
                    txtArticulo.Text = "";
                    txtArticulo.Focus();
                    return;
                }

                int mCantidad = 0;

                try
                {
                    mCantidad = Convert.ToInt32(txtCantidad.Text.Trim());
                }
                catch
                {
                    mCantidad = 0;
                }

                if (mCantidad <= 0)
                {
                    lbErrorTraspasos.Text = "Debe ingresar una cantidad válida";
                    txtCantidad.Focus();
                    return;
                }

                List<Clases.Traspasos> mTraspaso = new List<Clases.Traspasos>();
                mTraspaso = (List<Clases.Traspasos>)Session["Traspaso"];

                if (cbBodegaOrigen.Value.ToString() == cbBodegaDestino.Value.ToString())
                {
                    lbErrorTraspasos.Text = "Las bodegas origen y destino no pueden ser iguales";
                    cbBodegaOrigen.Focus();
                    return;
                }

                int mLinea = 1;
                if (mTraspaso.Count() > 0) mLinea = mTraspaso[mTraspaso.Count() - 1].Linea + 1;

                Clases.Traspasos mArticulo = new Clases.Traspasos();
                mArticulo.Traspaso = "";
                mArticulo.Fecha = DateTime.Now.Date;
                mArticulo.Linea = mLinea;
                mArticulo.Articulo = txtArticulo.Text.Trim();
                mArticulo.Descripcion = txtArticuloDescripcion.Text.Trim();
                mArticulo.Localizacion = cbLocalizacion.SelectedItem.ToString();
                mArticulo.BodegaOrigen = cbBodegaOrigen.Value.ToString();
                mArticulo.BodegaDestino = cbBodegaDestino.Value.ToString();
                mArticulo.Cantidad = mCantidad;
                mArticulo.Usuario = Convert.ToString(Session["Usuario"]);
                mArticulo.NombreUsuario = "";
                mArticulo.CreateDate = DateTime.Now;
                mArticulo.NombreRecibe = "";
                mArticulo.Consecutivo = "";
                mArticulo.Tooltip = txtArticuloDescripcion.ToolTip;
                mTraspaso.Add(mArticulo);

                gridTraspaso.Visible = true;
                gridTraspaso.DataSource = mTraspaso.AsEnumerable();
                gridTraspaso.DataMember = "Traspasos";
                gridTraspaso.DataBind();
                gridTraspaso.FocusedRowIndex = -1;
                gridTraspaso.SettingsPager.PageSize = 500;

                txtArticulo.Text = "";
                txtArticuloDescripcion.Text = "";
                txtCantidad.Text = "1";

                Session["Traspaso"] = mTraspaso;
                txtArticulo.Focus();
            }
            catch (Exception ex)
            {
                lbErrorTraspasos.Text = CatchClass.ExMessage(ex, "bodega", "btnAgregarArticulo_Click");
            }
        }

        protected void btnGrabarTraspaso_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoTraspasos.Text = "";
                lbErrorTraspasos.Text = "";

                List<Clases.Traspasos> mTraspaso = new List<Clases.Traspasos>();
                mTraspaso = (List<Clases.Traspasos>)Session["Traspaso"];

                string json = JsonConvert.SerializeObject(mTraspaso);
                byte[] data = Encoding.ASCII.GetBytes(json);

                WebRequest request = WebRequest.Create(string.Format("{0}/traspasos/", Convert.ToString(Session["UrlRestServices"])));

                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string mRetorna = responseString.ToString().Replace("\"", "");
                if (mRetorna.ToLower().Contains("error"))
                {
                    lbErrorTraspasos.Text = mRetorna;
                    return;
                }

                lbInfoTraspasos.Text = mRetorna;

                try
                {
                    DataSet dsTodos = new DataSet();
                    dsTodos = (DataSet)Session["dsTodos"];

                    List<Clases.Traspasos> mBlanco = new List<Clases.Traspasos>();
                    Session["Traspaso"] = mBlanco;
                    gridTraspaso.Visible = false;

                    txtArticulo.Text = "";
                    txtArticuloDescripcion.Text = "";
                    txtCantidad.Text = "1";

                    string[] mNumero = mRetorna.Split(new string[] { " " }, StringSplitOptions.None);

                    txtTraspaso.Text = mNumero[5];
                    txtDespachoTraspasoT.Text = mNumero[5];

                    foreach (var item in mTraspaso)
                    {
                        List<Clases.Articulos> mArticulos = ConsultaArticulo(item.Articulo);
                        dsTodos.Tables["articulos"].Rows.Find(item.Articulo)["Tooltip"] = mArticulos[0].Tooltip;
                    }

                    gridArticulosBuscar.DataSource = dsTodos;
                    gridArticulosBuscar.DataMember = "articulos";
                    gridArticulosBuscar.DataBind();
                    gridArticulosBuscar.FocusedRowIndex = -1;
                    gridArticulosBuscar.SettingsPager.PageSize = 35;

                    Session["dsTodos"] = dsTodos;

                   
                }
                catch
                {
                }

                txtArticulo.Focus();
            }
            catch (Exception ex)
            {
                lbErrorTraspasos.Text = CatchClass.ExMessage(ex, "bodega", "btnGrabarTraspaso_Click");
            }
        }

        protected void gridTraspaso_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
        {
            try
            {
                lbInfoTraspasos.Text = "";
                lbErrorTraspasos.Text = "";

                int mLinea = Convert.ToInt32(e.KeyValue.ToString());

                List<Clases.Traspasos> mTraspaso = new List<Clases.Traspasos>();
                mTraspaso = (List<Clases.Traspasos>)Session["Traspaso"];

                var mArticulo = mTraspaso.Single(x => x.Linea == mLinea);
                mTraspaso.Remove(mArticulo);

                gridTraspaso.DataSource = mTraspaso.AsEnumerable();
                gridTraspaso.DataMember = "Traspasos";
                gridTraspaso.DataBind();
                gridTraspaso.FocusedRowIndex = -1;
                gridTraspaso.SettingsPager.PageSize = 500;

                Session["Traspaso"] = mTraspaso;
                lbInfoTraspasos.Text = "El artículo fue eliminado exitosamente";
            }
            catch (Exception ex)
            {
                lbErrorTraspasos.Text = CatchClass.ExMessage(ex, "bodega", "gridTraspaso_RowCommand");
            }
        }

        protected void gridTraspaso_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.VisibleIndex >= 0) e.Cell.ToolTip = e.GetValue("Tooltip").ToString();
        }

        protected void CancelEditingTraspaso(CancelEventArgs e)
        {
            e.Cancel = true;
            gridTraspaso.CancelEdit();
        }

        void UpdateTraspaso(string linea, string articulo, string localizacion, string origen, string destino, string cantidad)
        {
            try
            {
                lbInfoTraspasos.Text = "";
                lbErrorTraspasos.Text = "";

                int mLinea = Convert.ToInt32(linea);
                int mCantidad = Convert.ToInt32(cantidad);

                List<Clases.Traspasos> mTraspaso = new List<Clases.Traspasos>();
                mTraspaso = (List<Clases.Traspasos>)Session["Traspaso"];

                bool mUpdate = true;
                if (mTraspaso.Where(x => x.Linea != mLinea && x.Articulo.Equals(articulo) && x.Localizacion.Equals(localizacion) && x.BodegaOrigen.Equals(origen) && x.BodegaDestino.Equals(destino)).Count() > 0)
                {
                    mUpdate = false;
                    lbErrorTraspasos.Text = string.Format("El artículo {0} en la localización, bodega origen y destino ya está agregado", articulo);
                }

                if (mUpdate)
                {
                    foreach (var item in mTraspaso)
                    {
                        if (item.Linea == mLinea)
                        {
                            if (mTraspaso.Where(x => x.Articulo.Equals(item.Articulo) && x.Localizacion.Equals(localizacion) && x.BodegaOrigen.Equals(origen) && x.BodegaDestino.Equals(destino)).Count() > 1)
                            {
                                lbErrorTraspasos.Text = "Este artículo en la localización, bodega origen y destino ya está agregado";
                                return;
                            }

                            item.Localizacion = localizacion;
                            item.BodegaOrigen = origen;
                            item.BodegaDestino = destino;
                            item.Cantidad = mCantidad;
                        }
                    }

                    gridTraspaso.DataSource = mTraspaso.AsEnumerable();
                    gridTraspaso.DataMember = "Traspasos";
                    gridTraspaso.DataBind();
                    gridTraspaso.FocusedRowIndex = -1;
                    gridTraspaso.SettingsPager.PageSize = 500;

                    Session["Traspaso"] = mTraspaso;
                }
            }
            catch (Exception ex)
            {
                lbErrorTraspasos.Text = CatchClass.ExMessage(ex, "bodega", "UpdateTraspaso");
            }
        }

        protected void gridTraspaso_BatchUpdate(object sender, ASPxDataBatchUpdateEventArgs e)
        {
            foreach (var args in e.UpdateValues)
                UpdateTraspaso(args.Keys["Linea"].ToString(), args.NewValues["Articulo"].ToString(), args.NewValues["Localizacion"].ToString(), args.NewValues["BodegaOrigen"].ToString(), args.NewValues["BodegaDestino"].ToString(), args.NewValues["Cantidad"].ToString());

            e.Handled = true;
        }

        protected void gridTraspaso_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            UpdateTraspaso(e.Keys["Linea"].ToString(), e.NewValues["Articulo"].ToString(), e.NewValues["Localizacion"].ToString(), e.NewValues["BodegaOrigen"].ToString(), e.NewValues["BodegaDestino"].ToString(), e.NewValues["Cantidad"].ToString());
            CancelEditingTraspaso(e);
        }

        protected void gridTraspaso_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            try
            {
                if (e.Column.FieldName == "BodegaOrigen" || e.Column.FieldName == "BodegaDestino")
                {
                    DataSet ds = new DataSet();
                    ds = (DataSet)Session["Bodegas"];

                    ASPxComboBox cmb = e.Editor as ASPxComboBox;
                    cmb.DataSource = ds.Tables["Bodegas"];
                    cmb.ValueField = "Bodega";
                    cmb.ValueType = typeof(System.String);
                    cmb.TextField = "Bodega";
                    cmb.DataBindItems();
                }

                if (e.Column.FieldName == "Localizacion")
                {
                    DataSet ds = new DataSet();
                    DataTable dt = new DataTable("Localizaciones");
                    dt.Columns.Add(new DataColumn("Localizacion", typeof(System.String)));

                    DataRow mRow1 = dt.NewRow();
                    mRow1["Localizacion"] = "ARMADO";
                    dt.Rows.Add(mRow1);

                    DataRow mRow2 = dt.NewRow();
                    mRow2["Localizacion"] = "CAJA";
                    dt.Rows.Add(mRow2);

                    DataRow mRow3 = dt.NewRow();
                    mRow3["Localizacion"] = "DESARMDO";
                    dt.Rows.Add(mRow3);

                    ds.Tables.Add(dt);

                    ASPxComboBox cmb = e.Editor as ASPxComboBox;
                    cmb.DataSource = ds.Tables["Localizaciones"];
                    cmb.ValueField = "Localizacion";
                    cmb.ValueType = typeof(System.String);
                    cmb.TextField = "Localizacion";
                    cmb.DataBindItems();
                }
            }
            catch
            {
                //Nothing
            }
        }

        protected void btnImprimirTraspaso_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoTraspasos.Text = "";
                lbErrorTraspasos.Text = "";

                if (txtTraspaso.Text.Trim().Length == 0) return;

                WebRequest request = WebRequest.Create(string.Format("{0}/traspasos/{1}", Convert.ToString(Session["UrlRestServices"]), txtTraspaso.Text));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                Clases.TraspasoImpresion mImpresion = new Clases.TraspasoImpresion();
                mImpresion = JsonConvert.DeserializeObject<Clases.TraspasoImpresion>(responseString);

                DataSet ds = new DataSet();
                DataTable dt = new DataTable("Traspaso");

                dt.Columns.Add(new DataColumn("Traspaso", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Fecha", typeof(System.DateTime)));
                dt.Columns.Add(new DataColumn("Linea", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("Articulo", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Descripcion", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Localizacion", typeof(System.String)));
                dt.Columns.Add(new DataColumn("BodegaOrigen", typeof(System.String)));
                dt.Columns.Add(new DataColumn("BodegaDestino", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Cantidad", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("Usuario", typeof(System.String)));
                dt.Columns.Add(new DataColumn("NombreUsuario", typeof(System.String)));
                dt.Columns.Add(new DataColumn("CreateDate", typeof(System.String)));
                dt.Columns.Add(new DataColumn("NombreRecibe", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Consecutivo", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Tooltip", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Mayoreo", typeof(System.String)));
                dt.Columns.Add(new DataColumn("SKU", typeof(System.String)));

                foreach (var item in mImpresion.Traspaso)
                {
                    DataRow mRow = dt.NewRow();
                    mRow["Traspaso"] = item.Traspaso;
                    mRow["Fecha"] = item.Fecha;
                    mRow["Linea"] = item.Linea;
                    mRow["Articulo"] = item.Articulo;
                    mRow["Descripcion"] = item.Descripcion;
                    mRow["Localizacion"] = item.Localizacion;
                    mRow["BodegaOrigen"] = item.BodegaOrigen;
                    mRow["BodegaDestino"] = item.BodegaDestino;
                    mRow["Cantidad"] = item.Cantidad;
                    mRow["Usuario"] = item.Usuario;
                    mRow["NombreUsuario"] = item.NombreUsuario;
                    mRow["CreateDate"] = item.CreateDate;
                    mRow["NombreRecibe"] = item.NombreRecibe;
                    mRow["Consecutivo"] = item.Consecutivo;
                    mRow["Tooltip"] = item.Tooltip;
                    mRow["Mayoreo"] = item.Mayoreo;
                    mRow["SKU"] = item.SKU;
                    dt.Rows.Add(mRow);
                }

                ds.Tables.Add(dt);

                using (ReportDocument reporte = new ReportDocument())
                {
                    string p = (Request.PhysicalApplicationPath + "reportes/rptTraspaso.rpt");
                    reporte.Load(p);

                    string mNombreDocumento = string.Format("{0}.pdf", txtTraspaso.Text);

                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                    reporte.SetDataSource(ds);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                lbErrorTraspasos.Text = CatchClass.ExMessage(ex, "bodega", "btnImprimirTraspaso_Click");
            }
        }

        protected void gridDiasDet_Load(object sender, EventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            var mDepartamento = grid.GetMasterRowKeyValue();

            Clases.DiasDespachos mDiasDespachos = new Clases.DiasDespachos();
            mDiasDespachos = (Clases.DiasDespachos)Session["DiasDespachos"];

            var q = mDiasDespachos.Municipios.Where(x => x.Departamento == mDepartamento.ToString() && x.Tipo == "M");

            grid.DataSource = q.AsEnumerable();
            grid.DataBind();
            grid.SettingsPager.PageSize = 500;
        }

        protected void lkDiasDespachos_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoDias.Text = "";
                lbErrorTraspasos.Text = "";

                WebRequest request = WebRequest.Create(string.Format("{0}/diasdespacho", Convert.ToString(Session["UrlRestServices"])));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                Clases.DiasDespachos mDiasDespachos = new Clases.DiasDespachos();
                mDiasDespachos = JsonConvert.DeserializeObject<Clases.DiasDespachos>(responseString);

                gridDias.Visible = true;
                gridDias.DataSource = mDiasDespachos.Departamentos.AsEnumerable();
                gridDias.DataMember = "Departamentos";
                gridDias.DataBind();
                gridDias.FocusedRowIndex = -1;
                gridDias.SettingsPager.PageSize = 500;

                lkGrabarDias.Visible = true;
                Session["DiasDespachos"] = mDiasDespachos;
            }
            catch (Exception ex)
            {
                lbErrorTraspasos.Text = CatchClass.ExMessage(ex, "bodega", "lkDiasDespachos_Click");
            }
        }

        void UpdateDepartamento(string departamento, bool lunes, bool martes, bool miercoles, bool jueves, bool viernes, bool sabado, bool domingo)
        {
            try
            {
                Clases.DiasDespachos mDiasDespachos = new Clases.DiasDespachos();
                mDiasDespachos = (Clases.DiasDespachos)Session["DiasDespachos"];

                var q = from d in mDiasDespachos.Departamentos where d.Departamento == departamento select d;
                foreach (var item in q)
                {
                    item.Lunes = lunes;
                    item.Martes = martes;
                    item.Miercoles = miercoles;
                    item.Jueves = jueves;
                    item.Viernes = viernes;
                    item.Sabado = sabado;
                    item.Domingo = domingo;
                }

                Session["DiasDespachos"] = mDiasDespachos;

                gridDias.DataSource = mDiasDespachos.Departamentos.AsEnumerable();
                gridDias.DataMember = "Departamentos";
                gridDias.DataBind();
                gridDias.FocusedRowIndex = -1;
                gridDias.SettingsPager.PageSize = 500;
            }
            catch (Exception ex)
            {
                lbErrorDias.Text = CatchClass.ExMessage(ex, "bodega", "UpdateDepartamento");
            }
        }

        protected void gridDias_BatchUpdate(object sender, ASPxDataBatchUpdateEventArgs e)
        {
            foreach (var args in e.UpdateValues)
                UpdateDepartamento(args.Keys["Departamento"].ToString()
                    , Convert.ToBoolean(args.NewValues["Lunes"])
                    , Convert.ToBoolean(args.NewValues["Martes"])
                    , Convert.ToBoolean(args.NewValues["Miercoles"])
                    , Convert.ToBoolean(args.NewValues["Jueves"])
                    , Convert.ToBoolean(args.NewValues["Viernes"])
                    , Convert.ToBoolean(args.NewValues["Sabado"])
                    , Convert.ToBoolean(args.NewValues["Domingo"])
                );

            e.Handled = true;
        }

        protected void gridDias_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            UpdateDepartamento(e.Keys["Departamento"].ToString()
                , Convert.ToBoolean(e.NewValues["Lunes"])
                , Convert.ToBoolean(e.NewValues["Martes"])
                , Convert.ToBoolean(e.NewValues["Miercoles"])
                , Convert.ToBoolean(e.NewValues["Jueves"])
                , Convert.ToBoolean(e.NewValues["Viernes"])
                , Convert.ToBoolean(e.NewValues["Sabado"])
                , Convert.ToBoolean(e.NewValues["Domingo"]));
            CancelEditingDepartamento(e);
        }

        protected void gridDias_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.VisibleIndex >= 0) e.Cell.ToolTip = e.GetValue("Tooltip").ToString();
        }

        protected void CancelEditingDepartamento(CancelEventArgs e)
        {
            e.Cancel = true;
            gridDias.CancelEdit();
        }

        void UpdateMunicipio(string municipio, bool lunes, bool martes, bool miercoles, bool jueves, bool viernes, bool sabado, bool domingo)
        {
            try
            {
                Clases.DiasDespachos mDiasDespachos = new Clases.DiasDespachos();
                mDiasDespachos = (Clases.DiasDespachos)Session["DiasDespachos"];

                var q = from m in mDiasDespachos.Municipios where m.Municipio == municipio select m;
                foreach (var item in q)
                {
                    item.Lunes = lunes;
                    item.Martes = martes;
                    item.Miercoles = miercoles;
                    item.Jueves = jueves;
                    item.Viernes = viernes;
                    item.Sabado = sabado;
                    item.Domingo = domingo;
                }

                Session["DiasDespachos"] = mDiasDespachos;

                //gridDiasDet.DataSource = mDiasDespachos.Municipios.AsEnumerable();
                //gridDiasDet.DataMember = "Municipios";
                //gridDiasDet.DataBind();
                //gridDiasDet.FocusedRowIndex = -1;
                //gridDiasDet.SettingsPager.PageSize = 500;
            }
            catch (Exception ex)
            {
                lbErrorDias.Text = CatchClass.ExMessage(ex, "bodega", "UpdateMunicipio");
            }
        }

        protected void gridDiasDet_BatchUpdate(object sender, ASPxDataBatchUpdateEventArgs e)
        {
            foreach (var args in e.UpdateValues)
                UpdateMunicipio(args.Keys["Municipio"].ToString()
                    , Convert.ToBoolean(args.NewValues["Lunes"])
                    , Convert.ToBoolean(args.NewValues["Martes"])
                    , Convert.ToBoolean(args.NewValues["Miercoles"])
                    , Convert.ToBoolean(args.NewValues["Jueves"])
                    , Convert.ToBoolean(args.NewValues["Viernes"])
                    , Convert.ToBoolean(args.NewValues["Sabado"])
                    , Convert.ToBoolean(args.NewValues["Domingo"])
                );

            e.Handled = true;
        }

        protected void gridDiasDet_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            UpdateMunicipio(e.Keys["Municipio"].ToString()
                , Convert.ToBoolean(e.NewValues["Lunes"])
                , Convert.ToBoolean(e.NewValues["Martes"])
                , Convert.ToBoolean(e.NewValues["Miercoles"])
                , Convert.ToBoolean(e.NewValues["Jueves"])
                , Convert.ToBoolean(e.NewValues["Viernes"])
                , Convert.ToBoolean(e.NewValues["Sabado"])
                , Convert.ToBoolean(e.NewValues["Domingo"]));
            CancelEditingMunicipio(e);
        }

        protected void gridDiasDet_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.VisibleIndex >= 0) e.Cell.ToolTip = e.GetValue("Tooltip").ToString();
        }

        protected void CancelEditingMunicipio(CancelEventArgs e)
        {
            e.Cancel = true;
            //gridDiasDet.CancelEdit();
        }

        protected void lkGrabarDias_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoDias.Text = "";
                lbErrorDias.Text = "";

                Clases.DiasDespachos mDiasDespachos = new Clases.DiasDespachos();
                mDiasDespachos = (Clases.DiasDespachos)Session["DiasDespachos"];

                mDiasDespachos.Usuario = Convert.ToString(Session["Usuario"]);

                string json = JsonConvert.SerializeObject(mDiasDespachos);
                byte[] data = Encoding.ASCII.GetBytes(json);

                WebRequest request = WebRequest.Create(string.Format("{0}/diasdespacho", Convert.ToString(Session["UrlRestServices"])));

                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string mMensaje = Convert.ToString(responseString).Replace("\"", "");
                if (mMensaje.ToLower().Contains("error"))
                {
                    lbErrorDias.Text = mMensaje;
                    return;
                }

                lbInfoDias.Text = mMensaje;
            }
            catch (Exception ex)
            {
                lbErrorTraspasos.Text = CatchClass.ExMessage(ex, "bodega", "lkGrabarDias_Click");
            }
        }


    }
}
