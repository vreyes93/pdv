﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="clientes.aspx.cs" Inherits="PuntoDeVenta.clientes" MaintainScrollPositionOnPostback="true" %>
<%@ Register assembly="DevExpress.Web.v16.2, Version=16.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            border-style: solid;
            border-width: 3px;
        }
        .style6
        {}
        .style10
        {
            text-decoration: underline;
        }
        .style11
        {
            height: 22px;
        }
        .style12
        {
        }
        .style13
        {
            color: #FF0000;
        }
        .style1000
        {
            height: 26px;
        }
        .style1001
        {
            width: 100%;
        }
        .auto-style1 {
            width: 117px;
        }
    </style>
    <script type="text/javascript">
        function tablaDireccionFactura() {
            if (document.getElementById("cbMismaDireccionFactura").checked)
            { tblDireccionFactura.style.visibility="hidden" }
            else
            { tblDireccionFactura.style.visibility = "visible" }
        }
        function comprueba() {
            return confirm("Confirme el postback");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>&nbsp;&nbsp;Clientes</h3>
    <div class="content2">
            <table id="tblInfo" runat="server" visible="false" align="center">
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="lkNuevoCliente" runat="server" 
                        ToolTip="Haga clic aquí para limpiar la página e ingresar datos para un cliente nuevo" 
                        onclick="lkNuevoCliente_Click" TabIndex="995">Crear cliente nuevo</asp:LinkButton>
                    &nbsp;&nbsp;
                    <asp:Label ID="lbInfo2" runat="server" ></asp:Label>
                </td>
            </tr>
        </table>
    <div class="clientes">
    <ul>
        <li>
    <table align="center">
        <tr>
            <td style="text-align: left">
                Código:&nbsp; </td>
            <td style="text-align: right">
                <asp:TextBox ID="txtCodigo" runat="server" BackColor="White" BorderStyle="None" Enabled="False" Font-Bold="True" Font-Size="Large" Width="100px" 
                    TabIndex="3" ToolTip="Esté es el código del cliente, al ser un cliente nuevo el código se asignará al momento de grabarlo.">0000000</asp:TextBox>
            </td>
            <td style="text-align: left">
                <asp:LinkButton ID="lbBuscarCliente" runat="server" 
                    ToolTip="Haga clic aquí para buscar un cliente" TabIndex="4" 
                    onclick="lbBuscarCliente_Click">Buscar</asp:LinkButton>
            </td>
            <td>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td style="text-align: right">
                Nivel de precio:</td>
            <td style="text-align: left">
                <asp:DropDownList ID="cbTipo" runat="server" Width="295px" 
                    ToolTip="Seleccione la promoción (Normal, Tarjeta Libre, etc.)" 
                    TabIndex="8" 
                    onselectedindexchanged="cbTipo_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td style="text-align: left">
                <asp:DropDownList ID="cbFinanciera" runat="server" Width="150px" 
                    TabIndex="10" 
                    ToolTip="Seleccione la financiera a utilizar" AutoPostBack="True" 
                    onselectedindexchanged="cbFinanciera_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td style="text-align: left">
                <asp:DropDownList ID="cbNivelPrecio" runat="server" Width="115px" 
                    TabIndex="12" ToolTip="Seleccione el nivel de precio" AutoPostBack="True" 
                    onselectedindexchanged="cbNivelPrecio_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
        </li>
    </ul>
    <ul>
        <li>
            <table align="center" runat="server" visible="false" id="tblBuscar">
                <tr>
                    <td colspan="11">
                        <a>Búsqueda de Clientes</a></td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td>
                        Código:</td>
                    <td>
                        <asp:TextBox ID="txtCodigoBuscar" runat="server" AutoPostBack="True" 
                            ontextchanged="txtCodigoBuscar_TextChanged" TabIndex="14" 
                            
                            ToolTip="Ingrese aquí el código o parte del código del cliente que desea buscar y luego presione ENTER.  No es necesario digitar los ceros a la izquierda del código." 
                            Width="80px"></asp:TextBox>
                    </td>
                    <td>
                        Nombre:</td>
                    <td>
                        <asp:TextBox ID="txtNombreBuscar" runat="server" Width="300px" AutoPostBack="True" 
                            ontextchanged="txtNombreBuscar_TextChanged" TabIndex="16" style="text-transform: uppercase;" 
                            
                            ToolTip="Ingrese aquí el nombre o parte del nombre del cliente que desea buscar y luego presione ENTER."></asp:TextBox>
                    </td>
                    <td>
                        NIT:</td>
                    <td>
                        <asp:TextBox ID="txtNitBuscar" runat="server" AutoPostBack="True" 
                            ontextchanged="txtNitBuscar_TextChanged" TabIndex="18" style="text-transform: uppercase;" 
                            
                            ToolTip="Ingrese aquí el NIT o parte del NIT que desea buscar y luego presione ENTER." 
                            Width="80px"></asp:TextBox>
                    </td>
                    <td>
                        Tel.:</td>
                    <td>
                        <asp:TextBox ID="txtTelefono" runat="server" AutoPostBack="True" 
                            ontextchanged="txtTelefono_TextChanged" TabIndex="20" Width="90px" 
                            
                            ToolTip="Ingrese aquí el teléfono que desea buscar y luego presione ENTER, se buscará en el teléfono de casa, trabajo y celular."></asp:TextBox>
                    </td>
                    <td>
                        <asp:LinkButton ID="lkOcultarBusqueda" runat="server" 
                            ToolTip="Haga clic aquí para ocultar la búsqueda de clientes." 
                            onclick="lkOcultarBusqueda_Click" TabIndex="22">Ocultar</asp:LinkButton>
                    </td>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </li>
        <li>
            <asp:GridView ID="gridClientes" runat="server" Visible="False" 
                AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" 
                BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" 
                onselectedindexchanged="gridClientes_SelectedIndexChanged" 
                onrowdatabound="gridClientes_RowDataBound" TabIndex="24">
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkSeleccionar" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkSeleccionar_Click" Text="Seleccionar"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Cliente" HeaderText="Cliente">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" >
                    <ItemStyle Font-Size="X-Small" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Nit" HeaderText="Nit" />
                    <asp:BoundField DataField="TelCasa" HeaderText="Tel Casa" />
                    <asp:BoundField DataField="TelTrabajo" HeaderText="Tel Trabajo" />
                    <asp:BoundField DataField="TelCelular" HeaderText="Celular" />
                    <asp:BoundField DataField="Tienda" HeaderText="Tienda">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Vendedor" HeaderText="Vendedor" />
                    <asp:BoundField DataField="FechaIngreso" HeaderText="Ingreso" Visible="False">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                </Columns>
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#FFF1D4" />
                <SortedAscendingHeaderStyle BackColor="#B95C30" />
                <SortedDescendingCellStyle BackColor="#F1E5CE" />
                <SortedDescendingHeaderStyle BackColor="#93451F" />
            </asp:GridView>
        </li>
    </ul>
    <ul>
        <li>
    <table align="center">
        <tr>
            <td colspan="4">
                <a>Datos Generales</a>&nbsp;&nbsp;&nbsp;<asp:Label ID="lbCliente" runat="server" 
                    Text="Nombre cliente" ForeColor="White" Font-Size="X-Small" Width="235px"></asp:Label></td>
            <td colspan="4" style="text-align: right">
                    <asp:Label ID="lbErrorGenerales" runat="server" Font-Bold="False" ForeColor="Red"></asp:Label>
                </td>
        </tr>
        <tr>
            <td>
                <span class="style10">NIT</span>:</td>
            <td>
                <asp:TextBox ID="txtNit" runat="server" Width="72px" TabIndex="26" 
                    ToolTip="NIT del cliente (CON GUION), ejemplo: 452158-7" style="text-transform: uppercase;" 
                    AutoPostBack="True" ontextchanged="txtNit_TextChanged" MaxLength="20"></asp:TextBox>
                <asp:CheckBox ID="cbConsumidorFinal" runat="server" Text="CF" TabIndex="28" 
                    ToolTip="Marque esta casilla si desea utilizar Consumidor Final" 
                    AutoPostBack="True" oncheckedchanged="cbConsumidorFinal_CheckedChanged" />
                <asp:CheckBox ID="cbPersonaJuridica" runat="server" Text="PJ" TabIndex="29" 
                    ToolTip="Marque esta casilla si el cliente es una persona jurídica, por ejmplo: Productos Múltiples, S,A." 
                    AutoPostBack="True" oncheckedchanged="cbPersonaJuridica_CheckedChanged" />
            </td>
            <td class="auto-style1">
                DPI:</td>
            <td>
                <asp:TextBox ID="txtDPI" runat="server" TabIndex="30" 
                    ToolTip="DPI, ejemplo: 2531-10324-0101 ó cédula de vecindad, ejemplo: A-1 631592" 
                    Width="124px" MaxLength="50"></asp:TextBox>
            </td>
            <td>
                Tel. Casa:</td>
            <td>
                <asp:TextBox ID="txtTelefono1" runat="server" TabIndex="46" 
                    ToolTip="Teléfono de casa, ejemplo: 2471-8596" Width="140px" 
                    MaxLength="15" ontextchanged="txtTelefono1_TextChanged" 
                    AutoPostBack="True"></asp:TextBox>
            </td>
            <td>
                Email:</td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server" Width="220px" TabIndex="60" style="text-transform: lowercase;"
                    ToolTip="Correo electrónico, ejemplo: juanperez@gmail.com" MaxLength="249"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <span class="style10">Nombres</span>:</td>
            <td>
                <asp:TextBox ID="txtPrimerNombre" runat="server" TabIndex="32" style="text-transform: uppercase;" 
                    
                    ToolTip="Primer Nombre, ejemplo: Juan y si es persona jurídica, aquí puede escribir algo como: Productos Múltiples, S.A." 
                    MaxLength="25" Width="155px"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtSegundoNombre" runat="server" TabIndex="34" style="text-transform: uppercase;" 
                    ToolTip="Segundo Nombre, ejemplo: Carlos" MaxLength="25" Width="155px"></asp:TextBox>
            </td>
            <td>
                Tel. Trabajo:</td>
            <td>
                <asp:TextBox ID="txtTelefono2" runat="server" TabIndex="48" 
                    ToolTip="Teléfono del trabajo, ejemplo: 2384-7200" Width="140px" 
                    MaxLength="15" ontextchanged="txtTelefono2_TextChanged" 
                    AutoPostBack="True"></asp:TextBox>
            </td>
            <td>
                Notas:</td>
            <td rowspan="2">
                <asp:TextBox ID="txtNotas" runat="server" Width="220px" TextMode="MultiLine" 
                    Height="48px" TabIndex="70" 
                    ToolTip="Texto libre para ingresar observaciones del cliente"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <span class="style10">Tercer Nom.</span>:</td>
            <td>
                <asp:TextBox ID="txtTercerNombre" runat="server" TabIndex="35" style="text-transform: uppercase;" 
                    ToolTip="Tercer Nombre, ejemplo: José" MaxLength="25" Width="155px"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:CheckBox ID="cbFacturarOtroNombre" runat="server" 
                    Text="Facturar a otro nombre"                     
                    ToolTip="Marque esta casilla si el cliente desea que la factura se emita a otro nombre" 
                    AutoPostBack="True" oncheckedchanged="cbFacturarOtroNombre_CheckedChanged" 
                    TabIndex="36" />
            </td>
            <td>
                Celular:</td>
            <td>
                <asp:TextBox ID="txtCelular" runat="server" TabIndex="50" ToolTip="Celular, ejemplo: 5414-4539" 
                    Width="140px" MaxLength="15" ontextchanged="txtCelular_TextChanged" 
                    AutoPostBack="True"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <span class="style10">Apellidos</span>:</td>
            <td>
                <asp:TextBox ID="txtPrimerApellido" runat="server" TabIndex="37" style="text-transform: uppercase;" 
                    ToolTip="Primer Apellido, ejemplo Pérez" MaxLength="25" Width="155px"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtSegundoApellido" runat="server" TabIndex="38" style="text-transform: uppercase;" 
                    ToolTip="Segundo Apellido, ejemplo: López" MaxLength="25" Width="155px"></asp:TextBox>
            </td>
            <td>
                Fecha Nac.:</td>
            <td style="margin-left: 40px">
                <asp:DropDownList ID="cbDia" runat="server" TabIndex="52" 
                    ToolTip="Seleccione el día de nacimiento del cliente">
                    <asp:ListItem>--</asp:ListItem>
                    <asp:ListItem Value="1">01</asp:ListItem>
                    <asp:ListItem Value="2">02</asp:ListItem>
                    <asp:ListItem Value="3">03</asp:ListItem>
                    <asp:ListItem Value="4">04</asp:ListItem>
                    <asp:ListItem Value="5">05</asp:ListItem>
                    <asp:ListItem Value="6">06</asp:ListItem>
                    <asp:ListItem Value="7">07</asp:ListItem>
                    <asp:ListItem Value="8">08</asp:ListItem>
                    <asp:ListItem Value="9">09</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="cbMes" runat="server" TabIndex="53" 
                    ToolTip="Seleccione el mes de nacimiento del cliente">
                    <asp:ListItem>--</asp:ListItem>
                    <asp:ListItem Value="1">Ene</asp:ListItem>
                    <asp:ListItem Value="2">Feb</asp:ListItem>
                    <asp:ListItem Value="3">Mar</asp:ListItem>
                    <asp:ListItem Value="4">Abr</asp:ListItem>
                    <asp:ListItem Value="5">May</asp:ListItem>
                    <asp:ListItem Value="6">Jun</asp:ListItem>
                    <asp:ListItem Value="7">Jul</asp:ListItem>
                    <asp:ListItem Value="8">Ago</asp:ListItem>
                    <asp:ListItem Value="9">Sep</asp:ListItem>
                    <asp:ListItem Value="10">Oct</asp:ListItem>
                    <asp:ListItem Value="11">Nov</asp:ListItem>
                    <asp:ListItem Value="12">Dic</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtAnio" runat="server" TabIndex="54" 
                    ToolTip="Ingrese aquí el año de nacimiento del cliente en formato de 4 dígitos, ejemplo:1970." 
                    Width="39px">2025</asp:TextBox>
                <asp:TextBox ID="txtFechaNacimiento" runat="server" TabIndex="55" Visible="false"
                    ToolTip="Fecha de nacimiento del cliente, ejemplo: 14/11/1975, recuerde ingresar la fecha en format dd/MM/yyyy" 
                    Width="10px"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Casada:</td>
            <td>
                <asp:TextBox ID="txtApellidoCasada" runat="server" TabIndex="40" style="text-transform: uppercase;" 
                    ToolTip="Apellido de Casada, ejemplo: de Juárez" MaxLength="25" 
                    Width="155px"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:RadioButton ID="rbHombre" runat="server" Text="Hombre" GroupName="Sexo" 
                    TabIndex="42" />
                &nbsp;&nbsp;
                <asp:RadioButton ID="rbMujer" runat="server" Text="Mujer" GroupName="Sexo" TabIndex="44" />
                </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
         <tr id="trExento" runat="server" visible="false">
                    
                    <td>
                        Impuestos: </td>
                    <td class="auto-style1">
                <asp:CheckBox ID="chkExento" runat="server" Text="Exento" TabIndex="28" 
                    ToolTip="Exento de impuestos" />
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
        </table>            
        </li>
        <li>
            <table align="center" id="tblNombreFacturacion" runat="server" visible="false">
                <tr>
                    <td colspan="6">
                <b><a class="style10">NIT y Nombre para Facturación</a></b></td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        NIT que aparecerá en la factura:</td>
                    <td>
                <asp:TextBox ID="txtNitFactura" runat="server" TabIndex="73" style="text-transform: uppercase;" 
                    ToolTip="NIT que aparecerá en la factura (CON GUION), ejemplo: 452158-7" 
                            MaxLength="20" AutoPostBack="True" 
                            ontextchanged="txtNitFactura_TextChanged"></asp:TextBox>
                    </td>
                    <td>
                        Nombre que aparecerá en la factura:</td>
                    <td>
                <asp:TextBox ID="txtNombreFactura" runat="server" Width="286px" TabIndex="74" style="text-transform: uppercase;" 
                    ToolTip="Ingrese aquí el nombre que el cliente desea que aparezca en la factura" 
                            MaxLength="80"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
               
            </table>
        </li>
        <li>
    <table align="center" class="style1" id="tblDireccionFactura" runat="server" visible="true">
        <tr>
            <td colspan="4">
                <a>Dirección de Facturación</a></td>
            <td colspan="4" style="text-align: left">
                <asp:CheckBox ID="cbMismaDireccionFactura" runat="server" Checked="True" 
                    Text="Entregar en esta misma dirección" 
                    ToolTip="Marque esta casilla si desea que la dirección de la factura sea la misma de la dirección de entrega" 
                    AutoPostBack="True" 
                    oncheckedchanged="cbMismaDireccionFactura_CheckedChanged" TabIndex="76" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="cbEntraCamion" runat="server" Text="Entra camión" 
                    ToolTip="Marque esta casilla si en la dirección de entrega se puede ingresar con camión" 
                    TabIndex="77" />
                    &nbsp;&nbsp;
                <asp:CheckBox ID="cbEntraPickup" runat="server" Text="Entra pickup" 
                    ToolTip="Marque esta casilla si en la dirección de entrega se puede ingresar con pickup" 
                    TabIndex="78" />
                    &nbsp;&nbsp;
                <asp:CheckBox ID="cbSegundoPiso" runat="server" Text="Es segundo piso" 
                    ToolTip="Marque esta casilla si la dirección de entrega es en un segundo nivel" 
                    TabIndex="79" />
            </td>
        </tr>
        <tr>
            <td>
                <span class="style10">Calle/Avenida</span>:</td>
            <td colspan="3">
                <asp:TextBox ID="txtCalleAvenidaFacturacion" runat="server" TabIndex="80" style="text-transform: uppercase;" 
                    ToolTip="Ingrese la calle o avenida, ejemplo: 4ta. Calle &quot;A&quot;, si es sobre carretera escriba el kilómetro y nombre de la misma, ejemplo: Km. 20 Carretera a Linda Vista" 
                    MaxLength="40"></asp:TextBox>
            </td>
            <td>
                Apartamento:</td>
            <td>
                <asp:TextBox ID="txtApartamentoFacturacion" runat="server" Width="180px" TabIndex="95" style="text-transform: uppercase;" 
                    ToolTip="Ingrese el apartamento, ejemplo: Apto. 16A y 16B, puede usar este espacio para escribir algo similar a Bodegas No. 15, 15A y 16B" 
                    MaxLength="40"></asp:TextBox>
            </td>
            <td>
                <span class="style10">Departamento</span>:</td>
            <td>
                <asp:DropDownList ID="cbDepartamentoFacturacion" runat="server" Width="250px" 
                    TabIndex="105" ToolTip="Seleccione el departamento" AutoPostBack="True" 
                    onselectedindexchanged="cbDepartamentoFacturacion_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style1000">
                # Casa:</td>
            <td class="style1000">
                <asp:TextBox ID="txtCasaFacturacion" runat="server" Width="80px" TabIndex="85" style="text-transform: uppercase;"
                    ToolTip="Ingrese el número de casa o lote, ejemplo: 18-42 o bien Lote 52" 
                    MaxLength="40"></asp:TextBox>
            </td>
            <td class="style1000">
                Zona:</td>
            <td class="style1000">
                <asp:DropDownList ID="cbZonaFacturacion" runat="server" TabIndex="90" 
                    
                    ToolTip="Seleccione la zona, si es en carretera deje la zona en el valor &quot;00&quot;">
                    <asp:ListItem Value="--"></asp:ListItem>
                    <asp:ListItem Value="00"></asp:ListItem>
                    <asp:ListItem Value="01"></asp:ListItem>
                    <asp:ListItem Value="02"></asp:ListItem>
                    <asp:ListItem Value="03"></asp:ListItem>
                    <asp:ListItem Value="04"></asp:ListItem>
                    <asp:ListItem Value="05"></asp:ListItem>
                    <asp:ListItem Value="06"></asp:ListItem>
                    <asp:ListItem Value="07"></asp:ListItem>
                    <asp:ListItem Value="08"></asp:ListItem>
                    <asp:ListItem Value="09"></asp:ListItem>
                    <asp:ListItem Value="10"></asp:ListItem>
                    <asp:ListItem Value="11"></asp:ListItem>
                    <asp:ListItem Value="12"></asp:ListItem>
                    <asp:ListItem Value="13"></asp:ListItem>
                    <asp:ListItem Value="14"></asp:ListItem>
                    <asp:ListItem Value="15"></asp:ListItem>
                    <asp:ListItem Value="16"></asp:ListItem>
                    <asp:ListItem Value="17"></asp:ListItem>
                    <asp:ListItem Value="18"></asp:ListItem>
                    <asp:ListItem Value="19"></asp:ListItem>
                    <asp:ListItem Value="20"></asp:ListItem>
                    <asp:ListItem Value="21"></asp:ListItem>
                    <asp:ListItem Value="22"></asp:ListItem>
                    <asp:ListItem Value="23"></asp:ListItem>
                    <asp:ListItem Value="24"></asp:ListItem>
                    <asp:ListItem Value="25"></asp:ListItem>
                    <asp:ListItem Value="26"></asp:ListItem>
                    <asp:ListItem Value="27"></asp:ListItem>
                    <asp:ListItem Value="28"></asp:ListItem>
                    <asp:ListItem Value="29"></asp:ListItem>
                    <asp:ListItem Value="30"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="style1000">
                Colonia:</td>
            <td class="style1000">
                <asp:TextBox ID="txtColoniaFacturacion" runat="server" Width="180px" TabIndex="100" style="text-transform: uppercase;" 
                    ToolTip="Ingrese la colonia, ejemplo: La Florida" MaxLength="40"></asp:TextBox>
            </td>
            <td class="style1000">
                <span class="style10">Municipio</span>:</td>
            <td class="style1000">
                <asp:DropDownList ID="cbMunicipioFacturacion" runat="server" Width="250px" TabIndex="110" 
                    ToolTip="Seleccione el municipio">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Indicaciones:</td>
            <td colspan="7">
                <asp:TextBox ID="txtIndicacionesParaLLegar" runat="server" Width="797px" 
                    TabIndex="112" style="text-transform: uppercase;"                    
                    ToolTip="Ingrese aquí las indicaciones que puedan ayudar a encontrar más fácilmente la dirección" 
                    MaxLength="500"></asp:TextBox>
            </td>
        </tr>
        </table>
        </li>
        <li>
    <table align="center" class="style1" id="direccionEntrega" runat="server" visible="false">
        <tr>
            <td colspan="4"><b><a class="style10">Dirección de Entrega</a></b></td>
            <td colspan="4" style="text-align: left">
            &nbsp;&nbsp;
                &nbsp;
                &nbsp;
                </td>
        </tr>
        <tr>
            <td>
                Calle/Avenida:</td>
            <td colspan="3">
                <asp:TextBox ID="txtCalleAvenida" runat="server" TabIndex="115" style="text-transform: uppercase;" 
                    ToolTip="Ingrese la calle o avenida, ejemplo: 4ta. Calle &quot;A&quot;, si es sobre carretera escriba el nombre de la misma, ejemplo: Carretera a Linda Vista" 
                    MaxLength="40"></asp:TextBox>
            </td>
            <td>
                Apartamento:</td>
            <td>
                <asp:TextBox ID="txtApartamento" runat="server" Width="180px" TabIndex="130" style="text-transform: uppercase;" 
                    ToolTip="Ingrese el apartamento, ejemplo: Apto. 16A y 16B, puede usar este espacio para escribir algo similar a Bodegas No. 15, 15A y 16B" 
                    MaxLength="40"></asp:TextBox>
            </td>
            <td>
                Departamento:</td>
            <td>
                <asp:DropDownList ID="cbDepartamento" runat="server" Width="250px" 
                    TabIndex="140" ToolTip="Seleccione el departamento" AutoPostBack="True" 
                    onselectedindexchanged="cbDepartamento_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                # Casa:</td>
            <td>
                <asp:TextBox ID="txtCasa" runat="server" Width="80px" TabIndex="120" 
                    ToolTip="Ingrese el número de casa o lote, ejemplo: 18-42 o bien Lote 52 y si es sobre carretera escriba el kilómetro, ejemplo: Km. 20" 
                    MaxLength="40"></asp:TextBox>
            </td>
            <td>
                Zona:</td>
            <td>
                <asp:DropDownList ID="cbZona" runat="server" TabIndex="125" 
                    
                    ToolTip="Seleccione la zona, si es en carretera deje la zona en el valor &quot;00&quot;">
                    <asp:ListItem Value="--"></asp:ListItem>
                    <asp:ListItem Value="00"></asp:ListItem>
                    <asp:ListItem Value="01"></asp:ListItem>
                    <asp:ListItem Value="02"></asp:ListItem>
                    <asp:ListItem Value="03"></asp:ListItem>
                    <asp:ListItem Value="04"></asp:ListItem>
                    <asp:ListItem Value="05"></asp:ListItem>
                    <asp:ListItem Value="06"></asp:ListItem>
                    <asp:ListItem Value="07"></asp:ListItem>
                    <asp:ListItem Value="08"></asp:ListItem>
                    <asp:ListItem Value="09"></asp:ListItem>
                    <asp:ListItem Value="10"></asp:ListItem>
                    <asp:ListItem Value="11"></asp:ListItem>
                    <asp:ListItem Value="12"></asp:ListItem>
                    <asp:ListItem Value="13"></asp:ListItem>
                    <asp:ListItem Value="14"></asp:ListItem>
                    <asp:ListItem Value="15"></asp:ListItem>
                    <asp:ListItem Value="16"></asp:ListItem>
                    <asp:ListItem Value="17"></asp:ListItem>
                    <asp:ListItem Value="18"></asp:ListItem>
                    <asp:ListItem Value="19"></asp:ListItem>
                    <asp:ListItem Value="20"></asp:ListItem>
                    <asp:ListItem Value="21"></asp:ListItem>
                    <asp:ListItem Value="22"></asp:ListItem>
                    <asp:ListItem Value="23"></asp:ListItem>
                    <asp:ListItem Value="24"></asp:ListItem>
                    <asp:ListItem Value="25"></asp:ListItem>
                    <asp:ListItem Value="26"></asp:ListItem>
                    <asp:ListItem Value="27"></asp:ListItem>
                    <asp:ListItem Value="28"></asp:ListItem>
                    <asp:ListItem Value="29"></asp:ListItem>
                    <asp:ListItem Value="30"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                Colonia:</td>
            <td>
                <asp:TextBox ID="txtColonia" runat="server" Width="180px" TabIndex="135" style="text-transform: uppercase;" 
                    ToolTip="Ingrese la colonia, ejemplo: La Florida" MaxLength="40"></asp:TextBox>
            </td>
            <td>
                Municipio:</td>
            <td>
                <asp:DropDownList ID="cbMunicipio" runat="server" Width="250px" TabIndex="145" 
                    ToolTip="Seleccione el municipio">
                </asp:DropDownList>
            </td>
        </tr>
        </table>
        </li>
        <li>
            <table align="center" id="tblSolicitudCredito" runat="server" visible="false">
                <tr>
                    <td colspan="3">
                        <a>Datos para Solicitud de Crédito</a></td>
                    <td colspan="5" style="text-align: right">
                    <asp:Label ID="lbErrorSolicitudCredito" runat="server" Font-Bold="False" ForeColor="Red" ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="style10">Nombre
                        empresa</span>:</td>
                    <td>
                        <asp:TextBox ID="txtEmpresa" runat="server" TabIndex="150" style="text-transform: uppercase;"
                            ToolTip="Ingrese el nombre de la empresa para la que labora el cliente" 
                            MaxLength="40"></asp:TextBox>
                        <asp:TextBox ID="txtEmpresaDireccion" runat="server" Width="10px" 
                            TabIndex="9584" style="text-transform: uppercase;" 
                            ToolTip="Ingrese la dirección de la empresa dónde labora el cliente" 
                            MaxLength="50" Visible="False"></asp:TextBox>
                    </td>
                    <td>
                        Calle/Avenida:</td>
                    <td class="style6">
                <asp:TextBox ID="txtCalleEmpresa" runat="server" TabIndex="200" style="text-transform: uppercase;" 
                    ToolTip="Ingrese la calle o avenida, ejemplo: 4ta. Calle &quot;A&quot;, si es sobre carretera escriba el nombre de la misma, ejemplo: Carretera a Linda Vista" 
                    MaxLength="50"></asp:TextBox>
                    </td>
                    <td class="style6">
                        # Casa:</td>
                    <td class="style6">
                <asp:TextBox ID="txtCasaEmpresa" runat="server" Width="82px" TabIndex="202" 
                    ToolTip="Ingrese el número de casa o lote, ejemplo: 18-42 o bien Lote 52 y si es sobre carretera escriba el kilómetro, ejemplo: Km. 20" 
                    MaxLength="15"></asp:TextBox>
                    </td>
                    <td class="style6">
                        Zona:</td>
                    <td class="style6">
                <asp:DropDownList ID="cbZonaEmpresa" runat="server" TabIndex="204" 
                    
                    
                            ToolTip="Seleccione la zona, si es en carretera deje la zona en el valor &quot;00&quot;">
                    <asp:ListItem Value="--"></asp:ListItem>
                    <asp:ListItem Value="00"></asp:ListItem>
                    <asp:ListItem Value="01"></asp:ListItem>
                    <asp:ListItem Value="02"></asp:ListItem>
                    <asp:ListItem Value="03"></asp:ListItem>
                    <asp:ListItem Value="04"></asp:ListItem>
                    <asp:ListItem Value="05"></asp:ListItem>
                    <asp:ListItem Value="06"></asp:ListItem>
                    <asp:ListItem Value="07"></asp:ListItem>
                    <asp:ListItem Value="08"></asp:ListItem>
                    <asp:ListItem Value="09"></asp:ListItem>
                    <asp:ListItem Value="10"></asp:ListItem>
                    <asp:ListItem Value="11"></asp:ListItem>
                    <asp:ListItem Value="12"></asp:ListItem>
                    <asp:ListItem Value="13"></asp:ListItem>
                    <asp:ListItem Value="14"></asp:ListItem>
                    <asp:ListItem Value="15"></asp:ListItem>
                    <asp:ListItem Value="16"></asp:ListItem>
                    <asp:ListItem Value="17"></asp:ListItem>
                    <asp:ListItem Value="18"></asp:ListItem>
                    <asp:ListItem Value="19"></asp:ListItem>
                    <asp:ListItem Value="20"></asp:ListItem>
                    <asp:ListItem Value="21"></asp:ListItem>
                    <asp:ListItem Value="22"></asp:ListItem>
                    <asp:ListItem Value="23"></asp:ListItem>
                    <asp:ListItem Value="24"></asp:ListItem>
                    <asp:ListItem Value="25"></asp:ListItem>
                    <asp:ListItem Value="26"></asp:ListItem>
                    <asp:ListItem Value="27"></asp:ListItem>
                    <asp:ListItem Value="28"></asp:ListItem>
                    <asp:ListItem Value="29"></asp:ListItem>
                    <asp:ListItem Value="30"></asp:ListItem>
                </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Profesión/Oficio:</td>
                    <td>
                <asp:DropDownList ID="cbProfesion" runat="server" Width="173px" 
                    TabIndex="160" ToolTip="Seleccione la profesión u oficio del cliente">
                </asp:DropDownList>
                    </td>
                    <td>
                        Colonia:</td>
                    <td class="style6">
                <asp:TextBox ID="txtColoniaEmpresa" runat="server" TabIndex="206" style="text-transform: uppercase;" 
                    ToolTip="Ingrese la colonia, ejemplo: La Florida" 
                    MaxLength="50"></asp:TextBox>
                    </td>
                    <td class="style6">
                        Apartamento:</td>
                    <td class="style6" colspan="3">
                <asp:TextBox ID="txtApartamentoEmpresa" runat="server" TabIndex="208" style="text-transform: uppercase;" 
                    ToolTip="Ingrese el apartamento, ejemplo: Apto. 16A y 16B, puede usar este espacio para escribir algo similar a Bodegas No. 15, 15A y 16B" 
                    MaxLength="15"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="style10">Puesto que ocupa</span>:</td>
                    <td>
                        <asp:DropDownList ID="cbPuesto" runat="server" Width="173px" 
                            TabIndex="170" ToolTip="Seleccione el puesto que ocupa el cliente en su lugar de trabajo">
                        </asp:DropDownList>
                    </td>
                    <td><span class="style10">Departamento</span>:</td>
                    <td class="style6">
                        <asp:DropDownList ID="cbDepartamentoEmpresa" runat="server" Width="173px" 
                            TabIndex="210" ToolTip="Seleccione el departamento de la dirección de la empresa" AutoPostBack="True" 
                            onselectedindexchanged="cbDepartamentoEmpresa_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <span class="style10">Municipio</span>:</td>
                    <td colspan="3">
                        <asp:DropDownList ID="cbMunicipioEmpresa" runat="server" Width="173px" TabIndex="220" 
                            ToolTip="Seleccione el municipio de la dirección de la empresa">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="style12">
                        <span class="style10">Fecha de ingreso:</span>:</td>
                    <td class="style12">
                <asp:DropDownList ID="cbDiaTrabajo" runat="server" TabIndex="175" 
                    ToolTip="Seleccione el día de inicio de trabajo">
                    <asp:ListItem>--</asp:ListItem>
                    <asp:ListItem Value="1">01</asp:ListItem>
                    <asp:ListItem Value="2">02</asp:ListItem>
                    <asp:ListItem Value="3">03</asp:ListItem>
                    <asp:ListItem Value="4">04</asp:ListItem>
                    <asp:ListItem Value="5">05</asp:ListItem>
                    <asp:ListItem Value="6">06</asp:ListItem>
                    <asp:ListItem Value="7">07</asp:ListItem>
                    <asp:ListItem Value="8">08</asp:ListItem>
                    <asp:ListItem Value="9">09</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="cbMesTrabajo" runat="server" TabIndex="180" 
                    ToolTip="Seleccione el mes de inicio de trabajo">
                    <asp:ListItem>--</asp:ListItem>
                    <asp:ListItem Value="1">Ene</asp:ListItem>
                    <asp:ListItem Value="2">Feb</asp:ListItem>
                    <asp:ListItem Value="3">Mar</asp:ListItem>
                    <asp:ListItem Value="4">Abr</asp:ListItem>
                    <asp:ListItem Value="5">May</asp:ListItem>
                    <asp:ListItem Value="6">Jun</asp:ListItem>
                    <asp:ListItem Value="7">Jul</asp:ListItem>
                    <asp:ListItem Value="8">Ago</asp:ListItem>
                    <asp:ListItem Value="9">Sep</asp:ListItem>
                    <asp:ListItem Value="10">Oct</asp:ListItem>
                    <asp:ListItem Value="11">Nov</asp:ListItem>
                    <asp:ListItem Value="12">Dic</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtAnioTrabajo" runat="server" TabIndex="185" 
                    ToolTip="Ingrese aquí el año en que empezó a trabajar en formato de 4 dígitos, ejemplo:1970." 
                    Width="39px">2025</asp:TextBox>
                        <asp:TextBox ID="txtTiempo" runat="server" TabIndex="9915" style="text-transform: uppercase;" 
                            ToolTip="Ingrese en años el tiempo que el cliente lleva de laborar para su patrono actual, por ejemplo: 7 años" 
                            MaxLength="46" Visible="False" Width="10px"></asp:TextBox>
                    </td>
                    <td class="style12">
                        <span class="style10">Tel. Empresa</span>:</td>
                    <td class="style12">
                        <asp:TextBox ID="txtEmpresaTelefono" runat="server" TabIndex="230" style="text-transform: uppercase;" 
                            ToolTip="Ingrese el teléfono de la empresa para la que labora el cliente" 
                            MaxLength="56"></asp:TextBox>
                    </td>
                    <td class="style12">
                        Extensión telefónica:</td>
                    <td class="style12" colspan="3">
                        <asp:TextBox ID="txtExtension" runat="server" TabIndex="240" style="text-transform: uppercase;" 
                            ToolTip="Ingrese la extensión de la empresa para contactar al cliente" 
                            MaxLength="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="style10">Cargas Familiares</span>:</td>
                    <td>
                        <asp:TextBox ID="txtCargasFamiliares" runat="server" TabIndex="190" style="text-transform: uppercase;" 
                            ToolTip="Ingrese la cantidad de cargas familiares que tiene el cliente" 
                            MaxLength="20"></asp:TextBox>
                    </td>
                    <td>
                        <span class="style10">Ingresos al mes</span>:</td>
                    <td class="style6">
                        <asp:TextBox ID="txtIngresos" runat="server" TabIndex="250" style="text-transform: uppercase;" 
                            ToolTip="Ingrese el valor de los ingresos del cliente" MaxLength="60"></asp:TextBox>
                    </td>
                    <td>
                        <span class="style10">Egresos al mes</span>:</td>
                    <td colspan="3">
                        <asp:TextBox ID="txtEgresos" runat="server" TabIndex="260" style="text-transform: uppercase;" 
                            ToolTip="Ingrese el valor de los egresos del cliente" MaxLength="62"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style12" colspan="2">
                        Persona expuesta políticamente:
                <asp:DropDownList ID="cbPoliticamente" runat="server" 
                    TabIndex="195" ToolTip="Indique aquí si el cliente es una persona expuesta políticamente" 
                    onselectedindexchanged="cbDepartamentoFacturacion_SelectedIndexChanged">
                    <asp:ListItem Value="N">No</asp:ListItem>
                    <asp:ListItem Value="S">Sí</asp:ListItem>
                </asp:DropDownList>
                    </td>
                    <td>
                        Ref. Bancarias:</td>
                    <td class="style12">
                <asp:DropDownList ID="cbBanco" runat="server" Width="173px" 
                    TabIndex="275" 
                            ToolTip="Seleccione el banco de referencia de la cuenta del cliente">
                </asp:DropDownList>
                    </td>
                    <td class="style12">
                        No. de cuenta:</td>
                    <td class="style12" colspan="3">
                        <asp:TextBox ID="txtReferenciasBancarias" runat="server" TabIndex="280" style="text-transform: uppercase;" 
                            ToolTip="Ingrese el número de cuenta de referencia del cliente" 
                            MaxLength="200"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style11">
                        &nbsp;</td>
                    <td class="style11">
                        &nbsp;</td>
                    <td class="style11">
                        &nbsp;</td>
                    <td class="style11">
                        &nbsp;</td>
                    <td class="style11">
                        &nbsp;</td>
                    <td class="style11" colspan="3">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style11">
                        Estado civil:</td>
                    <td class="style11">
                        <asp:DropDownList ID="cbEstadoCivil" runat="server" 
                            ToolTip="Seleccione el estado civil del cliente" TabIndex="290">
                            <asp:ListItem Value="S">Soltero(a)</asp:ListItem>
                            <asp:ListItem Value="C">Casado(a)</asp:ListItem>
                            <asp:ListItem Value="U">Unido(a)</asp:ListItem>
                            <asp:ListItem Value="D">Divorciado(a)</asp:ListItem>
                            <asp:ListItem Value="V">Viudo(a)</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="style11">
                        Nacionalidad:</td>
                    <td class="style11">
                        <asp:TextBox ID="txtNacionalidad" runat="server" TabIndex="295" 
                            
                            ToolTip="Ingrese la nacionalidad del cliente" MaxLength="40">Guatemalteco</asp:TextBox>
                        </td>
                    <td class="style11">
                        &nbsp;</td>
                    <td class="style11" colspan="3">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style11">
                        Tiene vehículo:</td>
                    <td class="style11">
                        <asp:DropDownList ID="cbTieneVehiculo" runat="server" 
                            ToolTip="Seleccione si el cliente posee vehículo" TabIndex="300">
                            <asp:ListItem Value="N">No</asp:ListItem>
                            <asp:ListItem Value="S">Sí</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="style11">
                        Marca y modelo:</td>
                    <td class="style11">
                        <asp:TextBox ID="txtMarcaModelo" runat="server" TabIndex="310" style="text-transform: uppercase;" 
                            ToolTip="Ingrese la marca y modelo del vehículo, por ejemplo: Honda Civic 2009" 
                            MaxLength="40"></asp:TextBox>
                        </td>
                    <td class="style11">
                        Placas del Vehículo:</td>
                    <td class="style11" colspan="3">
                        <asp:TextBox ID="txtPlacas" runat="server" TabIndex="315" style="text-transform: uppercase;"
                            ToolTip="Ingrese el número de placas del vehículo del cliente" 
                            MaxLength="40"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style11">
                        <span class="style10">Vive en</span>:</td>
                    <td class="style11">
                        <asp:RadioButton ID="rbCasaFamiliares" runat="server" GroupName="vivienda" 
                            TabIndex="318" Text="Casa de familiares" 
                            
                            
                            ToolTip="Seleccione esta casilla si el cliente vive en casa de familiares" />
                    </td>
                    <td class="style11">
                        <asp:RadioButton ID="rbCasaPropia" runat="server" GroupName="vivienda" 
                            TabIndex="320" Text="Casa propia" 
                            ToolTip="Seleccione esta casilla si el cliente posee casa propia" />
                        </td>
                    <td class="style11">
                        <asp:RadioButton ID="rbCasaAlquilada" runat="server" GroupName="vivienda" 
                            TabIndex="330" Text="Casa alquilada" 
                            ToolTip="Seleccione esta casilla si el cliente vive en casa alquilada" />
                        </td>
                    <td class="style11">
                        Pago mensual:</td>
                    <td class="style11" colspan="3">
                        <asp:TextBox ID="txtPagoMensual" runat="server" TabIndex="335" style="text-transform: uppercase;" 
                            ToolTip="Ingrese el monto del pago mensual de vivienda que realiza el cliente" 
                            MaxLength="40"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style11">
                        Tiempo de residir:</td>
                    <td class="style11">
                        <asp:TextBox ID="txtTiempoResidir" runat="server" TabIndex="340" style="text-transform: uppercase;" 
                            ToolTip="Ingrese el tiempo que tiene el cliente de residir en su vivienda, por ejemplo: 7años" 
                            MaxLength="40"></asp:TextBox>
                    </td>
                    <td class="style11">
                        Financiera:</td>
                    <td class="style11">
                        <asp:TextBox ID="txtFinancieraVivienda" runat="server" TabIndex="345" style="text-transform: uppercase;" 
                            ToolTip="Ingrese la financiera a la que el cliente paga para amortiguar la deuda de su vivienda" 
                            MaxLength="40"></asp:TextBox>
                        </td>
                    <td class="style11">
                        Número de Contador:</td>
                    <td class="style11" colspan="3">
                        <asp:TextBox ID="txtContador" runat="server" TabIndex="350" style="text-transform: uppercase;" 
                            ToolTip="Ingrese el número de contador de energía eléctrica de la vivienda del cliente" 
                            MaxLength="40"></asp:TextBox>
                    </td>
                </tr>
            </table>

            <table  align="center" id="tblJefe" runat="server" visible="false">
                <tr>
                    <td class="style13" colspan="8" style="text-align: center">Jefe inmediato</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Primer nombre</td>
                    <td>Segundo nombre</td>
                    <td>Tercer nombre</td>
                    <td>Primer apellido</td>
                    <td>Segundo apellido</td>
                    <td>Apellido casada</td>
                    <td>Celular</td>
                </tr>
                <tr>
                    <td>Jefe</td>
                    <td><asp:TextBox ID="txtJefePrimerNombre" runat="server" TabIndex="352" style="text-transform: uppercase;" ToolTip="Primer nombre del jefe inmediato, ejemplo Luis" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtJefeSegundoNombre" runat="server" TabIndex="354" style="text-transform: uppercase;" ToolTip="Segundo nombre del jefe inmediato, ejemplo Carlos" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtJefeTercerNombre" runat="server" TabIndex="356" style="text-transform: uppercase;" ToolTip="Tercer nombre del jefe inmediato, ejemplo Jorge" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtJefePrimerApellido" runat="server" TabIndex="358" style="text-transform: uppercase;" ToolTip="Primer apellido del jefe inmediato, ejemplo Pérez" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtJefeSegundoApellido" runat="server" TabIndex="360" style="text-transform: uppercase;" ToolTip="Segundo apellido del jefe inmediato, ejemplo López" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtJefeApellidoCasada" runat="server" TabIndex="362" style="text-transform: uppercase;" ToolTip="Aplleido de casada del jefe inmediato, ejemplo Juárez" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtJefeCelular" runat="server" TabIndex="364" style="text-transform: uppercase;" ToolTip="Celular, ejemplo 30412569" MaxLength="8" Width="113px"></asp:TextBox></td>
                </tr>

            </table>

            <table align="center" id="tblReferenciasInter" runat="server" visible="false">
                <tr>
                    <td class="style13" colspan="8" style="text-align: center">
                        Indispensable 2 referencias personales</td>
                </tr>
                <tr>
                    <td colspan="3">
                        Nombres completos</td>
                    <td class="style6">
                        Tel. Residencia</td>
                    <td>
                        Tel. Trabajo</td>
                    <td colspan="3">
                        Celular</td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:TextBox ID="txtReferenciaPersonal1" runat="server" TabIndex="380" style="text-transform: uppercase;"
                            ToolTip="Ingrese aquí el nombre de la primera referencia personal" 
                            Width="400px" MaxLength="100"></asp:TextBox>
                    </td>
                    <td class="style6">
                        <asp:TextBox ID="txtReferenciaPersonalResidencia1" runat="server" TabIndex="390" style="text-transform: uppercase;" 
                            ToolTip="Ingrese aquí el teléfono de residencia de la primera referencia personal" 
                            MaxLength="40"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtReferenciaPersonalTrabajo1" runat="server" TabIndex="400" style="text-transform: uppercase;" 
                            ToolTip="Ingrese aquí el teléfono del trabajo de la primera referencia personal" 
                            Width="120px" MaxLength="40"></asp:TextBox>
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="txtReferenciaPersonalCelular1" runat="server" TabIndex="410" style="text-transform: uppercase;" 
                            ToolTip="Ingrese aquí el teléfono celular de la primer referencia personal del cliente" 
                            MaxLength="40"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:TextBox ID="txtReferenciaPersonal2" runat="server" TabIndex="420" style="text-transform: uppercase;"
                            ToolTip="Ingrese aquí el nombre de la segunda referencia personal" 
                            Width="400px" MaxLength="100"></asp:TextBox>
                    </td>
                    <td class="style6">
                        <asp:TextBox ID="txtReferenciaPersonalResidencia2" runat="server" TabIndex="430" style="text-transform: uppercase;" 
                            ToolTip="Ingrese aquí el teléfono de residencia de la segunda referencia personal" 
                            MaxLength="40"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtReferenciaPersonalTrabajo2" runat="server" TabIndex="440" style="text-transform: uppercase;" 
                            ToolTip="Ingrese aquí el teléfono del trabajo de la segunda referencia personal" 
                            Width="120px" MaxLength="40"></asp:TextBox>
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="txtReferenciaPersonalCelular2" runat="server" TabIndex="450" style="text-transform: uppercase;" 
                            ToolTip="Ingrese aquí el teléfono de celular de la segunda referencia personal" 
                            MaxLength="40"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style13" colspan="8" style="text-align: center">Referencias comerciales (indispensable 2, para empresas individuales)</td>
                </tr>
                <tr>
                    <td colspan="3">Nombres completos</td>
                    <td class="style6">Tel. Residencia</td>
                    <td>Tel. Trabajo</td>
                    <td colspan="3">Celular</td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:TextBox ID="txtReferenciaComercial1" runat="server" TabIndex="460" style="text-transform: uppercase;"
                            ToolTip="Ingrese aquí el nombre de la primera referencia comercial" 
                            Width="400px" MaxLength="100"></asp:TextBox>
                    </td>
                    <td class="style6">
                        <asp:TextBox ID="txtReferenciaComercialTelefono1" runat="server" TabIndex="470" style="text-transform: uppercase;" 
                            ToolTip="Ingrese aquí el teléfono de la primera referencia comercial" 
                            MaxLength="40"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtReferenciaComercialFax1" runat="server" TabIndex="480" style="text-transform: uppercase;" 
                            ToolTip="Ingrese aquí el FAX de la primera referencia comercial" 
                            Width="120px" MaxLength="40"></asp:TextBox>
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="txtReferenciaComercialCelular1" runat="server" TabIndex="490" style="text-transform: uppercase;" 
                            ToolTip="Ingrese aquí el teléfono celular de la primera referencia comercial" 
                            MaxLength="40"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="style1000">
                        <asp:TextBox ID="txtReferenciaComercial2" runat="server" TabIndex="500" style="text-transform: uppercase;"
                            ToolTip="Ingrese aquí el nombre de la segunda referencia comercial" 
                            Width="400px" MaxLength="100"></asp:TextBox>
                    </td>
                    <td class="style1000">
                        <asp:TextBox ID="txtReferenciaComercialTelefono2" runat="server" TabIndex="510" style="text-transform: uppercase;" 
                            ToolTip="Ingrese aquí el teléfono de la segunda referencia comercial" 
                            MaxLength="40"></asp:TextBox>
                    </td>
                    <td class="style1000">
                        <asp:TextBox ID="txtReferenciaComercialFax2" runat="server" TabIndex="520" style="text-transform: uppercase;" 
                            ToolTip="Ingrese aquí el FAX de la segunda referencia comercial" 
                            Width="120px" MaxLength="40"></asp:TextBox>
                    </td>
                    <td class="style1000" colspan="3">
                        <asp:TextBox ID="txtReferenciaComercialCelular2" runat="server" TabIndex="530" style="text-transform: uppercase;" 
                            ToolTip="Ingrese aquí el teléfono celular de la segunda referencia comercial" 
                            MaxLength="40"></asp:TextBox>
                    </td>
                </tr>
            </table>

            <table align="center" id="tblConyugue" runat="server" visible="false">
                <tr>
                    <td class="style13" colspan="8" style="text-align: center">Cónyugue</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Primer nombre</td>
                    <td>Segundo nombre</td>
                    <td>Tercer nombre</td>
                    <td>Primer apellido</td>
                    <td>Segundo apellido</td>
                    <td>Apellido casada</td>
                    <td>Celular</td>
                </tr>
                <tr>
                    <td>Cónygue</td>
                    <td><asp:TextBox ID="txtConyuguePrimerNombre" runat="server" TabIndex="532" style="text-transform: uppercase;" ToolTip="Primer nombre del cónyugue, ejemplo Luis" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtConyugueSegundoNombre" runat="server" TabIndex="533" style="text-transform: uppercase;" ToolTip="Segundo nombre del cónyugue, ejemplo Carlos" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtConyugueTercerNombre" runat="server" TabIndex="534" style="text-transform: uppercase;" ToolTip="Tercer nombre del cónyugue, ejemplo Jorge" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtConyuguePrimerApellido" runat="server" TabIndex="536" style="text-transform: uppercase;" ToolTip="Primer apellido del cónyugue, ejemplo Pérez" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtConyugueSegundoApellido" runat="server" TabIndex="537" style="text-transform: uppercase;" ToolTip="Segundo apellido del cónyugue, ejemplo López" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtConyugueApellidoCasada" runat="server" TabIndex="538" style="text-transform: uppercase;" ToolTip="Aplleido casada del cónyugue, ejemplo Juárez" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtConyugeTelefono" runat="server" TabIndex="539" style="text-transform: uppercase;" ToolTip="Ingrese el número de celular del cónyugue" MaxLength="8" Width="113px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>DPI:</td>
                    <td><asp:TextBox ID="txtConyugeID" runat="server" TabIndex="540" style="text-transform: uppercase;" ToolTip="DPI del cónyugue, ejemplo: 2531-10324-0101" MaxLength="40" Width="113px"></asp:TextBox></td>
                    <td>NIT Cónyugue:</td>
                    <td><asp:TextBox ID="txtConyugueNIT" runat="server" TabIndex="541" style="text-transform: uppercase;" ToolTip="NIT del cónyugue" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td>Fecha nacimiento:</td>
                    <td>
                        <dx:ASPxDateEdit ID="ConyugueFechaNac" runat="server" Theme="SoftOrange"  DisplayFormatString="dd/MM/yyyy" CssClass="DateEditStyle" Width="113px" TabIndex="542" ToolTip="Fecha de nacimiento del cónyugue">
                            <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" ShowWeekNumbers="False" >
                                <Style Wrap="True"></Style>
                                <MonthGridPaddings Padding="3px" />
                                <DayStyle Font-Size="11px">
                                <Paddings Padding="3px" />
                                </DayStyle>
                            </CalendarProperties>
                        </dx:ASPxDateEdit>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Empresa:</td>
                    <td>
                        <asp:TextBox ID="txtEmpresaConyugue" runat="server" TabIndex="543" style="text-transform: uppercase;" ToolTip="Ingrese el nombre de la empresa para la que labora el cónyugue" MaxLength="100" Width="113px"></asp:TextBox>
                    </td>
                    <td>Cargo Cónyugue:</td>
                    <td>
                        <asp:TextBox ID="txtCargoConyugue" runat="server" TabIndex="544" style="text-transform: uppercase;" ToolTip="Ingrese el cargo que ocupa el cónyugue en la empresa para la que labora" MaxLength="30" Width="113px"></asp:TextBox>
                    </td>
                    <td>Ingresos:</td>
                    <td>
                        <asp:TextBox ID="txtIngresosConyugue" runat="server" TabIndex="546" style="text-transform: uppercase;" ToolTip="Ingrese el monto de los ingresos mensuales del cónyugue" MaxLength="30" Width="113px"></asp:TextBox>
                    </td>
                    <td>Tel. Empresa:</td>
                    <td>
                        <asp:TextBox ID="txtTelefonoEmpresaConyugue" runat="server" TabIndex="548" style="text-transform: uppercase;" ToolTip="Ingrese el teléfono de la empresa para la que labora el cónyugue" MaxLength="8" Width="113px"></asp:TextBox>
                    </td>
                </tr>
            </table>


            <table align="center" id="tblReferenciasAtid" runat="server" visible="false">
                <tr>
                    <td class="style13" colspan="8" style="text-align: center">Referencias familiares y personales</td>
                </tr>
                <tr>
                    <td>Tipo</td>
                    <td>Primer nombre</td>
                    <td>Segundo nombre</td>
                    <td>Tercer nombre</td>
                    <td>Primer apellido</td>
                    <td>Segundo apellido</td>
                    <td>Apellido casada</td>
                    <td>Celular</td>
                </tr>
                <tr>
                    <td style="font-weight: bold">Familiar</td>
                    <td><asp:TextBox ID="txtFamiliar1PrimerNombre" runat="server" TabIndex="550" style="text-transform: uppercase;" ToolTip="Primer nombre, ejemplo Luis" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtFamiliar1SegundoNombre" runat="server" TabIndex="555" style="text-transform: uppercase;" ToolTip="Segundo nombre, ejemplo Carlos" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtFamiliar1TercerNombre" runat="server" TabIndex="560" style="text-transform: uppercase;" ToolTip="Tercer nombre, ejemplo Jorge" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtFamiliar1PrimerApellido" runat="server" TabIndex="565" style="text-transform: uppercase;" ToolTip="Primer apellido, ejemplo Pérez" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtFamiliar1SegundoApellido" runat="server" TabIndex="568" style="text-transform: uppercase;" ToolTip="Segundo apellido, ejemplo López" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtFamiliar1ApellidoCasada" runat="server" TabIndex="569" style="text-transform: uppercase;" ToolTip="Aplleido casada, ejemplo Juárez" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtFamiliar1Celular" runat="server" TabIndex="570" style="text-transform: uppercase;" ToolTip="Celular, ejemplo 30412569" MaxLength="8" Width="113px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Tipo:</td>
                    <td><asp:DropDownList ID="cbFamiliar1Tipo" runat="server" Width="113px" TabIndex="571" ToolTip="Seleccione el tipo de referencia" Font-Size="X-Small" ></asp:DropDownList></td>
                    <td>Empresa labora:</td>
                    <td><asp:TextBox ID="txtFamiliar1Empresa" runat="server" TabIndex="572" style="text-transform: uppercase;" ToolTip="Nombre de la empresa donde labora la referencia" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td>Dirección trabajo:</td>
                    <td colspan="2"><asp:TextBox ID="txtFamiliar1EmpresaDireccion" runat="server" TabIndex="573" style="text-transform: uppercase;" ToolTip="Dirección del lugar de trabajo de la referencia" MaxLength="50" Width="237px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Departamento:</td>
                    <td>
                        <asp:DropDownList ID="cbFamiliar1EmpresaDepartamento" runat="server" Width="113px" 
                            TabIndex="574" ToolTip="Seleccione el departamento de la dirección de la empresa" AutoPostBack="True" 
                            onselectedindexchanged="cbFamiliar1EmpresaDepartamento_SelectedIndexChanged" Font-Size="X-Small">
                        </asp:DropDownList>
                    </td>
                    <td>Municipio</td>
                    <td>
                        <asp:DropDownList ID="cbFamiliar1EmpresaMunicipio" runat="server" Width="113px" Font-Size="X-Small"
                            TabIndex="575" ToolTip="Seleccione el municipio de la dirección de la empresa" >
                        </asp:DropDownList>
                    </td>
                    <td>Teléfono trabajo:</td>
                    <td colspan="2"><asp:TextBox ID="txtFamiliar1EmpresaTelefono" runat="server" TabIndex="576" style="text-transform: uppercase;" ToolTip="Teléfono de la empresa donde labora la referencia" MaxLength="8" Width="237px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Dirección casa:</td>
                    <td colspan="2"><asp:TextBox ID="txtFamiliar1CasaDireccion" runat="server" TabIndex="577" style="text-transform: uppercase;" ToolTip="Dirección de la casa de la referencia" MaxLength="50" Width="237px"></asp:TextBox></td>
                    <td>Departamento:</td>
                    <td>
                        <asp:DropDownList ID="cbFamiliar1CasaDepartamento" runat="server" Width="113px" Font-Size="X-Small" 
                            TabIndex="578" ToolTip="Seleccione el departamento de la dirección de la casa" AutoPostBack="True" 
                            onselectedindexchanged="cbFamiliar1CasaDepartamento_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>Municipio:</td>
                    <td>
                        <asp:DropDownList ID="cbFamiliar1CasaMunicipio" runat="server" Width="113px" TabIndex="579" ToolTip="Seleccione el municipio de la dirección de la casa" Font-Size="X-Small" ></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>Teléfono casa:</td>
                    <td colspan="2"><asp:TextBox ID="txtFamiliar1CasaTelefono" runat="server" TabIndex="580" style="text-transform: uppercase;" ToolTip="Teléfono de la casa de la referencia" MaxLength="8" Width="237px"></asp:TextBox></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="8">&nbsp;</td>
                </tr>
                <tr>
                    <td>Tipo</td>
                    <td>Primer nombre</td>
                    <td>Segundo nombre</td>
                    <td>Tercer nombre</td>
                    <td>Primer apellido</td>
                    <td>Segundo apellido</td>
                    <td>Apellido casada</td>
                    <td>Celular</td>
                </tr>
                <tr>
                    <td style="font-weight: bold">Familiar</td>
                    <td><asp:TextBox ID="txtFamiliar2PrimerNombre" runat="server" TabIndex="585" style="text-transform: uppercase;" ToolTip="Primer nombre, ejemplo Luis" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtFamiliar2SegundoNombre" runat="server" TabIndex="590" style="text-transform: uppercase;" ToolTip="Segundo nombre, ejemplo Carlos" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtFamiliar2TercerNombre" runat="server" TabIndex="595" style="text-transform: uppercase;" ToolTip="Tercer nombre, ejemplo Jorge" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtFamiliar2PrimerApellido" runat="server" TabIndex="600" style="text-transform: uppercase;" ToolTip="Primer apellido, ejemplo Pérez" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtFamiliar2SegundoApellido" runat="server" TabIndex="603" style="text-transform: uppercase;" ToolTip="Segundo apellido, ejemplo López" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtFamiliar2ApellidoCasada" runat="server" TabIndex="604" style="text-transform: uppercase;" ToolTip="Aplleido casada, ejemplo Juárez" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtFamiliar2Celular" runat="server" TabIndex="605" style="text-transform: uppercase;" ToolTip="Celular, ejemplo 30412569" MaxLength="8" Width="113px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Tipo:</td>
                    <td><asp:DropDownList ID="cbFamiliar2Tipo" runat="server" Width="113px" TabIndex="606" ToolTip="Seleccione el tipo de referencia" Font-Size="X-Small" ></asp:DropDownList></td>
                    <td>Empresa labora:</td>
                    <td><asp:TextBox ID="txtFamiliar2Empresa" runat="server" TabIndex="608" style="text-transform: uppercase;" ToolTip="Nombre de la empresa donde labora la referencia" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td>Dirección trabajo:</td>
                    <td colspan="2"><asp:TextBox ID="txtFamiliar2EmpresaDireccion" runat="server" TabIndex="609" style="text-transform: uppercase;" ToolTip="Dirección del lugar de trabajo de la referencia" MaxLength="50" Width="237px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Departamento:</td>
                    <td>
                        <asp:DropDownList ID="cbFamiliar2EmpresaDepartamento" runat="server" Width="113px" Font-Size="X-Small" 
                            TabIndex="610" ToolTip="Seleccione el departamento de la dirección de la empresa" AutoPostBack="True" 
                            onselectedindexchanged="cbFamiliar2EmpresaDepartamento_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>Municipio</td>
                    <td>
                        <asp:DropDownList ID="cbFamiliar2EmpresaMunicipio" runat="server" Width="113px" TabIndex="611" ToolTip="Seleccione el municipio de la dirección de la empresa" Font-Size="X-Small" ></asp:DropDownList>
                    </td>
                    <td>Teléfono trabajo:</td>
                    <td colspan="2"><asp:TextBox ID="txtFamiliar2EmpresaTelefono" runat="server" TabIndex="612" style="text-transform: uppercase;" ToolTip="Teléfono de la empresa donde labora la referencia" MaxLength="8" Width="237px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Dirección casa:</td>
                    <td colspan="2"><asp:TextBox ID="txtFamiliar2CasaDireccion" runat="server" TabIndex="613" style="text-transform: uppercase;" ToolTip="Dirección de la casa de la referencia" MaxLength="50" Width="237px"></asp:TextBox></td>
                    <td>Departamento:</td>
                    <td>
                        <asp:DropDownList ID="cbFamiliar2CasaDepartamento" runat="server" Width="113px" Font-Size="X-Small" 
                            TabIndex="614" ToolTip="Seleccione el departamento de la dirección de la casa" AutoPostBack="True" 
                            onselectedindexchanged="cbFamiliar2CasaDepartamento_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>Municipio:</td>
                    <td>
                        <asp:DropDownList ID="cbFamiliar2CasaMunicipio" runat="server" Width="113px" TabIndex="615" ToolTip="Seleccione el municipio de la dirección de la casa" Font-Size="X-Small" ></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>Teléfono casa:</td>
                    <td colspan="2"><asp:TextBox ID="txtFamiliar2CasaTelefono" runat="server" TabIndex="616" style="text-transform: uppercase;" ToolTip="Teléfono de la casa de la referencia" MaxLength="8" Width="237px"></asp:TextBox></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="8">&nbsp;</td>
                </tr>
                <tr>
                    <td>Tipo</td>
                    <td>Primer nombre</td>
                    <td>Segundo nombre</td>
                    <td>Tercer nombre</td>
                    <td>Primer apellido</td>
                    <td>Segundo apellido</td>
                    <td>Apellido casada</td>
                    <td>Celular</td>
                </tr>
                <tr>
                    <td style="font-weight: bold">Personal</td>
                    <td><asp:TextBox ID="txtPersonal1PrimerNombre" runat="server" TabIndex="620" style="text-transform: uppercase;" ToolTip="Primer nombre, ejemplo Luis" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtPersonal1SegundoNombre" runat="server" TabIndex="625" style="text-transform: uppercase;" ToolTip="Segundo nombre, ejemplo Carlos" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtPersonal1TercerNombre" runat="server" TabIndex="630" style="text-transform: uppercase;" ToolTip="Tercer nombre, ejemplo Jorge" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtPersonal1PrimerApellido" runat="server" TabIndex="635" style="text-transform: uppercase;" ToolTip="Primer apellido, ejemplo Pérez" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtPersonal1SegundoApellido" runat="server" TabIndex="638" style="text-transform: uppercase;" ToolTip="Segundo apellido, ejemplo López" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtPersonal1ApellidoCasada" runat="server" TabIndex="639" style="text-transform: uppercase;" ToolTip="Aplleido casada, ejemplo Juárez" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtPersonal1Celular" runat="server" TabIndex="640" style="text-transform: uppercase;" ToolTip="Celular, ejemplo 30412569" MaxLength="8" Width="113px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Tipo:</td>
                    <td><asp:DropDownList ID="cbPersonal1Tipo" runat="server" Width="113px" TabIndex="641" ToolTip="Seleccione el tipo de referencia" Font-Size="X-Small" ></asp:DropDownList></td>
                    <td>Empresa labora:</td>
                    <td><asp:TextBox ID="txtPersonal1Empresa" runat="server" TabIndex="642" style="text-transform: uppercase;" ToolTip="Nombre de la empresa donde labora la referencia" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td>Dirección trabajo:</td>
                    <td colspan="2"><asp:TextBox ID="txtPersonal1EmpresaDireccion" runat="server" TabIndex="643" style="text-transform: uppercase;" ToolTip="Dirección del lugar de trabajo de la referencia" MaxLength="50" Width="237px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Departamento:</td>
                    <td>
                        <asp:DropDownList ID="cbPersonal1EmpresaDepartamento" runat="server" Width="113px" Font-Size="X-Small" 
                            TabIndex="644" ToolTip="Seleccione el departamento de la dirección de la empresa" AutoPostBack="True" 
                            onselectedindexchanged="cbPersonal1EmpresaDepartamento_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>Municipio</td>
                    <td>
                        <asp:DropDownList ID="cbPersonal1EmpresaMunicipio" runat="server" Width="113px" TabIndex="645" ToolTip="Seleccione el municipio de la dirección de la empresa" Font-Size="X-Small" ></asp:DropDownList>
                    </td>
                    <td>Teléfono trabajo:</td>
                    <td colspan="2"><asp:TextBox ID="txtPersonal1EmpresaTelefono" runat="server" TabIndex="646" style="text-transform: uppercase;" ToolTip="Teléfono de la empresa donde labora la referencia" MaxLength="8" Width="237px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Dirección casa:</td>
                    <td colspan="2"><asp:TextBox ID="txtPersonal1CasaDireccion" runat="server" TabIndex="647" style="text-transform: uppercase;" ToolTip="Dirección de la casa de la referencia" MaxLength="50" Width="237px"></asp:TextBox></td>
                    <td>Departamento:</td>
                    <td>
                        <asp:DropDownList ID="cbPersonal1CasaDepartamento" runat="server" Width="113px" Font-Size="X-Small" 
                            TabIndex="648" ToolTip="Seleccione el departamento de la dirección de la casa" AutoPostBack="True" 
                            onselectedindexchanged="cbPersonal1CasaDepartamento_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>Municipio:</td>
                    <td>
                        <asp:DropDownList ID="cbPersonal1CasaMunicipio" runat="server" Width="113px" TabIndex="649" ToolTip="Seleccione el municipio de la dirección de la casa" Font-Size="X-Small" ></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>Teléfono casa:</td>
                    <td colspan="2"><asp:TextBox ID="txtPersonal1CasaTelefono" runat="server" TabIndex="650" style="text-transform: uppercase;" ToolTip="Teléfono de la casa de la referencia" MaxLength="8" Width="237px"></asp:TextBox></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="8">&nbsp;</td>
                </tr>

                <tr>
                    <td>Tipo</td>
                    <td>Primer nombre</td>
                    <td>Segundo nombre</td>
                    <td>Tercer nombre</td>
                    <td>Primer apellido</td>
                    <td>Segundo apellido</td>
                    <td>Apellido casada</td>
                    <td>Celular</td>
                </tr>
                <tr>
                    <td style="font-weight: bold">Personal</td>
                    <td><asp:TextBox ID="txtPersonal2PrimerNombre" runat="server" TabIndex="655" style="text-transform: uppercase;" ToolTip="Primer nombre, ejemplo Luis" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtPersonal2SegundoNombre" runat="server" TabIndex="660" style="text-transform: uppercase;" ToolTip="Segundo nombre, ejemplo Carlos" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtPersonal2TercerNombre" runat="server" TabIndex="665" style="text-transform: uppercase;" ToolTip="Tercer nombre, ejemplo Jorge" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtPersonal2PrimerApellido" runat="server" TabIndex="670" style="text-transform: uppercase;" ToolTip="Primer apellido, ejemplo Pérez" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtPersonal2SegundoApellido" runat="server" TabIndex="673" style="text-transform: uppercase;" ToolTip="Segundo apellido, ejemplo López" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtPersonal2ApellidoCasada" runat="server" TabIndex="674" style="text-transform: uppercase;" ToolTip="Aplleido casada, ejemplo Juárez" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtPersonal2Celular" runat="server" TabIndex="675" style="text-transform: uppercase;" ToolTip="Celular, ejemplo 30412569" MaxLength="8" Width="113px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Tipo:</td>
                    <td><asp:DropDownList ID="cbPersonal2Tipo" runat="server" Width="113px" TabIndex="676" ToolTip="Seleccione el tipo de referencia" Font-Size="X-Small" ></asp:DropDownList></td>
                    <td>Empresa labora:</td>
                    <td><asp:TextBox ID="txtPersonal2Empresa" runat="server" TabIndex="677" style="text-transform: uppercase;" ToolTip="Nombre de la empresa donde labora la referencia" MaxLength="50" Width="113px"></asp:TextBox></td>
                    <td>Dirección trabajo:</td>
                    <td colspan="2"><asp:TextBox ID="txtPersonal2EmpresaDireccion" runat="server" TabIndex="678" style="text-transform: uppercase;" ToolTip="Dirección del lugar de trabajo de la referencia" MaxLength="50" Width="237px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Departamento:</td>
                    <td>
                        <asp:DropDownList ID="cbPersonal2EmpresaDepartamento" runat="server" Width="113px" Font-Size="X-Small" 
                            TabIndex="679" ToolTip="Seleccione el departamento de la dirección de la empresa" AutoPostBack="True" 
                            onselectedindexchanged="cbPersonal2EmpresaDepartamento_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>Municipio</td>
                    <td>
                        <asp:DropDownList ID="cbPersonal2EmpresaMunicipio" runat="server" Width="113px" TabIndex="680" ToolTip="Seleccione el municipio de la dirección de la empresa" Font-Size="X-Small" ></asp:DropDownList>
                    </td>
                    <td>Teléfono trabajo:</td>
                    <td colspan="2"><asp:TextBox ID="txtPersonal2EmpresaTelefono" runat="server" TabIndex="681" style="text-transform: uppercase;" ToolTip="Teléfono de la empresa donde labora la referencia" MaxLength="8" Width="237px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Dirección casa:</td>
                    <td colspan="2"><asp:TextBox ID="txtPersonal2CasaDireccion" runat="server" TabIndex="682" style="text-transform: uppercase;" ToolTip="Dirección de la casa de la referencia" MaxLength="50" Width="237px"></asp:TextBox></td>
                    <td>Departamento:</td>
                    <td>
                        <asp:DropDownList ID="cbPersonal2CasaDepartamento" runat="server" Width="113px" Font-Size="X-Small" 
                            TabIndex="683" ToolTip="Seleccione el departamento de la dirección de la casa" AutoPostBack="True" 
                            onselectedindexchanged="cbPersonal2CasaDepartamento_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>Municipio:</td>
                    <td>
                        <asp:DropDownList ID="cbPersonal2CasaMunicipio" runat="server" Width="113px" TabIndex="684" ToolTip="Seleccione el municipio de la dirección de la casa" Font-Size="X-Small" ></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>Teléfono casa:</td>
                    <td colspan="2"><asp:TextBox ID="txtPersonal2CasaTelefono" runat="server" TabIndex="685" style="text-transform: uppercase;" ToolTip="Teléfono de la casa de la referencia" MaxLength="8" Width="237px"></asp:TextBox></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>


            <table class="style1001" runat="server" visible="false">
                <tr>
                    <td>
                        Nombre Cónyugue:</td>
                    <td colspan="3">
                        <asp:TextBox ID="txtConyugeNombre" runat="server" TabIndex="352" style="text-transform: uppercase;"
                            ToolTip="Ingrese el nombre del cónyugue del cliente" 
                            Width="450px" MaxLength="100"></asp:TextBox>
                    </td>
                    <td>
                        Empresa Cónyugue:</td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Direcc.Empresa</td>
                    <td colspan="3">
                        <asp:TextBox ID="txtDireccionEmpresaConyugue" runat="server" TabIndex="357" style="text-transform: uppercase;"
                            ToolTip="Ingrese la dirección de la empresa para la que labora el cónyugue" 
                            Width="450px" MaxLength="400"></asp:TextBox>
                    </td>
                    <td>
                        Tel. Empresa:</td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Cargo que ocupa:</td>
                    <td>
                    </td>
                    <td>
                        Tiempo Laborar:</td>
                    <td>
                        <asp:TextBox ID="txtTiempoConyugue" runat="server" TabIndex="360" style="text-transform: uppercase;"
                            ToolTip="Ingrese en años el tiempo que tiene el cónyugue de laborar para la empresa" 
                            MaxLength="30"></asp:TextBox>
                    </td>
                    <td>
                        Ingreso Mensual:</td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Fecha Nacimiento:</td>
                    <td>
                <asp:DropDownList ID="cbDiaConyugue" runat="server" TabIndex="364" 
                    ToolTip="Seleccione el día de nacimiento del cónyugue">
                    <asp:ListItem>--</asp:ListItem>
                    <asp:ListItem Value="1">01</asp:ListItem>
                    <asp:ListItem Value="2">02</asp:ListItem>
                    <asp:ListItem Value="3">03</asp:ListItem>
                    <asp:ListItem Value="4">04</asp:ListItem>
                    <asp:ListItem Value="5">05</asp:ListItem>
                    <asp:ListItem Value="6">06</asp:ListItem>
                    <asp:ListItem Value="7">07</asp:ListItem>
                    <asp:ListItem Value="8">08</asp:ListItem>
                    <asp:ListItem Value="9">09</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="cbMesConyugue" runat="server" TabIndex="366" 
                    ToolTip="Seleccione el mes de nacimiento del cónyugue" Height="16px">
                    <asp:ListItem>--</asp:ListItem>
                    <asp:ListItem Value="1">Ene</asp:ListItem>
                    <asp:ListItem Value="2">Feb</asp:ListItem>
                    <asp:ListItem Value="3">Mar</asp:ListItem>
                    <asp:ListItem Value="4">Abr</asp:ListItem>
                    <asp:ListItem Value="5">May</asp:ListItem>
                    <asp:ListItem Value="6">Jun</asp:ListItem>
                    <asp:ListItem Value="7">Jul</asp:ListItem>
                    <asp:ListItem Value="8">Ago</asp:ListItem>
                    <asp:ListItem Value="9">Sep</asp:ListItem>
                    <asp:ListItem Value="10">Oct</asp:ListItem>
                    <asp:ListItem Value="11">Nov</asp:ListItem>
                    <asp:ListItem Value="12">Dic</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtAnioConyugue" runat="server" TabIndex="368" 
                    ToolTip="Ingrese aquí el año de nacimiento del cónyugue en formato de 4 dígitos, ejemplo:1970." 
                    Width="39px">2025</asp:TextBox>
                    </td>
                    <td>
                        DPI Cónyugue:</td>
                    <td>
                    </td>
                    <td>
                        Tel. Cónyuge:</td>
                    <td>
                    </td>
                </tr>
            </table>

        </li>
        <li>
            <table align="center">
                <tr>
                    <td style="text-align: center" class="style11">
                        &nbsp;
                        <asp:LinkButton ID="lkLimpiar" runat="server" 
                            ToolTip="Haga clic aquí para limpiar los datos de la página e ingresar un cliente nuevo" 
                            TabIndex="910" onclick="lkLimpiar_Click">Limpiar</asp:LinkButton>
                        &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton ID="lkGrabar" runat="server" 
                        ToolTip="Haga clic aquí para grabar los datos del cliente" TabIndex="920" 
                            onclick="lkGrabar_Click">Grabar</asp:LinkButton>
                    </td>
                </tr>
            </table>
        </li>
    </ul>
    </div> 
        <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server" TabIndex="998"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" TabIndex="999"></asp:Label>
                </td>
            </tr>
        </table>
</div> 
</asp:Content>
