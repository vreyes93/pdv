﻿using System;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Reflection;
using System.Globalization;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using MF_Clases;

namespace PuntoDeVenta
{
    public partial class clientes : BasePage
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            base.LogActivity();
            if (!IsPostBack)
            {
                HookOnFocus(this.Page as Control);
                
                ViewState["url"] = Convert.ToString(Session["Url"]);
                ViewState["urlPrecios"] = Convert.ToString(Session["UrlPrecios"]);

                //para colocar al cliente el atributo de exento, por ejemplo para la venta de vehiculos
                if (Convert.ToString(Session["Tienda"]).Equals("F01"))
                    trExento.Visible = true;

                CargarTiposFinancieras();
                CargarDepartamentos();
                CargarProfesiones();
                CargarBancos();
                CargarPuestos();
                CargarTiposReferencia();

                if (Session["Cliente"] == null)
                {
                    limpiar();
                    cbTipo.Focus();
                    habilitarBusquedaCliente();
                }
                else
                {
                    if (!string.IsNullOrEmpty(Session["Cliente"].ToString()))
                    {
                        CargarCliente("C", Convert.ToString(Session["Cliente"]), "I");
                    }
                    else {
                        limpiar();
                        cbTipo.Focus();
                        habilitarBusquedaCliente();
                    }
                }

            }
            
            Page.ClientScript.RegisterStartupScript(
                typeof(clientes),
                "ScriptDoFocus",
                SCRIPT_DOFOCUS.Replace("REQUEST_LASTFOCUS", Request["__LASTFOCUS"]),
                true);
        }

        void CargarProfesiones()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveProfesiones().ToList();

            wsPuntoVenta.Profesiones item = new wsPuntoVenta.Profesiones();
            item.Profesion = "";
            q.Add(item);

            cbProfesion.DataSource = q;
            cbProfesion.DataTextField = "Profesion";
            cbProfesion.DataValueField = "Profesion";
            cbProfesion.DataBind();
        }

        void CargarPuestos()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelvePuestos().ToList();

            wsPuntoVenta.Puestos item = new wsPuntoVenta.Puestos();
            item.Puesto = "";
            q.Add(item);

            cbPuesto.DataSource = q;
            cbPuesto.DataTextField = "Puesto";
            cbPuesto.DataValueField = "Puesto";
            cbPuesto.DataBind();
        }

        void CargarBancos()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveBancosInterconsumo().ToList();

            wsPuntoVenta.Bancos item = new wsPuntoVenta.Bancos();
            item.Banco = "";
            item.Descripcion = "";
            q.Add(item);

            cbBanco.DataSource = q;
            cbBanco.DataTextField = "Descripcion";
            cbBanco.DataValueField = "Descripcion";
            cbBanco.DataBind();
        }

        void CargarTiposReferencia()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveTiposReferenciaFamiliar();

            cbFamiliar1Tipo.DataSource = q;
            cbFamiliar1Tipo.DataTextField = "Descripcion";
            cbFamiliar1Tipo.DataValueField = "Codigo";
            cbFamiliar1Tipo.DataBind();

            cbFamiliar2Tipo.DataSource = q;
            cbFamiliar2Tipo.DataTextField = "Descripcion";
            cbFamiliar2Tipo.DataValueField = "Codigo";
            cbFamiliar2Tipo.DataBind();

            try
            {
                cbFamiliar1Tipo.SelectedValue = q[0].Codigo;
            }
            catch
            {
                //Nothing
            }
            try
            {
                cbFamiliar2Tipo.SelectedValue = q[1].Codigo;
            }
            catch
            {
                //Nothing
            }
            
            var q2 = ws.DevuelveTiposReferenciaPersonal();

            cbPersonal1Tipo.DataSource = q2;
            cbPersonal1Tipo.DataTextField = "Descripcion";
            cbPersonal1Tipo.DataValueField = "Codigo";
            cbPersonal1Tipo.DataBind();

            cbPersonal2Tipo.DataSource = q2;
            cbPersonal2Tipo.DataTextField = "Descripcion";
            cbPersonal2Tipo.DataValueField = "Codigo";
            cbPersonal2Tipo.DataBind();

            try
            {
                cbPersonal1Tipo.SelectedValue = q2[0].Codigo;
            }
            catch
            {
                //Nothing
            }
            try
            {
                cbPersonal2Tipo.SelectedValue = q2[1].Codigo;
            }
            catch
            {
                //Nothing
            }
        }

        private const string SCRIPT_DOFOCUS = @"window.setTimeout('DoFocus()', 1); function DoFocus() {
            try {
                document.getElementById('REQUEST_LASTFOCUS').focus();
            } catch (ex) {}
        }";

        private void HookOnFocus(Control CurrentControl)
        {
            //checks if control is one of TextBox, DropDownList, ListBox or Button
            if ((CurrentControl is TextBox) ||
                (CurrentControl is DropDownList) ||
                (CurrentControl is ListBox) ||
                (CurrentControl is Button))
            //adds a script which saves active control on receiving focus 
            //in the hidden field __LASTFOCUS.
                (CurrentControl as WebControl).Attributes.Add(
                   "onfocus",
                   "try{document.getElementById('__LASTFOCUS').value=this.id} catch(e) {}");
                //checks if the control has children
                if (CurrentControl.HasControls())
                    //if yes do them all recursively
                    foreach (Control CurrentChildControl in CurrentControl.Controls)
                        HookOnFocus(CurrentChildControl);
        }
        
        void CargarTiposFinancieras()
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                var qTiposVenta = ws.DevuelveTiposVentaSeleccion(Convert.ToString(Session["Tienda"])).ToList();
                
                cbTipo.DataSource = qTiposVenta;
                cbTipo.DataTextField = "Descripcion";
                cbTipo.DataValueField = "CodigoTipoVenta";
                cbTipo.DataBind();

                cbTipo.SelectedValue = "NR";
                CargaFinancieras();
            }
            catch (Exception ex)
            {
                string.Format("Error al cargar las promociones {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }
        
        void CargaFinancieras()
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                var qFinancieras = ws.DevuelveFinancierasSeleccion(cbTipo.SelectedValue).ToList();

                wsPuntoVenta.Financieras itemFinanciera = new wsPuntoVenta.Financieras();
                itemFinanciera.Financiera = 0;
                itemFinanciera.Nombre = "Seleccione financiera ...";
                itemFinanciera.prefijo = "";
                itemFinanciera.Estatus = "Activa";
                qFinancieras.Add(itemFinanciera);

                cbFinanciera.DataSource = qFinancieras;
                cbFinanciera.DataTextField = "Nombre";
                cbFinanciera.DataValueField = "Financiera";
                cbFinanciera.DataBind();

                cbFinanciera.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                string.Format("Error al cargar las financieras {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }

        void CargarDepartamentos()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveDepartamentos().ToList();

            cbDepartamento.DataSource = q;
            cbDepartamento.DataTextField = "Departamento";
            cbDepartamento.DataValueField = "Departamento";
            cbDepartamento.DataBind();

            cbDepartamentoFacturacion.DataSource = q;
            cbDepartamentoFacturacion.DataTextField = "Departamento";
            cbDepartamentoFacturacion.DataValueField = "Departamento";
            cbDepartamentoFacturacion.DataBind();

            cbDepartamentoEmpresa.DataSource = q;
            cbDepartamentoEmpresa.DataTextField = "Departamento";
            cbDepartamentoEmpresa.DataValueField = "Departamento";
            cbDepartamentoEmpresa.DataBind();

            cbFamiliar1EmpresaDepartamento.DataSource = q;
            cbFamiliar1EmpresaDepartamento.DataTextField = "Departamento";
            cbFamiliar1EmpresaDepartamento.DataValueField = "Departamento";
            cbFamiliar1EmpresaDepartamento.DataBind();

            cbFamiliar1CasaDepartamento.DataSource = q;
            cbFamiliar1CasaDepartamento.DataTextField = "Departamento";
            cbFamiliar1CasaDepartamento.DataValueField = "Departamento";
            cbFamiliar1CasaDepartamento.DataBind();

            cbFamiliar2EmpresaDepartamento.DataSource = q;
            cbFamiliar2EmpresaDepartamento.DataTextField = "Departamento";
            cbFamiliar2EmpresaDepartamento.DataValueField = "Departamento";
            cbFamiliar2EmpresaDepartamento.DataBind();

            cbFamiliar2CasaDepartamento.DataSource = q;
            cbFamiliar2CasaDepartamento.DataTextField = "Departamento";
            cbFamiliar2CasaDepartamento.DataValueField = "Departamento";
            cbFamiliar2CasaDepartamento.DataBind();

            cbPersonal1EmpresaDepartamento.DataSource = q;
            cbPersonal1EmpresaDepartamento.DataTextField = "Departamento";
            cbPersonal1EmpresaDepartamento.DataValueField = "Departamento";
            cbPersonal1EmpresaDepartamento.DataBind();

            cbPersonal1CasaDepartamento.DataSource = q;
            cbPersonal1CasaDepartamento.DataTextField = "Departamento";
            cbPersonal1CasaDepartamento.DataValueField = "Departamento";
            cbPersonal1CasaDepartamento.DataBind();

            cbPersonal2EmpresaDepartamento.DataSource = q;
            cbPersonal2EmpresaDepartamento.DataTextField = "Departamento";
            cbPersonal2EmpresaDepartamento.DataValueField = "Departamento";
            cbPersonal2EmpresaDepartamento.DataBind();

            cbPersonal2CasaDepartamento.DataSource = q;
            cbPersonal2CasaDepartamento.DataTextField = "Departamento";
            cbPersonal2CasaDepartamento.DataValueField = "Departamento";
            cbPersonal2CasaDepartamento.DataBind();

            sugerirDepartamentos();

            CargaMunicipios();
            CargaMunicipiosFacturacion();
            CargaMunicipiosEmpresa();

            CargaMunicipiosFamiliar1Empresa();
            CargaMunicipiosFamiliar1Casa();
            CargaMunicipiosFamiliar2Empresa();
            CargaMunicipiosFamiliar2Casa();
            CargaMunicipiosPersonal1Empresa();
            CargaMunicipiosPersonal1Casa();
            CargaMunicipiosPersonal2Empresa();
            CargaMunicipiosPersonal2Casa();

            sugerirMunicipios();
        }

        void sugerirDepartamentos()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            string mDepartamento = ws.DepartamentoTienda(Convert.ToString(Session["Tienda"]));

            cbDepartamento.SelectedValue = mDepartamento;
            cbDepartamentoFacturacion.SelectedValue = mDepartamento;
            cbDepartamentoEmpresa.SelectedValue = mDepartamento;

            cbFamiliar1EmpresaDepartamento.SelectedValue = mDepartamento;
            cbFamiliar1CasaDepartamento.SelectedValue = mDepartamento;
            cbFamiliar2EmpresaDepartamento.SelectedValue = mDepartamento;
            cbFamiliar2CasaDepartamento.SelectedValue = mDepartamento;
            cbPersonal1EmpresaDepartamento.SelectedValue = mDepartamento;
            cbPersonal1CasaDepartamento.SelectedValue = mDepartamento;
            cbPersonal2EmpresaDepartamento.SelectedValue = mDepartamento;
            cbPersonal2CasaDepartamento.SelectedValue = mDepartamento;
        }

        void sugerirMunicipios()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            string mMunicipio = ws.MunicipioTienda(Convert.ToString(Session["Tienda"]));

            cbMunicipio.SelectedValue = mMunicipio;
            cbMunicipioFacturacion.SelectedValue = mMunicipio;
            cbMunicipioEmpresa.SelectedValue = mMunicipio;

            cbFamiliar1EmpresaMunicipio.SelectedValue = mMunicipio;
            cbFamiliar1CasaMunicipio.SelectedValue = mMunicipio;
            cbFamiliar2EmpresaMunicipio.SelectedValue = mMunicipio;
            cbFamiliar2CasaMunicipio.SelectedValue = mMunicipio;
            cbPersonal1EmpresaMunicipio.SelectedValue = mMunicipio;
            cbPersonal1CasaMunicipio.SelectedValue = mMunicipio;
            cbPersonal2EmpresaMunicipio.SelectedValue = mMunicipio;
            cbPersonal2CasaMunicipio.SelectedValue = mMunicipio;
        }

        void CargaMunicipios()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveMunicipios(cbDepartamento.SelectedValue).ToList();

            cbMunicipio.DataSource = q;
            cbMunicipio.DataTextField = "Municipio";
            cbMunicipio.DataValueField = "Municipio";
            cbMunicipio.DataBind();
        }

        void CargaMunicipiosFacturacion()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveMunicipios(cbDepartamentoFacturacion.SelectedValue);

            cbMunicipioFacturacion.DataSource = q;
            cbMunicipioFacturacion.DataTextField = "Municipio";
            cbMunicipioFacturacion.DataValueField = "Municipio";
            cbMunicipioFacturacion.DataBind();
        }

        void CargaMunicipiosEmpresa()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveMunicipios(cbDepartamentoEmpresa.SelectedValue);

            cbMunicipioEmpresa.DataSource = q;
            cbMunicipioEmpresa.DataTextField = "Municipio";
            cbMunicipioEmpresa.DataValueField = "Municipio";
            cbMunicipioEmpresa.DataBind();
        }

        void CargaMunicipiosFamiliar1Empresa()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveMunicipios(cbFamiliar1EmpresaDepartamento.SelectedValue);
            string m = cbFamiliar1EmpresaMunicipio.SelectedValue.ToString();
            cbFamiliar1EmpresaMunicipio.DataSource = q;
            cbFamiliar1EmpresaMunicipio.DataTextField = "Municipio";
            cbFamiliar1EmpresaMunicipio.DataValueField = "Municipio";
            cbFamiliar1EmpresaMunicipio.DataBind();
        }

        void CargaMunicipiosFamiliar1Casa()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveMunicipios(cbFamiliar1CasaDepartamento.SelectedValue);

            cbFamiliar1CasaMunicipio.DataSource = q;
            cbFamiliar1CasaMunicipio.DataTextField = "Municipio";
            cbFamiliar1CasaMunicipio.DataValueField = "Municipio";
            cbFamiliar1CasaMunicipio.DataBind();
        }

        void CargaMunicipiosFamiliar2Empresa()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveMunicipios(cbFamiliar2EmpresaDepartamento.SelectedValue);

            cbFamiliar2EmpresaMunicipio.DataSource = q;
            cbFamiliar2EmpresaMunicipio.DataTextField = "Municipio";
            cbFamiliar2EmpresaMunicipio.DataValueField = "Municipio";
            cbFamiliar2EmpresaMunicipio.DataBind();
        }

        void CargaMunicipiosFamiliar2Casa()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveMunicipios(cbFamiliar2CasaDepartamento.SelectedValue);

            cbFamiliar2CasaMunicipio.DataSource = q;
            cbFamiliar2CasaMunicipio.DataTextField = "Municipio";
            cbFamiliar2CasaMunicipio.DataValueField = "Municipio";
            cbFamiliar2CasaMunicipio.DataBind();
        }

        void CargaMunicipiosPersonal1Empresa()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveMunicipios(cbPersonal1EmpresaDepartamento.SelectedValue);

            cbPersonal1EmpresaMunicipio.DataSource = q;
            cbPersonal1EmpresaMunicipio.DataTextField = "Municipio";
            cbPersonal1EmpresaMunicipio.DataValueField = "Municipio";
            cbPersonal1EmpresaMunicipio.DataBind();
        }

        void CargaMunicipiosPersonal1Casa()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveMunicipios(cbPersonal1CasaDepartamento.SelectedValue);

            cbPersonal1CasaMunicipio.DataSource = q;
            cbPersonal1CasaMunicipio.DataTextField = "Municipio";
            cbPersonal1CasaMunicipio.DataValueField = "Municipio";
            cbPersonal1CasaMunicipio.DataBind();
        }

        void CargaMunicipiosPersonal2Empresa()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveMunicipios(cbPersonal2EmpresaDepartamento.SelectedValue);

            cbPersonal2EmpresaMunicipio.DataSource = q;
            cbPersonal2EmpresaMunicipio.DataTextField = "Municipio";
            cbPersonal2EmpresaMunicipio.DataValueField = "Municipio";
            cbPersonal2EmpresaMunicipio.DataBind();
        }

        void CargaMunicipiosPersonal2Casa()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveMunicipios(cbPersonal2CasaDepartamento.SelectedValue);

            cbPersonal2CasaMunicipio.DataSource = q;
            cbPersonal2CasaMunicipio.DataTextField = "Municipio";
            cbPersonal2CasaMunicipio.DataValueField = "Municipio";
            cbPersonal2CasaMunicipio.DataBind();
        }

        protected void cbFacturarOtroNombre_CheckedChanged(object sender, EventArgs e)
        {
            ComboFacturarOtroNombre();
        }

        void ComboFacturarOtroNombre()
        {
            tblNombreFacturacion.Visible = cbFacturarOtroNombre.Checked;
            if (cbFacturarOtroNombre.Checked)
            {
                txtNitFactura.Text = "";
                txtNombreFactura.Text = "";
                txtNitFactura.Focus();
            }
        }
        
        bool PasaValidaciones()
        {
            lbInfo.Text = "";
            lbInfo2.Text = "";
            lbError.Text = "";
            lbErrorGenerales.Text = "";
            lbErrorSolicitudCredito.Text = "";

            if (cbFinanciera.SelectedValue == "Seleccione financiera ...")
            {
                lbErrorGenerales.Text = "Debe seleccionar la financiera.";
                cbFinanciera.Focus();
                return false;
            }

            Int32 mFinanciera = 0;
            try
            {
                mFinanciera = Convert.ToInt32(cbFinanciera.SelectedValue);
            }
            catch
            {
                lbErrorGenerales.Text = "Debe seleccionar la financiera.";
                cbFinanciera.Focus();
                return false;
            }
            if (mFinanciera <= 0)
            {
                lbErrorGenerales.Text = "Debe seleccionar la financiera.";
                cbFinanciera.Focus();
                return false;
            }
            
            if (cbNivelPrecio.SelectedValue == "Seleccione ..." || cbNivelPrecio.Text.Trim().Length == 0 || cbNivelPrecio.Text == "Seleccione ...")
            {
                lbErrorGenerales.Text = "Debe seleccionar el nivel de precio.";
                cbTipo.Focus();
                return false;
            }
            if (txtNit.Text.Trim().Length == 0)
            {
                lbErrorGenerales.Text = "Debe ingresar el NIT.";
                txtNit.Focus();
                return false;
            }
            if (tblSolicitudCredito.Visible)
            {
                if (txtDPI.Text.Trim().Length == 0)
                {
                    lbErrorGenerales.Text = "Debe ingresar el DPI.";
                    lbErrorSolicitudCredito.Text = "Debe ingresar el DPI.";
                    txtDPI.Focus();
                    return false;
                }

                Int64 mDpi = 0;

                try
                {
                    mDpi = Convert.ToInt64(txtDPI.Text.Trim());
                }
                catch
                {
                    lbErrorGenerales.Text = "Debe ingresar un DPI válido, sin guiones, ni espacios, ni letras.";
                    lbErrorSolicitudCredito.Text = "Debe ingresar un DPI válido, sin guiones, ni espacios, ni letras.";
                    txtDPI.Focus();
                    return false;
                }

                if (mDpi <= 0 || mDpi.ToString().Contains("."))
                {
                    lbErrorGenerales.Text = "Debe ingresar un DPI válido, sin guiones, ni espacios, ni letras.";
                    lbErrorSolicitudCredito.Text = "Debe ingresar un DPI válido, sin guiones, ni espacios, ni letras.";
                    txtDPI.Focus();
                    return false;
                }
            }
            if (txtPrimerNombre.Text.Trim().Length == 0)
            {
                lbErrorGenerales.Text = "Debe ingresar el nombre.";
                txtPrimerNombre.Focus();
                return false;
            }
            if (!cbPersonaJuridica.Checked)
            {
                if (txtPrimerApellido.Text.Trim().Length == 0)
                {
                    lbErrorGenerales.Text = "Debe ingresar el apellido.";
                    txtPrimerApellido.Focus();
                    return false;
                }
            }

            if (!rbHombre.Checked && !rbMujer.Checked)
            {
                lbErrorGenerales.Text = "Debe seleccionar el género del cliente.";
                rbHombre.Focus();
                return false;
            }

            if (txtTelefono1.Text.Trim().Length == 0 && txtTelefono2.Text.Trim().Length == 0 && txtCelular.Text.Trim().Length == 0)
            {
                lbErrorGenerales.Text = "Debe ingresar al menos un número de teléfono.";
                txtTelefono1.Focus();
                return false;
            }


            if (txtEmail.Text.Trim().Length > 0)
            {
                WebRequest request = WebRequest.Create(string.Format("{0}/isEmailValido/?email={1}", Convert.ToString(Session["UrlRestServices"]), txtEmail.Text.Trim()));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                Clases.RetornaExito mRetorna = new Clases.RetornaExito();
                mRetorna = JsonConvert.DeserializeObject<Clases.RetornaExito>(responseString);

                if (!mRetorna.exito)
                {
                    lbErrorGenerales.Text = mRetorna.mensaje;
                    txtEmail.Focus();
                    return false;
                }
            }
            
            if (!cbEntraPickup.Checked && !cbEntraPickup.Checked)
            {
                lbErrorGenerales.Text = "Debe especificar si en la dirección del cliente entra un pickup y/o camión.";
                cbEntraCamion.Focus();
                return false;
            }

            if (txtCalleAvenidaFacturacion.Text.Trim().Length == 0)
            {
                lbErrorGenerales.Text = "Debe ingresar la Calle ó Avenida.";
                txtCalleAvenidaFacturacion.Focus();
                return false;
            }
            //if (txtCasaFacturacion.Text.Trim().Length == 0)
            //{
            //    lbErrorGenerales.Text = "Debe ingresar el número de casa.";
            //    txtCasaFacturacion.Focus();
            //    return false;
            //}
            if (cbDepartamentoFacturacion.Text.Trim().Length == 0)
            {
                lbErrorGenerales.Text = "Debe seleccionar el departamento.";
                cbDepartamentoFacturacion.Focus();
                return false;
            }
            if (cbMunicipioFacturacion.Text.Trim().Length == 0)
            {
                lbErrorGenerales.Text = "Debe seleccionar el municipio.";
                cbMunicipioFacturacion.Focus();
                return false;
            }
            if (cbFacturarOtroNombre.Checked)
            {
                if (txtCalleAvenidaFacturacion.Text.Trim().Length == 0)
                {
                    lbErrorGenerales.Text = "Debe ingresar la Calle ó Avenida en la dirección de la factura.";
                    txtCalleAvenidaFacturacion.Focus();
                    return false;
                }
                if (txtCasaFacturacion.Text.Trim().Length == 0)
                {
                    lbErrorGenerales.Text = "Debe ingresar el número de casa en la dirección de la factura.";
                    txtCasaFacturacion.Focus();
                    return false;
                }
                if (cbDepartamentoFacturacion.Text.Trim().Length == 0)
                {
                    lbErrorGenerales.Text = "Debe seleccionar el departamento en la dirección de la factura.";
                    cbDepartamentoFacturacion.Focus();
                    return false;
                }
                if (cbMunicipioFacturacion.Text.Trim().Length == 0)
                {
                    lbErrorGenerales.Text = "Debe seleccionar el municipio en la dirección de la factura.";
                    cbMunicipioFacturacion.Focus();
                    return false;
                }
            }
            if (tblNombreFacturacion.Visible)
            {
                if (txtNitFactura.Text.Trim().Length == 0)
                {
                    lbErrorGenerales.Text = "Debe ingresar el NIT para la factura.";
                    txtNitFactura.Focus();
                    return false;
                }
                if (txtNombreFactura.Text.Trim().Length == 0)
                {
                    lbErrorGenerales.Text = "Debe ingresar el nombre para la factura.";
                    txtNombreFactura.Focus();
                    return false;
                }
            }


            if (mFinanciera == 5 || mFinanciera == 7 || mFinanciera == 9 || mFinanciera == 12)
            {
                if (!PasaFechaNacimiento()) return false;
            }

            if (tblSolicitudCredito.Visible)
            {
                if (!PasaFechaTrabajo()) return false;

                if (txtEmail.Text.Trim().Length == 0)
                {
                    lbErrorSolicitudCredito.Text = "Debe ingresar el correo electrónico.";
                    txtEmail.Focus();
                    return false;
                }

                if (txtEmpresa.Text.Trim().Length == 0)
                {
                    lbErrorSolicitudCredito.Text = "Debe ingresar el nombre de la empresa.";
                    txtEmpresa.Focus();
                    return false;
                }
                if (txtCalleEmpresa.Text.Trim().Length == 0)
                {
                    lbErrorSolicitudCredito.Text = "Debe ingresar la dirección de la empresa.";
                    txtCalleEmpresa.Focus();
                    return false;
                }
                if (txtCasaEmpresa.Text.Trim().Length == 0)
                {
                    lbErrorSolicitudCredito.Text = "Debe completar la dirección de la empresa.";
                    txtCasaEmpresa.Focus();
                    return false;
                }
                if (cbProfesion.Text.Trim().Length == 0)
                {
                    lbErrorSolicitudCredito.Text = "Debe ingresar la profesión.";
                    cbProfesion.Focus();
                    return false;
                }
                if (cbPuesto.Text.Trim().Length == 0)
                {
                    lbErrorSolicitudCredito.Text = "Debe ingresar el puesto.";
                    cbPuesto.Focus();
                    return false;
                }

                
                if (txtReferenciasBancarias.Text.Trim().Length > 0)
                {
                    if (cbBanco.SelectedValue == "")
                    {
                        lbErrorSolicitudCredito.Text = "Debe ingresar un banco.";
                        cbBanco.Focus();
                        return false;
                    }

                    Int64 mCuenta = 0;

                    try
                    {
                        mCuenta = Convert.ToInt64(txtReferenciasBancarias.Text.Trim());
                    }
                    catch
                    {
                        lbErrorSolicitudCredito.Text = "En el número de cuenta solo debe ingresar números.";
                        txtReferenciasBancarias.Focus();
                        return false;
                    }

                    if (mCuenta <= 0 || mCuenta.ToString().Length < 7)
                    {
                        lbErrorSolicitudCredito.Text = "El número de cuenta ingresado es inválido.";
                        txtReferenciasBancarias.Focus();
                        return false;
                    }
                }

                if (txtEmpresaTelefono.Text.Trim().Length == 0)
                {
                    lbErrorSolicitudCredito.Text = "Debe ingrear el teléfono de la empresa.";
                    txtEmpresaTelefono.Focus();
                    return false;
                }
                if (txtIngresos.Text.Trim().Length == 0)
                {
                    lbErrorSolicitudCredito.Text = "Debe ingresar los ingresos mensuales.";
                    txtIngresos.Focus();
                    return false;
                }
                if (txtEgresos.Text.Trim().Length == 0)
                {
                    lbErrorSolicitudCredito.Text = "Debe ingresar los egresos mensuales.";
                    txtEgresos.Focus();
                    return false;
                }
                if (txtCargasFamiliares.Text.Trim().Length == 0)
                {
                    lbErrorSolicitudCredito.Text = "Debe ingresar las cargas familiares.";
                    txtCargasFamiliares.Focus();
                    return false;
                }

                Int32 mCargas = 0;

                try
                {
                    mCargas = Convert.ToInt32(txtCargasFamiliares.Text.Trim());
                }
                catch
                {
                    lbErrorSolicitudCredito.Text = "Los cargas familiares ingresadas son inválidas, de no tener, por favor ingrese cero.";
                    txtCargasFamiliares.Focus();
                    return false;
                }

                if (mCargas < 0)
                {
                    lbErrorSolicitudCredito.Text = "Las cargas familiares ingresadas son inválidas, de no tener, por favor ingrese cero.";
                    txtCargasFamiliares.Focus();
                    return false;
                }

                if (cbTieneVehiculo.SelectedValue == "S")
                {
                    if (txtMarcaModelo.Text.Trim().Length == 0)
                    {
                        lbErrorSolicitudCredito.Text = "Selecciono que el cliente tiene vehículo, debe ingresar la marca y el modelo.";
                        txtMarcaModelo.Focus();
                        return false;
                    }
                }
                if (!rbCasaPropia.Checked && !rbCasaFamiliares.Checked && !rbCasaAlquilada.Checked)
                {
                    lbErrorSolicitudCredito.Text = "Debe seleccionar el tipo de vivienda.";
                    rbCasaPropia.Focus();
                    return false;
                }

                if (mFinanciera == 5 || mFinanciera == 7 || mFinanciera == 9)
                {
                    if (txtReferenciaPersonal1.Text.Trim().Length == 0)
                    {
                        lbErrorSolicitudCredito.Text = "Debe ingresar la primera referencia personal.";
                        txtReferenciaPersonal1.Focus();
                        return false;
                    }
                    if (txtReferenciaPersonalResidencia1.Text.Trim().Length == 0 && txtReferenciaPersonalTrabajo1.Text.Trim().Length == 0 && txtReferenciaPersonalCelular1.Text.Trim().Length == 0)
                    {
                        lbErrorSolicitudCredito.Text = "Debe ingresar al menos un teléfono de la primera referencia personal.";
                        txtReferenciaPersonalTrabajo1.Focus();
                        return false;
                    }
                    if (txtReferenciaPersonal2.Text.Trim().Length == 0)
                    {
                        lbErrorSolicitudCredito.Text = "Debe ingresar la segunda referencia personal.";
                        txtReferenciaPersonal2.Focus();
                        return false;
                    }
                    if (txtReferenciaPersonalResidencia2.Text.Trim().Length == 0 && txtReferenciaPersonalTrabajo2.Text.Trim().Length == 0 && txtReferenciaPersonalCelular2.Text.Trim().Length == 0)
                    {
                        lbErrorSolicitudCredito.Text = "Debe ingresar al menos un teléfono de la segunda referencia personal.";
                        txtReferenciaPersonalTrabajo2.Focus();
                        return false;
                    }
                }
            }

            if (cbDia.SelectedValue != "--")
            {
                if (!PasaFechaNacimiento()) return false;
            }

            return true;
        }

        private string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                return "";
            }
            return match.Groups[1].Value + domainName;
        }

        bool PasaFechaNacimiento()
        {
            DateTime mFechaNacimiento = DateTime.Now.Date;
            string mMensaje = "Debe ingresar una fecha de nacimiento válida.";

            try
            {
                if (txtAnio.Text.Trim().Length == 2) txtAnio.Text = string.Format("20{0}", txtAnio.Text);
                mFechaNacimiento = new DateTime(Convert.ToInt32(txtAnio.Text), Convert.ToInt32(cbMes.SelectedValue), Convert.ToInt32(cbDia.SelectedValue));
            }
            catch
            {
                lbErrorGenerales.Text = mMensaje;
                lbErrorSolicitudCredito.Text = mMensaje;
                cbDia.Focus();
                return false;
            }

            if ((DateTime.Now.Year - mFechaNacimiento.Year) < 18)
            {
                lbErrorGenerales.Text = mMensaje;
                lbErrorSolicitudCredito.Text = mMensaje;
                cbDia.Focus();
                return false;
            }

            return true;
        }

        bool PasaFechaTrabajo()
        {
            DateTime mFechaTrabajo = DateTime.Now.Date;

            try
            {
                if (txtAnioTrabajo.Text.Trim().Length == 2) txtAnioTrabajo.Text = string.Format("20{0}", txtAnioTrabajo.Text);
                mFechaTrabajo = new DateTime(Convert.ToInt32(txtAnioTrabajo.Text), Convert.ToInt32(cbMesTrabajo.SelectedValue), Convert.ToInt32(cbDiaTrabajo.SelectedValue));
            }
            catch
            {
                lbErrorGenerales.Text = "Debe ingresar una fecha de inicio de labores válida.";
                lbErrorSolicitudCredito.Text = "Debe ingresar una fecha de inicio de labores válida.";
                cbDiaTrabajo.Focus();
                return false;
            }

            return true;
        }

        protected void  lkGrabar_Click(object sender, EventArgs e)
        {
            base.LogActivity(MethodBase.GetCurrentMethod().Name);
            
            if (!PasaValidaciones()) return;

            try
            {
                lbInfo.Text = "";
                lbInfo2.Text = "";
                lbError.Text = "";
                lbErrorGenerales.Text = "";
                lbErrorSolicitudCredito.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                wsPuntoVenta.Clientes[] q = new wsPuntoVenta.Clientes[1];
                string mCliente = txtCodigo.Text; string mMensaje = ""; string mConsumidorFinal = "N"; string mNombre = "";

                if (cbConsumidorFinal.Checked) mConsumidorFinal = "S";

                if (!cbFacturarOtroNombre.Checked)
                {
                    txtNitFactura.Text = txtNit.Text.ToUpper();
                    txtNombreFactura.Text = txtPrimerNombre.Text;
                    if (!cbPersonaJuridica.Checked) txtNombreFactura.Text = string.Format("{0}{1}{2}, {3}{4}{5}{6}{7}", txtPrimerApellido.Text, txtSegundoApellido.Text.Trim().Length == 0 ? "" : " ", txtSegundoApellido.Text, txtPrimerNombre.Text, txtSegundoNombre.Text.Trim().Length == 0 ? "" : " ", txtSegundoNombre.Text, txtTercerNombre.Text.Trim().Length == 0 ? "" : " ", txtTercerNombre.Text).ToUpper();
                }
                if (cbMismaDireccionFactura.Checked)
                {
                    txtCalleAvenida.Text = txtCalleAvenidaFacturacion.Text;
                    txtCasa.Text = txtCasaFacturacion.Text;
                    cbZona.SelectedValue = cbZonaFacturacion.SelectedValue;
                    txtApartamento.Text = txtApartamentoFacturacion.Text;
                    txtColonia.Text = txtColoniaFacturacion.Text;
                    cbDepartamento.SelectedValue = cbDepartamentoFacturacion.SelectedValue;
                    CargaMunicipios();
                    cbMunicipio.SelectedValue = cbMunicipioFacturacion.SelectedValue;
                }

                DateTime mFechaNacimiento = DateTime.Now.Date; bool mFechaNacimientoValida;
                DateTime mFechaNacimientoConyugue = DateTime.Now.Date; bool mFechaNacimientoValidaConyugue;
                DateTime mFechaTrabajo = DateTime.Now.Date; bool mFechaTrabajoValida;

                try
                {
                    if (txtAnio.Text.Trim().Length == 2) txtAnio.Text = string.Format("20{0}", txtAnio.Text);
                    mFechaNacimiento = new DateTime(Convert.ToInt32(txtAnio.Text), Convert.ToInt32(cbMes.SelectedValue), Convert.ToInt32(cbDia.SelectedValue));
                    mFechaNacimientoValida = true;
                }
                catch
                {
                    mFechaNacimientoValida = false;
                }
                try
                {
                    if (txtAnioConyugue.Text.Trim().Length == 2) txtAnioConyugue.Text = string.Format("20{0}", txtAnioConyugue.Text);
                    mFechaNacimientoConyugue = new DateTime(Convert.ToInt32(txtAnioConyugue.Text), Convert.ToInt32(cbMesConyugue.SelectedValue), Convert.ToInt32(cbDiaConyugue.SelectedValue));
                    mFechaNacimientoValidaConyugue = true;
                }
                catch
                {
                    mFechaNacimientoValidaConyugue = false;
                }
                try
                {
                    if (txtAnioTrabajo.Text.Trim().Length == 2) txtAnioTrabajo.Text = string.Format("20{0}", txtAnioTrabajo.Text);
                    mFechaTrabajo = new DateTime(Convert.ToInt32(txtAnioTrabajo.Text), Convert.ToInt32(cbMesTrabajo.SelectedValue), Convert.ToInt32(cbDiaTrabajo.SelectedValue));
                    mFechaTrabajoValida = true;
                }
                catch
                {
                    mFechaTrabajoValida = false;
                }

                wsPuntoVenta.Clientes item = new wsPuntoVenta.Clientes();
               
                item.Cliente = txtCodigo.Text;
                item.PrimerNombre = txtPrimerNombre.Text.Trim().ToUpper();
                item.SegundoNombre = txtSegundoNombre.Text.Trim().ToUpper();
                item.TercerNombre = txtTercerNombre.Text.Trim().ToUpper();
                item.PrimerApellido = txtPrimerApellido.Text.Trim().ToUpper();
                item.SegundoApellido = txtSegundoApellido.Text.Trim().ToUpper();
                item.ApellidoCasada = txtApellidoCasada.Text.Trim().ToUpper();
                item.CalleAvenida = txtCalleAvenida.Text;
                item.CasaNumero = txtCasa.Text;
                item.Apartamento = txtApartamento.Text;
                item.Zona = cbZona.SelectedValue;
                item.Colonia = txtColonia.Text;
                item.Departamento = cbDepartamento.SelectedValue;
                item.Municipio = cbMunicipio.SelectedValue;
                item.IndicacionesParaLlegar = txtIndicacionesParaLLegar.Text;
                item.Telefono1 = txtTelefono1.Text;
                item.Telefono2 = txtTelefono2.Text;
                item.Celular = txtCelular.Text;
                item.Nit = txtNit.Text.ToUpper();
                item.Notas = txtNotas.Text;
                item.Email = txtEmail.Text;
                item.Empresa = txtEmpresa.Text;
                item.EmpresaDireccion = txtEmpresaDireccion.Text;
                item.EmpresaTelefono = txtEmpresaTelefono.Text;
                item.Cargo = cbPuesto.SelectedValue;
                item.TiempoLaborar = txtTiempo.Text;
                item.IngresosMes = txtIngresos.Text;
                item.GastosMes = txtEgresos.Text;
                item.ConyugeNombre = txtConyugeNombre.Text;
                item.DPI = txtDPI.Text;
                item.Genero = rbHombre.Checked ? "H" : "M";
                item.FechaNacimientoValida = mFechaNacimientoValida;
                if (mFechaNacimientoValida) item.FechaNacimiento = mFechaNacimiento;
                item.FacturarOtroNombre = cbFacturarOtroNombre.Checked ? "S" : "N";
                item.NitFactura = txtNitFactura.Text.ToUpper();
                item.NombreFactura = txtNombreFactura.Text.ToUpper();
                item.EntraCamion = cbEntraCamion.Checked ? "S" : "N";
                item.EntraPickup = cbEntraPickup.Checked ? "S" : "N";
                item.EsSegundoPiso = cbSegundoPiso.Checked ? "S" : "N";
                item.UsarDireccEnFactura = cbMismaDireccionFactura.Checked ? "S" : "N";
                item.CalleAvenidaFacturacion = txtCalleAvenidaFacturacion.Text;
                item.CasaNumeroFacturacion = txtCasaFacturacion.Text;
                item.ApartamentoFacturacion = txtApartamentoFacturacion.Text;
                item.ZonaFacturacion = cbZonaFacturacion.SelectedValue;
                item.ColoniaFacturacion = txtColoniaFacturacion.Text;
                item.DepartamentoFacturacion = cbDepartamentoFacturacion.SelectedValue;
                item.MunicipioFacturacion = cbMunicipioFacturacion.SelectedValue;
                item.TipoVenta = cbTipo.SelectedValue;
                item.Financiera = Convert.ToInt32(cbFinanciera.SelectedValue);
                item.NivelPrecio = cbNivelPrecio.SelectedValue;
                item.EstadoCivil = cbEstadoCivil.SelectedValue;
                item.Nacionalidad = txtNacionalidad.Text;
                item.Profesion = cbProfesion.SelectedValue;
                item.Banco = cbBanco.SelectedValue;
                item.TieneVehiculo = cbTieneVehiculo.SelectedValue;
                item.MarcaModelo = txtMarcaModelo.Text;
                item.Placas = txtPlacas.Text;
                item.TipoCasa = rbCasaPropia.Checked ? "P" : rbCasaFamiliares.Checked ? "F" : rbCasaAlquilada.Checked ? "A" : "N";
                item.TiempoResidir = txtTiempoResidir.Text;
                item.PagoMensual = txtPagoMensual.Text;
                item.FinancieraPaga = txtFinancieraVivienda.Text;
                item.NumeroContador = txtContador.Text;
                item.RefPersonalNombre1 = txtReferenciaPersonal1.Text.ToUpper();
                item.RefPersonalTelResidencia1 = txtReferenciaPersonalResidencia1.Text;
                item.RefPersonalTelTrabajo1 = txtReferenciaPersonalTrabajo1.Text;
                item.RefPersonalCelular1 = txtReferenciaPersonalCelular1.Text;
                item.RefPersonalNombre2 = txtReferenciaPersonal2.Text.ToUpper();
                item.RefPersonalTelResidencia2 = txtReferenciaPersonalResidencia2.Text;
                item.RefPersonalTelTrabajo2 = txtReferenciaPersonalTrabajo2.Text;
                item.RefPersonalCelular2 = txtReferenciaPersonalCelular2.Text;
                item.RefComercialNombre1 = txtReferenciaComercial1.Text.ToUpper();
                item.RefComercialTelefono1 = txtReferenciaComercialTelefono1.Text;
                item.RefComercialFax1 = txtReferenciaComercialFax1.Text;
                item.RefComercialCelular1 = txtReferenciaComercialCelular1.Text;
                item.RefComercialNombre2 = txtReferenciaComercial2.Text.ToUpper();
                item.RefComercialTelefono2 = txtReferenciaComercialTelefono2.Text;
                item.RefComercialFax2 = txtReferenciaComercialFax2.Text;
                item.RefComercialCelular2 = txtReferenciaComercialCelular2.Text;
                item.Nombre = "";
                item.ConsumidorFinal = mConsumidorFinal;
                item.Extension = txtExtension.Text.Trim().ToUpper();
                item.CargasFamiliares = txtCargasFamiliares.Text.Trim().ToUpper();
                item.Politico = cbPoliticamente.SelectedValue;
                item.ReferenciasBancarias = txtReferenciasBancarias.Text.Trim().ToUpper();
                item.ConyugueTiempo = txtTiempoConyugue.Text.Trim().ToUpper();
                item.DepartamentoEmpresa = cbDepartamentoEmpresa.SelectedValue;
                item.MunicipioEmpresa = cbMunicipioEmpresa.SelectedValue;
                item.ConyugueFechaNacimientoValida = mFechaNacimientoValidaConyugue;
                if (mFechaNacimientoValidaConyugue) item.ConyugueFechaNacimiento = mFechaNacimientoConyugue;
                item.TotalFacturar = 0;
                item.Enganche = 0;
                item.SaldoFinanciar = 0;
                item.Recargos = 0;
                item.Monto = 0;
                item.Plazo = "";
                item.CantidadPagos1 = 0;
                item.MontoPagos1 = 0;
                item.CantidadPagos2 = 0;
                item.MontoPagos2 = 0;
                item.ConyugueEmpresaDireccion = txtDireccionEmpresaConyugue.Text.ToUpper();
                item.PersonaJuridica = cbPersonaJuridica.Checked ? "S" : "N";
                item.TrabajoFechaIngresoValida = mFechaTrabajoValida;
                if (mFechaTrabajoValida) item.TrabajoFechaIngreso = mFechaTrabajo;
                item.CalleAvenidaEmpresa = txtCalleEmpresa.Text.Trim().ToUpper();
                item.CasaNumeroEmpresa = txtCasaEmpresa.Text.Trim().ToUpper();
                item.ZonaEmpresa = cbZonaEmpresa.SelectedValue;
                item.ApartamentoEmpresa = txtApartamentoEmpresa.Text.Trim().ToUpper();
                item.ColoniaEmpresa = txtColoniaEmpresa.Text.Trim().ToUpper();

                item.Familiar1PrimerNombre = txtFamiliar1PrimerNombre.Text.Trim().ToUpper();
                item.Familiar1SegundoNombre = txtFamiliar1SegundoNombre.Text.Trim().ToUpper();
                item.Familiar1TercerNombre = txtFamiliar1TercerNombre.Text.Trim().ToUpper();
                item.Familiar1PrimerApellido = txtFamiliar1PrimerApellido.Text.Trim().ToUpper();
                item.Familiar1SegundoApellido = txtFamiliar1SegundoApellido.Text.Trim().ToUpper();
                item.Familiar1ApellidoCasada = txtFamiliar1ApellidoCasada.Text.Trim().ToUpper();
                item.Familiar1Celular = txtFamiliar1Celular.Text.Trim().ToUpper();
                item.Familiar2PrimerNombre = txtFamiliar2PrimerNombre.Text.Trim().ToUpper();
                item.Familiar2SegundoNombre = txtFamiliar2SegundoNombre.Text.Trim().ToUpper();
                item.Familiar2TercerNombre = txtFamiliar2TercerNombre.Text.Trim().ToUpper();
                item.Familiar2PrimerApellido = txtFamiliar2PrimerApellido.Text.Trim().ToUpper();
                item.Familiar2SegundoApellido = txtFamiliar2SegundoApellido.Text.Trim().ToUpper();
                item.Familiar2ApellidoCasada = txtFamiliar2ApellidoCasada.Text.Trim().ToUpper();
                item.Familiar2Celular = txtFamiliar2Celular.Text.Trim().ToUpper();

                item.Familiar1Tipo = cbFamiliar1Tipo.SelectedValue.ToString();
                item.Familiar1Empresa = txtFamiliar1Empresa.Text.Trim().ToUpper();
                item.Familiar1EmpresaDireccion = txtFamiliar1EmpresaDireccion.Text.Trim().ToUpper();
                item.Familiar1EmpresaDepartamento = cbFamiliar1EmpresaDepartamento.SelectedValue.ToString();
                item.Familiar1EmpresaMunicipio = cbFamiliar1EmpresaMunicipio.SelectedValue.ToString();
                item.Familiar1EmpresaTelefono = txtFamiliar1EmpresaTelefono.Text.Trim().ToUpper();
                item.Familiar1CasaDireccion = txtFamiliar1CasaDireccion.Text.Trim().ToUpper();
                item.Familiar1CasaDepartamento = cbFamiliar1CasaDepartamento.SelectedValue.ToString();
                item.Familiar1CasaMunicipio = cbFamiliar1CasaMunicipio.SelectedValue.ToString();
                item.Familiar1CasaTelefono = txtFamiliar1CasaTelefono.Text.Trim().ToUpper();

                item.Familiar2Tipo = cbFamiliar2Tipo.SelectedValue.ToString();
                item.Familiar2Empresa = txtFamiliar2Empresa.Text.Trim().ToUpper();
                item.Familiar2EmpresaDireccion = txtFamiliar2EmpresaDireccion.Text.Trim().ToUpper();
                item.Familiar2EmpresaDepartamento = cbFamiliar2EmpresaDepartamento.SelectedValue.ToString();
                item.Familiar2EmpresaMunicipio = cbFamiliar2EmpresaMunicipio.SelectedValue.ToString();
                item.Familiar2EmpresaTelefono = txtFamiliar2EmpresaTelefono.Text.Trim().ToUpper();
                item.Familiar2CasaDireccion = txtFamiliar2CasaDireccion.Text.Trim().ToUpper();
                item.Familiar2CasaDepartamento = cbFamiliar2CasaDepartamento.SelectedValue.ToString();
                item.Familiar2CasaMunicipio = cbFamiliar2CasaMunicipio.SelectedValue.ToString();
                item.Familiar2CasaTelefono = txtFamiliar2CasaTelefono.Text.Trim().ToUpper();

                item.Personal1PrimerNombre = txtPersonal1PrimerNombre.Text.Trim().ToUpper();
                item.Personal1SegundoNombre = txtPersonal1SegundoNombre.Text.Trim().ToUpper();
                item.Personal1TercerNombre = txtPersonal1TercerNombre.Text.Trim().ToUpper();
                item.Personal1PrimerApellido = txtPersonal1PrimerApellido.Text.Trim().ToUpper();
                item.Personal1SegundoApellido = txtPersonal1SegundoApellido.Text.Trim().ToUpper();
                item.Personal1ApellidoCasada = txtPersonal1ApellidoCasada.Text.Trim().ToUpper();
                item.Personal1Celular = txtPersonal1Celular.Text.Trim().ToUpper();
                item.Personal2PrimerNombre = txtPersonal2PrimerNombre.Text.Trim().ToUpper();
                item.Personal2SegundoNombre = txtPersonal2SegundoNombre.Text.Trim().ToUpper();
                item.Personal2TercerNombre = txtPersonal2TercerNombre.Text.Trim().ToUpper();
                item.Personal2PrimerApellido = txtPersonal2PrimerApellido.Text.Trim().ToUpper();
                item.Personal2SegundoApellido = txtPersonal2SegundoApellido.Text.Trim().ToUpper();
                item.Personal2ApellidoCasada = txtPersonal2ApellidoCasada.Text.Trim().ToUpper();
                item.Personal2Celular = txtPersonal2Celular.Text.Trim().ToUpper();

                item.Personal1Tipo = cbPersonal1Tipo.SelectedValue.ToString();
                item.Personal1Empresa = txtPersonal1Empresa.Text.Trim().ToUpper();
                item.Personal1EmpresaDireccion = txtPersonal1EmpresaDireccion.Text.Trim().ToUpper();
                item.Personal1EmpresaDepartamento = cbPersonal1EmpresaDepartamento.SelectedValue.ToString();
                item.Personal1EmpresaMunicipio = cbPersonal1EmpresaMunicipio.SelectedValue.ToString();
                item.Personal1EmpresaTelefono = txtPersonal1EmpresaTelefono.Text.Trim().ToUpper();
                item.Personal1CasaDireccion = txtPersonal1CasaDireccion.Text.Trim().ToUpper();
                item.Personal1CasaDepartamento = cbPersonal1CasaDepartamento.SelectedValue.ToString();
                item.Personal1CasaMunicipio = cbPersonal1CasaMunicipio.SelectedValue.ToString();
                item.Personal1CasaTelefono = txtPersonal1CasaTelefono.Text.Trim().ToUpper();

                item.Personal2Tipo = cbPersonal2Tipo.SelectedValue.ToString();
                item.Personal2Empresa = txtPersonal2Empresa.Text.Trim().ToUpper();
                item.Personal2EmpresaDireccion = txtPersonal2EmpresaDireccion.Text.Trim().ToUpper();
                item.Personal2EmpresaDepartamento = cbPersonal2EmpresaDepartamento.SelectedValue.ToString();
                item.Personal2EmpresaMunicipio = cbPersonal2EmpresaMunicipio.SelectedValue.ToString();
                item.Personal2EmpresaTelefono = txtPersonal2EmpresaTelefono.Text.Trim().ToUpper();
                item.Personal2CasaDireccion = txtPersonal2CasaDireccion.Text.Trim().ToUpper();
                item.Personal2CasaDepartamento = cbPersonal2CasaDepartamento.SelectedValue.ToString();
                item.Personal2CasaMunicipio = cbPersonal2CasaMunicipio.SelectedValue.ToString();
                item.Personal2CasaTelefono = txtPersonal2CasaTelefono.Text.Trim().ToUpper();

                item.JefePrimerNombre = txtJefePrimerNombre.Text.Trim().ToUpper();
                item.JefeSegundoNombre = txtJefeSegundoNombre.Text.Trim().ToUpper();
                item.JefeTercerNombre = txtJefeTercerNombre.Text.Trim().ToUpper();
                item.JefePrimerApellido = txtJefePrimerApellido.Text.Trim().ToUpper();
                item.JefeSegundoApellido = txtJefeSegundoApellido.Text.Trim().ToUpper();
                item.JefeApellidoCasada = txtJefeApellidoCasada.Text.Trim().ToUpper();
                item.JefeCelular = txtJefeCelular.Text.Trim().ToUpper();
                item.ConyuguePrimerNombre = txtConyuguePrimerNombre.Text.Trim().ToUpper();
                item.ConyugueSegundoNombre = txtConyugueSegundoNombre.Text.Trim().ToUpper();
                item.ConyugueTercerNombre = txtConyugueTercerNombre.Text.Trim().ToUpper();
                item.ConyuguePrimerApellido = txtConyuguePrimerApellido.Text.Trim().ToUpper();
                item.ConyugueSegundoApellido = txtConyugueSegundoApellido.Text.Trim().ToUpper();
                item.ConyugueApellidoCasada = txtConyugueApellidoCasada.Text.Trim().ToUpper();
                item.ConyugueTelefono = txtConyugeTelefono.Text.Trim().ToUpper();
               
                item.ConyugueCedula = txtConyugeID.Text.Trim().ToUpper();
                item.ConyugueNIT = txtConyugueNIT.Text.Trim().ToUpper();
                item.Exento = chkExento.Checked ? "S": "N";
                try
                {
                    item.ConyugueFechaNacimiento = Convert.ToDateTime(ConyugueFechaNac.Value);
                }
                catch
                {
                    item.ConyugueFechaNacimiento = null;
                }

                item.ConyugueEmpresa = txtEmpresaConyugue.Text.Trim().ToUpper();
                item.ConyugueCargo = txtCargoConyugue.Text.Trim().ToUpper();
                item.ConyugueIngresos = txtIngresosConyugue.Text.Trim().ToUpper();
                item.ConyugueEmpresaTelefono = txtTelefonoEmpresaConyugue.Text.ToUpper();

                q[0] = item;

                if (!ws.GrabarCliente(ref mCliente, Convert.ToString(Session["Tienda"]), Convert.ToString(Session["Vendedor"]), q, ref mMensaje, Convert.ToString(Session["Usuario"]), mConsumidorFinal, cbTipo.SelectedValue, Convert.ToInt32(cbFinanciera.SelectedValue), cbNivelPrecio.SelectedValue, ref mNombre))
                {
                    lbError.Text = mMensaje;
                    lbErrorGenerales.Text = mMensaje;
                    return;
                }

                txtCodigo.Text = mCliente;
                lbInfo.Text = mMensaje;
                lbInfo2.Text = mMensaje;
                tblInfo.Visible = true;

                Session["TipoVenta"] = null;
                Session["Cliente"] = txtCodigo.Text;
                Response.Redirect("pedidos.aspx");
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al grabar. {0}", ex.Message);
            }
        }

        protected void lkLimpiar_Click(object sender, EventArgs e)
        {
            base.LogActivity(MethodBase.GetCurrentMethod().Name);
            limpiar();
            tblInfo.Visible = false;
        }

        void limpiar()
        {
            try
            {
                lbError.Text = "";
                lbErrorGenerales.Text = "";
                lbErrorSolicitudCredito.Text = "";

                lbInfo.Text = "";
                lbInfo2.Text = "";
                chkExento.Checked = false;
                tblBuscar.Visible = false;
                gridClientes.Visible = false;

                txtCodigo.Text = "0000000";
                cbTipo.SelectedValue = "NR";
                cbFinanciera.SelectedValue = "0";

                cbConsumidorFinal.Checked = false;
                cbPersonaJuridica.Checked = false;

                txtNit.Text = "";
                txtNit.ReadOnly = false;

                txtPrimerNombre.MaxLength = 25;
                txtSegundoNombre.ReadOnly = false;
                txtTercerNombre.ReadOnly = false;
                txtPrimerApellido.ReadOnly = false;
                txtSegundoApellido.ReadOnly = false;
                txtApellidoCasada.ReadOnly = false;

                txtDPI.Text = "";
                txtPrimerNombre.Text = "";
                txtSegundoNombre.Text = "";
                txtTercerNombre.Text = "";
                txtPrimerApellido.Text = "";
                txtSegundoApellido.Text = "";
                txtApellidoCasada.Text = "";

                rbHombre.Checked = false;
                rbMujer.Checked = false;

                txtTelefono1.Text = "";
                txtTelefono2.Text = "";
                txtCelular.Text = "";
                txtEmail.Text = "";
                txtNotas.Text = "";

                //txtFechaNacimiento.Attributes.Clear();
                txtFechaNacimiento.Text = "";
                cbDia.SelectedValue = "--";
                cbMes.SelectedValue = "--";
                txtAnio.Text = "";

                cbFacturarOtroNombre.Checked = false;
                ComboFacturarOtroNombre();

                cbMismaDireccionFactura.Checked = true;
                ComboUsarEstaDireccion();

                cbEntraCamion.Checked = false;
                cbEntraPickup.Checked = false;
                cbSegundoPiso.Checked = false;

                txtCalleAvenida.Text = "";
                txtCasa.Text = "";
                cbZona.SelectedValue = "--";
                txtApartamento.Text = "";
                txtColonia.Text = "";
                txtIndicacionesParaLLegar.Text = "";

                txtCalleAvenidaFacturacion.Text = "";
                txtCasaFacturacion.Text = "";
                cbZonaFacturacion.SelectedValue = "--";
                txtApartamentoFacturacion.Text = "";
                txtColoniaFacturacion.Text = "";

                sugerirDepartamentos();

                CargaMunicipios();
                CargaMunicipiosFacturacion();
                CargaMunicipiosEmpresa();

                sugerirMunicipios();

                txtCalleEmpresa.Text = "";
                txtCasaEmpresa.Text = "";
                cbZonaEmpresa.SelectedValue = "--";
                txtApartamentoEmpresa.Text = "";
                txtColoniaEmpresa.Text = "";
                txtEmpresa.Text = "";
                txtEmpresaDireccion.Text = "";
                cbProfesion.SelectedValue = "";
                cbBanco.SelectedValue = "";
                cbPuesto.SelectedValue = "";
                txtEmpresaTelefono.Text = "";
                txtTiempo.Text = "";
                txtIngresos.Text = "";
                txtEgresos.Text = "";
                cbEstadoCivil.SelectedValue = "S";
                txtNacionalidad.Text = "Guatemalteco";
                txtConyugeNombre.Text = "";
                txtConyugeID.Text = "";
                txtConyugeTelefono.Text = "";
                cbTieneVehiculo.SelectedValue = "N";
                txtMarcaModelo.Text = "";
                txtPlacas.Text = "";
                rbCasaPropia.Checked = false;
                rbCasaFamiliares.Checked = false;
                rbCasaAlquilada.Checked = false;
                txtPagoMensual.Text = "";
                txtTiempoResidir.Text = "";
                txtFinancieraVivienda.Text = "";
                txtContador.Text = "";

                txtExtension.Text = "";
                txtCargasFamiliares.Text = "";
                cbPoliticamente.SelectedValue = "N";
                txtReferenciasBancarias.Text = "";
                txtEmpresaConyugue.Text = "";
                txtCargoConyugue.Text = "";
                txtTiempoConyugue.Text = "";
                txtIngresosConyugue.Text = "";
                txtDireccionEmpresaConyugue.Text = "";
                txtTelefonoEmpresaConyugue.Text = "";

                cbDiaTrabajo.SelectedValue = "--";
                cbMesTrabajo.SelectedValue = "--";
                txtAnioTrabajo.Text = "";

                cbDiaConyugue.SelectedValue = "--";
                cbMesConyugue.SelectedValue = "--";
                txtAnioConyugue.Text = "";

                txtReferenciaPersonal1.Text = "";
                txtReferenciaPersonalResidencia1.Text = "";
                txtReferenciaPersonalTrabajo1.Text = "";
                txtReferenciaPersonalCelular1.Text = "";
                txtReferenciaPersonal2.Text = "";
                txtReferenciaPersonalResidencia2.Text = "";
                txtReferenciaPersonalTrabajo2.Text = "";
                txtReferenciaPersonalCelular2.Text = "";

                txtReferenciaComercial1.Text = "";
                txtReferenciaComercialTelefono1.Text = "";
                txtReferenciaComercialFax1.Text = "";
                txtReferenciaComercialCelular1.Text = "";

                txtReferenciaComercial2.Text = "";
                txtReferenciaComercialTelefono2.Text = "";
                txtReferenciaComercialFax2.Text = "";
                txtReferenciaComercialCelular2.Text = "";

                txtFamiliar1PrimerNombre.Text = "";
                txtFamiliar1SegundoNombre.Text = "";
                txtFamiliar1TercerNombre.Text = "";
                txtFamiliar1PrimerApellido.Text = "";
                txtFamiliar1SegundoApellido.Text = "";
                txtFamiliar1ApellidoCasada.Text = "";
                txtFamiliar1Celular.Text = "";
                txtFamiliar2PrimerNombre.Text = "";
                txtFamiliar2SegundoNombre.Text = "";
                txtFamiliar2TercerNombre.Text = "";
                txtFamiliar2PrimerApellido.Text = "";
                txtFamiliar2SegundoApellido.Text = "";
                txtFamiliar2ApellidoCasada.Text = "";
                txtFamiliar2Celular.Text = "";

                txtFamiliar1Empresa.Text = "";
                txtFamiliar1EmpresaDireccion.Text = "";
                txtFamiliar1EmpresaTelefono.Text = "";
                txtFamiliar1CasaDireccion.Text = "";
                txtFamiliar1CasaTelefono.Text = "";
                txtFamiliar2Empresa.Text = "";
                txtFamiliar2EmpresaDireccion.Text = "";
                txtFamiliar2EmpresaTelefono.Text = "";
                txtFamiliar2CasaDireccion.Text = "";
                txtFamiliar2CasaTelefono.Text = "";

                txtPersonal1PrimerNombre.Text = "";
                txtPersonal1SegundoNombre.Text = "";
                txtPersonal1TercerNombre.Text = "";
                txtPersonal1PrimerApellido.Text = "";
                txtPersonal1SegundoApellido.Text = "";
                txtPersonal1ApellidoCasada.Text = "";
                txtPersonal1Celular.Text = "";
                txtPersonal2PrimerNombre.Text = "";
                txtPersonal2SegundoNombre.Text = "";
                txtPersonal2TercerNombre.Text = "";
                txtPersonal2PrimerApellido.Text = "";
                txtPersonal2SegundoApellido.Text = "";
                txtPersonal2ApellidoCasada.Text = "";
                txtPersonal2Celular.Text = "";

                txtPersonal1Empresa.Text = "";
                txtPersonal1EmpresaDireccion.Text = "";
                txtPersonal1EmpresaTelefono.Text = "";
                txtPersonal1CasaDireccion.Text = "";
                txtPersonal1CasaTelefono.Text = "";
                txtPersonal2Empresa.Text = "";
                txtPersonal2EmpresaDireccion.Text = "";
                txtPersonal2EmpresaTelefono.Text = "";
                txtPersonal2CasaDireccion.Text = "";
                txtPersonal2CasaTelefono.Text = "";

                txtJefePrimerNombre.Text = "";
                txtJefeSegundoNombre.Text = "";
                txtJefeTercerNombre.Text = "";
                txtJefePrimerApellido.Text = "";
                txtJefeSegundoApellido.Text = "";
                txtJefeApellidoCasada.Text = "";
                txtJefeCelular.Text = "";
                txtConyuguePrimerNombre.Text = "";
                txtConyugueSegundoNombre.Text = "";
                txtConyugueTercerNombre.Text = "";
                txtConyuguePrimerApellido.Text = "";
                txtConyugueSegundoApellido.Text = "";
                txtConyugueApellidoCasada.Text = "";
                txtConyugeTelefono.Text = "";

                txtConyugeID.Text = "";
                txtConyugueNIT.Text = "";

                try
                {
                    ConyugueFechaNac.Value = null;
                }
                catch
                {
                    //Nothing
                }

                txtEmpresaConyugue.Text = "";
                txtCargoConyugue.Text = "";
                txtIngresosConyugue.Text = "";
                txtTelefonoEmpresaConyugue.Text.ToUpper();

                cbTipo.Focus();
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al limpiar la página {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }

        protected void cbFinanciera_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaNivelesDePrecio();
        }

        void CargaNivelesDePrecio()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var qNivelesPrecio = ws.DevuelveNivelesPrecioSeleccionFinanciera(cbFinanciera.SelectedValue, Convert.ToString(Session["Tienda"])).ToList();

            wsPuntoVenta.NivelesPrecio itemNivel = new wsPuntoVenta.NivelesPrecio();
            itemNivel.NivelPrecio = "Seleccione ...";
            itemNivel.Factor = 0;
            itemNivel.TipoVenta = "";
            itemNivel.TipoDeVenta = "";
            itemNivel.Financiera = 0;
            itemNivel.NombreFinanciera = "";
            itemNivel.Estatus = "";
            itemNivel.Tipo = "";
            itemNivel.Pagos = "";
            qNivelesPrecio.Add(itemNivel);

            cbNivelPrecio.DataSource = qNivelesPrecio;
            cbNivelPrecio.DataTextField = "NivelPrecio";
            cbNivelPrecio.DataValueField = "NivelPrecio";
            cbNivelPrecio.DataBind();

            cbNivelPrecio.SelectedValue = "Seleccione ...";
        }

        protected void txtNit_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lbErrorGenerales.Text = "";
                if (txtNit.Text.Trim().Length == 0 || txtNit.Text == "CF") return;

                int mPosicionGuion = txtNit.Text.Length - 2;
                if (txtNit.Text.Substring(mPosicionGuion, 1) != "-") txtNit.Text = string.Format("{0}-{1}", txtNit.Text.Substring(0, mPosicionGuion + 1), txtNit.Text.Substring(mPosicionGuion + 1, 1));

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mMensaje = "";

                if (!ws.NitValido(txtNit.Text, ref mMensaje))
                {
                    limpiar();
                    lbErrorGenerales.Text = mMensaje;
                    txtNit.Text = "";
                    txtNit.Focus();
                    return;
                }

                bool mExisteCliente = ws.ExisteCliente("N", "", txtNit.Text, ref mMensaje);
                if (mExisteCliente) CargarCliente("N", "", txtNit.Text);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorGenerales.Text = string.Format("Error al validar el NIT {0} {1}", ex.Message, m);
            }
        }

        void CargarCliente(string tipo, string cliente, string nit)
        {
            limpiar();
            
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveCliente(tipo, cliente, nit);

            if (q[0].PersonaJuridica == "S") cbPersonaJuridica.Checked = true;
            VerificarPersonaJuridica();

            txtCodigo.Text = q[0].Cliente;
            txtPrimerNombre.Text = q[0].PrimerNombre;
            txtSegundoNombre.Text = q[0].SegundoNombre;
            txtTercerNombre.Text = q[0].TercerNombre;
            txtPrimerApellido.Text = q[0].PrimerApellido;
            txtSegundoApellido.Text = q[0].SegundoApellido;
            txtApellidoCasada.Text = q[0].ApellidoCasada;
            txtCalleAvenida.Text = q[0].CalleAvenida;
            txtCasa.Text = q[0].CasaNumero;
            txtApartamento.Text = q[0].Apartamento;
            lbCliente.Text = q[0].Nombre;
            //para la venta de vehiculos en F01
            chkExento.Checked = q[0].Exento.Equals("S") ? true : false;
            //-------
            try
            {
                cbZona.SelectedValue = q[0].Zona;
            }
            catch
            {
                cbZona.SelectedValue = "--";
            }

            rbHombre.Checked = false;
            rbMujer.Checked = false;

            if (q[0].Genero == "H")
            {
                rbHombre.Checked = true;
            }
            else
            {
                rbMujer.Checked = true;
            }

            txtColonia.Text = q[0].Colonia;

            cbDepartamento.AutoPostBack = false;
            cbDepartamentoFacturacion.AutoPostBack = false;

            cbMunicipio.Items.Clear();
            cbMunicipioFacturacion.Items.Clear();
            cbMunicipioEmpresa.Items.Clear();

            try
            {
                cbDepartamento.SelectedValue = q[0].Departamento;
                cbMunicipio.SelectedValue = null;
                CargaMunicipios();
            }
            catch
            {
                cbDepartamento.SelectedIndex = -1;
                lbErrorGenerales.Text = "Debe corregir el departamento.";
            }
            try
            {
                cbMunicipio.SelectedValue = q[0].Municipio;
            }
            catch
            {
                cbMunicipio.SelectedIndex = -1;
                lbErrorGenerales.Text = "Debe corregir el municipio.";
            }

            try
            {
                cbDepartamentoEmpresa.SelectedValue = q[0].DepartamentoEmpresa;
                cbMunicipioEmpresa.SelectedValue = null;
                CargaMunicipiosEmpresa();
            }
            catch
            {
                cbDepartamentoEmpresa.SelectedIndex = -1;
                lbErrorSolicitudCredito.Text = "Debe corregir el departamento de la dirección de la empresa.";
            }
            try
            {
                cbMunicipioEmpresa.SelectedValue = q[0].MunicipioEmpresa;
            }
            catch
            {
                cbMunicipioEmpresa.SelectedIndex = -1;
                lbErrorSolicitudCredito.Text = "Debe corregir el municipio de la dirección de la empresa.";
            }

            if (q[0].ConsumidorFinal == "S") cbConsumidorFinal.Checked = true;
            txtIndicacionesParaLLegar.Text = q[0].IndicacionesParaLlegar;
            txtTelefono1.Text = q[0].Telefono1;
            txtTelefono2.Text = q[0].Telefono2;
            txtCelular.Text = q[0].Celular;
            txtNit.Text = q[0].Nit;
            txtNotas.Text = q[0].Notas;
            txtEmail.Text = q[0].Email;
            txtEmpresa.Text = q[0].Empresa;
            txtEmpresaDireccion.Text = q[0].EmpresaDireccion;
            txtEmpresaTelefono.Text = q[0].EmpresaTelefono;
            cbPuesto.SelectedValue = q[0].Cargo;
            txtTiempo.Text = q[0].TiempoLaborar;
            txtIngresos.Text = q[0].IngresosMes;
            txtEgresos.Text = q[0].GastosMes;
            txtConyugeNombre.Text = q[0].ConyugeNombre;
            txtConyugeID.Text = q[0].ConyugueCedula;
            txtConyugeTelefono.Text = q[0].ConyugueTelefono;
            txtDPI.Text = q[0].DPI;

            txtExtension.Text = q[0].Extension;
            txtCargasFamiliares.Text = q[0].CargasFamiliares;
            cbPoliticamente.SelectedValue = q[0].Politico;
            txtReferenciasBancarias.Text = q[0].ReferenciasBancarias;
            txtEmpresaConyugue.Text = q[0].ConyugueEmpresa;
            txtCargoConyugue.Text = q[0].ConyugueCargo;
            txtTiempoConyugue.Text = q[0].ConyugueTiempo;
            txtIngresosConyugue.Text = q[0].ConyugueIngresos;
            txtDireccionEmpresaConyugue.Text = q[0].ConyugueEmpresaDireccion;
            txtTelefonoEmpresaConyugue.Text = q[0].ConyugueEmpresaTelefono;

            if (q[0].Genero == "M") rbMujer.Checked = true;

            try
            {
                txtFechaNacimiento.Text = q[0].FechaNacimiento.Value.ToShortDateString();

                cbDia.SelectedValue = q[0].FechaNacimiento.Value.Day.ToString();
                cbMes.SelectedValue = q[0].FechaNacimiento.Value.Month.ToString();
                txtAnio.Text = q[0].FechaNacimiento.Value.Year.ToString();

                //txtFechaNacimiento.Attributes.Add("Value", q[0].FechaNacimiento.Value.ToShortDateString());
            }
            catch
            {
                //txtFechaNacimiento.Attributes.Clear();
                txtFechaNacimiento.Text = "";
                cbDia.SelectedValue = "--";
                cbMes.SelectedValue = "--";
                txtAnio.Text = "";
            }
            try
            {
                cbDiaConyugue.SelectedValue = q[0].ConyugueFechaNacimiento.Value.Day.ToString();
                cbMesConyugue.SelectedValue = q[0].ConyugueFechaNacimiento.Value.Month.ToString();
                txtAnioConyugue.Text = q[0].ConyugueFechaNacimiento.Value.Year.ToString();
            }
            catch
            {
                cbDiaConyugue.SelectedValue = "--";
                cbMesConyugue.SelectedValue = "--";
                txtAnioConyugue.Text = "";
            }
            try
            {
                cbDiaTrabajo.SelectedValue = q[0].TrabajoFechaIngreso.Value.Day.ToString();
                cbMesTrabajo.SelectedValue = q[0].TrabajoFechaIngreso.Value.Month.ToString();
                txtAnioTrabajo.Text = q[0].TrabajoFechaIngreso.Value.Year.ToString();
            }
            catch
            {
                cbDiaTrabajo.SelectedValue = "--";
                cbMesTrabajo.SelectedValue = "--";
                txtAnioTrabajo.Text = "";
            }

            if (q[0].FacturarOtroNombre == "S")
            {
                cbFacturarOtroNombre.Checked = true;
                tblNombreFacturacion.Visible = true;
            }

            txtNitFactura.Text = q[0].NitFactura;
            txtNombreFactura.Text = q[0].NombreFactura;

            if (q[0].EntraCamion == "S") cbEntraCamion.Checked = true;
            if (q[0].EntraPickup == "S") cbEntraPickup.Checked = true;
            if (q[0].EsSegundoPiso == "S") cbSegundoPiso.Checked = true;

            if (q[0].UsarDireccEnFactura == "N")
            {
                cbMismaDireccionFactura.Checked = false;
                direccionEntrega.Visible = true;
            }

            txtCalleAvenidaFacturacion.Text = q[0].CalleAvenidaFacturacion;
            txtCasaFacturacion.Text = q[0].CasaNumeroFacturacion;
            txtApartamentoFacturacion.Text = q[0].ApartamentoFacturacion;

            try
            {
                cbZonaFacturacion.SelectedValue = q[0].ZonaFacturacion;
            }
            catch
            {
                cbZonaFacturacion.SelectedValue = "--";
            }
            
            txtColoniaFacturacion.Text = q[0].ColoniaFacturacion;

            try
            {
                cbDepartamentoFacturacion.SelectedValue = q[0].DepartamentoFacturacion;
                cbMunicipioFacturacion.SelectedValue = null;
                CargaMunicipiosFacturacion();
            }
            catch
            {
                cbDepartamentoFacturacion.SelectedIndex = -1;
                lbErrorGenerales.Text = "Debe corregir el departamento en la dirección de la factura.";
            }
            try
            {
                cbMunicipioFacturacion.SelectedValue = q[0].MunicipioFacturacion;
            }
            catch
            {
                cbMunicipioFacturacion.SelectedIndex = -1;
                lbErrorGenerales.Text = "Debe corregir el municipio en la dirección de la factura.";
            }

            if (Session["TipoVenta"] != null)
            {
                cbTipo.SelectedValue = Convert.ToString(Session["TipoVenta"]);
                cbFinanciera.SelectedValue = Convert.ToString(Session["Financiera"]);
                
                CargaNivelesDePrecio();
                cbNivelPrecio.SelectedValue = ws.DevuelveNivelPadre(cbTipo.SelectedValue, Convert.ToInt32(cbFinanciera.SelectedValue), Convert.ToString(Session["NivelPrecio"]));
                tblSolicitudCredito.Visible = true;

                string mMensajeDatos = "Debe revisar: DPI, Profesión, Puesto, Fecha Ingreso de Labores, Dirección de la empresa, Ingresos al mes, Egresos al mes, Cargas Familiares, Referencias Bancarias, No. de cuenta, Referencias personales y comerciales";

                lbErrorGenerales.Text = mMensajeDatos;
                lbErrorSolicitudCredito.Text = mMensajeDatos;
                cbProfesion.Focus();
            }
            else
            {
                try
                {
                    cbTipo.SelectedValue = q[0].TipoVenta;
                }
                catch
                {
                    cbTipo.SelectedValue = "NR";
                }

                try
                {
                    cbFinanciera.SelectedValue = q[0].Financiera.ToString();
                }
                catch
                {
                    cbFinanciera.SelectedValue = "1";
                }
                

                if (q[0].Financiera > 0)
                {
                    CargaNivelesDePrecio();

                    try
                    {
                        cbNivelPrecio.SelectedValue = ws.DevuelveNivelPadre(cbTipo.SelectedValue, q[0].Financiera, q[0].NivelPrecio);
                    }
                    catch
                    {
                        cbNivelPrecio.SelectedValue = "ContadoEfect";
                    }
                    
                    ValidaTipoNivelPrecio();
                }
            }

            cbEstadoCivil.SelectedValue = q[0].EstadoCivil;
            txtNacionalidad.Text = q[0].Nacionalidad;
            cbProfesion.SelectedValue = q[0].Profesion;
            cbBanco.SelectedValue = q[0].Banco;
            cbTieneVehiculo.SelectedValue = q[0].TieneVehiculo;
            txtMarcaModelo.Text = q[0].MarcaModelo;
            txtPlacas.Text = q[0].Placas;

            if (q[0].TipoCasa == "P") rbCasaPropia.Checked = true;
            if (q[0].TipoCasa == "F") rbCasaFamiliares.Checked = true;
            if (q[0].TipoCasa == "A") rbCasaAlquilada.Checked = true;

            txtTiempoResidir.Text = q[0].TiempoResidir;
            txtPagoMensual.Text = q[0].PagoMensual;
            txtFinancieraVivienda.Text = q[0].FinancieraPaga;
            txtContador.Text = q[0].NumeroContador;
            txtReferenciaPersonal1.Text = q[0].RefPersonalNombre1;
            txtReferenciaPersonalResidencia1.Text = q[0].RefPersonalTelResidencia1;
            txtReferenciaPersonalTrabajo1.Text = q[0].RefPersonalTelTrabajo1;
            txtReferenciaPersonalCelular1.Text = q[0].RefPersonalCelular1;
            txtReferenciaPersonal2.Text = q[0].RefPersonalNombre2;
            txtReferenciaPersonalResidencia2.Text = q[0].RefPersonalTelResidencia2;
            txtReferenciaPersonalTrabajo2.Text = q[0].RefPersonalTelTrabajo2;
            txtReferenciaPersonalCelular2.Text = q[0].RefPersonalCelular2;
            txtReferenciaComercial1.Text = q[0].RefComercialNombre1;
            txtReferenciaComercialTelefono1.Text = q[0].RefComercialTelefono1;
            txtReferenciaComercialFax1.Text = q[0].RefComercialFax1;
            txtReferenciaComercialCelular1.Text = q[0].RefComercialCelular1;
            txtReferenciaComercial2.Text = q[0].RefComercialNombre2;
            txtReferenciaComercialTelefono2.Text = q[0].RefComercialTelefono2;
            txtReferenciaComercialFax2.Text = q[0].RefComercialFax2;
            txtReferenciaComercialCelular2.Text = q[0].RefComercialCelular2;

            txtCalleEmpresa.Text = q[0].CalleAvenidaEmpresa;
            txtCasaEmpresa.Text = q[0].CasaNumeroEmpresa;
            cbZonaEmpresa.SelectedValue = q[0].ZonaEmpresa;
            txtApartamentoEmpresa.Text = q[0].ApartamentoEmpresa;
            txtColoniaEmpresa.Text = q[0].ColoniaEmpresa;

            cbDepartamento.AutoPostBack = true;
            cbDepartamentoFacturacion.AutoPostBack = true;

            string mProfesion2 = ws.DevuelveProfesion2(txtCodigo.Text);
            if (mProfesion2.Trim().Length > 0) cbProfesion.ToolTip = string.Format("Seleccione la profesión u oficio del cliente, anteriormente tenía: {0}.", mProfesion2);

            string mPuesto2 = ws.DevuelvePuesto2(txtCodigo.Text);
            if (mPuesto2.Trim().Length > 0) cbPuesto.ToolTip = string.Format("Seleccione el puesto que ocupa el cliente en su lugar de trabajo, anteriormente tenía: {0}.", mPuesto2);

            txtCalleEmpresa.ToolTip = string.Format("{0}{1}Dirección anterior: {2}", txtCalleEmpresa.ToolTip, System.Environment.NewLine, q[0].EmpresaDireccion);
            txtCasaEmpresa.ToolTip = string.Format("{0}{1}Dirección anterior: {2}", txtCasaEmpresa.ToolTip, System.Environment.NewLine, q[0].EmpresaDireccion);
            cbZonaEmpresa.ToolTip = string.Format("{0}{1}Dirección anterior: {2}", cbZonaEmpresa.ToolTip, System.Environment.NewLine, q[0].EmpresaDireccion);
            txtApartamentoEmpresa.ToolTip = string.Format("{0}{1}Dirección anterior: {2}", txtApartamentoEmpresa.ToolTip, System.Environment.NewLine, q[0].EmpresaDireccion);
            txtColoniaEmpresa.ToolTip = string.Format("{0}{1}Dirección anterior: {2}", txtColonia.ToolTip, System.Environment.NewLine, q[0].EmpresaDireccion);

            //Referencias
            txtFamiliar1PrimerNombre.Text = q[0].Familiar1PrimerNombre;
            txtFamiliar1SegundoNombre.Text = q[0].Familiar1SegundoNombre;
            txtFamiliar1TercerNombre.Text = q[0].Familiar1TercerNombre;
            txtFamiliar1PrimerApellido.Text = q[0].Familiar1PrimerApellido;
            txtFamiliar1SegundoApellido.Text = q[0].Familiar1SegundoApellido;
            txtFamiliar1ApellidoCasada.Text = q[0].Familiar1ApellidoCasada;
            txtFamiliar1Celular.Text = q[0].Familiar1Celular;
            txtFamiliar2PrimerNombre.Text = q[0].Familiar2PrimerNombre;
            txtFamiliar2SegundoNombre.Text = q[0].Familiar2SegundoNombre;
            txtFamiliar2TercerNombre.Text = q[0].Familiar2TercerNombre;
            txtFamiliar2PrimerApellido.Text = q[0].Familiar2PrimerApellido;
            txtFamiliar2SegundoApellido.Text = q[0].Familiar2SegundoApellido;
            txtFamiliar2ApellidoCasada.Text = q[0].Familiar2ApellidoCasada;
            txtFamiliar2Celular.Text = q[0].Familiar2Celular;

            string mInfo = "";

            cbFamiliar1CasaMunicipio.Items.Clear();
            cbFamiliar2EmpresaMunicipio.Items.Clear();
            cbFamiliar2CasaMunicipio.Items.Clear();
            cbPersonal1EmpresaMunicipio.Items.Clear();
            cbPersonal1CasaMunicipio.Items.Clear();
            cbPersonal2EmpresaMunicipio.Items.Clear();
            cbPersonal2CasaMunicipio.Items.Clear();

            cbFamiliar1EmpresaMunicipio.SelectedValue = null;
            cbFamiliar1CasaMunicipio.SelectedValue = null;
            cbFamiliar2EmpresaMunicipio.SelectedValue = null;
            cbFamiliar2CasaMunicipio.SelectedValue = null;
            cbPersonal1EmpresaMunicipio.SelectedValue = null;
            cbPersonal1CasaMunicipio.SelectedValue = null;
            cbPersonal2EmpresaMunicipio.SelectedValue = null;
            cbPersonal2CasaMunicipio.SelectedValue = null;

            cbFamiliar1EmpresaMunicipio.SelectedIndex = -1;
            cbFamiliar1CasaMunicipio.SelectedIndex = -1;
            cbFamiliar2EmpresaMunicipio.SelectedIndex = -1;
            cbFamiliar2CasaMunicipio.SelectedIndex = -1;
            cbPersonal1EmpresaMunicipio.SelectedIndex = -1;
            cbPersonal1CasaMunicipio.SelectedIndex = -1;
            cbPersonal2EmpresaMunicipio.SelectedIndex = -1;
            cbPersonal2CasaMunicipio.SelectedIndex = -1;

            try
            {
                mInfo = cbFamiliar1Tipo.SelectedValue.ToString();
                cbFamiliar1Tipo.SelectedValue = q[0].Familiar1Tipo;
            }
            catch
            {
                cbFamiliar1Tipo.SelectedValue = mInfo;
            }
            txtFamiliar1Empresa.Text = q[0].Familiar1Empresa;
            txtFamiliar1EmpresaDireccion.Text = q[0].Familiar1EmpresaDireccion;
            try
            {
                mInfo = cbFamiliar1EmpresaDepartamento.SelectedValue.ToString();
                cbFamiliar1EmpresaDepartamento.SelectedValue = q[0].Familiar1EmpresaDepartamento;
            }
            catch
            {
                cbFamiliar1EmpresaDepartamento.SelectedValue = mInfo;
            }
            try
            {
                CargaMunicipiosFamiliar1Empresa();
            }
            catch
            {
                //Nothing
            }
            try
            {
                mInfo = cbFamiliar1EmpresaMunicipio.SelectedValue.ToString();
                cbFamiliar1EmpresaMunicipio.SelectedValue = q[0].Familiar1EmpresaMunicipio;
            }
            catch
            {
                cbFamiliar1EmpresaMunicipio.SelectedValue = mInfo;
            }
            txtFamiliar1EmpresaTelefono.Text = q[0].Familiar1EmpresaTelefono;
            txtFamiliar1CasaDireccion.Text = q[0].Familiar1CasaDireccion;
            try
            {
                mInfo = cbFamiliar1CasaDepartamento.SelectedValue.ToString();
                cbFamiliar1CasaDepartamento.SelectedValue = q[0].Familiar1CasaDepartamento;
            }
            catch
            {
                cbFamiliar1CasaDepartamento.SelectedValue = mInfo;
            }
            try
            {
                CargaMunicipiosFamiliar1Casa();
            }
            catch
            {
                //Nothing
            }
            try
            {
                mInfo = cbFamiliar1CasaMunicipio.SelectedValue.ToString();
                cbFamiliar1CasaMunicipio.SelectedValue = q[0].Familiar1CasaMunicipio;
            }
            catch
            {
                cbFamiliar1CasaMunicipio.SelectedValue = mInfo;
            }
            txtFamiliar1CasaTelefono.Text = q[0].Familiar1CasaTelefono;

            try
            {
                mInfo = cbFamiliar2Tipo.SelectedValue.ToString();
                cbFamiliar2Tipo.SelectedValue = q[0].Familiar2Tipo;
            }
            catch
            {
                cbFamiliar2Tipo.SelectedValue = mInfo;
            }
            txtFamiliar2Empresa.Text = q[0].Familiar2Empresa;
            txtFamiliar2EmpresaDireccion.Text = q[0].Familiar2EmpresaDireccion;
            try
            {
                mInfo = cbFamiliar2EmpresaDepartamento.SelectedValue.ToString();
                cbFamiliar2EmpresaDepartamento.SelectedValue = q[0].Familiar2EmpresaDepartamento;
            }
            catch
            {
                cbFamiliar2EmpresaDepartamento.SelectedValue = mInfo;
            }
            try
            {
                CargaMunicipiosFamiliar2Empresa();
            }
            catch
            {
                //Nothing
            }
            try
            {
                mInfo = cbFamiliar2EmpresaMunicipio.SelectedValue.ToString();
                cbFamiliar2EmpresaMunicipio.SelectedValue = q[0].Familiar2EmpresaMunicipio;
            }
            catch
            {
                cbFamiliar2EmpresaMunicipio.SelectedValue = mInfo;
            }
            txtFamiliar2EmpresaTelefono.Text = q[0].Familiar2EmpresaTelefono;
            txtFamiliar2CasaDireccion.Text = q[0].Familiar2CasaDireccion;
            try
            {
                mInfo = cbFamiliar2CasaDepartamento.SelectedValue.ToString();
                cbFamiliar2CasaDepartamento.SelectedValue = q[0].Familiar2CasaDepartamento;
            }
            catch
            {
                cbFamiliar2CasaDepartamento.SelectedValue = mInfo;
            }
            try
            {
                CargaMunicipiosFamiliar2Casa();
            }
            catch
            {
                //Nothing
            }
            try
            {
                mInfo = cbFamiliar2CasaMunicipio.SelectedValue.ToString();
                cbFamiliar2CasaMunicipio.SelectedValue = q[0].Familiar2CasaMunicipio;
            }
            catch
            {
                cbFamiliar2CasaMunicipio.SelectedValue = mInfo;
            }
            txtFamiliar2CasaTelefono.Text = q[0].Familiar2CasaTelefono;

            txtPersonal1PrimerNombre.Text = q[0].Personal1PrimerNombre;
            txtPersonal1SegundoNombre.Text = q[0].Personal1SegundoNombre;
            txtPersonal1TercerNombre.Text = q[0].Personal1TercerNombre;
            txtPersonal1PrimerApellido.Text = q[0].Personal1PrimerApellido;
            txtPersonal1SegundoApellido.Text = q[0].Personal1SegundoApellido;
            txtPersonal1ApellidoCasada.Text = q[0].Personal1ApellidoCasada;
            txtPersonal1Celular.Text = q[0].Personal1Celular;
            txtPersonal2PrimerNombre.Text = q[0].Personal2PrimerNombre;
            txtPersonal2SegundoNombre.Text = q[0].Personal2SegundoNombre;
            txtPersonal2TercerNombre.Text = q[0].Personal2TercerNombre;
            txtPersonal2PrimerApellido.Text = q[0].Personal2PrimerApellido;
            txtPersonal2SegundoApellido.Text = q[0].Personal2SegundoApellido;
            txtPersonal2ApellidoCasada.Text = q[0].Personal2SegundoApellido;
            txtPersonal2Celular.Text = q[0].Personal2Celular;

            txtJefePrimerNombre.Text = q[0].JefePrimerNombre;
            txtJefeSegundoNombre.Text = q[0].JefeSegundoNombre;
            txtJefeTercerNombre.Text = q[0].JefeTercerNombre;
            txtJefePrimerApellido.Text = q[0].JefePrimerApellido;
            txtJefeSegundoApellido.Text = q[0].JefeSegundoApellido;
            txtJefeApellidoCasada.Text = q[0].JefeApellidoCasada;
            txtJefeCelular.Text = q[0].JefeCelular;
            txtConyuguePrimerNombre.Text = q[0].ConyuguePrimerNombre;
            txtConyugueSegundoNombre.Text = q[0].ConyugueSegundoNombre;
            txtConyugueTercerNombre.Text = q[0].ConyugueTercerNombre;
            txtConyuguePrimerApellido.Text = q[0].ConyuguePrimerApellido;
            txtConyugueSegundoApellido.Text = q[0].ConyugueSegundoApellido;
            txtConyugueApellidoCasada.Text = q[0].ConyugueApellidoCasada;
            txtConyugeTelefono.Text = q[0].ConyugueTelefono;

            try
            {
                mInfo = cbPersonal1Tipo.SelectedValue.ToString();
                cbPersonal1Tipo.SelectedValue = q[0].Personal1Tipo;
            }
            catch
            {
                cbPersonal1Tipo.SelectedValue = mInfo;
            }
            txtPersonal1Empresa.Text = q[0].Personal1Empresa;
            txtPersonal1EmpresaDireccion.Text = q[0].Personal1EmpresaDireccion;
            try
            {
                mInfo = cbPersonal1EmpresaDepartamento.SelectedValue.ToString();
                cbPersonal1EmpresaDepartamento.SelectedValue = q[0].Personal1EmpresaDepartamento;
            }
            catch
            {
                cbPersonal1EmpresaDepartamento.SelectedValue = mInfo;
            }
            try
            {
                CargaMunicipiosPersonal1Empresa();
            }
            catch
            {
                //Nothing
            }
            try
            {
                mInfo = cbPersonal1EmpresaMunicipio.SelectedValue.ToString();
                cbPersonal1EmpresaMunicipio.SelectedValue = q[0].Personal1EmpresaMunicipio;
            }
            catch
            {
                cbPersonal1EmpresaMunicipio.SelectedValue = mInfo;
            }
            txtPersonal1EmpresaTelefono.Text = q[0].Personal1EmpresaTelefono;
            txtPersonal1CasaDireccion.Text = q[0].Personal1CasaDireccion;
            try
            {
                mInfo = cbPersonal1CasaDepartamento.SelectedValue.ToString();
                cbPersonal1CasaDepartamento.SelectedValue = q[0].Personal1CasaDepartamento;
            }
            catch
            {
                cbPersonal1CasaDepartamento.SelectedValue = mInfo;
            }
            try
            {
                CargaMunicipiosPersonal1Casa();
            }
            catch
            {
                //Nothing
            }
            try
            {
                mInfo = cbPersonal1CasaMunicipio.SelectedValue.ToString();
                cbPersonal1CasaMunicipio.SelectedValue = q[0].Personal1CasaMunicipio;
            }
            catch
            {
                cbPersonal1CasaMunicipio.SelectedValue = mInfo;
            }
            txtPersonal1CasaTelefono.Text = q[0].Personal1CasaTelefono;

            try
            {
                mInfo = cbPersonal2Tipo.SelectedValue.ToString();
                cbPersonal2Tipo.SelectedValue = q[0].Personal2Tipo;
            }
            catch
            {
                cbPersonal2Tipo.SelectedValue = mInfo;
            }
            txtPersonal2Empresa.Text = q[0].Personal2Empresa;
            txtPersonal2EmpresaDireccion.Text = q[0].Personal2EmpresaDireccion;
            try
            {
                mInfo = cbPersonal2EmpresaDepartamento.SelectedValue.ToString();
                cbPersonal2EmpresaDepartamento.SelectedValue = q[0].Personal2EmpresaDepartamento;
            }
            catch
            {
                cbPersonal2EmpresaDepartamento.SelectedValue = mInfo;
            }
            try
            {
                CargaMunicipiosPersonal2Empresa();
            }
            catch
            {
                //Nothing
            }
            try
            {
                mInfo = cbPersonal2EmpresaMunicipio.SelectedValue.ToString();
                cbPersonal2EmpresaMunicipio.SelectedValue = q[0].Personal2EmpresaMunicipio;
            }
            catch
            {
                cbPersonal2EmpresaMunicipio.SelectedValue = mInfo;
            }
            txtPersonal2EmpresaTelefono.Text = q[0].Personal2EmpresaTelefono;
            txtPersonal2CasaDireccion.Text = q[0].Personal2CasaDireccion;
            try
            {
                mInfo = cbPersonal2CasaDepartamento.SelectedValue.ToString();
                cbPersonal2CasaDepartamento.SelectedValue = q[0].Personal2CasaDepartamento;
            }
            catch
            {
                cbPersonal2CasaDepartamento.SelectedValue = mInfo;
            }
            try
            {
                CargaMunicipiosPersonal2Casa();
            }
            catch
            {
                //Nothing
            }
            try
            {
                mInfo = cbPersonal2CasaMunicipio.SelectedValue.ToString();
                cbPersonal2CasaMunicipio.SelectedValue = q[0].Personal2CasaMunicipio;
            }
            catch
            {
                cbPersonal2CasaMunicipio.SelectedValue = mInfo;
            }
            txtPersonal2CasaTelefono.Text = q[0].Personal2CasaTelefono;

            txtConyugeID.Text = q[0].ConyugueDPI;
            txtConyugueNIT.Text = q[0].ConyugueNIT;

            try
            {
                ConyugueFechaNac.Value = q[0].ConyugueFechaNacimiento;
            }
            catch
            {
                //Nothing
            }

            Session["Cliente"] = q[0].Cliente;
            tblInfo.Visible = true;
        }

        protected void cbConsumidorFinal_CheckedChanged(object sender, EventArgs e)
        {
            if (cbConsumidorFinal.Checked)
            {
                txtNit.Text = "CF";
                txtNit.ReadOnly = true;
                txtDPI.Focus();
            }
            else
            {
                txtNit.Text = "";
                txtNit.ReadOnly = false;
                txtNit.Focus();
            }
        }

        protected void cbMismaDireccionFactura_CheckedChanged(object sender, EventArgs e)
        {
            ComboUsarEstaDireccion();
        }

        void ComboUsarEstaDireccion()
        {
            direccionEntrega.Visible = !cbMismaDireccionFactura.Checked;

            if (!cbMismaDireccionFactura.Checked)
            {
                txtCalleAvenida.Text = "";
                txtCasa.Text = "";
                cbZona.SelectedValue = "--";
                txtApartamento.Text = "";
                txtColonia.Text = "";
            }
        }

        protected void lbBuscarCliente_Click(object sender, EventArgs e)
        {
            habilitarBusquedaCliente();
        }

        void habilitarBusquedaCliente()
        {
            tblBuscar.Visible = true;

            lbErrorGenerales.Text = "";
            txtCodigoBuscar.Text = "";
            txtNombreBuscar.Text = "";
            txtNitBuscar.Text = "";
            txtTelefono.Text = "";

            txtNombreBuscar.Focus();
        }

        protected void cbDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaMunicipios();

            //if (cbMismaDireccionFactura.Checked)
            //{
            //    cbDepartamentoFacturacion.SelectedValue = cbDepartamento.SelectedValue;
            //    CargaMunicipiosFacturacion();
            //}
        }

        protected void cbDepartamentoFacturacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaMunicipiosFacturacion();
        }

        protected void cbNivelPrecio_SelectedIndexChanged(object sender, EventArgs e)
        {
            ValidaTipoNivelPrecio();
        }

        void ValidaTipoNivelPrecio()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            try
            {
                double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                cbNivelPrecio.ToolTip = string.Format("Factor: {0}", String.Format("{0:0.000000}", mFactor));
            }
            catch
            {
                //Nothing
            }

            string mTipo = ws.DevuelveTipoNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

            tblSolicitudCredito.Visible = false;
            tblReferenciasInter.Visible = false;

            tblJefe.Visible = false;
            tblConyugue.Visible = false;
            tblReferenciasAtid.Visible = false;

            if (mTipo == "CR")
            {
                tblSolicitudCredito.Visible = true;

                if (cbFinanciera.SelectedValue.ToString() == "12")
                {
                    tblJefe.Visible = true;
                    tblConyugue.Visible = true;
                    tblReferenciasAtid.Visible = true;
                }
                else
                {
                    tblReferenciasInter.Visible = true;
                }
            }
        }

        protected void lkNuevoCliente_Click(object sender, EventArgs e)
        {
            limpiar();
            Session["Cliente"] = null;
            tblInfo.Visible = false;
        }

        protected void lkSeleccionar_Click(object sender, EventArgs e)
        {

        }

        protected void txtCodigoBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtCodigoBuscar.Text.Trim().Length == 0) return;
            BuscarCliente("C");
        }

        protected void txtNombreBuscar_TextChanged(object sender, EventArgs e)
        {
            base.LogActivity(MethodBase.GetCurrentMethod().Name);
            if (txtNombreBuscar.Text.Trim().Length == 0) return;

            string mTipo = "N";
            try
            {
                Int32 mNumero = Convert.ToInt32(txtNombreBuscar.Text.Substring(0, 1));
                mTipo = "C";
            }
            catch
            {
                mTipo = "N";
            }

            BuscarCliente(mTipo);
        }

        protected void txtNitBuscar_TextChanged(object sender, EventArgs e)
        {
            base.LogActivity(MethodBase.GetCurrentMethod().Name);
            if (txtNitBuscar.Text.Trim().Length == 0) return;
            BuscarCliente("T");
        }

        protected void txtTelefono_TextChanged(object sender, EventArgs e)
        {
            base.LogActivity(MethodBase.GetCurrentMethod().Name);
            if (txtTelefono.Text.Trim().Length == 0) return;
            BuscarCliente("E");
        }

        void BuscarCliente(string tipo)
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            string mTipo = tipo;
            string mTelefono = txtTelefono.Text.Trim();

            if (tipo == "1")
            {
                mTipo = "E";
                mTelefono = txtTelefono1.Text.Trim();
            }
            if (tipo == "2")
            {
                mTipo = "E";
                mTelefono = txtTelefono2.Text.Trim();
            }
            if (tipo == "3")
            {
                mTipo = "E";
                mTelefono = txtCelular.Text.Trim();
            }
            
            string mCodigo = txtCodigoBuscar.Text.Trim();
            string mNombre = txtNombreBuscar.Text.Trim().ToUpper();

            if (mTipo == "C" && mNombre.Trim().Length > 0)
            {
                mCodigo = mNombre;
                mNombre = "";
            }

            var q = ws.DevuelveClientes(tipo, mCodigo, mNombre, txtNitBuscar.Text.Trim().ToUpper(), mTelefono);

            gridClientes.DataSource = q;
            gridClientes.DataBind();
            gridClientes.Visible = true;
            gridClientes.SelectedIndex = -1;

            lbErrorGenerales.Text = "";
            if (q.Length == 0 && tipo != "1" && tipo != "2" && tipo != "3") lbErrorGenerales.Text = "No existen clientes con el criterio de búsqueda ingresado.";

            if (q.Count() == 1 && tipo != "1" && tipo != "2" && tipo != "3")
            {
                gridClientes.SelectedIndex = 0;
                seleccionarCliente();
            }
            if (q.Count() > 0)
            {
                gridClientes.Focus();
                gridClientes.SelectedIndex = 0;
                LinkButton lkSeleccionar = (LinkButton)gridClientes.SelectedRow.FindControl("lkSeleccionar");

                lkSeleccionar.Focus();
            }
        }

        protected void lkOcultarBusqueda_Click(object sender, EventArgs e)
        {
            lbErrorGenerales.Text = "";
            tblBuscar.Visible = false;
            gridClientes.Visible = false;
            txtPrimerNombre.Focus();
        }

        void seleccionarCliente()
        {
            GridViewRow gvr = gridClientes.SelectedRow;
            CargarCliente("C", gvr.Cells[1].Text, "I");
        }

        protected void gridClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            seleccionarCliente();
        }

        protected void gridClientes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void cbTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaNivelesDePrecio();
        }

        protected void txtTelefono1_TextChanged(object sender, EventArgs e)
        {
            if (txtTelefono1.Text.Trim().Length == 0) return;
            if (!cbConsumidorFinal.Checked) return;
            BuscarCliente("1");
        }

        protected void txtTelefono2_TextChanged(object sender, EventArgs e)
        {
            if (txtTelefono2.Text.Trim().Length == 0) return;
            if (!cbConsumidorFinal.Checked) return;
            BuscarCliente("2");
        }

        protected void txtCelular_TextChanged(object sender, EventArgs e)
        {
            if (txtCelular.Text.Trim().Length == 0) return;
            if (!cbConsumidorFinal.Checked) return;
            BuscarCliente("3");
        }

        protected void cbDepartamentoEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaMunicipiosEmpresa();
        }

        protected void cbPersonaJuridica_CheckedChanged(object sender, EventArgs e)
        {
            VerificarPersonaJuridica();
        }

        void VerificarPersonaJuridica()
        {
            try
            {
                lbErrorGenerales.Text = "";

                txtPrimerNombre.Text = "";
                txtSegundoNombre.Text = "";
                txtTercerNombre.Text = "";
                txtPrimerApellido.Text = "";
                txtSegundoApellido.Text = "";
                txtApellidoCasada.Text = "";

                if (cbPersonaJuridica.Checked)
                {
                    txtPrimerNombre.MaxLength = 80;
                    txtSegundoNombre.ReadOnly = true;
                    txtTercerNombre.ReadOnly = true;
                    txtPrimerApellido.ReadOnly = true;
                    txtSegundoApellido.ReadOnly = true;
                    txtApellidoCasada.ReadOnly = true;
                }
                else
                {
                    txtPrimerNombre.MaxLength = 25;
                    txtSegundoNombre.ReadOnly = false;
                    txtTercerNombre.ReadOnly = false;
                    txtPrimerApellido.ReadOnly = false;
                    txtSegundoApellido.ReadOnly = false;
                    txtApellidoCasada.ReadOnly = false;
                }

                txtPrimerNombre.Focus();
            }
            catch (Exception ex)
            {
                lbErrorGenerales.Text = string.Format("Error {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }

        protected void txtNitFactura_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lbErrorGenerales.Text = "";
                if (txtNitFactura.Text.Trim().Length == 0 || txtNitFactura.Text == "CF") return;

                int mPosicionGuion = txtNitFactura.Text.Length - 2;
                if (txtNitFactura.Text.Substring(mPosicionGuion, 1) != "-") txtNitFactura.Text = string.Format("{0}-{1}", txtNitFactura.Text.Substring(0, mPosicionGuion + 1), txtNitFactura.Text.Substring(mPosicionGuion + 1, 1));

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mMensaje = "";

                if (!ws.NitValido(txtNitFactura.Text, ref mMensaje))
                {
                    lbErrorGenerales.Text = mMensaje;
                    txtNitFactura.Text = "";
                    txtNitFactura.Focus();
                    return;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorGenerales.Text = string.Format("Error al validar el NIT {0} {1}", ex.Message, m);
            }
        }

        protected void cbFamiliar1EmpresaDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaMunicipiosFamiliar1Empresa();
        }

        protected void cbFamiliar1CasaDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaMunicipiosFamiliar1Casa();
        }

        protected void cbFamiliar2EmpresaDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaMunicipiosFamiliar2Empresa();
        }

        protected void cbFamiliar2CasaDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaMunicipiosFamiliar2Casa();
        }

        protected void cbPersonal1EmpresaDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaMunicipiosPersonal1Empresa();
        }

        protected void cbPersonal1CasaDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaMunicipiosPersonal1Casa();
        }

        protected void cbPersonal2EmpresaDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaMunicipiosPersonal2Empresa();
        }

        protected void cbPersonal2CasaDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaMunicipiosPersonal2Casa();
        }

    }
}