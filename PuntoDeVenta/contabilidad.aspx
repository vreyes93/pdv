﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="contabilidad.aspx.cs" Inherits="PuntoDeVenta.contabilidad" MaintainScrollPositionOnPostback="true" %>
<%@ Register assembly="DevExpress.Web.v16.2, Version=16.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<%@ Register Src="ToolbarExport.ascx" TagName="ToolbarExport" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v16.2" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">


        function OnBatchEditEndEditing(s, e) {
            setTimeout(function () {
                s.UpdateEdit();
            }, 0);
        }


        function HidePopup() {
            pcCuentas.Hide();
        }
        function ShowPcCuentaInicial() {
            txtCuenta.SetText("I");
            pcCuentas.Show();
        }
        function ShowPcCuentaFinal() {
            txtCuenta.SetText("F");
            pcCuentas.Show();
        }
        function ShowPcCliente() {
            pcClientes.Show();
        }

        function OnClickConfimacion(s, e) {

            var dd = fechaRigeDesembolso.GetValue().getDate();
            var mm = fechaRigeDesembolso.GetValue().getMonth() + 1;
            var yyyy = fechaRigeDesembolso.GetValue().getFullYear();

            clrefFechaRigeDesembolso.SetText(dd + '/' + mm + '/' + yyyy);

            var popup = ASPxClientControl.Cast('pcConfirmacion');
            popup.Show();
        }

        function OnClickConfirmButton(s, e) {
            var popup = ASPxClientControl.Cast('pcConfirmacion');
            popup.Hide();
        }
    </script>

    <style type="text/css">
        .auto-style1 {
            width: 128px;
        }

        .NewStyleMessage {
            font-family: Verdana;
            font-size: 14px;
            color: #808082;
            font-weight: bold;
            align-content: center;
        }

        .auto-style2 {
            height: 23px;
        }

        .tags_bancredit {
            list-style: none;
            margin: 0;
            overflow: hidden;
            padding: 0;
        }

        .tag_bancredit::after {
            background: transparent;
            border-bottom: 13px solid transparent;
            border-left: 10px solid transparent;
            border-top: 13px solid transparent;
            content: '';
            border-left-color: orange;
            position: absolute;
            right: 0;
            top: 0;
        }

        .tags_bancredit li {
            float: left;
        }

        .tag_bancredit {
            background: orange;
            border-radius: 3px 0 0 3px;
            font-size: 7pt;
            color: #ffffff;
            display: inline-block;
            height: 26px;
            line-height: 26px;
            padding: 0 12px 0 12px;
            position: relative;
            margin: 5px 10px 10px 5px;
            text-decoration: none;
            -webkit-transition: color 0.2s;
        }

            .tag_bancredit:hover {
                background-color: blue;
                color: orange;
            }

                .tag_bancredit:hover::after {
                    border-left-color: blue;
                    background-color: blue;
                }

        .tag_inter:hover {
            background-color: gray;
            color: black;
        }

            .tag_inter:hover::after {
                border-left-color: transparent;
                background-color: transparent;
            }

        .tags_inter {
            list-style: none;
            margin: 0;
            overflow: hidden;
            padding: 0;
        }

        .tag_inter::after {
            background: transparent;
            border-bottom: 13px solid transparent;
            border-left: 10px solid transparent;
            border-top: 13px solid transparent;
            content: '';
            position: absolute;
            right: 0;
            top: 0;
        }

        .tags_inter li {
            float: left;
        }

        .tag_inter {
            background: red;
            border-radius: 3px 0 0 3px;
            font-size: 7pt;
            color: #fff;
            display: inline-block;
            height: 26px;
            line-height: 26px;
            padding: 0 12px 0 12px;
            position: relative;
            margin: 5px 10px 10px 5px;
            text-decoration: none;
            -webkit-transition: color 0.2s;
        }

        .tag_other:hover {
            background-color: cyan;
            color: white;
        }

            .tag_other:hover::after {
                border-left-color: darkcyan;
                background: #D9ECFF;
            }

        .tags_other {
            list-style: none;
            margin: 0;
            overflow: hidden;
            padding: 0;
        }

        .tag_other::after {
            background: transparent;
            border-bottom: 13px solid transparent;
            border-left: 10px solid transparent;
            border-top: 13px solid transparent;
            content: '';
            border-left-color: darkcyan;
            position: absolute;
            right: 0;
            top: 0;
        }

        .tags_other li {
            float: left;
        }

        .tag_other {
            background: darkcyan;
            border-radius: 3px 0 0 3px;
            font-size: 7pt;
            color: black;
            display: inline-block;
            height: 26px;
            line-height: 26px;
            padding: 0 12px 0 12px;
            position: relative;
            margin: 5px 10px 10px 5px;
            text-decoration: none;
            -webkit-transition: color 0.2s;
        }
    </style>
    
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <h3>&nbsp;&nbsp;Opciones de Contabilidad</h3>
    <div class="content2">
        <div class="clientes2">

            <dx:ASPxPopupControl ID="pcCuentas" runat="server" ClientInstanceName="pcCuentas" HeaderText="Cuentas contables" HeaderStyle-BackColor="#ff8a3f" HeaderStyle-ForeColor="White"
            Modal="true" Width="500px" AllowDragging="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" CloseAction="CloseButton" >
            <HeaderStyle BackColor="#FF8A3F" ForeColor="White" />
                <ContentCollection>
                    <dx:PopupControlContentControl>

                        <dx:ASPxGridView ID="gridCuentasBuscar" EnableTheming="True" Theme="SoftOrange" visible="true" runat="server" KeyFieldName="Cuenta" AutoGenerateColumns="False" Border-BorderStyle="None" Width="650px" CssClass="dxGrid" >
                            <Styles>
                                <StatusBar><Border BorderStyle="None" />
                                    <Border BorderStyle="None"></Border>
                                </StatusBar>
                                <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                <RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>
                                <FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                            </Styles>
                            <Settings ShowStatusBar="Hidden"></Settings>
                            <SettingsBehavior EnableRowHotTrack="True" AllowSort="false"></SettingsBehavior>
                            <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                            CommandBatchEditUpdate="Aplicar cambios"
                            ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                            CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                            SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                            <Settings ShowStatusBar="Hidden" />
                            <SettingsSearchPanel Visible="true" ShowApplyButton="true" ShowClearButton="true" />
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="Cuenta" ReadOnly="true" Caption="Cuenta" Visible="true" Width="100px">
                                    <DataItemTemplate>
                                        <dx:ASPxButton ID="btnCuenta" runat="server" AutoPostBack="false" Text='<%# Bind("Cuenta") %>' RenderMode="Link" OnClick="btnCuentaBuscar_Click" ToolTip="Haga clic aquí para seleccionar la cuenta contable"></dx:ASPxButton>
                                    </DataItemTemplate>
                                    <HeaderStyle BackColor="#ff8a3f"  />
                                    <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Descripcion" ReadOnly="true" Caption="Descripción" Visible="true" Width="550px">
                                    <HeaderStyle BackColor="#ff8a3f"  />
                                    <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                    <CellStyle Font-Size="X-Small"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <Styles>
                                <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                            </Styles>
                            <SettingsBehavior EnableRowHotTrack="true" />
                            <SettingsPager NumericButtonCount="3">
                                <Summary Visible="False" />
                            </SettingsPager>
                            <SettingsText SearchPanelEditorNullText="Ingrese aquí su criterio de búsqueda" CommandBatchEditUpdate="Aplicar cambios" CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"/>
                            <Border BorderStyle="None"></Border>
                        </dx:ASPxGridView>
                    </dx:PopupControlContentControl>
                </ContentCollection>
            </dx:ASPxPopupControl>

            <dx:ASPxPopupControl ID="pcClientes" runat="server" ClientInstanceName="pcClientes" HeaderText="Clientes" HeaderStyle-BackColor="#ff8a3f" HeaderStyle-ForeColor="White"
            Modal="true" Width="500px" AllowDragging="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" CloseAction="CloseButton" >
            <HeaderStyle BackColor="#FF8A3F" ForeColor="White" />
                <ContentCollection>
                    <dx:PopupControlContentControl>

                        <dx:ASPxGridView ID="gridClientesBuscar" EnableTheming="True" Theme="SoftOrange" visible="true" runat="server" KeyFieldName="Cliente" AutoGenerateColumns="False" Border-BorderStyle="None" Width="650px" CssClass="dxGrid" >
                            <Styles>
                                <StatusBar><Border BorderStyle="None" />
                                    <Border BorderStyle="None"></Border>
                                </StatusBar>
                                <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                <RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>
                                <FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                            </Styles>
                            <Settings ShowStatusBar="Hidden"></Settings>
                            <SettingsBehavior EnableRowHotTrack="True" AllowSort="false"></SettingsBehavior>
                            <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                            CommandBatchEditUpdate="Aplicar cambios"
                            ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                            CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                            SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                            <Settings ShowStatusBar="Hidden" />
                            <SettingsSearchPanel Visible="true" ShowApplyButton="true" ShowClearButton="true" />
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="Cliente" ReadOnly="true" Caption="Cliente" Visible="true" Width="100px">
                                    <DataItemTemplate>
                                        <dx:ASPxButton ID="btnCliente" runat="server" AutoPostBack="false" Text='<%# Bind("Cliente") %>' RenderMode="Link" OnClick="btnClienteBuscar_Click" ToolTip="Haga clic aquí para seleccionar el cliente"></dx:ASPxButton>
                                    </DataItemTemplate>
                                    <HeaderStyle BackColor="#ff8a3f"  />
                                    <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Nombre" ReadOnly="true" Caption="Nombre" Visible="true" Width="550px">
                                    <HeaderStyle BackColor="#ff8a3f"  />
                                    <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                    <CellStyle Font-Size="X-Small"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <Styles>
                                <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                            </Styles>
                            <SettingsBehavior EnableRowHotTrack="true" />
                            <SettingsPager NumericButtonCount="3">
                                <Summary Visible="False" />
                            </SettingsPager>
                            <SettingsText SearchPanelEditorNullText="Ingrese aquí su criterio de búsqueda" CommandBatchEditUpdate="Aplicar cambios" CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"/>
                            <Border BorderStyle="None"></Border>
                        </dx:ASPxGridView>
                    </dx:PopupControlContentControl>
                </ContentCollection>
            </dx:ASPxPopupControl>

            <ul>
                <li>
                    <table>
                        <tr>
                            <td>
                            </td>
                            <td colspan="2">
                                <dx:ASPxUploadControl runat="server" NullText="Clic aqu&#237; para examinar…" 
                                    Width="395px" Theme="SoftOrange" CssClass="uploadStyle" TabIndex="10" 
                                    ToolTip="Cargar archivo" ID="uploadFile">
                                    <ValidationSettings AllowedFileExtensions=".xls, .xlsx">
                                    </ValidationSettings>
                                    <Border BorderStyle="None">
                                    </Border>
                                </dx:ASPxUploadControl>
                            </td>
                            <td colspan="3">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>
                                Seleccione el mes, el año y la empresa:</td>
                            <td>
                                <dx:ASPxComboBox runat="server" DropDownWidth="60px" DropDownRows="25" 
                                    Width="130px" Theme="SoftOrange" CssClass="ComboBoxStyle" TabIndex="12" 
                                    ToolTip="Seleccione el mes" ID="cbMesLV">
                                    <Items>
                                        <dx:ListEditItem Text="Enero" Value="1">
                                        </dx:ListEditItem>
                                        <dx:ListEditItem Text="Febrero" Value="2">
                                        </dx:ListEditItem>
                                        <dx:ListEditItem Text="Marzo" Value="3">
                                        </dx:ListEditItem>
                                        <dx:ListEditItem Text="Abril" Value="4">
                                        </dx:ListEditItem>
                                        <dx:ListEditItem Text="Mayo" Value="5">
                                        </dx:ListEditItem>
                                        <dx:ListEditItem Text="Junio" Value="6">
                                        </dx:ListEditItem>
                                        <dx:ListEditItem Text="Julio" Value="7">
                                        </dx:ListEditItem>
                                        <dx:ListEditItem Text="Agosto" Value="8">
                                        </dx:ListEditItem>
                                        <dx:ListEditItem Text="Septiembre" Value="9">
                                        </dx:ListEditItem>
                                        <dx:ListEditItem Text="Octubre" Value="10">
                                        </dx:ListEditItem>
                                        <dx:ListEditItem Text="Noviembre" Value="11">
                                        </dx:ListEditItem>
                                        <dx:ListEditItem Text="Diciembre" Value="12">
                                        </dx:ListEditItem>
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <dx:ASPxTextBox runat="server" Width="120px" CssClass="textBoxStyle" 
                                    TabIndex="14" ToolTip="Ingrese el a&#241;o" ID="txtAnioLV">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <dx:ASPxComboBox runat="server" DropDownWidth="60px" DropDownRows="25" 
                                    Width="120px" Theme="SoftOrange" CssClass="ComboBoxStyle" TabIndex="16" 
                                    ToolTip="Seleccione la empresa" ID="cbEmpresaLV">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                </li>
            </ul>
         
            <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" Theme="SoftOrange" 
                Width="996px" ActiveTabIndex="1">
                <TabPages>
                    <dx:TabPage Text="Compras y ventas">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl1" runat="server">
                                <table>
                                    <tr>
                                        <td colspan="4">
                                            <a>Libro de Ventas</a>
                                        </td>
                                        <td style="text-align: right">
                                            <asp:Label ID="lbErrorLibroVentas" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                            <asp:Label ID="lbInfoLibroVentas" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            Número de factura:</td>
                                        <td>
                                            <dx:ASPxTextBox ID="txtFacturaLV" runat="server" CssClass="textBoxStyle" 
                                                TabIndex="40" ToolTip="Ingrese el número de factura" Width="110px">
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btnBienServicioLV" runat="server" Height="8px" 
                                                OnClick="btnBienServicioLV_Click" TabIndex="60" Text="Bien/Servicio" 
                                                ToolTip="Cambia de Bien a Servicio o viceversa" Width="8px">
                                                <Image IconID="actions_reset_16x16office2013">
                                                </Image>
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            <dx:ToolbarExport runat="server" ID="ToolbarExport" ExportItemTypes="LVPlantilla,LVCargarNotas,UndoNotas,PlantExencion,CargaExencion,PlantAnul,CargaAnul,LVExcel,LVAsl" OnItemClick="ToolbarExport_ItemClick" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>

                                <br />
                                <br />

                                <table>
                                    <tr>
                                        <td colspan="3">
                                            <a>Libro de Compras</a>
                                        </td>
                                        <td colspan="4" style="text-align: right">
                                            <asp:Label ID="lbErrorLibroCompras" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                            <asp:Label ID="lbInfoLibroCompras" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btnLibroCompras" runat="server" Height="8px" 
                                                OnClick="btnLibroCompras_Click" TabIndex="60" Text="Libro Compras" 
                                                ToolTip="Genera el libro de compras" Width="60px">
                                                <Image IconID="actions_buy_16x16devav">
                                                </Image>
                                            </dx:ASPxButton>
                                        </td>
                                        <td colspan="2">
                                            <dx:ASPxButton ID="btnLibroComprasASL" runat="server" Height="8px" 
                                                OnClick="btnLibroComprasASL_Click" TabIndex="62" Text="Libro Compras ASL" 
                                                ToolTip="Genera el libro de compras en formato ASL" Width="60px">
                                                <Image IconID="people_customersales_16x16devav">
                                                </Image>
                                            </dx:ASPxButton>
                                        </td>
                                        <td>                                            
                                            <dx:ASPxButton ID="btnLibroComprasPlantilla" runat="server" Height="8px" OnClick="btnLibroComprasPlantilla_Click" TabIndex="64" Text="Plantilla LC" ToolTip="" Width="60px">
                                                <Image IconID="actions_download_16x16">
                                                </Image>
                                            </dx:ASPxButton>
                                        </td>
                                        <td>    
                                            <dx:ASPxButton ID="btnLibroComprasModifica" runat="server" Height="8px" OnClick="btnLibroComprasModifica_Click" TabIndex="66" Text="Modifica LC" ToolTip="" Width="60px">
                                                <Image IconID="tasks_edittask_16x16">
                                                </Image>
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>

                                <br />
                                <br />

                                <table>
                                    <tr>
                                        <td colspan="3">
                                            <a>Estados Financieros</a>
                                        </td>
                                        <td colspan="2" style="text-align: right">
                                            <asp:Label ID="lbErrorEF" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                            <asp:Label ID="lbInfoEF" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btnGenerarEF" runat="server" Height="8px" 
                                                OnClick="btnGenerarEF_Click" TabIndex="60" Text="Generar EF" 
                                                ToolTip="Genera estados financieros" Width="120px">
                                                <ClientSideEvents Click="function(s, e) {
	                                                confirm('Desea genear los Estados Financieros?');
                                                }" />
                                                <Image IconID="functionlibrary_financial_16x16">
                                                </Image>
                                            </dx:ASPxButton>
                                        </td>
                                        <td colspan="2">
                                            <dx:ASPxButton ID="btnDescargarEF" runat="server" Height="8px" 
                                                OnClick="btnDescargarEF_Click" TabIndex="60" Text="Descargar EF" 
                                                ToolTip="Descargar estados financieros previamente generados" Width="120px">
                                                <Image IconID="chart_drilldownonseries_pie_16x16office2013">
                                                </Image>
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>

                                <br />

                                <dx:ASPxGridView ID="gridLibroVentas1" EnableTheming="True" Theme="SoftOrange" runat="server" AutoGenerateColumns="true" Visible="false" Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="280" >
                                    <Styles>
                                        <RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>
                                        <AlternatingRow BackColor="#FDE4CF"></AlternatingRow>
                                        <FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                        <Header BackColor="#ff8a3f"></Header>
                                    </Styles>
                                    <Settings ShowGroupFooter="VisibleIfExpanded" ShowStatusBar="Hidden" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible"></Settings>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"
                                    CommandBatchEditUpdate="Aplicar cambios" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible" />
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="30" Visible="true">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                                <dx:ASPxGridView ID="gridLibroVentas2" EnableTheming="True" Theme="SoftOrange" runat="server" AutoGenerateColumns="true" Visible="false" Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="283" >
                                    <Styles>
                                        <RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>
                                        <AlternatingRow BackColor="#FDE4CF"></AlternatingRow>
                                        <FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                        <Header BackColor="#ff8a3f"></Header>
                                    </Styles>
                                    <Settings ShowGroupFooter="VisibleIfExpanded" ShowStatusBar="Hidden" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible"></Settings>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"
                                    CommandBatchEditUpdate="Aplicar cambios" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible" />
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="30" Visible="true">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                                <dx:ASPxGridView ID="gridLibroVentas3" EnableTheming="True" Theme="SoftOrange" runat="server" AutoGenerateColumns="true" Visible="false" Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="285" >
                                    <Styles>
                                        <RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>
                                        <AlternatingRow BackColor="#FDE4CF"></AlternatingRow>
                                        <FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                        <Header BackColor="#ff8a3f"></Header>
                                    </Styles>
                                    <Settings ShowGroupFooter="VisibleIfExpanded" ShowStatusBar="Hidden" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible"></Settings>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"
                                    CommandBatchEditUpdate="Aplicar cambios" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible" />
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="30" Visible="true">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                                <dx:ASPxGridView ID="gridLibroVentas4" EnableTheming="True" Theme="SoftOrange" runat="server" AutoGenerateColumns="true" Visible="false" Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="287" >
                                    <Styles>
                                        <RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>
                                        <AlternatingRow BackColor="#FDE4CF"></AlternatingRow>
                                        <FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                        <Header BackColor="#ff8a3f"></Header>
                                    </Styles>
                                    <Settings ShowGroupFooter="VisibleIfExpanded" ShowStatusBar="Hidden" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible"></Settings>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"
                                    CommandBatchEditUpdate="Aplicar cambios" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible" />
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="30" Visible="true">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                                <dx:ASPxGridView ID="gridLibroVentas5" EnableTheming="True" Theme="SoftOrange" runat="server" AutoGenerateColumns="true" Visible="false" Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="289" >
                                    <Styles>
                                        <RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>
                                        <AlternatingRow BackColor="#FDE4CF"></AlternatingRow>
                                        <FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                        <Header BackColor="#ff8a3f"></Header>
                                    </Styles>
                                    <Settings ShowGroupFooter="VisibleIfExpanded" ShowStatusBar="Hidden" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible"></Settings>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"
                                    CommandBatchEditUpdate="Aplicar cambios" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible" />
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="30" Visible="true">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                                <dx:ASPxGridView ID="gridLibroVentas6" EnableTheming="True" Theme="SoftOrange" runat="server" AutoGenerateColumns="true" Visible="false" Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="291" >
                                    <Styles>
                                        <RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>
                                        <AlternatingRow BackColor="#FDE4CF"></AlternatingRow>
                                        <FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                        <Header BackColor="#ff8a3f"></Header>
                                    </Styles>
                                    <Settings ShowGroupFooter="VisibleIfExpanded" ShowStatusBar="Hidden" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible"></Settings>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"
                                    CommandBatchEditUpdate="Aplicar cambios" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible" />
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="30" Visible="true">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                                <dx:ASPxGridView ID="gridLibroVentas7" EnableTheming="True" Theme="SoftOrange" runat="server" AutoGenerateColumns="true" Visible="false" Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="293" >
                                    <Styles>
                                        <RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>
                                        <AlternatingRow BackColor="#FDE4CF"></AlternatingRow>
                                        <FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                        <Header BackColor="#ff8a3f"></Header>
                                    </Styles>
                                    <Settings ShowGroupFooter="VisibleIfExpanded" ShowStatusBar="Hidden" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible"></Settings>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"
                                    CommandBatchEditUpdate="Aplicar cambios" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible" />
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="30" Visible="true">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                                <dx:ASPxGridView ID="gridLibroVentas8" EnableTheming="True" Theme="SoftOrange" runat="server" AutoGenerateColumns="true" Visible="false" Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="295" >
                                    <Styles>
                                        <RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>
                                        <AlternatingRow BackColor="#FDE4CF"></AlternatingRow>
                                        <FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                        <Header BackColor="#ff8a3f"></Header>
                                    </Styles>
                                    <Settings ShowGroupFooter="VisibleIfExpanded" ShowStatusBar="Hidden" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible"></Settings>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"
                                    CommandBatchEditUpdate="Aplicar cambios" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible" />
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="30" Visible="true">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                                <dx:ASPxGridView ID="gridLibroVentas9" EnableTheming="True" Theme="SoftOrange" runat="server" AutoGenerateColumns="true" Visible="false" Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="295" >
                                    <Styles>
                                        <RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>
                                        <AlternatingRow BackColor="#FDE4CF"></AlternatingRow>
                                        <FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                        <Header BackColor="#ff8a3f"></Header>
                                    </Styles>
                                    <Settings ShowGroupFooter="VisibleIfExpanded" ShowStatusBar="Hidden" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible"></Settings>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"
                                    CommandBatchEditUpdate="Aplicar cambios" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible" />
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="30" Visible="true">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                                <dx:ASPxGridView ID="gridLibroVentas10" EnableTheming="True" Theme="SoftOrange" runat="server" AutoGenerateColumns="true" Visible="false" Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="295" >
                                    <Styles>
                                        <RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>
                                        <AlternatingRow BackColor="#FDE4CF"></AlternatingRow>
                                        <FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                        <Header BackColor="#ff8a3f"></Header>
                                    </Styles>
                                    <Settings ShowGroupFooter="VisibleIfExpanded" ShowStatusBar="Hidden" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible"></Settings>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"
                                    CommandBatchEditUpdate="Aplicar cambios" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible" />
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="30" Visible="true">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                                <dx:ASPxGridView ID="gridLibroVentas11" EnableTheming="True" Theme="SoftOrange" runat="server" AutoGenerateColumns="true" Visible="false" Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="295" >
                                    <Styles>
                                        <RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>
                                        <AlternatingRow BackColor="#FDE4CF"></AlternatingRow>
                                        <FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                        <Header BackColor="#ff8a3f"></Header>
                                    </Styles>
                                    <Settings ShowGroupFooter="VisibleIfExpanded" ShowStatusBar="Hidden" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible"></Settings>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"
                                    CommandBatchEditUpdate="Aplicar cambios" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible" />
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="30" Visible="true">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                                <dx:ASPxGridView ID="gridLibroVentas12" EnableTheming="True" Theme="SoftOrange" runat="server" AutoGenerateColumns="true" Visible="false" Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="295" >
                                    <Styles>
                                        <RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>
                                        <AlternatingRow BackColor="#FDE4CF"></AlternatingRow>
                                        <FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                        <Header BackColor="#ff8a3f"></Header>
                                    </Styles>
                                    <Settings ShowGroupFooter="VisibleIfExpanded" ShowStatusBar="Hidden" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible"></Settings>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"
                                    CommandBatchEditUpdate="Aplicar cambios" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible" />
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="30" Visible="true">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                                <dx:ASPxGridView ID="gridLibroVentas13" EnableTheming="True" Theme="SoftOrange" runat="server" AutoGenerateColumns="true" Visible="false" Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="295" >
                                    <Styles>
                                        <RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>
                                        <AlternatingRow BackColor="#FDE4CF"></AlternatingRow>
                                        <FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                        <Header BackColor="#ff8a3f"></Header>
                                    </Styles>
                                    <Settings ShowGroupFooter="VisibleIfExpanded" ShowStatusBar="Hidden" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible"></Settings>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                        <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"
                                    CommandBatchEditUpdate="Aplicar cambios" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible" />
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="30" Visible="true">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>


                    <dx:TabPage Text="Inventarios">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl4" runat="server">
                                <table>
                                    <tr>
                                        <td colspan="7">
                                            <a>Inventarios de bodega y tiendas</a>
                                        </td>
                                         <td colspan="7" style="text-align: right">
                                            <asp:Label ID="lbErrorInventarios" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                            <asp:Label ID="lbInfoInventarios" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;&nbsp;
                                        </td>
                                        <td>
                                            Bodega:
                                        </td>
                                        <td>
                                            <dx:ASPxComboBox ID="cbBodega" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle" 
                                                ValueType="System.String" Width="180px" DropDownStyle="DropDownList" 
                                                DropDownWidth="40" DropDownRows="20" TabIndex="2000">
                                            </dx:ASPxComboBox>
                                            <asp:CheckBox ID="chkFiltrarInvRep" Text="Filtrar por clasificación" runat="server" AutoPostBack="true"
                                                OnCheckedChanged="chkFiltrarInvRep_CheckedChanged" />
                                        </td>
                                        <td>
                                            <dx:ASPxMenu ID="ASPxMenu1" runat="server" AllowSelectItem="True" ShowPopOutImages="True" Width="78px"
                                                OnItemClick="ASPxMenu1_ItemClick">
                                                <Items>
                                                    <dx:MenuItem Name="pdf" Text="PDF">
                                                        <Image IconID="export_exporttopdf_16x16"></Image>
                                                        <Items>
                                                            <dx:MenuItem Name="clasificacion" Text="Ordenar por clasificación">
                                                            </dx:MenuItem>
                                                            <dx:MenuItem Name="descripcion" Text="Ordenar por código">
                                                            </dx:MenuItem>
                                                            <dx:MenuItem Name="normal" Text="Orden normal">
                                                            </dx:MenuItem>
                                                        </Items>
                                                    </dx:MenuItem>
                                                </Items>
                                            </dx:ASPxMenu>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btnExcelInventario" runat="server" Height="8px" 
                                                OnClick="btnExcelInventario_Click" TabIndex="2004" Text="Excel" 
                                                ToolTip="Descargar el EXCEL de la bodega seleccionada" Width="60px">
                                                <Image IconID="actions_download_16x16">
                                                </Image>
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btnSubirExcel" runat="server" Height="8px" 
                                                OnClick="btnSubirExcel_Click" TabIndex="2005" Text="Subir" 
                                                ToolTip="Subir el EXCEL de la bodega seleccionada" Width="60px">
                                                <Image IconID="export_export_16x16">
                                                </Image>
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btnExistencias" runat="server" Height="8px" 
                                                OnClick="btnExistencias_Click" TabIndex="2010" Text="Existencias" 
                                                ToolTip="Actualiza las existencias en la bodega seleccionada para el año y mes definido" Width="60px">
                                                <Image IconID="view_sales_16x16devav">
                                                </Image>
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btnPreliminar" runat="server" Height="8px" 
                                                OnClick="btnPreliminar_Click" TabIndex="2015" Text="Preliminar" 
                                                ToolTip="Genera el preliminar de la bodega seleccionada para el año y mes definido, si se selecciona un mes ya cerrado, se mostrará el cierre de dicho mes" Width="60px">
                                                <Image IconID="print_preview_16x16">
                                                </Image>
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btnPreliminarPDF" runat="server" Height="8px" 
                                                OnClick="btnPreliminarPDF_Click" TabIndex="2018" Text="" 
                                                ToolTip="Descarga el PDF preliminar de la bodega seleccionada para el año y mes definido" Width="10px">
                                                <Image IconID="print_preview_16x16gray">
                                                </Image>
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btnCerrar" runat="server" Height="8px" 
                                                OnClick="btnCerrar_Click" TabIndex="2020" Text="Cerrar" 
                                                ToolTip="Cierra el mes de la bodega seleccionada, si el mes ya está cerrado, desplegará como quedó el cierre de dicho mes" Width="60px">
                                                <Image IconID="save_saveto_16x16">
                                                </Image>
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btnReEnviarCierre" runat="server" Height="8px" 
                                                OnClick="btnReEnviarCierre_Click" TabIndex="2025" Text="Re-enviar" 
                                                ToolTip="Re-enviar el cierre del mes de la bodega seleccionada" Width="60px">
                                                <Image IconID="mail_send_16x16office2013">
                                                </Image>
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btnVerTodas" runat="server" Height="8px" 
                                                OnClick="btnVerTodas_Click" TabIndex="2030" Text="Todas" 
                                                ToolTip="Ver todas las bodegas en el mes seleccionado" Width="60px">
                                                <Image IconID="chart_previewchart_16x16office2013">
                                                </Image>
                                            </dx:ASPxButton>
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="12">
                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged" Text="Seleccionar todo" Visible="false" />
                                            <dx:ASPxListBox ID="lbClasificacion" runat="server" SelectionMode="CheckColumn" Visible="false"
                                                Width="285" Height="250" ValueField="Id" TextField="Name" Caption="Clasificación productos">
                                                <CaptionSettings Position="Top" />
                                            </dx:ASPxListBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="12" style="text-align: center">
                                            <dx:ASPxGridView ID="gridDiferencias" EnableTheming="True" Theme="SoftOrange" runat="server" AutoGenerateColumns="False" 
                                                Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="2035" Visible="False" >
                                                <Styles>
                                                    <StatusBar><Border BorderStyle="None" /></StatusBar>
                                                    <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                                    <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                                    <AlternatingRow BackColor="#fde4cf" />
                                                </Styles>
                                                    <SettingsBehavior EnableRowHotTrack="True"></SettingsBehavior>
                                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                                    CommandBatchEditUpdate="Aplicar cambios" 
                                                    ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                                    CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                                    SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                                <Columns>
                                                    <dx:GridViewDataColumn FieldName="Articulo" ReadOnly="true" Caption="Artículo" Visible="true" Width="90px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Left" />
                                                        <CellStyle HorizontalAlign="Left" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="Descripcion" ReadOnly="true" Caption="Descripción" Visible="true" Width="530px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Left" />
                                                        <CellStyle HorizontalAlign="Left" Font-Size="X-Small"/>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="Blanco1" ReadOnly="true" Caption="Diferencias" Visible="true" Width="50px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="Existencias" ReadOnly="true" Caption="Exist. Sistema" Visible="true" Width="50px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="Conteo" ReadOnly="true" Caption="Conteo físico" Visible="true" Width="50px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="Observaciones" ReadOnly="true" Caption="Observaciones" Visible="true" Width="100px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Left" />
                                                        <CellStyle HorizontalAlign="Left" />
                                                    </dx:GridViewDataColumn>
                                                </Columns>
                                                <Styles>
                                                    <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                                </Styles>
                                                <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                                    <SettingsPager PageSize="100" Visible="False">
                                                </SettingsPager>
                                                <Border BorderStyle="None"></Border>
                                            </dx:ASPxGridView>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td colspan="8" style="text-align: center">
                                            <dx:ASPxGridView ID="gridBodegas" EnableTheming="True" Theme="SoftOrange" runat="server" AutoGenerateColumns="False" KeyFieldName="Bodega" 
                                                Border-BorderStyle="None" Width="520px" CssClass="dxGrid" TabIndex="2040" Visible="False" OnRowCommand="gridBodegas_RowCommand" >
                                                <Styles>
                                                    <StatusBar><Border BorderStyle="None" /></StatusBar>
                                                    <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                                    <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                                    <AlternatingRow BackColor="#fde4cf" />
                                                </Styles>
                                                    <SettingsBehavior EnableRowHotTrack="True"></SettingsBehavior>
                                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                                    CommandBatchEditUpdate="Aplicar cambios" 
                                                    ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                                    CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                                    SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                                <Columns>
                                                    <dx:GridViewDataColumn FieldName="Bodega" ReadOnly="true" Caption="Bodega" Visible="true" Width="90px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="Nombre" ReadOnly="true" Caption="Nombre" Visible="true" Width="250px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Left" />
                                                        <CellStyle HorizontalAlign="Left" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="Estado" ReadOnly="true" Caption="Estado" Visible="true" Width="90px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataTextColumn FieldName="ReAbrir" ReadOnly="true" Caption=" " Visible="true" Width="75px">
	                                                    <EditFormSettings Visible="False" />
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
	                                                    <DataItemTemplate>
		                                                    <dx:ASPxButton ID="btnReAbrir" EncodeHtml="false" runat="server" AutoPostBack="false" Text="Re-Abrir" 
                                                            ToolTip="Haga clic aquí para re-abrir esta bodega." OnClick="btnReAbrir_Click" Font-Size="XX-Small"
                                                             ClientInstanceName="btnReAbrir">
                                                              <ClientSideEvents Click="function(s, e) {
                                                                    e.processOnServer = confirm('¿Desea re-abrir la bodega seleccionada?');
                                                                        }" />
		                                                    </dx:ASPxButton>
	                                                    </DataItemTemplate>
	                                                    <HeaderStyle BackColor="#ff8a3f"  />
	                                                    <CellStyle Font-Size="XX-Small" />
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <Styles>
                                                    <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                                </Styles>
                                                <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                                    <SettingsPager PageSize="100" Visible="False">
                                                </SettingsPager>
                                                <Border BorderStyle="None"></Border>
                                            </dx:ASPxGridView>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="7">
                                        </td>
                                        <td colspan="7">
                                        </td>
                                    </tr>
                                </table>

                                <br />

                                <table>
                                    <tr>
                                        <td colspan="1">
                                            <a>Libro de Inventario</a>
                                        </td>
                                         <td style="text-align: right">
                                            <asp:Label ID="lbErrorLibroInventario" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                            <asp:Label ID="lbInfoLibroInventario" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>  
                                        <td>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>  
                                        <td colspan="1">
                                            Seleccione el formato a exportar: 
                                        </td>
                                        <td>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td> 

                                        </tr>
                                        <tr>
                                        <td>
                                        </td>
                                        <td>

                                                                                        
<%--                                            <dx:ASPxButton ID="lkConsultarInventario" runat="server" Text="Consultar" AutoPostBack="false" 
                                            RenderMode = "Link"
                                            ToolTip="Haga clic aquí para generar el inventario." 
                                            ImagePosition = "Left"
                                            onclick="lkConsultarInventario_Click"
                                            >
                                            <Image IconID="actions_search_16x16devav"></Image> 
                                            </dx:ASPxButton>

                                            &nbsp;|&nbsp;

                                             <dx:ASPxButton ID="btImprimirInventario" runat="server" Text="Imprimir" AutoPostBack="false" 
                                            RenderMode = "Link"
                                            ToolTip="Haga clic aquí para imprimir el inventario." 
                                            ImagePosition = "Left"
                                            onclick="lkImprimirInventario_Click"
                                            >
                                            <Image IconID="actions_print_16x16devav"></Image> 
                                            </dx:ASPxButton>
                                            
                                            &nbsp;|&nbsp;--%>


                                            <dx:ASPxButton ID="btExportarXlsInventario" runat="server" Text="Exportar" AutoPostBack="false" 
                                            RenderMode = "Link"
                                            ToolTip="Haga clic aquí para exportar a Excel el inventario." 
                                            ImagePosition = "Left"
                                            onclick="lkExportarXlsInventario_Click"
                                            >
                                            <Image IconID="export_exporttoxls_32x32office2013"></Image> 
                                            </dx:ASPxButton>
<%--
                                            &nbsp;|&nbsp;

                                            <dx:ASPxButton ID="btLimpiarInventario" runat="server" Text="Limpiar" AutoPostBack="false" 
                                            RenderMode = "Link"
                                            ToolTip="Haga clic aquí para limpiar el cuadro de inventario." 
                                            ImagePosition = "Left"
                                            onclick="lkLimpiarInventario_Click"
                                            >
                                            <Image IconID="actions_clear_16x16"></Image> 
                                            </dx:ASPxButton>--%>

                                            </td>
                                            <td>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>

                                    </tr>
                                </table>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>



                    <dx:TabPage Text="Antigüedad de saldos">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl2" runat="server">
                                <table>
                                    <tr>
                                        <td colspan="5">
                                            <a>Antigüedad de saldos</a>
                                        </td>
                                        <td colspan="2" style="text-align: right">
                                            <asp:Label ID="lbErrorAntiguedad" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                            <asp:Label ID="lbInfoAntiguedad" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            Tipo:</td>
                                        <td>
                                            <dx:ASPxComboBox ID="cbTipoAntSaldos" runat="server" CssClass="ComboBoxStyle" 
                                                DropDownRows="25" DropDownWidth="60px" TabIndex="85" Theme="SoftOrange" 
                                                ToolTip="Seleccione los clientes que desea consultar" Width="130px">
                                                <Items>
                                                    <dx:ListEditItem Text="Fiesta" Value="F" />
                                                    <dx:ListEditItem Text="Mayoreo" Value="M" />
                                                    <dx:ListEditItem Text="Todos" Value="T" />
                                                </Items>
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td>
                                            <dx:ASPxTextBox ID="txtClienteAntSaldos" runat="server" CssClass="textBoxStyle" TabIndex="88" ToolTip="Ingrese el cliente que desea consultar" Width="120px"></dx:ASPxTextBox>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btnCliente" runat="server" Width="8px" Height="8px" Text="" 
                                                ToolTip="Ayuda de clientes" TabIndex="89" AutoPostBack="False" 
                                                OnClick="btnCliente_Click" >
                                                <Image IconID="support_question_16x16office2013" />
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            <dx:ToolbarExport runat="server" ID="ToolbarExportAntiguedad" ExportItemTypes="AntSaldos,CargarAntSaldos" OnItemClick="ToolbarExport_ItemClick" />
                                        </td>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="DMG">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl3" runat="server">
                                <table>
                                    <tr>
                                        <td colspan="4">
                                            <a>Diario Mayor General</a>
                                        </td>
                                        <td colspan="4" style="text-align: right">
                                            <dx:ASPxTextBox ID="txtCuenta" runat="server" ClientInstanceName="txtCuenta" 
                                                CssClass="textBoxStyleHide" Width="40px" TabIndex="9999" Visible="true" 
                                                ReadOnly="true" ForeColor="White" BackColor="#F9F9F9">
                                                <Border BorderColor="White" />
                                            </dx:ASPxTextBox>

                                            <asp:Label ID="lbErrorDMG" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                            <asp:Label ID="lbInfoDMG" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            Cuenta contable:
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btnCuentaInicial" runat="server" Width="8px" Height="8px" Text="" ToolTip="Ayuda de cuentas para seleccionar la cuenta inicial" TabIndex="65" AutoPostBack="False" OnClick="btnCuentaInicial_Click" >
                                                <Image IconID="support_question_16x16office2013" />
                                                <ClientSideEvents Click="function(s, e) { ShowPcCuentaInicial(); }" />
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            <dx:ASPxTextBox ID="txtCuentaInicial" runat="server" CssClass="textBoxStyle" TabIndex="70" ToolTip="Ingrese la cuenta inicial" Width="110px" >
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btnCuentaFinal" runat="server" Width="8px" Height="8px" Text="" ToolTip="Ayuda de cuentas para seleccionar la cuenta final" TabIndex="75" AutoPostBack="False" >
                                                <Image IconID="support_question_16x16office2013" />
                                                <ClientSideEvents Click="function(s, e) { ShowPcCuentaFinal(); }" />
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            <dx:ASPxTextBox ID="txtCuentaFinal" runat="server" CssClass="textBoxStyle" TabIndex="80" ToolTip="Ingrese la cuenta final" Width="110px" >
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btnDMG" runat="server" Width="8px" Height="8px" Text="" ToolTip="Generar DMG de la cuenta o el rango de cuentas ingresado de la empresa seleccionada" onclick="btnDMG_Click" TabIndex="85" >
                                                <Image IconID="math_calculatesheet_16x16" />
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="Financieras">
                        <ContentCollection>
                           
                            <dx:ContentControl runat="server">
                                <table style="width:100%">
                                    <tr>
                                        <td><a>
                                            Reportes</a></td>
                                        <td style="text-align: right">
                                            &nbsp;</td>
                                        <td style="text-align: right">
                                            <asp:Label ID="lbErrorFinanciera" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                            <asp:Label ID="lbInfoFinanciera" runat="server"></asp:Label>
                                        </td>
                                    </tr>

                                    
                                    <tr style="align-content:center">
                                        <td colspan="1" class="auto-style2">
                                            <table ;="" style="border:none">
                                                <tr>
                                                    <td>Financiera:&nbsp;&nbsp; </td>
                                                    <td>
                                                        <dx:ASPxComboBox ID="cmbFinanciera" runat="server" ClientInstanceName="cmbFinanciera" CssClass="ComboBoxStyle" DropDownRows="25" DropDownWidth="60px" TabIndex="16" Theme="SoftOrange" ToolTip="Seleccione la financiera" Width="130px">
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                         <td style="padding-left: 22px" >
                                             &nbsp;</td>
                                        <td colspan="2" style="padding-left: 22px">
                                            &nbsp;</td>
                                        <td style="padding-left: 22px">
                                            &nbsp;</td>
                                    </tr>
                                    
                                    
                                    <tr style="align-content:center">
                                        <td style="vertical-align: top">
                                            <dx:ASPxRadioButton ID="rdlRealizados" runat="server" Checked="True" GroupName="consultas" Text="Desembolsos realizados" Theme="SoftOrange">
                                                <ClientSideEvents CheckedChanged="function(s, e) {
                                                            
	                                                     cmbOptSolEnviadas.SetEnabled(false)
                                                        fechaRigeDesembolso.SetEnabled(true)
                                                        fechaDesembolsos.SetEnabled(true)
                                                           btImprimirDesembolsos.SetEnabled(true)
                                                          btCargarDesembolso.SetEnabled(true)
                                                            
                                                        }" ValueChanged="function(s, e) {
	                                                       

                                                        }" />
                                                <Border BorderStyle="None" />
                                            </dx:ASPxRadioButton>
                                        </td>
                                        <td style="text-align: center; align-content:center; clip: rect(auto, auto, auto, inherit); margin-left: auto; margin-right: auto;  padding-right: 25px; padding-bottom: inherit; padding-left: 25px; vertical-align: top;"  >
                                            <dx:ASPxRadioButton ID="rdlPendientes" runat="server" GroupName="consultas" Text="Desembolsos pendientes" Theme="SoftOrange">
                                                <ClientSideEvents CheckedChanged="function(s, e) {
                                              fechaRigeDesembolso.SetEnabled(false)
                                                fechaDesembolsos.SetEnabled(false)
                                                cmbOptSolEnviadas.SetEnabled(true)
                                                btImprimirDesembolsos.SetEnabled(false)
                                                  btCargarDesembolso.SetEnabled(false)
                                                    
                                                    }" ValueChanged="function(s, e) {
	                                             

                                                 }" />
                                                <Border BorderStyle="None" />
                                            </dx:ASPxRadioButton>
                                        </td>
                                        <td style="vertical-align: top" colspan="2">
                                            <dx:ASPxRadioButton ID="rdlRechazados" runat="server" GroupName="consultas" Text="Desembolsos rechazados" Theme="SoftOrange">
                                                <ClientSideEvents CheckedChanged="function(s, e) {
                                                            cmbOptSolEnviadas.SetEnabled(false)
                                                            fechaRigeDesembolso.SetEnabled(false)
                                                            fechaDesembolsos.SetEnabled(false)
                                                            btImprimirDesembolsos.SetEnabled(false)
                                                            btCargarDesembolso.SetEnabled(false)
                                                        
                                                    }" ValueChanged="function(s, e) {
                                                  
                                                    }" />
                                                <Border BorderStyle="None" />
                                            </dx:ASPxRadioButton>


                                        </td>
                                    </tr>
                                   
                                    <tr style="align-content:center">
                                        <td style="vertical-align: top">
                                            <table ;="" style="border:none">
                                                <tr>
                                                    <td>Fecha desembolso:</td>
                                                    <td>
                                                        <dx:ASPxDateEdit ID="fechaDesembolsos" runat="server" ClientInstanceName="fechaDesembolsos" DisplayFormatString="dd/MM/yyyy" TabIndex="52" Theme="SoftOrange" Width="100px">
                                                            <CalendarProperties ShowClearButton="False" ShowWeekNumbers="False">
                                                                





<MonthGridPaddings Padding="3px" />
                                                                











<DayStyle Font-Size="11px">
                                                                





<Paddings Padding="3px" />
                                                                





</DayStyle>
                                                                











<Style Wrap="True">
                                                                </Style>
                                                            





</CalendarProperties>
                                                        </dx:ASPxDateEdit>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style1">Fecha rige desembolso:</td>
                                                    <td>
                                                        <dx:ASPxDateEdit ID="fechaRigeDesembolso" runat="server" ClientInstanceName="fechaRigeDesembolso" CssClass="DateEditStyle" DisplayFormatString="dd/MM/yyyy" TabIndex="52" Theme="SoftOrange" Width="100px">
                                                            <CalendarProperties ShowClearButton="False" ShowWeekNumbers="False">
                                                                





<MonthGridPaddings Padding="3px" />
                                                                











<DayStyle Font-Size="11px">
                                                                





<Paddings Padding="3px" />
                                                                





</DayStyle>
                                                                











<Style Wrap="True">
                                                                </Style>
                                                            





</CalendarProperties>
                                                        </dx:ASPxDateEdit>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td colspan="2" style="text-align: center; align-content:center; clip: rect(auto, auto, auto, inherit); margin-left: auto; margin-right: auto;  padding-right: 25px; padding-bottom: inherit; padding-left: 25px; vertical-align: top;">
                                            <table ;="" class="NoBorder" style="border:none">
                                                <tr>
                                                    <td>Filtrar por: </td>
                                                    <td style="padding-left: 10px">
                                                        <dx:ASPxComboBox ID="cmbOptSolEnviadas" runat="server" ClientInstanceName="cmbOptSolEnviadas" CssClass="ComboBoxStyle" DropDownRows="25" DropDownWidth="60px" SelectedIndex="0" TabIndex="16" Theme="SoftOrange" ToolTip="Seleccione la empresa" Width="230px">
                                                            <Items>
                                                                <dx:ListEditItem Selected="True" Text="Pendientes de la financieras" Value="-1" />
                                                                <dx:ListEditItem Text="De más de una semana (financieras)" Value="1" />
                                                                <dx:ListEditItem Text="Pendientes de envío por tiendas" Value="2" />
                                                            </Items>
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <dx:ASPxPopupControl ID="pcConfirmacion" runat="server" AllowDragging="True" ClientInstanceName="pcConfirmacion" CloseAction="CloseButton" HeaderText="Confirmación" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="300px">
                                                <HeaderImage AlternateText="Img" IconID="comments_editcomment_16x16">
                                                </HeaderImage>
                                                <HeaderStyle BackColor="#FF8A3F" ForeColor="White" />
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl runat="server">
                                                        <table>
                                                            <tr>
                                                                <td>¿Está seguro(a) de cargar los desembolsos al sistema con la fecha seleccionada?
                                                                    <dx:ASPxLabel ID="refFechaRigeDesembolso" runat="server" AssociatedControlID="fechaRigeDesembolso" ClientInstanceName="clrefFechaRigeDesembolso" Font-Bold="True">
                                                                    </dx:ASPxLabel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <dx:ASPxButton ID="ButtonYes" runat="server" OnClick="lkCargarDesembolsos_Click" Text="Si">
                                                                        <ClientSideEvents Click="OnClickConfirmButton" />
                                                                    </dx:ASPxButton>
                                                                    <dx:ASPxButton ID="ButtonNo" runat="server" AutoPostBack="False" Text="No">
                                                                        <ClientSideEvents Click="OnClickConfirmButton" />
                                                                    </dx:ASPxButton>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" style="font-size: xx-small; color: #FF0000">
                                                                    <br />
                                                                    Este procedimiento está disponible únicamente para INTERCONSUMO y CREDIPLUS</td>
                                                            </tr>
                                                        </table>
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                            </dx:ASPxPopupControl>
                                        </td>
                                        <td colspan="2" style="vertical-align: top">&nbsp;</td>
                                    </tr>
                                   
                                    <tr style="align-content:center">
                                        <td colspan="4" style="padding-right: 25px; padding-left: 25px; position: relative; text-align: center;">
                                            <dx:ASPxButton ID="btConsultarDesembolsos" runat="server" OnClick="lkSeleccionarDesembolsos_Click" RenderMode="Link" Text="Consultar" ToolTip="Haga clic aquí para consultar los desembolsos de Interconsumo.">
                                                <Image IconID="actions_search_16x16devav">
                                                </Image>
                                            </dx:ASPxButton>
                                            &nbsp; |&nbsp;
                                            <dx:ASPxButton ID="btCargarDesembolso" runat="server" AutoPostBack="False" RenderMode="Link" Text="Guardar" ToolTip="Haga clic aquí para cargar los desembolsos al sistema." ClientInstanceName="btCargarDesembolso">
                                                <ClientSideEvents Click="OnClickConfimacion" />
                                                <Image IconID="actions_save_16x16devav">
                                                </Image>
                                            </dx:ASPxButton>
                                             &nbsp; |&nbsp;
                                            <dx:ASPxButton ID="btImprimirDesembolsos" runat="server" OnClick="lkImprimirDesembolsos_Click" RenderMode="Link" Text="Imprimir" ToolTip="Haga clic aquí para imprimir el listado de los desembolsos." ClientInstanceName="btImprimirDesembolsos">
                                                <Image IconID="actions_print_16x16devav">
                                                </Image>
                                            </dx:ASPxButton>
                                             &nbsp; |&nbsp;
                                            <dx:ASPxButton ID="btExportar" runat="server" OnClick="lkExportarDesembolsos_Click" RenderMode="Link" Text="Exportar" ToolTip="Haga clic aquí para exportar a Excel el listado de los desembolsos.">
                                                <Image IconID="export_exporttoxls_16x16">
                                                </Image>
                                            </dx:ASPxButton>
                                           
                                        </td>
                                    </tr>
                                   
                                    </table>
                                
                                <table style="align-content:center; border:none";>

<tr>
                                        <td style="align-content:center">
                                           
                                            <asp:Label ID="lblTotalItems" runat="server" CssClass="NewStyleMessage"></asp:Label>
&nbsp;</td>
                                    </tr>


                                    <tr>
                                        <td>
                                            <dx:ASPxGridView ID="gridFinanciera" EnableTheming="True" Theme="SoftOrange" 
                                            runat="server" AutoGenerateColumns="false" Visible="false" 
                                            Border-BorderStyle="None" Width="980px" CssClass="dxGrid" TabIndex="301" 
                                            KeyFieldName ="Prestamo"
                                            OnRowUpdating="gridInterconsumo_RowUpdating"
                                            oncelleditorinitialize="gridInterconsumo_CellEditorInitialize" 
                                            OnCommandButtonInitialize="gridInterconsumo_CommandButtonInitialize" 
                                            OnHtmlDataCellPrepared="gridInterconsumo_HtmlDataCellPrepared" 
                                            OnHtmlRowPrepared="gridInterconsumo_HtmlRowPrepared" ClientInstanceName="gridFinanciera" >
                                                <Styles>
                                                    <RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>
                                                    <AlternatingRow BackColor="#FDE4CF"></AlternatingRow>
                                                    <FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                                                    <StatusBar><Border BorderStyle="None" /></StatusBar>
                                                    <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                                    <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                                    <AlternatingRow BackColor="#fde4cf" />
                                                    <Header BackColor="#ff8a3f"></Header>
                                                </Styles>
                                                <Columns>

                                                    
                                                    <dx:GridViewDataTextColumn FieldName="Fecha" ReadOnly="true" Caption="Fecha" Visible="true" Width="80px" >
                                                        <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy" />
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataTextColumn>

                                                    <dx:GridViewDataColumn FieldName="Prestamo" ReadOnly="true" Caption="Préstamo" Visible="true" Width="100px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>

                                                    <dx:GridViewDataColumn FieldName="Solicitud" ReadOnly="true" Caption="Solicitud" Visible="true" Width="90px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>

                                                    <dx:GridViewDataColumn FieldName="Cliente" ReadOnly="true" Caption="Cliente" Visible="true" Width="70px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>

                                                    <dx:GridViewDataColumn FieldName="NombreCliente" ReadOnly="true" Caption="Nombre del cliente" Visible="true" Width="150px" >
                                                        <HeaderStyle BackColor="#ff8a3f" />
                                                    </dx:GridViewDataColumn>

                                                    <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" Caption="Valor" Visible="true" Width="75px" >
                                                        <PropertiesTextEdit DisplayFormatString="n2" />
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                                    </dx:GridViewDataTextColumn>

                                                    <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura" Visible="true" Width="70px" >
                                                        <HeaderStyle BackColor="#ff8a3f" />
                                                    </dx:GridViewDataColumn>

                                                    <dx:GridViewDataColumn FieldName="FacturaMF" ReadOnly="false" Caption="Factura MF" Visible="true" Width="70px" >
                                                        <HeaderStyle BackColor="#ff8a3f" />
                                                    </dx:GridViewDataColumn>

                                                    <dx:GridViewDataColumn FieldName="ClienteMF" ReadOnly="false" Caption="Cliente MF" Visible="true" Width="70px" >
                                                        <HeaderStyle BackColor="#ff8a3f" />
                                                    </dx:GridViewDataColumn>

                                                    <dx:GridViewDataTextColumn FieldName="ValorMF" ReadOnly="true" Caption="Valor MF" Visible="true" Width="70px" >
                                                        <PropertiesTextEdit DisplayFormatString="n2" />
                                                        <HeaderStyle BackColor="#ff8a3f" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="EngancheMF" ReadOnly="true" Caption="Enganche" Visible="true" Width="80px" >
                                                        <PropertiesTextEdit DisplayFormatString="n2" />
                                                        <HeaderStyle BackColor="#ff8a3f" />
                                                    </dx:GridViewDataTextColumn>

                                                    <dx:GridViewDataColumn FieldName="Criterio" ReadOnly="true" Caption="Criterio" Visible="true" Width="100px" >
                                                        <HeaderStyle BackColor="#ff8a3f" />
                                                    </dx:GridViewDataColumn>

                                                </Columns>


                                                <SettingsCommandButton>
                                                    <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>
                                                    <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                                </SettingsCommandButton>
                                                <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"
                                                CommandBatchEditUpdate="Aplicar cambios" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                                CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ></SettingsText>
                                                <Styles>
                                                    <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                                </Styles>
                                                <Settings HorizontalScrollBarMode="Auto" />
                                                <SettingsBehavior EnableRowHotTrack="true" AutoExpandAllGroups="true" AllowSort="false" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True"/>
                                                    <SettingsPager PageSize="30" Visible="true">
                                                </SettingsPager>
                                                <Border BorderStyle="None"></Border>

                                                <SettingsEditing Mode="Batch" />
                                                <ClientSideEvents BatchEditEndEditing="OnBatchEditEndEditing" />

                                            </dx:ASPxGridView>

                                            <dx:ASPxGridView ID="gridConsultas" EnableTheming="True" Theme="SoftOrange" 
                                            runat="server" AutoGenerateColumns="false" Visible="false" 
                                            Border-BorderStyle="None" Width="980px" CssClass="dxGrid" TabIndex="301" 
                                            KeyFieldName ="Factura" OnHtmlDataCellPrepared="gridConsultas_HtmlDataCellPrepared" OnHtmlRowPrepared="gridConsultas_HtmlRowPrepared" OnPageIndexChanged="gridConsultas_PageIndexChanged" ClientInstanceName="gridConsultas">
                                                <TotalSummary>
                                                    <dx:ASPxSummaryItem />
                                                </TotalSummary>
                                                <Styles>
                                                    <RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>
                                                    <AlternatingRow BackColor="#FDE4CF"></AlternatingRow>
                                                    <FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                                                    <StatusBar><Border BorderStyle="None" /></StatusBar>
                                                    <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                                    <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                                    <AlternatingRow BackColor="#fde4cf" />
                                                    <Header BackColor="#ff8a3f"></Header>
                                                </Styles>
                                                <SettingsPager Mode="ShowAllRecords">
                                                </SettingsPager>
                                                <Settings HorizontalScrollBarMode="Visible" />
                                                <SettingsBehavior AllowSort="False"  AutoExpandAllGroups="True" EnableRowHotTrack="True" />
                                                <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                                                <Columns>
                                                    <dx:GridViewCommandColumn ShowInCustomizationForm="True"  VisibleIndex="0" Visible="false" >
                                                    </dx:GridViewCommandColumn>
                                                    <dx:GridViewDataTextColumn Caption="Tienda" FieldName="COBRADOR" Name="COBRADOR"  VisibleIndex="1" Width="70px">
                                                        <HeaderStyle>
                                                        <Paddings PaddingBottom="5px" PaddingTop="5px" />
                                                        </HeaderStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Fecha Factura" FieldName="FECHA_FACTURA" VisibleIndex="2" Width="90px" >
                                                        <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Financiera" FieldName="NOMBRE_FINANCIERA" VisibleIndex="3" Width="95px" Visible="true">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Factura" FieldName="NUMERO_DOCUMENTO" VisibleIndex="4" Width="90px">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Solicitud" FieldName="SOLICITUD" VisibleIndex="5" Width="100px">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Fecha Envío" FieldName="FECHA_SOLICITUD" VisibleIndex="6" Width="100px">
                                                        <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Cliente" FieldName="CLIENTE" VisibleIndex="7" Width="70px">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Nombre Cliente" FieldName="NOMBRE_CLIENTE" VisibleIndex="8" Width="150px">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Valor Factura" FieldName="VALOR_FACTURA" VisibleIndex="9" Width="100px">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Enganche" FieldName="VALOR_ENGANCHE" VisibleIndex="10" Width="90px">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Saldo" FieldName="VALOR_SALDO" VisibleIndex="11" Width="90px">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Fecha Rechazo" FieldName="FECHA_RECHAZO_SOLICITUD" VisibleIndex="12" Width="100px">
                                                        <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy" />
                                                    </dx:GridViewDataTextColumn>
                                                    
                                                    
                                                    <dx:GridViewDataTextColumn Caption="Observaciones" FieldName="OBSERVACIONES" VisibleIndex="13" Width="450px"  Visible="true">
                                                        <CellStyle Font-Size="Smaller">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                     <dx:GridViewDataTextColumn Caption="IdFinanciera" FieldName="FINANCIERA" VisibleIndex="12" Width="100px" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                        
                                                    
                                                    </Columns>
                                                <Border BorderStyle="None" />
                                                 <SettingsEditing Mode="Batch" />
                                                <ClientSideEvents BatchEditEndEditing="OnBatchEditEndEditing" />
                                               
                                                </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                </table>
                            </dx:ContentControl>
                            
                        </ContentCollection>
                    </dx:TabPage>
                     <dx:TabPage Text="Expedientes">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControlDespachoInterconsumo" runat="server">
                                <div style="border-style: none; align-content:center; margin:25px 0px 25px 0px; width:900px; height:27px;">
                                    <table style="border-style: none; align-content:center; width:900px;"><tr><td style="text-align:left; padding-top:px; padding-right: 5px; width:5%; " ><a>Financiera:</a>&nbsp; </td><td style="text-align:left; padding-left: 5px;" >
                                            <dx:ASPxComboBox ID="cmbExpFinanciera" runat="server" CssClass="ComboBoxStyle" DropDownRows="25" DropDownWidth="80px" TabIndex="61" Theme="SoftOrange" Width="140px">
                                            </dx:ASPxComboBox>
                                        </td>
                                        </tr>
                                        </table>
                                   </div>
                                <table>
                                    <tr>
                                        <td colspan="1">
                                            <a>Facturas</a>
                                        </td>
                                        <td colspan="6" style="text-align: right">
                                            <asp:Label ID="lblErrorDespacho" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                            <asp:Label ID="lblInfoDespacho" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            Tienda:</td>
                                        <td >
                                            
                                            <dx:ASPxComboBox ID="tiendaDesFinanciera" runat="server" CssClass="ComboBoxStyle" DropDownRows="25" DropDownWidth="80px" TabIndex="61" Theme="SoftOrange" Width="140px">
                                            </dx:ASPxComboBox>
                                            
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                          
                                        </td>
                                        <td>
                                           
                                        </td>

                                    </tr>
                                    <tr>  
                                        <td>
                                        &nbsp;
                                        </td>
                                        <td>
                                            Fecha inicial:
                                        </td>
                                        <td >
                                            <dx:ASPxDateEdit ID="fechaInicialFacturas" runat="server" Theme="SoftOrange" 
                                                CssClass="DateEditStyle" Width="140px" TabIndex="63" DisplayFormatString="dd/MM/yyyy">
                                                <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                                    ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                                    ShowWeekNumbers="False" >
                                                    













<Style Wrap="True"></Style>
                                                    






<MonthGridPaddings Padding="3px" />
                                                    













<DayStyle Font-Size="11px">
                                                    






<Paddings Padding="3px" />
                                                    






</DayStyle>
                                                






</CalendarProperties>
                                            </dx:ASPxDateEdit>
                                        </td>
                                        <td>
                                            Fecha final:
                                        </td>
                                        <td >
                                        
                                            <dx:ASPxDateEdit ID="fechaFinalFacturas" runat="server" Theme="SoftOrange"
                                                CssClass="DateEditStyle" Width="140px" TabIndex="65" DisplayFormatString="dd/MM/yyyy">
                                                <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                                    ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                                    ShowWeekNumbers="False" >
                                                    













<Style Wrap="True"></Style>
                                                    






<MonthGridPaddings Padding="3px" />
                                                    













<DayStyle Font-Size="11px">
                                                    






<Paddings Padding="3px" />
                                                    






</DayStyle>
                                                






</CalendarProperties>
                                            </dx:ASPxDateEdit>
                                        </td>
                                            <td>

                                                                                        
                                            <dx:ASPxButton ID="lkConsultarFacturasInter" runat="server" Text="Consultar" 
                                            RenderMode = "Link"
                                           
                                            ToolTip="Haga clic aquí para consultar las facturas." 
                                            ImagePosition = "Left"
                                            onclick="lkConsultarFacturasInter_Click"
                                            >
                                            <Image IconID="actions_search_16x16devav"></Image> 
                                            </dx:ASPxButton>

                                            &nbsp;|&nbsp;

                                            <dx:ASPxButton ID="btAgregarFacturasInter" runat="server" Text="Agregar" 
                                            RenderMode = "Link"
                                            
                                            ToolTip="Haga clic aquí para agregar las facturas a enviar a Interconsumo." 
                                            ImagePosition = "Left"
                                            onclick="lkAgregarFacturasInter_Click"
                                            >
                                            <Image IconID="arrows_movedown_16x16office2013"></Image> 
                                            </dx:ASPxButton>

                                              
                                            &nbsp;|&nbsp;

                                            <dx:ASPxButton ID="btLimpiarFacturasInter" runat="server" Text="Limpiar" 
                                            RenderMode = "Link"
                                            ToolTip="Haga clic aquí para limpiar el cuadro de facturas." 
                                            ImagePosition = "Left"
                                            onclick="lkLimpiarFacturasInter_Click"
                                            >
                                            <Image IconID="actions_clear_16x16"></Image> 
                                            </dx:ASPxButton>

                                            </td>
                                            
                                    </tr>
                                    
                                    <tr>
                                        <td colspan="7">
                                            <dx:ASPxGridView ID="GridFacturasFinanciera" EnableTheming="True" 
                                                Theme="SoftOrange" runat="server" AutoGenerateColumns="true" 
                                                Visible="false"  KeyFieldName="NoFactura"
                                                Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="305"  >
                                                <Styles>
<Header BackColor="#FF8A3F"></Header>

                                                    <RowHotTrack BackColor="#d9ecff"></RowHotTrack>

<AlternatingRow BackColor="#FDE4CF"></AlternatingRow>

<FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>

<StatusBar Border-BorderStyle="None"></StatusBar>
                                                </Styles>
                                                <ClientSideEvents BatchEditEndEditing="OnBatchEditEndEditing" />
                                                    <SettingsPager PageSize="100" Visible="false">
                                                </SettingsPager>
                                                 <SettingsEditing Mode="Batch" />
                                                <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="false" 
                                                    AllowSort="False" />
                                                    <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                                                <Columns>
                                                 <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="6"  
                                                        Width="15px" >
                                                        </dx:GridViewCommandColumn>
                                                    <dx:GridViewDataColumn FieldName="NoFactura" ReadOnly="true" Caption="Factura" 
                                                        Visible="true" Width="85px" VisibleIndex="7">
                                                        <HeaderStyle BackColor="#ff8a3f"  />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataTextColumn FieldName="FechaFactura" ReadOnly="true" Caption="Fecha" Visible="true" Width="90px" VisibleIndex="8">
                                                        <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy" />
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataColumn FieldName="Cliente" ReadOnly="true" Caption="Cliente" 
                                                        Visible="true" Width="60px" VisibleIndex="9">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="NombreCliente" ReadOnly="true" 
                                                        Caption="Nombre de cliente" Visible="true" Width="250px" VisibleIndex="10">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Left" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" Caption="Monto" 
                                                        Visible="true" Width="75px" VisibleIndex="11" >
                                                        <PropertiesTextEdit DisplayFormatString="n2" />
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataColumn FieldName="Solicitud" ReadOnly="true" Caption="No. solicitud" Visible="false" Width="80px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="ConstanciaIngresos" ReadOnly="true" 
                                                        Caption="Constancia de ingreso" Visible="false" Width="105px" VisibleIndex="13" >
                                                        <HeaderStyle BackColor="#ff8a3f" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="EstadosDeCuenta" ReadOnly="true" 
                                                        Caption="Estados de cuenta" Visible="false" Width="105px" VisibleIndex="14" >
                                                        <HeaderStyle BackColor="#ff8a3f" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="Pagare" ReadOnly="true" Caption="Pagaré" 
                                                        Visible="false" Width="105px" VisibleIndex="15" >
                                                        <HeaderStyle BackColor="#ff8a3f" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="Observaciones" ReadOnly="true" 
                                                        Caption="Observaciones" Visible="false" Width="105px" VisibleIndex="16" >
                                                        <HeaderStyle BackColor="#ff8a3f" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" 
                                                        Visible="false" Width="105px" VisibleIndex="17" >
                                                        <HeaderStyle BackColor="#ff8a3f" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataTextColumn FieldName="exito" Visible="false" VisibleIndex="18">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="mensaje" Visible="false" VisibleIndex="19">
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <Styles>
                                                    <RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>
                                                    <AlternatingRow BackColor="#FDE4CF"></AlternatingRow>
                                                    <FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                                                    <StatusBar><Border BorderStyle="None" /></StatusBar>
                                                    <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                                    <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                                    <AlternatingRow BackColor="#fde4cf" />
                                                    <Header BackColor="#ff8a3f"></Header>
                                                </Styles>
                                                <Border BorderStyle="None"></Border>
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                            </table>
                        <ul>
                        </ul>
                            <table>
                                    <tr>
                                        <td colspan="2">
                                            <a>Envio a financiera</a>
                                        </td>

                                        <td colspan="3" style="text-align: right">
                                            <asp:Label ID="lblErrorEnvio" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                            <asp:Label ID="lblInfoEnvio" runat="server"></asp:Label>
                                        </td>
                                    </tr>


                                    <tr>  
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp; Fecha de envío: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td>

                                            <dx:ASPxDateEdit ID="fechaEnvioExpedientes" runat="server" CssClass="DateEditStyle" TabIndex="75" Theme="SoftOrange" Width="140px" DisplayFormatString="dd/MM/yyyy">
                                                <CalendarProperties ShowClearButton="False" ShowWeekNumbers="False">
                                                    





<MonthGridPaddings Padding="3px" />
                                                    











<DayStyle Font-Size="11px">
                                                    





<Paddings Padding="3px" />
                                                    





</DayStyle>
                                                    











<Style Wrap="True">
                                                    </Style>
                                                





</CalendarProperties>

                                            </dx:ASPxDateEdit>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                        <td >
                                            &nbsp;</td>
                                       <td>
                                            <dx:ASPxButton ID="btConsultarEnvioExpediente" runat="server" Text="Consultar" AutoPostBack="false" 
                                            RenderMode = "Link"
                                            ToolTip="Haga clic aquí para consultar los envios a realizar a Interconsumo." 
                                            ImagePosition = "Left"
                                            onclick="lkConsultarEnvioExpediente_Click"
                                            >
                                            <Image IconID="actions_search_16x16devav"></Image> 
                                            </dx:ASPxButton>

                                         &nbsp;|&nbsp;
                                          <dx:ASPxButton ID="btGuardarExpedienteInter" runat="server" Text="Guardar" AutoPostBack="false" 
                                            RenderMode = "Link"
                                            ToolTip="Haga clic aquí para guardar los envios al sistema." 
                                            ImagePosition = "Left"
                                            onclick ="lkGuardarExpedienteInter_Click">
                                            <Image IconID="actions_save_16x16devav"></Image> 
                                            </dx:ASPxButton>
                                           &nbsp;|&nbsp;
                                             <dx:ASPxButton ID="btImprimirEnvioInter" runat="server" Text="Imprimir" AutoPostBack="false" 
                                            RenderMode = "Link"
                                            ToolTip="Haga clic aquí para imprimir los expedientes a enviar." 
                                            ImagePosition = "Left"
                                            onclick="lkImprimirEnvioInter_Click"
                                            >
                                            <Image IconID="actions_print_16x16devav"></Image> 
                                            </dx:ASPxButton>
                                           &nbsp;|&nbsp;
                                            <dx:ASPxButton ID="btEnviarCorreo" runat="server" Text="Enviar correo" AutoPostBack="false" 
                                            RenderMode = "Link"
                                            ToolTip="Haga clic aquí para enviar el listado de expedientes por correo." 
                                            ImagePosition = "Left"
                                            onclick="lkEnviarCorreo_Click"
                                            >
                                            <Image IconID="mail_newmail_16x16"></Image> 
                                            </dx:ASPxButton>
                                            &nbsp;|&nbsp;

                                         <dx:ASPxButton ID="btLimpiarEnvioExpedientes" runat="server" Text="Limpiar" AutoPostBack="false" 
                                            RenderMode = "Link"
                                            ToolTip="Haga clic aquí para limpiar los envios a Interconsumo." 
                                            ImagePosition = "Left"
                                            onclick="lkLimpiarEnvioExpedientes_Click"
                                            >
                                            <Image IconID="actions_clear_16x16"></Image> 
                                            </dx:ASPxButton>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </td>
                                    </tr>
                                
                                    <tr>
                                        <td colspan="5">
                                        <dx:ASPxGridView ID="GridExpedientesFinanciera" EnableTheming="True" 
                                                Theme="SoftOrange" visible="false" 
                                    runat="server"  KeyFieldName="NoFactura" AutoGenerateColumns="false" 
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid"
                                    OnRowUpdating="GridExpedientesInterconsumor_RowUpdating"
                                    oncelleditorinitialize="GridExpedientesInterconsumo_CellEditorInitialize" 
                                    OnCommandButtonInitialize="GridExpedientesInterconsumo_CommandButtonInitialize" 
                                     TabIndex="309" OnRowCommand="GridExpedientesInterconsumo_RowCommand" OnDataBound="GridExpedientesFinanciera_DataBound" OnHtmlDataCellPrepared="GridExpedientesFinanciera_HtmlDataCellPrepared" OnHtmlRowPrepared="GridExpedientesFinanciera_HtmlRowPrepared1"
                                                 EditFormLayoutProperties-EncodeHtml="False">

                                        <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>

                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>

                                    <ClientSideEvents BatchEditEndEditing="OnBatchEditEndEditing" />

                                    <SettingsPager PageSize="20" Visible="False">
                                    </SettingsPager>

                                    <SettingsEditing Mode="Batch" />

                                    <Settings  HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible"></Settings>

                                    <SettingsBehavior EnableRowHotTrack="True"></SettingsBehavior>
                                    
                                            <SettingsDataSecurity AllowInsert="False" />

                                    <SettingsText SearchPanelEditorNullText="Ingrese aquí su criterio de búsqueda" CommandBatchEditUpdate="Aplicar cambios" 
                                    CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                    ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"/>

<EditFormLayoutProperties EncodeHtml="False"></EditFormLayoutProperties>

                                    <Columns>
                                    
                                        
                                        <dx:GridViewDataColumn FieldName="NoFactura" ReadOnly="true" Caption="Factura" 
                                            Visible="true" Width="90px" VisibleIndex="1" >
                                            <EditFormSettings Visible="false"/>
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                         <dx:GridViewDataColumn FieldName="NombreFinanciera" ReadOnly="true"
                                            Caption="Financiera" Visible="true" Width="135px" VisibleIndex="2" UnboundType="String" >
                                                        
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center"   />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataTextColumn FieldName="FechaFactura" ReadOnly="true" Caption="Fecha" Visible="true" Width="85" VisibleIndex="4">
                                                        <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy" />
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataColumn FieldName="Cliente" ReadOnly="true" 
                                            Caption="Cliente" Visible="true" Width="60px" VisibleIndex="5" >
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="NombreCliente" ReadOnly="true" 
                                            Caption="Nombre de cliente" Visible="true" Width="230px" VisibleIndex="6">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Left" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" 
                                            Caption="Monto" Visible="true" Width="75px" VisibleIndex="7" >
                                                        <PropertiesTextEdit DisplayFormatString="n2" />
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataColumn FieldName="Solicitud" ReadOnly="false" 
                                            Caption="No. solicitud" Visible="true" Width="90px" VisibleIndex="8">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                     <dx:GridViewDataComboBoxColumn FieldName="Identificacion" 
                                            ReadOnly="false" Caption="Identificación" Visible="true" Width="95px" 
                                            VisibleIndex="9" >
                                                        <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="40" DropDownRows="25" />
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" >
                                                            <Border BorderStyle="None"></Border>
                                                        </HeaderStyle>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataComboBoxColumn>
                                                    <dx:GridViewDataComboBoxColumn FieldName="OtraIdentificacion" 
                                            ReadOnly="false" Caption="Otra Identificación" Visible="true" Width="120px" 
                                            VisibleIndex="10" >
                                                        <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="40" DropDownRows="25" />
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" >
                                                            <Border BorderStyle="None"></Border>
                                                        </HeaderStyle>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataComboBoxColumn>
                                                    <dx:GridViewDataCheckColumn FieldName="ConstanciaIngresos" 
                                            ReadOnly="false" Caption="Constancia de ingreso" Visible="true" Width="130px" 
                                            ToolTip="Marque esta casilla si el expedinte tiene adjunto el pagare" 
                                            VisibleIndex="11">
                                                        <PropertiesCheckEdit ValueType="System.String" ValueChecked="S" ValueUnchecked="N"></PropertiesCheckEdit>
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataCheckColumn>
                                                    <dx:GridViewDataComboBoxColumn FieldName="DocumentoServiciosPublicos" 
                                            ReadOnly="false" Caption="Doc. servicio público" Visible="true" Width="140px" 
                                            VisibleIndex="12" >
                                                        <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="40" DropDownRows="25" />
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" >
                                                            <Border BorderStyle="None"></Border>
                                                        </HeaderStyle>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataComboBoxColumn>
                                                    <dx:GridViewDataColumn FieldName="EstadosDeCuenta" ReadOnly="false" 
                                            Caption="Estados de cuenta" Visible="true" Width="115px" VisibleIndex="13" >
                                                        <HeaderStyle BackColor="#ff8a3f" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataCheckColumn FieldName="Pagare" ReadOnly="false" 
                                            Caption="Pagaré" Visible="true" Width="80px" 
                                            ToolTip="Marque esta casilla si el expedinte tiene adjunto el pagare" 
                                            VisibleIndex="14">
                                                        <PropertiesCheckEdit ValueType="System.String" ValueChecked="S" ValueUnchecked="N"></PropertiesCheckEdit>
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataCheckColumn>

                                                    <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" 
                                                        Visible="false" Width="105px" VisibleIndex="15" >
                                                        <HeaderStyle BackColor="#ff8a3f" />
                                                    </dx:GridViewDataColumn>

                                                    <dx:GridViewDataColumn FieldName="Observaciones" ReadOnly="false" 
                                            Caption="Observaciones" Visible="true" Width="105px" VisibleIndex="16" >
                                                        <HeaderStyle BackColor="#ff8a3f" />
                                                        </dx:GridViewDataColumn>
                                                   <dx:GridViewDataColumn FieldName="EstadoActual" ReadOnly="false" 
                                            Caption="Estado Actual" Visible="true" Width="105px" VisibleIndex="17" >
                                                        <HeaderStyle BackColor="#ff8a3f" />
                                                        </dx:GridViewDataColumn>
                                                    
                                                    
                                                    <dx:GridViewDataTextColumn FieldName="Eliminar" ReadOnly="true" Caption="Eliminar" Visible="true" Width="75px">
	                                                    <EditFormSettings Visible="False" />
	                                                    <DataItemTemplate>
		                                                    <dx:ASPxButton ID="btnEliminarExpediente" EncodeHtml="false" runat="server" AutoPostBack="false" Text="Eliminar" 
                                                            ToolTip="Haga clic aquí para eliminar la fila." OnClick="btnEliminar_Click" Font-Size="XX-Small"
                                                             ClientInstanceName="btnEliminar">
                                                              <ClientSideEvents Click="function(s, e) {
                                                                    e.processOnServer = confirm('¿Desea eliminar la fila seleccionada?');
                                                                        }" />
		                                                    </dx:ASPxButton>
	                                                    </DataItemTemplate>
	                                                    <HeaderStyle BackColor="#ff8a3f"  />
	                                                    <CellStyle Font-Size="XX-Small" />
                                                    </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>

<AlternatingRow BackColor="#FDE4CF"></AlternatingRow>

<FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>

<StatusBar Border-BorderStyle="None"></StatusBar>
                                    </Styles>

                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                                        </td>

                                    </tr>
                                </table>
                                <br />
                                <br />
                                        
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                </TabPages>
            </dx:ASPxPageControl>
                   
            <dx:ASPxGridViewExporter ID="gridExport1" runat="server" GridViewID="gridLibroVentas1" onrenderbrick="gridExport_RenderBrick"></dx:ASPxGridViewExporter>
            <dx:ASPxGridViewExporter ID="gridExport2" runat="server" GridViewID="gridLibroVentas2" onrenderbrick="gridExport_RenderBrick"></dx:ASPxGridViewExporter>
            <dx:ASPxGridViewExporter ID="gridExport3" runat="server" GridViewID="gridLibroVentas3" onrenderbrick="gridExport_RenderBrick"></dx:ASPxGridViewExporter>
            <dx:ASPxGridViewExporter ID="gridExport4" runat="server" GridViewID="gridLibroVentas4" onrenderbrick="gridExport_RenderBrick"></dx:ASPxGridViewExporter>
            <dx:ASPxGridViewExporter ID="gridExport5" runat="server" GridViewID="gridLibroVentas5" onrenderbrick="gridExport_RenderBrick"></dx:ASPxGridViewExporter>
            <dx:ASPxGridViewExporter ID="gridExport6" runat="server" GridViewID="gridLibroVentas6" onrenderbrick="gridExport_RenderBrick"></dx:ASPxGridViewExporter>
            <dx:ASPxGridViewExporter ID="gridExport7" runat="server" GridViewID="gridLibroVentas7" onrenderbrick="gridExport_RenderBrick"></dx:ASPxGridViewExporter>
            <dx:ASPxGridViewExporter ID="gridExport8" runat="server" GridViewID="gridLibroVentas8" onrenderbrick="gridExport8_RenderBrick"></dx:ASPxGridViewExporter>
            <dx:ASPxGridViewExporter ID="gridExport9" runat="server" GridViewID="gridFinanciera" onrenderbrick="gridInterconsumo_RenderBrick"></dx:ASPxGridViewExporter>
            <dx:ASPxGridViewExporter ID="gridExport09" runat="server" GridViewID="gridLibroVentas9" onrenderbrick="gridExport_RenderBrick"></dx:ASPxGridViewExporter>
            <dx:ASPxGridViewExporter ID="gridExport10" runat="server" GridViewID="gridLibroVentas10" onrenderbrick="gridExport_RenderBrick"></dx:ASPxGridViewExporter>
            <dx:ASPxGridViewExporter ID="gridExport11" runat="server" GridViewID="gridLibroVentas11" onrenderbrick="gridExport_RenderBrick"></dx:ASPxGridViewExporter>
            <dx:ASPxGridViewExporter ID="gridExport12" runat="server" GridViewID="gridLibroVentas12" onrenderbrick="gridExport_RenderBrick"></dx:ASPxGridViewExporter>
            <dx:ASPxGridViewExporter ID="gridExport13" runat="server" GridViewID="gridLibroVentas13" onrenderbrick="gridExport_RenderBrick"></dx:ASPxGridViewExporter>
            <dx:ASPxGridViewExporter ID="gridExport14" runat="server" GridViewID="gridConsultas" onrenderbrick="gridConsultas_RenderBrick"></dx:ASPxGridViewExporter>
           
            <ul>
                <li>
                    
                    
                </li>
            </ul>
        </div>

        <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server" TabIndex="620"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" TabIndex="630"></asp:Label>
                </td>
            </tr>
        </table>
    </div>    
</asp:Content>
