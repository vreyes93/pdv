﻿using System;
using System.Data;
using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FastMember;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Net.Mail;
using System.Net;
using DevExpress.Web;
using DevExpress.Web.Data;
using DevExpress.Export;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using Newtonsoft.Json;
using System.Reflection;
using MF_Clases;
using System.Web.Configuration;
using PuntoDeVenta.Util;
using System.Globalization;
using RestSharp;
using MF_Clases.Restful;
using MF_Clases.Comun;
using static MF_Clases.Clases;

namespace PuntoDeVenta
{
    public partial class contabilidad : BasePage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {

                //Clases.LoteFacturasInterconsumo mFacturasInterconsumo = (Clases.LoteFacturasInterconsumo)Session["FacturasFinanciera"];

                //if (mFacturasInterconsumo == null)
                //    mFacturasInterconsumo = new Clases.LoteFacturasInterconsumo();

                //GridFacturasInterconsumo.DataSource = mFacturasInterconsumo.FacturasInterconsumo.AsEnumerable();
                //GridFacturasInterconsumo.DataMember = "FacturasFinanciera";
                //GridFacturasInterconsumo.DataBind();
                //GridFacturasInterconsumo.SettingsPager.PageSize = 500;

                //Clases.LoteFacturasInterconsumo mExpedientesInterconsumo = (Clases.LoteFacturasInterconsumo)Session["ExpedientesInterconsumo"];


                //if (mExpedientesInterconsumo == null)
                //    mExpedientesInterconsumo = new Clases.LoteFacturasInterconsumo();

                //GridExpedientesInterconsumo.DataSource = mExpedientesInterconsumo.FacturasInterconsumo.AsEnumerable();
                //GridExpedientesInterconsumo.DataMember = "FacturasFinanciera";
                //GridExpedientesInterconsumo.DataBind();
                //GridExpedientesInterconsumo.SettingsPager.PageSize = 500;


                DataSet ds = new DataSet();
                ds = (DataSet)Session["LibroVentas"];
                if (gridLibroVentas8 != null)
                {
                    gridLibroVentas8.DataColumns["Monto"].PropertiesEdit.DisplayFormatString = "#,##0.00;(#,##0.00)";
                    var cond = new GridViewFormatConditionHighlight()
                    {
                        FieldName = "Monto",
                        Expression = "Monto < 0",
                        Format = GridConditionHighlightFormat.Custom
                    };
                    cond.CellStyle.ForeColor = System.Drawing.Color.Red;

                    gridLibroVentas8.FormatConditions.Add(cond);
                    gridLibroVentas8.DataSource = ds;
                    gridLibroVentas8.DataMember = "Resumen";
                    gridLibroVentas8.DataBind();
                    gridLibroVentas8.FocusedRowIndex = -1;
                    gridLibroVentas8.SettingsPager.PageSize = 30;
                }



            }
            catch
            {
                //Nothing
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                HookOnFocus(this.Page as Control);

                DateTime mFecha = DateTime.Now.Date.AddMonths(-1);
                cbMesLV.SelectedIndex = mFecha.Month - 1;
                txtAnioLV.Text = mFecha.Year.ToString();

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                var q = ws.DevuelveEmpresas();

                cbEmpresaLV.DataSource = q;
                cbEmpresaLV.ValueField = "Empresa";
                cbEmpresaLV.ValueType = typeof(System.String);
                cbEmpresaLV.TextField = "Empresa";
                cbEmpresaLV.DataBindItems();

                int ii = 0; int jj = 0;
                foreach (var item in q)
                {
                    if (item.Empresa == "prodmult") ii = jj;
                    jj += 1;
                }

                cbEmpresaLV.SelectedIndex = ii;
                cbTipoAntSaldos.SelectedIndex = 2;
                ASPxPageControl1.ActiveTabIndex = 0;

                //-----------------------------------------------------
                // Fecha sugerida, para consultar el desembolso.
                //-----------------------------------------------------
                fechaDesembolsos.Value = DateTime.Now.Date.AddDays(-1);

                //-----------------------------------------------------
                // Fecha sugerida, para aplicar el documento en el sistema.
                //-----------------------------------------------------
                fechaRigeDesembolso.Value = DateTime.Now.Date;

                //-----------------------------------------------------
                // Si el mes de la fecha sugerida para aplicar el documento 
                // al sistema es distinta al mes del desembolso, entonces
                // se sugiere el ultimo dia del mes anterior.
                //
                // Esto se da en el caso de los cierres.
                //-----------------------------------------------------
                if (((fechaRigeDesembolso.Date.Year - fechaDesembolsos.Date.Year) * 12
                    + fechaRigeDesembolso.Date.Month - fechaDesembolsos.Date.Month) > 0)
                {
                    DateTime firstDayOfThisMonth = DateTime.Today.Date.AddDays(-(DateTime.Today.Date.Day - 1));
                    fechaRigeDesembolso.Value = firstDayOfThisMonth.AddDays(-1);
                }


                fechaInicialFacturas.Value = DateTime.Now.Date.AddDays(-1);
                fechaFinalFacturas.Value = DateTime.Now.Date.AddDays(-1);

                fechaEnvioExpedientes.Value = DateTime.Now.Date;

                Session["FacturasFinanciera"] = new Clases.LoteFacturasFinanciera(); ;
                Session["ExpedientesInterconsumo"] = new Clases.LoteFacturasFinanciera(); ;

                ////Para la pestaña de interconsumo habilitar las opciones de desembolsos realizados por defecto
                //rldDesembolsosRealizados_CheckedChanged(sender, e);

                CargarTiendas();
                CargarBodegas();
                CargarFinancieras();
                btnLibroComprasModifica.ToolTip = string.Format("Modifica el libro de compras, las hojas que deben existir son:{0}{0}Borrar (Se eliminarán los documentos contenidos en esta hoja){0}BienServicio (A los documentos de esta hoja se les cambiará de bien a servicio o viceversa){0}PC (Trasladar de pequeño contribuyente a factura normal o viceversa)", System.Environment.NewLine);
                rdlRealizados.Checked = true;
            }

            CargarCuentasContables();

            Page.ClientScript.RegisterStartupScript(
            typeof(contabilidad),
            "ScriptDoFocus",
            SCRIPT_DOFOCUS.Replace("REQUEST_LASTFOCUS", Request["__LASTFOCUS"]),
            true);
        }

        void CargarTiendas()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveTiendas();

            tiendaDesFinanciera.DataSource = q;
            tiendaDesFinanciera.ValueField = "Tienda";
            tiendaDesFinanciera.ValueType = typeof(System.String);
            tiendaDesFinanciera.TextField = "Tienda";
            tiendaDesFinanciera.DataBindItems();
        }
        /// <summary>
        /// Método para llenar los combos de financieras dentro de Utilitarios
        /// </summary>
        void CargarFinancieras()
        {
            log.Debug("Cargando las financieras de la pantalla de utilitarios");
            try
            {
                var url = Session["UrlRestServices"].ToString();


                var lstFinancieras = WebRestClient.ObtenerFinancieras(url);
                //combo de Expedientes
                cmbExpFinanciera.DataSource = lstFinancieras;
                cmbExpFinanciera.ValueField = "ID";
                cmbExpFinanciera.ValueType = typeof(System.String);
                cmbExpFinanciera.TextField = "Nombre";
                cmbExpFinanciera.DataBindItems();
                cmbExpFinanciera.SelectedIndex = 0;
                //combo de financieras
                cmbFinanciera.DataSource = lstFinancieras;
                cmbFinanciera.ValueField = "ID";
                cmbFinanciera.ValueType = typeof(System.String);
                cmbFinanciera.TextField = "Nombre";
                cmbFinanciera.DataBindItems();
                cmbFinanciera.Items.Insert(0, new ListEditItem("TODAS", -1));
                cmbFinanciera.SelectedIndex = 0;
            }
            catch (Exception i)
            {
                log.Error("Problema al cargar las financieras en la pantalla " + i.Message);
            }

        }
        private const string SCRIPT_DOFOCUS = @"window.setTimeout('DoFocus()', 1); function DoFocus() {
            try {
                document.getElementById('REQUEST_LASTFOCUS').focus();
            } catch (ex) {}
        }";

        private void HookOnFocus(Control CurrentControl)
        {
            //checks if control is one of TextBox, DropDownList, ListBox or Button
            if ((CurrentControl is TextBox) ||
                (CurrentControl is DropDownList) ||
                (CurrentControl is ListBox) ||
                (CurrentControl is Button))
                //adds a script which saves active control on receiving focus 
                //in the hidden field __LASTFOCUS.
                (CurrentControl as WebControl).Attributes.Add(
                   "onfocus",
                   "try{document.getElementById('__LASTFOCUS').value=this.id} catch(e) {}");
            //checks if the control has children
            if (CurrentControl.HasControls())
                //if yes do them all recursively
                foreach (Control CurrentChildControl in CurrentControl.Controls)
                    HookOnFocus(CurrentChildControl);
        }

        void CargarCuentasContables()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            DataSet dsCuentas = new DataSet(); string mMensaje = "";
            dsCuentas = ws.CuentasContables(ref mMensaje, cbEmpresaLV.SelectedItem.ToString());

            gridCuentasBuscar.DataSource = dsCuentas;
            gridCuentasBuscar.DataMember = "cuentas";
            gridCuentasBuscar.DataBind();
            gridCuentasBuscar.FocusedRowIndex = -1;
            gridCuentasBuscar.SettingsPager.PageSize = 35;
        }

        void CargarBodegas()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.DevuelveTiendasTodas();

            cbBodega.DataSource = q;
            cbBodega.ValueField = "Tienda";
            cbBodega.ValueType = typeof(System.String);
            cbBodega.TextField = "Nombre";
            cbBodega.DataBindItems();

            cbBodega.Value = Convert.ToString(Session["Tienda"]);
        }

        protected void ToolbarExport_ItemClick(object source, ExportItemClickEventArgs e)
        {
            try
            {
                lbInfoLibroVentas.Text = "";
                lbErrorLibroVentas.Text = "";

                lbInfoAntiguedad.Text = "";
                lbErrorAntiguedad.Text = "";

                int mAnio = 0; int mNumeroMes = 0;

                try
                {
                    mAnio = Convert.ToInt32(txtAnioLV.Text);
                }
                catch
                {
                    lbErrorLibroVentas.Text = "Ingrese un año válido";
                    return;
                }

                if (mAnio < 2010)
                {
                    lbErrorLibroVentas.Text = "Ingrese un año válido";
                    return;
                }


                try
                {
                    mNumeroMes = Convert.ToInt32(cbMesLV.SelectedItem.Value);
                }
                catch
                {
                    lbErrorLibroVentas.Text = "Debe seleccionar un mes";
                    cbMesLV.Focus();
                    return;
                }

                if (mNumeroMes < 0 || mNumeroMes > 12)
                {
                    lbErrorLibroVentas.Text = "Debe seleccionar un mes";
                    cbMesLV.Focus();
                    return;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);


                //txtAnioLV.Text, Convert.ToString(cbMesLV.SelectedItem.Value)

                string mMes = "Enero"; DateTime mFecha = new DateTime(mAnio, mNumeroMes, 1);
                string mFechaHoyString = string.Format("{0}{1}{2}{3}{4}", DateTime.Now.Day < 10 ? "0" : "", DateTime.Now.Day, DateTime.Now.Month < 10 ? "0" : "", DateTime.Now.Month, DateTime.Now.Year);

                if (mFecha.Month == 2) mMes = "Febrero";
                if (mFecha.Month == 3) mMes = "Marzo";
                if (mFecha.Month == 4) mMes = "Abril";
                if (mFecha.Month == 5) mMes = "Mayo";
                if (mFecha.Month == 6) mMes = "Junio";
                if (mFecha.Month == 7) mMes = "Julio";
                if (mFecha.Month == 8) mMes = "Agosto";
                if (mFecha.Month == 9) mMes = "Septiembre";
                if (mFecha.Month == 10) mMes = "Octubre";
                if (mFecha.Month == 11) mMes = "Noviembre";
                if (mFecha.Month == 12) mMes = "Diciembre";

                ws.Timeout = 999999999;
                string mMensaje = ""; DataSet ds = new DataSet();

                try
                {
                    Directory.CreateDirectory(@"C:\reportes\Contabilidad\");
                }
                catch
                {
                    //Nothing
                }



                if (e.ExportType == DemoExportFormat.LVPlantilla)
                {
                    ds = ws.PlantillaNotas(ref mMensaje);

                    if (mMensaje.Trim().Length > 0)
                    {
                        lbErrorLibroVentas.Text = mMensaje;
                        return;
                    }

                    gridLibroVentas1.DataSource = ds;
                    gridLibroVentas1.DataMember = "NotasCredito";
                    gridLibroVentas1.DataBind();
                    gridLibroVentas1.FocusedRowIndex = -1;
                    gridLibroVentas1.SettingsPager.PageSize = 30;

                    gridLibroVentas2.DataSource = ds;
                    gridLibroVentas2.DataMember = "NotasAbono";
                    gridLibroVentas2.DataBind();
                    gridLibroVentas2.FocusedRowIndex = -2;
                    gridLibroVentas2.SettingsPager.PageSize = 30;

                    gridLibroVentas3.DataSource = ds;
                    gridLibroVentas3.DataMember = "Consecutivos";
                    gridLibroVentas3.DataBind();
                    gridLibroVentas3.FocusedRowIndex = -3;
                    gridLibroVentas3.SettingsPager.PageSize = 30;

                    PrintingSystemBase ps = new PrintingSystemBase();
                    ps.XlSheetCreated += ps_XlSheetCreated;

                    PrintableComponentLinkBase link1 = new PrintableComponentLinkBase(ps);
                    link1.PaperName = "NotasCredito";
                    link1.Component = gridExport1;

                    PrintableComponentLinkBase link2 = new PrintableComponentLinkBase(ps);
                    link2.PaperName = "NotasAbono";
                    link2.Component = gridExport2;

                    PrintableComponentLinkBase link3 = new PrintableComponentLinkBase(ps);
                    link3.PaperName = "Consecutivos";
                    link3.Component = gridExport3;

                    CompositeLinkBase compositeLink = new CompositeLinkBase(ps);
                    compositeLink.Links.AddRange(new object[] { link1, link2, link3 });

                    compositeLink.CreatePageForEachLink();

                    using (MemoryStream stream = new MemoryStream())
                    {
                        XlsxExportOptions options = new XlsxExportOptions();
                        options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                        compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                        Response.Clear();
                        Response.Buffer = false;
                        Response.AppendHeader("Content-Type", "application/xlsx");
                        Response.AppendHeader("Content-Transfer-Encoding", "binary");
                        Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=Notas_{0}{1}.xlsx", mMes, mAnio));
                        Response.BinaryWrite(stream.ToArray());
                        Response.End();
                    }
                    ps.Dispose();
                }



                if (e.ExportType == DemoExportFormat.LVCargarNotas)
                {
                    string mFile = "";
                    try
                    {
                        mFile = uploadFile.UploadedFiles[0].FileName;
                    }
                    catch
                    {
                        mFile = "";
                    }

                    if (mFile.Trim().Length == 0)
                    {
                        lbErrorLibroVentas.Text = "No se ha cargado ningún archivo";
                        return;
                    }

                    string mArchivo = @"C:\reportes\Contabilidad\Notas_" + mMes + mAnio + ".xlsx";

                    try
                    {
                        File.Delete(mArchivo);
                    }
                    catch
                    {
                        //Nothing
                    }

                    uploadFile.UploadedFiles[0].SaveAs(mArchivo);

                    System.Data.OleDb.OleDbConnection mConexion = new System.Data.OleDb.OleDbConnection(string.Format("Provider=Microsoft.ACE.OLEDB.12.0; data source={0}; Extended Properties=Excel 8.0;", mArchivo));
                    System.Data.OleDb.OleDbDataAdapter daNotasCredito = new System.Data.OleDb.OleDbDataAdapter("SELECT Documento, Tipo, Fecha, Consecutivo FROM [NotasCredito$]", mConexion);
                    System.Data.OleDb.OleDbDataAdapter daNotasAbono = new System.Data.OleDb.OleDbDataAdapter("SELECT Documento, Tipo, Fecha FROM [NotasAbono$]", mConexion);

                    DataTable dtNotasCredito = new DataTable("NotasCredito");
                    DataTable dtNotasAbono = new DataTable("NotasAbono");

                    try
                    {
                        daNotasCredito.Fill(dtNotasCredito);
                    }
                    catch
                    {
                        lbErrorLibroVentas.Text = "El archivo no contiene una hoja llamada NotasCredito o bien las columnas no son las correctas, por favor descargue la plantilla";
                        return;
                    }

                    try
                    {
                        daNotasAbono.Fill(dtNotasAbono);
                    }
                    catch
                    {
                        lbErrorLibroVentas.Text = "El archivo no contiene una hoja llamada NotasAbono o bien las columnas no son las correctas, por favor descargue la plantilla";
                        return;
                    }

                    ds.Tables.Add(dtNotasCredito);
                    ds.Tables.Add(dtNotasAbono);

                    if (ds.Tables["NotasCredito"].Rows.Count == 0 && ds.Tables["NotasAbono"].Rows.Count == 0)
                    {
                        lbErrorLibroVentas.Text = "No se encontraron notas de crédito ni de abono";
                        return;
                    }

                    if (!ws.CargarNotasVentas(ds, ref mMensaje, Convert.ToString(Session["usuario"])))
                    {
                        lbErrorLibroVentas.Text = mMensaje;
                        return;
                    }

                    lbInfoLibroVentas.Text = mMensaje;
                }

                if (e.ExportType == DemoExportFormat.UndoNotas)
                {
                    string mFile = "";
                    try
                    {
                        mFile = uploadFile.UploadedFiles[0].FileName;
                    }
                    catch
                    {
                        mFile = "";
                    }

                    if (mFile.Trim().Length == 0)
                    {
                        lbErrorLibroVentas.Text = "No se ha cargado ningún archivo";
                        return;
                    }

                    string mArchivo = @"C:\reportes\Contabilidad\UndoNotas_" + mMes + mAnio + ".xlsx";

                    try
                    {
                        File.Delete(mArchivo);
                    }
                    catch
                    {
                        //Nothing
                    }

                    uploadFile.UploadedFiles[0].SaveAs(mArchivo);

                    System.Data.OleDb.OleDbConnection mConexion = new System.Data.OleDb.OleDbConnection(string.Format("Provider=Microsoft.ACE.OLEDB.12.0; data source={0}; Extended Properties=Excel 8.0;", mArchivo));
                    System.Data.OleDb.OleDbDataAdapter daNotasCredito = new System.Data.OleDb.OleDbDataAdapter("SELECT Documento, Tipo, Fecha, Consecutivo FROM [NotasCredito$]", mConexion);
                    System.Data.OleDb.OleDbDataAdapter daNotasAbono = new System.Data.OleDb.OleDbDataAdapter("SELECT Documento, Tipo, Fecha FROM [NotasAbono$]", mConexion);

                    DataTable dtNotasCredito = new DataTable("NotasCredito");
                    DataTable dtNotasAbono = new DataTable("NotasAbono");

                    try
                    {
                        daNotasCredito.Fill(dtNotasCredito);
                    }
                    catch
                    {
                        lbErrorLibroVentas.Text = "El archivo no contiene una hoja llamada NotasCredito o bien las columnas no son las correctas, por favor descargue la plantilla";
                        return;
                    }

                    try
                    {
                        daNotasAbono.Fill(dtNotasAbono);
                    }
                    catch
                    {
                        lbErrorLibroVentas.Text = "El archivo no contiene una hoja llamada NotasAbono o bien las columnas no son las correctas, por favor descargue la plantilla";
                        return;
                    }

                    ds.Tables.Add(dtNotasCredito);
                    ds.Tables.Add(dtNotasAbono);

                    if (ds.Tables["NotasCredito"].Rows.Count == 0 && ds.Tables["NotasAbono"].Rows.Count == 0)
                    {
                        lbErrorLibroVentas.Text = "No se encontraron notas de crédito ni de abono para revertir";
                        return;
                    }

                    if (!ws.UndoCargarNotasVentas(ds, ref mMensaje, Convert.ToString(Session["usuario"])))
                    {
                        lbErrorLibroVentas.Text = mMensaje;
                        return;
                    }

                    lbInfoLibroVentas.Text = mMensaje;
                }



                if (e.ExportType == DemoExportFormat.LVExcel)
                {
                    ds = ws.LibroVentas(ref mMensaje, txtAnioLV.Text, Convert.ToString(cbMesLV.SelectedItem.Value), cbEmpresaLV.SelectedItem.ToString().ToLower(), true);

                    if (mMensaje.Trim().Length > 0)
                    {
                        lbErrorLibroVentas.Text = mMensaje;
                        return;
                    }

                    gridLibroVentas1.DataSource = ds;
                    gridLibroVentas1.DataMember = "LibroVentas";
                    gridLibroVentas1.DataBind();
                    gridLibroVentas1.FocusedRowIndex = -1;
                    gridLibroVentas1.SettingsPager.PageSize = 100;

                    gridLibroVentas2.DataSource = ds;
                    gridLibroVentas2.DataMember = "Documentos";
                    gridLibroVentas2.DataBind();
                    gridLibroVentas2.FocusedRowIndex = -1;
                    gridLibroVentas2.SettingsPager.PageSize = 100;

                    gridLibroVentas3.DataSource = ds;
                    gridLibroVentas3.DataMember = "DMG";
                    gridLibroVentas3.DataBind();
                    gridLibroVentas3.FocusedRowIndex = -1;
                    gridLibroVentas3.SettingsPager.PageSize = 100;

                    gridLibroVentas4.DataSource = ds;
                    gridLibroVentas4.DataMember = "encabezado";
                    gridLibroVentas4.DataBind();
                    gridLibroVentas4.FocusedRowIndex = -1;
                    gridLibroVentas4.SettingsPager.PageSize = 100;

                    gridLibroVentas5.DataSource = ds;
                    gridLibroVentas5.DataMember = "detalle";
                    gridLibroVentas5.DataBind();
                    gridLibroVentas5.FocusedRowIndex = -1;
                    gridLibroVentas5.SettingsPager.PageSize = 100;

                    gridLibroVentas6.DataSource = ds;
                    gridLibroVentas6.DataMember = "GFaceCuadre";
                    gridLibroVentas6.DataBind();
                    gridLibroVentas6.FocusedRowIndex = -1;
                    gridLibroVentas6.SettingsPager.PageSize = 100;

                    gridLibroVentas7.DataSource = ds;
                    gridLibroVentas7.DataMember = "DocumentosCuadre";
                    gridLibroVentas7.DataBind();
                    gridLibroVentas7.FocusedRowIndex = -1;
                    gridLibroVentas7.SettingsPager.PageSize = 100;

                    gridLibroVentas8.DataSource = ds;
                    gridLibroVentas8.DataMember = "Resumen";
                    gridLibroVentas8.DataBind();
                    gridLibroVentas8.FocusedRowIndex = -1;
                    gridLibroVentas8.SettingsPager.PageSize = 100;

                    Session["LibroVentas"] = ds;
                    gridLibroVentas8.Settings.ShowColumnHeaders = false;

                    PrintingSystemBase ps = new PrintingSystemBase();
                    ps.XlSheetCreated += psLV_XlSheetCreated;

                    PrintableComponentLinkBase link1 = new PrintableComponentLinkBase(ps);
                    link1.PaperName = "LibroVentas";
                    link1.Component = gridExport1;

                    PrintableComponentLinkBase link2 = new PrintableComponentLinkBase(ps);
                    link2.PaperName = "Documentos";
                    link2.Component = gridExport2;

                    PrintableComponentLinkBase link3 = new PrintableComponentLinkBase(ps);
                    link3.PaperName = "DMG";
                    link3.Component = gridExport3;

                    PrintableComponentLinkBase link4 = new PrintableComponentLinkBase(ps);
                    link4.PaperName = "GFace";
                    link4.Component = gridExport4;

                    PrintableComponentLinkBase link5 = new PrintableComponentLinkBase(ps);
                    link5.PaperName = "GFaceDetalle";
                    link5.Component = gridExport5;

                    PrintableComponentLinkBase link6 = new PrintableComponentLinkBase(ps);
                    link6.PaperName = "GFaceCuadre";
                    link6.Component = gridExport6;

                    PrintableComponentLinkBase link7 = new PrintableComponentLinkBase(ps);
                    link7.PaperName = "DocumentosCuadre";
                    link7.Component = gridExport7;

                    PrintableComponentLinkBase link8 = new PrintableComponentLinkBase(ps);
                    link8.PaperName = "Resumen";
                    link8.Component = gridExport8;

                    CompositeLinkBase compositeLink = new CompositeLinkBase(ps);
                    compositeLink.Links.AddRange(new object[] { link1, link2, link3, link4, link5, link6, link7, link8 });

                    compositeLink.CreatePageForEachLink();

                    string mIniciales = "";
                    if (cbEmpresaLV.SelectedItem.ToString().ToLower() == "emprconc") mIniciales = "EC";

                    using (MemoryStream stream = new MemoryStream())
                    {
                        XlsxExportOptions options = new XlsxExportOptions();
                        options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                        compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                        Response.Clear();
                        Response.Buffer = false;
                        Response.AppendHeader("Content-Type", "application/xlsx");
                        Response.AppendHeader("Content-Transfer-Encoding", "binary");
                        Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=LibroVentas{0}_{1}{2}.xlsx", mIniciales, mMes, mAnio));
                        Response.BinaryWrite(stream.ToArray());
                        Response.End();
                    }
                    ps.Dispose();

                    gridLibroVentas8.Settings.ShowColumnHeaders = true;
                }

                if (e.ExportType == DemoExportFormat.LIXls)
                {
                    ds = ws.LibroInventario(ref mMensaje, txtAnioLV.Text, Convert.ToString(cbMesLV.SelectedItem.Value), cbEmpresaLV.SelectedItem.ToString().ToLower(), true);

                    if (mMensaje.Trim().Length > 0)
                    {
                        lbErrorLibroVentas.Text = mMensaje;
                        return;
                    }

                    gridLibroVentas1.DataSource = ds;
                    gridLibroVentas1.DataMember = "LibroInventario";
                    gridLibroVentas1.DataBind();
                    gridLibroVentas1.FocusedRowIndex = -1;
                    gridLibroVentas1.SettingsPager.PageSize = 100;

                    gridLibroVentas2.DataSource = ds;
                    gridLibroVentas2.DataMember = "LibroInventarioXBodega";
                    gridLibroVentas2.DataBind();
                    gridLibroVentas2.FocusedRowIndex = -1;
                    gridLibroVentas2.SettingsPager.PageSize = 100;

                    gridLibroVentas3.DataSource = ds;
                    gridLibroVentas3.DataMember = "PorDespachar";
                    gridLibroVentas3.DataBind();
                    gridLibroVentas3.FocusedRowIndex = -1;
                    gridLibroVentas3.SettingsPager.PageSize = 100;

                    gridLibroVentas4.DataSource = ds;
                    gridLibroVentas4.DataMember = "BalanceDeComprobacion";
                    gridLibroVentas4.DataBind();
                    gridLibroVentas4.FocusedRowIndex = -1;
                    gridLibroVentas4.SettingsPager.PageSize = 100;

                    gridLibroVentas8.DataSource = ds;
                    gridLibroVentas8.DataMember = "Resumen";
                    gridLibroVentas8.DataBind();
                    gridLibroVentas8.FocusedRowIndex = -1;
                    gridLibroVentas8.SettingsPager.PageSize = 100;



                    Session["LibroInventario"] = ds;
                    gridLibroVentas8.Settings.ShowColumnHeaders = false;

                    PrintingSystemBase ps = new PrintingSystemBase();
                    ps.XlSheetCreated += psLI_XlSheetCreated;

                    PrintableComponentLinkBase link1 = new PrintableComponentLinkBase(ps);
                    link1.PaperName = "LibroInventario";
                    link1.Component = gridExport1;

                    PrintableComponentLinkBase link2 = new PrintableComponentLinkBase(ps);
                    link2.PaperName = "LibroInventarioXBodega";
                    link2.Component = gridExport2;

                    PrintableComponentLinkBase link3 = new PrintableComponentLinkBase(ps);
                    link3.PaperName = "PorDespachar";
                    link3.Component = gridExport3;

                    PrintableComponentLinkBase link4 = new PrintableComponentLinkBase(ps);
                    link4.PaperName = "BalanceDeComprobacion";
                    link4.Component = gridExport4;

                    PrintableComponentLinkBase link8 = new PrintableComponentLinkBase(ps);
                    link8.PaperName = "Resumen";
                    link8.Component = gridExport8;


                    CompositeLinkBase compositeLink = new CompositeLinkBase(ps);
                    compositeLink.Links.AddRange(new object[] { link1, link2, link3, link4, link8 });

                    compositeLink.CreatePageForEachLink();

                    string mIniciales = "";
                    if (cbEmpresaLV.SelectedItem.ToString().ToLower() == "emprconc") mIniciales = "EC";

                    using (MemoryStream stream = new MemoryStream())
                    {
                        XlsxExportOptions options = new XlsxExportOptions();
                        options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                        compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                        Response.Clear();
                        Response.Buffer = false;
                        Response.AppendHeader("Content-Type", "application/xlsx");
                        Response.AppendHeader("Content-Transfer-Encoding", "binary");
                        Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=LibroInventario{0}_{1}{2}.xlsx", mIniciales, mMes, mAnio));
                        Response.BinaryWrite(stream.ToArray());
                        Response.End();
                    }
                    ps.Dispose();
                    gridLibroVentas8.Settings.ShowColumnHeaders = true;

                }


                if (e.ExportType == DemoExportFormat.LVAsl)
                {
                    try
                    {
                        string mMes1 = Convert.ToString(cbMesLV.SelectedItem.Value);
                    }
                    catch
                    {
                        lbErrorLibroVentas.Text = "Debe seleccionar un mes";
                        cbMesLV.Focus();
                        return;
                    }

                    ds = ws.LibroVentas(ref mMensaje, txtAnioLV.Text, Convert.ToString(cbMesLV.SelectedItem.Value), cbEmpresaLV.SelectedItem.ToString().ToLower(), false);

                    if (mMensaje.Trim().Length > 0)
                    {
                        lbErrorLibroVentas.Text = mMensaje;
                        return;
                    }

                    gridLibroVentas1.DataSource = ds;
                    gridLibroVentas1.DataMember = "LibroVentas";
                    gridLibroVentas1.DataBind();
                    gridLibroVentas1.FocusedRowIndex = -1;
                    gridLibroVentas1.SettingsPager.PageSize = 30;

                    gridLibroVentas1.Settings.ShowColumnHeaders = false;

                    PrintingSystemBase ps = new PrintingSystemBase();
                    ps.XlSheetCreated += ps_XlSheetCreated;

                    PrintableComponentLinkBase link1 = new PrintableComponentLinkBase(ps);
                    link1.PaperName = "LibroVentas";
                    link1.Component = gridExport1;

                    CompositeLinkBase compositeLink = new CompositeLinkBase(ps);
                    compositeLink.Links.AddRange(new object[] { link1 });

                    compositeLink.CreateDocument();

                    string mIniciales = "";
                    if (cbEmpresaLV.SelectedItem.ToString().ToLower() == "emprconc") mIniciales = "EC";

                    using (MemoryStream stream = new MemoryStream())
                    {
                        CsvExportOptions options = new CsvExportOptions();
                        compositeLink.ExportToCsv(stream, new CsvExportOptionsEx() { Separator = "|", ExportType = DevExpress.Export.ExportType.WYSIWYG });
                        Response.Clear();
                        Response.Buffer = false;
                        Response.AppendHeader("Content-Type", "application/asl");
                        Response.AppendHeader("Content-Transfer-Encoding", "binary");
                        Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=Carga{0} {1} {2} V.asl", mIniciales, mMes, mAnio));
                        Response.BinaryWrite(stream.ToArray());
                        Response.End();
                    }
                    ps.Dispose();

                    gridLibroVentas1.Settings.ShowColumnHeaders = true;
                }



                if (e.ExportType == DemoExportFormat.PlantExencion)
                {
                    ds = ws.PlantillaExenciones(ref mMensaje);

                    if (mMensaje.Trim().Length > 0)
                    {
                        lbErrorLibroVentas.Text = mMensaje;
                        return;
                    }

                    gridLibroVentas1.DataSource = ds;
                    gridLibroVentas1.DataMember = "Exenciones";
                    gridLibroVentas1.DataBind();
                    gridLibroVentas1.FocusedRowIndex = -1;
                    gridLibroVentas1.SettingsPager.PageSize = 30;

                    PrintingSystemBase ps = new PrintingSystemBase();
                    ps.XlSheetCreated += psEx_XlSheetCreated;

                    PrintableComponentLinkBase link1 = new PrintableComponentLinkBase(ps);
                    link1.PaperName = "Exenciones";
                    link1.Component = gridExport1;

                    CompositeLinkBase compositeLink = new CompositeLinkBase(ps);
                    compositeLink.Links.AddRange(new object[] { link1 });

                    compositeLink.CreatePageForEachLink();

                    using (MemoryStream stream = new MemoryStream())
                    {
                        XlsxExportOptions options = new XlsxExportOptions();
                        options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                        compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                        Response.Clear();
                        Response.Buffer = false;
                        Response.AppendHeader("Content-Type", "application/xlsx");
                        Response.AppendHeader("Content-Transfer-Encoding", "binary");
                        Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=Exenciones_{0}.xlsx", mFechaHoyString));
                        Response.BinaryWrite(stream.ToArray());
                        Response.End();
                    }
                    ps.Dispose();
                }



                if (e.ExportType == DemoExportFormat.CargaExencion)
                {
                    string mFile = "";
                    try
                    {
                        mFile = uploadFile.UploadedFiles[0].FileName;
                    }
                    catch
                    {
                        mFile = "";
                    }

                    if (mFile.Trim().Length == 0)
                    {
                        lbErrorLibroVentas.Text = "No se ha cargado ningún archivo";
                        return;
                    }

                    string mArchivo = @"C:\reportes\Contabilidad\Exenciones_" + mFechaHoyString + ".xlsx";

                    try
                    {
                        File.Delete(mArchivo);
                    }
                    catch
                    {
                        //Nothing
                    }

                    uploadFile.UploadedFiles[0].SaveAs(mArchivo);

                    System.Data.OleDb.OleDbConnection mConexion = new System.Data.OleDb.OleDbConnection(string.Format("Provider=Microsoft.ACE.OLEDB.12.0; data source={0}; Extended Properties=Excel 8.0;", mArchivo));
                    System.Data.OleDb.OleDbDataAdapter daExenciones = new System.Data.OleDb.OleDbDataAdapter("SELECT Tipo, Numero, Fecha, Valor FROM [Exenciones$]", mConexion);

                    DataTable dtExenciones = new DataTable("Exenciones");

                    try
                    {
                        daExenciones.Fill(dtExenciones);
                    }
                    catch
                    {
                        lbErrorLibroVentas.Text = "El archivo no contiene una hoja llamada Exenciones o bien las columnas no son las correctas, por favor descargue la plantilla";
                        return;
                    }

                    ds.Tables.Add(dtExenciones);

                    if (ds.Tables["Exenciones"].Rows.Count == 0)
                    {
                        lbErrorLibroVentas.Text = "No se encontraron exenciones";
                        return;
                    }

                    if (!ws.CargarExenciones(ds, ref mMensaje, Convert.ToString(Session["usuario"])))
                    {
                        lbErrorLibroVentas.Text = mMensaje;
                        return;
                    }

                    lbInfoLibroVentas.Text = mMensaje;
                }



                if (e.ExportType == DemoExportFormat.PlantAnul)
                {
                    ds = ws.PlantillaFacturasAnuladas(ref mMensaje);

                    if (mMensaje.Trim().Length > 0)
                    {
                        lbErrorLibroVentas.Text = mMensaje;
                        return;
                    }

                    gridLibroVentas1.DataSource = ds;
                    gridLibroVentas1.DataMember = "FacturasAnuladas";
                    gridLibroVentas1.DataBind();
                    gridLibroVentas1.FocusedRowIndex = -1;
                    gridLibroVentas1.SettingsPager.PageSize = 30;

                    gridLibroVentas2.DataSource = ds;
                    gridLibroVentas2.DataMember = "Consecutivos";
                    gridLibroVentas2.DataBind();
                    gridLibroVentas2.FocusedRowIndex = -3;
                    gridLibroVentas2.SettingsPager.PageSize = 30;

                    PrintingSystemBase ps = new PrintingSystemBase();
                    ps.XlSheetCreated += psAnuladas_XlSheetCreated;

                    PrintableComponentLinkBase link1 = new PrintableComponentLinkBase(ps);
                    link1.PaperName = "FacturasAnuladas";
                    link1.Component = gridExport1;

                    PrintableComponentLinkBase link2 = new PrintableComponentLinkBase(ps);
                    link2.PaperName = "Consecutivos";
                    link2.Component = gridExport2;

                    CompositeLinkBase compositeLink = new CompositeLinkBase(ps);
                    compositeLink.Links.AddRange(new object[] { link1, link2 });

                    compositeLink.CreatePageForEachLink();

                    using (MemoryStream stream = new MemoryStream())
                    {
                        XlsxExportOptions options = new XlsxExportOptions();
                        options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                        compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                        Response.Clear();
                        Response.Buffer = false;
                        Response.AppendHeader("Content-Type", "application/xlsx");
                        Response.AppendHeader("Content-Transfer-Encoding", "binary");
                        Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=FacturasAnuladas_{0}.xlsx", mFechaHoyString));
                        Response.BinaryWrite(stream.ToArray());
                        Response.End();
                    }
                    ps.Dispose();
                }



                if (e.ExportType == DemoExportFormat.CargaAnul)
                {
                    string mFile = "";
                    try
                    {
                        mFile = uploadFile.UploadedFiles[0].FileName;
                    }
                    catch
                    {
                        mFile = "";
                    }

                    if (mFile.Trim().Length == 0)
                    {
                        lbErrorLibroVentas.Text = "No se ha cargado ningún archivo";
                        return;
                    }

                    string mArchivo = @"C:\reportes\Contabilidad\FacturasAnuladas_" + mFechaHoyString + ".xlsx";

                    try
                    {
                        File.Delete(mArchivo);
                    }
                    catch
                    {
                        //Nothing
                    }

                    uploadFile.UploadedFiles[0].SaveAs(mArchivo);

                    System.Data.OleDb.OleDbConnection mConexion = new System.Data.OleDb.OleDbConnection(string.Format("Provider=Microsoft.ACE.OLEDB.12.0; data source={0}; Extended Properties=Excel 8.0;", mArchivo));
                    System.Data.OleDb.OleDbDataAdapter daFacturasAnuladas = new System.Data.OleDb.OleDbDataAdapter("SELECT Tienda, Factura, Fecha, Consecutivo FROM [FacturasAnuladas$]", mConexion);

                    DataTable dtFacturasAnuladas = new DataTable("FacturasAnuladas");

                    try
                    {
                        daFacturasAnuladas.Fill(dtFacturasAnuladas);
                    }
                    catch
                    {
                        lbErrorLibroVentas.Text = "El archivo no contiene una hoja llamada Facturas Anuladas o bien las columnas no son las correctas, por favor descargue la plantilla";
                        return;
                    }

                    ds.Tables.Add(dtFacturasAnuladas);

                    if (ds.Tables["FacturasAnuladas"].Rows.Count == 0)
                    {
                        lbErrorLibroVentas.Text = "No se encontraron facturas para anular";
                        return;
                    }

                    if (!ws.CargarFacturasAnuladas(ds, ref mMensaje, Convert.ToString(Session["usuario"])))
                    {
                        lbErrorLibroVentas.Text = mMensaje;
                        return;
                    }

                    lbInfoLibroVentas.Text = mMensaje;
                }



                if (e.ExportType == DemoExportFormat.AntSaldos)
                {
                    ds = ws.AntiguedadSaldos(ref mMensaje, txtAnioLV.Text, Convert.ToString(cbMesLV.SelectedItem.Value), cbEmpresaLV.SelectedItem.ToString().ToLower(), Convert.ToString(cbTipoAntSaldos.SelectedItem.Value), txtClienteAntSaldos.Text.Trim().ToUpper());

                    if (mMensaje.Trim().Length > 0)
                    {
                        lbErrorAntiguedad.Text = mMensaje;
                        return;
                    }

                    gridLibroVentas1.DataSource = ds;
                    gridLibroVentas1.DataMember = "AntiguedadSaldos";
                    gridLibroVentas1.DataBind();
                    gridLibroVentas1.FocusedRowIndex = -1;
                    gridLibroVentas1.SettingsPager.PageSize = 100;

                    gridLibroVentas2.DataSource = ds;
                    gridLibroVentas2.DataMember = "Documentos";
                    gridLibroVentas2.DataBind();
                    gridLibroVentas2.FocusedRowIndex = -1;
                    gridLibroVentas2.SettingsPager.PageSize = 100;

                    gridLibroVentas3.DataSource = ds;
                    gridLibroVentas3.DataMember = "EstadosCuenta";
                    gridLibroVentas3.DataBind();
                    gridLibroVentas3.FocusedRowIndex = -1;
                    gridLibroVentas3.SettingsPager.PageSize = 100;

                    gridLibroVentas4.DataSource = ds;
                    gridLibroVentas4.DataMember = "EdoCuentaAnalizar";
                    gridLibroVentas4.DataBind();
                    gridLibroVentas4.FocusedRowIndex = -1;
                    gridLibroVentas4.SettingsPager.PageSize = 100;

                    gridLibroVentas5.DataSource = ds;
                    gridLibroVentas5.DataMember = "CuentasPorCobrar";
                    gridLibroVentas5.DataBind();
                    gridLibroVentas5.FocusedRowIndex = -1;
                    gridLibroVentas5.SettingsPager.PageSize = 100;

                    gridLibroVentas6.DataSource = ds;
                    gridLibroVentas6.DataMember = "DMG";
                    gridLibroVentas6.DataBind();
                    gridLibroVentas6.FocusedRowIndex = -1;
                    gridLibroVentas6.SettingsPager.PageSize = 100;

                    gridLibroVentas8.DataSource = ds;
                    gridLibroVentas8.DataMember = "Resumen";
                    gridLibroVentas8.DataBind();
                    gridLibroVentas8.FocusedRowIndex = -1;
                    gridLibroVentas8.SettingsPager.PageSize = 100;

                    Session["LibroVentas"] = ds;
                    gridLibroVentas8.Settings.ShowColumnHeaders = false;

                    PrintingSystemBase ps = new PrintingSystemBase();
                    ps.XlSheetCreated += psAntSaldos_XlSheetCreated;

                    PrintableComponentLinkBase link1 = new PrintableComponentLinkBase(ps);
                    link1.PaperName = "AntiguedadSaldos";
                    link1.Component = gridExport1;

                    PrintableComponentLinkBase link2 = new PrintableComponentLinkBase(ps);
                    link2.PaperName = "Documentos";
                    link2.Component = gridExport2;

                    PrintableComponentLinkBase link3 = new PrintableComponentLinkBase(ps);
                    link3.PaperName = "EstadosCuenta";
                    link3.Component = gridExport3;

                    PrintableComponentLinkBase link4 = new PrintableComponentLinkBase(ps);
                    link4.PaperName = "EdoCuentaAnalizar";
                    link4.Component = gridExport4;

                    PrintableComponentLinkBase link5 = new PrintableComponentLinkBase(ps);
                    link5.PaperName = "CuentasPorCobrar";
                    link5.Component = gridExport5;

                    PrintableComponentLinkBase link6 = new PrintableComponentLinkBase(ps);
                    link6.PaperName = "DMG";
                    link6.Component = gridExport6;

                    PrintableComponentLinkBase link8 = new PrintableComponentLinkBase(ps);
                    link8.PaperName = "Resumen";
                    link8.Component = gridExport8;

                    CompositeLinkBase compositeLink = new CompositeLinkBase(ps);
                    compositeLink.Links.AddRange(new object[] { link1, link2, link3, link4, link5, link6, link8 });

                    compositeLink.CreatePageForEachLink();

                    string mIniciales = "";
                    if (cbEmpresaLV.SelectedItem.ToString().ToLower() != "prodmult")
                    {
                        if (cbEmpresaLV.SelectedItem.ToString().ToLower() == "emprconc")
                        {
                            mIniciales = "EC";
                        }
                        else
                        {
                            mIniciales = cbEmpresaLV.SelectedItem.ToString().ToUpper().Substring(0, 2);
                        }
                    }

                    using (MemoryStream stream = new MemoryStream())
                    {
                        XlsxExportOptions options = new XlsxExportOptions();
                        options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                        compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                        Response.Clear();
                        Response.Buffer = false;
                        Response.AppendHeader("Content-Type", "application/xlsx");
                        Response.AppendHeader("Content-Transfer-Encoding", "binary");
                        Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=AntiguedadSaldos{0}_{1}{2}.xlsx", mIniciales, mMes, mAnio));
                        Response.BinaryWrite(stream.ToArray());
                        Response.End();
                    }
                    ps.Dispose();

                    gridLibroVentas8.Settings.ShowColumnHeaders = true;
                }



                if (e.ExportType == DemoExportFormat.CargarAntSaldos)
                {
                    string mFile = "";
                    try
                    {
                        mFile = uploadFile.UploadedFiles[0].FileName;
                    }
                    catch
                    {
                        mFile = "";
                    }

                    if (mFile.Trim().Length == 0)
                    {
                        lbErrorAntiguedad.Text = "No se ha cargado ningún archivo";
                        return;
                    }

                    string mArchivo = @"C:\reportes\Contabilidad\CuentasPorCobrar_" + mFechaHoyString + ".xlsx";

                    try
                    {
                        File.Delete(mArchivo);
                    }
                    catch
                    {
                        //Nothing
                    }

                    uploadFile.UploadedFiles[0].SaveAs(mArchivo);

                    System.Data.OleDb.OleDbConnection mConexion = new System.Data.OleDb.OleDbConnection(string.Format("Provider=Microsoft.ACE.OLEDB.12.0; data source={0}; Extended Properties=Excel 8.0;", mArchivo));
                    System.Data.OleDb.OleDbDataAdapter daCuentasPorCobrar = new System.Data.OleDb.OleDbDataAdapter("SELECT [Cliente Debito] AS ClienteDebito, [Cliente Credito] AS ClienteCredito, Fecha, Monto FROM [CuentasPorCobrar$]", mConexion);

                    DataTable dtCuentasPorCobrar = new DataTable("CuentasPorCobrar");

                    try
                    {
                        daCuentasPorCobrar.Fill(dtCuentasPorCobrar);
                    }
                    catch
                    {
                        lbErrorAntiguedad.Text = "El archivo no contiene una hoja llamada CuentasPorCobrar o bien las columnas no son las correctas, por favor utilice la plantilla";
                        return;
                    }

                    ds.Tables.Add(dtCuentasPorCobrar);

                    if (ds.Tables["CuentasPorCobrar"].Rows.Count == 0)
                    {
                        lbErrorAntiguedad.Text = "No se encontraron documentos de cuentas por cobrar";
                        return;
                    }

                    if (!ws.CargarCuentasPorCobrar(ds, ref mMensaje, Convert.ToString(Session["usuario"])))
                    {
                        lbErrorAntiguedad.Text = mMensaje;
                        return;
                    }

                    lbInfoAntiguedad.Text = mMensaje;
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorLibroVentas.Text = string.Format("Error. {0} {1}", ex.Message, m);
                lbErrorAntiguedad.Text = string.Format("Error. {0} {1}", ex.Message, m);
            }
        }

        void ps_XlSheetCreated(object sender, XlSheetCreatedEventArgs e)
        {
            if (e.Index == 0) e.SheetName = "NotasCredito";
            if (e.Index == 1) e.SheetName = "NotasAbono";
            if (e.Index == 2) e.SheetName = "Consecutivos";
        }

        void psAnuladas_XlSheetCreated(object sender, XlSheetCreatedEventArgs e)
        {
            if (e.Index == 0) e.SheetName = "FacturasAnuladas";
            if (e.Index == 1) e.SheetName = "Consecutivos";
        }

        void psLV_XlSheetCreated(object sender, XlSheetCreatedEventArgs e)
        {
            if (e.Index == 0) e.SheetName = "LibroVentas";
            if (e.Index == 1) e.SheetName = "Documentos";
            if (e.Index == 2) e.SheetName = "DMG";
            if (e.Index == 3) e.SheetName = "GFace";
            if (e.Index == 4) e.SheetName = "GFaceDetalle";
            if (e.Index == 5) e.SheetName = "GFaceCuadre";
            if (e.Index == 6) e.SheetName = "DocumentosCuadre";
            if (e.Index == 7) e.SheetName = "Resumen";
        }

        void psLC_XlSheetCreated(object sender, XlSheetCreatedEventArgs e)
        {
            if (e.Index == 0) e.SheetName = "LibroCompras";
            if (e.Index == 1) e.SheetName = "Documentos";
            if (e.Index == 2) e.SheetName = "DMG";
            if (e.Index == 3) e.SheetName = "DMGVentas";
            if (e.Index == 4) e.SheetName = "Resumen";
        }


        void psConsultas_XlSheetCreated(object sender, XlSheetCreatedEventArgs e)
        {
            if (e.Index == 0)
                e.SheetName = "Solicitudes Rechazadas";
        }

        void psLCMod_XlSheetCreated(object sender, XlSheetCreatedEventArgs e)
        {
            if (e.Index == 0) e.SheetName = "Borrar";
            if (e.Index == 1) e.SheetName = "BienServicio";
            if (e.Index == 2) e.SheetName = "PC";
            if (e.Index == 3) e.SheetName = "FE";
            if (e.Index == 4) e.SheetName = "Editar";
        }


        void psLI_XlSheetCreated(object sender, XlSheetCreatedEventArgs e)
        {
            if (e.Index == 0) e.SheetName = "LibroInventario";
            if (e.Index == 1) e.SheetName = "LibroInventarioXBodega";
            if (e.Index == 2) e.SheetName = "PorDespachar";
            if (e.Index == 3) e.SheetName = "BalanceDeComprobacion";
            if (e.Index == 4) e.SheetName = "Resumen";
        }

        void psEF_XlSheetCreated(object sender, XlSheetCreatedEventArgs e)
        {
            if (e.Index == 0) e.SheetName = "ER";
            if (e.Index == 1) e.SheetName = "ER_Acumulado";
            if (e.Index == 2) e.SheetName = "ER_Detallado";
            if (e.Index == 3) e.SheetName = "ER_Acumulado_Detallado";
            if (e.Index == 4) e.SheetName = "BG";
            if (e.Index == 5) e.SheetName = "BG_Detallado";
            if (e.Index == 6) e.SheetName = "CentroCosto";
            if (e.Index == 7) e.SheetName = "CentroCostoAcum";
            if (e.Index == 8) e.SheetName = "CentroCostoAnioAnterior";
            if (e.Index == 9) e.SheetName = "CentroCostoAnioAnteriorAcum";
            if (e.Index == 10) e.SheetName = "Info";
        }

        void psGFace_XlSheetCreated(object sender, XlSheetCreatedEventArgs e)
        {
            if (e.Index == 0) e.SheetName = "Encabezado";
            if (e.Index == 1) e.SheetName = "Detalle";
        }

        void psEx_XlSheetCreated(object sender, XlSheetCreatedEventArgs e)
        {
            if (e.Index == 0) e.SheetName = "Exenciones";
        }

        void psDMG_XlSheetCreated(object sender, XlSheetCreatedEventArgs e)
        {
            if (e.Index == 0) e.SheetName = "DMG";
        }

        void psAntSaldos_XlSheetCreated(object sender, XlSheetCreatedEventArgs e)
        {
            if (e.Index == 0) e.SheetName = "AntiguedadSaldos";
            if (e.Index == 1) e.SheetName = "Documentos";
            if (e.Index == 2) e.SheetName = "EstadosCuenta";
            if (e.Index == 3) e.SheetName = "EdoCuentaAnalizar";
            if (e.Index == 4) e.SheetName = "CuentasPorCobrar";
            if (e.Index == 5) e.SheetName = "DMG";
            if (e.Index == 6) e.SheetName = "Resumen";
        }

        protected void gridExport_RenderBrick(object sender, ASPxGridViewExportRenderingEventArgs e)
        {
            try
            {
                if (e.RowType != GridViewRowType.Header) e.BrickStyle.BorderWidth = 0;

                GridViewDataColumn dc = (GridViewDataColumn)e.Column;

                if (e.RowType == GridViewRowType.Data && e.TextValue != null)
                {
                    double mNumericValue = 0;
                    if (Double.TryParse(e.TextValue.ToString(), out mNumericValue))
                    {
                        if (dc.FieldName != "Pendiente" && dc.FieldName != "Despachado" && dc.FieldName != "Facturado" && dc.FieldName != "CantidadPorDespachar" && dc.FieldName != "CantidadEnBodega" && dc.FieldName != "Tipo" && dc.FieldName != "Linea" && dc.FieldName != "NumeroDocumento" && dc.FieldName != "Cantidad" && dc.FieldName != "Anio" && dc.FieldName != "Mes" && dc.FieldName != "Nit" && dc.FieldName != "NIT" && dc.FieldName != "No. de registro de la cédula, DPI o Pasaporte" && dc.FieldName != "Número del documento" && dc.FieldName != "NIT del cliente/proveedor" && dc.FieldName != "Numero" && dc.FieldName != "Serie" && dc.FieldName != "Serie del documento" && dc.FieldName != "Establecimiento" && dc.FieldName != "Vale" && dc.FieldName != "Cliente" && dc.FieldName != "Coincidencia1" && dc.FieldName != "Coincidencia2" && dc.FieldName != "Coincidencia3" && dc.FieldName != "Aplicacion1" && dc.FieldName != "Aplicacion2" && dc.FieldName != "Aplicacion3" && dc.FieldName != "Consecutivo" && dc.FieldName != "ValeCH")
                        {
                            e.XlsxFormatString = "#,##0.00;(#,##0.00)";
                            e.TextValue = mNumericValue;
                        }
                    }
                }

                if (e.RowType == GridViewRowType.Data && dc != null)
                {
                    if (dc.FieldName == "Valor" || dc.FieldName == "Monto" || dc.FieldName == "Saldo" || dc.FieldName == "Debitos" || dc.FieldName == "Creditos" || dc.FieldName == "ValorNeto" || dc.FieldName == "IVA" || dc.FieldName == "ValorBruto" || dc.FieldName == "SubTotal" || dc.FieldName == "Total" || dc.FieldName == "SaldoInicial" || dc.FieldName == "SaldoFinal")
                        e.TextValueFormatString = "#,##0.00;(#,##0.00)";
                }
            }
            catch
            {
                //Nothing
            }
        }

        protected void gridExport8_RenderBrick(object sender, ASPxGridViewExportRenderingEventArgs e)
        {
            try
            {
                if (e.RowType != GridViewRowType.Header) e.BrickStyle.BorderWidth = 0;

                GridViewDataColumn dc = (GridViewDataColumn)e.Column;

                if (e.RowType == GridViewRowType.Data && dc != null)
                {
                    if (dc.FieldName == "Valor" || dc.FieldName == "Monto" || dc.FieldName == "Saldo" || dc.FieldName == "Debitos" || dc.FieldName == "Creditos" || dc.FieldName == "ValorNeto" || dc.FieldName == "IVA" || dc.FieldName == "ValorBruto" || dc.FieldName == "Subtotal" || dc.FieldName == "Total" || dc.FieldName == "SaldoInicial" || dc.FieldName == "SaldoFinal")
                        e.TextValueFormatString = "#,##0.00;(#,##0.00)";
                }

                if (Convert.ToString(e.GetValue("Referencia")).ToLower().Contains("total") || Convert.ToString(e.GetValue("Referencia")).ToLower().Contains("diferencia") || Convert.ToString(e.GetValue("Referencia")).ToLower().Contains("cuadre") || Convert.ToString(e.GetValue("Referencia")).ToLower().Contains("al ") || Convert.ToString(e.GetValue("Referencia")).ToLower().Contains("totales") || Convert.ToString(e.GetValue("Referencia")).ToLower().Contains("totales:") || Convert.ToString(e.GetValue("Referencia")).ToLower().Contains("Black"))
                {
                    e.BrickStyle.ForeColor = System.Drawing.Color.White;
                    e.BrickStyle.BackColor = System.Drawing.Color.Black;
                }

                if (e.RowType == GridViewRowType.Data && e.TextValue != null)
                {
                    if (e.TextValue.ToString().Contains("N_"))
                    {
                        e.TextValue = e.TextValue.ToString().Replace("N_", "");
                        e.BrickStyle.BackColor = System.Drawing.Color.Black;
                        e.BrickStyle.ForeColor = System.Drawing.Color.White;
                        e.BrickStyle.Font = new System.Drawing.Font("Times New Roman", 10, System.Drawing.FontStyle.Bold);
                    }

                    double mNumericValue = 0;
                    if (Double.TryParse(e.TextValue.ToString(), out mNumericValue))
                    {
                        if (dc.FieldName != "Pendiente" && dc.FieldName != "Despachado" && dc.FieldName != "Facturado" && dc.FieldName != "CantidadPorDespachar" && dc.FieldName != "CantidadEnBodega" && dc.FieldName != "Tipo" && dc.FieldName != "Linea" && dc.FieldName != "NumeroDocumento" && dc.FieldName != "Cantidad" && dc.FieldName != "Anio" && dc.FieldName != "Mes" && dc.FieldName != "Nit" && dc.FieldName != "NIT" && dc.FieldName != "No. de registro de la cédula, DPI o Pasaporte" && dc.FieldName != "Número del documento" && dc.FieldName != "NIT del cliente/proveedor" && dc.FieldName != "Numero" && dc.FieldName != "Serie" && dc.FieldName != "Serie del documento" && dc.FieldName != "Establecimiento" && dc.FieldName != "Vale" && dc.FieldName != "Consecutivo" && dc.FieldName != "ValeCH")
                        {
                            e.XlsxFormatString = "#,##0.00;(#,##0.00)";
                            e.TextValue = mNumericValue;
                        }
                    }

                }
            }
            catch
            {
                //Nothing
            }
        }

        protected void btnBienServicioLV_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoLibroVentas.Text = "";
                lbErrorLibroVentas.Text = "";

                if (txtFacturaLV.Text.Trim().Length == 0) return;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                ws.Timeout = 999999999;
                string mMensaje = "";

                if (!ws.FacturaBienServicio(ref mMensaje, txtFacturaLV.Text, cbEmpresaLV.SelectedItem.ToString()))
                {
                    lbErrorLibroVentas.Text = mMensaje;
                    return;
                }

                txtFacturaLV.Text = "";
                cbEmpresaLV.SelectedIndex = 0;
                txtFacturaLV.Focus();
                lbInfoLibroVentas.Text = mMensaje;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorLibroVentas.Text = string.Format("Error bien servicio. {0} {1}", ex.Message, m);
            }
        }

        protected void btnCuentaInicial_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoLibroVentas.Text = "";
                lbErrorLibroVentas.Text = "";

                txtCuenta.Text = "I";
                pcCuentas.ShowOnPageLoad = true;

                //var btn = sender as ASPxButton;
                //var container = btn.NamingContainer as GridViewDataItemTemplateContainer;
                //btn.ClientSideEvents.Click = string.Format("function(s,e){{ onCuentaClick(s,e,{0}); }}", txtCuenta.Text);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorLibroVentas.Text = string.Format("Error cuenta inicial. {0} {1}", ex.Message, m);
            }
        }

        protected void btnCuentaFinal_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoLibroVentas.Text = "";
                lbErrorLibroVentas.Text = "";

                txtCuenta.Text = "F";
                pcCuentas.ShowOnPageLoad = true;

                //var btn = sender as ASPxButton;
                //var container = btn.NamingContainer as GridViewDataItemTemplateContainer;
                //btn.ClientSideEvents.Click = string.Format("function(s,e){{ onCuentaClick(s,e,{0}); }}", txtCuenta.Text);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorLibroVentas.Text = string.Format("Error cuenta final. {0} {1}", ex.Message, m);
            }
        }

        protected void btnCuentaBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoLibroVentas.Text = "";
                lbErrorLibroVentas.Text = "";

                var btn = (sender as ASPxButton);
                var nc = btn.NamingContainer as GridViewDataItemTemplateContainer;

                string mCuenta = DataBinder.Eval(nc.DataItem, "Cuenta").ToString();

                if (txtCuenta.Text == "I")
                {
                    txtCuentaInicial.Text = mCuenta;
                    txtCuentaFinal.Focus();
                }
                else
                {
                    txtCuentaFinal.Text = mCuenta;
                    btnDMG.Focus();
                }

                pcCuentas.ShowOnPageLoad = false;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorLibroVentas.Text = string.Format("Error al seleccionar la cuenta. {0} {1}", ex.Message, m);
            }
        }

        protected void btnClienteBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoAntiguedad.Text = "";
                lbErrorAntiguedad.Text = "";

                var btn = (sender as ASPxButton);
                var nc = btn.NamingContainer as GridViewDataItemTemplateContainer;

                string mCliente = DataBinder.Eval(nc.DataItem, "Cliente").ToString();

                txtClienteAntSaldos.Text = mCliente;
                pcClientes.ShowOnPageLoad = false;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorAntiguedad.Text = string.Format("Error al seleccionar el cliente. {0} {1}", ex.Message, m);
            }
        }

        protected void btnCliente_Click(object sender, EventArgs e)
        {
            if (txtClienteAntSaldos.Text.Trim().Length == 0) return;

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            DataSet dsClientes = new DataSet(); string mMensaje = "";
            dsClientes = ws.Clientes(ref mMensaje, cbEmpresaLV.SelectedItem.ToString(), txtClienteAntSaldos.Text.Trim().ToUpper());

            gridClientesBuscar.DataSource = dsClientes;
            gridClientesBuscar.DataMember = "clientes";
            gridClientesBuscar.DataBind();
            gridClientesBuscar.FocusedRowIndex = -1;
            gridClientesBuscar.SettingsPager.PageSize = 35;

            pcClientes.ShowOnPageLoad = true;
        }

        protected void btnDMG_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoDMG.Text = "";
                lbErrorDMG.Text = "";

                int mAnio = 0; int mNumeroMes = 0;

                try
                {
                    mAnio = Convert.ToInt32(txtAnioLV.Text);
                }
                catch
                {
                    lbErrorDMG.Text = "Ingrese un año válido";
                    return;
                }

                if (mAnio < 2010)
                {
                    lbErrorDMG.Text = "Ingrese un año válido";
                    return;
                }


                try
                {
                    mNumeroMes = Convert.ToInt32(cbMesLV.SelectedItem.Value);
                }
                catch
                {
                    lbErrorDMG.Text = "Debe seleccionar un mes";
                    cbMesLV.Focus();
                    return;
                }

                if (mNumeroMes < 0 || mNumeroMes > 12)
                {
                    lbErrorDMG.Text = "Debe seleccionar un mes";
                    cbMesLV.Focus();
                    return;
                }

                string mMes = "Enero"; DateTime mFecha = new DateTime(mAnio, mNumeroMes, 1);

                if (mFecha.Month == 2) mMes = "Febrero";
                if (mFecha.Month == 3) mMes = "Marzo";
                if (mFecha.Month == 4) mMes = "Abril";
                if (mFecha.Month == 5) mMes = "Mayo";
                if (mFecha.Month == 6) mMes = "Junio";
                if (mFecha.Month == 7) mMes = "Julio";
                if (mFecha.Month == 8) mMes = "Agosto";
                if (mFecha.Month == 9) mMes = "Septiembre";
                if (mFecha.Month == 10) mMes = "Octubre";
                if (mFecha.Month == 11) mMes = "Noviembre";
                if (mFecha.Month == 12) mMes = "Diciembre";


                string mCuentaInicial = ""; string mCuentaFinal = ""; string mEmpresa = "";

                if (txtCuentaInicial.Text.Trim().Length == 0) return;

                mCuentaInicial = txtCuentaInicial.Text.Trim();
                mCuentaFinal = txtCuentaFinal.Text.Trim();
                mEmpresa = cbEmpresaLV.SelectedItem.ToString();

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                ws.Timeout = 999999999;
                string mMensaje = ""; DataSet ds = new DataSet();

                ds = ws.DMG(ref mMensaje, txtAnioLV.Text, Convert.ToString(cbMesLV.SelectedItem.Value), mEmpresa, mCuentaInicial, mCuentaFinal);

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorDMG.Text = mMensaje;
                    return;
                }

                gridLibroVentas1.DataSource = ds;
                gridLibroVentas1.DataMember = "DMG";
                gridLibroVentas1.DataBind();
                gridLibroVentas1.FocusedRowIndex = -1;
                gridLibroVentas1.SettingsPager.PageSize = 30;

                PrintingSystemBase ps = new PrintingSystemBase();
                ps.XlSheetCreated += psDMG_XlSheetCreated;

                PrintableComponentLinkBase link1 = new PrintableComponentLinkBase(ps);
                link1.PaperName = "DMG";
                link1.Component = gridExport1;

                CompositeLinkBase compositeLink = new CompositeLinkBase(ps);
                compositeLink.Links.AddRange(new object[] { link1 });

                compositeLink.CreatePageForEachLink();

                using (MemoryStream stream = new MemoryStream())
                {
                    XlsxExportOptions options = new XlsxExportOptions();
                    options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                    compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                    Response.Clear();
                    Response.Buffer = false;
                    Response.AppendHeader("Content-Type", "application/xlsx");
                    Response.AppendHeader("Content-Transfer-Encoding", "binary");
                    Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=DMG_{0}_{1}_{2}.xlsx", cbEmpresaLV.SelectedItem.ToString(), mMes, mAnio.ToString()));
                    Response.BinaryWrite(stream.ToArray());
                    Response.End();
                }
                ps.Dispose();

                lbInfoDMG.Text = "";
                lbErrorDMG.Text = "";
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDMG.Text = string.Format("Error DMG. {0} {1}", ex.Message, m);
            }
        }

        /// <summary>
        /// PR:
        /// Acción para consultar lo referente a los desembolsos de interconsumo, en febrero 2018 se agregaron 2 reportes nuevos
        /// quedando:
        /// 1)Consulta de Desembolsos realizados
        /// 2)Consulta de desembolsos rechazados (solicitudes rechazadas)
        /// 3)Consulta de desembolsos pendientes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lkSeleccionarDesembolsos_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbFinanciera.SelectedIndex == -1)
                {
                    lbErrorFinanciera.Text = "Por favor seleccione una financiera";
                    cmbFinanciera.Focus();
                    return;
                }
                gridConsultas.Visible = false;
                gridFinanciera.Visible = false;
                lbInfoFinanciera.Text = "";
                lblTotalItems.Text = "";
                lbErrorFinanciera.Text = "";
                int nIndexOpcion = 0;
                nIndexOpcion = rdlRealizados.Checked ? 0 : (rdlPendientes.Checked ? 1 : 2);
                switch (nIndexOpcion)
                {
                    case 0:
                        {
                            #region "Consulta Desembolsos"
                            DateTime mFecha = Convert.ToDateTime(fechaDesembolsos.Value);
                            btCargarDesembolso.Enabled = true;
                            btImprimirDesembolsos.Enabled = true;
                            try
                            {
                                var mDesembolso = WebRestClient.ConsultarDesembolsosRealizados(mFecha, Session["UrlRestServices"].ToString(), cmbFinanciera.SelectedItem.Value.ToString());
                                List<MF_Clases.Clases.Desembolso> mDesembolsos = new List<Clases.Desembolso>();
                                Clases.LoteDesombolsosFinanciera mLoteDesembolsos = new Clases.LoteDesombolsosFinanciera();
                                if (mDesembolso != null)
                                {

                                    mDesembolsos = mDesembolso.ToList();
                                    mLoteDesembolsos = new Clases.LoteDesombolsosFinanciera(mDesembolsos.ToList<MF_Clases.Clases.Desembolso>());
                                    mLoteDesembolsos.FechaDesembolsos = Convert.ToDateTime(fechaDesembolsos.Value);
                                    mLoteDesembolsos.Financiera = int.Parse(cmbFinanciera.SelectedItem.Value.ToString());
                                    DataSet ds = new DataSet();
                                    DataTable dt = new DataTable("desembolsos");
                                    using (var reader = ObjectReader.Create(mLoteDesembolsos.Desembolsos))
                                    {
                                        dt.Load(reader);
                                    }

                                    ds.Tables.Add(dt);
                                    gridFinanciera.KeyFieldName = "Factura";
                                    gridFinanciera.DataSource = ds;
                                    gridFinanciera.DataMember = "Desembolsos";
                                    gridFinanciera.DataBind();
                                    gridFinanciera.FocusedRowIndex = -1;
                                    gridFinanciera.SettingsPager.PageSize = 200;



                                    Session["dsDesembolsos"] = ds;
                                    Session["DesembolsosInterconsumo"] = mLoteDesembolsos;

                                    if (mDesembolsos.Count() > 0)
                                    {
                                        gridFinanciera.Visible = true;
                                        lblTotalItems.Text = gridFinanciera.VisibleRowCount.ToString() + " Desembolsos realizados";
                                    }


                                }
                                if (mDesembolso == null || mDesembolsos.Count() == 0)

                                {
                                    ///Si no hay desembolsos en la base de datos, consultar a INTERCONSUMO
                                    #region "desembolsos realizados"
                                    if (cmbFinanciera.SelectedItem.Text.Equals("INTERCONSUMO") || cmbFinanciera.SelectedItem.Text.Equals("CREDIPLUS"))
                                    {
                                        WebRequest request = WebRequest.Create(string.Format("{0}/DesembolsosInterconsumo/?anio={1}&mes={2}&dia={3}&Financiera={4}", Convert.ToString(Session["UrlRestServices"]), mFecha.Year, mFecha.Month, mFecha.Day, cmbFinanciera.SelectedItem.Value));

                                        request.Method = "GET";
                                        request.ContentType = "application/json";

                                        var response = (HttpWebResponse)request.GetResponse();
                                        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                                        mLoteDesembolsos = JsonConvert.DeserializeObject<Clases.LoteDesombolsosFinanciera>(responseString);
                                        mLoteDesembolsos.FechaDesembolsos = Convert.ToDateTime(fechaDesembolsos.Value);
                                        mLoteDesembolsos.FechaDesembolsos = Convert.ToDateTime(fechaDesembolsos.Value);

                                        DataSet ds = new DataSet();
                                        DataTable dt = new DataTable("desembolsos");

                                        ds = new DataSet();
                                        dt = new DataTable("desembolsos");
                                        using (var reader = ObjectReader.Create(mLoteDesembolsos.Desembolsos))
                                        {
                                            dt.Load(reader);
                                        }

                                        ds.Tables.Add(dt);
                                        gridFinanciera.KeyFieldName = "Prestamo";
                                        gridFinanciera.DataSource = ds;
                                        gridFinanciera.DataMember = "Desembolsos";
                                        gridFinanciera.DataBind();
                                        gridFinanciera.FocusedRowIndex = -1;
                                        gridFinanciera.SettingsPager.PageSize = 200;

                                        if (!mLoteDesembolsos.exito)
                                        {
                                            lbErrorFinanciera.Text = mLoteDesembolsos.mensaje;
                                            gridFinanciera.Visible = false;
                                            return;
                                        }

                                        Session["dsDesembolsos"] = ds;
                                        Session["DesembolsosInterconsumo"] = mLoteDesembolsos;
                                        gridFinanciera.Visible = true;
                                        lblTotalItems.Text = gridFinanciera.VisibleRowCount.ToString() + " Desembolsos";
                                    }

                                    #endregion

                                }


                            }
                            catch
                            {

                            }
                            #endregion
                        }
                        break;
                    case 1:
                        {
                            #region "solicitudes enviadas o pendientes de desembolso"
                            btCargarDesembolso.Enabled = false;
                            btImprimirDesembolsos.Enabled = false;
                            gridFinanciera.Visible = false;
                            gridConsultas.Visible = false;
                            try
                            {
                                List<MF_Clases.Clases.SolicitudInterconsumo> mPendientes = new List<Clases.SolicitudInterconsumo>();
                                if (cmbOptSolEnviadas.SelectedIndex < 2)
                                    mPendientes = WebRestClient.ConsultarDesembolsosPendientes(Session["UrlRestServices"].ToString(), cmbOptSolEnviadas.SelectedIndex == 0 ? true : false, int.Parse(cmbFinanciera.SelectedItem.Value.ToString())).ToList();
                                else
                                    mPendientes = WebRestClient.ConsultarDesembolsosPendientesTiendas(Session["UrlRestServices"].ToString(), int.Parse(cmbFinanciera.SelectedItem.Value.ToString())).ToList();

                                if (mPendientes != null)
                                {

                                    DataSet ds = new DataSet();
                                    DataTable dt = new DataTable("Pendientes");
                                    dt.Columns.Add("#");
                                    dt.Columns.Add("COBRADOR");
                                    dt.Columns.Add("FECHA_FACTURA");
                                    dt.Columns.Add("NUMERO_DOCUMENTO");
                                    dt.Columns.Add("SOLICITUD");
                                    dt.Columns.Add("FECHA_SOLICITUD");
                                    dt.Columns.Add("CLIENTE");
                                    dt.Columns.Add("NOMBRE_CLIENTE");
                                    dt.Columns.Add("VALOR_FACTURA");
                                    dt.Columns.Add("VALOR_ENGANCHE");
                                    dt.Columns.Add("VALOR_SALDO");
                                    dt.Columns.Add("FINANCIERA");
                                    dt.Columns.Add("OBSERVACIONES");
                                    dt.Columns[0].AutoIncrement = true;
                                    using (var reader = ObjectReader.Create(mPendientes))
                                    {
                                        dt.Load(reader);

                                    }

                                    ds.Tables.Add(dt);
                                    gridConsultas.SettingsPager.PageSize = 500;
                                    gridConsultas.DataSource = ds;
                                    gridConsultas.DataMember = "Pendientes";
                                    gridConsultas.DataBind();
                                    //ocultar la fecha de envío para dar espacio al saldo pendiente
                                    if (cmbOptSolEnviadas.SelectedIndex < 2)
                                    {
                                        gridConsultas.Columns["VALOR_SALDO"].Visible = false;
                                        gridConsultas.Columns["FECHA_SOLICITUD"].Visible = true;
                                    }
                                    else
                                    {
                                        gridConsultas.Columns["FECHA_SOLICITUD"].Visible = false;
                                        gridConsultas.Columns["VALOR_SALDO"].Visible = true;
                                    }

                                    gridConsultas.Columns["FECHA_RECHAZO_SOLICITUD"].Visible = false;
                                    gridConsultas.Columns["FINANCIERA"].Visible = false;
                                    gridConsultas.Columns["NOMBRE_FINANCIERA"].Visible = true;
                                    gridConsultas.Columns["OBSERVACIONES"].Visible = false;
                                    Session["dsPendientes"] = ds;
                                    gridConsultas.Visible = true;
                                    gridFinanciera.Visible = false;
                                }
                                if (mPendientes.Count() > 0)
                                {
                                    gridFinanciera.Visible = false;
                                    gridConsultas.Visible = true;
                                    lblTotalItems.Text = mPendientes.Count().ToString() + " Desembolsos pendientes";
                                }
                                else
                                {
                                    gridFinanciera.Visible = false;
                                    gridConsultas.Visible = false;
                                    lblTotalItems.Text = "";
                                }
                            }
                            catch (Exception ex)
                            {
                                string m = "";
                                try
                                {
                                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                                }
                                catch
                                {
                                    try
                                    {
                                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                                    }
                                    catch
                                    {
                                        //Nothing
                                    }
                                }

                                lbErrorFinanciera.Text = string.Format("Error solicitudes rechazadas. {0} {1}", ex.Message, m);
                            }


                            lblTotalItems.Text = gridConsultas.VisibleRowCount.ToString() + " Desembolsos";
                            #endregion
                        }
                        break;
                    case 2:
                        {
                            #region "desembolsos rechazados"
                            try
                            {
                                btCargarDesembolso.Enabled = false;
                                btImprimirDesembolsos.Enabled = false;
                                int nFinanciera = int.Parse(cmbFinanciera.SelectedItem.Value.ToString());
                                List<MF_Clases.Clases.SolicitudInterconsumo> mRechazos = WebRestClient.ConsultarSolicitudesRechazadas(Session["UrlRestServices"].ToString(), nFinanciera).ToList();
                                if (mRechazos != null)
                                {

                                    gridFinanciera.Visible = false;
                                    gridConsultas.Visible = true;
                                    DataSet ds = new DataSet();
                                    DataTable dt = new DataTable("rechazos");
                                    dt.Columns.Add("#");
                                    dt.Columns.Add("COBRADOR");
                                    dt.Columns.Add("FECHA_FACTURA");
                                    dt.Columns.Add("NUMERO_DOCUMENTO");
                                    dt.Columns.Add("SOLICITUD");
                                    dt.Columns.Add("FECHA_SOLICITUD");
                                    dt.Columns.Add("CLIENTE");
                                    dt.Columns.Add("NOMBRE_CLIENTE");
                                    dt.Columns.Add("VALOR_FACTURA");
                                    dt.Columns.Add("VALOR_ENGANCHE");
                                    dt.Columns.Add("FECHA_RECHAZO_SOLICITUD");
                                    dt.Columns.Add("OBSERVACIONES");
                                    dt.Columns.Add("NOMBRE_FINANCIERA");
                                    dt.Columns[0].AutoIncrement = true;
                                    using (var reader = ObjectReader.Create(mRechazos))
                                    {
                                        dt.Load(reader);
                                    }

                                    ds.Tables.Add(dt);
                                    gridConsultas.DataSource = ds;
                                    gridConsultas.DataMember = "Rechazos";
                                    gridConsultas.DataBind();
                                    Session["dsRechazos"] = ds;
                                    gridConsultas.Visible = true;
                                    gridFinanciera.Visible = false;
                                    gridConsultas.Columns["FECHA_RECHAZO_SOLICITUD"].Visible = true;
                                    gridConsultas.Columns["OBSERVACIONES"].Visible = true;
                                    gridConsultas.Columns["FINANCIERA"].Visible = false;
                                    gridConsultas.Columns["NOMBRE_FINANCIERA"].Visible = true;
                                }
                                if (mRechazos.Count() > 0)
                                {
                                    lblTotalItems.Text = mRechazos.Count() + " Desembolsos rechazados";
                                }
                                else
                                {
                                    gridConsultas.Visible = false;
                                }
                            }
                            catch (Exception ex)
                            {
                                string m = "";
                                try
                                {
                                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                                }
                                catch
                                {
                                    try
                                    {
                                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                                    }
                                    catch
                                    {
                                        //Nothing
                                    }
                                }

                                lbErrorFinanciera.Text = string.Format("Error solicitudes rechazadas. {0} {1}", ex.Message, m);
                            }
                            #endregion

                        }
                        break;
                }
                if (gridFinanciera.VisibleRowCount == 0 && rdlRealizados.Checked)
                    lbErrorFinanciera.Text = "No se encontraron registros.";
                else if (gridConsultas.VisibleRowCount == 0 && rdlRealizados.Checked == false)
                    lbErrorFinanciera.Text = "No se encontraron regitros.";
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorFinanciera.Text = string.Format("Error Desembolsos. {0} {1}", ex.Message, m);
            }
        }

        protected void gridInterconsumo_RenderBrick(object sender, ASPxGridViewExportRenderingEventArgs e)
        {
            try
            {

            }
            catch
            {
                //Nothing
            }
        }


        protected void lkCargarDesembolsos_Click(object sender, EventArgs e)
        {
            try
            {

                lbInfoFinanciera.Text = "";
                lbErrorFinanciera.Text = "";
                string UrlFinanciera = string.Empty;
                Clases.LoteDesombolsosFinanciera mDesembolsosFinanciera = (Clases.LoteDesombolsosFinanciera)Session["DesembolsosInterconsumo"];

                if (mDesembolsosFinanciera == null)
                {
                    lbErrorFinanciera.Text = "No existen desembolsos por cargar al sistema.";
                    return;
                }
                if (!cmbFinanciera.SelectedItem.Text.Equals("INTERCONSUMO") && !cmbFinanciera.SelectedItem.Text.Equals("CREDIPLUS"))
                {
                    lbErrorFinanciera.Text = "La financiera " + cmbFinanciera.SelectedItem.Text + " no posee el proceso de carga de desembolsos automáticos.";
                    return;
                }

                mDesembolsosFinanciera.Operacion = Const.ExpedienteFinanciera.DESEMBOLSADO;
                mDesembolsosFinanciera.Usuario = base.GetUsuario();
                mDesembolsosFinanciera.usuario = base.GetUsuario();
                mDesembolsosFinanciera.FechaRigeDesembolso = Convert.ToDateTime(fechaRigeDesembolso.Value);
                mDesembolsosFinanciera.Financiera = int.Parse(cmbFinanciera.SelectedItem.Value.ToString());
                string json = JsonConvert.SerializeObject(mDesembolsosFinanciera);
                byte[] data = Encoding.ASCII.GetBytes(json);


                WebRequest request = WebRequest.Create(string.Format("{0}/{1}/", Convert.ToString(Session["UrlRestServices"]), "DesembolsosInterconsumo"));



                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                //Respuesta 
                Respuesta mRetorna = new Respuesta();
                mRetorna = JsonConvert.DeserializeObject<Respuesta>(responseString);

                if (!mRetorna.Exito)
                {
                    lbErrorFinanciera.Text = mRetorna.Mensaje;
                    return;
                }
                else
                {
                    lbInfoFinanciera.Text = mRetorna.Mensaje;
                }

                //string mTipoReporte = "DESEMBOLSOS_CARGADOS";

                if (EnviarEmailReporteSaldos("DESEMBOLSOS_CARGADOS", cmbFinanciera.SelectedItem.Value.ToString(), cmbFinanciera.SelectedItem.Text))
                {
                    lbInfoFinanciera.Text = "Correo enviado exitosamente!";
                }

                if (EnviarEmailReporteSaldos("RESUMEN_SALDOS", cmbFinanciera.SelectedItem.Value.ToString(), cmbFinanciera.SelectedItem.Text))
                {
                    lbInfoFinanciera.Text = "Correo enviado exitosamente!";
                }
                if (EnviarEmailReporteSaldos("EXPEDIENTES_PENDIENTES", cmbFinanciera.SelectedItem.Value.ToString(), cmbFinanciera.SelectedItem.Text))
                {
                    lbInfoFinanciera.Text = "Correo enviado exitosamente!";
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorFinanciera.Text = string.Format("Error Desembolsos. {0} {1}", ex.Message, m);
            }
        }


        protected void lkExportarDesembolsos_Click(object sender, EventArgs e)
        {
            try
            {
                PrintingSystemBase ps;
                lbInfoFinanciera.Text = "";
                lbErrorFinanciera.Text = "";
                lblTotalItems.Text = "";
                int nIndexOpcion = 0;
                nIndexOpcion = rdlRealizados.Checked ? 0 : (rdlPendientes.Checked ? 1 : 2);
                switch (nIndexOpcion)
                {
                    //desembolsos realizados
                    case 0:
                        {
                            if (Session["dsDesembolsos"] == null)
                            {
                                lbErrorFinanciera.Text = "Antes debe consultar los desembolsos.";
                                return;
                            }

                            DataSet ds = new DataSet();
                            ds = (DataSet)Session["dsDesembolsos"];

                            if (ds.Tables["desembolsos"].Rows.Count == 0)
                            {
                                lbErrorFinanciera.Text = "No existen desembolsos para exportar.";
                                return;
                            }

                            gridFinanciera.DataSource = ds;
                            gridFinanciera.DataMember = "Desembolsos";
                            gridFinanciera.DataBind();
                            gridFinanciera.FocusedRowIndex = -1;
                            gridFinanciera.SettingsPager.PageSize = 200;

                            DateTime mFecha = Convert.ToDateTime(fechaDesembolsos.Value);

                            ps = new PrintingSystemBase();
                            ps.XlSheetCreated += psDesembolsos_XlSheetCreated;

                            PrintableComponentLinkBase link1 = new PrintableComponentLinkBase(ps);
                            link1.PaperName = "Desembolsos";
                            link1.Component = gridExport9;

                            CompositeLinkBase compositeLink = new CompositeLinkBase(ps);
                            compositeLink.Links.AddRange(new object[] { link1 });

                            compositeLink.CreatePageForEachLink();

                            using (MemoryStream stream = new MemoryStream())
                            {
                                XlsxExportOptions options = new XlsxExportOptions();
                                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                                Response.Clear();
                                Response.Buffer = false;
                                Response.AppendHeader("Content-Type", "application/xlsx");
                                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                                Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=Desembolsos_{0}{1}_{2}{3}_{4}.xlsx", mFecha.Day < 10 ? "0" : "", mFecha.Day.ToString(), mFecha.Month < 10 ? "0" : "", mFecha.Month.ToString(), mFecha.Year.ToString()));
                                Response.BinaryWrite(stream.ToArray());
                                Response.End();
                            }
                            ps.Dispose();

                        }
                        break;
                    //desembolsos pendientes
                    case 1:
                        #region "Desembolsos pendientes"
                        if (Session["dsPendientes"] != null)
                            gridConsultas.DataSource = Session["dsPendientes"];
                        gridConsultas.DataMember = "Pendientes";
                        gridConsultas.DataBind();
                        gridConsultas.FocusedRowIndex = -1;
                        gridConsultas.SettingsPager.PageSize = 100;
                        if (gridConsultas.VisibleRowCount > 0)
                        {
                            ps = new PrintingSystemBase();
                            ps.XlSheetCreated += psConsultas_XlSheetCreated;

                            PrintableComponentLinkBase link2 = new PrintableComponentLinkBase(ps);
                            link2.PaperName = "ReporteSolicitudesPendientes";
                            link2.Component = gridExport14;

                            CompositeLinkBase compositeLink2 = new CompositeLinkBase(ps);
                            compositeLink2.Links.AddRange(new object[] { link2 });

                            compositeLink2.CreateDocument();

                            using (MemoryStream stream = new MemoryStream())
                            {
                                CsvExportOptions options = new CsvExportOptions();
                                compositeLink2.ExportToXlsx(stream, new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG, SheetName = "Solicitudes Pendientes" });
                                Response.Clear();
                                Response.Buffer = false;
                                Response.AppendHeader("Content-Type", "application/asl");
                                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                                Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=Reporte de Desembolsos Pendientes " + (cmbOptSolEnviadas.SelectedIndex > 0 ? " con más de una semana" : "") + " al {0}.xlsx", DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year + " " + DateTime.Now.Hour + "_" + DateTime.Now.Minute));
                                Response.BinaryWrite(stream.ToArray());
                                Response.End();

                            }
                            ps.Dispose();
                        }
                        else
                        {
                            lbErrorFinanciera.Text = "No existen desembolsos para exportar.";
                            return;
                        }
                        #endregion
                        break;
                    case 2:
                        #region "Desembolsos rechazados"

                        if (Session["dsRechazos"] != null)
                            gridConsultas.DataSource = Session["dsRechazos"];
                        gridConsultas.DataMember = "Rechazos";
                        gridConsultas.DataBind();
                        gridConsultas.FocusedRowIndex = -1;
                        gridConsultas.SettingsPager.PageSize = 100;
                        if (gridConsultas.VisibleRowCount > 0)
                        {
                            ps = new PrintingSystemBase();
                            ps.XlSheetCreated += psConsultas_XlSheetCreated;

                            PrintableComponentLinkBase link3 = new PrintableComponentLinkBase(ps);
                            link3.PaperName = "ReporteSolicitudesRechazadas";
                            link3.Component = gridExport14;

                            CompositeLinkBase compositeLink3 = new CompositeLinkBase(ps);
                            compositeLink3.Links.AddRange(new object[] { link3 });

                            compositeLink3.CreateDocument();

                            using (MemoryStream stream = new MemoryStream())
                            {
                                CsvExportOptions options = new CsvExportOptions();
                                compositeLink3.ExportToXlsx(stream, new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG, SheetName = "Solicitudes Rechazadas" });
                                Response.Clear();
                                Response.Buffer = false;
                                Response.AppendHeader("Content-Type", "application/asl");
                                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                                Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=Reporte de Desembolsos Rechazados al {0}.xlsx", DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Year + " " + DateTime.Now.Hour + "_" + DateTime.Now.Minute));
                                Response.BinaryWrite(stream.ToArray());
                                Response.End();

                            }
                            ps.Dispose();
                        }
                        else
                        {
                            lbErrorFinanciera.Text = "No existen desembolsos para exportar.";
                            return;
                        }
                        #endregion
                        break;

                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorFinanciera.Text = string.Format("Error exportando desembolsos. {0} {1}", ex.Message, m);
            }
        }




        void psDesembolsos_XlSheetCreated(object sender, XlSheetCreatedEventArgs e)
        {
            if (e.Index == 0) e.SheetName = "Desembolsos";
        }

        public void IniFormaDespachoFinanciera()
        {
            lblErrorDespacho.Text = "";
            lblInfoDespacho.Text = "";
            lblErrorEnvio.Text = "";
            lblInfoEnvio.Text = "";

        }

        public void ObtenerFacturasFinanciera()
        {
            //TODO William
            try
            {
                IniFormaDespachoFinanciera();

                DateTime mFechaInicial = Convert.ToDateTime(fechaInicialFacturas.Value);
                DateTime mFechaFinal = Convert.ToDateTime(fechaFinalFacturas.Value);


                if (tiendaDesFinanciera.Value == null)
                {
                    lblInfoDespacho.Text = "Favor de ingresar el codigo de la tienda.";
                    return;
                }
                if (cmbExpFinanciera.SelectedIndex == -1)
                {
                    lblInfoDespacho.Text = "Por favor seleccione una financiera";
                    cmbExpFinanciera.Focus();
                    return;
                }
                string Tienda = tiendaDesFinanciera.Value.ToString();
                GridFacturasFinanciera.Visible = true;

                WebRequest request = WebRequest.Create(string.Format("{0}/FacturasInterconsumo?AnioInicio={1}&MesInicio={2}&DiaInicio={3}&AnioFinal={4}&MesFinal={5}&DiaFinal={6}&tienda={7}&nFinanciera={8}", Convert.ToString(Session["UrlRestServices"]), mFechaInicial.Year.ToString(), mFechaInicial.Month.ToString(), mFechaInicial.Day.ToString(), mFechaFinal.Year.ToString(), mFechaFinal.Month.ToString(), mFechaFinal.Day.ToString(), Tienda, cmbExpFinanciera.SelectedItem.Value));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                Clases.LoteFacturasFinanciera mLoteFacturas = new Clases.LoteFacturasFinanciera();
                mLoteFacturas = JsonConvert.DeserializeObject<Clases.LoteFacturasFinanciera>(responseString);


                if (!mLoteFacturas.exito)
                {
                    lblErrorDespacho.Text = mLoteFacturas.mensaje;
                    return;
                }
                if (mLoteFacturas.Facturas.Count() == 0)
                {
                    GridFacturasFinanciera.Visible = false;
                    lblInfoDespacho.Text = "No se encontraron facturas para esa fecha.";

                }
                else GridFacturasFinanciera.Visible = true;


                GridFacturasFinanciera.DataSource = mLoteFacturas.Facturas.AsEnumerable();
                GridFacturasFinanciera.DataMember = "FacturasFinanciera";
                GridFacturasFinanciera.DataBind();
                GridFacturasFinanciera.FocusedRowIndex = -1;
                GridFacturasFinanciera.SettingsPager.PageSize = 500;
                Session["FacturasFinanciera"] = mLoteFacturas;
                //GridExpedientesInterconsumo

            }
            catch (Exception ex)
            {
                lblErrorDespacho.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void LimpiarFacturasFinanciera()
        {
            Clases.LoteFacturasFinanciera mLoteFacturas = new Clases.LoteFacturasFinanciera();
            GridFacturasFinanciera.Visible = false;
            GridFacturasFinanciera.DataSource = mLoteFacturas.Facturas.AsEnumerable();
            GridFacturasFinanciera.DataMember = "FacturasFinanciera";
            GridFacturasFinanciera.DataBind();
            GridFacturasFinanciera.FocusedRowIndex = -1;
            GridFacturasFinanciera.SettingsPager.PageSize = 500;
            Session["FacturasFinanciera"] = mLoteFacturas;

        }

        public void CargarFacturasSeleccionadas()
        {
            IniFormaDespachoFinanciera();
            //TODO William
            try
            {
                //No hay nada para seleccionar
                Clases.LoteFacturasFinanciera mFacturasFinanciera = (Clases.LoteFacturasFinanciera)Session["FacturasFinanciera"];
                if (!(mFacturasFinanciera.Facturas.Count() > 0))
                    return;

                //GridFacturasInterconsumo.DataSource = mFacturasInterconsumo;
                //GridFacturasInterconsumo.DataMember = "FacturasFinanciera";
                //GridFacturasInterconsumo.DataBind();
                //GridFacturasInterconsumo.SettingsPager.PageSize = 500;

                Clases.LoteFacturasFinanciera mExpedientes = (Clases.LoteFacturasFinanciera)Session["ExpedientesInterconsumo"];

                if (mExpedientes == null)
                {
                    mExpedientes = new Clases.LoteFacturasFinanciera();
                }

                for (var i = 0; i < GridFacturasFinanciera.VisibleRowCount; ++i)
                {
                    //var name = grid.GetRowValues(i, "Name");
                    if (GridFacturasFinanciera.Selection.IsRowSelected(i))
                    {
                        Clases.FacturasFinanciera mExpediente = new Clases.FacturasFinanciera();
                        mExpediente = mFacturasFinanciera.Facturas.ElementAt(i);
                        mExpediente.Financiera = int.Parse(cmbExpFinanciera.SelectedItem.Value.ToString());
                        mExpediente.NombreFinanciera = cmbExpFinanciera.SelectedItem.Text;
                        var mExpedienteDuplicado = mExpedientes.Facturas.Find(item => item.NoFactura == mExpediente.NoFactura);
                        if (mExpedienteDuplicado == null)
                            mExpedientes.Facturas.Add(mExpediente);
                    }
                }

                if (mExpedientes.Facturas.Count() > 0)
                {
                    GridExpedientesFinanciera.Visible = true;
                    var ExpedientesOrdenados = mExpedientes.Facturas.OrderBy(x => x.Tienda).ThenBy(x => x.NoFactura).ToList();
                    mExpedientes.Facturas = ExpedientesOrdenados;
                }

                Session["ExpedientesInterconsumo"] = mExpedientes;
                GridExpedientesFinanciera.DataSource = mExpedientes.Facturas.AsEnumerable();
                GridExpedientesFinanciera.DataMember = "FacturasFinanciera";
                GridExpedientesFinanciera.DataBind();
                GridExpedientesFinanciera.SettingsPager.PageSize = 500;



            }
            catch (Exception ex)
            {
                lblErrorDespacho.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }


        }

        protected void lkConsultarFacturasInter_Click(object sender, EventArgs e)
        {
            IniFormaDespachoFinanciera();
            ObtenerFacturasFinanciera();
        }

        protected void lkAgregarFacturasInter_Click(object sender, EventArgs e)
        {
            IniFormaDespachoFinanciera();
            CargarFacturasSeleccionadas();
        }

        protected void lkLimpiarFacturasInter_Click(object sender, EventArgs e)
        {
            IniFormaDespachoFinanciera();
            LimpiarFacturasFinanciera();
        }



        protected void lkGuardarExpedienteInter_Click(object sender, EventArgs e)
        {
            //TODO William
            try
            {
                IniFormaDespachoFinanciera();

                DateTime mFechaInicial = Convert.ToDateTime(fechaInicialFacturas.Value);
                DateTime mFechaFinal = Convert.ToDateTime(fechaFinalFacturas.Value);



                Clases.LoteFacturasFinanciera mExpedientes = (Clases.LoteFacturasFinanciera)Session["ExpedientesInterconsumo"];

                mExpedientes.Operacion = Const.ExpedienteFinanciera.ENVIADO_FINANCIERA;
                mExpedientes.Usuario = base.GetUsuario();
                mExpedientes.usuario = base.GetUsuario();
                //Guardando el código de la financiera a donde se envió la solicitud de crédito

                if (mExpedientes.Facturas.Count() > 0)
                    GridExpedientesFinanciera.Visible = true;

                WebRequest request = WebRequest.Create(string.Format("{0}/expedientesinterconsumo/", Convert.ToString(Session["UrlRestServices"])));

                string json = JsonConvert.SerializeObject(mExpedientes);
                byte[] data = Encoding.ASCII.GetBytes(json);


                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                //Respuesta 
                Respuesta mRetorna = new Respuesta();
                mRetorna = JsonConvert.DeserializeObject<Respuesta>(responseString);

                if (!mRetorna.Exito)
                {
                    lblErrorEnvio.Text = mRetorna.Mensaje;
                    return;
                }
                else
                {
                    lblInfoEnvio.Text = mRetorna.Mensaje;
                }

                //lblInfoEnvio.Text = "Expedientes almacenados correctamente.";

            }
            catch (Exception ex)
            {
                lblErrorEnvio.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void LimpiarExpedientesInterconsumo()
        {
            Clases.LoteFacturasFinanciera mLoteFacturas = new Clases.LoteFacturasFinanciera();
            GridExpedientesFinanciera.Visible = false;
            GridExpedientesFinanciera.DataSource = mLoteFacturas.Facturas.AsEnumerable();
            GridExpedientesFinanciera.DataMember = "FacturasFinanciera";
            GridExpedientesFinanciera.DataBind();
            GridExpedientesFinanciera.FocusedRowIndex = -1;
            GridExpedientesFinanciera.SettingsPager.PageSize = 500;
            Session["ExpedientesInterconsumo"] = mLoteFacturas;

        }


        protected void lkLimpiarEnvioExpedientes_Click(object sender, EventArgs e)
        {
            LimpiarExpedientesInterconsumo();
        }

        protected void lkConsultarEnvioExpediente_Click(object sender, EventArgs e)
        {
            //TODO William
            try
            {
                IniFormaDespachoFinanciera();

                DateTime mFecha = Convert.ToDateTime(fechaEnvioExpedientes.Value);
                //DateTime mFechaFinal = Convert.ToDateTime(fechaFinalFacturas.Value);
                //string Tienda = tiendaDesInterconsumo.Value.ToString();

                WebRequest request = WebRequest.Create(string.Format("{0}/ExpedientesInterconsumo/?Anio={1}&Mes={2}&Dia={3}&nFinanciera={4}", Convert.ToString(Session["UrlRestServices"]), mFecha.Year.ToString(), mFecha.Month.ToString(), mFecha.Day.ToString(), cmbExpFinanciera.SelectedItem.Value));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                Clases.LoteFacturasFinanciera mLoteExpedientes = new Clases.LoteFacturasFinanciera();
                mLoteExpedientes = JsonConvert.DeserializeObject<Clases.LoteFacturasFinanciera>(responseString);


                if (!mLoteExpedientes.exito)
                {
                    lblErrorEnvio.Text = mLoteExpedientes.mensaje;
                    return;
                }

                if (mLoteExpedientes.Facturas.Count() == 0)
                {
                    lblInfoEnvio.Text = "No se encontraron envios pendientes para esa fecha.";
                    GridExpedientesFinanciera.Visible = false;
                }
                else GridExpedientesFinanciera.Visible = true;


                GridExpedientesFinanciera.DataSource = mLoteExpedientes.Facturas.AsEnumerable();
                GridExpedientesFinanciera.DataMember = "FacturasFinanciera";
                GridExpedientesFinanciera.DataBind();
                GridExpedientesFinanciera.FocusedRowIndex = -1;
                GridExpedientesFinanciera.SettingsPager.PageSize = 500;
                Session["ExpedientesInterconsumo"] = mLoteExpedientes;


            }
            catch (Exception ex)
            {
                lblErrorEnvio.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void lkImprimirEnvioInter_Click(object sender, EventArgs e)
        {

            try
            {
                IniFormaDespachoFinanciera();
                if (cmbExpFinanciera.SelectedIndex == -1)
                {
                    lblErrorEnvio.Text = "Por favor seleccione una financiera";
                    cmbExpFinanciera.Focus();
                    return;
                }
                DateTime mFechaInicial = Convert.ToDateTime(fechaInicialFacturas.Value);
                DateTime mFechaFinal = Convert.ToDateTime(fechaFinalFacturas.Value);

                //Se guarda antes de imprimir, por si aun no le ha dado guardar.
                lkGuardarExpedienteInter_Click(sender, e);


                Clases.LoteFacturasFinanciera mExpedientes = (Clases.LoteFacturasFinanciera)Session["ExpedientesInterconsumo"];

                mExpedientes.Operacion = Const.ExpedienteFinanciera.ENVIADO_FINANCIERA;
                mExpedientes.Usuario = base.GetUsuario();


                //TODO: agregar columna tienda y ordernar
                DataSet dsExpedientesFinanciera = new DataSet();
                DataTable dtExpedienteF = new DataTable("ExpedientesInterconsumo");

                dtExpedienteF.Columns.Add("Fecha", typeof(DateTime));
                dtExpedienteF.Columns.Add("TotalFacturasExpedientes", typeof(decimal));
                dtExpedienteF.Columns.Add("SaldoAnterior", typeof(decimal));
                dtExpedienteF.Columns.Add("TotalPendiente", typeof(decimal));

                DataTable dtExpedienteDet = new DataTable("ExpedientesInterconsumoDet");
                dtExpedienteDet.Columns.Add("Fecha", typeof(DateTime));
                dtExpedienteDet.Columns.Add("Factura", typeof(string));
                dtExpedienteDet.Columns.Add("FechaFactura", typeof(DateTime));
                dtExpedienteDet.Columns.Add("CodigoCliente", typeof(string));
                dtExpedienteDet.Columns.Add("NombreCliente", typeof(string));
                dtExpedienteDet.Columns.Add("ValorFactura", typeof(decimal));
                dtExpedienteDet.Columns.Add("NumeroSolicitud", typeof(string));
                dtExpedienteDet.Columns.Add("Identificacion", typeof(string));
                dtExpedienteDet.Columns.Add("OtraIdentificacion", typeof(string));
                dtExpedienteDet.Columns.Add("ConstanciaIngresos", typeof(string));
                dtExpedienteDet.Columns.Add("DocumentosServiciosPublicos", typeof(string));
                dtExpedienteDet.Columns.Add("EstadosDeCuenta", typeof(int));
                dtExpedienteDet.Columns.Add("Pagare", typeof(string));
                dtExpedienteDet.Columns.Add("Observaciones", typeof(string));
                dtExpedienteDet.Columns.Add("ID", typeof(int));
                dtExpedienteDet.Columns.Add("Tienda", typeof(string));

                int mi = 0;
                decimal mTotalFacturasExpediente = 0;
                foreach (var item in mExpedientes.Facturas)
                {
                    mi++;
                    mTotalFacturasExpediente = mTotalFacturasExpediente + item.Valor;
                    dtExpedienteDet.Rows.Add(
                        DateTime.Now.Date
                        , item.NoFactura
                        , item.FechaFactura.Date
                        , item.Cliente
                        , item.NombreCliente
                        , item.Valor
                        , item.Solicitud
                        , item.Identificacion
                        , item.OtraIdentificacion
                        , item.ConstanciaIngresos == "S" ? "X" : ""
                        , item.DocumentoServiciosPublicos
                        , item.EstadosDeCuenta
                        , item.Pagare == "S" ? "X" : ""
                        , item.Observaciones
                        , mi
                        , item.Tienda);
                }

                dtExpedienteF.Rows.Add(DateTime.Now.Date, mTotalFacturasExpediente, 0, 0);

                dsExpedientesFinanciera.Tables.Add(dtExpedienteF);
                dsExpedientesFinanciera.Tables.Add(dtExpedienteDet);

                using (ReportDocument reporte = new ReportDocument())
                {
                    string mNombreDocumento = string.Format("Envio-" + cmbExpFinanciera.SelectedItem.Text + "_{0}.{1}.{2}.pdf", DateTime.Now.Day < 10 ? "0" + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString(), DateTime.Now.Month < 10 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString(), DateTime.Now.Year);
                    string mFormato = "rptExpedienteInterconsumo.rpt";
                    string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mFormato);

                    reporte.Load(p);
                    reporte.SetDataSource(dsExpedientesFinanciera);
                    //reporte.SetParameterValue("titulo", string.Format("Fecha: {0}/{1}/{2}", DateTime.Now.Day < 10 ? "0" : "", DateTime.Now.Month < 10 ? "0" : "", DateTime.Now.Year));
                    reporte.SetParameterValue("usuario", this.GetUsuario());
                    reporte.SetParameterValue("financiera", cmbExpFinanciera.SelectedItem.Text);

                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");

                    try
                    {
                        if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                    }
                    catch
                    {
                        return;
                    }

                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }

                lblInfoEnvio.Text = "Expedientes con estado enviado.";

            }
            catch (Exception ex)
            {
                lblErrorDespacho.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lkEnviarCorreo_Click(object sender, EventArgs e)
        {

            try
            {
                IniFormaDespachoFinanciera();
                if (EnviarEmailReporteSaldos("ENVIO_EXPEDIENTES", cmbExpFinanciera.SelectedItem.Value.ToString(), cmbExpFinanciera.SelectedItem.Text))
                {
                    lblInfoEnvio.Text = "Correo enviado exitosamente!";
                }

                if (EnviarEmailReporteSaldos("RESUMEN_SALDOS", cmbExpFinanciera.SelectedItem.Value.ToString(), cmbExpFinanciera.SelectedItem.Text))
                {
                    lblInfoEnvio.Text = "Correo enviado exitosamente!";
                }

            }
            catch (Exception ex)
            {
                lblErrorDespacho.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        private bool EnviarEmailReporteSaldos(string TipoReporte, string nFinanciera = "7", string NombreFinanciera = "INTERCONSUMO")
        {
            try
            {
                //IniFormaDespachoInterconsumo();

                //string mTipoReporte = TipoReporte;
                List<string> lstParams = new List<string>();
                lstParams.Add(TipoReporte);
                lstParams.Add(nFinanciera);
                lstParams.Add(HttpUtility.UrlEncode(NombreFinanciera));
                string json = JsonConvert.SerializeObject(lstParams);
                byte[] data = Encoding.ASCII.GetBytes(json);

                WebRequest requestEmail = WebRequest.Create(string.Format("{0}/SaldosDesembolsoInterconsumo/", Convert.ToString(Session["UrlRestServices"])));
                requestEmail.Method = "POST";
                requestEmail.ContentType = "application/json";
                requestEmail.ContentLength = data.Length;

                using (var stream = requestEmail.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var responseEmail = (HttpWebResponse)requestEmail.GetResponse();
                var responseStringEmail = new StreamReader(responseEmail.GetResponseStream()).ReadToEnd();

                //Respuesta 
                Respuesta mRetornaEmail = new Respuesta();
                mRetornaEmail = JsonConvert.DeserializeObject<Respuesta>(responseStringEmail);

                if (!mRetornaEmail.Exito)
                {
                    if (TipoReporte == "DESEMBOLSOS_CARGADOS ")
                        lbErrorFinanciera.Text = mRetornaEmail.Mensaje;
                    if (TipoReporte == "ENVIO_EXPEDIENTES")
                        lblErrorEnvio.Text = mRetornaEmail.Mensaje;
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                if (TipoReporte == "DESEMBOLSOS_CARGADOS ")
                    lbErrorFinanciera.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
                if (TipoReporte == "ENVIO_EXPEDIENTES")
                    lblErrorEnvio.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }


        //=================================================================================================================================================
        //======================================G R I D   I N T E R C O N S U M O    (D E S E M B O L S O S)===============================================
        //=================================================================================================================================================

        //protected void CancelEditinggridInterconsumo(CancelEventArgs e)
        //{
        //    ValidarSesion();
        //    e.Cancel = true;
        //    gridInterconsumo.CancelEdit();
        //    gridInterconsumo.SettingsEditing.BatchEditSettings.AllowValidationOnEndEdit = true;
        //}

        protected void gridInterconsumo_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == ColumnCommandButtonType.Update || e.ButtonType == ColumnCommandButtonType.Cancel) e.Visible = false;
        }

        protected void gridInterconsumo_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            //if (e.DataColumn.Name == "ClienteMF") e.Cell.ToolTip = "Ingrese el codigo del cliente correcto.";
        }

        protected void gridInterconsumo_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            string Criterio = e.GetValue("Criterio") == null ? "" : e.GetValue("Criterio").ToString();
            if (string.IsNullOrEmpty(Criterio)) e.Row.ForeColor = System.Drawing.Color.Red;
        }

        protected void gridInterconsumo_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
        }

        //protected void gridInterconsumo_BatchUpdate(object sender, ASPxDataBatchUpdateEventArgs e)
        //{
        //    //foreach (var args in e.UpdateValues)
        //    //    UpdateDesembolsoInterconsumo(args.Keys["Prestamo"].ToString()
        //    //    , args.NewValues["ClienteMF"] == null ? "" : args.NewValues["ClienteMF"].ToString()
        //    //    , args.NewValues["FacturaMF"] == null ? "" : args.NewValues["FacturaMF"].ToString()
        //    //   );

        //    //e.Handled = true;
        //}

        protected void gridInterconsumo_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            UpdateDesembolsoInterconsumo(e.Keys["Prestamo"].ToString()
            , e.NewValues["ClienteMF"] == null ? "" : e.NewValues["ClienteMF"].ToString()
            , e.NewValues["FacturaMF"] == null ? "" : e.NewValues["FacturaMF"].ToString()
           );
            //CancelEditinggridInterconsumo(e);
            e.Cancel = true;
            gridFinanciera.CancelEdit();
            gridFinanciera.SettingsEditing.BatchEditSettings.AllowValidationOnEndEdit = true;
        }

        protected void UpdateDesembolsoInterconsumo(string Prestamo, string ClienteMF, string FacturaMF)
        {
            Clases.LoteDesombolsosFinanciera mDesembolsosInterconsumo = (Clases.LoteDesombolsosFinanciera)Session["DesembolsosInterconsumo"];
            var mDesembolsoSeleccionado = mDesembolsosInterconsumo.Desembolsos.Find(item => item.Prestamo == Prestamo);

            if (mDesembolsoSeleccionado != null)
            {
                mDesembolsoSeleccionado.ClienteMF = ClienteMF;
                mDesembolsoSeleccionado.FacturaMF = FacturaMF;

            }

            if (mDesembolsosInterconsumo.Desembolsos.Count() > 0)
                gridFinanciera.Visible = true;
            //TODO
            Session["DesembolsosInterconsumo"] = mDesembolsosInterconsumo;

            DataSet ds = new DataSet();
            DataTable dt = new DataTable("desembolsos");
            using (var reader = ObjectReader.Create(mDesembolsosInterconsumo.Desembolsos))
            {
                dt.Load(reader);
            }

            ds.Tables.Add(dt);
            gridFinanciera.DataSource = ds;
            gridFinanciera.DataMember = "Desembolsos";
            gridFinanciera.DataBind();
            gridFinanciera.FocusedRowIndex = -1;
            gridFinanciera.SettingsPager.PageSize = 200;

            Session["dsDesembolsos"] = ds;

        }

        protected void lkImprimirDesembolsos_Click(object sender, EventArgs e)
        {
            lbErrorFinanciera.Text = String.Empty;
            try
            {
                //Hay un reporte (rpt) para los desembolsos realizados.
                int nIndexOpcion = 0;
                nIndexOpcion = rdlRealizados.Checked ? 0 : (rdlPendientes.Checked ? 1 : 2);
                switch (nIndexOpcion)
                {
                    case 0:
                        #region "Desembolsos Realizados"

                        DateTime mFecha = DateTime.Now;
                        Clases.LoteDesombolsosFinanciera mDesembolsosInterconsumo = (Clases.LoteDesombolsosFinanciera)Session["DesembolsosInterconsumo"];

                        if (mDesembolsosInterconsumo == null)
                            return;

                        mDesembolsosInterconsumo.Operacion = Const.ExpedienteFinanciera.DESEMBOLSADO;
                        mDesembolsosInterconsumo.Usuario = base.GetUsuario();

                        DataSet dsExpedientesInterconsumo = new DataSet();
                        DataTable dtExpedienteInterconsumo = new DataTable("ExpedientesInterconsumo");

                        dtExpedienteInterconsumo.Columns.Add("Fecha", typeof(DateTime));
                        dtExpedienteInterconsumo.Columns.Add("TotalFacturasExpedientes", typeof(decimal));
                        dtExpedienteInterconsumo.Columns.Add("SaldoAnterior", typeof(decimal));
                        dtExpedienteInterconsumo.Columns.Add("TotalPendienteInterconsumo", typeof(decimal));

                        DataTable dtExpedienteInterconsumoDet = new DataTable("ExpedientesInterconsumoDet");
                        dtExpedienteInterconsumoDet.Columns.Add("Fecha", typeof(DateTime));
                        dtExpedienteInterconsumoDet.Columns.Add("Factura", typeof(string));
                        dtExpedienteInterconsumoDet.Columns.Add("CodigoCliente", typeof(string));
                        dtExpedienteInterconsumoDet.Columns.Add("NombreCliente", typeof(string));
                        dtExpedienteInterconsumoDet.Columns.Add("ID", typeof(int));
                        dtExpedienteInterconsumoDet.Columns.Add("MontoDesembolso", typeof(decimal));
                        dtExpedienteInterconsumoDet.Columns.Add("Prestamo", typeof(string));

                        int mi = 0;
                        decimal mTotalFacturasExpediente = 0;
                        foreach (var item in mDesembolsosInterconsumo.Desembolsos)
                        {
                            mi++;
                            mTotalFacturasExpediente = mTotalFacturasExpediente + item.Valor;
                            mFecha = item.Fecha;
                            dtExpedienteInterconsumoDet.Rows.Add(
                                item.Fecha.Date
                                , item.FacturaMF
                                , item.ClienteMF
                                , item.NombreCliente
                                , mi
                                , item.Valor
                                , item.Prestamo);
                        }

                        dtExpedienteInterconsumo.Rows.Add(mFecha.Date, mTotalFacturasExpediente, 0, 0);

                        dsExpedientesInterconsumo.Tables.Add(dtExpedienteInterconsumo);
                        dsExpedientesInterconsumo.Tables.Add(dtExpedienteInterconsumoDet);
                        if (cmbFinanciera.SelectedItem.Index > 0)
                        {
                            using (ReportDocument reporte = new ReportDocument())
                            {
                                string mNombreDocumento = string.Format("Desembolso-" + cmbFinanciera.SelectedItem.Text + "{0}.{1}.{2}.pdf", DateTime.Now.Day < 10 ? "0" + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString(), DateTime.Now.Month < 10 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString(), DateTime.Now.Year);
                                string mFormato = "rptDesembolsoInterconsumo.rpt";
                                string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mFormato);

                                reporte.Load(p);
                                reporte.SetDataSource(dsExpedientesInterconsumo);
                                reporte.SetParameterValue("usuario", this.GetUsuario());
                                reporte.SetParameterValue("NombreFinanciera", cmbFinanciera.SelectedItem.Text);

                                if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");

                                try
                                {
                                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                                }
                                catch
                                {
                                    return;
                                }

                                reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                                Response.Clear();
                                Response.ContentType = "application/pdf";

                                Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                                Response.WriteFile(@"C:\reportes\" + mNombreDocumento);

                                HttpContext.Current.ApplicationInstance.CompleteRequest();
                            }
                        }
                        #endregion
                        break;

                }

            }
            catch (Exception ex)
            {
                lbErrorFinanciera.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }



        //======================================G R I D   E X P E D I E N T E S    I N T E R C O N S U M O=================================================

        protected void GridExpedientesInterconsumor_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            UpdateExpedienteInterconsumo(e.Keys["NoFactura"].ToString()
                , e.NewValues["Solicitud"] == null ? "" : e.NewValues["Solicitud"].ToString()
                , e.NewValues["Identificacion"] == null ? "" : e.NewValues["Identificacion"].ToString()
               , e.NewValues["OtraIdentificacion"] == null ? "" : e.NewValues["OtraIdentificacion"].ToString()
               , Convert.ToInt32(e.NewValues["EstadosDeCuenta"])
               , e.NewValues["ConstanciaIngresos"] == null ? "" : e.NewValues["ConstanciaIngresos"].ToString()
               , e.NewValues["DocumentoServiciosPublicos"] == null ? "" : e.NewValues["DocumentoServiciosPublicos"].ToString()
               , e.NewValues["Pagare"] == null ? "" : e.NewValues["Pagare"].ToString()
               , e.NewValues["Observaciones"] == null ? "" : e.NewValues["Observaciones"].ToString()
               );

            e.Cancel = true;
            GridExpedientesFinanciera.CancelEdit();
            GridExpedientesFinanciera.SettingsEditing.BatchEditSettings.AllowValidationOnEndEdit = true;
        }

        protected void CancelEditingGridExpedientesInterconsumo(ASPxDataUpdatingEventArgs e)
        {

        }

        protected void UpdateExpedienteInterconsumo(string NoFactura
                , string Solicitud
                , string Identificacion
               , string OtraIdentificacion
               , int EstadosDeCuenta
               , string ConstanciaIngresos
               , string DocumentoServiciosPublicos
               , string Pagare
               , string Observaciones
               )
        {

            Clases.LoteFacturasFinanciera mExpedientesInterconsumo = (Clases.LoteFacturasFinanciera)Session["ExpedientesInterconsumo"];

            var mExpedienteSeleccionado = mExpedientesInterconsumo.Facturas.Find(item => item.NoFactura == NoFactura);
            if (mExpedienteSeleccionado != null)
            {
                mExpedienteSeleccionado.Solicitud = Solicitud;
                mExpedienteSeleccionado.Identificacion = Identificacion;
                mExpedienteSeleccionado.OtraIdentificacion = OtraIdentificacion;
                mExpedienteSeleccionado.EstadosDeCuenta = EstadosDeCuenta;
                mExpedienteSeleccionado.ConstanciaIngresos = ConstanciaIngresos;
                mExpedienteSeleccionado.DocumentoServiciosPublicos = DocumentoServiciosPublicos;
                mExpedienteSeleccionado.Pagare = Pagare;
                mExpedienteSeleccionado.Observaciones = Observaciones;

            }
            if (mExpedientesInterconsumo.Facturas.Count() > 0)
                GridExpedientesFinanciera.Visible = true;

            Session["ExpedientesInterconsumo"] = mExpedientesInterconsumo;
            GridExpedientesFinanciera.DataSource = mExpedientesInterconsumo.Facturas.AsEnumerable();
            GridExpedientesFinanciera.DataMember = "FacturasFinanciera";
            GridExpedientesFinanciera.DataBind();
            GridExpedientesFinanciera.SettingsPager.PageSize = 500;

        }



        protected void GridExpedientesInterconsumo_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            if (e.Column.FieldName == "Identificacion")
            {
                IList dsIdentificacion = new System.Collections.Generic.List<String>();
                dsIdentificacion.Add("DPI");
                dsIdentificacion.Add("Pasaporte");
                dsIdentificacion.Add("");

                ASPxComboBox cmbIdentificacion = e.Editor as ASPxComboBox;
                cmbIdentificacion.DataSource = dsIdentificacion;
                cmbIdentificacion.DataBind();
            }

            if (e.Column.FieldName == "OtraIdentificacion")
            {
                IList dsOtraIdentificacion = new System.Collections.Generic.List<String>();
                dsOtraIdentificacion.Add("IGSS");
                dsOtraIdentificacion.Add("IRTRA");
                dsOtraIdentificacion.Add("Licencia");
                dsOtraIdentificacion.Add("");

                ASPxComboBox cmbOtraIdentificacion = e.Editor as ASPxComboBox;
                cmbOtraIdentificacion.DataSource = dsOtraIdentificacion;
                cmbOtraIdentificacion.DataBind();
            }

            if (e.Column.FieldName == "DocumentoServiciosPublicos")
            {
                IList dsDocumentoServicioPublico = new System.Collections.Generic.List<String>();
                dsDocumentoServicioPublico.Add("Luz");
                dsDocumentoServicioPublico.Add("Teléfono");
                dsDocumentoServicioPublico.Add("Otro");
                dsDocumentoServicioPublico.Add("");

                ASPxComboBox cmbDocumentoServicioPublico = e.Editor as ASPxComboBox;
                cmbDocumentoServicioPublico.DataSource = dsDocumentoServicioPublico;
                cmbDocumentoServicioPublico.DataBind();
            }
        }

        protected void GridExpedientesInterconsumo_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == ColumnCommandButtonType.Update || e.ButtonType == ColumnCommandButtonType.Cancel) e.Visible = false;
        }

        protected void GridExpedientesInterconsumo_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
        }

        protected void GridExpedientesInterconsumo_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {

        }

        protected void GridExpedientesInterconsumo_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
        {
            try
            {
                IniFormaDespachoFinanciera();

                DateTime mFechaInicial = Convert.ToDateTime(fechaInicialFacturas.Value);
                DateTime mFechaFinal = Convert.ToDateTime(fechaFinalFacturas.Value);

                string mFactura = e.KeyValue.ToString();

                Clases.LoteFacturasFinanciera mExpedientesInterconsumo = (Clases.LoteFacturasFinanciera)Session["ExpedientesInterconsumo"];

                WebRequest request = WebRequest.Create(string.Format("{0}/expedientesinterconsumo/?Factura={1}", Convert.ToString(Session["UrlRestServices"]), mFactura));

                string json = JsonConvert.SerializeObject(mExpedientesInterconsumo);
                byte[] data = Encoding.ASCII.GetBytes(json);


                request.Method = "DELETE";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                //Respuesta 
                Respuesta mRetorna = new Respuesta();
                mRetorna = JsonConvert.DeserializeObject<Respuesta>(responseString);

                if (!mRetorna.Exito)
                {
                    lblErrorEnvio.Text = mRetorna.Mensaje;
                    return;
                }
                else
                {
                    lblInfoEnvio.Text = mRetorna.Mensaje;
                }

                mExpedientesInterconsumo.Facturas.RemoveAll(x => x.NoFactura == mFactura);


                if (mExpedientesInterconsumo.Facturas.Count() > 0)
                    GridExpedientesFinanciera.Visible = true;
                else GridExpedientesFinanciera.Visible = false;



                GridExpedientesFinanciera.DataSource = mExpedientesInterconsumo.Facturas.AsEnumerable();
                GridExpedientesFinanciera.DataMember = "FacturasFinanciera";
                GridExpedientesFinanciera.DataBind();
                GridExpedientesFinanciera.FocusedRowIndex = -1;
                GridExpedientesFinanciera.SettingsPager.PageSize = 500;
                Session["ExpedientesInterconsumo"] = mExpedientesInterconsumo;


            }
            catch (Exception ex)
            {
                lblErrorEnvio.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {

        }
        //--------------------------------------
        //Inventario
        protected void lkConsultarInventario_Click(object sender, EventArgs e)
        {

        }

        protected void lkImprimirInventario_Click(object sender, EventArgs e)
        {

        }

        //-----------------------------------------------------------------------------------------
        static readonly object EventItemClick = new object();

        public delegate void ExportItemClickEventHandler(object source, ExportItemClickEventArgs e);
        public event ExportItemClickEventHandler ItemClick
        {
            add { Events.AddHandler(EventItemClick, value); }
            remove { Events.RemoveHandler(EventItemClick, value); }
        }
        //-----------------------------------------------------------------------------------------

        protected void lkExportarXlsInventario_Click(object sender, EventArgs e)
        {
            ToolbarExport_ItemClick(sender, new ExportItemClickEventArgs(DemoExportFormat.LIXls));
        }

        protected void lkEnviarCorreoInventario_Click(object sender, EventArgs e)
        {

        }

        protected void lkLimpiarInventario_Click(object sender, EventArgs e)
        {

        }

        void LibroCompras(bool asl, bool plantilla)
        {
            try
            {
                int mAnio = 0; int mNumeroMes = 0;

                try
                {
                    mAnio = Convert.ToInt32(txtAnioLV.Text);
                }
                catch
                {
                    lbErrorLibroVentas.Text = "Ingrese un año válido";
                    return;
                }

                if (mAnio < 2010)
                {
                    lbErrorLibroVentas.Text = "Ingrese un año válido";
                    return;
                }

                try
                {
                    mNumeroMes = Convert.ToInt32(cbMesLV.SelectedItem.Value);
                }
                catch
                {
                    lbErrorLibroVentas.Text = "Debe seleccionar un mes";
                    cbMesLV.Focus();
                    return;
                }

                if (mNumeroMes < 0 || mNumeroMes > 12)
                {
                    lbErrorLibroVentas.Text = "Debe seleccionar un mes";
                    cbMesLV.Focus();
                    return;
                }

                string mMes = "Enero"; DateTime mFecha = new DateTime(mAnio, mNumeroMes, 1);

                if (mFecha.Month == 2) mMes = "Febrero";
                if (mFecha.Month == 3) mMes = "Marzo";
                if (mFecha.Month == 4) mMes = "Abril";
                if (mFecha.Month == 5) mMes = "Mayo";
                if (mFecha.Month == 6) mMes = "Junio";
                if (mFecha.Month == 7) mMes = "Julio";
                if (mFecha.Month == 8) mMes = "Agosto";
                if (mFecha.Month == 9) mMes = "Septiembre";
                if (mFecha.Month == 10) mMes = "Octubre";
                if (mFecha.Month == 11) mMes = "Noviembre";
                if (mFecha.Month == 12) mMes = "Diciembre";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DataSet ds = new DataSet(); string mMensaje = "";

                ws.Timeout = 999999999;
                ds = ws.LibroCompras(ref mMensaje, txtAnioLV.Text, Convert.ToString(cbMesLV.SelectedItem.Value), cbEmpresaLV.SelectedItem.ToString().ToLower(), plantilla);

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorLibroVentas.Text = mMensaje;
                    return;
                }

                string mIniciales = "";
                if (cbEmpresaLV.SelectedItem.ToString().ToLower() == "emprconc") mIniciales = "EC";

                if (plantilla)
                {
                    gridLibroVentas1.DataSource = ds;
                    gridLibroVentas1.DataMember = "Borrar";
                    gridLibroVentas1.DataBind();
                    gridLibroVentas1.FocusedRowIndex = -1;
                    gridLibroVentas1.SettingsPager.PageSize = 100;

                    gridLibroVentas2.DataSource = ds;
                    gridLibroVentas2.DataMember = "BienServicio";
                    gridLibroVentas2.DataBind();
                    gridLibroVentas2.FocusedRowIndex = -1;
                    gridLibroVentas2.SettingsPager.PageSize = 100;

                    gridLibroVentas3.DataSource = ds;
                    gridLibroVentas3.DataMember = "PC";
                    gridLibroVentas3.DataBind();
                    gridLibroVentas3.FocusedRowIndex = -1;
                    gridLibroVentas3.SettingsPager.PageSize = 100;

                    gridLibroVentas4.DataSource = ds;
                    gridLibroVentas4.DataMember = "FE";
                    gridLibroVentas4.DataBind();
                    gridLibroVentas4.FocusedRowIndex = -1;
                    gridLibroVentas4.SettingsPager.PageSize = 100;

                    gridLibroVentas5.DataSource = ds;
                    gridLibroVentas5.DataMember = "Editar";
                    gridLibroVentas5.DataBind();
                    gridLibroVentas5.FocusedRowIndex = -1;
                    gridLibroVentas5.SettingsPager.PageSize = 100;

                    PrintingSystemBase ps = new PrintingSystemBase();
                    ps.XlSheetCreated += psLCMod_XlSheetCreated;

                    PrintableComponentLinkBase link1 = new PrintableComponentLinkBase(ps);
                    link1.PaperName = "Borrar";
                    link1.Component = gridExport1;

                    PrintableComponentLinkBase link2 = new PrintableComponentLinkBase(ps);
                    link2.PaperName = "BienServicio";
                    link2.Component = gridExport2;

                    PrintableComponentLinkBase link3 = new PrintableComponentLinkBase(ps);
                    link3.PaperName = "PC";
                    link3.Component = gridExport3;

                    PrintableComponentLinkBase link4 = new PrintableComponentLinkBase(ps);
                    link4.PaperName = "FE";
                    link4.Component = gridExport4;

                    PrintableComponentLinkBase link5 = new PrintableComponentLinkBase(ps);
                    link5.PaperName = "Editar";
                    link5.Component = gridExport5;

                    CompositeLinkBase compositeLink = new CompositeLinkBase(ps);
                    compositeLink.Links.AddRange(new object[] { link1, link2, link3, link4, link5 });

                    compositeLink.CreatePageForEachLink();

                    using (MemoryStream stream = new MemoryStream())
                    {
                        XlsxExportOptions options = new XlsxExportOptions();
                        options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                        compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                        Response.Clear();
                        Response.Buffer = false;
                        Response.AppendHeader("Content-Type", "application/xlsx");
                        Response.AppendHeader("Content-Transfer-Encoding", "binary");
                        Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=LibroComprasModifica{0}_{1}{2}.xlsx", mIniciales, mMes, mAnio));
                        Response.BinaryWrite(stream.ToArray());
                        Response.End();
                    }

                    ps.Dispose();
                }
                else
                {
                    gridLibroVentas1.DataSource = ds;
                    gridLibroVentas1.DataMember = "LibroCompras";
                    gridLibroVentas1.DataBind();
                    gridLibroVentas1.FocusedRowIndex = -1;
                    gridLibroVentas1.SettingsPager.PageSize = 100;

                    gridLibroVentas2.DataSource = ds;
                    gridLibroVentas2.DataMember = "Documentos";
                    gridLibroVentas2.DataBind();
                    gridLibroVentas2.FocusedRowIndex = -1;
                    gridLibroVentas2.SettingsPager.PageSize = 100;

                    gridLibroVentas3.DataSource = ds;
                    gridLibroVentas3.DataMember = "DMG";
                    gridLibroVentas3.DataBind();
                    gridLibroVentas3.FocusedRowIndex = -1;
                    gridLibroVentas3.SettingsPager.PageSize = 100;

                    gridLibroVentas4.DataSource = ds;
                    gridLibroVentas4.DataMember = "DMGVentas";
                    gridLibroVentas4.DataBind();
                    gridLibroVentas4.FocusedRowIndex = -1;
                    gridLibroVentas4.SettingsPager.PageSize = 100;

                    gridLibroVentas8.DataSource = ds;
                    gridLibroVentas8.DataMember = "Resumen";
                    gridLibroVentas8.DataBind();
                    gridLibroVentas8.FocusedRowIndex = -1;
                    gridLibroVentas8.SettingsPager.PageSize = 100;

                    gridLibroVentas8.Settings.ShowColumnHeaders = false;

                    gridLibroVentas8.Columns["Referencia"].Visible = false;
                    gridLibroVentas8.Columns["Columna2"].Width = 300;
                    gridLibroVentas8.Columns["Columna3"].Width = 300;
                    gridLibroVentas8.Columns["Columna4"].Width = 300;
                    gridLibroVentas8.Columns["Columna7"].Width = 750;
                    gridLibroVentas8.Columns["Columna8"].Width = 300;
                    gridLibroVentas8.Columns["Columna9"].Width = 300;

                    PrintingSystemBase ps = new PrintingSystemBase();
                    ps.XlSheetCreated += psLC_XlSheetCreated;

                    PrintableComponentLinkBase link1 = new PrintableComponentLinkBase(ps);
                    link1.PaperName = "LibroCompras";
                    link1.Component = gridExport1;

                    if (asl)
                    {
                        gridLibroVentas1.Settings.ShowColumnHeaders = false;

                        CompositeLinkBase compositeLink = new CompositeLinkBase(ps);
                        compositeLink.Links.AddRange(new object[] { link1 });

                        compositeLink.CreateDocument();

                        using (MemoryStream stream = new MemoryStream())
                        {
                            CsvExportOptions options = new CsvExportOptions();
                            compositeLink.ExportToCsv(stream, new CsvExportOptionsEx() { Separator = "|", ExportType = DevExpress.Export.ExportType.WYSIWYG });
                            Response.Clear();
                            Response.Buffer = false;
                            Response.AppendHeader("Content-Type", "application/asl");
                            Response.AppendHeader("Content-Transfer-Encoding", "binary");
                            Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=Carga{0} {1} {2} C.asl", mIniciales, mMes, mAnio));
                            Response.BinaryWrite(stream.ToArray());
                            Response.End();
                        }
                    }
                    else
                    {
                        PrintableComponentLinkBase link2 = new PrintableComponentLinkBase(ps);
                        link2.PaperName = "Documentos";
                        link2.Component = gridExport2;

                        PrintableComponentLinkBase link3 = new PrintableComponentLinkBase(ps);
                        link3.PaperName = "DMG";
                        link3.Component = gridExport3;

                        PrintableComponentLinkBase link4 = new PrintableComponentLinkBase(ps);
                        link4.PaperName = "DMGVentas";
                        link4.Component = gridExport4;

                        PrintableComponentLinkBase link8 = new PrintableComponentLinkBase(ps);
                        link8.PaperName = "Resumen";
                        link8.Component = gridExport8;

                        CompositeLinkBase compositeLink = new CompositeLinkBase(ps);
                        compositeLink.Links.AddRange(new object[] { link1, link2, link3, link4, link8 });

                        compositeLink.CreatePageForEachLink();

                        using (MemoryStream stream = new MemoryStream())
                        {
                            XlsxExportOptions options = new XlsxExportOptions();
                            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                            compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                            Response.Clear();
                            Response.Buffer = false;
                            Response.AppendHeader("Content-Type", "application/xlsx");
                            Response.AppendHeader("Content-Transfer-Encoding", "binary");
                            Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=LibroCompras{0}_{1}{2}.xlsx", mIniciales, mMes, mAnio));
                            Response.BinaryWrite(stream.ToArray());
                            Response.End();
                        }
                    }

                    ps.Dispose();
                    gridLibroVentas8.Settings.ShowColumnHeaders = true;
                }

            }
            catch (Exception ex)
            {
                lbErrorLibroCompras.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnLibroCompras_Click(object sender, EventArgs e)
        {
            LibroCompras(false, false);
        }

        protected void btnLibroComprasASL_Click(object sender, EventArgs e)
        {
            LibroCompras(true, false);
        }

        protected void btnLibroComprasPlantilla_Click(object sender, EventArgs e)
        {
            LibroCompras(false, true);
        }

        protected void btnLibroComprasModifica_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoLibroCompras.Text = "";
                lbErrorLibroCompras.Text = "";

                string mFile = "";
                try
                {
                    mFile = uploadFile.UploadedFiles[0].FileName;
                }
                catch
                {
                    mFile = "";
                }

                if (mFile.Trim().Length == 0)
                {
                    lbErrorLibroVentas.Text = "No se ha cargado ningún archivo";
                    return;
                }

                int mAnio = 0; int mNumeroMes = 0;

                try
                {
                    mAnio = Convert.ToInt32(txtAnioLV.Text);
                }
                catch
                {
                    lbErrorLibroCompras.Text = "Ingrese un año válido";
                    return;
                }

                if (mAnio < 2010)
                {
                    lbErrorLibroCompras.Text = "Ingrese un año válido";
                    return;
                }

                try
                {
                    mNumeroMes = Convert.ToInt32(cbMesLV.SelectedItem.Value);
                }
                catch
                {
                    lbErrorLibroCompras.Text = "Debe seleccionar un mes";
                    cbMesLV.Focus();
                    return;
                }

                if (mNumeroMes < 0 || mNumeroMes > 12)
                {
                    lbErrorLibroCompras.Text = "Debe seleccionar un mes";
                    cbMesLV.Focus();
                    return;
                }


                string mMes = cbMesLV.SelectedItem.Text; DateTime mFecha = new DateTime(mAnio, mNumeroMes, 1);
                string mFechaHoyString = string.Format("{0}{1}{2}{3}{4}", DateTime.Now.Day < 10 ? "0" : "", DateTime.Now.Day, DateTime.Now.Month < 10 ? "0" : "", DateTime.Now.Month, DateTime.Now.Year);

                string mArchivo = @"C:\reportes\Contabilidad\LC_Modifica_" + mMes + mAnio + ".xlsx";

                try
                {
                    File.Delete(mArchivo);
                }
                catch
                {
                    //Nothing
                }

                uploadFile.UploadedFiles[0].SaveAs(mArchivo);

                string mQuery = "SELECT Documento, Serie, Numero, Fecha, NIT, Nombre, Columna, IVA, [Total Documento] AS TotalDocumento, Modulo, [Bien Servicio] AS BienServicio, Proveedor, [Caja Chica] AS CajaChica, [Vale CH] AS ValeCH, Vale, Doc FROM ";
                System.Data.OleDb.OleDbConnection mConexion = new System.Data.OleDb.OleDbConnection(string.Format("Provider=Microsoft.ACE.OLEDB.12.0; data source={0}; Extended Properties=Excel 8.0;", mArchivo));
                System.Data.OleDb.OleDbDataAdapter daBorrar = new System.Data.OleDb.OleDbDataAdapter(string.Format("{0} [Borrar$]", mQuery), mConexion);
                System.Data.OleDb.OleDbDataAdapter daBienServicio = new System.Data.OleDb.OleDbDataAdapter(string.Format("{0} [BienServicio$]", mQuery), mConexion);
                System.Data.OleDb.OleDbDataAdapter daPC = new System.Data.OleDb.OleDbDataAdapter(string.Format("{0} [PC$]", mQuery), mConexion);
                System.Data.OleDb.OleDbDataAdapter daFE = new System.Data.OleDb.OleDbDataAdapter(string.Format("{0} [FE$]", mQuery), mConexion);
                System.Data.OleDb.OleDbDataAdapter daEditar = new System.Data.OleDb.OleDbDataAdapter(string.Format("{0} [Editar$]", mQuery), mConexion);

                DataTable dtBorrar = new DataTable("Borrar");
                DataTable dtBienServicio = new DataTable("BienServicio");
                DataTable dtPC = new DataTable("PC");
                DataTable dtFE = new DataTable("FE");
                DataTable dtEditar = new DataTable("Editar");

                try
                {
                    daBorrar.Fill(dtBorrar);
                }
                catch
                {
                    lbErrorLibroCompras.Text = "El archivo no contiene una hoja llamada Borrar o bien las columnas no son las correctas, por favor descargue la plantilla";
                    return;
                }

                try
                {
                    daBienServicio.Fill(dtBienServicio);
                }
                catch
                {
                    lbErrorLibroCompras.Text = "El archivo no contiene una hoja llamada BienServicio o bien las columnas no son las correctas, por favor descargue la plantilla";
                    return;
                }

                try
                {
                    daPC.Fill(dtPC);
                }
                catch
                {
                    lbErrorLibroCompras.Text = "El archivo no contiene una hoja llamada PC o bien las columnas no son las correctas, por favor descargue la plantilla";
                    return;
                }

                try
                {
                    daFE.Fill(dtFE);
                }
                catch
                {
                    lbErrorLibroCompras.Text = "El archivo no contiene una hoja llamada FE o bien las columnas no son las correctas, por favor descargue la plantilla";
                    return;
                }

                try
                {
                    daEditar.Fill(dtEditar);
                }
                catch
                {
                    lbErrorLibroCompras.Text = "El archivo no contiene una hoja llamada Editar o bien las columnas no son las correctas, por favor descargue la plantilla";
                    return;
                }

                if (dtBorrar.Rows.Count == 0 && dtBienServicio.Rows.Count == 0 && dtPC.Rows.Count == 0)
                {
                    lbErrorLibroCompras.Text = "No se encontraron documentos para modificar el libro de compras";
                    return;
                }

                Clases.LibroComprasMod mLibroCompras = new Clases.LibroComprasMod();

                //Documentos a borrar
                for (int ii = 0; ii < dtBorrar.Rows.Count; ii++)
                {
                    string mDocumento = "";

                    try
                    {
                        mDocumento = Convert.ToString(dtBorrar.Rows[ii]["Documento"]);
                        if (mDocumento == null) mDocumento = "";
                    }
                    catch
                    {
                        mDocumento = "";
                    }

                    if (mDocumento.Trim().Length > 0)
                    {
                        string mProveedor = "";
                        string mCajaChica = "";
                        int mValeCH = 0;
                        int mVale = 0;

                        try
                        {
                            mProveedor = Convert.ToString(dtBorrar.Rows[ii]["Proveedor"]);
                        }
                        catch
                        {
                            mProveedor = "";
                        }
                        try
                        {
                            mCajaChica = Convert.ToString(dtBorrar.Rows[ii]["CajaChica"]);
                        }
                        catch
                        {
                            mCajaChica = "";
                        }
                        try
                        {
                            mValeCH = Convert.ToInt32(dtBorrar.Rows[ii]["ValeCH"]);
                        }
                        catch
                        {
                            mValeCH = 0;
                        }
                        try
                        {
                            mVale = Convert.ToInt32(dtBorrar.Rows[ii]["Vale"]);
                        }
                        catch
                        {
                            mVale = 0;
                        }

                        Clases.LibroCompras mItem = new Clases.LibroCompras
                        {
                            Documento = mDocumento,
                            Serie = Convert.ToString(dtBorrar.Rows[ii]["Serie"]),
                            Numero = Convert.ToString(dtBorrar.Rows[ii]["Numero"]),
                            Fecha = Convert.ToDateTime(dtBorrar.Rows[ii]["Fecha"]),
                            NIT = Convert.ToString(dtBorrar.Rows[ii]["NIT"]),
                            Nombre = Convert.ToString(dtBorrar.Rows[ii]["Nombre"]),
                            Columna = Convert.ToString(dtBorrar.Rows[ii]["Columna"]),
                            IVA = Convert.ToDecimal(dtBorrar.Rows[ii]["IVA"]),
                            TotalDocumento = Convert.ToDecimal(dtBorrar.Rows[ii]["TotalDocumento"]),
                            Modulo = Convert.ToString(dtBorrar.Rows[ii]["Modulo"]),
                            BienServicio = Convert.ToString(dtBorrar.Rows[ii]["BienServicio"]),
                            Proveedor = mProveedor,
                            CajaChica = mCajaChica,
                            ValeCH = mValeCH,
                            Vale = mVale,
                            Doc = Convert.ToString(dtBorrar.Rows[ii]["Doc"])
                        };
                        mLibroCompras.Borrar.Add(mItem);
                    }
                }

                //Bienes y Servicios
                for (int ii = 0; ii < dtBienServicio.Rows.Count; ii++)
                {
                    string mDocumento = "";

                    try
                    {
                        mDocumento = Convert.ToString(dtBienServicio.Rows[ii]["Documento"]);
                        if (mDocumento == null) mDocumento = "";
                    }
                    catch
                    {
                        mDocumento = "";
                    }

                    if (mDocumento.Trim().Length > 0)
                    {
                        string mProveedor = "";
                        string mCajaChica = "";
                        int mValeCH = 0;
                        int mVale = 0;

                        try
                        {
                            mProveedor = Convert.ToString(dtBienServicio.Rows[ii]["Proveedor"]);
                        }
                        catch
                        {
                            mProveedor = "";
                        }
                        try
                        {
                            mCajaChica = Convert.ToString(dtBienServicio.Rows[ii]["CajaChica"]);
                        }
                        catch
                        {
                            mCajaChica = "";
                        }
                        try
                        {
                            mValeCH = Convert.ToInt32(dtBienServicio.Rows[ii]["ValeCH"]);
                        }
                        catch
                        {
                            mValeCH = 0;
                        }
                        try
                        {
                            mVale = Convert.ToInt32(dtBienServicio.Rows[ii]["Vale"]);
                        }
                        catch
                        {
                            mVale = 0;
                        }

                        Clases.LibroCompras mItem = new Clases.LibroCompras
                        {
                            Documento = mDocumento,
                            Serie = Convert.ToString(dtBienServicio.Rows[ii]["Serie"]),
                            Numero = Convert.ToString(dtBienServicio.Rows[ii]["Numero"]),
                            Fecha = Convert.ToDateTime(dtBienServicio.Rows[ii]["Fecha"]),
                            NIT = Convert.ToString(dtBienServicio.Rows[ii]["NIT"]),
                            Nombre = Convert.ToString(dtBienServicio.Rows[ii]["Nombre"]),
                            Columna = Convert.ToString(dtBienServicio.Rows[ii]["Columna"]),
                            IVA = Convert.ToDecimal(dtBienServicio.Rows[ii]["IVA"]),
                            TotalDocumento = Convert.ToDecimal(dtBienServicio.Rows[ii]["TotalDocumento"]),
                            Modulo = Convert.ToString(dtBienServicio.Rows[ii]["Modulo"]),
                            BienServicio = Convert.ToString(dtBienServicio.Rows[ii]["BienServicio"]),
                            Proveedor = mProveedor,
                            CajaChica = mCajaChica,
                            ValeCH = mValeCH,
                            Vale = mVale,
                            Doc = Convert.ToString(dtBienServicio.Rows[ii]["Doc"])
                        };
                        mLibroCompras.BienServicio.Add(mItem);
                    }
                }

                //Pequeño contribuyente
                for (int ii = 0; ii < dtPC.Rows.Count; ii++)
                {
                    string mDocumento = "";

                    try
                    {
                        mDocumento = Convert.ToString(dtPC.Rows[ii]["Documento"]);
                        if (mDocumento == null) mDocumento = "";
                    }
                    catch
                    {
                        mDocumento = "";
                    }

                    if (mDocumento.Trim().Length > 0)
                    {
                        string mProveedor = "";
                        string mCajaChica = "";
                        int mValeCH = 0;
                        int mVale = 0;

                        try
                        {
                            mProveedor = Convert.ToString(dtPC.Rows[ii]["Proveedor"]);
                        }
                        catch
                        {
                            mProveedor = "";
                        }
                        try
                        {
                            mCajaChica = Convert.ToString(dtPC.Rows[ii]["CajaChica"]);
                        }
                        catch
                        {
                            mCajaChica = "";
                        }
                        try
                        {
                            mValeCH = Convert.ToInt32(dtPC.Rows[ii]["ValeCH"]);
                        }
                        catch
                        {
                            mValeCH = 0;
                        }
                        try
                        {
                            mVale = Convert.ToInt32(dtPC.Rows[ii]["Vale"]);
                        }
                        catch
                        {
                            mVale = 0;
                        }

                        Clases.LibroCompras mItem = new Clases.LibroCompras
                        {
                            Documento = mDocumento,
                            Serie = Convert.ToString(dtPC.Rows[ii]["Serie"]),
                            Numero = Convert.ToString(dtPC.Rows[ii]["Numero"]),
                            Fecha = Convert.ToDateTime(dtPC.Rows[ii]["Fecha"]),
                            NIT = Convert.ToString(dtPC.Rows[ii]["NIT"]),
                            Nombre = Convert.ToString(dtPC.Rows[ii]["Nombre"]),
                            Columna = Convert.ToString(dtPC.Rows[ii]["Columna"]),
                            IVA = Convert.ToDecimal(dtPC.Rows[ii]["IVA"]),
                            TotalDocumento = Convert.ToDecimal(dtPC.Rows[ii]["TotalDocumento"]),
                            Modulo = Convert.ToString(dtPC.Rows[ii]["Modulo"]),
                            BienServicio = Convert.ToString(dtPC.Rows[ii]["BienServicio"]),
                            Proveedor = mProveedor,
                            CajaChica = mCajaChica,
                            ValeCH = mValeCH,
                            Vale = mVale,
                            Doc = Convert.ToString(dtPC.Rows[ii]["Doc"])
                        };
                        mLibroCompras.PC.Add(mItem);
                    }
                }

                //Factura especial
                for (int ii = 0; ii < dtFE.Rows.Count; ii++)
                {
                    string mDocumento = "";

                    try
                    {
                        mDocumento = Convert.ToString(dtFE.Rows[ii]["Documento"]);
                        if (mDocumento == null) mDocumento = "";
                    }
                    catch
                    {
                        mDocumento = "";
                    }

                    if (mDocumento.Trim().Length > 0)
                    {
                        string mProveedor = "";
                        string mCajaChica = "";
                        int mValeCH = 0;
                        int mVale = 0;

                        try
                        {
                            mProveedor = Convert.ToString(dtFE.Rows[ii]["Proveedor"]);
                        }
                        catch
                        {
                            mProveedor = "";
                        }
                        try
                        {
                            mCajaChica = Convert.ToString(dtFE.Rows[ii]["CajaChica"]);
                        }
                        catch
                        {
                            mCajaChica = "";
                        }
                        try
                        {
                            mValeCH = Convert.ToInt32(dtFE.Rows[ii]["ValeCH"]);
                        }
                        catch
                        {
                            mValeCH = 0;
                        }
                        try
                        {
                            mVale = Convert.ToInt32(dtFE.Rows[ii]["Vale"]);
                        }
                        catch
                        {
                            mVale = 0;
                        }

                        Clases.LibroCompras mItem = new Clases.LibroCompras
                        {
                            Documento = mDocumento,
                            Serie = Convert.ToString(dtFE.Rows[ii]["Serie"]),
                            Numero = Convert.ToString(dtFE.Rows[ii]["Numero"]),
                            Fecha = Convert.ToDateTime(dtFE.Rows[ii]["Fecha"]),
                            NIT = Convert.ToString(dtFE.Rows[ii]["NIT"]),
                            Nombre = Convert.ToString(dtFE.Rows[ii]["Nombre"]),
                            Columna = Convert.ToString(dtFE.Rows[ii]["Columna"]),
                            IVA = Convert.ToDecimal(dtFE.Rows[ii]["IVA"]),
                            TotalDocumento = Convert.ToDecimal(dtFE.Rows[ii]["TotalDocumento"]),
                            Modulo = Convert.ToString(dtFE.Rows[ii]["Modulo"]),
                            BienServicio = Convert.ToString(dtFE.Rows[ii]["BienServicio"]),
                            Proveedor = mProveedor,
                            CajaChica = mCajaChica,
                            ValeCH = mValeCH,
                            Vale = mVale,
                            Doc = Convert.ToString(dtFE.Rows[ii]["Doc"])
                        };
                        mLibroCompras.FE.Add(mItem);
                    }
                }

                //Editar
                for (int ii = 0; ii < dtEditar.Rows.Count; ii++)
                {
                    string mDocumento = "";

                    try
                    {
                        mDocumento = Convert.ToString(dtEditar.Rows[ii]["Documento"]);
                        if (mDocumento == null) mDocumento = "";
                    }
                    catch
                    {
                        mDocumento = "";
                    }

                    if (mDocumento.Trim().Length > 0)
                    {
                        string mProveedor = "";
                        string mCajaChica = "";
                        int mValeCH = 0;
                        int mVale = 0;

                        try
                        {
                            mProveedor = Convert.ToString(dtEditar.Rows[ii]["Proveedor"]);
                        }
                        catch
                        {
                            mProveedor = "";
                        }
                        try
                        {
                            mCajaChica = Convert.ToString(dtEditar.Rows[ii]["CajaChica"]);
                        }
                        catch
                        {
                            mCajaChica = "";
                        }
                        try
                        {
                            mValeCH = Convert.ToInt32(dtEditar.Rows[ii]["ValeCH"]);
                        }
                        catch
                        {
                            mValeCH = 0;
                        }
                        try
                        {
                            mVale = Convert.ToInt32(dtEditar.Rows[ii]["Vale"]);
                        }
                        catch
                        {
                            mVale = 0;
                        }

                        Clases.LibroCompras mItem = new Clases.LibroCompras
                        {
                            Documento = mDocumento,
                            Serie = Convert.ToString(dtEditar.Rows[ii]["Serie"]),
                            Numero = Convert.ToString(dtEditar.Rows[ii]["Numero"]),
                            Fecha = Convert.ToDateTime(dtEditar.Rows[ii]["Fecha"]),
                            NIT = Convert.ToString(dtEditar.Rows[ii]["NIT"]),
                            Nombre = Convert.ToString(dtEditar.Rows[ii]["Nombre"]),
                            Columna = Convert.ToString(dtEditar.Rows[ii]["Columna"]),
                            IVA = Convert.ToDecimal(dtEditar.Rows[ii]["IVA"]),
                            TotalDocumento = Convert.ToDecimal(dtEditar.Rows[ii]["TotalDocumento"]),
                            Modulo = Convert.ToString(dtEditar.Rows[ii]["Modulo"]),
                            BienServicio = Convert.ToString(dtEditar.Rows[ii]["BienServicio"]),
                            Proveedor = mProveedor,
                            CajaChica = mCajaChica,
                            ValeCH = mValeCH,
                            Vale = mVale,
                            Doc = Convert.ToString(dtEditar.Rows[ii]["Doc"])
                        };
                        mLibroCompras.Editar.Add(mItem);
                    }
                }

                mLibroCompras.Usuario = Convert.ToString(Session["Usuario"]);

                string json = JsonConvert.SerializeObject(mLibroCompras);
                byte[] data = Encoding.ASCII.GetBytes(json);

                WebRequest request = WebRequest.Create(string.Format("{0}/LibroCompras/", Convert.ToString(Session["UrlRestServices"])));

                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string mRetorna = responseString.ToString().Replace("\"", "");
                if (mRetorna.ToLower().Contains("error"))
                {
                    lbErrorLibroCompras.Text = mRetorna;
                    return;
                }

                lbInfoLibroCompras.Text = mRetorna;
            }
            catch (Exception ex)
            {
                lbErrorLibroCompras.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnGenerarEF_Click(object sender, EventArgs e)
        {
            LlamarEstadosFinancieros(true);
        }

        protected void btnDescargarEF_Click(object sender, EventArgs e)
        {
            LlamarEstadosFinancieros(false);
        }

        void LlamarEstadosFinancieros(bool generar)
        {
            try
            {
                lbInfoEF.Text = "";
                lbErrorEF.Text = "";

                int mAnio = 0; int mNumeroMes = 0;

                try
                {
                    mAnio = Convert.ToInt32(txtAnioLV.Text);
                }
                catch
                {
                    lbErrorEF.Text = "Ingrese un año válido";
                    return;
                }

                if (mAnio < 2010)
                {
                    lbErrorEF.Text = "Ingrese un año válido";
                    return;
                }

                try
                {
                    mNumeroMes = Convert.ToInt32(cbMesLV.SelectedItem.Value);
                }
                catch
                {
                    lbErrorEF.Text = "Debe seleccionar un mes";
                    cbMesLV.Focus();
                    return;
                }

                if (mNumeroMes < 0 || mNumeroMes > 12)
                {
                    lbErrorEF.Text = "Debe seleccionar un mes";
                    cbMesLV.Focus();
                    return;
                }

                string mMes = "Enero"; DateTime mFecha = new DateTime(mAnio, mNumeroMes, 1);

                if (mFecha.Month == 2) mMes = "Febrero";
                if (mFecha.Month == 3) mMes = "Marzo";
                if (mFecha.Month == 4) mMes = "Abril";
                if (mFecha.Month == 5) mMes = "Mayo";
                if (mFecha.Month == 6) mMes = "Junio";
                if (mFecha.Month == 7) mMes = "Julio";
                if (mFecha.Month == 8) mMes = "Agosto";
                if (mFecha.Month == 9) mMes = "Septiembre";
                if (mFecha.Month == 10) mMes = "Octubre";
                if (mFecha.Month == 11) mMes = "Noviembre";
                if (mFecha.Month == 12) mMes = "Diciembre";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                DataSet ds = new DataSet(); string mMensaje = "";

                ws.Timeout = 999999999;
                ds = ws.EstadosFinancieros(ref mMensaje, txtAnioLV.Text, Convert.ToString(cbMesLV.SelectedItem.Value), cbEmpresaLV.SelectedItem.ToString().ToLower(), Convert.ToString(Session["Usuario"]), generar);

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorEF.Text = mMensaje;
                    return;
                }

                if (generar)
                {
                    if (Convert.ToString(ds.Tables["info"].Rows[0]["error"]) == "S")
                    {
                        lbErrorEF.Text = Convert.ToString(ds.Tables["info"].Rows[0]["mensaje"]);
                    }
                    else
                    {
                        lbInfoEF.Text = Convert.ToString(ds.Tables["info"].Rows[0]["mensaje"]);
                    }
                }
                else
                {
                    gridLibroVentas1.DataSource = ds;
                    gridLibroVentas1.DataMember = "ER";
                    gridLibroVentas1.DataBind();
                    gridLibroVentas1.FocusedRowIndex = -1;
                    gridLibroVentas1.SettingsPager.PageSize = 100;

                    gridLibroVentas2.DataSource = ds;
                    gridLibroVentas2.DataMember = "ER_Acumulado";
                    gridLibroVentas2.DataBind();
                    gridLibroVentas2.FocusedRowIndex = -1;
                    gridLibroVentas2.SettingsPager.PageSize = 100;

                    gridLibroVentas3.DataSource = ds;
                    gridLibroVentas3.DataMember = "ER_Detallado";
                    gridLibroVentas3.DataBind();
                    gridLibroVentas3.FocusedRowIndex = -1;
                    gridLibroVentas3.SettingsPager.PageSize = 100;

                    gridLibroVentas4.DataSource = ds;
                    gridLibroVentas4.DataMember = "ER_Acumulado_Detallado";
                    gridLibroVentas4.DataBind();
                    gridLibroVentas4.FocusedRowIndex = -1;
                    gridLibroVentas4.SettingsPager.PageSize = 100;

                    gridLibroVentas5.DataSource = ds;
                    gridLibroVentas5.DataMember = "BG";
                    gridLibroVentas5.DataBind();
                    gridLibroVentas5.FocusedRowIndex = -1;
                    gridLibroVentas5.SettingsPager.PageSize = 100;

                    gridLibroVentas6.DataSource = ds;
                    gridLibroVentas6.DataMember = "BG_Detallado";
                    gridLibroVentas6.DataBind();
                    gridLibroVentas6.FocusedRowIndex = -1;
                    gridLibroVentas6.SettingsPager.PageSize = 100;

                    gridLibroVentas7.DataSource = ds;
                    gridLibroVentas7.DataMember = "CentroCosto";
                    gridLibroVentas7.DataBind();
                    gridLibroVentas7.FocusedRowIndex = -1;
                    gridLibroVentas7.SettingsPager.PageSize = 100;

                    gridLibroVentas10.DataSource = ds;
                    gridLibroVentas10.DataMember = "CentroCostoAcum";
                    gridLibroVentas10.DataBind();
                    gridLibroVentas10.FocusedRowIndex = -1;
                    gridLibroVentas10.SettingsPager.PageSize = 100;

                    gridLibroVentas11.DataSource = ds;
                    gridLibroVentas11.DataMember = "CentroCostoAnioAnterior";
                    gridLibroVentas11.DataBind();
                    gridLibroVentas11.FocusedRowIndex = -1;
                    gridLibroVentas11.SettingsPager.PageSize = 100;

                    gridLibroVentas12.DataSource = ds;
                    gridLibroVentas12.DataMember = "CentroCostoAnioAnteriorAcum";
                    gridLibroVentas12.DataBind();
                    gridLibroVentas12.FocusedRowIndex = -1;
                    gridLibroVentas12.SettingsPager.PageSize = 100;

                    gridLibroVentas13.DataSource = ds;
                    gridLibroVentas13.DataMember = "info";
                    gridLibroVentas13.DataBind();
                    gridLibroVentas13.FocusedRowIndex = -1;
                    gridLibroVentas13.SettingsPager.PageSize = 100;

                    string mIniciales = "";
                    if (cbEmpresaLV.SelectedItem.ToString().ToLower() == "emprconc") mIniciales = "EC";

                    PrintingSystemBase ps = new PrintingSystemBase();
                    ps.XlSheetCreated += psEF_XlSheetCreated;

                    PrintableComponentLinkBase link1 = new PrintableComponentLinkBase(ps);
                    link1.PaperName = "ER";
                    link1.Component = gridExport1;

                    PrintableComponentLinkBase link2 = new PrintableComponentLinkBase(ps);
                    link2.PaperName = "ER_Acumulado";
                    link2.Component = gridExport2;

                    PrintableComponentLinkBase link3 = new PrintableComponentLinkBase(ps);
                    link3.PaperName = "ER_Detallado";
                    link3.Component = gridExport3;

                    PrintableComponentLinkBase link4 = new PrintableComponentLinkBase(ps);
                    link4.PaperName = "ER_Acumulado_Detallado";
                    link4.Component = gridExport4;

                    PrintableComponentLinkBase link5 = new PrintableComponentLinkBase(ps);
                    link5.PaperName = "BG";
                    link5.Component = gridExport5;

                    PrintableComponentLinkBase link6 = new PrintableComponentLinkBase(ps);
                    link6.PaperName = "BG_Detallado";
                    link6.Component = gridExport6;

                    PrintableComponentLinkBase link7 = new PrintableComponentLinkBase(ps);
                    link7.PaperName = "CentroCosto";
                    link7.Component = gridExport7;

                    PrintableComponentLinkBase link10 = new PrintableComponentLinkBase(ps);
                    link10.PaperName = "CentroCostoAcum";
                    link10.Component = gridExport10;

                    PrintableComponentLinkBase link11 = new PrintableComponentLinkBase(ps);
                    link11.PaperName = "CentroCostoAnioAnterior";
                    link11.Component = gridExport11;

                    PrintableComponentLinkBase link12 = new PrintableComponentLinkBase(ps);
                    link12.PaperName = "CentroCostoAnioAnteriorAcum";
                    link12.Component = gridExport12;

                    PrintableComponentLinkBase link13 = new PrintableComponentLinkBase(ps);
                    link13.PaperName = "Info";
                    link13.Component = gridExport13;

                    CompositeLinkBase compositeLink = new CompositeLinkBase(ps);
                    compositeLink.Links.AddRange(new object[] { link1, link2, link3, link4, link5, link6, link7, link10, link11, link12, link13 });

                    compositeLink.CreatePageForEachLink();

                    using (MemoryStream stream = new MemoryStream())
                    {
                        XlsxExportOptions options = new XlsxExportOptions();
                        options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                        compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                        Response.Clear();
                        Response.Buffer = false;
                        Response.AppendHeader("Content-Type", "application/xlsx");
                        Response.AppendHeader("Content-Transfer-Encoding", "binary");
                        Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=EFinancieros{0}_{1}{2}.xlsx", mIniciales, mMes, mAnio));
                        Response.BinaryWrite(stream.ToArray());
                        Response.End();
                    }

                    ps.Dispose();
                }
            }
            catch (Exception ex)
            {
                lbErrorEF.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        void TomaInventario(bool excel, string orderBy = null)
        {
            try
            {
                lbInfoInventarios.Text = "";
                lbErrorInventarios.Text = "";

                bool agrupa = false;
                string mBodega = cbBodega.Value.ToString();
                string url = $"{ General.FiestaNetRestService}/TomaInventario/?bodega={mBodega}&usuario={Convert.ToString(Session["Usuario"])}";
                bool order = !string.IsNullOrEmpty(orderBy) && !string.IsNullOrWhiteSpace(orderBy);

                if (order)
                {
                    agrupa = orderBy == "CLASIFICACION";

                    if (agrupa)
                        url = url + $"&orderBy={orderBy}";
                }

                List<int> selected = new List<int>();
                if (chkFiltrarInvRep.Checked)
                {
                    foreach (ListEditItem item in lbClasificacion.SelectedItems)
                    {
                        selected.Add(int.Parse(item.Value.ToString()));
                    }

                    string param = JsonConvert.SerializeObject(selected);
                    url = url + $"&clasificacion={param}";
                }

                WebRequest request = WebRequest.Create(url);
                request.Method = "GET";
                request.ContentType = "application/json";
                request.Timeout = 9999999;

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                List<Clases.ArticuloInventario> mArticulos = new List<Clases.ArticuloInventario>();
                mArticulos = JsonConvert.DeserializeObject<List<Clases.ArticuloInventario>>(responseString);
                if (mArticulos.Count(x => x.AgrupacionEspecial != null) > 0)
                {
                    agrupa = true;
                    mArticulos.ForEach(x =>
                    {
                        x.Clasificacion = x.AgrupacionEspecial != null ? x.AgrupacionEspecial : "";
                    });
                }

                dsPuntoVenta ds = new dsPuntoVenta();
                foreach (var item in mArticulos)
                {
                    DataRow mRow = ds.ArticulosInventario.NewRow();
                    mRow["Articulo"] = item.Articulo;
                    mRow["Descripcion"] = item.Descripcion;
                    mRow["Localizacion"] = "";
                    mRow["Clasificacion"] = item.Clasificacion;
                    mRow["UltimaCompra"] = item.UltimaCompra;
                    mRow["Blanco1"] = item.Blanco1;
                    mRow["Existencias"] = item.Existencias;
                    mRow["Blanco2"] = item.Blanco2;
                    mRow["ExistenciaFisica"] = item.ExistenciaFisica;
                    mRow["Conteo"] = item.Conteo;
                    mRow["Observaciones"] = item.Observaciones;
                    mRow["Tipo"] = item.Tipo;
                    ds.ArticulosInventario.Rows.Add(mRow);
                }

                DateTime mFecha = DateTime.Now.Date;
                using (ReportDocument reporte = new ReportDocument())
                {
                    string mReporte = order ? "rptTomaInventario.rpt" : "rptTomaInventarioNormal.rpt";
                    if (excel) mReporte = "rptInventarioExcel.rpt";


                    string p = (Request.PhysicalApplicationPath + "reportes/" + mReporte);

                    reporte.Load(p);

                    string mNombreDocumento = string.Format("Inventario{0}_{1}-{2}-{3}.pdf", mBodega, mFecha.Day, mFecha.Month, mFecha.Year);

                    try
                    {
                        if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                    }
                    catch
                    {
                        try
                        {
                            mNombreDocumento = mNombreDocumento.Replace(".pdf", "_1.pdf");
                            if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                        }
                        catch
                        {
                            try
                            {
                                mNombreDocumento = mNombreDocumento.Replace(".pdf", "_1.pdf");
                                if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                            }
                            catch
                            {
                                mNombreDocumento = mNombreDocumento.Replace(".pdf", "_1.pdf");
                                if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                            }
                        }
                    }

                    reporte.SetDataSource(ds);
                    reporte.SetParameterValue("Bodega", cbBodega.Text);
                    reporte.SetParameterValue("Titulo", string.Format("Inventario al {0}", mFecha.ToShortDateString()));

                    if (excel)
                    {
                        mNombreDocumento = mNombreDocumento.Replace(".pdf", ".xls");
                        reporte.ExportToDisk(ExportFormatType.ExcelRecord, @"C:\reportes\" + mNombreDocumento);
                    }
                    else
                    {
                        reporte.SetParameterValue("Agrupar", agrupa);
                        reporte.ExportToDisk(ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);
                    }

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                lbErrorInventarios.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnExcelInventario_Click(object sender, EventArgs e)
        {
            TomaInventario(true);
        }

        protected void btnSubirExcel_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoInventarios.Text = "";
                lbErrorInventarios.Text = "";

                string mFile = "";
                try
                {
                    mFile = uploadFile.UploadedFiles[0].FileName;
                }
                catch
                {
                    mFile = "";
                }

                if (mFile.Trim().Length == 0)
                {
                    lbErrorInventarios.Text = "No se ha cargado ningún archivo";
                    return;
                }
                if (!mFile.Contains("xls"))
                {
                    lbErrorInventarios.Text = "Archivo inválido";
                    return;
                }

                string[] mNombre = mFile.Split(new string[] { "_" }, StringSplitOptions.None);
                string mBodega = mNombre[0].Replace("Inventario", "");
                string mArchivo = @"C:\reportes\Inventario" + cbMesLV.Text + txtAnioLV.Text + "_" + mBodega + ".xls";

                try
                {
                    File.Delete(mArchivo);
                }
                catch
                {
                    //Nothing
                }

                uploadFile.UploadedFiles[0].SaveAs(mArchivo);

                DataSet ds = new DataSet();
                System.Data.OleDb.OleDbConnection mConexion = new System.Data.OleDb.OleDbConnection(string.Format("Provider=Microsoft.ACE.OLEDB.12.0; data source={0}; Extended Properties=Excel 8.0;", mArchivo));
                System.Data.OleDb.OleDbDataAdapter da = new System.Data.OleDb.OleDbDataAdapter("SELECT Articulo, Descripcion, Conteo, Observaciones FROM [Sheet1$]", mConexion);

                da.Fill(ds);
                Clases.TomaInventario mInventario = new Clases.TomaInventario();

                mInventario.Mes = Convert.ToInt32(cbMesLV.Value);
                mInventario.Anio = Convert.ToInt32(txtAnioLV.Text);
                mInventario.Tipo = "XLS";
                mInventario.Bodega = mBodega;
                mInventario.Usuario = Convert.ToString(Session["Usuario"]);

                for (int ii = 0; ii < ds.Tables[0].Rows.Count; ii++)
                {
                    Clases.ArticuloInventario mItem = new Clases.ArticuloInventario
                    {
                        Articulo = ds.Tables[0].Rows[ii]["Articulo"].ToString(),
                        Descripcion = ds.Tables[0].Rows[ii]["Descripcion"].ToString(),
                        Conteo = ds.Tables[0].Rows[ii]["Conteo"].ToString(),
                        Observaciones = ds.Tables[0].Rows[ii]["Observaciones"].ToString()
                    };

                    mInventario.Articulos.Add(mItem);
                }

                string json = JsonConvert.SerializeObject(mInventario);
                byte[] data = Encoding.ASCII.GetBytes(json);

                WebRequest request = WebRequest.Create(string.Format("{0}/TomaInventario/", Convert.ToString(Session["UrlRestServices"])));

                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string mRetorna = responseString.ToString().Replace("\"", "");
                if (mRetorna.ToLower().Contains("error"))
                {
                    lbErrorInventarios.Text = mRetorna;
                    return;
                }

                gridDiferencias.Visible = false;
                lbInfoInventarios.Text = mRetorna;
            }
            catch (Exception ex)
            {
                lbErrorInventarios.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnExistencias_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoInventarios.Text = "";
                lbErrorInventarios.Text = "";

                string mBodega = cbBodega.Value.ToString();
                WebRequest request = WebRequest.Create(string.Format("{0}/ExistenciasMes/?bodega={1}&anio={2}&mes={3}", Convert.ToString(Session["UrlRestServices"]), mBodega, txtAnioLV.Text, cbMesLV.Value.ToString()));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string mRetorna = responseString.ToString().Replace("\"", "");
                if (mRetorna.ToLower().Contains("error"))
                {
                    lbErrorInventarios.Text = mRetorna;
                    return;
                }

                gridDiferencias.Visible = false;
                lbInfoInventarios.Text = mRetorna;
            }
            catch (Exception ex)
            {
                lbErrorInventarios.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        /// <summary>
        /// Este método recibe bodega, anio y mes procesar el cierre
        /// </summary>
        /// <param name="tipo">"P" es preliminar, "D" descarga el PDF preliminar, "C" es el cierre definitivo, "R" es re-envio del cierre</param>
        void CierreInventario(string tipo)
        {
            try
            {
                lbInfoInventarios.Text = "";
                lbErrorInventarios.Text = "";

                string mBodega = cbBodega.Value.ToString();
                WebRequest request = WebRequest.Create(string.Format("{0}/CierreInventario/?bodega={1}&anio={2}&mes={3}&usuario={4}&tipo={5}", Convert.ToString(Session["UrlRestServices"]), mBodega, txtAnioLV.Text, cbMesLV.Value.ToString(), Convert.ToString(Session["Usuario"]), tipo));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                Clases.TomaInventario mInventario = new Clases.TomaInventario();
                mInventario = JsonConvert.DeserializeObject<Clases.TomaInventario>(responseString);

                string mRetorna = mInventario.Mensaje.Replace("\"", "");
                if (mRetorna.ToLower().Contains("error"))
                {
                    lbErrorInventarios.Text = mRetorna;
                    return;
                }

                gridDiferencias.Visible = false;
                lbInfoInventarios.Text = mRetorna;

                if (mInventario.Diferencias.Count() > 0)
                {
                    gridDiferencias.Visible = true;
                    gridDiferencias.DataSource = mInventario.Diferencias.AsEnumerable();
                    gridDiferencias.DataMember = "Articulos";
                    gridDiferencias.DataBind();
                    gridDiferencias.FocusedRowIndex = -1;
                    gridDiferencias.SettingsPager.PageSize = 500;
                }

                if (!mInventario.ConsultaCierre && (tipo == "C" || tipo == "R" || tipo == "D"))
                {
                    string normal = GenerarArchivos(true, false, mBodega, mInventario.Fecha, mInventario.Articulos);
                    string clasificacion = GenerarArchivos(false, true, mBodega, mInventario.Fecha, mInventario.ArticulosClasificacion);
                    string codigo = GenerarArchivos(false, false, mBodega, mInventario.Fecha, mInventario.ArticulosCodigo);
                    if (tipo == "C" || tipo == "R")
                    {
                        using (SmtpClient smtp = new SmtpClient())
                        {
                            using (MailMessage mail = new MailMessage())
                            {
                                mail.IsBodyHtml = true;
                                mail.Subject = mInventario.Asunto;
                                mail.ReplyToList.Add(mInventario.CorreoUsuario);
                                mail.From = new MailAddress(mInventario.CorreoUsuario, Convert.ToString(Session["NombreVendedor"]));

                                mInventario.Destinatarios.ForEach(x =>
                                {
                                    mail.To.Add(x);
                                });

                                mail.Body = mInventario.Html;
                                mail.Attachments.Add(new Attachment(normal));
                                mail.Attachments.Add(new Attachment(clasificacion));
                                mail.Attachments.Add(new Attachment(codigo));

                                smtp.Host = WebConfigurationManager.AppSettings["MailHost"];
                                smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort"]);

                                smtp.EnableSsl = true;
                                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                                try
                                {
                                    smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail"], WebConfigurationManager.AppSettings["PwdMail"]);
                                    smtp.Send(mail);
                                }
                                catch (Exception ex)
                                {
                                    lbErrorInventarios.Text = "Error al enviar el correo";
                                }
                            }
                        }
                    }
                    else
                    {
                        Response.Clear();
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + Path.GetFileName(normal));
                        Response.WriteFile(normal);
                        Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                lbErrorInventarios.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private string GenerarArchivos(bool generaNormal, bool ordenxClasificacion, string bodega, DateTime fecha, List<ArticuloInventario> data)
        {
            using (ReportDocument reporte = new ReportDocument())
            {
                string nombre = "";
                string archivo = generaNormal ? "rptTomaInventarioNormal.rpt" : "rptTomaInventario.rpt";
                string path = $"{Request.PhysicalApplicationPath}reportes/{archivo}";
                reporte.Load(path);

                if (generaNormal)
                    nombre = $"Inventario{bodega}_{fecha.ToString("dd-MM-yyyy")}.pdf";
                else if (ordenxClasificacion)
                    nombre = $"Inventario{bodega}_{fecha.ToString("dd-MM-yyyy")}_OrdenClasificacion.pdf";
                else if (!ordenxClasificacion)
                    nombre = $"Inventario{bodega}_{fecha.ToString("dd-MM-yyyy")}_OrdenCodigo.pdf";

                string descarga = $@"C:\reportes\{nombre}";
                bool agrupa = generaNormal ? false : ordenxClasificacion;

                if (File.Exists(descarga))
                    File.Delete(descarga);

                reporte.SetDataSource(GetDS(data));
                reporte.SetParameterValue("Bodega", cbBodega.Text);
                reporte.SetParameterValue("Titulo", string.Format("Inventario al {0}", fecha.ToShortDateString()));
                reporte.SetParameterValue("Agrupar", agrupa);

                reporte.ExportToDisk(ExportFormatType.PortableDocFormat, descarga);
                return descarga;
            }
        }

        private dsPuntoVenta GetDS(List<ArticuloInventario> data)
        {
            dsPuntoVenta ds = new dsPuntoVenta();

            data.ForEach(x =>
            {
                DataRow mRow = ds.ArticulosInventario.NewRow();
                mRow["Articulo"] = x.Articulo;
                mRow["Descripcion"] = x.Descripcion;
                mRow["Localizacion"] = "";
                mRow["Clasificacion"] = x.Clasificacion;
                mRow["UltimaCompra"] = x.UltimaCompra;
                mRow["Blanco1"] = x.Blanco1;
                mRow["Existencias"] = x.Existencias;
                mRow["Blanco2"] = x.Blanco2;
                mRow["ExistenciaFisica"] = x.ExistenciaFisica;
                mRow["Conteo"] = x.Conteo;
                mRow["Observaciones"] = x.Observaciones;
                mRow["Tipo"] = x.Tipo;
                ds.ArticulosInventario.Rows.Add(mRow);
            });

            return ds;
        }

        protected void btnPreliminar_Click(object sender, EventArgs e)
        {
            CierreInventario("P");
        }

        protected void btnPreliminarPDF_Click(object sender, EventArgs e)
        {
            CierreInventario("D");
        }

        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            CierreInventario("C");
        }

        protected void btnReEnviarCierre_Click(object sender, EventArgs e)
        {
            CierreInventario("R");
        }

        void verCierres()
        {
            try
            {
                lbInfoInventarios.Text = "";
                lbErrorInventarios.Text = "";

                gridBodegas.Visible = false;

                string mBodega = cbBodega.Value.ToString();
                WebRequest request = WebRequest.Create(string.Format("{0}/AjustesCierre/?anio={1}&mes={2}", Convert.ToString(Session["UrlRestServices"]), txtAnioLV.Text, cbMesLV.Value.ToString()));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                List<Clases.AjustesInventario> mBodegas = new List<Clases.AjustesInventario>();
                mBodegas = JsonConvert.DeserializeObject<List<Clases.AjustesInventario>>(responseString);

                if (mBodegas.Count() > 0)
                {
                    gridBodegas.Visible = true;
                    gridBodegas.DataSource = mBodegas.AsEnumerable();
                    gridBodegas.DataMember = "AjustesInventario";
                    gridBodegas.DataBind();
                    gridBodegas.FocusedRowIndex = -1;
                    gridBodegas.SettingsPager.PageSize = 500;
                }
                else
                {
                    lbErrorInventarios.Text = "No se encontraron bodegas";
                }
            }
            catch (Exception ex)
            {
                lbErrorInventarios.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnVerTodas_Click(object sender, EventArgs e)
        {
            verCierres();
        }

        protected void btnReAbrir_Click(object sender, EventArgs e)
        {
        }

        protected void gridBodegas_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
        {
            try
            {
                lbInfoInventarios.Text = "";
                lbErrorInventarios.Text = "";

                WebRequest request = WebRequest.Create(string.Format("{0}/AjustesCierre/?bodega={1}&anio={2}&mes={3}", Convert.ToString(Session["UrlRestServices"]), e.KeyValue.ToString(), txtAnioLV.Text, cbMesLV.Value.ToString()));

                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = 0;

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string mRetorna = responseString.ToString().Replace("\"", "");
                if (mRetorna.ToLower().Contains("error"))
                {
                    lbErrorInventarios.Text = mRetorna;
                    return;
                }

                verCierres();
                lbInfoInventarios.Text = mRetorna;
            }
            catch (Exception ex)
            {
                lbErrorInventarios.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gridConsultas_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            try
            {
                if (e.DataColumn.FieldName.ToUpper() == "NOMBRE_FINANCIERA")
                {
                    string Nombre = e.GetValue(e.DataColumn.FieldName).ToString();
                    if (Nombre.ToUpper().Equals("INTERCONSUMO"))
                    {
                        e.Cell.CssClass = "tag_inter";
                    }
                    else if (Nombre.ToUpper().Equals("BANCREDIT"))
                    { e.Cell.CssClass = "tag_bancredit"; }
                    else
                        e.Cell.CssClass = "tag_other";
                }
            }
            catch
            { }
        }

        protected void gridConsultas_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            //si el formato es numérico o de fecha, modificar también el método gridConsultas_RenderBrick
            string fecha = string.Empty;
            DateTime dtFechaAformatear;
            try
            {
                int nrestar = 1;

                try
                {
                    //Darle formato a la fecha de la factura
                    fecha = e.GetValue("FECHA_FACTURA") == null ? "" : e.GetValue("FECHA_FACTURA").ToString();
                    dtFechaAformatear = DateTime.Parse(fecha);
                    e.Row.Cells[gridConsultas.Columns["FECHA_FACTURA"].Index - nrestar].Text = Utilitarios.FormatoDDMMYYYY(dtFechaAformatear);
                }
                catch
                { }
                try
                {
                    //Darle formato a la fecha de la solicitud
                    fecha = e.GetValue("FECHA_SOLICITUD") == null ? "" : e.GetValue("FECHA_SOLICITUD").ToString();
                    dtFechaAformatear = DateTime.Parse(fecha);
                    e.Row.Cells[gridConsultas.Columns["FECHA_SOLICITUD"].Index - nrestar].Text = Utilitarios.FormatoDDMMYYYY(dtFechaAformatear);
                }
                catch
                { }
                try
                {
                    //Darle formato a la fecha del rechazo
                    fecha = e.GetValue("FECHA_RECHAZO_SOLICITUD") == null ? "" : e.GetValue("FECHA_RECHAZO_SOLICITUD").ToString();
                    dtFechaAformatear = DateTime.Parse(fecha);
                    e.Row.Cells[gridConsultas.Columns["FECHA_RECHAZO_SOLICITUD"].Index - nrestar].Text = Utilitarios.FormatoDDMMYYYY(dtFechaAformatear);
                }
                catch
                { }
                try
                {
                    //Formato de moneda a los montos
                    var monto = e.GetValue("VALOR_FACTURA") == null ? "0" : e.GetValue("VALOR_FACTURA").ToString();
                    var flMonto = float.Parse(monto);
                    var cultureInfo = CultureInfo.GetCultureInfo("ES-GT");
                    var formattedAmount = String.Format(cultureInfo, "{0:C}", flMonto);
                    e.Row.Cells[gridConsultas.Columns["VALOR_FACTURA"].Index - nrestar].Text = formattedAmount;
                    e.Row.Cells[gridConsultas.Columns["VALOR_FACTURA"].Index - nrestar].HorizontalAlign = HorizontalAlign.Right;
                }
                catch
                { }
                try
                {
                    //Formato de moneda a los montos
                    var monto = e.GetValue("VALOR_ENGANCHE") == null ? "0" : e.GetValue("VALOR_ENGANCHE").ToString();
                    var dlMonto = double.Parse(monto);
                    var cultureInfo = CultureInfo.GetCultureInfo("ES-GT");
                    var formattedAmount = String.Format(cultureInfo, "{0:C}", dlMonto);
                    e.Row.Cells[gridConsultas.Columns["VALOR_ENGANCHE"].Index - nrestar].Text = formattedAmount;
                    e.Row.Cells[gridConsultas.Columns["VALOR_ENGANCHE"].Index - nrestar].HorizontalAlign = HorizontalAlign.Right;
                }
                catch
                { }
                try
                {
                    if (cmbOptSolEnviadas.SelectedIndex > 1)
                    {
                        //Formato de moneda a los montos
                        var monto = e.GetValue("VALOR_SALDO") == null ? "0" : e.GetValue("VALOR_SALDO").ToString();
                        var dlMonto = double.Parse(monto);
                        var cultureInfo = CultureInfo.GetCultureInfo("ES-GT");
                        var formattedAmount = String.Format(cultureInfo, "{0:C}", dlMonto);
                        e.Row.Cells[gridConsultas.Columns["VALOR_SALDO"].Index].Text = formattedAmount;
                        e.Row.Cells[gridConsultas.Columns["VALOR_SALDO"].Index].HorizontalAlign = HorizontalAlign.Right;
                    }
                }
                catch
                { }
                try
                {
                    //Formato de moneda a los montos

                    e.Row.Cells[gridConsultas.Columns["OBSERVACIONES"].Index - nrestar].Font.Size = FontUnit.Smaller;
                }
                catch
                { }
            }
            catch
            {

            }
        }

        protected void gridConsultas_PageIndexChanged(object sender, EventArgs e)
        {
            gridConsultas.FocusedRowIndex = -1;
            gridConsultas.DataSource = Session["dsRechazos"];
            gridConsultas.DataBind();
        }

        protected void gridConsultas_RenderBrick(object sender, ASPxGridViewExportRenderingEventArgs e)
        {
            try
            {

                if (e.RowType != GridViewRowType.Header)
                    e.BrickStyle.BorderWidth = 1;

                GridViewDataColumn dc = (GridViewDataColumn)e.Column;

                if (e.RowType == GridViewRowType.Data && e.TextValue != null)
                {
                    double mNumericValue = 0;
                    if (Double.TryParse(e.TextValue.ToString(), out mNumericValue))
                    {
                        if (dc.FieldName == "VALOR_FACTURA" || dc.FieldName == "VALOR_ENGANCHE" || dc.FieldName == "VALOR_SALDO")
                        {
                            e.XlsxFormatString = "#,##0.00;(#,##0.00)";
                            e.TextValue = mNumericValue;
                            e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center);
                        }


                    }
                    else
                    if (e.RowType == GridViewRowType.Data && dc != null)
                        if (dc.FieldName == "FECHA_FACTURA" || dc.FieldName == "FECHA_SOLICITUD" || dc.FieldName == "FECHA_RECHAZO_SOLICITUD")
                        {
                            e.TextValueFormatString = "dd/MM/yyyy";
                            e.TextValue = Utilitarios.FormatoDDMMYYYY(DateTime.Parse(e.Text));
                            e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                        }
                }
                else
                    if (e.RowType == GridViewRowType.Header)
                {
                    e.BrickStyle.BackColor = System.Drawing.Color.Red;
                    e.BrickStyle.ForeColor = System.Drawing.Color.White;
                    e.BrickStyle.Padding = 10;
                    e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                }


            }
            catch
            {
                //Nothing
            }
        }

        protected void GridExpedientesFinanciera_HtmlRowPrepared1(object sender, ASPxGridViewTableRowEventArgs e)
        {

        }

        protected void GridExpedientesFinanciera_DataBound(object sender, EventArgs e)
        {

        }

        protected void GridExpedientesFinanciera_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            try
            {
                if (e.DataColumn.FieldName == "NOMBRE_FINANCIERA")
                {
                    string Nombre = e.GetValue(e.DataColumn.FieldName).ToString();
                    if (Nombre.ToUpper().Equals("INTERCONSUMO"))
                    {
                        e.Cell.CssClass = "tag_inter";
                    }
                    else if (Nombre.ToUpper().Equals("BANCREDIT"))
                    { e.Cell.CssClass = "tag_bancredit"; }
                    else
                        e.Cell.CssClass = "tag_other";
                }
            }
            catch
            { }
        }

        protected void ASPxPageControl1_ActiveTabChanged(object source, TabControlEventArgs e)
        {

        }

        protected void btnPrueba_Click(object sender, EventArgs e)
        {
            var mDesembolsoInterconsumo = new MF_Clases.Clases.SolicitudInterconsumo();
            mDesembolsoInterconsumo.SOLICITUD = 10016349;
            mDesembolsoInterconsumo.VALOR_SOLICITUD = Decimal.Parse("5726.17000000");
            mDesembolsoInterconsumo.NUMERO_DOCUMENTO = "F07C-005662";
            mDesembolsoInterconsumo.PRIMER_NOMBRE = "JULIA";
            mDesembolsoInterconsumo.PRIMER_APELLIDO = "DIAZ";
            mDesembolsoInterconsumo.SEGUNDO_NOMBRE = "";
            mDesembolsoInterconsumo.SEGUNDO_APELLIDO = "";
            mDesembolsoInterconsumo.COBRADOR = "F04";
            mDesembolsoInterconsumo.PLAZO = 12;
            mDesembolsoInterconsumo.CUOTA = 169;

            string json = JsonConvert.SerializeObject(mDesembolsoInterconsumo);
            byte[] data = Encoding.ASCII.GetBytes(json);

            WebRequest request = WebRequest.Create(string.Format("{0}/ValidarSolicitudInterconsumo/", Convert.ToString(Session["UrlRestServices"])));



            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            //Respuesta 
            Respuesta mRetorna = new Respuesta();
            mRetorna = JsonConvert.DeserializeObject<Respuesta>(responseString);

            if (!mRetorna.Exito)
            {
                lbErrorFinanciera.Text = mRetorna.Mensaje;
                return;
            }
            else
            {
                lbInfoFinanciera.Text = mRetorna.Mensaje;
            }
        }

        protected void ASPxMenu1_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name != "pdf")
            {
                if (e.Item.Name == "normal")
                    TomaInventario(false);
                else
                    TomaInventario(false, e.Item.Name.ToUpper());
            }
        }

        protected void chkFiltrarInvRep_CheckedChanged(object sender, EventArgs e)
        {
            bool result = ((CheckBox)sender).Checked;

            if (result)
            {
                Api api = new Api(General.FiestaNetRestService);
                List<Clasificacion> data = JsonConvert
                    .DeserializeObject<List<Clasificacion>>(api.Process(Method.GET, $"/ClasificacionProductos", null));
                lbClasificacion.DataSource = data;
                lbClasificacion.DataBind();

                chkSelectAll.Checked = true;
                lbClasificacion.SelectAll();
            }

            lbClasificacion.Visible = result;
            chkSelectAll.Visible = result;
        }

        protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
                lbClasificacion.SelectAll();
            else
                lbClasificacion.UnselectAll();
        }
    }

}