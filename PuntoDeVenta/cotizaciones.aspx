﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="cotizaciones.aspx.cs" Inherits="PuntoDeVenta.cotizaciones" MaintainScrollPositionOnPostback="true" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>
<%@ Register assembly="DevExpress.Web.v16.2, Version=16.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
        }
        .style2
        {
            width: 7px;
        }
        .style1000
        {
            height: 22px;
        }
        .auto-style1 {
            width: 221px;
        }
        .auto-style2 {
            margin-top: 2px;
        }
        .auto-style3 {
            width: 117px;
        }
        </style>
    <script type='text/javascript'>
        function clearTextBox() {
            document.getElementById('<%=txtEnganche.ClientID%>').value = "";
         }
        function Forzar() {
            __doPostBack('', '');
        }

        function mousePointer2() {
             document.getElementById("Contenido").style.cursor = "default";
        }

        function mousePointer1() {
            document.getElementById("Contenido").style.cursor = "wait";
           
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>&nbsp;&nbsp;Cotizaciones</h3>
    <div class="content2" id="Contenido" onload="mousePointer2">
            <table id="tblInfo" runat="server" visible="false" align="center">
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="lkNuevoPedido" runat="server" 
                        ToolTip="Haga clic aquí para limpiar la página e ingresar datos para una nueva cotización" 
                        onclick="lkNuevoPedido_Click" TabIndex="675">Crear nueva cotización</asp:LinkButton>
                </td>
            </tr>
        </table>
    <div class="clientes">
    <ul>
        <li>
            <table align="center">
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        Cotización No.</td>
                    <td>
                    <asp:TextBox ID="txtPedido" runat="server" BackColor="White" BorderStyle="None" 
                        Enabled="False" Font-Bold="True" Font-Size="Medium" Width="80px" 
                        TabIndex="640" 
                            ToolTip="Este es el número de cotización, al ser una nueva, el número de cotización se generará al momento de grabarla.">P000000</asp:TextBox>
                    </td>
                    <td>
                        Fecha:</td>
                    <td>
                    <asp:TextBox ID="txtFecha" runat="server" BackColor="White" BorderStyle="None" 
                        Enabled="False" Font-Bold="True" Font-Size="Medium" Width="95px" 
                        TabIndex="640" 
                            ToolTip="Esta es la fecha de la cotización">23/04/2022</asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                    <asp:TextBox ID="txtValor" runat="server" BackColor="White" BorderStyle="None" 
                        Enabled="False" Font-Bold="True" Font-Size="Medium" Width="2px" 
                        TabIndex="640" ToolTip="Este es el monto del pedido en Quetzales (Q)" 
                            Visible="False"></asp:TextBox>
                    </td>
                    <td>
                        <asp:LinkButton ID="lbBuscarArticulo" runat="server" 
                            ToolTip="Haga clic aquí para buscar cotizaciones" 
                            TabIndex="650" onclick="lbBuscarArticulo_Click">Buscar cotizaciones</asp:LinkButton>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                        <asp:LinkButton ID="lkSolicitudes" runat="server" ToolTip="Haga clic aquí para consultar el estado de sus solicitudes de descuento" 
                            TabIndex="651" onclick="lkSolicitudes_Click">Solicitudes</asp:LinkButton>
                        <asp:LinkButton ID="lkAutorizar" runat="server" ToolTip="Haga clic aquí para autorizar el crédito en línea con Interconsumo" onclick="lkAutorizar_Click" Visible="false">Autorizar crédito</asp:LinkButton>
                    </td>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
            </table>
        </li>
        <li>
            <table align="center">
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        Nivel de Precio:</td>
                    <td colspan="6">
                    <asp:DropDownList ID="cbTipo" runat="server" Width="295px" 
                        ToolTip="Seleccione el tipo de promoción (Normal, Tarjeta Libre, La Torre, etc.)" 
                        TabIndex="8" 
                        onselectedindexchanged="cbTipo_SelectedIndexChanged" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:DropDownList ID="cbFinanciera" runat="server" Width="150px" 
                        TabIndex="10" 
                        ToolTip="Seleccione la financiera a utilizar" AutoPostBack="True" 
                        onselectedindexchanged="cbFinanciera_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:DropDownList ID="cbNivelPrecio" runat="server" Width="115px" 
                        TabIndex="12" ToolTip="Seleccione el nivel de precio" AutoPostBack="True" 
                        onselectedindexchanged="cbNivelPrecio_SelectedIndexChanged">
                    </asp:DropDownList>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td>
                        <asp:LinkButton ID="lkArticulo" runat="server" onclick="lkArticulo_Click" 
                            ToolTip="Haga clic aquí para buscar un artículo" TabIndex="23">Artículo:</asp:LinkButton>
                        <asp:LinkButton ID="lkVale" runat="server" 
                            ToolTip="Haga clic aquí para aplicar vale de Q200 de descuento" 
                            onclick="lkVale_Click" TabIndex="47" Visible="False">Vale</asp:LinkButton>
                        <asp:LinkButton ID="lkSolicitar" runat="server" 
                            ToolTip="Haga clic aquí para solicitar descuento especial a gerencia del artículo que se encuentra seleccionado" 
                            onclick="lkSolicitar_Click" TabIndex="48" Visible="False">Solicitar</asp:LinkButton>
                    </td>
                    <td>
                        <asp:TextBox ID="articulo" runat="server" Width="100px" MaxLength="20" 
                            TabIndex="24" AutoPostBack="True" ontextchanged="articulo_TextChanged" style="text-transform: uppercase;" 
                            
                            
                            ToolTip="Ingrese aquí el código del artículo que desea agregar a la cotización.  Puede ingresar parte del código y esto generará una búsqueda"></asp:TextBox>
                    </td>
                    <td colspan="5">
                        <asp:TextBox ID="descripcion" runat="server" Width="453px" 
                            style="text-transform: uppercase;" BackColor="AliceBlue" ReadOnly="True" 
                            ToolTip="Esta es la descripción del artículo" TabIndex="700"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:LinkButton ID="lbLocalizacion" runat="server" 
                            
                            ToolTip="Haga clic aquí para ver las existencias del artículo por bodega y localización, para ocultar las localizaciones haga clic aquí de nuevo." 
                            onclick="lbLocalizacion_Click" TabIndex="29" Visible="False">Localización:</asp:LinkButton>
                        Cantidad:</td>
                    <td>
                        <asp:DropDownList ID="cbLocalizacion" runat="server" Width="98px" TabIndex="30" 
                            
                            ToolTip="Seleccione la localización del artículo que desea incluir en el pedido" 
                            Visible="False">
                            <asp:ListItem Value="ARMADO">ARMADO</asp:ListItem>
                            <asp:ListItem Value="CAJA">CAJA</asp:ListItem>
                            <asp:ListItem Value="DESARMDO">DESARMDO</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtCantidad" runat="server" Width="100px" TabIndex="40" 
                            AutoPostBack="True" ontextchanged="txtCantidad_TextChanged" 
                            ToolTip="Esta es la cantidad de unidades que se cotizarán de este artículo"></asp:TextBox>
                        <asp:DropDownList ID="cbBodega" runat="server" Width="70px" TabIndex="26" 
                            Visible="False">
                            <asp:ListItem>F01</asp:ListItem>
                            <asp:ListItem>F02</asp:ListItem>
                            <asp:ListItem>F04</asp:ListItem>
                            <asp:ListItem>F05</asp:ListItem>
                            <asp:ListItem>F07</asp:ListItem>
                            <asp:ListItem>F08</asp:ListItem>
                            <asp:ListItem>F09</asp:ListItem>
                            <asp:ListItem>F11</asp:ListItem>
                            <asp:ListItem>F12</asp:ListItem>
                            <asp:ListItem>F14</asp:ListItem>
                            <asp:ListItem>F16</asp:ListItem>
                            <asp:ListItem>F10</asp:ListItem>
                            <asp:ListItem>F13</asp:ListItem>
                            <asp:ListItem>F15</asp:ListItem>
                            <asp:ListItem>F17</asp:ListItem>
                            <asp:ListItem>F18</asp:ListItem>
                            <asp:ListItem>F19</asp:ListItem>
                            <asp:ListItem>F20</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="cbRequisicion" runat="server" 
                            
                            ToolTip="Mediante esta casilla seleccione si el artículo tendrá requisición de armado" 
                            TabIndex="28" Visible="False">
                            <asp:ListItem Value="No">No</asp:ListItem>
                            <asp:ListItem Value="Sí">Sí</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        Precio Unit.:</td>
                    <td>
                        <asp:TextBox ID="txtPrecioUnitario" runat="server" Width="80px" ontextchanged="txtPrecioUnitario_TextChanged" 
                            TabIndex="45" ToolTip="Este es el precio unitario del artículo"></asp:TextBox>
                        </td>
                    <td style="margin-left: 40px">
                        <asp:TextBox ID="txtDescMax" runat="server" TabIndex="913" 
                            Visible="False" Width="20px"></asp:TextBox>
                        </td>
                    <td class="auto-style1" colspan="2">
                        Total:
                        <asp:TextBox ID="txtTotal" runat="server" Width="80px" ReadOnly="True" 
                                ToolTip="Este es el precio total del artículo"></asp:TextBox>
                            &nbsp;G.E.L.:
                        <asp:DropDownList ID="cbGel" runat="server" 
                            
                            ToolTip="Seleccione aquí si el artículo tiene Garantía Especial Limitada, por sus siglas G.E.L." 
                            TabIndex="47" Width="45px">
                            <asp:ListItem Value="No">No</asp:ListItem>
                            <asp:ListItem Value="Sí">Sí</asp:ListItem>
                        </asp:DropDownList>
                            &nbsp;
                        <asp:LinkButton ID="lbAgregarArticulo" runat="server" 
                            ToolTip="Haga clic aquí para agregar el artículo a esta cotización" 
                            TabIndex="50" onclick="lbAgregarArticulo_Click">Agregar</asp:LinkButton>
                        <asp:LinkButton ID="lkAgregarArticulo2" runat="server" 
                            onclick="lkAgregarArticulo2_Click"                             
                            ToolTip="Haga clic aquí para agregar el artículo a esta cotización y posicionarse para seguir ingresando artículos" 
                            TabIndex="53" Visible="False">(+)</asp:LinkButton>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtVale" runat="server" TabIndex="913" 
                            Visible="False" Width="20px"></asp:TextBox>
                        <asp:TextBox ID="txtArticuloCondicion" runat="server" TabIndex="913" 
                            Visible="False" Width="20px"></asp:TextBox>
                        <asp:TextBox ID="txtPrecioUnitarioDebioFacturar" runat="server" TabIndex="913" 
                            Visible="False" Width="20px"></asp:TextBox>
                        </td>
                    <td>
                        <asp:TextBox 
                            ID="txtOferta" runat="server" 
                            Width="20px" TabIndex="911" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtFechaOfertaDesde" runat="server" TabIndex="912" 
                            Visible="False" Width="20px"></asp:TextBox>
                        <asp:TextBox ID="txtPrecioOriginal" runat="server" TabIndex="913" 
                            Visible="False" Width="20px"></asp:TextBox>
                        <asp:TextBox ID="txtTipoOferta" runat="server" TabIndex="913" 
                            Visible="False" Width="20px"></asp:TextBox>
                        </td>
                    <td>
                        <asp:TextBox ID="txtFacturarItem" runat="server" TabIndex="913" 
                            Visible="False" Width="20px"></asp:TextBox>
                        <asp:TextBox ID="txtFacturado" runat="server" TabIndex="913" 
                            Visible="False" Width="20px"></asp:TextBox>
                        <asp:TextBox ID="txtDespachado" runat="server" TabIndex="913" 
                            Visible="False" Width="20px"></asp:TextBox>
                        </td>
                    <td>
                        Descuento</td>
                    <td>
                        <asp:TextBox ID="txtDescuentoItem" runat="server" Width="75px" 
                            TabIndex="35" ToolTip="Ingrese el monto completo del accesorio, si es lo que va a dar de regalo" 
                            AutoPostBack="True" Font-Bold="True" OnTextChanged="txtDescuentoItem_TextChanged"></asp:TextBox>
                        </td>
                    <td style="margin-left: 40px; text-align: right;">
                        <asp:TextBox ID="txtAutorizacion" runat="server" TabIndex="913" 
                            Visible="False" Width="20px"></asp:TextBox>
                       </td>
                    <td class="auto-style3"> Saldo
                        Descuento:&nbsp;</td>
                    <td class="auto-style1"> <asp:TextBox ID="txtSaldoDescuento" runat="server" Width="73px" ReadOnly="True" 
                            ToolTip="Saldo disponible para regalar accesorios" CssClass="auto-style2"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtCotizacion" runat="server" TabIndex="913" 
                            Visible="False" Width="20px"></asp:TextBox>
                        <asp:TextBox ID="txtProfesion" runat="server" TabIndex="913" 
                            Visible="False" Width="20px"></asp:TextBox>
                        <asp:TextBox ID="txtCondicional" runat="server" TabIndex="913" 
                            Visible="False" Width="20px"></asp:TextBox>
                        </td>
                </tr>
                <tr>
                    <td colspan="9" style="text-align: center">
                            <asp:LinkButton ID="lkLaTorre" runat="server" onclick="lkLaTorre_Click" 
                                TabIndex="56" Visible="False" 
                            ToolTip="Haga clic aquí para aplicar la promoción de La Torre">La Torre</asp:LinkButton>
                        <asp:Label ID="lbError2" runat="server" ForeColor="Red" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>
        </li>
        <li>
            
            <table align="center" runat="server" id="tblSolicutudAutorizacion" visible="false">
                <tr>
                    <td class="style1000">
                        </td>
                    <td colspan="6" class="style1000">
                        <a runat="server" id="TextoSolicitud">Solicitud de autorización para precio especial</a>&nbsp;</td>
                    <td colspan="2" class="style1000">
                        <asp:Label ID="lbSolicitudEnviada" runat="server" Text="Solicitud enviada" 
                            Visible="False"></asp:Label>
                    </td>
                    <td class="style1000">
                        </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        Desc. a solicitar:</td>
                    <td>
                        <asp:TextBox ID="txtPrecioSolicitar" runat="server" Width="70px" 
                            TabIndex="58" 
                            
                            
                            
                            ToolTip="Ingrese aquí el monto de descuento que desea solicitar a gerencia (Este descuento se hará en el precio de lista del artículo)"></asp:TextBox>
                            </td>
                    <td style="text-align: right">
                        Observaciones:</td>
                    <td colspan="3">
                        <asp:TextBox ID="txtObservacionesSolicitarAut" runat="server" Width="495px" ontextchanged="txtPrecioUnitario_TextChanged" 
                            TabIndex="59" 
                            
                            
                            ToolTip="Ingrese aquí cualquier observación que desee vea el gerente para facilitar la autorización de la solicitud" 
                            MaxLength="101"></asp:TextBox>
                            </td>
                    <td>
                        <asp:LinkButton ID="lkEnviar" runat="server" onclick="lkEnviar_Click" 
                            TabIndex="60" 
                            
                            ToolTip="Haga clic aquí para enviar la solicitud de autorización a gerencia">Enviar</asp:LinkButton>
                    </td>
                    <td>
                        <asp:LinkButton ID="lkVerificar" runat="server" onclick="lkVerificar_Click" 
                            TabIndex="61" 
                            
                            ToolTip="Haga clic aquí para verificar el estado de la autorización del cliente">Verificar</asp:LinkButton>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        Respuesta:</td>
                    <td>
                        <asp:Label ID="lbRechazada" runat="server" Font-Bold="True" ForeColor="Red" 
                            Text="Rechazada" ToolTip="Indica que la solicitud fue rechazada" 
                            Visible="False"></asp:Label>
                        <asp:Label ID="lbAutorizada" runat="server" Font-Bold="True" ForeColor="Lime" 
                            Text="Autorizada" ToolTip="Indica que la solicitud fue autorizada" 
                            Visible="False"></asp:Label>
                        <asp:Label ID="lbPendiente" runat="server" Font-Bold="True" 
                            Text="Pendiente" 
                            ToolTip="Indica que la solicitud se encuentra en evaluación"></asp:Label>
                    </td>
                    <td style="text-align: right">
                        Gerente:</td>
                    <td>
                        <asp:TextBox ID="txtGerente" runat="server" ReadOnly="True" TabIndex="691" 
                            ToolTip="Este es el nombre del gerente que autorizó o rechazó la solicitud" 
                            Width="175px"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desc. Autorizado:</td>
                    <td>
                        <asp:TextBox ID="txtPrecioAutorizado" runat="server" Width="80px" 
                            TabIndex="691" 
                            
                            
                            ToolTip="Este es el precio que le autorizaron en Gerencia" ReadOnly="True"></asp:TextBox>
                    </td>
                    <td>
                        <asp:LinkButton ID="lkAplicar" runat="server" onclick="lkAplicar_Click" 
                            TabIndex="62" ToolTip="Haga clic aquí para aplicar la autorización recibida">Aplicar</asp:LinkButton>
                            &nbsp;&nbsp;
                    </td>
                    <td>
                        <asp:LinkButton ID="lkOcultarSolicitudAutorizacion" runat="server" 
                            onclick="lkOcultarSolicitudAutorizacion_Click" 
                            
                            
                            ToolTip="Haga clic aquí para ocultar los datos de la solicitud de autorización" 
                            TabIndex="63">Ocultar</asp:LinkButton>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtAut" runat="server" Width="20px" 
                            TabIndex="691" 
                            
                            
                            ToolTip="Este es el precio que le autorizaron en Gerencia" ReadOnly="True" 
                            Visible="False"></asp:TextBox>
                    </td>
                    <td style="text-align: right">
                        Comentario:</td>
                    <td colspan="3">
                        <asp:TextBox ID="txtGerenteComentario" runat="server" ReadOnly="True" 
                            TabIndex="690" 
                            ToolTip="Este es el comentario ingresado por el gerente al autorizar o rechazar la solicitud" 
                            Width="495px"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
            
        </li>
        <li>
            <asp:GridView ID="gridVales" runat="server" Visible="False" TabIndex="64"
                AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" 
                BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" 
                onrowdatabound="gridVales_RowDataBound" 
                onselectedindexchanged="gridVales_SelectedIndexChanged" >
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkSeleccionarVale" runat="server" CausesValidation="False" 
                                CommandName="Select" Text="Seleccionar" 
                                onclick="lkSeleccionarVale_Click" 
                                ToolTip="Haga clic aquí para seleccionar este vale"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Vale" HeaderText="No. de Vale" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Fecha Emitido" Visible="true">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtFecha" runat="server" Text='<%# Bind("Fecha") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbFecha" runat="server" Text='<%# Bind("Fecha", "{0:d}") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Fecha Vence" Visible="true">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtFechaVence" runat="server" Text='<%# Bind("FechaVence") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbFechaVence" runat="server" Text='<%# Bind("FechaVence", "{0:d}") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="Vigente" HeaderText="Vigente" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Canjeado" HeaderText="Canjeado" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Fecha Canjeado" Visible="true">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtFechaCanjeado" runat="server" Text='<%# Bind("FechaCanjeado") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbFechaCanjeado" runat="server" Text='<%# Bind("FechaCanjeado", "{0:d}") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#FFF1D4" />
                <SortedAscendingHeaderStyle BackColor="#B95C30" />
                <SortedDescendingCellStyle BackColor="#F1E5CE" />
                <SortedDescendingHeaderStyle BackColor="#93451F" />
            </asp:GridView>
        </li>
        <li>
            <asp:GridView ID="gridLocalizaciones" runat="server" 
                AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" 
                BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" 
                onrowdatabound="gridLocalizaciones_RowDataBound" 
                onselectedindexchanged="gridLocalizaciones_SelectedIndexChanged" 
                Visible="False" TabIndex="66">
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkSeleccionarLocalizacion" runat="server" CausesValidation="False" 
                                CommandName="Select" Text="Seleccionar" 
                                onclick="lkSeleccionarLocalizacion_Click" 
                                ToolTip="Haga clic aquí para seleccionar esta localización para el artículo"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Articulo" HeaderText="Artículo" />
                    <asp:BoundField DataField="Nombre" HeaderText="Nombre del Artículo" 
                        Visible="False" />
                    <asp:BoundField DataField="Bodega" HeaderText="Bodega" />
                    <asp:BoundField DataField="Localizacion" HeaderText="Localización" />
                    <asp:TemplateField HeaderText="Disponible Almacén">
                        <ItemTemplate>
                            <asp:Label ID="lbDisponibleAlmacen" runat="server" Text='<%# Bind("DisponibleAlmacen", "{0:####,###,###,###,##0}") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDisponibleAlmacen" runat="server" Text='<%# Bind("DisponibleAlmacen", "{0:####,###,###,###,##0}") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reservada Almacén">
                        <ItemTemplate>
                            <asp:Label ID="lbReservadaAlmacen" runat="server" Text='<%# Bind("ReservadaAlmacen", "{0:####,###,###,###,##0}") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtReservadaAlmacen" runat="server" Text='<%# Bind("ReservadaAlmacen", "{0:####,###,###,###,##0}") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remitida Almacén">
                        <ItemTemplate>
                            <asp:Label ID="lbRemitidaAlmacen" runat="server" Text='<%# Bind("RemitidaAlmacen", "{0:####,###,###,###,##0}") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRemitidaAlmacen" runat="server" Text='<%# Bind("RemitidaAlmacen", "{0:####,###,###,###,##0}") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total Almacén">
                        <ItemTemplate>
                            <asp:Label ID="lbTotalAlmacen" runat="server" Text='<%# Bind("TotalAlmacen", "{0:####,###,###,###,##0}") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTotalAlmacen" runat="server" Text='<%# Bind("TotalAlmacen", "{0:####,###,###,###,##0}") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#FFF1D4" />
                <SortedAscendingHeaderStyle BackColor="#B95C30" />
                <SortedDescendingCellStyle BackColor="#F1E5CE" />
                <SortedDescendingHeaderStyle BackColor="#93451F" />
            </asp:GridView>
        </li>
        <li>
            <table align="center">
                <tr>
                    <td>    
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td>
                        Precio del Bien:</td>
                    <td>
                    <asp:TextBox ID="txtFacturar" runat="server" BackColor="White" Font-Bold="True" 
                            Font-Size="Medium" Width="100px" 
                        TabIndex="640" 
                            ToolTip="Este es el monto a facturar por parte de Muebles Fiesta" 
                            ReadOnly="True">899,999.98</asp:TextBox>
                    </td>
                    <td>
                        <asp:DropDownList ID="cbPagos1" runat="server" Width="50px" TabIndex="70">
                            <asp:ListItem Value="01">01</asp:ListItem>
                            <asp:ListItem Value="02">02</asp:ListItem>
                            <asp:ListItem Value="03">03</asp:ListItem>
                            <asp:ListItem Value="04">04</asp:ListItem>
                            <asp:ListItem Value="05">05</asp:ListItem>
                            <asp:ListItem Value="06">06</asp:ListItem>
                            <asp:ListItem Value="07">07</asp:ListItem>
                            <asp:ListItem Value="08">08</asp:ListItem>
                            <asp:ListItem Value="09">09</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>21</asp:ListItem>
                            <asp:ListItem>22</asp:ListItem>
                            <asp:ListItem>23</asp:ListItem>
                            <asp:ListItem>24</asp:ListItem>
                            <asp:ListItem>25</asp:ListItem>
                            <asp:ListItem>26</asp:ListItem>
                            <asp:ListItem>27</asp:ListItem>
                            <asp:ListItem>28</asp:ListItem>
                            <asp:ListItem>29</asp:ListItem>
                            <asp:ListItem>30</asp:ListItem>
                            <asp:ListItem>31</asp:ListItem>
                            <asp:ListItem>32</asp:ListItem>
                            <asp:ListItem>33</asp:ListItem>
                            <asp:ListItem>34</asp:ListItem>
                            <asp:ListItem>35</asp:ListItem>
                            <asp:ListItem>36</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        Pagos de:</td>
                    <td>
                        <asp:TextBox ID="txtPagos1" runat="server" Width="80px" TabIndex="80"></asp:TextBox>
                    </td>
                    <td>
                        Atención a:</td>
                    <td>
                        <asp:TextBox ID="txtQuienAutorizo" runat="server" TabIndex="130" style="text-transform: uppercase;" 
                            ToolTip="Ingrese aquí el nombre de a quién va dirigida la cotización" 
                            Width="197px" MaxLength="200"></asp:TextBox>
                        Tel.:<asp:TextBox ID="txtNoAutorizacion" runat="server" TabIndex="133" style="text-transform: uppercase;" 
                            ToolTip="Ingrese aquí el número de teléfono para contactar al cliente" 
                            Width="80px"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        Enganche:</td>
                    <td>
                        <asp:TextBox ID="txtEnganche" runat="server" Width="100px" TabIndex="68" 
                            ToolTip="Ingrese aquí el enganche" AutoPostBack="True" onClick="clearTextBox()"
                            ontextchanged="txtEnganche_TextChanged"></asp:TextBox>
                    </td>
                    <td>
                        <asp:DropDownList ID="cbPagos2" runat="server" Width="50px" TabIndex="90">
                            <asp:ListItem Value="01">01</asp:ListItem>
                            <asp:ListItem Value="02">02</asp:ListItem>
                            <asp:ListItem Value="03">03</asp:ListItem>
                            <asp:ListItem Value="04">04</asp:ListItem>
                            <asp:ListItem Value="05">05</asp:ListItem>
                            <asp:ListItem Value="06">06</asp:ListItem>
                            <asp:ListItem Value="07">07</asp:ListItem>
                            <asp:ListItem Value="08">08</asp:ListItem>
                            <asp:ListItem Value="09">09</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>21</asp:ListItem>
                            <asp:ListItem>22</asp:ListItem>
                            <asp:ListItem>23</asp:ListItem>
                            <asp:ListItem>24</asp:ListItem>
                            <asp:ListItem>25</asp:ListItem>
                            <asp:ListItem>26</asp:ListItem>
                            <asp:ListItem>27</asp:ListItem>
                            <asp:ListItem>28</asp:ListItem>
                            <asp:ListItem>29</asp:ListItem>
                            <asp:ListItem>30</asp:ListItem>
                            <asp:ListItem>31</asp:ListItem>
                            <asp:ListItem>32</asp:ListItem>
                            <asp:ListItem>33</asp:ListItem>
                            <asp:ListItem>34</asp:ListItem>
                            <asp:ListItem>35</asp:ListItem>
                            <asp:ListItem>36</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        Pagos de:</td>
                    <td>
                        <asp:TextBox ID="txtPagos2" runat="server" Width="80px" TabIndex="100"></asp:TextBox>
                    </td>
                    <td>
                        E-mail:&nbsp;&nbsp;<asp:CheckBox 
                            ID="cbInternet" runat="server" TabIndex="135" 
                            
                            ToolTip="Marque esta casilla si desea que al momento de guardar la cotización esta se envíe automáticamente por correo electrónico al cliente" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtNombreRecibe" runat="server" Width="310px" TabIndex="140" 
                            ToolTip="Ingrese aquí el correo electrónico para enviar la cotización" 
                            style="text-transform: lowercase;" Font-Bold="False" MaxLength="200"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        Descuentos:</td>
                    <td>
                        <asp:TextBox ID="txtDescuentos" runat="server" Width="100px" TabIndex="69" 
                            ToolTip="Ingrese aquí el enganche" AutoPostBack="True"   onClick="clearTextBox()"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        Saldo Financiar:</td>
                    <td>
                        <asp:TextBox ID="txtSaldoFinanciar" runat="server" Width="100px" 
                            ReadOnly="True" TabIndex="660" ToolTip="Este es el saldo a financiar"></asp:TextBox>
                    </td>
                    <td>
                        Obs.:</td>
                    <td colspan="4" rowspan="2">
                        <asp:TextBox ID="txtObservaciones" runat="server" Width="535px" TabIndex="150" style="text-transform: uppercase; resize:none;"
                            ToolTip="Ingrese aquí las observaciones que desea agregar a la cotización" 
                            Height="50px" MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        Recargos:</td>
                    <td>
                    <asp:TextBox ID="txtIntereses" runat="server" BackColor="White" Font-Bold="False" Width="100px" 
                        TabIndex="640" 
                            ToolTip="Este es el valor que el cliente pagará en concepto de intereses a la financiera seleccionada" 
                            ReadOnly="True">99,999.88</asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        Monto:</td>
                    <td>
                        <asp:TextBox ID="txtMonto" runat="server" Width="100px" 
                            ReadOnly="True" TabIndex="660" 
                            ToolTip="Este es el monto que pagará en total el cliente" 
                            Font-Size="Medium" Font-Bold="True"></asp:TextBox>
                    </td>
                    <td>
                        Vence:</td>
                    <td colspan="2">
                <asp:DropDownList ID="cbDia" runat="server" TabIndex="258" 
                    ToolTip="Esta es la fecha de vencimiento de la cotización" Enabled="False">
                    <asp:ListItem>--</asp:ListItem>
                    <asp:ListItem Value="1">01</asp:ListItem>
                    <asp:ListItem Value="2">02</asp:ListItem>
                    <asp:ListItem Value="3">03</asp:ListItem>
                    <asp:ListItem Value="4">04</asp:ListItem>
                    <asp:ListItem Value="5">05</asp:ListItem>
                    <asp:ListItem Value="6">06</asp:ListItem>
                    <asp:ListItem Value="7">07</asp:ListItem>
                    <asp:ListItem Value="8">08</asp:ListItem>
                    <asp:ListItem Value="9">09</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="cbMes" runat="server" TabIndex="260" 
                    ToolTip="Esta es la fecha de vencimiento de la cotización" Enabled="False">
                    <asp:ListItem>--</asp:ListItem>
                    <asp:ListItem Value="1">Ene</asp:ListItem>
                    <asp:ListItem Value="2">Feb</asp:ListItem>
                    <asp:ListItem Value="3">Mar</asp:ListItem>
                    <asp:ListItem Value="4">Abr</asp:ListItem>
                    <asp:ListItem Value="5">May</asp:ListItem>
                    <asp:ListItem Value="6">Jun</asp:ListItem>
                    <asp:ListItem Value="7">Jul</asp:ListItem>
                    <asp:ListItem Value="8">Ago</asp:ListItem>
                    <asp:ListItem Value="9">Sep</asp:ListItem>
                    <asp:ListItem Value="10">Oct</asp:ListItem>
                    <asp:ListItem Value="11">Nov</asp:ListItem>
                    <asp:ListItem Value="12">Dic</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtAnio" runat="server" TabIndex="962" 
                    ToolTip="Esta es la fecha de vencimiento de la cotización" 
                    Width="39px" ReadOnly="True">2025</asp:TextBox>
                    </td>
                    <td colspan="2">
                        Si se cotizan ofertas, la fecha final de la oferta será el vencimiento<asp:CheckBox ID="cbOtro" runat="server" TabIndex="250" Text="Otros:" 
                            
                            
                            ToolTip="Marque esta casilla si el cliente escuchó de nuestros productos a través de cualquier otro medio" 
                            Visible="False" />
                        <asp:TextBox ID="txtOtro" runat="server" Width="20px" TabIndex="252" style="text-transform: uppercase;" 
                            
                            ToolTip="Ingrese aquí el otro medio por el cual el cliente escucho acerca de nuestros productos" 
                            Visible="False"></asp:TextBox>
                        <asp:DropDownList ID="cbEntrega" runat="server" TabIndex="264" 
                            
                            ToolTip="Seleccione la jornada en que el cliente prefiere que le entreguen su mercadería" 
                            Visible="False">
                            <asp:ListItem>AM</asp:ListItem>
                            <asp:ListItem>PM</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtGarantia" runat="server" TabIndex="254" style="text-transform: uppercase;" 
                            
                            ToolTip="Ingrese aquí el número de la garantía, si fueran varias garantías, debe ingresarlas separadas por coma" 
                            Visible="False" Width="20px"></asp:TextBox>
                        <asp:CheckBox ID="cbQuetzalteco" runat="server" Text="El Quetzalteco" 
                            TabIndex="220" 
                            
                            ToolTip="Marque esta casilla si el cliente escuchó de nuestros productos a través de El Quetzalteco" 
                            Visible="False" />
                        <asp:CheckBox ID="cbPrensa" runat="server" Text="Prensa Libre" TabIndex="210" 
                            
                            ToolTip="Marque esta casilla si el cliente escuchó de nuestros productos a través de Prensa Libre" 
                            Visible="False" />
                        <asp:CheckBox ID="cbVolante" runat="server" Text="Volante" TabIndex="200" 
                            
                            ToolTip="Marque esta casilla si el cliente escuchó de nuestros productos a través de un volante" 
                            Visible="False" />
                        <asp:TextBox ID="txtSolicitud" runat="server" Width="20px" TabIndex="120" 
                            ToolTip="Ingrese aquí el n úmero de solicitud" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtPagare" runat="server" Width="20px" TabIndex="110" style="text-transform: uppercase;" 
                            ToolTip="Ingrese aquí el número de pagaré" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtNotas" runat="server" Width="10px" TabIndex="170" 
                            Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtNotasTipoVenta" runat="server" TabIndex="160" style="text-transform: uppercase;" 
                            
                            ToolTip="Aquí se le solicitará el complemento de información de la promoción, por ejemplo la tienda de La Torre donde el cliente realizó su compra" 
                            Visible="False" Width="20px"></asp:TextBox>
                        <asp:CheckBox ID="cbNuestroDiario" runat="server" Text="Nuestro Diario" 
                            TabIndex="230" 
                            
                            ToolTip="Marque esta casilla si el cliente escuchó de nuestros productos a través de Nuestro Diario" 
                            Visible="False" />
                        <asp:CheckBox ID="cbRadio" runat="server" Text="Radio" TabIndex="180" 
                            
                            ToolTip="Marque esta casilla si el cliente escuchó de nuestros productos a través de la Radio" 
                            Visible="False" />
                        <asp:CheckBox ID="cbTelevision" runat="server" Text="TV" TabIndex="240" 
                            
                            ToolTip="Marque esta casilla si el cliente escuchó de nuestros productos a través de la Televisión" 
                            Visible="False" />
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td colspan="2">
                        <asp:Label ID="lbEstadoSolicitud" runat="server" Font-Bold="True" Font-Size="X-Small" Text="SOLICITUD GRABADA" Visible="False" ForeColor="Green"></asp:Label>
                    </td>
                    <td colspan="4">
                        <dx:ASPxButton ID="btnInterconsumo" runat="server" Height="10px" onclick="btnInterconsumo_Click" ToolTip="Enviar la solicitud de crédito" Visible="true" Width="20px" UseSubmitBehavior="False">
                            <Image IconID="export_export_16x16office2013" />
                            <ClientSideEvents Click="mousePointer1" />
                        </dx:ASPxButton>
                        <dx:ASPxButton ID="lkPortalInterconsumo" runat="server" Height="10px" onclick="btnPortal_Click" ToolTip="Haga clic aquí para acceder al portal de Interconsumo" Visible="true" Width="20px">
                            <Image IconID="actions_openhyperlink_16x16" />
                        </dx:ASPxButton>
                        <dx:ASPxButton ID="btnVerificar" runat="server" Height="10px" onclick="btnVerificar_Click" ToolTip="Verificar el estado de la solicitud en Interconsumo" Visible="true" Width="20px">
                            <Image IconID="actions_search_16x16devav" />
                        </dx:ASPxButton>
                        <dx:ASPxButton ID="btnDeclaracion" runat="server" Height="10px" onclick="btnDeclaracion_Click" ToolTip="Generar declaración de ingresos" Visible="true" Width="20px">
                            <Image IconID="view_card_16x16devav" />
                        </dx:ASPxButton>
                        <dx:ASPxButton ID="btnHuellayFoto" runat="server" Height="10px"  ToolTip="Grabar huella y fotografía" Visible="true" Width="20px" OnClick="btnHuellayFoto_Click">
                            <Image IconID="businessobjects_boperson_16x16" />
                        </dx:ASPxButton>
                        &nbsp;&nbsp;
                        <asp:LinkButton ID="lkSolicitudEnLinea" runat="server" ToolTip="Haga clic aquí para imprimir la solicitud de crédito" onclick="lkSolicitudEnLinea_Click" Visible="False">Solicitud</asp:LinkButton>
                        &nbsp;&nbsp;
                        <asp:LinkButton ID="lkPagareEnLinea" runat="server" ToolTip="Haga clic aquí para imprimir el pagaré" onclick="lkPagareEnLinea_Click" Visible="False">Pagaré</asp:LinkButton>
                        <asp:Label ID="lbLinkInterconsumo" runat="server" Text="" Visible="false"></asp:Label>
                        <br />
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Primer Pago:</td>
                    <td colspan="2">
                        <asp:DropDownList ID="cbDiaPrimerPago" runat="server" TabIndex="265" ToolTip="Seleccione el día para el primer pago" >
                            <asp:ListItem>--</asp:ListItem>
                            <asp:ListItem Value="1">01</asp:ListItem>
                            <asp:ListItem Value="2">02</asp:ListItem>
                            <asp:ListItem Value="3">03</asp:ListItem>
                            <asp:ListItem Value="4">04</asp:ListItem>
                            <asp:ListItem Value="5">05</asp:ListItem>
                            <asp:ListItem Value="6">06</asp:ListItem>
                            <asp:ListItem Value="7">07</asp:ListItem>
                            <asp:ListItem Value="8">08</asp:ListItem>
                            <asp:ListItem Value="9">09</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>21</asp:ListItem>
                            <asp:ListItem>22</asp:ListItem>
                            <asp:ListItem>23</asp:ListItem>
                            <asp:ListItem>24</asp:ListItem>
                            <asp:ListItem>25</asp:ListItem>
                            <asp:ListItem>26</asp:ListItem>
                            <asp:ListItem>27</asp:ListItem>
                            <asp:ListItem>28</asp:ListItem>
                            <asp:ListItem>29</asp:ListItem>
                            <asp:ListItem>30</asp:ListItem>
                            <asp:ListItem>31</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="cbMesPrimerPago" runat="server" TabIndex="266" ToolTip="Seleccione el mes para el primer pago">
                            <asp:ListItem>--</asp:ListItem>
                            <asp:ListItem Value="1">Ene</asp:ListItem>
                            <asp:ListItem Value="2">Feb</asp:ListItem>
                            <asp:ListItem Value="3">Mar</asp:ListItem>
                            <asp:ListItem Value="4">Abr</asp:ListItem>
                            <asp:ListItem Value="5">May</asp:ListItem>
                            <asp:ListItem Value="6">Jun</asp:ListItem>
                            <asp:ListItem Value="7">Jul</asp:ListItem>
                            <asp:ListItem Value="8">Ago</asp:ListItem>
                            <asp:ListItem Value="9">Sep</asp:ListItem>
                            <asp:ListItem Value="10">Oct</asp:ListItem>
                            <asp:ListItem Value="11">Nov</asp:ListItem>
                            <asp:ListItem Value="12">Dic</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtAnioPrimerPago" runat="server" TabIndex="267" 
                            ToolTip="Ingrese aquí el año para el primer día de pago en formato de 4 dígitos, ejemplo: 2019." 
                            Width="40px">2025</asp:TextBox>
                    </td>
                    <td colspan="2">
                        <asp:LinkButton ID="lkAccesorios" runat="server" onclick="lkAccesorios_Click" 
                            TabIndex="54">Fiesta de Accesorios</asp:LinkButton>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </li>
        <li>
        <asp:GridView ID="gridArticulos" runat="server" CellPadding="3" TabIndex="280" 
                    AutoGenerateColumns="False" style="text-align: left" 
                BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                CellSpacing="2" onrowdatabound="gridArticulos_RowDataBound" 
                onselectedindexchanged="gridArticulos_SelectedIndexChanged">
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lkEliminarArticulo" runat="server" CausesValidation="False" 
                                    CommandName="Select" Text="Eliminar" ToolTip="Haga clic aquí para eliminar este artículo"
                                    onclientclick="return confirm('Desea eliminar el artículo?');" 
                                    onclick="lkEliminarArticulo_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lkCambiarGel" runat="server" CausesValidation="False" 
                                    CommandName="Select" Text="GEL" 
                                    ToolTip="Haga clic aquí para cambiar la garantía especial limitada" 
                                    onclick="lkCambiarGel_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Artículo">
                            <ItemTemplate>
                                <asp:Label ID="lbArticulo" runat="server" Text='<%# Bind("Articulo") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulo" runat="server" Text='<%# Bind("Articulo") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nombre del Artículo">
                            <ItemTemplate>
                                <asp:Label ID="lbNombreArticulo" runat="server" Text='<%# Bind("Nombre") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtNombreArticulo" runat="server" Text='<%# Bind("Nombre") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemStyle Font-Size="X-Small" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Precio Unitario">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecio" runat="server" Text='<%# Bind("PrecioUnitario", "{0:####,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecio" runat="server" Text='<%# Bind("PrecioUnitario", "{0:####,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cantidad">
                            <ItemTemplate>
                                <asp:Label ID="lbCantidad" runat="server" Text='<%# Bind("CantidadPedida", "{0:####,###,###,###,##0}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCantidad" runat="server" Text='<%# Bind("CantidadPedida", "{0:####,###,###,###,##0}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total">
                            <ItemTemplate>
                                <asp:Label ID="lbTotal" runat="server" Text='<%# Bind("PrecioTotal", "{0:####,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotal" runat="server" Text='<%# Bind("PrecioTotal", "{0:####,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PorcDescuento" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbPorcDescuento" runat="server" Text='<%# Bind("PorcentajeDescuento", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                        <asp:TemplateField HeaderText="Descuento" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbDescuento" runat="server" Text='<%# Bind("Descuento", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                        <asp:TemplateField HeaderText="Con Descuento" Visible="True">
                        <ItemTemplate>
                            <asp:Label ID="NetoFactrar" runat="server" Text='<%# Bind("NetoFacturar", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                        <asp:TemplateField HeaderText="Bodega" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lbBodega" runat="server" Text='<%# Bind("Bodega") %>'></asp:Label>
                            </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtBodega" runat="server" Text='<%# Bind("Bodega") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Localización" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lbLocalizacion" runat="server" Text='<%# Bind("Localizacion") %>'></asp:Label>
                            </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtLocalizacion" runat="server" Text='<%# Bind("Localizacion") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Armado?" Visible="false">
                            <ItemTemplate>
                                <asp:LinkButton ID="lkRequisicionArmado" runat="server" Text='<%# Bind("RequisicionArmado") %>' 
                                    ToolTip="Haga clic aquí para indicar si el artículo tiene armado o servicio" 
                                    CommandName="Select"></asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtRequisicionArmado" runat="server" Text='<%# Bind("RequisicionArmado") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Descripcion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Comentario" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbComentario" runat="server" Text='<%# Bind("Comentario") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtComentario" runat="server" Text='<%# Bind("Comentario") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Estado" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbEstado" runat="server" Text='<%# Bind("Estado") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEstado" runat="server" Text='<%# Bind("Estado") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="NumeroPedido" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbNumeroPedido" runat="server" Text='<%# Bind("NumeroPedido") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtNumeroPedido" runat="server" Text='<%# Bind("NumeroPedido") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Oferta" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbOferta" runat="server" Text='<%# Bind("Oferta") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOferta" runat="server" Text='<%# Bind("Oferta") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FechaOfertaDesde" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbFechaOfertaDesde" runat="server" Text='<%# Bind("FechaOfertaDesde") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFechaOfertaDesde" runat="server" Text='<%# Bind("FechaOfertaDesde") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PrecioOriginal" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecioOriginal" runat="server" Text='<%# Bind("PrecioOriginal") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecioOriginal" runat="server" Text='<%# Bind("PrecioOriginal") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EsDetalleKit" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbEsDetalleKit" runat="server" Text='<%# Bind("EsDetalleKit") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEsDetalleKit" runat="server" Text='<%# Bind("EsDetalleKit") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PrecioFacturar" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecioFacturar" runat="server" Text='<%# Bind("PrecioFacturar") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecioFacturar" runat="server" Text='<%# Bind("PrecioFacturar") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TipoOferta" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbTipoOferta" runat="server" Text='<%# Bind("TipoOferta") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTipoOferta" runat="server" Text='<%# Bind("TipoOferta") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vale" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbVale" runat="server" Text='<%# Bind("Vale") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVale" runat="server" Text='<%# Bind("Vale") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Autorizacion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbAutorizacion" runat="server" Text='<%# Bind("Autorizacion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtAutorizacion" runat="server" Text='<%# Bind("Autorizacion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Condicional" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbCondicional" runat="server" Text='<%# Bind("Condicional") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCondicional" runat="server" Text='<%# Bind("Condicional") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ArticuloCondicion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbArticuloCondicion" runat="server" Text='<%# Bind("ArticuloCondicion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticuloCondicion" runat="server" Text='<%# Bind("ArticuloCondicion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PrecioUnitarioDebioFacturar" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecioUnitarioDebioFacturar" runat="server" Text='<%# Bind("PrecioUnitarioDebioFacturar", "{0:####,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecioUnitarioDebioFacturar" runat="server" Text='<%# Bind("PrecioUnitarioDebioFacturar", "{0:####,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PrecioSugerido" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecioSugerido" runat="server" Text='<%# Bind("PrecioSugerido", "{0:####,###,###,###,###.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecioSugerido" runat="server" Text='<%# Bind("PrecioSugerido", "{0:####,###,###,###,###.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PrecioBaseLocal" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecioBaseLocal" runat="server" Text='<%# Bind("PrecioBaseLocal") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecioBaseLocal" runat="server" Text='<%# Bind("PrecioBaseLocal") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Linea" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lbLinea" runat="server" Text='<%# Bind("Linea") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtLinea" runat="server" Text='<%# Bind("Linea") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Gel" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lbGel" runat="server" Text='<%# Bind("Gel") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGel" runat="server" Text='<%# Bind("Gel") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PorcentajeDescuento" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lbPorcentajeDescuento" runat="server" Text='<%# Bind("PorcentajeDescuento") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPorcentajeDescuento" runat="server" Text='<%# Bind("PorcentajeDescuento") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Beneficiario" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lbBeneficiario" runat="server" Text='<%# Bind("Beneficiario") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtBeneficiario" runat="server" Text='<%# Bind("Beneficiario") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#F8AA55" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
            </asp:GridView>
        </li>
        <li>
        <table id="tablaBusquedaDet" runat="server" visible="false" >
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td colspan="3">
                <a>Búsqueda de artículos</a>&nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <asp:Label ID="Label6" runat="server" ForeColor="Red"></asp:Label>
                <asp:Label ID="Label7" runat="server"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>
                Artículo:</td>
            <td>
                <asp:TextBox ID="codigoArticuloDet" runat="server" TabIndex="290" 
                    AutoPostBack="True" ontextchanged="codigoArticuloDet_TextChanged" 
                    
                    ToolTip="Ingrese aquí el código del artículo o parte del código que desea buscar y presione ENTER."></asp:TextBox>
            </td>
            <td>
                Nombre:</td>
            <td>
                <asp:TextBox ID="descripcionArticuloDet" runat="server" TabIndex="300" 
                    MaxLength="100" AutoPostBack="True" 
                    ontextchanged="descripcionArticuloDet_TextChanged" style="text-transform: uppercase;" 
                    ToolTip="Ingrese aquí el nombre o parte del nombre del artículo que desea buscar y presione ENTER"></asp:TextBox>
            </td>
            <td>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="6" style="text-align: center">
                <asp:LinkButton ID="lbBuscarArticuloDet" runat="server" 
                    
                    ToolTip="Haga clic aquí para buscar artículos con los criterios de búsqueda ingresados" onclick="lbBuscarArticuloDet_Click" 
                    TabIndex="310">Buscar artículo</asp:LinkButton>&nbsp;&nbsp;&nbsp;|&nbsp;
                <asp:LinkButton ID="lbCerrarBusquedaDet" runat="server" 
                    ToolTip="Cierra la ventana de búsqueda" onclick="lbCerrarBusquedaDet_Click" 
                    TabIndex="320">Cerrar Búsqueda</asp:LinkButton>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        </table>
        </li>
        <li>
        <asp:GridView ID="gridArticulosDet" runat="server" CellPadding="3" TabIndex="360" 
                    AutoGenerateColumns="False" style="text-align: left" 
                BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                CellSpacing="2" onrowdatabound="gridArticulosDet_RowDataBound" 
                onselectedindexchanged="gridArticulosDet_SelectedIndexChanged">
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lkSeleccionarArticulo" runat="server" CausesValidation="False" 
                                    CommandName="Select" Text="Seleccionar"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Articulo" HeaderText="Artículo" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre del Artículo" />
                        <asp:TemplateField HeaderText="Precio">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecio" runat="server" Text='<%# Bind("Precio", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecio" runat="server" Text='<%# Bind("Precio", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="NivelPrecio" HeaderText="Nivel de Precio" />
                        <asp:TemplateField HeaderText="Oferta" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lbOferta" runat="server" Text='<%# Bind("Oferta") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOferta" runat="server" Text='<%# Bind("Oferta") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PrecioOriginal" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecioOriginal" runat="server" Text='<%# Bind("PrecioOriginal") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecioOriginal" runat="server" Text='<%# Bind("PrecioOriginal") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FechaVence" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbFechaVence" runat="server" Text='<%# Bind("FechaVence") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFechaVence" runat="server" Text='<%# Bind("FechaVence") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DescuentoContado" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbDescuentoContado" runat="server" Text='<%# Bind("DescuentoContado") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescuentoContado" runat="server" Text='<%# Bind("DescuentoContado") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DescuentoCredito" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbDescuentoCredito" runat="server" Text='<%# Bind("DescuentoCredito") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescuentoCredito" runat="server" Text='<%# Bind("DescuentoCredito") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Condicional" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbCondicional" runat="server" Text='<%# Bind("Condicional") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCondicional" runat="server" Text='<%# Bind("Condicional") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PrecioOferta" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecioOferta" runat="server" Text='<%# Bind("PrecioOferta") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecioOferta" runat="server" Text='<%# Bind("PrecioOferta") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Condiciones" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbCondiciones" runat="server" Text='<%# Bind("Condiciones") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCondiciones" runat="server" Text='<%# Bind("Condiciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Promocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbPromocion" runat="server" Text='<%# Bind("Promocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPromocion" runat="server" Text='<%# Bind("Promocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TipoPromocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbTipoPromocion" runat="server" Text='<%# Bind("TipoPromocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTipoPromocion" runat="server" Text='<%# Bind("TipoPromocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ValorPromocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbValorPromocion" runat="server" Text='<%# Bind("ValorPromocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtValorPromocion" runat="server" Text='<%# Bind("ValorPromocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VencePromocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbVencePromocion" runat="server" Text='<%# Bind("VencePromocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVencePromocion" runat="server" Text='<%# Bind("VencePromocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FechaVencePromocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbFechaVencePromocion" runat="server" Text='<%# Bind("FechaVencePromocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFechaVencePromocion" runat="server" Text='<%# Bind("FechaVencePromocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DescripcionPromocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbDescripcionPromocion" runat="server" Text='<%# Bind("DescripcionPromocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescripcionPromocion" runat="server" Text='<%# Bind("DescripcionPromocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ObservacionesPromocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbObservacionesPromocion" runat="server" Text='<%# Bind("ObservacionesPromocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtObservacionesPromocion" runat="server" Text='<%# Bind("ObservacionesPromocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pagos" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbPagos" runat="server" Text='<%# Bind("Pagos") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPagos" runat="server" Text='<%# Bind("Pagos") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FechaDesde" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbFechaDesde" runat="server" Text='<%# Bind("FechaDesde") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFechaDesde" runat="server" Text='<%# Bind("FechaDesde") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TipoOferta" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbTipoOferta" runat="server" Text='<%# Bind("TipoOferta") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTipoOferta" runat="server" Text='<%# Bind("TipoOferta") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#F8AA55" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
            </asp:GridView>
        </li>
        <li>
            <table align="center" id="tblFactura" runat="server" visible="false">
                <tr>
                    <td style="text-align: center">
                        Número de factura:&nbsp;&nbsp;<asp:TextBox ID="txtNumeroFactura" runat="server" style="text-transform: uppercase;" 
                            TabIndex="586"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lkNumeroFactura" runat="server" 
                            ToolTip="Haga clic aquí para que el sistema le sugiera el número de factura" 
                            onclick="lkNumeroFactura_Click" TabIndex="587">Sugerir factura</asp:LinkButton>&nbsp;&nbsp;
                            <asp:CheckBox ID="cbNumeroFactura" runat="server" 
                            Text="Acepto el número de factura" TabIndex="588" 
                            ToolTip="Haga clic en esta casilla para confirmar que acepta el número de factura" />
                            &nbsp;&nbsp;&nbsp;
                        <asp:LinkButton ID="lkFacturar" runat="server" 
                            ToolTip="Haga clic aquí para facturar el pedido.  Esta opción solo funcionará al grabar un pedido." 
                            TabIndex="589" onclick="lkFacturar_Click" 
                            onclientclick="return confirm('Desea facturar el pedido?');">Generar Factura</asp:LinkButton>
                            &nbsp;
                    </td>
                </tr>
            </table>
        </li>
        <li>
            <table align="center" id="tblDespacho" runat="server" visible="false">
                <tr>
                    <td style="text-align: center">
                        Número de Envío:&nbsp;&nbsp;<asp:TextBox ID="txtNumeroDespacho" runat="server" style="text-transform: uppercase;" 
                            TabIndex="586"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lkNumeroDespacho" runat="server" 
                            ToolTip="Haga clic aquí para que el sistema le sugiera el número de envío" 
                            onclick="lkNumeroEnvio_Click" TabIndex="587">Sugerir envío</asp:LinkButton>&nbsp;&nbsp;
                            
                            <asp:CheckBox ID="cbNumeroDespacho" runat="server" 
                            Text="Acepto el número de envío" TabIndex="588" 
                            ToolTip="Marque esta casilla para confirmar que acepta el número de envío" />
                            &nbsp;&nbsp;&nbsp;
                        <asp:LinkButton ID="lkDespachar" runat="server" 
                            ToolTip="Haga clic aquí para despachar el pedido.  Esta opción solo funcionará después de facturar el pedido." 
                            TabIndex="589" onclick="lkDespachar_Click" 
                            onclientclick="return confirm('Desea despachar el pedido?');">Generar Despacho</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center">
                        Observaciones del envío:&nbsp;&nbsp;<asp:TextBox ID="txtObservacionesDespacho" runat="server" 
                            TabIndex="589" style="text-transform: uppercase;" 
                            ToolTip="Ingrese aquí las observaciones que serán impresas en el envío" 
                            Width="730px"></asp:TextBox></td>
                </tr>
            </table>
        </li>
        <li runat="server" id="liReImprimir" visible="false">
            <table align="center">
                <tr>
                    <td style="text-align: center">
                        <asp:TextBox ID="txtFacturaReImprimir" runat="server"></asp:TextBox>&nbsp;&nbsp;
                        <asp:LinkButton ID="lkReImprimirFactura" runat="server" onclick="lkReImprimirFactura_Click">Re-imprimir factura</asp:LinkButton>&nbsp;&nbsp;
                        <asp:TextBox ID="txtDespachoReImprimir" runat="server"></asp:TextBox>&nbsp;&nbsp;
                        <asp:LinkButton ID="lkReImprimirDespacho" runat="server" onclick="lkReImprimirDespacho_Click">Re-imprimir despacho</asp:LinkButton>&nbsp;&nbsp;
                    </td>
                </tr>
            </table>            
        </li>
        <li>
            <asp:GridView ID="gridPedidos" runat="server" BackColor="#DEBA84" 
                AutoGenerateColumns="False" style="text-align: left" 
                BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" TabIndex="588" 
                CellSpacing="2" onrowdatabound="gridPedidos_RowDataBound" 
                onselectedindexchanged="gridPedidos_SelectedIndexChanged" Visible="False">
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkEliminarPedido" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkEliminarPedido_Click" Text="Eliminar" 
                                ToolTip="Haga clic aquí para eliminar el pedido" 
                                onclientclick="return confirm('Desea eliminar el pedido?');" 
                                Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkSeleccionarPedido" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkSeleccionarPedido_Click" Text="Seleccionar" 
                                ToolTip="Haga clic aquí para seleccionar el pedido" Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkSolicitudCredito" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkSolicitudCredito_Click" Text="Solicitud" 
                                ToolTip="Haga clic aquí para imprimir la solicitud de crédito" 
                                Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkPagare" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkPagare_Click" Text="Pagaré" 
                                ToolTip="Haga clic aquí para imprimir el pagaré" Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkExpediente" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkExpediente_Click" Text="Expediente" 
                                ToolTip="Haga clic aquí para imprimir el Expediente de Muebles Fiesta" 
                                Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkFacturarPedido" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkFacturarPedido_Click" Text="Facturar" 
                                ToolTip="Haga clic aquí para facturar el pedido" Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkDespacharPedido" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkDespacharPedido_Click" Text="Despachar" 
                                ToolTip="Haga clic aquí para despachar el pedido" Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="NumeroPedido" HeaderText="Pedido">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Cliente" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbCliente" runat="server" Text='<%# Bind("Cliente") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCliente" runat="server" Text='<%# Bind("Cliente") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Fecha" Visible="true">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtFecha" runat="server" Text='<%# Bind("Fecha") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbFecha" runat="server" Text='<%# Bind("Fecha", "{0:d}") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Garantia" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbGarantia" runat="server" Text='<%# Bind("Garantia") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGarantia" runat="server" Text='<%# Bind("Garantia") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Observaciones" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbObservaciones" runat="server" Text='<%# Bind("Observaciones") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtObservaciones" runat="server" Text='<%# Bind("Observaciones") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ObservacionesTipoVenta" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbObservacionesTipoVenta" runat="server" Text='<%# Bind("ObservacionesTipoVenta") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtObservacionesTipoVenta" runat="server" Text='<%# Bind("ObservacionesTipoVenta") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Precio del Bien" Visible="True">
                        <ItemTemplate>
                            <asp:Label ID="lbTotalFacturar" runat="server" Text='<%# Bind("TotalFacturar", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTotalFacturar" runat="server" Text='<%# Bind("TotalFacturar", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Enganche" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbEnganche" runat="server" Text='<%# Bind("Enganche", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtEnganche" runat="server" Text='<%# Bind("Enganche", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Saldo Financiar" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbSaldoFinanciar" runat="server" Text='<%# Bind("SaldoFinanciar", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtSaldoFinanciar" runat="server" Text='<%# Bind("SaldoFinanciar", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Recargos" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbRecargos" runat="server" Text='<%# Bind("Recargos", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRecargos" runat="server" Text='<%# Bind("Recargos", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Monto" Visible="True">
                        <ItemTemplate>
                            <asp:Label ID="lbMonto" runat="server" Text='<%# Bind("Monto", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtMonto" runat="server" Text='<%# Bind("Monto", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="TipoVenta" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbTipoVenta" runat="server" Text='<%# Bind("TipoVenta") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTipoVenta" runat="server" Text='<%# Bind("TipoVenta") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Financiera" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbFinanciera" runat="server" Text='<%# Bind("Financiera") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtFinanciera" runat="server" Text='<%# Bind("Financiera") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="NivelPrecio" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbNivelPrecio" runat="server" Text='<%# Bind("NivelPrecio") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNivelPrecio" runat="server" Text='<%# Bind("NivelPrecio") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tienda" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbTienda" runat="server" Text='<%# Bind("Tienda") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTienda" runat="server" Text='<%# Bind("Tienda") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vendedor" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Bodega" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbBodega" runat="server" Text='<%# Bind("Bodega") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtBodega" runat="server" Text='<%# Bind("Bodega") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="NombreCliente" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbNombreCliente" runat="server" Text='<%# Bind("NombreCliente") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNombreCliente" runat="server" Text='<%# Bind("NombreCliente") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="EntregaAMPM" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbEntregaAMPM" runat="server" Text='<%# Bind("EntregaAMPM") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtEntregaAMPM" runat="server" Text='<%# Bind("EntregaAMPM") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="MercaderiaSale" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbMercaderiaSale" runat="server" Text='<%# Bind("MercaderiaSale") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtMercaderiaSale" runat="server" Text='<%# Bind("MercaderiaSale") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Desarmarla" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbDesarmarla" runat="server" Text='<%# Bind("Desarmarla") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDesarmarla" runat="server" Text='<%# Bind("Desarmarla") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="NotasTipoVenta" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbNotasTipoVenta" runat="server" Text='<%# Bind("NotasTipoVenta") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNotasTipoVenta" runat="server" Text='<%# Bind("NotasTipoVenta") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Pagare" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbPagare" runat="server" Text='<%# Bind("Pagare") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPagare" runat="server" Text='<%# Bind("Pagare") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="NombreAutorizacion" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbNombreAutorizacion" runat="server" Text='<%# Bind("NombreAutorizacion") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNombreAutorizacion" runat="server" Text='<%# Bind("NombreAutorizacion") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Autorizacion" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbAutorizacion" runat="server" Text='<%# Bind("Autorizacion") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAutorizacion" runat="server" Text='<%# Bind("Autorizacion") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Solicitud" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbSolicitud" runat="server" Text='<%# Bind("Solicitud") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtSolicitud" runat="server" Text='<%# Bind("Solicitud") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CantidadPagos1" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbCantidadPagos1" runat="server" Text='<%# Bind("CantidadPagos1") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCantidadPagos1" runat="server" Text='<%# Bind("CantidadPagos1") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="MontoPagos1" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbMontoPagos1" runat="server" Text='<%# Bind("MontoPagos1") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtMontoPagos1" runat="server" Text='<%# Bind("MontoPagos1") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CantidadPagos2" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbCantidadPagos2" runat="server" Text='<%# Bind("CantidadPagos2") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCantidadPagos2" runat="server" Text='<%# Bind("CantidadPagos2") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="MontoPagos2" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbMontoPagos2" runat="server" Text='<%# Bind("MontoPagos2") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtMontoPagos2" runat="server" Text='<%# Bind("MontoPagos2") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="TipoReferencia" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbTipoReferencia" runat="server" Text='<%# Bind("TipoReferencia") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTipoReferencia" runat="server" Text='<%# Bind("TipoReferencia") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ObservacionesReferencia" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbObservacionesReferencia" runat="server" Text='<%# Bind("ObservacionesReferencia") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtObservacionesReferencia" runat="server" Text='<%# Bind("ObservacionesReferencia") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="TipoPedido" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbTipoPedido" runat="server" Text='<%# Bind("TipoPedido") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTipoPedido" runat="server" Text='<%# Bind("TipoPedido") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vale" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbVale" runat="server" Text='<%# Bind("Vale") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtVale" runat="server" Text='<%# Bind("Vale") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cotizacion" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbCotizacion" runat="server" Text='<%# Bind("Cotizacion") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCotizacion" runat="server" Text='<%# Bind("Cotizacion") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="FechaEntrega" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtFechaEntrega" runat="server" Text='<%# Bind("FechaEntrega") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbFechaEntrega" runat="server" Text='<%# Bind("FechaEntrega", "{0:d}") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="NombreRecibe" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbNombreRecibe" runat="server" Text='<%# Bind("NombreRecibe") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNombreRecibe" runat="server" Text='<%# Bind("NombreRecibe") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#FFF1D4" />
                <SortedAscendingHeaderStyle BackColor="#B95C30" />
                <SortedDescendingCellStyle BackColor="#F1E5CE" />
                <SortedDescendingHeaderStyle BackColor="#93451F" />
            </asp:GridView>
        </li>
        <li>
            <table align="center" runat="server" visible="false" id="tblClientes">
            <tr>
                <td style="text-align: left">
                    <span class="style1">Cliente:</span>&nbsp; </td>
                <td style="text-align: right">
                    <asp:TextBox ID="txtCodigo" runat="server" BackColor="White" BorderStyle="None" 
                        Enabled="False" Font-Bold="True" Font-Size="Large" Width="85px" 
                        TabIndex="640"></asp:TextBox>
                </td>
                <td style="text-align: left">
                    <asp:DropDownList ID="cbTipoPedido" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="cbTipoPedido_SelectedIndexChanged" TabIndex="4" 
                        Visible="False" Width="30px">
                        <asp:ListItem Value="A">Armado</asp:ListItem>
                        <asp:ListItem Value="F">Flete</asp:ListItem>
                        <asp:ListItem Value="N">Normal</asp:ListItem>
                        <asp:ListItem Value="R">Repuestos</asp:ListItem>
                    </asp:DropDownList>
                        <asp:DropDownList ID="cbTienda" runat="server" TabIndex="6" 
                        AutoPostBack="True" onselectedindexchanged="cbTienda_SelectedIndexChanged" 
                        Visible="False">
                            <asp:ListItem>F01</asp:ListItem>
                            <asp:ListItem>F02</asp:ListItem>
                            <asp:ListItem>F04</asp:ListItem>
                            <asp:ListItem>F05</asp:ListItem>
                            <asp:ListItem>F07</asp:ListItem>
                            <asp:ListItem>F08</asp:ListItem>
                            <asp:ListItem>F09</asp:ListItem>
                            <asp:ListItem>F11</asp:ListItem>
                            <asp:ListItem>F12</asp:ListItem>
                            <asp:ListItem>F14</asp:ListItem>
                            <asp:ListItem>F16</asp:ListItem>
                        </asp:DropDownList>
                    <asp:DropDownList ID="cbVendedor" runat="server" Width="30px" 
                        TabIndex="7" 
                        ToolTip="Seleccione el vendedor a utilizar en el pedido" Visible="False">
                    </asp:DropDownList>
                    <asp:LinkButton ID="lbBuscarCliente" runat="server" 
                        ToolTip="Haga clic aquí para buscar un cliente" TabIndex="2" 
                        onclick="lbBuscarCliente_Click">Buscar cliente</asp:LinkButton>
                </td>
                <td style="text-align: right">
                    Nombre:</td>
                <td style="text-align: left">
                    <asp:TextBox ID="txtNombre" runat="server" BackColor="White" BorderStyle="None" 
                        Enabled="False" Font-Bold="True" Width="270px" 
                        TabIndex="640"></asp:TextBox>
                </td>
                <td style="text-align: right" class="style1">
                    Nit:
                    <asp:TextBox ID="txtNit" runat="server" BackColor="White" BorderStyle="None" 
                        Enabled="False" Font-Bold="True" Width="115px" 
                        TabIndex="640"></asp:TextBox>
                </td>
                <td style="text-align: left">
                    <asp:LinkButton ID="lkPedidosCliente" runat="server" 
                        onclick="lkPedidosCliente_Click" TabIndex="69" 
                        ToolTip="Haga clic aquí para ver los pedidos del cliente" Visible="False">Pedidos</asp:LinkButton>
                    <asp:LinkButton ID="lkAsignarCliente" runat="server" 
                        onclick="lkAsignarCliente_Click" TabIndex="69" 
                        ToolTip="Haga clic aquí para asignar este cliente a la cotización seleccionada" Visible="true">Asignar Cliente</asp:LinkButton>
                </td>
            </tr>
            </table>
        </li>
        <li>
            <table align="center" runat="server" visible="false" id="tblBuscar">
                <tr>
                    <td colspan="6">
                        <a>Búsqueda de Clientes</a>&nbsp;&nbsp;<asp:LinkButton ID="lkReImprimir" 
                            runat="server" onclick="lkReImprimir_Click">&nbsp;&nbsp;</asp:LinkButton>
                    </td>
                    <td colspan="6" style="text-align: right">
                        <asp:Label ID="lbCotizacionSeleccionada" runat="server" Visible="False"></asp:Label>
                        <asp:Label ID="lbInfoCliente" runat="server"></asp:Label>
                        <asp:Label ID="lbErrorCliente" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td>
                        Código:</td>
                    <td>
                        <asp:TextBox ID="txtCodigoBuscar" runat="server" AutoPostBack="True" 
                            ontextchanged="txtCodigoBuscar_TextChanged" TabIndex="14" 
                            
                            ToolTip="Ingrese aquí el código o parte del código del cliente que desea buscar y luego presione ENTER.  No es necesario digitar los ceros a la izquierda del código." 
                            Width="80px"></asp:TextBox>
                    </td>
                    <td>
                        Nombre:</td>
                    <td>
                        <asp:TextBox ID="txtNombreBuscar" runat="server" Width="300px" AutoPostBack="True" 
                            ontextchanged="txtNombreBuscar_TextChanged" TabIndex="16" style="text-transform: uppercase;" 
                            ToolTip="Ingrese aquí el nombre o parte del nombre del cliente que desea buscar y luego presione ENTER."></asp:TextBox>
                    </td>
                    <td colspan="2">
                        NIT:</td>
                    <td>
                        <asp:TextBox ID="txtNitBuscar" runat="server" AutoPostBack="True" 
                            ontextchanged="txtNitBuscar_TextChanged" TabIndex="18" style="text-transform: uppercase;" 
                            
                            ToolTip="Ingrese aquí el NIT o parte del NIT que desea buscar y luego presione ENTER." 
                            Width="80px"></asp:TextBox>
                    </td>
                    <td>
                        Tel.:</td>
                    <td>
                        <asp:TextBox ID="txtTelefono" runat="server" AutoPostBack="True" 
                            ontextchanged="txtTelefono_TextChanged" TabIndex="20" Width="90px" 
                            
                            
                            ToolTip="Ingrese aquí el teléfono que desea buscar y luego presione ENTER, se buscará en el teléfono de casa, trabajo y celular."></asp:TextBox>
                    </td>
                    <td>
                        <asp:LinkButton ID="lkOcultarBusqueda" runat="server" 
                            ToolTip="Haga clic aquí para ocultar la búsqueda de clientes." 
                            onclick="lkOcultarBusqueda_Click" TabIndex="22">Ocultar</asp:LinkButton>
                    </td>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </li>
        <li>
            <asp:GridView ID="gridClientes" runat="server" Visible="False" 
                AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" 
                BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" 
                onselectedindexchanged="gridClientes_SelectedIndexChanged" 
                onrowdatabound="gridClientes_RowDataBound" TabIndex="14">
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkSeleccionar" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkSeleccionar_Click" Text="Asignar" 
                                ToolTip="Haga clic aquí para asignar este cliente a la cotización seleccionada"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Cliente" HeaderText="Cliente">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" >
                    <ItemStyle Font-Size="X-Small" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Nit" HeaderText="Nit" />
                    <asp:BoundField DataField="TelCasa" HeaderText="Tel Casa" />
                    <asp:BoundField DataField="TelTrabajo" HeaderText="Tel Trabajo" />
                    <asp:BoundField DataField="TelCelular" HeaderText="Celular" />
                    <asp:BoundField DataField="Tienda" HeaderText="Tienda">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Vendedor" HeaderText="Vendedor" />
                    <asp:BoundField DataField="FechaIngreso" HeaderText="Ingreso" Visible="False">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                </Columns>
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#FFF1D4" />
                <SortedAscendingHeaderStyle BackColor="#B95C30" />
                <SortedDescendingCellStyle BackColor="#F1E5CE" />
                <SortedDescendingHeaderStyle BackColor="#93451F" />
            </asp:GridView>
        </li>
        <li>
            
            <table align="center" visible="false" id="tblSolicitudDescuento" runat="server">
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        <a>Solicitud de autorización</a></td>
                    <td colspan="3" style="text-align: right">
                        <asp:Label ID="lbInfoSolicitud" runat="server">Solicitud enviada exitosamente</asp:Label>
                    <asp:Label ID="lbErrorSolicitud" runat="server" Font-Bold="True" ForeColor="Red" 
                        TabIndex="630"></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        Desea emitir una factura por valor de:</td>
                    <td>
                        <asp:TextBox ID="txtSolicitudPrecio" runat="server" Width="85px" 
                            ToolTip="Ingrese aquí el monto por el cual desea emitir la factura"></asp:TextBox>
                    </td>
                    <td>
                        Observaciones:</td>
                    <td rowspan="2">
                        <asp:TextBox ID="txtSolicitudObservaciones" runat="server" Height="40px" Width="460px" style="resize:none"
                            ToolTip="Ingrese aquí las observaciones necesarias para que gerencia pueda entender su solicitud y así esta pueda ser autorizada" 
                            TextMode="MultiLine" MaxLength="800"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;&nbsp;&nbsp;
                        <asp:LinkButton ID="lkRetractarSolicitud" runat="server" visible="false"
                            ToolTip="Haga clic aquí para retractarse de la solicitud enviada para esta cotización" 
                            onclick="lkRetractarSolicitud_Click">Retractarse</asp:LinkButton>
                    </td>
                    <td colspan="2" style="text-align: right">
                        <asp:LinkButton ID="lkEnviarSolicitud" runat="server" 
                            ToolTip="Haga clic aquí para enviar la solicitud a gerencia.  Mientras este en proceso de autorización no podrá convertirla en pedido.  Para facturar la cotización deberá esperar a que la autoricen o bien haciendo clic en Retractarse" 
                            onclick="lkEnviarSolicitud_Click">Enviar solicitud </asp:LinkButton>
                        </td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
            
        </li>
        <li>
            <table align="center" runat="server" id="tblCotizaciones" visible="false">
                <tr>
                    <td colspan="4">
                        <a>Búsqueda de cotizaciones</a></td>
                    <td colspan="4" style="text-align: right">
                        <asp:Label ID="lbInfoCotizaciones" runat="server"></asp:Label>
                        <asp:Label ID="lbErrorCotizaciones" runat="server" Font-Bold="True" 
                            ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        Fecha Inicial:</td>
                    <td>
                <asp:DropDownList ID="cbDiaIni" runat="server" TabIndex="258" 
                    ToolTip="Esta es la fecha de vencimiento de la cotización">
                    <asp:ListItem>--</asp:ListItem>
                    <asp:ListItem Value="1">01</asp:ListItem>
                    <asp:ListItem Value="2">02</asp:ListItem>
                    <asp:ListItem Value="3">03</asp:ListItem>
                    <asp:ListItem Value="4">04</asp:ListItem>
                    <asp:ListItem Value="5">05</asp:ListItem>
                    <asp:ListItem Value="6">06</asp:ListItem>
                    <asp:ListItem Value="7">07</asp:ListItem>
                    <asp:ListItem Value="8">08</asp:ListItem>
                    <asp:ListItem Value="9">09</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="cbMesIni" runat="server" TabIndex="260" 
                    ToolTip="Esta es la fecha de vencimiento de la cotización">
                    <asp:ListItem>--</asp:ListItem>
                    <asp:ListItem Value="1">Ene</asp:ListItem>
                    <asp:ListItem Value="2">Feb</asp:ListItem>
                    <asp:ListItem Value="3">Mar</asp:ListItem>
                    <asp:ListItem Value="4">Abr</asp:ListItem>
                    <asp:ListItem Value="5">May</asp:ListItem>
                    <asp:ListItem Value="6">Jun</asp:ListItem>
                    <asp:ListItem Value="7">Jul</asp:ListItem>
                    <asp:ListItem Value="8">Ago</asp:ListItem>
                    <asp:ListItem Value="9">Sep</asp:ListItem>
                    <asp:ListItem Value="10">Oct</asp:ListItem>
                    <asp:ListItem Value="11">Nov</asp:ListItem>
                    <asp:ListItem Value="12">Dic</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtAnioIni" runat="server" TabIndex="962" 
                    ToolTip="Esta es la fecha de vencimiento de la cotización" 
                    Width="39px">2025</asp:TextBox>
                    </td>
                    <td colspan="2">
                        Fecha Final:</td>
                    <td>
                <asp:DropDownList ID="cbDiaFin" runat="server" TabIndex="258" 
                    ToolTip="Esta es la fecha de vencimiento de la cotización">
                    <asp:ListItem>--</asp:ListItem>
                    <asp:ListItem Value="1">01</asp:ListItem>
                    <asp:ListItem Value="2">02</asp:ListItem>
                    <asp:ListItem Value="3">03</asp:ListItem>
                    <asp:ListItem Value="4">04</asp:ListItem>
                    <asp:ListItem Value="5">05</asp:ListItem>
                    <asp:ListItem Value="6">06</asp:ListItem>
                    <asp:ListItem Value="7">07</asp:ListItem>
                    <asp:ListItem Value="8">08</asp:ListItem>
                    <asp:ListItem Value="9">09</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="cbMesFin" runat="server" TabIndex="260" 
                    ToolTip="Esta es la fecha de vencimiento de la cotización">
                    <asp:ListItem>--</asp:ListItem>
                    <asp:ListItem Value="1">Ene</asp:ListItem>
                    <asp:ListItem Value="2">Feb</asp:ListItem>
                    <asp:ListItem Value="3">Mar</asp:ListItem>
                    <asp:ListItem Value="4">Abr</asp:ListItem>
                    <asp:ListItem Value="5">May</asp:ListItem>
                    <asp:ListItem Value="6">Jun</asp:ListItem>
                    <asp:ListItem Value="7">Jul</asp:ListItem>
                    <asp:ListItem Value="8">Ago</asp:ListItem>
                    <asp:ListItem Value="9">Sep</asp:ListItem>
                    <asp:ListItem Value="10">Oct</asp:ListItem>
                    <asp:ListItem Value="11">Nov</asp:ListItem>
                    <asp:ListItem Value="12">Dic</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtAnioFin" runat="server" TabIndex="962" 
                    ToolTip="Esta es la fecha de vencimiento de la cotización" 
                    Width="39px">2025</asp:TextBox>
                    </td>
                    <td>
                        <asp:LinkButton ID="lkCotizaciones" runat="server" onclick="lkCotizaciones_Click" 
                            ToolTip="Haga clic aquí para listar las cotizaciones en el rango de fechas indicado">Listar Cotizaciones</asp:LinkButton>
                        &nbsp;&nbsp;&nbsp;
                        <asp:LinkButton ID="lkOcultarCotizaciones" runat="server" onclick="lkOcultarCotizaciones_Click" 
                            ToolTip="Haga clic aquí ocultar la búsqueda de cotizaciones">Ocultar</asp:LinkButton>
                    </td>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                </table>
        </li>
        <li>
            <asp:GridView ID="gridCotizaciones" runat="server" BackColor="#DEBA84" 
                AutoGenerateColumns="False" style="text-align: left" 
                BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" TabIndex="588" 
                CellSpacing="2" onrowdatabound="gridCotizaciones_RowDataBound" 
                onselectedindexchanged="gridCotizaciones_SelectedIndexChanged" Visible="False">
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkEliminarCotizacion" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkEliminarCotizacion_Click" Text="Eliminar" 
                                ToolTip="Haga clic aquí para eliminar la cotización" 
                                onclientclick="return confirm('Desea eliminar la cotización?');" 
                                Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkSeleccionarCotizacion" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkSeleccionarCotizacion_Click" Text="Seleccionar" 
                                ToolTip="Haga clic aquí para seleccionar esta cotización y modificarla" 
                                Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkAsignar" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkAsignar_Click" Text="Asignar" 
                                ToolTip="Haga clic aquí para asignarle código de cliente a esta cotización" 
                                Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkImprimirCotizacion" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkImprimirCotizacion_Click" Text="Imprimir" 
                                ToolTip="Haga clic aquí para imprimir la cotización" Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkEnviarCotizacion" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkEnviarCotizacion_Click" Text="Enviar" 
                                ToolTip="Haga clic aquí para enviar la cotización vía correo electrónico" 
                                Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkInterconsumo" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkInterconsumo_Click" Text="Interconsumo" 
                                ToolTip="Haga clic aquí para actualizar intereses y cuotas en línea con Interconsumo" 
                                Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkSolicitudCotizacion" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkSolicitudCotizacion_Click" Text="Solicitud" 
                                ToolTip="Haga clic aquí para generar solicitud de crédito de esta cotización" 
                                Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkPagareCotizacion" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkPagareCotizacion_Click" Text="Pagaré" 
                                ToolTip="Haga clic aquí para imprimir el pagaré" Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkSolicitarCotizacion" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkSolicitarCotizacion_Click" Text="Solicitar" 
                                ToolTip="Haga clic aquí para hacer solicitudes varias sobre esta cotización" Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cotización">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCotizacion" runat="server" Text='<%# Bind("NumeroPedido") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbCotizacion" runat="server" Text='<%# Bind("NumeroPedido") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cliente" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbCliente" runat="server" Text='<%# Bind("Cliente") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCliente" runat="server" Text='<%# Bind("Cliente") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Fecha" Visible="true">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtFecha" runat="server" Text='<%# Bind("Fecha") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbFecha" runat="server" Text='<%# Bind("Fecha", "{0:d}") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Garantia" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbGarantia" runat="server" Text='<%# Bind("Garantia") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGarantia" runat="server" Text='<%# Bind("Garantia") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Observaciones" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbObservaciones" runat="server" Text='<%# Bind("Observaciones") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtObservaciones" runat="server" Text='<%# Bind("Observaciones") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ObservacionesTipoVenta" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbObservacionesTipoVenta" runat="server" Text='<%# Bind("ObservacionesTipoVenta") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtObservacionesTipoVenta" runat="server" Text='<%# Bind("ObservacionesTipoVenta") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Precio" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lbTotalFacturar" runat="server" Text='<%# Bind("TotalFacturar", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTotalFacturar" runat="server" Text='<%# Bind("TotalFacturar", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Enganche" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbEnganche" runat="server" Text='<%# Bind("Enganche", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtEnganche" runat="server" Text='<%# Bind("Enganche", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Saldo Financiar" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbSaldoFinanciar" runat="server" Text='<%# Bind("SaldoFinanciar", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtSaldoFinanciar" runat="server" Text='<%# Bind("SaldoFinanciar", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Recargos" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbRecargos" runat="server" Text='<%# Bind("Recargos", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRecargos" runat="server" Text='<%# Bind("Recargos", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Monto" Visible="True">
                        <ItemTemplate>
                            <asp:Label ID="lbMonto" runat="server" Text='<%# Bind("Monto", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtMonto" runat="server" Text='<%# Bind("Monto", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="TipoVenta" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lbTipoVenta" runat="server" Text='<%# Bind("TipoVenta") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTipoVenta" runat="server" Text='<%# Bind("TipoVenta") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Financiera" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbFinanciera" runat="server" Text='<%# Bind("Financiera") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtFinanciera" runat="server" Text='<%# Bind("Financiera") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tienda" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbTienda" runat="server" Text='<%# Bind("Tienda") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTienda" runat="server" Text='<%# Bind("Tienda") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vendedor" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Bodega" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lbBodega" runat="server" Text='<%# Bind("Bodega") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtBodega" runat="server" Text='<%# Bind("Bodega") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nombre" Visible="true">
                        <ItemTemplate>
                            <asp:Label ID="lbNombreCliente" runat="server" 
                                Text='<%# Bind("NombreCliente") %>' Font-Size="X-Small"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNombreCliente" runat="server" Text='<%# Bind("NombreCliente") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="EntregaAMPM" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbEntregaAMPM" runat="server" Text='<%# Bind("EntregaAMPM") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtEntregaAMPM" runat="server" Text='<%# Bind("EntregaAMPM") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="MercaderiaSale" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbMercaderiaSale" runat="server" Text='<%# Bind("MercaderiaSale") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtMercaderiaSale" runat="server" Text='<%# Bind("MercaderiaSale") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Desarmarla" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbDesarmarla" runat="server" Text='<%# Bind("Desarmarla") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDesarmarla" runat="server" Text='<%# Bind("Desarmarla") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Financiera" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lbNotasTipoVenta" runat="server" Text='<%# Bind("NotasTipoVenta") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNotasTipoVenta" runat="server" Text='<%# Bind("NotasTipoVenta") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nivel Precio" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lbNivelPrecio" runat="server" Text='<%# Bind("NivelPrecio") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNivelPrecio" runat="server" Text='<%# Bind("NivelPrecio") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Pagare" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbPagare" runat="server" Text='<%# Bind("Pagare") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPagare" runat="server" Text='<%# Bind("Pagare") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="NombreAutorizacion" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbNombreAutorizacion" runat="server" Text='<%# Bind("NombreAutorizacion") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNombreAutorizacion" runat="server" Text='<%# Bind("NombreAutorizacion") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Autorizacion" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbAutorizacion" runat="server" Text='<%# Bind("Autorizacion") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAutorizacion" runat="server" Text='<%# Bind("Autorizacion") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Solicitud" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbSolicitud" runat="server" Text='<%# Bind("Solicitud") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtSolicitud" runat="server" Text='<%# Bind("Solicitud") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CantidadPagos1" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbCantidadPagos1" runat="server" Text='<%# Bind("CantidadPagos1") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCantidadPagos1" runat="server" Text='<%# Bind("CantidadPagos1") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="MontoPagos1" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbMontoPagos1" runat="server" Text='<%# Bind("MontoPagos1") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtMontoPagos1" runat="server" Text='<%# Bind("MontoPagos1") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CantidadPagos2" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbCantidadPagos2" runat="server" Text='<%# Bind("CantidadPagos2") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCantidadPagos2" runat="server" Text='<%# Bind("CantidadPagos2") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="MontoPagos2" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbMontoPagos2" runat="server" Text='<%# Bind("MontoPagos2") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtMontoPagos2" runat="server" Text='<%# Bind("MontoPagos2") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="TipoReferencia" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbTipoReferencia" runat="server" Text='<%# Bind("TipoReferencia") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTipoReferencia" runat="server" Text='<%# Bind("TipoReferencia") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ObservacionesReferencia" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbObservacionesReferencia" runat="server" Text='<%# Bind("ObservacionesReferencia") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtObservacionesReferencia" runat="server" Text='<%# Bind("ObservacionesReferencia") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="TipoPedido" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lbTipoPedido" runat="server" Text='<%# Bind("TipoPedido") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTipoPedido" runat="server" Text='<%# Bind("TipoPedido") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vale" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbVale" runat="server" Text='<%# Bind("Vale") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtVale" runat="server" Text='<%# Bind("Vale") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cotizacion" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbCotizacion2" runat="server" Text='<%# Bind("Cotizacion") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCotizacion2" runat="server" Text='<%# Bind("Cotizacion") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vence" Visible="true">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtFechaEntrega" runat="server" Text='<%# Bind("FechaEntrega") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbFechaEntrega" runat="server" Text='<%# Bind("FechaEntrega", "{0:d}") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="NombreRecibe" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbNombreRecibe" runat="server" Text='<%# Bind("NombreRecibe") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNombreRecibe" runat="server" Text='<%# Bind("NombreRecibe") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PrimerPago" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPrimerPago" runat="server" Text='<%# Bind("PrimerPago") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbPrimerPago" runat="server" Text='<%# Bind("PrimerPago", "{0:d}") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#FFF1D4" />
                <SortedAscendingHeaderStyle BackColor="#B95C30" />
                <SortedDescendingCellStyle BackColor="#F1E5CE" />
                <SortedDescendingHeaderStyle BackColor="#93451F" />
            </asp:GridView>
            <asp:GridView ID="gridCotizacionesAutorizacion" runat="server" BackColor="#DEBA84" 
                AutoGenerateColumns="False" style="text-align: left" 
                BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" TabIndex="588" 
                CellSpacing="2" onrowdatabound="gridCotizacionesAutorizacion_RowDataBound" 
                onselectedindexchanged="gridCotizacionesAutorizacion_SelectedIndexChanged" Visible="False">
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkAsignarAutorizacion" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkAsignarAutorizacion_Click" Text="Asignar" 
                                ToolTip="Haga clic aquí para asignarle código de cliente a esta cotización" 
                                Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkSolicitudCotizacionAutorizacion" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkSolicitudCotizacionAutorizacion_Click" Text="Solicitud"                                
                                ToolTip="Haga clic aquí para generar solicitud de crédito de esta cotización" 
                                Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkPagareCotizacionAutorizacion" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkPagareCotizacionAutorizacion_Click" Text="Pagaré" 
                                ToolTip="Haga clic aquí para imprimir el pagaré" Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkRetractarCotizacion" runat="server" CausesValidation="False" 
                                CommandName="Select" onclick="lkRetractarCotizacion_Click" Text="Retractarse" 
                                onclientclick="return confirm('Desea retractarse de esta solicitud?');" 
                                ToolTip="Haga clic aquí para retractarse de la solicitud de descuento de esta cotización" Font-Size="X-Small"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cotización">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCotizacion" runat="server" Text='<%# Bind("Cotizacion") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbCotizacion" runat="server" Text='<%# Bind("Cotizacion") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Facturar" Visible="true">
                        <ItemTemplate>
                            <asp:Label ID="lbFacturar" runat="server" Text='<%# Bind("FacturaSolicitada", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtFacturar" runat="server" Text='<%# Bind("FacturaSolicitada", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Estatus" Visible="true">
                        <ItemTemplate>
                            <asp:Label ID="lbEstatus" runat="server" Text='<%# Bind("Estatus") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtEstatus" runat="server" Text='<%# Bind("Estatus") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nombre" Visible="true">
                        <ItemTemplate>
                            <asp:Label ID="lbNombre" runat="server" Text='<%# Bind("Nombre") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNombre" runat="server" Text='<%# Bind("Nombre") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ObservacionesSolicitud" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbObservacionesSolicitud" runat="server" Text='<%# Bind("ObservacionesSolicitud") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtObservacionesSolicitud" runat="server" Text='<%# Bind("ObservacionesSolicitud") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Gerente" Visible="true">
                        <ItemTemplate>
                            <asp:Label ID="lbGerente" runat="server" Text='<%# Bind("Gerente") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGerente" runat="server" Text='<%# Bind("Gerente") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Observaciones Gerente" Visible="true">
                        <ItemTemplate>
                            <asp:Label ID="lbObservacionesGerente" runat="server" Text='<%# Bind("ObservacionesAutoriza") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtObservacionesGerente" runat="server" Text='<%# Bind("ObservacionesAutoriza") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemStyle Font-Size="X-Small" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cliente" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbCliente" runat="server" Text='<%# Bind("Cliente") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCliente" runat="server" Text='<%# Bind("Cliente") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="TipoVenta" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbTipoVenta" runat="server" Text='<%# Bind("TipoVenta") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTipoVenta" runat="server" Text='<%# Bind("TipoVenta") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Financiera" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbFinanciera" runat="server" Text='<%# Bind("Financiera") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtFinanciera" runat="server" Text='<%# Bind("Financiera") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nivel" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbNivelPrecio" runat="server" Text='<%# Bind("Nivel") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNivelPrecio" runat="server" Text='<%# Bind("Nivel") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#FFF1D4" />
                <SortedAscendingHeaderStyle BackColor="#B95C30" />
                <SortedDescendingCellStyle BackColor="#F1E5CE" />
                <SortedDescendingHeaderStyle BackColor="#93451F" />
            </asp:GridView>
        </li>
        <li>
            <table align="center">
                <tr>
                    <td style="text-align: center">
                        &nbsp;
                        <asp:LinkButton ID="lkLimpiar" runat="server" 
                            ToolTip="Haga clic aquí para limpiar los datos de la página e ingresar una cotización nueva" 
                            TabIndex="590" onclick="lkLimpiar_Click">Limpiar</asp:LinkButton>
                        &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton ID="lkGrabar" runat="server" 
                            ToolTip="Haga clic aquí para grabar los datos de la cotización" TabIndex="600" 
                            onclick="lkGrabar_Click">Grabar</asp:LinkButton>
                    </td>
                </tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
            </table>
        </li>
    </ul>
    </div>
        <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server" TabIndex="620"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" 
                        TabIndex="630"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
