﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MF_Clases;
using System.Web.Configuration;
using System.Globalization;
using PuntoDeVenta.Util;
using RestSharp;
using System.Threading;
using MF_Clases.Facturacion;
using System.Text.RegularExpressions;
using MF.Comun.Dto.Facturacion;

namespace PuntoDeVenta
{
    public partial class cotizaciones : BasePage
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                string strEnviarSolicitud = Request.QueryString["EnviarSol"];
                string strCoti = Request.QueryString["P"];
                LimpiarListaArticulos();

                if (strCoti != null)
                    Session["strCoti"] = strCoti;

                string strRespuesta = Request.QueryString["ErrorMsg2"];
                
                ViewState["url"] = Convert.ToString(Session["Url"]);
                ViewState["urlPrecios"] = Convert.ToString(Session["UrlPrecios"]);

                List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();
                ViewState["qArticulos"] = q;
                ViewState["Accesorios"] = "N";

                CargarTiposFinancieras();
                
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mCajeros = ws.DevuelveCajeros();
                string mCajerosCambianTienda = ws.DevuelveCajerosCambianTienda();
                
                if (mCajeros.Contains(Convert.ToString(Session["Usuario"])))
                {
                    cbVendedor.Visible = true;
                    if (mCajerosCambianTienda.Contains(Convert.ToString(Session["Usuario"]))) cbTienda.Visible = true;

                    AsignaTiendaCombo();
                    cbBodega.SelectedValue = cbTienda.SelectedValue;
                }
                else
                {
                    AsignaTiendaCombo();
                    cbBodega.SelectedValue = cbTienda.SelectedValue;

                    cbTienda.Visible = false;
                    cbVendedor.Visible = false;
                }

                txtProfesion.Text = "";

                //Validando la respuesta de CREDIPLUS
                if (strRespuesta != null)
                {
                    Session["Data"] = null;
                    Session["delay"] = null;
                    if (strRespuesta.ToUpper().Contains("ERROR") || strRespuesta.ToUpper().Contains("PROBLEMA"))
                        lbError.Text = strRespuesta;
                    else
                    {
                        lbInfo.Text = "<div><b>" + strRespuesta + "</b><br/>";
                    }
                    //limpiar el QueryString de la pantalla
                    string s = @"<script language=JavaScript>var clean_uri = location.protocol + '//' + location.host + location.pathname; window.history.replaceState({}, document.title, clean_uri);</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "test", s);

                }



            }

        }
        
        void AsignaTiendaCombo()
        {
            if (Convert.ToString(Session["NombreVendedor"]) == "Alerta")
            {
                lkGrabar.Visible = false;
                lkVale.Visible = false;
                lkSolicitar.Visible = false;
                cbTienda.SelectedValue = "F01";
                lbError.Text = "No tiene vendedor asignado, informar de inmediato al departamento de informática.";
            }
            else
            {
                cbVendedor.SelectedValue = null;
                cbTienda.SelectedValue = Convert.ToString(Session["Tienda"]);
                CargaVendedores();
                cbVendedor.SelectedValue = Convert.ToString(Session["Vendedor"]);
            }
        }
        
        void CargarCliente(string tipo, string cliente, string nit)
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveCliente(tipo, cliente, nit);

            txtCodigo.Text = q[0].Cliente;
            txtNombre.Text = q[0].Nombre;
            txtNit.Text = q[0].Nit;

            lkAsignarCliente.Focus();
        }

        void ValidarDatosCrediticios(string tipoVenta, string financiera, string nivel)
        {
            if (cbNivelPrecio.SelectedValue != "Seleccione ...")
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mTipo = ws.DevuelveTipoNivelPrecio(tipoVenta, financiera, nivel);

                if (mTipo == "CR")
                {
                    if (txtProfesion.Text.Trim().Length == 0)
                    {
                        Session["TipoVenta"] = cbTipo.SelectedValue;
                        Session["Financiera"] = cbFinanciera.SelectedValue;
                        Session["NivelPrecio"] = cbNivelPrecio.SelectedValue;
                        Response.Redirect("clientes.aspx");
                    }
                    else
                    {
                        Session["TipoVenta"] = null;
                        Session["Financiera"] = null;
                        Session["NivelPrecio"] = null;
                    }
                }
            }
        }

        void CargarTiposFinancieras()
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var qTiposVenta = ws.DevuelveTiposVentaSeleccion(Convert.ToString(Session["Tienda"])).ToList();

                cbTipo.DataSource = qTiposVenta;
                cbTipo.DataTextField = "Descripcion";
                cbTipo.DataValueField = "CodigoTipoVenta";
                cbTipo.DataBind();

                cbTipo.SelectedValue = "NR";
                CargaFinancieras();
            }
            catch (Exception ex)
            {
                string.Format("Error al cargar las promociones {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }

        void CargaFinancieras()
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var qFinancieras = ws.DevuelveFinancierasSeleccion(cbTipo.SelectedValue).ToList();

                wsPuntoVenta.Financieras itemFinanciera = new wsPuntoVenta.Financieras();
                itemFinanciera.Financiera = 0;
                itemFinanciera.Nombre = "Seleccione financiera ...";
                itemFinanciera.prefijo = "";
                itemFinanciera.Estatus = "Activa";
                qFinancieras.Add(itemFinanciera);

                cbFinanciera.DataSource = qFinancieras;
                cbFinanciera.DataTextField = "Nombre";
                cbFinanciera.DataValueField = "Financiera";
                cbFinanciera.DataBind();

                cbFinanciera.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                string.Format("Error al cargar las financieras {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }

        void CargaNivelesDePrecio()
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var qNivelesPrecio = ws.DevuelveNivelesPrecioSeleccionFinanciera(cbFinanciera.SelectedValue, Convert.ToString(Session["Tienda"])).ToList();

                wsPuntoVenta.NivelesPrecio itemNivel = new wsPuntoVenta.NivelesPrecio();
                itemNivel.NivelPrecio = "Seleccione ...";
                itemNivel.Factor = 0;
                itemNivel.TipoVenta = "";
                itemNivel.TipoDeVenta = "";
                itemNivel.Financiera = 0;
                itemNivel.NombreFinanciera = "";
                itemNivel.Estatus = "";
                itemNivel.Tipo = "";
                itemNivel.Pagos = "";
                qNivelesPrecio.Add(itemNivel);

                cbNivelPrecio.DataSource = qNivelesPrecio;
                cbNivelPrecio.DataTextField = "NivelPrecio";
                cbNivelPrecio.DataValueField = "NivelPrecio";
                cbNivelPrecio.DataBind();

                cbNivelPrecio.SelectedValue = "Seleccione ...";
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al cargar los niveles de precio {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }

        void limpiar(bool nuevo)
        {
            try
            {
                lbError.Text = "";
                lbError2.Text = "";
                lbError2.Visible = false;
                lbInfo.Text = "";
                tblClientes.Visible = false;
                txtDescuentos.Text = "0.00";
                txtDescuentoItem.Text = "0.00";
                txtDescMax.Text = "0.00";
                txtSaldoDescuento.Text = "0.00";
                CargaVendedores();
                Session["lstNuevoPrecioProducto"] = null;
                lbInfoSolicitud.Text = "";
                lbErrorSolicitud.Text = "";
                txtSolicitudPrecio.Text = "";
                txtSolicitudObservaciones.Text = "";
                tblSolicitudDescuento.Visible = false;

                tblSolicutudAutorizacion.Visible = false;
                lbEstadoSolicitud.Visible = false;
                lkSolicitudEnLinea.Visible = false;
                lkPagareEnLinea.Visible = false;

                cbTipo.Enabled = true;
                cbFinanciera.Enabled = true;
                cbNivelPrecio.Enabled = true;
                articulo.ReadOnly = false;
                descripcion.ReadOnly = true;
                txtPrecioUnitario.ReadOnly = false;
                txtPrecioUnitario.AutoPostBack = true;
                cbRequisicion.Enabled = true;
                cbLocalizacion.Enabled = true;

                //if (Convert.ToString(Session["Usuario"]) == "ROLAPINE" || Convert.ToString(Session["usuario"]) == "SANDMATA")
                //{
                //    txtPrecioUnitario.ReadOnly = false;
                //    txtPrecioUnitario.AutoPostBack = true;
                //}

                cbRequisicion.SelectedValue = "No";
                cbLocalizacion.SelectedValue = "ARMADO";
                cbBodega.SelectedValue = cbTienda.SelectedValue;

                if (nuevo)
                {
                    cbTipoPedido.SelectedValue = "N";
                    txtPedido.Text = "000000";
                    txtFecha.Text = DateTime.Now.Date.ToShortDateString();
                }

                txtValor.Text = "0.00";
                txtIntereses.Text = "0.00";
                txtFacturar.Text = "0.00";
                txtFacturarItem.Text = "0.00";
                txtPrecioUnitarioDebioFacturar.Text = "0.00";

                articulo.Text = "";
                descripcion.Text = "";
                txtCantidad.Text = "1";
                txtPrecioUnitario.Text = "0.00";
                txtTotal.Text = "0.00";
                txtOferta.Text = "N";
                txtFechaOfertaDesde.Text = new DateTime(1980, 1, 1).ToShortDateString();
                txtPrecioOriginal.Text = "0.00";
                txtTipoOferta.Text = "N";
                txtVale.Text = "0";
                txtCotizacion.Text = "0";
                txtCondicional.Text = "N";
                txtArticuloCondicion.Text = "";
                txtAutorizacion.Text = "0";
                txtCantidad.ReadOnly = false;

                txtEnganche.Text = "0.00";
                txtSaldoFinanciar.Text = "0.00";
                txtMonto.Text = "0.00";

                cbPagos1.SelectedValue = "01";
                cbPagos2.SelectedValue = "01";
                txtPagos1.Text = "0.00";
                txtPagos2.Text = "0.00";

                if (nuevo)
                {
                    txtPagare.Text = "";
                    txtSolicitud.Text = "";
                    txtQuienAutorizo.Text = "";
                    txtNoAutorizacion.Text = "";
                    txtGarantia.Text = "";

                    txtObservaciones.Text = "";
                    txtNotasTipoVenta.Text = "";
                    txtNotas.Text = "";
                }

                cbPrensa.Checked = false;
                cbRadio.Checked = false;
                cbInternet.Checked = false;
                cbVolante.Checked = false;
                cbTelevision.Checked = false;
                cbQuetzalteco.Checked = false;
                cbNuestroDiario.Checked = false;
                cbOtro.Checked = false;
                txtOtro.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mMensaje = "";
                Int32 mDiasVigencia = 0; Int32 mDiasGracia = 0;

                if (!ws.DevuelveDiasCotizaciones(ref mMensaje, ref mDiasVigencia, ref mDiasGracia))
                {
                    lbError.Text = mMensaje;
                    return;
                }

                DateTime mFechaVence = DateTime.Now.Date.AddDays(mDiasVigencia);

                cbDia.SelectedValue = mFechaVence.Day.ToString();
                cbMes.SelectedValue = mFechaVence.Month.ToString();
                txtAnio.Text = mFechaVence.Year.ToString();

                DateTime mPrimerPago = DateTime.Now.Date.AddDays(30);
                cbDiaPrimerPago.SelectedValue = mPrimerPago.Day.ToString();
                cbMesPrimerPago.SelectedValue = mPrimerPago.Month.ToString();
                txtAnioPrimerPago.Text = mPrimerPago.Year.ToString();


                if (cbTipo.SelectedValue != "NR")
                {
                    mFechaVence = ws.FechaVencePromocion(cbTipo.SelectedValue);
                    cbDia.SelectedValue = mFechaVence.Day.ToString();
                    cbMes.SelectedValue = mFechaVence.Month.ToString();
                    txtAnio.Text = mFechaVence.Year.ToString();
                }

                if (nuevo)
                {
                    txtNombreRecibe.Text = "";
                }
                cbEntrega.SelectedValue = "AM";

                txtNumeroFactura.Text = "";
                cbNumeroFactura.Checked = false;
                tblFactura.Visible = false;

                ViewState["Factura"] = "";
                lkFacturar.Enabled = false;
                lkDespachar.Enabled = false;

                txtFacturado.Text = "N";
                txtDespachado.Text = "N";
                txtNumeroDespacho.Text = "";
                cbNumeroDespacho.Checked = false;
                txtObservacionesDespacho.Text = "";
                tblDespacho.Visible = false;

                txtNotasTipoVenta.ToolTip = string.Format("Use esta casilla para anotar información adicional si la compra corresponde a una promoción.{0}{0}Si se trata de una promoción de La Torre se le pedirá que ingrese UNICAMENTE la tienda de La Torre donde cliente realizó su compra, es decir, no es necesario que escriba la frase 'La Torre'", System.Environment.NewLine);

                tblBuscar.Visible = false;
                gridClientes.Visible = false;
                gridLocalizaciones.Visible = false;

                List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();
                ViewState["qArticulos"] = q;

                ViewState["LaTorre"] = false;
                ViewState["Accesorios"] = "N";

                gridArticulos.DataSource = q;
                gridArticulos.DataBind();

                gridVales.Visible = false;
                gridPedidos.Visible = false;

                tablaBusquedaDet.Visible = false;
                gridArticulosDet.Visible = false;
                tblSolicutudAutorizacion.Visible = false;
                tblCotizaciones.Visible = false;
                gridCotizaciones.Visible = false;
                gridCotizacionesAutorizacion.Visible = false;

                tblInfo.Visible = false;
                lkGrabar.Enabled = true;
                lkGrabar.Visible = true;

                lbError2.Text = "";
                lbError2.Visible = false;

                Label6.Text = "";
                Label7.Text = "";

                if (cbNivelPrecio.Text.Trim().Length > 0 && cbNivelPrecio.Text != "Seleccione ..." && cbFinanciera.Text != "Seleccione financiera ..." && cbFinanciera.Text != "0")
                {
                    double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                    cbNivelPrecio.ToolTip = string.Format("Factor: {0}", String.Format("{0:0.000000}", mFactor));

                    articulo.Focus();
                }
                else
                {
                    if (cbNivelPrecio.Text == "Seleccione ...")
                    {
                        cbNivelPrecio.Focus();
                    }
                    else
                    {
                        cbTipo.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al limpiar la página {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }

        protected void lbBuscarCliente_Click(object sender, EventArgs e)
        {
            mostrarBusquedaCliente();
        }

        void mostrarBusquedaCliente()
        {
            tblBuscar.Visible = true;
            gridArticulosDet.Visible = false;
            tablaBusquedaDet.Visible = false;
            tblSolicutudAutorizacion.Visible = false;

            lbError.Text = "";
            lbError2.Text = "";
            lbError2.Visible = false;
            txtCodigoBuscar.Text = "";
            txtNombreBuscar.Text = "";
            txtNitBuscar.Text = "";

            txtCodigo.Text = "";
            txtNombre.Text = "";
            txtNit.Text = "";
            lbInfoCliente.Text = "";
            lbErrorCliente.Text = "";

            txtNombreBuscar.Focus();
        }

        protected void cbFinanciera_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaNivelesDePrecio();
            limpiar(true);
        }

        protected void lkOcultarBusqueda_Click(object sender, EventArgs e)
        {
            tblClientes.Visible = false;
            tblBuscar.Visible = false;
            gridClientes.Visible = false;
            cbTipo.Focus();
        }

        protected void txtCodigoBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtCodigoBuscar.Text.Trim().Length == 0) return;
            BuscarCliente("C");
        }

        protected void txtNombreBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtNombreBuscar.Text.Trim().Length == 0) return;

            string mTipo = "N";
            try
            {
                Int32 mNumero = Convert.ToInt32(txtNombreBuscar.Text.Substring(0, 1));
                mTipo = "C";
            }
            catch
            {
                mTipo = "N";
            }

            BuscarCliente(mTipo);
        }

        protected void txtNitBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtNitBuscar.Text.Trim().Length == 0) return;
            BuscarCliente("T");
        }

        protected void txtTelefono_TextChanged(object sender, EventArgs e)
        {
            if (txtTelefono.Text.Trim().Length == 0) return;
            BuscarCliente("E");
        }

        void BuscarCliente(string tipo)
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            string mCodigo = txtCodigoBuscar.Text.Trim();
            string mNombre = txtNombreBuscar.Text.Trim().ToUpper();

            if (tipo == "C" && mNombre.Trim().Length > 0)
            {
                mCodigo = mNombre;
                mNombre = "";
            }

            var q = ws.DevuelveClientes(tipo, mCodigo, mNombre, txtNitBuscar.Text.Trim().ToUpper(), txtTelefono.Text.Trim());

            gridClientes.DataSource = q;
            gridClientes.DataBind();
            gridClientes.Visible = true;
            gridClientes.SelectedIndex = -1;

            lbError.Text = "";
            lbError2.Text = "";
            lbError2.Visible = false;

            if (q.Length == 0) lbError.Text = "No existen clientes con el criterio de búsqueda ingresado.";

            if (q.Count() == 1)
            {
                gridClientes.SelectedIndex = 0;
                seleccionarCliente();
            }
            if (q.Count() > 0)
            {
                gridClientes.Focus();
                gridClientes.SelectedIndex = 0;
                LinkButton lkSeleccionar = (LinkButton)gridClientes.SelectedRow.FindControl("lkSeleccionar");

                lkSeleccionar.Focus();
            }
        }

        void seleccionarCliente()
        {
            GridViewRow gvr = gridClientes.SelectedRow;
            CargarCliente("C", gvr.Cells[1].Text, "I");
        }

        protected void gridClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            AsignarClienteGrid();
        }

        protected void lkSeleccionar_Click(object sender, EventArgs e)
        {

        }

        bool pasaValidaciones()
        {
            try
            {
                if (gridArticulos.Rows.Count == 0)
                {
                    lbError.Text = "La cotización debe contener al menos un artículo.";
                    articulo.Focus();
                    return false;
                }

                decimal mPago1, mPago2;

                try
                {
                    mPago1 = Convert.ToDecimal(txtPagos1.Text);
                }
                catch
                {
                    lbError.Text = "El monto de los pagos es inválido.";
                    txtPagos1.Focus();
                    return false;
                }
                if (mPago1 < 0)
                {
                    lbError.Text = "El monto de los pagos es inválido.";
                    txtPagos1.Focus();
                    return false;
                }

                try
                {
                    mPago2 = Convert.ToDecimal(txtPagos2.Text);
                }
                catch
                {
                    lbError.Text = "El monto de los pagos es inválido.";
                    txtPagos2.Focus();
                    return false;
                }
                if (mPago1 < 0)
                {
                    lbError.Text = "El monto de los pagos es inválido.";
                    txtPagos2.Focus();
                    return false;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mTipo = ws.DevuelveTipoNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

                if (mTipo == "CR")
                {
                    int mPagos1 = Convert.ToInt32(cbPagos1.SelectedValue);
                    int mPagos2 = Convert.ToInt32(cbPagos2.SelectedValue);

                    decimal mMonto = Convert.ToDecimal(txtMonto.Text);

                    if (mMonto != ((mPagos1 * mPago1) + (mPagos2 * mPago2)))
                    {
                        lbError.Text = "El valor de las pagos no coincide con el monto de la cotización.";
                        txtPagos1.Focus();
                        return false;
                    }
                }

                if (txtQuienAutorizo.Text.Trim().Length == 0)
                {
                    lbError.Text = "Debe ingresar el nombre de a quién va dirigida la cotización.";
                    txtQuienAutorizo.Focus();
                    return false;
                }

                if (cbInternet.Checked)
                {
                    if (txtNombreRecibe.Text.Trim().Length == 0)
                    {
                        lbError.Text = "Debe ingresar el correo electrónico para enviar la cotización";
                        txtNombreRecibe.Focus();
                        return false;
                    }
                    if (!txtNombreRecibe.Text.Contains("@"))
                    {
                        lbError.Text = "Debe ingresar un Email válido.";
                        txtNombreRecibe.Focus();
                        return false;
                    }
                }

                var qNiveles = ws.DevuelveTipoVentaNiveles(cbTipo.SelectedValue);

                if (qNiveles.Length > 0)
                {
                    bool mNivelValido = false;
                    for (int ii = 0; ii < qNiveles.Length; ii++)
                    {
                        if (qNiveles[ii].Nivel == cbNivelPrecio.SelectedValue) mNivelValido = true;
                    }

                    if (!mNivelValido)
                    {
                        lbError.Text = string.Format("El nivel {0} no es válido para {1}", cbNivelPrecio.Text, lkLaTorre.Text);
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al validar los datos {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
                return false;
            }
        }

        bool VerificaMontoInterconsumo()
        {
            try
            {
                decimal mEnganche = 0;
                decimal mFacturar = Convert.ToDecimal(txtFacturar.Text.Replace(",", ""));
                decimal mSaldoFinanciar = Convert.ToDecimal(txtSaldoFinanciar.Text.Replace(",", ""));

                try
                {
                    mEnganche = Convert.ToDecimal(txtEnganche.Text.Replace(",", ""));
                }
                catch
                {
                    mEnganche = 0;
                }

                if (mSaldoFinanciar < 1001)
                {
                    if (mEnganche > 0)
                    {
                        lbError.Text = "El saldo a financiar en Interconsumo debe ser mayor o igual a Q1,001.00, debe modificar el enganche";
                    }
                    else
                    {
                        lbError.Text = "El saldo a financiar en Interconsumo debe ser mayor o igual a Q1,001.00";
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al validar el monto mínimo de Interconsumo. {0}", ex.Message);
                return false;
            }

            return true;
        }

        protected void lkGrabar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!pasaValidaciones()) return;
                
                lbInfo.Text = "";
                lbError.Text = "";
                lbError2.Text = "";
                lbError2.Visible = false;

                tblBuscar.Visible = false;
                tablaBusquedaDet.Visible = false;

                gridClientes.Visible = false;
                gridArticulosDet.Visible = false;
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);
                var factor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);


                string mMensaje = ""; string mPedido = txtPedido.Text; string mReferencia = "";
                decimal mTotalFacturar, mEnganche, mSaldoFinanciar, mRecargos, mMonto, mMontoPagos1, mMontoPagos2, mPrecioOriginal, descuentos;
                try
                {
                    descuentos = Convert.ToDecimal(txtDescuentos.Text.Replace(",", ""));
                }
                catch
                {
                    descuentos = 0;
                }

                try
                {
                    mTotalFacturar = Convert.ToDecimal(txtFacturar.Text.Replace(",", ""));
                }
                catch
                {
                    mTotalFacturar = 0;
                }
                try
                {
                    mEnganche = Convert.ToDecimal(txtEnganche.Text.Replace(",", ""));
                }
                catch
                {
                    mEnganche = 0;
                }
                try
                {
                    mSaldoFinanciar = Convert.ToDecimal(txtSaldoFinanciar.Text.Replace(",", ""));
                }
                catch
                {
                    mSaldoFinanciar = 0;
                }
                try
                {
                    mRecargos = Convert.ToDecimal(txtIntereses.Text.Replace(",", ""));
                }
                catch
                {
                    mRecargos = 0;
                }
                try
                {
                    mMonto = Convert.ToDecimal(txtMonto.Text.Replace(",", ""));
                }
                catch
                {
                    mMonto = 0;
                }
                try
                {
                    mMontoPagos1 = Convert.ToDecimal(txtPagos1.Text.Replace(",", ""));
                }
                catch
                {
                    mMontoPagos1 = 0;
                }
                try
                {
                    mMontoPagos2 = Convert.ToDecimal(txtPagos2.Text.Replace(",", ""));
                }
                catch
                {
                    mMontoPagos2 = 0;
                }

                if (txtOtro.Text.Trim().Length > 0) cbOtro.Checked = true;

                if (cbRadio.Checked) mReferencia = "R";
                if (cbInternet.Checked) mReferencia = string.Format("{0}I", mReferencia);
                if (cbVolante.Checked) mReferencia = string.Format("{0}V", mReferencia);
                if (cbPrensa.Checked) mReferencia = string.Format("{0}P", mReferencia);
                if (cbQuetzalteco.Checked) mReferencia = string.Format("{0}Q", mReferencia);
                if (cbNuestroDiario.Checked) mReferencia = string.Format("{0}N", mReferencia);
                if (cbTelevision.Checked) mReferencia = string.Format("{0}T", mReferencia);
                if (cbOtro.Checked) mReferencia = string.Format("{0}O", mReferencia);

                DateTime mFechaEntrega = new DateTime(1980, 1, 1);

                DateTime mPrimerPago = new DateTime(1980, 1, 1);

                try
                {
                    if (txtAnioPrimerPago.Text.Trim().Length == 2) txtAnioPrimerPago.Text = string.Format("20{0}", txtAnio.Text);
                    mPrimerPago = new DateTime(Convert.ToInt32(txtAnioPrimerPago.Text), Convert.ToInt32(cbMesPrimerPago.SelectedValue), Convert.ToInt32(cbDiaPrimerPago.SelectedValue));
                }
                catch
                {
                    mPrimerPago = new DateTime(1980, 1, 1);
                }

                if (mPrimerPago < DateTime.Now.Date)
                {
                    lbError.Text = "La fecha del primer pago es inválida";
                    return;
                }

               
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                wsPuntoVenta.Pedido[] q = new wsPuntoVenta.Pedido[1];
                wsPuntoVenta.PedidoLinea[] qLinea = new wsPuntoVenta.PedidoLinea[1];

                string mTienda = Convert.ToString(Session["Tienda"]);
                string mVendedor = Convert.ToString(Session["Vendedor"]);

                wsPuntoVenta.Pedido itemPedido = new wsPuntoVenta.Pedido();
                itemPedido.NumeroPedido = txtPedido.Text;
                itemPedido.DescuentoTotal = descuentos;
                itemPedido.Cliente = txtCodigo.Text;
                itemPedido.Fecha = DateTime.Now.Date;
                itemPedido.Garantia = txtGarantia.Text.ToUpper();
                itemPedido.Observaciones = txtObservaciones.Text.ToUpper();
                itemPedido.ObservacionesTipoVenta = txtNotasTipoVenta.Text.ToUpper();
                itemPedido.TotalFacturar = mTotalFacturar;
                itemPedido.Enganche = mEnganche;
                itemPedido.SaldoFinanciar = mSaldoFinanciar;
                itemPedido.Recargos = mRecargos;
                itemPedido.Monto = Math.Round(mMonto,2,MidpointRounding.AwayFromZero);
                itemPedido.TipoVenta = cbTipo.SelectedValue;
                itemPedido.Financiera = Convert.ToInt32(cbFinanciera.SelectedValue);
                itemPedido.NivelPrecio = cbNivelPrecio.SelectedValue;
                itemPedido.Tienda = mTienda;
                itemPedido.Vendedor = mVendedor;
                itemPedido.Bodega = cbBodega.SelectedValue;
                itemPedido.NombreCliente = txtQuienAutorizo.Text.ToUpper();
                itemPedido.EntregaAMPM = "A";
                itemPedido.MercaderiaSale = cbBodega.SelectedValue;
                itemPedido.Desarmarla = "N";
                itemPedido.NotasTipoVenta = txtNotasTipoVenta.Text.ToUpper();
                itemPedido.Pagare = txtPagare.Text.ToUpper();
                itemPedido.NombreAutorizacion = txtQuienAutorizo.Text.ToUpper();
                itemPedido.Autorizacion = txtNoAutorizacion.Text.ToUpper();
                itemPedido.Solicitud = txtSolicitud.Text.ToUpper();
                itemPedido.CantidadPagos1 = Convert.ToInt32(cbPagos1.SelectedValue);
                itemPedido.MontoPagos1 = mMontoPagos1;
                itemPedido.CantidadPagos2 = Convert.ToInt32(cbPagos2.SelectedValue);
                itemPedido.MontoPagos2 = mMontoPagos2;
                itemPedido.TipoReferencia = mReferencia;
                itemPedido.ObservacionesReferencia = txtOtro.Text.ToUpper();
                itemPedido.TipoPedido = cbTipoPedido.SelectedValue;
                itemPedido.Cotizacion = Convert.ToInt32(txtCotizacion.Text);
                itemPedido.EntregaAMPM = cbEntrega.SelectedValue;
                itemPedido.NombreRecibe = txtNombreRecibe.Text.ToLower();
                itemPedido.FechaEntrega = mFechaEntrega;
                itemPedido.PrimerPago = mPrimerPago;
                itemPedido.DescuentoAccesorios = Convert.ToString(ViewState["Accesorios"]);
               
                decimal descTotal = 0;
                int jj = 0;
                for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                {
                    jj++;

                    Label lbPrecioUnitario = (Label)gridArticulos.Rows[ii].FindControl("lbPrecio");
                    Label lbCantidadPedida = (Label)gridArticulos.Rows[ii].FindControl("lbCantidad");
                    Label lbPrecioTotal = (Label)gridArticulos.Rows[ii].FindControl("lbTotal");                    
                    Label lbDescripcion = (Label)gridArticulos.Rows[ii].FindControl("lbDescripcion");
                    Label lbComentario = (Label)gridArticulos.Rows[ii].FindControl("lbComentario");
                    Label lbEstado = (Label)gridArticulos.Rows[ii].FindControl("lbEstado");
                    Label lbNumeroPedido = (Label)gridArticulos.Rows[ii].FindControl("lbNumeroPedido");
                    Label lbOferta = (Label)gridArticulos.Rows[ii].FindControl("lbOferta");
                    Label lbFechaOfertaDesde = (Label)gridArticulos.Rows[ii].FindControl("lbFechaOfertaDesde");
                    Label lbPrecioOriginal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioOriginal");
                    Label lbEsDetalleKit = (Label)gridArticulos.Rows[ii].FindControl("lbEsDetalleKit");
                    Label lbPrecioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioFacturar");
                    Label lbTipoOferta = (Label)gridArticulos.Rows[ii].FindControl("lbTipoOferta");
                    Label lbVale = (Label)gridArticulos.Rows[ii].FindControl("lbVale");
                    Label lbAutorizacion = (Label)gridArticulos.Rows[ii].FindControl("lbAutorizacion");
                    Label lbBodega = (Label)gridArticulos.Rows[ii].FindControl("lbBodega");
                    Label lbLocalizacion = (Label)gridArticulos.Rows[ii].FindControl("lbLocalizacion");
                    LinkButton lkRequisicionArmado = (LinkButton)gridArticulos.Rows[ii].FindControl("lkRequisicionArmado");
                    Label lbArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbArticulo");
                    Label lbNombreArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbNombreArticulo");
                    Label lbGel = (Label)gridArticulos.Rows[ii].FindControl("lbGel");
                    Label lbPorcentajeDescuento = (Label)gridArticulos.Rows[ii].FindControl("lbPorcentajeDescuento");
                    Label lbBeneficiario = (Label)gridArticulos.Rows[ii].FindControl("lbBeneficiario");
                    var descuento = ((Label)gridArticulos.Rows[ii].FindControl("lbDescuento")).Text;
                    var Porcdescuento = ((Label)gridArticulos.Rows[ii].FindControl("lbPorcentajeDescuento")).Text;
                    decimal PrecioSugerido = 0;

                    try
                    {
                        PrecioSugerido = decimal.Parse(((Label)gridArticulos.Rows[ii].FindControl("lbPrecioSugerido")).Text.Replace(",",""));
                    }
                    catch
                    {
                        PrecioSugerido = decimal.Parse(lbPrecioTotal.Text.Replace(",", ""));
                    }
                    DateTime mFechaOfertaDesde = new DateTime(1980, 1, 1);

                    try
                    {
                        mFechaOfertaDesde = new DateTime(Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(6, 4)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(3, 2)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(0, 2)));
                    }
                    catch
                    {
                        //Nothing
                    }
                    try
                    {
                        mPrecioOriginal = Convert.ToDecimal(lbPrecioOriginal.Text.Replace(",", ""));
                    }
                    catch
                    {
                        mPrecioOriginal = 0;
                    }

                    wsPuntoVenta.PedidoLinea itemPedidoLinea = new wsPuntoVenta.PedidoLinea();

                    double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                    decimal PrecioFacturarCompleto = Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", ""));
                        //Math.Round(Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", "")) + decimal.Parse(descuento.Replace(",", "")), 2, MidpointRounding.AwayFromZero) - decimal.Parse(lbPrecioTotal.Text.Replace(",","")) > 1 ? Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", "")) :
                        //                   Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", "")) + decimal.Parse(descuento.Replace(",", ""));

                    itemPedidoLinea.Articulo = lbArticulo.Text;
                    itemPedidoLinea.Nombre = lbNombreArticulo.Text;
                    itemPedidoLinea.PrecioUnitario = Convert.ToDecimal(lbPrecioUnitario.Text.Replace(",", ""));
                    itemPedidoLinea.CantidadPedida = Convert.ToDecimal(lbCantidadPedida.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioSugerido = PrecioSugerido;
                    itemPedidoLinea.PrecioFacturar = Math.Round(PrecioFacturarCompleto,2,MidpointRounding.AwayFromZero);

                    itemPedidoLinea.PrecioTotal = decimal.Parse(lbPrecioTotal.Text.Replace(",", ""));
                    itemPedidoLinea.Bodega = lbBodega.Text;
                    itemPedidoLinea.Localizacion = lbLocalizacion.Text;
                    itemPedidoLinea.RequisicionArmado = lkRequisicionArmado.Text;
                    itemPedidoLinea.Descripcion = lbDescripcion.Text;
                    itemPedidoLinea.Comentario = lbComentario.Text;
                    itemPedidoLinea.Estado = lbEstado.Text;
                    itemPedidoLinea.NumeroPedido = lbNumeroPedido.Text;
                    itemPedidoLinea.Oferta = lbOferta.Text;
                    itemPedidoLinea.FechaOfertaDesde = mFechaOfertaDesde;
                    itemPedidoLinea.PrecioOriginal = Math.Round(Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", "")) + decimal.Parse(descuento.Replace(",", "")), 2, MidpointRounding.AwayFromZero) - itemPedidoLinea.PrecioTotal > 1 ?
                                                    Math.Round((Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", "")) - decimal.Parse(descuento.Replace(",", ""))) * (decimal)mFactor, 2, MidpointRounding.AwayFromZero)
                                                    : Math.Round((Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", ""))) * (decimal)mFactor, 2, MidpointRounding.AwayFromZero);
                    // Math.Round(Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", "")) * (decimal)mFactor, 2, MidpointRounding.AwayFromZero);

                    itemPedidoLinea.TipoOferta = lbTipoOferta.Text;
                    itemPedidoLinea.EsDetalleKit = lbEsDetalleKit.Text;
                    itemPedidoLinea.Vale = Convert.ToInt32(lbVale.Text == "" ? "0" : lbVale.Text);
                    itemPedidoLinea.Autorizacion = Convert.ToInt64(lbAutorizacion.Text == "" ? "0" : lbAutorizacion.Text);
                    itemPedidoLinea.Gel = lbGel.Text;
                    itemPedidoLinea.PorcentajeDescuento = decimal.Parse(Porcdescuento.Replace(",", ""));
                    itemPedidoLinea.Beneficiario = lbBeneficiario.Text;
                    itemPedidoLinea.Descuento = decimal.Parse(descuento.Replace(",", ""));
                    if (jj > 1) Array.Resize(ref qLinea, jj);
                    qLinea[jj - 1] = itemPedidoLinea;

                    descTotal += decimal.Parse(descuento.Replace(",", ""));
                }
                var totalFacturar = qLinea.Sum(x => x.PrecioFacturar);
                var Monto = qLinea.Sum(x => x.PrecioTotal);
                //itemPedido.Monto -= descTotal;
                q[0] = itemPedido;
                string json = JsonConvert.SerializeObject(q);
                log.Debug("guarda cotización :" + json);

                if (!ws.GrabarCotizacion(ref mPedido, mTienda, mVendedor, q, qLinea, ref mMensaje, Convert.ToString(Session["Usuario"]), cbTipo.SelectedValue, Convert.ToInt32(cbFinanciera.SelectedValue), cbNivelPrecio.SelectedValue))
                {
                    lbError.Visible = true;
                    lbError.Text = mMensaje;
                    return;
                }

                bool mEnviar = false;
                if (cbInternet.Checked) mEnviar = true;

                if (mEnviar)
                {
                    q = ws.DevuelveCotizacion(mPedido);

                    gridCotizaciones.DataSource = q;
                    gridCotizaciones.DataBind();
                    gridCotizaciones.Visible = true;
                    
                    gridCotizaciones.Focus();
                    gridCotizaciones.SelectedIndex = 0;
                    LinkButton lkSeleccionar = (LinkButton)gridCotizaciones.SelectedRow.FindControl("lkSeleccionarCotizacion");

                    lkSeleccionar.Focus();
                    ImprimirCotizacion(true);

                    string mMailCliente = txtNombreRecibe.Text;

                    limpiar(true);
                    lbInfo.Text = string.Format("La cotización fue enviada a {0}", mMailCliente);
                }
                else
                {
                    limpiar(true);
                    lbInfo.Text = mMensaje;
                }

                cbTipo.SelectedValue = "NR";
                cbFinanciera.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbError.Text = string.Format("Error al grabar la cotización. {0} {1}", ex.Message, m);
                return;
            }
        }

        protected void lkLimpiar_Click(object sender, EventArgs e)
        {
            limpiar(true);
        }
        
        protected void gridClientes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void codigoArticuloDet_TextChanged(object sender, EventArgs e)
        {
            BuscarArticulo("C");
        }

        protected void descripcionArticuloDet_TextChanged(object sender, EventArgs e)
        {
            BuscarArticulo("D");
        }

        void BuscarArticulo(string tipo)
        {
            try
            {
                lbError.Text = "";

               
                if (cbFinanciera.SelectedValue == "Seleccione financiera ...")
                {
                    lbError.Text = "Debe seleccionar la financiera.";
                    cbFinanciera.Focus();
                    return;
                }

                Int32 mFinanciera = 0;
                try
                {
                    mFinanciera = Convert.ToInt32(cbFinanciera.SelectedValue);
                }
                catch
                {
                    lbError.Text = "Debe seleccionar la financiera.";
                    cbFinanciera.Focus();
                    return;
                }
                if (mFinanciera <= 0)
                {
                    lbError.Text = "Debe seleccionar la financiera.";
                    cbFinanciera.Focus();
                    return;
                }

                if (cbNivelPrecio.SelectedValue == "Seleccione ..." || cbNivelPrecio.Text.Trim().Length == 0 || cbNivelPrecio.Text == "Seleccione ...")
                {
                    lbError.Text = "Debe seleccionar el nivel de precio.";
                    cbTipo.Focus();
                    return;
                }

                string mTipo = ""; bool mCriterio = false;
                if (!validarCriterio(ref mTipo, ref mCriterio)) return;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                ws.Timeout = 999999999;
                mTipo = string.Format("${0}", mTipo);
                var q = ws.DevuelveArticulosPreciosListado(false, cbTipo.SelectedValue, Convert.ToInt32(cbFinanciera.SelectedValue), cbNivelPrecio.SelectedValue, mCriterio, mTipo, codigoArticuloDet.Text.Trim(), descripcionArticuloDet.Text.Trim(), Convert.ToString(Session["Tienda"]));

                gridArticulosDet.DataSource = q;
                gridArticulosDet.DataBind();

                lbError2.Text = "";
                lbError2.Visible = false;

                Label6.Text = "";
                Label7.Text = "";

                setToolTips();

                if (gridArticulosDet.Rows.Count > 0)
                {
                    gridArticulosDet.Focus();
                    gridArticulosDet.SelectedIndex = 0;

                    Label7.Text = string.Format("Se encontraron {0} artículos.", gridArticulosDet.Rows.Count);
                    LinkButton lkSeleccionarArticulo = (LinkButton)gridArticulosDet.SelectedRow.FindControl("lkSeleccionarArticulo");

                    lkSeleccionarArticulo.Focus();
                }
                if (gridArticulosDet.Rows.Count == 1)
                {
                    if (gridArticulosDet.Rows[0].Cells[1].Text != "000000-000")
                    {
                        gridArticulosDet.SelectedIndex = 0;
                        seleccionarArticulo();
                    }
                }
                if (gridArticulosDet.Rows.Count == 0)
                {
                    codigoArticuloDet.Text = "";
                    descripcionArticuloDet.Text = "";

                    lbError2.Visible = true;
                    lbError2.Text = "No se encontraron artículos.";
                    Label6.Text = "No se encontraron artículos.";

                    if (tipo == "C")
                    {
                        codigoArticuloDet.Focus();
                    }
                    else
                    {
                        descripcionArticuloDet.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error la buscar el artículo {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }

        void setToolTips()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);
            
            for (int ii = 0; ii < gridArticulosDet.Rows.Count; ii++)
            {
                Label lbOferta = (Label)gridArticulosDet.Rows[ii].FindControl("lbOferta");
                Label lbPrecio = (Label)gridArticulosDet.Rows[ii].FindControl("lbPrecio");
                Label lbPrecioOriginal = (Label)gridArticulosDet.Rows[ii].FindControl("lbPrecioOriginal");
                Label lbFechaVence = (Label)gridArticulosDet.Rows[ii].FindControl("lbFechaVence");
                Label lbDescuentoContado = (Label)gridArticulosDet.Rows[ii].FindControl("lbDescuentoContado");
                Label lbDescuentoCredito = (Label)gridArticulosDet.Rows[ii].FindControl("lbDescuentoCredito");
                Label lbCondicional = (Label)gridArticulosDet.Rows[ii].FindControl("lbCondicional");
                Label lbPrecioOferta = (Label)gridArticulosDet.Rows[ii].FindControl("lbPrecioOferta");
                Label lbCondiciones = (Label)gridArticulosDet.Rows[ii].FindControl("lbCondiciones");
                Label lbPromocion = (Label)gridArticulosDet.Rows[ii].FindControl("lbPromocion");
                Label lbTipoPromocion = (Label)gridArticulosDet.Rows[ii].FindControl("lbTipoPromocion");
                Label lbValorPromocion = (Label)gridArticulosDet.Rows[ii].FindControl("lbValorPromocion");
                Label lbVencePromocion = (Label)gridArticulosDet.Rows[ii].FindControl("lbVencePromocion");
                Label lbFechaVencePromocion = (Label)gridArticulosDet.Rows[ii].FindControl("lbFechaVencePromocion");
                Label lbDescripcionPromocion = (Label)gridArticulosDet.Rows[ii].FindControl("lbDescripcionPromocion");
                Label lbObservacionesPromocion = (Label)gridArticulosDet.Rows[ii].FindControl("lbObservacionesPromocion");
                Label lbPagos = (Label)gridArticulosDet.Rows[ii].FindControl("lbPagos");

                if (lbOferta.Text == "S")
                {
                    if (lbCondicional.Text == "S")
                    {
                        string mCondiciones = ws.DevuelveCondiciones(gridArticulosDet.Rows[ii].Cells[1].Text, Convert.ToString(Session["Tienda"]));
                        lbPrecio.ToolTip = string.Format("Este artículo está en OFERTA!!!{0}{3}{1} y vence el: {2}{0}La oferta aplica en la compra de:{0}{4}", Environment.NewLine, "", Convert.ToDateTime(lbFechaVence.Text).ToShortDateString(), Convert.ToDecimal(lbPrecioOriginal.Text) > 0 ? string.Format("Precio de Oferta: Q {0}", String.Format("{0:0,0.00}", Convert.ToDecimal(lbPrecioOferta.Text))) : string.Format("{0}% desc. al contado y {1}% desc. al crédito.", lbDescuentoContado.Text.Replace(".0000", ""), lbDescuentoCredito.Text.Replace(".0000", "")), mCondiciones);
                    }
                    else
                    {
                        lbPrecio.ToolTip = string.Format("Este artículo está en OFERTA!!!{0}{3}{1}{0}La oferta vence el: {2}", Environment.NewLine, "", Convert.ToDateTime(lbFechaVence.Text).ToShortDateString(), Convert.ToDecimal(lbPrecioOriginal.Text) > 0 ? string.Format("Precio Original: Q {0}", String.Format("{0:0,0.00}", Convert.ToDecimal(lbPrecioOriginal.Text))) : string.Format("{0}% desc. al contado y {1}% desc. al crédito.", lbDescuentoContado.Text.Replace(".0000", ""), lbDescuentoCredito.Text.Replace(".0000", "")));
                    }
                }

                if (lbPromocion.Text == "S")
                    lbPrecio.ToolTip = string.Format("Este artículo está en PROMOCION!!!{0}{1}{0}{2}{0}{3}", Environment.NewLine, lbDescripcionPromocion.Text, lbObservacionesPromocion.Text, lbVencePromocion.Text == "N" ? "" : string.Format("La promoción vence el: {0}", Convert.ToDateTime(lbFechaVencePromocion.Text).ToShortDateString()));

                int mPagos = 0;
                string mPagosString = lbPagos.Text.Replace(" PAGOS", "");

                try
                {
                    mPagos = Convert.ToInt32(mPagosString);
                }
                catch
                {
                    // Nada
                }

                if (mPagos > 0)
                {
                    decimal mPrecio = Convert.ToDecimal(lbPrecio.Text);
                    gridArticulosDet.Rows[ii].Cells[4].ToolTip = string.Format("Cuotas de Q {0}", String.Format("{0:0,0.00}", mPrecio / mPagos));
                }
            }
        }

        bool validarCriterio(ref string tipo, ref bool criterio)
        {
            if (codigoArticuloDet.Text.Trim().Length > 0 || descripcionArticuloDet.Text.Trim().Length > 0) criterio = true;

            if (criterio)
            {
                if (codigoArticuloDet.Text.Trim().Length == 0 && descripcionArticuloDet.Text.Trim().Length == 0)
                {
                    lbError.Text = "Debe ingresar un criterio de búsqueda ya sea el código, la descripción o ambos.";
                    codigoArticuloDet.Focus();
                    return false;
                }

                if (codigoArticuloDet.Text.Trim().Length > 0) tipo = "C";
                if (descripcionArticuloDet.Text.Trim().Length > 0) tipo = "D";
                if (codigoArticuloDet.Text.Trim().Length > 0 && descripcionArticuloDet.Text.Trim().Length > 0) tipo = "A";

                return true;
            }
            else
            {
                return true;
            }
        }

        protected void lbBuscarArticuloDet_Click(object sender, EventArgs e)
        {
            string mTipo = "C";
            if (descripcionArticuloDet.Text.Trim().Length > 0) mTipo = "D";
            BuscarArticulo(mTipo);
        }

        protected void lbCerrarBusquedaDet_Click(object sender, EventArgs e)
        {
            tablaBusquedaDet.Visible = false;
            gridArticulosDet.Visible = false;
        }

        protected void gridArticulos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridArticulos_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lbError.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                switch (ViewState["AccionArticulo"].ToString())
                {
                    case "Eliminar":
                        bool blArticulosValidos = true;
                        List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();
                        double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

                        txtValor.Text = "0.00";
                        txtFacturar.Text = "0.00";
                        try
                        {
                            List<NuevoPrecioProducto> lstprod = (List<NuevoPrecioProducto>)Session["lstNuevoPrecioProducto"];
                            lstprod.Remove(lstprod.Find(x => x.Index == gridArticulos.SelectedIndex));
                            var Saldo = Facturacion.ObtenerSaldoDescuento(lstprod);
                            if (Saldo>=0)
                            {
                                //quitando el producto recientemente agregado
                                Session["lstNuevoPrecioProducto"] = lstprod;
                                LimpiarListaArticulos();
                                
                            }
                            else
                            {
                               
                                LimpiarListaArticulos();
                                limpiar(true);
                                lbError2.Text = "Los productos restantes no cumplen con la condición de precios para facturarse.";
                                lbError2.Visible = true;
                                blArticulosValidos = false;
                            }

                        }
                        catch
                        { }
                        if (blArticulosValidos)
                        {
                            foreach (GridViewRow gvr in gridArticulos.Rows)
                            {
                                Label lbCondicional = (Label)gvr.FindControl("lbCondicional");
                                Label lbArticuloCondicion = (Label)gvr.FindControl("lbArticuloCondicion");
                                Label lbFechaOfertaDesde = (Label)gvr.FindControl("lbFechaOfertaDesde");
                                Label lbArticulo = (Label)gvr.FindControl("lbArticulo");
                                Label lbNombreArticulo = (Label)gvr.FindControl("lbNombreArticulo");
                                Label lbPrecioOriginal = (Label)gvr.FindControl("lbPrecioOriginal");

                                Label lbArticuloSeleccionado = (Label)gridArticulos.SelectedRow.FindControl("lbArticulo");
                                string mArticulo = lbArticuloSeleccionado.Text;

                                if (lbArticulo.Text != mArticulo && lbArticuloCondicion.Text != mArticulo)
                                {
                                    decimal mPrecioOriginal = 0;
                                    DateTime mFechaOfertaDesde = new DateTime(1980, 1, 1);

                                    try
                                    {
                                        mFechaOfertaDesde = new DateTime(Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(6, 4)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(3, 2)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(0, 2)));
                                    }
                                    catch
                                    {
                                        //Nothing
                                    }

                                    try
                                    {
                                        mPrecioOriginal = Convert.ToDecimal(lbPrecioOriginal.Text.Replace(",", ""));
                                    }
                                    catch
                                    {
                                        //Nothing
                                    }

                                    Label lbPrecio = (Label)gvr.FindControl("lbPrecio");
                                    Label lbPrecioFacturar = (Label)gvr.FindControl("lbPrecioFacturar");
                                    Label lbCantidad = (Label)gvr.FindControl("lbCantidad");
                                    Label lbTotal = (Label)gvr.FindControl("lbTotal");
                                    Label lbBodega = (Label)gvr.FindControl("lbBodega");
                                    Label lbLocalizacion = (Label)gvr.FindControl("lbLocalizacion");
                                    LinkButton lkRequisicionArmado = (LinkButton)gvr.FindControl("lkRequisicionArmado");
                                    Label lbDescripcion = (Label)gvr.FindControl("lbDescripcion");
                                    Label lbComentario = (Label)gvr.FindControl("lbComentario");
                                    Label lbEstado = (Label)gvr.FindControl("lbEstado");
                                    Label lbNumeroPedido = (Label)gvr.FindControl("lbNumeroPedido");
                                    Label lbOferta = (Label)gvr.FindControl("lbOferta");
                                    Label lbTipoOferta = (Label)gvr.FindControl("lbTipoOferta");
                                    Label lbEsDetalleKit = (Label)gvr.FindControl("lbEsDetalleKit");


                                    wsPuntoVenta.PedidoLinea item = new wsPuntoVenta.PedidoLinea();
                                    item.Articulo = lbArticulo.Text;
                                    item.Nombre = lbNombreArticulo.Text;
                                    item.PrecioUnitario = Convert.ToDecimal(lbPrecio.Text.Replace(",", ""));
                                    item.CantidadPedida = Convert.ToDecimal(lbCantidad.Text.Replace(",", ""));
                                    item.PrecioTotal = Convert.ToDecimal(lbTotal.Text.Replace(",", ""));
                                    item.PrecioFacturar = Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", ""));
                                    item.Bodega = lbBodega.Text;
                                    item.Localizacion = lbLocalizacion.Text;
                                    item.RequisicionArmado = lkRequisicionArmado.Text;
                                    item.Descripcion = lbDescripcion.Text;
                                    item.Comentario = lbComentario.Text;
                                    item.Estado = lbEstado.Text;
                                    item.NumeroPedido = lbNumeroPedido.Text;
                                    item.Oferta = lbOferta.Text;
                                    item.TipoOferta = lbTipoOferta.Text;
                                    item.FechaOfertaDesde = mFechaOfertaDesde;
                                    item.PrecioOriginal = mPrecioOriginal;
                                    item.Condicional = lbCondicional.Text;
                                    item.ArticuloCondicion = lbArticuloCondicion.Text;
                                    item.EsDetalleKit = lbEsDetalleKit.Text;
                                    item.PrecioSugerido = Convert.ToDecimal(((Label)gvr.FindControl("lbPrecioSugerido")).Text.Replace(",",""));
                                    item.NetoFacturar = Math.Round(((item.PrecioTotal)), 2, MidpointRounding.AwayFromZero);
                                    q.Add(item);

                                    txtFacturar.Text = String.Format("{0:0,0.00}", (Convert.ToDecimal(txtFacturar.Text.Replace(",", "")) + Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", ""))));

                                    //if (lbOferta.Text == "S")
                                    //{
                                    //    txtFacturar.Text = String.Format("{0:0,0.00}", (Convert.ToDecimal(txtFacturar.Text.Replace(",", "")) + Convert.ToDecimal(lbTotal.Text.Replace(",", ""))));
                                    //}
                                    //else
                                    //{
                                    //    txtFacturar.Text = String.Format("{0:0,0.00}", (Convert.ToDouble(txtFacturar.Text.Replace(",", "")) + (Convert.ToDouble(lbTotal.Text.Replace(",", "")) / mFactor)));
                                    //}
                                }
                            }

                            if (q.Count() == 0)
                            {
                                txtEnganche.Text = "0.00";
                                txtDescuentos.Text = "0.00";
                                tablaBusquedaDet.Visible = false;
                                gridArticulosDet.Visible = false;
                            }

                            ViewState["qArticulos"] = q;
                            ValidaTipoNivelPrecio();

                            gridArticulos.DataSource = q;
                            gridArticulos.DataBind();
                            gridArticulos.Visible = true;

                            articulo.Text = "";
                            descripcion.Text = "";
                            cbRequisicion.SelectedValue = "No";
                            cbLocalizacion.SelectedValue = "ARMADO";
                            txtCantidad.Text = "1";
                            txtPrecioUnitario.Text = "0.00";
                            txtTotal.Text = "0.00";
                            txtPrecioUnitarioDebioFacturar.Text = "0.00";

                            articulo.Focus();
                        }
                        break;
                    case "Gel":
                        Label lbLinea = (Label)gridArticulos.SelectedRow.FindControl("lbLinea");
                        Label lbNombre = (Label)gridArticulos.SelectedRow.FindControl("lbNombreArticulo");
                        Label lbDesc = (Label)gridArticulos.SelectedRow.FindControl("lbDescripcion");

                        Clases.Info mInfo = new Clases.Info();
                        mInfo.Documento = txtPedido.Text;

                        string json = JsonConvert.SerializeObject(mInfo);
                        byte[] data = Encoding.ASCII.GetBytes(json);

                        WebRequest request = WebRequest.Create(string.Format("{0}/gel/{1}", Convert.ToString(Session["UrlRestServices"]), lbLinea.Text));

                        request.Method = "PUT";
                        request.ContentType = "application/json";
                        request.ContentLength = data.Length;

                        using (var stream = request.GetRequestStream())
                        {
                            stream.Write(data, 0, data.Length);
                        }

                        var response = (HttpWebResponse)request.GetResponse();
                        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                        Clases.RetornaExito mRetorna = new Clases.RetornaExito();
                        mRetorna = JsonConvert.DeserializeObject<Clases.RetornaExito>(responseString);

                        string mGel = " - GARANTÍA ESPECIAL LIMITADA";
                        if (!mRetorna.exito)
                        {
                            if (mRetorna.mensaje == "No se encontró la línea de la cotización, debe grabarla antes." || mRetorna.mensaje == "Cotización inválida.")
                            {
                                Label lbGel = (Label)gridArticulos.SelectedRow.FindControl("lbGel");

                                if (lbGel.Text == "No")
                                {
                                    lbGel.Text = "Sí";
                                    lbNombre.Text = string.Format("{0}{1}", lbNombre.Text, mGel);
                                }
                                else
                                {
                                    lbGel.Text = "No";
                                    lbNombre.Text = lbNombre.Text.Replace(mGel, "");
                                }
                            }
                            else
                            {
                                lbError.Text = mRetorna.mensaje;
                                return;
                            }

                            mRetorna.mensaje = lbNombre.Text;
                        }

                        List<wsPuntoVenta.PedidoLinea> qGel = new List<wsPuntoVenta.PedidoLinea>();
                        qGel = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                        foreach (var item in qGel)
                        {
                            if (item.Linea == Convert.ToInt32(lbLinea.Text))
                            {
                                item.Nombre = mRetorna.mensaje;
                                item.Descripcion = mRetorna.mensaje;

                                item.Gel = "No";
                                if (mRetorna.mensaje.Contains(mGel)) item.Gel = "Sí";
                            }
                        }

                        ViewState["qArticulos"] = qGel;
                        lbNombre.Text = mRetorna.mensaje;
                        lbDesc.Text = mRetorna.mensaje;

                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                lbError.Text = CatchClass.ExMessage(ex, "cotizaciones", "gridArticulos_SelectedIndexChanged");
            }
        }

        protected void lbBuscarArticulo_Click(object sender, EventArgs e)
        {
            MostrarConsultaCotizaciones(true);
        }

        void habilitarBusquedaArticulos()
        {
            tablaBusquedaDet.Visible = true;
            gridArticulosDet.Visible = true;

            List<wsCambioPrecios.Articulos> q = new List<wsCambioPrecios.Articulos>();

            gridArticulosDet.DataSource = q;
            gridArticulosDet.DataBind();

            lbError2.Text = "";
            lbError2.Visible = false;

            Label6.Text = "";
            Label7.Text = "";
            codigoArticuloDet.Text = "";
            descripcionArticuloDet.Text = "";
            descripcionArticuloDet.Focus();
        }

        void ValidaBotonPromocion()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveTipoVenta(cbTipo.SelectedValue);
            if (q[0].Valor > 0 || q[0].DescuentoNormal > 0 || q[0].DescuentoOfertado > 0)
            {
                bool mMostrar = false;

                if (q[0].Financiera == 0 || q[0].Financiera.ToString() == cbFinanciera.SelectedValue) mMostrar = true;

                if (mMostrar)
                {
                    lkLaTorre.Text = q[0].TextoPromocion;
                    lkLaTorre.ToolTip = string.Format("Haga clic aquí para aplicar la promoción de {0}", q[0].TextoPromocion);
                    lkLaTorre.Visible = true;
                }
                else
                {
                    lkLaTorre.Visible = false;
                }
            }
            else
            {
                lkLaTorre.Visible = false;
            }
        }

        protected void cbNivelPrecio_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                limpiar(false);
                ValidaBotonPromocion();
            }
            catch (Exception ex)
            {
                string m = "";

                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    //Nothing
                }

                lbError2.Visible = true;
                lbError2.Text = string.Format("Error al cambiar el nivel. {0} {1}", ex.Message, m);
            }
        }

        protected void gridArticulosDet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }

            try
            {
                Label lbOferta = (Label)e.Row.FindControl("lbOferta");
                Label lbPromocion = (Label)e.Row.FindControl("lbPromocion");

                //if (lbOferta.Text == "S")
                //{
                //    e.Row.ForeColor = System.Drawing.Color.FromArgb(227, 89, 4);
                //    e.Row.Font.Bold = true;
                //}

                if (lbPromocion.Text == "S")
                {
                    e.Row.ForeColor = System.Drawing.Color.FromArgb(165, 81, 41);
                    e.Row.Font.Bold = true;
                }
            }
            catch
            {
                //Nothing
            }
        }

        void seleccionarArticulo()
        {
            try
            {
                List<NuevoPrecioProducto> lstProductos = new List<NuevoPrecioProducto>();
                lstProductos = ((List<NuevoPrecioProducto>)Session["lstNuevoPrecioProducto"]);
                if (lstProductos == null)
                    lstProductos = new List<NuevoPrecioProducto>();

                

                txtVale.Text = "0";
                lbError2.Text = "";
                lbError2.Visible = false;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                Label lbPrecio = (Label)gridArticulosDet.SelectedRow.FindControl("lbPrecio");
                Label lbOferta = (Label)gridArticulosDet.SelectedRow.FindControl("lbOferta");
                Label lbFechaDesde = (Label)gridArticulosDet.SelectedRow.FindControl("lbFechaDesde");
                Label lbPrecioOriginal = (Label)gridArticulosDet.SelectedRow.FindControl("lbPrecioOriginal");
                Label lbTipoOferta = (Label)gridArticulosDet.SelectedRow.FindControl("lbTipoOferta");
                Label lbCondicional = (Label)gridArticulosDet.SelectedRow.FindControl("lbCondicional");
                Label lbPrecioOferta = (Label)gridArticulosDet.SelectedRow.FindControl("lbPrecioOferta");

                string mPrecio = lbPrecio.Text;
                string mArticulo = gridArticulosDet.SelectedRow.Cells[1].Text;
                string mCondiciones = ws.DevuelveCondiciones(gridArticulosDet.SelectedRow.Cells[1].Text, Convert.ToString(Session["Tienda"]));
                string mDescripcion = gridArticulosDet.SelectedRow.Cells[2].Text.Replace("&#39;", "'").Replace("&#225;", "á").Replace("&#233;", "é").Replace("&#237;", "í").Replace("&#243;", "ó").Replace("&#250;", "ú").Replace("&quot;", "\"").Replace("&#209;", "Ñ").Replace("&#241;", "Ñ").Replace("&#252;", "ü").Replace("&#193;", "Á").Replace("&#201;", "É").Replace("&#205;", "Í").Replace("&#211;", "Ó").Replace("&#218;", "Ú").Replace("&#220;", "Ü").Replace("&#191;", "¿").Replace("&#161;", "¡");
                if (decimal.Parse(mPrecio.Replace(",", "")) > 0)
                {
                    if (lbCondicional.Text == "S")
                    {
                        bool mCumpleCondicion = false;
                        string mArticuloCondicion = "";

                        for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                        {
                            if (mCondiciones.Contains(gridArticulos.Rows[ii].Cells[1].Text))
                            {
                                mCumpleCondicion = true;
                                mArticuloCondicion = gridArticulos.Rows[ii].Cells[1].Text;
                            }
                        }

                        lbError2.Visible = true;

                        if (mCumpleCondicion)
                        {
                            mPrecio = lbPrecioOferta.Text;

                            txtCondicional.Text = "S";
                            txtArticuloCondicion.Text = mArticuloCondicion;
                            lbError2.Text = string.Format("Este artículo aplica el precio de oferta Q{0} ya que está comprando el artículo {1}.", String.Format("{0:0,0.00}", Convert.ToDecimal(lbPrecioOferta.Text)), mArticuloCondicion);
                        }
                        else
                        {
                            lbError2.Text = string.Format("Este artículo aplicaría el precio de oferta Q{0} si el cliente adquiere alguno de los siguientes articulos:{1}{2}.", String.Format("{0:0,0.00}", Convert.ToDecimal(lbPrecioOferta.Text)), System.Environment.NewLine, mCondiciones);
                        }

                    }

                    int Pagos = 1;
                    List<string> numbers = Regex.Split(cbNivelPrecio.SelectedValue, @"\D+").ToList();
                    numbers.ForEach(x =>
                    {
                        if (!string.IsNullOrEmpty(x))
                        {
                            Pagos = int.Parse(x);
                        }
                    });
                    if (WebConfigurationManager.AppSettings["PrecioOriginal"].ToString().Equals("1"))
                    {

                        log.Info("articulo" + mArticulo + "Precio " + mPrecio);

                        Respuesta result = Facturacion.ObtenerDatosFacturacion(Convert.ToString(Session["Tienda"]), mArticulo, Pagos, cbFinanciera.SelectedValue, decimal.Parse(mPrecio.Replace(",", "")));
                        if (!result.Exito)
                        {
                            lbError2.Text = result.Mensaje;
                            lbError2.Visible = true;
                            txtDescMax.Text = "0.00";
                        }
                        else
                        {
                            //Se obtiene el factor para la formula del calculo de la cuota
                            NuevoPrecioProducto datosPrecio = ((NuevoPrecioProducto)result.Objeto);
                            mPrecio = String.Format("{0:0,0.00}", (datosPrecio.originalPrice)); //+ (cbTipoPedido.SelectedValue == "N" ? (Convert.ToDecimal(txtTotal.Text.Replace(",", "")) / datosPrecio.factor) : Convert.ToDecimal(txtTotal.Text.Replace(",", "")));
                            txtDescMax.Text = String.Format("{0:0,0.00}", datosPrecio.originalPrice - datosPrecio.originalDiscountPrice);
                            log.Info("Precio " + datosPrecio.originalPrice);
                            if (lstProductos.Count == gridArticulos.Rows.Count)
                            {
                                
                                lstProductos.Add(datosPrecio);
                                Session["lstNuevoPrecioProducto"] = lstProductos;
                            }
                            txtSaldoDescuento.Text = String.Format("{0:0,0.00}", Facturacion.ObtenerSaldoDescuento(lstProductos));

                        }


                    }



                    articulo.Text = mArticulo;
                    descripcion.Text = mDescripcion;
                    txtPrecioUnitario.Text = String.Format("{0:0,0.00}", Convert.ToDecimal(mPrecio.Replace(",", "")));
                    txtPrecioUnitarioDebioFacturar.Text = String.Format("{0:0,0.00}", Convert.ToDecimal(mPrecio.Replace(",", "")));
                    txtTotal.Text = String.Format("{0:0,0.00}", Convert.ToDecimal(mPrecio.Replace(",", "")) * Convert.ToInt32(txtCantidad.Text.Replace(",", "")));
                    txtOferta.Text = lbOferta.Text;
                    txtFechaOfertaDesde.Text = lbFechaDesde.Text;
                    txtPrecioOriginal.Text = lbPrecioOriginal.Text;
                    txtTipoOferta.Text = lbTipoOferta.Text;

                    decimal mNuevoPrecio = 0;
                    decimal mNuevoPrecioFacturar = 0;
                    decimal mTotal = 0;
                    string mMensaje = "";
                    if (WebConfigurationManager.AppSettings["PrecioOriginal"].ToString().Equals("0") && ws.TienePromocion(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue, mArticulo, txtCantidad.Text.Replace(",", ""), mPrecio.Replace(",", ""), ref mNuevoPrecio, ref mNuevoPrecioFacturar, ref mTotal, ref mMensaje))
                    {
                        txtPrecioUnitario.Text = String.Format("{0:0,0.00}", mNuevoPrecio);
                        txtPrecioUnitarioDebioFacturar.Text = String.Format("{0:0,0.00}", mNuevoPrecioFacturar);
                        txtTotal.Text = String.Format("{0:0,0.00}", mTotal);

                    }

                    tablaBusquedaDet.Visible = false;
                    gridArticulosDet.Visible = false;
                    gridLocalizaciones.Visible = false;
                    tblSolicutudAutorizacion.Visible = false;

                    cbRequisicion.SelectedValue = ws.RequiereArmado(mArticulo);
                    if (Convert.ToString(Session["Tienda"]) == "F01")
                    {
                        if (mArticulo.Substring(0, 1) == "S")
                        {
                            descripcion.Text = "";
                            descripcion.ReadOnly = false;
                            descripcion.Focus();
                            descripcion.TextMode = TextBoxMode.MultiLine;
                            descripcion.Height = new Unit(121);
                        }
                        else
                        {
                            txtPrecioUnitario.Focus();
                            descripcion.Height = new Unit(22);
                            descripcion.TextMode = TextBoxMode.SingleLine;
                        }
                    }
                    else
                    {
                        lbAgregarArticulo.Focus();
                    }
                }
                else
                {
                    lbError2.Visible = true;
                    lbError2.Text = "No se aceptan precios con valor 0.00";
                }


            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                    descripcion.Height = new Unit(22);
                    descripcion.TextMode = TextBoxMode.SingleLine;
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format("Error al agregar el artículo {0} {1}", ex.Message, m);
            }

        }

        protected void gridArticulosDet_SelectedIndexChanged(object sender, EventArgs e)
        {
            seleccionarArticulo();
        }

        void ValidaTotalPedido()
        {
            double mFactor = 1;
            if (cbTipoPedido.SelectedValue == "N")
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                //Se obtiene el factor para la formula del calculo de la cuota
                mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

                //Se obtiene el tipo de pago, Credito, Contado, etc.
                string mTipo = ws.DevuelveTipoNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

                //Si el tipo de pago es de contado, el factor siempre es 1
                if (mTipo == "CO") mFactor = 1;


            }

            var mDescuento = double.Parse(txtDescuentoItem.Text.Replace(",", ""));

            txtFacturar.Text = String.Format("{0:0,0.00}", (Convert.ToDouble(txtFacturar.Text.Replace(",", "")) + (Convert.ToDouble(txtTotal.Text.Replace(",", "")) / mFactor)));
            txtFacturarItem.Text = String.Format("{0:0,0.00}", Convert.ToDouble(txtTotal.Text.Replace(",", "")) / mFactor);
        }

        void ValidaTipoNivelPrecio()
        {
            decimal mEnganche = 0;
            decimal mDescuento = 0;
            decimal mDescuentoItem = 0;
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU")
                ws.Url = Convert.ToString(ViewState["url"]);

            // Se obtiene el tipo de pago, Credito, Contado, etc.
            string mTipo = ws.DevuelveTipoNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

            //Se obtiene el factor para la formula del calculo de la cuota
            double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
            try
            {
                var PrecioUnitarioItem = Convert.ToDecimal(txtPrecioUnitario.Text.Replace(",", ""));
                mDescuentoItem = (Convert.ToDecimal(txtDescuentoItem.Text.Replace(",", "")) > (PrecioUnitarioItem / (decimal)mFactor) ? PrecioUnitarioItem / (decimal)mFactor : Convert.ToDecimal(txtDescuentoItem.Text.Replace(",", "")));
                //Convert.ToDecimal(txtDescuentoItem.Text.Replace(",", ""));
                //decimal DescuentoAplicado = 
            }
            catch
            {
                mDescuentoItem = 0;
                txtDescuentoItem.Text = "0.00";
            }
            try
            {
                mDescuento = Convert.ToDecimal(txtDescuentos.Text.Replace(",", ""));
                
            }
            catch
            {
                mDescuento = 0;
                txtDescuentos.Text = "0.00";
            }
            try
            {
                mEnganche = Convert.ToDecimal(txtEnganche.Text.Replace(",", ""));
            }
            catch
            {
                mEnganche = 0;
                txtEnganche.Text = "0.00";
            }

            


            txtIntereses.Text = "0.00";
            txtSaldoFinanciar.Text = "0.00";
            txtMonto.Text = String.Format("{0:0,0.00}", decimal.Parse(txtFacturar.Text.Replace(",", "")) - mDescuento);
            mDescuento += mDescuentoItem;
            txtDescuentos.Text = String.Format("{0:0,0.00}", mDescuento);
            //Si el tipo de pago es Credito realiza las siguientes validaciones
            if (mTipo == "CR")
            {


                decimal mMonto = 0; int ii = 0;
                List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();
                q = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                foreach (var item in q)
                {
                    ii += 1;

                    mMonto += item.PrecioTotal;
                }

                decimal mFacturar = 0, mFacturarCompleto = 0;
                decimal mSaldoFinanciar = 0; decimal mSaldoFinanciarCompleto = 0;
                decimal mIntereses = 0;

                if (ii == 0)
                {

                    //El Saldo a Financiar es el total de la factura, menos el enganche
                    mSaldoFinanciar = Convert.ToDecimal(txtFacturar.Text.Replace(",", "")) - Convert.ToDecimal(txtEnganche.Text.Replace(",", "")) - mDescuento;

                    //El monto a financiar es igual al saldo a financiar por el factor
                    mMonto = mSaldoFinanciar * (decimal)mFactor;

                    //Los intereses se calculas respecto al monto a financiar - saldo a financiar
                    mIntereses = mMonto - mSaldoFinanciar;

                    txtSaldoFinanciar.Text = String.Format("{0:0,0.00}", mSaldoFinanciar);
                    txtMonto.Text = String.Format("{0:0,0.00}", mMonto);
                    txtIntereses.Text = String.Format("{0:0,0.00}", mIntereses);
                }
                else
                {
                    #region "cálculo de precio del bien"
                    var lstNivelPrecioValidacion = new List<string>();
                    if (Session["NivelPrecioValidacion"] == null)
                    {
                        lstNivelPrecioValidacion = WebRestClient.ObtenerNivelPrecioFacturacion(Session["UrlRestServices"].ToString());
                        Session["NivelPrecioValidacion"] = lstNivelPrecioValidacion;
                    }
                    else
                    {
                        lstNivelPrecioValidacion = ((List<string>)Session["NivelPrecioValidacion"]);
                    }

                    if (lstNivelPrecioValidacion != null && lstNivelPrecioValidacion.Contains(cbNivelPrecio.SelectedValue))//forma de cálculo restándo el enganche
                    {
                        mFacturarCompleto = ((mMonto - mEnganche) / (decimal)mFactor) + mEnganche;
                        mFacturar = Math.Round(Convert.ToDecimal((double)(mMonto - mEnganche) / mFactor) + mEnganche, 2, MidpointRounding.AwayFromZero);
                    }
                    else
                    {
                        //mFacturarCompleto = monto total divido el factor, Monto total es la sumatoria del precio total de cada articulo
                        mFacturarCompleto = mMonto / (decimal)mFactor + mDescuento;

                        //mFacturar= lo mismo que mFacturarCompleto a dos decimales
                        mFacturar = Math.Round(Convert.ToDecimal(((double)mMonto / mFactor )+ (double)mDescuento), 2, MidpointRounding.AwayFromZero);
                    }
                    #endregion
                    //Saldo a financiar = a facturar - enganche
                    mSaldoFinanciar = mFacturar - mEnganche - mDescuento;


                    mSaldoFinanciarCompleto = mFacturarCompleto - mEnganche - mDescuento;

                    mMonto = Math.Round(Convert.ToDecimal((double)mSaldoFinanciarCompleto * mFactor), 2, MidpointRounding.AwayFromZero);
                    mIntereses = mMonto - mSaldoFinanciar;

                    txtMonto.Text = String.Format("{0:0,0.00}", mMonto);
                    txtFacturar.Text = String.Format("{0:0,0.00}", mFacturar);
                    txtSaldoFinanciar.Text = String.Format("{0:0,0.00}", mSaldoFinanciar);
                    txtIntereses.Text = String.Format("{0:0,0.00}", mIntereses);
                }
                txtDescuentos.Text = String.Format("{0:0,0.00}", mDescuento);

                Int32 mPagos; string mPagos1;
                string mPagosTexto = ws.DevuelvePagosNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

                try
                {
                    mPagos = Convert.ToInt32(mPagosTexto.Substring(0, 2).Replace(" ", ""));
                }
                catch
                {
                    mPagos = 1;
                }

                decimal mCuotas = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) / mPagos;



                if (mPagos <= 10)
                {
                    mPagos1 = string.Format("0{0}", mPagos - 1);
                }
                else
                {
                    mPagos1 = (mPagos - 1).ToString();
                }

                cbPagos1.SelectedValue = mPagos1;
                cbPagos2.SelectedValue = "01";

                if (mCuotas.ToString().Substring(mCuotas.ToString().Length - 2, 2) == "00")
                {
                    txtPagos1.Text = String.Format("{0:0,0.00}", mCuotas);
                    txtPagos2.Text = String.Format("{0:0,0.00}", mCuotas);
                }
                else
                {
                    if (cbFinanciera.SelectedValue == Const.FINANCIERA.BANCREDIT)
                    {
                        txtPagos1.Text = String.Format("{0:0,0.00}", Math.Round(mCuotas, 0, MidpointRounding.AwayFromZero));
                        decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (Math.Round(mCuotas, 0, MidpointRounding.AwayFromZero) * (mPagos - 1));
                        txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
                    }
                    else
                    {
                        string[] mCuotasString = mCuotas.ToString().Split(new string[] { "." }, StringSplitOptions.None);

                        Int32 mCuota = 1 + Convert.ToInt32(mCuotasString[0]);
                        decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (mCuota * (mPagos - 1));

                        txtPagos1.Text = String.Format("{0:0,0.00}", mCuota);
                        txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
                    }
                }
            }
            else
            {

                txtMonto.Text = String.Format("{0:0,0.00}", decimal.Parse(txtFacturar.Text.Replace(",", "")) - Convert.ToDecimal(txtDescuentos.Text.Replace(",", "")));
            }
        }

        void agregarArticulo()
        {
            try
            {
                //OriginalDiscountPrice
                decimal mPrecioSugerido = 0;
                lbError2.Text = "";
                lbError2.Visible = false;
                var ws = new wsPuntoVenta.wsPuntoVenta();
                double mFactor = 0;
                double mFactorFinanciera = 0;
                string mTipo = "";
                bool blDescuentosValidos = true;

                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU")
                    ws.Url = Convert.ToString(ViewState["url"]);

                if (articulo.Text.Trim().Length == 0)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('Debe seleccionar un artículo.');", true);
                    articulo.Focus();
                    return;
                }
                if (txtTotal.Text == "0.00")
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('Debe seleccionar un artículo.');", true);
                    articulo.Focus();
                    return;
                }
                try
                {
                    txtSaldoDescuento.Text = String.Format("{0:0,0.00}", Facturacion.ObtenerSaldoDescuento((List<NuevoPrecioProducto>)Session["lstNuevoPrecioProducto"]));
                }
                catch
                { }

                try
                {
                    mFactorFinanciera = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                    mTipo = ws.DevuelveTipoNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                    mFactor = mFactorFinanciera;
                }
                catch
                {
                    lbError2.Visible = true;
                    lbError2.Text = "Debe seleccionar el nivel de precio";
                    cbNivelPrecio.Focus();
                    return;
                }


                if (WebConfigurationManager.AppSettings["PrecioOriginal"].ToString().Equals("1"))
                {
                    try
                    {
                        decimal descMax = decimal.Parse(txtSaldoDescuento.Text.Replace(",", ""));
                        decimal descuento = decimal.Parse(txtDescuentoItem.Text.Replace(",", ""));

                        if (Convert.ToDecimal(txtPrecioUnitario.Text.Replace(",", "")) <= 1 && cbTipoPedido.SelectedValue != "N")
                        {
                            lbError2.Visible = true;
                            lbError2.Text = "El precio es inválido, por favor revisar";
                            return;
                        }

                        //el monto del descuento no debe ser mayor a descMax
                        //se le agrega el monto del descuento, ya que en este momento descMax ya se le ha restado el descuento
                        if (descuento > (descMax + descuento))
                        {
                            lbError2.Visible = true;
                            lbError2.Text = "El valor del descuento no puede ser mayor a Q" + txtDescMax.Text;
                            txtDescuentoItem.Focus();
                            return;
                        }
                        if (descuento < 0)
                        {
                            lbError2.Visible = true;
                            lbError2.Text = "El valor del descuento no puede ser menor a cero.";
                            txtDescuentoItem.Focus();
                            return;
                        }

                       
                        else
                        {
                            List<NuevoPrecioProducto> lstProd = ((List<NuevoPrecioProducto>)Session["lstNuevoPrecioProducto"]);
                            //obtenemos el último ingresado
                            NuevoPrecioProducto nuevoProducto = lstProd[lstProd.Count - 1];

                            List<NuevoPrecioProducto> lstProdAux = new List<NuevoPrecioProducto>();
                            for (int i = 0; i < (lstProd.Count - 1); i++)
                            {
                                lstProdAux.Add(lstProd[i]);
                            }
                            if (!Facturacion.ValidarArticuloRegaloAccesorio(lstProdAux, nuevoProducto, (decimal)mFactor))
                            {
                                blDescuentosValidos = false;
                                //quitando el producto recientemente agregado
                                Session["lstNuevoPrecioProducto"] = lstProdAux;
                                LimpiarListaArticulos();
                                lbError2.Text = "No se puede agregar el artículo porque no cumple con las condiciones de saldos de descuentos o precio normal, necesarios";
                                lbError2.Visible = true;
                            }
                            else
                            {
                                mPrecioSugerido = nuevoProducto.originalDiscountPrice;
                                lstProd[lstProd.Count - 1].Index = gridArticulos.Rows.Count;
                                mFactor = (double)nuevoProducto.factor;
                                LimpiarListaArticulos();
                            }
                        }


                    }
                    catch
                    { }
                }
                if (blDescuentosValidos)
                {
                    string mBeneficiario = "";
                    if (articulo.Text == "S00013-000" || articulo.Text == "S00012-000" || articulo.Text == "S00011-000" || articulo.Text == "S00010-000")
                    {
                        if (txtNotasTipoVenta.Text.Trim().Length == 0)
                        {
                            lbError2.Visible = true;
                            lbError2.Text = "Debe ingresar el nombre del beneficiario del certificado en la casilla Obs. Promoción.";
                            txtNotasTipoVenta.Focus();
                            return;
                        }

                        mBeneficiario = txtNotasTipoVenta.Text.Trim().ToUpper();
                        txtNotasTipoVenta.Text = "";
                    }

                    List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();
                    q = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                    int mLinea = q.Count();

                    DateTime mFechaOfertaDesde = new DateTime(1980, 1, 1);

                    try
                    {
                        mFechaOfertaDesde = new DateTime(Convert.ToInt32(txtFechaOfertaDesde.Text.Substring(6, 4)), Convert.ToInt32(txtFechaOfertaDesde.Text.Substring(3, 2)), Convert.ToInt32(txtFechaOfertaDesde.Text.Substring(0, 2)));
                    }
                    catch
                    {
                        //Nothing
                    }



                    if (mTipo == "CO") mFactor = 1;
                    double mPrecioBaseLocal = ws.DevuelvePrecioBase(articulo.Text, Convert.ToString(Session["Tienda"]));
                    decimal descuento = decimal.Parse(txtDescuentoItem.Text.Replace(",", ""));
                    string mGel = "";
                    if (cbGel.SelectedItem.Text == "Sí") mGel = " - GARANTÍA ESPECIAL LIMITADA";


                    
                    wsPuntoVenta.PedidoLinea item = new wsPuntoVenta.PedidoLinea();
                    item.PrecioUnitario = Convert.ToDecimal(txtPrecioUnitario.Text.Replace(",", ""));

                    decimal DescuentoAplicado = (Convert.ToDecimal(txtDescuentoItem.Text.Replace(",", "")) > (item.PrecioUnitario / (decimal)mFactor) ? item.PrecioUnitario / (decimal)mFactor : Convert.ToDecimal(txtDescuentoItem.Text.Replace(",", "")));

                    item.Articulo = articulo.Text;
                    item.Nombre = string.Format("{0}{1}", descripcion.Text.ToUpper(), mGel);
                   
                    item.CantidadPedida = Convert.ToDecimal(txtCantidad.Text.Replace(",", ""));
                    item.PrecioTotal = ((Convert.ToDecimal(txtTotal.Text.Replace(",", "")) / (decimal)mFactor) - DescuentoAplicado) * (decimal)mFactor;
                    item.Bodega = cbBodega.SelectedValue;
                    item.Localizacion = cbLocalizacion.SelectedItem.Text;
                    item.RequisicionArmado = cbRequisicion.SelectedItem.Text;
                    item.Descripcion = "";
                    item.Comentario = "";
                    item.Estado = "N";
                    item.NumeroPedido = "";
                    item.Oferta = txtOferta.Text;
                    item.FechaOfertaDesde = mFechaOfertaDesde;
                    item.PrecioOriginal = Convert.ToDecimal(txtPrecioOriginal.Text.Replace(",", ""));
                    item.TipoOferta = txtTipoOferta.Text;
                    item.EsDetalleKit = "N";
                    item.PrecioFacturar = Convert.ToDecimal(txtTotal.Text.Replace(",", ""))/ (decimal)mFactorFinanciera;
                    item.PrecioUnitarioDebioFacturar = (Convert.ToDecimal(txtPrecioUnitarioDebioFacturar.Text.Replace(",", "")) / (decimal)mFactor) / item.CantidadPedida;
                    item.PrecioSugerido = mPrecioSugerido;
                    item.Vale = Convert.ToInt32(txtVale.Text);
                    item.Condicional = txtCondicional.Text;
                    item.ArticuloCondicion = txtArticuloCondicion.Text;
                    item.Autorizacion = Convert.ToInt64(txtAutorizacion.Text);
                    item.PrecioBaseLocal = mPrecioBaseLocal;
                    item.Linea = mLinea;
                    item.Gel = cbGel.SelectedItem.Text;
                    item.PorcentajeDescuento = DescuentoAplicado;
                    item.Descuento = DescuentoAplicado;
                    item.Beneficiario = mBeneficiario;
                    item.NetoFacturar = Math.Round(((item.PrecioTotal)), 2 ,MidpointRounding.AwayFromZero);
                    q.Add(item);

                    gridArticulos.DataSource = q;
                    gridArticulos.DataBind();
                    gridArticulos.Visible = true;

                    ViewState["qArticulos"] = q;


                    //cambia los totales visibles en pantalla
                    ValidaTotalPedido();
                    ValidaTipoNivelPrecio();

                    if (articulo.Text.Substring(0, 1) == "S")
                    {
                        int mSumar = 1;
                        DateTime mFechaEntrega = DateTime.Now.Date;

                        if (txtAnio.Text.Trim().Length == 2) txtAnio.Text = string.Format("20{0}", txtAnio.Text);
                        mFechaEntrega = new DateTime(Convert.ToInt32(txtAnio.Text), Convert.ToInt32(cbMes.SelectedValue), Convert.ToInt32(cbDia.SelectedValue));

                        if (mFechaEntrega.DayOfWeek == DayOfWeek.Saturday) mSumar = 2;
                        mFechaEntrega = mFechaEntrega.AddDays(mSumar);

                        cbDia.SelectedValue = mFechaEntrega.Day.ToString();
                        cbMes.SelectedValue = mFechaEntrega.Month.ToString();
                        txtAnio.Text = mFechaEntrega.Year.ToString();
                    }

                    articulo.Text = "";
                    descripcion.Text = "";
                    txtCantidad.Text = "1";
                    
                    txtTotal.Text = "0.00";
                    txtDescuentoItem.Text = "0.00";
                    txtPrecioUnitario.Text = "0.00";
                    txtFacturarItem.Text = "0.00";
                    txtVale.Text = "0";
                    txtCondicional.Text = "N";
                    txtArticuloCondicion.Text = "";
                    txtAutorizacion.Text = "0";
                    txtCantidad.ReadOnly = false;

                    cbGel.SelectedValue = "No";
                    cbRequisicion.SelectedValue = "No";
                    cbLocalizacion.SelectedValue = "ARMADO";

                    cbBodega.SelectedValue = cbTienda.SelectedValue;

                    gridVales.Visible = false;
                    lbError2.Text = "";
                    lbError2.Visible = false;
                    tblSolicutudAutorizacion.Visible = false;

                    cbRequisicion.SelectedValue = "No";
                    cbLocalizacion.SelectedValue = "ARMADO";

                    gridArticulos.SelectedIndex = -1;
                    gridArticulosDet.SelectedIndex = -1;

                    if (cbTipoPedido.SelectedValue == "P" || cbTipoPedido.SelectedValue == "S")
                    {
                        string mArticulo = "S00003-000";
                        string mDescripcion = "";

                        if (cbTipoPedido.SelectedValue == "S")
                        {
                            mArticulo = "S00002-000";
                            mDescripcion = "";
                            descripcion.TextMode = TextBoxMode.MultiLine;
                            descripcion.Height = new Unit(121);
                        }

                        articulo.Text = mArticulo;
                        descripcion.Text = mDescripcion;

                        descripcion.Focus();

                    }
                    else
                    {
                        articulo.Focus();
                        descripcion.Height = new Unit(22);
                        descripcion.TextMode = TextBoxMode.SingleLine;
                    }
                }
                else
                {
                    articulo.Text = "";
                    descripcion.Text = "";
                    txtCantidad.Text = "1";
                    txtPrecioUnitario.Text = "0.00";
                    txtTotal.Text = "0.00";
                    txtDescuentoItem.Text = "0.00";
                    txtFacturarItem.Text = "0.00";
                    txtVale.Text = "0";
                    txtCondicional.Text = "N";
                    txtArticuloCondicion.Text = "";
                    txtAutorizacion.Text = "0";
                    txtCantidad.ReadOnly = false;
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format("Error al agregar el artículo. {0} {1}", ex.Message, m);
            }
        }


        private void LimpiarListaArticulos()
        {
            List<NuevoPrecioProducto> lstProd = (List<NuevoPrecioProducto>)Session["lstNuevoPrecioProducto"];
            if (lstProd != null)
            {
                lstProd.Remove(lstProd.Find(x => x.Index == -1));
                Session["lstNuevoPrecioProducto"] = lstProd;
                txtSaldoDescuento.Text = String.Format("{0:0,0.00}", Facturacion.ObtenerSaldoDescuento(lstProd));
            }
        }
        protected void lbAgregarArticulo_Click(object sender, EventArgs e)
        {
            agregarArticulo();
        }

        protected void txtCantidad_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string mMensaje = "";
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                lbError2.Text = "";
                lbError2.Visible = false;

                Int32 mCantidad = 0;

                try
                {
                    mCantidad = Convert.ToInt32(txtCantidad.Text);
                }
                catch
                {
                    txtCantidad.Text = "1";
                    lbError2.Visible = true;
                    lbError2.Text = "Cantidad inválida.";
                    return;
                }

                if (mCantidad <= 0)
                {
                    txtCantidad.Text = "1";
                    lbError2.Visible = true;
                    lbError2.Text = "Cantidad inválida.";
                    return;
                }

                if (!ws.CantidadValida(articulo.Text, txtCantidad.Text, ref mMensaje))
                {
                    txtCantidad.Text = "1";
                    lbError2.Visible = true;
                    lbError2.Text = mMensaje;
                    return;
                }

                CalcularTotalArticulo();
                lbAgregarArticulo.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format("Error al validar la cantidad. {0} {1}", ex.Message, m);
            }
        }

        protected void txtPrecioUnitario_TextChanged(object sender, EventArgs e)
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                lbError2.Text = "";
                lbError2.Visible = false;

                if (cbTipoPedido.SelectedValue != "N") txtPrecioUnitarioDebioFacturar.Text = txtPrecioUnitario.Text;

                var qParametrosPrecios = ws.ParametrosModificarPrecios();

                decimal mPrecioUnitario = Convert.ToDecimal(txtPrecioUnitario.Text.Replace(",", ""));
                decimal mPrecioUnitarioDebioFacturar = Convert.ToDecimal(txtPrecioUnitarioDebioFacturar.Text.Replace(",", ""));

                if (mPrecioUnitario > mPrecioUnitarioDebioFacturar)
                {
                    if (qParametrosPrecios[0].CambiarPrecioHaciaArriba == "S")
                    {
                        decimal mDiferencia = mPrecioUnitario - mPrecioUnitarioDebioFacturar;

                        if (mDiferencia > qParametrosPrecios[0].MaximoPrecioHaciaArriba)
                        {
                            txtPrecioUnitario.Text = mPrecioUnitarioDebioFacturar.ToString();
                            lbError2.Visible = true;
                            lbError2.Text = "El precio modificado es inválido";
                        }
                    }
                    else
                    {
                        txtPrecioUnitario.Text = mPrecioUnitarioDebioFacturar.ToString();
                        lbError2.Visible = true;
                        lbError2.Text = "No está permitido el cambio de precios hacia arriba";
                    }
                }
                if (mPrecioUnitario < mPrecioUnitarioDebioFacturar)
                {
                    if (qParametrosPrecios[0].CambiarPrecioHaciaAbajo == "S")
                    {
                        decimal mDiferencia = mPrecioUnitarioDebioFacturar - mPrecioUnitario;

                        if (mDiferencia > qParametrosPrecios[0].MaximoPrecioHaciaAbajo)
                        {
                            txtPrecioUnitario.Text = mPrecioUnitarioDebioFacturar.ToString();
                            lbError2.Visible = true;
                            lbError2.Text = "El precio modificado es inválido";
                        }
                    }
                    else
                    {
                        txtPrecioUnitario.Text = mPrecioUnitarioDebioFacturar.ToString();
                        lbError2.Visible = true;
                        lbError2.Text = "No está permitido el cambio de precios hacia abajo";
                    }
                }

                CalcularTotalArticulo();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format("Error al modificar el precio. {0} {1}", ex.Message, m);
            }
        }

        void CalcularTotalArticulo()
        {
            txtTotal.Text = String.Format("{0:0,0.00}", Convert.ToDecimal(txtPrecioUnitario.Text.Replace(",", "")) * Convert.ToInt32(txtCantidad.Text.Replace(",", "")));
            lbAgregarArticulo.Focus();
        }

        protected void txtEnganche_TextChanged(object sender, EventArgs e)
        {
            lbError.Text = "";

            if (cbTipo.SelectedValue != "NR" && txtPedido.Text == "000000")
            {
                lbError.Text = "En las promociones, debe grabar primero la cotización, luego seleccionarla y aplicar el enganche.";
                txtEnganche.Text = "0.00";
                return;
            }

            ValidaTipoNivelPrecio();
        }

        protected void lkArticulo_Click(object sender, EventArgs e)
        {
            if (cbTipoPedido.SelectedValue == "N") habilitarBusquedaArticulos();
        }

        protected void cbTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CargaNivelesDePrecio();
                limpiar(true);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    //Nothing
                }

                lbError2.Visible = true;
                lbError2.Text = string.Format("Error al cambiar la promoción. {0} {1}", ex.Message, m);
            }
        }

        protected void lkAgregarArticulo2_Click(object sender, EventArgs e)
        {
            agregarArticulo();
            if (cbTipoPedido.SelectedValue == "N") habilitarBusquedaArticulos();
        }

        protected void articulo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                LimpiarListaArticulos();

                if (articulo.Text.Trim().Length == 0)
                    return;



                    lbError2.Text = "";
                    lbError2.Visible = false;

                    string mPrimerCaracter = articulo.Text.Substring(0, 1);

                    descripcion.Text = "";
                    txtCantidad.Text = "1";
                    txtPrecioUnitario.Text = "0.00";
                    txtTotal.Text = "0.00";
                    txtFacturarItem.Text = "0.00";
                    txtVale.Text = "0";
                    txtCondicional.Text = "N";
                    txtArticuloCondicion.Text = "";

                    try
                    {
                        int mDigito = Convert.ToInt32(mPrimerCaracter);

                        descripcionArticuloDet.Text = "";
                        codigoArticuloDet.Text = articulo.Text;
                        BuscarArticulo("C");
                    }
                    catch
                    {
                        codigoArticuloDet.Text = "";
                        descripcionArticuloDet.Text = articulo.Text.ToUpper();
                        BuscarArticulo("D");
                    }

                    if (gridArticulosDet.Rows.Count == 1)
                    {
                        gridArticulosDet.SelectedIndex = 0;
                        //seleccionarArticulo();
                    }
                    if (gridArticulosDet.Rows.Count > 1)
                    {
                        tablaBusquedaDet.Visible = true;
                        gridArticulosDet.Visible = true;

                        lbError2.Text = "";
                        lbError2.Visible = false;

                        Label6.Text = "";
                        Label7.Text = "";
                        codigoArticuloDet.Focus();
                    }
                
            }
            catch (Exception ex)
            {
                lbError2.Visible = true;
                lbError2.Text = string.Format("Error al validar el artículo {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }

        protected void lbLocalizacion_Click(object sender, EventArgs e)
        {
            if (cbTipoPedido.SelectedValue == "N")
            {
                if (gridLocalizaciones.Visible)
                {
                    gridLocalizaciones.Visible = false;
                    return;
                }
                if (articulo.Text.Trim().Length == 0) return;

                cargarLocalizaciones(articulo.Text.Trim());
            }
        }

        void cargarLocalizaciones(string articulo)
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveBodegaLocalizacion(articulo);

            gridLocalizaciones.DataSource = q;
            gridLocalizaciones.DataBind();
            gridLocalizaciones.Visible = true;

            if (gridLocalizaciones.Rows.Count > 0)
            {
                gridLocalizaciones.Focus();
                gridLocalizaciones.SelectedIndex = 0;
                LinkButton lkSeleccionar = (LinkButton)gridLocalizaciones.SelectedRow.FindControl("lkSeleccionarLocalizacion");

                lkSeleccionar.Focus();
            }
        }

        protected void gridLocalizaciones_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridLocalizaciones_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbBodega.SelectedValue = gridLocalizaciones.SelectedRow.Cells[3].Text;
            cbLocalizacion.SelectedValue = gridLocalizaciones.SelectedRow.Cells[4].Text;

            gridLocalizaciones.Visible = false;
            lbAgregarArticulo.Focus();
        }

        protected void lkSeleccionarLocalizacion_Click(object sender, EventArgs e)
        {

        }

        bool pasaValidacionesFactura()
        {
            if (txtPedido.Text == "P000000")
            {
                lbError.Text = "Debe seleccionar un pedido para facturar.";
                return false;
            }
            if (txtFacturado.Text == "S")
            {
                lbError.Text = string.Format("El pedido {0} ya se encuentra facturado.", txtPedido.Text);
                return false;
            }
            if (txtNumeroFactura.Text.Trim().Length == 0)
            {
                lbError.Text = "Debe ingresar el número de factura.";
                SugerirNumeroFactura();
                return false;
            }
            if (!cbNumeroFactura.Checked)
            {
                lbError.Text = "Debe aceptar el número de factura.";
                cbNumeroFactura.Focus();
                return false;
            }

            return true;
        }
           
        protected void lkFacturar_Click(object sender, EventArgs e)
        {
        }

        bool pasaValidacionesDespacho()
        {
            if (txtPedido.Text == "P000000")
            {
                lbError.Text = "Debe seleccionar un pedido para despachar.";
                return false;
            }
            if (txtFacturado.Text == "N")
            {
                lbError.Text = string.Format("El pedido {0} debe estar facturado para poderlo despachar.", txtPedido.Text);
                return false;
            }
            if (txtDespachado.Text == "S")
            {
                lbError.Text = string.Format("El pedido {0} ya se encuentra despachado.", txtPedido.Text);
                return false;
            }
            if (!cbNumeroDespacho.Checked)
            {
                lbError.Text = "Debe aceptar el número de envío.";
                cbNumeroFactura.Focus();
                return false;
            }

            return true;
        }

        protected void lkDespachar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!pasaValidacionesDespacho()) return;

                lbInfo.Text = "";
                lbError.Text = "";
                lbError2.Text = "";
                lbError2.Visible = false;

                tblBuscar.Visible = false;
                tablaBusquedaDet.Visible = false;

                gridClientes.Visible = false;
                gridArticulosDet.Visible = false;

                string mTienda = Convert.ToString(Session["Tienda"]);
                string mMensaje = ""; string mDespacho = txtNumeroDespacho.Text;
                if (mTienda.Length > 4) mTienda = "F01";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.GrabarDespacho(Convert.ToString(ViewState["Factura"]), mDespacho, ref mMensaje, Convert.ToString(Session["Usuario"]), txtObservacionesDespacho.Text, mTienda, ""))
                {
                    lbError.Text = mMensaje;
                    return;
                }

                lbInfo.Text = mMensaje;

                txtNumeroDespacho.Text = "";
                cbNumeroDespacho.Checked = false;
                txtObservacionesDespacho.Text = "";
                tblDespacho.Visible = false;
                lkDespachar.Enabled = false;

                DataSet dsInfo = new DataSet();
                wsPuntoVenta.Despacho[] q = new wsPuntoVenta.Despacho[1];
                wsPuntoVenta.DespachoLinea[] qLinea = new wsPuntoVenta.DespachoLinea[1];

                mMensaje = "";
                if (!ws.DevuelveImpresionDespacho(mDespacho, Convert.ToString(ViewState["Factura"]), ref mMensaje, ref q, ref qLinea, ref dsInfo))
                {
                    lbError.Text = mMensaje;
                }

                dsPuntoVenta ds = new dsPuntoVenta();

                DataRow row = ds.Despacho.NewRow();
                row["NoDespacho"] = q[0].NoDespacho;
                row["Fecha"] = q[0].Fecha;
                row["Cliente"] = q[0].Cliente;
                row["Nombre"] = q[0].Nombre;
                row["Direccion"] = q[0].Direccion;
                row["Indicaciones"] = q[0].Indicaciones;
                row["Telefonos"] = q[0].Telefonos;
                row["FormaPago"] = q[0].FormaPago;
                row["Tienda"] = q[0].Tienda;
                row["Vendedor"] = q[0].Vendedor;
                row["NombreVendedor"] = q[0].NombreVendedor;
                row["Factura"] = q[0].Factura;
                row["Observaciones"] = q[0].Observaciones;
                row["Notas"] = q[0].Notas;
                ds.Despacho.Rows.Add(row);

                for (int ii = 0; ii < qLinea.Count(); ii++)
                {
                    DataRow rowDet = ds.DespachoLinea.NewRow();
                    rowDet["NoDespacho"] = qLinea[ii].NoDespacho;
                    rowDet["Articulo"] = qLinea[ii].Articulo;
                    rowDet["Cantidad"] = qLinea[ii].Cantidad;
                    rowDet["Descripcion"] = qLinea[ii].Descripcion;
                    rowDet["Bodega"] = qLinea[ii].Bodega;
                    ds.DespachoLinea.Rows.Add(rowDet);
                }

                tblDespacho.Visible = false;
                txtDespachado.Text = "S";

                using (ReportDocument reporte = new ReportDocument())
                {
                    string p = (Request.PhysicalApplicationPath + "reportes/rptDespachoPDF.rpt");
                    reporte.Load(p);

                    string mNombreDocumento = string.Format("{0}.pdf", mDespacho);

                    reporte.SetDataSource(ds);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }

                //ScriptManager.RegisterStartupScript(this, typeof(Page), "jsKeys", "javascript:Forzar();", true);
                limpiar(true);
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al generar el envío. {0} {1}", ex.Message, ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")));
                return;
            }
        }

        protected void lkNuevoPedido_Click(object sender, EventArgs e)
        {
            limpiar(true);
            mostrarBusquedaCliente();
        }

        protected void lkNumeroFactura_Click(object sender, EventArgs e)
        {
            SugerirNumeroFactura();
        }

        void SugerirNumeroFactura()
        {
            try
            {
                txtNumeroFactura.Text = "";
                cbNumeroFactura.Checked = false;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mTienda = Convert.ToString(Session["Tienda"]);
                if (mTienda.Length > 4) mTienda = "F01";

                string mTipo = ws.DevuelveTipoNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                string mFactura = ws.NumeroFactura(mTienda, mTipo);

                txtNumeroFactura.Text = mFactura;

                if (mFactura == "No disponible")
                {
                    txtNumeroFactura.Enabled = false;
                }
                else
                {
                    lbError.Text = "";
                    lbError2.Text = "";
                    lbError2.Visible = false;
                    txtNumeroFactura.Enabled = true;
                }

                cbNumeroFactura.Focus();
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al sugerir el número de factura. {0} {1}", ex.Message, ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")));
                return;
            }
        }

        protected void lkNumeroEnvio_Click(object sender, EventArgs e)
        {
            SugerirNumeroDespacho();
        }

        void SugerirNumeroDespacho()
        {
            try
            {
                txtNumeroDespacho.Text = "";
                txtObservacionesDespacho.Text = "";
                cbNumeroDespacho.Checked = false;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mTienda = Convert.ToString(Session["Tienda"]);
                if (mTienda.Length > 4) mTienda = "F01";

                string mDespacho = ws.NumeroDespacho(mTienda);

                txtNumeroDespacho.Text = mDespacho;
                cbNumeroDespacho.Focus();
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al sugerir el número de envío. {0} {1}", ex.Message, ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")));
                return;
            }
        }

        protected void cbTipoPedido_SelectedIndexChanged(object sender, EventArgs e)
        {
            string mTipoPedido = cbTipoPedido.SelectedValue;
            limpiar(true);
            cbTipoPedido.SelectedValue = mTipoPedido;

            if (cbTipoPedido.SelectedValue == "N") return;

            cbTipo.SelectedValue = "NR";
            cbFinanciera.SelectedValue = "1";

            CargaNivelesDePrecio();
            cbNivelPrecio.SelectedValue = "ContadoEfect";

            if (cbTipoPedido.SelectedValue == "A")
            {
                articulo.Text = "AIA001-000";
                cbRequisicion.SelectedValue = "Sí";
                descripcion.Text = "Ingrese aquí la descripción del armado.";
            }
            else
            {
                articulo.Text = "S00002-000";
                cbRequisicion.SelectedValue = "No";
                descripcion.Text = "Ingrese aquí la descripción del flete o repuesto.";
            }

            cbTipo.Enabled = false;
            cbFinanciera.Enabled = false;
            cbNivelPrecio.Enabled = false;
            articulo.ReadOnly = true;
            descripcion.ReadOnly = false;
            txtPrecioUnitario.ReadOnly = false;
            txtPrecioUnitario.AutoPostBack = true;
            cbRequisicion.Enabled = false;
            cbLocalizacion.Enabled = false;

            cbLocalizacion.SelectedValue = "ARMADO";
            cbBodega.SelectedValue = Convert.ToString(cbTienda.SelectedValue);

            descripcion.Focus();
        }
        
        protected void cbTienda_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaVendedores();
        }

        void CargaVendedores()
        {
            cbVendedor.SelectedValue = null;

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveVendedoresTienda(cbTienda.SelectedValue).ToList();

            cbVendedor.DataSource = q;
            cbVendedor.DataTextField = "Nombre";
            cbVendedor.DataValueField = "Vendedor";
            cbVendedor.DataBind();
        }

        protected void lkReImprimirFactura_Click(object sender, EventArgs e)
        {
            string mMensaje = "";

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            wsPuntoVenta.Factura[] q = new wsPuntoVenta.Factura[1];
            wsPuntoVenta.FacturaLinea[] qLinea = new wsPuntoVenta.FacturaLinea[1];
            
            mMensaje = "";
            if (!ws.DevuelveImpresionFactura(txtFacturaReImprimir.Text, ref mMensaje, ref q, ref qLinea)) lbError.Text = mMensaje;

            dsPuntoVenta ds = new dsPuntoVenta();

            DataRow row = ds.Factura.NewRow();
            row["NoFactura"] = q[0].NoFactura;
            row["Fecha"] = q[0].Fecha;
            row["Cliente"] = q[0].Cliente;
            row["Nombre"] = q[0].Nombre;
            row["Nit"] = q[0].Nit;
            row["Direccion"] = q[0].Direccion;
            row["Vendedor"] = q[0].Vendedor;
            row["Monto"] = q[0].Monto;
            row["MontoLetras"] = q[0].MontoLetras;
            ds.Factura.Rows.Add(row);
            
            for (int ii = 0; ii < qLinea.Count(); ii++)
            {
                DataRow rowDet = ds.FacturaLinea.NewRow();
                rowDet["NoFactura"] = qLinea[ii].NoFactura;
                rowDet["Articulo"] = qLinea[ii].Articulo;
                rowDet["Cantidad"] = qLinea[ii].Cantidad;
                rowDet["Descripcion"] = qLinea[ii].Descripcion;
                rowDet["PrecioUnitario"] = qLinea[ii].PrecioUnitario;
                rowDet["PrecioTotal"] = qLinea[ii].PrecioTotal;
                ds.FacturaLinea.Rows.Add(rowDet);
            }

            using (ReportDocument reporte = new ReportDocument())
            {
                string p = (Request.PhysicalApplicationPath + "reportes/rptFacturaPDF.rpt");
                reporte.Load(p);

                string mNombreDocumento = string.Format("{0}.pdf", txtFacturaReImprimir.Text);

                reporte.SetDataSource(ds);
                reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                Response.End();
            }

            lkFacturar.Enabled = false;
            tblFactura.Visible = false;
            tblDespacho.Visible = true;

            ScriptManager.RegisterStartupScript(this, typeof(Page), "jsKeys", "javascript:Forzar();", true);
        }
        
        protected void lkReImprimirDespacho_Click(object sender, EventArgs e)
        {
            lbError.Text = "";
            lbError2.Text = "";
            lbError2.Visible = false;

            string mMensaje = "";

            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                DataSet dsInfo = new DataSet();
                wsPuntoVenta.Despacho[] q = new wsPuntoVenta.Despacho[1];
                wsPuntoVenta.DespachoLinea[] qLinea = new wsPuntoVenta.DespachoLinea[1];

                mMensaje = "";
                if (!ws.DevuelveImpresionDespacho(txtDespachoReImprimir.Text, txtFacturaReImprimir.Text, ref mMensaje, ref q, ref qLinea, ref dsInfo))
                {
                    lbError.Text = mMensaje;
                    return;
                }

                dsPuntoVenta ds = new dsPuntoVenta();

                DataRow row = ds.Despacho.NewRow();
                row["NoDespacho"] = q[0].NoDespacho;
                row["Fecha"] = q[0].Fecha;
                row["Cliente"] = q[0].Cliente;
                row["Nombre"] = q[0].Nombre;
                row["Direccion"] = q[0].Direccion;
                row["Indicaciones"] = q[0].Indicaciones;
                row["Telefonos"] = q[0].Telefonos;
                row["FormaPago"] = q[0].FormaPago;
                row["Tienda"] = q[0].Tienda;
                row["Vendedor"] = q[0].Vendedor;
                row["NombreVendedor"] = q[0].NombreVendedor;
                row["Factura"] = q[0].Factura;
                row["Observaciones"] = q[0].Observaciones;
                row["Notas"] = q[0].Notas;
                ds.Despacho.Rows.Add(row);

                for (int ii = 0; ii < qLinea.Count(); ii++)
                {
                    DataRow rowDet = ds.DespachoLinea.NewRow();
                    rowDet["NoDespacho"] = qLinea[ii].NoDespacho;
                    rowDet["Articulo"] = qLinea[ii].Articulo;
                    rowDet["Cantidad"] = qLinea[ii].Cantidad;
                    rowDet["Descripcion"] = qLinea[ii].Descripcion;
                    rowDet["Bodega"] = qLinea[ii].Bodega;
                    ds.DespachoLinea.Rows.Add(rowDet);
                }

                using (ReportDocument reporte = new ReportDocument())
                {
                    string p = (Request.PhysicalApplicationPath + "reportes/rptDespachoPDF.rpt");
                    reporte.Load(p);

                    string mNombreDocumento = string.Format("{0}.pdf", txtDespachoReImprimir.Text);

                    reporte.SetDataSource(ds);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }

                ScriptManager.RegisterStartupScript(this, typeof(Page), "jsKeys", "javascript:Forzar();", true);
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al imprimir el envío. {0} {1} {2}", ex.Message, mMensaje, ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")));
                return;
            }
        }

        protected void lkReImprimir_Click(object sender, EventArgs e)
        {
            liReImprimir.Visible = !liReImprimir.Visible;
        }

        protected void lkVale_Click(object sender, EventArgs e)
        {
            if (txtCodigo.Text.Trim().Length == 0)
            {
                lbError2.Visible = true;
                lbError2.Text = "Debe seleccionar un cliente.";
                return;
            }

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveValesCliente(txtCodigo.Text);

            if (q.Length == 0)
            {
                gridVales.Visible = false;
                lbError2.Visible = true;
                lbError2.Text = string.Format("El cliente {0} no tiene vales disponibles.", txtCodigo.Text);
                return;
            }

            gridVales.DataSource = q;
            gridVales.DataBind();
            gridVales.Visible = true;
            gridVales.SelectedIndex = -1;

            lbError.Text = "";
            lbError2.Text = "";
            lbError2.Visible = false;

            if (q.Count() == 1) gridVales.SelectedIndex = 0;
            if (q.Count() > 0)
            {
                gridVales.Focus();
                gridVales.SelectedIndex = 0;
                LinkButton lkSeleccionar = (LinkButton)gridVales.SelectedRow.FindControl("lkSeleccionarVale");

                lkSeleccionar.Focus();
            }
        }

        protected void lkSeleccionarVale_Click(object sender, EventArgs e)
        {

        }

        protected void gridVales_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridVales_SelectedIndexChanged(object sender, EventArgs e)
        {
            double mTotal = Convert.ToDouble(txtTotal.Text.Replace(",", ""));

            if (mTotal < 2000)
            {
                gridVales.Visible = false;
                lbError2.Visible = true;
                lbError2.Text = "Para poder aplicar el vale, la compra debe ser mayor a Q2,000.00";
                return;
            }
            if (descripcion.Text.Trim().Length == 0)
            {
                gridVales.Visible = false;
                lbError2.Visible = true;
                lbError2.Text = "Debe seleccionar un artículo al que se le aplicará el vale.";
                return;
            }
            for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
            {
                Label lbVale = (Label)gridArticulos.Rows[ii].FindControl("lbVale");

                if (lbVale.Text == gridVales.SelectedRow.Cells[1].Text)
                {
                    gridVales.Visible = false;
                    lbError2.Visible = true;
                    lbError2.Text = string.Format("El vale {0} ya está siendo utilizado en el artículo {1}.", gridVales.SelectedRow.Cells[1].Text, gridArticulos.Rows[ii].Cells[1].Text);
                    return;
                }
                if (Convert.ToInt32(lbVale.Text) > 0)
                {
                    gridVales.Visible = false;
                    lbError2.Visible = true;
                    lbError2.Text = "Solo está permitido aplicar un vale por compra.";
                    return;
                }
            }

            string mVigente = gridVales.SelectedRow.Cells[4].Text;

            if (mVigente == "No")
            {
                gridVales.Visible = false;
                lbError2.Visible = true;
                lbError2.Text = "El vale seleccionado ya no se encuentra vigente.";
                return;
            }

            gridVales.Visible = false;
            lbError2.Visible = false;
            lbError2.Text = "";

            txtVale.Text = gridVales.SelectedRow.Cells[1].Text;
            descripcion.Text = string.Format("** Vale por Q200 desc. ** {0}", descripcion.Text);
            txtPrecioUnitario.Text = String.Format("{0:0,0.00}", Convert.ToDecimal(txtPrecioUnitario.Text.Replace(",", "")) - (200 / Convert.ToInt32(txtCantidad.Text.Replace(",", ""))));
            CalcularTotalArticulo();
        }

        void CargarPedidosCliente()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelvePedidosCliente(txtCodigo.Text);

            if (q.Length == 0)
            {
                gridPedidos.Visible = false;
            }
            else
            {
                gridPedidos.DataSource = q;
                gridPedidos.DataBind();
                gridPedidos.Visible = true;
                gridPedidos.SelectedIndex = -1;
            }
        }

        protected void gridPedidos_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string mMensaje = "";
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                lbInfo.Text = "";
                lbError.Text = "";

                string mPedido = gridPedidos.SelectedRow.Cells[7].Text;
                Label lbFinanciera = (Label)gridPedidos.SelectedRow.FindControl("lbFinanciera");

                switch (ViewState["AccionPedido"].ToString())
                {
                    case "Eliminar":
                        if (ws.PedidoFacturado(mPedido))
                        {
                            lbError.Text = "Este pedido se encuentra facturado, no es posible eliminarlo";
                            return;
                        }
                        if (ws.FacturaDespachada(mPedido))
                        {
                            lbError.Text = "La factura de este pedido se encuentra despachada, no es posible eliminarlo";
                            return;
                        }

                        if (ws.EliminarPedido(ref mMensaje, mPedido))
                        {
                            lbInfo.Text = mMensaje;
                            CargarPedidosCliente();
                        }
                        else
                        {
                            lbError.Text = mMensaje;
                            return;
                        }
                        break;
                    case "Seleccionar":
                        SeleccionarPedido();

                        if (ws.PedidoFacturado(mPedido))
                        {
                            lbError.Text = "Este pedido se encuentra facturado, no le podrá realizar cambios";
                            lkGrabar.Visible = false;
                            lkVale.Visible = false;
                            lkSolicitar.Visible = false;
                        }
                        if (ws.FacturaDespachada(mPedido))
                        {
                            lbError.Text = "La factura de este pedido se encuentra despachada, no le podrá realizar cambios";
                            lkGrabar.Visible = false;
                            lkVale.Visible = false;
                            lkSolicitar.Visible = false;
                        }

                        break;
                    case "Facturar":
                        if (ws.PedidoFacturado(mPedido))
                        {
                            lbError.Text = "Este pedido ya se encuentra facturado";
                            return;
                        }

                        SeleccionarPedido();

                        tblInfo.Visible = true;
                        lkGrabar.Visible = false;
                        lkVale.Visible = false;
                        lkSolicitar.Visible = false;
                        lkFacturar.Enabled = true;
                        tblFactura.Visible = true;
                        SugerirNumeroFactura();

                        break;
                    case "Despachar":
                        if (!ws.PedidoFacturado(mPedido))
                        {
                            lbError.Text = "Este pedido no se encuentra facturado";
                            return;
                        }
                        if (ws.FacturaDespachada(mPedido))
                        {
                            lbError.Text = "La factura de este pedido ya se encuentra despachada";
                            return;
                        }

                        SeleccionarPedido();
                        txtFacturado.Text = "S";

                        tblInfo.Visible = true;
                        lkGrabar.Visible = false;
                        lkVale.Visible = false;
                        lkSolicitar.Visible = false;

                        lkFacturar.Enabled = false;
                        tblFactura.Visible = false;
                        tblDespacho.Visible = true;
                        lkDespachar.Enabled = true;

                        if (cbTipoPedido.SelectedValue == "N")
                        {
                            ViewState["Factura"] = ws.DevuelveFacturaDePedido(txtPedido.Text);
                            SugerirNumeroDespacho();
                        }
                        else
                        {
                            limpiar(true);
                            lbError.Text = "Este tipo de pedido no genera despacho";
                            lbError2.Text = "Este tipo de pedido no genera despacho";
                        }

                        break;
                    case "Solicitud":
                        lbError.Text = "";
                        
                        if (ws.EsBanco(lbFinanciera.Text))
                        {
                            var q = ws.DevuelveSolicitudCredito("P", mPedido);

                            using (ReportDocument reporte = new ReportDocument())
                            {
                                string p = (Request.PhysicalApplicationPath + "reportes/SolicitudInterConsumo.rpt");
                                reporte.Load(p);

                                string mNombreDocumento = string.Format("Solicitud_{0}.pdf", mPedido);

                                reporte.SetParameterValue("PrimerNombre", q[0].PrimerNombre);
                                reporte.SetParameterValue("SegundoNombre", q[0].SegundoNombre);
                                reporte.SetParameterValue("TercerNombre", q[0].TercerNombre);
                                reporte.SetParameterValue("PrimerApellido", q[0].PrimerApellido);
                                reporte.SetParameterValue("SegundoApellido", q[0].SegundoApellido);
                                reporte.SetParameterValue("ApellidoCasada", q[0].ApellidoCasada);
                                reporte.SetParameterValue("DPI", q[0].DPI);
                                reporte.SetParameterValue("FechaNacimiento", q[0].FechaNacimiento);
                                reporte.SetParameterValue("EstadoCivil", q[0].EstadoCivil);
                                reporte.SetParameterValue("Genero", q[0].Genero);
                                reporte.SetParameterValue("Nacionalidad", q[0].Nacionalidad);
                                reporte.SetParameterValue("Profesion", q[0].Profesion);
                                reporte.SetParameterValue("NIT", q[0].Nit);
                                reporte.SetParameterValue("Direccion", string.Format("{0} {1} Zona {2} {3} {4}", q[0].CalleAvenidaFacturacion, q[0].CasaNumeroFacturacion, q[0].ZonaFacturacion, q[0].ApartamentoFacturacion, q[0].ColoniaFacturacion));
                                reporte.SetParameterValue("Municipio", q[0].MunicipioFacturacion);
                                reporte.SetParameterValue("Politico", q[0].Politico);
                                reporte.SetParameterValue("Departamento", q[0].DepartamentoFacturacion);
                                reporte.SetParameterValue("TelefonoResidencia", q[0].Telefono1);
                                reporte.SetParameterValue("TelefonoCelular", q[0].Celular);
                                reporte.SetParameterValue("CargasFamiliares", q[0].CargasFamiliares);
                                reporte.SetParameterValue("TipoCasa", q[0].TipoCasa);
                                reporte.SetParameterValue("VehiculoAnio", q[0].MarcaModelo);
                                reporte.SetParameterValue("VehiculoPlacas", q[0].Placas);
                                reporte.SetParameterValue("Email", q[0].Email);
                                reporte.SetParameterValue("PagoMensual", q[0].PagoMensual);
                                reporte.SetParameterValue("TiempoResidir", q[0].TiempoResidir);
                                reporte.SetParameterValue("ReferenciasBancarias", q[0].ReferenciasBancarias);
                                reporte.SetParameterValue("Empresa", q[0].Empresa);
                                reporte.SetParameterValue("Cargo", q[0].Cargo);
                                reporte.SetParameterValue("TiempoTrabajar", q[0].TiempoLaborar);
                                reporte.SetParameterValue("IngresosMensuales", q[0].IngresosMes);
                                reporte.SetParameterValue("EgresosMensuales", q[0].GastosMes);
                                reporte.SetParameterValue("DireccionTrabajo", q[0].EmpresaDireccion);
                                reporte.SetParameterValue("MunicipioTrabajo", q[0].MunicipioEmpresa);
                                reporte.SetParameterValue("DepartamentoTrabajo", q[0].DepartamentoEmpresa);
                                reporte.SetParameterValue("TelefonoTrabajo", q[0].EmpresaTelefono);
                                reporte.SetParameterValue("Extension", q[0].Extension);
                                reporte.SetParameterValue("NombreConyugue", q[0].ConyugeNombre);
                                reporte.SetParameterValue("FechaNacimientoConyugue", q[0].ConyugueFechaNacimiento);
                                reporte.SetParameterValue("DPIConyugue", q[0].ConyugueCedula);
                                reporte.SetParameterValue("TrabajoConyugue", q[0].ConyugueEmpresa);
                                reporte.SetParameterValue("CargoConyugue", q[0].ConyugueCargo);
                                reporte.SetParameterValue("TiempoConyugue", q[0].ConyugueTiempo);
                                reporte.SetParameterValue("IngresosConyugue", q[0].ConyugueIngresos);
                                reporte.SetParameterValue("DireccionTrabajoConyugue", q[0].ConyugueEmpresaDireccion);
                                reporte.SetParameterValue("TelefonoTrabajoConyugue", q[0].ConyugueEmpresaTelefono);
                                reporte.SetParameterValue("ReferenciaPersonal1", q[0].RefPersonalNombre1);
                                reporte.SetParameterValue("ReferenciaPersonalResidencia1", q[0].RefPersonalTelResidencia1);
                                reporte.SetParameterValue("ReferenciaPersonalTrabajo1", q[0].RefPersonalTelTrabajo1);
                                reporte.SetParameterValue("ReferenciaPersonalCelular1", q[0].RefPersonalCelular1);
                                reporte.SetParameterValue("ReferenciaPersonal2", q[0].RefPersonalNombre2);
                                reporte.SetParameterValue("ReferenciaPersonalResidencia2", q[0].RefPersonalTelResidencia2);
                                reporte.SetParameterValue("ReferenciaPersonalTrabajo2", q[0].RefPersonalTelTrabajo2);
                                reporte.SetParameterValue("ReferenciaPersonalCelular2", q[0].RefPersonalCelular2);
                                reporte.SetParameterValue("ReferenciaComercial1", q[0].RefComercialNombre1);
                                reporte.SetParameterValue("ReferenciaComercialResidencia1", q[0].RefComercialTelefono1);
                                reporte.SetParameterValue("ReferenciaComercialTrabajo1", q[0].RefComercialFax1);
                                reporte.SetParameterValue("ReferenciaComercialCelular1", q[0].RefComercialCelular1);
                                reporte.SetParameterValue("ReferenciaComercial2", q[0].RefComercialNombre2);
                                reporte.SetParameterValue("ReferenciaComercialResidencia2", q[0].RefComercialTelefono2);
                                reporte.SetParameterValue("ReferenciaComercialTrabajo2", q[0].RefComercialFax2);
                                reporte.SetParameterValue("ReferenciaComercialCelular2", q[0].RefComercialCelular2);
                                reporte.SetParameterValue("PrecioDelBien", q[0].TotalFacturar);
                                reporte.SetParameterValue("Enganche", q[0].Enganche);
                                reporte.SetParameterValue("SaldoFinanciar", q[0].SaldoFinanciar);
                                reporte.SetParameterValue("Recargos", q[0].Recargos);
                                reporte.SetParameterValue("Monto", q[0].Monto);
                                reporte.SetParameterValue("Plazo", q[0].Plazo);
                                reporte.SetParameterValue("CantidadPagos1", q[0].CantidadPagos1);
                                reporte.SetParameterValue("MontoPagos1", q[0].MontoPagos1);
                                reporte.SetParameterValue("CantidadPagos2", q[0].CantidadPagos2);
                                reporte.SetParameterValue("MontoPagos2", q[0].MontoPagos2);
                                reporte.SetParameterValue("Solicitud", "95624");
                                reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                                Response.Clear();
                                Response.ContentType = "application/pdf";
                                Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                                Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                                Response.End();
                            }
                        }
                        else
                        {
                            lbError.Text = "Esta opción solo aplica para un banco";
                        }

                        break;
                    case "Pagaré":
                        lbError.Text = "";

                        if (ws.EsBanco(lbFinanciera.Text))
                        {
                            //----------------------------------------------------------------
                            //Validamos si esta habilitado el modulo
                            //----------------------------------------------------------------
                            WebRequest request = WebRequest.Create(string.Format("{0}/ModuloHabilitado/?Modulo={1}", Convert.ToString(Session["UrlRestServices"]), Const.MODULO.INTERCONSUMO_PAGARE));
                            request.Method = "GET";
                            request.ContentType = "application/json";
                            var response = (HttpWebResponse)request.GetResponse();
                            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                            //----------------------------------------------------------------
                            //Obtenemos las respuesta de las validaciones del pagaré.
                            //----------------------------------------------------------------
                            bool mModuloHabilitado = JsonConvert.DeserializeObject<bool>(responseString);
                            if (!mModuloHabilitado)
                            {
                                lbError.Text = "Modulo no habilitado.";
                                return;
                            }

                            //----------------------------------------------------------------
                            //Aplicamos las reglas de negocio para poder imprimir el pagare.
                            //----------------------------------------------------------------
                            request = WebRequest.Create(string.Format("{0}/RN001DocumentoInterconsumo/?NumeroDocumento={1}", Convert.ToString(Session["UrlRestServices"]), mPedido));
                            request.Method = "GET";
                            request.ContentType = "application/json";
                            response = (HttpWebResponse)request.GetResponse();
                            responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                            //----------------------------------------------------------------
                            //Obtenemos las respuesta de las validaciones del pagaré.
                            //----------------------------------------------------------------
                            MF_Clases.Respuesta mRespuesta = JsonConvert.DeserializeObject<MF_Clases.Respuesta>(responseString);
                            if (!mRespuesta.Exito)
                            {
                                lbError.Text = mRespuesta.Mensaje;
                                return;
                            }

                            //----------------------------------------------------------------
                            //Procedemos a imprimir el pagaré.
                            //----------------------------------------------------------------
                            MF_Clases.Clases.Pedido mPedidoInterconsumo = JsonConvert.DeserializeObject<MF_Clases.Clases.Pedido>(mRespuesta.Objeto.ToString());
                            Response.Redirect(string.Format("{0}/DocumentoPdfInterconsumo/?Tipo={1}&NumeroReferencia={2}", Convert.ToString(Session["UrlRestServices"]), Const.DocumentoInterconsumo.PAGARE, mPedidoInterconsumo.SOLICITUD));

                        }
                        else
                        {
                            lbError.Text = "Esta opción solo aplica para un banco";
                        }

                        break;
                    case "Expediente":
                        lbError.Text = "";

                        dsPuntoVenta ds = new dsPuntoVenta();
                        var qExpediente = ws.DevuelveExpediente(mPedido, "");
                        var qExpedienteDet = ws.DevuelveExpedienteDet(mPedido, "");

                        DataRow mRowExpediente = ds.Expediente.NewRow();
                        mRowExpediente["Cliente"] = qExpediente[0].Cliente;
                        mRowExpediente["Pedido"] = qExpediente[0].Pedido;
                        mRowExpediente["Fecha"] = qExpediente[0].Fecha;
                        mRowExpediente["Vendedor"] = qExpediente[0].Vendedor;
                        mRowExpediente["CodigoVendedor"] = qExpediente[0].CodigoVendedor;
                        mRowExpediente["Tienda"] = qExpediente[0].Tienda;
                        mRowExpediente["PrimerNombre"] = qExpediente[0].PrimerNombre;
                        mRowExpediente["SegundoNombre"] = qExpediente[0].SegundoNombre;
                        mRowExpediente["TercerNombre"] = qExpediente[0].TercerNombre;
                        mRowExpediente["PrimerApellido"] = qExpediente[0].PrimerApellido;
                        mRowExpediente["SegundoApellido"] = qExpediente[0].SegundoApellido;
                        mRowExpediente["ApellidoCasada"] = qExpediente[0].ApellidoCasada;
                        mRowExpediente["DPI"] = qExpediente[0].DPI;
                        mRowExpediente["NIT"] = qExpediente[0].NIT;
                        mRowExpediente["Direccion"] = qExpediente[0].Direccion;
                        mRowExpediente["Colonia"] = qExpediente[0].Colonia;
                        mRowExpediente["Zona"] = qExpediente[0].Zona;
                        mRowExpediente["Departamento"] = qExpediente[0].Departamento;
                        mRowExpediente["Municipio"] = qExpediente[0].Municipio;
                        mRowExpediente["TelefonoResidencia"] = qExpediente[0].TelefonoResidencia;
                        mRowExpediente["TelefonoCelular"] = qExpediente[0].TelefonoCelular;
                        mRowExpediente["TelefonoTrabajo"] = qExpediente[0].TelefonoTrabajo;
                        mRowExpediente["Email"] = qExpediente[0].Email;
                        mRowExpediente["EntregarMismaDireccion"] = qExpediente[0].EntregarMismaDireccion;
                        mRowExpediente["NombreEntrega"] = qExpediente[0].NombreEntrega;
                        mRowExpediente["DireccionEntrega"] = qExpediente[0].DireccionEntrega;
                        mRowExpediente["ColoniaEntrega"] = qExpediente[0].ColoniaEntrega;
                        mRowExpediente["ZonaEntrega"] = qExpediente[0].ZonaEntrega;
                        mRowExpediente["DepartamentoEntrega"] = qExpediente[0].DepartamentoEntrega;
                        mRowExpediente["MunicipioEntrega"] = qExpediente[0].MunicipioEntrega;
                        mRowExpediente["EntraCamion"] = qExpediente[0].EntraCamion;
                        mRowExpediente["EntraPickup"] = qExpediente[0].EntraPickup;
                        mRowExpediente["EsSegundoPiso"] = qExpediente[0].EsSegundoPiso;
                        mRowExpediente["Notas"] = qExpediente[0].Notas;
                        mRowExpediente["BodegaSale"] = qExpediente[0].BodegaSale;
                        mRowExpediente["TiendaSale"] = qExpediente[0].TiendaSale;
                        mRowExpediente["ObsVendedor"] = qExpediente[0].ObsVendedor;
                        mRowExpediente["ObsGerencia"] = qExpediente[0].ObsGerencia;
                        mRowExpediente["Gerente"] = qExpediente[0].Gerente;
                        mRowExpediente["Factura"] = qExpediente[0].Factura;
                        mRowExpediente["Garantia"] = qExpediente[0].Garantia;
                        mRowExpediente["Promocion"] = qExpediente[0].Promocion;
                        mRowExpediente["Financiera"] = qExpediente[0].Financiera;
                        mRowExpediente["QuienAutorizo"] = qExpediente[0].QuienAutorizo;
                        mRowExpediente["NoAutorizacion"] = qExpediente[0].NoAutorizacion;
                        mRowExpediente["Enganche"] = qExpediente[0].Enganche;
                        mRowExpediente["Plan"] = qExpediente[0].Plan;
                        mRowExpediente["NoSolicitud"] = qExpediente[0].NoSolicitud;
                        mRowExpediente["FechaHora"] = qExpediente[0].FechaHora;
                        mRowExpediente["CantidadPagos1"] = qExpediente[0].CantidadPagos1;
                        mRowExpediente["MontoPagos1"] = qExpediente[0].MontoPagos1;
                        mRowExpediente["CantidadPagos2"] = qExpediente[0].CantidadPagos2;
                        mRowExpediente["MontoPagos2"] = qExpediente[0].MontoPagos2;
                        mRowExpediente["Despacho"] = qExpediente[0].Despacho;
                        mRowExpediente["Radio"] = qExpediente[0].Radio;
                        mRowExpediente["Volante"] = qExpediente[0].Volante;
                        mRowExpediente["PrensaLibre"] = qExpediente[0].PrensaLibre;
                        mRowExpediente["ElQuetzalteco"] = qExpediente[0].ElQuetzalteco;
                        mRowExpediente["NuestroDiario"] = qExpediente[0].NuestroDiario;
                        mRowExpediente["Internet"] = qExpediente[0].Internet;
                        mRowExpediente["TV"] = qExpediente[0].TV;
                        mRowExpediente["Otros"] = qExpediente[0].Otros;
                        mRowExpediente["ObsOtros"] = qExpediente[0].ObsOtros;
                        mRowExpediente["AutorizacionEspecial"] = qExpediente[0].AutorizacionEspecial;
                        mRowExpediente["ObservacionesAdicionales"] = qExpediente[0].ObservacionesAdicionales;
                        mRowExpediente["IndicacionesLlegar"] = qExpediente[0].IndicacionesLlegar;
                        mRowExpediente["EntregaAMPM"] = qExpediente[0].EntregaAMPM;
                        mRowExpediente["FechaEntrega"] = qExpediente[0].FechaEntrega;
                        ds.Expediente.Rows.Add(mRowExpediente);

                        for (int ii = 0; ii < qExpedienteDet.Length; ii++)
                        {
                            DataRow mRowExpedienteDet = ds.ExpedienteDet.NewRow();
                            mRowExpedienteDet["Cliente"] = qExpedienteDet[ii].Cliente;
                            mRowExpedienteDet["Pedido"] = qExpedienteDet[ii].Pedido;
                            mRowExpedienteDet["Articulo"] = qExpedienteDet[ii].Articulo;
                            mRowExpedienteDet["Descripcion"] = qExpedienteDet[ii].Descripcion;
                            mRowExpedienteDet["PrecioLista"] = qExpedienteDet[ii].PrecioLista;
                            mRowExpedienteDet["Cantidad"] = qExpedienteDet[ii].Cantidad;
                            mRowExpedienteDet["Factor"] = qExpedienteDet[ii].Factor;
                            mRowExpedienteDet["PrecioTotal"] = qExpedienteDet[ii].PrecioTotal;
                            ds.ExpedienteDet.Rows.Add(mRowExpedienteDet);

                            if (qExpedienteDet[ii].TipoLinea == "K")
                            {
                                var qKit = ws.DevuelveKit(qExpedienteDet[ii].Articulo);

                                for (int jj = 0; jj < qKit.Length; jj++)
                                {
                                    DataRow mRowExpedienteDetKit = ds.ExpedienteDet.NewRow();
                                    mRowExpedienteDetKit["Cliente"] = qExpedienteDet[ii].Cliente;
                                    mRowExpedienteDetKit["Pedido"] = qExpedienteDet[ii].Pedido;
                                    mRowExpedienteDetKit["Articulo"] = qKit[jj].Articulo;
                                    mRowExpedienteDetKit["Descripcion"] = string.Format("     {0}", qKit[jj].Descripcion);
                                    mRowExpedienteDetKit["PrecioLista"] = 0;
                                    mRowExpedienteDetKit["Cantidad"] = qKit[jj].Cantidad;
                                    mRowExpedienteDetKit["Factor"] = 0;
                                    mRowExpedienteDetKit["PrecioTotal"] = 0;
                                    ds.ExpedienteDet.Rows.Add(mRowExpedienteDetKit);
                                }
                            }
                        }

                        int mRows = ds.ExpedienteDet.Rows.Count;
                        for (int ii = mRows; ii < 11; ii++)
                        {
                            DataRow mRowExpedienteDet = ds.ExpedienteDet.NewRow();
                            mRowExpedienteDet["Cliente"] = qExpediente[0].Cliente;
                            mRowExpedienteDet["Pedido"] = qExpediente[0].Pedido;
                            mRowExpedienteDet["Articulo"] = "";
                            mRowExpedienteDet["Descripcion"] = "";
                            mRowExpedienteDet["PrecioLista"] = 0;
                            mRowExpedienteDet["Cantidad"] = 0;
                            mRowExpedienteDet["Factor"] = 0;
                            mRowExpedienteDet["PrecioTotal"] = 0;
                            ds.ExpedienteDet.Rows.Add(mRowExpedienteDet);
                        }

                        using (ReportDocument ReporteExpediente = new ReportDocument())
                        {
                            string m = (Request.PhysicalApplicationPath + "reportes/rptExpediente.rpt");
                            ReporteExpediente.Load(m);

                            string mExpediente = string.Format("Expediente_{0}.pdf", mPedido);

                            ReporteExpediente.SetDataSource(ds);
                            ReporteExpediente.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mExpediente);

                            Response.Clear();
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mExpediente);
                            Response.WriteFile(@"C:\reportes\" + mExpediente);
                            Response.End();
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error en el pedido. {0} {1}", ex.Message, ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")));
            }
        }

        void SeleccionarPedido()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            Label lbFecha = (Label)gridPedidos.SelectedRow.FindControl("lbFecha");
            Label lbTotalFacturar = (Label)gridPedidos.SelectedRow.FindControl("lbTotalFacturar");
            Label lbEnganche = (Label)gridPedidos.SelectedRow.FindControl("lbEnganche");
            Label lbSaldoFinanciar = (Label)gridPedidos.SelectedRow.FindControl("lbSaldoFinanciar");
            Label lbRecargos = (Label)gridPedidos.SelectedRow.FindControl("lbRecargos");
            Label lbMonto = (Label)gridPedidos.SelectedRow.FindControl("lbMonto");
            Label lbCantidadPagos1 = (Label)gridPedidos.SelectedRow.FindControl("lbCantidadPagos1");
            Label lbMontoPagos1 = (Label)gridPedidos.SelectedRow.FindControl("lbMontoPagos1");
            Label lbCantidadPagos2 = (Label)gridPedidos.SelectedRow.FindControl("lbCantidadPagos2");
            Label lbMontoPagos2 = (Label)gridPedidos.SelectedRow.FindControl("lbMontoPagos2");
            Label lbPagare = (Label)gridPedidos.SelectedRow.FindControl("lbPagare");
            Label lbSolicitud = (Label)gridPedidos.SelectedRow.FindControl("lbSolicitud");
            Label lbNombreAutorizacion = (Label)gridPedidos.SelectedRow.FindControl("lbNombreAutorizacion");
            Label lbAutorizacion = (Label)gridPedidos.SelectedRow.FindControl("lbAutorizacion");
            Label lbObservaciones = (Label)gridPedidos.SelectedRow.FindControl("lbObservaciones");
            Label lbObservacionesTipoVenta = (Label)gridPedidos.SelectedRow.FindControl("lbObservacionesTipoVenta");
            Label lbTipoReferencia = (Label)gridPedidos.SelectedRow.FindControl("lbTipoReferencia");
            Label lbObservacionesReferencia = (Label)gridPedidos.SelectedRow.FindControl("lbObservacionesReferencia");
            Label lbGarantia = (Label)gridPedidos.SelectedRow.FindControl("lbGarantia");
            Label lbCotizacion = (Label)gridPedidos.SelectedRow.FindControl("lbCotizacion");
            Label lbTipoPedido = (Label)gridPedidos.SelectedRow.FindControl("lbTipoPedido");
            Label lbTienda = (Label)gridPedidos.SelectedRow.FindControl("lbTienda");
            Label lbVendedor = (Label)gridPedidos.SelectedRow.FindControl("lbVendedor");
            Label lbTipoVenta = (Label)gridPedidos.SelectedRow.FindControl("lbTipoVenta");
            Label lbFinanciera = (Label)gridPedidos.SelectedRow.FindControl("lbFinanciera");
            Label lbNivelPrecio = (Label)gridPedidos.SelectedRow.FindControl("lbNivelPrecio");
            Label lbFechaEntrega = (Label)gridPedidos.SelectedRow.FindControl("lbFechaEntrega");
            Label lbNombreRecibe = (Label)gridPedidos.SelectedRow.FindControl("lbNombreRecibe");
            Label lbEntregaAMPM = (Label)gridPedidos.SelectedRow.FindControl("lbEntregaAMPM");

            limpiar(true);
            tblBuscar.Visible = false;
            gridClientes.Visible = false;

            txtPedido.Text = gridPedidos.SelectedRow.Cells[7].Text;
            txtFecha.Text = lbFecha.Text;
            txtFacturar.Text = lbTotalFacturar.Text;
            txtEnganche.Text = lbEnganche.Text;
            txtSaldoFinanciar.Text = lbSaldoFinanciar.Text;
            txtIntereses.Text = lbRecargos.Text;
            txtMonto.Text = lbMonto.Text;
            cbPagos1.SelectedValue = lbCantidadPagos1.Text;
            txtPagos1.Text = Convert.ToDouble(lbMontoPagos1.Text.Replace(",", "")) > 0 ? String.Format("{0:0,0.00}", Convert.ToDouble(lbMontoPagos1.Text.Replace(",", ""))) : "0.00";
            cbPagos2.SelectedValue = lbCantidadPagos2.Text;
            txtPagos2.Text = Convert.ToDouble(lbMontoPagos2.Text.Replace(",", "")) > 0 ? String.Format("{0:0,0.00}", Convert.ToDouble(lbMontoPagos2.Text.Replace(",", ""))) : "0.00";
            txtPagare.Text = lbPagare.Text;
            txtSolicitud.Text = lbSolicitud.Text;
            txtQuienAutorizo.Text = lbNombreAutorizacion.Text;
            txtNoAutorizacion.Text = lbAutorizacion.Text;
            txtObservaciones.Text = lbObservaciones.Text;
            txtNotasTipoVenta.Text = lbObservacionesTipoVenta.Text;
            txtOtro.Text = lbObservacionesReferencia.Text;
            txtGarantia.Text = lbGarantia.Text;
            cbTipoPedido.SelectedValue = lbTipoPedido.Text;
            cbTienda.SelectedValue = lbTienda.Text;

            txtNombreRecibe.Text = lbNombreRecibe.Text;
            cbEntrega.SelectedValue = lbEntregaAMPM.Text;

            try
            {
                DateTime mFechaEntrega = Convert.ToDateTime(lbFechaEntrega.Text);

                cbDia.SelectedValue = mFechaEntrega.Day.ToString();
                cbMes.SelectedValue = mFechaEntrega.Month.ToString();
                txtAnio.Text = mFechaEntrega.Year.ToString();
            }
            catch
            {
                cbDia.SelectedValue = "--";
                cbMes.SelectedValue = "--";
                txtAnio.Text = "";
            }

            CargaVendedores();
            cbVendedor.SelectedValue = lbVendedor.Text;
            cbTipo.SelectedValue = lbTipoVenta.Text;
            cbFinanciera.SelectedValue = lbFinanciera.Text;
            CargaNivelesDePrecio();
            cbNivelPrecio.SelectedValue = ws.DevuelveNivelPadre(lbTipoVenta.Text, Convert.ToInt32(lbFinanciera.Text), lbNivelPrecio.Text);

            if (lbTipoReferencia.Text.Contains("R")) cbRadio.Checked = true;
            if (lbTipoReferencia.Text.Contains("I")) cbInternet.Checked = true;
            if (lbTipoReferencia.Text.Contains("V")) cbVolante.Checked = true;
            if (lbTipoReferencia.Text.Contains("P")) cbPrensa.Checked = true;
            if (lbTipoReferencia.Text.Contains("Q")) cbQuetzalteco.Checked = true;
            if (lbTipoReferencia.Text.Contains("N")) cbNuestroDiario.Checked = true;
            if (lbTipoReferencia.Text.Contains("T")) cbTelevision.Checked = true;
            if (lbTipoReferencia.Text.Contains("O")) cbOtro.Checked = true;

            try
            {
                txtCotizacion.Text = lbCotizacion.Text;
                if (txtCotizacion.Text.Trim().Length == 0) txtCotizacion.Text = "0";
            }
            catch
            {
                txtCotizacion.Text = "0";
            }

            var qq = ws.DevuelvePedidoLinea(txtPedido.Text);
            List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();
            ViewState["qArticulos"] = q;

            for (int ii = 0; ii < qq.Length; ii++)
            {
                wsPuntoVenta.PedidoLinea item = new wsPuntoVenta.PedidoLinea();
                item.Articulo = qq[ii].Articulo;
                item.Nombre = qq[ii].Descripcion.ToUpper();
                item.PrecioUnitario = qq[ii].PrecioUnitario;
                item.CantidadPedida = qq[ii].CantidadPedida;
                item.PrecioTotal = qq[ii].PrecioTotal;
                item.Bodega = qq[ii].Bodega;
                item.Localizacion = qq[ii].Localizacion;
                item.RequisicionArmado = qq[ii].RequisicionArmado;
                item.Descripcion = qq[ii].Descripcion;
                item.Comentario = qq[ii].Comentario;
                item.Estado = qq[ii].Estado;
                item.NumeroPedido = qq[ii].NumeroPedido;
                item.Oferta = qq[ii].Oferta;
                item.FechaOfertaDesde = qq[ii].FechaOfertaDesde;
                item.PrecioOriginal = qq[ii].PrecioOriginal;
                item.TipoOferta = qq[ii].TipoOferta;
                item.EsDetalleKit = qq[ii].EsDetalleKit;
                item.PrecioFacturar = qq[ii].PrecioFacturar;
                item.Vale = qq[ii].Vale;
                item.Autorizacion = qq[ii].Autorizacion;
                q.Add(item);
            }

            gridArticulos.DataSource = q;
            gridArticulos.DataBind();
            gridArticulos.Visible = true;
            gridArticulos.SelectedIndex = -1;

            ViewState["qArticulos"] = q;
        }

        protected void gridPedidos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        bool ValidarSolicitud(bool ValidarArticulo)
        {
            if (txtCodigo.Text.Trim().Length == 0)
            {
                lbError2.Visible = true;
                lbError2.Text = "Debe seleccionar un cliente.";
                mostrarBusquedaCliente();
                return false;
            }
            if (cbVendedor.Text.Trim().Length == 0)
            {
                lbError2.Visible = true;
                lbError2.Text = "Debe seleccionar el vendedor.";
                cbVendedor.Focus();
                return false;
            }
            if (articulo.Text.Trim().Length == 0 && ValidarArticulo)
            {
                lbError2.Visible = true;
                lbError2.Text = "Debe seleccionar un artículo para solicitar autorización";
                articulo.Focus();
                return false;
            }
            if (txtOferta.Text == "S")
            {
                lbError2.Visible = true;
                lbError2.Text = "Este artículo está en oferta, no es posible solicitar descuento especial";
                articulo.Focus();
                return false;
            }

            return true;
        }

        protected void lkSolicitar_Click(object sender, EventArgs e)
        {
            MostrarInfoSolicitud();
        }

        void MostrarInfoSolicitud()
        {
            if (cbTipoPedido.SelectedValue != "N")
            {
                lbError2.Visible = true;
                lbError2.Text = "En este tipo de pedidos no es posible realizar solicitudes especiales.";
                return;
            }

            LimpiarCamposSolicitud();

            TextoSolicitud.InnerText = "Solicitud especial";
            if (articulo.Text.Trim().Length > 0) TextoSolicitud.InnerText = string.Format("Solicitud especial para el artículo  {0} - {1}.", articulo.Text, descripcion.Text);

            lbAutorizada.Visible = false;
            lbRechazada.Visible = false;
            lbPendiente.Visible = true;

            tblSolicutudAutorizacion.Visible = true;
            txtPrecioSolicitar.Focus();
        }

        void LimpiarCamposSolicitud()
        {
            txtPrecioSolicitar.Text = "";
            txtObservacionesSolicitarAut.Text = "";
            txtGerente.Text = "";
            txtGerenteComentario.Text = "";
            txtPrecioAutorizado.Text = "";
            txtAut.Text = "";

            txtPrecioSolicitar.ReadOnly = false;
            txtObservacionesSolicitarAut.ReadOnly = false;

            lbError2.Text = "";
            lbError2.Visible = false;
            lbSolicitudEnviada.Visible = false;
        }

        protected void lkEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtAut.Text.Length > 0)
                {
                    lbError2.Visible = true;
                    lbError2.Text = "Esta solicitud está en tránsito, no es posible enviar nada en este momento";
                    return;
                }

                if (!ValidarSolicitud(true)) return;

                decimal mPrecioSolicitado = 0; decimal mPrecioUnitario = 0;

                try
                {
                    mPrecioSolicitado = Convert.ToDecimal(txtPrecioSolicitar.Text.Replace(",", ""));
                }
                catch
                {
                    lbError2.Visible = true;
                    lbError2.Text = "El precio a solicitar es inválido";
                    txtPrecioSolicitar.Focus();
                    return;
                }
                try
                {
                    mPrecioUnitario = Convert.ToDecimal(txtPrecioUnitario.Text.Replace(",", ""));
                }
                catch
                {
                    lbError2.Visible = true;
                    lbError2.Text = "El precio unitario del artículo es inválido";
                    articulo.Focus();
                    return;
                }

                if (mPrecioSolicitado <= 0)
                {
                    lbError2.Visible = true;
                    lbError2.Text = "El precio a solicitar es inválido";
                    txtPrecioSolicitar.Focus();
                    return;
                }
                if (mPrecioSolicitado > mPrecioUnitario)
                {
                    lbError2.Visible = true;
                    lbError2.Text = "El precio a solicitar es mayor al precio unitario, no es posible continuar";
                    txtPrecioSolicitar.Focus();
                    return;
                }

                if (txtObservacionesSolicitarAut.Text.Trim().Length == 0)
                {
                    lbError2.Visible = true;
                    lbError2.Text = "Debe ingresar las observaciones para la solicitud";
                    txtObservacionesSolicitarAut.Focus();
                    return;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mMensaje = ""; Int64 mAutorizacion = 0;
                wsPuntoVenta.Autorizaciones[] q = new wsPuntoVenta.Autorizaciones[1];

                wsPuntoVenta.Autorizaciones item = new wsPuntoVenta.Autorizaciones();
                item.Autorizacion = 0;
                item.Fecha = DateTime.Now;
                item.Cliente = txtCodigo.Text;
                item.Tipo = "P";
                item.ClienteNombre = txtNombre.Text;
                item.Articulo = articulo.Text;
                item.ArticuloNombre = descripcion.Text;
                item.TipoVenta = cbTipo.SelectedValue;
                item.Financiera = cbFinanciera.SelectedValue;
                item.NivelPrecio = cbNivelPrecio.SelectedValue;
                item.TipoVentaNombre = "";
                item.FinancieraNombre = "";
                item.Precio = Convert.ToDecimal(txtPrecioUnitario.Text.Replace(",", ""));
                item.PrecioLista = 0;
                item.PrecioSolicitado = mPrecioSolicitado;
                item.Cantidad = Convert.ToInt32(txtCantidad.Text.Replace(",", ""));
                item.Diferencia = 0;
                item.Observaciones = txtObservacionesSolicitarAut.Text.Trim();
                item.Tienda = cbTienda.SelectedValue;
                item.Vendedor = cbVendedor.SelectedValue;
                item.VendedorNombre = "";
                item.Gerente = "";
                item.Comentario = "";
                item.PrecioAutorizado = 0;
                item.Costo = 0;
                item.PrecioContado = 0;
                item.PctjDescuento = 0;
                item.Margen = 0;
                item.MargenAutorizar = 0;
                item.Factor = 0;
                item.FechaVence = DateTime.Now;
                item.Estatus = "";
                item.EstatusDescripcion = "";
                item.Estado = "";
                item.EstadoDescripcion = "";
                q[0] = item;

                if (!ws.GrabarSolicitudAutorizacion(ref mAutorizacion, ref mMensaje, q, Convert.ToString(Session["Usuario"])))
                {
                    lbError2.Visible = true;
                    lbError2.Text = mMensaje;
                    return;
                }

                lbError2.Visible = false;
                lbError2.Text = "";
                lbSolicitudEnviada.Visible = true;
            }
            catch (Exception ex)
            {
                lbError2.Visible = true;
                lbError2.Text = string.Format("Error al enviar la solicitud {0}.", ex.Message);
            }
        }

        protected void lkVerificar_Click(object sender, EventArgs e)
        {
            VerificarSolicitud();
        }

        void VerificarSolicitud()
        {
            if (!ValidarSolicitud(false)) return;

            LimpiarCamposSolicitud();

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveAutorizacionesCliente(txtCodigo.Text, cbVendedor.SelectedValue);

            if (q.Length == 0)
            {
                lbError2.Visible = true;
                lbError2.Text = "El cliente no tiene solicitudes pendientes";
                TextoSolicitud.InnerText = "Solicitud especial";
            }
            else
            {
                txtPrecioSolicitar.Text = String.Format("{0:0,0.00}", q[0].PrecioSolicitado);
                txtObservacionesSolicitarAut.Text = q[0].Observaciones;
                txtGerente.Text = q[0].Gerente;
                txtPrecioAutorizado.Text = q[0].Estatus == "A" ? String.Format("{0:0,0.00}", q[0].PrecioAutorizado) : "";
                txtGerenteComentario.Text = q[0].Comentario;
                txtAut.Text = q[0].Autorizacion.ToString();

                lbAutorizada.Visible = false;
                lbRechazada.Visible = false;
                lbPendiente.Visible = false;

                txtPrecioSolicitar.ReadOnly = true;
                txtObservacionesSolicitarAut.ReadOnly = true;

                if (q[0].Estatus == "P") lbPendiente.Visible = true;
                if (q[0].Estatus == "A") lbAutorizada.Visible = true;
                if (q[0].Estatus == "R") lbRechazada.Visible = true;

                TextoSolicitud.InnerText = string.Format("Solicitud especial para el artículo  {0} - {1}.", q[0].Articulo, q[0].ArticuloNombre);
            }
        }

        protected void lkAplicar_Click(object sender, EventArgs e)
        {
            AplicarSolicitud();
        }

        void AplicarSolicitud()
        {
            try
            {
                lbError2.Text = "";
                lbError2.Visible = false;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (txtAut.Text == "")
                {
                    lbError2.Visible = true;
                    lbError2.Text = "No hay ninguna solicitud para aplicar";
                    return;
                }
                if (!lbAutorizada.Visible)
                {
                    lbError2.Visible = true;
                    lbError2.Text = "La solicitud no está autorizada, no es posible aplicarla";
                    return;
                }

                var q = ws.DevuelveAutorizacion(txtAut.Text);

                if (q[0].Cliente != txtCodigo.Text)
                {
                    lbError2.Visible = true;
                    lbError2.Text = string.Format("Esta solicitud pertenece al cliente {0}", q[0].Cliente);
                    return;
                }

                if (DateTime.Now.Date > q[0].FechaVence)
                {
                    lbError2.Visible = true;
                    lbError2.Text = string.Format("La autorización de esta solicitud venció el {0}", q[0].FechaVence.ToShortDateString());
                    return;
                }

                if (cbTipo.SelectedValue != q[0].TipoVenta || cbFinanciera.SelectedValue != q[0].Financiera || cbNivelPrecio.SelectedValue != q[0].NivelPrecio)
                {
                    cbTipo.SelectedValue = q[0].TipoVenta;
                    cbFinanciera.SelectedValue = q[0].Financiera;

                    CargaNivelesDePrecio();
                    cbNivelPrecio.SelectedValue = ws.DevuelveNivelPadre(q[0].TipoVenta, Convert.ToInt32(q[0].Financiera), q[0].NivelPrecio);

                    List<wsPuntoVenta.PedidoLinea> qq = new List<wsPuntoVenta.PedidoLinea>();
                    ViewState["qArticulos"] = qq;

                    gridArticulos.DataSource = qq;
                    gridArticulos.DataBind();
                }

                txtAutorizacion.Text = txtAut.Text;
                articulo.Text = q[0].Articulo;
                codigoArticuloDet.Text = q[0].Articulo;

                BuscarArticulo("C");
                gridArticulosDet.SelectedIndex = 0;

                descripcion.Text = q[0].ArticuloNombre;
                txtCantidad.Text = q[0].Cantidad.ToString();
                txtPrecioUnitario.Text = String.Format("{0:0,0.00}", q[0].PrecioAutorizadoNivel);
                txtTotal.Text = String.Format("{0:0,0.00}", Convert.ToDecimal(txtPrecioUnitario.Text.Replace(",", "")) * Convert.ToInt32(txtCantidad.Text.Replace(",", "")));

                tablaBusquedaDet.Visible = false;
                gridArticulosDet.Visible = false;

                txtCantidad.ReadOnly = true;
                tblSolicutudAutorizacion.Visible = false;
            }
            catch (Exception ex)
            {
                lbError2.Visible = true;
                lbError2.Text = string.Format("Error al aplicar la solicitud. {0}", ex.Message);
            }
        }

        protected void lkOcultarSolicitudAutorizacion_Click(object sender, EventArgs e)
        {
            lbError2.Text = "";
            lbError2.Visible = false;
            tblSolicutudAutorizacion.Visible = false;
        }

        protected void lkFacturarPedido_Click(object sender, EventArgs e)
        {
            ViewState["AccionPedido"] = "Facturar";
        }

        protected void lkDespacharPedido_Click(object sender, EventArgs e)
        {
            ViewState["AccionPedido"] = "Despachar";
        }

        protected void lkPedidosCliente_Click(object sender, EventArgs e)
        {
            if (txtCodigo.Text.Trim().Length == 0)
            {
                lbError2.Visible = true;
                lbError.Text = "Debe seleccionar un cliente";
                return;
            }

            lbError2.Text = "";
            lbError2.Visible = false;

            tblFactura.Visible = false;
            tblDespacho.Visible = false;
            tblSolicutudAutorizacion.Visible = false;
            tablaBusquedaDet.Visible = false;
            gridArticulosDet.Visible = false;

            CargarPedidosCliente();
        }

        protected void lkEliminarPedido_Click(object sender, EventArgs e)
        {
            ViewState["AccionPedido"] = "Eliminar";
        }

        protected void lkSeleccionarPedido_Click(object sender, EventArgs e)
        {
            ViewState["AccionPedido"] = "Seleccionar";
        }

        protected void lkSolicitudCredito_Click(object sender, EventArgs e)
        {
            ViewState["AccionPedido"] = "Solicitud";
        }

        protected void lkPagare_Click(object sender, EventArgs e)
        {
            ViewState["AccionPedido"] = "Pagaré";
        }

        protected void lkExpediente_Click(object sender, EventArgs e)
        {
            ViewState["AccionPedido"] = "Expediente";
        }

        void MostrarConsultaCotizaciones(bool mostrar)
        {
            tblCotizaciones.Visible = mostrar;
            gridCotizaciones.Visible = mostrar;

            DateTime mFechaInicial = DateTime.Now.Date.AddMonths(-1);
            mFechaInicial = new DateTime(mFechaInicial.Year, mFechaInicial.Month, 1);

            cbDiaIni.SelectedValue = mFechaInicial.Day.ToString();
            cbMesIni.SelectedValue = mFechaInicial.Month.ToString();
            txtAnioIni.Text = mFechaInicial.Year.ToString();

            cbDiaFin.SelectedValue = DateTime.Now.Day.ToString();
            cbMesFin.SelectedValue = DateTime.Now.Month.ToString();
            txtAnioFin.Text = DateTime.Now.Year.ToString();
            
            CargarCotizaciones(mostrar);
        }

        protected void lkCotizaciones_Click(object sender, EventArgs e)
        {
            CargarCotizaciones(true);
        }

        void CargarCotizaciones(bool mostrar)
        {
            try
            {
                lbInfoCotizaciones.Text = "";
                lbErrorCotizaciones.Text = "";
                
                DateTime mFechaIni = new DateTime(Convert.ToInt32(txtAnioIni.Text), Convert.ToInt32(cbMesIni.SelectedValue), Convert.ToInt32(cbDiaIni.SelectedValue));
                DateTime mFechaFin = new DateTime(Convert.ToInt32(txtAnioFin.Text), Convert.ToInt32(cbMesFin.SelectedValue), Convert.ToInt32(cbDiaFin.SelectedValue));

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var q = ws.DevuelveCotizaciones(mFechaIni, mFechaFin, Convert.ToString(Session["Vendedor"]));
                if (!mostrar) q = ws.DevuelveCotizacion(txtPedido.Text);

                if (q.Length == 0)
                {
                    gridCotizaciones.Visible = false;
                    lbInfoCotizaciones.Text = "No se encontraron cotizaciones";
                }
                else
                {
                    gridCotizaciones.DataSource = q;
                    gridCotizaciones.DataBind();
                    gridCotizaciones.Visible = true;
                    
                    lbInfoCotizaciones.Text = string.Format("Se encontraron {0} cotizaciones", q.Length);

                    gridCotizaciones.Focus();
                    gridCotizaciones.SelectedIndex = 0;
                    LinkButton lkSeleccionar = (LinkButton)gridCotizaciones.SelectedRow.FindControl("lkSeleccionarCotizacion");

                    lkSeleccionar.Focus();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorCotizaciones.Text = string.Format("Error al cargar las cotizaciones {0} {1}", ex.Message, m);
            }
        }

        protected void lkEliminarCotizacion_Click(object sender, EventArgs e)
        {
            ViewState["AccionCotizacion"] = "Eliminar";
        }

        protected void lkSeleccionarCotizacion_Click(object sender, EventArgs e)
        {
            ViewState["AccionCotizacion"] = "Seleccionar";
        }

        protected void lkAsignar_Click(object sender, EventArgs e)
        {
            ViewState["AccionCotizacion"] = "Asignar";
        }

        protected void lkImprimirCotizacion_Click(object sender, EventArgs e)
        {
            ViewState["AccionCotizacion"] = "Imprimir";
        }

        protected void lkEnviarCotizacion_Click(object sender, EventArgs e)
        {
            ViewState["AccionCotizacion"] = "Enviar";
        }

        protected void lkInterconsumo_Click(object sender, EventArgs e)
        {
            ViewState["AccionCotizacion"] = "Interconsumo";
        }

        protected void lkSolicitudCotizacion_Click(object sender, EventArgs e)
        {
            ViewState["AccionCotizacion"] = "Solicitud";
        }

        protected void lkPagareCotizacion_Click(object sender, EventArgs e)
        {
            ViewState["AccionCotizacion"] = "Pagaré";
        }

        protected void lkSolicitarCotizacion_Click(object sender, EventArgs e)
        {
            ViewState["AccionCotizacion"] = "Solicitar";
        }

        protected void gridCotizaciones_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");

                Label lbObservaciones = (Label)e.Row.FindControl("lbObservaciones");
                Label lbCotizacion = (Label)e.Row.FindControl("lbCotizacion");
                Label lbEmail = (Label)e.Row.FindControl("lbNombreRecibe");
                LinkButton lkEnviar = (LinkButton)e.Row.FindControl("lkEnviarCotizacion");

                lbCotizacion.ToolTip = string.Format("Observaciones:{0}{1}", System.Environment.NewLine, lbObservaciones.Text);
                lkEnviar.ToolTip = string.Format("Haga clic aquí para enviar la cotización a la dirección {0}", lbEmail.Text);
            }
        }

        protected void gridCotizaciones_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string mMensaje = "";
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                lbInfo.Text = "";
                lbError.Text = "";

                int mRow = gridCotizaciones.SelectedIndex;
                Label lbCotizacion = (Label)gridCotizaciones.SelectedRow.FindControl("lbCotizacion");
                Label lbFinanciera = (Label)gridCotizaciones.SelectedRow.FindControl("lbFinanciera");
                Label lbCliente = (Label)gridCotizaciones.SelectedRow.FindControl("lbCliente");
                Label lbTipoVenta = (Label)gridCotizaciones.SelectedRow.FindControl("lbTipoVenta");
                Label lbNivelPrecio = (Label)gridCotizaciones.SelectedRow.FindControl("lbNivelPrecio");

                Session["Cliente"] = lbCliente.Text;
                string mCotizacion = lbCotizacion.Text;

                switch (ViewState["AccionCotizacion"].ToString())
                {
                    case "Eliminar":
                        if (!ws.EliminarCotizacion(ref mMensaje, mCotizacion))
                        {
                            lbError.Text = mMensaje;
                            return;
                        }

                        lbInfo.Text = mMensaje;
                        CargarCotizaciones(true);
                        break;
                    case "Seleccionar":
                        SeleccionarCotizacion();
                        break;
                    case "Asignar":
                        AsignarCliente(lbCotizacion.Text);
                        break;
                    case "Imprimir":
                        ImprimirCotizacion(false);
                        break;
                    case "Enviar":
                        ImprimirCotizacion(true);
                        break;
                    case "Interconsumo":
                        WebRequest request = WebRequest.Create(string.Format("{0}/precios/?cotizacion={1}", Convert.ToString(Session["UrlRestServices"]), mCotizacion));

                        request.Method = "GET";
                        request.ContentType = "application/json";

                        var response = (HttpWebResponse)request.GetResponse();
                        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                        Clases.Precios mRetorna = new Clases.Precios();
                        mRetorna = JsonConvert.DeserializeObject<Clases.Precios>(responseString);

                        if (!mRetorna.RetornaExito[0].exito)
                        {
                            lbError.Text = mRetorna.RetornaExito[0].mensaje;
                            return;
                        }

                        limpiar(true);
                        CargarCotizaciones(true);
                        lbInfo.Text = mRetorna.RetornaExito[0].mensaje;

                        break;
                    case "Solicitud":
                        ImprimirSolicitudCotizacion(mCotizacion, lbFinanciera.Text, lbCliente.Text, lbTipoVenta.Text, lbNivelPrecio.Text);
                        break;
                    case "Pagaré":
                        ImprimirPagareCotizacion(mCotizacion, lbFinanciera.Text, lbCliente.Text, lbTipoVenta.Text, lbNivelPrecio.Text);
                        break;
                    case "Solicitar":
                        MostrarSolicitud();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error en la cotización. {0} {1}", ex.Message, m);
            }
        }

        void ImprimirPagareCotizacion(string cotizacion, string financiera, string cliente, string tipoVenta, string nivel)
        {
            lbError.Text = "";
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            if (ws.EsBanco(financiera))
            {
                try
                {
                    Int32 mCliente = Convert.ToInt32(cliente);
                }
                catch
                {
                    lbInfoCotizaciones.Text = "";
                    lbError.Text = "Debe asignarle cliente a esta cotización";
                    lbErrorCotizaciones.Text = "Debe asignarle cliente a esta cotización";
                    return;
                }

                if (ws.DevuelveProfesion(cliente).Trim().Length == 0)
                {
                    Session["TipoVenta"] = tipoVenta;
                    Session["Financiera"] = financiera;
                    Session["NivelPrecio"] = nivel;
                    Session["Cliente"] = cliente;
                    Response.Redirect("clientes.aspx");
                    return;
                }

                if (financiera == "12")
                {
                    try
                    {
                        lbInfo.Text = "";
                        lbError.Text = "";
                        Response.Redirect(string.Format("{0}/Report/PagareAtid?id={1}", WebConfigurationManager.AppSettings["ReportServer"], cotizacion), false);
                    }
                    catch (Exception ex)
                    {
                        lbError.Text = CatchClass.ExMessage(ex, "pedidos", "ImprimirSolicitud");
                    }

                    return;
                }


                if (financiera == Const.FINANCIERA.INTERCONSUMO)
                {
                    //----------------------------------------------------------------
                    //Aplicamos las reglas de negocio para poder imprimir el pagare.
                    //----------------------------------------------------------------
                    WebRequest request = WebRequest.Create(string.Format("{0}/RN001DocumentoInterconsumo/?NumeroDocumento={1}", Convert.ToString(Session["UrlRestServices"]), cotizacion));
                    request.Method = "GET";
                    request.ContentType = "application/json";
                    var response = (HttpWebResponse)request.GetResponse();
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    //----------------------------------------------------------------
                    //Obtenemos las respuesta de las validaciones del pagaré.
                    //----------------------------------------------------------------
                    MF_Clases.Respuesta mRespuesta = JsonConvert.DeserializeObject<MF_Clases.Respuesta>(responseString);
                    if (!mRespuesta.Exito)
                    {
                        lbError.Text = mRespuesta.Mensaje;
                        return;
                    }

                    //----------------------------------------------------------------
                    //Procedemos a imprimir el pagaré.
                    //----------------------------------------------------------------
                    MF_Clases.Clases.Pedido mCotizacionInterconsumo = JsonConvert.DeserializeObject<MF_Clases.Clases.Pedido>(mRespuesta.Objeto.ToString());
                    Response.Redirect(string.Format("{0}/DocumentoPdfInterconsumo/?Tipo={1}&NumeroReferencia={2}", Convert.ToString(Session["UrlRestServices"]), Const.DocumentoInterconsumo.PAGARE, mCotizacionInterconsumo.SOLICITUD));
                }
                else if (financiera == Const.FINANCIERA.BANCREDIT)
                {
                    var q = ws.DevuelveSolicitudCreditoCotizacion("C", cotizacion);
                    using (ReportDocument reporte = new ReportDocument())
                    {

                        string p = (Request.PhysicalApplicationPath + "reportes/PagareBancredit.rpt");
                        reporte.Load(p);

                        string mNombreDocumento = string.Format("Pagare_{0}.pdf", cotizacion);
                        Int32 mMesesFin = q[0].Colonia == "3 PAGOS" ? 3 : q[0].Colonia == "6 PAGOS" ? 6 : q[0].Colonia == "9 PAGOS" ? 9 : q[0].Colonia == "12 PAGOS" ? 12 : q[0].Colonia == "18 PAGOS" ? 18 : q[0].Colonia == "24 PAGOS" ? 24 : 36;
                        Cast mCast = new Cast();
                        int edad = DateTime.Today.AddTicks(-(q[0].FechaNacimiento ?? DateTime.Now).Ticks).Year - 1;
                        DateTime nacimiento = q[0].FechaNacimiento ?? DateTime.Now;
                        DateTime mFechaPedido = q[0].FechaPedido ?? DateTime.Now;
                        Bancredit mFechasBancredit = new Bancredit();

                        Cast mOrdinalCast = new Cast();

                        mOrdinalCast.NumeroLetras._Ordinal = true;

                        Numalet mNumerosLetras = new Numalet();
                        mNumerosLetras.ApocoparUnoParteEntera = true;

                        Numalet mNumerosLetrasOrdinal = new Numalet(true);
                        mNumerosLetrasOrdinal.ApocoparUnoParteEntera = true;


                        reporte.SetParameterValue("Nombre", string.Format("{0} {1}{2}{3} {4} {5} {6}", q[0].PrimerNombre, q[0].SegundoNombre, q[0].TercerNombre.Trim().Length == 0 ? "" : " ", q[0].TercerNombre, q[0].PrimerApellido, q[0].SegundoApellido, q[0].ApellidoCasada));
                        reporte.SetParameterValue("Edad", mCast.ConvertirNumLetra(edad.ToString(), true));
                        reporte.SetParameterValue("EstadoCivil", q[0].EstadoCivil.ToUpper());
                        reporte.SetParameterValue("Profesion", q[0].Profesion);
                        reporte.SetParameterValue("Nacionalidad", q[0].Nacionalidad);
                        reporte.SetParameterValue("DPILetras", mCast.ConvertirDPILetra(q[0].DPI, true));
                        reporte.SetParameterValue("Departamento", q[0].DepartamentoFacturacion);
                        reporte.SetParameterValue("Municipio", q[0].MunicipioFacturacion);

                        string s = q[0].Monto.ToString("0.00", CultureInfo.InvariantCulture);

                        string[] parts = s.Split('.');
                        int i1 = int.Parse(parts[0]);
                        int i2 = int.Parse(parts[1]);

                        if (i2 > 0)
                            reporte.SetParameterValue("MontoTotalLetras", 
                                //mCast.ConvertirNumLetra(i1.ToString(), true)
                                mNumerosLetras.ToCustomLetter(i1).ToUpper()
                                + " QUETZALES CON " + 
                                //mCast.ConvertirNumLetra(i2.ToString(), true)
                                mNumerosLetras.ToCustomLetter(i2).ToUpper()
                                + " CENTAVOS");
                        else reporte.SetParameterValue("MontoTotalLetras",
                            //mCast.ConvertirNumLetra(i1.ToString(), true) 
                            mNumerosLetras.ToCustomLetter(i1).ToUpper()
                            + " QUETZALES");

                        s = q[0].MontoPagos1.ToString("0.00", CultureInfo.InvariantCulture);
                        parts = s.Split('.');
                        i1 = int.Parse(parts[0]);
                        i2 = int.Parse(parts[1]);

                        if (i2 > 0)
                            reporte.SetParameterValue("MontoCuotaLetras",
                                //mCast.ConvertirNumLetra(i1.ToString(), true) 
                                mNumerosLetras.ToCustomLetter(i1).ToUpper()
                                + " QUETZALES CON " +
                                //mCast.ConvertirNumLetra(i2.ToString(), true) 
                                mNumerosLetras.ToCustomLetter(i2).ToUpper()
                                + " CENTAVOS");
                        else reporte.SetParameterValue("MontoCuotaLetras", 
                            //mCast.ConvertirNumLetra(i1.ToString(), true)
                            mNumerosLetras.ToCustomLetter(i1).ToUpper()
                            + " QUETZALES");

                        reporte.SetParameterValue("CantidadCuotasLetras",
                             //mCast.ConvertirNumLetra(q[0].CantidadPagos1.ToString(), true)
                             mNumerosLetras.ToCustomLetter(q[0].CantidadPagos1).ToUpper()
                            );

                        s = q[0].MontoPagos2.ToString("0.00", CultureInfo.InvariantCulture);

                        parts = s.Split('.');
                        i1 = int.Parse(parts[0]);
                        i2 = int.Parse(parts[1]);

                        if (i2 > 0)
                            reporte.SetParameterValue("MontoUltimaCuotaLetras",
                                //mCast.ConvertirNumLetra(i1.ToString(), true) 
                                mNumerosLetras.ToCustomLetter(i1).ToUpper()
                                + " QUETZALES CON " +
                                //mCast.ConvertirNumLetra(i2.ToString(), true) 
                                mNumerosLetras.ToCustomLetter(i2).ToUpper()
                                + " CENTAVOS");
                        else reporte.SetParameterValue("MontoUltimaCuotaLetras", 
                            ///mCast.ConvertirNumLetra(i1.ToString(), true)
                            mNumerosLetras.ToCustomLetter(i1).ToUpper()
                            + " QUETZALES");

                        reporte.SetParameterValue("FechaFinLetras", mCast.ConvertirFechaLetra(
                            mFechasBancredit.getUltimaFecha(q[0].FechaPedido ?? DateTime.Now, (q[0].CantidadPagos1 + 1))

                            )

                            );
                        reporte.SetParameterValue("FechaPrimerPagoLetras",
                            mCast.ConvertirFechaLetra(mFechasBancredit.getFechaPrimerPago(q[0].FechaPedido ?? DateTime.Now)));

                        reporte.SetParameterValue("FechaSegundoPagoMesLetras", mCast.ConvertirMesLetra(mFechasBancredit.getFechaPrimerPago(q[0].FechaPedido ?? DateTime.Now).Month));
                        reporte.SetParameterValue("FechaCorteDiaLetras", mCast.ConvertirNumLetra(mFechasBancredit.getFechaCorte(q[0].FechaPedido ?? DateTime.Now).ToString(), true));


                        reporte.SetParameterValue("Domicilio", mCast.ConvertirDireccionLetras(
                            string.Format("{0} {1}{2}{3} {4} {5}, {6}",
                            mCast.ConvertirNumeroDireccionLetras(q[0].CalleAvenidaFacturacion.Replace("AV.", "AVENIDA"), true)
                            , q[0].CasaNumeroFacturacion, q[0].ZonaFacturacion.Replace("Z.", "ZONA ")
                            , q[0].ApartamentoFacturacion
                            , q[0].ColoniaFacturacion, q[0].MunicipioFacturacion, q[0].DepartamentoFacturacion).ToUpper())

                            );

                        reporte.SetParameterValue("FechaPedidoDiaLetras", mCast.ConvertirNumLetra(mFechaPedido.Day.ToString(), true));
                        reporte.SetParameterValue("FechaPedidoMesLetras", mCast.ConvertirMesLetra(mFechaPedido.Month));
                        reporte.SetParameterValue("FechaPedidoAnoLetras", mCast.ConvertirNumLetra(mFechaPedido.Year.ToString(), true));



                        if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                        if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                        reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                        Response.Clear();
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                        Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                        Response.End();


                    }
                }

            }
            else
            {
                lbError.Text = "Esta opción solo aplica para un banco";
            }
        }

        void ImprimirSolicitudCotizacion(string cotizacion, string financiera, string cliente, string tipoVenta, string nivel)
        {
            lbError.Text = "";
            lbErrorCotizaciones.Text = "";

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            if (ws.EsBanco(financiera))
            {
                try
                {
                    Int32 mCliente = Convert.ToInt32(cliente);
                }
                catch
                {
                    lbInfoCotizaciones.Text = "";
                    lbError.Text = "Debe asignarle cliente a esta cotización";
                    lbErrorCotizaciones.Text = "Debe asignarle cliente a esta cotización";
                    return;
                }

                if (!ws.ValidaDatosCrediticios("C", cotizacion))
                {
                    Session["TipoVenta"] = tipoVenta;
                    Session["Financiera"] = financiera;
                    Session["NivelPrecio"] = nivel;
                    Session["Cliente"] = cliente;
                    Response.Redirect("clientes.aspx");
                }

                if (financiera == "12")
                {
                    try
                    {
                        lbInfo.Text = "";
                        lbError.Text = "";
                        Response.Redirect(string.Format("{0}/Report/SolicitudAtid?id={1}", WebConfigurationManager.AppSettings["ReportServer"], cotizacion), false);
                    }
                    catch (Exception ex)
                    {
                        lbError.Text = CatchClass.ExMessage(ex, "pedidos", "ImprimirSolicitud");
                    }

                    return;
                }

                var q = ws.DevuelveSolicitudCreditoCotizacion("S", cotizacion);

                using (ReportDocument reporte = new ReportDocument())
                {
                    WebRequest request = WebRequest.Create(string.Format("{0}/SolicitudPagare/{1}", Convert.ToString(Session["UrlRestServices"]), financiera));

                    request.Method = "GET";
                    request.ContentType = "application/json";

                    var response = (HttpWebResponse)request.GetResponse();
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    string[] mReportes = Convert.ToString(responseString).Replace("\"", "").Split(new string[] { "," }, StringSplitOptions.None);

                    string p = (Request.PhysicalApplicationPath + string.Format("reportes/{0}", mReportes[0]));
                    reporte.Load(p);

                    string mNombreDocumento = string.Format("Solicitud_{0}.pdf", q[0].Departamento);

                    reporte.SetParameterValue("PrimerNombre", q[0].PrimerNombre);
                    reporte.SetParameterValue("SegundoNombre", q[0].SegundoNombre);
                    reporte.SetParameterValue("TercerNombre", q[0].TercerNombre);
                    reporte.SetParameterValue("PrimerApellido", q[0].PrimerApellido);
                    reporte.SetParameterValue("SegundoApellido", q[0].SegundoApellido);
                    reporte.SetParameterValue("ApellidoCasada", q[0].ApellidoCasada);
                    reporte.SetParameterValue("DPI", q[0].DPI);
                    reporte.SetParameterValue("FechaNacimiento", q[0].FechaNacimiento);
                    reporte.SetParameterValue("EstadoCivil", q[0].EstadoCivil);
                    reporte.SetParameterValue("Genero", q[0].Genero);
                    reporte.SetParameterValue("Nacionalidad", q[0].Nacionalidad);
                    reporte.SetParameterValue("Profesion", q[0].Profesion);
                    reporte.SetParameterValue("NIT", q[0].Nit);
                    reporte.SetParameterValue("Direccion", string.Format("{0} {1} {2} {3} {4}", q[0].CalleAvenidaFacturacion, q[0].CasaNumeroFacturacion, q[0].ZonaFacturacion == "" || q[0].ZonaFacturacion == "00" ? "" : string.Format("Zona {0}", q[0].ZonaFacturacion), q[0].ApartamentoFacturacion, q[0].ColoniaFacturacion));
                    reporte.SetParameterValue("Municipio", q[0].MunicipioFacturacion);
                    reporte.SetParameterValue("Politico", q[0].Politico);
                    reporte.SetParameterValue("Departamento", q[0].DepartamentoFacturacion);
                    reporte.SetParameterValue("TelefonoResidencia", q[0].Telefono1);
                    reporte.SetParameterValue("TelefonoCelular", q[0].Celular);
                    reporte.SetParameterValue("CargasFamiliares", q[0].CargasFamiliares);
                    reporte.SetParameterValue("TipoCasa", q[0].TipoCasa);
                    reporte.SetParameterValue("VehiculoAnio", q[0].MarcaModelo);
                    reporte.SetParameterValue("VehiculoPlacas", q[0].Placas);
                    reporte.SetParameterValue("Email", q[0].Email);
                    reporte.SetParameterValue("PagoMensual", q[0].PagoMensual);
                    reporte.SetParameterValue("TiempoResidir", q[0].TiempoResidir);
                    reporte.SetParameterValue("ReferenciasBancarias", q[0].ReferenciasBancarias);
                    reporte.SetParameterValue("Empresa", q[0].Empresa);
                    reporte.SetParameterValue("Cargo", q[0].Cargo);
                    reporte.SetParameterValue("TiempoTrabajar", q[0].TiempoLaborar);
                    reporte.SetParameterValue("IngresosMensuales", q[0].IngresosMes);
                    reporte.SetParameterValue("EgresosMensuales", q[0].GastosMes);
                    reporte.SetParameterValue("DireccionTrabajo", q[0].EmpresaDireccion);
                    reporte.SetParameterValue("MunicipioTrabajo", q[0].MunicipioEmpresa);
                    reporte.SetParameterValue("DepartamentoTrabajo", q[0].DepartamentoEmpresa);
                    reporte.SetParameterValue("TelefonoTrabajo", q[0].EmpresaTelefono);
                    reporte.SetParameterValue("Extension", q[0].Extension);
                    reporte.SetParameterValue("NombreConyugue", q[0].ConyugeNombre);
                    reporte.SetParameterValue("VendedorInterconsumo", q[0].VendedorInterconsumo);
                    reporte.SetParameterValue("AutorizadoPor", q[0].AutorizadoPor);
                    reporte.SetParameterValue("NoAutorizacion", q[0].NoAutorizacion);

                    try
                    {
                        DateTime mFechaAutorizacion = Convert.ToDateTime(q[0].FechaAutorizacion);

                        if (mFechaAutorizacion.Year == 1)
                        {
                            reporte.SetParameterValue("FechaAutorizacion", new DateTime(1901, 1, 1));
                        }
                        else
                        {
                            reporte.SetParameterValue("FechaAutorizacion", mFechaAutorizacion);
                        }
                    }
                    catch
                    {
                        reporte.SetParameterValue("FechaAutorizacion", new DateTime(1901, 1, 1));
                    }

                    try
                    {
                        DateTime mFechaNacimientoConyugue = Convert.ToDateTime(q[0].ConyugueFechaNacimiento);

                        if (mFechaNacimientoConyugue.Year == 1)
                        {
                            reporte.SetParameterValue("FechaNacimientoConyugue", new DateTime(1901, 1, 1));
                        }
                        else
                        {
                            reporte.SetParameterValue("FechaNacimientoConyugue", mFechaNacimientoConyugue);
                        }
                    }
                    catch
                    {
                        reporte.SetParameterValue("FechaNacimientoConyugue", new DateTime(1901, 1, 1));
                    }

                    reporte.SetParameterValue("DPIConyugue", q[0].ConyugueCedula);
                    reporte.SetParameterValue("TrabajoConyugue", q[0].ConyugueEmpresa);
                    reporte.SetParameterValue("CargoConyugue", q[0].ConyugueCargo);
                    reporte.SetParameterValue("TiempoConyugue", q[0].ConyugueTiempo);
                    reporte.SetParameterValue("IngresosConyugue", q[0].ConyugueIngresos);
                    reporte.SetParameterValue("DireccionTrabajoConyugue", q[0].ConyugueEmpresaDireccion);
                    reporte.SetParameterValue("TelefonoTrabajoConyugue", q[0].ConyugueEmpresaTelefono);
                    reporte.SetParameterValue("ReferenciaPersonal1", q[0].RefPersonalNombre1);
                    reporte.SetParameterValue("ReferenciaPersonalResidencia1", q[0].RefPersonalTelResidencia1);
                    reporte.SetParameterValue("ReferenciaPersonalTrabajo1", q[0].RefPersonalTelTrabajo1);
                    reporte.SetParameterValue("ReferenciaPersonalCelular1", q[0].RefPersonalCelular1);
                    reporte.SetParameterValue("ReferenciaPersonal2", q[0].RefPersonalNombre2);
                    reporte.SetParameterValue("ReferenciaPersonalResidencia2", q[0].RefPersonalTelResidencia2);
                    reporte.SetParameterValue("ReferenciaPersonalTrabajo2", q[0].RefPersonalTelTrabajo2);
                    reporte.SetParameterValue("ReferenciaPersonalCelular2", q[0].RefPersonalCelular2);
                    reporte.SetParameterValue("ReferenciaComercial1", q[0].RefComercialNombre1);
                    reporte.SetParameterValue("ReferenciaComercialResidencia1", q[0].RefComercialTelefono1);
                    reporte.SetParameterValue("ReferenciaComercialTrabajo1", q[0].RefComercialFax1);
                    reporte.SetParameterValue("ReferenciaComercialCelular1", q[0].RefComercialCelular1);
                    reporte.SetParameterValue("ReferenciaComercial2", q[0].RefComercialNombre2);
                    reporte.SetParameterValue("ReferenciaComercialResidencia2", q[0].RefComercialTelefono2);
                    reporte.SetParameterValue("ReferenciaComercialTrabajo2", q[0].RefComercialFax2);
                    reporte.SetParameterValue("ReferenciaComercialCelular2", q[0].RefComercialCelular2);
                    reporte.SetParameterValue("PrecioDelBien", q[0].TotalFacturar);
                    reporte.SetParameterValue("Enganche", q[0].Enganche);
                    reporte.SetParameterValue("SaldoFinanciar", q[0].SaldoFinanciar);
                    reporte.SetParameterValue("Recargos", q[0].Recargos);
                    reporte.SetParameterValue("Monto", q[0].Monto);
                    reporte.SetParameterValue("Plazo", q[0].Plazo);
                    reporte.SetParameterValue("CantidadPagos1", q[0].CantidadPagos1);
                    reporte.SetParameterValue("MontoPagos1", q[0].MontoPagos1);
                    reporte.SetParameterValue("CantidadPagos2", q[0].CantidadPagos2);
                    reporte.SetParameterValue("MontoPagos2", q[0].MontoPagos2);
                    reporte.SetParameterValue("Solicitud", q[0].Departamento);
                    reporte.SetParameterValue("TextoEncabezado", q[0].EntraCamion);

                    if (!Directory.Exists(@"C:\reportes\")) Directory.CreateDirectory(@"C:\reportes\");
                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }
            }
            else
            {
                lbInfo.Text = "";
                lbError.Text = "Esta opción solo aplica para un banco";
                lbErrorCotizaciones.Text = "Esta opción solo aplica para un banco";
            }
        }

        void ImprimirCotizacion(bool enviar)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);
                


                Label lbCotizacion = (Label)gridCotizaciones.SelectedRow.FindControl("lbCotizacion");
                Label lbTipoVenta = (Label)gridCotizaciones.SelectedRow.FindControl("lbTipoVenta");
                Label lbFinanciera = (Label)gridCotizaciones.SelectedRow.FindControl("lbNotasTipoVenta");
                Label lbNivelPrecio = (Label)gridCotizaciones.SelectedRow.FindControl("lbNivelPrecio");
                Label lbFecha = (Label)gridCotizaciones.SelectedRow.FindControl("lbFecha");
                Label lbFechaVence = (Label)gridCotizaciones.SelectedRow.FindControl("lbFechaEntrega");
                Label lbTotalFacturar = (Label)gridCotizaciones.SelectedRow.FindControl("lbTotalFacturar");
                Label lbEnganche = (Label)gridCotizaciones.SelectedRow.FindControl("lbEnganche");
                Label lbSaldoFinanciar = (Label)gridCotizaciones.SelectedRow.FindControl("lbSaldoFinanciar");
                Label lbRecargos = (Label)gridCotizaciones.SelectedRow.FindControl("lbRecargos");
                Label lbMonto = (Label)gridCotizaciones.SelectedRow.FindControl("lbMonto");
                Label lbCantidadPagos1 = (Label)gridCotizaciones.SelectedRow.FindControl("lbCantidadPagos1");
                Label lbMontoPagos1 = (Label)gridCotizaciones.SelectedRow.FindControl("lbMontoPagos1");
                Label lbCantidadPagos2 = (Label)gridCotizaciones.SelectedRow.FindControl("lbCantidadPagos2");
                Label lbMontoPagos2 = (Label)gridCotizaciones.SelectedRow.FindControl("lbMontoPagos2");
                Label lbObservaciones = (Label)gridCotizaciones.SelectedRow.FindControl("lbObservaciones");
                Label lbNombreRecibe = (Label)gridCotizaciones.SelectedRow.FindControl("lbNombreRecibe");
                Label lbNombre = (Label)gridCotizaciones.SelectedRow.FindControl("lbNombreAutorizacion");
                Label lbPlan = (Label)gridCotizaciones.SelectedRow.FindControl("lbAutorizacion");
                Label lbTelefono = (Label)gridCotizaciones.SelectedRow.FindControl("lbEntregaAMPM");
                Label lbEmailVendedor = (Label)gridCotizaciones.SelectedRow.FindControl("lbDesarmarla");
                Label lbTienda = (Label)gridCotizaciones.SelectedRow.FindControl("lbGarantia");
                Label lbVendedor = (Label)gridCotizaciones.SelectedRow.FindControl("lbObservacionesTipoVenta");
                Label lbDepartamento = (Label)gridCotizaciones.SelectedRow.FindControl("lbTipoReferencia");
                Label lbTelefonoCliente = (Label)gridCotizaciones.SelectedRow.FindControl("lbObservacionesReferencia");
                Label lbObservacionesVendedor = (Label)gridCotizaciones.SelectedRow.FindControl("lbTipoPedido");

                dsPuntoVenta ds = new dsPuntoVenta();
                

                DataRow row = ds.Cotizacion.NewRow();
                row["Cotizacion"] = lbCotizacion.Text;
                row["Nombre"] = lbNombre.Text;
                row["Fecha"] = Convert.ToDateTime(lbFecha.Text);
                row["TipoVenta"] = lbTipoVenta.Text;
                row["Financiera"] = lbFinanciera.Text;
                row["NivelPrecio"] = lbNivelPrecio.Text;
                row["Plan"] = lbPlan.Text;
                row["Email"] = lbNombreRecibe.Text;
                row["Observaciones"] = lbObservaciones.Text;
                row["FechaVence"] = Convert.ToDateTime(lbFechaVence.Text);
                row["TotalFacturar"] = Convert.ToDecimal(lbTotalFacturar.Text);
                row["Enganche"] = Convert.ToDecimal(lbEnganche.Text);
                row["SaldoFinanciar"] = Convert.ToDecimal(lbSaldoFinanciar.Text);
                row["Recargos"] = Convert.ToDecimal(lbRecargos.Text);
                row["Monto"] = Convert.ToDecimal(lbMonto.Text);
                row["CantidadPagos1"] = lbCantidadPagos1.Text;
                row["MontoPagos1"] = Convert.ToDecimal(lbMontoPagos1.Text);
                row["CantidadPagos2"] = lbCantidadPagos2.Text;
                row["MontoPagos2"] = Convert.ToDecimal(lbMontoPagos2.Text);
                row["Telefono"] = lbTelefono.Text;
                row["EmailVendedor"] = lbEmailVendedor.Text;
                row["Tienda"] = lbTienda.Text;
                row["Vendedor"] = lbVendedor.Text;
                row["Departamento"] = lbDepartamento.Text;
                row["TelefonoCliente"] = lbTelefonoCliente.Text;
                row["ObservacionesVendedor"] = lbObservacionesVendedor.Text;
                ds.Cotizacion.Rows.Add(row);

                var qq = ws.DevuelveCotizacionLinea(lbCotizacion.Text);
                List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();
               // var factor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue,cbFinanciera.SelectedValue,cbNivelPrecio.SelectedValue);
                for (int ii = 0; ii < qq.Length; ii++)
                {
                    decimal total = Math.Round( Convert.ToDecimal(qq[ii].PrecioTotal), 2, MidpointRounding.AwayFromZero);

                    DataRow rowDet = ds.CotizacionDet.NewRow();
                    rowDet["Cotizacion"] = lbCotizacion.Text;
                    rowDet["Articulo"] = qq[ii].Articulo;
                    rowDet["Descripcion"] = qq[ii].Descripcion.ToUpper();
                    rowDet["PrecioUnitario"] = Convert.ToDecimal(qq[ii].PrecioUnitario);
                    rowDet["Cantidad"] = Convert.ToInt32(qq[ii].CantidadPedida);
                    rowDet["Total"] = total;//(Convert.ToDecimal(qq[ii].PrecioTotal));
                    rowDet["Cuota"] =  Convert.ToDecimal(qq[ii].PrecioOriginal) == 0  ?  0: Convert.ToDecimal(qq[ii].Comentario);
                    rowDet["Descuento"] = total==0? Convert.ToDecimal(qq[ii].PrecioUnitario): Convert.ToDecimal(qq[ii].Descuento);
                    ds.CotizacionDet.Rows.Add(rowDet);
                }

                using (ReportDocument ReporteExpediente = new ReportDocument())
                {
                    string m = (Request.PhysicalApplicationPath + "reportes/rptCotizacion.rpt");
                    ReporteExpediente.Load(m);

                    string mCoti = string.Format("Cotizacion_{0}.pdf", lbCotizacion.Text);
                    if (!Directory.Exists(@"C:\reportes\")) Directory.CreateDirectory(@"C:\reportes\");
                    if (File.Exists(@"C:\reportes\" + mCoti)) File.Delete(@"C:\reportes\" + mCoti);

                    ReporteExpediente.SetDataSource(ds);
                    ReporteExpediente.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mCoti);

                    if (enviar)
                    {
                        SmtpClient smtp = new SmtpClient();
                        MailMessage mail = new MailMessage();

                        mail.IsBodyHtml = true;
                        mail.To.Add(lbNombreRecibe.Text);

                        try
                        {
                            mail.ReplyToList.Add(lbEmailVendedor.Text);
                            mail.From = new MailAddress(lbEmailVendedor.Text, Session["NombreVendedor"].ToString());
                        }
                        catch
                        {
                            mail.ReplyToList.Add("servicioalcliente@mueblesfiesta.com");
                            mail.From = new MailAddress("servicioalcliente@mueblesfiesta.com", "Muebles Fiesta");
                        }

                        mail.Subject = string.Format("Cotización No. {0} - Muebles Fiesta", lbCotizacion.Text);

                        StringBuilder mBody = new StringBuilder();

                        mBody.Append("<!DOCTYPE html><html lang='en-us'>");
                        mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } </style></head>");
                        mBody.Append("<body>");
                        mBody.Append(string.Format("<p>Estimado(a) Sr(a). {0},</p>", lbNombre.Text));
                        mBody.Append("<p>Adjunto encontrará la cotización solicitada, por favor no olvide visitar nuestra <a href='http://www.mueblesfiesta.com' tabindex='0'>Sitio Web</a>.</p>");
                        mBody.Append("<p>Atentamente,</p>");
                        mBody.Append(string.Format("<p>{0}<BR>Teléfono {1}<BR>{2}<BR><b>Muebles Fiesta - {3}</b></p>", lbVendedor.Text, lbTelefono.Text, lbEmailVendedor.Text, lbTienda.Text));
                        mBody.Append("</body>");
                        mBody.Append("</html>");

                        mail.Body = mBody.ToString();
                        mail.Attachments.Add(new Attachment(@"C:\reportes\" + mCoti));

                        smtp.Host = WebConfigurationManager.AppSettings["MailHost"];
                        smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort"]);
                        smtp.EnableSsl = true;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                        try
                        {
                            smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail"], WebConfigurationManager.AppSettings["PwdMail"]);
                            smtp.Send(mail);
                        }
                        catch (Exception ex2)
                        {
                            lbError.Text = string.Format("Error al enviar la cotización {0}", ex2.Message);
                            return;
                        }

                        lbInfo.Text = string.Format("La cotización fue enviada a {0}", lbNombreRecibe.Text);
                    }
                    else
                    {
                        Response.Clear();
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mCoti);
                        Response.WriteFile(@"C:\reportes\" + mCoti);
                        Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error en la cotización. {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }

        void AsignarCliente(string cotizacion)
        {
            try
            {
                mostrarBusquedaCliente();
                lbCotizacionSeleccionada.Text = cotizacion;
                lkAsignarCliente.ToolTip = string.Format("Asignar este cliente a la Cotización No. {0}", cotizacion);
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al asignar cliente. {0} {1}", ex.Message, ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")));
            }
        }

         void SeleccionarCotizacion()
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";
                decimal mDescuentoTotal = 0M;
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                Label lbCotizacion = (Label)gridCotizaciones.SelectedRow.FindControl("lbCotizacion");
                Label lbTipoVenta = (Label)gridCotizaciones.SelectedRow.FindControl("lbTipoVenta");
                Label lbFinanciera = (Label)gridCotizaciones.SelectedRow.FindControl("lbFinanciera");
                Label lbNivelPrecio = (Label)gridCotizaciones.SelectedRow.FindControl("lbNivelPrecio");
                Label lbFecha = (Label)gridCotizaciones.SelectedRow.FindControl("lbFecha");
                Label lbFechaVence = (Label)gridCotizaciones.SelectedRow.FindControl("lbFechaEntrega");
                Label lbTotalFacturar = (Label)gridCotizaciones.SelectedRow.FindControl("lbTotalFacturar");
                Label lbEnganche = (Label)gridCotizaciones.SelectedRow.FindControl("lbEnganche");
                Label lbSaldoFinanciar = (Label)gridCotizaciones.SelectedRow.FindControl("lbSaldoFinanciar");
                Label lbRecargos = (Label)gridCotizaciones.SelectedRow.FindControl("lbRecargos");
                Label lbMonto = (Label)gridCotizaciones.SelectedRow.FindControl("lbMonto");
                Label lbCantidadPagos1 = (Label)gridCotizaciones.SelectedRow.FindControl("lbCantidadPagos1");
                Label lbMontoPagos1 = (Label)gridCotizaciones.SelectedRow.FindControl("lbMontoPagos1");
                Label lbCantidadPagos2 = (Label)gridCotizaciones.SelectedRow.FindControl("lbCantidadPagos2");
                Label lbMontoPagos2 = (Label)gridCotizaciones.SelectedRow.FindControl("lbMontoPagos2");
                Label lbObservaciones = (Label)gridCotizaciones.SelectedRow.FindControl("lbObservaciones");
                Label lbNombreRecibe = (Label)gridCotizaciones.SelectedRow.FindControl("lbNombreRecibe");
                Label lbNombreAutorizacion = (Label)gridCotizaciones.SelectedRow.FindControl("lbNombreAutorizacion");
                Label lbTelefono = (Label)gridCotizaciones.SelectedRow.FindControl("lbObservacionesReferencia");
                Label lbPrimerPago = (Label)gridCotizaciones.SelectedRow.FindControl("lbPrimerPago");
                
                try
                {
                    cbTipo.SelectedValue = lbTipoVenta.Text;
                }
                catch
                {
                    lbError.Text = "La promoción de esta cotización no está disponible.";
                    return;
                }

                try
                {
                    cbFinanciera.SelectedValue = lbFinanciera.Text;
                }
                catch
                {
                    lbError.Text = "La financiera de esta cotización no está disponible.";
                    return;
                }

                CargaNivelesDePrecio();

                try
                {
                    cbNivelPrecio.SelectedValue = ws.DevuelveNivelPadre(lbTipoVenta.Text, Convert.ToInt32(lbFinanciera.Text), lbNivelPrecio.Text);
                }
                catch
                {
                    lbError.Text = "El nivel de precio de esta cotización no está disponible.";
                    return;
                }

                limpiar(true);

                #region "Detalle cotización"

                //-------------
                //nueva lista de descuentos en artículos
                //-------------
                List<NuevoPrecioProducto> lstProductos = new List<NuevoPrecioProducto>();

                //Se obtiene el factor para la formula del calculo de la cuota
                double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);


                List<wsPuntoVenta.PedidoLinea> qBlanco = new List<wsPuntoVenta.PedidoLinea>();
                ViewState["qArticulos"] = qBlanco;

                gridArticulos.DataSource = qBlanco;
                gridArticulos.DataBind();

                var qq = ws.DevuelveCotizacionLinea(lbCotizacion.Text);
                List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();
                ViewState["qArticulos"] = q;

                cbTipo.SelectedValue = lbTipoVenta.Text;
                cbFinanciera.SelectedValue = lbFinanciera.Text;
                CargaNivelesDePrecio();
                cbNivelPrecio.SelectedValue = ws.DevuelveNivelPadre(lbTipoVenta.Text, Convert.ToInt32(lbFinanciera.Text), lbNivelPrecio.Text);


                var factor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                mDescuentoTotal = qq.Sum(x => x.Descuento);//CAMBIO
                var mMontoTotal = qq.Sum(x =>x.PrecioTotal);
                var mTotalFacturar = Math.Round( mMontoTotal /(decimal)factor + mDescuentoTotal,2,MidpointRounding.AwayFromZero);
                for (int ii = 0; ii < qq.Length; ii++)
                {
                    decimal ValorSinDescuento = qq[ii].PrecioTotal;//(qq[ii].PrecioFacturar / (decimal)factor - qq[ii].Descuento) * (decimal)factor;//factor==1 ? qq[ii].PrecioFacturar- qq[ii].Descuento : (qq[ii].PrecioFacturar - qq[ii].Descuento)*(decimal)factor;

                    wsPuntoVenta.PedidoLinea item = new wsPuntoVenta.PedidoLinea();
                    item.Articulo = qq[ii].Articulo;
                    item.Nombre = qq[ii].Descripcion.ToUpper();
                    item.PrecioUnitario = qq[ii].PrecioUnitario;
                    item.PrecioSugerido= qq[ii].PrecioSugerido;
                    item.CantidadPedida = qq[ii].CantidadPedida;
                    item.PrecioTotal = qq[ii].PrecioTotal;
                    item.Bodega = qq[ii].Bodega;
                    item.Localizacion = qq[ii].Localizacion;
                    item.RequisicionArmado = qq[ii].RequisicionArmado;
                    item.Descripcion = qq[ii].Descripcion;
                    item.Comentario = qq[ii].Comentario;
                    item.Estado = qq[ii].Estado;
                    item.NumeroPedido = qq[ii].NumeroPedido;
                    item.Oferta = qq[ii].Oferta;
                    item.FechaOfertaDesde = qq[ii].FechaOfertaDesde;
                    item.PrecioOriginal = qq[ii].PrecioOriginal;
                    item.TipoOferta = qq[ii].TipoOferta;
                    item.EsDetalleKit = qq[ii].EsDetalleKit;
                    item.PrecioFacturar = qq[ii].PrecioFacturar;
                    item.Vale = qq[ii].Vale;
                    item.Autorizacion = qq[ii].Autorizacion;
                    item.Gel = qq[ii].Gel;
                    item.PorcentajeDescuento = qq[ii].PorcentajeDescuento;
                    item.Descuento = qq[ii].Descuento;
                    item.Linea = qq[ii].Linea;
                    
                    item.NetoFacturar = ValorSinDescuento < 0.10M ? 0: ValorSinDescuento;
                    q.Add(item);

                    //Lista de Precios con descuento
                    lstProductos.Add(new NuevoPrecioProducto {
                                    Index = ii,
                                    factor= (decimal)mFactor,
                                    itemCode=item.Articulo,
                                    originalPrice= item.PrecioUnitario,
                                    originalDiscountPrice=item.PrecioSugerido,
                                    price= (item.PrecioUnitario) - qq[ii].Descuento,

                    });

                }
                Session["lstNuevoPrecioProducto"] = lstProductos;
                var SAldoDesc = Facturacion.ObtenerSaldoDescuento(lstProductos);
                txtSaldoDescuento.Text = String.Format("{0:0,0.00}", SAldoDesc >0 ? SAldoDesc:0);

                gridArticulos.DataSource = q;
                gridArticulos.DataBind();
                gridArticulos.Visible = true;
                gridArticulos.SelectedIndex = -1;

                ViewState["qArticulos"] = q;
                #endregion
                //encabezado
                #region encabezado
                

                txtPedido.Text = lbCotizacion.Text;
                txtFecha.Text = lbFecha.Text;
                txtFacturar.Text = lbTotalFacturar.Text;
                txtFacturar.Text = String.Format("{0:0,0.00}", mTotalFacturar);
                txtEnganche.Text = lbEnganche.Text;
                txtSaldoFinanciar.Text = lbSaldoFinanciar.Text;
                txtIntereses.Text = lbRecargos.Text;
                txtMonto.Text = String.Format("{0:0,0.00}", mMontoTotal);// String.Format("{0:0,0.00}", (decimal.Parse(lbTotalFacturar.Text.Replace(",",""))/(decimal)mFactor - mDescuentoTotal)*(decimal)mFactor);
                cbPagos1.SelectedValue = Convert.ToInt32(lbCantidadPagos1.Text) < 10 ? string.Format("0{0}", lbCantidadPagos1.Text) : lbCantidadPagos1.Text;
                txtPagos1.Text = Convert.ToDouble(lbMontoPagos1.Text.Replace(",", "")) > 0 ? String.Format("{0:0,0.00}", Convert.ToDouble(lbMontoPagos1.Text.Replace(",", ""))) : "0.00";
                cbPagos2.SelectedValue = Convert.ToInt32(lbCantidadPagos2.Text) < 10 ? string.Format("0{0}", lbCantidadPagos2.Text) : lbCantidadPagos2.Text;
                txtPagos2.Text = Convert.ToDouble(lbMontoPagos2.Text.Replace(",", "")) > 0 ? String.Format("{0:0,0.00}", Convert.ToDouble(lbMontoPagos2.Text.Replace(",", ""))) : "0.00";
                txtObservaciones.Text = lbObservaciones.Text;
                txtQuienAutorizo.Text = lbNombreAutorizacion.Text;
                txtNombreRecibe.Text = lbNombreRecibe.Text;
                txtNoAutorizacion.Text = lbTelefono.Text;
                txtDescuentos.Text = String.Format("{0:0,0.00}", mDescuentoTotal);//(Convert.ToDouble(lbTotalFacturar.Text.Replace(",", "")) - Convert.ToDouble(txtEnganche.Text.Replace(",", "")) - Convert.ToDouble(txtSaldoFinanciar.Text.Replace(",", ""))).ToString();
                

                DateTime mFechaVence;
                try
                {
                    mFechaVence = Convert.ToDateTime(lbFechaVence.Text);
                }
                catch
                {
                    mFechaVence = DateTime.Now.Date.AddDays(15);
                }

                cbDia.SelectedValue = mFechaVence.Day.ToString();
                cbMes.SelectedValue = mFechaVence.Month.ToString();
                txtAnio.Text = mFechaVence.Year.ToString();

                DateTime mPrimerPago;
                try
                {
                    mPrimerPago = Convert.ToDateTime(lbPrimerPago.Text);
                }
                catch
                {
                    mPrimerPago = DateTime.Now.Date.AddDays(30);
                }

                cbDiaPrimerPago.SelectedValue = mPrimerPago.Day.ToString();
                cbMesPrimerPago.SelectedValue = mPrimerPago.Month.ToString();
                txtAnioPrimerPago.Text = mPrimerPago.Year.ToString();

                #endregion

                OcultarCotizaciones();
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al seleccionar la cotización {0} {1}", ex.Message, ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")));
            }
        }

        protected void lkOcultarCotizaciones_Click(object sender, EventArgs e)
        {
            OcultarCotizaciones();
        }

        void OcultarCotizaciones()
        {
            tblCotizaciones.Visible = false;
            gridCotizaciones.Visible = false;
        }

        protected void lkAsignarCliente_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoCliente.Text = "";
                lbErrorCliente.Text = "";

                if (txtCodigo.Text.Trim().Length == 0)
                {
                    lbErrorCliente.Text = "Debe seleccionar un cliente para poder asignarlo";
                    return;
                }

                AsignarClienteGrid();
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al asignar el cliente {0} {1}", ex.Message, ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")));
            }
        }

        void AsignarClienteGrid()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            string mMensaje = "";
            string mCliente = gridClientes.SelectedRow.Cells[1].Text;

            if (!ws.AsignarClienteCotizacion(ref mMensaje, mCliente, lbCotizacionSeleccionada.Text))
            {
                lbErrorCliente.Text = mMensaje;
                return;
            }

            lbInfoCliente.Text = mMensaje;
            Session["Cliente"] = mCliente;

            limpiar(true);
            CargarCotizaciones(true);
            lbInfo.Text = mMensaje;
        }

        protected void lkEliminarArticulo_Click(object sender, EventArgs e)
        {
            ViewState["AccionArticulo"] = "Eliminar";
        }

        protected void lkCambiarGel_Click(object sender, EventArgs e)
        {
            ViewState["AccionArticulo"] = "Gel";
        }

        protected void lkLaTorre_Click(object sender, EventArgs e)
        {
            try
            {
                lbError2.Text = "";
                lbError2.Visible = false;

                List<wsPuntoVenta.PedidoLinea> qOriginal = new List<wsPuntoVenta.PedidoLinea>();
                qOriginal = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                wsPuntoVenta.PedidoLinea[] q = new wsPuntoVenta.PedidoLinea[1];
                wsPuntoVenta.PedidoLinea[] qLinea = new wsPuntoVenta.PedidoLinea[1];
                List<MF_Clases.Comun.PedidoLinea> qqLinea = new List<MF_Clases.Comun.PedidoLinea>();

                int jj = 0;
                for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                {
                    jj++;

                    Label lbPrecioUnitario = (Label)gridArticulos.Rows[ii].FindControl("lbPrecio");
                    Label lbCantidadPedida = (Label)gridArticulos.Rows[ii].FindControl("lbCantidad");
                    Label lbPrecioTotal = (Label)gridArticulos.Rows[ii].FindControl("lbTotal");
                    Label lbDescripcion = (Label)gridArticulos.Rows[ii].FindControl("lbDescripcion");
                    Label lbComentario = (Label)gridArticulos.Rows[ii].FindControl("lbComentario");
                    Label lbEstado = (Label)gridArticulos.Rows[ii].FindControl("lbEstado");
                    Label lbNumeroPedido = (Label)gridArticulos.Rows[ii].FindControl("lbNumeroPedido");
                    Label lbOferta = (Label)gridArticulos.Rows[ii].FindControl("lbOferta");
                    Label lbFechaOfertaDesde = (Label)gridArticulos.Rows[ii].FindControl("lbFechaOfertaDesde");
                    Label lbPrecioOriginal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioOriginal");
                    Label lbEsDetalleKit = (Label)gridArticulos.Rows[ii].FindControl("lbEsDetalleKit");
                    Label lbPrecioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioFacturar");
                    Label lbTipoOferta = (Label)gridArticulos.Rows[ii].FindControl("lbTipoOferta");
                    Label lbVale = (Label)gridArticulos.Rows[ii].FindControl("lbVale");
                    Label lbAutorizacion = (Label)gridArticulos.Rows[ii].FindControl("lbAutorizacion");
                    Label lbBodega = (Label)gridArticulos.Rows[ii].FindControl("lbBodega");
                    Label lbLocalizacion = (Label)gridArticulos.Rows[ii].FindControl("lbLocalizacion");
                    LinkButton lkRequisicionArmado = (LinkButton)gridArticulos.Rows[ii].FindControl("lkRequisicionArmado");
                    Label lbArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbArticulo");
                    Label lbNombreArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbNombreArticulo");
                    Label lbPrecioUnitarioDebioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioUnitarioDebioFacturar");
                    Label lbPrecioSugerido = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioSugerido");
                    Label lbPrecioBaseLocal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioBaseLocal");
                    Label lbGel = (Label)gridArticulos.Rows[ii].FindControl("lbGel");
                    Label lbLinea = (Label)gridArticulos.Rows[ii].FindControl("lbLinea");
                    Label lbPorcentajeDescuento = (Label)gridArticulos.Rows[ii].FindControl("lbPorcentajeDescuento");
                    Label lbBeneficiario = (Label)gridArticulos.Rows[ii].FindControl("lbBeneficiario");
                    Label lbDescuento = (Label)gridArticulos.Rows[ii].FindControl("lbDescuento");

                    decimal mPrecioOriginal = 0;
                    DateTime mFechaOfertaDesde = new DateTime(1980, 1, 1);

                    try
                    {
                        mFechaOfertaDesde = new DateTime(Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(6, 4)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(3, 2)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(0, 2)));
                    }
                    catch
                    {
                        //Nothing
                    }
                    try
                    {
                        mPrecioOriginal = Convert.ToDecimal(lbPrecioOriginal.Text.Replace(",", ""));
                    }
                    catch
                    {
                        mPrecioOriginal = 0;
                    }

                    MF_Clases.Comun.PedidoLinea linea = new MF_Clases.Comun.PedidoLinea
                    {
                        Articulo = lbArticulo.Text,
                        Nombre = lbNombreArticulo.Text,
                        PrecioUnitario = Convert.ToDecimal(lbPrecioUnitario.Text.Replace(",", "")),
                        CantidadPedida = Convert.ToDecimal(lbCantidadPedida.Text.Replace(",", "")),
                        PrecioTotal = Convert.ToDecimal(lbPrecioTotal.Text.Replace(",", "")),
                        PrecioFacturar = Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", "")),
                        Bodega = lbBodega.Text,
                        Localizacion = lbLocalizacion.Text,
                        RequisicionArmado = lkRequisicionArmado.Text,
                        Descripcion = lbDescripcion.Text,
                        Comentario = lbComentario.Text,
                        Estado = lbEstado.Text,
                        NumeroPedido = lbNumeroPedido.Text,
                        Oferta = lbOferta.Text,
                        FechaOfertaDesde = mFechaOfertaDesde,
                        PrecioOriginal = mPrecioOriginal,
                        TipoOferta = lbTipoOferta.Text,
                        EsDetalleKit = lbEsDetalleKit.Text,
                        Vale = Convert.ToInt32(lbVale.Text == "" ? "0" : lbVale.Text),
                        Autorizacion = Convert.ToInt64(lbAutorizacion.Text == "" ? "0" : lbAutorizacion.Text),
                        PrecioUnitarioDebioFacturar = Convert.ToDecimal(lbPrecioUnitarioDebioFacturar.Text.Replace(",", "")),
                        PrecioSugerido = Convert.ToDecimal(lbPrecioSugerido.Text.Replace(",", "")),
                        PrecioBaseLocal = Convert.ToDouble(lbPrecioBaseLocal.Text.Replace(",", "")),
                        Gel = lbGel.Text,
                        Linea = Convert.ToInt32(lbLinea.Text),
                        PorcentajeDescuento = Convert.ToDecimal(lbPorcentajeDescuento.Text),
                        Beneficiario = lbBeneficiario.Text,
                        Descuento= Convert.ToDecimal(lbDescuento.Text)
                    };
                    qqLinea.Add(linea);

                    wsPuntoVenta.PedidoLinea itemPedidoLinea = new wsPuntoVenta.PedidoLinea();
                    itemPedidoLinea.Articulo = lbArticulo.Text;
                    itemPedidoLinea.Nombre = lbNombreArticulo.Text;
                    itemPedidoLinea.PrecioUnitario = Convert.ToDecimal(lbPrecioUnitario.Text.Replace(",", ""));
                    itemPedidoLinea.CantidadPedida = Convert.ToDecimal(lbCantidadPedida.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioTotal = Convert.ToDecimal(lbPrecioTotal.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioFacturar = Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.Bodega = lbBodega.Text;
                    itemPedidoLinea.Localizacion = lbLocalizacion.Text;
                    itemPedidoLinea.RequisicionArmado = lkRequisicionArmado.Text;
                    itemPedidoLinea.Descripcion = lbDescripcion.Text;
                    itemPedidoLinea.Comentario = lbComentario.Text;
                    itemPedidoLinea.Estado = lbEstado.Text;
                    itemPedidoLinea.NumeroPedido = lbNumeroPedido.Text;
                    itemPedidoLinea.Oferta = lbOferta.Text;
                    itemPedidoLinea.FechaOfertaDesde = mFechaOfertaDesde;
                    itemPedidoLinea.PrecioOriginal = mPrecioOriginal;
                    itemPedidoLinea.TipoOferta = lbTipoOferta.Text;
                    itemPedidoLinea.EsDetalleKit = lbEsDetalleKit.Text;
                    itemPedidoLinea.Vale = Convert.ToInt32(lbVale.Text == "" ? "0" : lbVale.Text);
                    itemPedidoLinea.Autorizacion = Convert.ToInt64(lbAutorizacion.Text == "" ? "0" : lbAutorizacion.Text);
                    itemPedidoLinea.PrecioUnitarioDebioFacturar = Convert.ToDecimal(lbPrecioUnitarioDebioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioSugerido = Convert.ToDecimal(lbPrecioSugerido.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioBaseLocal = Convert.ToDouble(lbPrecioBaseLocal.Text.Replace(",", ""));
                    itemPedidoLinea.Gel = lbGel.Text;
                    itemPedidoLinea.Linea = Convert.ToInt32(lbLinea.Text);
                    itemPedidoLinea.PorcentajeDescuento = Convert.ToDecimal(lbPorcentajeDescuento.Text);
                    itemPedidoLinea.Beneficiario = lbBeneficiario.Text;
                    itemPedidoLinea.Descuento= Convert.ToDecimal(lbDescuento.Text);

                    if (jj > 1) Array.Resize(ref q, jj);
                    if (jj > 1) Array.Resize(ref qLinea, jj);

                    q[jj - 1] = itemPedidoLinea;
                    qLinea[jj - 1] = itemPedidoLinea;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta(); 
                string mMensaje = ""; 
                string mPrecioBien = ""; 
                string mMonto = ""; 
                string mNotas = "";
                string DescuentoTotal = "";

                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.AplicaPromocion(ref qLinea, ref mPrecioBien, ref mMonto, cbNivelPrecio.Text, ref mMensaje, ref mNotas, cbTipo.SelectedValue.ToString(), ref DescuentoTotal, Convert.ToString(Session["Tienda"]), Convert.ToString(Session["Vendedor"])))
               // if (!Facturacion.AplicaPromocion(ref qqLinea, ref mPrecioBien, ref mMonto, ref DescuentoTotal, cbNivelPrecio.Text, ref mMensaje, ref mNotas, cbTipo.SelectedValue.ToString(), int.Parse(cbFinanciera.SelectedValue), "", Convert.ToString(Session["Tienda"]), Convert.ToString(Session["Vendedor"]),Session["Cliente"].ToString()))
                {
                    lbError2.Visible = true;
                    lbError2.Text = mMensaje;
                    return;
                }

                txtNotasTipoVenta.Text = mNotas;
                int i = 0;
                foreach (var item in qLinea)
                    {
                    item.PrecioTotal = qLinea[i].PrecioTotal;
                    item.PrecioUnitario = qLinea[i].PrecioUnitario;
                    item.Descuento = qLinea[i].Descuento;
                    item.PorcentajeDescuento = qLinea[i].PorcentajeDescuento;
                    item.NetoFacturar = qLinea[i].NetoFacturar == 0 ? qLinea[i].PrecioTotal : qLinea[i].NetoFacturar;
                    item.PrecioOriginal = qLinea[i].PrecioTotal;
                    item.PrecioFacturar = qLinea[i].PrecioFacturar;
                    i++;
                    //item.PrecioTotal = qqLinea.Find(x => x.Articulo == item.Articulo).PrecioTotal;
                    //item.Descuento = qqLinea.Find(x => x.Articulo == item.Articulo).Descuento;
                    //item.PorcentajeDescuento = qqLinea.Find(x => x.Articulo == item.Articulo).PorcentajeDescuento;
                    //item.NetoFacturar = qqLinea.Find(x => x.Articulo == item.Articulo).NetoFacturar;
                    //item.PrecioOriginal = qqLinea.Find(x => x.Articulo == item.Articulo).PrecioTotal;
                    //item.PrecioFacturar = qqLinea.Find(x => x.Articulo == item.Articulo).PrecioFacturar ;

                };
                
                gridArticulos.DataSource = qLinea;
                gridArticulos.DataBind();

                
                ViewState["qArticulos"] = qLinea;
                lkLaTorre.Visible = false;
                decimal mDescuentos = Convert.ToDecimal(DescuentoTotal);
                //mDescuentos += Convert.ToDecimal(txtDescuentos.Text.Replace(",", ""));
                if (cbFinanciera.SelectedValue == "1" || cbFinanciera.SelectedValue == "5" || cbFinanciera.SelectedValue == "9")
                {
                    txtMonto.Text = mMonto;
                    txtFacturar.Text = mPrecioBien;
                    txtDescuentos.Text = String.Format("{0:0,0.00}", mDescuentos);
                }

                if (ws.EsBanco(cbFinanciera.SelectedValue.ToString()))
                {
                    double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                    decimal mEnganche = Convert.ToDecimal(txtEnganche.Text.Trim().Replace(",", ""));
                    decimal mSaldoFinanciar = Convert.ToDecimal(mPrecioBien.Replace(",", "")) - mEnganche - mDescuentos;
                    decimal mMontoTotal = Convert.ToDecimal(Math.Round((double)mSaldoFinanciar * mFactor, 2, MidpointRounding.AwayFromZero));
                    decimal mRecargos = mMontoTotal - mSaldoFinanciar;

                    txtFacturar.Text = mPrecioBien;
                    txtSaldoFinanciar.Text = String.Format("{0:0,0.00}", mSaldoFinanciar);
                    txtIntereses.Text = String.Format("{0:0,0.00}", mRecargos);
                    txtMonto.Text = String.Format("{0:0,0.00}", mMontoTotal);
                    txtDescuentos.Text = String.Format("{0:0,0.00}", mDescuentos);

                    Int32 mPagos; string mPagos1;
                    string mPagosTexto = ws.DevuelvePagosNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

                    try
                    {
                        mPagos = Convert.ToInt32(mPagosTexto.Substring(0, 2).Replace(" ", ""));
                    }
                    catch
                    {
                        mPagos = 1;
                    }

                    decimal mCuotas = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) / mPagos;

                    if (mPagos <= 10)
                    {
                        mPagos1 = string.Format("0{0}", mPagos - 1);
                    }
                    else
                    {
                        mPagos1 = (mPagos - 1).ToString();
                    }

                    cbPagos1.SelectedValue = mPagos1;
                    cbPagos2.SelectedValue = "01";

                    if (mCuotas.ToString().Substring(mCuotas.ToString().Length - 2, 2) == "00")
                    {
                        txtPagos1.Text = String.Format("{0:0,0.00}", mCuotas);
                        txtPagos2.Text = String.Format("{0:0,0.00}", mCuotas);
                    }
                    else
                    {
                        if (cbFinanciera.SelectedValue == Const.FINANCIERA.BANCREDIT)
                        {
                            txtPagos1.Text = String.Format("{0:0,0.00}", Math.Round(mCuotas, 0, MidpointRounding.AwayFromZero));
                            decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (Math.Round(mCuotas, 0, MidpointRounding.AwayFromZero) * (mPagos - 1));
                            txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
                        }
                        else
                        {
                            string[] mCuotasString = mCuotas.ToString().Split(new string[] { "." }, StringSplitOptions.None);

                            Int32 mCuota = 1 + Convert.ToInt32(mCuotasString[0]);
                            decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (mCuota * (mPagos - 1));

                            txtPagos1.Text = String.Format("{0:0,0.00}", mCuota);
                            txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
                        }
                    }
                }

                //if (cbFinanciera.SelectedValue == "3" || cbFinanciera.SelectedValue == "10")
                //{
                //    ValidaTotalPedido();
                //    ValidaTipoNivelPrecio();
                //}

                lkLaTorre.Visible = false;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format(" - Error al aplicar la promoción {2}. {0} {1}", ex.Message, m, lkLaTorre.Text);
            }
        }

        void MostrarSolicitud()
        {
            try
            {
                SeleccionarCotizacion();

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                lkGrabar.Visible = false;
                lbInfoSolicitud.Text = "";
                lbErrorSolicitud.Text = "";
                txtSolicitudPrecio.Text = "";
                txtSolicitudObservaciones.Text = "";
                tblSolicitudDescuento.Visible = true;

                txtSolicitudPrecio.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error en la cotización. {0} {1}", ex.Message, m);
            }
        }

        protected void lkEnviarSolicitud_Click(object sender, EventArgs e)
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                lbInfoSolicitud.Text = "";
                lbErrorSolicitud.Text = "";

                if (cbTipo.SelectedValue != "NR")
                {
                    lbErrorSolicitud.Text = "No es posible solicitar descuento a una promoción.";
                    return;
                }

                decimal mValor = 0;

                try
                {
                    mValor = Convert.ToDecimal(txtSolicitudPrecio.Text.Trim().Replace(",", "").Replace(" ", ""));
                }
                catch
                {
                    txtSolicitudPrecio.Focus();
                    lbErrorSolicitud.Text = "El valor ingresado es inválido";
                    return;
                }

                Label lbTotalFacturar = (Label)gridCotizaciones.SelectedRow.FindControl("lbTotalFacturar");
                decimal mFacturar = Convert.ToDecimal(lbTotalFacturar.Text.Trim().Replace(",", "").Replace(" ", ""));

                if (mValor <= 0)
                {
                    txtSolicitudPrecio.Focus();
                    lbErrorSolicitud.Text = "El valor debe ser mayor que cero";
                    return;
                }

                if (mValor >= mFacturar)
                {
                    txtSolicitudPrecio.Focus();
                    lbErrorSolicitud.Text = "El valor ingresado no puede ser mayor o igual a la cotización";
                    return;
                }

                if (txtSolicitudObservaciones.Text.Trim().Length <= 100)
                {
                    txtSolicitudObservaciones.Focus();
                    lbErrorSolicitud.Text = "Debe ser más explícito en las observaciones";
                    return;
                }

                Label lbCotizacion = (Label)gridCotizaciones.SelectedRow.FindControl("lbCotizacion");
                Label lbFinanciera = (Label)gridCotizaciones.SelectedRow.FindControl("lbFinanciera");
                Label lbCliente = (Label)gridCotizaciones.SelectedRow.FindControl("lbCliente");
                Label lbTipoVenta = (Label)gridCotizaciones.SelectedRow.FindControl("lbTipoVenta");
                Label lbNivelPrecio = (Label)gridCotizaciones.SelectedRow.FindControl("lbNivelPrecio");

                string mMensaje = "";
                string mCotizacion = lbCotizacion.Text;

                if (!ws.GrabarSolicitudAutorizacionCotizacion(mCotizacion, txtSolicitudPrecio.Text, txtSolicitudObservaciones.Text, ref mMensaje, Convert.ToString(Session["usuario"])))
                {
                    lbErrorSolicitud.Text = mMensaje;
                    return;
                }

                limpiar(true);
                lbInfo.Text = mMensaje;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorSolicitud.Text = string.Format("Error al enviar la solicitud. {0} {1}", ex.Message, m);
            }
        }

        protected void lkRetractarSolicitud_Click(object sender, EventArgs e)
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                lbInfoSolicitud.Text = "";
                lbErrorSolicitud.Text = "";

                Label lbCotizacion = (Label)gridCotizaciones.SelectedRow.FindControl("lbCotizacion");
                Label lbFinanciera = (Label)gridCotizaciones.SelectedRow.FindControl("lbFinanciera");
                Label lbCliente = (Label)gridCotizaciones.SelectedRow.FindControl("lbCliente");
                Label lbTipoVenta = (Label)gridCotizaciones.SelectedRow.FindControl("lbTipoVenta");
                Label lbNivelPrecio = (Label)gridCotizaciones.SelectedRow.FindControl("lbNivelPrecio");

                string mCotizacion = lbCotizacion.Text;

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorSolicitud.Text = string.Format("Error al retractarse de la solicitud. {0} {1}", ex.Message, m);
            }
        }

        protected void lkSolicitudes_Click(object sender, EventArgs e)
        {
            try
            {
                limpiar(true);
                lbInfo.Text = "";
                lbError.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var q = ws.DevuelveCotizacionesPendientes(Convert.ToString(Session["usuario"]), Convert.ToString(Session["Vendedor"]));

                if (q.Length == 0)
                {
                    gridCotizacionesAutorizacion.Visible = false;
                    lbError.Text = "No se encontraron cotizaciones pendientes de autorización";
                }
                else
                {
                    gridCotizacionesAutorizacion.DataSource = q;
                    gridCotizacionesAutorizacion.DataBind();
                    gridCotizacionesAutorizacion.Visible = true;

                    lbInfo.Text = string.Format("Se encontraron {0} cotizaciones", q.Length);
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al cargar las cotizaciones {0} {1}", ex.Message, m);
            }
        }

        protected void lkAsignarAutorizacion_Click(object sender, EventArgs e)
        {
            ViewState["AccionCotizacionAutorizacion"] = "Asignar";
        }

        protected void lkSolicitudCotizacionAutorizacion_Click(object sender, EventArgs e)
        {
            ViewState["AccionCotizacionAutorizacion"] = "Solicitud";
        }

        protected void lkPagareCotizacionAutorizacion_Click(object sender, EventArgs e)
        {
            ViewState["AccionCotizacionAutorizacion"] = "Pagaré";
        }

        protected void lkRetractarCotizacion_Click(object sender, EventArgs e)
        {
            ViewState["AccionCotizacionAutorizacion"] = "Retractarse";
        }

        protected void gridCotizacionesAutorizacion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");

                Label lbObservaciones = (Label)e.Row.FindControl("lbObservacionesGerente");
                Label lbObservacionesSolicitud = (Label)e.Row.FindControl("lbObservacionesSolicitud");

                lbObservaciones.ToolTip = string.Format("Usted escribió:{0}{1}", System.Environment.NewLine, lbObservacionesSolicitud.Text);
            }
        }

        protected void gridCotizacionesAutorizacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                lbInfo.Text = "";
                lbError.Text = "";

                Label lbCotizacion = (Label)gridCotizacionesAutorizacion.SelectedRow.FindControl("lbCotizacion");
                Label lbFinanciera = (Label)gridCotizacionesAutorizacion.SelectedRow.FindControl("lbFinanciera");
                Label lbCliente = (Label)gridCotizacionesAutorizacion.SelectedRow.FindControl("lbCliente");
                Label lbTipoVenta = (Label)gridCotizacionesAutorizacion.SelectedRow.FindControl("lbTipoVenta");
                Label lbNivelPrecio = (Label)gridCotizacionesAutorizacion.SelectedRow.FindControl("lbNivelPrecio");

                string mCotizacion = lbCotizacion.Text;

                switch (ViewState["AccionCotizacionAutorizacion"].ToString())
                {
                    case "Asignar":
                        AsignarCliente(lbCotizacion.Text);
                        break;
                    case "Solicitud":
                        ImprimirSolicitudCotizacion(mCotizacion, lbFinanciera.Text, lbCliente.Text, lbTipoVenta.Text, lbNivelPrecio.Text);
                        break;
                    case "Pagaré":
                        ImprimirPagareCotizacion(mCotizacion, lbFinanciera.Text, lbCliente.Text, lbTipoVenta.Text, lbNivelPrecio.Text);
                        break;
                    case "Retractarse":
                        string mMensaje = "";

                        if (!ws.RetractaCotizacion(mCotizacion, Convert.ToString(Session["Usuario"]), ref mMensaje))
                        {
                            lbError.Text = mMensaje;
                            return;
                        }

                        limpiar(true);
                        lbInfo.Text = mMensaje;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error en la cotización. {0} {1}", ex.Message, m);
            }
        }

        protected void lkAutorizar_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                string mMensaje = ""; string mSolicitud = ""; string mAutorizacion = ""; string mRespuesta = ""; string mQuienAutorizo = ""; string mLink = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.PreAutorizacionInterconsumo(ref mMensaje, "C", txtPedido.Text, ref mSolicitud, ref mAutorizacion, ref mRespuesta, ref mQuienAutorizo, ref mLink))
                {
                    lbError.Text = mMensaje;

                    if (mMensaje == "cliente")
                    {
                        Session["TipoVenta"] = cbTipo.SelectedValue;
                        Session["Financiera"] = cbFinanciera.SelectedValue;
                        Session["NivelPrecio"] = cbNivelPrecio.SelectedValue;
                        Response.Redirect("clientes.aspx");
                    }

                    return;
                }

                if (mSolicitud.Trim().Length > 0)
                {
                    txtSolicitud.Text = mSolicitud;
                    txtPagare.Text = mSolicitud;
                }

                lbEstadoSolicitud.Visible = true;
                lbEstadoSolicitud.Text = mRespuesta;

                if (mRespuesta.Contains("APROBADA"))
                {
                    lkPagareEnLinea.Visible = true;
                    lkSolicitudEnLinea.Visible = true;
                    lbEstadoSolicitud.ForeColor = System.Drawing.Color.Green;
                }

                if (mRespuesta.Contains("PREAUTORIZADA")) lbEstadoSolicitud.ForeColor = System.Drawing.Color.Black;
                if (mRespuesta.Contains("DIFERIDA")) lbEstadoSolicitud.ForeColor = System.Drawing.Color.Gray;
                if (mRespuesta.Contains("ESTADO GRIS")) lbEstadoSolicitud.ForeColor = System.Drawing.Color.Gray;
                if (mRespuesta.Contains("RECHAZADA")) lbEstadoSolicitud.ForeColor = System.Drawing.Color.Red;

                lbInfo.Text = mMensaje;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al autorizar el crédito {0} {1}", ex.Message, m);
            }
        }

        protected void lkSolicitudEnLinea_Click(object sender, EventArgs e)
        {
            Label lbCliente = (Label)gridCotizaciones.SelectedRow.FindControl("lbCliente");
            ImprimirSolicitudCotizacion(txtPedido.Text, "7", lbCliente.Text, cbTipo.SelectedValue.ToString(), cbNivelPrecio.SelectedValue.ToString());
        }

        protected void lkPagareEnLinea_Click(object sender, EventArgs e)
        {
            Label lbCliente = (Label)gridCotizaciones.SelectedRow.FindControl("lbCliente");
            ImprimirPagareCotizacion(txtPedido.Text, "7", lbCliente.Text, cbTipo.SelectedValue.ToString(), cbNivelPrecio.SelectedValue.ToString());
        }

        /// <summary>
        /// El parametro enviar directamente corresponde a pasar o no pasar por el wizard de la huella digital y la foto
        /// </summary>
        /// <param name="blEnviarDirectamente"></param>
        void EnviarSolicitud()
        {
            lbEstadoSolicitud.Visible = false;
            string mMensaje = ""; string mSolicitud = ""; string mAutorizacion = ""; string mRespuesta = ""; string mQuienAutorizo = "";
            try
            {
                
                    if (cbFinanciera.SelectedValue.ToString() == "12")
                    {
                        //Validar datos del cliente
                        WebRequest request = WebRequest.Create(string.Format("{0}/validacionescrediplus/{1}", Convert.ToString(Session["UrlRestServices"]), txtPedido.Text));

                        request.Method = "GET";
                        request.ContentType = "application/json";

                        var response = (HttpWebResponse)request.GetResponse();
                        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                        string mRespuestaValidacion = responseString.ToString().Replace("\"", "");

                        if (mRespuestaValidacion.Length > 2)
                        {
                            lbError.Text = mRespuestaValidacion;
                            return;
                        }
                    }

                    List<Clases.Catalogo> varFinancieras = new List<Clases.Catalogo>();
                    List<Clases.Catalogo> varTiendas = new List<Clases.Catalogo>();
                    lbInfo.Text = "";
                    lbError.Text = "";
                    #region "Huella digital y fotografía"
                    try
                    {

                        varFinancieras = WebRestClient.ObtenerFinancierasConHuellaYFoto(Convert.ToString(Session["UrlRestServices"]));

                        varTiendas = WebRestClient.ObtenerTiendasConHuellaYFoto(Convert.ToString(Session["UrlRestServices"]));
                    }
                    catch
                    {
                    }
                    #endregion

                    //si está habilitado y la tienda está en el catálogo de financieras habilitadas
                    if (varFinancieras.Count > 0 && varFinancieras.Find(x => x.Codigo == cbFinanciera.SelectedItem.Value) != null && varTiendas.Count > 0 && varTiendas.Find(x => x.Codigo == Session["Tienda"].ToString()) != null)
                    {

                        //**
                        //antes de enviar la solicitud, grabar la huella o verificarla
                        //*

                        Enviar_Solicitud_ATID(txtPedido.Text);
                        Mostrar_Respuesta_Solicitud();

                    }
                    else if (varFinancieras.Count > 0 && varFinancieras.Find(x => x.Codigo == cbFinanciera.SelectedItem.Value) != null && varTiendas.Count > 0 && varTiendas.Find(x => x.Codigo == Session["Tienda"].ToString()) == null)
                    {
                        lbError.Text = "Esta tienda no tiene habilitada la opción de solicitud";
                    }
                    else if (cbFinanciera.SelectedValue.Equals("7"))
                    {


                        WebRequest request = WebRequest.Create(string.Format("{0}/PreAutorizacionInterconsumo/?documento={1}", Convert.ToString(Session["UrlRestServices"]), txtPedido.Text));

                        request.Method = "GET";
                        request.ContentType = "application/json";

                        var response = (HttpWebResponse)request.GetResponse();
                        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                        dynamic mInterconsumo = JObject.Parse(responseString);
                        mMensaje = mInterconsumo.mensaje;

                        if (!Convert.ToBoolean(mInterconsumo.exito))
                        {
                            lbError.Text = mMensaje;

                            if (mMensaje == "cliente")
                            {
                                Session["TipoVenta"] = cbTipo.SelectedValue;
                                Session["Financiera"] = cbFinanciera.SelectedValue;
                                Session["NivelPrecio"] = cbNivelPrecio.SelectedValue;
                                Response.Redirect("clientes.aspx");
                            }

                            return;
                        }
                        mSolicitud = mInterconsumo.solicitud;
                        mAutorizacion = mInterconsumo.autorizacion;
                        mRespuesta = mInterconsumo.resultadoSolicitud;
                        mQuienAutorizo = mInterconsumo.quienAutorizo;
                    }
                

                if (mSolicitud.Trim().Length > 0)
                {
                    txtSolicitud.Text = mSolicitud;
                    txtPagare.Text = mSolicitud;
                    txtNoAutorizacion.Text = mAutorizacion;
                    txtQuienAutorizo.Text = mQuienAutorizo;
                }

                lbEstadoSolicitud.Visible = true;
                lbEstadoSolicitud.Text = mRespuesta;
                string mMensaje2 = "";
                if (mRespuesta.Contains("GRABADA"))
                {
                    lkPagareEnLinea.Visible = true;
                    lkSolicitudEnLinea.Visible = true;
                    lbEstadoSolicitud.ForeColor = System.Drawing.Color.Green;
                }

                lbInfo.Text = string.Format("{0}{1}", mMensaje, mMensaje2);


            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al enviar la solicitud a la financiera {0} {1}", ex.Message, m);
            }
        }

        

        protected void btnInterconsumo_Click(object sender, EventArgs e)
        {
           
                EnviarSolicitud();
            
        }

        protected void btnPortal_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                WebRequest request = WebRequest.Create(string.Format("{0}/LinkInterconsumo/?documento={1}", Convert.ToString(Session["UrlRestServices"]), txtPedido.Text));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                dynamic mLinkInterconsumo = JObject.Parse(responseString);

                if (!Convert.ToBoolean(mLinkInterconsumo.exito))
                {
                    lbError.Text = mLinkInterconsumo.mensaje;
                    return;
                }

                string mLink = mLinkInterconsumo.link;

                string mArchivo = string.Format("{0}_{1}.mf", txtPedido.Text, txtCodigo.Text);
                System.IO.File.WriteAllText(@"C:\reportes\" + mArchivo, mLink);

                Response.Clear();
                Response.ContentType = "application/mf";

                Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mArchivo);
                Response.WriteFile(@"C:\reportes\" + mArchivo);
                Response.End();

                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al abrir el portal {0} {1}", ex.Message, m);
            }
        }

        protected void btnVerificar_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                lbEstadoSolicitud.Visible = false;
                string mMensaje = ""; string mMensaje2 = ""; string mAutorizacion = ""; string mQuienAutorizo = ""; string mNombre = "";

                WebRequest request = WebRequest.Create(string.Format("{0}/ConsultaInterconsumo/?documento={1}", Convert.ToString(Session["UrlRestServices"]), txtPedido.Text));

                request.Method = "GET";
                request.ContentType = "application/json";                

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                dynamic mConsultaInterconsumo = JObject.Parse(responseString);

                if (!Convert.ToBoolean(mConsultaInterconsumo.exito))
                {
                    lbError.Text = mConsultaInterconsumo.mensaje;
                    return;
                }

                mMensaje = mConsultaInterconsumo.mensaje;
                mMensaje2 = mConsultaInterconsumo.mensaje2;
                mAutorizacion = mConsultaInterconsumo.autorizacion;
                mQuienAutorizo = mConsultaInterconsumo.quienAutorizo;
                mNombre = mConsultaInterconsumo.nombre;

                lbEstadoSolicitud.Visible = true;
                lbEstadoSolicitud.Text = mMensaje;

                lbInfo.Text = mMensaje2;
                lkSolicitudEnLinea.Visible = true;
                lkPagareEnLinea.Visible = true;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al verificar el estado de la solicitud {0} {1}", ex.Message, m);
            }
        }

        protected void btnDeclaracion_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                WebRequest request = WebRequest.Create(string.Format("{0}/DeclaracionIngresosInterconsumo/?documento={1}", Convert.ToString(Session["UrlRestServices"]), txtPedido.Text));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                dynamic mLinkInterconsumo = JObject.Parse(responseString);

                if (!Convert.ToBoolean(mLinkInterconsumo.exito))
                {
                    lbError.Text = mLinkInterconsumo.mensaje;
                    return;
                }

                string mLink = mLinkInterconsumo.link;

                string mArchivo = string.Format("{0}_{1}.mf", txtPedido.Text, txtCodigo.Text);
                System.IO.File.WriteAllText(@"C:\reportes\" + mArchivo, mLink);

                Response.Clear();
                Response.ContentType = "application/mf";

                Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mArchivo);
                Response.WriteFile(@"C:\reportes\" + mArchivo);
                Response.End();

                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error en la declaración de ingresos {0} {1}", ex.Message, m);
            }
        }

        protected void btnRefreshSolCredito_Click(object sender, EventArgs e)
        {
            RespuestaSolicitud oRespuesta = new RespuestaSolicitud();
            if (txtPedido.Text != "" && txtPedido.Text.ToUpper().Contains("P"))
            {
                string urlRest = Convert.ToString(Session["UrlRestServices"]);
                if (urlRest != string.Empty)
                {
                    var client = new RestClient(urlRest);
                    var restRequest = new RestRequest("api/RespuestaSolicitud/" + txtPedido.Text, Method.GET)
                    {
                        RequestFormat = DataFormat.Json
                    };

                    var result = client.Get(restRequest);
                    if (result != null)
                    {
                        oRespuesta = JsonConvert.DeserializeObject<RespuestaSolicitud>(result.Content);
                        if (oRespuesta.MensajeError == null)
                        {
                            txtSolicitud.Text = oRespuesta.NumeroSolicitud;
                            txtPagare.Text = oRespuesta.NumeroSolicitud;
                            txtNoAutorizacion.Text = oRespuesta.NumeroCredito;
                            txtQuienAutorizo.Text = "";

                            lbEstadoSolicitud.Visible = true;
                        }
                        else
                        {
                            lbLinkInterconsumo.Text = oRespuesta.MensajeError;
                            lbEstadoSolicitud.Visible = false;
                        }

                    }
                }
            }
        }

        private void Enviar_Solicitud_ATID(string pedido)
        {
            try
            {

                var client = new RestClient(string.Format("{0}/SolicitudCredito/", Convert.ToString(Session["UrlRestServices"])));
                var request = new RestRequest("/EnviarSolicitud/", Method.POST);

                request.ReadWriteTimeout = 36000000;
                request.AddParameter("application/json", txtPedido.Text.Trim(), RestSharp.ParameterType.RequestBody);
                ////----------------------------------------------------------------------
                ////Respuesta de parte del servicio devolviendo un objeto UsuarioPDVDto e
                ////en el atributo response.Data
                log.Info(string.Format("Envio solicitud atid: {0}", txtPedido.Text.Trim()));

                //IRestResponse<bool> response = client.Execute<bool>(request);

                client.ExecuteAsync(request, response1 =>
                {
                    callback(response1.Content);
                });


            }
            catch (Exception ex)
            {
                lbError.Text = CatchClass.ExMessage(ex, "pedidos.aspx", "Enviar_Solicitud_ATID");
            }
        }

        private void Mostrar_Respuesta_Solicitud()
        {
            if (Session["delay"] != null)
                Session["delay"] = int.Parse(Session["delay"].ToString()) + 1;
            else
                Session["delay"] = 1;

            if (Session["Data"] == null)
            {
                //no más de 15 ciclos (75 segundos de espera)
                if (int.Parse(Session["delay"].ToString()) < 15)
                {
                    Thread.Sleep(5000);
                    Mostrar_Respuesta_Solicitud();
                }
            }
            else
                Response.Redirect("~/cotizaciones.aspx?ErrorMsg2=" + Session["Data"].ToString());
        }

        private void callback(string content)
        {
            ThreadStart childthreat = new ThreadStart(() => RespuestaSolicitud(content));
            Thread child = new Thread(childthreat);

            child.Start();
           
        }
        private void RespuestaSolicitud(string content)
        {
            try
            {
                lbError.Visible = true;
                lbInfo.Visible = true;
                string resultado = content;
                Respuesta oResp1 = JsonConvert.DeserializeObject<Respuesta>(content);
                if (oResp1.Objeto != null)
                {
                    RespuestaSolicitud oResp = new RespuestaSolicitud();

                    oResp = JsonConvert.DeserializeObject<RespuestaSolicitud>(oResp1.Objeto.ToString());

                    if (oResp.MensajeError.Length > 0)
                    {
                        if(oResp.MensajeError.Contains("An error occurred while sending the request"))
                            lbError.Text = "Ocurrió un inconveniente con el servicio de CREDIPLUS, por favor intente nuevamente en unos minutos.";
                        else
                            lbError.Text = "Ocurrió un error con el servicio de CREDIPLUS: [" + oResp.MensajeError + "]";
                        Session["Data"] = oResp.MensajeError;
                    }
                    else
                    {
                        if (oResp.MensajeError.Length == 0)
                        {
                            if (oResp.ResultadoPrecalificacion == "Prohibitivo")
                            {
                                lbError.Text = "En este momento CrediPlus no le puede otorgar crédito.";
                                Session["Data"] = lbError.Text;
                            }
                            else if (oResp.ResultadoPrecalificacion.ToLower().Equals("aceptable") || oResp.ResultadoPrecalificacion.ToLower().Equals("optimo"))
                            {
                                lbInfo.Text = "Tu solicitud ha sido PRE-AUTORIZADA, favor de completar la papelería del cliente. CRÉDITO No. " + oResp.NumeroCredito + " | SOLICITUD No. " + oResp.NumeroSolicitud;
                                Session["Data"] = lbInfo.Text;
                            }
                            else
                                Session["Data"] = oResp.ResultadoPrecalificacion;
                        }
                    }
                }
                else
                    if (oResp1.Mensaje != null)
                {
                    lbInfo.Text = oResp1.Mensaje;
                    Session["Data"] = oResp1.Mensaje;
                }

            }
            catch (Exception ex)
            {
                lbError.Text = "Error con solcitud: " + ex.Message;
            }
        }

        protected void btnHuellayFoto_Click(object sender, EventArgs e)
        {
            string s = "<script language=JavaScript> window.open('" + Session["UrlFiestaNetERP"].ToString() + "/IdentidadCliente/" + txtPedido.Text.Trim() + "/cotizaciones'); </script>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "biometricos", s);
        }

        protected void lkAccesorios_Click(object sender, EventArgs e)
        {
            try
            {
                lbError.Text = "";
                lbInfo.Text = "";

                if (Convert.ToString(ViewState["Accesorios"]) == "S")
                {
                    lbError.Text = "La Fiesta de Accesorios ya fue aplicada en esta cotización";
                    return;
                }

                List<wsPuntoVenta.PedidoLinea> qOriginal = new List<wsPuntoVenta.PedidoLinea>();
                qOriginal = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                wsPuntoVenta.PedidoLinea[] q = new wsPuntoVenta.PedidoLinea[1];
                wsPuntoVenta.PedidoLinea[] qLinea = new wsPuntoVenta.PedidoLinea[1];

                int jj = 0;
                for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                {
                    jj++;

                    Label lbPrecioUnitario = (Label)gridArticulos.Rows[ii].FindControl("lbPrecio");
                    Label lbCantidadPedida = (Label)gridArticulos.Rows[ii].FindControl("lbCantidad");
                    Label lbPrecioTotal = (Label)gridArticulos.Rows[ii].FindControl("lbTotal");
                    Label lbDescripcion = (Label)gridArticulos.Rows[ii].FindControl("lbDescripcion");
                    Label lbComentario = (Label)gridArticulos.Rows[ii].FindControl("lbComentario");
                    Label lbEstado = (Label)gridArticulos.Rows[ii].FindControl("lbEstado");
                    Label lbNumeroPedido = (Label)gridArticulos.Rows[ii].FindControl("lbNumeroPedido");
                    Label lbOferta = (Label)gridArticulos.Rows[ii].FindControl("lbOferta");
                    Label lbFechaOfertaDesde = (Label)gridArticulos.Rows[ii].FindControl("lbFechaOfertaDesde");
                    Label lbPrecioOriginal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioOriginal");
                    Label lbEsDetalleKit = (Label)gridArticulos.Rows[ii].FindControl("lbEsDetalleKit");
                    Label lbPrecioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioFacturar");
                    Label lbTipoOferta = (Label)gridArticulos.Rows[ii].FindControl("lbTipoOferta");
                    Label lbVale = (Label)gridArticulos.Rows[ii].FindControl("lbVale");
                    Label lbAutorizacion = (Label)gridArticulos.Rows[ii].FindControl("lbAutorizacion");
                    Label lbBodega = (Label)gridArticulos.Rows[ii].FindControl("lbBodega");
                    Label lbLocalizacion = (Label)gridArticulos.Rows[ii].FindControl("lbLocalizacion");
                    LinkButton lkRequisicionArmado = (LinkButton)gridArticulos.Rows[ii].FindControl("lkRequisicionArmado");
                    Label lbArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbArticulo");
                    Label lbNombreArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbNombreArticulo");
                    Label lbPrecioUnitarioDebioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioUnitarioDebioFacturar");
                    Label lbPrecioSugerido = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioSugerido");
                    Label lbPrecioBaseLocal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioBaseLocal");
                    Label lbGel = (Label)gridArticulos.Rows[ii].FindControl("lbGel");
                    Label lbLinea = (Label)gridArticulos.Rows[ii].FindControl("lbLinea");
                    Label lbPorcentajeDescuento = (Label)gridArticulos.Rows[ii].FindControl("lbPorcentajeDescuento");
                    Label lbDescuento = (Label)gridArticulos.Rows[ii].FindControl("lbDescuento");
                    Label lbBeneficiario = (Label)gridArticulos.Rows[ii].FindControl("lbBeneficiario");

                    decimal mPrecioOriginal = 0;
                    DateTime mFechaOfertaDesde = new DateTime(1980, 1, 1);

                    try
                    {
                        mFechaOfertaDesde = new DateTime(Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(6, 4)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(3, 2)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(0, 2)));
                    }
                    catch
                    {
                        //Nothing
                    }
                    try
                    {
                        mPrecioOriginal = Convert.ToDecimal(lbPrecioOriginal.Text.Replace(",", ""));
                    }
                    catch
                    {
                        mPrecioOriginal = 0;
                    }

                    wsPuntoVenta.PedidoLinea itemPedidoLinea = new wsPuntoVenta.PedidoLinea();
                    itemPedidoLinea.Articulo = lbArticulo.Text;
                    itemPedidoLinea.Nombre = lbNombreArticulo.Text;
                    itemPedidoLinea.PrecioUnitario = Convert.ToDecimal(lbPrecioUnitario.Text.Replace(",", ""));
                    itemPedidoLinea.CantidadPedida = Convert.ToDecimal(lbCantidadPedida.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioTotal = Convert.ToDecimal(lbPrecioTotal.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioFacturar = Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.Bodega = lbBodega.Text;
                    itemPedidoLinea.Localizacion = lbLocalizacion.Text;
                    itemPedidoLinea.RequisicionArmado = lkRequisicionArmado.Text;
                    itemPedidoLinea.Descripcion = lbDescripcion.Text;
                    itemPedidoLinea.Comentario = lbComentario.Text;
                    itemPedidoLinea.Estado = lbEstado.Text;
                    itemPedidoLinea.NumeroPedido = lbNumeroPedido.Text;
                    itemPedidoLinea.Oferta = lbOferta.Text;
                    itemPedidoLinea.FechaOfertaDesde = mFechaOfertaDesde;
                    itemPedidoLinea.PrecioOriginal = mPrecioOriginal;
                    itemPedidoLinea.TipoOferta = lbTipoOferta.Text;
                    itemPedidoLinea.EsDetalleKit = lbEsDetalleKit.Text;
                    itemPedidoLinea.Vale = Convert.ToInt32(lbVale.Text == "" ? "0" : lbVale.Text);
                    itemPedidoLinea.Autorizacion = Convert.ToInt64(lbAutorizacion.Text == "" ? "0" : lbAutorizacion.Text);
                    itemPedidoLinea.PrecioUnitarioDebioFacturar = Convert.ToDecimal(lbPrecioUnitarioDebioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioSugerido = Convert.ToDecimal(lbPrecioSugerido.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioBaseLocal = Convert.ToDouble(lbPrecioBaseLocal.Text.Replace(",", ""));
                    itemPedidoLinea.Gel = lbGel.Text;
                    itemPedidoLinea.Linea = Convert.ToInt32(lbLinea.Text);
                    itemPedidoLinea.PorcentajeDescuento = Convert.ToDecimal(lbPorcentajeDescuento.Text);
                    itemPedidoLinea.Beneficiario = lbBeneficiario.Text;
                    itemPedidoLinea.NetoFacturar = Convert.ToDecimal(lbPrecioTotal.Text.Replace(",", "")) - itemPedidoLinea.Descuento;
                    itemPedidoLinea.Descuento= Convert.ToDecimal(lbDescuento.Text.Replace(",", ""));
                    if (jj > 1) Array.Resize(ref q, jj);
                    if (jj > 1) Array.Resize(ref qLinea, jj);

                    q[jj - 1] = itemPedidoLinea;
                    qLinea[jj - 1] = itemPedidoLinea;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = ""; string mPrecioBien = ""; string mMonto = ""; string mNotas = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.FiestaDeAccesorios(ref qLinea, ref mPrecioBien, ref mMonto, cbNivelPrecio.Text, ref mMensaje, ref mNotas, "", txtPedido.Text, false))
                {
                    lbError.Visible = true;
                    lbError.Text = mMensaje;
                    return;
                }

                string mSeparador = "";
                if (txtNotasTipoVenta.Text.Trim().Length > 0) mSeparador = " - ";
                txtNotasTipoVenta.Text = string.Format("{0}{1}{2}", txtNotasTipoVenta.Text, mSeparador, mNotas);

                gridArticulos.DataSource = qLinea;
                gridArticulos.DataBind();

                ViewState["Accesorios"] = "S";
                ViewState["qArticulos"] = qLinea;

                var Descuentos = qLinea.Sum(x => x.Descuento);

                if (cbFinanciera.SelectedValue == "1" || cbFinanciera.SelectedValue == "5" || cbFinanciera.SelectedValue == "9")
                {
                    txtMonto.Text = mMonto;
                    txtFacturar.Text = String.Format("{0:0,0.00}", (Convert.ToDecimal(mMonto.Replace(",", "")) + Descuentos));
                    txtDescuentos.Text = String.Format("{0:0,0.00}", Descuentos);
                }

                if (ws.EsBanco(cbFinanciera.SelectedValue.ToString()))
                {
                    double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                    decimal mEnganche = Convert.ToDecimal(txtEnganche.Text.Trim().Replace(",", ""));
                    decimal mSaldoFinanciar = Convert.ToDecimal(mPrecioBien.Replace(",", "")) - mEnganche - Descuentos;
                    decimal mMontoTotal = Convert.ToDecimal(Math.Round((double)mSaldoFinanciar * mFactor, 2, MidpointRounding.AwayFromZero));
                    decimal mRecargos = mMontoTotal - mSaldoFinanciar;

                    txtFacturar.Text = mPrecioBien;
                    txtSaldoFinanciar.Text = String.Format("{0:0,0.00}", mSaldoFinanciar);
                    txtIntereses.Text = String.Format("{0:0,0.00}", mRecargos);
                    txtMonto.Text = String.Format("{0:0,0.00}", mMontoTotal);
                    txtDescuentos.Text = String.Format("{0:0,0.00}", Descuentos);
                    Int32 mPagos; string mPagos1;
                    string mPagosTexto = ws.DevuelvePagosNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

                    try
                    {
                        mPagos = Convert.ToInt32(mPagosTexto.Substring(0, 2).Replace(" ", ""));
                    }
                    catch
                    {
                        mPagos = 1;
                    }

                    decimal mCuotas = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) / mPagos;

                    if (mPagos <= 10)
                    {
                        mPagos1 = string.Format("0{0}", mPagos - 1);
                    }
                    else
                    {
                        mPagos1 = (mPagos - 1).ToString();
                    }

                    cbPagos1.SelectedValue = mPagos1;
                    cbPagos2.SelectedValue = "01";

                    if (mCuotas.ToString().Substring(mCuotas.ToString().Length - 2, 2) == "00")
                    {
                        txtPagos1.Text = String.Format("{0:0,0.00}", mCuotas);
                        txtPagos2.Text = String.Format("{0:0,0.00}", mCuotas);
                    }
                    else
                    {
                        #region "aproximaciones para imprimir documentos de financieras"
                        if (cbFinanciera.SelectedValue == Const.FINANCIERA.BANCREDIT)
                        {
                            txtPagos1.Text = String.Format("{0:0,0.00}", Math.Round(mCuotas, 0, MidpointRounding.AwayFromZero));
                            decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (Math.Round(mCuotas, 0, MidpointRounding.AwayFromZero) * (mPagos - 1));
                            txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
                        }
                        else
                        {
                            string[] mCuotasString = mCuotas.ToString().Split(new string[] { "." }, StringSplitOptions.None);

                            Int32 mCuota = 1 + Convert.ToInt32(mCuotasString[0]);
                            decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (mCuota * (mPagos - 1));

                            txtPagos1.Text = String.Format("{0:0,0.00}", mCuota);
                            txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
                        }
                        #endregion
                    }
                }

                lbInfo.Text = mMensaje;
            }
            catch (Exception ex)
            {
                lbError.Text = CatchClass.ExMessage(ex, "pedidos.aspx", "lkAccesorios_Click");
            }
        }
        //protected void lkAccesorios_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        lbError.Text = "";
        //        lbInfo.Text = "";

        //        if (Convert.ToString(ViewState["Accesorios"]) == "S")
        //        {
        //            lbError.Text = "La Fiesta de Accesorios ya fue aplicada en esta cotización";
        //            return;
        //        }

        //        List<wsPuntoVenta.PedidoLinea> qOriginal = new List<wsPuntoVenta.PedidoLinea>();
        //        qOriginal = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

        //        wsPuntoVenta.PedidoLinea[] q = new wsPuntoVenta.PedidoLinea[1];
        //        wsPuntoVenta.PedidoLinea[] qLinea = new wsPuntoVenta.PedidoLinea[1];

        //        int jj = 0;
        //        for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
        //        {
        //            jj++;

        //            Label lbPrecioUnitario = (Label)gridArticulos.Rows[ii].FindControl("lbPrecio");
        //            Label lbCantidadPedida = (Label)gridArticulos.Rows[ii].FindControl("lbCantidad");
        //            Label lbPrecioTotal = (Label)gridArticulos.Rows[ii].FindControl("lbTotal");
        //            Label lbDescripcion = (Label)gridArticulos.Rows[ii].FindControl("lbDescripcion");
        //            Label lbComentario = (Label)gridArticulos.Rows[ii].FindControl("lbComentario");
        //            Label lbEstado = (Label)gridArticulos.Rows[ii].FindControl("lbEstado");
        //            Label lbNumeroPedido = (Label)gridArticulos.Rows[ii].FindControl("lbNumeroPedido");
        //            Label lbOferta = (Label)gridArticulos.Rows[ii].FindControl("lbOferta");
        //            Label lbFechaOfertaDesde = (Label)gridArticulos.Rows[ii].FindControl("lbFechaOfertaDesde");
        //            Label lbPrecioOriginal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioOriginal");
        //            Label lbEsDetalleKit = (Label)gridArticulos.Rows[ii].FindControl("lbEsDetalleKit");
        //            Label lbPrecioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioFacturar");
        //            Label lbTipoOferta = (Label)gridArticulos.Rows[ii].FindControl("lbTipoOferta");
        //            Label lbVale = (Label)gridArticulos.Rows[ii].FindControl("lbVale");
        //            Label lbAutorizacion = (Label)gridArticulos.Rows[ii].FindControl("lbAutorizacion");
        //            Label lbBodega = (Label)gridArticulos.Rows[ii].FindControl("lbBodega");
        //            Label lbLocalizacion = (Label)gridArticulos.Rows[ii].FindControl("lbLocalizacion");
        //            LinkButton lkRequisicionArmado = (LinkButton)gridArticulos.Rows[ii].FindControl("lkRequisicionArmado");
        //            Label lbArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbArticulo");
        //            Label lbNombreArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbNombreArticulo");
        //            Label lbPrecioUnitarioDebioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioUnitarioDebioFacturar");
        //            Label lbPrecioSugerido = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioSugerido");
        //            Label lbPrecioBaseLocal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioBaseLocal");
        //            Label lbGel = (Label)gridArticulos.Rows[ii].FindControl("lbGel");
        //            Label lbLinea = (Label)gridArticulos.Rows[ii].FindControl("lbLinea");
        //            Label lbPorcentajeDescuento = (Label)gridArticulos.Rows[ii].FindControl("lbPorcentajeDescuento");
        //            Label lbBeneficiario = (Label)gridArticulos.Rows[ii].FindControl("lbBeneficiario");

        //            decimal mPrecioOriginal = 0;
        //            DateTime mFechaOfertaDesde = new DateTime(1980, 1, 1);

        //            try
        //            {
        //                mFechaOfertaDesde = new DateTime(Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(6, 4)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(3, 2)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(0, 2)));
        //            }
        //            catch
        //            {
        //                //Nothing
        //            }
        //            try
        //            {
        //                mPrecioOriginal = Convert.ToDecimal(lbPrecioOriginal.Text.Replace(",", ""));
        //            }
        //            catch
        //            {
        //                mPrecioOriginal = 0;
        //            }

        //            wsPuntoVenta.PedidoLinea itemPedidoLinea = new wsPuntoVenta.PedidoLinea();
        //            itemPedidoLinea.Articulo = lbArticulo.Text;
        //            itemPedidoLinea.Nombre = lbNombreArticulo.Text;
        //            itemPedidoLinea.PrecioUnitario = Convert.ToDecimal(lbPrecioUnitario.Text.Replace(",", ""));
        //            itemPedidoLinea.CantidadPedida = Convert.ToDecimal(lbCantidadPedida.Text.Replace(",", ""));
        //            itemPedidoLinea.PrecioTotal = Convert.ToDecimal(lbPrecioTotal.Text.Replace(",", ""));
        //            itemPedidoLinea.PrecioFacturar = Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", ""));
        //            itemPedidoLinea.Bodega = lbBodega.Text;
        //            itemPedidoLinea.Localizacion = lbLocalizacion.Text;
        //            itemPedidoLinea.RequisicionArmado = lkRequisicionArmado.Text;
        //            itemPedidoLinea.Descripcion = lbDescripcion.Text;
        //            itemPedidoLinea.Comentario = lbComentario.Text;
        //            itemPedidoLinea.Estado = lbEstado.Text;
        //            itemPedidoLinea.NumeroPedido = lbNumeroPedido.Text;
        //            itemPedidoLinea.Oferta = lbOferta.Text;
        //            itemPedidoLinea.FechaOfertaDesde = mFechaOfertaDesde;
        //            itemPedidoLinea.PrecioOriginal = mPrecioOriginal;
        //            itemPedidoLinea.TipoOferta = lbTipoOferta.Text;
        //            itemPedidoLinea.EsDetalleKit = lbEsDetalleKit.Text;
        //            itemPedidoLinea.Vale = Convert.ToInt32(lbVale.Text == "" ? "0" : lbVale.Text);
        //            itemPedidoLinea.Autorizacion = Convert.ToInt64(lbAutorizacion.Text == "" ? "0" : lbAutorizacion.Text);
        //            itemPedidoLinea.PrecioUnitarioDebioFacturar = Convert.ToDecimal(lbPrecioUnitarioDebioFacturar.Text.Replace(",", ""));
        //            itemPedidoLinea.PrecioSugerido = Convert.ToDecimal(lbPrecioSugerido.Text.Replace(",", ""));
        //            itemPedidoLinea.PrecioBaseLocal = Convert.ToDouble(lbPrecioBaseLocal.Text.Replace(",", ""));
        //            itemPedidoLinea.Gel = lbGel.Text;
        //            itemPedidoLinea.Linea = Convert.ToInt32(lbLinea.Text);
        //            itemPedidoLinea.PorcentajeDescuento = Convert.ToDecimal(lbPorcentajeDescuento.Text);
        //            itemPedidoLinea.Beneficiario = lbBeneficiario.Text;

        //            if (jj > 1) Array.Resize(ref q, jj);
        //            if (jj > 1) Array.Resize(ref qLinea, jj);

        //            q[jj - 1] = itemPedidoLinea;
        //            qLinea[jj - 1] = itemPedidoLinea;
        //        }

        //        var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = ""; string mPrecioBien = ""; string mMonto = ""; string mNotas = "";
        //        if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

        //        if (!ws.FiestaDeAccesorios(ref qLinea, ref mPrecioBien, ref mMonto, cbNivelPrecio.Text, ref mMensaje, ref mNotas, "", txtPedido.Text, false))
        //        {
        //            lbError.Visible = true;
        //            lbError.Text = mMensaje;
        //            return;
        //        }

        //        string mSeparador = "";
        //        if (txtNotasTipoVenta.Text.Trim().Length > 0) mSeparador = " - ";
        //        txtNotasTipoVenta.Text = string.Format("{0}{1}{2}", txtNotasTipoVenta.Text, mSeparador, mNotas);

        //        gridArticulos.DataSource = qLinea;
        //        gridArticulos.DataBind();

        //        ViewState["Accesorios"] = "S";
        //        ViewState["qArticulos"] = qLinea;

        //        if (cbFinanciera.SelectedValue == "1" || cbFinanciera.SelectedValue == "5" || cbFinanciera.SelectedValue == "9")
        //        {
        //            txtMonto.Text = mMonto;
        //            txtFacturar.Text = mMonto;
        //        }

        //        if (ws.EsBanco(cbFinanciera.SelectedValue.ToString()))
        //        {
        //            double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
        //            decimal mEnganche = Convert.ToDecimal(txtEnganche.Text.Trim().Replace(",", ""));
        //            decimal mSaldoFinanciar = Convert.ToDecimal(mPrecioBien.Replace(",", "")) - mEnganche;
        //            decimal mMontoTotal = Convert.ToDecimal(Math.Round((double)mSaldoFinanciar * mFactor, 2, MidpointRounding.AwayFromZero));
        //            decimal mRecargos = mMontoTotal - mSaldoFinanciar;

        //            txtFacturar.Text = mPrecioBien;
        //            txtSaldoFinanciar.Text = String.Format("{0:0,0.00}", mSaldoFinanciar);
        //            txtIntereses.Text = String.Format("{0:0,0.00}", mRecargos);
        //            txtMonto.Text = String.Format("{0:0,0.00}", mMontoTotal);

        //            Int32 mPagos; string mPagos1;
        //            string mPagosTexto = ws.DevuelvePagosNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

        //            try
        //            {
        //                mPagos = Convert.ToInt32(mPagosTexto.Substring(0, 2).Replace(" ", ""));
        //            }
        //            catch
        //            {
        //                mPagos = 1;
        //            }

        //            decimal mCuotas = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) / mPagos;

        //            if (mPagos <= 10)
        //            {
        //                mPagos1 = string.Format("0{0}", mPagos - 1);
        //            }
        //            else
        //            {
        //                mPagos1 = (mPagos - 1).ToString();
        //            }

        //            cbPagos1.SelectedValue = mPagos1;
        //            cbPagos2.SelectedValue = "01";

        //            if (mCuotas.ToString().Substring(mCuotas.ToString().Length - 2, 2) == "00")
        //            {
        //                txtPagos1.Text = String.Format("{0:0,0.00}", mCuotas);
        //                txtPagos2.Text = String.Format("{0:0,0.00}", mCuotas);
        //            }
        //            else
        //            {
        //                #region "aproximaciones para imprimir documentos de financieras"
        //                if (cbFinanciera.SelectedValue == Const.FINANCIERA.BANCREDIT)
        //                {
        //                    txtPagos1.Text = String.Format("{0:0,0.00}", Math.Round(mCuotas, 0, MidpointRounding.AwayFromZero));
        //                    decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (Math.Round(mCuotas, 0, MidpointRounding.AwayFromZero) * (mPagos - 1));
        //                    txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
        //                }
        //                else
        //                {
        //                    string[] mCuotasString = mCuotas.ToString().Split(new string[] { "." }, StringSplitOptions.None);

        //                    Int32 mCuota = 1 + Convert.ToInt32(mCuotasString[0]);
        //                    decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (mCuota * (mPagos - 1));

        //                    txtPagos1.Text = String.Format("{0:0,0.00}", mCuota);
        //                    txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
        //                }
        //                #endregion
        //            }
        //        }

        //        lbInfo.Text = mMensaje;
        //    }
        //    catch (Exception ex)
        //    {
        //        lbError.Text = CatchClass.ExMessage(ex, "pedidos.aspx", "lkAccesorios_Click");
        //    }
        //}

        protected void txtDescuentoItem_TextChanged(object sender, EventArgs e)
        {
            lbError2.Text = "";
            lbError.Visible = false;
            decimal descMax = decimal.Parse(txtSaldoDescuento.Text.Replace(",", ""));
            decimal descuento = decimal.Parse(txtDescuentoItem.Text.Replace(",", ""));
            if (descuento > (descMax))
            {
                lbError2.Visible = true;
                lbError2.Text = "El valor del descuento no puede ser mayor a Q" + txtDescMax.Text;
                txtDescuentoItem.Text = "0.00";
                txtDescuentoItem.Focus();
                return;
            }
            if (descuento < 0)
            {
                lbError2.Visible = true;
                lbError2.Text = "El valor del descuento no puede ser menor a cero.";
                txtDescuentoItem.Focus();
                return;
            }

            List<NuevoPrecioProducto> lstProd = (List<NuevoPrecioProducto>)Session["lstNuevoPrecioProducto"];
            if (lstProd != null)
            {
                if (lstProd.Count > 0)
                {
                    lstProd[lstProd.Count - 1].price = decimal.Parse(txtTotal.Text.Replace(",", "")) - decimal.Parse(txtDescuentoItem.Text.Replace(",", ""));
                    Session["lstNuevoPrecioProducto"] = lstProd;
                    // txtDescMax.Text = String.Format("{0:0,0.00}", decimal.Parse(txtDescMax.Text.Replace(",",""))-decimal.Parse(txtDescuentoItem.Text.Replace(",","")));
                    txtSaldoDescuento.Text = String.Format("{0:0,0.00}", decimal.Parse(txtSaldoDescuento.Text.Replace(",", "")) - (decimal.Parse(txtDescuentoItem.Text.Replace(",", ""))));

                }
            }
            txtSaldoDescuento.Text = String.Format("{0:0,0.00}", Facturacion.ObtenerSaldoDescuento(lstProd));
        }
    }

}
