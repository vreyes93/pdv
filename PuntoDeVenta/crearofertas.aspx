﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="crearofertas.aspx.cs" Inherits="PuntoDeVenta.crearofertas" MaintainScrollPositionOnPostback="true" %>
<%@ Register assembly="DevExpress.Web.v16.2, Version=16.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>&nbsp;&nbsp;Publicación de Ofertas</h3>
    <div class="content2">
    <div class="clientes">
        <ul>
            <li>
                <table align="center" runat="server" id="tabla1">
                    <tr>
                        <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            Tipo de Oferta:</td>
                        <td>
                            <asp:RadioButton ID="rbPrecioBase" runat="server" GroupName="TipoOferta" 
                                Text="Al Precio de Lista" 
                                
                                ToolTip="Seleccione esta opción para que la oferta sea aplicada al precio de lista de un artículo específico, es decir que se afectarán todos los niveles de precio de dicho artículo" 
                                Checked="True" AutoPostBack="True" 
                                oncheckedchanged="rbPrecioBase_CheckedChanged" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rbCondicional" runat="server" AutoPostBack="True" 
                                GroupName="TipoOferta" TabIndex="5" Text="Condicional" 
                                
                                ToolTip="Seleccione esta opción si la oferta del artículo estará condicionada a la compra previa de otro(s) artículo(s)" 
                                oncheckedchanged="rbCondicional_CheckedChanged" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rbContado" runat="server" GroupName="TipoOferta" 
                                Text="Contado" 
                                
                                ToolTip="Seleccione esta opción para afectar en un porcentaje de descuento el precio de lista de los artículos seleccionados en todos los niveles de contado" 
                                AutoPostBack="True" oncheckedchanged="rbContado_CheckedChanged" 
                                TabIndex="10" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rbCredito" runat="server" GroupName="TipoOferta" 
                                Text="Crédito" 
                                
                                ToolTip="Seleccione esta opción para afectar en un porcentaje de descuento el precio de lista de los artículos seleccionados en los niveles de crédito que haya marcado" 
                                AutoPostBack="True" oncheckedchanged="rbCredito_CheckedChanged" 
                                TabIndex="15" />
                        </td>
                        <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                </table>
            </li>
            <li>
                    <table align="center" runat="server" id="tablaArticulo">
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                Artículo:</td>
                            <td>
                                <asp:TextBox ID="articulo" runat="server" Width="120px" AutoPostBack="True" style="text-transform: uppercase;"  
                                    ontextchanged="articulo_TextChanged" TabIndex="17"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="descripcion" runat="server" Width="340px" 
                                    BackColor="AliceBlue" Font-Bold="True" ReadOnly="True" TabIndex="5" 
                                    ToolTip="Nothing"></asp:TextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lbBuscarArticuloOferta" runat="server" 
                                    onclick="lbBuscarArticuloOferta_Click" TabIndex="19">Buscar Artículo</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                        </table>
            </li>
            <li>

                <table id="tablaBusqueda" runat="server" align="center">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            Artículo:</td>
                        <td>
                            <asp:TextBox ID="codigoArticulo" runat="server" TabIndex="20"></asp:TextBox>
                        </td>
                        <td>
                            Nombre Artículo:</td>
                        <td>
                            <asp:TextBox ID="descripcionArticulo" runat="server" TabIndex="30" 
                                MaxLength="100" Width="380px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" 
                                ToolTip="Busca artículos según el criterio de búsqueda ingresado" 
                                onclick="btnBuscar_Click" TabIndex="40" />
                            </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td colspan="5" style="text-align: center">
                            <asp:Label ID="Label1" runat="server" ForeColor="Red" TabIndex="610"></asp:Label>
                            <asp:Label ID="Label5" runat="server" TabIndex="620"></asp:Label>
                            <asp:LinkButton ID="lbCerrarBusqueda" runat="server" 
                                ToolTip="Cierra la ventana de búsqueda" onclick="lbCerrarBusqueda_Click" 
                                TabIndex="60">Cerrar Búsqueda</asp:LinkButton>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                </table>
            </li>
            <li>
                <asp:GridView ID="gridArticulosOferta" runat="server" CellPadding="3" 
                    onselectedindexchanged="gridArticulosOferta_SelectedIndexChanged" TabIndex="50" 
                    onrowdatabound="gridArticulosOferta_RowDataBound" BackColor="#DEBA84" 
                    BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellSpacing="2" 
                    AutoGenerateColumns="False">
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                                    CommandName="Select" 
                                    Text="Seleccionar"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Articulo" HeaderText="Articulo" />
                        <asp:BoundField DataField="Descripción" HeaderText="Descripción" />
                        <asp:TemplateField HeaderText="PrecioLista" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecioLista" runat="server" Text='<%# Bind("PrecioLista", "{0:####,###,###,###,##.00000000}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("PrecioLista", "{0:####,###,###,###,##.00000000}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
            </li>
            <li>
                <table align="center" runat="server" id="tablaOfertaArticulo">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            </td>
                        <td>
                            </td>
                        <td>
                            Precio con IVA</td>
                        <td>
                            </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            Precio Original:</td>
                        <td>
                            <asp:TextBox ID="PrecioOriginal" runat="server" Width="140px" 
                                BackColor="AliceBlue" Font-Bold="True" ReadOnly="True" TabIndex="672" 
                                ToolTip="Este es el precio de lista original del artículo"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="PrecioOriginalConIVA" runat="server" Width="110px" 
                                BackColor="AliceBlue" Font-Bold="True" ReadOnly="True" TabIndex="673" 
                                
                                ToolTip="Este es el precio de lista original del artículo con IVA INCLUIDO."></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="pctjDescuento" runat="server" Width="25px" TabIndex="55" 
                                
                                
                                ToolTip="Ingrese aquí el porcentaje de descuento que desea aplicarle al precio de lista del artículo"></asp:TextBox>
                        &nbsp;<asp:Button ID="btnDescuento" runat="server" onclick="btnDescuento_Click" 
                                Text="Aplicar % dscto." Width="110px" 
                                
                                ToolTip="Haga clic aquí para aplicar el porcentaje de descuento ingresado al precio de lista del artículo" 
                                TabIndex="57" />
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            Precio Costo:</td>
                        <td>
                            <asp:TextBox ID="Costo" runat="server" Width="140px" 
                                BackColor="AliceBlue" Font-Bold="True" ReadOnly="True" TabIndex="670" 
                                ToolTip="Este es el costo del artículo"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="CostoConIVA" runat="server" Width="110px" 
                                BackColor="AliceBlue" Font-Bold="True" ReadOnly="True" TabIndex="680" 
                                ToolTip="Este es el costo del artículo con IVA INCLUIDO."></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            Precio Oferta:</td>
                        <td>
                            <asp:TextBox ID="PrecioOferta" runat="server" Width="140px" TabIndex="80" 
                                AutoPostBack="True" ontextchanged="PrecioOferta_TextChanged" 
                                ToolTip="Ingrese aquí el precio de oferta del artículo (sin IVA)"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="PrecioOfertaConIVA" runat="server" Width="110px" 
                                Font-Bold="True" TabIndex="82" AutoPostBack="True" 
                                ontextchanged="PrecioOfertaConIVA_TextChanged" 
                                ToolTip="Ingrese aquí el precio de oferta del artículo con IVA"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="descripcionOferta" runat="server" Width="30px" TabIndex="90" 
                                MaxLength="50" 
                                ToolTip="Ingrese aquí una descripción para identificar la oferta" 
                                Visible="False"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            Fecha Inicial:</td>
                        <td>
                            <asp:TextBox ID="FechaInicial" runat="server" Width="140px" TabIndex="86" 
                                MaxLength="10" TextMode="Date" 
                                ToolTip="Ingrese la fecha inicial de la oferta (dd/MM/aaaa)"></asp:TextBox>
                        </td>
                        <td style="text-align: right">
                            Fecha Final:</td>
                        <td>
                            <asp:TextBox ID="FechaFinal" runat="server" Width="140px" TabIndex="88" 
                                MaxLength="10" TextMode="Date" 
                                ToolTip="Ingrese la fecha final de la oferta (dd/MM/aaaa)"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="6">
                <asp:Label ID="lbInfoCondicion2" runat="server" 
                    
                                Text="La oferta estará condicionada a que el cliente adquiera alguno de los siguientes artículos:" 
                                Visible="False"></asp:Label>
                                &nbsp;&nbsp;&nbsp;
                        <asp:LinkButton ID="lbAgregarArticulosCondicion" runat="server" 
                            ToolTip="Haga clic aquí para agregar artículos los cuales condicionarán la oferta, es decir, solo si el cliente compra los artículos agregdos podrá recibir esta oferta" 
                            TabIndex="92" onclick="lbBuscarArticulo_Click" Visible="False">Agregar artículos</asp:LinkButton>
                        </td>
                    </tr>
                    </table>
            </li>
            <li>
            <table align="center" runat="server" id="tablaCreditoContado" visible="false">
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        % Descuento:</td>
                    <td>
                        <asp:TextBox ID="txtPorcentaje" runat="server" Width="140px" TabIndex="132"></asp:TextBox>&nbsp;</td>
                    <td>
                        <asp:LinkButton ID="lbBuscarArticulo" runat="server" 
                            ToolTip="Haga clic aquí para buscar un artículo y agregarlo a esta oferta" 
                            TabIndex="134" onclick="lbBuscarArticulo_Click">Agregar artículos</asp:LinkButton>
                            <asp:TextBox ID="descripcionOfertaContadoCredito" runat="server" 
                            Width="30px" TabIndex="134" 
                                MaxLength="50" 
                                ToolTip="Ingrese aquí una descripción para identificar la oferta" 
                            Visible="False"></asp:TextBox>
                    </td>
                </tr>
                    <tr>
                        <td>
                            <asp:LinkButton ID="lkMarcarTodosNiveles" runat="server" 
                                ToolTip="Haga clic aquí para marcar todos los niveles de precio" 
                                onclick="lkMarcarTodosNiveles_Click" TabIndex="138">Marcar todos</asp:LinkButton>
                                &nbsp;
                            <asp:LinkButton ID="lkDesMarcarTodosNiveles" runat="server" 
                                ToolTip="Haga clic aquí para desmarcar todos los niveles de precio" 
                                onclick="lkDesMarcarTodosNiveles_Click" TabIndex="139">Desmarcar todos</asp:LinkButton>
                        </td>
                        <td>
                            Fecha Inicial:</td>
                        <td>
                            <asp:TextBox ID="FechaInicialContadoCredito" runat="server" Width="140px" TabIndex="136" 
                                MaxLength="10" TextMode="Date" 
                                ToolTip="Ingrese la fecha inicial de la oferta (dd/MM/aaaa)"></asp:TextBox>
                        </td>
                        <td style="text-align: right">
                            Fecha Final:</td>
                        <td>
                            <asp:TextBox ID="FechaFinalContadoCredito" runat="server" Width="140px" TabIndex="137" 
                                MaxLength="10" TextMode="Date" 
                                ToolTip="Ingrese la fecha final de la oferta (dd/MM/aaaa)"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
                <asp:GridView ID="gvNivelesPrecio" runat="server" AutoGenerateColumns="False" 
                    BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="3" CellSpacing="2" 
                    onrowdatabound="gvNivelesPrecio_RowDataBound" TabIndex="140" 
                    Visible="False">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="cbNivelPrecio" runat="server" Checked="True" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" />
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="NivelPrecio" HeaderText="Nivel de Precio" />
                        <asp:BoundField DataField="Factor" HeaderText="Factor" />
                        <asp:BoundField DataField="TipoVenta" HeaderText="TipoVenta" Visible="False" />
                        <asp:BoundField DataField="TipoDeVenta" HeaderText="Tipo de Venta" 
                            Visible="False" />
                        <asp:BoundField DataField="Financiera" HeaderText="Financiera" 
                            Visible="False" />
                        <asp:BoundField DataField="NombreFinanciera" HeaderText="Financiera" />
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" Visible="False" />
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" Visible="False" />
                        <asp:BoundField DataField="Pagos" HeaderText="Pagos" />
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#F8AA55" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
    <table id="tablaBusquedaDet" runat="server" visible="false" >
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="4">
                <asp:Label ID="Label6" runat="server" ForeColor="Red"></asp:Label>
                <asp:Label ID="Label7" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>
                Artículo:</td>
            <td>
                <asp:TextBox ID="codigoArticuloDet" runat="server" TabIndex="150" 
                    AutoPostBack="True" ontextchanged="codigoArticuloDet_TextChanged"></asp:TextBox>
            </td>
            <td>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                Nombre:</td>
            <td>
                <asp:TextBox ID="descripcionArticuloDet" runat="server" TabIndex="160" 
                    MaxLength="100" AutoPostBack="True" 
                    ontextchanged="descripcionArticuloDet_TextChanged"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="4" style="text-align: center">
                <asp:LinkButton ID="lbBuscarArticuloDet" runat="server" 
                    
                    
                    
                    
                    ToolTip="Haga clic aquí para buscar artículos con los criterios de búsqueda ingresados" onclick="lbBuscarArticuloDet_Click" 
                    TabIndex="162">Buscar artículo</asp:LinkButton>&nbsp;&nbsp;&nbsp;|&nbsp;
                <asp:LinkButton ID="lbCerrarBusquedaDet" runat="server" 
                    ToolTip="Cierra la ventana de búsqueda" onclick="lbCerrarBusquedaDet_Click" 
                    TabIndex="164">Cerrar Búsqueda</asp:LinkButton>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="4" style="text-align: left">
                <asp:LinkButton 
                            ID="lbAgregarArtículo" runat="server" 
                            ToolTip="Haga clic aquí para agregar el o los artículos seleccionados." 
                            TabIndex="166" onclick="lbAgregarArtículo_Click">Agregar seleccionados</asp:LinkButton>
                            &nbsp;&nbsp;
                            <asp:LinkButton ID="lkMarcarTodosArticulos" runat="server" 
                                ToolTip="Haga clic aquí para marcar todos los artículos" 
                                onclick="lkMarcarTodosArticulos_Click" TabIndex="168">Marcar todos</asp:LinkButton>
                            &nbsp;&nbsp;
                            <asp:LinkButton ID="lkDesMarcarTodosArticulos" runat="server" 
                                ToolTip="Haga clic aquí para desmarcar todos los artículos" 
                                onclick="lkDesMarcarTodosArticulos_Click" TabIndex="170">Desmarcar todos</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lbInfoCondicion" runat="server" 
                    
                    Text="La oferta estará condicionada a la compra de los artículos agregados" 
                    Visible="False"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        </table>
    <asp:GridView ID="gridArticulosDet" runat="server" CellPadding="3" TabIndex="172" 
                    AutoGenerateColumns="False" onrowdatabound="gridArticulosDet_RowDataBound" 
                    style="text-align: left" BackColor="#DEBA84" BorderColor="#DEBA84" 
                    BorderStyle="None" BorderWidth="1px" CellSpacing="2" Visible="False">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="cbArticulo" runat="server" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" />
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False" Visible="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1Det" runat="server" CausesValidation="False" 
                                    CommandName="Select" 
                                    Text="Seleccionar"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Articulo" HeaderText="Artículo" />
                        <asp:BoundField DataField="Descripción" HeaderText="Nombre del Artículo" />
                        <asp:BoundField DataField="PrecioLista" HeaderText="PrecioLista" 
                            Visible="False" />
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#F8AA55" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
        <asp:GridView ID="gridArticulos" runat="server" AutoGenerateColumns="False" 
                            BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                            CellPadding="3" CellSpacing="2" Font-Size="Small" 
                TabIndex="174" Visible="False" DataKeyNames="Articulo" 
                onrowdeleted="gridArticulos_RowDeleted" 
                onrowdeleting="gridArticulos_RowDeleting" 
                onselectedindexchanged="gridArticulos_SelectedIndexChanged" 
                onrowdatabound="gridArticulos_RowDataBound">
                            <Columns>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkEliminarArticulo" runat="server" CausesValidation="False" 
                                            CommandName="Select" onclick="lkEliminarArticulo_Click" 
                                            onclientclick="return confirm('Desea eliminar el artículo?');" Text="Eliminar"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Articulo" HeaderText="Artículo" />
                                <asp:BoundField DataField="Nombre" HeaderText="Nombre del Artículo" />
                                <asp:BoundField DataField="Tipo" HeaderText="Tipo Descuento" Visible="false" />
                                <asp:BoundField DataField="Valor" HeaderText="% Desc" 
                                    HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" >
<HeaderStyle HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Observaciones" HeaderText="Observaciones" Visible="False" />
                                <asp:TemplateField HeaderText="Costo">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Costo", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Costo", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Costo Con IVA">
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("CostoIVA", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("CostoIVA", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Precio Lista">
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("PrecioLista", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("PrecioLista", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                            <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                            <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                            <SelectedRowStyle BackColor="#F8AA55" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#FFF1D4" />
                            <SortedAscendingHeaderStyle BackColor="#B95C30" />
                            <SortedDescendingCellStyle BackColor="#F1E5CE" />
                            <SortedDescendingHeaderStyle BackColor="#93451F" />
                        </asp:GridView>        
            </li>
            <li>
                <table align="center">
                    <tr>
                        <td style="text-align: center">
                            &nbsp;
                            <asp:LinkButton ID="lkGrabar" runat="server" 
                                ToolTip="Haga clic aquí para grabar la oferta" TabIndex="260" 
                                onclick="lkGrabar_Click">Grabar</asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="lkLimpiar" runat="server" 
                                ToolTip="Haga clic aquí para limpiar los datos de la página e ingresar un tipo de venta nuevo" 
                                TabIndex="270" onclick="lkLimpiar_Click">Limpiar</asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="lkEliminarCancelar" runat="server" 
                                ToolTip="Haga clic aquí para Eliminar o Cancelar una oferta" 
                                TabIndex="272" onclick="lkEliminarCancelar_Click">Eliminar/Cancelar</asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </li>
            <li runat="server" id = "liBuscarOferta" visible="false">
                <table align="center" id="tablaBuscarOferta" runat="server" visible="false">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td>
                            Fecha Mayor o Igual al:</td>
                        <td>
                            <asp:TextBox ID="FechaBuscar" runat="server" Width="140px" TabIndex="274" 
                                MaxLength="10" TextMode="Date" 
                                ToolTip="Ingrese la fecha inicial de la oferta (dd/MM/aaaa)"></asp:TextBox>
                        </td>
                        <td>
                            Texto en la Descripción:</td>
                        <td>
                            <asp:TextBox ID="TextBox2" runat="server" TabIndex="276" Width="210px"></asp:TextBox>
                        </td>
                        <td>
                <asp:LinkButton ID="lbBuscarOferta" runat="server" 
                    ToolTip="Haga clic aquí para buscar ofertas" onclick="lbBuscarOferta_Click" 
                    TabIndex="278">Buscar Oferta</asp:LinkButton>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="gridOfertas" runat="server" Visible="False" 
                    BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="3" CellSpacing="2" AutoGenerateColumns="False" 
                    onselectedindexchanged="gridOfertas_SelectedIndexChanged" 
                    onrowdatabound="gridOfertas_RowDataBound">
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lkEliminarOferta" runat="server" CausesValidation="False" 
                                    CommandName="Select" onclick="lkEliminarOferta_Click" Text="Eliminar" 
                                    ToolTip="Haga clic aquí para eliminar la oferta" 
                                    onclientclick="return confirm('Desea eliminar la oferta?');"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lkCancelarOferta" runat="server" CausesValidation="False" 
                                    CommandName="Select" onclick="lkCancelarOferta_Click" Text="Cancelar" 
                                    ToolTip="Haga clic aquí para cancelar la oferta" 
                                    onclientclick="return confirm('Desea cancelar la oferta?');"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="FechaInicial" HeaderText="Inicio" DataFormatString="{0:d}" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FechaFinal" HeaderText="Fin" DataFormatString="{0:d}" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Articulo" HeaderText="Artículo" >
                        <HeaderStyle Width="80px" />
                        <ItemStyle Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripción" ItemStyle-Font-Size="X-Small" >
                            <ItemStyle Font-Size="X-Small"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" />
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" ItemStyle-Font-Size="X-Small" >
                            <ItemStyle Font-Size="X-Small"></ItemStyle>
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="identificador" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbIdentificador" runat="server" 
                                    Text='<%# Bind("identificador") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtIdentificador" runat="server" Text='<%# Bind("identificador") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
            </li>
        </ul>
    </div>
        <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server" TabIndex="520"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" TabIndex="530"></asp:Label>
                                <asp:Label ID="Label2" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
