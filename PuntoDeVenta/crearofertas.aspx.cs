﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace PuntoDeVenta
{
    public partial class crearofertas : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                HtmlGenericControl submenu = new HtmlGenericControl();
                submenu = (HtmlGenericControl)Master.FindControl("submenu");
                submenu.Visible = true;

                HtmlGenericControl lkTiposVenta = new HtmlGenericControl();
                lkTiposVenta = (HtmlGenericControl)Master.FindControl("lkTiposVenta");
                lkTiposVenta.Visible = true;

                HtmlGenericControl lkFinancieras = new HtmlGenericControl();
                lkFinancieras = (HtmlGenericControl)Master.FindControl("lkFinancieras");
                lkFinancieras.Visible = true;

                HtmlGenericControl lkNivelesPrecio = new HtmlGenericControl();
                lkNivelesPrecio = (HtmlGenericControl)Master.FindControl("lkNivelesPrecio");
                lkNivelesPrecio.Visible = true;

                HtmlGenericControl lkCrearOfertas = new HtmlGenericControl();
                lkCrearOfertas = (HtmlGenericControl)Master.FindControl("lkCrearOfertas");
                lkCrearOfertas.Visible = true;

                HtmlGenericControl lkEnviarMails = new HtmlGenericControl();
                lkEnviarMails = (HtmlGenericControl)Master.FindControl("lkEnviarMails");
                lkEnviarMails.Visible = true;

                ViewState["url"] = Convert.ToString(Session["Url"]);
                ViewState["urlPrecios"] = Convert.ToString(Session["UrlPrecios"]);

                List<wsPuntoVenta.TipoVentaDet> q = new List<wsPuntoVenta.TipoVentaDet>();
                ViewState["qArticulos"] = q;

                gridArticulos.DataSource = q;
                gridArticulos.DataBind();
                gridArticulos.Visible = true;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                ViewState["iva"] = ws.iva();

                Label1.Text = "";
                Label2.Text = "";
                Label5.Text = "";
                articulo.Focus();
                tablaBusqueda.Visible = false;
                gridArticulosOferta.Visible = false;
            }
        }

        bool pasaValidaciones()
        {
            DateTime mFechaInicial, mFechaFinal;
            decimal mPrecioOriginal, mPrecioOferta, mPorcentaje;

            if (rbPrecioBase.Checked)
            {
                if (articulo.Text.Trim().Length == 0)
                {
                    lbInfo.Text = "";
                    lbError.Text = "Debe ingresar un artículo a ofertar.";
                    articulo.Focus();
                    return false;
                }

                mPrecioOriginal = Convert.ToDecimal(PrecioOriginal.Text.Replace(",", ""));

                if (PrecioOferta.Text.Trim().Length == 0)
                {
                    lbInfo.Text = "";
                    lbError.Text = "Debe ingresar el precio a ofertar.";
                    PrecioOferta.Focus();
                    return false;
                }

                try
                {
                    mPrecioOferta = Convert.ToDecimal(PrecioOferta.Text.Replace(",", ""));
                }
                catch
                {
                    lbInfo.Text = "";
                    lbError.Text = "El precio de oferta ingresado es inválido.";
                    PrecioOferta.Focus();
                    return false;
                }

                if (mPrecioOferta <= 0)
                {
                    lbInfo.Text = "";
                    lbError.Text = "El precio de oferta ingresado es inválido.";
                    PrecioOferta.Focus();
                    return false;
                }

                if (mPrecioOriginal == mPrecioOferta)
                {
                    lbInfo.Text = "";
                    lbError.Text = "El precio de la oferta es igual al precio original.";
                    PrecioOferta.Focus();
                    return false;
                }

                //if (descripcionOferta.Text.Trim().Length == 0)
                //{
                //    lbInfo.Text = "";
                //    lbError.Text = "Debe ingresar una descripción para esta oferta.";
                //    descripcionOferta.Focus();
                //    return false;
                //}

                //if (descripcionOferta.Text.Trim().Length <= 10)
                //{
                //    lbInfo.Text = "";
                //    lbError.Text = "Debe ser más explícito al ingresar la descripción para esta oferta.";
                //    descripcionOferta.Focus();
                //    return false;
                //}

                if (FechaInicial.Text.Trim().Length == 0)
                {
                    lbInfo.Text = "";
                    lbError.Text = "Debe ingresar la fecha inicial de la oferta.";
                    FechaInicial.Focus();
                    return false;
                }

                if (FechaFinal.Text.Trim().Length == 0)
                {
                    lbInfo.Text = "";
                    lbError.Text = "Debe ingresar la fecha final de la oferta.";
                    FechaFinal.Focus();
                    return false;
                }


                try
                {
                    mFechaInicial = Convert.ToDateTime(FechaInicial.Text);
                }
                catch
                {
                    lbInfo.Text = "";
                    lbError.Text = "La fecha inicial ingresada es inválida.";
                    FechaInicial.Focus();
                    return false;
                }

                //if (mFechaInicial <= DateTime.Now.Date)
                //{
                //    lbInfo.Text = "";
                //    lbError.Text = "La fecha inicial ingresada debe ser mayor al día de hoy.";
                //    FechaInicial.Focus();
                //    return false;
                //}

                try
                {
                    mFechaFinal = Convert.ToDateTime(FechaFinal.Text);
                }
                catch
                {
                    lbInfo.Text = "";
                    lbError.Text = "La fecha final ingresada es inválida.";
                    FechaFinal.Focus();
                    return false;
                }

                //if (mFechaFinal <= DateTime.Now.Date)
                //{
                //    lbInfo.Text = "";
                //    lbError.Text = "La fecha final ingresada debe ser mayor al día de hoy.";
                //    FechaFinal.Focus();
                //    return false;
                //}

                if (mFechaInicial > mFechaFinal)
                {
                    lbInfo.Text = "";
                    lbError.Text = "La fecha inicial no puede ser mayor a la fecha final.";
                    FechaInicial.Focus();
                    return false;
                }

                if (Convert.ToDecimal(PrecioOriginal.Text) == 0)
                {
                    lbInfo.Text = "";
                    lbError.Text = "El artículo seleccionado no tiene Precio Base, no es posible ofertarlo.";
                    codigoArticulo.Focus();
                    return false;
                }
            }
            
            if (rbContado.Checked || rbCredito.Checked)
            {
                if (txtPorcentaje.Text.Trim().Length == 0)
                {
                    lbInfo.Text = "";
                    lbError.Text = "Debe ingresar el porcentaje de descuento de esta oferta.";
                    txtPorcentaje.Focus();
                    return false;
                }

                try
                {
                    mPorcentaje = Convert.ToDecimal(txtPorcentaje.Text.Replace(",", ""));
                }
                catch
                {
                    lbInfo.Text = "";
                    lbError.Text = "El porcentaje ingresado es inválido.";
                    txtPorcentaje.Focus();
                    return false;
                }

                if (mPorcentaje <= 0)
                {
                    lbInfo.Text = "";
                    lbError.Text = "El porcentaje ingresado es inválido.";
                    txtPorcentaje.Focus();
                    return false;
                }

                //if (descripcionOfertaContadoCredito.Text.Trim().Length == 0)
                //{
                //    lbInfo.Text = "";
                //    lbError.Text = "Debe ingresar una descripción para esta oferta.";
                //    descripcionOfertaContadoCredito.Focus();
                //    return false;
                //}

                //if (descripcionOfertaContadoCredito.Text.Trim().Length <= 10)
                //{
                //    lbInfo.Text = "";
                //    lbError.Text = "Debe ser más explícito al ingresar la descripción para esta oferta.";
                //    descripcionOfertaContadoCredito.Focus();
                //    return false;
                //}

                if (FechaInicialContadoCredito.Text.Trim().Length == 0)
                {
                    lbInfo.Text = "";
                    lbError.Text = "Debe ingresar la fecha inicial de la oferta.";
                    FechaInicialContadoCredito.Focus();
                    return false;
                }

                if (FechaFinalContadoCredito.Text.Trim().Length == 0)
                {
                    lbInfo.Text = "";
                    lbError.Text = "Debe ingresar la fecha final de la oferta.";
                    FechaFinalContadoCredito.Focus();
                    return false;
                }


                try
                {
                    mFechaInicial = Convert.ToDateTime(FechaInicialContadoCredito.Text);
                }
                catch
                {
                    lbInfo.Text = "";
                    lbError.Text = "La fecha inicial ingresada es inválida.";
                    FechaInicialContadoCredito.Focus();
                    return false;
                }

                //if (mFechaInicial <= DateTime.Now.Date)
                //{
                //    lbInfo.Text = "";
                //    lbError.Text = "La fecha inicial ingresada debe ser mayor al día de hoy.";
                //    FechaInicialContadoCredito.Focus();
                //    return false;
                //}

                try
                {
                    mFechaFinal = Convert.ToDateTime(FechaFinalContadoCredito.Text);
                }
                catch
                {
                    lbInfo.Text = "";
                    lbError.Text = "La fecha final ingresada es inválida.";
                    FechaFinalContadoCredito.Focus();
                    return false;
                }

                //if (mFechaFinal <= DateTime.Now.Date)
                //{
                //    lbInfo.Text = "";
                //    lbError.Text = "La fecha final ingresada debe ser mayor al día de hoy.";
                //    FechaFinalContadoCredito.Focus();
                //    return false;
                //}

                if (mFechaInicial > mFechaFinal)
                {
                    lbInfo.Text = "";
                    lbError.Text = "La fecha inicial no puede ser mayor a la fecha final.";
                    FechaInicialContadoCredito.Focus();
                    return false;
                }

                int jj = 0;
                for (int ii = 0; ii < gvNivelesPrecio.Rows.Count; ii++)
                {
                    CheckBox cbNivelPrecio = (CheckBox)gvNivelesPrecio.Rows[ii].FindControl("cbNivelPrecio");
                    if (cbNivelPrecio.Checked) jj++;
                }

                if (jj == 0)
                {
                    lbInfo.Text = "";
                    lbError.Text = "Debe seleccionar al menos un nivel de precio.";
                    txtPorcentaje.Focus();
                    return false;
                }

                if (gridArticulos.Rows.Count == 0)
                {
                    lbInfo.Text = "";
                    lbError.Text = "Debe agregar al menos un artículo a esta oferta.";
                    txtPorcentaje.Focus();
                    return false;
                }

                for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                {
                    decimal mPctj = Convert.ToDecimal(gridArticulos.Rows[ii].Cells[4].Text);

                    if (mPctj != mPorcentaje)
                    {
                        lbInfo.Text = "";
                        lbError.Text = "Existen artículos con diferente porcentaje que el ingressado para la oferta.";
                        txtPorcentaje.Focus();
                        return false;
                    }
                }
            }

            if (rbCondicional.Checked)
            {
                if (gridArticulos.Rows.Count == 0)
                {
                    lbInfo.Text = "";
                    lbError.Text = "Debe agregar al menos un artículo a esta oferta.";
                    txtPorcentaje.Focus();
                    return false;
                }
            }

            return true;
        }

        protected void lkGrabar_Click(object sender, EventArgs e)
        {
            if (!pasaValidaciones()) return;
            DateTime mFechaInicial, mFechaFinal;

            var ws = new wsCambioPrecios.wsCambioPrecios(); string mMensaje = "";
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["urlPrecios"]);

            if (rbPrecioBase.Checked || rbCondicional.Checked)
            {
                bool mCondicional = false;
                string[] articulos = new string[1];

                mFechaInicial = Convert.ToDateTime(FechaInicial.Text);
                mFechaFinal = Convert.ToDateTime(FechaFinal.Text);

                for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                {
                    if (ii > 0) Array.Resize(ref articulos, ii + 1);
                    articulos[ii] = gridArticulos.Rows[ii].Cells[1].Text;
                }

                if (rbCondicional.Checked) mCondicional = true;
                descripcionOferta.Text = string.Format("Oferta del {0} al {1}", mFechaInicial.ToShortDateString(), mFechaFinal.ToShortDateString());

                if (!ws.GrabarOfertaArticuloNormalCondicional(ref mMensaje, articulo.Text, mFechaInicial, mFechaFinal, Convert.ToDecimal(PrecioOriginal.Text.Replace(",", "")), Convert.ToDecimal(PrecioOferta.Text.Replace(",", "")), descripcionOferta.Text, mCondicional, articulos))
                {
                    Label1.Text = "";
                    Label2.Text = "";
                    lbInfo.Text = "";
                    Label5.Text = "";
                    lbError.Text = mMensaje;
                    return;
                }
            }

            if (rbContado.Checked || rbCredito.Checked)
            {
                int jj = 0; string mTipo = "CR";
                string[] articulos = new string[1]; string[] niveles = new string[1];

                mFechaInicial = Convert.ToDateTime(FechaInicialContadoCredito.Text);
                mFechaFinal = Convert.ToDateTime(FechaFinalContadoCredito.Text);

                for (int ii = 0; ii < gvNivelesPrecio.Rows.Count; ii++)
                {
                    CheckBox cbNivelPrecio = (CheckBox)gvNivelesPrecio.Rows[ii].FindControl("cbNivelPrecio");
                    if (cbNivelPrecio.Checked)
                    {
                        if (jj > 0) Array.Resize(ref niveles, jj + 1);
                        niveles[jj] = gvNivelesPrecio.Rows[ii].Cells[1].Text;

                        jj++;
                    }
                }

                for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                {
                    if (ii > 0) Array.Resize(ref articulos, ii + 1);
                    articulos[ii] = gridArticulos.Rows[ii].Cells[1].Text;
                }

                if (rbContado.Checked) mTipo = "CO";
                descripcionOfertaContadoCredito.Text = string.Format("Oferta del {0} al {1}", mFechaInicial.ToShortDateString(), mFechaFinal.ToShortDateString());

                if (!ws.GrabarOfertaArticulosNiveles(ref mMensaje, articulos, niveles, mFechaInicial, mFechaFinal, Convert.ToDecimal(txtPorcentaje.Text.Replace(",", "")), descripcionOfertaContadoCredito.Text, mTipo))
                {
                    Label1.Text = "";
                    Label2.Text = "";
                    lbInfo.Text = "";
                    Label5.Text = "";
                    lbError.Text = mMensaje;
                    return;
                }
            }

            Label1.Text = "";
            Label2.Text = "";
            lbError.Text = "";
            Label5.Text = "";
            lbInfo.Text = mMensaje;

            articulo.Text = "";
            descripcion.Text = "";
            PrecioOriginal.Text = "";
            PrecioOriginalConIVA.Text = "";
            Costo.Text = "";
            CostoConIVA.Text = "";
            PrecioOferta.Text = "";
            PrecioOfertaConIVA.Text = "";
            pctjDescuento.Text = "";
            txtPorcentaje.Text = "";
            mostrarSoloPrecioBase();

            articulo.Focus();
            tablaBusqueda.Visible = false;
            gridArticulosOferta.Visible = false;
            rbContado.Checked = false;
            rbCredito.Checked = false;
            rbCondicional.Checked = false;
            rbPrecioBase.Checked = true;

            List<wsPuntoVenta.TipoVentaDet> qEnBlanco = new List<wsPuntoVenta.TipoVentaDet>();
            ViewState["qArticulos"] = qEnBlanco;

            gridArticulos.DataSource = qEnBlanco;
            gridArticulos.DataBind();
        }

        protected void lbBuscarArticuloOferta_Click(object sender, EventArgs e)
        {
            Label1.Text = "";
            Label5.Text = "";
            codigoArticulo.Text = "";
            descripcionArticulo.Text = "";
            tablaBusqueda.Visible = true;
            gridArticulosOferta.Visible = true;
            descripcionArticulo.Focus();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            string mTipo = "";

            Label1.Text = "";
            Label5.Text = "";

            if (codigoArticulo.Text.Trim().Length == 0 && descripcionArticulo.Text.Trim().Length == 0)
            {
                Label1.Text = "Debe ingresar un criterio de búsqueda ya sea el código, la descripción o ambos.";
                codigoArticulo.Focus();
                return;
            }

            if (codigoArticulo.Text.Trim().Length > 0) mTipo = "C";
            if (descripcionArticulo.Text.Trim().Length > 0) mTipo = "D";
            if (codigoArticulo.Text.Trim().Length > 0 && descripcionArticulo.Text.Trim().Length > 0) mTipo = "A";

            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["urlPrecios"]);

            var q = ws.ListarArticulosCriterioConPrecio(mTipo, codigoArticulo.Text, descripcionArticulo.Text);
            var articulos = q.ToArray();

            gridArticulosOferta.DataSource = articulos;
            gridArticulosOferta.DataBind();
            gridArticulosOferta.Visible = true;

            descripcionArticulo.Focus();
            if (q.Length == 0)
            {
                Label1.Text = "No se encontraron artículos con el criterio de búsqueda ingresado.";
            }
            else
            {
                Label5.Text = string.Format("Se encontraron {0} artículos con el criterio de búsqueda ingresado.", q.Length);
            }
        }

        protected void lbCerrarBusqueda_Click(object sender, EventArgs e)
        {
            tablaBusqueda.Visible = false;
            gridArticulosOferta.Visible = false;
        }

        protected void btnDescuento_Click(object sender, EventArgs e)
        {
            PrecioOfertaConIVA.Text = Convert.ToString(Math.Round(Convert.ToDouble(PrecioOriginalConIVA.Text.Replace(",", "")) * (1 - (Convert.ToDouble(pctjDescuento.Text)) / 100), 2, MidpointRounding.AwayFromZero));
            PrecioOferta.Text = Convert.ToString(Math.Round(Convert.ToDouble(PrecioOfertaConIVA.Text) / Convert.ToDouble(ViewState["iva"]), 8, MidpointRounding.AwayFromZero));
        }

        protected void lkLimpiar_Click(object sender, EventArgs e)
        {
            articulo.Text = "";
            descripcion.Text = "";
            PrecioOriginal.Text = "";
            PrecioOriginalConIVA.Text = "";
            Costo.Text = "";
            CostoConIVA.Text = "";
            PrecioOferta.Text = "";
            PrecioOfertaConIVA.Text = "";
            tablaBusqueda.Visible = false;
            gridArticulosOferta.Visible = false;
            rbPrecioBase.Checked = true;
            mostrarSoloPrecioBase();

            List<wsPuntoVenta.TipoVentaDet> qEnBlanco = new List<wsPuntoVenta.TipoVentaDet>();
            gridArticulos.DataSource = qEnBlanco;
            gridArticulos.DataBind();
        }

        protected void articulo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lbError.Text = "";

                if (articulo.Text.Trim().Length == 0)
                {
                    Label1.Text = "";
                    Label5.Text = "";
                    descripcion.Text = "";
                    PrecioOriginal.Text = "";
                    PrecioOriginalConIVA.Text = "";
                    Costo.Text = "";
                    CostoConIVA.Text = "";
                    PrecioOferta.Text = "";
                    PrecioOfertaConIVA.Text = "";
                    return;
                }

                var ws = new wsCambioPrecios.wsCambioPrecios();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["urlPrecios"]);

                string mPrimerCaracter = articulo.Text.Substring(0, 1);

                try
                {
                    int mDigito = Convert.ToInt32(mPrimerCaracter);

                    codigoArticulo.Text = articulo.Text;
                    descripcionArticulo.Text = "";

                    var q = ws.ListarArticulosCriterio("C", articulo.Text, "");
                    var articulos = q.ToArray();

                    gridArticulosOferta.DataSource = articulos;
                    gridArticulosOferta.DataBind();
                }
                catch
                {
                    codigoArticulo.Text = "";
                    descripcionArticulo.Text = articulo.Text.ToUpper();

                    var q = ws.ListarArticulosCriterio("D", "", articulo.Text);
                    var articulos = q.ToArray();

                    gridArticulosOferta.DataSource = articulos;
                    gridArticulosOferta.DataBind();
                }

                if (gridArticulosOferta.Rows.Count == 0)
                {
                    articulo.Text = "";
                    descripcion.Text = "";
                    PrecioOriginal.Text = "";
                    PrecioOriginalConIVA.Text = "";
                    Costo.Text = "";
                    CostoConIVA.Text = "";
                    PrecioOferta.Text = "";
                    PrecioOfertaConIVA.Text = "";
                    Label1.Text = "El código de artículo ingresado no existe.";
                    articulo.Focus();
                    return;
                }
                
                if (gridArticulosOferta.Rows.Count == 1)
                {
                    GridViewRow gvr = gridArticulosOferta.Rows[0];
                    Label lbPrecioLista = (Label)gvr.FindControl("lbPrecioLista");

                    articulo.Text = gridArticulosOferta.Rows[0].Cells[1].Text;
                    descripcion.Text = gridArticulosOferta.Rows[0].Cells[2].Text;

                    CostoArticulo();
                    PrecioOriginal.Text = lbPrecioLista.Text;
                    PrecioOriginalConIVA.Text = String.Format("{0:0,0.00}", Math.Round((Convert.ToDouble(lbPrecioLista.Text.Replace(",", "")) * Convert.ToDouble(ViewState["iva"])), 2, MidpointRounding.AwayFromZero));

                    PrecioOferta.Text = "";
                    PrecioOfertaConIVA.Text = "";

                    Label1.Text = "";
                    Label2.Text = "";
                    Label5.Text = "";

                    pctjDescuento.Focus();
                    tablaBusqueda.Visible = false;
                    gridArticulosOferta.Visible = false;
                }
                else
                {
                    tablaBusqueda.Visible = true;
                    gridArticulosOferta.Visible = true;
                    gridArticulosOferta.Focus();
                }
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Se produjo el siguiente error al buscar el artículo {0} {1}.", ex.Message, ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")));
            }
        }

        protected void PrecioOferta_TextChanged(object sender, EventArgs e)
        {
            lbError.Text = "";

            try
            {
                PrecioOfertaConIVA.Text = Math.Round(Convert.ToDouble(PrecioOferta.Text) * Convert.ToDouble(ViewState["iva"]), 2, MidpointRounding.AwayFromZero).ToString();
            }
            catch
            {
                lbError.Text = "Debe ingresar un precio válido.";
                PrecioOferta.Focus();
            }
        }

        protected void PrecioOfertaConIVA_TextChanged(object sender, EventArgs e)
        {
            lbError.Text = "";

            try
            {
                PrecioOferta.Text = Math.Round(Convert.ToDouble(PrecioOfertaConIVA.Text) / Convert.ToDouble(ViewState["iva"]), 8, MidpointRounding.AwayFromZero).ToString();
            }
            catch
            {
                lbError.Text = "Debe ingresar un precio válido.";
                PrecioOfertaConIVA.Focus();
            }

            descripcionOferta.Focus();
        }

        protected void gridArticulosOferta_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }

            foreach (TableCell cel in e.Row.Cells)
            {
                cel.Attributes.Add("style", "text-align: left");
            }
        }

        protected void gridArticulosOferta_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = gridArticulosOferta.SelectedRow;
            Label lbPrecioLista = (Label)gvr.FindControl("lbPrecioLista");

            articulo.Text = gridArticulosOferta.SelectedRow.Cells[1].Text;
            descripcion.Text = gridArticulosOferta.SelectedRow.Cells[2].Text;
            PrecioOriginal.Text = lbPrecioLista.Text;
            PrecioOriginalConIVA.Text = String.Format("{0:0,0.00}", Math.Round(Convert.ToDouble(lbPrecioLista.Text.Replace(",", "")) * Convert.ToDouble(ViewState["iva"]), 2, MidpointRounding.AwayFromZero));

            CostoArticulo();
            if (descripcion.Text == "&nbsp;") descripcion.Text = "";

            descripcion.Text = descripcion.Text.Replace("&#39;", "'");
            descripcion.Text = descripcion.Text.Replace("&#225;", "á");
            descripcion.Text = descripcion.Text.Replace("&#233;", "é");
            descripcion.Text = descripcion.Text.Replace("&#237;", "í");
            descripcion.Text = descripcion.Text.Replace("&#243;", "ó");
            descripcion.Text = descripcion.Text.Replace("&#250;", "ú");

            Label1.Text = "";
            Label2.Text = "";
            Label5.Text = "";

            pctjDescuento.Focus();
            tablaBusqueda.Visible = false;
            gridArticulosOferta.Visible = false;
        }

        void CostoArticulo()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            decimal mCosto = ws.CostoArticulo(articulo.Text);
            decimal mCostoIVA = mCosto * ws.iva();

            Costo.Text = String.Format("{0:0,0.00}", mCosto);
            CostoConIVA.Text = String.Format("{0:0,0.00}", mCostoIVA, 2, MidpointRounding.AwayFromZero);
        }

        protected void rbPrecioBase_CheckedChanged(object sender, EventArgs e)
        {
            if (rbPrecioBase.Checked) mostrarSoloPrecioBase();
        }

        protected void rbCondicional_CheckedChanged(object sender, EventArgs e)
        {
            if (rbCondicional.Checked)
            {
                mostrarSoloPrecioBase();
                lbInfoCondicion.Visible = true;
                lbInfoCondicion2.Visible = true;
                gridArticulos.Columns[4].Visible = false;
                lbAgregarArticulosCondicion.Visible = true;
                gridArticulos.Visible = true;
                gridOfertas.Visible = false;
            }
        }

        protected void rbContado_CheckedChanged(object sender, EventArgs e)
        {
            if (rbContado.Checked) mostrarContadoCredito("CO");
        }

        protected void rbCredito_CheckedChanged(object sender, EventArgs e)
        {
            if (rbCredito.Checked) mostrarContadoCredito("CR");
        }

        void mostrarSoloPrecioBase()
        {
            tablaArticulo.Visible = true;
            tablaBusqueda.Visible = false;
            tablaOfertaArticulo.Visible = true;
            tablaCreditoContado.Visible = false;
            tablaBusquedaDet.Visible = false;
            gvNivelesPrecio.Visible = false;
            gridArticulosDet.Visible = false;
            gridArticulos.Visible = false;
            gridArticulosOferta.Visible = false;

            lbInfoCondicion.Visible = false;
            lbInfoCondicion2.Visible = false;
            gridArticulos.Columns[4].Visible = true;
            lbAgregarArticulosCondicion.Visible = false;
            gridOfertas.Visible = false;

            List<wsPuntoVenta.TipoVentaDet> q = new List<wsPuntoVenta.TipoVentaDet>();
            ViewState["qArticulos"] = q;

            gridArticulos.DataSource = q;
            gridArticulos.DataBind();
            gridArticulos.Visible = true;

            articulo.Focus();
        }

        void mostrarContadoCredito(string tipo)
        {
            tablaArticulo.Visible = false;
            tablaBusqueda.Visible = false;
            tablaOfertaArticulo.Visible = false;
            tablaCreditoContado.Visible = true;
            tablaBusquedaDet.Visible = false;
            gvNivelesPrecio.Visible = true;
            gridArticulosDet.Visible = false;
            gridArticulos.Visible = false;
            gridOfertas.Visible = false;
            gridArticulosOferta.Visible = false;

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveNivelesPrecioContadoCredito(tipo);

            gvNivelesPrecio.DataSource = q;
            gvNivelesPrecio.DataBind();
            gvNivelesPrecio.Visible = true;

            List<wsPuntoVenta.TipoVentaDet> qq = new List<wsPuntoVenta.TipoVentaDet>();
            ViewState["qArticulos"] = qq;

            gridArticulos.DataSource = qq;
            gridArticulos.DataBind();
            gridArticulos.Visible = true;

            txtPorcentaje.Focus();
        }

        void BuscarArticulo()
        {
            string mTipo = "";

            Label6.Text = "";
            Label7.Text = "";

            if (codigoArticuloDet.Text.Trim().Length == 0 && descripcionArticuloDet.Text.Trim().Length == 0)
            {
                Label6.Text = "Debe ingresar un criterio de búsqueda ya sea el código, la descripción o ambos.";
                codigoArticuloDet.Focus();
                return;
            }

            if (codigoArticuloDet.Text.Trim().Length > 0) mTipo = "C";
            if (descripcionArticuloDet.Text.Trim().Length > 0) mTipo = "D";
            if (codigoArticuloDet.Text.Trim().Length > 0 && descripcionArticuloDet.Text.Trim().Length > 0) mTipo = "A";

            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["urlPrecios"]);

            var q = ws.ListarArticulosCriterioConPrecio(mTipo, codigoArticuloDet.Text, descripcionArticuloDet.Text);
            var articulos = q.ToArray();

            gridArticulosDet.DataSource = articulos;
            gridArticulosDet.DataBind();

            descripcionArticuloDet.Focus();
            if (q.Length == 0)
            {
                Label6.Text = "No se encontraron artículos con el criterio de búsqueda ingresado.";
            }
            else
            {
                Label7.Text = string.Format("Se encontraron {0} artículos con el criterio de búsqueda ingresado.", q.Length);
            }
        }

        protected void lbBuscarArticulo_Click(object sender, EventArgs e)
        {
            tablaBusquedaDet.Visible = true;
            gridArticulosDet.Visible = true;

            List<wsCambioPrecios.Articulos> q = new List<wsCambioPrecios.Articulos>();

            gridArticulosDet.DataSource = q;
            gridArticulosDet.DataBind();

            Label6.Text = "";
            Label7.Text = "";
            codigoArticuloDet.Text = "";
            descripcionArticuloDet.Text = "";
            descripcionArticuloDet.Focus();
        }

        protected void gvNivelesPrecio_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void lbBuscarArticuloDet_Click(object sender, EventArgs e)
        {
            BuscarArticulo();
        }

        protected void lbCerrarBusquedaDet_Click(object sender, EventArgs e)
        {
            tablaBusquedaDet.Visible = false;
            gridArticulosDet.Visible = false;
        }

        protected void lbAgregarArtículo_Click(object sender, EventArgs e)
        {
            if (rbCondicional.Checked)
            {
                txtPorcentaje.Text = "10";
            }
            else
            {
                if (txtPorcentaje.Text.Trim().Length == 0)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('Debe ingresar un valor o porcentaje válido.');", true);
                    txtPorcentaje.Focus();
                    return;
                }

                try
                {
                    decimal mPctjValor = Convert.ToDecimal(txtPorcentaje.Text);

                    if (mPctjValor <= 0)
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('Debe ingresar un valor o porcentaje válido.');", true);
                        txtPorcentaje.Focus();
                        return;
                    }
                }
                catch
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('Debe ingresar un valor o porcentaje válido.');", true);
                    txtPorcentaje.Focus();
                    return;
                }
            }

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            List<wsPuntoVenta.TipoVentaDet> q = new List<wsPuntoVenta.TipoVentaDet>();
            q = (List<wsPuntoVenta.TipoVentaDet>)ViewState["qArticulos"];

            foreach (GridViewRow gvr in gridArticulosDet.Rows)
            {
                CheckBox cb = (CheckBox)gvr.FindControl("cbArticulo");

                if (cb.Checked)
                {
                    foreach (GridViewRow gvrArticulo in gridArticulos.Rows)
                    {
                        if (gvr.Cells[2].Text == gvrArticulo.Cells[1].Text)
                        {
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('El artículo {0} ya se encuentra dentro de esta oferta.');", gvr.Cells[2].Text), true);
                            return;
                        }
                    }

                    wsPuntoVenta.TipoVentaDet item = new wsPuntoVenta.TipoVentaDet();
                    item.Articulo = gvr.Cells[2].Text;
                    item.Nombre = gvr.Cells[3].Text;
                    item.Observaciones = "";
                    item.Tipo = "P";
                    item.Valor = Convert.ToDecimal(txtPorcentaje.Text);
                    item.Costo = ws.CostoArticulo(gvr.Cells[2].Text);
                    item.CostoIVA = ws.CostoArticulo(gvr.Cells[2].Text) * ws.iva();
                    item.PrecioLista = ws.PrecioLista(gvr.Cells[2].Text, Convert.ToString(Session["Tienda"]));
                    q.Add(item);

                    gridArticulos.DataSource = q;
                    gridArticulos.DataBind();
                    gridArticulos.Visible = true;
                }

            }

            ViewState["qArticulos"] = q;
            tablaBusquedaDet.Visible = false;
            gridArticulosDet.Visible = false;
        }

        protected void gridArticulosDet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridArticulos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridArticulos_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {

        }

        protected void gridArticulos_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void gridArticulos_SelectedIndexChanged(object sender, EventArgs e)
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            List<wsPuntoVenta.TipoVentaDet> q = new List<wsPuntoVenta.TipoVentaDet>();

            foreach (GridViewRow gvr in gridArticulos.Rows)
            {
                if (gvr.Cells[1].Text != gridArticulos.SelectedRow.Cells[1].Text)
                {
                    string mNombre = gvr.Cells[2].Text;
                    string mObservaciones = gvr.Cells[5].Text;

                    mNombre = mNombre.Replace("&#39;", "'").Replace("&#225;", "á").Replace("&#233;", "é").Replace("&#237;", "í").Replace("&#243;", "ó").Replace("&#243;", "ú").Replace("&quot;", "\"");
                    mObservaciones = mObservaciones.Replace("&#39;", "'").Replace("&#225;", "á").Replace("&#233;", "é").Replace("&#237;", "í").Replace("&#243;", "ó").Replace("&#243;", "ú").Replace("&quot;", "\"");

                    wsPuntoVenta.TipoVentaDet item = new wsPuntoVenta.TipoVentaDet();
                    item.Articulo = gvr.Cells[1].Text; //Tarjeta Libre&#39;s a&#225; e&#233; i&#237; o&#243; u&#243; comillas&quot;
                    item.Nombre = mNombre;
                    item.Observaciones = mObservaciones;
                    item.Tipo = gvr.Cells[3].Text;
                    item.Valor = Convert.ToDecimal(gvr.Cells[4].Text);
                    item.Costo = ws.CostoArticulo(gvr.Cells[1].Text);
                    item.CostoIVA = ws.CostoArticulo(gvr.Cells[1].Text) * ws.iva();
                    item.PrecioLista = ws.PrecioLista(gvr.Cells[1].Text, Convert.ToString(Session["Tienda"]));
                    q.Add(item);
                }
            }

            gridArticulos.DataSource = q;
            gridArticulos.DataBind();
            gridArticulos.SelectedIndex = -1;

            ViewState["qArticulos"] = q;
            tablaBusquedaDet.Visible = false;
            gridArticulosDet.Visible = false;
        }

        protected void codigoArticuloDet_TextChanged(object sender, EventArgs e)
        {
            BuscarArticulo();
        }

        protected void descripcionArticuloDet_TextChanged(object sender, EventArgs e)
        {
            BuscarArticulo();
        }

        protected void lkEliminarArticulo_Click(object sender, EventArgs e)
        {

        }

        protected void lkMarcarTodosNiveles_Click(object sender, EventArgs e)
        {
            for (int ii = 0; ii < gvNivelesPrecio.Rows.Count; ii++)
            {
                CheckBox cbNivelPrecio = (CheckBox)gvNivelesPrecio.Rows[ii].FindControl("cbNivelPrecio");
                cbNivelPrecio.Checked = true;
            }
        }
        
        protected void lkDesMarcarTodosNiveles_Click(object sender, EventArgs e)
        {
            for (int ii = 0; ii < gvNivelesPrecio.Rows.Count; ii++)
            {
                CheckBox cbNivelPrecio = (CheckBox)gvNivelesPrecio.Rows[ii].FindControl("cbNivelPrecio");
                cbNivelPrecio.Checked = false;
            }
        }

        protected void lkMarcarTodosArticulos_Click(object sender, EventArgs e)
        {
            for (int ii = 0; ii < gridArticulosDet.Rows.Count; ii++)
            {
                CheckBox cbArticulo = (CheckBox)gridArticulosDet.Rows[ii].FindControl("cbArticulo");
                cbArticulo.Checked = true;
            }
        }

        protected void lkDesMarcarTodosArticulos_Click(object sender, EventArgs e)
        {
            for (int ii = 0; ii < gridArticulosDet.Rows.Count; ii++)
            {
                CheckBox cbArticulo = (CheckBox)gridArticulosDet.Rows[ii].FindControl("cbArticulo");
                cbArticulo.Checked = false;
            }
        }

        protected void lkEliminarCancelar_Click(object sender, EventArgs e)
        {
            cargarAdministracionOfertas();
        }

        void cargarAdministracionOfertas()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            //var q = ws.DevuelveAdministracionOfertas();

            //gridOfertas.DataSource = q;
            //gridOfertas.DataBind();
            //gridOfertas.SelectedIndex = -1;

            liBuscarOferta.Visible = true;
            gridOfertas.Visible = true;
        }

        protected void lbBuscarOferta_Click(object sender, EventArgs e)
        {

        }

        protected void lkEliminarOferta_Click(object sender, EventArgs e)
        {
            ViewState["AccionOferta"] = "Eliminar";
        }

        protected void lkCancelarOferta_Click(object sender, EventArgs e)
        {
            ViewState["AccionOferta"] = "Cancelar";
        }

        protected void gridOfertas_SelectedIndexChanged(object sender, EventArgs e)
        {
            string mMensaje = "";
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            lbInfo.Text = "";
            lbError.Text = "";

            DateTime mFechaInicial = Convert.ToDateTime(gridOfertas.SelectedRow.Cells[2].Text);
            DateTime mFechaFinal = Convert.ToDateTime(gridOfertas.SelectedRow.Cells[3].Text);

            string mArticulo = gridOfertas.SelectedRow.Cells[4].Text;
            Label lbIdentificador = (Label)gridOfertas.SelectedRow.FindControl("lbIdentificador");
            Int32 mIdentificador = Convert.ToInt32(lbIdentificador.Text);

            switch (ViewState["AccionOferta"].ToString())
            {
                case "Eliminar" :
                    if (ws.EliminarOferta(ref mMensaje, mArticulo, mIdentificador, mFechaInicial))
                    {
                        lbInfo.Text = mMensaje;
                        cargarAdministracionOfertas();
                        liBuscarOferta.Visible = false;
                        gridOfertas.Visible = false;
                    }
                    else
                    {
                        lbError.Text = mMensaje;
                        return;
                    }
                    break;
                case "Cancelar" :
                    if (ws.CancelarOferta(ref mMensaje, mArticulo, mIdentificador, mFechaInicial, Convert.ToString(Session["usuario"]), mFechaFinal))
                    {
                        lbInfo.Text = mMensaje;
                        cargarAdministracionOfertas();
                        liBuscarOferta.Visible = false;
                        gridOfertas.Visible = false;
                    }
                    else
                    {
                        lbError.Text = mMensaje;
                        return;
                    }
                    break;
                default :
                    break;
            }
        }

        protected void gridOfertas_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

    }
}