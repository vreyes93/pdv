﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="crediplus.aspx.cs" Inherits="PuntoDeVenta.crediplus" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h3>&nbsp;&nbsp;Crediplus</h3>
    <div class="content2">
        <div class="clientes2">

            <ul>
                <li>
                    <table>
                        <tr>
                            <td colspan="4">
                                <a>Impresión de facturas</a>
                            </td>
                            <td colspan="4" style="text-align: right">
                                <asp:Label ID="lbErrorImpresionFacturas" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                <asp:Label ID="lbInfoImpresionFactura" runat="server"></asp:Label>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>Factura inicial:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtFacturaInicial" runat="server" Width="110px" style="text-transform: uppercase;"
                                    CssClass="textBoxStyle" TabIndex="10" ToolTip="Ingrese el número de la factura inicial" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>Factura final:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtFacturaFinal" runat="server" Width="110px" style="text-transform: uppercase;"
                                    CssClass="textBoxStyle" TabIndex="15" ToolTip="Ingrese el número de la factura final" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnImprimirFacturas" runat="server" Width="8px" Height="8px"
                                    ToolTip="Impresión de facturas" onclick="btnImprimirFacturas_Click" TabIndex="20" >
                                    <Image IconID="print_print_16x16office2013" />
                                </dx:ASPxButton>
                            </td>
                            <td>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4"></td>
                            <td colspan="4"></td>
                        </tr>

                    </table>
                </li>
            </ul>

            <ul>
                <li>
                    <table>
                        <tr>
                            <td colspan="4">
                                <a>Re-facturación</a>
                            </td>
                            <td colspan="4" style="text-align: right">
                                <asp:Label ID="lbErrorRefacturacion" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                <asp:Label ID="lbInfoRefacturacion" runat="server"></asp:Label>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>Refacturar:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtRefacturar" runat="server" Width="110px" style="text-transform: uppercase;"
                                    CssClass="textBoxStyle" TabIndex="50" ToolTip="Ingrese el número de la factura que desea re-generar (se anulará dicha factura y se generará una nueva con los mismos valores" >
                                </dx:ASPxTextBox>
                            </td>
                            <td></td>
                            <td>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnRefacturar" runat="server" Width="8px" Height="8px"
                                    ToolTip="Refacturar según la factura ingresada" onclick="btnRefacturar_Click" TabIndex="55" >
                                    <Image IconID="math_calculatenow_16x16" />
                                </dx:ASPxButton>
                            </td>
                            <td>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4"></td>
                            <td colspan="4"></td>
                        </tr>

                    </table>
                </li>
            </ul>

        </div>
        <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server" TabIndex="620"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" TabIndex="630"></asp:Label>
                </td>
            </tr>
        </table>
    </div>

</asp:Content>
