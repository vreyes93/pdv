﻿using System;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CrystalDecisions.CrystalReports.Engine;
using System.Net;
using DevExpress.XtraPrinting;
using Newtonsoft.Json;
using MF_Clases;

namespace PuntoDeVenta
{
    public partial class crediplus : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                HookOnFocus(this.Page as Control);

                txtFacturaInicial.Text = "";
                txtFacturaFinal.Text = "";
                txtRefacturar.Text = "";
            }

            Page.ClientScript.RegisterStartupScript(
            typeof(crediplus),
            "ScriptDoFocus",
            SCRIPT_DOFOCUS.Replace("REQUEST_LASTFOCUS", Request["__LASTFOCUS"]),
            true);

        }

        private const string SCRIPT_DOFOCUS = @"window.setTimeout('DoFocus()', 1); function DoFocus() {
            try {
                document.getElementById('REQUEST_LASTFOCUS').focus();
            } catch (ex) {}
        }";

        private void HookOnFocus(Control CurrentControl)
        {
            //checks if control is one of TextBox, DropDownList, ListBox or Button
            if ((CurrentControl is TextBox) ||
                (CurrentControl is DropDownList) ||
                (CurrentControl is ListBox) ||
                (CurrentControl is Button))
                //adds a script which saves active control on receiving focus 
                //in the hidden field __LASTFOCUS.
                (CurrentControl as WebControl).Attributes.Add(
                   "onfocus",
                   "try{document.getElementById('__LASTFOCUS').value=this.id} catch(e) {}");
            //checks if the control has children
            if (CurrentControl.HasControls())
                //if yes do them all recursively
                foreach (Control CurrentChildControl in CurrentControl.Controls)
                    HookOnFocus(CurrentChildControl);
        }

        protected void btnImprimirFacturas_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoImpresionFactura.Text = "";
                lbErrorImpresionFacturas.Text = "";

                string mInicial = txtFacturaInicial.Text.Trim().ToUpper();
                string mFinal = txtFacturaFinal.Text.Trim().ToUpper();

                if (mFinal.Trim().Length == 0) mFinal = mInicial;

                WebRequest request = WebRequest.Create(string.Format("{0}/impresionfacturas/?empresa=crediplus&inicial={1}&final={2}", Convert.ToString(Session["UrlRestServices"]), mInicial, mFinal));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                List<Clases.ImpresionFactura> mFacturas = new List<Clases.ImpresionFactura>();
                mFacturas = JsonConvert.DeserializeObject<List<Clases.ImpresionFactura>>(responseString);

                if (mFacturas.Count() == 0)
                {
                    lbInfoImpresionFactura.Text = "No se encontraron facturas para impresión";
                    lbErrorImpresionFacturas.Visible = true;
                    return;
                }

                dsPuntoVenta ds = new dsPuntoVenta();

                foreach (var item in mFacturas)
                {
                    for (int jj = 1; jj <= 3; jj++)
                    {
                        DataRow row = ds.Factura.NewRow();
                        row["NoFactura"] = item.NoFactura;
                        row["Fecha"] = item.Fecha;
                        row["Cliente"] = item.Cliente;
                        row["Nombre"] = item.Nombre;
                        row["Nit"] = item.Nit;
                        row["Direccion"] = item.Direccion;
                        row["Vendedor"] = item.Vendedor;
                        row["Monto"] = item.Monto;
                        row["MontoLetras"] = item.MontoLetras;
                        row["Observaciones"] = item.Observaciones;
                        row["Numero"] = jj;
                        row["Moneda"] = item.Moneda;
                        ds.Factura.Rows.Add(row);
                    }

                    foreach (var detalle in item.detalleFactura)
                    {
                        DataRow rowDet = ds.FacturaLinea.NewRow();
                        rowDet["NoFactura"] = detalle.NoFactura;
                        rowDet["Articulo"] = detalle.Articulo;
                        rowDet["Cantidad"] = detalle.Cantidad;
                        rowDet["Descripcion"] = detalle.Descripcion;
                        rowDet["PrecioUnitario"] = detalle.PrecioUnitario;
                        rowDet["PrecioTotal"] = detalle.PrecioTotal;
                        rowDet["Numero"] = 1;
                        ds.FacturaLinea.Rows.Add(rowDet);
                    }

                }

                string mFormatoFactura = "rptFacturaPV_Full_Lote.rpt";
                using (ReportDocument reporte = new ReportDocument())
                {
                    string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mFormatoFactura);
                    reporte.Load(p);

                    string mNombreDocumento = string.Format("FacturasCrediplus.pdf");
                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                    reporte.SetDataSource(ds);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }


            }
            catch (Exception ex)
            {
                lbErrorImpresionFacturas.Text = CatchClass.ExMessage(ex, "crediplus", "btnImprimirFacturas");
            }
        }

        protected void btnRefacturar_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoRefacturacion.Text = "";
                lbErrorRefacturacion.Text = "";

                string mInicial = txtFacturaInicial.Text.Trim().ToUpper();
                string mFinal = txtFacturaFinal.Text.Trim().ToUpper();

                if (mFinal.Trim().Length == 0) mFinal = mInicial;

                WebRequest request = WebRequest.Create(string.Format("{0}/refacturacionatid/{1}", Convert.ToString(Session["UrlRestServices"]), txtRefacturar.Text.Trim().ToUpper()));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string mRespuesta = responseString.ToString().Replace("\"", "");
                if (mRespuesta.ToLower().Contains("error"))
                {
                    lbErrorRefacturacion.Text = mRespuesta;
                }
                else
                {
                    lbInfoRefacturacion.Text = mRespuesta;

                    try
                    {
                        string[] mInfo = mRespuesta.Split(new string[] { " " }, StringSplitOptions.None);
                        txtFacturaInicial.Text = mInfo[7];
                    }
                    catch
                    {
                        //Nothing
                    }
                }
            }
            catch (Exception ex)
            {
                lbErrorRefacturacion.Text = CatchClass.ExMessage(ex, "crediplus", "btnRefacturar");
            }
        }

    }
}