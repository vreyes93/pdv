﻿using System;

namespace PuntoDeVenta
{
    public partial class descargar : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                string mNombreDocumento = Convert.ToString(Session["Archivo"]);

                Response.Clear();
                Response.ContentType = "application/pdf";

                Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                Response.WriteFile(@"C:\reportes\" + mNombreDocumento);

                Response.End();
            }

        }
    }
}