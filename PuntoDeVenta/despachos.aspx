﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="despachos.aspx.cs" Inherits="PuntoDeVenta.despachos" %>
<%@ Register assembly="DevExpress.Web.v16.2, Version=16.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #middle {
            vertical-align: middle;
        }
        .style1000
        {
            height: 40px;
        }
        a:-webkit-any-link{
            text-decoration: none;
        }        
    </style>

    <script type="text/javascript">
        function OnBatchEditEndEditing(s, e) {
            setTimeout(function () {
                s.UpdateEdit();
            }, 0);
        }
        function HidePopup() {
            popupArticulos.Hide();
        }
        function onDescriptionClick(s, e, key) {
            txtLinea.SetText(key);
            popupArticulos.Show();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="content2">
        <div class="clientes2">
    <h3>&nbsp;&nbsp;Despachos y Facturación</h3>

            <dx:ASPxPopupControl ID="ASPxPopupControl2" runat="server" ClientInstanceName="popupArticulos" HeaderText="Cambiar Artículo" HeaderStyle-BackColor="#ff8a3f" HeaderStyle-ForeColor="White"
            Modal="true" Width="500px" AllowDragging="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
            <HeaderStyle BackColor="#FF8A3F" ForeColor="White" />
                <ContentCollection>
                    <dx:PopupControlContentControl>
                        <dx:ASPxGridView ID="gridArticulosBuscar" EnableTheming="True" Theme="SoftOrange" visible="true"
                            runat="server" KeyFieldName="Linea" AutoGenerateColumns="False" OnHtmlDataCellPrepared="gridArticulosBuscar_HtmlDataCellPrepared" 
                            Border-BorderStyle="None" Width="650px" CssClass="dxGrid" >
                            <Styles>
                                <StatusBar><Border BorderStyle="None" />
<Border BorderStyle="None"></Border>
                                </StatusBar>
                                <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />

<RowHotTrack BackColor="#D9ECFF" ForeColor="#E35904"></RowHotTrack>

<FocusedRow BackColor="#FCD4A9" ForeColor="#E35E04"></FocusedRow>
                            </Styles>

<Settings ShowStatusBar="Hidden"></Settings>

                                <SettingsBehavior EnableRowHotTrack="True" AllowSort="false"></SettingsBehavior>
                                <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                CommandBatchEditUpdate="Aplicar cambios"
                                ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                            <Settings ShowStatusBar="Hidden" />
                            <SettingsSearchPanel Visible="true" ShowApplyButton="true" ShowClearButton="true" />
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="Articulo" ReadOnly="true" Caption="Artículo" Visible="true" Width="100px">
                                    <DataItemTemplate>
                                        <dx:ASPxButton ID="btnArticulo" runat="server" AutoPostBack="false" Text='<%# Bind("Articulo") %>' RenderMode="Link" OnClick="btnArticuloBuscar_Click" ToolTip="Haga clic aquí para seleccionar el artículo a cambiar"></dx:ASPxButton>
                                    </DataItemTemplate>
                                    <HeaderStyle BackColor="#ff8a3f"  />
                                    <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Descripcion" ReadOnly="true" Caption="Descripción" Visible="true" Width="550px">
                                    <HeaderStyle BackColor="#ff8a3f"  />
                                    <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>

<CellStyle Font-Size="X-Small"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Existencias" ReadOnly="true" Caption="Existencias" Visible="false" Width="20px">
                                    <HeaderStyle BackColor="#ff8a3f"  />
                                    <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                    <CellStyle Font-Size="X-Small"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Tooltip" ReadOnly="true" Caption="Tooltip" Visible="false" Width="20px">
                                    <HeaderStyle BackColor="#ff8a3f"  />
                                    <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                    <CellStyle Font-Size="X-Small"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <Styles>
                                <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                            </Styles>
                            <SettingsBehavior EnableRowHotTrack="true" />
                            <SettingsPager NumericButtonCount="3">
                                <Summary Visible="False" />
                            </SettingsPager>
                            <SettingsText SearchPanelEditorNullText="Ingrese aquí su criterio de búsqueda" CommandBatchEditUpdate="Aplicar cambios" CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"/>
                            <Border BorderStyle="None"></Border>
                        </dx:ASPxGridView>
                    </dx:PopupControlContentControl>
                </ContentCollection>
            </dx:ASPxPopupControl>

            <ul>
                <li>
                    
                    <table >
                        <tr>
                            <td colspan="3">
                                <a>Aprobación de facturas para despacho</a></td>
                            <td colspan="3" style="text-align: right">
                                <asp:Label ID="lbErrorDespachos" runat="server" Font-Bold="True" ForeColor="Red" ></asp:Label>
                                <asp:Label ID="lbInfoDespachos" runat="server" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                Rango de fechas por factura:</td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaInicial" runat="server" Theme="SoftOrange" CssClass="DateEditStyle" Width="140px" TabIndex="10">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaFinal" runat="server" Theme="SoftOrange" 
                                    CssClass="DateEditStyle" Width="140px" TabIndex="20">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkSeleccionarFechas" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar facturas según el rango de fechas seleccionado" 
                                    onclick="lkSeleccionarFechas_Click" TabIndex="30">Por fechas de factura</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Rango de fechas de entrega:</td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaInicialE" runat="server" Theme="SoftOrange" 
                                    CssClass="DateEditStyle" Width="140px" TabIndex="32">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaFinalE" runat="server" Theme="SoftOrange" 
                                    CssClass="DateEditStyle" Width="140px" TabIndex="34">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkSeleccionarFechasE" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar facturas según la fecha de entrega de las mismas" 
                                    onclick="lkSeleccionarFechasE_Click" TabIndex="36">Por fechas de entrega</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Código o parte del nombre cliente:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtCliente" runat="server" Width="140px" style="text-transform: uppercase;"
                                    CssClass="textBoxStyle" TabIndex="40" AutoPostBack="True" 
                                    ToolTip="Ingrese parte del código o parte del nombre para buscar" 
                                    ontextchanged="txtCliente_TextChanged" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkSeleccionarCliente" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar documentos según el cliente ingresado" 
                                    onclick="lkSeleccionarCliente_Click" TabIndex="50">Seleccionar cliente</asp:LinkButton>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkSeleccionarAprobadas" runat="server" 
                                    ToolTip="Haga clic aquí para ver las facturas aprobadas que aún no se han despachado" 
                                    onclick="lkSeleccionarAprobadas_Click" TabIndex="55">Ver aprobadas</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Tienda que desea consultar:</td>
                            <td>
                                <dx:ASPxComboBox ID="tienda" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle" 
                                    ValueType="System.String" Width="75px" DropDownStyle="DropDownList" 
                                    DropDownWidth="40" DropDownRows="25" TabIndex="60">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkSeleccionarTienda" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar documentos según la tienda seleccionada" 
                                    onclick="lkSeleccionarTienda_Click" TabIndex="70">Seleccionar tienda</asp:LinkButton>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPedido" runat="server" Visible="False" Width="20px" TabIndex="9999"></asp:TextBox>
                                <asp:TextBox ID="txtTienda" runat="server" Visible="False" Width="20px" TabIndex="9999"></asp:TextBox>
                                <dx:ASPxTextBox ID="txtLinea" runat="server" ClientInstanceName="txtLinea" CssClass="textBoxStyleHide" Width="40px" TabIndex="9999" Visible="true" ReadOnly="true" ForeColor="White"></dx:ASPxTextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style1000">
                                </td>
                            <td class="style1000">
                                Número de factura a buscar:</td>
                            <td class="style1000">
                                <dx:ASPxTextBox ID="txtFactura" runat="server" Width="140px" 
                                    CssClass="textBoxStyle" TabIndex="80" 
                                    ToolTip="Ingrese el número de factura" >
                                </dx:ASPxTextBox>
                            </td>
                            <td class="style1000">
                                <asp:LinkButton ID="lkCargarFactura" runat="server" 
                                    ToolTip="Haga clic aquí para cargar los artículos de la factura" 
                                    onclick="lkCargarFactura_Click" TabIndex="85">Seleccionar factura</asp:LinkButton>
                            </td>
                            <td class="style1000">
                                <dx:ASPxLabel ID="lbPedido" runat="server" TabIndex="87">
                                </dx:ASPxLabel>
                                </td>
                            <td class="style1000">
                                </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Código y nombre del cliente:</td>
                            <td colspan="3">
                                <dx:ASPxTextBox ID="txtNombreCliente" runat="server" Width="430px" 
                                    CssClass="textBoxStyle" TabIndex="90" 
                                    ToolTip="Este es el código y el nombre del cliente" ReadOnly="True" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkExpediente" runat="server" 
                                    ToolTip="Haga clic aquí para ver el expediente de esta factura" 
                                    onclick="lkExpediente_Click" TabIndex="92">Expediente</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Teléfonos del cliente:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtTelefonos" runat="server" Width="140px" 
                                    CssClass="textBoxStyle" TabIndex="94" 
                                    ToolTip="Este es el código y el nombre del cliente" ReadOnly="True" 
                                    Font-Size="X-Small" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Fecha de entrega:</strong></td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaEntrega" runat="server" Theme="SoftOrange" 
                                    CssClass="DateEditStyle" Width="140px" TabIndex="96" Font-Bold="True">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkVerFactura" runat="server" 
                                    ToolTip="Haga clic aquí para ver la factura en pantalla" 
                                    onclick="lkVerFactura_Click" TabIndex="87">Factura</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Dirección del cliente:</td>
                            <td colspan="3">
                                <dx:ASPxTextBox ID="txtDireccion" runat="server" Width="430px" 
                                    CssClass="textBoxStyle" TabIndex="98" 
                                    ToolTip="Esta es la dirección que saldrá impresa en el despacho" 
                                    ReadOnly="True" Font-Size="X-Small" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkCliente" runat="server" 
                                    ToolTip="Haga clic aquí para ver los datos del cliente" 
                                    onclick="lkCliente_Click" TabIndex="100">Cliente</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Persona que recibirá dónde cliente:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtPersonaRecibe" runat="server" Width="140px" 
                                    CssClass="textBoxStyle" TabIndex="102" 
                                    ToolTip="Este es el nombre de la persona que recibirá en casa de cliente" 
                                    ReadOnly="True" Font-Size="X-Small" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;Nombre del vendedor:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtVendedor" runat="server" Width="140px" 
                                    CssClass="textBoxStyle" TabIndex="104" 
                                    ToolTip="Este es el nombre del vendedor" 
                                    ReadOnly="True" Font-Size="X-Small" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkPedidos" runat="server" 
                                    ToolTip="Haga clic aquí para ver los pedidos del cliente" 
                                    onclick="lkPedidos_Click" TabIndex="106">Pedidos</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Observaciones del vendedor:</td>
                            <td colspan="3">
                                <dx:ASPxTextBox ID="txtObservacionesVendedor" runat="server" Width="430px" 
                                    CssClass="textBoxStyle" TabIndex="108" Font-Size="X-Small" 
                                    ToolTip="Estas son las observaciones que ingresó el vendedor" 
                                    ReadOnly="True" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridFacturaDet" EnableTheming="True" Theme="SoftOrange" visible="false"
                                    runat="server" KeyFieldName="Linea" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" 
                                    onbatchupdate="gridFacturaDet_BatchUpdate" 
                                    onrowupdating="gridFacturaDet_RowUpdating" 
                                    oncommandbuttoninitialize="gridFacturaDet_CommandButtonInitialize" 
                                    oncelleditorinitialize="gridFacturaDet_CellEditorInitialize" 
                                    onhtmldatacellprepared="gridFacturaDet_HtmlDataCellPrepared" 
                                    onhtmlrowprepared="gridFacturaDet_HtmlRowPrepared" TabIndex="110" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                        <SettingsBehavior EnableRowHotTrack="True" AllowSort="false"></SettingsBehavior>
                                        <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura" Visible="true" Width="100px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Linea" ReadOnly="true" Caption="Línea" Visible="false" Width="25px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Articulo" ReadOnly="true" Caption="Artículo" Visible="false" Width="100px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Articulo2" ReadOnly="true" Caption="Artículo" Visible="true" Width="100px">
                                            <DataItemTemplate>
                                                <dx:ASPxButton ID="btnArticulo2" EncodeHtml="false" runat="server" AutoPostBack="false" Text='<%# Bind("Articulo2") %>' RenderMode="Link" OnClick="btnArticulo2_Click" ToolTip="Haga clic aquí para cambiar el color del artículo"></dx:ASPxButton>
                                            </DataItemTemplate>
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Descripcion" ReadOnly="true" Caption="Descripción" Visible="true" Width="350px">
                                            <EditFormSettings Visible="False" />
                                            <DataItemTemplate>
                                                <dx:ASPxButton ID="btnDescripcion" EncodeHtml="false" runat="server" AutoPostBack="false" Text='<%# Bind("Descripcion") %>' RenderMode="Link" ToolTip="Haga clic aquí para cambiar el artículo" OnInit="btnDescripcion_Init" Font-Size="X-Small">
                                                </dx:ASPxButton>
                                            </DataItemTemplate>
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                            <CellStyle Font-Size="X-Small" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="Cantidad" ReadOnly="false" Caption="Cantidad" Visible="true" Width="50px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="Bodega" ReadOnly="false" Caption="Bodega" Visible="true" Width="20px" >
                                            <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="40" DropDownRows="25" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" >
                                                <Border BorderStyle="None"></Border>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="Localizacion" ReadOnly="false" Caption="Localización" Visible="true" Width="80px">
                                            <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="40" DropDownRows="5" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" >
                                                <Border BorderStyle="None"></Border>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataTextColumn FieldName="Tooltip" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Existencias" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Tipo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Cliente" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Consecutivo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Despacho" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Cobrador" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="NombreCliente" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Direccion" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="CostoTotal" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="CostoTotalDolar" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="LineaOrigen" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PrecioTotal" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Telefonos" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Pedido" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="NombreRecibe" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Vendedor" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ObservacionesVendedor" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="FechaEntrega" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Observaciones" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsEditing Mode="Batch" />
                                    <ClientSideEvents BatchEditEndEditing="OnBatchEditEndEditing" />
                                    <SettingsBehavior EnableRowHotTrack="true" />
                                    <SettingsPager PageSize="20" Visible="False">
                                    </SettingsPager>
                                    <SettingsText SearchPanelEditorNullText="Ingrese aquí su criterio de búsqueda" CommandBatchEditUpdate="Aplicar cambios" CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"/>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxComboBox ID="cbSerie" runat="server" Theme="SoftOrange" 
                                    CssClass="ComboBoxStyle" Width="140px" 
                                    DropDownWidth="40px" DropDownRows="25" TabIndex="210" SelectedIndex="0" 
                                    DropDownStyle="DropDownList" AutoPostBack="True" 
                                    onselectedindexchanged="cbSerie_SelectedIndexChanged" Visible="False">
                                    <Items>
                                        <dx:ListEditItem Selected="True" Text="Seleccione serie" Value="" />
                                        <dx:ListEditItem Text="Serie A" Value="A" />
                                        <dx:ListEditItem Text="Serie O" Value="O" />
                                        <dx:ListEditItem Text="Serie Sueña" Value="S" />
                                    </Items>
                                </dx:ASPxComboBox>                            
                            </td>
                            <td>
                                Observaciones para el despacho:</td>
                            <td colspan="3">
                                <dx:ASPxTextBox ID="txtObservaciones" runat="server" Width="430px" style="text-transform: uppercase;"
                                    CssClass="textBoxStyle" TabIndex="115" 
                                    ToolTip="Ingrese las observaciones del envío" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkGrabarDespacho" runat="server" 
                                    ToolTip="Haga clic aquí para grabar el despacho" 
                                    onclick="lkGrabarDespacho_Click" TabIndex="230" Visible="False">Grabar despacho</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td colspan="4" style="text-align: center">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxTextBox ID="txtDespacho" runat="server" Width="140px" 
                                    CssClass="textBoxStyle" TabIndex="220" 
                                    ToolTip="Este es el número de envío a generar" Visible="False">
                                </dx:ASPxTextBox>
                            </td>
                            <td colspan="4" style="text-align: center">
                                <asp:LinkButton ID="lkAprobarFactura" runat="server" 
                                    ToolTip="Haga clic aquí para aprobar la factura" 
                                    onclick="lkAprobarFactura_Click" TabIndex="120">Aprobar factura</asp:LinkButton>
                            &nbsp;|&nbsp;
                                <asp:LinkButton ID="lkDesAprobarFactura" runat="server" 
                                    ToolTip="Haga clic aquí para desaprobar la factura" 
                                    onclick="lkDesAprobarFactura_Click" TabIndex="125">Desaprobar Factura</asp:LinkButton>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnImprimirDespacho" runat="server" Width="8px" Height="8px"
                                    ToolTip="Imprimir el envío" onclick="btnImprimirDespacho_Click" 
                                    TabIndex="240" Visible="False" >
                                    <Image IconID="print_print_16x16office2013">
                                    </Image>
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="ASPxButton1" runat="server" Height="8px" 
                                    onclick="ASPxButton1_Click" TabIndex="361" ToolTip="Limpiar la pantalla" 
                                    Width="8px">
                                    <Image IconID="actions_clear_16x16">
                                    </Image>
                                </dx:ASPxButton>                            
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridFacturas" EnableTheming="True" Theme="SoftOrange" 
                                    runat="server" KeyFieldName="Factura" AutoGenerateColumns="False" 
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" 
                                    onbatchupdate="gridFacturas_BatchUpdate" 
                                    onrowupdating="gridFacturas_RowUpdating" 
                                    oncommandbuttoninitialize="gridFacturas_CommandButtonInitialize" 
                                    oncelleditorinitialize="gridFacturas_CellEditorInitialize" 
                                    onfocusedrowchanged="gridFacturas_FocusedRowChanged" TabIndex="130" 
                                    Visible="False" onhtmlrowprepared="gridFacturas_HtmlRowPrepared" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                        <SettingsBehavior EnableRowHotTrack="True"></SettingsBehavior>
                                        <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                        CommandBatchEditUpdate="Aplicar cambios" 
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura" Visible="true" Width="85px">
                                            <DataItemTemplate>
                                                <asp:LinkButton ID="lkFacturaCliente" runat="server" 
                                                    Text='<%# Bind("Factura") %>' CausesValidation="true" CommandName="Select" 
                                                    ToolTip="Haga clic aquí para seleccionar la factura" ></asp:LinkButton>
                                            </DataItemTemplate>
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Fecha" ReadOnly="true" Caption="Fecha" Visible="true" Width="65px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="FechaEntrega" ReadOnly="true" Caption="Fecha Entrega" Visible="true" Width="90px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Cliente" ReadOnly="true" Caption="Cliente" Visible="true" Width="60px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreCliente" ReadOnly="true" Caption="Nombre Cliente" Visible="true" Width="350px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                            <CellStyle Font-Size="X-Small" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Monto" ReadOnly="true" Caption="Monto" Visible="true" Width="75px" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Aprobada" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="100" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>                                
                            </td>
                        </tr>
                    </table>
                </li>
            </ul>

        </div>
        <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server" TabIndex="620"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" TabIndex="630"></asp:Label>
                </td>
            </tr>
        </table>
    </div>

</asp:Content>
