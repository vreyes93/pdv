﻿using System;
using System.Data;
using System.IO;
using System.ComponentModel;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CrystalDecisions.CrystalReports.Engine;
using DevExpress.Web.Data;
using DevExpress.Web;
//Estos son los cambios a medias de comisiones
namespace PuntoDeVenta
{
    public partial class despachos : BasePage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsFacturas"];

                gridFacturas.DataSource = ds;
                gridFacturas.DataMember = "documentos";
                gridFacturas.DataBind();
                gridFacturas.SettingsPager.PageSize = 35;
            }
            catch (Exception ex)
            {
                lbErrorDespachos.Text = string.Format("Sesión expirada, por favor salga del sistema y entre de nuevo {0}.", ex.Message);
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                HookOnFocus(this.Page as Control);
                
                ViewState["url"] = Convert.ToString(Session["Url"]);
                ViewState["urlPrecios"] = Convert.ToString(Session["UrlPrecios"]);

                Session["dsArticulos"] = null;
                ViewState["Accion"] = null;
                Session["dsFacturas"] = null;

                CargarBodegas();
                CargarArticulosBusqueda();
                LimpiarControlesDespacho();

                if (Request.QueryString["f"] != null)
                {
                    txtFactura.Text = Request.QueryString["f"].ToString();
                    CargarFactura();

                    var ws = new wsPuntoVenta.wsPuntoVenta(); string mInfo = ""; string mObservaciones = "";
                    if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                    ws.InfoCambioArticulo(ref mInfo, ref mObservaciones, txtFactura.Text);
                    lbInfoDespachos.Text = string.Format("{0} {1}", mInfo, mObservaciones);
                }
            }
            else
            {
                try
                {
                    DataSet dsTodos = new DataSet();
                    dsTodos = (DataSet)Session["dsTodos"];

                    gridArticulosBuscar.DataSource = dsTodos;
                    gridArticulosBuscar.DataMember = "articulos";
                    gridArticulosBuscar.DataBind();
                    gridArticulosBuscar.FocusedRowIndex = -1;
                    gridArticulosBuscar.SettingsPager.PageSize = 35;
                }
                catch
                {
                    lbErrorDespachos.Text = "Sesión expirada, por favor salga del sistema y vuelva a ingresar.";
                }
            }

            Page.ClientScript.RegisterStartupScript(
                typeof(despachos),
                "ScriptDoFocus",
                SCRIPT_DOFOCUS.Replace("REQUEST_LASTFOCUS", Request["__LASTFOCUS"]),
                true);
        }

        private const string SCRIPT_DOFOCUS = @"window.setTimeout('DoFocus()', 1); function DoFocus() {
            try {
                document.getElementById('REQUEST_LASTFOCUS').focus();
            } catch (ex) {}
        }";

        private void HookOnFocus(Control CurrentControl)
        {
            //checks if control is one of TextBox, DropDownList, ListBox or Button
            if ((CurrentControl is TextBox) ||
                (CurrentControl is DropDownList) ||
                (CurrentControl is ListBox) ||
                (CurrentControl is Button))
                //adds a script which saves active control on receiving focus 
                //in the hidden field __LASTFOCUS.
                (CurrentControl as WebControl).Attributes.Add(
                   "onfocus",
                   "try{document.getElementById('__LASTFOCUS').value=this.id} catch(e) {}");
            //checks if the control has children
            if (CurrentControl.HasControls())
                //if yes do them all recursively
                foreach (Control CurrentChildControl in CurrentControl.Controls)
                    HookOnFocus(CurrentChildControl);
        }

        void CargarBodegas()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveTiendas();
            
            tienda.DataSource = q;
            tienda.ValueField = "Tienda";
            tienda.ValueType = typeof(System.String);
            tienda.TextField = "Tienda";
            tienda.DataBindItems();
        }

        void CargarArticulosBusqueda()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            DataSet dsTodos = new DataSet();
            dsTodos = ws.DevuelveArticulosTodos();

            gridArticulosBuscar.DataSource = dsTodos;
            gridArticulosBuscar.DataMember = "articulos";
            gridArticulosBuscar.DataBind();
            gridArticulosBuscar.FocusedRowIndex = -1;
            gridArticulosBuscar.SettingsPager.PageSize = 35;

            Session["dsTodos"] = dsTodos;
        }

        void LimpiarControlesDespacho()
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                DateTime mFechaFinal = DateTime.Now.Date;
                DateTime mFechaInicial = mFechaFinal.AddDays(-7);

                fechaInicial.Value = mFechaInicial;
                fechaFinal.Value = mFechaFinal;

                fechaInicialE.Value = DateTime.Now.Date;
                fechaFinalE.Value = DateTime.Now.Date;

                txtCliente.Text = "";
                tienda.Value = "F01";
                txtFactura.Text = "";
                txtFactura.ToolTip = "Ingrese el número de factura a buscar";
                lbPedido.Text = "";

                fechaInicial.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al inicializar los despachos. {0} {1}", ex.Message, m);
            }
        }

        protected void gridFacturas_BatchUpdate(object sender, ASPxDataBatchUpdateEventArgs e)
        {

        }

        protected void gridFacturas_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {

        }

        protected void gridFacturas_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {

        }

        protected void gridFacturas_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {

        }

        protected void lkSeleccionarFechas_Click(object sender, EventArgs e)
        {
            CargarFacturas("F");
        }

        protected void lkSeleccionarFechasE_Click(object sender, EventArgs e)
        {
            CargarFacturas("E");
        }

        protected void lkSeleccionarCliente_Click(object sender, EventArgs e)
        {
            CargarFacturas("C");
        }

        protected void lkSeleccionarTienda_Click(object sender, EventArgs e)
        {
            CargarFacturas("T");
        }

        protected void lkSeleccionarAprobadas_Click(object sender, EventArgs e)
        {
            CargarFacturas("A");
        }

        void CargarFacturas(string tipo)
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                gridFacturas.Visible = false;
                gridFacturaDet.Visible = false;

                txtFactura.Text = "";
                txtPedido.Text = "";
                txtTienda.Text = "";
                txtNombreCliente.Text = "";
                txtDireccion.Text = "";
                txtTelefonos.Text = "";
                txtObservaciones.Text = "";
                txtPersonaRecibe.Text = "";
                txtVendedor.Text = "";
                txtObservacionesVendedor.Text = "";
                fechaEntrega.Value = DateTime.Now.Date;
                txtFactura.ToolTip = "Ingrese el número de factura a buscar";
                lbPedido.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                DateTime mFechaInicial = Convert.ToDateTime(fechaInicial.Value);
                DateTime mFechaFinal = Convert.ToDateTime(fechaFinal.Value);

                if (tipo == "E")
                {
                    mFechaInicial = Convert.ToDateTime(fechaInicialE.Value);
                    mFechaFinal = Convert.ToDateTime(fechaFinalE.Value);
                }

                DataSet ds = new DataSet(); string mMensaje = "";
                ds = ws.DocumentosPorDespachar(ref mMensaje, mFechaInicial, mFechaFinal, txtCliente.Text.Trim().ToUpper(), Convert.ToString(tienda.Value), tipo);

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorDespachos.Text = mMensaje;
                    return;
                }

                if (ds.Tables["documentos"].Rows.Count > 0)
                {
                    gridFacturas.Visible = true;
                    gridFacturas.DataSource = ds;
                    gridFacturas.DataMember = "documentos";
                    gridFacturas.DataBind();
                    gridFacturas.FocusedRowIndex = -1;

                    Session["dsFacturas"] = ds;
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al mostrar los documentos. {0} {1}", ex.Message, m);
            }

        }

        void CargarFactura()
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                if (txtFactura.Text.Trim().Length == 0) return;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                DataSet ds = new DataSet(); string mMensaje = "";
                ds = ws.DocumentoPorDespachar(ref mMensaje, txtFactura.Text.Trim().ToUpper(), Convert.ToString(ViewState["Usuario"]));

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorDespachos.Text = mMensaje;
                    return;
                }

                if (ds.Tables["documento"].Rows.Count > 0)
                {
                    //gridFacturas.Visible = false;
                    gridFacturaDet.Visible = true;
                    gridFacturaDet.DataSource = ds;
                    gridFacturaDet.DataMember = "documento";
                    gridFacturaDet.DataBind();
                    gridFacturaDet.FocusedRowIndex = -1;

                    Session["Cliente"] = Convert.ToString(ds.Tables["documento"].Rows[0]["Cliente"]);
                    txtDespacho.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Despacho"]);
                    txtNombreCliente.Text = string.Format("{0} - {1}", Convert.ToString(ds.Tables["documento"].Rows[0]["Cliente"]), Convert.ToString(ds.Tables["documento"].Rows[0]["NombreCliente"]));
                    txtTelefonos.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Telefonos"]);
                    txtDireccion.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Direccion"]);
                    txtTienda.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Cobrador"]);
                    txtPedido.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Pedido"]);
                    txtPersonaRecibe.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["NombreRecibe"]);
                    txtVendedor.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Vendedor"]);
                    txtObservacionesVendedor.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["ObservacionesVendedor"]);
                    fechaEntrega.Value = Convert.ToDateTime(ds.Tables["documento"].Rows[0]["FechaEntrega"]);
                    txtObservaciones.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Observaciones"]);
                    txtFactura.ToolTip = string.Format("Esta factura tiene el Pedido No. {0}", Convert.ToString(ds.Tables["documento"].Rows[0]["Pedido"]));
                    lbPedido.Text = string.Format("Pedido No. {0}", Convert.ToString(ds.Tables["documento"].Rows[0]["Pedido"])); 

                    txtTelefonos.ReadOnly = true;
                    txtDireccion.ReadOnly = true;
                    txtNombreCliente.ReadOnly = true;

                    string mSerie = "O";
                    if (Convert.ToString(ds.Tables["documento"].Rows[0]["Consecutivo"]) == "DESPA") mSerie = "A";
                    if (Convert.ToString(ds.Tables["documento"].Rows[0]["Consecutivo"]) == "DES SUENIA") mSerie = "S";

                    cbSerie.Value = mSerie;

                    if (Convert.ToString(ds.Tables["documento"].Rows[0]["Consecutivo"]) == "DES SUENIA")
                    {
                        txtTelefonos.ReadOnly = false;
                        txtDireccion.ReadOnly = false;
                        txtNombreCliente.ReadOnly = false;

                        txtNombreCliente.Focus();
                    }
                    else
                    {
                        txtObservaciones.Focus();
                    }
                }

                Session["dsArticulos"] = ds;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al mostrar el documento. {0} {1}", ex.Message, m);
            }
        }

        protected void CancelEditing(CancelEventArgs e)
        {
            e.Cancel = true;
            gridFacturaDet.CancelEdit();
        }

        void UpdateFactura(string linea, string bodega, string cantidad, string localizacion)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsArticulos"];

                ds.Tables["documento"].Rows.Find(linea)["Bodega"] = bodega;
                ds.Tables["documento"].Rows.Find(linea)["Cantidad"] = Convert.ToInt32(cantidad);
                ds.Tables["documento"].Rows.Find(linea)["Localizacion"] = localizacion;
                Session["dsArticulos"] = ds;

                gridFacturaDet.DataSource = ds;
                gridFacturaDet.DataMember = "documento";
                gridFacturaDet.DataBind();
                gridFacturaDet.FocusedRowIndex = -1;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al actualizar los datos UPDATE {0} {1}", ex.Message, m);
            }
        }


        protected void gridFacturaDet_BatchUpdate(object sender, ASPxDataBatchUpdateEventArgs e)
        {
            foreach (var args in e.UpdateValues)
                UpdateFactura(args.Keys["Linea"].ToString(), args.NewValues["Bodega"].ToString(), args.NewValues["Cantidad"].ToString(), args.NewValues["Localizacion"].ToString());

            e.Handled = true;
        }

        protected void gridFacturaDet_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            UpdateFactura(e.Keys["Linea"].ToString(), e.NewValues["Bodega"].ToString(), e.NewValues["Cantidad"].ToString(), e.NewValues["Localizacion"].ToString());
            CancelEditing(e);
        }

        protected void gridFacturaDet_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            if (e.Column.FieldName == "Bodega")
            {
                var q = ws.DevuelveTiendas();

                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                cmb.DataSource = q;
                cmb.ValueField = "Tienda";
                cmb.ValueType = typeof(System.String);
                cmb.TextField = "Tienda";
                cmb.DataBindItems();
            }

            if (e.Column.FieldName == "Localizacion")
            {
                DataSet ds = new DataSet();
                ds = ws.Localizaciones();

                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                cmb.DataSource = ds.Tables["Localizaciones"];
                cmb.ValueField = "Localizacion";
                cmb.ValueType = typeof(System.String);
                cmb.TextField = "Localizacion";
                cmb.DataBindItems();
            }
        }

        protected void gridFacturaDet_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == ColumnCommandButtonType.Update || e.ButtonType == ColumnCommandButtonType.Cancel) e.Visible = false;
        }

        protected void lkCargarFactura_Click(object sender, EventArgs e)
        {
            CargarFactura();
        }

        protected void txtCliente_TextChanged(object sender, EventArgs e)
        {
            CargarFacturas("C");
        }

        protected void lkGrabarDespacho_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsArticulos"];

                if (!ws.GrabarDespachoF01(txtDespacho.Text.Trim().ToUpper(), ref mMensaje, Convert.ToString(Session["Usuario"]), txtObservaciones.Text, ds, txtNombreCliente.Text, txtTelefonos.Text, txtDireccion.Text, "", Convert.ToDateTime(fechaEntrega.Value), "0", "O"))
                {
                    lbErrorDespachos.Text = mMensaje;
                    return;
                }

                //gridFacturas.Visible = false;
                gridFacturaDet.Visible = false;

                txtFactura.Text = "";
                txtPedido.Text = "";
                txtTienda.Text = "";
                txtNombreCliente.Text = "";
                txtDireccion.Text = "";
                txtTelefonos.Text = "";
                txtObservaciones.Text = "";
                txtPersonaRecibe.Text = "";
                txtVendedor.Text = "";
                txtObservacionesVendedor.Text = "";
                fechaEntrega.Value = DateTime.Now.Date;
                txtFactura.ToolTip = "Ingrese el número de factura a buscar";
                lbPedido.Text = "";

                lbInfoDespachos.Text = mMensaje;
                btnImprimirDespacho.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al grabar el despacho. {0} {1}", ex.Message, m);
            }
        }

        protected void gridFacturaDet_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.VisibleIndex >= 0) e.Cell.ToolTip = e.GetValue("Tooltip").ToString();
        }

        protected void gridFacturas_FocusedRowChanged(object sender, EventArgs e)
        {
            //if (!gridFacturas.Visible) return;
            if (gridFacturas.FocusedRowIndex == -1) return;
            if (txtFactura.Text.Trim().Length > 0) return;
            if (Convert.ToString(gridFacturas.GetRowValues(gridFacturas.FocusedRowIndex, "Factura")).Trim().Length == 0) return;
            if (txtFactura.Text == Convert.ToString(gridFacturas.GetRowValues(gridFacturas.FocusedRowIndex, "Factura"))) return;

            txtFactura.Text = Convert.ToString(gridFacturas.GetRowValues(gridFacturas.FocusedRowIndex, "Factura"));
            CargarFactura();
        }

        protected void gridFacturas_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (Convert.ToString(e.GetValue("Aprobada")) == "S") e.Row.BackColor = System.Drawing.Color.LightGreen;
        }

        protected void gridFacturaDet_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (Convert.ToInt32(e.GetValue("Existencias")) == 0 && Convert.ToString(e.GetValue("Tipo")) != "K") e.Row.ForeColor = System.Drawing.Color.Red;
        }

        void ImprimirExpediente(string pedido)
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.PedidoFacturado(pedido))
                {
                    lbErrorDespachos.Text = "Este pedido NO se encuentra facturado, no es posible continuar";
                    return;
                }

                dsPuntoVenta ds = new dsPuntoVenta();
                var qExpediente = ws.DevuelveExpediente(pedido, "");
                var qExpedienteDet = ws.DevuelveExpedienteDet(pedido, "");

                DataRow mRowExpediente = ds.Expediente.NewRow();
                mRowExpediente["Cliente"] = qExpediente[0].Cliente;
                mRowExpediente["Pedido"] = qExpediente[0].Pedido;
                mRowExpediente["Fecha"] = qExpediente[0].Fecha;
                mRowExpediente["Vendedor"] = qExpediente[0].Vendedor;
                mRowExpediente["CodigoVendedor"] = qExpediente[0].CodigoVendedor;
                mRowExpediente["Tienda"] = qExpediente[0].Tienda;
                mRowExpediente["PrimerNombre"] = qExpediente[0].PrimerNombre;
                mRowExpediente["SegundoNombre"] = qExpediente[0].SegundoNombre;
                mRowExpediente["PrimerApellido"] = qExpediente[0].PrimerApellido;
                mRowExpediente["SegundoApellido"] = qExpediente[0].SegundoApellido;
                mRowExpediente["ApellidoCasada"] = qExpediente[0].ApellidoCasada;
                mRowExpediente["DPI"] = qExpediente[0].DPI;
                mRowExpediente["NIT"] = qExpediente[0].NIT;
                mRowExpediente["Direccion"] = qExpediente[0].Direccion;
                mRowExpediente["Colonia"] = qExpediente[0].Colonia;
                mRowExpediente["Zona"] = qExpediente[0].Zona;
                mRowExpediente["Departamento"] = qExpediente[0].Departamento;
                mRowExpediente["Municipio"] = qExpediente[0].Municipio;
                mRowExpediente["TelefonoResidencia"] = qExpediente[0].TelefonoResidencia;
                mRowExpediente["TelefonoCelular"] = qExpediente[0].TelefonoCelular;
                mRowExpediente["TelefonoTrabajo"] = qExpediente[0].TelefonoTrabajo;
                mRowExpediente["Email"] = qExpediente[0].Email;
                mRowExpediente["EntregarMismaDireccion"] = qExpediente[0].EntregarMismaDireccion;
                mRowExpediente["NombreEntrega"] = qExpediente[0].NombreEntrega;
                mRowExpediente["DireccionEntrega"] = qExpediente[0].DireccionEntrega;
                mRowExpediente["ColoniaEntrega"] = qExpediente[0].ColoniaEntrega;
                mRowExpediente["ZonaEntrega"] = qExpediente[0].ZonaEntrega;
                mRowExpediente["DepartamentoEntrega"] = qExpediente[0].DepartamentoEntrega;
                mRowExpediente["MunicipioEntrega"] = qExpediente[0].MunicipioEntrega;
                mRowExpediente["EntraCamion"] = qExpediente[0].EntraCamion;
                mRowExpediente["EntraPickup"] = qExpediente[0].EntraPickup;
                mRowExpediente["EsSegundoPiso"] = qExpediente[0].EsSegundoPiso;
                mRowExpediente["Notas"] = qExpediente[0].Notas;
                mRowExpediente["BodegaSale"] = qExpediente[0].BodegaSale;
                mRowExpediente["TiendaSale"] = qExpediente[0].TiendaSale;
                mRowExpediente["ObsVendedor"] = qExpediente[0].ObsVendedor;
                mRowExpediente["ObsGerencia"] = qExpediente[0].ObsGerencia;
                mRowExpediente["Gerente"] = qExpediente[0].Gerente;
                mRowExpediente["Factura"] = qExpediente[0].Factura;
                mRowExpediente["Garantia"] = qExpediente[0].Garantia;
                mRowExpediente["Promocion"] = qExpediente[0].Promocion;
                mRowExpediente["Financiera"] = qExpediente[0].Financiera;
                mRowExpediente["QuienAutorizo"] = qExpediente[0].QuienAutorizo;
                mRowExpediente["NoAutorizacion"] = qExpediente[0].NoAutorizacion;
                mRowExpediente["Enganche"] = qExpediente[0].Enganche;
                mRowExpediente["Plan"] = qExpediente[0].Plan;
                mRowExpediente["NoSolicitud"] = qExpediente[0].NoSolicitud;
                mRowExpediente["FechaHora"] = qExpediente[0].FechaHora;
                mRowExpediente["CantidadPagos1"] = qExpediente[0].CantidadPagos1;
                mRowExpediente["MontoPagos1"] = qExpediente[0].MontoPagos1;
                mRowExpediente["CantidadPagos2"] = qExpediente[0].CantidadPagos2;
                mRowExpediente["MontoPagos2"] = qExpediente[0].MontoPagos2;
                mRowExpediente["Despacho"] = qExpediente[0].Despacho;
                mRowExpediente["Radio"] = qExpediente[0].Radio;
                mRowExpediente["Volante"] = qExpediente[0].Volante;
                mRowExpediente["PrensaLibre"] = qExpediente[0].PrensaLibre;
                mRowExpediente["ElQuetzalteco"] = qExpediente[0].ElQuetzalteco;
                mRowExpediente["NuestroDiario"] = qExpediente[0].NuestroDiario;
                mRowExpediente["Internet"] = qExpediente[0].Internet;
                mRowExpediente["TV"] = qExpediente[0].TV;
                mRowExpediente["Otros"] = qExpediente[0].Otros;
                mRowExpediente["ObsOtros"] = qExpediente[0].ObsOtros;
                mRowExpediente["AutorizacionEspecial"] = qExpediente[0].AutorizacionEspecial;
                mRowExpediente["ObservacionesAdicionales"] = qExpediente[0].ObservacionesAdicionales;
                mRowExpediente["IndicacionesLlegar"] = qExpediente[0].IndicacionesLlegar;
                mRowExpediente["EntregaAMPM"] = qExpediente[0].EntregaAMPM;
                mRowExpediente["FechaEntrega"] = qExpediente[0].FechaEntrega;
                mRowExpediente["NotasPrecio"] = qExpediente[0].NotasPrecio;
                mRowExpediente["Factor"] = qExpediente[0].Factor;
                mRowExpediente["Recibos"] = qExpediente[0].Recibos;
                ds.Expediente.Rows.Add(mRowExpediente);

                for (int ii = 0; ii < qExpedienteDet.Length; ii++)
                {
                    DataRow mRowExpedienteDet = ds.ExpedienteDet.NewRow();
                    mRowExpedienteDet["Cliente"] = qExpedienteDet[ii].Cliente;
                    mRowExpedienteDet["Pedido"] = qExpedienteDet[ii].Pedido;
                    mRowExpedienteDet["Articulo"] = qExpedienteDet[ii].Articulo;
                    mRowExpedienteDet["Descripcion"] = qExpedienteDet[ii].Descripcion;
                    mRowExpedienteDet["PrecioLista"] = qExpedienteDet[ii].PrecioLista;
                    mRowExpedienteDet["Cantidad"] = qExpedienteDet[ii].Cantidad;
                    mRowExpedienteDet["Factor"] = qExpedienteDet[ii].Factor;
                    mRowExpedienteDet["PrecioTotal"] = qExpedienteDet[ii].PrecioTotal;
                    mRowExpedienteDet["Bodega"] = qExpedienteDet[ii].Bodega;
                    ds.ExpedienteDet.Rows.Add(mRowExpedienteDet);
                }

                int mRows = ds.ExpedienteDet.Rows.Count;
                for (int ii = mRows; ii < 16; ii++)
                {
                    DataRow mRowExpedienteDet = ds.ExpedienteDet.NewRow();
                    mRowExpedienteDet["Cliente"] = qExpediente[0].Cliente;
                    mRowExpedienteDet["Pedido"] = qExpediente[0].Pedido;
                    mRowExpedienteDet["Articulo"] = "";
                    mRowExpedienteDet["Descripcion"] = "";
                    mRowExpedienteDet["PrecioLista"] = 0;
                    mRowExpedienteDet["Cantidad"] = 0;
                    mRowExpedienteDet["Factor"] = 0;
                    mRowExpedienteDet["PrecioTotal"] = 0;
                    mRowExpedienteDet["Bodega"] = "";
                    ds.ExpedienteDet.Rows.Add(mRowExpedienteDet);
                }

                using (ReportDocument ReporteExpediente = new ReportDocument())
                {
                    string m = (Request.PhysicalApplicationPath + "reportes/rptExpediente.rpt");
                    ReporteExpediente.Load(m);

                    string mExpediente = string.Format("Expediente_{0}.pdf", pedido);
                    ReporteExpediente.SetDataSource(ds);

                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");

                    try
                    {
                        if (File.Exists(@"C:\reportes\" + mExpediente)) File.Delete(@"C:\reportes\" + mExpediente);
                    }
                    catch
                    {
                        mExpediente = mExpediente.Replace(".pdf", "");
                        mExpediente = string.Format("{0}_2.pdf", mExpediente);

                        try
                        {
                            if (File.Exists(@"C:\reportes\" + mExpediente)) File.Delete(@"C:\reportes\" + mExpediente);
                        }
                        catch
                        {
                            mExpediente = mExpediente.Replace(".pdf", "");
                            mExpediente = string.Format("{0}_2.pdf", mExpediente);

                            try
                            {
                                if (File.Exists(@"C:\reportes\" + mExpediente)) File.Delete(@"C:\reportes\" + mExpediente);
                            }
                            catch
                            {
                                mExpediente = mExpediente.Replace(".pdf", "");
                                mExpediente = string.Format("{0}_2.pdf", mExpediente);
                                if (File.Exists(@"C:\reportes\" + mExpediente)) File.Delete(@"C:\reportes\" + mExpediente);
                            }
                        }
                    }

                    ReporteExpediente.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mExpediente);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mExpediente);
                    Response.WriteFile(@"C:\reportes\" + mExpediente);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorDespachos.Text = string.Format("Error al imprimir el expediente {0} {1}", ex.Message, m);
            }
        }


        protected void cbSerie_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                if (cbSerie.Value.ToString() == "")
                {
                    txtDespacho.Text = "";
                    return;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                txtDespacho.Text = ws.NumeroDespachoF01(cbSerie.Value.ToString());
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al seleccionar el consecutivo. {0} {1}", ex.Message, m);
            }
        }

        protected void btnImprimirDespacho_Click(object sender, EventArgs e)
        {
            string mMensaje = "";

            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                DataSet dsInfo = new DataSet();
                wsPuntoVenta.Despacho[] q = new wsPuntoVenta.Despacho[1];
                wsPuntoVenta.DespachoLinea[] qLinea = new wsPuntoVenta.DespachoLinea[1];

                mMensaje = ""; string mFactura = "";
                if (!ws.DevuelveImpresionDespacho(txtDespacho.Text.Trim().ToUpper(), mFactura, ref mMensaje, ref q, ref qLinea, ref dsInfo))
                {
                    lbErrorDespachos.Text = mMensaje;
                    return;
                }

                dsPuntoVenta ds = new dsPuntoVenta();

                DataRow row = ds.Despacho.NewRow();
                row["NoDespacho"] = q[0].NoDespacho;
                row["Fecha"] = q[0].Fecha;
                row["Cliente"] = q[0].Cliente;
                row["Nombre"] = q[0].Nombre;
                row["Direccion"] = q[0].Direccion;
                row["Indicaciones"] = q[0].Indicaciones;
                row["Telefonos"] = q[0].Telefonos;
                row["FormaPago"] = q[0].FormaPago;
                row["Tienda"] = q[0].Tienda;
                row["Vendedor"] = q[0].Vendedor;
                row["NombreVendedor"] = q[0].NombreVendedor;
                row["Factura"] = q[0].Factura;
                row["Observaciones"] = q[0].Observaciones;
                row["Notas"] = q[0].Notas;
                row["Transportista"] = q[0].Transportista;
                row["NombreTransportista"] = q[0].NombreTransportista;
                ds.Despacho.Rows.Add(row);

                for (int ii = 0; ii < qLinea.Count(); ii++)
                {
                    DataRow rowDet = ds.DespachoLinea.NewRow();
                    rowDet["NoDespacho"] = qLinea[ii].NoDespacho;
                    rowDet["Articulo"] = qLinea[ii].Articulo;
                    rowDet["Cantidad"] = qLinea[ii].Cantidad;
                    rowDet["Descripcion"] = qLinea[ii].Descripcion;
                    rowDet["Bodega"] = qLinea[ii].Bodega;
                    rowDet["Localizacion"] = qLinea[ii].Localizacion;
                    ds.DespachoLinea.Rows.Add(rowDet);
                }

                using (ReportDocument reporte = new ReportDocument())
                {
                    string p = (Request.PhysicalApplicationPath + "reportes/rptDespachoPVF01.rpt");
                    reporte.Load(p);

                    string mNombreDocumento = string.Format("{0}.pdf", txtDespacho.Text);

                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                    reporte.SetDataSource(ds);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al imprimir el envío. {0} {1} {2}", ex.Message, mMensaje, m);
                return;
            }

        }

        protected void lkExpediente_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                if (txtPedido.Text.Trim().Length == 0)
                {
                    lbErrorDespachos.Text = "Debe seleccionar una factura";
                    return;
                }

                if (txtTienda.Text == "F01")
                {
                    lbErrorDespachos.Text = "El expediente sólo está disponible para facturas de tiendas";
                    return;
                }

                ImprimirExpediente(txtPedido.Text);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al imprimir el expediente. {0} {1}", ex.Message, m);
                return;
            }
        }

        protected void lkVerFactura_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                if (txtPedido.Text.Trim().Length == 0)
                {
                    lbErrorDespachos.Text = "Debe seleccionar una factura";
                    return;
                }

                if (txtTienda.Text == "F01")
                {
                    lbErrorDespachos.Text = "Esta opción sólo está disponible para facturas de tiendas";
                    return;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                wsPuntoVenta.Factura[] q = new wsPuntoVenta.Factura[1];
                wsPuntoVenta.FacturaLinea[] qLinea = new wsPuntoVenta.FacturaLinea[1];

                string mMensaje = "";
                if (!ws.DevuelveImpresionFactura(txtFactura.Text, ref mMensaje, ref q, ref qLinea))
                {
                    lbErrorDespachos.Text = mMensaje;
                    return;
                }

                dsPuntoVenta ds = new dsPuntoVenta();

                DataRow row = ds.Factura.NewRow();
                row["NoFactura"] = q[0].NoFactura;
                row["Fecha"] = q[0].Fecha;
                row["Cliente"] = q[0].Cliente;
                row["Nombre"] = q[0].Nombre;
                row["Nit"] = q[0].Nit;
                row["Direccion"] = q[0].Direccion;
                row["Vendedor"] = q[0].Vendedor;
                row["Monto"] = q[0].Monto;
                row["MontoLetras"] = q[0].MontoLetras;
                ds.Factura.Rows.Add(row);

                for (int ii = 0; ii < qLinea.Count(); ii++)
                {
                    DataRow rowDet = ds.FacturaLinea.NewRow();
                    rowDet["NoFactura"] = qLinea[ii].NoFactura;
                    rowDet["Articulo"] = qLinea[ii].Articulo;
                    rowDet["Cantidad"] = qLinea[ii].Cantidad;
                    rowDet["Descripcion"] = qLinea[ii].Descripcion;
                    rowDet["PrecioUnitario"] = qLinea[ii].PrecioUnitario;
                    rowDet["PrecioTotal"] = qLinea[ii].PrecioTotal;
                    ds.FacturaLinea.Rows.Add(rowDet);
                }

                using (ReportDocument reporte = new ReportDocument())
                {
                    string mFormatoFactura = "rptFacturaPV.rpt";
                    if (Convert.ToString(Session["Tienda"]).Length == 3) mFormatoFactura = ws.DevuelveFormatoFactura(Convert.ToString(Session["Tienda"]), txtFactura.Text);

                    string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mFormatoFactura);
                    reporte.Load(p);

                    string mNombreDocumento = string.Format("{0}.pdf", txtFactura.Text);
                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                    reporte.SetDataSource(ds);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al re-imprimir la factura {0} {1}", ex.Message, m);
            }
        }

        protected void lkAprobarFactura_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsArticulos"];

                if (!ws.AprobarDespachoF01(txtFactura.Text.Trim().ToUpper(), ref mMensaje, Convert.ToString(Session["Usuario"]), txtObservaciones.Text, ds, txtNombreCliente.Text, txtTelefonos.Text, txtDireccion.Text, Convert.ToDateTime(fechaEntrega.Value)))
                {
                    lbErrorDespachos.Text = mMensaje;
                    return;
                }

                //gridFacturas.Visible = false;
                gridFacturaDet.Visible = false;

                DataSet dsFacturas = new DataSet();
                dsFacturas = (DataSet)Session["dsFacturas"];

                dsFacturas.Tables["documentos"].Rows.Find(txtFactura.Text.Trim().ToUpper())["Aprobada"] = "S";
                Session["dsFacturas"] = dsFacturas;

                gridFacturas.DataSource = dsFacturas;
                gridFacturas.DataMember = "documentos";
                gridFacturas.DataBind();
                gridFacturas.SettingsPager.PageSize = 35;

                txtFactura.Text = "";
                txtPedido.Text = "";
                txtTienda.Text = "";
                txtNombreCliente.Text = "";
                txtDireccion.Text = "";
                txtTelefonos.Text = "";
                txtObservaciones.Text = "";
                txtPersonaRecibe.Text = "";
                txtVendedor.Text = "";
                txtObservacionesVendedor.Text = "";
                fechaEntrega.Value = DateTime.Now.Date;
                txtFactura.ToolTip = "Ingrese el número de factura a buscar";
                lbPedido.Text = "";

                lbInfoDespachos.Text = mMensaje;
                fechaInicial.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al aprobar la factura. {0} {1}", ex.Message, m);
            }
        }

        protected void lkDesAprobarFactura_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsArticulos"];

                if (!ws.DesAprobarDespachoF01(txtFactura.Text.Trim().ToUpper(), ref mMensaje))
                {
                    lbErrorDespachos.Text = mMensaje;
                    return;
                }

                //gridFacturas.Visible = false;
                gridFacturaDet.Visible = false;

                DataSet dsFacturas = new DataSet();
                dsFacturas = (DataSet)Session["dsFacturas"];

                dsFacturas.Tables["documentos"].Rows.Find(txtFactura.Text.Trim().ToUpper())["Aprobada"] = "N";
                Session["dsFacturas"] = dsFacturas;

                gridFacturas.DataSource = dsFacturas;
                gridFacturas.DataMember = "documentos";
                gridFacturas.DataBind();
                gridFacturas.SettingsPager.PageSize = 35;

                txtFactura.Text = "";
                txtPedido.Text = "";
                txtTienda.Text = "";
                txtNombreCliente.Text = "";
                txtDireccion.Text = "";
                txtTelefonos.Text = "";
                txtObservaciones.Text = "";
                txtPersonaRecibe.Text = "";
                txtVendedor.Text = "";
                txtObservacionesVendedor.Text = "";
                fechaEntrega.Value = DateTime.Now.Date;
                txtFactura.ToolTip = "Ingrese el número de factura a buscar";
                lbPedido.Text = "";

                lbInfoDespachos.Text = mMensaje;
                fechaInicial.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al desaprobar la factura. {0} {1}", ex.Message, m);
            }
        }

        protected void lkCliente_Click(object sender, EventArgs e)
        {
            if (txtPedido.Text.Trim().Length == 0)
            {
                lbErrorDespachos.Text = "Debe seleccionar una factura";
                return;
            }

            if (txtTienda.Text == "F01")
            {
                lbErrorDespachos.Text = "Esta opción sólo está disponible para facturas de tiendas";
                return;
            }

            string _open = "window.open('clientes.aspx', '_newtab');";
            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), _open, true);
        }

        protected void lkPedidos_Click(object sender, EventArgs e)
        {
            if (txtPedido.Text.Trim().Length == 0)
            {
                lbErrorDespachos.Text = "Debe seleccionar una factura";
                return;
            }

            if (txtTienda.Text == "F01")
            {
                lbErrorDespachos.Text = "Esta opción sólo está disponible para facturas de tiendas";
                return;
            }

            string _open = "window.open('pedidos.aspx', '_newtab');";
            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), _open, true);
        }

        protected void btnArticulo2_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var btn = (sender as ASPxButton);
                var nc = btn.NamingContainer as GridViewDataItemTemplateContainer;

                string mPedido = DataBinder.Eval(nc.DataItem, "Pedido").ToString();
                string mLinea = DataBinder.Eval(nc.DataItem, "Linea").ToString();
                string mFactura = DataBinder.Eval(nc.DataItem, "Factura").ToString();
                string mArticulo = DataBinder.Eval(nc.DataItem, "Articulo").ToString();
                string mTipo = DataBinder.Eval(nc.DataItem, "Tipo").ToString();

                string mArticuloNuevo = ""; string mDescripcionNueva = ""; string mTooltip = "";
                if (!ws.CambiarColorDespachoF01(ref mMensaje, mPedido, mFactura, mLinea, mArticulo, ref mArticuloNuevo, ref mDescripcionNueva, ref mTooltip))
                {
                    lbErrorDespachos.Text = mMensaje;
                    return;
                }

                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsArticulos"];

                ds.Tables["documento"].Rows.Find(mLinea)["Articulo"] = mArticuloNuevo;
                ds.Tables["documento"].Rows.Find(mLinea)["Articulo2"] = mArticuloNuevo;
                ds.Tables["documento"].Rows.Find(mLinea)["Descripcion"] = mDescripcionNueva;
                ds.Tables["documento"].Rows.Find(mLinea)["Tooltip"] = mTooltip;

                if (mTipo == "C")
                {
                    ds.Tables["documento"].Rows.Find(mLinea)["Articulo2"] = string.Format("&nbsp;&nbsp;&nbsp;&nbsp;{0}", mArticuloNuevo);
                    ds.Tables["documento"].Rows.Find(mLinea)["Descripcion"] = string.Format("&nbsp;&nbsp;&nbsp;&nbsp;{0}", mDescripcionNueva);
                }

                Session["dsArticulos"] = ds;

                gridFacturaDet.DataSource = ds;
                gridFacturaDet.DataMember = "documento";
                gridFacturaDet.DataBind();
                gridFacturaDet.FocusedRowIndex = -1;
               
                lbInfoDespachos.Text = mMensaje;
                fechaInicial.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al cambiar el color. {0} {1}", ex.Message, m);
            }
        }

        protected void btnArticuloBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var btn = (sender as ASPxButton);
                var nc = btn.NamingContainer as GridViewDataItemTemplateContainer;

                string mLinea = txtLinea.Text;
                string mArticulo = DataBinder.Eval(nc.DataItem, "Articulo").ToString();
                string mDescripcion = DataBinder.Eval(nc.DataItem, "Descripcion").ToString();
                string mTooltip = DataBinder.Eval(nc.DataItem, "Tooltip").ToString();

                if (!ws.CambiarArticuloDespachoF01(ref mMensaje, txtPedido.Text, txtFactura.Text, txtLinea.Text, mArticulo, mDescripcion))
                {
                    lbErrorDespachos.Text = mMensaje;
                    return;
                }

                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsArticulos"];

                ds.Tables["documento"].Rows.Find(mLinea)["Articulo"] = mArticulo;
                ds.Tables["documento"].Rows.Find(mLinea)["Articulo2"] = mArticulo;
                ds.Tables["documento"].Rows.Find(mLinea)["Descripcion"] = mDescripcion;
                ds.Tables["documento"].Rows.Find(mLinea)["Tooltip"] = mTooltip;
                string mTipo = ds.Tables["documento"].Rows.Find(mLinea)["Tipo"].ToString();

                if (mTipo == "C")
                {
                    ds.Tables["documento"].Rows.Find(mLinea)["Articulo2"] = string.Format("&nbsp;&nbsp;&nbsp;&nbsp;{0}", mArticulo);
                    ds.Tables["documento"].Rows.Find(mLinea)["Descripcion"] = string.Format("&nbsp;&nbsp;&nbsp;&nbsp;{0}", mDescripcion);
                }

                Session["dsArticulos"] = ds;

                gridFacturaDet.DataSource = ds;
                gridFacturaDet.DataMember = "documento";
                gridFacturaDet.DataBind();
                gridFacturaDet.FocusedRowIndex = -1;

                lbInfoDespachos.Text = mMensaje;
                fechaInicial.Focus();

                ASPxPopupControl2.ShowOnPageLoad = false;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al seleccionar el artículo del listado para cambiar. {0} {1}", ex.Message, m);
            }
        }

        protected void btnDescripcion_Init(object sender, EventArgs e)
        {
            var btn = sender as ASPxButton;
            var container = btn.NamingContainer as GridViewDataItemTemplateContainer;
            btn.ClientSideEvents.Click = string.Format("function(s,e){{ onDescriptionClick(s,e,{0}); }}", container.KeyValue ?? -1);
        }

        protected void gridArticulosBuscar_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.VisibleIndex >= 0) e.Cell.ToolTip = e.GetValue("Tooltip").ToString();
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            txtFactura.Text = "";
            txtPedido.Text = "";
            txtTienda.Text = "";
            txtNombreCliente.Text = "";
            txtDireccion.Text = "";
            txtTelefonos.Text = "";
            txtObservaciones.Text = "";
            txtPersonaRecibe.Text = "";
            txtVendedor.Text = "";
            txtObservacionesVendedor.Text = "";
            fechaEntrega.Value = DateTime.Now.Date;
            txtFactura.ToolTip = "Ingrese el número de factura a buscar";
            lbPedido.Text = "";
            gridFacturaDet.Visible = false;
        }

    }
}