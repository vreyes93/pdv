﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="enviarMail.aspx.cs" Inherits="PuntoDeVenta.enviarMail" MaintainScrollPositionOnPostback="true" %>
<%@ Register assembly="DevExpress.Web.v16.2, Version=16.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>&nbsp;&nbsp;Envío de correos</h3>
    <div class="content2">
    <div class="clientes">
        <ul>
            <li>
                
                <table align="center">
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:LinkButton ID="lkEnviarMailVale200" runat="server" 
                                onclick="lkEnviarMailVale200_Click" 
                                onclientclick="return confirm('Desea enviar los correos de vales?');">Enviar mail de vale por Q200.00 de descuento</asp:LinkButton>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
                
            </li>
        </ul>
    </div>
        <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server" TabIndex="520"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" TabIndex="530"></asp:Label>
                                <asp:Label ID="Label2" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
