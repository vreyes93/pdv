﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace PuntoDeVenta
{
    public partial class enviarMail : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                HtmlGenericControl submenu = new HtmlGenericControl();
                submenu = (HtmlGenericControl)Master.FindControl("submenu");
                submenu.Visible = true;

                HtmlGenericControl lkTiposVenta = new HtmlGenericControl();
                lkTiposVenta = (HtmlGenericControl)Master.FindControl("lkTiposVenta");
                lkTiposVenta.Visible = true;

                HtmlGenericControl lkFinancieras = new HtmlGenericControl();
                lkFinancieras = (HtmlGenericControl)Master.FindControl("lkFinancieras");
                lkFinancieras.Visible = true;

                HtmlGenericControl lkNivelesPrecio = new HtmlGenericControl();
                lkNivelesPrecio = (HtmlGenericControl)Master.FindControl("lkNivelesPrecio");
                lkNivelesPrecio.Visible = true;

                HtmlGenericControl lkCrearOfertas = new HtmlGenericControl();
                lkCrearOfertas = (HtmlGenericControl)Master.FindControl("lkCrearOfertas");
                lkCrearOfertas.Visible = true;

                HtmlGenericControl lkEnviarMails = new HtmlGenericControl();
                lkEnviarMails = (HtmlGenericControl)Master.FindControl("lkEnviarMails");
                lkEnviarMails.Visible = true;

                ViewState["url"] = Convert.ToString(Session["Url"]);
                ViewState["urlPrecios"] = Convert.ToString(Session["UrlPrecios"]);
            }

        }

        protected void lkEnviarMailVale200_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mMensaje = "";

                if (!ws.EnviarMailVale200(ref mMensaje))
                {
                    lbError.Text = mMensaje;
                    return;
                }

                lbInfo.Text = mMensaje;

            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al enviar los mails. {0}", ex.Message);
            }
        }

    }   
}