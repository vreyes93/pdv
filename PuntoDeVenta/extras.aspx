﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="extras.aspx.cs" Inherits="PuntoDeVenta.extras" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function onDisponibleTiendasClick(s, e, visibleIndex) {
            gridExistencias.GetRowValues(visibleIndex, 'id;Articulo;Descripcion', GetRowValuesCallback);
        }

        function GetRowValuesCallback(values) {
            if (values === undefined || values === null || values.length !== 3)
                return;
            var key = values[0];
            var Articulo = values[1];
            var Descripcion = values[2];

            txtInfo.SetText(key);
            txtArticuloTiendas.SetText(Articulo + " " + Descripcion);

            var filterCondition = "[id] = " + key;
            gridExistenciasTiendas.ApplyFilter(filterCondition);

            popupDisponibleTiendas.Show();
        }

        function DoProcessEnterKey(htmlEvent, editName) {
            if (htmlEvent.keyCode == 13) {
                ASPxClientUtils.PreventEventAndBubble(htmlEvent);
                lbInfoExistencias.SetText("Hello");
                lkExistencias.DoClick();
            }
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>&nbsp;&nbsp;Extras</h3>
    <div class="content2">
        <div class="clientes2">

            <dx:ASPxPopupControl ID="ASPxPopupControl1" runat="server" ClientInstanceName="popupDisponibleTiendas" HeaderText="Disponibilidad en tiendas" HeaderStyle-BackColor="#ff8a3f" HeaderStyle-ForeColor="White"
            Modal="true" Width="300px" AllowDragging="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
            <HeaderStyle BackColor="#FF8A3F" ForeColor="White" />
                <ContentCollection>
                    <dx:PopupControlContentControl>
                        <dx:ASPxTextBox ID="txtArticuloTiendas" runat="server" Width="370px" style="text-transform: uppercase;" AutoPostBack="false" ClientInstanceName="txtArticuloTiendas"
                            CssClass="textBoxStyle" TabIndex="10" ToolTip="Se muestran las existencias de este artículo." ReadOnly="true" >
                        </dx:ASPxTextBox>
                        <br />
                        <dx:ASPxGridView ID="gridExistenciasTiendas" EnableTheming="True" Theme="SoftOrange" ClientInstanceName="gridExistenciasTiendas"
                                runat="server" AutoGenerateColumns="False" KeyFieldName="id"
                                Border-BorderStyle="None" Width="280px" CssClass="dxGrid" 
                                TabIndex="22" onhtmldatacellprepared="gridExistenciasTiendas_HtmlDataCellPrepared"
                                Visible="true" >
                            <Styles>
                                <StatusBar><Border BorderStyle="None" /></StatusBar>
                                <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                <AlternatingRow BackColor="#fde4cf" />
                            </Styles>
                                <SettingsBehavior EnableRowHotTrack="True"></SettingsBehavior>
                                <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                CommandBatchEditUpdate="Aplicar cambios" 
                                ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                            <Columns>
                                <dx:GridViewDataColumn FieldName="id" ReadOnly="true" Caption="id" Visible="false">
                                    <HeaderStyle BackColor="#ff8a3f"  />
                                    <CellStyle Font-Size="Small" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn FieldName="Articulo" ReadOnly="true" Caption="Artículo" 
                                    Visible="false" VisibleIndex="0" >
                                    <HeaderStyle BackColor="#ff8a3f"  />
                                    <CellStyle Font-Size="Small" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn FieldName="Descripcion" ReadOnly="true" 
                                    Caption="Descripción" Visible="false" VisibleIndex="1" >
                                    <HeaderStyle BackColor="#ff8a3f"  />
                                    <CellStyle Font-Size="Small" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" 
                                    Visible="true" Width="40px" VisibleIndex="4">
                                    <HeaderStyle BackColor="#ff8a3f"  />
                                    <CellStyle Font-Size="Small" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn FieldName="Localizacion" ReadOnly="true" 
                                    Caption="Localización" Visible="true" Width="60px" VisibleIndex="5">
                                    <HeaderStyle BackColor="#ff8a3f"  />
                                    <CellStyle Font-Size="Small" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn FieldName="Disponible" ReadOnly="true" 
                                    Caption="Disponible" Visible="true" Width="60px" VisibleIndex="6">
                                    <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn FieldName="Reserva" ReadOnly="true" Caption="Reserva" 
                                    Visible="true" Width="60px" VisibleIndex="7">
                                    <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn FieldName="Existencia" ReadOnly="true" 
                                    Caption="Existencia" Visible="true" Width="60px" VisibleIndex="8" >
                                    <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn FieldName="DisponibleTiendas" ReadOnly="true" 
                                    Caption="Disponible Tiendas" Visible="false" VisibleIndex="2">
                                    <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn FieldName="Tooltip" ReadOnly="true" Caption="Tooltip" 
                                    Visible="false" VisibleIndex="3">
                                    <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn FieldName="TooltipReservas" ReadOnly="true" Caption="Tooltip" Visible="false">
                                    <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn FieldName="ArticuloDescripcion" ReadOnly="true" Caption="ArticuloDescripcion" 
                                    Visible="false" VisibleIndex="3">
                                    <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                            </Columns>
                            <Styles>
                                <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                            </Styles>
                            <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                <SettingsPager PageSize="100" Visible="False">
                            </SettingsPager>
                            <Border BorderStyle="None"></Border>
                        </dx:ASPxGridView>

                    </dx:PopupControlContentControl>
                </ContentCollection>
            </dx:ASPxPopupControl>

            <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" Theme="SoftOrange" Width="996px" ActiveTabIndex="0">
                <TabPages>
                    <dx:TabPage Text="Existencias">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl1" runat="server">
                                <table align="center">
                                    <tr>
                                        <td colspan="3">
                                            <a>Consulta de existencias</a>
                                        </td>
                                        <td colspan="3" style="text-align: right">
                                            <asp:Label ID="lbErrorExistencias" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                            <asp:Label ID="lbInfoExistencias" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            Artículo:</td>
                                        <td>
                                            <dx:ASPxTextBox ID="txtArticuloExistencia" runat="server" Width="180px" style="text-transform: uppercase;" AutoPostBack="false"
                                                CssClass="textBoxStyle" TabIndex="10" ToolTip="Ingrese parte del código o parte del nombre para buscar" >
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lkExistencias" runat="server" ClientInstanceName="lkExistencias"
                                                ToolTip="Haga clic aquí para consultar las existencias del artículo ingresado" 
                                                onclick="lkExistencias_Click" TabIndex="14">Consultar existencias</asp:LinkButton>
                                        </td>
                                        <td>
                                            &nbsp;|&nbsp;
                                            <asp:LinkButton ID="lkExistenciasOcultar" runat="server" 
                                                ToolTip="Haga clic aquí para ocultar el listado de existencias" 
                                                onclick="lkExistenciasOcultar_Click" TabIndex="18">Ocultar</asp:LinkButton>
                                        </td>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <dx:ASPxGridView ID="gridExistencias" EnableTheming="True" Theme="SoftOrange" 
                                                    runat="server" AutoGenerateColumns="False" KeyFieldName="id"
                                                    Border-BorderStyle="None" Width="960px" CssClass="dxGrid" 
                                                    TabIndex="22" ClientInstanceName="gridExistencias"
                                                    Visible="False" OnHtmlDataCellPrepared="gridExistencias_HtmlDataCellPrepared" >
                                                <Styles>
                                                    <StatusBar><Border BorderStyle="None" /></StatusBar>
                                                    <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                                    <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                                    <AlternatingRow BackColor="#fde4cf" />
                                                </Styles>
                                                    <SettingsBehavior EnableRowHotTrack="True"></SettingsBehavior>
                                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                                    CommandBatchEditUpdate="Aplicar cambios" 
                                                    ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                                    CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                                    SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                                <Columns>
                                                    <dx:GridViewDataColumn FieldName="id" ReadOnly="true" Caption="id" Visible="false">
                                                        <HeaderStyle BackColor="#ff8a3f"  />
                                                        <CellStyle Font-Size="Small" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="Articulo" ReadOnly="true" Caption="Artículo" Visible="true" Width="100px">
                                                        <HeaderStyle BackColor="#ff8a3f"  />
                                                        <CellStyle Font-Size="Small" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="Descripcion" ReadOnly="true" Caption="Descripción" Visible="true" Width="600px">
                                                        <HeaderStyle BackColor="#ff8a3f"  />
                                                        <CellStyle Font-Size="Small" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="Disponible" ReadOnly="true" Caption="Disponible F01" Visible="true" Width="60px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="Reserva" ReadOnly="true" Caption="Reserva F01" Visible="true" Width="60px">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="Existencia" ReadOnly="true" Caption="Existencia F01" Visible="true" Width="60px" >
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="DisponibleTiendas" ReadOnly="true" Caption="Disponible Tiendas" Visible="true" Width="80px">
                                                        <EditFormSettings Visible="False" />
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnDisponibleTiendas" EncodeHtml="false" runat="server" AutoPostBack="false" Text='<%# Bind("DisponibleTiendas") %>' RenderMode="Link" ToolTip="Haga clic aquí para ver el detalle de las tiendas" OnInit="btnDisponibleTiendas_Init" Font-Size="Small">
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="Tooltip" ReadOnly="true" Caption="Tooltip" Visible="false">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="TooltipReservas" ReadOnly="true" Caption="Tooltip" Visible="false">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="ArticuloDescripcion" ReadOnly="true" Caption="ArticuloDescripcion" 
                                                        Visible="false" VisibleIndex="3">
                                                        <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                </Columns>
                                                <Styles>
                                                    <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                                </Styles>
                                                <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                                    <SettingsPager PageSize="100" Visible="False">
                                                </SettingsPager>
                                                <Border BorderStyle="None"></Border>
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                        </td>
                                    </tr>
                                </table>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                </TabPages>
            </dx:ASPxPageControl>

        </div>
        <table align="center">
            <tr>
                <td style="text-align: center">
                    <dx:ASPxTextBox ID="txtInfo" runat="server" ClientInstanceName="txtInfo" CssClass="textBoxStyleHide" Width="5px" TabIndex="9999" Visible="true" ReadOnly="true" ForeColor="White">
                        <Border BorderColor="White" />
                    </dx:ASPxTextBox>

                    <asp:Label ID="lbInfo" runat="server" TabIndex="9999"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" TabIndex="9999"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

