﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Reflection;
using DevExpress.Web;
using DevExpress.XtraPrinting;
using Newtonsoft.Json;
using MF_Clases;

namespace PuntoDeVenta
{
    public partial class extras : BasePage
    {

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                Clases.RetornaExistencias mRetorna = new Clases.RetornaExistencias();
                mRetorna = (Clases.RetornaExistencias)Session["ExistenciasReservas"];

                gridExistencias.DataSource = mRetorna.Existencias.AsEnumerable();
                gridExistencias.DataMember = "Existencias";
                gridExistencias.DataBind();
                gridExistencias.SettingsPager.PageSize = 500;

                gridExistenciasTiendas.DataSource = mRetorna.ExistenciasTiendas.AsEnumerable();
                gridExistenciasTiendas.DataMember = "ExistenciasTiendas";
                gridExistenciasTiendas.DataBind();
                gridExistenciasTiendas.SettingsPager.PageSize = 500;
            }
            catch{}
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                Session["ExistenciasReservas"] = null;
            }

            Page.ClientScript.RegisterStartupScript(
                typeof(extras),
                "ScriptDoFocus",
                SCRIPT_DOFOCUS.Replace("REQUEST_LASTFOCUS", Request["__LASTFOCUS"]),
                true);

        }

        private const string SCRIPT_DOFOCUS = @"window.setTimeout('DoFocus()', 1); function DoFocus() {
            try {
                document.getElementById('REQUEST_LASTFOCUS').focus();
            } catch (ex) {}
        }";

        private void HookOnFocus(Control CurrentControl)
        {
            //checks if control is one of TextBox, DropDownList, ListBox or Button
            if ((CurrentControl is TextBox) ||
                (CurrentControl is DropDownList) ||
                (CurrentControl is ListBox) ||
                (CurrentControl is Button))
                //adds a script which saves active control on receiving focus 
                //in the hidden field __LASTFOCUS.
                (CurrentControl as WebControl).Attributes.Add(
                   "onfocus",
                   "try{document.getElementById('__LASTFOCUS').value=this.id} catch(e) {}");
            //checks if the control has children
            if (CurrentControl.HasControls())
                //if yes do them all recursively
                foreach (Control CurrentChildControl in CurrentControl.Controls)
                    HookOnFocus(CurrentChildControl);
        }
        
        protected void txtArticuloExistencia_TextChanged(object sender, EventArgs e)
        {
            CargarExistencias();
        }

        protected void lkExistencias_Click(object sender, EventArgs e)
        {
            CargarExistencias();
        }


        void CargarExistencias()
        {
            try
            {
                lbErrorExistencias.Text = "";
                lbInfoExistencias.Text = "";

                gridExistencias.Visible = false;

                WebRequest request = WebRequest.Create(string.Format("{0}/existencias/{1}", Convert.ToString(Session["UrlRestServices"]), txtArticuloExistencia.Text));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                Clases.RetornaExistencias mRetorna = new Clases.RetornaExistencias();
                mRetorna = JsonConvert.DeserializeObject<Clases.RetornaExistencias>(responseString);

                if (!mRetorna.RetornaExito[0].exito)
                {
                    lbErrorExistencias.Text = mRetorna.RetornaExito[0].mensaje;
                    return;
                }

                gridExistencias.Visible = true;
                gridExistencias.DataSource = mRetorna.Existencias.AsEnumerable();
                gridExistencias.DataMember = "Existencias";
                gridExistencias.DataBind();
                gridExistencias.FocusedRowIndex = -1;
                gridExistencias.SettingsPager.PageSize = 500;

                gridExistenciasTiendas.DataSource = mRetorna.ExistenciasTiendas.AsEnumerable();
                gridExistenciasTiendas.DataMember = "ExistenciasTiendas";
                gridExistenciasTiendas.DataBind();
                gridExistenciasTiendas.FocusedRowIndex = -1;
                gridExistenciasTiendas.SettingsPager.PageSize = 500;

                Session["ExistenciasReservas"] = mRetorna;
            }
            catch (Exception ex)
            {
                lbErrorExistencias.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        //void CargarExistencias()
        //{
        //    try
        //    {
        //        if (mRetorna.Existencias.Count == 0 || mRetorna.ExistenciasTiendas.Count == 0)
        //        {
        //            ValidarSesion();

        //            lbErrorExistencias.Text = "";
        //            lbInfoExistencias.Text = "";
        //            string mm = Convert.ToString(Session["UrlRestServices"]);
        //            string kk = txtArticuloExistencia.Text;
        //            string jj = string.Format("{0}/existencias/{1}", Convert.ToString(Session["UrlRestServices"]), txtArticuloExistencia.Text);
        //            WebRequest request = WebRequest.Create(string.Format("{0}/existencias/{1}", Convert.ToString(Session["UrlRestServices"]), txtArticuloExistencia.Text));

        //            request.Method = "GET";
        //            request.ContentType = "application/json";

        //            var response = (HttpWebResponse)request.GetResponse();
        //            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

        //            mRetorna = JsonConvert.DeserializeObject<Clases.RetornaExistencias>(responseString);

        //            if (!mRetorna.RetornaExito[0].exito)
        //            {
        //                lbErrorExistencias.Text = mRetorna.RetornaExito[0].mensaje;
        //                return;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        lbErrorExistencias.Text = CatchClass.ExMessage(ex, this.Page.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        //protected void gridExistencias_DataBinding(object sender, EventArgs e)
        //{
        //    CargarExistencias();
        //    gridExistencias.Visible = true;
        //    gridExistencias.DataSource = mRetorna.Existencias;
        //    gridExistencias.DataMember = "Existencias";
        //    gridExistencias.FocusedRowIndex = -1;
        //    gridExistencias.SettingsPager.PageSize = 500;
        //}

        //protected void gridExistenciasTiendas_DataBinding(object sender, EventArgs e)
        //{
        //    CargarExistencias();
        //    gridExistenciasTiendas.DataSource = mRetorna.ExistenciasTiendas;
        //    gridExistenciasTiendas.DataMember = "ExistenciasTiendas";
        //    gridExistenciasTiendas.FocusedRowIndex = -1;
        //    gridExistenciasTiendas.SettingsPager.PageSize = 500;
        //}

        protected void lkExistenciasOcultar_Click(object sender, EventArgs e)
        {
            gridExistencias.Visible = false;
        }

        protected void btnDisponibleTiendas_Init(object sender, EventArgs e)
        {
            var btn = sender as ASPxButton;
            var container = btn.NamingContainer as GridViewDataItemTemplateContainer;

            if (container != null) btn.ClientSideEvents.Click = string.Format(
                "function(s,e){{ onDisponibleTiendasClick(s,e,{0}); }}",
                container.VisibleIndex);
        }

        protected void gridExistenciasTiendas_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.VisibleIndex >= 0) e.Cell.ToolTip = e.GetValue("TooltipReservas").ToString();
            if (Convert.ToInt32(e.GetValue("Reserva")) > 0)
            {
                e.Cell.Font.Bold = true;
                e.Cell.ForeColor = System.Drawing.Color.Red;
            } 
        }

        protected void gridExistencias_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.VisibleIndex >= 0)
            {
                if (e.DataColumn.FieldName == "Reserva") e.Cell.ToolTip = e.GetValue("TooltipReservas").ToString();
                if (e.DataColumn.FieldName == "Articulo" || e.DataColumn.FieldName == "Descripcion" || e.DataColumn.FieldName == "Existencia") e.Cell.ToolTip = e.GetValue("Tooltip").ToString();
            }
        }
   
    }
}