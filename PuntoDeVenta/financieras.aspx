﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="financieras.aspx.cs" Inherits="PuntoDeVenta.financieras" MaintainScrollPositionOnPostback="true" %>
<%@ Register assembly="DevExpress.Web.v16.2, Version=16.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>&nbsp;&nbsp;Financieras</h3>
    <div class="content2">
    <div class="clientes">
        <ul>
            <li>
                <table align="center">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            Financiera:</td>
                        <td>
                            <asp:TextBox ID="txtNombre" runat="server" MaxLength="200" Width="352px"></asp:TextBox>
                            <asp:TextBox ID="txtFinanciera" runat="server" MaxLength="400" Width="20px" 
                                TabIndex="90" Visible="False"></asp:TextBox>
                        </td>
                        <td>
                            Prefijo:</td>
                        <td>
                            <asp:TextBox ID="txtPrefijo" runat="server" MaxLength="12" TabIndex="10" 
                                ToolTip="El prefijo se utilizará para generará los niveles de precio de la financiera, por ejemplo un prefijo puede ser InterCons para que al tener un nivel de precio de 12 meses se genrará el nombre InterCons12M"></asp:TextBox>
                        </td>
                        <td>
                            Estatus:</td>
                        <td>
                            <asp:DropDownList ID="cbEstatus" runat="server" TabIndex="20">
                                <asp:ListItem Value="Activa">Activa</asp:ListItem>
                                <asp:ListItem Value="Inactiva">Inactiva</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                </table>
            </li>
            <li>
                <asp:GridView ID="gridFinancieras" runat="server" BackColor="#DEBA84" 
                    BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
                    CellSpacing="2" AutoGenerateColumns="False" 
                    onselectedindexchanged="gridFinancieras_SelectedIndexChanged" 
                    onrowdatabound="gridFinancieras_RowDataBound" TabIndex="80">
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                                    CommandName="Select" Text="Seleccionar" TabIndex="85"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Nombre" HeaderText="Financiera" />
                        <asp:BoundField DataField="prefijo" HeaderText="Prefijo" />
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" />
                        <asp:TemplateField HeaderText="Financiera" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbFinanciera" runat="server" Text='<%# Bind("Financiera") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Financiera") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#F8AA55" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
            </li>
            <li>
                <table align="center">
                    <tr>
                        <td style="text-align: center">
                            &nbsp;
                            <asp:LinkButton ID="lkGrabar" runat="server" 
                                ToolTip="Haga clic aquí para grabar los datos del tipo de venta" TabIndex="30" 
                                onclick="lkGrabar_Click">Grabar</asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="lkLimpiar" runat="server" 
                                ToolTip="Haga clic aquí para limpiar los datos de la página e ingresar un tipo de venta nuevo" 
                                TabIndex="40" onclick="lkLimpiar_Click">Limpiar</asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </li>
        </ul>
    </div>
        <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server" TabIndex="520"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" TabIndex="530"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
