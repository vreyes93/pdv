﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace PuntoDeVenta
{
    public partial class financieras : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                HtmlGenericControl submenu = new HtmlGenericControl();
                submenu = (HtmlGenericControl)Master.FindControl("submenu");
                submenu.Visible = true;

                HtmlGenericControl lkTiposVenta = new HtmlGenericControl();
                lkTiposVenta = (HtmlGenericControl)Master.FindControl("lkTiposVenta");
                lkTiposVenta.Visible = true;

                HtmlGenericControl lkFinancieras = new HtmlGenericControl();
                lkFinancieras = (HtmlGenericControl)Master.FindControl("lkFinancieras");
                lkFinancieras.Visible = true;

                HtmlGenericControl lkNivelesPrecio = new HtmlGenericControl();
                lkNivelesPrecio = (HtmlGenericControl)Master.FindControl("lkNivelesPrecio");
                lkNivelesPrecio.Visible = true;

                HtmlGenericControl lkCrearOfertas = new HtmlGenericControl();
                lkCrearOfertas = (HtmlGenericControl)Master.FindControl("lkCrearOfertas");
                lkCrearOfertas.Visible = true;

                HtmlGenericControl lkEnviarMails = new HtmlGenericControl();
                lkEnviarMails = (HtmlGenericControl)Master.FindControl("lkEnviarMails");
                lkEnviarMails.Visible = true;

                ViewState["url"] = Convert.ToString(Session["Url"]);
                ViewState["urlPrecios"] = Convert.ToString(Session["UrlPrecios"]);

                limpiar();
            }
        }

        void limpiar()
        {
            lbInfo.Text = "";
            lbError.Text = "";
            txtFinanciera.Text = "";
            txtNombre.Text = "";
            txtPrefijo.Text = "";
            cbEstatus.SelectedValue = "Activa";

            cargarFinancieras();
        }

        void cargarFinancieras()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveFinancieras();

            gridFinancieras.DataSource = q;
            gridFinancieras.DataBind();
            gridFinancieras.Visible = true;
            gridFinancieras.SelectedIndex = -1;
        }

        bool pasaValidaciones()
        {
            if (txtNombre.Text.Trim().Length == 0)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('Debe ingresar el nombre de la Financiera.');"), true);
                txtNombre.Focus();
                return false;
            }
            if (txtPrefijo.Text.Trim().Length == 0)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('Debe ingresar el prefijo a utilizar en los niveles de precio.');"), true);
                txtPrefijo.Focus();
                return false;
            }

            return true;
        }

        protected void lkGrabar_Click(object sender, EventArgs e)
        {
            if (!pasaValidaciones()) return;

            string mensaje = "";
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            lbError.Text = "";
            lbInfo.Text = "";

            if (!ws.GrabarFinanciera(txtFinanciera.Text, txtNombre.Text, cbEstatus.SelectedValue, txtPrefijo.Text, ref mensaje, Convert.ToString(Session["usuario"])))
            {
                lbError.Text = mensaje;
                txtNombre.Focus();
                return;
            }

            //Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('{0}');", mensaje), true);
            lbInfo.Text = "";
            limpiar();
        }

        protected void lkLimpiar_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        protected void gridFinancieras_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = gridFinancieras.SelectedRow;
            Label lbFinanciera = (Label)gvr.FindControl("lbFinanciera");

            string mNombre = gvr.Cells[1].Text;
            mNombre = mNombre.Replace("&#39;", "'").Replace("&#225;", "á").Replace("&#233;", "é").Replace("&#237;", "í").Replace("&#243;", "ó").Replace("&#243;", "ú").Replace("&quot;", "\"").Replace("&#209;", "Ñ").Replace("&#241;", "Ñ");

            txtFinanciera.Text = lbFinanciera.Text;
            txtNombre.Text = mNombre;
            txtPrefijo.Text = gvr.Cells[2].Text;
            cbEstatus.SelectedValue = gvr.Cells[3].Text;
        }

        protected void gridFinancieras_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }
    }
}