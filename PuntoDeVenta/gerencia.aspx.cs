﻿using System;
using System.IO;
using System.Linq;
using System.Data;
using DevExpress.Web;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;

namespace PuntoDeVenta
{
    public partial class gerencia : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                ViewState["url"] = Convert.ToString(Session["Url"]);
                ViewState["urlPrecios"] = Convert.ToString(Session["UrlPrecios"]);

                CargarTasas();
            }
        }

        void CargarTasas()
        {
            try
            {
                lbInfoTasa.Text = "";
                lbErrorTasa.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var qTasas = ws.DevuelveTasaCambio().ToList();

                gridTasaCambio.DataSource = qTasas;
                gridTasaCambio.DataBind();

                txtTasaCambio.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorTasa.Text = string.Format("Error al cargar las tasas {0} {1}", ex.Message, m);
            }
        }

        void GrabarTasaCambio()
        {
            try
            {
                lbInfoTasa.Text = "";
                lbErrorTasa.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mMensaje = "";

                if (!ws.GrabarTasaCambio(txtTasaCambio.Text, Convert.ToString(Session["Usuario"]), ref mMensaje))
                {
                    lbErrorTasa.Text = mMensaje;
                    txtTasaCambio.Focus();
                    return;
                }

                CargarTasas();

                txtTasaCambio.Text = "";
                txtTasaCambio.Focus();
                lbInfoTasa.Text = mMensaje;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorTasa.Text = string.Format("Error al grabar la tasa {0} {1}", ex.Message, m);
            }
        }

        protected void lkGrabarTasaCambio_Click(object sender, EventArgs e)
        {
            GrabarTasaCambio();
        }

        protected void txtTasaCambio_TextChanged(object sender, EventArgs e)
        {
            GrabarTasaCambio();
        }

        protected void lkEnviarCorreoTasa_Click(object sender, EventArgs e)
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            string mMensaje = "";
            ws.EnviarCorreoTasa(ref mMensaje);
        }
        
        protected void ToolbarExport_ItemClick(object source, ExportItemClickEventArgs e)
        {
            try
            {
                lbInfoPrecios.Text = "";
                lbErrorPrecios.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                ws.Timeout = 999999999;
                string mMensaje = ""; string mArchivo = ""; DataSet ds = new DataSet();
                string mFecha = string.Format("{0}{1}{2}{3}{4}", DateTime.Now.Year.ToString(), DateTime.Now.Month < 10 ? "0" : "", DateTime.Now.Month.ToString(), DateTime.Now.Day < 10 ? "0" : "", DateTime.Now.Day.ToString());
                string mHoraMinuto = string.Format("{0}{1}{2}{3}", DateTime.Now.Hour < 10 ? "0" : "", DateTime.Now.Hour.ToString(), DateTime.Now.Minute < 10 ? "0" : "", DateTime.Now.Minute.ToString());

                try
                {
                    Directory.CreateDirectory(@"C:\reportes\Precios\");
                }
                catch
                {
                    //Nothing
                }

                switch (e.ExportType)
                {
                    case DemoExportFormat.Art:
                        #region "Articulos"
                        ds = ws.ReporteArticulos(ref mMensaje);

                        if (mMensaje.Trim().Length > 0)
                        {
                            lbErrorPrecios.Text = mMensaje;
                            return;
                        }

                        gridArticulos1.DataSource = ds;
                        gridArticulos1.DataMember = "articulos1";
                        gridArticulos1.DataBind();
                        gridArticulos1.FocusedRowIndex = -1;
                        gridArticulos1.SettingsPager.PageSize = 30;

                        gridArticulos2.DataSource = ds;
                        gridArticulos2.DataMember = "articulos2";
                        gridArticulos2.DataBind();
                        gridArticulos2.FocusedRowIndex = -1;
                        gridArticulos2.SettingsPager.PageSize = 30;

                        gridArticulos3.DataSource = ds;
                        gridArticulos3.DataMember = "articulos3";
                        gridArticulos3.DataBind();
                        gridArticulos3.FocusedRowIndex = -1;
                        gridArticulos3.SettingsPager.PageSize = 30;

                        gridFormasPago.DataSource = ds;
                        gridFormasPago.DataMember = "formasPago";
                        gridFormasPago.DataBind();
                        gridFormasPago.FocusedRowIndex = -1;
                        gridFormasPago.SettingsPager.PageSize = 30;

                        gridArticulos1.Columns["Artículo"].Width = 450;
                        gridArticulos1.Columns["Descripcion"].Width = 450;

                        gridArticulos2.Columns["Artículo"].Width = 450;
                        gridArticulos2.Columns["Descripcion"].Width = 450;

                        gridArticulos3.Columns["Descripcion"].Width = 450;

                        PrintingSystemBase ps = new PrintingSystemBase();
                        ps.XlSheetCreated += ps_XlSheetCreated;

                        PrintableComponentLinkBase link1 = new PrintableComponentLinkBase(ps);
                        link1.PaperName = "Articulos";
                        link1.Component = gridExport1;

                        PrintableComponentLinkBase link2 = new PrintableComponentLinkBase(ps);
                        link2.PaperName = "ArticulosCorto";
                        link2.Component = gridExport2;

                        PrintableComponentLinkBase link3 = new PrintableComponentLinkBase(ps);
                        link3.PaperName = "Ofertas";
                        link3.Component = gridExport3;

                        PrintableComponentLinkBase link4 = new PrintableComponentLinkBase(ps);
                        link4.PaperName = "FormasPago";
                        link4.Component = gridExport4;

                        CompositeLinkBase compositeLink = new CompositeLinkBase(ps);
                        compositeLink.Links.AddRange(new object[] { link1, link2, link3, link4 });

                        compositeLink.CreatePageForEachLink();

                        using (MemoryStream stream = new MemoryStream())
                        {
                            XlsxExportOptions options = new XlsxExportOptions();
                            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                            compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                            Response.Clear();
                            Response.Buffer = false;
                            Response.AppendHeader("Content-Type", "application/xlsx");
                            Response.AppendHeader("Content-Transfer-Encoding", "binary");
                            Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=Articulos{0}.xlsx", mFecha));
                            Response.BinaryWrite(stream.ToArray());
                            Response.End();
                        }
                        ps.Dispose();
#endregion
                        break;
                    case DemoExportFormat.Pre:
                        #region "Precios"
                        string mFile = "";
                        try
                        {
                            mFile = uploadFile.UploadedFiles[0].FileName;
                        }
                        catch
                        {
                            mFile = "";
                        }

                        if (mFile.Trim().Length == 0)
                        {
                            lbErrorPrecios.Text = "No se ha cargado ningún archivo";
                            return;
                        }

                        mArchivo = @"C:\reportes\Precios\PreciosOfertas_" + mFecha + "_" + mHoraMinuto + ".xlsx";
                        uploadFile.UploadedFiles[0].SaveAs(mArchivo);
                        
                        System.Data.OleDb.OleDbConnection mConexion = new System.Data.OleDb.OleDbConnection(string.Format("Provider=Microsoft.ACE.OLEDB.12.0; data source={0}; Extended Properties=Excel 8.0;", mArchivo));
                        System.Data.OleDb.OleDbDataAdapter da = new System.Data.OleDb.OleDbDataAdapter("SELECT Articulo, Descripcion, [Precio Base] AS PrecioBase FROM [PreciosBase$]", mConexion);

                        try
                        {
                            da.Fill(ds);
                        }
                        catch
                        {
                            lbErrorPrecios.Text = "El archivo no contiene una hoja llamada PreciosBase o bien las columnas no son las correctas, por favor descargue la plantilla";
                            return;
                        }

                        if (ds.Tables[0].Rows.Count == 0)
                        {
                            lbErrorPrecios.Text = "No se encontraron precios base";
                            return;
                        }

                        if (!ws.GrabarPreciosBase(ds, ref mMensaje, Convert.ToString(Session["usuario"]), mFecha, mHoraMinuto))
                        {
                            lbErrorPrecios.Text = mMensaje;
                            return;
                        }

                        lbInfoPrecios.Text = mMensaje;
#endregion
                        break;
                    case DemoExportFormat.Ofe:
                        #region "Ofertas"
                        string mFileOfertas = "";
                        try
                        {
                            mFileOfertas = uploadFile.UploadedFiles[0].FileName;
                        }
                        catch
                        {
                            mFileOfertas = "";
                        }

                        if (mFileOfertas.Trim().Length == 0)
                        {
                            lbErrorPrecios.Text = "No se ha cargado ningún archivo";
                            return;
                        }

                        mArchivo = @"C:\reportes\Precios\PreciosOfertas_" + mFecha + "_" + mHoraMinuto + ".xlsx";
                        uploadFile.UploadedFiles[0].SaveAs(mArchivo);
                        
                        System.Data.OleDb.OleDbConnection mConexionOfertas = new System.Data.OleDb.OleDbConnection(string.Format("Provider=Microsoft.ACE.OLEDB.12.0; data source={0}; Extended Properties=Excel 8.0;", mArchivo));
                        System.Data.OleDb.OleDbDataAdapter daOfertas = new System.Data.OleDb.OleDbDataAdapter("SELECT Articulo, Descripcion, [Precio Base Oferta] AS PrecioBaseOferta, [Fecha Inicial] AS FechaInicial, [Fecha Final] AS FechaFinal FROM [Ofertas$]", mConexionOfertas);

                        try
                        {
                            daOfertas.Fill(ds);
                        }
                        catch
                        {
                            lbErrorPrecios.Text = "El archivo no contiene una hoja llamada Ofertas o bien las columnas no son las correctas, por favor descargue la plantilla";
                            return;
                        }

                        if (ds.Tables[0].Rows.Count == 0)
                        {
                            lbErrorPrecios.Text = "No se encontraron ofertas";
                            return;
                        }
                        log.Info(string.Format("/n/r-------***INICIANDO EL PROCESO DE CARGA DE OFERTAS ARCHIVO: {0} ****", mFileOfertas));

                        if (!ws.GrabarOfertas(ds, ref mMensaje, Convert.ToString(Session["usuario"]), mFecha, mHoraMinuto))
                        {
                            lbErrorPrecios.Text = mMensaje;
                            return;
                        }
                        log.Info(string.Format("/n/r-------*** FIN PROCESO DE CARGA DE OFERTAS **** {0}", mFileOfertas));
                        lbInfoPrecios.Text = mMensaje;
#endregion
                        break;
                    case DemoExportFormat.OfeVig:
                        #region "Ofertas Vigentes"
                        ds = ws.OfertasVigentes(ref mMensaje);

                        if (mMensaje.Trim().Length > 0)
                        {
                            lbErrorPrecios.Text = mMensaje;
                            return;
                        }

                        if (ds.Tables["articulos1"].Rows.Count == 0)
                        {
                            lbErrorPrecios.Text = "No hay ofertas vigentes";
                            return;
                        }

                        gridArticulos1.DataSource = ds;
                        gridArticulos1.DataMember = "articulos1";
                        gridArticulos1.DataBind();
                        gridArticulos1.FocusedRowIndex = -1;
                        gridArticulos1.SettingsPager.PageSize = 30;

                        PrintingSystemBase psVigentes = new PrintingSystemBase();
                        psVigentes.XlSheetCreated += psOfertas_XlSheetCreated;

                        PrintableComponentLinkBase link1Vigentes = new PrintableComponentLinkBase(psVigentes);
                        link1Vigentes.PaperName = "Ofertas";
                        link1Vigentes.Component = gridExport1;

                        CompositeLinkBase compositeLinkVigentes = new CompositeLinkBase(psVigentes);
                        compositeLinkVigentes.Links.AddRange(new object[] { link1Vigentes });

                        compositeLinkVigentes.CreatePageForEachLink();

                        using (MemoryStream stream = new MemoryStream())
                        {
                            XlsxExportOptions options = new XlsxExportOptions();
                            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                            compositeLinkVigentes.PrintingSystemBase.ExportToXlsx(stream, options);
                            Response.Clear();
                            Response.Buffer = false;
                            Response.AppendHeader("Content-Type", "application/xlsx");
                            Response.AppendHeader("Content-Transfer-Encoding", "binary");
                            Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=OfertasVigentes_{0}.xlsx", mFecha));
                            Response.BinaryWrite(stream.ToArray());
                            Response.End();
                        }
                        psVigentes.Dispose();
#endregion 
                        break;
                    case DemoExportFormat.OfePend:
                        #region "Ofertas Pendientes"
                        ds = ws.OfertasPendientes(ref mMensaje);

                        if (mMensaje.Trim().Length > 0)
                        {
                            lbErrorPrecios.Text = mMensaje;
                            return;
                        }

                        if (ds.Tables["articulos1"].Rows.Count == 0)
                        {
                            lbErrorPrecios.Text = "No hay ofertas pendientes";
                            return;
                        }

                        gridArticulos1.DataSource = ds;
                        gridArticulos1.DataMember = "articulos1";
                        gridArticulos1.DataBind();
                        gridArticulos1.FocusedRowIndex = -1;
                        gridArticulos1.SettingsPager.PageSize = 30;

                        PrintingSystemBase psPendientes = new PrintingSystemBase();
                        psPendientes.XlSheetCreated += psOfertas_XlSheetCreated;

                        PrintableComponentLinkBase link1Pendientes = new PrintableComponentLinkBase(psPendientes);
                        link1Pendientes.PaperName = "Ofertas";
                        link1Pendientes.Component = gridExport1;

                        CompositeLinkBase compositeLinkPendientes = new CompositeLinkBase(psPendientes);
                        compositeLinkPendientes.Links.AddRange(new object[] { link1Pendientes });

                        compositeLinkPendientes.CreatePageForEachLink();

                        using (MemoryStream stream = new MemoryStream())
                        {
                            XlsxExportOptions options = new XlsxExportOptions();
                            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                            compositeLinkPendientes.PrintingSystemBase.ExportToXlsx(stream, options);
                            Response.Clear();
                            Response.Buffer = false;
                            Response.AppendHeader("Content-Type", "application/xlsx");
                            Response.AppendHeader("Content-Transfer-Encoding", "binary");
                            Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=OfertasPendientes_{0}.xlsx", mFecha));
                            Response.BinaryWrite(stream.ToArray());
                            Response.End();
                        }
                        psPendientes.Dispose();
#endregion
                        break;
                    case DemoExportFormat.ArtMay:
                        #region "Art May"
                        ds = ws.ReporteArticulosMayoreo(ref mMensaje);

                        if (mMensaje.Trim().Length > 0)
                        {
                            lbErrorPrecios.Text = mMensaje;
                            return;
                        }

                        gridArticulos1.DataSource = ds;
                        gridArticulos1.DataMember = "articulos1";
                        gridArticulos1.DataBind();
                        gridArticulos1.FocusedRowIndex = -1;
                        gridArticulos1.SettingsPager.PageSize = 30;

                        PrintingSystemBase psMayoreo = new PrintingSystemBase();
                        psMayoreo.XlSheetCreated += ps_XlSheetCreated;

                        PrintableComponentLinkBase link1Mayoreo = new PrintableComponentLinkBase(psMayoreo);
                        link1Mayoreo.PaperName = "Articulos";
                        link1Mayoreo.Component = gridExport1;

                        CompositeLinkBase compositeLinkMayoreo = new CompositeLinkBase(psMayoreo);
                        compositeLinkMayoreo.Links.AddRange(new object[] { link1Mayoreo });

                        compositeLinkMayoreo.CreatePageForEachLink();

                        using (MemoryStream stream = new MemoryStream())
                        {
                            XlsxExportOptions options = new XlsxExportOptions();
                            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                            compositeLinkMayoreo.PrintingSystemBase.ExportToXlsx(stream, options);
                            Response.Clear();
                            Response.Buffer = false;
                            Response.AppendHeader("Content-Type", "application/xlsx");
                            Response.AppendHeader("Content-Transfer-Encoding", "binary");
                            Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=ArticulosMayoreo{0}.xlsx", mFecha));
                            Response.BinaryWrite(stream.ToArray());
                            Response.End();
                        }
                        psMayoreo.Dispose();
#endregion
                        break;
                    case DemoExportFormat.May:
                        #region "May"
                        string mFileMayoreo = "";
                        try
                        {
                            mFileMayoreo = uploadFile.UploadedFiles[0].FileName;
                        }
                        catch
                        {
                            mFileMayoreo = "";
                        }

                        if (mFileMayoreo.Trim().Length == 0)
                        {
                            lbErrorPrecios.Text = "No se ha cargado ningún archivo";
                            return;
                        }

                        mArchivo = @"C:\reportes\Precios\PreciosMayoreo_" + mFecha + "_" + mHoraMinuto + ".xlsx";
                        uploadFile.UploadedFiles[0].SaveAs(mArchivo);

                        System.Data.OleDb.OleDbConnection mConexionMayoreo = new System.Data.OleDb.OleDbConnection(string.Format("Provider=Microsoft.ACE.OLEDB.12.0; data source={0}; Extended Properties=Excel 8.0;", mArchivo));
                        System.Data.OleDb.OleDbDataAdapter daMayoreo = new System.Data.OleDb.OleDbDataAdapter("SELECT CLIENTE, NOMBRE, ARTICULO, DESCRIPCION, PRECIO_BASE_LOCAL, PORCENTAJE_DESCUENTO, TIENE_REGALO, REGALO, CANTIDAD_REGALO, ACTIVO, SKU FROM [Articulos$]", mConexionMayoreo);

                        try
                        {
                            daMayoreo.Fill(ds);
                        }
                        catch
                        {
                            lbErrorPrecios.Text = "El archivo no contiene una hoja llamada Articulos o bien las columnas no son las correctas, por favor descargue el archivo de mayoreo";
                            return;
                        }

                        if (ds.Tables[0].Rows.Count == 0)
                        {
                            lbErrorPrecios.Text = "No se encontraron articulos de mayoreo";
                            return;
                        }

                        if (!ws.GrabarPreciosMayoreo(ds, ref mMensaje, Convert.ToString(Session["usuario"]), mFecha, mHoraMinuto))
                        {
                            lbErrorPrecios.Text = mMensaje;
                            return;
                        }

                        lbInfoPrecios.Text = mMensaje;
#endregion
                        break;
                    case DemoExportFormat.Mues:
                        #region "Mues"
                        ds = ws.PlantillaPrecios(ref mMensaje);

                        if (mMensaje.Trim().Length > 0)
                        {
                            lbErrorPrecios.Text = mMensaje;
                            return;
                        }

                        gridArticulos1.DataSource = ds;
                        gridArticulos1.DataMember = "precios";
                        gridArticulos1.DataBind();
                        gridArticulos1.FocusedRowIndex = -1;
                        gridArticulos1.SettingsPager.PageSize = 30;

                        PrintingSystemBase psPlantilla = new PrintingSystemBase();
                        psPlantilla.XlSheetCreated += psPlantilla_XlSheetCreated;

                        PrintableComponentLinkBase link1Plantilla = new PrintableComponentLinkBase(psPlantilla);
                        link1Plantilla.PaperName = "PreciosBase";
                        link1Plantilla.Component = gridExport1;

                        CompositeLinkBase compositeLinkPlantilla = new CompositeLinkBase(psPlantilla);
                        compositeLinkPlantilla.Links.AddRange(new object[] { link1Plantilla });

                        compositeLinkPlantilla.CreatePageForEachLink();

                        using (MemoryStream stream = new MemoryStream())
                        {
                            XlsxExportOptions options = new XlsxExportOptions();
                            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                            compositeLinkPlantilla.PrintingSystemBase.ExportToXlsx(stream, options);
                            Response.Clear();
                            Response.Buffer = false;
                            Response.AppendHeader("Content-Type", "application/xlsx");
                            Response.AppendHeader("Content-Transfer-Encoding", "binary");
                            Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=PreciosBase_{0}.xlsx", mFecha));
                            Response.BinaryWrite(stream.ToArray());
                            Response.End();
                        }
                        psPlantilla.Dispose();
#endregion
                        break;
                    case DemoExportFormat.PlantOfe:
                        #region "Plantilla Ofertas"
                        ds = ws.PlantillaOfertas(ref mMensaje);

                        if (mMensaje.Trim().Length > 0)
                        {
                            lbErrorPrecios.Text = mMensaje;
                            return;
                        }

                        gridArticulos1.DataSource = ds;
                        gridArticulos1.DataMember = "ofertas";
                        gridArticulos1.DataBind();
                        gridArticulos1.FocusedRowIndex = -1;
                        gridArticulos1.SettingsPager.PageSize = 30;

                        PrintingSystemBase psPlantillaOfertas = new PrintingSystemBase();
                        psPlantillaOfertas.XlSheetCreated += psOfertas_XlSheetCreated;

                        PrintableComponentLinkBase link1PlantillaOfertas = new PrintableComponentLinkBase(psPlantillaOfertas);
                        link1PlantillaOfertas.PaperName = "Ofertas";
                        link1PlantillaOfertas.Component = gridExport1;

                        CompositeLinkBase compositeLinkPlantillaOfertas = new CompositeLinkBase(psPlantillaOfertas);
                        compositeLinkPlantillaOfertas.Links.AddRange(new object[] { link1PlantillaOfertas });

                        compositeLinkPlantillaOfertas.CreatePageForEachLink();

                        using (MemoryStream stream = new MemoryStream())
                        {
                            XlsxExportOptions options = new XlsxExportOptions();
                            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                            compositeLinkPlantillaOfertas.PrintingSystemBase.ExportToXlsx(stream, options);
                            Response.Clear();
                            Response.Buffer = false;
                            Response.AppendHeader("Content-Type", "application/xlsx");
                            Response.AppendHeader("Content-Transfer-Encoding", "binary");
                            Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=Ofertas_{0}.xlsx", mFecha));
                            Response.BinaryWrite(stream.ToArray());
                            Response.End();
                        }
                        psPlantillaOfertas.Dispose();
#endregion
                        break;
                    case DemoExportFormat.SinPre:
                        #region "Sin Precios"
                        ds = ws.SinPrecioBase(ref mMensaje);

                        if (mMensaje.Trim().Length > 0)
                        {
                            lbErrorPrecios.Text = mMensaje;
                            return;
                        }

                        gridArticulos1.DataSource = ds;
                        gridArticulos1.DataMember = "precios";
                        gridArticulos1.DataBind();
                        gridArticulos1.FocusedRowIndex = -1;
                        gridArticulos1.SettingsPager.PageSize = 30;

                        PrintingSystemBase psSinPrecio = new PrintingSystemBase();
                        psSinPrecio.XlSheetCreated += psPlantilla_XlSheetCreated;

                        PrintableComponentLinkBase link1SinPrecio = new PrintableComponentLinkBase(psSinPrecio);
                        link1SinPrecio.PaperName = "PreciosBase";
                        link1SinPrecio.Component = gridExport1;

                        CompositeLinkBase compositeLinkSinPrecio = new CompositeLinkBase(psSinPrecio);
                        compositeLinkSinPrecio.Links.AddRange(new object[] { link1SinPrecio });

                        compositeLinkSinPrecio.CreatePageForEachLink();

                        using (MemoryStream stream = new MemoryStream())
                        {
                            XlsxExportOptions options = new XlsxExportOptions();
                            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                            compositeLinkSinPrecio.PrintingSystemBase.ExportToXlsx(stream, options);
                            Response.Clear();
                            Response.Buffer = false;
                            Response.AppendHeader("Content-Type", "application/xlsx");
                            Response.AppendHeader("Content-Transfer-Encoding", "binary");
                            Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=Articulos_SinPrecioBase_{0}.xlsx", mFecha));
                            Response.BinaryWrite(stream.ToArray());
                            Response.End();
                        }
                        psSinPrecio.Dispose();
#endregion
                        break;
                    case DemoExportFormat.UndoOfe:
                        #region "Cancelar Ofertas"
                        string mFileUndoOfertas = "";
                        try
                        {
                            mFileUndoOfertas = uploadFile.UploadedFiles[0].FileName;
                            
                        }
                        catch
                        {
                            mFileUndoOfertas = "";
                        }

                        if (mFileUndoOfertas.Trim().Length == 0)
                        {
                            lbErrorPrecios.Text = "No se ha cargado ningún archivo";
                            return;
                        }

                        

                        mArchivo = @"C:\reportes\Precios\PreciosOfertas_" + mFecha + "_" + mHoraMinuto + ".xlsx";
                        uploadFile.UploadedFiles[0].SaveAs(mArchivo);

                        System.Data.OleDb.OleDbConnection mConexionUndoOfertas = new System.Data.OleDb.OleDbConnection(string.Format("Provider=Microsoft.ACE.OLEDB.12.0; data source={0}; Extended Properties=Excel 8.0;", mArchivo));
                        System.Data.OleDb.OleDbDataAdapter daUndoOfertas = new System.Data.OleDb.OleDbDataAdapter("SELECT Articulo, Descripcion, [Precio Base Oferta] AS PrecioBaseOferta, [Fecha Inicial] AS FechaInicial, [Fecha Final] AS FechaFinal FROM [Ofertas$]", mConexionUndoOfertas);

                        try
                        {
                            daUndoOfertas.Fill(ds);
                        }
                        catch
                        {
                            lbErrorPrecios.Text = "El archivo no contiene una hoja llamada Ofertas o bien las columnas no son las correctas, por favor descargue la plantilla";
                            return;
                        }

                        if (ds.Tables[0].Rows.Count == 0)
                        {
                            lbErrorPrecios.Text = "No se encontraron ofertas";
                            return;
                        }
                        
                        log.Info("Iniciando proceso de cancelación de ofertas, archivo " + mFileUndoOfertas +" con "+ds.Tables[0].Rows.Count+" artículos");
                        if (!ws.CancelarOfertas(ds, ref mMensaje, Convert.ToString(Session["usuario"]), mFecha, mHoraMinuto))
                        {
                            lbErrorPrecios.Text = mMensaje;
                            return;
                        }
                        log.Info("Finalizando el proceso de cancelación de ofertas");
                        lbInfoPrecios.Text = mMensaje;

                        break;
                    #endregion 
                    case DemoExportFormat.AntMay:
                        #region "AntMay"
                        //Antigüedad de saldos de Mayoreo

                        ds = ws.AntiguedadSaldosMayoreo(ref mMensaje, DateTime.Now.Date, false);

                        if (mMensaje.Trim().Length > 0)
                        {
                            lbErrorPrecios.Text = mMensaje;
                            return;
                        }

                        gridArticulos1.DataSource = ds;
                        gridArticulos1.DataMember = "AntiguedadMayoreo";
                        gridArticulos1.DataBind();
                        gridArticulos1.FocusedRowIndex = -1;
                        gridArticulos1.SettingsPager.PageSize = 30;

                        gridArticulos2.DataSource = ds;
                        gridArticulos2.DataMember = "AntiguedadMayoreoCobros";
                        gridArticulos2.DataBind();
                        gridArticulos2.FocusedRowIndex = -1;
                        gridArticulos2.SettingsPager.PageSize = 30;

                        gridArticulos3.DataSource = ds;
                        gridArticulos3.DataMember = "AntiguedadMayoreoFacturas";
                        gridArticulos3.DataBind();
                        gridArticulos3.FocusedRowIndex = -1;
                        gridArticulos3.SettingsPager.PageSize = 30;

                        PrintingSystemBase psAntSaldosMayoreo = new PrintingSystemBase();
                        psAntSaldosMayoreo.XlSheetCreated += psAntSaldosMayoreo_XlSheetCreated;

                        PrintableComponentLinkBase link1AntSaldosMayoreo = new PrintableComponentLinkBase(psAntSaldosMayoreo);
                        link1AntSaldosMayoreo.PaperName = "AntiguedadMayoreo";
                        link1AntSaldosMayoreo.Component = gridExport1;

                        PrintableComponentLinkBase link1AntSaldosMayoreo2 = new PrintableComponentLinkBase(psAntSaldosMayoreo);
                        link1AntSaldosMayoreo2.PaperName = "Cobros";
                        link1AntSaldosMayoreo2.Component = gridExport2;

                        PrintableComponentLinkBase link1AntSaldosMayoreo3 = new PrintableComponentLinkBase(psAntSaldosMayoreo);
                        link1AntSaldosMayoreo3.PaperName = "Facturas";
                        link1AntSaldosMayoreo3.Component = gridExport3;

                        CompositeLinkBase compositeLinkAntSaldosMayoreo = new CompositeLinkBase(psAntSaldosMayoreo);
                        compositeLinkAntSaldosMayoreo.Links.AddRange(new object[] { link1AntSaldosMayoreo, link1AntSaldosMayoreo2, link1AntSaldosMayoreo3 });

                        compositeLinkAntSaldosMayoreo.CreatePageForEachLink();

                        using (MemoryStream stream = new MemoryStream())
                        {
                            XlsxExportOptions options = new XlsxExportOptions();
                            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                            compositeLinkAntSaldosMayoreo.PrintingSystemBase.ExportToXlsx(stream, options);
                            Response.Clear();
                            Response.Buffer = false;
                            Response.AppendHeader("Content-Type", "application/xlsx");
                            Response.AppendHeader("Content-Transfer-Encoding", "binary");
                            Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=AntiguedadMayoreo_{0}.xlsx", mFecha));
                            Response.BinaryWrite(stream.ToArray());
                            Response.End();
                        }
                        psAntSaldosMayoreo.Dispose();
                        #endregion
                        break;
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorPrecios.Text = string.Format("Error. {0} {1}", ex.Message, m);
            }

        }

        void ps_XlSheetCreated(object sender, XlSheetCreatedEventArgs e)
        {
            if (e.Index == 0) e.SheetName = "Articulos";
            if (e.Index == 1) e.SheetName = "ArticulosCorto";
            if (e.Index == 2) e.SheetName = "Ofertas";
            if (e.Index == 3) e.SheetName = "FormasPago";
        }

        void psPlantilla_XlSheetCreated(object sender, XlSheetCreatedEventArgs e)
        {
            if (e.Index == 0) e.SheetName = "PreciosBase";
            if (e.Index == 1) e.SheetName = "Ofertas";
        }

        void psOfertas_XlSheetCreated(object sender, XlSheetCreatedEventArgs e)
        {
            if (e.Index == 0) e.SheetName = "Ofertas";
        }

        void psAntSaldosMayoreo_XlSheetCreated(object sender, XlSheetCreatedEventArgs e)
        {
            if (e.Index == 0) e.SheetName = "AntiguedadMayoreo";
            if (e.Index == 1) e.SheetName = "Cobros";
            if (e.Index == 2) e.SheetName = "Facturas";
        }

        protected void gridExport_RenderBrick(object sender, ASPxGridViewExportRenderingEventArgs e)
        {
            if (e.RowType != GridViewRowType.Header) e.BrickStyle.BorderWidth = 0;
        }



    }
}
