﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="inventario.aspx.cs" Inherits="PuntoDeVenta.inventario" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3 runat="server" id="TituloInventario">&nbsp;&nbsp;Inventario</h3>
    <div class="content2">
    <div class="clientes">
    <ul>
        <li>
            <asp:GridView ID="gridInventario" runat="server" AutoGenerateColumns="False" 
                BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                CellPadding="3" CellSpacing="2" 
                onrowdatabound="gridInventario_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="Articulo" HeaderText="Artículo" >
                    <HeaderStyle Width="90px" />
                    <ItemStyle Width="90px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Descripcion" HeaderText="Descripción" >
                    <HeaderStyle Width="640px" />
                    <ItemStyle Font-Size="Small" Width="640px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Localizacion" HeaderText="Localización" >
                    <HeaderStyle Font-Size="Small" />
                    <ItemStyle Font-Size="Small" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Ultima Compra">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtUltimaCompra" runat="server" Text='<%# Bind("UltimaCompra") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbUltimaCompra" runat="server" Text='<%# Bind("UltimaCompra", "{0:d}") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" Font-Size="Small" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="Blanco1" HeaderText="No Escribir" >
                    <HeaderStyle Font-Size="Small" />
                    <ItemStyle Font-Size="Small" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Existencias" HeaderText="Existencia Total">
                    <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                    <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Blanco2" HeaderText="Existencia Física">
                    <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                    <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                </Columns>
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#FFF1D4" />
                <SortedAscendingHeaderStyle BackColor="#B95C30" />
                <SortedDescendingCellStyle BackColor="#F1E5CE" />
                <SortedDescendingHeaderStyle BackColor="#93451F" />
            </asp:GridView>
        </li>
    </ul>
    </div> 
        <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server" TabIndex="520"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" TabIndex="530"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
