﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace PuntoDeVenta
{
    public partial class inventario : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            
            HtmlGenericControl submenu = new HtmlGenericControl();
            submenu = (HtmlGenericControl)Master.FindControl("submenu");
            submenu.Visible = true;

            HtmlGenericControl lkTiposVenta = new HtmlGenericControl();
            lkTiposVenta = (HtmlGenericControl)Master.FindControl("lkTiposVenta");
            lkTiposVenta.Visible = true;

            HtmlGenericControl lkFinancieras = new HtmlGenericControl();
            lkFinancieras = (HtmlGenericControl)Master.FindControl("lkFinancieras");
            lkFinancieras.Visible = true;

            HtmlGenericControl lkNivelesPrecio = new HtmlGenericControl();
            lkNivelesPrecio = (HtmlGenericControl)Master.FindControl("lkNivelesPrecio");
            lkNivelesPrecio.Visible = true;

            HtmlGenericControl lkCrearOfertas = new HtmlGenericControl();
            lkCrearOfertas = (HtmlGenericControl)Master.FindControl("lkCrearOfertas");
            lkCrearOfertas.Visible = true;

            HtmlGenericControl lkEnviarMails = new HtmlGenericControl();
            lkEnviarMails = (HtmlGenericControl)Master.FindControl("lkEnviarMails");
            lkEnviarMails.Visible = true;

            ViewState["url"] = Convert.ToString(Session["Url"]);
            CargarInventario();
        }

        void CargarInventario()
        {
            bool mMostrarDetalle = false;
            string mBodega = Convert.ToString(Session["Tienda"]);
            if (mBodega.Length > 4) mBodega = "F01";

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveArticulosInventario(DateTime.Now.Date, mBodega, "2015", "2", Convert.ToString(Session["Usuario"]), ref mMostrarDetalle);
            TituloInventario.InnerText = string.Format(" Inventario - {0} (Se listan {1} artículos)", mBodega, q.Count().ToString());

            gridInventario.DataSource = q;
            gridInventario.DataBind();
        }

        protected void gridInventario_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

    }
}