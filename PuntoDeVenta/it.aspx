﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="it.aspx.cs" Inherits="PuntoDeVenta.it"  MaintainScrollPositionOnPostback="true" %>
<%@ Register assembly="DevExpress.Web.v16.2, Version=16.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<%@ Register Src="ToolbarExport.ascx" TagName="ToolbarExport" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>&nbsp;&nbsp;Opciones de IT</h3>
    <div class="content2">
        <div class="clientes2">

            <ul>
                <li>
                    <table>
                        <tr>
                            <td colspan="4">
                                <a>Usuarios con privilegio a realizar despachos de tiendas</a>
                            </td>
                            <td colspan="4" style="text-align: right">
                                <asp:Label ID="lbErrorUsuariosBodega" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                <asp:Label ID="lbInfoUsuariosBodega" runat="server"></asp:Label>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>
                                Usuario:
                            </td>
                            <td>
                                <dx:ASPxComboBox ID="cbUsuario" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle" 
                                    ValueType="System.String" Width="250px" DropDownStyle="DropDownList" 
                                    DropDownWidth="200" DropDownRows="25" TabIndex="10">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                Bodega:
                            </td>
                            <td>
                                <dx:ASPxComboBox ID="cbBodega" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle" 
                                    ValueType="System.String" Width="75px" DropDownStyle="DropDownList" 
                                    DropDownWidth="40" DropDownRows="25" TabIndex="15">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnAgregarUsuario" runat="server" Width="8px" Height="8px"
                                    ToolTip="Agregar usuario" onclick="btnAgregarUsuario_Click" TabIndex="20" >
                                    <Image IconID="actions_add_16x16" />
                                </dx:ASPxButton>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnVerUsuarios" runat="server" Width="8px" Height="8px"
                                    ToolTip="Ver usuarios" onclick="btnVerUsuarios_Click" TabIndex="25" >
                                    <Image IconID="people_usergroup_16x16" />
                                </dx:ASPxButton>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridUsuariosBodegas" EnableTheming="True" Theme="SoftOrange" 
                                    runat="server" KeyFieldName="ID" AutoGenerateColumns="False" 
                                    Border-BorderStyle="None" Width="450px" CssClass="dxGrid" TabIndex="30" Visible="False" OnRowCommand="gridUsuariosBodegas_RowCommand" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                        <SettingsBehavior EnableRowHotTrack="True"></SettingsBehavior>
                                        <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                        CommandBatchEditUpdate="Aplicar cambios" 
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="Eliminar" ReadOnly="true" Caption="Eliminar" Visible="true" Width="80px">
	                                        <EditFormSettings Visible="False" />
	                                        <DataItemTemplate>
		                                        <dx:ASPxButton ID="btnEliminarExpediente" EncodeHtml="false" runat="server" AutoPostBack="false" Text="Eliminar" 
                                                ToolTip="Haga clic aquí para eliminar la fila." Font-Size="XX-Small"
                                                    ClientInstanceName="btnEliminar">
                                                    <ClientSideEvents Click="function(s, e) {
                                                        e.processOnServer = confirm('¿Desea eliminar la fila seleccionada?');
                                                            }" />
		                                        </dx:ASPxButton>
	                                        </DataItemTemplate>
	                                        <HeaderStyle BackColor="#ff8a3f"  />
	                                        <CellStyle Font-Size="XX-Small" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="ID" ReadOnly="true" Caption="ID" Visible="false" Width="10px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Usuario" ReadOnly="true" Caption="Usuario" Visible="false" Width="100px">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Nombre" ReadOnly="true" Caption="Usuario" Visible="true" Width="200px" VisibleIndex="1">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Bodega" ReadOnly="true" Caption="Bodega" Visible="true" Width="100px" VisibleIndex="2">
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="100" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>
                            </td>
                            <td></td>
                        </tr>
                    </table>
                </li>
            </ul>


        </div>
        <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server" TabIndex="620"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" TabIndex="630"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
