﻿using System;
using System.Data;
using System.Text;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using DevExpress.Web;
using DevExpress.XtraPrinting;
using Newtonsoft.Json;
using MF_Clases;

namespace PuntoDeVenta
{
    public partial class it : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                HookOnFocus(this.Page as Control);
                CargarUsuariosBodegas(false);
            }

            Page.ClientScript.RegisterStartupScript(
            typeof(it),
            "ScriptDoFocus",
            SCRIPT_DOFOCUS.Replace("REQUEST_LASTFOCUS", Request["__LASTFOCUS"]),
            true);
        }

        private const string SCRIPT_DOFOCUS = @"window.setTimeout('DoFocus()', 1); function DoFocus() {
            try {
                document.getElementById('REQUEST_LASTFOCUS').focus();
            } catch (ex) {}
        }";

        private void HookOnFocus(Control CurrentControl)
        {
            //checks if control is one of TextBox, DropDownList, ListBox or Button
            if ((CurrentControl is TextBox) ||
                (CurrentControl is DropDownList) ||
                (CurrentControl is ListBox) ||
                (CurrentControl is Button))
                //adds a script which saves active control on receiving focus 
                //in the hidden field __LASTFOCUS.
                (CurrentControl as WebControl).Attributes.Add(
                   "onfocus",
                   "try{document.getElementById('__LASTFOCUS').value=this.id} catch(e) {}");
            //checks if the control has children
            if (CurrentControl.HasControls())
                //if yes do them all recursively
                foreach (Control CurrentChildControl in CurrentControl.Controls)
                    HookOnFocus(CurrentChildControl);
        }

        void CargarUsuariosBodegas(bool mostrar)
        {
            try
            {
                lbInfoUsuariosBodega.Text = "";
                lbErrorUsuariosBodega.Text = "";

                WebRequest request = WebRequest.Create(string.Format("{0}/UsuariosBodega/", Convert.ToString(Session["UrlRestServices"])));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                Clases.RetornaUsuarioBodega mRetorna = new Clases.RetornaUsuarioBodega();
                mRetorna = JsonConvert.DeserializeObject<Clases.RetornaUsuarioBodega>(responseString);

                if (!mostrar)
                {
                    cbUsuario.DataSource = mRetorna.Usuarios;
                    cbUsuario.ValueField = "Usuario";
                    cbUsuario.ValueType = typeof(System.String);
                    cbUsuario.TextField = "Nombre";
                    cbUsuario.DataBindItems();

                    cbBodega.DataSource = mRetorna.Bodegas;
                    cbBodega.ValueField = "Bodega";
                    cbBodega.ValueType = typeof(System.String);
                    cbBodega.TextField = "Bodega";
                    cbBodega.DataBindItems();

                    cbUsuario.SelectedIndex = 0;
                    cbBodega.SelectedIndex = 0;
                }

                if (!mRetorna.Exito[0].exito)
                {
                    lbErrorUsuariosBodega.Text = mRetorna.Exito[0].mensaje;
                    return;
                }

                if (mostrar)
                {
                    gridUsuariosBodegas.Visible = true;
                    gridUsuariosBodegas.DataSource = mRetorna.UsuarioBodega.AsEnumerable();
                    gridUsuariosBodegas.DataMember = "UsuarioBodega";
                    gridUsuariosBodegas.DataBind();
                    gridUsuariosBodegas.FocusedRowIndex = -1;
                    gridUsuariosBodegas.SettingsPager.PageSize = 500;
                }
            }
            catch (Exception ex)
            {
                lbErrorUsuariosBodega.Text = CatchClass.ExMessage(ex, "it", "CargarUsuariosBodegas");
            }
        }

        protected void btnAgregarUsuario_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoUsuariosBodega.Text = "";
                lbErrorUsuariosBodega.Text = "";

                string mID = string.Format("{0}-{1}-{2}", cbUsuario.Value.ToString(), cbBodega.Value.ToString(), Convert.ToString(Session["Usuario"]));

                string json = JsonConvert.SerializeObject(mID);
                byte[] data = Encoding.ASCII.GetBytes(json);

                WebRequest request = WebRequest.Create(string.Format("{0}/UsuariosBodega/", Convert.ToString(Session["UrlRestServices"])));

                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string mRespuesta = responseString.ToString();

                if (mRespuesta.ToLower().Contains("error"))
                {
                    lbErrorUsuariosBodega.Text = mRespuesta.Replace("\"", ""); ;
                    return;
                }

                CargarUsuariosBodegas(true);
                lbInfoUsuariosBodega.Text = mRespuesta.Replace("\"", "");
            }
            catch (Exception ex)
            {
                lbErrorUsuariosBodega.Text = CatchClass.ExMessage(ex, "it", "btnAgregarUsuario_Click");
            }
        }

        protected void btnVerUsuarios_Click(object sender, EventArgs e)
        {
            CargarUsuariosBodegas(true);
        }

        protected void gridUsuariosBodegas_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
        {
            try
            {
                lbInfoUsuariosBodega.Text = "";
                lbErrorUsuariosBodega.Text = "";

                string mID = e.KeyValue.ToString();

                WebRequest request = WebRequest.Create(string.Format("{0}/UsuariosBodega/{1}", Convert.ToString(Session["UrlRestServices"]), mID));

                request.Method = "DELETE";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string mRespuesta = responseString.ToString();

                if (mRespuesta.ToLower().Contains("error"))
                {
                    lbErrorUsuariosBodega.Text = mRespuesta.Replace("\"", ""); ;
                    return;
                }

                CargarUsuariosBodegas(true);
                lbInfoUsuariosBodega.Text = mRespuesta.Replace("\"", ""); ;
            }
            catch (Exception ex)
            {
                lbErrorUsuariosBodega.Text = CatchClass.ExMessage(ex, "it", "gridUsuariosBodegas_RowCommand");
            }
        }

    }
}