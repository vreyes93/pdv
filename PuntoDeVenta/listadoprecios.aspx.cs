﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using System.Reflection;

namespace PuntoDeVenta
{
    public partial class listadoprecios  : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             base.LogActivity();
            if (!IsPostBack)
            {
                ValidarSesion();
                Tools.IconHandler(Master, Session["Tienda"].ToString());

                Label lbUsuario = (Label)this.Master.FindControl("lblUsuario");
                if (Convert.ToString(Session["NombreVendedor"]) == "Alerta") Session["Vendedor"] = null;
                lbUsuario.Text = string.Format("{0}  -  {1}", Convert.ToString(Session["NombreVendedor"]), Convert.ToString(Session["Tienda"]));


                ViewState["url"] = Convert.ToString(Session["Url"]);
                ViewState["urlPrecios"] = Convert.ToString(Session["UrlPrecios"]);

                codigoArticuloDet.Text = "";
                descripcionArticuloDet.Text = "";
                cbCriterio.ToolTip = string.Format("Marque esta casilla si desea listar artículos aplicando un criterio de búsqueda ya sea por código o por nombre.{0}{0}Este criterio también se aplicará si busca artículos por nivel de precio (opciones a la derecha de este link)", System.Environment.NewLine);
                
                cargarTiposFinancieras();
                codigoArticuloDet.Focus();
            }
        }
        
        void cargarTiposFinancieras()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var qTiposVenta = ws.DevuelveTiposVentaSeleccion(Convert.ToString(Session["Tienda"])).ToList();
            var qFinancieras = ws.DevuelveFinancierasSeleccion("NR").ToList();

            //wsPuntoVenta.TipoVenta itemTipoVenta = new wsPuntoVenta.TipoVenta();
            //itemTipoVenta.CodigoTipoVenta = "0";
            //itemTipoVenta.Descripcion = "Seleccione promoción ...";
            //itemTipoVenta.Estatus = "Activa";
            //itemTipoVenta.Observaciones = "";
            //qTiposVenta.Add(itemTipoVenta);

            //wsPuntoVenta.Financieras itemFinanciera = new wsPuntoVenta.Financieras();
            //itemFinanciera.Financiera = 0;
            //itemFinanciera.Nombre = "Seleccione financiera ...";
            //itemFinanciera.prefijo = "";
            //itemFinanciera.Estatus = "Activa";
            //qFinancieras.Add(itemFinanciera);

            cbTipo.DataSource = qTiposVenta;
            cbTipo.DataTextField = "Descripcion";
            cbTipo.DataValueField = "CodigoTipoVenta";
            cbTipo.DataBind();

            cbFinanciera.DataSource = qFinancieras;
            cbFinanciera.DataTextField = "Nombre";
            cbFinanciera.DataValueField = "Financiera";
            cbFinanciera.DataBind();

            cbTipo.SelectedValue = "NR";
            cbFinanciera.SelectedValue = "7";
        }

        protected void cbCriterio_CheckedChanged(object sender, EventArgs e)
        {
            if (cbCriterio.Checked)
            {
                tablaBusquedaDet.Visible = true;
                codigoArticuloDet.Focus();
            }
            else
            {
                tablaBusquedaDet.Visible = false;
            }
        }
        
        bool validarCriterio(ref string tipo, ref bool criterio)
        {
            if (codigoArticuloDet.Text.Trim().Length > 0 || descripcionArticuloDet.Text.Trim().Length > 0) criterio = true;

            if (criterio)
            {
                if (codigoArticuloDet.Text.Trim().Length == 0 && descripcionArticuloDet.Text.Trim().Length == 0)
                {
                    lbError.Text = "Debe ingresar un criterio de búsqueda ya sea el código, la descripción, o ambos.";
                    codigoArticuloDet.Focus();
                    return false;
                }

                //if (codigoArticuloDet.Text.Trim().Length > 0) tipo = "C";
                //if (descripcionArticuloDet.Text.Trim().Length > 0) tipo = "D";
                //if (codigoArticuloDet.Text.Trim().Length > 0 && descripcionArticuloDet.Text.Trim().Length > 0) tipo = "A";

                string mCaracter = codigoArticuloDet.Text.Trim().Substring(0, 1);

                try
                {
                    Int32 mNumero = Convert.ToInt32(mCaracter);
                    tipo = "C";
                }
                catch
                {
                    tipo = "D";
                    if (mCaracter == "M" || mCaracter == "A") tipo = "C";
                }

                return true;
            }
            else
            {
                return true;
            }
        }

        protected void lkPreciosLista_Click(object sender, EventArgs e)
        {
            base.LogActivity(MethodBase.GetCurrentMethod().Name);
            buscarPreciosLista(true);
        }

        void ValidarSesion()
        {
            if (Session["Usuario"] == null) Response.Redirect("login.aspx");
        }
        
        void buscarPreciosLista(bool lista)
        {
            try
            {
                lbError.Text = "";

                ValidarSesion();
                string mTipo = ""; bool mCriterio = false;
                if (!validarCriterio(ref mTipo, ref mCriterio)) return;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                ws.Timeout = 999999999;
                var q = ws.DevuelveArticulosPreciosListado(lista, cbTipo.SelectedValue, Convert.ToInt32(cbFinanciera.SelectedValue), "", mCriterio, mTipo, codigoArticuloDet.Text.Trim(), codigoArticuloDet.Text.Trim(), Convert.ToString(Session["Tienda"]));

                gridArticulos.Visible = false;
                gridArticulosLista.Visible = false;
                lkImprimirLista.Visible = false;

                if (lista)
                {
                    gridArticulosLista.Visible = true;
                    lkImprimirLista.Visible = true;

                    gridArticulosLista.DataSource = q;
                    gridArticulosLista.DataBind();

                    dsPuntoVenta ds = new dsPuntoVenta();

                    for (int ii = 0; ii < q.Count(); ii++)
                    {
                        if (q[ii].Precio > 0)
                        {
                            DataRow row = ds.ListaPrecios.NewRow();
                            row["Articulo"] = q[ii].Articulo;
                            row["Descripcion"] = q[ii].Nombre;
                            row["Precio"] = q[ii].Precio;
                            ds.ListaPrecios.Rows.Add(row);
                        }
                    }

                    ViewState["dsListaPrecios"] = ds;
                }
                else
                {
                    gridArticulos.Visible = true;
                    gridArticulos.DataSource = q;
                    gridArticulos.DataBind();

                    setToolTips();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Se produjo el siguiente error al buscar los precios. {0} {1}", ex.Message, m);
            }
        }

        protected void lkListar_Click(object sender, EventArgs e)
        {
            ValidarSesion();
            if (cbTipo.SelectedValue == "0")
            {
                lbError.Text = "Debe seleccionar un tipo de venta.";
                cbTipo.Focus();
                return;
            }
            if (cbFinanciera.SelectedValue == "0")
            {
                lbError.Text = "Debe seleccionar una financiera.";
                cbFinanciera.Focus();
                return;
            }
            if (cbNivelPrecio.SelectedValue == "Seleccione ...")
            {
                lbError.Text = "Debe seleccionar un nivel de precio.";
                cbNivelPrecio.Focus();
                return;
            }

            string mTipo = ""; bool mCriterio = false;
            if (!validarCriterio(ref mTipo, ref mCriterio)) return;

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            ws.Timeout = 999999999;
            var q = ws.DevuelveArticulosPrecios(false, cbTipo.SelectedValue, Convert.ToInt32(cbFinanciera.SelectedValue), cbNivelPrecio.SelectedValue, mCriterio, mTipo, codigoArticuloDet.Text.Trim(), descripcionArticuloDet.Text.Trim(), Convert.ToString(Session["Tienda"]));

            gridArticulos.DataSource = q;
            gridArticulos.DataBind();

            setToolTips();
        }

        protected void cbFinanciera_SelectedIndexChanged(object sender, EventArgs e)
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var qNivelesPrecio = ws.DevuelveNivelesPrecioSeleccionFinanciera(cbFinanciera.SelectedValue, Convert.ToString(Session["Tienda"])).ToList();

            wsPuntoVenta.NivelesPrecio itemNivel = new wsPuntoVenta.NivelesPrecio();
            itemNivel.NivelPrecio = "Seleccione ...";
            itemNivel.Factor = 0;
            itemNivel.TipoVenta = "";
            itemNivel.TipoDeVenta = "";
            itemNivel.Financiera = 0;
            itemNivel.NombreFinanciera = "";
            itemNivel.Estatus = "";
            itemNivel.Tipo = "";
            itemNivel.Pagos = "";
            qNivelesPrecio.Add(itemNivel);
              
            cbNivelPrecio.DataSource = qNivelesPrecio;
            cbNivelPrecio.DataTextField = "NivelPrecio";
            cbNivelPrecio.DataValueField = "NivelPrecio";
            cbNivelPrecio.DataBind();

            cbNivelPrecio.SelectedValue = "Seleccione ...";
        }

        protected void gridArticulos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }

            try
            {
                Label lbOferta = (Label)e.Row.FindControl("lbOferta");
                Label lbPromocion = (Label)e.Row.FindControl("lbPromocion");
                
                if (lbOferta.Text == "S")
                {
                    e.Row.ForeColor = System.Drawing.Color.FromArgb(227, 89, 4);
                    e.Row.Font.Bold = true;
                }

                if (lbPromocion.Text == "S")
                {
                    e.Row.ForeColor = System.Drawing.Color.FromArgb(165, 81, 41);
                    e.Row.Font.Bold = true;
                }
            }
            catch
            {
                //Nothing
            }
        }
        
        void setToolTips()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
            {
                Label lbOferta = (Label)gridArticulos.Rows[ii].FindControl("lbOferta");
                Label lbPrecio = (Label)gridArticulos.Rows[ii].FindControl("lbPrecio");
                Label lbPrecioOriginal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioOriginal");
                Label lbFechaVence = (Label)gridArticulos.Rows[ii].FindControl("lbFechaVence");
                Label lbDescuentoContado = (Label)gridArticulos.Rows[ii].FindControl("lbDescuentoContado");
                Label lbDescuentoCredito = (Label)gridArticulos.Rows[ii].FindControl("lbDescuentoCredito");
                Label lbCondicional = (Label)gridArticulos.Rows[ii].FindControl("lbCondicional");
                Label lbPrecioOferta = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioOferta");
                Label lbCondiciones = (Label)gridArticulos.Rows[ii].FindControl("lbCondiciones");
                Label lbPromocion = (Label)gridArticulos.Rows[ii].FindControl("lbPromocion");
                Label lbTipoPromocion = (Label)gridArticulos.Rows[ii].FindControl("lbTipoPromocion");
                Label lbValorPromocion = (Label)gridArticulos.Rows[ii].FindControl("lbValorPromocion");
                Label lbVencePromocion = (Label)gridArticulos.Rows[ii].FindControl("lbVencePromocion");
                Label lbFechaVencePromocion = (Label)gridArticulos.Rows[ii].FindControl("lbFechaVencePromocion");
                Label lbDescripcionPromocion = (Label)gridArticulos.Rows[ii].FindControl("lbDescripcionPromocion");
                Label lbObservacionesPromocion = (Label)gridArticulos.Rows[ii].FindControl("lbObservacionesPromocion");
                Label lbPagos = (Label)gridArticulos.Rows[ii].FindControl("lbPagos");

                if (lbOferta.Text == "S")
                {
                    if (lbCondicional.Text == "S")
                    {
                        string mCondiciones = ws.DevuelveCondiciones(gridArticulos.Rows[ii].Cells[1].Text, Convert.ToString(Session["Tienda"]));
                        lbPrecio.ToolTip = string.Format("Este artículo está en OFERTA!!!{0}{3}{1} y vence el: {2}{0}La oferta aplica en la compra de:{0}{4}", Environment.NewLine, "", Convert.ToDateTime(lbFechaVence.Text).ToShortDateString(), Convert.ToDecimal(lbPrecioOriginal.Text) > 0 ? string.Format("Precio de Oferta: Q {0}", String.Format("{0:0,0.00}", Convert.ToDecimal(lbPrecioOferta.Text))) : string.Format("{0}% desc. al contado y {1}% desc. al crédito.", lbDescuentoContado.Text.Replace(".0000", ""), lbDescuentoCredito.Text.Replace(".0000", "")), mCondiciones);
                    }
                    else
                    {
                        lbPrecio.ToolTip = string.Format("Este artículo está en OFERTA!!!{0}{3}{1}{0}La oferta vence el: {2}", Environment.NewLine, "", Convert.ToDateTime(lbFechaVence.Text).ToShortDateString(), Convert.ToDecimal(lbPrecioOriginal.Text) > 0 ? string.Format("Precio Original: Q {0}", String.Format("{0:0,0.00}", Convert.ToDecimal(lbPrecioOriginal.Text))) : string.Format("{0}% desc. al contado y {1}% desc. al crédito.", lbDescuentoContado.Text.Replace(".0000", ""), lbDescuentoCredito.Text.Replace(".0000", "")));
                    }
                }

                if (lbPromocion.Text == "S")
                    lbPrecio.ToolTip = string.Format("Este artículo está en PROMOCION!!!{0}{1}{0}{2}{0}{3}", Environment.NewLine, lbDescripcionPromocion.Text, lbObservacionesPromocion.Text, lbVencePromocion.Text == "N" ? "" : string.Format("La promoción vence el: {0}", Convert.ToDateTime(lbFechaVencePromocion.Text).ToShortDateString()));

                int mPagos = 0;
                string mPagosString = lbPagos.Text.Replace(" PAGOS", "");

                try
                {
                    mPagos = Convert.ToInt32(mPagosString);
                }
                catch
                {
                    // Nada
                }

                if (mPagos > 0)
                {
                    decimal mPrecio = Convert.ToDecimal(lbPrecio.Text);
                    gridArticulos.Rows[ii].Cells[4].Text = String.Format("{0:0,0.00}", mPrecio / mPagos);
                    gridArticulos.Rows[ii].Cells[5].ToolTip = string.Format("Cuotas de Q {0}", String.Format("{0:0,0.00}", mPrecio / mPagos));
                }
            }
        }

        protected void codigoArticuloDet_TextChanged(object sender, EventArgs e)
        {
            if (chComparar.Checked)
            {
                CompararPrecios();
            }
            else
            {
                buscarPreciosLista(false);
            }
        }

        protected void descripcionArticuloDet_TextChanged(object sender, EventArgs e)
        {
            if (chComparar.Checked)
            {
                CompararPrecios();
            }
            else
            {
                buscarPreciosLista(false);
            }
        }

        void CompararPrecios()
        {
            ValidarSesion();

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            ws.Timeout = 999999999;
            var q = ws.DevuelveArticulosPreciosComparar(codigoArticuloDet.Text.Trim(), descripcionArticuloDet.Text.Trim(), Convert.ToString(Session["Tienda"]));

            gridArticulos.DataSource = q;
            gridArticulos.DataBind();

            setToolTips();
        }

        protected void lkComparar_Click(object sender, EventArgs e)
        {
            CompararPrecios();
        }

        protected void lkLimpiarSeleccionados_Click(object sender, EventArgs e)
        {
            int jj = 0; string mCodigoValidar = "";
            wsPuntoVenta.ArticulosPrecios[] q = new wsPuntoVenta.ArticulosPrecios[1];

            foreach (GridViewRow row in gridArticulos.Rows)
            {
                CheckBox cb = (CheckBox)row.FindControl("cbArticulo");

                if (cb.Checked) mCodigoValidar = row.Cells[1].Text;

                if (row.Cells[1].Text == mCodigoValidar)
                {
                    jj++;
                    string mArticulo = row.Cells[1].Text;
                    string mDescripcion = row.Cells[2].Text;
                    string mNivel = row.Cells[4].Text;

                    Label lbOferta = (Label)row.FindControl("lbOferta");
                    Label lbPrecio = (Label)row.FindControl("lbPrecio");
                    Label lbPrecioOriginal = (Label)row.FindControl("lbPrecioOriginal");
                    Label lbFechaVence = (Label)row.FindControl("lbFechaVence");
                    Label lbDescuentoContado = (Label)row.FindControl("lbDescuentoContado");
                    Label lbDescuentoCredito = (Label)row.FindControl("lbDescuentoCredito");
                    Label lbCondicional = (Label)row.FindControl("lbCondicional");
                    Label lbPrecioOferta = (Label)row.FindControl("lbPrecioOferta");
                    Label lbCondiciones = (Label)row.FindControl("lbCondiciones");
                    Label lbPromocion = (Label)row.FindControl("lbPromocion");
                    Label lbTipoPromocion = (Label)row.FindControl("lbTipoPromocion");
                    Label lbValorPromocion = (Label)row.FindControl("lbValorPromocion");
                    Label lbVencePromocion = (Label)row.FindControl("lbVencePromocion");
                    Label lbFechaVencePromocion = (Label)row.FindControl("lbFechaVencePromocion");
                    Label lbDescripcionPromocion = (Label)row.FindControl("lbDescripcionPromocion");
                    Label lbObservacionesPromocion = (Label)row.FindControl("lbObservacionesPromocion");

                    wsPuntoVenta.ArticulosPrecios item = new wsPuntoVenta.ArticulosPrecios();
                    item.Articulo = mArticulo;
                    item.Nombre = mDescripcion;
                    item.Precio = Convert.ToDecimal(lbPrecio.Text.Replace(",", ""));
                    item.NivelPrecio = mNivel;
                    item.Oferta = lbOferta.Text;
                    item.PrecioOriginal = Convert.ToDecimal(lbPrecioOriginal.Text.Replace(",", ""));
                    item.FechaVence = Convert.ToDateTime(lbFechaVence.Text);
                    item.DescuentoContado = Convert.ToDecimal(lbDescuentoContado.Text.Replace(",", ""));
                    item.DescuentoCredito = Convert.ToDecimal(lbDescuentoCredito.Text.Replace(",", ""));
                    item.Condicional = lbCondicional.Text;
                    item.PrecioOferta = Convert.ToDecimal(lbPrecioOferta.Text.Replace(",", ""));
                    item.Condiciones = lbCondiciones.Text;
                    item.Promocion = lbPromocion.Text;
                    item.TipoPromocion = lbTipoPromocion.Text;
                    item.ValorPromocion = Convert.ToDecimal(lbValorPromocion.Text.Replace(",", ""));
                    item.VencePromocion = lbVencePromocion.Text;
                    item.FechaVencePromocion = Convert.ToDateTime(lbFechaVencePromocion.Text);
                    item.DescripcionPromocion = lbDescripcionPromocion.Text;
                    item.ObservacionesPromocion = lbObservacionesPromocion.Text;

                    if (jj > 1) Array.Resize(ref q, jj);
                    q[jj - 1] = item;
                }
            }

            gridArticulos.DataSource = q;
            gridArticulos.DataBind();
            gridArticulos.SelectedIndex = -1;

            setToolTips();
        }

        protected void gridArticulosLista_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void lkBuscar_Click(object sender, EventArgs e)
        {
            if (codigoArticuloDet.Text.Trim().Length == 0)
            {
                lbError.Text = "Debe ingresar un criterio para buscar";
                return;
            }

            buscarPreciosLista(false);
        }

        protected void lkImprimirLista_Click(object sender, EventArgs e)
        {
            base.LogActivity(MethodBase.GetCurrentMethod().Name);
            string mNombreDocumento = "";

            try
            {
                ValidarSesion();

                dsPuntoVenta ds = new dsPuntoVenta();
                ds = (dsPuntoVenta)ViewState["dsListaPrecios"];

                using (ReportDocument reporte = new ReportDocument())
                {
                    string p = (Request.PhysicalApplicationPath + "reportes/rptListaPrecios.rpt");
                    reporte.Load(p);

                    try
                    {
                        mNombreDocumento = string.Format("ListaPrecios{0}.pdf", DateTime.Now.Date.ToString().Replace("/", "").Replace(" 00:00:00", "").Replace(" 12:00:00 AM", "").Replace(" 12:00:00 a.m.", "").Replace("AM", "").Replace("am", "").Replace("PM", "").Replace("pm", "").Replace(":", "").Replace(".", "").Replace(" ", ""));
                    }
                    catch
                    {
                        mNombreDocumento = string.Format("ListaPrecios{0}.pdf", DateTime.Now.Date.Year.ToString());
                    }

                    if (File.Exists(mNombreDocumento)) File.Delete(mNombreDocumento);

                    reporte.SetDataSource(ds);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al imprimir {0} {1}", mNombreDocumento, ex.Message);
            }
        }

    }
}