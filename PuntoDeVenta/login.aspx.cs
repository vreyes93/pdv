﻿using System;
using System.Web.UI;

namespace PuntoDeVenta
{
    public partial class login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string proc = Request.QueryString["proc"];

            if (!string.IsNullOrEmpty(proc) && !string.IsNullOrWhiteSpace(proc))
                switch (proc)
                {
                    case "33588":
                        Response.Redirect("gerencia.aspx");
                        break;
                    case "33598":
                        string factura = Request.QueryString["a"];
                        Response.Redirect($"despachos.aspx?f={factura}");
                        break;
                    default:
                        Response.Redirect("~/");
                        break;
                }
            else
                Response.Redirect("~/");
        }
    }
}