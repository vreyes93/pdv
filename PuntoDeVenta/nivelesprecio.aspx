﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="nivelesprecio.aspx.cs" Inherits="PuntoDeVenta.nivelesprecio" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>&nbsp;&nbsp;Niveles de Precio</h3>
    <div class="content2">
    <div class="clientes">
        <ul>
            <li>
                <table align="center">
                    <tr>
                        <td>
                            Financiera:</td>
                        <td>
                            <asp:DropDownList ID="cbFinanciera" runat="server" Width="170px" 
                                AutoPostBack="True" onselectedindexchanged="cbFinanciera_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbContado" runat="server" Text="Contado" GroupName="tipo" 
                                TabIndex="10" AutoPostBack="True" 
                                oncheckedchanged="rbContado_CheckedChanged" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rbCredito" runat="server" Text="Crédito" GroupName="tipo" 
                                TabIndex="20" AutoPostBack="True" 
                                oncheckedchanged="rbCredito_CheckedChanged" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td>
                            Factor:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFactor" runat="server" Width="60px" TabIndex="30" 
                                
                                ToolTip="Ingrese el factor a utilizar en este nivel de precio, por ejemplo: 1.201600 (se aceptará un máximo de 6 decimales)"></asp:TextBox>
                        </td>
                        <td>
                            Nivel de Precio:
                        </td>
                        <td>
                            <asp:TextBox ID="txtNivelPrecio" runat="server" MaxLength="12" TabIndex="40" 
                                AutoPostBack="True" Width="90px" 
                                
                                ToolTip="Ingrese el nombre del nivel de precio, por ejemplo: Intercons12M (solo se aceptan 12 caracteres)"></asp:TextBox>
                        </td>
                        <td>
                            Estatus:</td>
                        <td>
                            <asp:DropDownList ID="cbEstatus" runat="server" TabIndex="50">
                                <asp:ListItem Value="Activo">Activo</asp:ListItem>
                                <asp:ListItem Value="Inactivo">Inactivo</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>                
            </li>
            <li>
                <table align="center">
                    <tr>
                        <td>
                            <asp:RadioButton ID="rb1Pago" runat="server" Text="1 Pago" GroupName="pagos" 
                                TabIndex="70" AutoPostBack="True" 
                                oncheckedchanged="rb1Pago_CheckedChanged" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rb3Pagos" runat="server" Text="3 Pagos" 
                                GroupName="pagos" TabIndex="80" AutoPostBack="True" 
                                oncheckedchanged="rb3Pagos_CheckedChanged" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rb4Pagos" runat="server" Text="4 Pagos" 
                                GroupName="pagos" TabIndex="90" AutoPostBack="True" 
                                oncheckedchanged="rb4Pagos_CheckedChanged" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rb6Pagos" runat="server" Text="6 Pagos" 
                                GroupName="pagos" TabIndex="100" AutoPostBack="True" 
                                oncheckedchanged="rb6Pagos_CheckedChanged" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rb9Pagos" runat="server" Text="9 Pagos" 
                                GroupName="pagos" TabIndex="110" AutoPostBack="True" 
                                oncheckedchanged="rb9Pagos_CheckedChanged" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rb10Pagos" runat="server" Text="10 Pagos" 
                                GroupName="pagos" TabIndex="120" AutoPostBack="True" 
                                oncheckedchanged="rb10Pagos_CheckedChanged" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rb12Pagos" runat="server" Text="12 Pagos" 
                                GroupName="pagos" TabIndex="130" AutoPostBack="True" 
                                oncheckedchanged="rb12Pagos_CheckedChanged" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rb15Pagos" runat="server" Text="15 Pagos" 
                                GroupName="pagos" TabIndex="140" AutoPostBack="True" 
                                oncheckedchanged="rb15Pagos_CheckedChanged" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rb18Pagos" runat="server" Text="18 Pagos" 
                                GroupName="pagos" TabIndex="150" AutoPostBack="True" 
                                oncheckedchanged="rb18Pagos_CheckedChanged" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rb24Pagos" runat="server" Text="24 Pagos" 
                                GroupName="pagos" TabIndex="160" AutoPostBack="True" 
                                oncheckedchanged="rb24Pagos_CheckedChanged" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rb36Pagos" runat="server" Text="36 Pagos" 
                                GroupName="pagos" TabIndex="170" AutoPostBack="True" 
                                oncheckedchanged="rb36Pagos_CheckedChanged" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Observa-ciones:</td>
                        <td colspan="10">
                            <asp:TextBox ID="txtObservaciones" runat="server" Width="830px" Height="50px" 
                                MaxLength="400" TabIndex="175" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Texto:</td>
                        <td colspan="8">
                            <asp:TextBox ID="txtTextoEncabezado" runat="server" MaxLength="200" style="text-transform: uppercase;"
                                TabIndex="176" Width="366px" 
                                ToolTip="Ingrese aquí el encabezado que saldrá impreso en las solicitudes de crédito"></asp:TextBox>
                        &nbsp;Es Contado:
                            <asp:DropDownList ID="cbPrecioContado" runat="server" TabIndex="177" 
                                ToolTip="Seleccione si el nivel aplicará Precio de Contado">
                                <asp:ListItem Value="N">No</asp:ListItem>
                                <asp:ListItem Value="S">Sí</asp:ListItem>
                            </asp:DropDownList>
&nbsp;Dias para pagar:
                            <asp:TextBox ID="txtDiasParaPagar" runat="server" TabIndex="178" 
                                ToolTip="Ingrese aquí los días para calcular la fecha de la primera cuota" 
                                Width="25px"></asp:TextBox>
                        </td>
                        <td style="text-align: right">
                            Tipo:</td>
                        <td>
                            <asp:DropDownList ID="cbEsCiudad" runat="server" TabIndex="179">
                                <asp:ListItem Value="A">Ambos</asp:ListItem>
                                <asp:ListItem Value="S">Ciudad</asp:ListItem>
                                <asp:ListItem Value="N">Departamental</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    </table>
            </li>
            <li>
                
                <table align="center">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <asp:TextBox ID="txtTipoVenta" runat="server" TabIndex="800" Visible="False" 
                                Width="5px"></asp:TextBox>
                            Buscar por nivel de precio:</td>
                        <td>
                            <asp:TextBox ID="txtNivelBuscar" runat="server" TabIndex="180" 
                                AutoPostBack="True" ontextchanged="txtNivelBuscar_TextChanged" 
                                ToolTip="Ingrese aquí parte del nombre del nivel de precios que desea buscar."></asp:TextBox>
                        </td>
                        <td>
                            Buscar por financiera:</td>
                        <td>
                            <asp:DropDownList ID="cbFinancieraBuscar" runat="server" Width="170px" 
                                AutoPostBack="True" 
                                onselectedindexchanged="cbFinancieraBuscar_SelectedIndexChanged" 
                                TabIndex="185" ToolTip="Seleccione la financiera que desea buscar"></asp:DropDownList>
                        </td>
                        <td>
                <asp:LinkButton ID="lbBuscarNivel" runat="server" 
                    ToolTip="Haga clic aquí para buscar un nivel de precio" TabIndex="190" 
                                onclick="lbBuscarNivel_Click">Buscar</asp:LinkButton>
                        </td>
                        <td>
                            <asp:LinkButton ID="lkBuscarTodos" runat="server" onclick="lkBuscarTodos_Click" 
                                ToolTip="Haga clic aquí para realizar la búsqueda entre todos los nivles de precio existentes en el sistema">..</asp:LinkButton>
                        </td>
                        <td>
                            <asp:LinkButton ID="lkListarTodos" runat="server" onclick="lkListarTodos_Click" 
                                ToolTip="Haga clic aquí para listar todos los niveles de precio existencies en el sistema">...</asp:LinkButton>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                </table>
                
            </li>
            <li>
                <asp:GridView ID="gvNivelesPrecio" runat="server" AutoGenerateColumns="False" 
                    BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="3" CellSpacing="2" 
                    onrowdatabound="gvNivelesPrecio_RowDataBound" 
                    onselectedindexchanged="gvNivelesPrecio_SelectedIndexChanged" TabIndex="200" 
                    Visible="False">
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lkSeleccionar" runat="server" CausesValidation="False" 
                                    CommandName="Select" Text="Seleccionar"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="NivelPrecio" HeaderText="Nivel de Precio" />
                        <asp:TemplateField HeaderText="Factor">
                            <ItemTemplate>
                                <asp:Label ID="lbFactor" runat="server" Text='<%# Bind("Factor", "{0:####,###,###,###,##.000000}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFactor" runat="server" Text='<%# Bind("Factor", "{0:####,###,###,###,##.000000}") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TipoVenta" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbTipoVenta" runat="server" Text='<%# Bind("TipoVenta") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTipoVenta" runat="server" Text='<%# Bind("TipoVenta") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="TipoDeVenta" HeaderText="Tipo de Venta" />
                        <asp:TemplateField HeaderText="Financiera" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbFinanciera" runat="server" Text='<%# Bind("Financiera") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFinanciera" runat="server" Text='<%# Bind("Financiera") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="NombreFinanciera" HeaderText="Financiera" />
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" />
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" />
                        <asp:BoundField DataField="Pagos" HeaderText="Pagos" />
                        <asp:TemplateField HeaderText="Observaciones" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbObservaciones" runat="server" Text='<%# Bind("Observaciones") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtObservaciones" runat="server" Text='<%# Bind("Observaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EsCiudad" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbEsCiudad" runat="server" Text='<%# Bind("EsCiudad") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEsCiudad" runat="server" Text='<%# Bind("EsCiudad") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TextoEncabezado" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbTextoEncabezado" runat="server" Text='<%# Bind("TextoEncabezado") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTextoEncabezado" runat="server" Text='<%# Bind("TextoEncabezado") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DiasParaPagar" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbDiasParaPagar" runat="server" Text='<%# Bind("DiasParaPagar") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDiasParaPagar" runat="server" Text='<%# Bind("DiasParaPagar") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PrecioContado" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecioContado" runat="server" Text='<%# Bind("PrecioContado") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecioContado" runat="server" Text='<%# Bind("PrecioContado") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#F8AA55" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
            </li>
            <li>
                <table align="center">
                    <tr>
                        <td style="text-align: center">
                            &nbsp;
                            <asp:LinkButton ID="lkGrabar" runat="server" 
                                ToolTip="Haga clic aquí para grabar los datos del tipo de venta" TabIndex="210" 
                                onclick="lkGrabar_Click">Grabar</asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="lkLimpiar" runat="server" 
                                ToolTip="Haga clic aquí para limpiar los datos de la página e ingresar un tipo de venta nuevo" 
                                TabIndex="220" onclick="lkLimpiar_Click">Limpiar</asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </li>
        </ul>
    </div>
        <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server" TabIndex="520"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" TabIndex="530"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
