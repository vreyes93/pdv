﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace PuntoDeVenta
{
    public partial class nivelesprecio : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                HtmlGenericControl submenu = new HtmlGenericControl();
                submenu = (HtmlGenericControl)Master.FindControl("submenu");
                submenu.Visible = true;

                HtmlGenericControl lkTiposVenta = new HtmlGenericControl();
                lkTiposVenta = (HtmlGenericControl)Master.FindControl("lkTiposVenta");
                lkTiposVenta.Visible = true;

                HtmlGenericControl lkFinancieras = new HtmlGenericControl();
                lkFinancieras = (HtmlGenericControl)Master.FindControl("lkFinancieras");
                lkFinancieras.Visible = true;

                HtmlGenericControl lkNivelesPrecio = new HtmlGenericControl();
                lkNivelesPrecio = (HtmlGenericControl)Master.FindControl("lkNivelesPrecio");
                lkNivelesPrecio.Visible = true;

                HtmlGenericControl lkCrearOfertas = new HtmlGenericControl();
                lkCrearOfertas = (HtmlGenericControl)Master.FindControl("lkCrearOfertas");
                lkCrearOfertas.Visible = true;

                HtmlGenericControl lkEnviarMails = new HtmlGenericControl();
                lkEnviarMails = (HtmlGenericControl)Master.FindControl("lkEnviarMails");
                lkEnviarMails.Visible = true;

                ViewState["url"] = Convert.ToString(Session["Url"]);
                ViewState["urlPrecios"] = Convert.ToString(Session["UrlPrecios"]);

                limpiar();
            }
        }
        
        void limpiar()
        {
            txtNivelPrecio.Text = "";
            txtNivelBuscar.Text = "";
            txtFactor.Text = "";
            txtObservaciones.Text = "";
            txtTipoVenta.Text = "NR";
            txtTextoEncabezado.Text = "";
            cbEsCiudad.SelectedValue = "A";
            txtDiasParaPagar.Text = "30";
            cbPrecioContado.SelectedValue = "N";

            gvNivelesPrecio.Visible = false;
            cbEstatus.SelectedValue = "Activo";

            rbContado.Checked = false;
            rbCredito.Checked = false;

            rb1Pago.Checked = false;
            rb3Pagos.Checked = false;
            rb4Pagos.Checked = false;
            rb6Pagos.Checked = false;
            rb9Pagos.Checked = false;
            rb10Pagos.Checked = false;
            rb12Pagos.Checked = false;
            rb15Pagos.Checked = false;
            rb18Pagos.Checked = false;
            rb24Pagos.Checked = false;
            rb36Pagos.Checked = false;

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            DataSet ds = new DataSet(); DataColumn[] dc = new DataColumn[1];
            var qFinancieras = ws.DevuelveFinancierasSeleccion("NR").ToList();

            wsPuntoVenta.Financieras item = new wsPuntoVenta.Financieras();
            item.Financiera = 0;
            item.Nombre = "Seleccione financiera ...";
            item.prefijo = "";
            item.Estatus = "Activa";
            qFinancieras.Add(item);

            ds.Tables.Add(new DataTable("Financieras"));
            ds.Tables["financieras"].Columns.Add(new DataColumn("Financiera", typeof(System.Int32)));
            ds.Tables["financieras"].Columns.Add(new DataColumn("Nombre", typeof(System.String)));
            ds.Tables["financieras"].Columns.Add(new DataColumn("prefijo", typeof(System.String)));
            ds.Tables["financieras"].Columns.Add(new DataColumn("Estatus", typeof(System.String)));

            for (int ii = 0; ii < qFinancieras.Count; ii++)
            {
                DataRow row = ds.Tables["Financieras"].NewRow();
                row["Financiera"] = qFinancieras[ii].Financiera;
                row["Nombre"] = qFinancieras[ii].Nombre;
                row["prefijo"] = qFinancieras[ii].prefijo;
                row["Estatus"] = qFinancieras[ii].Estatus;
                ds.Tables["Financieras"].Rows.Add(row);
            }

            dc[0] = ds.Tables["Financieras"].Columns["Financiera"];
            ds.Tables["Financieras"].PrimaryKey = dc;

            ViewState["nuevo"] = true;
            ViewState["dsFinancieras"] = ds;

            cbFinanciera.DataSource = qFinancieras;
            cbFinanciera.DataTextField = "Nombre";
            cbFinanciera.DataValueField = "Financiera";
            cbFinanciera.DataBind();

            cbFinancieraBuscar.DataSource = qFinancieras;
            cbFinancieraBuscar.DataTextField = "Nombre";
            cbFinancieraBuscar.DataValueField = "Financiera";
            cbFinancieraBuscar.DataBind();

            ocultarBusquedaNivel();
            cbFinanciera.Focus();

            cbFinanciera.SelectedValue = "0";
            cbFinancieraBuscar.SelectedValue = "0";
        }

        bool pasaValidaciones()
        {
            if (cbFinanciera.SelectedValue == "0")
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('Debe seleccionarla financiera para este nivel de precio.');"), true);
                cbFinanciera.Focus();
                return false;
            }
                
            if (txtNivelPrecio.Text.Trim().Length == 0)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('Debe ingresar el nombre del nivel de precio, Ejemplo: INTERCONS12M.');"), true);
                txtNivelPrecio.Focus();
                return false;
            }

            try
            {
                decimal mFactor = Convert.ToDecimal(txtFactor.Text);

                if (mFactor <= 0)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('Debe ingresar factor válido.');", true);
                    txtFactor.Focus();
                    return false;
                }
            }
            catch
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('Debe ingresar un factor válido.');", true);
                txtFactor.Focus();
                return false;
            }

            if (!rbContado.Checked && !rbCredito.Checked)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('Debe seleccionar si el nivel de precio de de contado o crédito.');", true);
                rbContado.Focus();
                return false;
            }

            if (!rb1Pago.Checked && !rb3Pagos.Checked && !rb4Pagos.Checked && !rb6Pagos.Checked & !rb9Pagos.Checked & !rb10Pagos.Checked & !rb12Pagos.Checked && !rb15Pagos.Checked && !rb18Pagos.Checked && !rb24Pagos.Checked & !rb36Pagos.Checked)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('Debe seleccionar la cantidad de pagos de este nivel de precio.');", true);
                rbContado.Focus();
                return false;
            }

            //if (rbContado.Checked)
            //{
            //    if (!rb1Pago.Checked)
            //    {
            //        Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('Si seleccionó Contado, también debe seleccionar 1 Pago.');", true);
            //        rb1Pago.Focus();
            //        return false;
            //    }
            //}
            //if (rbCredito.Checked)
            //{
            //    if (rb1Pago.Checked)
            //    {
            //        Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('Si seleccionó Crédito, no puede seleccionar 1 Pago.');", true);
            //        rb1Pago.Focus();
            //        return false;
            //    }
            //}

            return true;
        }

        protected void lkGrabar_Click(object sender, EventArgs e)
        {
            if (!pasaValidaciones()) return;

            string mensaje = ""; string mTipo = ""; string mPagos = "";
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            lbInfo.Text = "";
            lbError.Text = "";

            if (rbContado.Checked) mTipo = "CO";
            if (rbCredito.Checked) mTipo = "CR";

            if (rb1Pago.Checked) mPagos = "Un pago de Contado";
            if (rb3Pagos.Checked) mPagos = "3";
            if (rb4Pagos.Checked) mPagos = "4";
            if (rb6Pagos.Checked) mPagos = "6";
            if (rb9Pagos.Checked) mPagos = "9";
            if (rb10Pagos.Checked) mPagos = "10";
            if (rb12Pagos.Checked) mPagos = "12";
            if (rb15Pagos.Checked) mPagos = "15";
            if (rb18Pagos.Checked) mPagos = "18";
            if (rb24Pagos.Checked) mPagos = "24";
            if (rb36Pagos.Checked) mPagos = "36";

            if (!ws.GrabarNivelPrecio(txtNivelPrecio.Text, Convert.ToDecimal(txtFactor.Text), txtTipoVenta.Text, Convert.ToInt32(cbFinanciera.SelectedValue), cbFinanciera.SelectedItem.Text, cbEstatus.SelectedValue, mTipo, mPagos, Convert.ToBoolean(ViewState["nuevo"]), ref mensaje, Convert.ToString(Session["usuario"]), txtObservaciones.Text, cbEsCiudad.SelectedValue, txtTextoEncabezado.Text, txtDiasParaPagar.Text, cbPrecioContado.SelectedValue))
            {
                lbError.Text = mensaje;
                txtNivelBuscar.Focus();
                return;
            }

            //Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('{0}');", mensaje), true);
            lbInfo.Text = mensaje;
            limpiar();
        }

        protected void lkLimpiar_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        protected void lbBuscarNivel_Click(object sender, EventArgs e)
        {
            if (lbBuscarNivel.Text == "Buscar")
            {
                BuscarNivel("N");
            }
            else
            {
                ocultarBusquedaNivel();
            }
        }

        protected void cbFinancieraBuscar_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuscarNivel("F");
        }

        void ocultarBusquedaNivel()
        {
            gvNivelesPrecio.Visible = false;
            
            lbBuscarNivel.Text = "Buscar";
            lbBuscarNivel.ToolTip = "Haga clic aquí para buscar un nivel de precio";

            txtNivelBuscar.Focus();
        }

        void BuscarNivel(string tipo)
        {
            if (tipo == "F")
            {
                if (cbFinancieraBuscar.SelectedValue == "0")
                {
                    cbFinancieraBuscar.Focus();
                    return;
                }
            }
            else
            {
                if (txtNivelBuscar.Text.Trim().Length == 0)
                {
                    txtNivelBuscar.Focus();
                    return;
                }
            }

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveNivelesPrecio(txtNivelBuscar.Text, Convert.ToInt32(cbFinancieraBuscar.SelectedValue), tipo);

            gvNivelesPrecio.DataSource = q;
            gvNivelesPrecio.DataBind();
            gvNivelesPrecio.Visible = true;

            if (q.Count() > 0)
            {
                gvNivelesPrecio.Focus();
                gvNivelesPrecio.SelectedIndex = 0;
                LinkButton lkSeleccionar = (LinkButton)gvNivelesPrecio.SelectedRow.FindControl("lkSeleccionar");

                lkSeleccionar.Focus();
            }

            lbBuscarNivel.Text = "Ocultar";
            lbBuscarNivel.ToolTip = "Haga clic aquí para ocultar la búsqueda de los niveles de precio";
        }

        protected void gvNivelesPrecio_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gvNivelesPrecio_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["nuevo"] = false;
            GridViewRow gvr = gvNivelesPrecio.SelectedRow;

            Label lbFinanciera = (Label)gvr.FindControl("lbFinanciera");
            Label lbTipoVenta = (Label)gvr.FindControl("lbTipoVenta");
            Label lbObservaciones = (Label)gvr.FindControl("lbObservaciones");
            Label lbFactor = (Label)gvr.FindControl("lbFactor");
            Label lbEsCiudad = (Label)gvr.FindControl("lbEsCiudad");
            Label lbTextoEncabezado = (Label)gvr.FindControl("lbTextoEncabezado");
            Label lbDiasParaPagar = (Label)gvr.FindControl("lbDiasParaPagar");
            Label lbPrecioContado = (Label)gvr.FindControl("lbPrecioContado");

            txtNivelPrecio.Text = gvr.Cells[1].Text;
            txtFactor.Text = lbFactor.Text;
            txtTipoVenta.Text = lbTipoVenta.Text;
            cbFinanciera.SelectedValue = lbFinanciera.Text;
            cbEstatus.SelectedValue = gvr.Cells[7].Text;
            txtObservaciones.Text = lbObservaciones.Text;
            cbEsCiudad.SelectedValue = lbEsCiudad.Text;
            txtTextoEncabezado.Text = lbTextoEncabezado.Text;
            txtDiasParaPagar.Text = lbDiasParaPagar.Text;
            cbPrecioContado.SelectedValue = lbPrecioContado.Text;

            if (gvr.Cells[8].Text == "Contado")
            {
                rbContado.Checked = true;
            }
            else
            {
                rbCredito.Checked = true;
            }

            rb1Pago.Checked = false;
            rb3Pagos.Checked = false;
            rb4Pagos.Checked = false;
            rb6Pagos.Checked = false;
            rb9Pagos.Checked = false;
            rb10Pagos.Checked = false;
            rb12Pagos.Checked = false;
            rb15Pagos.Checked = false;
            rb18Pagos.Checked = false;
            rb24Pagos.Checked = false;
            rb36Pagos.Checked = false;

            string m = gvr.Cells[9].Text;
            switch (gvr.Cells[9].Text)
            {
                case "3 PAGOS":
                    rb3Pagos.Checked = true;
                    break;
                case "4 PAGOS":
                    rb4Pagos.Checked = true;
                    break;
                case "6 PAGOS":
                    rb6Pagos.Checked = true;
                    break;
                case "9 PAGOS":
                    rb9Pagos.Checked = true;
                    break;
                case "10 PAGOS":
                    rb10Pagos.Checked = true;
                    break;
                case "12 PAGOS":
                    rb12Pagos.Checked = true;
                    break;
                case "15 PAGOS":
                    rb15Pagos.Checked = true;
                    break;
                case "18 PAGOS":
                    rb18Pagos.Checked = true;
                    break;
                case "24 PAGOS":
                    rb24Pagos.Checked = true;
                    break;
                case "36 PAGOS":
                    rb36Pagos.Checked = true;
                    break;
                default:
                    rb1Pago.Checked = true;
                    break;
            }       

            ocultarBusquedaNivel();
        }

        protected void txtNivelBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarNivel("N");
        }

        protected void rbContado_CheckedChanged(object sender, EventArgs e)
        {
            if (rbContado.Checked) rb1Pago.Checked = true;
            txtNivelPrecio.Text = devuelveNombreNivelPrecio();
        }

        protected void rbCredito_CheckedChanged(object sender, EventArgs e)
        {
            if (rbCredito.Checked) rb3Pagos.Checked = true;
            txtNivelPrecio.Text = devuelveNombreNivelPrecio();

            lbInfo.Text = "";
            lbError.Text = "";
        }

        protected void cbFinanciera_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtNivelPrecio.Text = devuelveNombreNivelPrecio();
        }

        string devuelveNombreNivelPrecio()
        {
            DataSet ds = new DataSet(); string mPagos = "";
            ds = (DataSet)ViewState["dsFinancieras"]; string mRetorna = "";

            if (rb3Pagos.Checked) mPagos = "03M";
            if (rb4Pagos.Checked) mPagos = "04M";
            if (rb6Pagos.Checked) mPagos = "06M";
            if (rb9Pagos.Checked) mPagos = "09M";
            if (rb10Pagos.Checked) mPagos = "10M";
            if (rb12Pagos.Checked) mPagos = "12M";
            if (rb15Pagos.Checked) mPagos = "15M";
            if (rb18Pagos.Checked) mPagos = "18M";
            if (rb24Pagos.Checked) mPagos = "24M";
            if (rb36Pagos.Checked) mPagos = "36M";

            mRetorna = string.Format("{0}{1}", ds.Tables["Financieras"].Rows.Find(cbFinanciera.SelectedValue)["prefijo"], mPagos);
            if (mRetorna.Length > 12) mRetorna = string.Format("{0}{1}", ds.Tables["Financieras"].Rows.Find(cbFinanciera.SelectedValue)["prefijo"].ToString().Substring(0, 9), mPagos);

            return mRetorna;
        }

        protected void rb1Pago_CheckedChanged(object sender, EventArgs e)
        {
            txtNivelPrecio.Text = devuelveNombreNivelPrecio();
        }

        protected void rb3Pagos_CheckedChanged(object sender, EventArgs e)
        {
            txtNivelPrecio.Text = devuelveNombreNivelPrecio();
        }

        protected void rb4Pagos_CheckedChanged(object sender, EventArgs e)
        {
            txtNivelPrecio.Text = devuelveNombreNivelPrecio();
        }

        protected void rb6Pagos_CheckedChanged(object sender, EventArgs e)
        {
            txtNivelPrecio.Text = devuelveNombreNivelPrecio();
        }

        protected void rb9Pagos_CheckedChanged(object sender, EventArgs e)
        {
            txtNivelPrecio.Text = devuelveNombreNivelPrecio();
        }

        protected void rb10Pagos_CheckedChanged(object sender, EventArgs e)
        {
            txtNivelPrecio.Text = devuelveNombreNivelPrecio();
        }

        protected void rb12Pagos_CheckedChanged(object sender, EventArgs e)
        {
            txtNivelPrecio.Text = devuelveNombreNivelPrecio();
        }

        protected void rb15Pagos_CheckedChanged(object sender, EventArgs e)
        {
            txtNivelPrecio.Text = devuelveNombreNivelPrecio();
        }

        protected void rb18Pagos_CheckedChanged(object sender, EventArgs e)
        {
            txtNivelPrecio.Text = devuelveNombreNivelPrecio();
        }

        protected void rb24Pagos_CheckedChanged(object sender, EventArgs e)
        {
            txtNivelPrecio.Text = devuelveNombreNivelPrecio();
        }

        protected void rb36Pagos_CheckedChanged(object sender, EventArgs e)
        {
            txtNivelPrecio.Text = devuelveNombreNivelPrecio();
        }
        
        protected void lkBuscarTodos_Click(object sender, EventArgs e)
        {
            if (txtNivelBuscar.Text.Trim().Length == 0)
            {
                txtNivelBuscar.Focus();
                return;
            }

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveNivelesPrecioTodos(txtNivelBuscar.Text);

            gvNivelesPrecio.DataSource = q;
            gvNivelesPrecio.DataBind();
            gvNivelesPrecio.Visible = true;

            lbBuscarNivel.Text = "Ocultar";
            lbBuscarNivel.ToolTip = "Haga clic aquí para ocultar la búsqueda de los niveles de precio";
        }

        protected void lkListarTodos_Click(object sender, EventArgs e)
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveNivelesPrecioListar();

            gvNivelesPrecio.DataSource = q;
            gvNivelesPrecio.DataBind();
            gvNivelesPrecio.Visible = true;

            lbBuscarNivel.Text = "Ocultar";
            lbBuscarNivel.ToolTip = "Haga clic aquí para ocultar la búsqueda de los niveles de precio";
        }

    }
}