﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="ofertas.aspx.cs" Inherits="PuntoDeVenta.ofertas" MaintainScrollPositionOnPostback="true" %>
<%@ Register assembly="DevExpress.Web.v16.2, Version=16.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>&nbsp;&nbsp;O F E R T A S &nbsp;&nbsp;! ! !</h3>
    <div class="content2">
    <div class="clientes">
        <ul>
            <li>
                <table align="center">
                    <tr>
                        <td style="text-align: center">
                            <asp:Label ID="lbInfoOfertas" runat="server" Text="Las siguientes ofertas se encuentran vigentes:"></asp:Label>
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="gridOfertas" runat="server" 
                    BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="3" CellSpacing="2" AutoGenerateColumns="False" 
                    onrowdatabound="gridOfertas_RowDataBound">
                    <Columns>
                        <asp:TemplateField ShowHeader="False" Visible="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lkEliminarOferta" runat="server" CausesValidation="False" 
                                    CommandName="Select" Text="Eliminar" 
                                    ToolTip="Haga clic aquí para eliminar la oferta" 
                                    onclientclick="return confirm('Desea eliminar la oferta?');"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False" Visible="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lkCancelarOferta" runat="server" CausesValidation="False" 
                                    CommandName="Select" Text="Cancelar" 
                                    ToolTip="Haga clic aquí para cancelar la oferta" 
                                    onclientclick="return confirm('Desea cancelar la oferta?');"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="FechaInicial" HeaderText="Fecha Inicial" DataFormatString="{0:d}" Visible="false" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FechaFinal" HeaderText="Fecha Vence" DataFormatString="{0:d}" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Articulo" HeaderText="Artículo" >
                        <HeaderStyle Width="100px" />
                        <ItemStyle Width="100px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripción" ItemStyle-Font-Size="X-Small" ><ItemStyle Font-Size="X-Small"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" Visible="false" />
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" Visible="false" />
                        <asp:TemplateField HeaderText="identificador" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbIdentificador" runat="server" 
                                    Text='<%# Bind("identificador") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtIdentificador" runat="server" Text='<%# Bind("identificador") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Precio Original">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecioOriginal" runat="server" Text='<%# Bind("PrecioOriginal", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecioOriginal" runat="server" Text='<%# Bind("PrecioOriginal", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Precio Oferta">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecioOferta" runat="server" Text='<%# Bind("PrecioOferta", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecioOferta" runat="server" Text='<%# Bind("PrecioOferta", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" Font-Bold="True" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
<asp:GridView ID="gridArticulos" runat="server" CellPadding="3" TabIndex="130" 
                    AutoGenerateColumns="False" 
                    style="text-align: left" BackColor="#DEBA84" BorderColor="#DEBA84" 
                    BorderStyle="None" BorderWidth="1px" CellSpacing="2" 
                onrowdatabound="gridArticulos_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="Articulo" HeaderText="Artículo" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre del Artículo en OFERTA" />
                        <asp:TemplateField HeaderText="Precio">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecio" runat="server" Text='<%# Bind("Precio", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecio" runat="server" Text='<%# Bind("Precio", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="NivelPrecio" HeaderText="Nivel de Precio" />
                        <asp:TemplateField HeaderText="Oferta" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbOferta" runat="server" Text='<%# Bind("Oferta") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOferta" runat="server" Text='<%# Bind("Oferta") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PrecioOriginal" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecioOriginal" runat="server" Text='<%# Bind("PrecioOriginal") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecioOriginal" runat="server" Text='<%# Bind("PrecioOriginal") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FechaVence" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbFechaVence" runat="server" Text='<%# Bind("FechaVence") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFechaVence" runat="server" Text='<%# Bind("FechaVence") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DescuentoContado" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbDescuentoContado" runat="server" Text='<%# Bind("DescuentoContado") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescuentoContado" runat="server" Text='<%# Bind("DescuentoContado") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DescuentoCredito" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbDescuentoCredito" runat="server" Text='<%# Bind("DescuentoCredito") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescuentoCredito" runat="server" Text='<%# Bind("DescuentoCredito") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Condicional" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbCondicional" runat="server" Text='<%# Bind("Condicional") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCondicional" runat="server" Text='<%# Bind("Condicional") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PrecioOferta" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecioOferta" runat="server" Text='<%# Bind("PrecioOferta") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecioOferta" runat="server" Text='<%# Bind("PrecioOferta") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Condiciones" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbCondiciones" runat="server" Text='<%# Bind("Condiciones") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCondiciones" runat="server" Text='<%# Bind("Condiciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Promocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbPromocion" runat="server" Text='<%# Bind("Promocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPromocion" runat="server" Text='<%# Bind("Promocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TipoPromocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbTipoPromocion" runat="server" Text='<%# Bind("TipoPromocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTipoPromocion" runat="server" Text='<%# Bind("TipoPromocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ValorPromocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbValorPromocion" runat="server" Text='<%# Bind("ValorPromocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtValorPromocion" runat="server" Text='<%# Bind("ValorPromocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VencePromocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbVencePromocion" runat="server" Text='<%# Bind("VencePromocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVencePromocion" runat="server" Text='<%# Bind("VencePromocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FechaVencePromocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbFechaVencePromocion" runat="server" Text='<%# Bind("FechaVencePromocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFechaVencePromocion" runat="server" Text='<%# Bind("FechaVencePromocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DescripcionPromocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbDescripcionPromocion" runat="server" Text='<%# Bind("DescripcionPromocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescripcionPromocion" runat="server" Text='<%# Bind("DescripcionPromocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ObservacionesPromocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbObservacionesPromocion" runat="server" Text='<%# Bind("ObservacionesPromocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtObservacionesPromocion" runat="server" Text='<%# Bind("ObservacionesPromocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pagos" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbPagos" runat="server" Text='<%# Bind("Pagos") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPagos" runat="server" Text='<%# Bind("Pagos") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FechaDesde" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbFechaDesde" runat="server" Text='<%# Bind("FechaDesde") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFechaDesde" runat="server" Text='<%# Bind("FechaDesde") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TipoOferta" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbTipoOferta" runat="server" Text='<%# Bind("TipoOferta") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTipoOferta" runat="server" Text='<%# Bind("TipoOferta") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#F8AA55" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
            </asp:GridView>
<asp:GridView ID="gridTiposVenta" runat="server" AutoGenerateColumns="False" 
                            BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                            CellPadding="3" CellSpacing="2" Visible="true" 
                onrowdatabound="gridTiposVenta_RowDataBound" 
                TabIndex="600">
                            <Columns>
                                <asp:TemplateField ShowHeader="False" Visible="false">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkSeleccionarTipoVenta" runat="server" 
                                            CausesValidation="False" CommandName="Select" Text="Seleccionar" 
                                            ToolTip="Haga clic aquí para seleccionar un tipo de venta para modificarlo"></asp:LinkButton>
                                    </ItemTemplate>
                                    <HeaderStyle Width="40px" />
                                    <ItemStyle Width="40px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="CodigoTipoVenta" HeaderText="Código" Visible="false">
                                <HeaderStyle Width="50px" />
                                <ItemStyle Width="50px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Descripcion" HeaderText="Promoción" >
                                <HeaderStyle Width="100px" />
                                <ItemStyle Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Estatus" HeaderText="Estatus" visible="false">
                                <HeaderStyle Width="60px" />
                                <ItemStyle Width="60px" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Observaciones" Visible="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lbObservaciones" runat="server" 
                                            Text='<%# Bind("Observaciones") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtObservaciones" runat="server" Text='<%# Bind("Observaciones") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="TodosLosNiveles" Visible="False">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtTodosLosNiveles" runat="server" Text='<%# Bind("TodosLosNiveles") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbTodosLosNiveles" runat="server" Text='<%# Bind("TodosLosNiveles") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vence" Visible="false">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtVence" runat="server" Text='<%# Bind("Vence") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbVence" runat="server" Text='<%# Bind("Vence") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Fecha Vence" Visible="true">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtFechaVence" runat="server" Text='<%# Bind("FechaVence") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbFechaVence" runat="server" Text='<%# Bind("FechaVence", "{0:d}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                            <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                            <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                            <SelectedRowStyle BackColor="#F8AA55" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#FFF1D4" />
                            <SortedAscendingHeaderStyle BackColor="#B95C30" />
                            <SortedDescendingCellStyle BackColor="#F1E5CE" />
                            <SortedDescendingHeaderStyle BackColor="#93451F" />
                        </asp:GridView>    
<asp:GridView ID="gridArticulosPromocion" runat="server" CellPadding="3" TabIndex="130" 
                    AutoGenerateColumns="False" 
                    style="text-align: left" BackColor="#DEBA84" BorderColor="#DEBA84" 
                    BorderStyle="None" BorderWidth="1px" CellSpacing="2" 
                onrowdatabound="gridArticulosPromocion_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="Articulo" HeaderText="Artículo" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre del Artículo en PROMOCION" />
                        <asp:TemplateField HeaderText="Precio">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecio" runat="server" Text='<%# Bind("Precio", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecio" runat="server" Text='<%# Bind("Precio", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="NivelPrecio" HeaderText="Nivel de Precio" />
                        <asp:TemplateField HeaderText="Oferta" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbOferta" runat="server" Text='<%# Bind("Oferta") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtOferta" runat="server" Text='<%# Bind("Oferta") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PrecioOriginal" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecioOriginal" runat="server" Text='<%# Bind("PrecioOriginal") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecioOriginal" runat="server" Text='<%# Bind("PrecioOriginal") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FechaVence" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbFechaVence" runat="server" Text='<%# Bind("FechaVence") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFechaVence" runat="server" Text='<%# Bind("FechaVence") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DescuentoContado" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbDescuentoContado" runat="server" Text='<%# Bind("DescuentoContado") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescuentoContado" runat="server" Text='<%# Bind("DescuentoContado") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DescuentoCredito" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbDescuentoCredito" runat="server" Text='<%# Bind("DescuentoCredito") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescuentoCredito" runat="server" Text='<%# Bind("DescuentoCredito") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Condicional" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbCondicional" runat="server" Text='<%# Bind("Condicional") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCondicional" runat="server" Text='<%# Bind("Condicional") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PrecioOferta" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecioOferta" runat="server" Text='<%# Bind("PrecioOferta") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecioOferta" runat="server" Text='<%# Bind("PrecioOferta") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Condiciones" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbCondiciones" runat="server" Text='<%# Bind("Condiciones") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCondiciones" runat="server" Text='<%# Bind("Condiciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Promocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbPromocion" runat="server" Text='<%# Bind("Promocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPromocion" runat="server" Text='<%# Bind("Promocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TipoPromocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbTipoPromocion" runat="server" Text='<%# Bind("TipoPromocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTipoPromocion" runat="server" Text='<%# Bind("TipoPromocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ValorPromocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbValorPromocion" runat="server" Text='<%# Bind("ValorPromocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtValorPromocion" runat="server" Text='<%# Bind("ValorPromocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VencePromocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbVencePromocion" runat="server" Text='<%# Bind("VencePromocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtVencePromocion" runat="server" Text='<%# Bind("VencePromocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FechaVencePromocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbFechaVencePromocion" runat="server" Text='<%# Bind("FechaVencePromocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFechaVencePromocion" runat="server" Text='<%# Bind("FechaVencePromocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DescripcionPromocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbDescripcionPromocion" runat="server" Text='<%# Bind("DescripcionPromocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescripcionPromocion" runat="server" Text='<%# Bind("DescripcionPromocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ObservacionesPromocion" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbObservacionesPromocion" runat="server" Text='<%# Bind("ObservacionesPromocion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtObservacionesPromocion" runat="server" Text='<%# Bind("ObservacionesPromocion") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#F8AA55" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
            </asp:GridView>
        </li>
        </ul>
    </div>
        <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server" TabIndex="520"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" TabIndex="530"></asp:Label>
                                <asp:Label ID="Label2" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
