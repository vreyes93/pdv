﻿using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace PuntoDeVenta
{
    public partial class ofertas : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                ViewState["url"] = Convert.ToString(Session["Url"]);

                buscarOfertasVigentes();
            }
        }
        
        void buscarOfertasVigentes()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveOfertasListado(Convert.ToString(Session["Tienda"]));
            
            gridOfertas.DataSource = q;
            gridOfertas.DataBind();
            gridOfertas.SelectedIndex = -1;
        }

        void buscarPreciosLista()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveArticulosPrecios(true, "", 0, "", false, "A", "", "", Convert.ToString(Session["Tienda"]));
            //var qPromociones = ws.DevuelveArticulosPrecios(true, "", 0, "", false, "A", "", "");

            q = q.Where(x => x.Oferta == "S").ToArray();
            //qPromociones = qPromociones.Where(x => x.Promocion == "S").ToArray();

            gridArticulos.DataSource = q;
            gridArticulos.DataBind();
            gridArticulos.SelectedIndex = -1;

            //gridArticulosPromocion.DataSource = qPromociones;
            //gridArticulosPromocion.DataBind();
            //gridArticulosPromocion.SelectedIndex = -1;

            if (q.Length == 0)
            {
                lbInfoOfertas.Text = "No existen ofertas vigentes en este momento.";
            }
            else
            {
                lbInfoOfertas.Text = "Las siguientes ofertas se encuentran vigentes:";
            }

            setToolTips();
            setToolTipsPromocion();
        }

        void buscarPromociones()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveTiposVenta();
            q = q.Where(x => x.Estatus == "Activo").ToArray();

            gridTiposVenta.DataSource = q;
            gridTiposVenta.DataBind();
        }

        void setToolTips()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
            {
                Label lbOferta = (Label)gridArticulos.Rows[ii].FindControl("lbOferta");
                Label lbPrecio = (Label)gridArticulos.Rows[ii].FindControl("lbPrecio");
                Label lbPrecioOriginal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioOriginal");
                Label lbFechaVence = (Label)gridArticulos.Rows[ii].FindControl("lbFechaVence");
                Label lbDescuentoContado = (Label)gridArticulos.Rows[ii].FindControl("lbDescuentoContado");
                Label lbDescuentoCredito = (Label)gridArticulos.Rows[ii].FindControl("lbDescuentoCredito");
                Label lbCondicional = (Label)gridArticulos.Rows[ii].FindControl("lbCondicional");
                Label lbPrecioOferta = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioOferta");
                Label lbCondiciones = (Label)gridArticulos.Rows[ii].FindControl("lbCondiciones");
                Label lbPromocion = (Label)gridArticulos.Rows[ii].FindControl("lbPromocion");
                Label lbTipoPromocion = (Label)gridArticulos.Rows[ii].FindControl("lbTipoPromocion");
                Label lbValorPromocion = (Label)gridArticulos.Rows[ii].FindControl("lbValorPromocion");
                Label lbVencePromocion = (Label)gridArticulos.Rows[ii].FindControl("lbVencePromocion");
                Label lbFechaVencePromocion = (Label)gridArticulos.Rows[ii].FindControl("lbFechaVencePromocion");
                Label lbDescripcionPromocion = (Label)gridArticulos.Rows[ii].FindControl("lbDescripcionPromocion");
                Label lbObservacionesPromocion = (Label)gridArticulos.Rows[ii].FindControl("lbObservacionesPromocion");
                Label lbPagos = (Label)gridArticulos.Rows[ii].FindControl("lbPagos");

                if (lbOferta.Text == "S")
                {
                    if (lbCondicional.Text == "S")
                    {
                        string mCondiciones = ws.DevuelveCondiciones(gridArticulos.Rows[ii].Cells[0].Text, Convert.ToString(Session["Tienda"]));
                        lbPrecio.ToolTip = string.Format("Este artículo está en OFERTA!!!{0}{3}{1} y vence el: {2}{0}La oferta aplica en la compra de:{0}{4}", Environment.NewLine, "", Convert.ToDateTime(lbFechaVence.Text).ToShortDateString(), Convert.ToDecimal(lbPrecioOriginal.Text) > 0 ? string.Format("Precio de Oferta: Q {0}", String.Format("{0:0,0.00}", Convert.ToDecimal(lbPrecioOferta.Text))) : string.Format("{0}% desc. al contado y {1}% desc. al crédito.", lbDescuentoContado.Text.Replace(".0000", ""), lbDescuentoCredito.Text.Replace(".0000", "")), mCondiciones);
                    }
                    else
                    {
                        lbPrecio.ToolTip = string.Format("Este artículo está en OFERTA!!!{0}{3}{1}{0}La oferta vence el: {2}", Environment.NewLine, "", Convert.ToDateTime(lbFechaVence.Text).ToShortDateString(), Convert.ToDecimal(lbPrecioOriginal.Text) > 0 ? string.Format("Precio Original: Q {0}", String.Format("{0:0,0.00}", Convert.ToDecimal(lbPrecioOriginal.Text))) : string.Format("{0}% desc. al contado y {1}% desc. al crédito.", lbDescuentoContado.Text.Replace(".0000", ""), lbDescuentoCredito.Text.Replace(".0000", "")));
                    }
                }

                if (lbPromocion.Text == "S")
                    lbPrecio.ToolTip = string.Format("Este artículo está en PROMOCION!!!{0}{1}{0}{2}{0}{3}", Environment.NewLine, lbDescripcionPromocion.Text, lbObservacionesPromocion.Text, lbVencePromocion.Text == "N" ? "" : string.Format("La promoción vence el: {0}", Convert.ToDateTime(lbFechaVencePromocion.Text).ToShortDateString()));

                int mPagos = 0;
                string mPagosString = lbPagos.Text.Replace(" PAGOS", "");

                try
                {
                    mPagos = Convert.ToInt32(mPagosString);
                }
                catch
                {
                    // Nada
                }

                if (mPagos > 0)
                {
                    decimal mPrecio = Convert.ToDecimal(lbPrecio.Text);
                    gridArticulos.Rows[ii].Cells[3].ToolTip = string.Format("Cuotas de Q {0}", String.Format("{0:0,0.00}", mPrecio / mPagos));
                }
            }
        }

        void setToolTipsPromocion()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            for (int ii = 0; ii < gridArticulosPromocion.Rows.Count; ii++)
            {
                Label lbOferta = (Label)gridArticulosPromocion.Rows[ii].FindControl("lbOferta");
                Label lbPrecio = (Label)gridArticulosPromocion.Rows[ii].FindControl("lbPrecio");
                Label lbPrecioOriginal = (Label)gridArticulosPromocion.Rows[ii].FindControl("lbPrecioOriginal");
                Label lbFechaVence = (Label)gridArticulosPromocion.Rows[ii].FindControl("lbFechaVence");
                Label lbDescuentoContado = (Label)gridArticulosPromocion.Rows[ii].FindControl("lbDescuentoContado");
                Label lbDescuentoCredito = (Label)gridArticulosPromocion.Rows[ii].FindControl("lbDescuentoCredito");
                Label lbCondicional = (Label)gridArticulosPromocion.Rows[ii].FindControl("lbCondicional");
                Label lbPrecioOferta = (Label)gridArticulosPromocion.Rows[ii].FindControl("lbPrecioOferta");
                Label lbCondiciones = (Label)gridArticulosPromocion.Rows[ii].FindControl("lbCondiciones");
                Label lbPromocion = (Label)gridArticulosPromocion.Rows[ii].FindControl("lbPromocion");
                Label lbTipoPromocion = (Label)gridArticulosPromocion.Rows[ii].FindControl("lbTipoPromocion");
                Label lbValorPromocion = (Label)gridArticulosPromocion.Rows[ii].FindControl("lbValorPromocion");
                Label lbVencePromocion = (Label)gridArticulosPromocion.Rows[ii].FindControl("lbVencePromocion");
                Label lbFechaVencePromocion = (Label)gridArticulosPromocion.Rows[ii].FindControl("lbFechaVencePromocion");
                Label lbDescripcionPromocion = (Label)gridArticulosPromocion.Rows[ii].FindControl("lbDescripcionPromocion");
                Label lbObservacionesPromocion = (Label)gridArticulosPromocion.Rows[ii].FindControl("lbObservacionesPromocion");

                if (lbOferta.Text == "S")
                {
                    if (lbCondicional.Text == "S")
                    {
                        string mCondiciones = ws.DevuelveCondiciones(gridArticulosPromocion.Rows[ii].Cells[0].Text, Convert.ToString(Session["Tienda"]));
                        lbPrecio.ToolTip = string.Format("Este artículo está en OFERTA!!!{0}{3}{1}{0}La oferta vence el: {2}{0}La oferta aplica en la compra de:{0}{4}", Environment.NewLine, "", Convert.ToDateTime(lbFechaVence.Text).ToShortDateString(), Convert.ToDecimal(lbPrecioOriginal.Text) > 0 ? string.Format("Precio de Oferta: Q {0}", String.Format("{0:0,0.00}", Convert.ToDecimal(lbPrecioOferta.Text))) : string.Format("{0}% desc. al contado y {1}% desc. al crédito.", lbDescuentoContado.Text.Replace(".0000", ""), lbDescuentoCredito.Text.Replace(".0000", "")), mCondiciones);
                    }
                    else
                    {
                        lbPrecio.ToolTip = string.Format("Este artículo está en OFERTA!!!{0}{3}{1}{0}La oferta vence el: {2}", Environment.NewLine, "", Convert.ToDateTime(lbFechaVence.Text).ToShortDateString(), Convert.ToDecimal(lbPrecioOriginal.Text) > 0 ? string.Format("Precio Original: Q {0}", String.Format("{0:0,0.00}", Convert.ToDecimal(lbPrecioOriginal.Text))) : string.Format("{0}% desc. al contado y {1}% desc. al crédito.", lbDescuentoContado.Text.Replace(".0000", ""), lbDescuentoCredito.Text.Replace(".0000", "")));
                    }
                }

                if (lbPromocion.Text == "S")
                    lbPrecio.ToolTip = string.Format("Este artículo está en PROMOCION!!!{0}{1}{0}{2}{0}{3}", Environment.NewLine, lbDescripcionPromocion.Text, lbObservacionesPromocion.Text, lbVencePromocion.Text == "N" ? "" : string.Format("La promoción vence el: {0}", Convert.ToDateTime(lbFechaVencePromocion.Text).ToShortDateString()));
            }
        }

        protected void gridArticulos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }

            try
            {
                Label lbOferta = (Label)e.Row.FindControl("lbOferta");
                Label lbPromocion = (Label)e.Row.FindControl("lbPromocion");

                if (lbOferta.Text == "S")
                {
                    e.Row.ForeColor = System.Drawing.Color.FromArgb(227, 89, 4);
                    e.Row.Font.Bold = true;
                }

                if (lbPromocion.Text == "S")
                {
                    e.Row.ForeColor = System.Drawing.Color.FromArgb(165, 81, 41);
                    e.Row.Font.Bold = true;
                }
            }
            catch
            {
                //Nothing
            }
        }

        protected void gridOfertas_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridTiposVenta_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");

                Label lbVence = (Label)e.Row.FindControl("lbVence");
                Label lbFechaVence = (Label)e.Row.FindControl("lbFechaVence");

                if (lbVence.Text == "N") lbFechaVence.Text = "";
            }
        }

        protected void gridArticulosPromocion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }

            try
            {
                Label lbOferta = (Label)e.Row.FindControl("lbOferta");
                Label lbPromocion = (Label)e.Row.FindControl("lbPromocion");

                if (lbOferta.Text == "S")
                {
                    e.Row.ForeColor = System.Drawing.Color.FromArgb(227, 89, 4);
                    e.Row.Font.Bold = true;
                }

                if (lbPromocion.Text == "S")
                {
                    e.Row.ForeColor = System.Drawing.Color.FromArgb(165, 81, 41);
                    e.Row.Font.Bold = true;
                }
            }
            catch
            {
                //Nothing
            }
        }

    }
}