﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using System.Net;
using System.Net.Mail;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MF_Clases;
using static MF_Clases.Clases;
using System.Web.Configuration;
using System.Globalization;
using PuntoDeVenta.Util;
using RestSharp;
using System.Threading;
using MF_Clases.Restful;
using System.Text.RegularExpressions;
using MF_Clases.Facturacion;
using MF.Comun.Dto.Facturacion;
using MF_Clases.Comun;
using Microsoft.Build.Framework.XamlTypes;
using DevExpress.Office.Utils;
using DevExpress.Utils.MVVM;

namespace PuntoDeVenta
{
    public partial class pedidos : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {

                string strEnviarSolicitud = Request.QueryString["EnviarSol"];
                string strPedido = Request.QueryString["P"];
                if (strPedido != null)
                    Session["Pedido"] = strPedido;

                ///variable que contiene el resultado de la solicitud a CREDIPLUS
                string strRespuesta = Request.QueryString["ErrorMsg2"];
               
                ViewState["url"] = Convert.ToString(Session["Url"]);
                ViewState["urlPrecios"] = Convert.ToString(Session["UrlPrecios"]);

                List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();
                ViewState["qArticulos"] = q;

                CargarBodegas();
                CargarTiposFinancieras();
                CargarTiposDeServicio();
                CargarMedios();

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mCajeros = ws.DevuelveCajeros();
                string mCajerosCambianTienda = ws.DevuelveCajerosCambianTienda();

                //Pongo que todos los usuarios seleccionen el cajero
                cbVendedor.Visible = true;
                cbAceptaVendedor.Visible = true;

                AsignaTiendaCombo();
                if (Convert.ToString(Session["Tienda"]) != "")
                    if (ws.EsCiudad(Convert.ToString(Session["Tienda"])))
                    {
                        cbBodega.SelectedValue = "F01";
                    }
                    else
                    {
                        cbBodega.SelectedValue = Convert.ToString(Session["Tienda"]);
                    }

                txtProfesion.Text = "";
                ViewState["Accesorios"] = "N";

                if (Session["Cliente"] == null)
                {
                    if (General.Ambiente == "DES")
                    {
                        Session["Cliente"] = "0022730";
                    }
                    limpiar(true);
                    mostrarBusquedaCliente();
                }
                else
                {
                    CargarCliente("C", Convert.ToString(Session["Cliente"]), "I");

                    if (Request.QueryString["accion"] == "sol")
                    {
                        MostrarInfoSolicitud();
                        VerificarSolicitud();
                        AplicarSolicitud();
                    }
                }

                //Validando la respuesta de CREDIPLUS
                if (strRespuesta != null)
                {
                    Session["Data"] = null;
                    Session["delay"] = null;
                    if (strRespuesta.ToUpper().Contains("ERROR") || strRespuesta.ToUpper().Contains("PROBLEMA"))
                        lbError.Text = strRespuesta;
                    else
                    {
                        lbInfo.Text = "<div><b>" + strRespuesta + "</b><br/>";
                    }

                    string s = @"<script language=JavaScript>var clean_uri = location.protocol + '//' + location.host + location.pathname; window.history.replaceState({}, document.title, clean_uri);</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "test", s);

                }
                try
                {
                    //*-----------------------------------
                    //solo al momento del Page Load, 
                    //ya que en el método Limpiar que se llama al cambiar Financiera
                    // dejaba por defecto
                    //contado Efectivo, y no permitia elegir una financiera
                    //-------------------------------------
                    string mTienda = Convert.ToString(Session["Tienda"]);
                    if (mTienda == "F01")
                    {
                        cbAceptaVendedor.Checked = true;
                        cbOtro.Checked = true;
                        cbTipo.SelectedValue = "NR";
                        cbFinanciera.SelectedValue = "1";
                        CargaNivelesDePrecio();
                        cbNivelPrecio.SelectedValue = "ContadoEfect";
                    }
                }
                catch
                {
                    //Nothing
                }
            }
            else
            {
                if (ViewState["PostBack"] != null)
                {
                    switch (ViewState["PostBack"].ToString())
                    {
                        case "PuntosCanjear":
                            txtAutorizacionPuntos.Focus();
                            break;
                        default:
                            break;
                    }
                }
            }
            
            Page.ClientScript.RegisterStartupScript(
                typeof(pedidos),
                "ScriptDoFocus",
                SCRIPT_DOFOCUS.Replace("REQUEST_LASTFOCUS", Request["__LASTFOCUS"]),
                true);

        }

        private const string SCRIPT_DOFOCUS = @"window.setTimeout('DoFocus()', 1); function DoFocus() {
            try {
                document.getElementById('REQUEST_LASTFOCUS').focus();
            } catch (ex) {}
        }";

        private void HookOnFocus(Control CurrentControl)
        {
            //checks if control is one of TextBox, DropDownList, ListBox or Button
            if ((CurrentControl is TextBox) ||
                (CurrentControl is DropDownList) ||
                (CurrentControl is ListBox) ||
                (CurrentControl is Button))
                //adds a script which saves active control on receiving focus 
                //in the hidden field __LASTFOCUS.
                (CurrentControl as WebControl).Attributes.Add(
                   "onfocus",
                   "try{document.getElementById('__LASTFOCUS').value=this.id} catch(e) {}");
            //checks if the control has children
            if (CurrentControl.HasControls())
                //if yes do them all recursively
                foreach (Control CurrentChildControl in CurrentControl.Controls)
                    HookOnFocus(CurrentChildControl);
        }
        
        private void CargarTiposDeServicio()
        {

            try
            {
                WebRequest request = WebRequest.Create(string.Format("{0}/TipoServicio/{1}", Convert.ToString(Session["UrlRestServices"]), Const.CATALOGO.TIPO_DE_SERVICIO_ARMADO_PEDIDOS));
                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                if (response.StatusCode != HttpStatusCode.OK)
                {

                    lbError.Text = string.Format("Enviar soporte a Informática indicando sobre el error: {0}", "Error al cargar los tipos de servicio del catálgo.");
                    return;
                }
                else
                {
                    List<Catalogo> mListadoServicio = new List<Catalogo>();
                    mListadoServicio = JsonConvert.DeserializeObject<List<Catalogo>>(responseString);

                    foreach (var item in mListadoServicio)
                    {
                        ListItem oItem = new ListItem(item.Descripcion, item.Codigo);
                        cbTipoArmado.Items.Add(oItem);
                    }
                }
            }
            catch
            {
                lbError.Text = string.Format("Enviar soporte a Informática indicando sobre el error: {0}", "Error al cargar los tipos de servicio del catálgo.");
            }
        }

        void CargarBodegas()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveTiendas();

            cbBodega.DataSource = q;
            cbBodega.DataTextField = "Tienda";
            cbBodega.DataValueField = "Tienda";
            cbBodega.DataBind();

            cbTienda.DataSource = q;
            cbTienda.DataTextField = "Tienda";
            cbTienda.DataValueField = "Tienda";
            cbTienda.DataBind();
        }

        void AsignaTiendaCombo()
        {
            if (Convert.ToString(Session["NombreVendedor"]) == "Alerta")
            {
                lkGrabar.Visible = false;
                lkVale.Visible = false;
                lkSolicitar.Visible = false;
                cbTienda.SelectedValue = "F01";
                lbError.Text = "No tiene vendedor asignado, informar de inmediato al departamento de informática.";
            }
            else
            {
                cbVendedor.SelectedValue = null;
                string mTienda = Convert.ToString(Session["Tienda"]);
                cbTienda.SelectedValue = mTienda;
                CargaVendedores();
                cbVendedor.SelectedValue = Convert.ToString(Session["Vendedor"]);
            }
        }

        void CargarCliente(string tipo, string cliente, string nit)
        {
            try
            {
                limpiar(true);

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var q = ws.DevuelveCliente(tipo, cliente, nit);

                txtCodigo.Text = q[0].Cliente;
                txtNombre.Text = q[0].Nombre;
                txtNit.Text = q[0].Nit;

                WebRequest request = WebRequest.Create(string.Format("{0}/refacturacion/{1}", Convert.ToString(Session["UrlRestServices"]), q[0].Cliente));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                List<Clases.FacturasCliente> mFacturas = new List<Clases.FacturasCliente>();
                mFacturas = JsonConvert.DeserializeObject<List<Clases.FacturasCliente>>(responseString);

                cbRefacturacion.DataSource = mFacturas.AsEnumerable();
                cbRefacturacion.DataTextField = "Texto";
                cbRefacturacion.DataValueField = "Factura";
                cbRefacturacion.DataBind();

                Session["Cliente"] = q[0].Cliente;
                cbRefacturacion.SelectedValue = "N";

                if (Convert.ToString(Session["Tienda"]) == "F01")
                {
                    request = WebRequest.Create(string.Format("{0}/clientesdolares", Convert.ToString(Session["UrlRestServices"])));

                    request.Method = "GET";
                    request.ContentType = "application/json";

                    response = (HttpWebResponse)request.GetResponse();
                    responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    string mClientesDolares = Convert.ToString(responseString).Replace("\"", "");

                    if (mClientesDolares.Contains(q[0].Cliente))
                    {
                        request = WebRequest.Create(string.Format("{0}/vendedorexportaciones", Convert.ToString(Session["UrlRestServices"])));

                        request.Method = "GET";
                        request.ContentType = "application/json";

                        response = (HttpWebResponse)request.GetResponse();
                        responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                        string mVendedorExportaciones = Convert.ToString(responseString).Replace("\"", "");
                        cbVendedor.SelectedValue = mVendedorExportaciones;
                    }

                    txtNombreRecibe.Text = q[0].Nombre;
                }
                else
                {
                    cbVendedor.SelectedValue = Convert.ToString(Session["Vendedor"]);

                    try
                    {
                        cbTipo.SelectedValue = q[0].TipoVenta;
                    }
                    catch
                    {
                        cbTipo.SelectedValue = "NR";
                    }

                    cbFinanciera.SelectedValue = q[0].Financiera.ToString();
                    txtProfesion.Text = q[0].Profesion;
                    txtNombreRecibe.Text = string.Format("{0} {1}", q[0].PrimerNombre, q[0].PrimerApellido);

                    CargaNivelesDePrecio();
                    cbNivelPrecio.SelectedValue = ws.DevuelveNivelPadre(cbTipo.SelectedValue, q[0].Financiera, q[0].NivelPrecio);

                    ToolTipPromo();
                    ValidaBotonPromocion();
                }

                articulo.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    //Nothing
                }
                lbError.Text = string.Format("Error al cargar el cliente {0} {1}", ex.Message, m);
            }


        }

        void ValidarDatosCrediticios(string pedido)
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.ValidaDatosCrediticios("P", pedido))
                {
                    Session["TipoVenta"] = cbTipo.SelectedValue;
                    Session["Financiera"] = cbFinanciera.SelectedValue;
                    Session["NivelPrecio"] = cbNivelPrecio.SelectedValue;
                    Response.Redirect("clientes.aspx");
                }
                else
                {
                    Session["TipoVenta"] = null;
                    Session["Financiera"] = null;
                    Session["NivelPrecio"] = null;
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    //Nothing
                }
                lbError.Text = string.Format("Error al validar los datos crediticios {0} {1}", ex.Message, m);
            }
        }

        void CargarTiposFinancieras()
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var qTiposVenta = ws.DevuelveTiposVentaSeleccion(Convert.ToString(Session["Tienda"])).ToList();

                cbTipo.DataSource = qTiposVenta;
                cbTipo.DataTextField = "Descripcion";
                cbTipo.DataValueField = "CodigoTipoVenta";
                cbTipo.DataBind();

                cbTipo.SelectedValue = "NR";
                CargaFinancieras();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    //Nothing
                }
                lbError.Text = string.Format("Error al cargar las promociones {0} {1}", ex.Message, m);
            }
        }

        void CargaFinancieras()
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var qFinancieras = ws.DevuelveFinancierasSeleccion(cbTipo.SelectedValue).ToList();

                wsPuntoVenta.Financieras itemFinanciera = new wsPuntoVenta.Financieras();
                itemFinanciera.Financiera = 0;
                itemFinanciera.Nombre = "Seleccione financiera ...";
                itemFinanciera.prefijo = "";
                itemFinanciera.Estatus = "Activa";
                qFinancieras.Add(itemFinanciera);

                cbFinanciera.DataSource = qFinancieras;
                cbFinanciera.DataTextField = "Nombre";
                cbFinanciera.DataValueField = "Financiera";
                cbFinanciera.DataBind();

                cbFinanciera.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    //Nothing
                }
                lbError.Text = string.Format("Error al cargar las financieras {0} {1}", ex.Message, m);
            }
        }

        void CargaNivelesDePrecio()
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var qNivelesPrecio = ws.DevuelveNivelesPrecioSeleccionFinanciera(cbFinanciera.SelectedValue, Convert.ToString(Session["Tienda"])).ToList();

                wsPuntoVenta.NivelesPrecio itemNivel = new wsPuntoVenta.NivelesPrecio();
                itemNivel.NivelPrecio = "Seleccione ...";
                itemNivel.Factor = 0;
                itemNivel.TipoVenta = "";
                itemNivel.TipoDeVenta = "";
                itemNivel.Financiera = 0;
                itemNivel.NombreFinanciera = "";
                itemNivel.Estatus = "";
                itemNivel.Tipo = "";
                itemNivel.Pagos = "";
                qNivelesPrecio.Add(itemNivel);

                cbNivelPrecio.DataSource = qNivelesPrecio;
                cbNivelPrecio.DataTextField = "NivelPrecio";
                cbNivelPrecio.DataValueField = "NivelPrecio";
                cbNivelPrecio.DataBind();

                cbNivelPrecio.SelectedValue = "Seleccione ...";
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbError.Text = string.Format("Error al cargar los niveles de precio {0} {1}", ex.Message, m);
            }
        }

        void limpiar(bool nuevo)
        {
            try
            {
                lstVales.Items.Clear();
                ViewState["Accesorios"] = "N";
                Session["lstNuevoPrecioProducto"] = null;
                string mTienda = Convert.ToString(Session["Tienda"]);
                cbTienda.SelectedValue = mTienda;
                txtDescuentoItem.Text = "0.00";
                txtDescMax.Text = "0.00";
                txtSaldoDescuento.Text = "0.00";
                CargaVendedores();
                Session["lstNuevoPrecioProducto"] = null;
                lbError.Text = "";
                lbError2.Text = "";
                lblInfoVales.Text = "";
                lblErrorVales.Text = "";
                lbError2.Visible = false;
                lbInfo.Text = "";
                lbLineaArticulo.Text = "";
                lkCanjeVales.Visible = true;
                btnActualizarEntrega.Visible = false;
                txtDescuentos.Text = "0.00";
                txtDescuentosVales2.Text = "0.00";
                tblVale.Visible = false;
                tblFacturaPuntos.Visible = false;
                txtCodVale.Text = "";
                txtAutorizacionPuntos.Text = "";
                txtValorVale.Text = "";
                txtNumeroFacturaPuntos.Text = "";
                cbNumeroFacturaPuntos.Checked = false;
                txtDescuentosVales.Text = "0";
                txtDescuentosVales.ReadOnly = false;
                tblRegalo.Visible = false;
                lbEstadoSolicitud.Visible = false;
                lkSolicitudEnLinea.Visible = false;
                lkPagareEnLinea.Visible = false;
                trGrabarPedido.Visible = true;
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                cbAceptaVendedor.Checked = false;
                cbTipo.Enabled = true;
                cbFinanciera.Enabled = true;
                cbNivelPrecio.Enabled = true;
                articulo.ReadOnly = false;
                descripcion.ReadOnly = true;
                txtPrecioUnitario.ReadOnly = true;
                txtPrecioUnitario.AutoPostBack = false;
                cbRequisicion.Enabled = true;
                cbLocalizacion.Enabled = true;

                cbGel.SelectedValue = "No";
                cbRequisicion.SelectedValue = "No";
                cbLocalizacion.SelectedValue = "ARMADO";

                if (nuevo)
                {
                    cbTipoPedido.SelectedValue = "N";
                    txtPedido.Text = "P000000";
                    txtFecha.Text = DateTime.Now.Date.ToShortDateString();
                }

                txtValor.Text = "0.00";
                txtIntereses.Text = "0.00";
                txtFacturar.Text = "0.00";
                txtFacturarItem.Text = "0.00";
                txtPrecioUnitarioDebioFacturar.Text = "0.00";
                txtDescuentos.Text = "0.00";
                articulo.Text = "";
                descripcion.Text = "";
                txtCantidad.Text = "1";
                txtPrecioUnitario.Text = "0.00";
                txtTotal.Text = "0.00";
                txtDescuentoItem.Text = "0.00";
                txtOferta.Text = "N";
                txtFechaOfertaDesde.Text = new DateTime(1980, 1, 1).ToShortDateString();
                txtPrecioOriginal.Text = "0.00";
                txtTipoOferta.Text = "N";
                txtVale1.Text = "0";
                txtCotizacion.Text = "0";
                txtCondicional.Text = "N";
                txtArticuloCondicion.Text = "";
                txtAutorizacion.Text = "0";
                txtCantidad.ReadOnly = false;

                txtEnganche.Text = "0.00";
                txtSaldoFinanciar.Text = "0.00";
                txtMonto.Text = "0.00";

                cbPagos1.SelectedValue = "01";
                cbPagos2.SelectedValue = "01";
                txtPagos1.Text = "0.00";
                txtPagos2.Text = "0.00";

                if (nuevo)
                {
                    txtPagare.Text = "";
                    txtSolicitud.Text = "";
                    txtQuienAutorizo.Text = "";
                    txtNoAutorizacion.Text = "";
                    txtGarantia.Text = "";

                    txtObservaciones.Text = "";
                    txtNotasTipoVenta.Text = "";
                    txtNotas.Text = "";

                    cbPrensa.Checked = false;
                    cbRadio.Checked = false;
                    cbInternet.Checked = false;
                    cbVolante.Checked = false;
                    cbTelevision.Checked = false;
                    cbQuetzalteco.Checked = false;
                    cbNuestroDiario.Checked = false;
                    cbOtro.Checked = false;
                    txtOtro.Text = "";

                    cbMedio.SelectedValue = "0";
                    if (Convert.ToString(Session["Tienda"]) == "F01") cbMedio.SelectedValue = "1";
                }

                txtNumeroFactura.Text = "";
                cbNumeroFactura.Checked = false;
                tblFactura.Visible = false;

                lkFacturar.Enabled = false;
                lkDespachar.Enabled = false;

                lkRefacturacion.Visible = false;
                cbRefacturacion.SelectedValue = "N";

                cbLocalF09.Visible = false;
                cbLocalF09.SelectedValue = "N";

                if (Convert.ToString(Session["Tienda"]) == "F05")
                {
                    cbLocalF09.Visible = true;
                    cbLocalF09.SelectedValue = "S";
                }

                txtFacturado.Text = "N";
                txtDespachado.Text = "N";
                txtNumeroDespacho.Text = "";
                cbNumeroDespacho.Checked = false;
                txtObservacionesDespacho.Text = "";
                tblDespacho.Visible = false;

                tblBuscar.Visible = false;
                gridClientes.Visible = false;
                gridLocalizaciones.Visible = false;

                List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();
                ViewState["qArticulos"] = q;

                gridArticulos.DataSource = q;
                gridArticulos.DataBind();

                gridVales.Visible = false;
                gridPedidos.Visible = false;
                gridCotizaciones.Visible = false;

                tablaBusquedaDet.Visible = false;
                gridArticulosDet.Visible = false;
                tblSolicutudAutorizacion.Visible = false;
                liEnviarExpediente.Visible = false;
                liReqArmados.Visible = false;
                liGarantias.Visible = false;

                tblInfo.Visible = false;
                lkGrabar.Enabled = true;
                lkGrabar.Visible = true;
                lkVale.Visible = false;
                lkSolicitar.Visible = false;

                var qParametrosPrecios = ws.ParametrosModificarPrecios();

                if (qParametrosPrecios[0].CambiarPrecioHaciaAbajo == "S" || qParametrosPrecios[0].CambiarPrecioHaciaArriba == "S")
                {
                    txtPrecioUnitario.ReadOnly = false;
                    txtPrecioUnitario.AutoPostBack = true;
                }
                else
                {
                    txtPrecioUnitario.ReadOnly = true;
                    txtPrecioUnitario.AutoPostBack = false;
                }

                if (cbNivelPrecio.Text.Trim().Length > 0 && cbNivelPrecio.Text != "Seleccione ...")
                {
                    double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                    cbNivelPrecio.ToolTip = string.Format("Factor: {0}", String.Format("{0:0.000000}", mFactor));

                    articulo.Focus();
                }
                else
                {
                    if (cbNivelPrecio.Text == "Seleccione ...")
                    {
                        cbNivelPrecio.Focus();
                    }
                    else
                    {
                        cbTipo.Focus();
                    }
                }

                DateTime mPrimerPago = DateTime.Now.Date.AddDays(30);
                cbDiaPrimerPago.SelectedValue = mPrimerPago.Day.ToString();
                cbMesPrimerPago.SelectedValue = mPrimerPago.Month.ToString();
                txtAnioPrimerPago.Text = mPrimerPago.Year.ToString();

                lbError2.Text = "";
                lbError2.Visible = false;

                Label6.Text = "";
                Label7.Text = "";

                WebRequest request = WebRequest.Create(string.Format("{0}/bodegadefault/{1}", Convert.ToString(Session["UrlRestServices"]), Convert.ToString(Session["Tienda"])));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string mBodegaDefault = Convert.ToString(responseString).Replace("\"", "");
                cbBodega.SelectedValue = mBodegaDefault;
                ViewState["BodegaDefault"] = mBodegaDefault;

                int mDias = 1;
                if (mTienda != mBodegaDefault) mDias = 2;
                DateTime mFechaValidar = DateTime.Now.Date.AddDays(mDias);

                cbDia.SelectedValue = mFechaValidar.Day.ToString();
                cbMes.SelectedValue = mFechaValidar.Month.ToString();
                txtAnio.Text = mFechaValidar.Year.ToString();
                cbEntrega.SelectedValue = "AM";

                if (mTienda == "F01")
                {
                    request = WebRequest.Create(string.Format("{0}/vendedormayoreo", Convert.ToString(Session["UrlRestServices"])));

                    request.Method = "GET";
                    request.ContentType = "application/json";

                    response = (HttpWebResponse)request.GetResponse();
                    responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    string mVendedorMayoreo = Convert.ToString(responseString).Replace("\"", "");

                    try
                    {
                        cbVendedor.SelectedValue = mVendedorMayoreo;
                        cbAceptaVendedor.Checked = true;
                        cbOtro.Checked = true;

                        cbTipo.SelectedValue = "NR";
                    }
                    catch
                    {
                        //Nothing
                    }
                }
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al limpiar la página {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }

        protected void lbBuscarCliente_Click(object sender, EventArgs e)
        {
            mostrarBusquedaCliente();
        }

        void mostrarBusquedaCliente()
        {
            tblBuscar.Visible = true;
            gridArticulosDet.Visible = false;
            tablaBusquedaDet.Visible = false;
            tblSolicutudAutorizacion.Visible = false;

            lbError.Text = "";
            lbError2.Text = "";
            lbError2.Visible = false;
            txtCodigoBuscar.Text = "";
            txtNombreBuscar.Text = "";
            txtNitBuscar.Text = "";

            txtNombreBuscar.Focus();
        }

        protected void cbFinanciera_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaNivelesDePrecio();
            limpiar(true);
        }

        protected void lkOcultarBusqueda_Click(object sender, EventArgs e)
        {
            tblBuscar.Visible = false;
            gridClientes.Visible = false;
            cbTipo.Focus();
        }

        protected void txtCodigoBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtCodigoBuscar.Text.Trim().Length == 0) return;
            BuscarCliente("C");
        }

        protected void txtNombreBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtNombreBuscar.Text.Trim().Length == 0) return;

            string mTipo = "N";
            try
            {
                Int32 mNumero = Convert.ToInt32(txtNombreBuscar.Text.Substring(0, 1));
                mTipo = "C";
            }
            catch
            {
                mTipo = "N";
            }

            BuscarCliente(mTipo);
        }

        protected void txtNitBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtNitBuscar.Text.Trim().Length == 0) return;
            BuscarCliente("T");
        }

        protected void txtTelefono_TextChanged(object sender, EventArgs e)
        {
            if (txtTelefono.Text.Trim().Length == 0) return;
            BuscarCliente("E");
        }

        void BuscarCliente(string tipo)
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            string mCodigo = txtCodigoBuscar.Text.Trim();
            string mNombre = txtNombreBuscar.Text.Trim().ToUpper();

            if (tipo == "C" && mNombre.Trim().Length > 0)
            {
                mCodigo = mNombre;
                mNombre = "";
            }

            var q = ws.DevuelveClientes(tipo, mCodigo, mNombre, txtNitBuscar.Text.Trim().ToUpper(), txtTelefono.Text.Trim());

            gridClientes.DataSource = q;
            gridClientes.DataBind();
            gridClientes.Visible = true;
            gridClientes.SelectedIndex = -1;

            lbError.Text = "";
            lbError2.Text = "";
            lbError2.Visible = false;

            if (q.Length == 0) lbError.Text = "No existen clientes con el criterio de búsqueda ingresado.";

            if (q.Count() == 1)
            {
                gridClientes.SelectedIndex = 0;
                seleccionarCliente();
            }
            if (q.Count() > 0)
            {
                gridClientes.Focus();
                gridClientes.SelectedIndex = 0;
                LinkButton lkSeleccionar = (LinkButton)gridClientes.SelectedRow.FindControl("lkSeleccionar");

                lkSeleccionar.Focus();
            }
        }

        void seleccionarCliente()
        {
            GridViewRow gvr = gridClientes.SelectedRow;
            CargarCliente("C", gvr.Cells[1].Text, "I");
        }

        protected void gridClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            seleccionarCliente();
        }

        protected void lkSeleccionar_Click(object sender, EventArgs e)
        {

        }

        bool pasaValidaciones()
        {
            try
            {
                if (txtCodigo.Text.Trim().Length == 0)
                {
                    lbError.Text = "Debe seleccionar un cliente.";
                    mostrarBusquedaCliente();
                    return false;
                }

                if (cbVendedor.Text.Trim().Length == 0)
                {
                    lbError.Text = "Debe seleccionar el vendedor.";
                    cbVendedor.Focus();
                    return false;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                //Pongo que todos los usuarios seleccionen el cajero
                if (!cbAceptaVendedor.Checked)
                {
                    lbError.Text = "Debe aceptar el vendedor seleccionado";
                    cbAceptaVendedor.Focus();
                    return false;
                }

                if (cbLocalF09.Visible)
                {
                    if (cbLocalF09.SelectedValue == "S")
                    {
                        lbError.Text = "Debe seleccionar el local desde donde está vendiendo.";
                        cbLocalF09.Focus();
                        return false;
                    }
                }

                if (gridArticulos.Rows.Count == 0)
                {
                    lbError.Text = "El pedido debe contener al menos un artículo.";
                    articulo.Focus();
                    return false;
                }

                decimal mPago1, mPago2;

                try
                {
                    mPago1 = Convert.ToDecimal(txtPagos1.Text);
                }
                catch
                {
                    lbError.Text = "El monto de los pagos es inválido.";
                    txtPagos1.Focus();
                    return false;
                }
                if (mPago1 < 0)
                {
                    lbError.Text = "El monto de los pagos es inválido.";
                    txtPagos1.Focus();
                    return false;
                }

                try
                {
                    mPago2 = Convert.ToDecimal(txtPagos2.Text);
                }
                catch
                {
                    lbError.Text = "El monto de los pagos es inválido.";
                    txtPagos2.Focus();
                    return false;
                }
                if (mPago1 < 0)
                {
                    lbError.Text = "El monto de los pagos es inválido.";
                    txtPagos2.Focus();
                    return false;
                }

                string mTipo = ws.DevuelveTipoNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                if (mTipo == "CR")
                {
                    int mPagos1 = Convert.ToInt32(cbPagos1.SelectedValue);
                    int mPagos2 = Convert.ToInt32(cbPagos2.SelectedValue);

                    decimal mMonto = Convert.ToDecimal(txtMonto.Text);

                    if (mMonto != ((mPagos1 * mPago1) + (mPagos2 * mPago2)))
                    {
                        lbError.Text = "El valor de las pagos no coincide con el monto del pedido.";
                        txtPagos1.Focus();
                        return false;
                    }
                }

                var q = ws.DevuelveTipoVenta(cbTipo.SelectedValue);

                if (q[0].MensajeValidacion.Trim().Length > 0)
                {
                    if (txtNotasTipoVenta.Text.Trim().Length == 0)
                    {
                        lbError.Text = string.Format("Debe ingresar la información requerida de {0}", q[0].TextoPromocion);
                        txtNotasTipoVenta.Focus();
                        return false;
                    }
                    if (txtNotasTipoVenta.Text.Trim().Length < 5)
                    {
                        lbError.Text = q[0].MensajeValidacion;
                        txtNotasTipoVenta.Focus();
                        return false;
                    }
                    if (txtNotasTipoVenta.Text.Trim().ToUpper() == "NINGUNA")
                    {
                        lbError.Text = q[0].MensajeValidacion;
                        txtNotasTipoVenta.Focus();
                        return false;
                    }
                }

                if (txtOtro.Text.Trim().Length > 0) cbOtro.Checked = true;

                if (cbTipoPedido.SelectedValue == "N")
                {
                    //***** Se cambia por combo de Medios
                    //if (!cbRadio.Checked && !cbInternet.Checked && !cbVolante.Checked && !cbPrensa.Checked && !cbQuetzalteco.Checked && !cbNuestroDiario.Checked && !cbTelevision.Checked && !cbOtro.Checked)
                    //{
                    //    lbError.Text = "Debe seleccionar al menos una opción de donde escucho el cliente acerca de nuestros productos.";
                    //    cbRadio.Focus();
                    //    return false;
                    //}

                    if (cbMedio.SelectedValue.ToString() == "0")
                    {
                        lbError.Text = "Debe seleccionar el medio por el cuál el cliente se enteró de nuestros productos";
                        cbMedio.Focus();
                        return false;
                    }
                }

                if (txtNombreRecibe.Text.Trim().Length == 0)
                {
                    lbError.Text = "Debe ingresar el nombre de la persona que recibirá la mercadería";
                    txtNombreRecibe.Focus();
                    return false;
                }


                DateTime mFechaEntrega = DateTime.Now.Date;

                try
                {
                    if (txtAnio.Text.Trim().Length == 2) txtAnio.Text = string.Format("20{0}", txtAnio.Text);
                    mFechaEntrega = new DateTime(Convert.ToInt32(txtAnio.Text), Convert.ToInt32(cbMes.SelectedValue), Convert.ToInt32(cbDia.SelectedValue));
                }
                catch
                {
                    lbError.Text = "Debe ingresar una fecha de entrega válida.";
                    cbDia.Focus();
                    return false;
                }

                if (mFechaEntrega < DateTime.Now.Date)
                {
                    lbError.Text = "La fecha de entrega ingresada es inválida.";
                    cbDia.Focus();
                    return false;
                }

                bool mEsDeAutorizacion = false;
                for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                {
                    Label lbAutorizacion = (Label)gridArticulos.Rows[ii].FindControl("lbAutorizacion");
                    if (lbAutorizacion.Text != "0") mEsDeAutorizacion = true;
                }

                if (cbTipo.SelectedValue == "NR" && !mEsDeAutorizacion)
                {
                    if (tblRegalo.Visible)
                    {
                        if (!rbAcepto.Checked && !rbNoAcepto.Checked)
                        {
                            lbError.Text = "Debe marcar la casilla para confirmar si acepta el regalo y luego hacer clic de nuevo en 'Guardar'";
                            rbAcepto.Focus();
                            return false;
                        }
                    }
                    else
                    {
                        txtArticuloRegalo.Text = "";
                        txtDescripcionRegalo.Text = "";
                        rbAcepto.Checked = false;
                        rbNoAcepto.Checked = false;
                        tblRegalo.Visible = false;

                        var qRegalo = ws.DevuelveArticulosRegalo(cbTipo.SelectedValue, cbFinanciera.SelectedValue, Convert.ToString(Session["Tienda"]));
                        decimal mValorValidar = 0; decimal mSaldoFinanciar = 0;

                        try
                        {
                            mSaldoFinanciar = Convert.ToDecimal(txtSaldoFinanciar.Text.Replace(",", ""));
                        }
                        catch
                        {
                            mSaldoFinanciar = 0;
                        }

                        for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                        {
                            Label lbOferta = (Label)gridArticulos.Rows[ii].FindControl("lbOferta");
                            Label lbArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbArticulo");
                            Label lbNombreArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbNombreArticulo");

                            string mIncluyeProductoOfertado = "N";

                            for (int jj = 0; jj < qRegalo.Length; jj++)
                            {
                                mIncluyeProductoOfertado = qRegalo[jj].IncluyeProductoOfertado;
                            }

                            if (mIncluyeProductoOfertado == "S")
                            {
                                if (!lbNombreArticulo.Text.ToUpper().Contains("REGALO")) mValorValidar += ws.DevuelvePrecioLista(lbArticulo.Text, Convert.ToString(Session["Tienda"]));
                            }
                            else
                            {
                                if (lbOferta.Text == "N" && !lbNombreArticulo.Text.ToUpper().Contains("REGALO")) mValorValidar += ws.DevuelvePrecioLista(lbArticulo.Text, Convert.ToString(Session["Tienda"]));
                            }

                            if (lbNombreArticulo.Text.ToUpper().Contains("LUXUR") && ws.PelotaEnCamasLuxurious()) mValorValidar += ws.DevuelvePrecioLista(lbArticulo.Text, Convert.ToString(Session["Tienda"]));

                            for (int jj = 0; jj < qRegalo.Length; jj++)
                            {
                                if (lbArticulo.Text == qRegalo[jj].Articulo) mValorValidar = 0;
                            }

                        }

                        if (mValorValidar > 0)
                        {
                            bool mAgregarRegalo = false;
                            string mArticuloRegalo = ""; string mArticuloRegaloDescripcion = "";

                            for (int jj = 0; jj < qRegalo.Length; jj++)
                            {
                                decimal mValorValidarRegalo = mValorValidar;
                                if (qRegalo[jj].TipoValidacion == "SF") mValorValidarRegalo = mSaldoFinanciar;

                                if (qRegalo[jj].Tipo == "T")
                                {
                                    var qArticulosCondicion = ws.DevuelveArticulosRegaloCondicion(cbTipo.SelectedValue, qRegalo[jj].Articulo);

                                    foreach (var articuloCondicion in qArticulosCondicion)
                                    {
                                        for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                                        {
                                            Label lbArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbArticulo");

                                            if (articuloCondicion.Articulo == lbArticulo.Text)
                                            {
                                                mAgregarRegalo = true;

                                                mArticuloRegalo = qRegalo[jj].Articulo;
                                                mArticuloRegaloDescripcion = qRegalo[jj].Descripcion;
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    if (mValorValidarRegalo >= qRegalo[jj].CompraMinimaNR)
                                    {
                                        mAgregarRegalo = true;

                                        mArticuloRegalo = qRegalo[jj].Articulo;
                                        mArticuloRegaloDescripcion = qRegalo[jj].Descripcion;
                                    }
                                }

                                if (mAgregarRegalo)
                                {
                                    txtArticuloRegalo.Text = mArticuloRegalo;
                                    txtDescripcionRegalo.Text = mArticuloRegaloDescripcion;

                                    tblRegalo.Visible = true;
                                    lbError.Text = "Debe confirmar si acepta el regalo y luego hacer clic de nuevo en 'Guardar'";
                                    rbAcepto.Focus();
                                    return false;
                                }
                            }
                        }

                    }
                }

                var qNiveles = ws.DevuelveTipoVentaNiveles(cbTipo.SelectedValue);

                if (qNiveles.Length > 0)
                {
                    bool mNivelValido = false;
                    for (int ii = 0; ii < qNiveles.Length; ii++)
                    {
                        if (qNiveles[ii].Nivel == cbNivelPrecio.SelectedValue) mNivelValido = true;
                    }

                    if (!mNivelValido)
                    {
                        lbError.Text = string.Format("El nivel {0} no es válido para {1}", cbNivelPrecio.Text, lkLaTorre.Text);
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbError.Text = string.Format("Error al validar los datos {0} {1}", ex.Message, m);
                return false;
            }
        }

        bool VerificaMontoInterconsumo()
        {
            try
            {
                if (cbFinanciera.SelectedValue != "7") return true;

                decimal mEnganche = 0;
                decimal mFacturar = Convert.ToDecimal(txtFacturar.Text.Replace(",", ""));
                decimal mSaldoFinanciar = Convert.ToDecimal(txtSaldoFinanciar.Text.Replace(",", ""));

                try
                {
                    mEnganche = Convert.ToDecimal(txtEnganche.Text.Replace(",", ""));
                }
                catch
                {
                    mEnganche = 0;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                decimal mMontoMinimo = ws.MontoMinimoInterconsumo();

                if (mSaldoFinanciar < mMontoMinimo)
                {
                    if (mEnganche > 0)
                    {
                        lbError.Text = string.Format("El saldo a financiar en Interconsumo debe ser mayor o igual a Q{0}, debe modificar el enganche", String.Format("{0:0,0.00}", mMontoMinimo));
                    }
                    else
                    {
                        lbError.Text = string.Format("El saldo a financiar en Interconsumo debe ser mayor o igual a Q{0}", String.Format("{0:0,0.00}", mMontoMinimo));
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al validar el monto mínimo de Interconsumo. {0}", ex.Message);
                return false;
            }

            return true;
        }

        void AgregaRegalo()
        {
            try
            {
                if (txtArticuloRegalo.Text.Trim().Length == 0) return;

                if (rbAcepto.Checked)
                {
                    List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();
                    q = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                    DateTime mFechaOfertaDesde = new DateTime(1980, 1, 1);
                    Label lbBodega = (Label)gridArticulos.Rows[0].FindControl("lbBodega");

                    wsPuntoVenta.PedidoLinea item = new wsPuntoVenta.PedidoLinea();
                    item.Articulo = txtArticuloRegalo.Text;
                    item.Nombre = txtDescripcionRegalo.Text;
                    item.PrecioUnitario = 0;
                    item.CantidadPedida = 1;
                    item.PrecioTotal = 0;
                    item.Bodega = lbBodega.Text;
                    item.Localizacion = "ARMADO";
                    item.RequisicionArmado = "No";
                    item.Descripcion = "";
                    item.Comentario = "";
                    item.Estado = "N";
                    item.NumeroPedido = "";
                    item.Oferta = "N";
                    item.FechaOfertaDesde = mFechaOfertaDesde;
                    item.PrecioOriginal = 0;
                    item.TipoOferta = "N";
                    item.EsDetalleKit = "N";
                    item.PrecioFacturar = 0;
                    item.PrecioUnitarioDebioFacturar = 0;
                    item.PrecioSugerido = 0;
                    item.Vale = 0;
                    item.Condicional = "N";
                    item.ArticuloCondicion = "";
                    item.Autorizacion = 0;
                    q.Add(item);

                    gridArticulos.DataSource = q;
                    gridArticulos.DataBind();
                    gridArticulos.Visible = true;

                    ViewState["qArticulos"] = q;
                }
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al agregar el regalo. {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
                return;
            }
        }

        protected void lkGrabar_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";
                lbError2.Text = "";
                lbError2.Visible = false;

                if (!pasaValidaciones()) return;
                AgregaRegalo();

                lbInfo.Text = "";
                lbError.Text = "";
                lbError2.Text = "";
                lbError2.Visible = false;

                tblBuscar.Visible = false;
                tablaBusquedaDet.Visible = false;

                gridClientes.Visible = false;
                gridArticulosDet.Visible = false;

                string mMensaje = ""; string mPedido = txtPedido.Text; string mReferencia = "";
                decimal mTotalFacturar, mEnganche, mSaldoFinanciar, mRecargos, mMonto, mMontoPagos1, mMontoPagos2, mPrecioOriginal;

                try
                {
                    mTotalFacturar = Convert.ToDecimal(txtFacturar.Text.Replace(",", ""));
                }
                catch
                {
                    mTotalFacturar = 0;
                }
                try
                {
                    mEnganche = Convert.ToDecimal(txtEnganche.Text.Replace(",", ""));
                }
                catch
                {
                    mEnganche = 0;
                }
                try
                {
                    mSaldoFinanciar = Convert.ToDecimal(txtSaldoFinanciar.Text.Replace(",", ""));
                }
                catch
                {
                    mSaldoFinanciar = 0;
                }
                try
                {
                    mRecargos = Convert.ToDecimal(txtIntereses.Text.Replace(",", ""));
                }
                catch
                {
                    mRecargos = 0;
                }
                try
                {
                    mMonto = Convert.ToDecimal(txtMonto.Text.Replace(",", ""));
                }
                catch
                {
                    mMonto = 0;
                }
                try
                {
                    mMontoPagos1 = Convert.ToDecimal(txtPagos1.Text.Replace(",", ""));
                }
                catch
                {
                    mMontoPagos1 = 0;
                }
                try
                {
                    mMontoPagos2 = Convert.ToDecimal(txtPagos2.Text.Replace(",", ""));
                }
                catch
                {
                    mMontoPagos2 = 0;
                }

                //*****Se cambia por combo de medios
                //if (txtOtro.Text.Trim().Length > 0) cbOtro.Checked = true;
                //if (cbRadio.Checked) mReferencia = "R";
                //if (cbInternet.Checked) mReferencia = string.Format("{0}I", mReferencia);
                //if (cbVolante.Checked) mReferencia = string.Format("{0}V", mReferencia);
                //if (cbPrensa.Checked) mReferencia = string.Format("{0}P", mReferencia);
                //if (cbQuetzalteco.Checked) mReferencia = string.Format("{0}Q", mReferencia);
                //if (cbNuestroDiario.Checked) mReferencia = string.Format("{0}N", mReferencia);
                //if (cbTelevision.Checked) mReferencia = string.Format("{0}W", mReferencia);
                //if (cbOtro.Checked) mReferencia = string.Format("{0}O", mReferencia);

                mReferencia = cbMedio.SelectedValue.ToString();
                decimal DescVales = 0;
                try {
                    DescVales = decimal.Parse(txtDescuentosVales.Text.Replace(",", "").Replace("Q",""));
                } catch
                {
                    DescVales = 0;
                }
                DateTime mFechaEntrega = new DateTime(1980, 1, 1);

                try
                {
                    if (txtAnio.Text.Trim().Length == 2) txtAnio.Text = string.Format("20{0}", txtAnio.Text);
                    mFechaEntrega = new DateTime(Convert.ToInt32(txtAnio.Text), Convert.ToInt32(cbMes.SelectedValue), Convert.ToInt32(cbDia.SelectedValue));
                }
                catch
                {
                    mFechaEntrega = new DateTime(1980, 1, 1);
                }
                

                DateTime mPrimerPago = new DateTime(1980, 1, 1);

                try
                {
                    if (txtAnioPrimerPago.Text.Trim().Length == 2) txtAnioPrimerPago.Text = string.Format("20{0}", txtAnio.Text);
                    mPrimerPago = new DateTime(Convert.ToInt32(txtAnioPrimerPago.Text), Convert.ToInt32(cbMesPrimerPago.SelectedValue), Convert.ToInt32(cbDiaPrimerPago.SelectedValue));
                }
                catch
                {
                    mPrimerPago = new DateTime(1980, 1, 1);
                }

                if (mPrimerPago < DateTime.Now.Date)
                {
                    lbError.Text = "La fecha del primer pago es inválida";
                    return;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                wsPuntoVenta.Pedido[] q = new wsPuntoVenta.Pedido[1];
                wsPuntoVenta.PedidoLinea[] qLinea = new wsPuntoVenta.PedidoLinea[1];

                wsPuntoVenta.Pedido itemPedido = new wsPuntoVenta.Pedido();
                itemPedido.NumeroPedido = txtPedido.Text;
                itemPedido.Cliente = txtCodigo.Text;
                itemPedido.Fecha = DateTime.Now.Date;
                itemPedido.Garantia = txtGarantia.Text.ToUpper();
                itemPedido.Observaciones = txtObservaciones.Text.ToUpper();
                itemPedido.ObservacionesTipoVenta = txtNotasTipoVenta.Text.ToUpper();
                itemPedido.TotalFacturar = mTotalFacturar;
                itemPedido.Enganche = mEnganche;
                itemPedido.SaldoFinanciar = mSaldoFinanciar;
                itemPedido.Recargos = mRecargos;
                itemPedido.Monto = Math.Round(mMonto, 2, MidpointRounding.AwayFromZero);
                itemPedido.TipoVenta = cbTipo.SelectedValue;
                itemPedido.Financiera = Convert.ToInt32(cbFinanciera.SelectedValue);
                itemPedido.NivelPrecio = cbNivelPrecio.SelectedValue;
                itemPedido.Tienda = cbTienda.SelectedValue;
                itemPedido.Vendedor = cbVendedor.SelectedValue;
                itemPedido.Bodega = cbBodega.SelectedValue;
                itemPedido.NombreCliente = txtNombre.Text;
                itemPedido.EntregaAMPM = cbEntrega.SelectedValue;
                itemPedido.MercaderiaSale = cbBodega.SelectedValue;
                itemPedido.Desarmarla = "N";
                itemPedido.NotasTipoVenta = txtNotasTipoVenta.Text.ToUpper();
                itemPedido.Pagare = txtPagare.Text.ToUpper();
                itemPedido.NombreAutorizacion = txtQuienAutorizo.Text.ToUpper();
                itemPedido.Autorizacion = txtNoAutorizacion.Text.ToUpper();
                itemPedido.Solicitud = txtSolicitud.Text.ToUpper();
                itemPedido.CantidadPagos1 = Convert.ToInt32(cbPagos1.SelectedValue);
                itemPedido.MontoPagos1 = mMontoPagos1;
                itemPedido.CantidadPagos2 = Convert.ToInt32(cbPagos2.SelectedValue);
                itemPedido.MontoPagos2 = mMontoPagos2;
                itemPedido.TipoReferencia = mReferencia;
                itemPedido.ObservacionesReferencia = txtOtro.Text.ToUpper();
                itemPedido.TipoPedido = cbTipoPedido.SelectedValue;
                itemPedido.Cotizacion = Convert.ToInt32(txtCotizacion.Text);
                itemPedido.EntregaAMPM = cbEntrega.SelectedValue;
                itemPedido.NombreRecibe = txtNombreRecibe.Text.ToUpper();
                itemPedido.FechaEntrega = mFechaEntrega;
                itemPedido.Puntos = "";
                itemPedido.AutorizacionPuntos = txtAutorizacionPuntos.Text.Trim();
                itemPedido.LocalF09 = cbLocalF09.SelectedValue.ToString();
                itemPedido.Refacturacion = cbRefacturacion.SelectedValue.ToString();
                itemPedido.PrimerPago = mPrimerPago;
                itemPedido.DescuentoAccesorios = Convert.ToString(ViewState["Accesorios"]);
                itemPedido.DescuentoVales = DescVales;

                decimal descTotal = 0;
                int jj = 0;
                for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                {
                    jj++;

                    Label lbPrecioUnitario = (Label)gridArticulos.Rows[ii].FindControl("lbPrecio");
                    Label lbCantidadPedida = (Label)gridArticulos.Rows[ii].FindControl("lbCantidad");
                    Label lbPrecioTotal = (Label)gridArticulos.Rows[ii].FindControl("lbTotal");
                    Label lbDescripcion = (Label)gridArticulos.Rows[ii].FindControl("lbDescripcion");
                    Label lbComentario = (Label)gridArticulos.Rows[ii].FindControl("lbComentario");
                    Label lbEstado = (Label)gridArticulos.Rows[ii].FindControl("lbEstado");
                    Label lbNumeroPedido = (Label)gridArticulos.Rows[ii].FindControl("lbNumeroPedido");
                    Label lbOferta = (Label)gridArticulos.Rows[ii].FindControl("lbOferta");
                    Label lbFechaOfertaDesde = (Label)gridArticulos.Rows[ii].FindControl("lbFechaOfertaDesde");
                    Label lbPrecioOriginal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioOriginal");
                    Label lbEsDetalleKit = (Label)gridArticulos.Rows[ii].FindControl("lbEsDetalleKit");
                    Label lbPrecioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioFacturar");
                    Label lbTipoOferta = (Label)gridArticulos.Rows[ii].FindControl("lbTipoOferta");
                    Label lbVale = (Label)gridArticulos.Rows[ii].FindControl("lbVale");
                    Label lbAutorizacion = (Label)gridArticulos.Rows[ii].FindControl("lbAutorizacion");
                    Label lbBodega = (Label)gridArticulos.Rows[ii].FindControl("lbBodega");
                    Label lbLocalizacion = (Label)gridArticulos.Rows[ii].FindControl("lbLocalizacion");
                    LinkButton lkRequisicionArmado = (LinkButton)gridArticulos.Rows[ii].FindControl("lkRequisicionArmado");
                    Label lbArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbArticulo");
                    Label lbNombreArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbNombreArticulo");
                    Label lbPrecioUnitarioDebioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioUnitarioDebioFacturar");
                    Label lbPrecioSugerido = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioSugerido");
                    Label lbPrecioBaseLocal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioBaseLocal");
                    Label lbGel = (Label)gridArticulos.Rows[ii].FindControl("lbGel");
                    Label lbPorcentajeDescuento = (Label)gridArticulos.Rows[ii].FindControl("lbPorcentajeDescuento");
                    Label lbBeneficiario = (Label)gridArticulos.Rows[ii].FindControl("lbBeneficiario");
                    var descuento = ((Label)gridArticulos.Rows[ii].FindControl("lbDescuento")).Text;


                    DateTime mFechaOfertaDesde = new DateTime(1980, 1, 1);

                    try
                    {
                        mFechaOfertaDesde = new DateTime(Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(6, 4)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(3, 2)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(0, 2)));
                    }
                    catch
                    {
                        //Nothing
                    }
                    try
                    {
                        mPrecioOriginal = Convert.ToDecimal(lbPrecioOriginal.Text.Replace(",", ""));
                    }
                    catch
                    {
                        mPrecioOriginal = 0;
                    }


                    wsPuntoVenta.PedidoLinea itemPedidoLinea = new wsPuntoVenta.PedidoLinea();
                    itemPedidoLinea.Articulo = lbArticulo.Text;
                    itemPedidoLinea.Nombre = lbNombreArticulo.Text;
                    itemPedidoLinea.PrecioUnitario = Math.Round(Convert.ToDecimal(lbPrecioUnitario.Text.Replace(",", "")),2,MidpointRounding.AwayFromZero);
                    itemPedidoLinea.CantidadPedida = Convert.ToDecimal(lbCantidadPedida.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioTotal = Math.Round(Convert.ToDecimal(lbPrecioTotal.Text.Replace(",", "")), 2, MidpointRounding.AwayFromZero);
                    itemPedidoLinea.PrecioFacturar = Math.Round(Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", "")), 2, MidpointRounding.AwayFromZero);
                    itemPedidoLinea.Bodega = lbBodega.Text;
                    itemPedidoLinea.Localizacion = lbLocalizacion.Text;
                    itemPedidoLinea.RequisicionArmado = lkRequisicionArmado.Text;
                    itemPedidoLinea.Descripcion = lbDescripcion.Text;
                    itemPedidoLinea.Comentario = lbComentario.Text;
                    itemPedidoLinea.Estado = lbEstado.Text;
                    itemPedidoLinea.NumeroPedido = lbNumeroPedido.Text;
                    itemPedidoLinea.Oferta = lbOferta.Text;
                    itemPedidoLinea.FechaOfertaDesde = mFechaOfertaDesde;
                    itemPedidoLinea.PrecioOriginal = mPrecioOriginal;
                    itemPedidoLinea.TipoOferta = lbTipoOferta.Text;
                    itemPedidoLinea.EsDetalleKit = lbEsDetalleKit.Text;
                    itemPedidoLinea.Vale = Convert.ToInt32(lbVale.Text == "" ? "0" : lbVale.Text);
                    itemPedidoLinea.Autorizacion = Convert.ToInt64(lbAutorizacion.Text == "" ? "0" : lbAutorizacion.Text);
                    itemPedidoLinea.PrecioUnitarioDebioFacturar = Convert.ToDecimal(lbPrecioUnitarioDebioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioSugerido = Convert.ToDecimal(lbPrecioSugerido.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioBaseLocal = Convert.ToDouble(lbPrecioBaseLocal.Text.Replace(",", ""));
                    itemPedidoLinea.Gel = lbGel.Text;
                    itemPedidoLinea.PorcentajeDescuento = Convert.ToDecimal(lbPorcentajeDescuento.Text);
                    itemPedidoLinea.Descuento = Math.Round(decimal.Parse(descuento.Replace(",", "")), 2, MidpointRounding.AwayFromZero);
                    itemPedidoLinea.Beneficiario = lbBeneficiario.Text;
                    descTotal += decimal.Parse(descuento.Replace(",", ""));

                    if (jj > 1) Array.Resize(ref qLinea, jj);
                    qLinea[jj - 1] = itemPedidoLinea;
                }

                //itemPedido.Monto += descTotal;
                q[0] = itemPedido;
                string json = JsonConvert.SerializeObject(q);
                log.Debug("PEDIDO " + json);

                 json = JsonConvert.SerializeObject(qLinea);
                log.Debug("LINEA " + json);


                string mVendedorEnviar = cbVendedor.SelectedValue;
                if (!ws.GrabarPedido(ref mPedido, cbTienda.SelectedValue, mVendedorEnviar, q, qLinea, ref mMensaje, Convert.ToString(Session["Usuario"]), cbTipo.SelectedValue, Convert.ToInt32(cbFinanciera.SelectedValue), cbNivelPrecio.SelectedValue, ""))
                {
                    lbError.Text = mMensaje;
                    return;
                }

                Session["Pedido"] = mPedido;

                limpiar(true);
                lbInfo.Text = mMensaje;
                CargarPedidosCliente();
                cbVendedor.SelectedValue = Convert.ToString(Session["Vendedor"]);
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al grabar el pedido. {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
                return;
            }
        }

        protected void lkLimpiar_Click(object sender, EventArgs e)
        {
            limpiar(true);
        }

        protected void gridClientes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void codigoArticuloDet_TextChanged(object sender, EventArgs e)
        {
            BuscarArticulo("C");
        }

        protected void descripcionArticuloDet_TextChanged(object sender, EventArgs e)
        {
            BuscarArticulo("D");
        }

        void BuscarArticulo(string tipo)
        {
            try
            {
                lbError.Text = "";

                if (txtCodigo.Text.Trim().Length == 0)
                {
                    lbError.Text = "Debe seleccionar un cliente.";
                    mostrarBusquedaCliente();
                    return;
                }

                if (cbVendedor.Text.Trim().Length == 0)
                {
                    lbError.Text = "Debe seleccionar el vendedor.";
                    cbVendedor.Focus();
                    return;
                }

                if (cbFinanciera.SelectedValue == "Seleccione financiera ...")
                {
                    lbError.Text = "Debe seleccionar la financiera.";
                    cbFinanciera.Focus();
                    return;
                }

                Int32 mFinanciera = 0;
                try
                {
                    mFinanciera = Convert.ToInt32(cbFinanciera.SelectedValue);
                }
                catch
                {
                    lbError.Text = "Debe seleccionar la financiera.";
                    cbFinanciera.Focus();
                    return;
                }
                if (mFinanciera <= 0)
                {
                    lbError.Text = "Debe seleccionar la financiera.";
                    cbFinanciera.Focus();
                    return;
                }

                if (cbNivelPrecio.SelectedValue == "Seleccione ..." || cbNivelPrecio.Text.Trim().Length == 0 || cbNivelPrecio.Text == "Seleccione ...")
                {
                    lbError.Text = "Debe seleccionar el nivel de precio.";
                    cbTipo.Focus();
                    return;
                }

                string mTipo = ""; bool mCriterio = false;
                if (!validarCriterio(ref mTipo, ref mCriterio)) return;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                ws.Timeout = 999999999;
                mTipo = string.Format("${0}", mTipo);
                var q = ws.DevuelveArticulosPreciosListado(false, cbTipo.SelectedValue, Convert.ToInt32(cbFinanciera.SelectedValue), cbNivelPrecio.SelectedValue, mCriterio, mTipo, codigoArticuloDet.Text.Trim(), descripcionArticuloDet.Text.Trim(), Convert.ToString(Session["Tienda"]));

                gridArticulosDet.DataSource = q;
                gridArticulosDet.DataBind();

                lbError2.Text = "";
                lbError2.Visible = false;

                Label6.Text = "";
                Label7.Text = "";

                setToolTips();

                if (gridArticulosDet.Rows.Count > 0)
                {
                    gridArticulosDet.Focus();
                    gridArticulosDet.SelectedIndex = 0;

                    Label7.Text = string.Format("Se encontraron {0} artículos.", gridArticulosDet.Rows.Count);
                    LinkButton lkSeleccionarArticulo = (LinkButton)gridArticulosDet.SelectedRow.FindControl("lkSeleccionarArticulo");

                    lkSeleccionarArticulo.Focus();
                }
                if (gridArticulosDet.Rows.Count == 1)
                {
                    if (gridArticulosDet.Rows[0].Cells[1].Text != "000000-000")
                    {
                        gridArticulosDet.SelectedIndex = 0;
                        seleccionarArticulo();
                    }
                }
                if (gridArticulosDet.Rows.Count == 0)
                {
                    codigoArticuloDet.Text = "";
                    descripcionArticuloDet.Text = "";

                    lbError2.Visible = true;
                    lbError2.Text = "No se encontraron artículos.";
                    Label6.Text = "No se encontraron artículos.";

                    if (tipo == "C")
                    {
                        codigoArticuloDet.Focus();
                    }
                    else
                    {
                        descripcionArticuloDet.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error la buscar el artículo {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }

        void setToolTips()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            for (int ii = 0; ii < gridArticulosDet.Rows.Count; ii++)
            {
                Label lbOferta = (Label)gridArticulosDet.Rows[ii].FindControl("lbOferta");
                Label lbPrecio = (Label)gridArticulosDet.Rows[ii].FindControl("lbPrecio");
                Label lbPrecioOriginal = (Label)gridArticulosDet.Rows[ii].FindControl("lbPrecioOriginal");
                Label lbFechaVence = (Label)gridArticulosDet.Rows[ii].FindControl("lbFechaVence");
                Label lbDescuentoContado = (Label)gridArticulosDet.Rows[ii].FindControl("lbDescuentoContado");
                Label lbDescuentoCredito = (Label)gridArticulosDet.Rows[ii].FindControl("lbDescuentoCredito");
                Label lbCondicional = (Label)gridArticulosDet.Rows[ii].FindControl("lbCondicional");
                Label lbPrecioOferta = (Label)gridArticulosDet.Rows[ii].FindControl("lbPrecioOferta");
                Label lbCondiciones = (Label)gridArticulosDet.Rows[ii].FindControl("lbCondiciones");
                Label lbPromocion = (Label)gridArticulosDet.Rows[ii].FindControl("lbPromocion");
                Label lbTipoPromocion = (Label)gridArticulosDet.Rows[ii].FindControl("lbTipoPromocion");
                Label lbValorPromocion = (Label)gridArticulosDet.Rows[ii].FindControl("lbValorPromocion");
                Label lbVencePromocion = (Label)gridArticulosDet.Rows[ii].FindControl("lbVencePromocion");
                Label lbFechaVencePromocion = (Label)gridArticulosDet.Rows[ii].FindControl("lbFechaVencePromocion");
                Label lbDescripcionPromocion = (Label)gridArticulosDet.Rows[ii].FindControl("lbDescripcionPromocion");
                Label lbObservacionesPromocion = (Label)gridArticulosDet.Rows[ii].FindControl("lbObservacionesPromocion");
                Label lbPagos = (Label)gridArticulosDet.Rows[ii].FindControl("lbPagos");

                if (lbOferta.Text == "S")
                {
                    if (lbCondicional.Text == "S")
                    {
                        string mCondiciones = ws.DevuelveCondiciones(gridArticulosDet.Rows[ii].Cells[1].Text, Convert.ToString(Session["Tienda"]));
                        lbPrecio.ToolTip = string.Format("Este artículo está en OFERTA!!!{0}{3}{1} y vence el: {2}{0}La oferta aplica en la compra de:{0}{4}", Environment.NewLine, "", Convert.ToDateTime(lbFechaVence.Text).ToShortDateString(), Convert.ToDecimal(lbPrecioOriginal.Text) > 0 ? string.Format("Precio de Oferta: Q {0}", String.Format("{0:0,0.00}", Convert.ToDecimal(lbPrecioOferta.Text))) : string.Format("{0}% desc. al contado y {1}% desc. al crédito.", lbDescuentoContado.Text.Replace(".0000", ""), lbDescuentoCredito.Text.Replace(".0000", "")), mCondiciones);
                    }
                    else
                    {
                        lbPrecio.ToolTip = string.Format("Este artículo está en OFERTA!!!{0}{3}{1}{0}La oferta vence el: {2}", Environment.NewLine, "", Convert.ToDateTime(lbFechaVence.Text).ToShortDateString(), Convert.ToDecimal(lbPrecioOriginal.Text) > 0 ? string.Format("Precio Original: Q {0}", String.Format("{0:0,0.00}", Convert.ToDecimal(lbPrecioOriginal.Text))) : string.Format("{0}% desc. al contado y {1}% desc. al crédito.", lbDescuentoContado.Text.Replace(".0000", ""), lbDescuentoCredito.Text.Replace(".0000", "")));
                    }
                }

                if (lbPromocion.Text == "S")
                    lbPrecio.ToolTip = string.Format("Este artículo está en PROMOCION!!!{0}{1}{0}{2}{0}{3}", Environment.NewLine, lbDescripcionPromocion.Text, lbObservacionesPromocion.Text, lbVencePromocion.Text == "N" ? "" : string.Format("La promoción vence el: {0}", Convert.ToDateTime(lbFechaVencePromocion.Text).ToShortDateString()));

                int mPagos = 0;
                string mPagosString = lbPagos.Text.Replace(" PAGOS", "");

                try
                {
                    mPagos = Convert.ToInt32(mPagosString);
                }
                catch
                {
                    // Nada
                }

                if (mPagos > 0)
                {
                    decimal mPrecio = Convert.ToDecimal(lbPrecio.Text);
                    gridArticulosDet.Rows[ii].Cells[4].ToolTip = string.Format("Cuotas de Q {0}", String.Format("{0:0,0.00}", mPrecio / mPagos));
                }
            }
        }

        bool validarCriterio(ref string tipo, ref bool criterio)
        {
            if (codigoArticuloDet.Text.Trim().Length > 0 || descripcionArticuloDet.Text.Trim().Length > 0) criterio = true;

            if (criterio)
            {
                if (codigoArticuloDet.Text.Trim().Length == 0 && descripcionArticuloDet.Text.Trim().Length == 0)
                {
                    lbError.Text = "Debe ingresar un criterio de búsqueda ya sea el código, la descripción o ambos.";
                    codigoArticuloDet.Focus();
                    return false;
                }

                if (codigoArticuloDet.Text.Trim().Length > 0) tipo = "C";
                if (descripcionArticuloDet.Text.Trim().Length > 0) tipo = "D";
                if (codigoArticuloDet.Text.Trim().Length > 0 && descripcionArticuloDet.Text.Trim().Length > 0) tipo = "A";

                return true;
            }
            else
            {
                return true;
            }
        }

        protected void lbBuscarArticuloDet_Click(object sender, EventArgs e)
        {
            string mTipo = "C";
            if (descripcionArticuloDet.Text.Trim().Length > 0) mTipo = "D";
            BuscarArticulo(mTipo);
        }

        protected void lbCerrarBusquedaDet_Click(object sender, EventArgs e)
        {
            tablaBusquedaDet.Visible = false;
            gridArticulosDet.Visible = false;
        }

        protected void gridArticulos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        void EliminarArticulo()
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                bool blArticulosValidos = true;
                List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();
                double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

                txtValor.Text = "0.00";
                txtFacturar.Text = "0.00";
               

                try
                {
                    List<NuevoPrecioProducto> lstprod = (List<NuevoPrecioProducto>)Session["lstNuevoPrecioProducto"];
                    lstprod.Remove(lstprod.Find(x => x.Index == gridArticulos.SelectedIndex));
                    var Saldo = Facturacion.ObtenerSaldoDescuento(lstprod);
                    if (Saldo >= 0)
                    {
                        
                        Session["lstNuevoPrecioProducto"] = lstprod;
                        LimpiarListaArticulos();

                    }
                    else
                    {

                        LimpiarListaArticulos();
                        limpiar(true);
                        lbError2.Text = "Los productos restantes no cumplen con la condición de precios para facturarse.";
                        lbError2.Visible = true;
                        blArticulosValidos = false;
                    }

                }
                catch
                { }
                if (blArticulosValidos)
                {
                    foreach (GridViewRow gvr in gridArticulos.Rows)
                    {
                        Label lbCondicional = (Label)gvr.FindControl("lbCondicional");
                        Label lbArticuloCondicion = (Label)gvr.FindControl("lbArticuloCondicion");
                        Label lbFechaOfertaDesde = (Label)gvr.FindControl("lbFechaOfertaDesde");
                        Label lbLinea = (Label)gvr.FindControl("lbLinea");
                        Label lbArticulo = (Label)gvr.FindControl("lbArticulo");
                        Label lbNombreArticulo = (Label)gvr.FindControl("lbNombreArticulo");
                        Label lbPrecioOriginal = (Label)gvr.FindControl("lbPrecioOriginal");
                        Label lbPorcentajeDescuento = (Label)gvr.FindControl("lbPorcentajeDescuento");
                        Label lbLineaSeleccionada = (Label)gridArticulos.SelectedRow.FindControl("lbLinea");
                        Label lbArticuloSeleccionado = (Label)gridArticulos.SelectedRow.FindControl("lbArticulo");
                        string mArticulo = lbArticuloSeleccionado.Text;

                        if (lbLinea.Text != lbLineaSeleccionada.Text && lbArticuloCondicion.Text != mArticulo)
                        {
                            decimal mPrecioOriginal = 0;
                            DateTime mFechaOfertaDesde = new DateTime(1980, 1, 1);

                            try
                            {
                                mFechaOfertaDesde = new DateTime(Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(6, 4)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(3, 2)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(0, 2)));
                            }
                            catch
                            {
                                //Nothing
                            }

                            try
                            {
                                mPrecioOriginal = Convert.ToDecimal(lbPrecioOriginal.Text.Replace(",", ""));
                            }
                            catch
                            {
                                //Nothing
                            }

                            Label lbPrecio = (Label)gvr.FindControl("lbPrecio");
                            Label lbPrecioFacturar = (Label)gvr.FindControl("lbPrecioFacturar");
                            Label lbCantidad = (Label)gvr.FindControl("lbCantidad");
                            Label lbTotal = (Label)gvr.FindControl("lbTotal");
                            Label lbBodega = (Label)gvr.FindControl("lbBodega");
                            Label lbLocalizacion = (Label)gvr.FindControl("lbLocalizacion");
                            LinkButton lkRequisicionArmado = (LinkButton)gvr.FindControl("lkRequisicionArmado");
                            Label lbDescripcion = (Label)gvr.FindControl("lbDescripcion");
                            Label lbComentario = (Label)gvr.FindControl("lbComentario");
                            Label lbEstado = (Label)gvr.FindControl("lbEstado");
                            Label lbNumeroPedido = (Label)gvr.FindControl("lbNumeroPedido");
                            Label lbOferta = (Label)gvr.FindControl("lbOferta");
                            Label lbTipoOferta = (Label)gvr.FindControl("lbTipoOferta");
                            Label lbEsDetalleKit = (Label)gvr.FindControl("lbEsDetalleKit");

                            wsPuntoVenta.PedidoLinea item = new wsPuntoVenta.PedidoLinea();
                            item.Articulo = lbArticulo.Text;
                            item.Nombre = lbNombreArticulo.Text;
                            item.PrecioUnitario = Convert.ToDecimal(lbPrecio.Text.Replace(",", ""));
                            item.CantidadPedida = Convert.ToDecimal(lbCantidad.Text.Replace(",", ""));
                            item.PrecioTotal = Convert.ToDecimal(lbTotal.Text.Replace(",", ""));
                            item.PrecioFacturar = Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", ""));
                            item.Bodega = lbBodega.Text;
                            item.Localizacion = lbLocalizacion.Text;
                            item.RequisicionArmado = lkRequisicionArmado.Text;
                            item.Descripcion = lbDescripcion.Text;
                            item.Comentario = lbComentario.Text;
                            item.Estado = lbEstado.Text;
                            item.NumeroPedido = lbNumeroPedido.Text;
                            item.Oferta = lbOferta.Text;
                            item.TipoOferta = lbTipoOferta.Text;
                            item.FechaOfertaDesde = mFechaOfertaDesde;
                            item.PrecioOriginal = mPrecioOriginal;
                            item.Condicional = lbCondicional.Text;
                            item.ArticuloCondicion = lbArticuloCondicion.Text;
                            item.EsDetalleKit = lbEsDetalleKit.Text;
                            item.Linea = Convert.ToInt32(lbLinea.Text);
                            item.PrecioSugerido = Convert.ToDecimal(((Label)gvr.FindControl("lbPrecioSugerido")).Text.Replace(",", ""));
                            item.PorcentajeDescuento= Convert.ToDecimal(lbPorcentajeDescuento.Text);
                            item.Descuento= Convert.ToDecimal(((Label)gvr.FindControl("lbDescuento")).Text.Replace(",", ""));
                            item.NetoFacturar = Math.Round(((item.PrecioTotal)), 2, MidpointRounding.AwayFromZero);
                            q.Add(item);

                            txtFacturar.Text = String.Format("{0:0,0.00}", (Convert.ToDecimal(txtFacturar.Text.Replace(",", "")) + Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", ""))));
                        }
                    }

                    if (q.Count() == 0)
                    {
                        txtEnganche.Text = "0.00";
                        txtDescuentos.Text = "0.00";
                        tablaBusquedaDet.Visible = false;
                        gridArticulosDet.Visible = false;
                    }
                    else
                    {
                        txtDescuentos.Text = String.Format("{0:0,0.00}", q.Sum(x => x.Descuento));
                    }

                    ViewState["qArticulos"] = q;
                    ValidaTipoNivelPrecio();

                    gridArticulos.DataSource = q;
                    gridArticulos.DataBind();
                    gridArticulos.Visible = true;

                    articulo.Text = "";
                    descripcion.Text = "";
                    cbRequisicion.SelectedValue = "No";
                    cbLocalizacion.SelectedValue = "ARMADO";
                    txtCantidad.Text = "1";
                    txtPrecioUnitario.Text = "0.00";
                    txtTotal.Text = "0.00";
                    txtDescuentoItem.Text = "0.00";
                    articulo.Focus();
                    gridArticulos.SelectedIndex = -1;
                    txtDescuentosVales_TextChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al borrar la línea {0} {1}", ex.Message, m);
            }
        }

        void GarantiaArticulo()
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                lbInfo.Text = "";
                lbError.Text = "";

                if (txtPedido.Text == "P000000")
                {
                    lbError.Text = "Primero debe facturar el pedido.";
                    return;
                }

                if (!ws.PedidoFacturado(txtPedido.Text))
                {
                    lbError.Text = "Para continuar, primero debe facturar el pedido.";
                    return;
                }

                Label lbArticuloG = (Label)gridArticulos.SelectedRow.FindControl("lbArticulo");
                Label lbDescripcionG = (Label)gridArticulos.SelectedRow.FindControl("lbNombreArticulo");
                Label lbLinea = (Label)gridArticulos.SelectedRow.FindControl("lbLinea");

                liGarantias.Visible = true;
                lbLineaArticulo.Text = lbLinea.Text;
                lbArticuloGarantia.Text = lbArticuloG.Text;
                lbArticuloDescripcionGarantia.Text = lbDescripcionG.Text;

                lkGarantiaCamas.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error en la garantía {0} {1}", ex.Message, m);
            }
        }

        protected void gridArticulos_SelectedIndexChanged(object sender, EventArgs e)
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            switch (ViewState["AccionArticulo"].ToString())
            {
                case "Eliminar":
                    EliminarArticulo();
                    break;
                case "Cambiar":
                    ViewState["CambiarLocalizacion"] = "grid";
                    Label lbArticuloSeleccionado = (Label)gridArticulos.SelectedRow.FindControl("lbArticulo");
                    cargarLocalizaciones(lbArticuloSeleccionado.Text.Trim());
                    break;
                case "Garantía":
                    GarantiaArticulo();
                    break;
                case "Requisicion":
                    RequisicionArmado();
                    break;
                case "Gel":
                    Label lbLinea = (Label)gridArticulos.SelectedRow.FindControl("lbLinea");
                    Label lbNombre = (Label)gridArticulos.SelectedRow.FindControl("lbNombreArticulo");
                    Label lbDescripcion = (Label)gridArticulos.SelectedRow.FindControl("lbDescripcion");

                    Clases.Info mInfo = new Clases.Info();
                    mInfo.Documento = txtPedido.Text;

                    string json = JsonConvert.SerializeObject(mInfo);
                    byte[] data = Encoding.ASCII.GetBytes(json);

                    WebRequest request = WebRequest.Create(string.Format("{0}/gel/{1}", Convert.ToString(Session["UrlRestServices"]), lbLinea.Text));

                    request.Method = "PUT";
                    request.ContentType = "application/json";
                    request.ContentLength = data.Length;

                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }

                    var response = (HttpWebResponse)request.GetResponse();
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    Clases.RetornaExito mRetorna = new Clases.RetornaExito();
                    mRetorna = JsonConvert.DeserializeObject<Clases.RetornaExito>(responseString);

                    string mGel = " - GARANTÍA ESPECIAL LIMITADA";
                    if (!mRetorna.exito)
                    {

                        if (mRetorna.mensaje == "No se encontró la línea del pedido, debe grabarlo antes.")
                        {
                            Label lbGel = (Label)gridArticulos.SelectedRow.FindControl("lbGel");

                            if (lbGel.Text == "No")
                            {
                                lbGel.Text = "Sí";
                                lbNombre.Text = string.Format("{0}{1}", lbNombre.Text, mGel);
                            }
                            else
                            {
                                lbGel.Text = "No";
                                lbNombre.Text = lbNombre.Text.Replace(mGel, "");
                            }

                            mRetorna.mensaje = lbNombre.Text;
                        }
                        else
                        {
                            lbError.Text = mRetorna.mensaje;
                            return;
                        }
                    }

                    List<wsPuntoVenta.PedidoLinea> qGel = new List<wsPuntoVenta.PedidoLinea>();
                    qGel = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                    foreach (var item in qGel)
                    {
                        if (item.Linea == Convert.ToInt32(lbLinea.Text))
                        {
                            item.Nombre = mRetorna.mensaje;
                            item.Descripcion = mRetorna.mensaje;

                            item.Gel = "No";
                            if (mRetorna.mensaje.Contains(mGel)) item.Gel = "Sí";
                        }
                    }

                    ViewState["qArticulos"] = qGel;
                    lbNombre.Text = mRetorna.mensaje;
                    lbDescripcion.Text = mRetorna.mensaje;

                    break;
                default:
                    break;
            }
        }

        protected void lbBuscarArticulo_Click(object sender, EventArgs e)
        {
            habilitarBusquedaArticulos();
        }

        void habilitarBusquedaArticulos()
        {
            tablaBusquedaDet.Visible = true;
            gridArticulosDet.Visible = true;

            List<wsCambioPrecios.Articulos> q = new List<wsCambioPrecios.Articulos>();

            gridArticulosDet.DataSource = q;
            gridArticulosDet.DataBind();

            lbError2.Visible = false;
            lbError2.Text = "";

            Label6.Text = "";
            Label7.Text = "";

            codigoArticuloDet.Text = "";
            descripcionArticuloDet.Text = "";
            descripcionArticuloDet.Focus();
        }

        void ValidaBotonPromocion()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            ws.Url = General.FiestaNETService;
            bool mMostrar = false;

            var q = ws.DevuelveTipoVenta(cbTipo.SelectedValue);
            if (q[0].Valor > 0 || q[0].DescuentoNormal > 0 || q[0].DescuentoOfertado > 0)
            {
            if (q[0].Financiera == 0 || q[0].Financiera.ToString() == cbFinanciera.SelectedValue) 
                    mMostrar = true;

                if (mMostrar && q[0].ManejaVale == "N")
                {
                    lkLaTorre.Text = q[0].TextoPromocion;
                    lkLaTorre.ToolTip = string.Format("Haga clic aquí para aplicar la promoción de {0}", q[0].Descripcion);
                    lkLaTorre.Visible = true;
                }
                else if (q[0].ManejaVale == "S")
                {   
                   // tblVale.Visible = true;
                    lkLaTorre.Visible = false;
                }
                else
                {

                }
            }
            else
            {
                lkLaTorre.Visible = false;
            }
        }

        protected void cbNivelPrecio_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                limpiar(false);
                ValidaBotonPromocion();
            }
            catch (Exception ex)
            {
                string m = "";

                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    //Nothing
                }

                lbError2.Visible = true;
                lbError2.Text = string.Format("Error al cambiar el nivel. {0} {1}", ex.Message, m);
            }
        }

        protected void gridArticulosDet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }

            try
            {
                Label lbOferta = (Label)e.Row.FindControl("lbOferta");
                Label lbPromocion = (Label)e.Row.FindControl("lbPromocion");

                //if (lbOferta.Text == "S")
                //{
                //    e.Row.ForeColor = System.Drawing.Color.FromArgb(227, 89, 4);
                //    e.Row.Font.Bold = true;
                //}

                //if (lbPromocion.Text == "S")
                //{
                //    e.Row.ForeColor = System.Drawing.Color.FromArgb(165, 81, 41);
                //    e.Row.Font.Bold = true;
                //}
            }
            catch
            {
                //Nothing
            }
        }

        
        void seleccionarArticulo()
        {
            try
            {
                List<NuevoPrecioProducto> lstProductos = new List<NuevoPrecioProducto>();
                lstProductos = ((List<NuevoPrecioProducto>)Session["lstNuevoPrecioProducto"]);
                if (lstProductos == null)
                    lstProductos = new List<NuevoPrecioProducto>();

                if (lstProductos.Count == gridArticulos.Rows.Count)
                {

                } else
                {
                    string json = JsonConvert.SerializeObject(lstProductos);
                    log.Debug("lstProductos: " + json+ " gridArticulos.Rows.Count"+ gridArticulos.Rows.Count);
                }

                    txtVale1.Text = "0";
                    lbError2.Text = "";
                    lbError2.Visible = false;

                    var ws = new wsPuntoVenta.wsPuntoVenta();
                    if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                    Label lbPrecio = (Label)gridArticulosDet.SelectedRow.FindControl("lbPrecio");
                    Label lbOferta = (Label)gridArticulosDet.SelectedRow.FindControl("lbOferta");
                    Label lbFechaDesde = (Label)gridArticulosDet.SelectedRow.FindControl("lbFechaDesde");
                    Label lbPrecioOriginal = (Label)gridArticulosDet.SelectedRow.FindControl("lbPrecioOriginal");
                    Label lbTipoOferta = (Label)gridArticulosDet.SelectedRow.FindControl("lbTipoOferta");
                    Label lbCondicional = (Label)gridArticulosDet.SelectedRow.FindControl("lbCondicional");
                    Label lbPrecioOferta = (Label)gridArticulosDet.SelectedRow.FindControl("lbPrecioOferta");

                    string mPrecio = lbPrecio.Text;
                    string mArticulo = gridArticulosDet.SelectedRow.Cells[1].Text;
                    string mCondiciones = ws.DevuelveCondiciones(gridArticulosDet.SelectedRow.Cells[1].Text, Convert.ToString(Session["Tienda"]));
                    string mDescripcion = gridArticulosDet.SelectedRow.Cells[2].Text.Replace("&#39;", "'").Replace("&#225;", "á").Replace("&#233;", "é").Replace("&#237;", "í").Replace("&#243;", "ó").Replace("&#250;", "ú").Replace("&quot;", "\"").Replace("&#209;", "Ñ").Replace("&#241;", "Ñ").Replace("&#252;", "ü").Replace("&#193;", "Á").Replace("&#201;", "É").Replace("&#205;", "Í").Replace("&#211;", "Ó").Replace("&#218;", "Ú").Replace("&#220;", "Ü").Replace("&#191;", "¿").Replace("&#161;", "¡");

                    if (lbCondicional.Text == "S")
                    {
                        bool mCumpleCondicion = false;
                        string mArticuloCondicion = "";

                        for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                        {
                            if (mCondiciones.Contains(gridArticulos.Rows[ii].Cells[1].Text))
                            {
                                mCumpleCondicion = true;
                                mArticuloCondicion = gridArticulos.Rows[ii].Cells[1].Text;
                            }
                        }

                        lbError2.Visible = true;

                        if (mCumpleCondicion)
                        {
                            mPrecio = lbPrecioOferta.Text;

                            txtCondicional.Text = "S";
                            txtArticuloCondicion.Text = mArticuloCondicion;
                            lbError2.Text = string.Format("Este artículo aplica el precio de oferta Q{0} ya que está comprando el artículo {1}.", String.Format("{0:0,0.00}", Convert.ToDecimal(lbPrecioOferta.Text)), mArticuloCondicion);
                        }
                        else
                        {
                            lbError2.Text = string.Format("Este artículo aplicaría el precio de oferta Q{0} si el cliente adquiere alguno de los siguientes articulos:{1}{2}.", String.Format("{0:0,0.00}", Convert.ToDecimal(lbPrecioOferta.Text)), System.Environment.NewLine, mCondiciones);
                        }

                    }

                    int Pagos = 1;
                    List<string> numbers = Regex.Split(cbNivelPrecio.SelectedValue, @"\D+").ToList();
                    numbers.ForEach(x =>
                    {
                        if (!string.IsNullOrEmpty(x))
                        {
                            Pagos = int.Parse(x);
                        }
                    });
                    if (WebConfigurationManager.AppSettings["PrecioOriginal"].ToString().Equals("1"))
                    {

                    log.Info("articulo" + mArticulo+"Precio "+ mPrecio);

                    Respuesta result = Facturacion.ObtenerDatosFacturacion(Convert.ToString(Session["Tienda"]), mArticulo, Pagos, cbFinanciera.SelectedValue, decimal.Parse(mPrecio.Replace(",", "")));
                        if (!result.Exito)
                        {
                            lbError2.Text = result.Mensaje;
                            lbError2.Visible = true;
                            txtDescMax.Text = "0.00";
                        }
                        else
                        {
                            //Se obtiene el factor para la formula del calculo de la cuota
                            NuevoPrecioProducto datosPrecio = ((NuevoPrecioProducto)result.Objeto);
                            mPrecio = String.Format("{0:0,0.00}", (datosPrecio.originalPrice)); //+ (cbTipoPedido.SelectedValue == "N" ? (Convert.ToDecimal(txtTotal.Text.Replace(",", "")) / datosPrecio.factor) : Convert.ToDecimal(txtTotal.Text.Replace(",", "")));
                            txtDescMax.Text = String.Format("{0:0,0.00}", datosPrecio.originalPrice - datosPrecio.originalDiscountPrice);
                            log.Info("Precio " + datosPrecio.originalPrice);
                            if (lstProductos.Count == gridArticulos.Rows.Count)
                            {
                                lstProductos.Add(datosPrecio);
                                Session["lstNuevoPrecioProducto"] = lstProductos;
                            }

                        var SaldoDescuento = Facturacion.ObtenerSaldoDescuento(lstProductos);

                         bool blModificarPrecios=  PuedeModificarPrecios();

                        if (SaldoDescuento < 0 && blModificarPrecios)
                            SaldoDescuento = 0;

                        txtSaldoDescuento.Text = String.Format("{0:0,0.00}", SaldoDescuento);
                        
                        }
                    
                    }



                    articulo.Text = mArticulo;
                    descripcion.Text = mDescripcion;
                    txtPrecioUnitario.Text = String.Format("{0:0,0.00}", Convert.ToDecimal(mPrecio.Replace(",", "")));
                    txtPrecioUnitarioDebioFacturar.Text = String.Format("{0:0,0.00}", Convert.ToDecimal(mPrecio.Replace(",", "")));
                    txtTotal.Text = String.Format("{0:0,0.00}", Convert.ToDecimal(mPrecio.Replace(",", "")) * Convert.ToInt32(txtCantidad.Text.Replace(",", "")));
                    txtOferta.Text = lbOferta.Text;
                    txtFechaOfertaDesde.Text = lbFechaDesde.Text;
                    txtPrecioOriginal.Text = lbPrecioOriginal.Text;
                    txtTipoOferta.Text = lbTipoOferta.Text;

                    decimal mNuevoPrecio = 0;
                    decimal mNuevoPrecioFacturar = 0;
                    decimal mTotal = 0;
                    string mMensaje = "";
                    if (WebConfigurationManager.AppSettings["PrecioOriginal"].ToString().Equals("0") && ws.TienePromocion(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue, mArticulo, txtCantidad.Text.Replace(",", ""), mPrecio.Replace(",", ""), ref mNuevoPrecio, ref mNuevoPrecioFacturar, ref mTotal, ref mMensaje))
                    {
                        txtPrecioUnitario.Text = String.Format("{0:0,0.00}", mNuevoPrecio);
                        txtPrecioUnitarioDebioFacturar.Text = String.Format("{0:0,0.00}", mNuevoPrecioFacturar);
                        txtTotal.Text = String.Format("{0:0,0.00}", mTotal);

                    }

                    tablaBusquedaDet.Visible = false;
                    gridArticulosDet.Visible = false;
                    gridLocalizaciones.Visible = false;
                    tblSolicutudAutorizacion.Visible = false;

                    cbRequisicion.SelectedValue = ws.RequiereArmado(mArticulo);
                    if (Convert.ToString(Session["Tienda"]) == "F01")
                    {
                        if (mArticulo.Substring(0, 1) == "S")
                        {
                            descripcion.Text = "";
                            descripcion.ReadOnly = false;
                            descripcion.Focus();
                            descripcion.TextMode = TextBoxMode.MultiLine;
                            descripcion.Height = new Unit(121);
                        }
                        else
                        {
                            txtPrecioUnitario.Focus();
                            descripcion.Height = new Unit(22);
                            descripcion.TextMode = TextBoxMode.SingleLine;
                        }
                    }
                    else
                    {
                        lbAgregarArticulo.Focus();
                    }

                
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                    descripcion.Height = new Unit(22);
                    descripcion.TextMode = TextBoxMode.SingleLine;
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format("Error al agregar el artículo {0} {1}", ex.Message, m);
            }
        
        }

       

        protected void gridArticulosDet_SelectedIndexChanged(object sender, EventArgs e)
        {
            seleccionarArticulo();
        }

        void ValidaTotalPedido()
        {
            double mFactor = 1;
            if (cbTipoPedido.SelectedValue == "N")
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                //Se obtiene el factor para la formula del calculo de la cuota
                mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

                //Se obtiene el tipo de pago, Credito, Contado, etc.
                string mTipo = ws.DevuelveTipoNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

                //Si el tipo de pago es de contado, el factor siempre es 1
                if (mTipo == "CO") mFactor = 1;

               
            }

            var mDescuento = double.Parse(txtDescuentoItem.Text.Replace(",",""));
           
            txtFacturar.Text = String.Format("{0:0,0.00}", (Convert.ToDouble(txtFacturar.Text.Replace(",", "")) + Math.Round((Convert.ToDouble(txtTotal.Text.Replace(",", "")) / mFactor),4,MidpointRounding.AwayFromZero)));
            txtFacturarItem.Text = String.Format("{0:0,0.00}", Convert.ToDouble(txtTotal.Text.Replace(",", "")) / mFactor );
        }

        void ValidaTipoNivelPrecio()
        {
            decimal mEnganche = 0;
            decimal mDescuento = 0;
            decimal mDescuentoItem = 0;
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU")
                ws.Url = Convert.ToString(ViewState["url"]);
             
            //Se obtiene el tipo de pago, Credito, Contado, etc.
            string mTipo = ws.DevuelveTipoNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

            //Se obtiene el factor para la formula del calculo de la cuota
            double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
            double mFactorDefault = mFactor>1 ? ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, "12", "Crediplus"+cbNivelPrecio.SelectedValue.Substring(cbNivelPrecio.SelectedValue.Length-3,3))
                                            :1;
            var PrecioUnitarioItem = Convert.ToDecimal(txtPrecioUnitario.Text.Replace(",", ""));

            try
            {
                //mDescuentoItem = Convert.ToDecimal(txtDescuentoItem.Text.Replace(",", ""));
               
                mDescuentoItem = (Convert.ToDecimal(txtDescuentoItem.Text.Replace(",", "")) > (PrecioUnitarioItem / (decimal)mFactor) ? PrecioUnitarioItem / (decimal)mFactor : Convert.ToDecimal(txtDescuentoItem.Text.Replace(",", "")));

            }
            catch
            {
                mDescuentoItem = 0;
                txtDescuentoItem.Text = "0.00";
            }
            try
            {
                mDescuento = Convert.ToDecimal(txtDescuentos.Text.Replace(",", ""));
            }
            catch
            {
                mDescuento = 0;
                txtDescuentos.Text = "0.00";
            }
            try
            {
                mEnganche = Convert.ToDecimal(txtEnganche.Text.Replace(",", ""));
            }
            catch
            {
                mEnganche = 0;
                txtEnganche.Text = "0.00";
            }

            

            txtIntereses.Text = "0.00";
            txtSaldoFinanciar.Text = "0.00";
            txtMonto.Text = String.Format("{0:0,0.00}", decimal.Parse(txtFacturar.Text.Replace(",",""))-mDescuento);
            //esta parte es para colocar el descuento real aplicado para un articulo de financira <> Crediplus
            mDescuento += Math.Abs(mDescuentoItem- PrecioUnitarioItem / (decimal)mFactorDefault)<1? mDescuentoItem *(decimal)mFactorDefault/(decimal)mFactor: mDescuentoItem;
            txtDescuentos.Text = String.Format("{0:0,0.00}", mDescuento);
            //Si el tipo de pago es Credito realiza las siguientes validaciones
            if (mTipo == "CR")
            {


                decimal mMonto = 0; int ii = 0;
                List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();
                q = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                foreach (var item in q)
                {
                    ii += 1;

                    mMonto += item.PrecioTotal;
                }

                decimal mFacturar = 0, mFacturarCompleto = 0;
                decimal mSaldoFinanciar = 0; decimal mSaldoFinanciarCompleto = 0;
                decimal mIntereses = 0;
                
                if (ii == 0)
                {
                    
                    //El Saldo a Financiar es el total de la factura, menos el enganche
                    mSaldoFinanciar = Convert.ToDecimal(txtFacturar.Text.Replace(",", "")) - Convert.ToDecimal(txtEnganche.Text.Replace(",", "")) - mDescuento;

                    //El monto a financiar es igual al saldo a financiar por el factor
                    mMonto = mSaldoFinanciar * (decimal)mFactor;

                    //Los intereses se calculas respecto al monto a financiar - saldo a financiar
                    mIntereses = mMonto - mSaldoFinanciar;

                    txtSaldoFinanciar.Text = String.Format("{0:0,0.00}", mSaldoFinanciar);
                    txtMonto.Text = String.Format("{0:0,0.00}", mMonto);
                    txtIntereses.Text = String.Format("{0:0,0.00}", mIntereses);
                }
                else
                {
                    #region "cálculo de precio del bien"
                    var lstNivelPrecioValidacion = new List<string>();
                    if (Session["NivelPrecioValidacion"] == null)
                    {
                        lstNivelPrecioValidacion = WebRestClient.ObtenerNivelPrecioFacturacion(Session["UrlRestServices"].ToString());
                        Session["NivelPrecioValidacion"] = lstNivelPrecioValidacion;
                    }
                    else
                    {
                        lstNivelPrecioValidacion = ((List<string>)Session["NivelPrecioValidacion"]);
                    }

                    if (lstNivelPrecioValidacion != null && lstNivelPrecioValidacion.Contains(cbNivelPrecio.SelectedValue))//forma de cálculo restándo el enganche
                    {
                        mFacturarCompleto = ((mMonto - mEnganche) / (decimal)mFactor) + mEnganche;
                        mFacturar = Math.Round(Convert.ToDecimal((double)(mMonto - mEnganche) / mFactor) + mEnganche, 2, MidpointRounding.AwayFromZero);
                    }
                    else
                    {
                        //mFacturarCompleto = monto total divido el factor, Monto total es la sumatoria del precio total de cada articulo
                        mFacturarCompleto = mMonto / (decimal)mFactor + mDescuento;

                        //mFacturar= lo mismo que mFacturarCompleto a dos decimales
                        mFacturar = Math.Round(Convert.ToDecimal(((double)mMonto / mFactor) + (double)mDescuento), 2, MidpointRounding.AwayFromZero);
                    }
                    #endregion
                    //Saldo a financiar = a facturar - enganche
                    mSaldoFinanciar = mFacturar - mEnganche- mDescuento;


                    mSaldoFinanciarCompleto = mFacturarCompleto - mEnganche- mDescuento;

                    mMonto = Math.Round(Convert.ToDecimal((double)mSaldoFinanciarCompleto * mFactor), 2, MidpointRounding.AwayFromZero);
                    mIntereses = mMonto - mSaldoFinanciar;
                    
                    txtMonto.Text = String.Format("{0:0,0.00}", mMonto);
                    txtFacturar.Text = String.Format("{0:0,0.00}", mFacturar);
                    txtSaldoFinanciar.Text = String.Format("{0:0,0.00}", mSaldoFinanciar);
                    txtIntereses.Text = String.Format("{0:0,0.00}", mIntereses);
                }
                txtDescuentos.Text = String.Format("{0:0,0.00}", mDescuento);

                Int32 mPagos; string mPagos1;
                string mPagosTexto = ws.DevuelvePagosNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

                try
                {
                    mPagos = Convert.ToInt32(mPagosTexto.Substring(0, 2).Replace(" ", ""));
                }
                catch
                {
                    mPagos = 1;
                }

                decimal mCuotas = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) / mPagos;



                if (mPagos <= 10)
                {
                    mPagos1 = string.Format("0{0}", mPagos - 1);
                }
                else
                {
                    mPagos1 = (mPagos - 1).ToString();
                }

                cbPagos1.SelectedValue = mPagos1;
                cbPagos2.SelectedValue = "01";

                if (mCuotas.ToString().Substring(mCuotas.ToString().Length - 2, 2) == "00")
                {
                    txtPagos1.Text = String.Format("{0:0,0.00}", mCuotas);
                    txtPagos2.Text = String.Format("{0:0,0.00}", mCuotas);
                }
                else
                {
                    if (cbFinanciera.SelectedValue == Const.FINANCIERA.BANCREDIT)
                    {
                        txtPagos1.Text = String.Format("{0:0,0.00}", Math.Round(mCuotas, 0, MidpointRounding.AwayFromZero));
                        decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (Math.Round(mCuotas, 0, MidpointRounding.AwayFromZero) * (mPagos - 1));
                        txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
                    }
                    else
                    {
                        string[] mCuotasString = mCuotas.ToString().Split(new string[] { "." }, StringSplitOptions.None);

                        Int32 mCuota = 1 + Convert.ToInt32(mCuotasString[0]);
                        decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (mCuota * (mPagos - 1));

                        txtPagos1.Text = String.Format("{0:0,0.00}", mCuota);
                        txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
                    }
                }
            }else
            {
               
                txtMonto.Text = String.Format("{0:0,0.00}", decimal.Parse(txtFacturar.Text.Replace(",", "")) - Convert.ToDecimal(txtDescuentos.Text.Replace(",","")));
            }
        }

        void agregarArticulo()
        {
            try
            {
                //OriginalDiscountPrice
                decimal mPrecioSugerido = 0;
                lbError2.Text = "";
                lbError2.Visible = false;
                var ws = new wsPuntoVenta.wsPuntoVenta();
                double mFactor = 0;
                double mFactorFinanciera = 0;
                string mTipo = "";
                bool blDescuentosValidos = true;

                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU")
                    ws.Url = Convert.ToString(ViewState["url"]);

                if (articulo.Text.Trim().Length == 0)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('Debe seleccionar un artículo.');", true);
                    articulo.Focus();
                    return;
                }
                if (txtTotal.Text == "0.00")
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('Debe seleccionar un artículo.');", true);
                    articulo.Focus();
                    return;
                }
                try
                {
                    txtSaldoDescuento.Text = String.Format("{0:0,0.00}", Facturacion.ObtenerSaldoDescuento((List<NuevoPrecioProducto>)Session["lstNuevoPrecioProducto"]));
                }
                catch
                { }

                try
                {
                    
                    mTipo = ws.DevuelveTipoNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                    mFactorFinanciera = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                    mFactor = mFactorFinanciera!=1? ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, "12", "Crediplus" + cbNivelPrecio.SelectedValue.Substring(cbNivelPrecio.SelectedValue.Length - 3, 3))
                        :1;
                }
                catch
                {
                    lbError2.Visible = true;
                    lbError2.Text = "Debe seleccionar el nivel de precio";
                    cbNivelPrecio.Focus();
                    return;
                }



                if (WebConfigurationManager.AppSettings["PrecioOriginal"].ToString().Equals("1"))
                {
                    try
                    {
                        bool modificarPrecios = PuedeModificarPrecios();
                        decimal descMax = decimal.Parse(txtSaldoDescuento.Text.Replace(",", ""));

                        decimal descuento = 0;
                        try
                        {
                            descuento=decimal.Parse(txtDescuentoItem.Text.Replace(",", "")); }
                        catch {
                            descuento = 0;
                            txtDescuentoItem.Text = "0.00";
                        }

                        if (Convert.ToDecimal(txtPrecioUnitario.Text.Replace(",", "")) <= 1 && cbTipoPedido.SelectedValue != "N")
                        {
                            lbError2.Visible = true;
                            lbError2.Text = "El precio es inválido, por favor revisar";
                            return;
                        }

                        //el monto del descuento no debe ser mayor a descMax
                        //se le agrega el monto del descuento, ya que en este momento descMax ya se le ha restado el descuento
                        if (descuento > (descMax + descuento) && modificarPrecios == false)
                        {
                            lbError2.Visible = true;
                            lbError2.Text = "El valor del descuento no puede ser mayor a Q" + txtDescMax.Text;
                            txtDescuentoItem.Focus();
                            return;
                        }
                        if (descuento < 0 && modificarPrecios == false)
                        {
                            lbError2.Visible = true;
                            lbError2.Text = "El valor del descuento no puede ser menor a cero.";
                            txtDescuentoItem.Focus();
                            return;
                        }

                        
                        else
                        {
                            List<NuevoPrecioProducto> lstProd = ((List<NuevoPrecioProducto>)Session["lstNuevoPrecioProducto"]);
                            //obtenemos el último ingresado
                            NuevoPrecioProducto nuevoProducto = lstProd[lstProd.Count - 1];
                           

                            List<NuevoPrecioProducto> lstProdAux = new List<NuevoPrecioProducto>();
                            for (int i = 0; i < (lstProd.Count - 1); i++)
                            {
                                lstProdAux.Add(lstProd[i]);
                            }
                            if (!Facturacion.ValidarArticuloRegaloAccesorio(lstProdAux, nuevoProducto, (decimal)mFactor) && modificarPrecios==false)
                            {
                                blDescuentosValidos = false;
                                //quitando el producto recientemente agregado
                                Session["lstNuevoPrecioProducto"] = lstProdAux;
                                LimpiarListaArticulos();
                            }
                            else
                            {
                                mPrecioSugerido = nuevoProducto.originalDiscountPrice;
                                lstProd[lstProd.Count - 1].Index = gridArticulos.Rows.Count;
                                mFactor = (double)nuevoProducto.factor;
                                LimpiarListaArticulos();
                            }
                        }


                        
                    }
                    catch
                    { }
                }
                if (blDescuentosValidos)
                {
                    string mBeneficiario = "";
                    if (articulo.Text == "S00013-000" || articulo.Text == "S00012-000" || articulo.Text == "S00011-000" || articulo.Text == "S00010-000")
                    {
                        if (txtNotasTipoVenta.Text.Trim().Length == 0)
                        {
                            lbError2.Visible = true;
                            lbError2.Text = "Debe ingresar el nombre del beneficiario del certificado en la casilla Obs. Promoción.";
                            txtNotasTipoVenta.Focus();
                            return;
                        }

                        mBeneficiario = txtNotasTipoVenta.Text.Trim().ToUpper();
                        txtNotasTipoVenta.Text = "";
                    }

                    List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();
                    q = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                    int mLinea = q.Count();

                    DateTime mFechaOfertaDesde = new DateTime(1980, 1, 1);

                    try
                    {
                        mFechaOfertaDesde = new DateTime(Convert.ToInt32(txtFechaOfertaDesde.Text.Substring(6, 4)), Convert.ToInt32(txtFechaOfertaDesde.Text.Substring(3, 2)), Convert.ToInt32(txtFechaOfertaDesde.Text.Substring(0, 2)));
                    }
                    catch
                    {
                        //Nothing
                    }


                    if (mTipo == "CO") mFactor = 1;
                    double mPrecioBaseLocal = ws.DevuelvePrecioBase(articulo.Text, Convert.ToString(Session["Tienda"]));
                    decimal descuento = 0;
                    try
                    {
                        descuento = decimal.Parse(txtDescuentoItem.Text.Replace(",", ""));
                    }
                    catch
                    {
                        descuento = 0;
                        txtDescuentoItem.Text = "0.00";
                    }
                    string mGel = "";
                    if (cbGel.SelectedItem.Text == "Sí") mGel = " - GARANTÍA ESPECIAL LIMITADA";



                    wsPuntoVenta.PedidoLinea item = new wsPuntoVenta.PedidoLinea();
                    item.PrecioUnitario = Math.Round(Convert.ToDecimal(txtPrecioUnitario.Text.Replace(",", "")), 2, MidpointRounding.AwayFromZero);

                    decimal DescuentoAplicado = Math.Round(Convert.ToDecimal(txtDescuentoItem.Text.Replace(",", "")) > (item.PrecioUnitario / (decimal)mFactor) ? item.PrecioUnitario / (decimal)mFactor : Convert.ToDecimal(txtDescuentoItem.Text.Replace(",", "")),2,MidpointRounding.AwayFromZero);
                    decimal mPrecioFacturar = Convert.ToDecimal(txtTotal.Text.Replace(",", "")) / (decimal)mFactorFinanciera;
                    var PorcentajeDescuento = Convert.ToDecimal(txtTotal.Text.Replace(",", "")) > 0 ?
                                                    Math.Abs(DescuentoAplicado * (decimal)mFactor - Convert.ToDecimal(txtTotal.Text.Replace(",", ""))) < 1
                                                    ? 100 : Math.Round(DescuentoAplicado/mPrecioFacturar,4,MidpointRounding.AwayFromZero)*100
                                                 : 0;
                    DescuentoAplicado = PorcentajeDescuento < 100 ? DescuentoAplicado : DescuentoAplicado * (decimal)mFactor / (decimal)mFactorFinanciera;




                    item.Articulo = articulo.Text;
                    item.Nombre = string.Format("{0}{1}", descripcion.Text.ToUpper(), mGel);

                    item.CantidadPedida = Convert.ToDecimal(txtCantidad.Text.Replace(",", ""));
                    item.PrecioTotal = ((Convert.ToDecimal(txtTotal.Text.Replace(",", "")) / (decimal)mFactor) *(1-PorcentajeDescuento/100)) * (decimal)mFactor;
                    item.Bodega = cbBodega.SelectedValue;
                    item.Localizacion = cbLocalizacion.SelectedItem.Text;
                    item.RequisicionArmado = cbRequisicion.SelectedItem.Text;
                    item.Descripcion = "";
                    item.Comentario = "";
                    item.Estado = "N";
                    item.NumeroPedido = "";
                    item.Oferta = txtOferta.Text;
                    item.FechaOfertaDesde = mFechaOfertaDesde;
                    item.PrecioOriginal = Convert.ToDecimal(txtPrecioOriginal.Text.Replace(",", ""));
                    item.PrecioSugerido = mPrecioSugerido;
                    item.TipoOferta = txtTipoOferta.Text;
                    item.EsDetalleKit = "N";
                    item.PrecioFacturar = mPrecioFacturar;
                    item.PrecioUnitarioDebioFacturar = (Convert.ToDecimal(txtPrecioUnitarioDebioFacturar.Text.Replace(",", "")) / (decimal)mFactor) / item.CantidadPedida;
                    item.Vale = Convert.ToInt32(txtVale1.Text);
                    item.Condicional = txtCondicional.Text;
                    item.ArticuloCondicion = txtArticuloCondicion.Text;
                    item.Autorizacion = Convert.ToInt64(txtAutorizacion.Text);
                    item.PrecioBaseLocal = mPrecioBaseLocal;
                    item.Linea = mLinea;
                    item.Gel = cbGel.SelectedItem.Text;
                    item.PorcentajeDescuento = PorcentajeDescuento;
                    item.Descuento = DescuentoAplicado;
                    item.Beneficiario = mBeneficiario;
                    item.NetoFacturar = Math.Round(((item.PrecioTotal)), 2, MidpointRounding.AwayFromZero);
                    q.Add(item);
                    gridArticulos.DataSource = q;
                    gridArticulos.DataBind();
                    gridArticulos.Visible = true;

                    ViewState["qArticulos"] = q;


                    //cambia los totales visibles en pantalla
                    ValidaTotalPedido();
                    ValidaTipoNivelPrecio();

                    if (articulo.Text.Substring(0, 1) == "S")
                    {
                        int mSumar = 1;
                        DateTime mFechaEntrega = DateTime.Now.Date;

                        if (txtAnio.Text.Trim().Length == 2) txtAnio.Text = string.Format("20{0}", txtAnio.Text);
                        mFechaEntrega = new DateTime(Convert.ToInt32(txtAnio.Text), Convert.ToInt32(cbMes.SelectedValue), Convert.ToInt32(cbDia.SelectedValue));

                        if (mFechaEntrega.DayOfWeek == DayOfWeek.Saturday) mSumar = 2;
                        mFechaEntrega = mFechaEntrega.AddDays(mSumar);

                        cbDia.SelectedValue = mFechaEntrega.Day.ToString();
                        cbMes.SelectedValue = mFechaEntrega.Month.ToString();
                        txtAnio.Text = mFechaEntrega.Year.ToString();
                    }

                    articulo.Text = "";
                    descripcion.Text = "";
                    txtCantidad.Text = "1";
                    txtTotal.Text = "0.00";
                    txtDescuentoItem.Text = "0.00";
                    txtPrecioUnitario.Text = "0.00";
                    txtCodVale.Text = "";
                    txtValorVale.Text = "0.00";
                    txtFacturarItem.Text = "0.00";
                    txtVale1.Text = "0";
                    txtCondicional.Text = "N";
                    txtArticuloCondicion.Text = "";
                    txtAutorizacion.Text = "0";
                    txtCantidad.ReadOnly = false;

                    cbGel.SelectedValue = "No";
                    cbRequisicion.SelectedValue = "No";
                    cbLocalizacion.SelectedValue = "ARMADO";

                    cbBodega.SelectedValue = Convert.ToString(ViewState["BodegaDefault"]);

                    gridVales.Visible = false;
                    lbError2.Text = "";
                    lbError2.Visible = false;
                    tblSolicutudAutorizacion.Visible = false;

                    cbRequisicion.SelectedValue = "No";
                    cbLocalizacion.SelectedValue = "ARMADO";

                    gridArticulos.SelectedIndex = -1;
                    gridArticulosDet.SelectedIndex = -1;

                    if (cbTipoPedido.SelectedValue == "P" || cbTipoPedido.SelectedValue == "S")
                    {
                        string mArticulo = "S00003-000";
                        string mDescripcion = "";

                        if (cbTipoPedido.SelectedValue == "S")
                        {
                            mArticulo = "S00002-000";
                            mDescripcion = "";
                            descripcion.TextMode = TextBoxMode.MultiLine;
                            descripcion.Height = new Unit(121);
                        }

                        articulo.Text = mArticulo;
                        descripcion.Text = mDescripcion;

                        descripcion.Focus();

                    }
                    else
                    {
                        articulo.Focus();
                        descripcion.Height = new Unit(22);
                        descripcion.TextMode = TextBoxMode.SingleLine;
                    }
                }
                else
                {
                    articulo.Text = "";
                    descripcion.Text = "";
                    txtCantidad.Text = "1";
                    txtPrecioUnitario.Text = "0.00";
                    txtTotal.Text = "0.00";
                    txtDescuentoItem.Text = "0.00";
                    txtFacturarItem.Text = "0.00";
                    txtVale1.Text = "0";
                    txtCondicional.Text = "N";
                    txtArticuloCondicion.Text = "";
                    txtAutorizacion.Text = "0";
                    txtCantidad.ReadOnly = false;
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format("Error al agregar el artículo. {0} {1}", ex.Message, m);
            }
        }

        protected void lbAgregarArticulo_Click(object sender, EventArgs e)
        {
            agregarArticulo();
        }

        protected void txtCantidad_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string mMensaje = "";
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                lbError2.Text = "";
                lbError2.Visible = false;

                Int32 mCantidad = 0;

                try
                {
                    mCantidad = Convert.ToInt32(txtCantidad.Text);
                }
                catch
                {
                    txtCantidad.Text = "1";
                    lbError2.Visible = true;
                    lbError2.Text = "Cantidad inválida.";
                    return;
                }

                if (mCantidad <= 0)
                {
                    txtCantidad.Text = "1";
                    lbError2.Visible = true;
                    lbError2.Text = "Cantidad inválida.";
                    return;
                }

                if (!ws.CantidadValida(articulo.Text, txtCantidad.Text, ref mMensaje) && Convert.ToString(Session["Tienda"]) != "F01")
                {
                    txtCantidad.Text = "1";
                    lbError2.Visible = true;
                    lbError2.Text = mMensaje;
                    return;
                }

                CalcularTotalArticulo();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format("Error al validar la cantidad. {0} {1}", ex.Message, m);
            }
        }

        bool PuedeModificarPrecios()
        {
            WebRequest request = WebRequest.Create(string.Format("{0}/modificaprecios/{1}", Convert.ToString(Session["UrlRestServices"]), Convert.ToString(Session["Usuario"])));

            request.Method = "GET";
            request.ContentType = "application/json";

            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            string mModificaPrecios = Convert.ToString(responseString).Replace("\"", "");
            return mModificaPrecios == "S";
        }

        protected void txtPrecioUnitario_TextChanged(object sender, EventArgs e)
        {
            try
            {

                bool modificarPrecios = PuedeModificarPrecios();
                if (!modificarPrecios)
                {
                    var ws = new wsPuntoVenta.wsPuntoVenta();
                    if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                    lbError2.Text = "";
                    lbError2.Visible = false;

                    if (cbTipoPedido.SelectedValue != "N") txtPrecioUnitarioDebioFacturar.Text = txtPrecioUnitario.Text;

                    var qParametrosPrecios = ws.ParametrosModificarPrecios();

                    decimal mPrecioUnitario = Convert.ToDecimal(txtPrecioUnitario.Text.Replace(",", ""));
                    decimal mPrecioUnitarioDebioFacturar = Convert.ToDecimal(txtPrecioUnitarioDebioFacturar.Text.Replace(",", ""));

                    if (mPrecioUnitario > mPrecioUnitarioDebioFacturar)
                    {
                        if (qParametrosPrecios[0].CambiarPrecioHaciaArriba == "S")
                        {
                            decimal mDiferencia = mPrecioUnitario - mPrecioUnitarioDebioFacturar;

                            if (mDiferencia > qParametrosPrecios[0].MaximoPrecioHaciaArriba)
                            {
                                txtPrecioUnitario.Text = mPrecioUnitarioDebioFacturar.ToString();
                                lbError2.Visible = true;
                                lbError2.Text = "El precio modificado es inválido";
                            }
                        }
                        else
                        {
                            txtPrecioUnitario.Text = mPrecioUnitarioDebioFacturar.ToString();
                            lbError2.Visible = true;
                            lbError2.Text = "No está permitido el cambio de precios hacia arriba";
                        }
                    }
                    if (mPrecioUnitario < mPrecioUnitarioDebioFacturar)
                    {
                        if (qParametrosPrecios[0].CambiarPrecioHaciaAbajo == "S")
                        {
                            decimal mDiferencia = mPrecioUnitarioDebioFacturar - mPrecioUnitario;

                            if (mDiferencia > qParametrosPrecios[0].MaximoPrecioHaciaAbajo)
                            {
                                txtPrecioUnitario.Text = mPrecioUnitarioDebioFacturar.ToString();
                                lbError2.Visible = true;
                                lbError2.Text = "El precio modificado es inválido";
                            }
                        }
                        else
                        {
                            txtPrecioUnitario.Text = mPrecioUnitarioDebioFacturar.ToString();
                            lbError2.Visible = true;
                            lbError2.Text = "No está permitido el cambio de precios hacia abajo";
                        }
                    }
                }

                CalcularTotalArticulo();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format("Error al modificar el precio. {0} {1}", ex.Message, m);
            }
        }

        void CalcularTotalArticulo()
        {
            txtTotal.Text = String.Format("{0:0,0.00}", Convert.ToDecimal(txtPrecioUnitario.Text.Replace(",", "")) * Convert.ToInt32(txtCantidad.Text.Replace(",", "")));
            cbGel.Focus();
        }

        protected void txtEnganche_TextChanged(object sender, EventArgs e)
        {
            lbError.Text = "";

            if (cbTipo.SelectedValue != "NR" && txtPedido.Text == "P000000")
            {
                lbError.Text = "En las promociones, debe grabar primero el pedido, luego seleccionarlo y aplicar el enganche.";
                txtEnganche.Text = "0.00";
                return;
            }

            ValidaTipoNivelPrecio();
        }

        protected void lkArticulo_Click(object sender, EventArgs e)
        {
            if (cbTipoPedido.SelectedValue == "N") habilitarBusquedaArticulos();
        }

        void ToolTipPromo()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            ws.Url = General.FiestaNETService;

            var q = ws.DevuelveTipoVenta(cbTipo.SelectedValue);
            txtNotasTipoVenta.ToolTip = q[0].Tooltip;
        }

        protected void cbTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CargaNivelesDePrecio();
                limpiar(true);
                ToolTipPromo();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    //Nothing
                }

                lbError2.Visible = true;
                lbError2.Text = string.Format("Error al cambiar la promoción. {0} {1}", ex.Message, m);
            }
        }

        private void LimpiarListaArticulos()
        {
            List<NuevoPrecioProducto> lstProd = (List<NuevoPrecioProducto>)Session["lstNuevoPrecioProducto"];
            if (lstProd != null)
            {
                lstProd.Remove(lstProd.Find(x => x.Index == -1));
                Session["lstNuevoPrecioProducto"] = lstProd;
                txtSaldoDescuento.Text = String.Format("{0:0,0.00}", Facturacion.ObtenerSaldoDescuento(lstProd));
            }
        }

        protected void lkAgregarArticulo_Click(object sender, EventArgs e)
        {
            agregarArticulo();
            if (cbTipoPedido.SelectedValue == "N") habilitarBusquedaArticulos();
        }

        protected void articulo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                LimpiarListaArticulos();

                if (articulo.Text.Trim().Length == 0) return;
                lbError2.Text = "";
                lbError2.Visible = false;

                string mPrimerCaracter = articulo.Text.Substring(0, 1);

                descripcion.Text = "";
                txtCantidad.Text = "1";
                txtPrecioUnitario.Text = "0.00";
                txtTotal.Text = "0.00";
                txtFacturarItem.Text = "0.00";
                txtDescuentoItem.Text = "0.00";
                txtVale1.Text = "0";
                txtCondicional.Text = "N";
                txtArticuloCondicion.Text = "";

                try
                {
                    int mDigito = Convert.ToInt32(mPrimerCaracter);

                    descripcionArticuloDet.Text = "";
                    codigoArticuloDet.Text = articulo.Text;
                    BuscarArticulo("C");
                }
                catch
                {
                    codigoArticuloDet.Text = "";
                    descripcionArticuloDet.Text = articulo.Text.ToUpper();
                    BuscarArticulo("D");
                }

                if (gridArticulosDet.Rows.Count == 1)
                {
                    gridArticulosDet.SelectedIndex = 0;
                    //seleccionarArticulo();
                }
                if (gridArticulosDet.Rows.Count > 1)
                {
                    tablaBusquedaDet.Visible = true;
                    gridArticulosDet.Visible = true;

                    lbError2.Text = "";
                    lbError2.Visible = false;

                    Label6.Text = "";
                    Label7.Text = "";
                    codigoArticuloDet.Focus();
                }
            }
            catch (Exception ex)
            {
                lbError2.Visible = true;
                lbError2.Text = string.Format("Error al validar el artículo {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }

        protected void lbLocalizacion_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbTipoPedido.SelectedValue == "N")
                {
                    if (gridLocalizaciones.Visible)
                    {
                        gridLocalizaciones.Visible = false;
                        return;
                    }
                    if (articulo.Text.Trim().Length == 0) return;

                    cargarLocalizaciones(articulo.Text.Trim());
                }
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al llamar las localizaciones {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }

        void cargarLocalizaciones(string articulo)
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var q = ws.DevuelveBodegaLocalizacion(articulo);

                gridLocalizaciones.DataSource = q;
                gridLocalizaciones.DataBind();
                gridLocalizaciones.Visible = true;

                if (gridLocalizaciones.Rows.Count > 0)
                {
                    gridLocalizaciones.Focus();
                    gridLocalizaciones.SelectedIndex = 0;
                    LinkButton lkSeleccionar = (LinkButton)gridLocalizaciones.SelectedRow.FindControl("lkSeleccionarLocalizacion");

                    lkSeleccionar.Focus();
                }
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al cargar las localizaciones {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }

        protected void gridLocalizaciones_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridLocalizaciones_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(ViewState["CambiarLocalizacion"]) == "grid")
                {
                    Label lbBodega = (Label)gridArticulos.SelectedRow.FindControl("lbBodega");
                    Label lbLocalizacion = (Label)gridArticulos.SelectedRow.FindControl("lbLocalizacion");

                    lbBodega.Text = gridLocalizaciones.SelectedRow.Cells[3].Text;
                    lbLocalizacion.Text = gridLocalizaciones.SelectedRow.Cells[4].Text;
                }
                else
                {
                    cbBodega.SelectedValue = gridLocalizaciones.SelectedRow.Cells[3].Text;
                    cbLocalizacion.SelectedValue = gridLocalizaciones.SelectedRow.Cells[4].Text;
                }

                ViewState["CambiarLocalizacion"] = null;
                gridLocalizaciones.Visible = false;
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al seleccionar la localización {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }

        protected void lkSeleccionarLocalizacion_Click(object sender, EventArgs e)
        {

        }

        bool pasaValidacionesFactura(ref bool enviarExpedienteBodega)
        {
            try
            {
                if (txtPedido.Text == "P000000")
                {
                    lbError.Text = "Debe seleccionar un pedido para facturar.";
                    return false;
                }
                if (txtFacturado.Text == "S")
                {
                    lbError.Text = string.Format("El pedido {0} ya se encuentra facturado.", txtPedido.Text);
                    return false;
                }
                if (txtNumeroFactura.Text.Trim().Length == 0)
                {
                    lbError.Text = "Debe ingresar el número de factura.";
                    SugerirNumeroFactura();
                    return false;
                }
                if (!cbNumeroFactura.Checked)
                {
                    lbError.Text = "Debe aceptar el número de factura.";
                    cbNumeroFactura.Focus();
                    return false;
                }

                //var ws = new wsPuntoVenta.wsPuntoVenta();
                //if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                //bool mValidarFecha = ws.EsCiudad(Convert.ToString(Session["Tienda"]));

                //if (!mValidarFecha)
                //{
                //    List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();
                //    q = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                //    foreach (var item in q)
                //    {
                //        mValidarFecha = ws.EsCiudad(item.Bodega);
                //    }
                //}

                WebRequest request = WebRequest.Create(string.Format("{0}/bodegadefault/{1}", Convert.ToString(Session["UrlRestServices"]), Convert.ToString(Session["Tienda"])));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                DateTime mFechaEntrega = DateTime.Now.Date;
                string mBodegaDefault = Convert.ToString(responseString).Replace("\"", "");

                try
                {
                    if (txtAnio.Text.Trim().Length == 2) txtAnio.Text = string.Format("20{0}", txtAnio.Text);
                    mFechaEntrega = new DateTime(Convert.ToInt32(txtAnio.Text), Convert.ToInt32(cbMes.SelectedValue), Convert.ToInt32(cbDia.SelectedValue));
                }
                catch
                {
                    lbError.Text = "Debe ingresar una fecha de entrega válida.";
                    cbDia.Focus();
                    return false;
                }

                DateTime mFechaValidar = DateTime.Now.Date.AddDays(2);
                if (mFechaValidar.DayOfWeek == DayOfWeek.Sunday && mFechaValidar.Month != 7 && mFechaValidar.Month != 12) mFechaValidar = mFechaValidar.AddDays(1);

                Label lbBodega = (Label)gridArticulos.Rows[0].FindControl("lbBodega");

                if (lbBodega.Text == Convert.ToString(Session["Tienda"]))
                {
                    mFechaValidar = DateTime.Now.Date;
                }
                else
                {
                    request = WebRequest.Create(string.Format("{0}/diasdespacho?fecha={1}-{2}-{3}&cliente={4}", Convert.ToString(Session["UrlRestServices"]), mFechaEntrega.Year.ToString(), mFechaEntrega.Month.ToString(), mFechaEntrega.Day.ToString(), txtCodigo.Text));

                    request.Method = "GET";
                    request.ContentType = "application/json";

                    response = (HttpWebResponse)request.GetResponse();
                    responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    string mMensaje = Convert.ToString(responseString).Replace("\"", "");
                    if (mMensaje.ToLower().Contains("error"))
                    {
                        lbError.Text = mMensaje;
                        cbDia.Focus();
                        return false;
                    }

                    enviarExpedienteBodega = true;
                }

                if (mFechaEntrega < mFechaValidar)
                {
                    lbError.Text = string.Format("La fecha de entrega mínima admitida es a partir del {0}{1}/{2}{3}/{4}", mFechaValidar.Day < 10 ? "0" : "", mFechaValidar.Day, mFechaValidar.Month < 10 ? "0" : "", mFechaValidar.Month, mFechaValidar.Year);
                    cbDia.Focus();
                    return false;
                }


                return true;
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al validar los datos para facturar {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
                return false;
            }
        }

        protected void lkFacturar_Click(object sender, EventArgs e)
        {
            dsPuntoVenta ds = new dsPuntoVenta(); string mMensajeFactura = "";
            string mMensaje = ""; string mPedido = txtPedido.Text; string mFactura = txtNumeroFactura.Text.ToUpper();

            var ws = new wsPuntoVenta.wsPuntoVenta();
            ws.Url = General.FiestaNETService;
            tblVale.Visible = false;
            try
            {
                bool mEnviarExpedientebodega = false;
                if (!pasaValidacionesFactura(ref mEnviarExpedientebodega))
                    return;
                if (!VerificaMontoInterconsumo())
                    return;

                ValidarDatosCrediticios(mPedido);

                lbInfo.Text = "";
                lbError.Text = "";
                lbError2.Text = "";
                lbError2.Visible = false;

                tblBuscar.Visible = false;
                tablaBusquedaDet.Visible = false;

                gridClientes.Visible = false;
                gridArticulosDet.Visible = false;

                if (txtAnio.Text.Trim().Length == 2) txtAnio.Text = string.Format("20{0}", txtAnio.Text);

                string mAutorizacion = ""; string mQuienAutorizo = "";
                DateTime mFechaEntrega = new DateTime(Convert.ToInt32(txtAnio.Text), Convert.ToInt32(cbMes.SelectedValue), Convert.ToInt32(cbDia.SelectedValue));

                ws.Timeout = 999999999;
                #region "Formar la estructura para canje de vales de descuento (si hubiese)"
                
                List<wsPuntoVenta.ExchangeInfoApiRequest> lVales = new List<wsPuntoVenta.ExchangeInfoApiRequest>();
                if (lstVales.Items.Count > 0)
                {
                    foreach(ListItem x in lstVales.Items)
                    {

                        wsPuntoVenta.ExchangeInfoApiRequest re = new wsPuntoVenta.ExchangeInfoApiRequest
                        {
                            CouponCode = x.Text,
                            CouponValue= decimal.Parse(x.Value),
                            exchangeReferente = Session["Tienda"].ToString(),
                            
                            advertisingCompany = cbEmpresaPromotora.SelectedValue,
                            UserName = Session["Usuario"].ToString()
                        };
                        lVales.Add(re);
                    };
                }
                #endregion
                if (!ws.GrabarFactura(mPedido, ref mFactura, ref mMensaje, Convert.ToString(Session["Usuario"]), lVales.ToArray(), Convert.ToString(Session["Tienda"]), mFechaEntrega, cbEntrega.SelectedValue.ToString(), ref mAutorizacion, ref mQuienAutorizo))
                {
                    lbError.Text = mMensaje;
                    return;
                }

                if (mFechaEntrega >= (DateTime.Now.Date.AddDays(7)) && mEnviarExpedientebodega)
                {
                    Label lbPedido = (Label)gridPedidos.SelectedRow.FindControl("lbPedido");
                    ImprimirExpediente(lbPedido.Text, "B", txtObservacionesEnviar.Text);

                    ws.MarcarEnviadoBodega(lbPedido.Text);
                    mMensaje = string.Format("{0}  Y se envió el expediente a bodega para que se reserven sus artículos.", mMensaje);
                }

                lbInfo.Text = mMensaje;
                mMensajeFactura = mMensaje;

                ViewState["Factura"] = mFactura;
                txtNumeroFactura.Text = mFactura;

                tblInfo.Visible = true;
                lkGrabar.Enabled = false;
                lkFacturar.Enabled = false;
                lkDespachar.Enabled = true;

                txtNumeroFactura.Text = "";
                cbNumeroFactura.Checked = false;

                if (mAutorizacion.Trim().Length > 0)
                {
                    txtQuienAutorizo.Text = mQuienAutorizo;
                    txtNoAutorizacion.Text = mAutorizacion;
                }

                wsPuntoVenta.Factura[] q = new wsPuntoVenta.Factura[1];
                wsPuntoVenta.FacturaLinea[] qLinea = new wsPuntoVenta.FacturaLinea[1];

                mMensaje = "";
                if (!ws.DevuelveImpresionFactura(mFactura, ref mMensaje, ref q, ref qLinea))
                    lbError.Text = mMensaje;

                DataRow row = ds.Factura.NewRow();
                row["NoFactura"] = q[0].NoFactura;
                row["Fecha"] = q[0].Fecha;
                row["Cliente"] = q[0].Cliente;
                row["Nombre"] = q[0].Nombre;
                row["Nit"] = q[0].Nit;
                row["Direccion"] = q[0].Direccion;
                row["Vendedor"] = q[0].Vendedor;
                row["Monto"] = q[0].Monto;
                row["MontoLetras"] = q[0].MontoLetras;
                ds.Factura.Rows.Add(row);

                for (int ii = 0; ii < qLinea.Count(); ii++)
                {
                    DataRow rowDet = ds.FacturaLinea.NewRow();
                    rowDet["NoFactura"] = qLinea[ii].NoFactura;
                    rowDet["Articulo"] = qLinea[ii].Articulo;
                    rowDet["Cantidad"] = qLinea[ii].Cantidad;
                    rowDet["Descripcion"] = qLinea[ii].Descripcion;
                    rowDet["PrecioUnitario"] = qLinea[ii].PrecioUnitario;
                    rowDet["PrecioTotal"] = qLinea[ii].PrecioTotal;
                    ds.FacturaLinea.Rows.Add(rowDet);
                }

                txtFacturado.Text = "S";


                //Grabar factura en Interconsumo
                try
                {
                    WebRequest requestInter = WebRequest.Create(string.Format("{0}/GrabarFacturaInterconsumo/?documento={1}", Convert.ToString(Session["UrlRestServices"]), txtPedido.Text));

                    requestInter.Method = "GET";
                    requestInter.ContentType = "application/json";

                    var responseInter = (HttpWebResponse)requestInter.GetResponse();
                    var responseStringInter = new StreamReader(responseInter.GetResponseStream()).ReadToEnd();

                    dynamic mInterconsumo = JObject.Parse(responseStringInter);
                    mMensaje = mInterconsumo.mensaje;

                    //if (!Convert.ToBoolean(mInterconsumo.exito))
                    //{
                    //    lbError.Text = mMensaje;
                    //    return;
                    //}

                    //lbInfo.Text = string.Format("{0} {1}", lbInfo.Text, mMensaje);
                }
                catch
                {
                    //Nothing
                }

                //Refacturacion
                ReFacturacion(false);
            }
            catch (Exception ex)
            {
                string m = "";

                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    //Nothing
                }

                lbError.Text = string.Format("Error al generar la factura. {0} {1}", ex.Message, m);
                return;
            }


            string mNombreDocumento = string.Format("{0}.pdf", mFactura);
            string mNombreDocumentoGarantia = string.Format("Garantia_{0}.pdf", mFactura);

            using (ReportDocument reporte = new ReportDocument())
            {
                string mFormatoFactura = "rptFacturaPV.rpt";
                if (Convert.ToString(Session["Tienda"]).Length == 3) mFormatoFactura = ws.DevuelveFormatoFactura(Convert.ToString(Session["Tienda"]), mFactura);

                string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mFormatoFactura);
                reporte.Load(p);

                if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                reporte.SetDataSource(ds);
                reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);
            }

            using (ReportDocument reporte = new ReportDocument())
            {
                string mFormato = "rptGarantia.rpt";

                string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mFormato);
                reporte.Load(p);

                reporte.SetParameterValue("factura", mFactura);
                reporte.SetParameterValue("fecha", DateTime.Now.Date);
                reporte.SetParameterValue("cliente", string.Format("{0}  -  {1}", txtCodigo.Text, txtNombre.Text));

                if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                if (File.Exists(@"C:\reportes\" + mNombreDocumentoGarantia)) File.Delete(@"C:\reportes\" + mNombreDocumentoGarantia);

                reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumentoGarantia);
            }

            lkFacturar.Visible = false;
            lkImprimirFactura.Visible = true;
            lkImprimirGarantia.Visible = true;
           

            List<wsPuntoVenta.PedidoLinea> qArticulos = new List<wsPuntoVenta.PedidoLinea>();
            qArticulos = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

            bool mArmado = false;
            foreach (var item in qArticulos)
            {
                if (item.RequisicionArmado == "Sí")
                {
                    if (item.Bodega == "F01" || item.Bodega == "B05")
                    {
                        mArmado = true;
                    }
                    else
                    {
                        if (ws.Existencias(item.Articulo, item.Bodega, item.Localizacion) > 0) mArmado = true;
                    }
                }
            }

            if (mArmado)
            {
                DateTime mFechaMinimaArmado = ws.FechaMinimaArmado(mPedido);

                cbDiaArmado.SelectedValue = mFechaMinimaArmado.Day.ToString();
                cbMesArmado.SelectedValue = mFechaMinimaArmado.Month.ToString();
                txtAnioArmado.Text = mFechaMinimaArmado.Year.ToString();

                cbTipoArmado.SelectedValue = "A";
                txtObsArmado.Text = "";
                liReqArmados.Visible = true;
                txtObsArmado.Focus();

                lbInfo.Text = string.Format("{0} Envíe su requisición de armado en este momento, y luego haga clic en Imprimir Factura e Imprimir Garantía.", mMensajeFactura);
            }
            else
            {
                lbInfo.Text = string.Format("{0} Haga clic en Imprimir Factura e Imprimir Garantía.", mMensajeFactura);
            }
        }

        protected void lkImprimirFactura_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                if (liReqArmados.Visible)
                {
                    lbError.Text = "Debe enviar la requisición de armado antes de imprimir la factura.";
                    txtObsArmado.Focus();
                    return;
                }

                string mFactura = Convert.ToString(ViewState["Factura"]);
                string mNombreDocumento = string.Format("{0}.pdf", mFactura);

                WebRequest request = WebRequest.Create(string.Format("{0}/felactivado/", Convert.ToString(Session["UrlRestServices"])));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string mFelActivado = Convert.ToString(responseString).Replace("\"", "");
                if (mFelActivado == "S")
                {
                    string mReportServer = WebConfigurationManager.AppSettings["ReportServer"].ToString();
                    if (Convert.ToString(Session["Ambiente"]) == "DES" || Convert.ToString(Session["Ambiente"]) == "PRU") mReportServer = WebConfigurationManager.AppSettings["ReportServerPruebas"].ToString();
                    Response.Redirect(string.Format("{0}/Report/FacturaMF_FEL/{1}", mReportServer, mFactura), false);
                }
                else
                {
                    Response.Clear();
                    Response.ContentType = "application/pdf";

                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }

                lkFacturar.Visible = true;
                lkImprimirFactura.Visible = false;
                lkImprimirGarantia.Visible = false;
                tblFactura.Visible = false;

                limpiar(true);
                CargarPedidosCliente();
                lbInfo.Text = string.Format("La factura No. {0} fue impresa exitosamente.", mFactura);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbError.Text = string.Format("Error al imprimir factura {0} {1}", ex.Message, m);
            }
        }

        protected void lkImprimirGarantia_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                string mFactura = Convert.ToString(ViewState["Factura"]);
                string mNombreDocumento = string.Format("Garantia_{0}.pdf", mFactura);

                Response.Clear();
                Response.ContentType = "application/pdf";

                Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                Response.WriteFile(@"C:\reportes\" + mNombreDocumento);

                HttpContext.Current.ApplicationInstance.CompleteRequest();

                lkFacturar.Visible = true;
                lkImprimirFactura.Visible = false;
                lkImprimirGarantia.Visible = false;
                tblFactura.Visible = false;

                limpiar(true);
                CargarPedidosCliente();
                lbInfo.Text = string.Format("La garantía Garantia_{0}.pdf fue impresa exitosamente.", mFactura);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbError.Text = string.Format("Error al imprimir garantía {0} {1}", ex.Message, m);
            }
        }

        protected void lkRefacturacion_Click(object sender, EventArgs e)
        {
            ReFacturacion(true);
        }

        void ReFacturacion(bool mensaje)
        {
            WebRequest request = WebRequest.Create(string.Format("{0}/Refacturacion/?pedido={1}&refacturacion={2}&usuario={3}", Convert.ToString(Session["UrlRestServices"]), txtPedido.Text, cbRefacturacion.SelectedValue.ToString(), Convert.ToString(Session["Usuario"])));

            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = 0;

            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            string mRespuesta = responseString.ToString().Replace("\"", "");

            if (mRespuesta.ToLower().Contains("error"))
            {
                lbError.Text = mRespuesta;
                return;
            }

            if (mensaje)
            {
                lbInfo.Text = "";
                lbError.Text = "";

                lbInfo.Text = mRespuesta;
            }
        }

        bool pasaValidacionesDespacho()
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (txtPedido.Text == "P000000")
                {
                    lbError.Text = "Debe seleccionar un pedido para despachar.";
                    return false;
                }
                if (!ws.PedidoFacturado(txtPedido.Text))
                {
                    lbError.Text = string.Format("El pedido {0} debe estar facturado para poderlo despachar.", txtPedido.Text);
                    return false;
                }
                if (ws.PedidoDespachado(txtPedido.Text))
                {
                    lbError.Text = string.Format("El pedido {0} ya se encuentra despachado.", txtPedido.Text);
                    return false;
                }
                if (!cbNumeroDespacho.Checked)
                {
                    lbError.Text = "Debe aceptar el número de envío.";
                    cbNumeroDespacho.Focus();
                    return false;
                }
                if (cbTransportista.SelectedValue == "0")
                {
                    lbError.Text = "Debe seleccionar el transportista.";
                    cbTransportista.Focus();
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al validar los datos para despachar {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
                return false;
            }
        }

        protected void lkDespachar_Click(object sender, EventArgs e)
        {
            string mm = " uno ";
            string mMensaje = "";

            try
            {
                if (liReqArmados.Visible)
                {
                    lbError.Text = "Debe enviar la requisición de armado antes de generar el envío.";
                    txtObsArmado.Focus();
                    return;
                }

                if (!pasaValidacionesDespacho()) return;

                lbInfo.Text = "";
                lbError.Text = "";
                lbError2.Text = "";
                lbError2.Visible = false;

                tblBuscar.Visible = false;
                tablaBusquedaDet.Visible = false;

                gridClientes.Visible = false;
                gridArticulosDet.Visible = false;

                string mTienda = Convert.ToString(Session["Tienda"]);
                string mDespacho = txtNumeroDespacho.Text.ToUpper();
                if (mTienda.Length > 4) mTienda = "F01";

                mm += " dos ";
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.GrabarDespacho(Convert.ToString(ViewState["Factura"]), mDespacho, ref mMensaje, Convert.ToString(Session["Usuario"]), txtObservacionesDespacho.Text, mTienda, cbTransportista.SelectedValue))
                {
                    lbError.Text = mMensaje;

                    if (mMensaje.Contains("armado"))
                    {
                        DateTime mFechaMinimaArmado = ws.FechaMinimaArmado(txtPedido.Text);

                        cbDiaArmado.SelectedValue = mFechaMinimaArmado.Day.ToString();
                        cbMesArmado.SelectedValue = mFechaMinimaArmado.Month.ToString();
                        txtAnioArmado.Text = mFechaMinimaArmado.Year.ToString();

                        cbTipoArmado.SelectedValue = "A";
                        txtObsArmado.Text = "";
                        liReqArmados.Visible = true;
                        txtObsArmado.Focus();
                    }

                    return;
                }
                mm += " tres ";
                lbInfo.Text = mMensaje;

                txtNumeroDespacho.Text = "";
                cbNumeroDespacho.Checked = false;
                txtObservacionesDespacho.Text = "";
                tblDespacho.Visible = false;
                lkDespachar.Enabled = false;

                DataSet dsInfo = new DataSet();
                wsPuntoVenta.Despacho[] q = new wsPuntoVenta.Despacho[1];
                wsPuntoVenta.DespachoLinea[] qLinea = new wsPuntoVenta.DespachoLinea[1];
                mm += " cuatro ";
                mMensaje = "";
                if (!ws.DevuelveImpresionDespacho(mDespacho, Convert.ToString(ViewState["Factura"]), ref mMensaje, ref q, ref qLinea, ref dsInfo))
                {
                    lbError.Text = mMensaje;
                }
                mm += " cinco ";
                dsPuntoVenta ds = new dsPuntoVenta();

                DataRow row = ds.Despacho.NewRow();
                row["NoDespacho"] = q[0].NoDespacho;
                row["Fecha"] = q[0].Fecha;
                row["Cliente"] = q[0].Cliente;
                row["Nombre"] = q[0].Nombre;
                row["Direccion"] = q[0].Direccion;
                row["Indicaciones"] = q[0].Indicaciones;
                row["Telefonos"] = q[0].Telefonos;
                row["FormaPago"] = q[0].FormaPago;
                row["Tienda"] = q[0].Tienda;
                row["Vendedor"] = q[0].Vendedor;
                row["NombreVendedor"] = q[0].NombreVendedor;
                row["Factura"] = q[0].Factura;
                row["Observaciones"] = q[0].Observaciones;
                row["Notas"] = q[0].Notas;
                row["Transportista"] = q[0].Transportista;
                row["NombreTransportista"] = q[0].NombreTransportista;
                ds.Despacho.Rows.Add(row);
                mm += " seis ";
                for (int ii = 0; ii < qLinea.Count(); ii++)
                {
                    DataRow rowDet = ds.DespachoLinea.NewRow();
                    rowDet["NoDespacho"] = qLinea[ii].NoDespacho;
                    rowDet["Articulo"] = qLinea[ii].Articulo;
                    rowDet["Cantidad"] = qLinea[ii].Cantidad;
                    rowDet["Descripcion"] = qLinea[ii].Descripcion;
                    rowDet["Bodega"] = qLinea[ii].Bodega;
                    rowDet["Localizacion"] = qLinea[ii].Localizacion;
                    ds.DespachoLinea.Rows.Add(rowDet);
                }
                mm += " siete ";
                tblDespacho.Visible = false;
                txtDespachado.Text = "S";

                //Response.Write("<script>window.open('despacho.aspx', '_blank');</script>");

                using (ReportDocument reporte = new ReportDocument())
                {
                    string p = (Request.PhysicalApplicationPath + "reportes/rptDespachoPV.rpt");
                    reporte.Load(p);
                    mm += " ocho ";
                    string mNombreDocumento = string.Format("{0}.pdf", mDespacho);
                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                    reporte.SetDataSource(ds);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                    mm += " nueve ";
                }

                ScriptManager.RegisterStartupScript(this, typeof(Page), "jsKeys", "javascript:Forzar();", true);
                limpiar(true);
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al generar el envío. {0} {1} {2} {3}", mm, mMensaje, ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
                return;
            }
        }

        protected void lkNuevoPedido_Click(object sender, EventArgs e)
        {
            limpiar(true);
            mostrarBusquedaCliente();
        }

        protected void lkNumeroFactura_Click(object sender, EventArgs e)
        {
            SugerirNumeroFactura();
        }

        void SugerirNumeroFactura()
        {
            try
            {
                txtNumeroFactura.Text = "";
                cbNumeroFactura.Checked = false;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mTienda = Convert.ToString(Session["Tienda"]);
                if (mTienda.Length > 4) mTienda = "F01";

                string mTipo = ws.DevuelveTipoNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                string mFactura = ws.NumeroFactura(mTienda, mTipo);

                txtNumeroFactura.Text = mFactura;

                WebRequest request = WebRequest.Create(string.Format("{0}/felactivado/", Convert.ToString(Session["UrlRestServices"])));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string mFelActivado = Convert.ToString(responseString).Replace("\"", "");
                if (mFelActivado == "S")
                {
                    txtNumeroFactura.Text = "FAC. ELECTRONICA";
                    cbNumeroFactura.Checked = true;
                }
                else
                {
                    if (mFactura == "No disponible")
                    {
                        txtNumeroFactura.Enabled = false;
                    }
                    else
                    {
                        //lbError.Text = "";
                        lbError2.Text = "";
                        lbError2.Visible = false;
                        txtNumeroFactura.Enabled = true;
                    }

                    cbNumeroFactura.Focus();
                }
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al sugerir el número de factura. {0} {1}", ex.Message, ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")));
                return;
            }
        }

        protected void lkNumeroEnvio_Click(object sender, EventArgs e)
        {
            SugerirNumeroDespacho();
        }

        void SugerirNumeroDespacho()
        {
            try
            {
                txtNumeroDespacho.Text = "";
                txtObservacionesDespacho.Text = "";
                cbNumeroDespacho.Checked = false;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mTienda = Convert.ToString(Session["Tienda"]);
                if (mTienda.Length > 4) mTienda = "F01";

                string mDespacho = ws.NumeroDespacho(mTienda);

                txtNumeroDespacho.Text = mDespacho;
                cbNumeroDespacho.Focus();
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al sugerir el número de envío. {0} {1}", ex.Message, ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")));
                return;
            }
        }

        protected void cbTipoPedido_SelectedIndexChanged(object sender, EventArgs e)
        {
            string mTipoPedido = cbTipoPedido.SelectedValue;
            limpiar(true);
            cbTipoPedido.SelectedValue = mTipoPedido;

            if (cbTipoPedido.SelectedValue == "N") return;

            cbTipo.SelectedValue = "NR";
            cbFinanciera.SelectedValue = "1";

            CargaNivelesDePrecio();
            cbNivelPrecio.SelectedValue = "ContadoEfect";

            cbRequisicion.SelectedValue = "No";
            if (cbTipoPedido.SelectedValue == "A")
            {
                articulo.Text = "AIA001-000";
                cbRequisicion.SelectedValue = "Sí";
                descripcion.Text = "Ingrese aquí la descripción del armado.";
            }
            if (cbTipoPedido.SelectedValue == "F" || cbTipoPedido.SelectedValue == "R")
            {
                articulo.Text = "S00002-000";
                descripcion.Text = "Ingrese aquí la descripción del flete o repuesto.";
            }
            if (cbTipoPedido.SelectedValue == "P")
            {
                articulo.Text = "S00003-000";
                descripcion.Text = "";
            }
            if (cbTipoPedido.SelectedValue == "S")
            {
                articulo.Text = "S00002-000";
                descripcion.Text = "";
            }

            cbTipo.Enabled = false;
            cbFinanciera.Enabled = false;
            cbNivelPrecio.Enabled = false;
            descripcion.ReadOnly = false;
            txtPrecioUnitario.ReadOnly = false;
            txtPrecioUnitario.AutoPostBack = true;
            cbRequisicion.Enabled = false;
            cbLocalizacion.Enabled = false;
            lkVale.Visible = false;
            lkSolicitar.Visible = false;

            cbLocalizacion.SelectedValue = "ARMADO";
            cbBodega.SelectedValue = Convert.ToString(cbTienda.SelectedValue);

            descripcion.Focus();
        }

        protected void cbTienda_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaVendedores();
        }

        void CargaVendedores()
        {
            cbVendedor.SelectedValue = null;

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveVendedoresTienda(cbTienda.SelectedValue).ToList();

            cbVendedor.DataSource = q;
            cbVendedor.DataTextField = "Nombre";
            cbVendedor.DataValueField = "Vendedor";
            cbVendedor.DataBind();
        }

        void CargarMedios()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveMedios();

            cbMedio.DataSource = q;
            cbMedio.DataTextField = "Descripcion";
            cbMedio.DataValueField = "Medio";
            cbMedio.DataBind();

            if (Convert.ToString(Session["Tienda"]) == "F01") cbMedio.SelectedValue = "1";
        }

        protected void lkReImprimirFactura_Click(object sender, EventArgs e)
        {
            lbError.Text = "";
            lbError2.Text = "";
            lbError2.Visible = false;

            string mMensaje = "";

            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                wsPuntoVenta.Factura[] q = new wsPuntoVenta.Factura[1];
                wsPuntoVenta.FacturaLinea[] qLinea = new wsPuntoVenta.FacturaLinea[1];

                mMensaje = "";
                if (!ws.DevuelveImpresionFactura(txtFacturaReImprimir.Text, ref mMensaje, ref q, ref qLinea))
                {
                    lbError.Text = mMensaje;
                    return;
                }

                dsPuntoVenta ds = new dsPuntoVenta();

                DataRow row = ds.Factura.NewRow();
                row["NoFactura"] = q[0].NoFactura;
                row["Fecha"] = q[0].Fecha;
                row["Cliente"] = q[0].Cliente;
                row["Nombre"] = q[0].Nombre;
                row["Nit"] = q[0].Nit;
                row["Direccion"] = q[0].Direccion;
                row["Vendedor"] = q[0].Vendedor;
                row["Monto"] = q[0].Monto;
                row["MontoLetras"] = q[0].MontoLetras;
                ds.Factura.Rows.Add(row);

                for (int ii = 0; ii < qLinea.Count(); ii++)
                {
                    DataRow rowDet = ds.FacturaLinea.NewRow();
                    rowDet["NoFactura"] = qLinea[ii].NoFactura;
                    rowDet["Articulo"] = qLinea[ii].Articulo;
                    rowDet["Cantidad"] = qLinea[ii].Cantidad;
                    rowDet["Descripcion"] = qLinea[ii].Descripcion;
                    rowDet["PrecioUnitario"] = qLinea[ii].PrecioUnitario;
                    rowDet["PrecioTotal"] = qLinea[ii].PrecioTotal;
                    ds.FacturaLinea.Rows.Add(rowDet);
                }

                using (ReportDocument reporte = new ReportDocument())
                {
                    string mFormatoFactura = "rptFacturaPV.rpt";
                    if (Convert.ToString(Session["Tienda"]).Length == 3) mFormatoFactura = ws.DevuelveFormatoFactura(Convert.ToString(Session["Tienda"]), txtFacturaReImprimir.Text);

                    string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mFormatoFactura);
                    reporte.Load(p);

                    string mNombreDocumento = string.Format("{0}.pdf", txtFacturaReImprimir.Text);

                    reporte.SetDataSource(ds);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }

                lkFacturar.Enabled = false;
                tblFactura.Visible = false;
                tblDespacho.Visible = true;

                ScriptManager.RegisterStartupScript(this, typeof(Page), "jsKeys", "javascript:Forzar();", true);
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al re-imprimir la factura {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }

        protected void lkReImprimirDespacho_Click(object sender, EventArgs e)
        {
            lbError.Text = "";
            lbError2.Text = "";
            lbError2.Visible = false;

            string mMensaje = "";

            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                DataSet dsInfo = new DataSet();
                wsPuntoVenta.Despacho[] q = new wsPuntoVenta.Despacho[1];
                wsPuntoVenta.DespachoLinea[] qLinea = new wsPuntoVenta.DespachoLinea[1];

                mMensaje = "";
                if (!ws.DevuelveImpresionDespacho(txtDespachoReImprimir.Text, txtFacturaReImprimir.Text, ref mMensaje, ref q, ref qLinea, ref dsInfo))
                {
                    lbError.Text = mMensaje;
                    return;
                }

                dsPuntoVenta ds = new dsPuntoVenta();

                DataRow row = ds.Despacho.NewRow();
                row["NoDespacho"] = q[0].NoDespacho;
                row["Fecha"] = q[0].Fecha;
                row["Cliente"] = q[0].Cliente;
                row["Nombre"] = q[0].Nombre;
                row["Direccion"] = q[0].Direccion;
                row["Indicaciones"] = q[0].Indicaciones;
                row["Telefonos"] = q[0].Telefonos;
                row["FormaPago"] = q[0].FormaPago;
                row["Tienda"] = q[0].Tienda;
                row["Vendedor"] = q[0].Vendedor;
                row["NombreVendedor"] = q[0].NombreVendedor;
                row["Factura"] = q[0].Factura;
                row["Observaciones"] = q[0].Observaciones;
                row["Notas"] = q[0].Notas;
                ds.Despacho.Rows.Add(row);

                for (int ii = 0; ii < qLinea.Count(); ii++)
                {
                    DataRow rowDet = ds.DespachoLinea.NewRow();
                    rowDet["NoDespacho"] = qLinea[ii].NoDespacho;
                    rowDet["Articulo"] = qLinea[ii].Articulo;
                    rowDet["Cantidad"] = qLinea[ii].Cantidad;
                    rowDet["Descripcion"] = qLinea[ii].Descripcion;
                    rowDet["Bodega"] = qLinea[ii].Bodega;
                    rowDet["Localizacion"] = qLinea[ii].Localizacion;
                    ds.DespachoLinea.Rows.Add(rowDet);
                }

                using (ReportDocument reporte = new ReportDocument())
                {
                    string p = (Request.PhysicalApplicationPath + "reportes/rptDespachoPV.rpt");
                    reporte.Load(p);

                    string mNombreDocumento = string.Format("{0}.pdf", txtDespachoReImprimir.Text);

                    reporte.SetDataSource(ds);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }

                ScriptManager.RegisterStartupScript(this, typeof(Page), "jsKeys", "javascript:Forzar();", true);
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al imprimir el envío. {0} {1} {2}", ex.Message, mMensaje, ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")));
                return;
            }
        }

        protected void lkReImprimir_Click(object sender, EventArgs e)
        {
            liReImprimir.Visible = !liReImprimir.Visible;
        }

        protected void lkVale_Click(object sender, EventArgs e)
        {
            if (txtCodigo.Text.Trim().Length == 0)
            {
                lbError2.Visible = true;
                lbError2.Text = "Debe seleccionar un cliente.";
                return;
            }

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveValesCliente(txtCodigo.Text);

            if (q.Length == 0)
            {
                gridVales.Visible = false;
                lbError2.Visible = true;
                lbError2.Text = string.Format("El cliente {0} no tiene certificados disponibles.", txtCodigo.Text);
                return;
            }

            gridVales.DataSource = q;
            gridVales.DataBind();
            gridVales.Visible = true;
            gridVales.SelectedIndex = -1;

            lbError.Text = "";
            lbError2.Text = "";
            lbError2.Visible = false;

            if (q.Count() == 1) gridVales.SelectedIndex = 0;
            if (q.Count() > 0)
            {
                gridVales.Focus();
                gridVales.SelectedIndex = 0;
                LinkButton lkSeleccionar = (LinkButton)gridVales.SelectedRow.FindControl("lkSeleccionarVale");

                lkSeleccionar.Focus();
            }
        }

        protected void lkSeleccionarVale_Click(object sender, EventArgs e)
        {

        }

        protected void gridVales_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridVales_SelectedIndexChanged(object sender, EventArgs e)
        {
            double mTotal = Convert.ToDouble(txtTotal.Text.Replace(",", ""));

            if (mTotal < 2000)
            {
                gridVales.Visible = false;
                lbError2.Visible = true;
                lbError2.Text = "Para poder aplicar el certificado, la compra debe ser mayor a Q2,000.00";
                return;
            }
            if (descripcion.Text.Trim().Length == 0)
            {
                gridVales.Visible = false;
                lbError2.Visible = true;
                lbError2.Text = "Debe seleccionar un artículo al que se le aplicará el certificado.";
                return;
            }
            for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
            {
                Label lbVale = (Label)gridArticulos.Rows[ii].FindControl("lbVale");

                if (lbVale.Text == gridVales.SelectedRow.Cells[1].Text)
                {
                    gridVales.Visible = false;
                    lbError2.Visible = true;
                    lbError2.Text = string.Format("El certificado {0} ya está siendo utilizado en el artículo {1}.", gridVales.SelectedRow.Cells[1].Text, gridArticulos.Rows[ii].Cells[1].Text);
                    return;
                }
                if (Convert.ToInt32(lbVale.Text) > 0)
                {
                    gridVales.Visible = false;
                    lbError2.Visible = true;
                    lbError2.Text = "Solo está permitido aplicar un certificado por compra.";
                    return;
                }
            }

            string mVigente = gridVales.SelectedRow.Cells[4].Text;

            if (mVigente == "No")
            {
                gridVales.Visible = false;
                lbError2.Visible = true;
                lbError2.Text = "El certificado seleccionado ya no se encuentra vigente.";
                return;
            }

            gridVales.Visible = false;
            lbError2.Visible = false;
            lbError2.Text = "";

            txtVale1.Text = gridVales.SelectedRow.Cells[1].Text;
            descripcion.Text = string.Format("** Certificado por Q200 desc. ** {0}", descripcion.Text);
            txtPrecioUnitario.Text = String.Format("{0:0,0.00}", Convert.ToDecimal(txtPrecioUnitario.Text.Replace(",", "")) - (200 / Convert.ToInt32(txtCantidad.Text.Replace(",", ""))));
            CalcularTotalArticulo();
        }

        void CargarPedidosCliente()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelvePedidosCliente(txtCodigo.Text);

            if (q.Length == 0)
            {
                gridPedidos.Visible = false;
            }
            else
            {
                gridPedidos.DataSource = q;
                gridPedidos.DataBind();
                gridPedidos.Visible = true;
                gridPedidos.SelectedIndex = -1;
            }
        }

        void FacturarPedido(string pedido)
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            if (ws.PedidoFacturado(pedido))
            {
                lbError.Text = "Este pedido ya se encuentra facturado";
                return;
            }

            SeleccionarPedido();

            tblInfo.Visible = true;
            lkGrabar.Visible = false;
            lkVale.Visible = false;
            
            lkSolicitar.Visible = false;

            if (lbError.Text.Trim().Length == 0)
            {
                
                lkFacturar.Visible = true;
                lkFacturar.Enabled = true;
            }
            
            tblFactura.Visible = true;

            trGrabarPedido.Visible = false;
            lkImprimirFactura.Visible = false;
            lkImprimirGarantia.Visible = false;
            lkImprimirVale.Visible = false;
             SugerirNumeroFactura();
            if (!txtDescuentosVales.Text.Equals("0") && !txtDescuentosVales.Text.Equals("0.00"))
            {
                lkCanjeVales.Visible = true;
                lkFacturar.Visible = false;
            }
            else
            {
                lkCanjeVales.Visible = false;
                lkFacturar.Visible = true;
            } 
        }

        protected void gridPedidos_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //ValidarSesion();

                string mMensaje = "";
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                lbInfo.Text = "";
                lbError.Text = "";

                Label lbPedido = (Label)gridPedidos.SelectedRow.FindControl("lbPedido");
                Label lbFinanciera = (Label)gridPedidos.SelectedRow.FindControl("lbFinanciera");

                string mPedido = lbPedido.Text;
                string mEstado = ws.DevuelveEstadoPedido(mPedido);
                DateTime mFechaMinimaArmado = ws.FechaMinimaArmado(mPedido);

                switch (ViewState["AccionPedido"].ToString())
                {
                    case "Eliminar":
                        if (ws.PedidoFacturado(mPedido))
                        {
                            lbError.Text = "Este pedido se encuentra facturado, no es posible eliminarlo";
                            return;
                        }
                        if (ws.FacturaDespachada(mPedido))
                        {
                            lbError.Text = "La factura de este pedido se encuentra despachada, no es posible eliminarlo";
                            return;
                        }

                        if (ws.EliminarPedido(ref mMensaje, mPedido))
                        {
                            lbInfo.Text = mMensaje;
                            CargarPedidosCliente();
                        }
                        else
                        {
                            lbError.Text = mMensaje;
                            return;
                        }
                        break;
                    case "Seleccionar":
                        SeleccionarPedido();
                        btnActualizarEntrega.Visible = false;

                        if (ws.PedidoFacturado(mPedido))
                        {
                            lbError.Text = "Se le informa que este pedido ya está facturado.";
                            lkGrabar.Visible = false;
                            lkVale.Visible = false;
                            lkSolicitar.Visible = false;
                            btnActualizarEntrega.Visible = true;
                            //lkRefacturacion.Visible = true;
                        }
                        if (ws.FacturaDespachada(mPedido))
                        {
                            lbError.Text = "Se le informa que la factura de este pedido ya está despachada.";
                            lkGrabar.Visible = false;
                            lkVale.Visible = false;
                            lkSolicitar.Visible = false;
                            btnActualizarEntrega.Visible = false;
                            //lkRefacturacion.Visible = true;
                        }

                        break;
                    case "Facturar":
                        Session["Pedido"] = mPedido;

                        if (Convert.ToString(Session["Tienda"]) == "F01")
                        {
                            Response.Redirect("administracion.aspx");
                        }
                        else
                        {
                            FacturarPedido(mPedido);
                        }
                        break;

                    case "Despachar":
                        if (!ws.PedidoFacturado(mPedido))
                        {
                            lbError.Text = "Este pedido no se encuentra facturado";
                            return;
                        }
                        if (ws.FacturaDespachada(mPedido))
                        {
                            lbError.Text = "La factura de este pedido ya se encuentra despachada";
                            return;
                        }

                        SeleccionarPedido();
                        txtFacturado.Text = "S";

                        tblInfo.Visible = true;
                        lkGrabar.Visible = false;
                        lkVale.Visible = false;
                        lkSolicitar.Visible = false;

                        lkFacturar.Enabled = false;
                        tblFactura.Visible = false;
                        tblDespacho.Visible = true;
                        lkDespachar.Enabled = true;

                        if (lbError.Text.Contains("El vendedor de este pedido no está disponible")) lbError.Text = "";

                        if (cbTipoPedido.SelectedValue == "N")
                        {
                            string mFactura = ws.DevuelveFacturaDePedido(mPedido);

                            ViewState["Factura"] = mFactura;
                            SugerirNumeroDespacho();

                            var qTransportistas = ws.DevuelveTransportistas(Convert.ToString(Session["Tienda"]), "");

                            DataTable dt = new DataTable("transportistas");
                            DataTable dt2 = new DataTable("transportistas");

                            dt.Columns.Add(new DataColumn("Nombre", typeof(System.String)));
                            dt.Columns.Add(new DataColumn("Transportista", typeof(System.String)));
                            dt2.Columns.Add(new DataColumn("Nombre", typeof(System.String)));
                            dt2.Columns.Add(new DataColumn("Transportista", typeof(System.String)));

                            foreach (var item in qTransportistas)
                            {
                                DataRow row = dt.NewRow();
                                row["Nombre"] = item.Nombre;
                                row["Transportista"] = item.Transportista;
                                dt.Rows.Add(row);
                            }

                            DataRow[] rows = dt.Select("", "Nombre");

                            for (int ii = 0; ii < rows.Length; ii++)
                            {
                                DataRow row = dt2.NewRow();
                                row["Nombre"] = rows[ii]["Nombre"];
                                row["Transportista"] = rows[ii]["Transportista"];
                                dt2.Rows.Add(row);
                            }

                            cbTransportista.DataSource = dt2;
                            cbTransportista.DataTextField = "Nombre";
                            cbTransportista.DataValueField = "Transportista";
                            cbTransportista.DataBind();

                            //Valido si se debe generar requisición de armado
                            mMensaje = "";
                            if (ws.RequiereArmadoAntesDespachar(mPedido, ref mMensaje))
                            {
                                cbDiaArmado.SelectedValue = mFechaMinimaArmado.Day.ToString();
                                cbMesArmado.SelectedValue = mFechaMinimaArmado.Month.ToString();
                                txtAnioArmado.Text = mFechaMinimaArmado.Year.ToString();

                                cbTipoArmado.SelectedValue = "A";
                                txtObsArmado.Text = "";
                                liReqArmados.Visible = true;
                                txtObsArmado.Focus();

                                lbError.Text = mMensaje;
                            }

                            cbTransportista.SelectedValue = "0";
                            cbNumeroDespacho.Focus();
                        }
                        else
                        {
                            limpiar(true);
                            lbError.Text = "Este tipo de pedido no genera despacho";
                            lbError2.Text = "Este tipo de pedido no genera despacho";
                        }
                        break;
                    case "Interconsumo":
                        WebRequest request = WebRequest.Create(string.Format("{0}/precios/?pedido={1}", Convert.ToString(Session["UrlRestServices"]), mPedido));

                        request.Method = "GET";
                        request.ContentType = "application/json";

                        var response = (HttpWebResponse)request.GetResponse();
                        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                        Clases.Precios mRetorna = new Clases.Precios();
                        mRetorna = JsonConvert.DeserializeObject<Clases.Precios>(responseString);

                        if (!mRetorna.RetornaExito[0].exito)
                        {
                            lbError.Text = mRetorna.RetornaExito[0].mensaje;
                            return;
                        }

                        limpiar(true);
                        CargarPedidosCliente();
                        lbInfo.Text = mRetorna.RetornaExito[0].mensaje;

                        break;
                    case "Solicitud":
                        ImprimirSolicitud(mPedido, lbFinanciera.Text, false, "", "");
                        break;
                    case "Pagaré":
                        ImprimirPagare(mPedido, lbFinanciera.Text);
                        break;
                    case "Expediente":
                        ImprimirExpediente(mPedido, "N", "");
                        break;
                    case "ExpedienteEnviar":
                        if (!ws.PedidoFacturado(mPedido))
                        {
                            lbError.Text = "Este pedido NO se encuentra facturado, no es posible continuar";
                            return;
                        }

                        txtObservacionesEnviar.Text = "";
                        liEnviarExpediente.Visible = true;
                        lkEnviarExpedienteOficina.Visible = false;
                        lkEnviarExpedienteVendedor.Visible = true;
                        lkEnviarArmados.Visible = false;
                        lkEnviarReqTransportista.Visible = false;
                        txtObservacionesEnviar.Focus();
                        break;
                    case "ReqTransportista":
                        if (mEstado != "F")
                        {
                            lbError.Text = "El pedido debe estar facturado";
                            return;
                        }

                        txtObservacionesEnviar.Text = "";
                        liEnviarExpediente.Visible = true;
                        lkEnviarExpedienteOficina.Visible = false;
                        lkEnviarExpedienteVendedor.Visible = false;
                        lkEnviarArmados.Visible = false;
                        lkEnviarReqTransportista.Visible = true;
                        txtObservacionesEnviar.Focus();
                        break;
                    case "ReqArmados":
                        if (mEstado != "F")
                        {
                            lbError.Text = "El pedido debe estar facturado";
                            return;
                        }

                        SeleccionarPedido();

                        lkGrabar.Visible = false;
                        lkVale.Visible = false;
                        lkSolicitar.Visible = false;

                        cbDiaArmado.SelectedValue = mFechaMinimaArmado.Day.ToString();
                        cbMesArmado.SelectedValue = mFechaMinimaArmado.Month.ToString();
                        txtAnioArmado.Text = mFechaMinimaArmado.Year.ToString();

                        cbTipoArmado.SelectedValue = "A";
                        txtObsArmado.Text = "";
                        liReqArmados.Visible = true;
                        txtObsArmado.Focus();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error en el pedido. {0} {1}", ex.Message, m);
            }
        }

        void ImprimirSolicitud(string pedido, string financiera, bool EnviarCarolyn, string vendedorPedido, string tiendaPedido)
        {
            try
            {
                lbError.Text = "";
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (ws.EsBanco(financiera))
                {
                    ValidarDatosCrediticios(pedido);

                    if (financiera == "12")
                    {
                        try
                        {
                            lbInfo.Text = "";
                            lbError.Text = "";
                            Response.Redirect(string.Format("{0}/Report/SolicitudAtid?id={1}", General.PortalService, pedido), false);
                        }
                        catch (Exception ex)
                        {
                            lbError.Text = CatchClass.ExMessage(ex, "pedidos", "ImprimirSolicitud");
                        }

                        return;
                    }

                    string mMensaje = "";
                    if (!ws.ValidarTransaccionEnLinea("P", pedido, ref mMensaje))
                    {
                        lbError.Text = mMensaje;
                        return;
                    }

                    var q = ws.DevuelveSolicitudCredito("S", pedido);

                    using (ReportDocument reporte = new ReportDocument())
                    {
                        WebRequest request = WebRequest.Create(string.Format("{0}/SolicitudPagare/{1}", Convert.ToString(Session["UrlRestServices"]), financiera));

                        request.Method = "GET";
                        request.ContentType = "application/json";

                        var response = (HttpWebResponse)request.GetResponse();
                        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                        string[] mReportes = Convert.ToString(responseString).Replace("\"", "").Split(new string[] { "," }, StringSplitOptions.None);

                        string p = (Request.PhysicalApplicationPath + string.Format("reportes/{0}", mReportes[0]));
                        reporte.Load(p);

                        string mNombreDocumento = string.Format("Solicitud_{0}.pdf", pedido);

                        reporte.SetParameterValue("PrimerNombre", q[0].PrimerNombre);
                        reporte.SetParameterValue("SegundoNombre", q[0].SegundoNombre);
                        reporte.SetParameterValue("TercerNombre", q[0].TercerNombre);
                        reporte.SetParameterValue("PrimerApellido", q[0].PrimerApellido);
                        reporte.SetParameterValue("SegundoApellido", q[0].SegundoApellido);
                        reporte.SetParameterValue("ApellidoCasada", q[0].ApellidoCasada);
                        reporte.SetParameterValue("DPI", q[0].DPI);
                        reporte.SetParameterValue("FechaNacimiento", q[0].FechaNacimiento);
                        reporte.SetParameterValue("EstadoCivil", q[0].EstadoCivil);
                        reporte.SetParameterValue("Genero", q[0].Genero);
                        reporte.SetParameterValue("Nacionalidad", q[0].Nacionalidad);
                        reporte.SetParameterValue("Profesion", q[0].Profesion);
                        reporte.SetParameterValue("NIT", q[0].Nit);
                        reporte.SetParameterValue("Direccion", string.Format("{0} {1} {2} {3} {4}", q[0].CalleAvenidaFacturacion, q[0].CasaNumeroFacturacion, q[0].ZonaFacturacion == "" || q[0].ZonaFacturacion == "00" ? "" : string.Format("Zona {0}", q[0].ZonaFacturacion), q[0].ApartamentoFacturacion, q[0].ColoniaFacturacion));
                        reporte.SetParameterValue("Municipio", q[0].MunicipioFacturacion);
                        reporte.SetParameterValue("Politico", q[0].Politico);
                        reporte.SetParameterValue("Departamento", q[0].DepartamentoFacturacion);
                        reporte.SetParameterValue("TelefonoResidencia", q[0].Telefono1);
                        reporte.SetParameterValue("TelefonoCelular", q[0].Celular);
                        reporte.SetParameterValue("CargasFamiliares", q[0].CargasFamiliares);
                        reporte.SetParameterValue("TipoCasa", q[0].TipoCasa);
                        reporte.SetParameterValue("VehiculoAnio", q[0].MarcaModelo);
                        reporte.SetParameterValue("VehiculoPlacas", q[0].Placas);
                        reporte.SetParameterValue("Email", q[0].Email);
                        reporte.SetParameterValue("PagoMensual", q[0].PagoMensual);
                        reporte.SetParameterValue("TiempoResidir", q[0].TiempoResidir);
                        reporte.SetParameterValue("ReferenciasBancarias", q[0].ReferenciasBancarias);
                        reporte.SetParameterValue("Empresa", q[0].Empresa);
                        reporte.SetParameterValue("Cargo", q[0].Cargo);
                        reporte.SetParameterValue("TiempoTrabajar", q[0].TiempoLaborar);
                        reporte.SetParameterValue("IngresosMensuales", q[0].IngresosMes);
                        reporte.SetParameterValue("EgresosMensuales", q[0].GastosMes);
                        reporte.SetParameterValue("DireccionTrabajo", q[0].EmpresaDireccion);
                        reporte.SetParameterValue("MunicipioTrabajo", q[0].MunicipioEmpresa);
                        reporte.SetParameterValue("DepartamentoTrabajo", q[0].DepartamentoEmpresa);
                        reporte.SetParameterValue("TelefonoTrabajo", q[0].EmpresaTelefono);
                        reporte.SetParameterValue("Extension", q[0].Extension);
                        reporte.SetParameterValue("NombreConyugue", q[0].ConyugeNombre);

                        try
                        {
                            DateTime mFechaNacimientoConyugue = Convert.ToDateTime(q[0].ConyugueFechaNacimiento);

                            if (mFechaNacimientoConyugue.Year == 1)
                            {
                                reporte.SetParameterValue("FechaNacimientoConyugue", new DateTime(1901, 1, 1));
                            }
                            else
                            {
                                reporte.SetParameterValue("FechaNacimientoConyugue", mFechaNacimientoConyugue);
                            }
                        }
                        catch
                        {
                            reporte.SetParameterValue("FechaNacimientoConyugue", new DateTime(1901, 1, 1));
                        }

                        reporte.SetParameterValue("DPIConyugue", q[0].ConyugueCedula);
                        reporte.SetParameterValue("TrabajoConyugue", q[0].ConyugueEmpresa);
                        reporte.SetParameterValue("CargoConyugue", q[0].ConyugueCargo);
                        reporte.SetParameterValue("TiempoConyugue", q[0].ConyugueTiempo);
                        reporte.SetParameterValue("IngresosConyugue", q[0].ConyugueIngresos);
                        reporte.SetParameterValue("DireccionTrabajoConyugue", q[0].ConyugueEmpresaDireccion);
                        reporte.SetParameterValue("TelefonoTrabajoConyugue", q[0].ConyugueEmpresaTelefono);
                        reporte.SetParameterValue("ReferenciaPersonal1", q[0].RefPersonalNombre1);
                        reporte.SetParameterValue("ReferenciaPersonalResidencia1", q[0].RefPersonalTelResidencia1);
                        reporte.SetParameterValue("ReferenciaPersonalTrabajo1", q[0].RefPersonalTelTrabajo1);
                        reporte.SetParameterValue("ReferenciaPersonalCelular1", q[0].RefPersonalCelular1);
                        reporte.SetParameterValue("ReferenciaPersonal2", q[0].RefPersonalNombre2);
                        reporte.SetParameterValue("ReferenciaPersonalResidencia2", q[0].RefPersonalTelResidencia2);
                        reporte.SetParameterValue("ReferenciaPersonalTrabajo2", q[0].RefPersonalTelTrabajo2);
                        reporte.SetParameterValue("ReferenciaPersonalCelular2", q[0].RefPersonalCelular2);
                        reporte.SetParameterValue("ReferenciaComercial1", q[0].RefComercialNombre1);
                        reporte.SetParameterValue("ReferenciaComercialResidencia1", q[0].RefComercialTelefono1);
                        reporte.SetParameterValue("ReferenciaComercialTrabajo1", q[0].RefComercialFax1);
                        reporte.SetParameterValue("ReferenciaComercialCelular1", q[0].RefComercialCelular1);
                        reporte.SetParameterValue("ReferenciaComercial2", q[0].RefComercialNombre2);
                        reporte.SetParameterValue("ReferenciaComercialResidencia2", q[0].RefComercialTelefono2);
                        reporte.SetParameterValue("ReferenciaComercialTrabajo2", q[0].RefComercialFax2);
                        reporte.SetParameterValue("ReferenciaComercialCelular2", q[0].RefComercialCelular2);
                        reporte.SetParameterValue("PrecioDelBien", q[0].TotalFacturar);
                        reporte.SetParameterValue("Enganche", q[0].Enganche);
                        reporte.SetParameterValue("SaldoFinanciar", q[0].SaldoFinanciar);
                        reporte.SetParameterValue("Recargos", q[0].Recargos);
                        reporte.SetParameterValue("Monto", q[0].Monto);
                        reporte.SetParameterValue("Plazo", q[0].Plazo);
                        reporte.SetParameterValue("CantidadPagos1", q[0].CantidadPagos1);
                        reporte.SetParameterValue("MontoPagos1", q[0].MontoPagos1);
                        reporte.SetParameterValue("CantidadPagos2", q[0].CantidadPagos2);
                        reporte.SetParameterValue("MontoPagos2", q[0].MontoPagos2);
                        reporte.SetParameterValue("Solicitud", q[0].Departamento);
                        reporte.SetParameterValue("TextoEncabezado", q[0].EntraCamion);
                        reporte.SetParameterValue("VendedorInterconsumo", q[0].VendedorInterconsumo);
                        reporte.SetParameterValue("AutorizadoPor", q[0].AutorizadoPor);
                        reporte.SetParameterValue("NoAutorizacion", q[0].NoAutorizacion);

                        try
                        {
                            DateTime mFechaAutorizacion = Convert.ToDateTime(q[0].FechaAutorizacion);

                            if (mFechaAutorizacion.Year == 1)
                            {
                                reporte.SetParameterValue("FechaAutorizacion", new DateTime(1901, 1, 1));
                            }
                            else
                            {
                                reporte.SetParameterValue("FechaAutorizacion", mFechaAutorizacion);
                            }
                        }
                        catch
                        {
                            reporte.SetParameterValue("FechaAutorizacion", new DateTime(1901, 1, 1));
                        }


                        try
                        {
                            if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");

                            try
                            {
                                if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                            }
                            catch
                            {
                                mNombreDocumento = mNombreDocumento.Replace(".pdf", "");
                                mNombreDocumento = string.Format("{0}_2.pdf", mNombreDocumento);

                                try
                                {
                                    mNombreDocumento = mNombreDocumento.Replace(".pdf", "");
                                    mNombreDocumento = string.Format("{0}_2.pdf", mNombreDocumento);
                                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                                }
                                catch
                                {
                                    mNombreDocumento = mNombreDocumento.Replace(".pdf", "");
                                    mNombreDocumento = string.Format("{0}_2.pdf", mNombreDocumento);
                                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                                }
                            }

                            reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);
                        }
                        catch
                        {
                            //No hacer nada para que el proceso continúe
                        }

                        if (EnviarCarolyn)
                        {
                            SmtpClient smtp = new SmtpClient();
                            MailMessage mail = new MailMessage();

                            mail.Subject = string.Format("Solicitud del cliente {0} - {1}", txtCodigo.Text, txtNombre.Text);
                            string mCuerpo = string.Format("Adjunto encontrará la solicitud de crédito para el Pedido No. {0} para su revisión.", pedido);

                            mail.IsBodyHtml = true;
                            mail.To.Add("carolyn.altalef@mueblesfiesta.com");
                            mail.From = new MailAddress("puntodeventa@productosmultiples.com", string.Format("{0} - {1}", vendedorPedido, tiendaPedido));

                            StringBuilder mBody = new StringBuilder();

                            mBody.Append("<!DOCTYPE html><html lang='en-us'>");
                            mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } </style></head>");
                            mBody.Append("<body>");
                            mBody.Append(string.Format("<p>Doña Carolyn,</p>"));
                            mBody.Append(string.Format("<p>{0}</p>", mCuerpo));
                            mBody.Append("<p>Atentamente,</p>");
                            mBody.Append(string.Format("<p>{0}<BR><b>Muebles Fiesta - {1}</b></p>", vendedorPedido, tiendaPedido));
                            mBody.Append("</body>");
                            mBody.Append("</html>");

                            mail.Body = mBody.ToString();
                            mail.Attachments.Add(new Attachment(@"C:\reportes\" + mNombreDocumento));

                            smtp.Host = WebConfigurationManager.AppSettings["MailHost"];
                            smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort"]);

                            smtp.EnableSsl = true;
                            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                            try
                            {
                                smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail"], WebConfigurationManager.AppSettings["PwdMail"]);
                                //smtp.Credentials = new System.Net.NetworkCredential("puntodeventa", "jh$%Pjma");
                                smtp.Send(mail);
                            }
                            catch (Exception ex2)
                            {
                                string mm = "";
                                try
                                {
                                    mm = ex2.StackTrace.Substring(ex2.StackTrace.IndexOf("línea"));
                                }
                                catch
                                {
                                    try
                                    {
                                        mm = ex2.StackTrace.Substring(ex2.StackTrace.IndexOf("line"));
                                    }
                                    catch
                                    {
                                        //Nothing
                                    }
                                }
                            }

                        }
                        else
                        {
                            Response.Clear();
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                            Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                            Response.End();
                        }
                    }
                }
                else
                {
                    lbError.Text = "Esta opción solo aplica para Interconsumo";
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al imprimir la solicitud {0} {1}", ex.Message, m);
            }
        }

        void ImprimirPagare(string pedido, string financiera)
        {
            try
            {
                lbError.Text = "";
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (ws.EsBanco(financiera))
                {
                    ValidarDatosCrediticios(pedido);

                    if (financiera == "12")
                    {
                        try
                        {
                            lbInfo.Text = "";
                            lbError.Text = "";

                            string mReportServer = WebConfigurationManager.AppSettings["ReportServer"];
                            if (Convert.ToString(Session["Ambiente"]) == "DES" || Convert.ToString(Session["Ambiente"]) == "PRU") mReportServer = WebConfigurationManager.AppSettings["ReportServerPruebas"];

                            Response.Redirect(string.Format("{0}/Report/PagareAtid?id={1}", mReportServer, pedido), false);
                        }
                        catch (Exception ex)
                        {
                            lbError.Text = CatchClass.ExMessage(ex, "pedidos", "ImprimirSolicitud");
                        }

                        return;
                    }

                    string mMensaje = "";
                    if (!ws.ValidarTransaccionEnLinea("P", pedido, ref mMensaje))
                    {
                        lbError.Text = mMensaje;
                        return;
                    }

                    if (financiera == Const.FINANCIERA.INTERCONSUMO)
                    {
                        //----------------------------------------------------------------
                        //Validamos si esta habilitado el modulo
                        //----------------------------------------------------------------
                        WebRequest request = WebRequest.Create(string.Format("{0}/ModuloHabilitado/?Modulo={1}", Convert.ToString(Session["UrlRestServices"]), Const.MODULO.INTERCONSUMO_PAGARE));
                        request.Method = "GET";
                        request.ContentType = "application/json";
                        var response = (HttpWebResponse)request.GetResponse();
                        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                        //----------------------------------------------------------------
                        //Obtenemos las respuesta de las validaciones del pagaré.
                        //----------------------------------------------------------------
                        bool mModuloHabilitado = JsonConvert.DeserializeObject<bool>(responseString);
                        if (!mModuloHabilitado)
                        {
                            lbError.Text = "Modulo no habilitado.";
                            return;
                        }

                        //----------------------------------------------------------------
                        //Aplicamos las reglas de negocio para poder imprimir el pagare.
                        //----------------------------------------------------------------
                        request = WebRequest.Create(string.Format("{0}/RN001DocumentoInterconsumo/?NumeroDocumento={1}", Convert.ToString(Session["UrlRestServices"]), pedido));
                        request.Method = "GET";
                        request.ContentType = "application/json";
                        response = (HttpWebResponse)request.GetResponse();
                        responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                        //----------------------------------------------------------------
                        //Obtenemos las respuesta de las validaciones del pagaré.
                        //----------------------------------------------------------------
                        MF_Clases.Respuesta mRespuesta = JsonConvert.DeserializeObject<MF_Clases.Respuesta>(responseString);
                        if (!mRespuesta.Exito)
                        {
                            lbError.Text = mRespuesta.Mensaje;
                            return;
                        }

                        //----------------------------------------------------------------
                        //Procedemos a imprimir el pagaré.
                        //----------------------------------------------------------------
                        MF_Clases.Clases.Pedido mPedido = JsonConvert.DeserializeObject<MF_Clases.Clases.Pedido>(mRespuesta.Objeto.ToString());
                        Response.Redirect(string.Format("{0}/DocumentoPdfInterconsumo/?Tipo={1}&NumeroReferencia={2}", Convert.ToString(Session["UrlRestServices"]), Const.DocumentoInterconsumo.PAGARE, mPedido.SOLICITUD));

                    }
                    else if (financiera == Const.FINANCIERA.BANCREDIT)
                    {
                        var q = ws.DevuelveSolicitudCredito("P", pedido);
                        using (ReportDocument reporte = new ReportDocument())
                        {
                            string p = (Request.PhysicalApplicationPath + "reportes/PagareBancredit.rpt");
                            reporte.Load(p);

                            string mNombreDocumento = string.Format("Pagare_{0}.pdf", pedido);
                            Int32 mMesesFin = q[0].Colonia == "3 PAGOS" ? 3 : q[0].Colonia == "6 PAGOS" ? 6 : q[0].Colonia == "9 PAGOS" ? 9 : q[0].Colonia == "12 PAGOS" ? 12 : q[0].Colonia == "18 PAGOS" ? 18 : q[0].Colonia == "24 PAGOS" ? 24 : 36;
                            Cast mCast = new Cast();
                            int edad = DateTime.Today.AddTicks(-(q[0].FechaNacimiento ?? DateTime.Now).Ticks).Year - 1;
                            DateTime nacimiento = q[0].FechaNacimiento ?? DateTime.Now;
                            DateTime mFechaPedido = q[0].FechaPedido ?? DateTime.Now;
                            Bancredit mFechasBancredit = new Bancredit();

                            Cast mOrdinalCast = new Cast();

                            mOrdinalCast.NumeroLetras._Ordinal = true;

                            Numalet mNumerosLetras = new Numalet();
                            mNumerosLetras.ApocoparUnoParteEntera = true;

                            Numalet mNumerosLetrasOrdinal = new Numalet(true);
                            mNumerosLetrasOrdinal.ApocoparUnoParteEntera = true;


                            reporte.SetParameterValue("Nombre", string.Format("{0} {1}{2}{3} {4} {5} {6}", q[0].PrimerNombre, q[0].SegundoNombre, q[0].TercerNombre.Trim().Length == 0 ? "" : " ", q[0].TercerNombre, q[0].PrimerApellido, q[0].SegundoApellido, q[0].ApellidoCasada));
                            reporte.SetParameterValue("Edad", mCast.ConvertirNumLetra(edad.ToString(), true));
                            reporte.SetParameterValue("EstadoCivil", q[0].EstadoCivil.ToUpper());
                            reporte.SetParameterValue("Profesion", q[0].Profesion);
                            reporte.SetParameterValue("Nacionalidad", q[0].Nacionalidad);
                            reporte.SetParameterValue("DPILetras", mCast.ConvertirDPILetra(q[0].DPI, true));
                            reporte.SetParameterValue("Departamento", q[0].DepartamentoFacturacion);
                            reporte.SetParameterValue("Municipio", q[0].MunicipioFacturacion);

                            string s = q[0].Monto.ToString("0.00", CultureInfo.InvariantCulture);
                            string[] parts = s.Split('.');
                            int i1 = int.Parse(parts[0]);
                            int i2 = int.Parse(parts[1]);

                            if (i2 > 0)
                                reporte.SetParameterValue("MontoTotalLetras",
                                    mNumerosLetras.ToCustomLetter(i1).ToUpper()
                                    //mCast.ConvertirNumLetra(i1.ToString(),true) 
                                    + " QUETZALES CON " +
                                    mNumerosLetras.ToCustomLetter(i2).ToUpper()
                                    //mCast.ConvertirNumLetra(i2.ToString(), true) 
                                    + " CENTAVOS");
                            else reporte.SetParameterValue("MontoTotalLetras",
                                //mCast.ConvertirNumLetra(i1.ToString(), true)
                                mNumerosLetras.ToCustomLetter(i1).ToUpper()
                                + " QUETZALES EXACTOS");

                            s = q[0].MontoPagos1.ToString("0.00", CultureInfo.InvariantCulture);
                            parts = s.Split('.');
                            i1 = int.Parse(parts[0]);
                            i2 = int.Parse(parts[1]);

                            if (i2 > 0)
                                reporte.SetParameterValue("MontoCuotaLetras",
                                    //mCast.ConvertirNumLetra(i1.ToString(), true)
                                    mNumerosLetras.ToCustomLetter(i1).ToUpper()
                                    + " QUETZALES CON " +
                                    //mCast.ConvertirNumLetra(i2.ToString(), true) 
                                    mNumerosLetras.ToCustomLetter(i2).ToUpper()
                                    + " CENTAVOS");
                            else reporte.SetParameterValue("MontoCuotaLetras",
                                //mCast.ConvertirNumLetra(i1.ToString(), true)
                                mNumerosLetras.ToCustomLetter(i1).ToUpper()
                                + " QUETZALES EXACTOS");

                            reporte.SetParameterValue("CantidadCuotasLetras",
                                 //mCast.ConvertirNumLetra(q[0].CantidadPagos1.ToString(), true)
                                 mNumerosLetras.ToCustomLetter(q[0].CantidadPagos1).ToUpper()
                                );

                            s = q[0].MontoPagos2.ToString("0.00", CultureInfo.InvariantCulture);

                            parts = s.Split('.');
                            i1 = int.Parse(parts[0]);
                            i2 = int.Parse(parts[1]);

                            if (i2 > 0)
                                reporte.SetParameterValue("MontoUltimaCuotaLetras",
                                    //mCast.ConvertirNumLetra(i1.ToString(), true)
                                    mNumerosLetras.ToCustomLetter(i1).ToUpper()
                                    + " QUETZALES CON " +
                                    //mCast.ConvertirNumLetra(i2.ToString(), true) 
                                    mNumerosLetras.ToCustomLetter(i2).ToUpper()
                                    + " CENTAVOS");
                            else reporte.SetParameterValue("MontoUltimaCuotaLetras",
                                //mCast.ConvertirNumLetra(i1.ToString(), true) 
                                mNumerosLetras.ToCustomLetter(i1).ToUpper()
                                + " QUETZALES EXACTOS");

                            reporte.SetParameterValue("FechaFinLetras", mCast.ConvertirFechaLetra(
                                mFechasBancredit.getUltimaFecha(q[0].FechaPedido ?? DateTime.Now, (q[0].CantidadPagos1 + 1))

                                )

                                );
                            reporte.SetParameterValue("FechaPrimerPagoLetras",
                                mCast.ConvertirFechaLetra(mFechasBancredit.getFechaPrimerPago(q[0].FechaPedido ?? DateTime.Now)));

                            reporte.SetParameterValue("FechaSegundoPagoMesLetras", mCast.ConvertirMesLetra(mFechasBancredit.getFechaPrimerPago(q[0].FechaPedido ?? DateTime.Now).Month));
                            reporte.SetParameterValue("FechaCorteDiaLetras", mCast.ConvertirNumLetra(mFechasBancredit.getFechaCorte(q[0].FechaPedido ?? DateTime.Now).ToString(), true));


                            reporte.SetParameterValue("Domicilio", mCast.ConvertirDireccionLetras(
                                string.Format("{0} {1}{2}{3} {4} {5}, {6}",

                                mOrdinalCast.ConvertirNumeroDireccionLetras(
                                    q[0].CalleAvenidaFacturacion.Replace("AV.", "AVENIDA"), true)
                                , q[0].CasaNumeroFacturacion
                                , q[0].ZonaFacturacion.Replace("00", "").Length > 0 ? " ZONA " + q[0].ZonaFacturacion.Replace("Z.", "ZONA ") + " " : ""
                                , q[0].ApartamentoFacturacion.Length > 0 ? " " + q[0].ApartamentoFacturacion + " " : ""
                                , " " + q[0].ColoniaFacturacion
                                , q[0].MunicipioFacturacion
                                , q[0].DepartamentoFacturacion).ToUpper()).ToUpper()

                                );

                            reporte.SetParameterValue("FechaPedidoDiaLetras", mCast.ConvertirNumLetra(mFechaPedido.Day.ToString(), true));
                            reporte.SetParameterValue("FechaPedidoMesLetras", mCast.ConvertirMesLetra(mFechaPedido.Month));
                            reporte.SetParameterValue("FechaPedidoAnoLetras", mCast.ConvertirNumLetra(mFechaPedido.Year.ToString(), true));



                            if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                            if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                            reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                            Response.Clear();
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                            Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                            Response.End();
                        }


                    }
                }
                else
                {
                    lbError.Text = "Esta opción solo aplica para Interconsumo";
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al imprimir el pagaré {0} {1}", ex.Message, m);
            }
        }

        void ImprimirExpediente(string pedido, string EnviarA, string observaciones)
        {
            try
            {
                lbError.Text = "";
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.PedidoFacturado(pedido))
                {
                    lbError.Text = "Este pedido NO se encuentra facturado, no es posible continuar";
                    return;
                }

                dsPuntoVenta ds = new dsPuntoVenta();
                var qExpediente = ws.DevuelveExpediente(pedido, EnviarA);
                var qExpedienteDet = ws.DevuelveExpedienteDet(pedido, EnviarA);

                DataRow mRowExpediente = ds.Expediente.NewRow();
                mRowExpediente["Cliente"] = qExpediente[0].Cliente;
                mRowExpediente["Pedido"] = qExpediente[0].Pedido;
                mRowExpediente["Fecha"] = qExpediente[0].Fecha;
                mRowExpediente["Vendedor"] = qExpediente[0].Vendedor;
                mRowExpediente["CodigoVendedor"] = qExpediente[0].CodigoVendedor;
                mRowExpediente["Tienda"] = qExpediente[0].Tienda;
                mRowExpediente["PrimerNombre"] = qExpediente[0].PrimerNombre;
                mRowExpediente["SegundoNombre"] = qExpediente[0].SegundoNombre;
                mRowExpediente["TercerNombre"] = qExpediente[0].TercerNombre;
                mRowExpediente["PrimerApellido"] = qExpediente[0].PrimerApellido;
                mRowExpediente["SegundoApellido"] = qExpediente[0].SegundoApellido;
                mRowExpediente["ApellidoCasada"] = qExpediente[0].ApellidoCasada;
                mRowExpediente["DPI"] = qExpediente[0].DPI;
                mRowExpediente["NIT"] = qExpediente[0].NIT;
                mRowExpediente["Direccion"] = qExpediente[0].Direccion;
                mRowExpediente["Colonia"] = qExpediente[0].Colonia;
                mRowExpediente["Zona"] = qExpediente[0].Zona;
                mRowExpediente["Departamento"] = qExpediente[0].Departamento;
                mRowExpediente["Municipio"] = qExpediente[0].Municipio;
                mRowExpediente["TelefonoResidencia"] = qExpediente[0].TelefonoResidencia;
                mRowExpediente["TelefonoCelular"] = qExpediente[0].TelefonoCelular;
                mRowExpediente["TelefonoTrabajo"] = qExpediente[0].TelefonoTrabajo;
                mRowExpediente["Email"] = qExpediente[0].Email;
                mRowExpediente["EntregarMismaDireccion"] = qExpediente[0].EntregarMismaDireccion;
                mRowExpediente["NombreEntrega"] = qExpediente[0].NombreEntrega;
                mRowExpediente["DireccionEntrega"] = qExpediente[0].DireccionEntrega;
                mRowExpediente["ColoniaEntrega"] = qExpediente[0].ColoniaEntrega;
                mRowExpediente["ZonaEntrega"] = qExpediente[0].ZonaEntrega;
                mRowExpediente["DepartamentoEntrega"] = qExpediente[0].DepartamentoEntrega;
                mRowExpediente["MunicipioEntrega"] = qExpediente[0].MunicipioEntrega;
                mRowExpediente["EntraCamion"] = qExpediente[0].EntraCamion;
                mRowExpediente["EntraPickup"] = qExpediente[0].EntraPickup;
                mRowExpediente["EsSegundoPiso"] = qExpediente[0].EsSegundoPiso;
                mRowExpediente["Notas"] = qExpediente[0].Notas;
                mRowExpediente["BodegaSale"] = qExpediente[0].BodegaSale;
                mRowExpediente["TiendaSale"] = qExpediente[0].TiendaSale;
                mRowExpediente["ObsVendedor"] = qExpediente[0].ObsVendedor;
                mRowExpediente["ObsGerencia"] = qExpediente[0].ObsGerencia;
                mRowExpediente["Gerente"] = qExpediente[0].Gerente;
                mRowExpediente["Factura"] = qExpediente[0].Factura;
                mRowExpediente["Garantia"] = qExpediente[0].Garantia;
                mRowExpediente["Promocion"] = qExpediente[0].Promocion;
                mRowExpediente["Financiera"] = qExpediente[0].Financiera;
                mRowExpediente["QuienAutorizo"] = qExpediente[0].QuienAutorizo;
                mRowExpediente["NoAutorizacion"] = qExpediente[0].NoAutorizacion;
                mRowExpediente["Enganche"] = qExpediente[0].Enganche;
                mRowExpediente["Plan"] = qExpediente[0].Plan;
                mRowExpediente["NoSolicitud"] = qExpediente[0].NoSolicitud;
                mRowExpediente["FechaHora"] = qExpediente[0].FechaHora;
                mRowExpediente["CantidadPagos1"] = qExpediente[0].CantidadPagos1;
                mRowExpediente["MontoPagos1"] = qExpediente[0].MontoPagos1;
                mRowExpediente["CantidadPagos2"] = qExpediente[0].CantidadPagos2;
                mRowExpediente["MontoPagos2"] = qExpediente[0].MontoPagos2;
                mRowExpediente["Despacho"] = qExpediente[0].Despacho;
                mRowExpediente["Radio"] = qExpediente[0].Radio;
                mRowExpediente["Volante"] = qExpediente[0].Volante;
                mRowExpediente["PrensaLibre"] = qExpediente[0].PrensaLibre;
                mRowExpediente["ElQuetzalteco"] = qExpediente[0].ElQuetzalteco;
                mRowExpediente["NuestroDiario"] = qExpediente[0].NuestroDiario;
                mRowExpediente["Internet"] = qExpediente[0].Internet;
                mRowExpediente["TV"] = qExpediente[0].TV;
                mRowExpediente["Otros"] = qExpediente[0].Otros;
                mRowExpediente["ObsOtros"] = qExpediente[0].ObsOtros;
                mRowExpediente["AutorizacionEspecial"] = qExpediente[0].AutorizacionEspecial;
                mRowExpediente["ObservacionesAdicionales"] = qExpediente[0].ObservacionesAdicionales;
                mRowExpediente["IndicacionesLlegar"] = qExpediente[0].IndicacionesLlegar;
                mRowExpediente["EntregaAMPM"] = qExpediente[0].EntregaAMPM;
                mRowExpediente["FechaEntrega"] = qExpediente[0].FechaEntrega;
                mRowExpediente["NotasPrecio"] = qExpediente[0].NotasPrecio;
                mRowExpediente["Factor"] = qExpediente[0].Factor;
                mRowExpediente["Recibos"] = qExpediente[0].Recibos;
                mRowExpediente["Requisitos"] = qExpediente[0].Requisitos;
                ds.Expediente.Rows.Add(mRowExpediente);

                for (int ii = 0; ii < qExpedienteDet.Length; ii++)
                {
                    DataRow mRowExpedienteDet = ds.ExpedienteDet.NewRow();
                    mRowExpedienteDet["Cliente"] = qExpedienteDet[ii].Cliente;
                    mRowExpedienteDet["Pedido"] = qExpedienteDet[ii].Pedido;
                    mRowExpedienteDet["Articulo"] = qExpedienteDet[ii].Articulo;
                    mRowExpedienteDet["Descripcion"] = qExpedienteDet[ii].Descripcion;
                    mRowExpedienteDet["PrecioLista"] = qExpedienteDet[ii].PrecioLista;
                    mRowExpedienteDet["Cantidad"] = qExpedienteDet[ii].Cantidad;
                    mRowExpedienteDet["Factor"] = qExpedienteDet[ii].Factor;
                    mRowExpedienteDet["PrecioTotal"] = qExpedienteDet[ii].PrecioTotal;
                    mRowExpedienteDet["Bodega"] = qExpedienteDet[ii].Bodega;
                    ds.ExpedienteDet.Rows.Add(mRowExpedienteDet);
                }

                int mRows = ds.ExpedienteDet.Rows.Count;
                for (int ii = mRows; ii < 16; ii++)
                {
                    DataRow mRowExpedienteDet = ds.ExpedienteDet.NewRow();
                    mRowExpedienteDet["Cliente"] = qExpediente[0].Cliente;
                    mRowExpedienteDet["Pedido"] = qExpediente[0].Pedido;
                    mRowExpedienteDet["Articulo"] = "";
                    mRowExpedienteDet["Descripcion"] = "";
                    mRowExpedienteDet["PrecioLista"] = 0;
                    mRowExpedienteDet["Cantidad"] = 0;
                    mRowExpedienteDet["Factor"] = 0;
                    mRowExpedienteDet["PrecioTotal"] = 0;
                    mRowExpedienteDet["Bodega"] = "";
                    ds.ExpedienteDet.Rows.Add(mRowExpedienteDet);
                }

                using (ReportDocument ReporteExpediente = new ReportDocument())
                {
                    // ReportDocument ReporteExpediente = new ReportDocument();
                    string m = (Request.PhysicalApplicationPath + "reportes/rptExpediente.rpt");
                    ReporteExpediente.Load(m);

                    string mExpediente = string.Format("Expediente_{0}.pdf", pedido);
                    if (EnviarA == "A") mExpediente = string.Format("Requisicion_{0}_{1}.pdf", txtAutorizacion.Text, pedido);

                    ReporteExpediente.SetDataSource(ds);

                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");

                    try
                    {
                        if (File.Exists(@"C:\reportes\" + mExpediente)) File.Delete(@"C:\reportes\" + mExpediente);
                    }
                    catch
                    {
                        mExpediente = mExpediente.Replace(".pdf", "");
                        mExpediente = string.Format("{0}_2.pdf", mExpediente);

                        try
                        {
                            if (File.Exists(@"C:\reportes\" + mExpediente)) File.Delete(@"C:\reportes\" + mExpediente);
                        }
                        catch
                        {
                            mExpediente = mExpediente.Replace(".pdf", "");
                            mExpediente = string.Format("{0}_2.pdf", mExpediente);

                            try
                            {
                                if (File.Exists(@"C:\reportes\" + mExpediente)) File.Delete(@"C:\reportes\" + mExpediente);
                            }
                            catch
                            {
                                mExpediente = mExpediente.Replace(".pdf", "");
                                mExpediente = string.Format("{0}_2.pdf", mExpediente);
                                if (File.Exists(@"C:\reportes\" + mExpediente)) File.Delete(@"C:\reportes\" + mExpediente);
                            }
                        }
                    }

                    ReporteExpediente.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mExpediente);

                    if (EnviarA == "N")
                    {
                        Response.Clear();
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mExpediente);
                        Response.WriteFile(@"C:\reportes\" + mExpediente);
                        Response.End();
                    }
                    else
                    {
                        string mCuerpo = ""; string mInfo = "";
                        var q = ws.DevuelveConfiguracion();

                        string mNombreVendedor = qExpediente[0].Vendedor;
                        string mVendedorBuscar = qExpediente[0].CodigoVendedor;

                        if (qExpediente[0].TiendaSale != "N")
                        {
                            mVendedorBuscar = Convert.ToString(Session["Vendedor"]);
                            mNombreVendedor = Convert.ToString(Session["NombreVendedor"]);
                        }

                        string mMailVendedor = ws.MailVendedor(mVendedorBuscar); string mInfoArmado = ""; string mInfoArmado2 = "";
                        string mClienteConcatenar = string.Format("Cliente {0} - {1}", txtCodigo.Text, txtNombre.Text);
                        //Destinatarios de correo
                        List<string> mCorreo = new List<string>();
                        string mAsunto = string.Empty;
                        if (mMailVendedor.Contains("@")) mCorreo.Add(mMailVendedor);
                        switch (EnviarA)
                        {
                            case "O":
                                //Enviar a Oficinas
                                if (q[0].MailOficina1.Length > 0) mCorreo.Add(q[0].MailOficina1);
                                if (q[0].MailOficina2.Length > 0) mCorreo.Add(q[0].MailOficina2);
                                if (q[0].MailOficina3.Length > 0) mCorreo.Add(q[0].MailOficina3);
                                if (mMailVendedor.Contains("@")) mCorreo.Add(mMailVendedor);

                                mInfo = "El expediente fue enviado exitosamente";
                                mAsunto = string.Format("Expediente del Pedido No. {0} {1}", pedido, mClienteConcatenar);
                                mCuerpo = string.Format("Adjunto encontrarán el expediente del Pedido No. {0} {1} para su revisión.", pedido, mClienteConcatenar);

                                if (ws.EsBanco(cbFinanciera.SelectedValue.ToString())) ImprimirSolicitud(pedido, cbFinanciera.SelectedValue, true, qExpediente[0].Vendedor, qExpediente[0].Tienda);
                                break;
                            case "B":
                                //Enviar a Bodega
                                string mBodegaSale = qExpedienteDet[0].Bodega;

                                WebRequest request = WebRequest.Create(string.Format("{0}/destinatariosbodega/{1}", Convert.ToString(Session["UrlRestServices"]), mBodegaSale));

                                request.Method = "GET";
                                request.ContentType = "application/json";

                                var response = (HttpWebResponse)request.GetResponse();
                                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                                string mDestinatarios = Convert.ToString(responseString).Replace("\"", "");

                                if (mDestinatarios.ToLower().Contains("error")) return;
                                mCorreo.Add(mDestinatarios);

                                if (txtAnio.Text.Trim().Length == 2) txtAnio.Text = string.Format("20{0}", txtAnio.Text);
                                DateTime mFechaEntrega = new DateTime(Convert.ToInt32(txtAnio.Text), Convert.ToInt32(cbMes.SelectedValue), Convert.ToInt32(cbDia.SelectedValue));

                                string mFactura = ws.DevuelveFacturaDePedido(pedido);

                                mInfo = "El expediente para reservar fue enviado exitosamente";
                                mAsunto = string.Format("Reservar artículos de la factura {0} para entrega {1}{2}/{3}{4}/{5}", mFactura, mFechaEntrega.Day < 10 ? "0" : "", mFechaEntrega.Day, mFechaEntrega.Month < 10 ? "0" : "", mFechaEntrega.Month, mFechaEntrega.Year);
                                mCuerpo = string.Format("Favor reservar los artículos del expediente adjunto que corresponden a la factura {0} cuya entrega está programada para el {1}{2}/{3}{4}/{5}", mFactura, mFechaEntrega.Day < 10 ? "0" : "", mFechaEntrega.Day, mFechaEntrega.Month < 10 ? "0" : "", mFechaEntrega.Month, mFechaEntrega.Year);
                                break;
                            case "M":
                                //Enviar solo al vendedor
                                if (mMailVendedor.Contains("@")) mCorreo.Add(mMailVendedor);

                                mInfo = string.Format("El expediente fue enviado exitosamente a {0}", mMailVendedor);
                                mAsunto = string.Format("Expediente del Pedido No. {0} {1}", pedido, mClienteConcatenar);
                                mCuerpo = string.Format("Adjunto encontrará el expediente del Pedido No. {0} {1} para su archivo.", pedido, mClienteConcatenar);
                                break;
                            case "A":
                                //Enviar a Armados Industriales
                                if (mRows == 0)
                                {
                                    txtTipoOferta.Text = "Error";
                                    lbError.Text = "En este pedido no hay artículos marcados para Armado";
                                    return;
                                }

                                //mail.To.Add(ws.DestinatarioArmados(txtCodigo.Text));
                                //if (mMailVendedor.Contains("@")) mail.CC.Add(mMailVendedor);

                                if (mMailVendedor.Contains("@")) mCorreo.Add(mMailVendedor);

                                mInfo = "  Se envió copia a su correo.";
                                mAsunto = string.Format("Requisición de Armado No. {0} - Tienda: {1}", txtAutorizacion.Text, qExpediente[0].Tienda);
                                mCuerpo = string.Format("Adjunto encontrarán la Requisición de Armado No. {0} generada en {1} para su revisión.", txtAutorizacion.Text, qExpediente[0].Tienda);
                                mInfoArmado = string.Format("<BR><b>Tipo:</b> {0}<BR><b>Fecha de Armado:</b> {1}/{2}/{3}", cbTipoArmado.SelectedItem.Text, cbDiaArmado.SelectedItem.Text, cbMesArmado.Text, txtAnioArmado.Text);

                                if (txtFacturado.Text.ToUpper() == "TRUE") mInfoArmado2 = "<p><b>NOTA: </b>El vendedor ya había enviado esta requisición, favor tomarlo en cuenta</p>";


                                break;
                            default:
                                break;
                        }

                        StringBuilder mBody = new StringBuilder();

                        mBody.Append("<!DOCTYPE html><html lang='en-us'>");
                        mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } </style></head>");
                        mBody.Append("<body>");
                        mBody.Append(string.Format("<p>Estimado(s),</p>"));
                        mBody.Append(string.Format("<p>{0}</p>", mCuerpo));
                        if (EnviarA == "A") mBody.Append(mInfoArmado);
                        mBody.Append(string.Format("<p><b>Observaciones: </b>{0}</p>", txtObservacionesEnviar.Text));
                        if (mInfoArmado2.Length > 0) mBody.Append(mInfoArmado2);
                        mBody.Append("<p>Atentamente,</p>");
                        mBody.Append(string.Format("<p>{0}<BR><b>Muebles Fiesta - {1}</b></p>", mNombreVendedor, qExpediente[0].Tienda));
                        mBody.Append("</body>");
                        mBody.Append("</html>");

                        //Enviamos el correo
                        List<Attachment> attachments = new List<Attachment>();
                        attachments.Add(new Attachment(@"C:\reportes\" + mExpediente)); 

                        Mail.EnviarEmail(WebConfigurationManager.AppSettings["UsrMail"].ToString(), mCorreo, mAsunto, mBody, mNombreVendedor,null, attachments);
                        #region "Anterior"
                        //using (SmtpClient smtp = new SmtpClient())
                        //{
                        //    using (MailMessage mail = new MailMessage())
                        //    {
                        //        switch (EnviarA)
                        //        {
                        //            case "O":
                        //                //Enviar a Oficinas
                        //                if (q[0].MailOficina1.Length > 0) mail.To.Add(q[0].MailOficina1);
                        //                if (q[0].MailOficina2.Length > 0) mail.To.Add(q[0].MailOficina2);
                        //                if (q[0].MailOficina3.Length > 0) mail.To.Add(q[0].MailOficina3);
                        //                if (mMailVendedor.Contains("@")) mail.CC.Add(mMailVendedor);

                        //                mInfo = "El expediente fue enviado exitosamente";
                        //                mail.Subject = string.Format("Expediente del Pedido No. {0} {1}", pedido, mClienteConcatenar);
                        //                mCuerpo = string.Format("Adjunto encontrarán el expediente del Pedido No. {0} {1} para su revisión.", pedido, mClienteConcatenar);

                        //                if (ws.EsBanco(cbFinanciera.SelectedValue.ToString())) ImprimirSolicitud(pedido, cbFinanciera.SelectedValue, true, qExpediente[0].Vendedor, qExpediente[0].Tienda);
                        //                break;
                        //            case "B":
                        //                //Enviar a Bodega
                        //                string mBodegaSale = qExpedienteDet[0].Bodega;

                        //                //if (q[0].MailBodega1.Length > 0) mail.To.Add(q[0].MailBodega1);
                        //                //if (q[0].MailBodega2.Length > 0) mail.To.Add(q[0].MailBodega2);
                        //                //if (q[0].MailBodega3.Length > 0) mail.To.Add(q[0].MailBodega3);

                        //                WebRequest request = WebRequest.Create(string.Format("{0}/destinatariosbodega/{1}", Convert.ToString(Session["UrlRestServices"]), mBodegaSale));

                        //                request.Method = "GET";
                        //                request.ContentType = "application/json";

                        //                var response = (HttpWebResponse)request.GetResponse();
                        //                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                        //                string mDestinatarios = Convert.ToString(responseString).Replace("\"", "");

                        //                if (mDestinatarios.ToLower().Contains("error")) return;
                        //                mail.To.Add(mDestinatarios);

                        //                if (txtAnio.Text.Trim().Length == 2) txtAnio.Text = string.Format("20{0}", txtAnio.Text);
                        //                DateTime mFechaEntrega = new DateTime(Convert.ToInt32(txtAnio.Text), Convert.ToInt32(cbMes.SelectedValue), Convert.ToInt32(cbDia.SelectedValue));

                        //                string mFactura = ws.DevuelveFacturaDePedido(pedido);

                        //                mInfo = "El expediente para reservar fue enviado exitosamente";
                        //                mail.Subject = string.Format("Reservar artículos de la factura {0} para entrega {1}{2}/{3}{4}/{5}", mFactura, mFechaEntrega.Day < 10 ? "0" : "", mFechaEntrega.Day, mFechaEntrega.Month < 10 ? "0" : "", mFechaEntrega.Month, mFechaEntrega.Year);
                        //                mCuerpo = string.Format("Favor reservar los artículos del expediente adjunto que corresponden a la factura {0} cuya entrega está programada para el {1}{2}/{3}{4}/{5}", mFactura, mFechaEntrega.Day < 10 ? "0" : "", mFechaEntrega.Day, mFechaEntrega.Month < 10 ? "0" : "", mFechaEntrega.Month, mFechaEntrega.Year);
                        //                break;
                        //            case "M":
                        //                //Enviar solo al vendedor
                        //                if (mMailVendedor.Contains("@")) mail.To.Add(mMailVendedor);

                        //                mInfo = string.Format("El expediente fue enviado exitosamente a {0}", mMailVendedor);
                        //                mail.Subject = string.Format("Expediente del Pedido No. {0} {1}", pedido, mClienteConcatenar);
                        //                mCuerpo = string.Format("Adjunto encontrará el expediente del Pedido No. {0} {1} para su archivo.", pedido, mClienteConcatenar);
                        //                break;
                        //            case "A":
                        //                //Enviar a Armados Industriales
                        //                if (mRows == 0)
                        //                {
                        //                    txtTipoOferta.Text = "Error";
                        //                    lbError.Text = "En este pedido no hay artículos marcados para Armado";
                        //                    return;
                        //                }

                        //                //mail.To.Add(ws.DestinatarioArmados(txtCodigo.Text));
                        //                //if (mMailVendedor.Contains("@")) mail.CC.Add(mMailVendedor);

                        //                if (mMailVendedor.Contains("@")) mail.To.Add(mMailVendedor);

                        //                mInfo = "  Se envió copia a su correo.";
                        //                mail.Subject = string.Format("Requisición de Armado No. {0} - Tienda: {1}", txtAutorizacion.Text, qExpediente[0].Tienda);
                        //                mCuerpo = string.Format("Adjunto encontrarán la Requisición de Armado No. {0} generada en {1} para su revisión.", txtAutorizacion.Text, qExpediente[0].Tienda);
                        //                mInfoArmado = string.Format("<BR><b>Tipo:</b> {0}<BR><b>Fecha de Armado:</b> {1}/{2}/{3}", cbTipoArmado.SelectedItem.Text, cbDiaArmado.SelectedItem.Text, cbMesArmado.Text, txtAnioArmado.Text);

                        //                if (txtFacturado.Text.ToUpper() == "TRUE") mInfoArmado2 = "<p><b>NOTA: </b>El vendedor ya había enviado esta requisición, favor tomarlo en cuenta</p>";

                        //                break;
                        //            default:
                        //                break;
                        //        }

                        //        //mail.IsBodyHtml = true;
                        //        //mail.ReplyToList.Add(mMailVendedor);
                        //        //mail.From = new MailAddress(mMailVendedor, mNombreVendedor);
                        //        //mail.From = new MailAddress("puntodeventa@mueblesfiesta.com", mNombreVendedor);

                        //        StringBuilder mBody = new StringBuilder();

                        //        mBody.Append("<!DOCTYPE html><html lang='en-us'>");
                        //        mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } </style></head>");
                        //        mBody.Append("<body>");
                        //        mBody.Append(string.Format("<p>Estimado(s),</p>"));
                        //        mBody.Append(string.Format("<p>{0}</p>", mCuerpo));
                        //        if (EnviarA == "A") mBody.Append(mInfoArmado);
                        //        mBody.Append(string.Format("<p><b>Observaciones: </b>{0}</p>", txtObservacionesEnviar.Text));
                        //        if (mInfoArmado2.Length > 0) mBody.Append(mInfoArmado2);
                        //        mBody.Append("<p>Atentamente,</p>");
                        //        mBody.Append(string.Format("<p>{0}<BR><b>Muebles Fiesta - {1}</b></p>", mNombreVendedor, qExpediente[0].Tienda));
                        //        mBody.Append("</body>");
                        //        mBody.Append("</html>");

                        //        mail.Body = mBody.ToString();
                        //        mail.Attachments.Add(new Attachment(@"C:\reportes\" + mExpediente));

                        //        smtp.Host = WebConfigurationManager.AppSettings["MailHost"];
                        //        smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort"]);

                        //        smtp.EnableSsl = true;
                        //        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                        //        try
                        //        {
                        //            smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail"], WebConfigurationManager.AppSettings["PwdMail"]);
                        //            smtp.Send(mail);
                        //        }
                        //        catch (Exception ex2)
                        //        {
                        //            string mm = "";
                        //            try
                        //            {
                        //                mm = ex2.StackTrace.Substring(ex2.StackTrace.IndexOf("línea"));
                        //            }
                        //            catch
                        //            {
                        //                try
                        //                {
                        //                    mm = ex2.StackTrace.Substring(ex2.StackTrace.IndexOf("line"));
                        //                }
                        //                catch
                        //                {
                        //                    //Nothing
                        //                }
                        //            }
                        //            lbError.Text = string.Format("Error al enviar {0} {1}", ex2.Message, mm);
                        //            return;
                        //        }

                        //        lbInfo.Text = string.Format("{0}{1}", txtVale.Text, mInfo);
                        //        liEnviarExpediente.Visible = false;
                        //    }
                        //}
                        #endregion
                        lbInfo.Text = string.Format("{0}{1}", txtVale1.Text, mInfo);
                        liEnviarExpediente.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al imprimir el expediente {0} {1}", ex.Message, m);
            }
        }

        void SeleccionarPedido()
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                Label lbFecha = (Label)gridPedidos.SelectedRow.FindControl("lbFecha");
                Label lbTotalFacturar = (Label)gridPedidos.SelectedRow.FindControl("lbTotalFacturar");
                Label lbEnganche = (Label)gridPedidos.SelectedRow.FindControl("lbEnganche");
                Label lbSaldoFinanciar = (Label)gridPedidos.SelectedRow.FindControl("lbSaldoFinanciar");
                Label lbRecargos = (Label)gridPedidos.SelectedRow.FindControl("lbRecargos");
                Label lbMonto = (Label)gridPedidos.SelectedRow.FindControl("lbMonto");
                Label lbCantidadPagos1 = (Label)gridPedidos.SelectedRow.FindControl("lbCantidadPagos1");
                Label lbMontoPagos1 = (Label)gridPedidos.SelectedRow.FindControl("lbMontoPagos1");
                Label lbCantidadPagos2 = (Label)gridPedidos.SelectedRow.FindControl("lbCantidadPagos2");
                Label lbMontoPagos2 = (Label)gridPedidos.SelectedRow.FindControl("lbMontoPagos2");
                Label lbPagare = (Label)gridPedidos.SelectedRow.FindControl("lbPagare");
                Label lbSolicitud = (Label)gridPedidos.SelectedRow.FindControl("lbSolicitud");
                Label lbNombreAutorizacion = (Label)gridPedidos.SelectedRow.FindControl("lbNombreAutorizacion");
                Label lbAutorizacion = (Label)gridPedidos.SelectedRow.FindControl("lbAutorizacion");
                Label lbObservaciones = (Label)gridPedidos.SelectedRow.FindControl("lbObservaciones");
                Label lbObservacionesTipoVenta = (Label)gridPedidos.SelectedRow.FindControl("lbObservacionesTipoVenta");
                Label lbTipoReferencia = (Label)gridPedidos.SelectedRow.FindControl("lbTipoReferencia");
                Label lbObservacionesReferencia = (Label)gridPedidos.SelectedRow.FindControl("lbObservacionesReferencia");
                Label lbGarantia = (Label)gridPedidos.SelectedRow.FindControl("lbGarantia");
                Label lbCotizacion = (Label)gridPedidos.SelectedRow.FindControl("lbCotizacion");
                Label lbTipoPedido = (Label)gridPedidos.SelectedRow.FindControl("lbTipoPedido");
                Label lbTienda = (Label)gridPedidos.SelectedRow.FindControl("lbTienda");
                Label lbVendedor = (Label)gridPedidos.SelectedRow.FindControl("lbVendedor");
                Label lbTipoVenta = (Label)gridPedidos.SelectedRow.FindControl("lbTipoVenta");
                Label lbFinanciera = (Label)gridPedidos.SelectedRow.FindControl("lbFinanciera");
                Label lbNivelPrecio = (Label)gridPedidos.SelectedRow.FindControl("lbNivelPrecio");
                Label lbFechaEntrega = (Label)gridPedidos.SelectedRow.FindControl("lbFechaEntrega");
                Label lbNombreRecibe = (Label)gridPedidos.SelectedRow.FindControl("lbNombreRecibe");
                Label lbEntregaAMPM = (Label)gridPedidos.SelectedRow.FindControl("lbEntregaAMPM");
                Label lbPedido = (Label)gridPedidos.SelectedRow.FindControl("lbPedido");
                Label lbPuntos = (Label)gridPedidos.SelectedRow.FindControl("lbPuntos");
                Label lbValorPuntos = (Label)gridPedidos.SelectedRow.FindControl("lbValorPuntos");
                Label lbAutorizacionPuntos = (Label)gridPedidos.SelectedRow.FindControl("lbAutorizacionPuntos");
                Label lbLocalF09 = (Label)gridPedidos.SelectedRow.FindControl("lbLocalF09");
                Label lbRefacturacion = (Label)gridPedidos.SelectedRow.FindControl("lbRefacturacion");
                Label lbPrimerPago = (Label)gridPedidos.SelectedRow.FindControl("lbPrimerPago");
                string descuento = "";
                string descuentoVales = string.Empty;
                try
                {
                     descuento = ((Label)gridPedidos.SelectedRow.FindControl("lbDescuento")).Text;
                }
                catch
                {
                    descuento = "0.00";
                }
                try
                {
                    descuentoVales = ((Label)gridPedidos.SelectedRow.FindControl("lbDescuentoVales")).Text;
                }
                catch
                {
                    descuentoVales = "0.00";
                }
                limpiar(true);
                tblBuscar.Visible = false;
                gridClientes.Visible = false;

               
                CargaVendedores();

                try
                {
                    cbVendedor.SelectedValue = lbVendedor.Text;
                }
                catch
                {
                    if (lbVendedor.Text != "0002" && lbVendedor.Text != "0107")
                    {
                        lbError.Text = "El vendedor de este pedido no está disponible, si lo desea facturar, por favor haga otro pedido.";
                        lkGrabar.Visible = false;
                        lkFacturar.Visible = false;
                    }
                }

                try
                {
                    cbTipo.SelectedValue = lbTipoVenta.Text;
                }
                catch
                {
                    lbError.Text = "La promoción de este pedido no está disponible.";
                    lkGrabar.Visible = false;
                    lkFacturar.Visible = false;
                }

                try
                {
                    cbFinanciera.SelectedValue = lbFinanciera.Text;
                }
                catch
                {
                    lbError.Text = "La financiera de este pedido no está disponible.";
                    lkGrabar.Visible = false;
                    lkFacturar.Visible = false;
                }

                CargaNivelesDePrecio();

                try
                {
                    cbNivelPrecio.SelectedValue = ws.DevuelveNivelPadre(lbTipoVenta.Text, Convert.ToInt32(lbFinanciera.Text), lbNivelPrecio.Text);
                }
                catch
                {
                    lbError.Text = "El nivel de precio de este pedido no está disponible.";
                    lkGrabar.Visible = false;
                    lkFacturar.Visible = false;
                }

                try
                {
                    cbMedio.SelectedValue = lbTipoReferencia.Text;
                }
                catch
                {
                    cbMedio.SelectedValue = "0";
                }

                if (Convert.ToString(Session["Tienda"]) == "F01") cbMedio.SelectedValue = "1";

                try
                {
                    txtCotizacion.Text = lbCotizacion.Text;
                    if (txtCotizacion.Text.Trim().Length == 0) txtCotizacion.Text = "0";
                }
                catch
                {
                    txtCotizacion.Text = "0";
                }

                List<wsPuntoVenta.PedidoLinea> qBlanco = new List<wsPuntoVenta.PedidoLinea>();
                ViewState["qArticulos"] = qBlanco;

                gridArticulos.DataSource = qBlanco;
                gridArticulos.DataBind();
                txtPedido.Text = lbPedido.Text;
                txtFecha.Text = lbFecha.Text;

                //-------------
                //nueva lista de descuentos en artículos
                //-------------
                List<NuevoPrecioProducto> lstProductos = new List<NuevoPrecioProducto>();

                //Se obtiene el factor para la formula del calculo de la cuota
                double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);


                var qq = ws.DevuelvePedidoLinea(txtPedido.Text);
                List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();

                var mDescuentoTotal = qq.Sum(x => x.Descuento);
                var mMontoTotal = decimal.Parse(lbMonto.Text.Replace(",", ""));
                var mTotalFacturar = Math.Round(mMontoTotal / (decimal)mFactor + mDescuentoTotal, 2, MidpointRounding.AwayFromZero);
                mTotalFacturar = decimal.Parse(lbTotalFacturar.Text.Replace(",",""));

                ViewState["qArticulos"] = q;


                for (int ii = 0; ii < qq.Length; ii++)
                {
                    wsPuntoVenta.PedidoLinea item = new wsPuntoVenta.PedidoLinea();
                    item.Articulo = qq[ii].Articulo;
                    item.Nombre = qq[ii].Descripcion.ToUpper();
                    item.PrecioUnitario = qq[ii].PrecioUnitario;
                    item.CantidadPedida = qq[ii].CantidadPedida;
                    item.PrecioTotal = qq[ii].PrecioTotal;
                    item.Bodega = qq[ii].Bodega;
                    item.Localizacion = qq[ii].Localizacion;
                    item.RequisicionArmado = qq[ii].RequisicionArmado;
                    item.Descripcion = qq[ii].Descripcion;
                    item.Comentario = qq[ii].Comentario;
                    item.Estado = qq[ii].Estado;
                    item.NumeroPedido = qq[ii].NumeroPedido;
                    item.Oferta = qq[ii].Oferta;
                    item.FechaOfertaDesde = qq[ii].FechaOfertaDesde;
                    item.PrecioOriginal = qq[ii].PrecioOriginal;
                    item.PrecioSugerido = qq[ii].PrecioSugerido;
                    item.TipoOferta = qq[ii].TipoOferta;
                    item.EsDetalleKit = qq[ii].EsDetalleKit;
                    item.PrecioFacturar = qq[ii].PrecioFacturar;
                    item.Vale = qq[ii].Vale;
                    item.Autorizacion = qq[ii].Autorizacion;
                    item.PrecioBaseLocal = qq[ii].PrecioBaseLocal;
                    item.Linea = qq[ii].Linea;
                    item.Gel = qq[ii].Gel;
                    
                    item.Descuento = qq.Where(x => x.Articulo == item.Articulo).FirstOrDefault().Descuento;
                    item.PorcentajeDescuento = item.PrecioFacturar>0?item.Descuento*100/ item.PrecioFacturar:0;
                    item.NetoFacturar = item.PrecioTotal;

                    q.Add(item);

                    //Lista de Precios con descuento
                    int Pagos = 1;
                    List<string> numbers = Regex.Split(cbNivelPrecio.SelectedValue, @"\D+").ToList();
                    numbers.ForEach(x =>
                    {
                        if (!string.IsNullOrEmpty(x))
                        {
                            Pagos = int.Parse(x);
                        }
                    });
                    lstProductos.Add(new NuevoPrecioProducto
                    {
                        Index = ii,
                        factor = (decimal)mFactor,
                        itemCode = item.Articulo,
                        originalPrice = item.PrecioUnitario,
                        originalDiscountPrice = item.PrecioSugerido,
                        price = (item.PrecioUnitario) - qq[ii].Descuento
                        ,payment= Pagos
                    });
                }

                Session["lstNuevoPrecioProducto"] = lstProductos;
                //string json = JsonConvert.SerializeObject(lstProductos);
                //log.Debug("JSON ARTICULOS VALIDAR: " + json);
                bool modificarPrecios = PuedeModificarPrecios();
                var SAldoDesc = modificarPrecios ? 0 : Facturacion.ObtenerSaldoDescuento(lstProductos);
                txtSaldoDescuento.Text = String.Format("{0:0,0.00}", SAldoDesc > 0 ? SAldoDesc : 0);
               

                gridArticulos.DataSource = q;
                gridArticulos.DataBind();
                gridArticulos.Visible = true;
                gridArticulos.SelectedIndex = -1;

                ToolTipPromo();
                ViewState["qArticulos"] = q;


                #region "Encabezado"

                txtFacturar.Text = String.Format("{0:0,0.00}", mTotalFacturar);
                txtEnganche.Text = lbEnganche.Text;
                txtSaldoFinanciar.Text = lbSaldoFinanciar.Text;
                txtIntereses.Text = lbRecargos.Text;
                txtMonto.Text = lbMonto.Text;// String.Format("{0:0,0.00}", (decimal.Parse(lbTotalFacturar.Text.Replace(",",""))/(decimal)mFactor - mDescuentoTotal)*(decimal)mFactor);
                txtDescuentosVales.Text = descuentoVales;
                txtDescuentosVales2.Text = descuentoVales;
                cbPagos1.SelectedValue = Convert.ToInt32(lbCantidadPagos1.Text) < 10 ? string.Format("0{0}", lbCantidadPagos1.Text) : lbCantidadPagos1.Text;
                txtPagos1.Text = Convert.ToDouble(lbMontoPagos1.Text.Replace(",", "")) > 0 ? String.Format("{0:0,0.00}", Convert.ToDouble(lbMontoPagos1.Text.Replace(",", ""))) : "0.00";
                cbPagos2.SelectedValue = Convert.ToInt32(lbCantidadPagos2.Text) < 10 ? string.Format("0{0}", lbCantidadPagos2.Text) : lbCantidadPagos2.Text;
                txtPagos2.Text = Convert.ToDouble(lbMontoPagos2.Text.Replace(",", "")) > 0 ? String.Format("{0:0,0.00}", Convert.ToDouble(lbMontoPagos2.Text.Replace(",", ""))) : "0.00";
                txtPagare.Text = lbPagare.Text == "0" ? "" : lbPagare.Text;
                txtSolicitud.Text = lbSolicitud.Text == "0" ? "" : lbSolicitud.Text;
                txtQuienAutorizo.Text = lbNombreAutorizacion.Text;
                txtObservaciones.Text = lbObservaciones.Text;
                txtQuienAutorizo.Text = lbNombreAutorizacion.Text;
                txtNoAutorizacion.Text = lbAutorizacion.Text;
                txtObservaciones.Text = lbObservaciones.Text;
                txtNotasTipoVenta.Text = lbObservacionesTipoVenta.Text;
                txtOtro.Text = lbObservacionesReferencia.Text;
                txtNombreRecibe.Text = lbNombreRecibe.Text;
                txtNoAutorizacion.Text = lbAutorizacion.Text;
                txtDescuentos.Text = String.Format("{0:0,0.00}", mDescuentoTotal);
                txtGarantia.Text = lbGarantia.Text;
                cbTipoPedido.SelectedValue = lbTipoPedido.Text;
                cbTienda.SelectedValue = lbTienda.Text;
                txtNombreRecibe.Text = lbNombreRecibe.Text;
                cbEntrega.SelectedValue = lbEntregaAMPM.Text;
                cbLocalF09.SelectedValue = lbLocalF09.Text;


                try
                {
                    cbRefacturacion.SelectedValue = lbRefacturacion.Text;
                }
                catch
                {
                    cbRefacturacion.SelectedValue = "N";
                }

                try
                {
                    DateTime mFechaEntrega = Convert.ToDateTime(lbFechaEntrega.Text);

                    cbDia.SelectedValue = mFechaEntrega.Day.ToString();
                    cbMes.SelectedValue = mFechaEntrega.Month.ToString();
                    txtAnio.Text = mFechaEntrega.Year.ToString();
                }
                catch
                {
                    cbDia.SelectedValue = "--";
                    cbMes.SelectedValue = "--";
                    txtAnio.Text = "";
                }

                DateTime mPrimerPago;
                try
                {
                    mPrimerPago = Convert.ToDateTime(lbPrimerPago.Text);
                }
                catch
                {
                    mPrimerPago = DateTime.Now.Date.AddDays(30);
                }

                cbDiaPrimerPago.SelectedValue = mPrimerPago.Day.ToString();
                cbMesPrimerPago.SelectedValue = mPrimerPago.Month.ToString();
                txtAnioPrimerPago.Text = mPrimerPago.Year.ToString();

                #endregion

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al seleccionar el pedido {0} {1}", ex.Message, m);
            }
        }

        protected void gridPedidos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");

                    Label lbCliente = (Label)e.Row.FindControl("lbCliente");
                    Label lbNombre = (Label)e.Row.FindControl("lbNombreCliente");
                    LinkButton lkExpedienteEnviar = (LinkButton)e.Row.FindControl("lkExpedienteEnviar");
                    LinkButton lkReqTransportista = (LinkButton)e.Row.FindControl("lkReqTransportista");

                    lbCliente.ToolTip = lbNombre.Text;

                    var ws = new wsPuntoVenta.wsPuntoVenta();
                    if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                    var q = ws.DevuelveConfiguracion();

                    string mTipArmados = "Enviar el expediente al personal de Armados Industriales. (El pedido debe estar facturado)";
                    string mTipExpedienteEnviar = "Haga clic aquí para enviar el expediente a oficinas centrales. (El Pedido debe estar facturado)";
                    string mTipReqTransportista = "Haga clic aquí para enviar requisición de transportista para este pedido. (El Pedido debe estar facturado)";

                    if (q[0].MailOficina1.Trim().Length > 0 || q[0].MailOficina2.Trim().Length > 0 || q[0].MailOficina3.Trim().Length > 0) mTipExpedienteEnviar = string.Format("{0}{1}{2}", mTipExpedienteEnviar, System.Environment.NewLine, "Será enviado a las siguientes direcciones:");
                    if (q[0].MailBodega1.Trim().Length > 0 || q[0].MailBodega2.Trim().Length > 0 || q[0].MailBodega3.Trim().Length > 0) mTipReqTransportista = string.Format("{0}{1}{2}", mTipReqTransportista, System.Environment.NewLine, "Será enviada a las siguientes direcciones:");

                    if (q[0].MailOficina1.Trim().Length > 0) mTipExpedienteEnviar = string.Format("{0}{1}{2}", mTipExpedienteEnviar, System.Environment.NewLine, q[0].MailOficina1);
                    if (q[0].MailOficina2.Trim().Length > 0) mTipExpedienteEnviar = string.Format("{0}{1}{2}", mTipExpedienteEnviar, System.Environment.NewLine, q[0].MailOficina2);
                    if (q[0].MailOficina3.Trim().Length > 0) mTipExpedienteEnviar = string.Format("{0}{1}{2}", mTipExpedienteEnviar, System.Environment.NewLine, q[0].MailOficina3);

                    if (q[0].MailBodega1.Trim().Length > 0) mTipReqTransportista = string.Format("{0}{1}{2}", mTipReqTransportista, System.Environment.NewLine, q[0].MailBodega1);
                    if (q[0].MailBodega2.Trim().Length > 0) mTipReqTransportista = string.Format("{0}{1}{2}", mTipReqTransportista, System.Environment.NewLine, q[0].MailBodega2);
                    if (q[0].MailBodega3.Trim().Length > 0) mTipReqTransportista = string.Format("{0}{1}{2}", mTipReqTransportista, System.Environment.NewLine, q[0].MailBodega3);

                    if (q[0].MailArmados1.Trim().Length > 0) mTipArmados = string.Format("{0}{1}{2}", mTipArmados, System.Environment.NewLine, q[0].MailArmados1);
                    if (q[0].MailArmados2.Trim().Length > 0) mTipArmados = string.Format("{0}{1}{2}", mTipArmados, System.Environment.NewLine, q[0].MailArmados2);
                    if (q[0].MailArmados3.Trim().Length > 0) mTipArmados = string.Format("{0}{1}{2}", mTipArmados, System.Environment.NewLine, q[0].MailArmados3);

                    lkEnviarArmados.ToolTip = mTipArmados.Replace(" (El Pedido debe estar facturado)", "");
                    lkEnviarExpedienteOficina.ToolTip = mTipExpedienteEnviar.Replace(" (El Pedido debe estar facturado)", "");
                    lkEnviarReqTransportista.ToolTip = mTipReqTransportista.Replace(" (El Pedido debe estar facturado)", "");
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al trabajar el pedido {0} {1}", ex.Message, m);
            }
        }

        bool ValidarSolicitud(bool ValidarArticulo)
        {
            if (txtCodigo.Text.Trim().Length == 0)
            {
                lbError2.Visible = true;
                lbError2.Text = "Debe seleccionar un cliente.";
                mostrarBusquedaCliente();
                return false;
            }
            if (cbVendedor.Text.Trim().Length == 0)
            {
                lbError2.Visible = true;
                lbError2.Text = "Debe seleccionar el vendedor.";
                cbVendedor.Focus();
                return false;
            }
            if (articulo.Text.Trim().Length == 0 && ValidarArticulo)
            {
                lbError2.Visible = true;
                lbError2.Text = "Debe seleccionar un artículo para solicitar autorización";
                articulo.Focus();
                return false;
            }
            //if (txtOferta.Text == "S")
            //{
            //    lbError2.Visible = true;
            //    lbError2.Text = "Este artículo está en oferta, no es posible solicitar descuento especial";
            //    articulo.Focus();
            //    return false;
            //}

            return true;
        }

        protected void lkSolicitar_Click(object sender, EventArgs e)
        {
            MostrarInfoSolicitud();
        }

        void MostrarInfoSolicitud()
        {
            if (cbTipoPedido.SelectedValue != "N")
            {
                lbError2.Visible = true;
                lbError2.Text = "En este tipo de pedidos no es posible realizar solicitudes especiales.";
                return;
            }

            LimpiarCamposSolicitud();

            TextoSolicitud.InnerText = "Solicitud especial";
            if (articulo.Text.Trim().Length > 0) TextoSolicitud.InnerText = string.Format("Solicitud especial para el artículo  {0} - {1}.", articulo.Text, descripcion.Text);

            lbAutorizada.Visible = false;
            lbRechazada.Visible = false;
            lbPendiente.Visible = true;

            tblSolicutudAutorizacion.Visible = true;
            txtPrecioSolicitar.Focus();
        }

        void LimpiarCamposSolicitud()
        {
            txtPrecioSolicitar.Text = "";
            txtObservacionesSolicitarAut.Text = "";
            txtGerente.Text = "";
            txtGerenteComentario.Text = "";
            txtPrecioAutorizado.Text = "";
            txtAut.Text = "";

            txtPrecioSolicitar.ReadOnly = false;
            txtObservacionesSolicitarAut.ReadOnly = false;

            lbError2.Text = "";
            lbError2.Visible = false;
            lbSolicitudEnviada.Visible = false;
        }

        protected void lkEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtAut.Text.Length > 0)
                {
                    lbError2.Visible = true;
                    lbError2.Text = "Esta solicitud está en tránsito, no es posible enviar nada en este momento";
                    return;
                }

                if (!ValidarSolicitud(true)) return;

                decimal mPrecioSolicitado = 0; decimal mPrecioUnitario = 0;

                try
                {
                    mPrecioSolicitado = Convert.ToDecimal(txtPrecioSolicitar.Text.Replace(",", ""));
                }
                catch
                {
                    lbError2.Visible = true;
                    lbError2.Text = "El precio a solicitar es inválido";
                    txtPrecioSolicitar.Focus();
                    return;
                }
                try
                {
                    mPrecioUnitario = Convert.ToDecimal(txtPrecioUnitario.Text.Replace(",", ""));
                }
                catch
                {
                    lbError2.Visible = true;
                    lbError2.Text = "El precio unitario del artículo es inválido";
                    articulo.Focus();
                    return;
                }

                if (mPrecioSolicitado <= 0)
                {
                    lbError2.Visible = true;
                    lbError2.Text = "El precio a solicitar es inválido";
                    txtPrecioSolicitar.Focus();
                    return;
                }
                //if (mPrecioSolicitado > mPrecioUnitario)
                //{
                //    lbError2.Visible = true;
                //    lbError2.Text = "El precio a solicitar es mayor al precio unitario, no es posible continuar";
                //    txtPrecioSolicitar.Focus();
                //    return;
                //}

                if (txtObservacionesSolicitarAut.Text.Trim().Length == 0)
                {
                    lbError2.Visible = true;
                    lbError2.Text = "Debe ingresar las observaciones para la solicitud";
                    txtObservacionesSolicitarAut.Focus();
                    return;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mMensaje = ""; Int64 mAutorizacion = 0;
                wsPuntoVenta.Autorizaciones[] q = new wsPuntoVenta.Autorizaciones[1];

                wsPuntoVenta.Autorizaciones item = new wsPuntoVenta.Autorizaciones();
                item.Autorizacion = 0;
                item.Fecha = DateTime.Now;
                item.Cliente = txtCodigo.Text;
                item.Tipo = "P";
                item.ClienteNombre = txtNombre.Text;
                item.Articulo = articulo.Text;
                item.ArticuloNombre = descripcion.Text;
                item.TipoVenta = cbTipo.SelectedValue;
                item.Financiera = cbFinanciera.SelectedValue;
                item.NivelPrecio = cbNivelPrecio.SelectedValue;
                item.TipoVentaNombre = "";
                item.FinancieraNombre = "";
                item.Precio = Convert.ToDecimal(txtPrecioUnitario.Text.Replace(",", ""));
                item.PrecioLista = 0;
                item.PrecioSolicitado = mPrecioSolicitado;
                item.Cantidad = Convert.ToInt32(txtCantidad.Text.Replace(",", ""));
                item.Diferencia = 0;
                item.Observaciones = txtObservacionesSolicitarAut.Text.Trim();
                item.Tienda = cbTienda.SelectedValue;
                item.Vendedor = cbVendedor.SelectedValue;
                item.VendedorNombre = "";
                item.Gerente = "";
                item.Comentario = "";
                item.PrecioAutorizado = 0;
                item.Costo = 0;
                item.PrecioContado = 0;
                item.PctjDescuento = 0;
                item.Margen = 0;
                item.MargenAutorizar = 0;
                item.Factor = 0;
                item.FechaVence = DateTime.Now;
                item.Estatus = "";
                item.EstatusDescripcion = "";
                item.Estado = "";
                item.EstadoDescripcion = "";
                q[0] = item;

                if (!ws.GrabarSolicitudAutorizacion(ref mAutorizacion, ref mMensaje, q, Convert.ToString(Session["Usuario"])))
                {
                    lbError2.Visible = true;
                    lbError2.Text = mMensaje;
                    return;
                }

                lbError2.Visible = false;
                lbError2.Text = "";
                lbSolicitudEnviada.Visible = true;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format("Error al enviar la solicitud {0}.", ex.Message, m);
            }
        }

        protected void lkVerificar_Click(object sender, EventArgs e)
        {
            VerificarSolicitud();
        }

        void VerificarSolicitud()
        {
            try
            {
                if (!ValidarSolicitud(false)) return;

                LimpiarCamposSolicitud();

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var q = ws.DevuelveAutorizacionesCliente(txtCodigo.Text, cbVendedor.SelectedValue);

                if (q.Length == 0)
                {
                    lbError2.Visible = true;
                    lbError2.Text = "El cliente no tiene solicitudes pendientes";
                    TextoSolicitud.InnerText = "Solicitud especial";
                }
                else
                {
                    txtPrecioSolicitar.Text = String.Format("{0:0,0.00}", q[0].PrecioSolicitado);
                    txtObservacionesSolicitarAut.Text = q[0].Observaciones;
                    txtGerente.Text = q[0].Gerente;
                    txtPrecioAutorizado.Text = q[0].Estatus == "A" ? String.Format("{0:0,0.00}", q[0].PrecioAutorizado) : "";
                    txtGerenteComentario.Text = q[0].Comentario;
                    txtAut.Text = q[0].Autorizacion.ToString();
                    cbVendedor.SelectedValue = q[0].Vendedor;

                    lbAutorizada.Visible = false;
                    lbRechazada.Visible = false;
                    lbPendiente.Visible = false;

                    txtPrecioSolicitar.ReadOnly = true;
                    txtObservacionesSolicitarAut.ReadOnly = true;

                    if (q[0].Estatus == "P") lbPendiente.Visible = true;
                    if (q[0].Estatus == "A") lbAutorizada.Visible = true;
                    if (q[0].Estatus == "R") lbRechazada.Visible = true;

                    TextoSolicitud.InnerText = string.Format("Solicitud especial para el artículo  {0} - {1}.", q[0].Articulo, q[0].ArticuloNombre);
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbError.Text = string.Format("Error al aplicar {0} {1}", ex.Message, m);
            }
        }

        protected void lkAplicar_Click(object sender, EventArgs e)
        {
            AplicarSolicitud();
        }

        void AplicarSolicitud()
        {
            try
            {
                lbError2.Text = "";
                lbError2.Visible = false;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (txtAut.Text == "")
                {
                    lbError2.Visible = true;
                    lbError2.Text = "No hay ninguna solicitud para aplicar";
                    return;
                }
                if (!lbAutorizada.Visible)
                {
                    lbError2.Visible = true;
                    lbError2.Text = "La solicitud no está autorizada, no es posible aplicarla";
                    return;
                }

                var q = ws.DevuelveAutorizacion(txtAut.Text);

                if (q[0].Cliente != txtCodigo.Text)
                {
                    lbError2.Visible = true;
                    lbError2.Text = string.Format("Esta solicitud pertenece al cliente {0}", q[0].Cliente);
                    return;
                }

                if (DateTime.Now.Date > q[0].FechaVence)
                {
                    lbError2.Visible = true;
                    lbError2.Text = string.Format("La autorización de esta solicitud venció el {0}", q[0].FechaVence.ToShortDateString());
                    return;
                }

                if (cbTipo.SelectedValue != q[0].TipoVenta || cbFinanciera.SelectedValue != q[0].Financiera || cbNivelPrecio.SelectedValue != q[0].NivelPrecio)
                {
                    cbTipo.SelectedValue = q[0].TipoVenta;
                    cbFinanciera.SelectedValue = q[0].Financiera;

                    CargaNivelesDePrecio();
                    cbNivelPrecio.SelectedValue = ws.DevuelveNivelPadre(q[0].TipoVenta, Convert.ToInt32(q[0].Financiera), q[0].NivelPrecio);

                    List<wsPuntoVenta.PedidoLinea> qq = new List<wsPuntoVenta.PedidoLinea>();
                    ViewState["qArticulos"] = qq;

                    gridArticulos.DataSource = qq;
                    gridArticulos.DataBind();
                }

                txtAutorizacion.Text = txtAut.Text;
                articulo.Text = q[0].Articulo;
                codigoArticuloDet.Text = q[0].Articulo;

                BuscarArticulo("C");
                gridArticulosDet.SelectedIndex = 0;

                descripcion.Text = q[0].ArticuloNombre;
                txtCantidad.Text = q[0].Cantidad.ToString();
                txtPrecioUnitario.Text = String.Format("{0:0,0.00}", q[0].PrecioAutorizadoNivel);
                txtPrecioUnitarioDebioFacturar.Text = String.Format("{0:0,0.00}", q[0].PrecioAutorizadoNivel);
                txtTotal.Text = String.Format("{0:0,0.00}", Convert.ToDecimal(txtPrecioUnitario.Text.Replace(",", "")) * Convert.ToInt32(txtCantidad.Text.Replace(",", "")));

                tablaBusquedaDet.Visible = false;
                gridArticulosDet.Visible = false;

                txtPrecioUnitario.ReadOnly = true;
                txtPrecioUnitario.AutoPostBack = false;

                txtCantidad.ReadOnly = true;
                tblSolicutudAutorizacion.Visible = false;
            }
            catch (Exception ex)
            {
                lbError2.Visible = true;
                lbError2.Text = string.Format("Error al aplicar la solicitud. {0}", ex.Message);
            }
        }

        protected void lkOcultarSolicitudAutorizacion_Click(object sender, EventArgs e)
        {
            lbError2.Text = "";
            lbError2.Visible = false;
            tblSolicutudAutorizacion.Visible = false;
        }

        protected void lkPedidosCliente_Click(object sender, EventArgs e)
        {
            if (txtCodigo.Text.Trim().Length == 0)
            {
                lbError2.Visible = true;
                lbError.Text = "Debe seleccionar un cliente";
                return;
            }

            lbError2.Text = "";
            lbError2.Visible = false;

            tblFactura.Visible = false;
            tblDespacho.Visible = false;
            tblSolicutudAutorizacion.Visible = false;
            tablaBusquedaDet.Visible = false;
            gridArticulosDet.Visible = false;

            CargarPedidosCliente();
        }

        protected void lkFacturarPedido_Click(object sender, EventArgs e)
        {
            ViewState["AccionPedido"] = "Facturar";
        }

        protected void lkDespacharPedido_Click(object sender, EventArgs e)
        {
            ViewState["AccionPedido"] = "Despachar";
        }

        protected void lkEliminarPedido_Click(object sender, EventArgs e)
        {
            ViewState["AccionPedido"] = "Eliminar";
        }

        protected void lkSeleccionarPedido_Click(object sender, EventArgs e)
        {
            ViewState["AccionPedido"] = "Seleccionar";
        }

        protected void lkInterconsumo_Click(object sender, EventArgs e)
        {
            ViewState["AccionPedido"] = "Interconsumo";
        }

        protected void lkSolicitudCredito_Click(object sender, EventArgs e)
        {
            ViewState["AccionPedido"] = "Solicitud";
        }

        protected void lkPagare_Click(object sender, EventArgs e)
        {
            ViewState["AccionPedido"] = "Pagaré";
        }

        protected void lkExpediente_Click(object sender, EventArgs e)
        {
            ViewState["AccionPedido"] = "Expediente";
        }

        protected void lkExpedienteEnviar_Click(object sender, EventArgs e)
        {
            ViewState["AccionPedido"] = "ExpedienteEnviar";
        }

        protected void lkReqTransportista_Click(object sender, EventArgs e)
        {
            ViewState["AccionPedido"] = "ReqTransportista";
        }

        protected void lkReqArmados_Click(object sender, EventArgs e)
        {
            ViewState["AccionPedido"] = "ReqArmados";
        }

        protected void gridCotizaciones_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");

                Label lbObservaciones = (Label)e.Row.FindControl("lbObservaciones");
                Label lbCotizacion = (Label)e.Row.FindControl("lbCotizacion");

                lbCotizacion.ToolTip = string.Format("Observaciones:{0}{1}", System.Environment.NewLine, lbObservaciones.Text);
            }
        }

        protected void lkEliminarCotizacion_Click(object sender, EventArgs e)
        {
            ViewState["AccionCotizacion"] = "Eliminar";
        }

        protected void lkSeleccionarCotizacion_Click(object sender, EventArgs e)
        {
            ViewState["AccionCotizacion"] = "Seleccionar";
        }

        protected void lkImprimirCotizacion_Click(object sender, EventArgs e)
        {
            ViewState["AccionCotizacion"] = "Imprimir";
        }

        protected void lkEnviarCotizacion_Click(object sender, EventArgs e)
        {
            ViewState["AccionCotizacion"] = "Enviar";
        }

        protected void gridCotizaciones_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string mMensaje = "";
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                lbInfo.Text = "";
                lbError.Text = "";

                Label lbCotizacion = (Label)gridCotizaciones.SelectedRow.FindControl("lbCotizacion");
                string mCotizacion = lbCotizacion.Text;

                switch (ViewState["AccionCotizacion"].ToString())
                {
                    case "Eliminar":
                        if (!ws.EliminarCotizacion(ref mMensaje, mCotizacion))
                        {
                            lbError.Text = mMensaje;
                            return;
                        }

                        lbInfo.Text = mMensaje;
                        CargarCotizaciones();
                        break;
                    case "Seleccionar":
                        SeleccionarCotizacion();
                        break;
                    case "Imprimir":
                        ImprimirCotizacion(false);
                        break;
                    case "Enviar":
                        ImprimirCotizacion(true);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbError.Text = string.Format("Error en la cotización. {0} {1}", ex.Message, m);
            }
        }

        void CargarCotizaciones()
        {
            try
            {
                lbError.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var q = ws.DevuelveCotizacionesCliente(txtCodigo.Text, Convert.ToString(Session["Vendedor"]));

                if (q.Length == 0)
                {
                    gridCotizaciones.Visible = false;
                }
                else
                {
                    gridCotizaciones.DataSource = q;
                    gridCotizaciones.DataBind();
                    gridCotizaciones.Visible = true;
                    gridCotizaciones.SelectedIndex = -1;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al cargar las cotizaciones {0} {1}", ex.Message, m);
            }
        }

        void SeleccionarCotizacion()
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                Label lbCotizacion = (Label)gridCotizaciones.SelectedRow.FindControl("lbCotizacion");
                Label lbTipoVenta = (Label)gridCotizaciones.SelectedRow.FindControl("lbTipoVenta");
                Label lbFinanciera = (Label)gridCotizaciones.SelectedRow.FindControl("lbFinanciera");
                Label lbNivelPrecio = (Label)gridCotizaciones.SelectedRow.FindControl("lbNivelPrecio");
                Label lbFecha = (Label)gridCotizaciones.SelectedRow.FindControl("lbFecha");
                Label lbFechaVence = (Label)gridCotizaciones.SelectedRow.FindControl("lbFechaEntrega");
                Label lbTotalFacturar = (Label)gridCotizaciones.SelectedRow.FindControl("lbTotalFacturar");
                Label lbEnganche = (Label)gridCotizaciones.SelectedRow.FindControl("lbEnganche");
                Label lbSaldoFinanciar = (Label)gridCotizaciones.SelectedRow.FindControl("lbSaldoFinanciar");
                Label lbRecargos = (Label)gridCotizaciones.SelectedRow.FindControl("lbRecargos");
                Label lbMonto = (Label)gridCotizaciones.SelectedRow.FindControl("lbMonto");
                Label lbCantidadPagos1 = (Label)gridCotizaciones.SelectedRow.FindControl("lbCantidadPagos1");
                Label lbMontoPagos1 = (Label)gridCotizaciones.SelectedRow.FindControl("lbMontoPagos1");
                Label lbCantidadPagos2 = (Label)gridCotizaciones.SelectedRow.FindControl("lbCantidadPagos2");
                Label lbMontoPagos2 = (Label)gridCotizaciones.SelectedRow.FindControl("lbMontoPagos2");
                Label lbObservaciones = (Label)gridCotizaciones.SelectedRow.FindControl("lbObservaciones");
                Label lbVendedor = (Label)gridCotizaciones.SelectedRow.FindControl("lbVendedor");
                Label lbNombreCliente = (Label)gridCotizaciones.SelectedRow.FindControl("lbNombreCliente");
                Label lbSolicitud = (Label)gridCotizaciones.SelectedRow.FindControl("lbSolicitud");
                Label lbPagare = (Label)gridCotizaciones.SelectedRow.FindControl("lbPagare");
                Label lbAutorizacionInterconsumo = (Label)gridCotizaciones.SelectedRow.FindControl("lbAutorizacionInterconsumo");
                Label lbNombreAutorizacionInterconsumo = (Label)gridCotizaciones.SelectedRow.FindControl("lbNombreAutorizacionInterconsumo");
                Label lbBodega = (Label)gridCotizaciones.SelectedRow.FindControl("lbBodega");
                decimal Descuento = decimal.Parse(((Label)gridCotizaciones.SelectedRow.FindControl("lbDescuento")).Text.Replace(",",""));
                try
                {
                    cbTipo.SelectedValue = lbTipoVenta.Text;
                }
                catch
                {
                    lbError.Text = "La promoción de esta cotización no está disponible.";
                    return;
                }

                try
                {
                    cbFinanciera.SelectedValue = lbFinanciera.Text;
                }
                catch
                {
                    lbError.Text = "La financiera de esta cotización no está disponible.";
                    return;
                }

                CargaNivelesDePrecio();

                try
                {
                    cbNivelPrecio.SelectedValue = ws.DevuelveNivelPadre(lbTipoVenta.Text, Convert.ToInt32(lbFinanciera.Text), lbNivelPrecio.Text);
                }
                catch
                {
                    lbError.Text = "El nivel de precio de esta cotización no está disponible.";
                    return;
                }

                

                DateTime mFechaVence;

                try
                {
                    string mFechaVenceSTR = lbFechaVence.Text.Replace("-", "/");

                    char[] Separador = { '/' };
                    string[] stringFechaVence = mFechaVenceSTR.Split(Separador);

                    if (stringFechaVence[0].Length == 4)
                    {
                        mFechaVence = new DateTime(Convert.ToInt32(stringFechaVence[0]), Convert.ToInt32(stringFechaVence[1]), Convert.ToInt32(stringFechaVence[2]));
                    }
                    else
                    {
                        mFechaVence = new DateTime(Convert.ToInt32(stringFechaVence[2]), Convert.ToInt32(stringFechaVence[1]), Convert.ToInt32(stringFechaVence[0]));
                    }
                }
                catch
                {
                    mFechaVence = DateTime.Now.Date;
                }

                string mMensaje = "";
                Int32 mDiasVigencia = 0; Int32 mDiasGracia = 0;

                if (!ws.DevuelveDiasCotizaciones(ref mMensaje, ref mDiasVigencia, ref mDiasGracia))
                {
                    lbError.Text = mMensaje;
                    return;
                }

                if (cbTipo.SelectedValue != "NR") mDiasGracia = 0;
                mFechaVence = mFechaVence.AddDays(mDiasGracia);

                if (DateTime.Now.Date > mFechaVence)
                {
                    lbError.Text = string.Format("Esta cotización ya está vencida y se terminaron los {0} dias de gracia autorizados, no es posible utilizarla.", mDiasGracia);
                    return;
                }

                limpiar(true);

                //txtFacturar.Text = lbTotalFacturar.Text;
                //txtEnganche.Text = lbEnganche.Text;
                //txtSaldoFinanciar.Text = lbSaldoFinanciar.Text;
                //txtIntereses.Text = lbRecargos.Text;
                //txtDescuentos.Text = String.Format("{0:0,0.00}", (Convert.ToDecimal(lbTotalFacturar.Text.Replace(",", "")) - Convert.ToDecimal(lbSaldoFinanciar.Text.Replace(",", "")) - Convert.ToDecimal(lbEnganche.Text.Replace(",", ""))).ToString());
                //txtMonto.Text = decimal.Parse(lbMonto.Text.Replace(",","");
                //cbPagos1.SelectedValue = Convert.ToInt32(lbCantidadPagos1.Text) < 10 ? string.Format("0{0}", lbCantidadPagos1.Text) : lbCantidadPagos1.Text;
                //txtPagos1.Text = Convert.ToDouble(lbMontoPagos1.Text.Replace(",", "")) > 0 ? String.Format("{0:0,0.00}", Convert.ToDouble(lbMontoPagos1.Text.Replace(",", ""))) : "0.00";
                //cbPagos2.SelectedValue = Convert.ToInt32(lbCantidadPagos2.Text) < 10 ? string.Format("0{0}", lbCantidadPagos2.Text) : lbCantidadPagos2.Text;
                //txtPagos2.Text = Convert.ToDouble(lbMontoPagos2.Text.Replace(",", "")) > 0 ? String.Format("{0:0,0.00}", Convert.ToDouble(lbMontoPagos2.Text.Replace(",", ""))) : "0.00";
                //txtNombreRecibe.Text = lbNombreCliente.Text;
                //txtCotizacion.Text = lbCotizacion.Text;
                //txtSolicitud.Text = lbSolicitud.Text;
                //txtPagare.Text = lbPagare.Text;
                //txtQuienAutorizo.Text = lbNombreAutorizacionInterconsumo.Text;
                //txtNoAutorizacion.Text = lbAutorizacionInterconsumo.Text;
                
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU")
                    ws.Url = Convert.ToString(ViewState["url"]);
                var factor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

                txtEnganche.Text = lbEnganche.Text;
                txtSaldoFinanciar.Text = lbSaldoFinanciar.Text;
                txtIntereses.Text = factor > 1 ? lbRecargos.Text : "0.00" ;
                txtDescuentos.Text = factor > 1 ? (Descuento>0? String.Format("{0:0,0.00}", Descuento): 
                                                    String.Format("{0:0,0.00}", Math.Round((decimal.Parse(lbMonto.Text.Replace(",", "")) - Convert.ToDecimal(lbSaldoFinanciar.Text.Replace(",", "")) - Convert.ToDecimal(lbEnganche.Text.Replace(",", ""))- Convert.ToDecimal(lbRecargos.Text.Replace(",",""))), 2, MidpointRounding.AwayFromZero).ToString()))
                                                : String.Format("{0:0,0.00}", Descuento) ;
                txtFacturar.Text = String.Format("{0:0,0.00}", decimal.Parse(lbTotalFacturar.Text.Replace(",", "")));
                txtMonto.Text = factor >1 ? String.Format("{0:0,0.00}", decimal.Parse(lbSaldoFinanciar.Text.Replace(",", "")) + decimal.Parse(lbRecargos.Text.Replace(",", ""))) : lbMonto.Text;
                cbPagos1.SelectedValue = Convert.ToInt32(lbCantidadPagos1.Text) < 10 ? string.Format("0{0}", lbCantidadPagos1.Text) : lbCantidadPagos1.Text;
                txtPagos1.Text = Convert.ToDouble(lbMontoPagos1.Text.Replace(",", "")) > 0 ? String.Format("{0:0,0.00}", Convert.ToDouble(lbMontoPagos1.Text.Replace(",", ""))) : "0.00";
                cbPagos2.SelectedValue = Convert.ToInt32(lbCantidadPagos2.Text) < 10 ? string.Format("0{0}", lbCantidadPagos2.Text) : lbCantidadPagos2.Text;
                txtPagos2.Text = Convert.ToDouble(lbMontoPagos2.Text.Replace(",", "")) > 0 ? String.Format("{0:0,0.00}", Convert.ToDouble(lbMontoPagos2.Text.Replace(",", ""))) : "0.00";
                txtNombreRecibe.Text = lbNombreCliente.Text;
                txtCotizacion.Text = lbCotizacion.Text;
                txtSolicitud.Text = lbSolicitud.Text;
                txtPagare.Text = lbPagare.Text;
                txtQuienAutorizo.Text = lbNombreAutorizacionInterconsumo.Text;
                txtNoAutorizacion.Text = lbAutorizacionInterconsumo.Text;

                cbTipo.SelectedValue = lbTipoVenta.Text;
                cbFinanciera.SelectedValue = lbFinanciera.Text;
                CargaNivelesDePrecio();
                cbNivelPrecio.SelectedValue = ws.DevuelveNivelPadre(lbTipoVenta.Text, Convert.ToInt32(lbFinanciera.Text), lbNivelPrecio.Text);

                if (cbTipo.SelectedValue != "NR") 
                    txtNotasTipoVenta.Text = ws.NombrePromocion(lbTipoVenta.Text).ToUpper();

                var qq = ws.DevuelveCotizacionLinea(lbCotizacion.Text);
                List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();
                ViewState["qArticulos"] = q;

                for (int ii = 0; ii < qq.Length; ii++)
                {
                    wsPuntoVenta.PedidoLinea item = new wsPuntoVenta.PedidoLinea();
                    item.Articulo = qq[ii].Articulo;
                    item.Nombre = qq[ii].Descripcion.ToUpper();
                    item.PrecioUnitario = qq[ii].PrecioUnitario;
                    item.CantidadPedida = qq[ii].CantidadPedida;
                    item.PrecioTotal = qq[ii].PrecioTotal;
                    item.Bodega = qq[ii].Bodega;
                    item.Localizacion = qq[ii].Localizacion;
                    item.RequisicionArmado = qq[ii].RequisicionArmado;
                    item.Descripcion = qq[ii].Descripcion;
                    item.Comentario = qq[ii].Comentario;
                    item.Estado = qq[ii].Estado;
                    item.NumeroPedido = qq[ii].NumeroPedido;
                    item.Oferta = qq[ii].Oferta;
                    item.FechaOfertaDesde = qq[ii].FechaOfertaDesde;
                    item.PrecioOriginal = qq[ii].PrecioOriginal;
                    item.TipoOferta = qq[ii].TipoOferta;
                    item.EsDetalleKit = qq[ii].EsDetalleKit;
                    item.PrecioFacturar = qq[ii].PrecioFacturar;
                    item.Vale = qq[ii].Vale;
                    item.Autorizacion = qq[ii].Autorizacion;
                    item.PrecioBaseLocal = qq[ii].PrecioBaseLocal;
                    item.Linea = qq[ii].Linea;
                    item.Gel = qq[ii].Gel;
                    item.PorcentajeDescuento = qq[ii].PorcentajeDescuento;
                    item.Descuento = qq[ii].Descuento;
                    item.NetoFacturar = item.PrecioTotal;

                    q.Add(item);
                }

                gridArticulos.DataSource = q;
                gridArticulos.DataBind();
                gridArticulos.Visible = true;
                gridArticulos.SelectedIndex = -1;

                cbTienda.SelectedValue = lbBodega.Text;
                CargaVendedores();

                try
                {
                    cbVendedor.SelectedValue = lbVendedor.Text;
                }
                catch
                {
                    lbError.Text = string.Format("Esta cotización no le pertenece a usted, comuníquese con {0} de {1} antes de continuar.", ws.DevuelveNombreVendedor(lbVendedor.Text), qq[0].Bodega);
                    cbTienda.SelectedValue = Convert.ToString(Session["Tienda"]);
                    CargaVendedores();
                }

                ToolTipPromo();
                ViewState["qArticulos"] = q;

                string mPrecioBien = ""; string mMonto = ""; string mNotas = "";
                wsPuntoVenta.PedidoLinea[] qLinea = new wsPuntoVenta.PedidoLinea[1];

                if (!ws.FiestaDeAccesorios(ref qLinea, ref mPrecioBien, ref mMonto, cbNivelPrecio.Text, ref mMensaje, ref mNotas, "", lbCotizacion.Text, true)) ViewState["Accesorios"] = "S";
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbError.Text = string.Format("Error al seleccionar la cotización {0} {1}", ex.Message, m);
            }
        }

        protected void lkAsignar_Click(object sender, EventArgs e)
        {
        }

        protected void lkCotizacionesCliente_Click(object sender, EventArgs e)
        {
            CargarCotizaciones();
        }

        void ImprimirCotizacion(bool enviar)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                Label lbCotizacion = (Label)gridCotizaciones.SelectedRow.FindControl("lbCotizacion");
                Label lbTipoVenta = (Label)gridCotizaciones.SelectedRow.FindControl("lbTipoVenta");
                Label lbFinanciera = (Label)gridCotizaciones.SelectedRow.FindControl("lbNotasTipoVenta");
                Label lbNivelPrecio = (Label)gridCotizaciones.SelectedRow.FindControl("lbNivelPrecio");
                Label lbFecha = (Label)gridCotizaciones.SelectedRow.FindControl("lbFecha");
                Label lbFechaVence = (Label)gridCotizaciones.SelectedRow.FindControl("lbFechaEntrega");
                Label lbTotalFacturar = (Label)gridCotizaciones.SelectedRow.FindControl("lbTotalFacturar");
                Label lbEnganche = (Label)gridCotizaciones.SelectedRow.FindControl("lbEnganche");
                Label lbSaldoFinanciar = (Label)gridCotizaciones.SelectedRow.FindControl("lbSaldoFinanciar");
                Label lbRecargos = (Label)gridCotizaciones.SelectedRow.FindControl("lbRecargos");
                Label lbMonto = (Label)gridCotizaciones.SelectedRow.FindControl("lbMonto");
                Label lbCantidadPagos1 = (Label)gridCotizaciones.SelectedRow.FindControl("lbCantidadPagos1");
                Label lbMontoPagos1 = (Label)gridCotizaciones.SelectedRow.FindControl("lbMontoPagos1");
                Label lbCantidadPagos2 = (Label)gridCotizaciones.SelectedRow.FindControl("lbCantidadPagos2");
                Label lbMontoPagos2 = (Label)gridCotizaciones.SelectedRow.FindControl("lbMontoPagos2");
                Label lbObservaciones = (Label)gridCotizaciones.SelectedRow.FindControl("lbObservaciones");
                Label lbNombreRecibe = (Label)gridCotizaciones.SelectedRow.FindControl("lbNombreRecibe");
                Label lbNombre = (Label)gridCotizaciones.SelectedRow.FindControl("lbNombreAutorizacion");
                Label lbPlan = (Label)gridCotizaciones.SelectedRow.FindControl("lbAutorizacion");
                Label lbTelefono = (Label)gridCotizaciones.SelectedRow.FindControl("lbEntregaAMPM");
                Label lbEmailVendedor = (Label)gridCotizaciones.SelectedRow.FindControl("lbDesarmarla");
                Label lbTienda = (Label)gridCotizaciones.SelectedRow.FindControl("lbGarantia");
                Label lbVendedor = (Label)gridCotizaciones.SelectedRow.FindControl("lbObservacionesTipoVenta");
                Label lbDepartamento = (Label)gridCotizaciones.SelectedRow.FindControl("lbTipoReferencia");
                Label lbTelefonoCliente = (Label)gridCotizaciones.SelectedRow.FindControl("lbObservacionesReferencia");

                dsPuntoVenta ds = new dsPuntoVenta();

                DataRow row = ds.Cotizacion.NewRow();
                row["Cotizacion"] = lbCotizacion.Text;
                row["Nombre"] = lbNombre.Text;
                row["Fecha"] = Convert.ToDateTime(lbFecha.Text);
                row["TipoVenta"] = lbTipoVenta.Text;
                row["Financiera"] = lbFinanciera.Text;
                row["NivelPrecio"] = lbNivelPrecio.Text;
                row["Plan"] = lbPlan.Text;
                row["Email"] = lbNombreRecibe.Text;
                row["Observaciones"] = lbObservaciones.Text;
                row["FechaVence"] = Convert.ToDateTime(lbFechaVence.Text);
                row["TotalFacturar"] = Convert.ToDecimal(lbTotalFacturar.Text);
                row["Enganche"] = Convert.ToDecimal(lbEnganche.Text);
                row["SaldoFinanciar"] = Convert.ToDecimal(lbSaldoFinanciar.Text);
                row["Recargos"] = Convert.ToDecimal(lbRecargos.Text);
                row["Monto"] = Convert.ToDecimal(lbMonto.Text);
                row["CantidadPagos1"] = lbCantidadPagos1.Text;
                row["MontoPagos1"] = Convert.ToDecimal(lbMontoPagos1.Text);
                row["CantidadPagos2"] = lbCantidadPagos2.Text;
                row["MontoPagos2"] = Convert.ToDecimal(lbMontoPagos2.Text);
                row["Telefono"] = lbTelefono.Text;
                row["EmailVendedor"] = lbEmailVendedor.Text;
                row["Tienda"] = lbTienda.Text;
                row["Vendedor"] = lbVendedor.Text;
                row["Departamento"] = lbDepartamento.Text;
                row["TelefonoCliente"] = lbTelefonoCliente.Text;
                ds.Cotizacion.Rows.Add(row);

                var qq = ws.DevuelveCotizacionLinea(lbCotizacion.Text);
                List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();

                for (int ii = 0; ii < qq.Length; ii++)
                {
                    DataRow rowDet = ds.CotizacionDet.NewRow();
                    rowDet["Cotizacion"] = lbCotizacion.Text;
                    rowDet["Articulo"] = qq[ii].Articulo;
                    rowDet["Descripcion"] = qq[ii].Descripcion.ToUpper();
                    rowDet["PrecioUnitario"] = Convert.ToDecimal(qq[ii].PrecioUnitario);
                    rowDet["Cantidad"] = Convert.ToInt32(qq[ii].CantidadPedida);
                    rowDet["Total"] = Convert.ToDecimal(qq[ii].PrecioTotal);
                    rowDet["Cuota"] = Convert.ToDecimal(qq[ii].Comentario);
                    ds.CotizacionDet.Rows.Add(rowDet);
                }

                using (ReportDocument ReporteExpediente = new ReportDocument())
                {
                    string m = (Request.PhysicalApplicationPath + "reportes/rptCotizacion.rpt");
                    ReporteExpediente.Load(m);

                    string mCoti = string.Format("Cotizacion_{0}.pdf", lbCotizacion.Text);
                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                    if (File.Exists(@"C:\reportes\" + mCoti)) File.Delete(@"C:\reportes\" + mCoti);

                    ReporteExpediente.SetDataSource(ds);
                    ReporteExpediente.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mCoti);

                    if (enviar)
                    {
                        SmtpClient smtp = new SmtpClient();
                        MailMessage mail = new MailMessage();

                        mail.IsBodyHtml = true;
                        mail.To.Add(lbNombreRecibe.Text);
                        mail.ReplyToList.Add(lbEmailVendedor.Text);
                        mail.From = new MailAddress("puntodeventa@productosmultiples.com", "Punto de Venta");

                        mail.Subject = string.Format("Cotización No. {0} - Muebles Fiesta", lbCotizacion.Text);

                        StringBuilder mBody = new StringBuilder();

                        mBody.Append("<!DOCTYPE html><html lang='en-us'>");
                        mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } </style></head>");
                        mBody.Append("<body>");
                        mBody.Append(string.Format("<p>Estimado(a) Sr(a). {0},</p>", lbNombre.Text));
                        mBody.Append("<p>Adjunto encontrará la cotización solicitada, por favor no olvide visitar nuestra <a href='http://www.mueblesfiesta.com' tabindex='0'>Sitio Web</a>.</p>");
                        mBody.Append("<p>Atentamente,</p>");
                        mBody.Append(string.Format("<p>{0}<BR>Teléfono {1}<BR>{2}<BR><b>Muebles Fiesta - {3}</b></p>", lbVendedor.Text, lbTelefono.Text, lbEmailVendedor.Text, lbTienda.Text));
                        mBody.Append("</body>");
                        mBody.Append("</html>");

                        mail.Body = mBody.ToString();
                        mail.Attachments.Add(new Attachment(@"C:\reportes\" + mCoti));

                        smtp.Host = WebConfigurationManager.AppSettings["MailHost"];
                        smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort"]);

                        smtp.EnableSsl = true;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                        try
                        {
                            smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail"], WebConfigurationManager.AppSettings["PwdMail"]);
                            smtp.Send(mail);
                        }
                        catch (Exception ex2)
                        {
                            lbError.Text = string.Format("Error al enviar la cotización {0}", ex2.Message);
                            return;
                        }

                        lbInfo.Text = string.Format("La cotización fue enviada a {0}", lbNombreRecibe.Text);
                    }
                    else
                    {
                        Response.Clear();
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mCoti);
                        Response.WriteFile(@"C:\reportes\" + mCoti);
                        Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbError.Text = string.Format("Error en la cotización. {0} {1}", ex.Message, m);
            }
        }

        protected void lkAgregarArticulo2_Click(object sender, EventArgs e)
        {
           
        }

        protected void lkEliminarArticulo_Click(object sender, EventArgs e)
        {
            ViewState["AccionArticulo"] = "Eliminar";
        }

        protected void lkCambiarGel_Click(object sender, EventArgs e)
        {
            ViewState["AccionArticulo"] = "Gel";
        }

        protected void lkCambiarBodega_Click(object sender, EventArgs e)
        {
            ViewState["AccionArticulo"] = "Cambiar";
        }

        protected void lkGarantias_Click(object sender, EventArgs e)
        {
            ViewState["AccionArticulo"] = "Garantía";
        }

        void SegundoCloset()
        {
            try
            {
                lbError2.Text = "";
                lbError2.Visible = false;

                List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();
                q = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                int mLinea1a = -1; int mLinea2a = -1;
                decimal mPrecio1a = 0; decimal mPrecio2a = 0;

                int mLinea1b = -1; int mLinea2b = -1;
                decimal mPrecio1b = 0; decimal mPrecio2b = 0;

                int mLinea1c = -1; int mLinea2c = -1;
                decimal mPrecio1c = 0; decimal mPrecio2c = 0;

                int mLinea1d = -1; int mLinea2d = -1;
                decimal mPrecio1d = 0; decimal mPrecio2d = 0;

                int mLinea1e = -1; int mLinea2e = -1;
                decimal mPrecio1e = 0; decimal mPrecio2e = 0;

                int mLinea1f = -1; int mLinea2f = -1;
                decimal mPrecio1f = 0; decimal mPrecio2f = 0;

                int mLinea1g = -1; int mLinea2g = -1;
                decimal mPrecio1g = 0; decimal mPrecio2g = 0;

                int mLinea1h = -1; int mLinea2h = -1;
                decimal mPrecio1h = 0; decimal mPrecio2h = 0;

                int mLinea1i = -1; int mLinea2i = -1;
                decimal mPrecio1i = 0; decimal mPrecio2i = 0;

                int mLinea1j = -1; int mLinea2j = -1;
                decimal mPrecio1j = 0; decimal mPrecio2j = 0;

                foreach (var item in q)
                {
                    if (item.Nombre.Substring(0, 6) == "CLOSET" && item.Oferta == "N")
                    {
                        if (mPrecio1a == 0)
                        {
                            mLinea1a = item.Linea;
                            mPrecio1a = Convert.ToDecimal(item.PrecioTotal);
                        }
                        else
                        {
                            if (mPrecio2a == 0)
                            {
                                mLinea2a = item.Linea;
                                mPrecio2a = Convert.ToDecimal(item.PrecioTotal);
                            }
                            else
                            {
                                if (mPrecio1b == 0)
                                {
                                    mLinea1b = item.Linea;
                                    mPrecio1b = Convert.ToDecimal(item.PrecioTotal);
                                }
                                else
                                {
                                    if (mPrecio2b == 0)
                                    {
                                        mLinea2b = item.Linea;
                                        mPrecio2b = Convert.ToDecimal(item.PrecioTotal);
                                    }
                                    else
                                    {
                                        if (mPrecio1c == 0)
                                        {
                                            mLinea1c = item.Linea;
                                            mPrecio1c = Convert.ToDecimal(item.PrecioTotal);
                                        }
                                        else
                                        {
                                            if (mPrecio2c == 0)
                                            {
                                                mLinea2c = item.Linea;
                                                mPrecio2c = Convert.ToDecimal(item.PrecioTotal);
                                            }
                                            else
                                            {
                                                if (mPrecio1d == 0)
                                                {
                                                    mLinea1d = item.Linea;
                                                    mPrecio1d = Convert.ToDecimal(item.PrecioTotal);
                                                }
                                                else
                                                {
                                                    if (mPrecio2d == 0)
                                                    {
                                                        mLinea2d = item.Linea;
                                                        mPrecio2d = Convert.ToDecimal(item.PrecioTotal);
                                                    }
                                                    else
                                                    {
                                                        if (mPrecio1e == 0)
                                                        {
                                                            mLinea1e = item.Linea;
                                                            mPrecio1e = Convert.ToDecimal(item.PrecioTotal);
                                                        }
                                                        else
                                                        {
                                                            if (mPrecio2e == 0)
                                                            {
                                                                mLinea2e = item.Linea;
                                                                mPrecio2e = Convert.ToDecimal(item.PrecioTotal);
                                                            }
                                                            else
                                                            {
                                                                if (mPrecio1f == 0)
                                                                {
                                                                    mLinea1f = item.Linea;
                                                                    mPrecio1f = Convert.ToDecimal(item.PrecioTotal);
                                                                }
                                                                else
                                                                {
                                                                    if (mPrecio2f == 0)
                                                                    {
                                                                        mLinea2f = item.Linea;
                                                                        mPrecio2f = Convert.ToDecimal(item.PrecioTotal);
                                                                    }
                                                                    else
                                                                    {
                                                                        if (mPrecio1g == 0)
                                                                        {
                                                                            mLinea1g = item.Linea;
                                                                            mPrecio1g = Convert.ToDecimal(item.PrecioTotal);
                                                                        }
                                                                        else
                                                                        {
                                                                            if (mPrecio2g == 0)
                                                                            {
                                                                                mLinea2g = item.Linea;
                                                                                mPrecio2g = Convert.ToDecimal(item.PrecioTotal);
                                                                            }
                                                                            else
                                                                            {
                                                                                if (mPrecio1h == 0)
                                                                                {
                                                                                    mLinea1h = item.Linea;
                                                                                    mPrecio1h = Convert.ToDecimal(item.PrecioTotal);
                                                                                }
                                                                                else
                                                                                {
                                                                                    if (mPrecio2h == 0)
                                                                                    {
                                                                                        mLinea2h = item.Linea;
                                                                                        mPrecio2h = Convert.ToDecimal(item.PrecioTotal);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        if (mPrecio1i == 0)
                                                                                        {
                                                                                            mLinea1i = item.Linea;
                                                                                            mPrecio1i = Convert.ToDecimal(item.PrecioTotal);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            if (mPrecio2i == 0)
                                                                                            {
                                                                                                mLinea2i = item.Linea;
                                                                                                mPrecio2i = Convert.ToDecimal(item.PrecioTotal);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                if (mPrecio1j == 0)
                                                                                                {
                                                                                                    mLinea1j = item.Linea;
                                                                                                    mPrecio1j = Convert.ToDecimal(item.PrecioTotal);
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    if (mPrecio2j == 0)
                                                                                                    {
                                                                                                        mLinea2j = item.Linea;
                                                                                                        mPrecio2j = Convert.ToDecimal(item.PrecioTotal);
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (mPrecio1a == 0 || mPrecio2a == 0)
                {
                    lbError2.Visible = true;
                    lbError2.Text = "Deben haber dos closets no ofertados en el pedido.";
                    return;
                }

                int mLineaModificar1 = -1;
                int mLineaModificar2 = -1;
                int mLineaModificar3 = -1;
                int mLineaModificar4 = -1;
                int mLineaModificar5 = -1;
                int mLineaModificar6 = -1;
                int mLineaModificar7 = -1;
                int mLineaModificar8 = -1;
                int mLineaModificar9 = -1;
                int mLineaModificar10 = -1;

                mLineaModificar1 = mLinea2a;
                if (mPrecio1a < mPrecio2a) mLineaModificar1 = mLinea1a;

                mLineaModificar2 = mLinea2b;
                if (mPrecio1b < mPrecio2b) mLineaModificar2 = mLinea1b;

                mLineaModificar3 = mLinea2c;
                if (mPrecio1c < mPrecio2c) mLineaModificar3 = mLinea1c;

                mLineaModificar4 = mLinea2d;
                if (mPrecio1d < mPrecio2d) mLineaModificar4 = mLinea1d;

                mLineaModificar5 = mLinea2e;
                if (mPrecio1e < mPrecio2e) mLineaModificar5 = mLinea1e;

                mLineaModificar6 = mLinea2f;
                if (mPrecio1f < mPrecio2f) mLineaModificar6 = mLinea1f;

                mLineaModificar7 = mLinea2g;
                if (mPrecio1g < mPrecio2g) mLineaModificar7 = mLinea1g;

                mLineaModificar8 = mLinea2h;
                if (mPrecio1h < mPrecio2h) mLineaModificar8 = mLinea1h;

                mLineaModificar9 = mLinea2i;
                if (mPrecio1i < mPrecio2i) mLineaModificar9 = mLinea1i;

                mLineaModificar10 = mLinea2j;
                if (mPrecio1j < mPrecio2j) mLineaModificar10 = mLinea1j;

                foreach (var item in q)
                {
                    if (item.Linea == mLineaModificar1 || item.Linea == mLineaModificar2 || item.Linea == mLineaModificar3 || item.Linea == mLineaModificar4 || item.Linea == mLineaModificar5 || item.Linea == mLineaModificar6 || item.Linea == mLineaModificar7 || item.Linea == mLineaModificar8 || item.Linea == mLineaModificar9 || item.Linea == mLineaModificar10)
                    {
                        item.PrecioFacturar = item.PrecioFacturar / 2;
                        item.PrecioTotal = item.PrecioTotal / 2;
                        item.PrecioUnitario = item.PrecioUnitario / 2;
                        item.PorcentajeDescuento = 50;
                    }
                }

                decimal mPrecioBien = 0; decimal mMonto = 0;
                foreach (var item in q)
                {
                    mPrecioBien = mPrecioBien + item.PrecioFacturar;
                    mMonto = mMonto + item.PrecioTotal;
                }

                txtNotasTipoVenta.Text = "SEGUNDO CLOSET A MITAD DE PRECIO";

                gridArticulos.DataSource = q;
                gridArticulos.DataBind();

                ViewState["qArticulos"] = q;

                if (cbFinanciera.SelectedValue == "1" || cbFinanciera.SelectedValue == "5" || cbFinanciera.SelectedValue == "9")
                {
                    txtMonto.Text = String.Format("{0:0,0.00}", mMonto);
                    txtFacturar.Text = String.Format("{0:0,0.00}", mPrecioBien);
                }
                else
                {
                    ValidaTotalPedido();
                    ValidaTipoNivelPrecio();
                }

                lkLaTorre.Visible = false;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format(" - Error al aplicar la promoción del segundo closet . {0} {1}", ex.Message, m);
            }
        }

        void FiestaDeDormitorios()
        {
            try
            {
                lbError2.Text = "";
                lbError2.Visible = false;

                List<wsPuntoVenta.PedidoLinea> qOriginal = new List<wsPuntoVenta.PedidoLinea>();
                qOriginal = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                wsPuntoVenta.PedidoLinea[] q = new wsPuntoVenta.PedidoLinea[1];
                wsPuntoVenta.PedidoLinea[] qLinea = new wsPuntoVenta.PedidoLinea[1];

                int jj = 0;
                for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                {
                    jj++;

                    Label lbPrecioUnitario = (Label)gridArticulos.Rows[ii].FindControl("lbPrecio");
                    Label lbCantidadPedida = (Label)gridArticulos.Rows[ii].FindControl("lbCantidad");
                    Label lbPrecioTotal = (Label)gridArticulos.Rows[ii].FindControl("lbTotal");
                    Label lbDescripcion = (Label)gridArticulos.Rows[ii].FindControl("lbDescripcion");
                    Label lbComentario = (Label)gridArticulos.Rows[ii].FindControl("lbComentario");
                    Label lbEstado = (Label)gridArticulos.Rows[ii].FindControl("lbEstado");
                    Label lbNumeroPedido = (Label)gridArticulos.Rows[ii].FindControl("lbNumeroPedido");
                    Label lbOferta = (Label)gridArticulos.Rows[ii].FindControl("lbOferta");
                    Label lbFechaOfertaDesde = (Label)gridArticulos.Rows[ii].FindControl("lbFechaOfertaDesde");
                    Label lbPrecioOriginal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioOriginal");
                    Label lbEsDetalleKit = (Label)gridArticulos.Rows[ii].FindControl("lbEsDetalleKit");
                    Label lbPrecioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioFacturar");
                    Label lbTipoOferta = (Label)gridArticulos.Rows[ii].FindControl("lbTipoOferta");
                    Label lbVale = (Label)gridArticulos.Rows[ii].FindControl("lbVale");
                    Label lbAutorizacion = (Label)gridArticulos.Rows[ii].FindControl("lbAutorizacion");
                    Label lbBodega = (Label)gridArticulos.Rows[ii].FindControl("lbBodega");
                    Label lbLocalizacion = (Label)gridArticulos.Rows[ii].FindControl("lbLocalizacion");
                    LinkButton lkRequisicionArmado = (LinkButton)gridArticulos.Rows[ii].FindControl("lkRequisicionArmado");
                    Label lbArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbArticulo");
                    Label lbNombreArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbNombreArticulo");
                    Label lbPrecioUnitarioDebioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioUnitarioDebioFacturar");
                    Label lbPrecioSugerido = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioSugerido");
                    Label lbPrecioBaseLocal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioBaseLocal");
                    Label lbGel = (Label)gridArticulos.Rows[ii].FindControl("lbGel");
                    Label lbLinea = (Label)gridArticulos.Rows[ii].FindControl("lbLinea");

                    decimal mPrecioOriginal = 0;
                    DateTime mFechaOfertaDesde = new DateTime(1980, 1, 1);

                    try
                    {
                        mFechaOfertaDesde = new DateTime(Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(6, 4)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(3, 2)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(0, 2)));
                    }
                    catch
                    {
                        //Nothing
                    }
                    try
                    {
                        mPrecioOriginal = Convert.ToDecimal(lbPrecioOriginal.Text.Replace(",", ""));
                    }
                    catch
                    {
                        mPrecioOriginal = 0;
                    }


                    wsPuntoVenta.PedidoLinea itemPedidoLinea = new wsPuntoVenta.PedidoLinea();
                    itemPedidoLinea.Articulo = lbArticulo.Text;
                    itemPedidoLinea.Nombre = lbNombreArticulo.Text;
                    itemPedidoLinea.PrecioUnitario = Convert.ToDecimal(lbPrecioUnitario.Text.Replace(",", ""));
                    itemPedidoLinea.CantidadPedida = Convert.ToDecimal(lbCantidadPedida.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioTotal = Convert.ToDecimal(lbPrecioTotal.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioFacturar = Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.Bodega = lbBodega.Text;
                    itemPedidoLinea.Localizacion = lbLocalizacion.Text;
                    itemPedidoLinea.RequisicionArmado = lkRequisicionArmado.Text;
                    itemPedidoLinea.Descripcion = lbDescripcion.Text;
                    itemPedidoLinea.Comentario = lbComentario.Text;
                    itemPedidoLinea.Estado = lbEstado.Text;
                    itemPedidoLinea.NumeroPedido = lbNumeroPedido.Text;
                    itemPedidoLinea.Oferta = lbOferta.Text;
                    itemPedidoLinea.FechaOfertaDesde = mFechaOfertaDesde;
                    itemPedidoLinea.PrecioOriginal = mPrecioOriginal;
                    itemPedidoLinea.TipoOferta = lbTipoOferta.Text;
                    itemPedidoLinea.EsDetalleKit = lbEsDetalleKit.Text;
                    itemPedidoLinea.Vale = Convert.ToInt32(lbVale.Text == "" ? "0" : lbVale.Text);
                    itemPedidoLinea.Autorizacion = Convert.ToInt64(lbAutorizacion.Text == "" ? "0" : lbAutorizacion.Text);
                    itemPedidoLinea.PrecioUnitarioDebioFacturar = Convert.ToDecimal(lbPrecioUnitarioDebioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioSugerido = Convert.ToDecimal(lbPrecioSugerido.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioBaseLocal = Convert.ToDouble(lbPrecioBaseLocal.Text.Replace(",", ""));
                    itemPedidoLinea.Gel = lbGel.Text;
                    itemPedidoLinea.Linea = Convert.ToInt32(lbLinea.Text);

                    if (jj > 1) Array.Resize(ref q, jj);
                    if (jj > 1) Array.Resize(ref qLinea, jj);

                    q[jj - 1] = itemPedidoLinea;
                    qLinea[jj - 1] = itemPedidoLinea;
                }


                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = ""; string mTotal = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.FiestaDeDormitorios(q, ref qLinea, ref mMensaje, cbNivelPrecio.Text, ref mTotal))
                {
                    lbError2.Visible = true;
                    lbError2.Text = mMensaje;
                    return;
                }

                List<wsPuntoVenta.PedidoLinea> qModificar = new List<wsPuntoVenta.PedidoLinea>();
                qModificar = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                foreach (var item in qLinea)
                {
                    foreach (var itemModificar in qModificar)
                    {
                        if (item.PrecioFacturar != itemModificar.PrecioFacturar && item.Linea == itemModificar.Linea)
                        {
                            itemModificar.PrecioFacturar = item.PrecioFacturar;
                            itemModificar.PrecioTotal = item.PrecioTotal;
                            itemModificar.PrecioUnitario = item.PrecioUnitario;
                        }
                    }
                }

                txtNotasTipoVenta.Text = "FIESTA DE DORMITORIOS";

                gridArticulos.DataSource = qModificar;
                gridArticulos.DataBind();

                ViewState["qArticulos"] = qModificar;
                lkLaTorre.Visible = false;

                ValidaTotalPedido();
                ValidaTipoNivelPrecio();

                if (cbFinanciera.SelectedValue == "1" || cbFinanciera.SelectedValue == "5" || cbFinanciera.SelectedValue == "9")
                {
                    txtMonto.Text = mTotal;
                    txtFacturar.Text = mTotal;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format(" - Error al aplicar la Fiesta de Dormitorios. {0} {1}", ex.Message, m);
            }
        }

        void FiestaDeCocinas()
        {
            try
            {
                lbError2.Text = "";
                lbError2.Visible = false;

                List<wsPuntoVenta.PedidoLinea> qOriginal = new List<wsPuntoVenta.PedidoLinea>();
                qOriginal = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                wsPuntoVenta.PedidoLinea[] q = new wsPuntoVenta.PedidoLinea[1];
                wsPuntoVenta.PedidoLinea[] qLinea = new wsPuntoVenta.PedidoLinea[1];

                int jj = 0;
                for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                {
                    jj++;

                    Label lbPrecioUnitario = (Label)gridArticulos.Rows[ii].FindControl("lbPrecio");
                    Label lbCantidadPedida = (Label)gridArticulos.Rows[ii].FindControl("lbCantidad");
                    Label lbPrecioTotal = (Label)gridArticulos.Rows[ii].FindControl("lbTotal");
                    Label lbDescripcion = (Label)gridArticulos.Rows[ii].FindControl("lbDescripcion");
                    Label lbComentario = (Label)gridArticulos.Rows[ii].FindControl("lbComentario");
                    Label lbEstado = (Label)gridArticulos.Rows[ii].FindControl("lbEstado");
                    Label lbNumeroPedido = (Label)gridArticulos.Rows[ii].FindControl("lbNumeroPedido");
                    Label lbOferta = (Label)gridArticulos.Rows[ii].FindControl("lbOferta");
                    Label lbFechaOfertaDesde = (Label)gridArticulos.Rows[ii].FindControl("lbFechaOfertaDesde");
                    Label lbPrecioOriginal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioOriginal");
                    Label lbEsDetalleKit = (Label)gridArticulos.Rows[ii].FindControl("lbEsDetalleKit");
                    Label lbPrecioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioFacturar");
                    Label lbTipoOferta = (Label)gridArticulos.Rows[ii].FindControl("lbTipoOferta");
                    Label lbVale = (Label)gridArticulos.Rows[ii].FindControl("lbVale");
                    Label lbAutorizacion = (Label)gridArticulos.Rows[ii].FindControl("lbAutorizacion");
                    Label lbBodega = (Label)gridArticulos.Rows[ii].FindControl("lbBodega");
                    Label lbLocalizacion = (Label)gridArticulos.Rows[ii].FindControl("lbLocalizacion");
                    LinkButton lkRequisicionArmado = (LinkButton)gridArticulos.Rows[ii].FindControl("lkRequisicionArmado");
                    Label lbArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbArticulo");
                    Label lbNombreArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbNombreArticulo");
                    Label lbPrecioUnitarioDebioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioUnitarioDebioFacturar");
                    Label lbPrecioSugerido = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioSugerido");
                    Label lbPrecioBaseLocal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioBaseLocal");
                    Label lbGel = (Label)gridArticulos.Rows[ii].FindControl("lbGel");
                    Label lbLinea = (Label)gridArticulos.Rows[ii].FindControl("lbLinea");

                    decimal mPrecioOriginal = 0;
                    DateTime mFechaOfertaDesde = new DateTime(1980, 1, 1);

                    try
                    {
                        mFechaOfertaDesde = new DateTime(Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(6, 4)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(3, 2)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(0, 2)));
                    }
                    catch
                    {
                        //Nothing
                    }
                    try
                    {
                        mPrecioOriginal = Convert.ToDecimal(lbPrecioOriginal.Text.Replace(",", ""));
                    }
                    catch
                    {
                        mPrecioOriginal = 0;
                    }


                    wsPuntoVenta.PedidoLinea itemPedidoLinea = new wsPuntoVenta.PedidoLinea();
                    itemPedidoLinea.Articulo = lbArticulo.Text;
                    itemPedidoLinea.Nombre = lbNombreArticulo.Text;
                    itemPedidoLinea.PrecioUnitario = Convert.ToDecimal(lbPrecioUnitario.Text.Replace(",", ""));
                    itemPedidoLinea.CantidadPedida = Convert.ToDecimal(lbCantidadPedida.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioTotal = Convert.ToDecimal(lbPrecioTotal.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioFacturar = Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.Bodega = lbBodega.Text;
                    itemPedidoLinea.Localizacion = lbLocalizacion.Text;
                    itemPedidoLinea.RequisicionArmado = lkRequisicionArmado.Text;
                    itemPedidoLinea.Descripcion = lbDescripcion.Text;
                    itemPedidoLinea.Comentario = lbComentario.Text;
                    itemPedidoLinea.Estado = lbEstado.Text;
                    itemPedidoLinea.NumeroPedido = lbNumeroPedido.Text;
                    itemPedidoLinea.Oferta = lbOferta.Text;
                    itemPedidoLinea.FechaOfertaDesde = mFechaOfertaDesde;
                    itemPedidoLinea.PrecioOriginal = mPrecioOriginal;
                    itemPedidoLinea.TipoOferta = lbTipoOferta.Text;
                    itemPedidoLinea.EsDetalleKit = lbEsDetalleKit.Text;
                    itemPedidoLinea.Vale = Convert.ToInt32(lbVale.Text == "" ? "0" : lbVale.Text);
                    itemPedidoLinea.Autorizacion = Convert.ToInt64(lbAutorizacion.Text == "" ? "0" : lbAutorizacion.Text);
                    itemPedidoLinea.PrecioUnitarioDebioFacturar = Convert.ToDecimal(lbPrecioUnitarioDebioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioSugerido = Convert.ToDecimal(lbPrecioSugerido.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioBaseLocal = Convert.ToDouble(lbPrecioBaseLocal.Text.Replace(",", ""));
                    itemPedidoLinea.Gel = lbGel.Text;
                    itemPedidoLinea.Linea = Convert.ToInt32(lbLinea.Text);

                    if (jj > 1) Array.Resize(ref q, jj);
                    if (jj > 1) Array.Resize(ref qLinea, jj);

                    q[jj - 1] = itemPedidoLinea;
                    qLinea[jj - 1] = itemPedidoLinea;
                }


                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = ""; string mTotal = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.FiestaDeCocinas(q, ref qLinea, ref mMensaje, cbNivelPrecio.Text, ref mTotal))
                {
                    lbError2.Visible = true;
                    lbError2.Text = mMensaje;
                    return;
                }

                List<wsPuntoVenta.PedidoLinea> qModificar = new List<wsPuntoVenta.PedidoLinea>();
                qModificar = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                foreach (var item in qLinea)
                {
                    foreach (var itemModificar in qModificar)
                    {
                        if (item.PrecioFacturar != itemModificar.PrecioFacturar && item.Linea == itemModificar.Linea)
                        {
                            itemModificar.PrecioFacturar = item.PrecioFacturar;
                            itemModificar.PrecioTotal = item.PrecioTotal;
                            itemModificar.PrecioUnitario = item.PrecioUnitario;
                        }
                    }
                }

                txtNotasTipoVenta.Text = "FIESTA DE COCINAS";

                gridArticulos.DataSource = qModificar;
                gridArticulos.DataBind();

                ViewState["qArticulos"] = qModificar;
                lkLaTorre.Visible = false;

                ValidaTotalPedido();
                ValidaTipoNivelPrecio();

                if (cbFinanciera.SelectedValue == "1" || cbFinanciera.SelectedValue == "5" || cbFinanciera.SelectedValue == "9")
                {
                    txtMonto.Text = mTotal;
                    txtFacturar.Text = mTotal;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format(" - Error al aplicar la Fiesta de Cocinas. {0} {1}", ex.Message, m);
            }
        }

        void FiestaDeMayo2016()
        {
            try
            {
                lbError2.Text = "";
                lbError2.Visible = false;

                List<wsPuntoVenta.PedidoLinea> qOriginal = new List<wsPuntoVenta.PedidoLinea>();
                qOriginal = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                wsPuntoVenta.PedidoLinea[] q = new wsPuntoVenta.PedidoLinea[1];
                wsPuntoVenta.PedidoLinea[] qLinea = new wsPuntoVenta.PedidoLinea[1];

                int jj = 0;
                for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                {
                    jj++;

                    Label lbPrecioUnitario = (Label)gridArticulos.Rows[ii].FindControl("lbPrecio");
                    Label lbCantidadPedida = (Label)gridArticulos.Rows[ii].FindControl("lbCantidad");
                    Label lbPrecioTotal = (Label)gridArticulos.Rows[ii].FindControl("lbTotal");
                    Label lbDescripcion = (Label)gridArticulos.Rows[ii].FindControl("lbDescripcion");
                    Label lbComentario = (Label)gridArticulos.Rows[ii].FindControl("lbComentario");
                    Label lbEstado = (Label)gridArticulos.Rows[ii].FindControl("lbEstado");
                    Label lbNumeroPedido = (Label)gridArticulos.Rows[ii].FindControl("lbNumeroPedido");
                    Label lbOferta = (Label)gridArticulos.Rows[ii].FindControl("lbOferta");
                    Label lbFechaOfertaDesde = (Label)gridArticulos.Rows[ii].FindControl("lbFechaOfertaDesde");
                    Label lbPrecioOriginal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioOriginal");
                    Label lbEsDetalleKit = (Label)gridArticulos.Rows[ii].FindControl("lbEsDetalleKit");
                    Label lbPrecioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioFacturar");
                    Label lbTipoOferta = (Label)gridArticulos.Rows[ii].FindControl("lbTipoOferta");
                    Label lbVale = (Label)gridArticulos.Rows[ii].FindControl("lbVale");
                    Label lbAutorizacion = (Label)gridArticulos.Rows[ii].FindControl("lbAutorizacion");
                    Label lbBodega = (Label)gridArticulos.Rows[ii].FindControl("lbBodega");
                    Label lbLocalizacion = (Label)gridArticulos.Rows[ii].FindControl("lbLocalizacion");
                    LinkButton lkRequisicionArmado = (LinkButton)gridArticulos.Rows[ii].FindControl("lkRequisicionArmado");
                    Label lbArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbArticulo");
                    Label lbNombreArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbNombreArticulo");
                    Label lbPrecioUnitarioDebioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioUnitarioDebioFacturar");
                    Label lbPrecioSugerido = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioSugerido");
                    Label lbPrecioBaseLocal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioBaseLocal");
                    Label lbGel = (Label)gridArticulos.Rows[ii].FindControl("lbGel");
                    Label lbLinea = (Label)gridArticulos.Rows[ii].FindControl("lbLinea");

                    decimal mPrecioOriginal = 0;
                    DateTime mFechaOfertaDesde = new DateTime(1980, 1, 1);

                    try
                    {
                        mFechaOfertaDesde = new DateTime(Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(6, 4)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(3, 2)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(0, 2)));
                    }
                    catch
                    {
                        //Nothing
                    }
                    try
                    {
                        mPrecioOriginal = Convert.ToDecimal(lbPrecioOriginal.Text.Replace(",", ""));
                    }
                    catch
                    {
                        mPrecioOriginal = 0;
                    }


                    wsPuntoVenta.PedidoLinea itemPedidoLinea = new wsPuntoVenta.PedidoLinea();
                    itemPedidoLinea.Articulo = lbArticulo.Text;
                    itemPedidoLinea.Nombre = lbNombreArticulo.Text;
                    itemPedidoLinea.PrecioUnitario = Convert.ToDecimal(lbPrecioUnitario.Text.Replace(",", ""));
                    itemPedidoLinea.CantidadPedida = Convert.ToDecimal(lbCantidadPedida.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioTotal = Convert.ToDecimal(lbPrecioTotal.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioFacturar = Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.Bodega = lbBodega.Text;
                    itemPedidoLinea.Localizacion = lbLocalizacion.Text;
                    itemPedidoLinea.RequisicionArmado = lkRequisicionArmado.Text;
                    itemPedidoLinea.Descripcion = lbDescripcion.Text;
                    itemPedidoLinea.Comentario = lbComentario.Text;
                    itemPedidoLinea.Estado = lbEstado.Text;
                    itemPedidoLinea.NumeroPedido = lbNumeroPedido.Text;
                    itemPedidoLinea.Oferta = lbOferta.Text;
                    itemPedidoLinea.FechaOfertaDesde = mFechaOfertaDesde;
                    itemPedidoLinea.PrecioOriginal = mPrecioOriginal;
                    itemPedidoLinea.TipoOferta = lbTipoOferta.Text;
                    itemPedidoLinea.EsDetalleKit = lbEsDetalleKit.Text;
                    itemPedidoLinea.Vale = Convert.ToInt32(lbVale.Text == "" ? "0" : lbVale.Text);
                    itemPedidoLinea.Autorizacion = Convert.ToInt64(lbAutorizacion.Text == "" ? "0" : lbAutorizacion.Text);
                    itemPedidoLinea.PrecioUnitarioDebioFacturar = Convert.ToDecimal(lbPrecioUnitarioDebioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioSugerido = Convert.ToDecimal(lbPrecioSugerido.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioBaseLocal = Convert.ToDouble(lbPrecioBaseLocal.Text.Replace(",", ""));
                    itemPedidoLinea.Gel = lbGel.Text;
                    itemPedidoLinea.Linea = Convert.ToInt32(lbLinea.Text);

                    if (jj > 1) Array.Resize(ref q, jj);
                    if (jj > 1) Array.Resize(ref qLinea, jj);

                    q[jj - 1] = itemPedidoLinea;
                    qLinea[jj - 1] = itemPedidoLinea;
                }


                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = ""; string mTotal = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.FiestaDeMayo2016(q, ref qLinea, ref mMensaje, cbNivelPrecio.Text, ref mTotal))
                {
                    lbError2.Visible = true;
                    lbError2.Text = mMensaje;
                    return;
                }

                List<wsPuntoVenta.PedidoLinea> qModificar = new List<wsPuntoVenta.PedidoLinea>();
                qModificar = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];


                decimal mFacturar = 0;
                foreach (var item in qLinea)
                {
                    mFacturar += item.PrecioFacturar;

                    foreach (var itemModificar in qModificar)
                    {
                        if (item.PrecioFacturar != itemModificar.PrecioFacturar && item.Linea == itemModificar.Linea)
                        {
                            itemModificar.PrecioFacturar = item.PrecioFacturar;
                            itemModificar.PrecioTotal = item.PrecioTotal;
                            itemModificar.PrecioUnitario = item.PrecioUnitario;
                        }
                    }
                }

                txtNotasTipoVenta.Text = "FIESTA DE MAYO 2016";

                gridArticulos.DataSource = qModificar;
                gridArticulos.DataBind();

                ViewState["qArticulos"] = qModificar;
                lkLaTorre.Visible = false;

                if (cbFinanciera.SelectedValue == "1" || cbFinanciera.SelectedValue == "5" || cbFinanciera.SelectedValue == "9")
                {
                    txtMonto.Text = mTotal;
                    txtFacturar.Text = mTotal;
                }

                if (cbFinanciera.SelectedValue == "7")
                {
                    double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                    decimal mEnganche = Convert.ToDecimal(txtEnganche.Text.Trim().Replace(",", ""));
                    decimal mSaldoFinanciar = mFacturar - mEnganche;
                    decimal mMonto = Convert.ToDecimal(Math.Round((double)mSaldoFinanciar * mFactor, 2, MidpointRounding.AwayFromZero));
                    decimal mRecargos = mMonto - mSaldoFinanciar;

                    txtFacturar.Text = String.Format("{0:0,0.00}", mFacturar);
                    txtSaldoFinanciar.Text = String.Format("{0:0,0.00}", mSaldoFinanciar);
                    txtIntereses.Text = String.Format("{0:0,0.00}", mRecargos);
                    txtMonto.Text = String.Format("{0:0,0.00}", mMonto);

                    Int32 mPagos; string mPagos1;
                    string mPagosTexto = ws.DevuelvePagosNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

                    try
                    {
                        mPagos = Convert.ToInt32(mPagosTexto.Substring(0, 2).Replace(" ", ""));
                    }
                    catch
                    {
                        mPagos = 1;
                    }

                    decimal mCuotas = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) / mPagos;

                    if (mPagos <= 10)
                    {
                        mPagos1 = string.Format("0{0}", mPagos - 1);
                    }
                    else
                    {
                        mPagos1 = (mPagos - 1).ToString();
                    }

                    cbPagos1.SelectedValue = mPagos1;
                    cbPagos2.SelectedValue = "01";

                    if (mCuotas.ToString().Substring(mCuotas.ToString().Length - 2, 2) == "00")
                    {
                        txtPagos1.Text = String.Format("{0:0,0.00}", mCuotas);
                        txtPagos2.Text = String.Format("{0:0,0.00}", mCuotas);
                    }
                    else
                    {
                        string[] mCuotasString = mCuotas.ToString().Split(new string[] { "." }, StringSplitOptions.None);

                        Int32 mCuota = 1 + Convert.ToInt32(mCuotasString[0]);
                        decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (mCuota * (mPagos - 1));

                        txtPagos1.Text = String.Format("{0:0,0.00}", mCuota);
                        txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
                    }
                }


                if (cbFinanciera.SelectedValue == "3" || cbFinanciera.SelectedValue == "10")
                {
                    ValidaTotalPedido();
                    ValidaTipoNivelPrecio();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format(" - Error al aplicar la Fiesta de Mayo 2016. {0} {1}", ex.Message, m);
            }
        }

        void FiestaDeDescuentosJunio2016()
        {
            try
            {
                lbError2.Text = "";
                lbError2.Visible = false;

                List<wsPuntoVenta.PedidoLinea> qOriginal = new List<wsPuntoVenta.PedidoLinea>();
                qOriginal = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                wsPuntoVenta.PedidoLinea[] q = new wsPuntoVenta.PedidoLinea[1];
                wsPuntoVenta.PedidoLinea[] qLinea = new wsPuntoVenta.PedidoLinea[1];

                int jj = 0;
                for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                {
                    jj++;

                    Label lbPrecioUnitario = (Label)gridArticulos.Rows[ii].FindControl("lbPrecio");
                    Label lbCantidadPedida = (Label)gridArticulos.Rows[ii].FindControl("lbCantidad");
                    Label lbPrecioTotal = (Label)gridArticulos.Rows[ii].FindControl("lbTotal");
                    Label lbDescripcion = (Label)gridArticulos.Rows[ii].FindControl("lbDescripcion");
                    Label lbComentario = (Label)gridArticulos.Rows[ii].FindControl("lbComentario");
                    Label lbEstado = (Label)gridArticulos.Rows[ii].FindControl("lbEstado");
                    Label lbNumeroPedido = (Label)gridArticulos.Rows[ii].FindControl("lbNumeroPedido");
                    Label lbOferta = (Label)gridArticulos.Rows[ii].FindControl("lbOferta");
                    Label lbFechaOfertaDesde = (Label)gridArticulos.Rows[ii].FindControl("lbFechaOfertaDesde");
                    Label lbPrecioOriginal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioOriginal");
                    Label lbEsDetalleKit = (Label)gridArticulos.Rows[ii].FindControl("lbEsDetalleKit");
                    Label lbPrecioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioFacturar");
                    Label lbTipoOferta = (Label)gridArticulos.Rows[ii].FindControl("lbTipoOferta");
                    Label lbVale = (Label)gridArticulos.Rows[ii].FindControl("lbVale");
                    Label lbAutorizacion = (Label)gridArticulos.Rows[ii].FindControl("lbAutorizacion");
                    Label lbBodega = (Label)gridArticulos.Rows[ii].FindControl("lbBodega");
                    Label lbLocalizacion = (Label)gridArticulos.Rows[ii].FindControl("lbLocalizacion");
                    LinkButton lkRequisicionArmado = (LinkButton)gridArticulos.Rows[ii].FindControl("lkRequisicionArmado");
                    Label lbArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbArticulo");
                    Label lbNombreArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbNombreArticulo");
                    Label lbPrecioUnitarioDebioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioUnitarioDebioFacturar");
                    Label lbPrecioSugerido = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioSugerido");
                    Label lbPrecioBaseLocal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioBaseLocal");
                    Label lbGel = (Label)gridArticulos.Rows[ii].FindControl("lbGel");
                    Label lbLinea = (Label)gridArticulos.Rows[ii].FindControl("lbLinea");
                    Label lbPorcentajeDescuento = (Label)gridArticulos.Rows[ii].FindControl("lbPorcentajeDescuento");

                    decimal mPrecioOriginal = 0;
                    DateTime mFechaOfertaDesde = new DateTime(1980, 1, 1);

                    try
                    {
                        mFechaOfertaDesde = new DateTime(Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(6, 4)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(3, 2)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(0, 2)));
                    }
                    catch
                    {
                        //Nothing
                    }
                    try
                    {
                        mPrecioOriginal = Convert.ToDecimal(lbPrecioOriginal.Text.Replace(",", ""));
                    }
                    catch
                    {
                        mPrecioOriginal = 0;
                    }


                    wsPuntoVenta.PedidoLinea itemPedidoLinea = new wsPuntoVenta.PedidoLinea();
                    itemPedidoLinea.Articulo = lbArticulo.Text;
                    itemPedidoLinea.Nombre = lbNombreArticulo.Text;
                    itemPedidoLinea.PrecioUnitario = Convert.ToDecimal(lbPrecioUnitario.Text.Replace(",", ""));
                    itemPedidoLinea.CantidadPedida = Convert.ToDecimal(lbCantidadPedida.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioTotal = Convert.ToDecimal(lbPrecioTotal.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioFacturar = Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.Bodega = lbBodega.Text;
                    itemPedidoLinea.Localizacion = lbLocalizacion.Text;
                    itemPedidoLinea.RequisicionArmado = lkRequisicionArmado.Text;
                    itemPedidoLinea.Descripcion = lbDescripcion.Text;
                    itemPedidoLinea.Comentario = lbComentario.Text;
                    itemPedidoLinea.Estado = lbEstado.Text;
                    itemPedidoLinea.NumeroPedido = lbNumeroPedido.Text;
                    itemPedidoLinea.Oferta = lbOferta.Text;
                    itemPedidoLinea.FechaOfertaDesde = mFechaOfertaDesde;
                    itemPedidoLinea.PrecioOriginal = mPrecioOriginal;
                    itemPedidoLinea.TipoOferta = lbTipoOferta.Text;
                    itemPedidoLinea.EsDetalleKit = lbEsDetalleKit.Text;
                    itemPedidoLinea.Vale = Convert.ToInt32(lbVale.Text == "" ? "0" : lbVale.Text);
                    itemPedidoLinea.Autorizacion = Convert.ToInt64(lbAutorizacion.Text == "" ? "0" : lbAutorizacion.Text);
                    itemPedidoLinea.PrecioUnitarioDebioFacturar = Convert.ToDecimal(lbPrecioUnitarioDebioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioSugerido = Convert.ToDecimal(lbPrecioSugerido.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioBaseLocal = Convert.ToDouble(lbPrecioBaseLocal.Text.Replace(",", ""));
                    itemPedidoLinea.Gel = lbGel.Text;
                    itemPedidoLinea.Linea = Convert.ToInt32(lbLinea.Text);
                    itemPedidoLinea.PorcentajeDescuento = Convert.ToDecimal(lbPorcentajeDescuento.Text);

                    if (jj > 1) Array.Resize(ref q, jj);
                    if (jj > 1) Array.Resize(ref qLinea, jj);

                    q[jj - 1] = itemPedidoLinea;
                    qLinea[jj - 1] = itemPedidoLinea;
                }


                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = ""; string mTotal = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.FiestaDeDescuentosJunio2016(q, ref qLinea, ref mMensaje, cbNivelPrecio.Text, ref mTotal))
                {
                    lbError2.Visible = true;
                    lbError2.Text = mMensaje;
                    return;
                }

                List<wsPuntoVenta.PedidoLinea> qModificar = new List<wsPuntoVenta.PedidoLinea>();
                qModificar = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];


                decimal mFacturar = 0;
                foreach (var item in qLinea)
                {
                    mFacturar += item.PrecioFacturar;

                    foreach (var itemModificar in qModificar)
                    {
                        if (item.PrecioFacturar != itemModificar.PrecioFacturar && item.Linea == itemModificar.Linea)
                        {
                            itemModificar.PrecioFacturar = item.PrecioFacturar;
                            itemModificar.PrecioTotal = item.PrecioTotal;
                            itemModificar.PrecioUnitario = item.PrecioUnitario;
                            itemModificar.PorcentajeDescuento = item.PorcentajeDescuento;
                        }
                    }
                }

                txtNotasTipoVenta.Text = "FIESTA DE DESCUENTOS JUNIO 2016";

                gridArticulos.DataSource = qModificar;
                gridArticulos.DataBind();

                ViewState["qArticulos"] = qModificar;
                lkLaTorre.Visible = false;

                if (cbFinanciera.SelectedValue == "1" || cbFinanciera.SelectedValue == "5" || cbFinanciera.SelectedValue == "9")
                {
                    txtMonto.Text = mTotal;
                    txtFacturar.Text = mTotal;
                }

                if (cbFinanciera.SelectedValue == "7")
                {
                    double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                    decimal mEnganche = Convert.ToDecimal(txtEnganche.Text.Trim().Replace(",", ""));
                    decimal mSaldoFinanciar = mFacturar - mEnganche;
                    decimal mMonto = Convert.ToDecimal(Math.Round((double)mSaldoFinanciar * mFactor, 2, MidpointRounding.AwayFromZero));
                    decimal mRecargos = mMonto - mSaldoFinanciar;

                    txtFacturar.Text = String.Format("{0:0,0.00}", mFacturar);
                    txtSaldoFinanciar.Text = String.Format("{0:0,0.00}", mSaldoFinanciar);
                    txtIntereses.Text = String.Format("{0:0,0.00}", mRecargos);
                    txtMonto.Text = String.Format("{0:0,0.00}", mMonto);

                    Int32 mPagos; string mPagos1;
                    string mPagosTexto = ws.DevuelvePagosNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

                    try
                    {
                        mPagos = Convert.ToInt32(mPagosTexto.Substring(0, 2).Replace(" ", ""));
                    }
                    catch
                    {
                        mPagos = 1;
                    }

                    decimal mCuotas = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) / mPagos;

                    if (mPagos <= 10)
                    {
                        mPagos1 = string.Format("0{0}", mPagos - 1);
                    }
                    else
                    {
                        mPagos1 = (mPagos - 1).ToString();
                    }

                    cbPagos1.SelectedValue = mPagos1;
                    cbPagos2.SelectedValue = "01";

                    if (mCuotas.ToString().Substring(mCuotas.ToString().Length - 2, 2) == "00")
                    {
                        txtPagos1.Text = String.Format("{0:0,0.00}", mCuotas);
                        txtPagos2.Text = String.Format("{0:0,0.00}", mCuotas);
                    }
                    else
                    {
                        string[] mCuotasString = mCuotas.ToString().Split(new string[] { "." }, StringSplitOptions.None);

                        Int32 mCuota = 1 + Convert.ToInt32(mCuotasString[0]);
                        decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (mCuota * (mPagos - 1));

                        txtPagos1.Text = String.Format("{0:0,0.00}", mCuota);
                        txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
                    }
                }


                if (cbFinanciera.SelectedValue == "3" || cbFinanciera.SelectedValue == "10")
                {
                    ValidaTotalPedido();
                    ValidaTipoNivelPrecio();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format(" - Error al aplicar la Fiesta de Descuentos Junio 2016. {0} {1}", ex.Message, m);
            }
        }

        void FiestaDeModulosRacks()
        {
            try
            {
                lbError2.Text = "";
                lbError2.Visible = false;

                List<wsPuntoVenta.PedidoLinea> qOriginal = new List<wsPuntoVenta.PedidoLinea>();
                qOriginal = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                wsPuntoVenta.PedidoLinea[] q = new wsPuntoVenta.PedidoLinea[1];
                wsPuntoVenta.PedidoLinea[] qLinea = new wsPuntoVenta.PedidoLinea[1];

                int jj = 0;
                for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                {
                    jj++;

                    Label lbPrecioUnitario = (Label)gridArticulos.Rows[ii].FindControl("lbPrecio");
                    Label lbCantidadPedida = (Label)gridArticulos.Rows[ii].FindControl("lbCantidad");
                    Label lbPrecioTotal = (Label)gridArticulos.Rows[ii].FindControl("lbTotal");
                    Label lbDescripcion = (Label)gridArticulos.Rows[ii].FindControl("lbDescripcion");
                    Label lbComentario = (Label)gridArticulos.Rows[ii].FindControl("lbComentario");
                    Label lbEstado = (Label)gridArticulos.Rows[ii].FindControl("lbEstado");
                    Label lbNumeroPedido = (Label)gridArticulos.Rows[ii].FindControl("lbNumeroPedido");
                    Label lbOferta = (Label)gridArticulos.Rows[ii].FindControl("lbOferta");
                    Label lbFechaOfertaDesde = (Label)gridArticulos.Rows[ii].FindControl("lbFechaOfertaDesde");
                    Label lbPrecioOriginal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioOriginal");
                    Label lbEsDetalleKit = (Label)gridArticulos.Rows[ii].FindControl("lbEsDetalleKit");
                    Label lbPrecioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioFacturar");
                    Label lbTipoOferta = (Label)gridArticulos.Rows[ii].FindControl("lbTipoOferta");
                    Label lbVale = (Label)gridArticulos.Rows[ii].FindControl("lbVale");
                    Label lbAutorizacion = (Label)gridArticulos.Rows[ii].FindControl("lbAutorizacion");
                    Label lbBodega = (Label)gridArticulos.Rows[ii].FindControl("lbBodega");
                    Label lbLocalizacion = (Label)gridArticulos.Rows[ii].FindControl("lbLocalizacion");
                    LinkButton lkRequisicionArmado = (LinkButton)gridArticulos.Rows[ii].FindControl("lkRequisicionArmado");
                    Label lbArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbArticulo");
                    Label lbNombreArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbNombreArticulo");
                    Label lbPrecioUnitarioDebioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioUnitarioDebioFacturar");
                    Label lbPrecioSugerido = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioSugerido");
                    Label lbPrecioBaseLocal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioBaseLocal");
                    Label lbGel = (Label)gridArticulos.Rows[ii].FindControl("lbGel");
                    Label lbLinea = (Label)gridArticulos.Rows[ii].FindControl("lbLinea");
                    Label lbPorcentajeDescuento = (Label)gridArticulos.Rows[ii].FindControl("lbPorcentajeDescuento");

                    decimal mPrecioOriginal = 0;
                    DateTime mFechaOfertaDesde = new DateTime(1980, 1, 1);

                    try
                    {
                        mFechaOfertaDesde = new DateTime(Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(6, 4)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(3, 2)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(0, 2)));
                    }
                    catch
                    {
                        //Nothing
                    }
                    try
                    {
                        mPrecioOriginal = Convert.ToDecimal(lbPrecioOriginal.Text.Replace(",", ""));
                    }
                    catch
                    {
                        mPrecioOriginal = 0;
                    }


                    wsPuntoVenta.PedidoLinea itemPedidoLinea = new wsPuntoVenta.PedidoLinea();
                    itemPedidoLinea.Articulo = lbArticulo.Text;
                    itemPedidoLinea.Nombre = lbNombreArticulo.Text;
                    itemPedidoLinea.PrecioUnitario = Convert.ToDecimal(lbPrecioUnitario.Text.Replace(",", ""));
                    itemPedidoLinea.CantidadPedida = Convert.ToDecimal(lbCantidadPedida.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioTotal = Convert.ToDecimal(lbPrecioTotal.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioFacturar = Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.Bodega = lbBodega.Text;
                    itemPedidoLinea.Localizacion = lbLocalizacion.Text;
                    itemPedidoLinea.RequisicionArmado = lkRequisicionArmado.Text;
                    itemPedidoLinea.Descripcion = lbDescripcion.Text;
                    itemPedidoLinea.Comentario = lbComentario.Text;
                    itemPedidoLinea.Estado = lbEstado.Text;
                    itemPedidoLinea.NumeroPedido = lbNumeroPedido.Text;
                    itemPedidoLinea.Oferta = lbOferta.Text;
                    itemPedidoLinea.FechaOfertaDesde = mFechaOfertaDesde;
                    itemPedidoLinea.PrecioOriginal = mPrecioOriginal;
                    itemPedidoLinea.TipoOferta = lbTipoOferta.Text;
                    itemPedidoLinea.EsDetalleKit = lbEsDetalleKit.Text;
                    itemPedidoLinea.Vale = Convert.ToInt32(lbVale.Text == "" ? "0" : lbVale.Text);
                    itemPedidoLinea.Autorizacion = Convert.ToInt64(lbAutorizacion.Text == "" ? "0" : lbAutorizacion.Text);
                    itemPedidoLinea.PrecioUnitarioDebioFacturar = Convert.ToDecimal(lbPrecioUnitarioDebioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioSugerido = Convert.ToDecimal(lbPrecioSugerido.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioBaseLocal = Convert.ToDouble(lbPrecioBaseLocal.Text.Replace(",", ""));
                    itemPedidoLinea.Gel = lbGel.Text;
                    itemPedidoLinea.Linea = Convert.ToInt32(lbLinea.Text);
                    itemPedidoLinea.PorcentajeDescuento = Convert.ToDecimal(lbPorcentajeDescuento.Text);

                    if (jj > 1) Array.Resize(ref q, jj);
                    if (jj > 1) Array.Resize(ref qLinea, jj);

                    q[jj - 1] = itemPedidoLinea;
                    qLinea[jj - 1] = itemPedidoLinea;
                }


                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = ""; string mPrecioBien = ""; string mMonto = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.FiestaDeModulosRacks(ref qLinea, ref mPrecioBien, ref mMonto, cbNivelPrecio.Text, ref mMensaje))
                {
                    lbError2.Visible = true;
                    lbError2.Text = mMensaje;
                    return;
                }

                txtNotasTipoVenta.Text = "FIESTA DE MODULOS Y RACKS";

                gridArticulos.DataSource = qLinea;
                gridArticulos.DataBind();

                ViewState["qArticulos"] = qLinea;
                lkLaTorre.Visible = false;

                if (cbFinanciera.SelectedValue == "1" || cbFinanciera.SelectedValue == "5" || cbFinanciera.SelectedValue == "9")
                {
                    txtMonto.Text = mMonto;
                    txtFacturar.Text = mMonto;
                }

                if (cbFinanciera.SelectedValue == "7")
                {
                    double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                    decimal mEnganche = Convert.ToDecimal(txtEnganche.Text.Trim().Replace(",", ""));
                    decimal mSaldoFinanciar = Convert.ToDecimal(mPrecioBien.Replace(",", "")) - mEnganche;
                    decimal mMontoTotal = Convert.ToDecimal(Math.Round((double)mSaldoFinanciar * mFactor, 2, MidpointRounding.AwayFromZero));
                    decimal mRecargos = mMontoTotal - mSaldoFinanciar;

                    txtFacturar.Text = mPrecioBien;
                    txtSaldoFinanciar.Text = String.Format("{0:0,0.00}", mSaldoFinanciar);
                    txtIntereses.Text = String.Format("{0:0,0.00}", mRecargos);
                    txtMonto.Text = String.Format("{0:0,0.00}", mMontoTotal);

                    Int32 mPagos; string mPagos1;
                    string mPagosTexto = ws.DevuelvePagosNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

                    try
                    {
                        mPagos = Convert.ToInt32(mPagosTexto.Substring(0, 2).Replace(" ", ""));
                    }
                    catch
                    {
                        mPagos = 1;
                    }

                    decimal mCuotas = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) / mPagos;

                    if (mPagos <= 10)
                    {
                        mPagos1 = string.Format("0{0}", mPagos - 1);
                    }
                    else
                    {
                        mPagos1 = (mPagos - 1).ToString();
                    }

                    cbPagos1.SelectedValue = mPagos1;
                    cbPagos2.SelectedValue = "01";

                    if (mCuotas.ToString().Substring(mCuotas.ToString().Length - 2, 2) == "00")
                    {
                        txtPagos1.Text = String.Format("{0:0,0.00}", mCuotas);
                        txtPagos2.Text = String.Format("{0:0,0.00}", mCuotas);
                    }
                    else
                    {
                        string[] mCuotasString = mCuotas.ToString().Split(new string[] { "." }, StringSplitOptions.None);

                        Int32 mCuota = 1 + Convert.ToInt32(mCuotasString[0]);
                        decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (mCuota * (mPagos - 1));

                        txtPagos1.Text = String.Format("{0:0,0.00}", mCuota);
                        txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
                    }
                }


                if (cbFinanciera.SelectedValue == "3" || cbFinanciera.SelectedValue == "10")
                {
                    ValidaTotalPedido();
                    ValidaTipoNivelPrecio();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format(" - Error al aplicar la Fiesta de Módulos y Racks. {0} {1}", ex.Message, m);
            }
        }

        void FiestaDeMediaNoche()
        {
            try
            {
                lbError2.Text = "";
                lbError2.Visible = false;

                List<wsPuntoVenta.PedidoLinea> qOriginal = new List<wsPuntoVenta.PedidoLinea>();
                qOriginal = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                wsPuntoVenta.PedidoLinea[] q = new wsPuntoVenta.PedidoLinea[1];
                wsPuntoVenta.PedidoLinea[] qLinea = new wsPuntoVenta.PedidoLinea[1];

                int jj = 0;
                for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                {
                    jj++;

                    Label lbPrecioUnitario = (Label)gridArticulos.Rows[ii].FindControl("lbPrecio");
                    Label lbCantidadPedida = (Label)gridArticulos.Rows[ii].FindControl("lbCantidad");
                    Label lbPrecioTotal = (Label)gridArticulos.Rows[ii].FindControl("lbTotal");
                    Label lbDescripcion = (Label)gridArticulos.Rows[ii].FindControl("lbDescripcion");
                    Label lbComentario = (Label)gridArticulos.Rows[ii].FindControl("lbComentario");
                    Label lbEstado = (Label)gridArticulos.Rows[ii].FindControl("lbEstado");
                    Label lbNumeroPedido = (Label)gridArticulos.Rows[ii].FindControl("lbNumeroPedido");
                    Label lbOferta = (Label)gridArticulos.Rows[ii].FindControl("lbOferta");
                    Label lbFechaOfertaDesde = (Label)gridArticulos.Rows[ii].FindControl("lbFechaOfertaDesde");
                    Label lbPrecioOriginal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioOriginal");
                    Label lbEsDetalleKit = (Label)gridArticulos.Rows[ii].FindControl("lbEsDetalleKit");
                    Label lbPrecioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioFacturar");
                    Label lbTipoOferta = (Label)gridArticulos.Rows[ii].FindControl("lbTipoOferta");
                    Label lbVale = (Label)gridArticulos.Rows[ii].FindControl("lbVale");
                    Label lbAutorizacion = (Label)gridArticulos.Rows[ii].FindControl("lbAutorizacion");
                    Label lbBodega = (Label)gridArticulos.Rows[ii].FindControl("lbBodega");
                    Label lbLocalizacion = (Label)gridArticulos.Rows[ii].FindControl("lbLocalizacion");
                    LinkButton lkRequisicionArmado = (LinkButton)gridArticulos.Rows[ii].FindControl("lkRequisicionArmado");
                    Label lbArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbArticulo");
                    Label lbNombreArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbNombreArticulo");
                    Label lbPrecioUnitarioDebioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioUnitarioDebioFacturar");
                    Label lbPrecioSugerido = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioSugerido");
                    Label lbPrecioBaseLocal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioBaseLocal");
                    Label lbGel = (Label)gridArticulos.Rows[ii].FindControl("lbGel");
                    Label lbLinea = (Label)gridArticulos.Rows[ii].FindControl("lbLinea");
                    Label lbPorcentajeDescuento = (Label)gridArticulos.Rows[ii].FindControl("lbPorcentajeDescuento");

                    decimal mPrecioOriginal = 0;
                    DateTime mFechaOfertaDesde = new DateTime(1980, 1, 1);

                    try
                    {
                        mFechaOfertaDesde = new DateTime(Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(6, 4)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(3, 2)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(0, 2)));
                    }
                    catch
                    {
                        //Nothing
                    }
                    try
                    {
                        mPrecioOriginal = Convert.ToDecimal(lbPrecioOriginal.Text.Replace(",", ""));
                    }
                    catch
                    {
                        mPrecioOriginal = 0;
                    }


                    wsPuntoVenta.PedidoLinea itemPedidoLinea = new wsPuntoVenta.PedidoLinea();
                    itemPedidoLinea.Articulo = lbArticulo.Text;
                    itemPedidoLinea.Nombre = lbNombreArticulo.Text;
                    itemPedidoLinea.PrecioUnitario = Convert.ToDecimal(lbPrecioUnitario.Text.Replace(",", ""));
                    itemPedidoLinea.CantidadPedida = Convert.ToDecimal(lbCantidadPedida.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioTotal = Convert.ToDecimal(lbPrecioTotal.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioFacturar = Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.Bodega = lbBodega.Text;
                    itemPedidoLinea.Localizacion = lbLocalizacion.Text;
                    itemPedidoLinea.RequisicionArmado = lkRequisicionArmado.Text;
                    itemPedidoLinea.Descripcion = lbDescripcion.Text;
                    itemPedidoLinea.Comentario = lbComentario.Text;
                    itemPedidoLinea.Estado = lbEstado.Text;
                    itemPedidoLinea.NumeroPedido = lbNumeroPedido.Text;
                    itemPedidoLinea.Oferta = lbOferta.Text;
                    itemPedidoLinea.FechaOfertaDesde = mFechaOfertaDesde;
                    itemPedidoLinea.PrecioOriginal = mPrecioOriginal;
                    itemPedidoLinea.TipoOferta = lbTipoOferta.Text;
                    itemPedidoLinea.EsDetalleKit = lbEsDetalleKit.Text;
                    itemPedidoLinea.Vale = Convert.ToInt32(lbVale.Text == "" ? "0" : lbVale.Text);
                    itemPedidoLinea.Autorizacion = Convert.ToInt64(lbAutorizacion.Text == "" ? "0" : lbAutorizacion.Text);
                    itemPedidoLinea.PrecioUnitarioDebioFacturar = Convert.ToDecimal(lbPrecioUnitarioDebioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioSugerido = Convert.ToDecimal(lbPrecioSugerido.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioBaseLocal = Convert.ToDouble(lbPrecioBaseLocal.Text.Replace(",", ""));
                    itemPedidoLinea.Gel = lbGel.Text;
                    itemPedidoLinea.Linea = Convert.ToInt32(lbLinea.Text);
                    itemPedidoLinea.PorcentajeDescuento = Convert.ToDecimal(lbPorcentajeDescuento.Text);

                    if (jj > 1) Array.Resize(ref q, jj);
                    if (jj > 1) Array.Resize(ref qLinea, jj);

                    q[jj - 1] = itemPedidoLinea;
                    qLinea[jj - 1] = itemPedidoLinea;
                }


                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = ""; string mPrecioBien = ""; string mMonto = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.FiestaDeMediaNoche(ref qLinea, ref mPrecioBien, ref mMonto, cbNivelPrecio.Text, ref mMensaje))
                {
                    lbError2.Visible = true;
                    lbError2.Text = mMensaje;
                    return;
                }

                txtNotasTipoVenta.Text = "FIESTA DE MEDIA NOCHE";

                gridArticulos.DataSource = qLinea;
                gridArticulos.DataBind();

                ViewState["qArticulos"] = qLinea;
                lkLaTorre.Visible = false;

                if (cbFinanciera.SelectedValue == "1" || cbFinanciera.SelectedValue == "5" || cbFinanciera.SelectedValue == "9")
                {
                    txtMonto.Text = mMonto;
                    txtFacturar.Text = mMonto;
                }

                if (cbFinanciera.SelectedValue == "7")
                {
                    double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                    decimal mEnganche = Convert.ToDecimal(txtEnganche.Text.Trim().Replace(",", ""));
                    decimal mSaldoFinanciar = Convert.ToDecimal(mPrecioBien.Replace(",", "")) - mEnganche;
                    decimal mMontoTotal = Convert.ToDecimal(Math.Round((double)mSaldoFinanciar * mFactor, 2, MidpointRounding.AwayFromZero));
                    decimal mRecargos = mMontoTotal - mSaldoFinanciar;

                    txtFacturar.Text = mPrecioBien;
                    txtSaldoFinanciar.Text = String.Format("{0:0,0.00}", mSaldoFinanciar);
                    txtIntereses.Text = String.Format("{0:0,0.00}", mRecargos);
                    txtMonto.Text = String.Format("{0:0,0.00}", mMontoTotal);

                    Int32 mPagos; string mPagos1;
                    string mPagosTexto = ws.DevuelvePagosNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

                    try
                    {
                        mPagos = Convert.ToInt32(mPagosTexto.Substring(0, 2).Replace(" ", ""));
                    }
                    catch
                    {
                        mPagos = 1;
                    }

                    decimal mCuotas = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) / mPagos;

                    if (mPagos <= 10)
                    {
                        mPagos1 = string.Format("0{0}", mPagos - 1);
                    }
                    else
                    {
                        mPagos1 = (mPagos - 1).ToString();
                    }

                    cbPagos1.SelectedValue = mPagos1;
                    cbPagos2.SelectedValue = "01";

                    if (mCuotas.ToString().Substring(mCuotas.ToString().Length - 2, 2) == "00")
                    {
                        txtPagos1.Text = String.Format("{0:0,0.00}", mCuotas);
                        txtPagos2.Text = String.Format("{0:0,0.00}", mCuotas);
                    }
                    else
                    {
                        string[] mCuotasString = mCuotas.ToString().Split(new string[] { "." }, StringSplitOptions.None);

                        Int32 mCuota = 1 + Convert.ToInt32(mCuotasString[0]);
                        decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (mCuota * (mPagos - 1));

                        txtPagos1.Text = String.Format("{0:0,0.00}", mCuota);
                        txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
                    }
                }


                if (cbFinanciera.SelectedValue == "3" || cbFinanciera.SelectedValue == "10")
                {
                    ValidaTotalPedido();
                    ValidaTipoNivelPrecio();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format(" - Error al aplicar la Fiesta de media noche. {0} {1}", ex.Message, m);
            }
        }

        bool CargarInfoPromocion(ref wsPuntoVenta.PedidoLinea[] qLinea)
        {
            bool mRetorna = true;

            try
            {

            }
            catch (Exception ex)
            {
                mRetorna = false;
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format(" - Error al aplicar la promoción. {0} {1}", ex.Message, m);
            }

            return mRetorna;
        }

        void BlackFiesta()
        {
            try
            {
                wsPuntoVenta.PedidoLinea[] qLinea = new wsPuntoVenta.PedidoLinea[1];
                if (!CargarInfoPromocion(ref qLinea)) return;

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format(" - Error al aplicar la Black Fiesta. {0} {1}", ex.Message, m);
            }
        }

        protected void lkLaTorre_Click(object sender, EventArgs e)
        {
            try
            {
                lbError2.Text = "";
                lbError2.Visible = false;

                List<wsPuntoVenta.PedidoLinea> qOriginal = new List<wsPuntoVenta.PedidoLinea>();
                qOriginal = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                wsPuntoVenta.PedidoLinea[] q = new wsPuntoVenta.PedidoLinea[1];
                wsPuntoVenta.PedidoLinea[] qLinea = new wsPuntoVenta.PedidoLinea[1];
                List<PedidoLinea> qqLinea = new List<PedidoLinea>();

                int jj = 0;
                for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                {
                    jj++;

                    Label lbPrecioUnitario = (Label)gridArticulos.Rows[ii].FindControl("lbPrecio");
                    Label lbCantidadPedida = (Label)gridArticulos.Rows[ii].FindControl("lbCantidad");
                    Label lbPrecioTotal = (Label)gridArticulos.Rows[ii].FindControl("lbTotal");
                    Label lbDescripcion = (Label)gridArticulos.Rows[ii].FindControl("lbDescripcion");
                    Label lbComentario = (Label)gridArticulos.Rows[ii].FindControl("lbComentario");
                    Label lbEstado = (Label)gridArticulos.Rows[ii].FindControl("lbEstado");
                    Label lbNumeroPedido = (Label)gridArticulos.Rows[ii].FindControl("lbNumeroPedido");
                    Label lbOferta = (Label)gridArticulos.Rows[ii].FindControl("lbOferta");
                    Label lbFechaOfertaDesde = (Label)gridArticulos.Rows[ii].FindControl("lbFechaOfertaDesde");
                    Label lbPrecioOriginal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioOriginal");
                    Label lbEsDetalleKit = (Label)gridArticulos.Rows[ii].FindControl("lbEsDetalleKit");
                    Label lbPrecioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioFacturar");
                    Label lbTipoOferta = (Label)gridArticulos.Rows[ii].FindControl("lbTipoOferta");
                    Label lbVale = (Label)gridArticulos.Rows[ii].FindControl("lbVale");
                    Label lbAutorizacion = (Label)gridArticulos.Rows[ii].FindControl("lbAutorizacion");
                    Label lbBodega = (Label)gridArticulos.Rows[ii].FindControl("lbBodega");
                    Label lbLocalizacion = (Label)gridArticulos.Rows[ii].FindControl("lbLocalizacion");
                    LinkButton lkRequisicionArmado = (LinkButton)gridArticulos.Rows[ii].FindControl("lkRequisicionArmado");
                    Label lbArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbArticulo");
                    Label lbNombreArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbNombreArticulo");
                    Label lbPrecioUnitarioDebioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioUnitarioDebioFacturar");
                    Label lbPrecioSugerido = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioSugerido");
                    Label lbPrecioBaseLocal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioBaseLocal");
                    Label lbGel = (Label)gridArticulos.Rows[ii].FindControl("lbGel");
                    Label lbLinea = (Label)gridArticulos.Rows[ii].FindControl("lbLinea");
                    Label lbPorcentajeDescuento = (Label)gridArticulos.Rows[ii].FindControl("lbPorcentajeDescuento");
                    Label lbBeneficiario = (Label)gridArticulos.Rows[ii].FindControl("lbBeneficiario");
                    Label lbDescuento = (Label)gridArticulos.Rows[ii].FindControl("lbDescuento");
                    decimal mPrecioOriginal = 0;
                    DateTime mFechaOfertaDesde = new DateTime(1980, 1, 1);

                    try
                    {
                        mFechaOfertaDesde = new DateTime(Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(6, 4)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(3, 2)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(0, 2)));
                    }
                    catch
                    {
                        //Nothing
                    }
                    try
                    {
                        mPrecioOriginal = Convert.ToDecimal(lbPrecioOriginal.Text.Replace(",", ""));
                    }
                    catch
                    {
                        mPrecioOriginal = 0;
                    }

                    wsPuntoVenta.PedidoLinea itemPedidoLinea = new wsPuntoVenta.PedidoLinea();
                    PedidoLinea linea = new PedidoLinea
                    {
                        Articulo = lbArticulo.Text,
                        Nombre = lbNombreArticulo.Text,
                        PrecioUnitario = Convert.ToDecimal(lbPrecioUnitario.Text.Replace(",", "")),
                        CantidadPedida = Convert.ToDecimal(lbCantidadPedida.Text.Replace(",", "")),
                        PrecioTotal = Convert.ToDecimal(lbPrecioTotal.Text.Replace(",", "")),
                        PrecioFacturar = Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", "")),
                        Bodega = lbBodega.Text,
                        Localizacion = lbLocalizacion.Text,
                        RequisicionArmado = lkRequisicionArmado.Text,
                        Descripcion = lbDescripcion.Text,
                        Comentario = lbComentario.Text,
                        Estado = lbEstado.Text,
                        NumeroPedido = lbNumeroPedido.Text,
                        Oferta = lbOferta.Text,
                        FechaOfertaDesde = mFechaOfertaDesde,
                        PrecioOriginal = mPrecioOriginal,
                        TipoOferta = lbTipoOferta.Text,
                        EsDetalleKit = lbEsDetalleKit.Text,
                        Vale = Convert.ToInt32(lbVale.Text == "" ? "0" : lbVale.Text),
                        Autorizacion = Convert.ToInt64(lbAutorizacion.Text == "" ? "0" : lbAutorizacion.Text),
                        PrecioUnitarioDebioFacturar = Convert.ToDecimal(lbPrecioUnitarioDebioFacturar.Text.Replace(",", "")),
                        PrecioSugerido = Convert.ToDecimal(lbPrecioSugerido.Text.Replace(",", "")),
                        PrecioBaseLocal = Convert.ToDouble(lbPrecioBaseLocal.Text.Replace(",", "")),
                        Gel = lbGel.Text,
                        Linea = Convert.ToInt32(lbLinea.Text),
                        PorcentajeDescuento = Convert.ToDecimal(lbPorcentajeDescuento.Text),
                        Beneficiario = lbBeneficiario.Text,
                        Descuento = Convert.ToDecimal(lbDescuento.Text)
                    };
                    qqLinea.Add(linea);
                    itemPedidoLinea.Articulo = lbArticulo.Text;
                    itemPedidoLinea.Nombre = lbNombreArticulo.Text;
                    itemPedidoLinea.PrecioUnitario = Convert.ToDecimal(lbPrecioUnitario.Text.Replace(",", ""));
                    itemPedidoLinea.CantidadPedida = Convert.ToDecimal(lbCantidadPedida.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioTotal = Convert.ToDecimal(lbPrecioTotal.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioFacturar = Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.Bodega = lbBodega.Text;
                    itemPedidoLinea.Localizacion = lbLocalizacion.Text;
                    itemPedidoLinea.RequisicionArmado = lkRequisicionArmado.Text;
                    itemPedidoLinea.Descripcion = lbDescripcion.Text;
                    itemPedidoLinea.Comentario = lbComentario.Text;
                    itemPedidoLinea.Estado = lbEstado.Text;
                    itemPedidoLinea.NumeroPedido = lbNumeroPedido.Text;
                    itemPedidoLinea.Oferta = lbOferta.Text;
                    itemPedidoLinea.FechaOfertaDesde = mFechaOfertaDesde;
                    itemPedidoLinea.PrecioOriginal = mPrecioOriginal;
                    itemPedidoLinea.TipoOferta = lbTipoOferta.Text;
                    itemPedidoLinea.EsDetalleKit = lbEsDetalleKit.Text;
                    itemPedidoLinea.Vale = Convert.ToInt32(lbVale.Text == "" ? "0" : lbVale.Text);
                    itemPedidoLinea.Autorizacion = Convert.ToInt64(lbAutorizacion.Text == "" ? "0" : lbAutorizacion.Text);
                    itemPedidoLinea.PrecioUnitarioDebioFacturar = Convert.ToDecimal(lbPrecioUnitarioDebioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioSugerido = Convert.ToDecimal(lbPrecioSugerido.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioBaseLocal = Convert.ToDouble(lbPrecioBaseLocal.Text.Replace(",", ""));
                    itemPedidoLinea.Gel = lbGel.Text;
                    itemPedidoLinea.Linea = Convert.ToInt32(lbLinea.Text);
                    itemPedidoLinea.PorcentajeDescuento = Convert.ToDecimal(lbPorcentajeDescuento.Text);
                    itemPedidoLinea.Beneficiario = lbBeneficiario.Text;
                    itemPedidoLinea.Descuento = Convert.ToDecimal(lbDescuento.Text);

                    if (jj > 1) Array.Resize(ref q, jj);
                    if (jj > 1) Array.Resize(ref qLinea, jj);

                    q[jj - 1] = itemPedidoLinea;
                    qLinea[jj - 1] = itemPedidoLinea;
                   
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                string mMensaje = ""; 
                string mPrecioBien = ""; 
                string mMonto = "";
                string mNotas = "";

                mNotas = txtNotasTipoVenta.Text;
                string DescuentoTotal = "";

                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") 
                    ws.Url = Convert.ToString(ViewState["url"]);

                string mTipoVenta = cbTipo.SelectedValue.ToString();
               

                if (mTipoVenta == "NR") 
                    return;

                if (!ws.AplicaPromocion(ref qLinea, ref mPrecioBien, ref mMonto, cbNivelPrecio.Text, ref mMensaje, ref mNotas, mTipoVenta, ref DescuentoTotal, Convert.ToString(Session["Tienda"]), Convert.ToString(Session["Vendedor"])))

              // if(!Facturacion.AplicaPromocion(ref qqLinea ,ref mPrecioBien, ref mMonto,ref DescuentoTotal, cbNivelPrecio.Text, ref mMensaje, ref mNotas, mTipoVenta, int.Parse(cbFinanciera.SelectedValue), "", Convert.ToString(Session["Tienda"]), Convert.ToString(Session["Vendedor"]), Session["Cliente"].ToString()))
                {
                    lbError2.Visible = true;
                    lbError2.Text = mMensaje;
                    return;
                }

                txtNotasTipoVenta.Text = mNotas;

                int i = 0;
                foreach (var item in qLinea)
                {

                
                    item.PrecioTotal = qLinea[i].PrecioTotal;
                    item.PrecioUnitario= qLinea[i].PrecioUnitario;
                    item.Descuento = qLinea[i].Descuento;
                    item.PorcentajeDescuento = qLinea[i].PorcentajeDescuento;
                    item.NetoFacturar= qLinea[i].NetoFacturar;
                    item.PrecioOriginal = qLinea[i].PrecioTotal;
                    item.PrecioFacturar = qLinea[i].PrecioFacturar;
                    i++;
                    //item.PrecioTotal = qqLinea.Find(x => x.Articulo == item.Articulo).PrecioTotal;
                    //item.Descuento = qqLinea.Find(x => x.Articulo == item.Articulo).Descuento;
                    //item.PorcentajeDescuento = qqLinea.Find(x => x.Articulo == item.Articulo).PorcentajeDescuento;
                    //item.NetoFacturar = qqLinea.Find(x => x.Articulo == item.Articulo).NetoFacturar;
                    //item.PrecioOriginal = qqLinea.Find(x => x.Articulo == item.Articulo).PrecioTotal;
                    //item.PrecioFacturar = qqLinea.Find(x => x.Articulo == item.Articulo).PrecioFacturar;

                };

                gridArticulos.DataSource = qLinea;
                gridArticulos.DataBind();
                ViewState["qArticulos"] = qLinea;
                lkLaTorre.Visible = false;
                decimal mDescuentos = Convert.ToDecimal(DescuentoTotal);
                //mDescuentos += Convert.ToDecimal(txtDescuentos.Text.Replace(",", ""));
                if (cbFinanciera.SelectedValue == "1" || cbFinanciera.SelectedValue == "5" || cbFinanciera.SelectedValue == "9")
                {
                    txtMonto.Text = mMonto;
                    txtFacturar.Text = mPrecioBien;
                    txtDescuentos.Text = String.Format("{0:0,0.00}", mDescuentos);
                }

                if (ws.EsBanco(cbFinanciera.SelectedValue.ToString()))
                {
                    double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                    decimal mEnganche = Convert.ToDecimal(txtEnganche.Text.Trim().Replace(",", ""));
                    decimal mSaldoFinanciar = Convert.ToDecimal(mPrecioBien.Replace(",", "")) - mEnganche-mDescuentos;
                    decimal mMontoTotal = Convert.ToDecimal(Math.Round((double)mSaldoFinanciar * mFactor, 2, MidpointRounding.AwayFromZero));
                    decimal mRecargos = mMontoTotal - mSaldoFinanciar;

                    txtFacturar.Text = mPrecioBien;
                    txtSaldoFinanciar.Text = String.Format("{0:0,0.00}", mSaldoFinanciar);
                    txtIntereses.Text = String.Format("{0:0,0.00}", mRecargos);
                    txtMonto.Text = String.Format("{0:0,0.00}", mMontoTotal);
                    txtDescuentos.Text = String.Format("{0:0,0.00}", mDescuentos);

                    Int32 mPagos; string mPagos1;
                    string mPagosTexto = ws.DevuelvePagosNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

                    try
                    {
                        mPagos = Convert.ToInt32(mPagosTexto.Substring(0, 2).Replace(" ", ""));
                    }
                    catch
                    {
                        mPagos = 1;
                    }

                    decimal mCuotas = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) / mPagos;

                    if (mPagos <= 10)
                    {
                        mPagos1 = string.Format("0{0}", mPagos - 1);
                    }
                    else
                    {
                        mPagos1 = (mPagos - 1).ToString();
                    }

                    cbPagos1.SelectedValue = mPagos1;
                    cbPagos2.SelectedValue = "01";

                    if (mCuotas.ToString().Substring(mCuotas.ToString().Length - 2, 2) == "00")
                    {
                        txtPagos1.Text = String.Format("{0:0,0.00}", mCuotas);
                        txtPagos2.Text = String.Format("{0:0,0.00}", mCuotas);
                    }
                    else
                    {
                        #region "aproximaciones para imprimir documentos de financieras"
                        if (cbFinanciera.SelectedValue == Const.FINANCIERA.BANCREDIT)
                        {
                            txtPagos1.Text = String.Format("{0:0,0.00}", Math.Round(mCuotas, 0, MidpointRounding.AwayFromZero));
                            decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (Math.Round(mCuotas, 0, MidpointRounding.AwayFromZero) * (mPagos - 1));
                            txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
                        }
                        else
                        {
                            string[] mCuotasString = mCuotas.ToString().Split(new string[] { "." }, StringSplitOptions.None);

                            Int32 mCuota = 1 + Convert.ToInt32(mCuotasString[0]);
                            decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (mCuota * (mPagos - 1));

                            txtPagos1.Text = String.Format("{0:0,0.00}", mCuota);
                            txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
                        }
                        #endregion
                    }
                }

                //if (cbFinanciera.SelectedValue == "3" || cbFinanciera.SelectedValue == "10")
                //{
                //    ValidaTotalPedido();
                //    ValidaTipoNivelPrecio();
                //}

                lkLaTorre.Visible = false;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError2.Visible = true;
                lbError2.Text = string.Format(" - Error al aplicar la promoción {2}. {0} {1}", ex.Message, m, lkLaTorre.Text);
            }
        }

        protected void lkEnviarExpedienteOficina_Click(object sender, EventArgs e)
        {
            try
            {
                Label lbPedido = (Label)gridPedidos.SelectedRow.FindControl("lbPedido");
                ImprimirExpediente(lbPedido.Text, "O", txtObservacionesEnviar.Text);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error en el pedido Oficina {0} {1}", ex.Message, m);
            }
        }

        protected void lkEnviarExpedienteVendedor_Click(object sender, EventArgs e)
        {
            try
            {
                Label lbPedido = (Label)gridPedidos.SelectedRow.FindControl("lbPedido");
                ImprimirExpediente(lbPedido.Text, "M", txtObservacionesEnviar.Text);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error en el pedido Vendedor {0} {1}", ex.Message, m);
            }
        }

        protected void lkEnviarReqTransportista_Click(object sender, EventArgs e)
        {
            try
            {
                Label lbPedido = (Label)gridPedidos.SelectedRow.FindControl("lbPedido");
                ImprimirExpediente(lbPedido.Text, "B", txtObservacionesEnviar.Text);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbError.Text = string.Format("Error en el pedido Req {0} {1}", ex.Message, m);
            }
        }

        protected void lkOcultarEnviar_Click(object sender, EventArgs e)
        {
            liEnviarExpediente.Visible = false;
        }

        protected void lkEnviarArmados_Click(object sender, EventArgs e)
        {
            //Este botón queda obsoleto por enviar desde el PDV las requisiciones de armado
            try
            {

                lbError.Text = "Por favor usar las requisiciones de armados.";

                //Label lbPedido = (Label)gridPedidos.SelectedRow.FindControl("lbPedido");
                //ImprimirExpediente(lbPedido.Text, "A", txtObservacionesEnviar.Text);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al enviar expediente a Armados {0} {1}", ex.Message, m);
            }
        }

        protected void lkReqArmado_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                txtVale1.Text = "0";
                txtFacturado.Text = "N";
                txtTipoOferta.Text = "N";
                txtAutorizacion.Text = "0";
                txtObservacionesEnviar.Text = "";

                if (cbTipoArmado.SelectedValue == "--")
                {
                    lbError.Text = "Debe seleccionar el tipo de armado";
                    cbTipoArmado.Focus();
                    return;
                }

                DateTime mFechaArmado = DateTime.Now.Date;

                DateTime mFechaArmadoValidar = DateTime.Now.Date.AddDays(2);
                if (mFechaArmadoValidar.DayOfWeek == DayOfWeek.Sunday) mFechaArmadoValidar = mFechaArmadoValidar.AddDays(1);

                try
                {
                    if (txtAnioArmado.Text.Trim().Length == 2) txtAnioArmado.Text = string.Format("20{0}", txtAnioArmado.Text);
                    mFechaArmado = new DateTime(Convert.ToInt32(txtAnioArmado.Text), Convert.ToInt32(cbMesArmado.SelectedValue), Convert.ToInt32(cbDiaArmado.SelectedValue));
                }
                catch
                {
                    lbError.Text = "Debe ingresar una fecha de armado válida.";
                    cbDiaArmado.Focus();
                    return;
                }

                if (mFechaArmado < mFechaArmadoValidar)
                {
                    lbError.Text = "La fecha de armado ingresada es inválida.";
                    cbDiaArmado.Focus();
                    return;
                }

                txtObsArmado.Text = txtObsArmado.Text.Replace(">", "").Replace("<", "").Replace("&", "");

                if (txtObsArmado.Text.Trim().Length == 0)
                {
                    lbError.Text = "Debe ingresar las observaciones del armado";
                    txtObsArmado.Focus();
                    return;
                }

                //var ws = new wsPuntoVenta.wsPuntoVenta();
                //if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                //string mMensaje = ""; string mRequisicion = ""; bool mYaEnviada = false;

                //if (!ws.GrabarReqArmado(txtPedido.Text, cbTipoArmado.SelectedValue, mFechaArmado, txtObsArmado.Text.Trim(), ref mMensaje, ref mRequisicion, ref mYaEnviada, Convert.ToString(Session["usuario"])))
                //{
                //    lbError.Text = mMensaje;
                //    return;
                //}

                Desarmado mDesarmado = new Desarmado();

                mDesarmado.Tienda = Convert.ToString(Session["Tienda"]);
                mDesarmado.Vendedor = Convert.ToString(Session["Vendedor"]);
                mDesarmado.Usuario = Convert.ToString(Session["usuario"]);
                mDesarmado.FechaDesarmado = mFechaArmado;
                mDesarmado.Observaciones = txtObsArmado.Text.Trim();
                mDesarmado.Cliente = txtCodigo.Text;
                mDesarmado.Tipo = cbTipoArmado.SelectedValue.ToString();
                mDesarmado.Pedido = txtPedido.Text;

                string json = JsonConvert.SerializeObject(mDesarmado);
                byte[] data = Encoding.ASCII.GetBytes(json);

                WebRequest request = WebRequest.Create(string.Format("{0}/SolicitudDesarmado/", Convert.ToString(Session["UrlRestServices"])));

                request.Method = "PUT";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string mRetorna = responseString.ToString().Replace("\"", "");
                if (mRetorna.ToLower().Contains("error"))
                {
                    lbError.Text = mRetorna;
                    return;
                }

                request = WebRequest.Create(string.Format("{0}/SolicitudDesarmado/{1}", Convert.ToString(Session["UrlRestServices"]), txtPedido.Text));
                request.Method = "GET";

                response = (HttpWebResponse)request.GetResponse();
                responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                mDesarmado = JsonConvert.DeserializeObject<Desarmado>(responseString);

                txtVale1.Text = mDesarmado.Mensaje;
                txtFacturado.Text = mDesarmado.YaEnviada.ToString();
                txtAutorizacion.Text = mDesarmado.NoRequisicion.ToString();
                txtObservacionesEnviar.Text = txtObsArmado.Text.Trim();

                ImprimirExpediente(txtPedido.Text, "A", "");

                if (txtTipoOferta.Text == "N")
                {
                    mDesarmado.Mensaje = lbInfo.Text;

                    txtObsArmado.Text = "";
                    liReqArmados.Visible = false;
                    lbInfo.Text = mDesarmado.Mensaje;

                    txtVale1.Text = "0";
                    txtFacturado.Text = "N";
                    txtTipoOferta.Text = "N";
                    txtAutorizacion.Text = "0";
                    txtObservacionesEnviar.Text = "";
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al enviar la Requisición de Armado {0} {1}", ex.Message, m);
            }
        }

        void RequisicionArmado()
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mMensaje = "";
                Label lbNumeroPedido = (Label)gridArticulos.SelectedRow.FindControl("lbNumeroPedido");
                Label lbLinea = (Label)gridArticulos.SelectedRow.FindControl("lbLinea");
                LinkButton lkArmado = (LinkButton)gridArticulos.SelectedRow.FindControl("lkRequisicionArmado");

                lbInfo.Text = "";
                lbError.Text = "";

                string mArmado = lkArmado.Text;

                if (!ws.CambiarArmado(lbNumeroPedido.Text, lbLinea.Text, ref mMensaje, ref mArmado))
                {
                    lbError.Text = mMensaje;
                    return;
                }

                lkArmado.Text = mArmado;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al enviar la Requisición de Armado {0} {1}", ex.Message, m);
            }
        }

        protected void lkRequisicionArmado_Click(object sender, EventArgs e)
        {
            ViewState["AccionArticulo"] = "Requisicion";
        }

        void generarGarantia(string serie)
        {
            try
            {
                DateTime mFechaFactura = DateTime.Now.Date;
                string mMensaje = ""; string mNumero = ""; string mFactura = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                lbInfo.Text = "";
                lbError.Text = "";

                if (!ws.GrabarGarantia(serie, ref mNumero, ref mFactura, ref mFechaFactura, txtPedido.Text, lbLineaArticulo.Text, lbArticuloGarantia.Text, Convert.ToString(Session["Usuario"]), ref mMensaje))
                {
                    lbError.Text = mMensaje;
                    return;
                }

                lbInfo.Text = mMensaje;


                using (ReportDocument reporte = new ReportDocument())
                {
                    string mReporte = "rptGarantiaCamas.rpt";
                    if (serie == "E") mReporte = "rptGarantiaEspecial.rpt";
                    if (serie == "S") mReporte = "rptGarantiaSalas.rpt";
                    if (serie == "V") mReporte = "rptGarantiaVarios.rpt";

                    string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mReporte);
                    reporte.Load(p);

                    reporte.SetParameterValue("serie", serie);
                    reporte.SetParameterValue("numero", mNumero);
                    reporte.SetParameterValue("cliente", txtNombre.Text);
                    reporte.SetParameterValue("fechaCompra", mFechaFactura);
                    reporte.SetParameterValue("factura", mFactura);
                    reporte.SetParameterValue("articulo", lbArticuloGarantia.Text);
                    reporte.SetParameterValue("descripcion", lbArticuloDescripcionGarantia.Text);

                    string mNombreDocumento = string.Format("Garantia_{0}_{1}.pdf", serie, mNumero);
                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error en la garantía {0} {1}", ex.Message, m);
            }
        }

        protected void lkGarantiaCamas_Click(object sender, EventArgs e)
        {
            generarGarantia("C");
        }

        protected void lkGarantiaEspecial_Click(object sender, EventArgs e)
        {
            generarGarantia("E");
        }

        protected void lkGarantiaSalas_Click(object sender, EventArgs e)
        {
            generarGarantia("S");
        }

        protected void lkGarantiaVarios_Click(object sender, EventArgs e)
        {
            generarGarantia("V");
        }

        protected void lkGrabarGarantia_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                bool fileOK = false;
                string mExtension = "";

                if (ArchivoGarantia.HasFile)
                {
                    string fileExtension = System.IO.Path.GetExtension(ArchivoGarantia.FileName).ToLower();
                    string[] allowedExtensions = { ".pdf", ".jpg", ".jpeg" };
                    for (int i = 0; i < allowedExtensions.Length; i++)
                    {
                        if (fileExtension == allowedExtensions[i]) fileOK = true;
                    }

                    mExtension = fileExtension;
                }

                if (fileOK)
                {
                    mExtension = mExtension.ToLower().Replace(".", "");
                    string mArchivo = @"C:\reportes\" + ArchivoGarantia.PostedFile.FileName;

                    if (File.Exists(mArchivo)) File.Delete(mArchivo);
                    ArchivoGarantia.PostedFile.SaveAs(mArchivo);

                    byte[] documento;

                    using (FileStream lfilestram = new FileStream(mArchivo, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        using (BinaryReader lbinaryread = new BinaryReader(lfilestram))
                        {
                            documento = lbinaryread.ReadBytes((Int32)lfilestram.Length);
                        }
                    }

                    string mMensaje = "";
                    var ws = new wsPuntoVenta.wsPuntoVenta();
                    if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                    if (!ws.GrabarGarantiaArchivo(ref mMensaje, txtPedido.Text, lbLineaArticulo.Text, documento, mExtension))
                    {
                        lbError.Text = mMensaje;
                        return;
                    }

                    lbInfo.Text = mMensaje;
                }
                else
                {
                    lbError.Text = "Solo se permiten archivos de imagen (JPG) o PDF.";
                    return;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al cargar el archvio escaneado de la garantía {0} {1}", ex.Message, m);
            }
        }

        protected void lkGarantiaVer_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                string mMensaje = "";
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                byte[] documento = null; string mExtension = ""; string mNombreDocumento = "";

                if (!ws.DevuelveGarantiaArchivo(ref mMensaje, txtPedido.Text, lbLineaArticulo.Text, ref documento, ref mExtension, ref mNombreDocumento))
                {
                    lbError.Text = mMensaje;
                    return;
                }

                if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                FileStream fs = new FileStream(@"C:\reportes\" + mNombreDocumento, FileMode.OpenOrCreate, FileAccess.Write);
                fs.Write(documento, 0, documento.Length);
                fs.Close();

                Response.Clear();
                Response.ContentType = "application/" + mExtension;
                Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                Response.End();

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al descargar el archvio escaneado de la garantía {0} {1}", ex.Message, m);
            }
        }

        protected void lkImprimirCartaArmado_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                DataSet dsArmado = new DataSet();
                DataTable dtArmado = new DataTable("Articulos");

                dtArmado.Columns.Add("Articulo", typeof(System.String));
                dtArmado.Columns.Add("Descripcion", typeof(System.String));

                foreach (GridViewRow gvr in gridArticulos.Rows)
                {
                    Label lbArticulo = (Label)gvr.FindControl("lbArticulo");
                    Label lbNombreArticulo = (Label)gvr.FindControl("lbNombreArticulo");
                    LinkButton lkArmado = (LinkButton)gvr.FindControl("lkRequisicionArmado");

                    if (lkArmado.Text == "Sí")
                    {
                        DataRow mRow = dtArmado.NewRow();
                        mRow["Articulo"] = lbArticulo.Text;
                        mRow["Descripcion"] = lbNombreArticulo.Text;
                        dtArmado.Rows.Add(mRow);
                    }
                }

                if (dtArmado.Rows.Count == 0)
                {
                    lbError.Text = "En este pedido no hay artículos para armado.";
                    return;
                }

                dsArmado.Tables.Add(dtArmado);

                DateTime mFechaFactura = DateTime.Now.Date;
                string mMensaje = ""; string mFactura = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.DevuelveFechaFactura(txtPedido.Text, ref mFactura, ref mFechaFactura, ref mMensaje))
                {
                    lbError.Text = mMensaje;
                    return;
                }

                using (ReportDocument reporte = new ReportDocument())
                {
                    string mReporte = "rptCartaArmado.rpt";

                    string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mReporte);
                    reporte.Load(p);

                    reporte.SetDataSource(dsArmado);
                    reporte.SetParameterValue("serie", "");
                    reporte.SetParameterValue("numero", txtPedido.Text);
                    reporte.SetParameterValue("cliente", txtNombre.Text);
                    reporte.SetParameterValue("fechaCompra", mFechaFactura);
                    reporte.SetParameterValue("factura", mFactura);
                    reporte.SetParameterValue("articulo", "");
                    reporte.SetParameterValue("descripcion", "");

                    string mNombreDocumento = string.Format("CartaArmado_{0}.pdf", txtPedido.Text);
                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al imprmir la carta de armado {0} {1}", ex.Message, m);
            }
        }

        protected void lkGrabarCartaArmado_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                bool fileOK = false;
                string mExtension = "";

                if (ArchivoArmado.HasFile)
                {
                    string fileExtension = System.IO.Path.GetExtension(ArchivoArmado.FileName).ToLower();
                    string[] allowedExtensions = { ".pdf", ".jpg", ".jpeg" };
                    for (int i = 0; i < allowedExtensions.Length; i++)
                    {
                        if (fileExtension == allowedExtensions[i]) fileOK = true;
                    }

                    mExtension = fileExtension;
                }

                if (fileOK)
                {
                    mExtension = mExtension.ToLower().Replace(".", "");
                    string mArchivo = @"C:\reportes\" + ArchivoArmado.PostedFile.FileName;

                    if (File.Exists(mArchivo)) File.Delete(mArchivo);
                    ArchivoArmado.PostedFile.SaveAs(mArchivo);

                    byte[] documento;

                    using (FileStream lfilestram = new FileStream(mArchivo, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        using (BinaryReader lbinaryread = new BinaryReader(lfilestram))
                        {
                            documento = lbinaryread.ReadBytes((Int32)lfilestram.Length);
                        }
                    }

                    string mMensaje = "";
                    var ws = new wsPuntoVenta.wsPuntoVenta();
                    if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                    if (!ws.GrabarCartaArmadoArchivo(ref mMensaje, txtPedido.Text, documento, mExtension))
                    {
                        lbError.Text = mMensaje;
                        return;
                    }

                    lbInfo.Text = mMensaje;
                }
                else
                {
                    lbError.Text = "Solo se permiten archivos de imagen (JPG) o PDF.";
                    return;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al grabar la carta de armado {0} {1}", ex.Message, m);
            }
        }

        protected void lkVerCartaArmado_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                string mMensaje = "";
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                byte[] documento = null; string mExtension = ""; string mNombreDocumento = "";

                if (!ws.DevuelveCartaArmadoArchivo(ref mMensaje, txtPedido.Text, ref documento, ref mExtension, ref mNombreDocumento))
                {
                    lbError.Text = mMensaje;
                    return;
                }

                if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                FileStream fs = new FileStream(@"C:\reportes\" + mNombreDocumento, FileMode.OpenOrCreate, FileAccess.Write);
                fs.Write(documento, 0, documento.Length);
                fs.Close();

                Response.Clear();
                Response.ContentType = "application/" + mExtension;
                Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                Response.End();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al descargar el archvio escaneado de la carta de armado {0} {1}", ex.Message, m);
            }
        }

        protected void lkAutorizar_Click(object sender, EventArgs e)
        {
            EnviarSolicitud();
        }

        protected void lkSolicitudEnLinea_Click(object sender, EventArgs e)
        {
            ImprimirSolicitud(txtPedido.Text, "7", false, "", "");
        }

        protected void lkPagareEnLinea_Click(object sender, EventArgs e)
        {
            ImprimirPagare(txtPedido.Text, "7");
        }

        protected void btnActualizarEntrega_Click(object sender, EventArgs e)
        {
            try
            {
                lbError.Text = "";
                lbInfo.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                bool mEnviarExpedienteBodega = false;
                bool mValidarFecha = ws.EsCiudad(Convert.ToString(Session["Tienda"]));

                if (!mValidarFecha)
                {
                    List<wsPuntoVenta.PedidoLinea> q = new List<wsPuntoVenta.PedidoLinea>();
                    q = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                    foreach (var item in q)
                    {
                        mValidarFecha = ws.EsCiudad(item.Bodega);
                    }
                }

                DateTime mFechaEntrega = DateTime.Now.Date;

                try
                {
                    if (txtAnio.Text.Trim().Length == 2) txtAnio.Text = string.Format("20{0}", txtAnio.Text);
                    mFechaEntrega = new DateTime(Convert.ToInt32(txtAnio.Text), Convert.ToInt32(cbMes.SelectedValue), Convert.ToInt32(cbDia.SelectedValue));
                }
                catch
                {
                    lbError.Text = "Debe ingresar una fecha de entrega válida.";
                    cbDia.Focus();
                    return;
                }

                if (mValidarFecha)
                {
                    DateTime mFechaValidar = DateTime.Now.Date.AddDays(2);
                    if (mFechaValidar.DayOfWeek == DayOfWeek.Sunday && mFechaValidar.Month != 7 && mFechaValidar.Month != 12) mFechaValidar = mFechaValidar.AddDays(1);

                    if (mFechaEntrega < mFechaValidar)
                    {
                        lbError.Text = string.Format("La fecha de entrega mínima admitida es a partir del {0}{1}/{2}{3}/{4}", mFechaValidar.Day < 10 ? "0" : "", mFechaValidar.Day, mFechaValidar.Month < 10 ? "0" : "", mFechaValidar.Month, mFechaValidar.Year);
                        cbDia.Focus();
                        return;
                    }

                    mEnviarExpedienteBodega = true;
                }

                bool mYaEnviadoBodega = false;
                if (!ws.ActualizaFechaEntrega(ref mMensaje, Convert.ToString(Session["Usuario"]).ToString(), txtPedido.Text, mFechaEntrega, cbEntrega.SelectedValue.ToString(), ref mYaEnviadoBodega))
                {
                    lbError.Text = mMensaje;
                    return;
                }

                if (mFechaEntrega >= (DateTime.Now.Date.AddDays(7)) && mEnviarExpedienteBodega)
                {
                    if (mYaEnviadoBodega)
                    {
                        mMensaje = string.Format("{0}  El expediente fue enviado a bodega cuando se grabó la factura.", mMensaje);
                    }
                    else
                    {
                        Label lbPedido = (Label)gridPedidos.SelectedRow.FindControl("lbPedido");
                        ImprimirExpediente(lbPedido.Text, "B", txtObservacionesEnviar.Text);

                        ws.MarcarEnviadoBodega(lbPedido.Text);
                        mMensaje = string.Format("{0}  Y se envió el expediente a bodega para que se reserven sus artículos", mMensaje);
                    }
                }


                lbInfo.Text = mMensaje;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al actualizar la fecha de entrega {0} {1}", ex.Message, m);
            }
        }

        protected void btnVerificar_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                if (txtPedido.Text == "P000000")
                {
                    lbError.Text = "Debe seleccionar un pedido";
                    return;
                }

                lbEstadoSolicitud.Visible = false;
                string mMensaje = ""; string mMensaje2 = ""; string mAutorizacion = ""; string mQuienAutorizo = ""; string mNombre = "";

                WebRequest request = WebRequest.Create(string.Format("{0}/ConsultaInterconsumo/?documento={1}", Convert.ToString(Session["UrlRestServices"]), txtPedido.Text));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                dynamic mConsultaInterconsumo = JObject.Parse(responseString);

                if (!Convert.ToBoolean(mConsultaInterconsumo.exito))
                {
                    lbError.Text = mConsultaInterconsumo.mensaje;
                    return;
                }

                mMensaje = mConsultaInterconsumo.mensaje;
                mMensaje2 = mConsultaInterconsumo.mensaje2;
                mAutorizacion = mConsultaInterconsumo.autorizacion;
                mQuienAutorizo = mConsultaInterconsumo.quienAutorizo;
                mNombre = mConsultaInterconsumo.nombre;

                FacturarPedido(txtPedido.Text);

                lbEstadoSolicitud.Visible = true;
                lbEstadoSolicitud.Text = mMensaje;

                lbInfo.Text = mMensaje2;
                lkSolicitudEnLinea.Visible = true;
                lkPagareEnLinea.Visible = true;
                txtPagare.Text = mConsultaInterconsumo.solicitud;
                txtSolicitud.Text = mConsultaInterconsumo.solicitud;
                txtQuienAutorizo.Text = mQuienAutorizo;
                txtNoAutorizacion.Text = mAutorizacion;

                if (mNombre.Trim().Length > 0) txtNombre.Text = mNombre;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al verificar el estado de la solicitud {0} {1}", ex.Message, m);
            }
        }

        protected void btnPortal_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                if (txtPedido.Text == "P000000")
                {
                    lbError.Text = "Debe seleccionar un pedido";
                    return;
                }

                WebRequest request = WebRequest.Create(string.Format("{0}/LinkInterconsumo/?documento={1}", Convert.ToString(Session["UrlRestServices"]), txtPedido.Text));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                dynamic mLinkInterconsumo = JObject.Parse(responseString);

                if (!Convert.ToBoolean(mLinkInterconsumo.exito))
                {
                    lbError.Text = mLinkInterconsumo.mensaje;
                    return;
                }

                string mLink = mLinkInterconsumo.link;

                string mArchivo = string.Format("{0}_{1}.mf", txtPedido.Text, txtCodigo.Text);
                System.IO.File.WriteAllText(@"C:\reportes\" + mArchivo, mLink);

                Response.Clear();
                Response.ContentType = "application/mf";

                Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mArchivo);
                Response.WriteFile(@"C:\reportes\" + mArchivo);
                Response.End();

                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al abrir el portal {0} {1}", ex.Message, m);
            }
        }

        protected void btnDeclaracion_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                if (txtPedido.Text == "P000000")
                {
                    lbError.Text = "Debe seleccionar un pedido";
                    return;
                }

                if (cbFinanciera.SelectedValue == "12")
                {
                    Response.Redirect(string.Format("{0}/Report/IngresosAtid?id={1}", WebConfigurationManager.AppSettings["ReportServer"], txtPedido.Text), false);
                }
                else
                {
                    WebRequest request = WebRequest.Create(string.Format("{0}/DeclaracionIngresosInterconsumo/?documento={1}", Convert.ToString(Session["UrlRestServices"]), txtPedido.Text));

                    request.Method = "GET";
                    request.ContentType = "application/json";

                    var response = (HttpWebResponse)request.GetResponse();
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    dynamic mLinkInterconsumo = JObject.Parse(responseString);

                    if (!Convert.ToBoolean(mLinkInterconsumo.exito))
                    {
                        lbError.Text = mLinkInterconsumo.mensaje;
                        return;
                    }

                    string mLink = mLinkInterconsumo.link;

                    string mArchivo = string.Format("{0}_{1}.mf", txtPedido.Text, txtCodigo.Text);
                    System.IO.File.WriteAllText(@"C:\reportes\" + mArchivo, mLink);

                    Response.Clear();
                    Response.ContentType = "application/mf";

                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mArchivo);
                    Response.WriteFile(@"C:\reportes\" + mArchivo);
                    Response.End();

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
            }
            catch (Exception ex)
            {
                lbError.Text = CatchClass.ExMessage(ex, "pedidos.aspx", "btnDeclaracion_Click");
            }
        }

        protected void btnBienvenida_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                if (txtPedido.Text == "P000000")
                {
                    lbError.Text = "Debe seleccionar un pedido";
                    return;
                }

                Response.Redirect(string.Format("{0}/Report/BienvenidaAtid?id={1}", WebConfigurationManager.AppSettings["ReportServer"], txtPedido.Text), false);
            }
            catch (Exception ex)
            {
                lbError.Text = CatchClass.ExMessage(ex, "pedidos.aspx", "btnBienvenida_Click");
            }
        }

        protected void btnInterconsumo_Click(object sender, EventArgs e)
        {
           
                EnviarSolicitud();
        }

        void EnviarSolicitud()
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                if (txtPedido.Text == "P000000")
                {
                    lbError.Text = "Debe seleccionar un pedido";
                    return;
                }

                List<Catalogo> varFinancieras = new List<Catalogo>();
                List<Clases.Catalogo> varTiendas = new List<Clases.Catalogo>();
                lbEstadoSolicitud.Visible = false;
                string mMensaje = ""; string mSolicitud = ""; string mAutorizacion = ""; string mRespuesta = ""; string mQuienAutorizo = "";
                
                    if (cbFinanciera.SelectedValue.ToString() == "12")
                    {
                        //Validar datos del cliente
                        WebRequest request = WebRequest.Create(string.Format("{0}/validacionescrediplus/{1}", Convert.ToString(Session["UrlRestServices"]), txtPedido.Text));

                        request.Method = "GET";
                        request.ContentType = "application/json";

                        var response = (HttpWebResponse)request.GetResponse();
                        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                        string mRespuestaValidacion = responseString.ToString().Replace("\"", "");

                        if (mRespuestaValidacion.Length > 2)
                        {
                            lbError.Text = mRespuestaValidacion;
                            return;
                        }
                    }

                    #region "Huella digital y fotografía"
                    try
                    {

                        varFinancieras = WebRestClient.ObtenerFinancierasConHuellaYFoto(Convert.ToString(Session["UrlRestServices"]));
                        varTiendas = WebRestClient.ObtenerTiendasConHuellaYFoto(Convert.ToString(Session["UrlRestServices"]));
                    }
                    catch
                    {
                    }
                    #endregion


                    if (varFinancieras.Count > 0 && varFinancieras.Find(x => x.Codigo == cbFinanciera.SelectedItem.Value) != null && varTiendas.Count > 0 && varTiendas.Find(x => x.Codigo == Session["Tienda"].ToString()) != null)
                    {

                        //**
                        //antes de enviar la solicitud, grabar la huella o verificarla
                        //*
                        //btnRefreshSolCredito.Visible = true;
                        //string s = "<script language=JavaScript> window.open('"+ Session["UrlFiestaNetERP"].ToString()  +"/IdentidadCliente/" + txtPedido.Text.Trim()+ "/pedidos'); </script>";
                        //Page.ClientScript.RegisterStartupScript(this.GetType(), "test", s);

                        //Response.Redirect(Session["UrlFiestaNetERP"].ToString() + "/IdentidadCliente/" + txtPedido.Text.Trim());
                        
                            Enviar_Solicitud_ATID(txtPedido.Text);
                        

                        Mostrar_Respuesta_Solicitud();
                        //string s = "<script language=JavaScript> window.open('" + Session["UrlFiestaNetERP"].ToString() + "/ResultadoPreCalificacion/" + txtPedido.Text.Trim() + "'); </script>";
                        //Page.ClientScript.RegisterStartupScript(this.GetType(), "test", s);
                        
                    }
                    else if (varFinancieras.Count > 0 && varFinancieras.Find(x => x.Codigo == cbFinanciera.SelectedItem.Value) != null && varTiendas.Count > 0 && varTiendas.Find(x => x.Codigo == Session["Tienda"].ToString()) == null)
                    {
                        lbError.Text = "Esta tienda no tiene habilitada la opción de solicitud";
                    }
                    else if (cbFinanciera.SelectedValue.Equals(7))
                    {
                        #region "INTERCONSUMO"
                        WebRequest request = WebRequest.Create(string.Format("{0}/PreAutorizacionInterconsumo/?documento={1}", Convert.ToString(Session["UrlRestServices"]), txtPedido.Text));

                        request.Method = "GET";
                        request.ContentType = "application/json";

                        var response = (HttpWebResponse)request.GetResponse();
                        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                        dynamic mInterconsumo = JObject.Parse(responseString);
                        mMensaje = mInterconsumo.mensaje;

                        if (!Convert.ToBoolean(mInterconsumo.exito))
                        {
                            lbError.Text = mMensaje;

                            if (mMensaje == "cliente")
                            {
                                Session["TipoVenta"] = cbTipo.SelectedValue;
                                Session["Financiera"] = cbFinanciera.SelectedValue;
                                Session["NivelPrecio"] = cbNivelPrecio.SelectedValue;
                                Response.Redirect("clientes.aspx");
                            }

                            return;
                        }

                        mSolicitud = mInterconsumo.solicitud;
                        mAutorizacion = mInterconsumo.autorizacion;
                        mRespuesta = mInterconsumo.resultadoSolicitud;
                        mQuienAutorizo = mInterconsumo.quienAutorizo;
                        #endregion
                    }
                



                if (mSolicitud.Trim().Length > 0)
                {
                    txtSolicitud.Text = mSolicitud;
                    txtPagare.Text = mSolicitud;
                    txtNoAutorizacion.Text = mAutorizacion;
                    txtQuienAutorizo.Text = mQuienAutorizo;
                }
                
                lbEstadoSolicitud.Visible = true;
                lbEstadoSolicitud.Text = mRespuesta;

                string mMensaje2 = "";
                if (mRespuesta.Contains("GRABADA"))
                {
                    lkPagareEnLinea.Visible = true;
                    lkSolicitudEnLinea.Visible = true;
                    lbEstadoSolicitud.ForeColor = System.Drawing.Color.Green;

                    if (!cbLocalF09.Visible)
                    {
                        lkPagareEnLinea.Text = "Pagaré";
                        lkSolicitudEnLinea.Text = "Solicitud";
                    }
                }

                lbInfo.Text = string.Format("{0}{1}", mMensaje, mMensaje2);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al enviar la solicitud a interconsumo {0} {1}", ex.Message, m);
            }
        }

        private void Enviar_Solicitud_ATID(string pedido)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";

                if (txtPedido.Text == "P000000")
                {
                    lbError.Text = "Debe seleccionar un pedido";
                    return;
                }

                var client = new RestClient(string.Format("{0}/SolicitudCredito/", Convert.ToString(Session["UrlRestServices"])));
                var request = new RestRequest("/EnviarSolicitud/", Method.POST);

                request.ReadWriteTimeout = 36000000;
                request.AddParameter("application/json", txtPedido.Text.Trim(), RestSharp.ParameterType.RequestBody);
                ////----------------------------------------------------------------------
                ////Respuesta de parte del servicio devolviendo un objeto UsuarioPDVDto e
                ////en el atributo response.Data
                log.Info(string.Format("Envio solicitud atid: {0}", txtPedido.Text.Trim()));

                //IRestResponse<bool> response = client.Execute<bool>(request);

                client.ExecuteAsync(request, response1 =>
                {
                    callback(response1.Content);
                });


            }
            catch (Exception ex)
            {
                lbError.Text = CatchClass.ExMessage(ex, "pedidos.aspx", "Enviar_Solicitud_ATID");
            }
        }

        private void Mostrar_Respuesta_Solicitud()
        {
            if (Session["delay"] != null)
                Session["delay"] = int.Parse(Session["delay"].ToString()) + 1;
            else
                Session["delay"] = 1;

            if (Session["Data"] == null)
            {
                //no más de 15 ciclos (75 segundos de espera)
                if (int.Parse(Session["delay"].ToString()) < 15)
                {
                    Thread.Sleep(5000);
                    Mostrar_Respuesta_Solicitud();
                }
            }
            else
                Response.Redirect("~/pedidos.aspx?ErrorMsg2=" + Session["Data"].ToString());
        }
        protected void lkImprimirVale_Click(object sender, EventArgs e)
        {
            try
            {
                lbError.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mMensaje = "";
                string mFactura = Convert.ToString(ViewState["Factura"]);

                var q = ws.DevuelveCertificados(mFactura, ref mMensaje);

                if (mMensaje.Trim().Length > 0)
                {
                    lbError.Text = mMensaje;
                    return;
                }

                dsPuntoVenta ds = new dsPuntoVenta();

                foreach (var item in q)
                {
                    DataRow mRow = ds.Certificados.NewRow();
                    mRow["Vale"] = item.Vale;
                    mRow["Beneficiario"] = item.Beneficiario;
                    mRow["Comprador"] = item.Comprador;
                    mRow["FechaCompra"] = item.FechaCompra;
                    mRow["FechaVence"] = item.FechaVence;
                    mRow["Valor"] = item.Valor;
                    ds.Certificados.Rows.Add(mRow);
                }

                string mNombreDocumento = string.Format("ValeFactura_{0}.pdf", mFactura);

                using (ReportDocument reporte = new ReportDocument())
                {
                    string mFormato = "rptCertificado.rpt";

                    string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mFormato);
                    reporte.Load(p);

                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                    reporte.SetDataSource(ds);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);
                }

                Response.Clear();
                Response.ContentType = "application/pdf";

                Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                Response.WriteFile(@"C:\reportes\" + mNombreDocumento);

                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                lbError.Text = CatchClass.ExMessage(ex, "pedidos", "lkImprimirVAle_Click");
            }
        }

        protected void btnRefreshSolCredito_Click(object sender, EventArgs e)
        {
            RespuestaSolicitud oRespuesta = new RespuestaSolicitud();
            if (txtPedido.Text != "" && txtPedido.Text.ToUpper().Contains("P"))
            {
                string urlRest = Convert.ToString(Session["UrlRestServices"]);
                if (urlRest != string.Empty)
                {
                    var client = new RestClient(urlRest);
                    var restRequest = new RestRequest("api/RespuestaSolicitud/" + txtPedido.Text, Method.GET)
                    {
                        RequestFormat = DataFormat.Json
                    };

                    var result = client.Get(restRequest);
                    if (result != null)
                    {
                        oRespuesta = JsonConvert.DeserializeObject<RespuestaSolicitud>(result.Content);
                        if (oRespuesta.MensajeError == null)
                        {
                            txtSolicitud.Text = oRespuesta.NumeroSolicitud;
                            txtPagare.Text = oRespuesta.NumeroSolicitud;
                            txtNoAutorizacion.Text = oRespuesta.NumeroCredito;
                            txtQuienAutorizo.Text = "";

                            lbEstadoSolicitud.Visible = true;
                        }
                        else
                        {
                            lbLinkInterconsumo.Text = oRespuesta.MensajeError;
                            lbEstadoSolicitud.Visible = false;
                        }

                    }
                }
            }
        }

        private void callback(string content)
        {
            ThreadStart childthreat = new ThreadStart(() => RespuestaSolicitud(content));
            Thread child = new Thread(childthreat);
            
            child.Start();
            //Thread.Sleep(3000);
            //child.Abort();
            
            
        }
        private void RespuestaSolicitud(string content)
        {
            try
            {
                lbError.Visible = true;
                lbInfo.Visible = true;
                string resultado = content;
                Respuesta oResp1 = JsonConvert.DeserializeObject<Respuesta>(content);
                if (oResp1.Objeto != null)
                {
                    RespuestaSolicitud oResp = new RespuestaSolicitud();

                    oResp = JsonConvert.DeserializeObject<RespuestaSolicitud>(oResp1.Objeto.ToString());

                    if (oResp.MensajeError.Length > 0)
                    {
                        if(oResp.MensajeError.Contains("An error occurred while sending the request"))
                            lbError.Text = "Ocurrió un inconveniente con el servicio de CREDIPLUS, por favor intente nuevamente en unos minutos.";
                        else
                            lbError.Text = "Ocurrió un error con el servicio de CREDIPLUS: [" + oResp.MensajeError + "]";
                        Session["Data"] = oResp.MensajeError;
                    }
                    else
                    {
                        if (oResp.MensajeError.Length == 0)
                        {
                            if (oResp.ResultadoPrecalificacion == "Prohibitivo")
                            {
                                lbError.Text = "En este momento CrediPlus no le puede otorgar crédito. Por favor contacte a los asesores de CREDIPLUS al PBX. 66261351";
                                Session["Data"] = lbError.Text;
                            }
                            else if(oResp.ResultadoPrecalificacion.ToLower().Equals("aceptable") || oResp.ResultadoPrecalificacion.ToLower().Equals("optimo"))
                            {
                                lbInfo.Text = "Tu solicitud ha sido PRE-AUTORIZADA, favor de completar la papelería del cliente. CRÉDITO No. "+oResp.NumeroCredito+" | SOLICITUD No. "+oResp.NumeroSolicitud;
                                Session["Data"] = lbInfo.Text;
                            }
                            else
                                Session["Data"] = oResp.ResultadoPrecalificacion;
                        }
                    }
                }
                else
                    if (oResp1.Mensaje != null)
                {
                    lbInfo.Text = oResp1.Mensaje;
                    Session["Data"] = oResp1.Mensaje;
                }

            }
            catch (Exception ex)
            {
                lbError.Text = "Error con solicitud: " + ex.Message;
            }
        }

        protected void btnHuellayFoto_Click(object sender, EventArgs e)
        {
            lbError.Text = "";

            if (!txtPedido.Text.Equals("P000000"))
            {
                string s = "<script language=JavaScript> window.open('" + Session["UrlFiestaNetERP"].ToString() + "/IdentidadCliente/" + txtPedido.Text.Trim() + "/pedidos'); </script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "test", s);
            }
            else
                lbError.Text = "Debe seleccionar un pedido primero";
        }

        protected void lkAccesorios_Click(object sender, EventArgs e)
        {
            try
            {
                lbError.Text = "";
                lbInfo.Text = "";

                if (Convert.ToString(ViewState["Accesorios"]) == "S")
                {
                    lbError.Text = "La Fiesta de Accesorios ya fue aplicada en este pedido";
                    return;
                }

                List<wsPuntoVenta.PedidoLinea> qOriginal = new List<wsPuntoVenta.PedidoLinea>();
                qOriginal = (List<wsPuntoVenta.PedidoLinea>)ViewState["qArticulos"];

                wsPuntoVenta.PedidoLinea[] q = new wsPuntoVenta.PedidoLinea[1];
                wsPuntoVenta.PedidoLinea[] qLinea = new wsPuntoVenta.PedidoLinea[1];

                int jj = 0;
                for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                {
                    jj++;

                    Label lbPrecioUnitario = (Label)gridArticulos.Rows[ii].FindControl("lbPrecio");
                    Label lbCantidadPedida = (Label)gridArticulos.Rows[ii].FindControl("lbCantidad");
                    Label lbPrecioTotal = (Label)gridArticulos.Rows[ii].FindControl("lbTotal");
                    Label lbDescripcion = (Label)gridArticulos.Rows[ii].FindControl("lbDescripcion");
                    Label lbComentario = (Label)gridArticulos.Rows[ii].FindControl("lbComentario");
                    Label lbEstado = (Label)gridArticulos.Rows[ii].FindControl("lbEstado");
                    Label lbNumeroPedido = (Label)gridArticulos.Rows[ii].FindControl("lbNumeroPedido");
                    Label lbOferta = (Label)gridArticulos.Rows[ii].FindControl("lbOferta");
                    Label lbFechaOfertaDesde = (Label)gridArticulos.Rows[ii].FindControl("lbFechaOfertaDesde");
                    Label lbPrecioOriginal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioOriginal");
                    Label lbEsDetalleKit = (Label)gridArticulos.Rows[ii].FindControl("lbEsDetalleKit");
                    Label lbPrecioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioFacturar");
                    Label lbTipoOferta = (Label)gridArticulos.Rows[ii].FindControl("lbTipoOferta");
                    Label lbVale = (Label)gridArticulos.Rows[ii].FindControl("lbVale");
                    Label lbAutorizacion = (Label)gridArticulos.Rows[ii].FindControl("lbAutorizacion");
                    Label lbBodega = (Label)gridArticulos.Rows[ii].FindControl("lbBodega");
                    Label lbLocalizacion = (Label)gridArticulos.Rows[ii].FindControl("lbLocalizacion");
                    LinkButton lkRequisicionArmado = (LinkButton)gridArticulos.Rows[ii].FindControl("lkRequisicionArmado");
                    Label lbArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbArticulo");
                    Label lbNombreArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbNombreArticulo");
                    Label lbPrecioUnitarioDebioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioUnitarioDebioFacturar");
                    Label lbPrecioSugerido = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioSugerido");
                    Label lbPrecioBaseLocal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioBaseLocal");
                    Label lbGel = (Label)gridArticulos.Rows[ii].FindControl("lbGel");
                    Label lbLinea = (Label)gridArticulos.Rows[ii].FindControl("lbLinea");
                    Label lbPorcentajeDescuento = (Label)gridArticulos.Rows[ii].FindControl("lbPorcentajeDescuento");
                    Label lbBeneficiario = (Label)gridArticulos.Rows[ii].FindControl("lbBeneficiario");
                    var descuento = decimal.Parse(((Label)gridArticulos.Rows[ii].FindControl("lbDescuento")).Text.Replace(",",""));
                    decimal mPrecioOriginal = 0;
                    DateTime mFechaOfertaDesde = new DateTime(1980, 1, 1);

                    try
                    {
                        mFechaOfertaDesde = new DateTime(Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(6, 4)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(3, 2)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(0, 2)));
                    }
                    catch
                    {
                        //Nothing
                    }
                    try
                    {
                        mPrecioOriginal = Convert.ToDecimal(lbPrecioOriginal.Text.Replace(",", ""));
                    }
                    catch
                    {
                        mPrecioOriginal = 0;
                    }

                    wsPuntoVenta.PedidoLinea itemPedidoLinea = new wsPuntoVenta.PedidoLinea();
                    itemPedidoLinea.Articulo = lbArticulo.Text;
                    itemPedidoLinea.Nombre = lbNombreArticulo.Text;
                    itemPedidoLinea.PrecioUnitario = Convert.ToDecimal(lbPrecioUnitario.Text.Replace(",", ""));
                    itemPedidoLinea.CantidadPedida = Convert.ToDecimal(lbCantidadPedida.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioTotal = Convert.ToDecimal(lbPrecioTotal.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioFacturar = Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.Bodega = lbBodega.Text;
                    itemPedidoLinea.Localizacion = lbLocalizacion.Text;
                    itemPedidoLinea.RequisicionArmado = lkRequisicionArmado.Text;
                    itemPedidoLinea.Descripcion = lbDescripcion.Text;
                    itemPedidoLinea.Comentario = lbComentario.Text;
                    itemPedidoLinea.Estado = lbEstado.Text;
                    itemPedidoLinea.NumeroPedido = lbNumeroPedido.Text;
                    itemPedidoLinea.Oferta = lbOferta.Text;
                    itemPedidoLinea.FechaOfertaDesde = mFechaOfertaDesde;
                    itemPedidoLinea.PrecioOriginal = mPrecioOriginal;
                    itemPedidoLinea.TipoOferta = lbTipoOferta.Text;
                    itemPedidoLinea.EsDetalleKit = lbEsDetalleKit.Text;
                    itemPedidoLinea.Vale = Convert.ToInt32(lbVale.Text == "" ? "0" : lbVale.Text);
                    itemPedidoLinea.Autorizacion = Convert.ToInt64(lbAutorizacion.Text == "" ? "0" : lbAutorizacion.Text);
                    itemPedidoLinea.PrecioUnitarioDebioFacturar = Convert.ToDecimal(lbPrecioUnitarioDebioFacturar.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioSugerido = Convert.ToDecimal(lbPrecioSugerido.Text.Replace(",", ""));
                    itemPedidoLinea.PrecioBaseLocal = Convert.ToDouble(lbPrecioBaseLocal.Text.Replace(",", ""));
                    itemPedidoLinea.Gel = lbGel.Text;
                    itemPedidoLinea.Linea = Convert.ToInt32(lbLinea.Text);
                    itemPedidoLinea.PorcentajeDescuento = Convert.ToDecimal(lbPorcentajeDescuento.Text);
                    itemPedidoLinea.Beneficiario = lbBeneficiario.Text;
                    itemPedidoLinea.Descuento= descuento;
                    itemPedidoLinea.NetoFacturar = Convert.ToDecimal(lbPrecioTotal.Text.Replace(",", "")) - itemPedidoLinea.Descuento;

                    if (jj > 1) Array.Resize(ref q, jj);
                    if (jj > 1) Array.Resize(ref qLinea, jj);

                    q[jj - 1] = itemPedidoLinea;
                    qLinea[jj - 1] = itemPedidoLinea;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta(); 
                string mMensaje = ""; 
                string mPrecioBien = ""; 
                string mMonto = ""; 
                string mNotas = "";
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);


                if (!ws.FiestaDeAccesorios(ref qLinea, ref mPrecioBien, ref mMonto, cbNivelPrecio.Text, ref mMensaje, ref mNotas, txtPedido.Text, "", false))
                {
                    lbError.Visible = true;
                    lbError.Text = mMensaje;
                    return;
                }

                string mSeparador = "";
                if (txtNotasTipoVenta.Text.Trim().Length > 0) mSeparador = " - ";
                txtNotasTipoVenta.Text = string.Format("{0}{1}{2}", txtNotasTipoVenta.Text, mSeparador, mNotas);

                gridArticulos.DataSource = qLinea;
                gridArticulos.DataBind();

                ViewState["Accesorios"] = "S";
                ViewState["qArticulos"] = qLinea;

                var Descuentos = qLinea.Sum(x => x.Descuento);

                if (cbFinanciera.SelectedValue == "1" || cbFinanciera.SelectedValue == "5" || cbFinanciera.SelectedValue == "9")
                {
                    txtMonto.Text = mMonto;
                    txtFacturar.Text = String.Format("{0:0,0.00}", (Convert.ToDecimal(mMonto.Replace(",", "")) + Descuentos));
                    txtDescuentos.Text = String.Format("{0:0,0.00}", Descuentos);
                }

                if (ws.EsBanco(cbFinanciera.SelectedValue.ToString()))
                {
                    double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                    decimal mEnganche = Convert.ToDecimal(txtEnganche.Text.Trim().Replace(",", ""));
                    decimal mSaldoFinanciar = Convert.ToDecimal(mPrecioBien.Replace(",", "")) - mEnganche - Descuentos;
                    decimal mMontoTotal = Convert.ToDecimal(Math.Round((double)mSaldoFinanciar * mFactor, 2, MidpointRounding.AwayFromZero));
                    decimal mRecargos = mMontoTotal - mSaldoFinanciar;

                    txtFacturar.Text = mPrecioBien;
                    txtSaldoFinanciar.Text = String.Format("{0:0,0.00}", mSaldoFinanciar);
                    txtIntereses.Text = String.Format("{0:0,0.00}", mRecargos);
                    txtMonto.Text = String.Format("{0:0,0.00}", mMontoTotal);
                    txtDescuentos.Text = String.Format("{0:0,0.00}", Descuentos);

                    Int32 mPagos; string mPagos1;
                    string mPagosTexto = ws.DevuelvePagosNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

                    try
                    {
                        mPagos = Convert.ToInt32(mPagosTexto.Substring(0, 2).Replace(" ", ""));
                    }
                    catch
                    {
                        mPagos = 1;
                    }

                    decimal mCuotas = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) / mPagos;

                    if (mPagos <= 10)
                    {
                        mPagos1 = string.Format("0{0}", mPagos - 1);
                    }
                    else
                    {
                        mPagos1 = (mPagos - 1).ToString();
                    }

                    cbPagos1.SelectedValue = mPagos1;
                    cbPagos2.SelectedValue = "01";

                    if (mCuotas.ToString().Substring(mCuotas.ToString().Length - 2, 2) == "00")
                    {
                        txtPagos1.Text = String.Format("{0:0,0.00}", mCuotas);
                        txtPagos2.Text = String.Format("{0:0,0.00}", mCuotas);
                    }
                    else
                    {
                        #region "aproximaciones para imprimir documentos de financieras"
                        if (cbFinanciera.SelectedValue == Const.FINANCIERA.BANCREDIT)
                        {
                            txtPagos1.Text = String.Format("{0:0,0.00}", Math.Round(mCuotas, 0, MidpointRounding.AwayFromZero));
                            decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (Math.Round(mCuotas, 0, MidpointRounding.AwayFromZero) * (mPagos - 1));
                            txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
                        }
                        else
                        {
                            string[] mCuotasString = mCuotas.ToString().Split(new string[] { "." }, StringSplitOptions.None);

                            Int32 mCuota = 1 + Convert.ToInt32(mCuotasString[0]);
                            decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (mCuota * (mPagos - 1));

                            txtPagos1.Text = String.Format("{0:0,0.00}", mCuota);
                            txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
                        }
                        #endregion
                    }
                }

                lbInfo.Text = mMensaje;
            }
            catch (Exception ex)
            {
                lbError.Text = CatchClass.ExMessage(ex, "pedidos.aspx", "lkAccesorios_Click");
            }
        }
        /// <summary>
        /// Este botón permite imprimir una carta con descuento aplicable
        /// para ventas con crediplus exclusivamente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCuponCrediplus_Click(object sender, EventArgs e)
        {
            /*
             * Condiciones:
             * •	Cupón
                La idea principal es que cada cliente obtenga un cupón de Q.300.00 el cual podrá ser aplicado a su préstamo, y las condiciones son:
                	Creditos mayores o iguales a Q.4,000.00
                	Lo puede aplicar a partir de la primera cuota pagada
                	La cuota debe ser pagada en tiempo
                	El periodo de la promoción es en créditos facturados hasta el 31.08.2019
                	Se entregará una carta al cliente la cual deberá ser presentada con el primer pago en tiendas de Muebles Fiesta
                	El cupón será aplicado en Phoenix como ajustes a los intereses.
                	Solo en plazos de 3 hasta 12 meses
             */
            if (validacionesCuponCrediplus())
            {
                string mReportServer = WebConfigurationManager.AppSettings["ReportServer"].ToString();
                if (Convert.ToString(Session["Ambiente"]) == "DES" || Convert.ToString(Session["Ambiente"]) == "PRU") mReportServer = WebConfigurationManager.AppSettings["ReportServerPruebas"].ToString();
                Response.Redirect(string.Format("{0}/Report/CuponCrediplus?pedido={1}&credito={2}", mReportServer, txtPedido.Text.ToUpper().Trim(),txtSolicitud.Text.Trim()), false);
            }
            
        }
        private bool validacionesCuponCrediplus()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU")
                ws.Url = Convert.ToString(ViewState["url"]);
            try
            {
                //1) que sea de crediplus
                if (!cbFinanciera.SelectedValue.Equals("12"))
                {
                    lbError.Text = "El cupón aplica solo a CREDIPLUS.";
                    return false;
                }
                //2) que sea el crédito de 3-12 meses
                if (!(cbNivelPrecio.SelectedValue.Equals("Crediplus03M") || cbNivelPrecio.SelectedValue.Equals("Crediplus06M") || cbNivelPrecio.SelectedValue.Equals("Crediplus09M") || cbNivelPrecio.SelectedValue.Equals("Crediplus12M")))
                {
                    lbError.Text = "El cupón aplica para créditos de hasta 12 meses.";
                    return false;
                }
                //3) que haya un pedido
                if (txtPedido.Text.Equals("P000000"))
                {
                    lbError.Text = "No hay pedido guardado";
                    return false;
                }
                //4) que el saldo a financiar sea igual o mayor a Q4,000
                if (decimal.Parse(txtSaldoFinanciar.Text)<4000)
                {
                    lbError.Text = "El saldo a financiar debe ser mayor o igual a Q4,000.00";
                    return false;
                }
                //5) que el pedido esté facturado
                if (!ws.PedidoFacturado(txtPedido.Text))
                {
                    lbError.Text = "El pedido no ha sido facturado";
                    return false;
                }
                //6) periodo de vigencia desde
                try
                {
                    DateTime dtFechaHasta = new DateTime(2019, 8, 31);
                    DateTime dtFechaDesde = new DateTime(2019, 7, 18);
                    DateTime dtFechaPedido = DateTime.Parse(txtFecha.Text);
                    if (dtFechaPedido > dtFechaHasta)
                    {
                        lbError.Text = "La vigencia de la promoción con Crediplus ha expirado. (31/08/2019)";
                        return false;
                    }
                    //7) periodo de vigencia hasta
                    if (dtFechaPedido < dtFechaDesde)
                    {
                        lbError.Text = "La vigencia de la promoción con Crediplus es a partir del 18/07/2019";
                        return false;
                    }
                }
                catch
                { }
                return true;
            } catch (Exception ex)
            {
                log.Error("Problema en las validaciones para el cupón de CREDIPLUS "+ex.Message);
                return false;
            }
        }

        protected void txtDescuentoItem_TextChanged(object sender, EventArgs e)
        {
            lbError2.Text = "";
            lbError2.Visible = false;
            bool modificarPrecios = PuedeModificarPrecios();
            if (!modificarPrecios)
            {
                decimal descMax = decimal.Parse(txtSaldoDescuento.Text.Replace(",", ""));
                decimal descuento = 0;
                try
                {
                    descuento = decimal.Parse(txtDescuentoItem.Text.Replace(",", ""));
                }
                catch
                {
                    descuento = 0;
                   
                }
                if (descuento > (descMax))
                {
                    lbError2.Visible = true;
                    lbError2.Text = "El valor del descuento no puede ser mayor a Q" + txtDescMax.Text;
                    txtDescuentoItem.Text = "0.00";
                    txtDescuentoItem.Focus();
                    return;
                }
                if (descuento < 0)
                {
                    lbError2.Visible = true;
                    lbError2.Text = "El valor del descuento no puede ser menor a cero.";
                    txtDescuentoItem.Focus();
                    return;
                }
            }
            List<NuevoPrecioProducto> lstProd = (List<NuevoPrecioProducto>)Session["lstNuevoPrecioProducto"];
            if ( lstProd.Count > 0)
            {
                lstProd[lstProd.Count - 1].price=decimal.Parse(txtTotal.Text.Replace(",",""))- (modificarPrecios ? 0 : decimal.Parse(txtDescuentoItem.Text.Replace(",","")));
                lstProd[lstProd.Count - 1] = Facturacion.ValidarPorcentajeDescuento(lstProd[lstProd.Count - 1]);
                Session["lstNuevoPrecioProducto"] = lstProd;
                txtSaldoDescuento.Text= String.Format("{0:0,0.00}", modificarPrecios ? 0 : Facturacion.ObtenerSaldoDescuento(lstProd));

            }
        }

        protected void btnValidarValeDscto_Click(object sender, EventArgs e)
        {
            lblInfoVales.Text = "";
            lblErrorVales.Text = "";
            decimal TotalVales = 0;
            //validar la existencia y disponibilidad del vale
            decimal valesValidar = 0;
            if (!decimal.TryParse(txtDescuentosVales.Text, out valesValidar))
            {
                txtDescuentosVales.Text = "0";
            }
            if (txtCodVale.Text.Trim() != string.Empty)
            {

                if (lstVales.Items.FindByText(txtCodVale.Text.Trim()) == null)
                {

                    lstVales.Items.Add(new ListItem { Text = txtCodVale.Text, Value = txtValorVale.Text });
                    if (lstVales.Items.Count > 0)
                        lstVales.Visible = true;
                    txtValorVale.Text = string.Empty;
                    txtCodVale.Text = string.Empty;
                    txtCodVale.Focus();
                }
            }
            else
            {
                lblErrorVales.Text = "No hay nada para canjear";
            }
            if (lstVales.Items.Count > 0)
            {
                lstVales.Visible = true;
                ListItemCollection data = lstVales.Items;
                foreach (var i in data)
                {
                    ListItem a = (ListItem)i;
                    decimal ValorVale = 0;
                    if (decimal.TryParse(a.Value, out ValorVale))
                        TotalVales += decimal.Parse(a.Value);
                    else
                        lblErrorVales.Text = $"No se puede validar el valor del vale {a.Value}, intente nuevamente e ingrese un monto entero (100,500 o 1000).";

                }
                if (decimal.Parse(txtDescuentosVales.Text) == TotalVales)
                {
                    tblFactura.Visible = true;
                    lkFacturar.Visible = true;
                }
                else if (lblErrorVales.Text.Trim() == string.Empty)
                {
                    lblInfoVales.Text = "Hace falta " + String.Format("{0:0,0.00}", decimal.Parse(txtDescuentosVales.Text) - TotalVales) + " en vales para proceder a facturar.";
                }
            }
          
        }

        protected void lkCanjeVales_Click(object sender, EventArgs e)
        {
            lblInfoVales.Text = "";
            tblVale.Visible = true;
            lstVales.Items.Clear();
            lkCanjeVales.Visible = false;
            tblFactura.Visible = false;
        }

        protected void txtDescuentosVales_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bool blOperar = true;
                decimal montoVale2 = 0;
                try
                {
                    montoVale2 = decimal.Parse(txtDescuentosVales2.Text.Trim().Replace(",", ""));
                }
                catch
                {
                    montoVale2 = 0;
                }
                decimal montoVale = 0;
                try
                {
                    montoVale = decimal.Parse(txtDescuentosVales.Text.Trim().Replace(",", ""));
                    var montoAux = 0;
                    int.TryParse(txtDescuentosVales.Text.Trim().Replace(",", ""), out montoAux);
                    if (montoAux != montoVale)
                    {
                        lbError.Text="El valor debe ser entero, no se aceptan decimales para los vales.";
                        return;
                    }
                    if (montoVale > decimal.Parse(txtFacturar.Text.Replace(",", "")))
                         {
                        lbError.Text = "No puede aplicar un vale mayor que el monto de la factura";
                        return;
                    }
                        
                }
                catch
                {
                    montoVale = 0;
                    txtDescuentosVales.Focus();
                    blOperar = false;
                }
                if (montoVale >= 0 && blOperar)
                {
                    wsPuntoVenta.PedidoLinea[] q = new wsPuntoVenta.PedidoLinea[1];
                    wsPuntoVenta.PedidoLinea[] qLinea = new wsPuntoVenta.PedidoLinea[1];
                    List<wsPuntoVenta.PedidoLinea> qqLinea = new List<wsPuntoVenta.PedidoLinea>();
                    List<PedidoLinea> articulosPL = new List<PedidoLinea>();
                    int jj = 0;
                    for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
                    {
                        jj++;

                        Label lbPrecioUnitario = (Label)gridArticulos.Rows[ii].FindControl("lbPrecio");
                        Label lbCantidadPedida = (Label)gridArticulos.Rows[ii].FindControl("lbCantidad");
                        Label lbPrecioTotal = (Label)gridArticulos.Rows[ii].FindControl("lbTotal");
                        Label lbDescripcion = (Label)gridArticulos.Rows[ii].FindControl("lbDescripcion");
                        Label lbComentario = (Label)gridArticulos.Rows[ii].FindControl("lbComentario");
                        Label lbEstado = (Label)gridArticulos.Rows[ii].FindControl("lbEstado");
                        Label lbNumeroPedido = (Label)gridArticulos.Rows[ii].FindControl("lbNumeroPedido");
                        Label lbOferta = (Label)gridArticulos.Rows[ii].FindControl("lbOferta");
                        Label lbFechaOfertaDesde = (Label)gridArticulos.Rows[ii].FindControl("lbFechaOfertaDesde");
                        Label lbPrecioOriginal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioOriginal");
                        Label lbEsDetalleKit = (Label)gridArticulos.Rows[ii].FindControl("lbEsDetalleKit");
                        Label lbPrecioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioFacturar");
                        Label lbTipoOferta = (Label)gridArticulos.Rows[ii].FindControl("lbTipoOferta");
                        Label lbVale = (Label)gridArticulos.Rows[ii].FindControl("lbVale");
                        Label lbAutorizacion = (Label)gridArticulos.Rows[ii].FindControl("lbAutorizacion");
                        Label lbBodega = (Label)gridArticulos.Rows[ii].FindControl("lbBodega");
                        Label lbLocalizacion = (Label)gridArticulos.Rows[ii].FindControl("lbLocalizacion");
                        LinkButton lkRequisicionArmado = (LinkButton)gridArticulos.Rows[ii].FindControl("lkRequisicionArmado");
                        Label lbArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbArticulo");
                        Label lbNombreArticulo = (Label)gridArticulos.Rows[ii].FindControl("lbNombreArticulo");
                        Label lbPrecioUnitarioDebioFacturar = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioUnitarioDebioFacturar");
                        Label lbPrecioSugerido = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioSugerido");
                        Label lbPrecioBaseLocal = (Label)gridArticulos.Rows[ii].FindControl("lbPrecioBaseLocal");
                        Label lbGel = (Label)gridArticulos.Rows[ii].FindControl("lbGel");
                        Label lbLinea = (Label)gridArticulos.Rows[ii].FindControl("lbLinea");
                        Label lbPorcentajeDescuento = (Label)gridArticulos.Rows[ii].FindControl("lbPorcentajeDescuento");
                        Label lbBeneficiario = (Label)gridArticulos.Rows[ii].FindControl("lbBeneficiario");
                        Label lbDescuento = (Label)gridArticulos.Rows[ii].FindControl("lbDescuento");
                        decimal mPrecioOriginal = 0;
                        DateTime mFechaOfertaDesde = new DateTime(1980, 1, 1);

                        try
                        {
                            mFechaOfertaDesde = new DateTime(Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(6, 4)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(3, 2)), Convert.ToInt32(lbFechaOfertaDesde.Text.Substring(0, 2)));
                        }
                        catch
                        {
                            //Nothing
                        }
                        try
                        {
                            mPrecioOriginal = Convert.ToDecimal(lbPrecioOriginal.Text.Replace(",", ""));
                        }
                        catch
                        {
                            mPrecioOriginal = 0;
                        }


                        wsPuntoVenta.PedidoLinea linea = new wsPuntoVenta.PedidoLinea
                        {
                            Articulo = lbArticulo.Text,
                            Nombre = lbNombreArticulo.Text,
                            PrecioUnitario = Convert.ToDecimal(lbPrecioUnitario.Text.Replace(",", "")),
                            CantidadPedida = Convert.ToDecimal(lbCantidadPedida.Text.Replace(",", "")),
                            PrecioTotal = Convert.ToDecimal(lbPrecioTotal.Text.Replace(",", "")),
                            PrecioFacturar = Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", "")),
                            Bodega = lbBodega.Text,
                            Localizacion = lbLocalizacion.Text,
                            RequisicionArmado = lkRequisicionArmado.Text,
                            Descripcion = lbDescripcion.Text,
                            Comentario = lbComentario.Text,
                            Estado = lbEstado.Text,
                            NumeroPedido = lbNumeroPedido.Text,
                            Oferta = lbOferta.Text,
                            FechaOfertaDesde = mFechaOfertaDesde,
                            PrecioOriginal = mPrecioOriginal,
                            TipoOferta = lbTipoOferta.Text,
                            EsDetalleKit = lbEsDetalleKit.Text,
                            Vale = Convert.ToInt32(lbVale.Text == "" ? "0" : lbVale.Text),
                            Autorizacion = Convert.ToInt64(lbAutorizacion.Text == "" ? "0" : lbAutorizacion.Text),
                            PrecioUnitarioDebioFacturar = Convert.ToDecimal(lbPrecioUnitarioDebioFacturar.Text.Replace(",", "")),
                            PrecioSugerido = Convert.ToDecimal(lbPrecioSugerido.Text.Replace(",", "")),
                            PrecioBaseLocal = Convert.ToDouble(lbPrecioBaseLocal.Text.Replace(",", "")),
                            Gel = lbGel.Text,
                            Linea = Convert.ToInt32(lbLinea.Text),
                            PorcentajeDescuento = Convert.ToDecimal(lbPorcentajeDescuento.Text),
                            Beneficiario = lbBeneficiario.Text,
                            Descuento = Convert.ToDecimal(lbDescuento.Text)
                        };
                        PedidoLinea plinea = new PedidoLinea
                        {
                            Articulo = lbArticulo.Text,
                            Nombre = lbNombreArticulo.Text,
                            PrecioUnitario = Convert.ToDecimal(lbPrecioUnitario.Text.Replace(",", "")),
                            CantidadPedida = Convert.ToDecimal(lbCantidadPedida.Text.Replace(",", "")),
                            PrecioTotal = Convert.ToDecimal(lbPrecioTotal.Text.Replace(",", "")),
                            PrecioFacturar = Convert.ToDecimal(lbPrecioFacturar.Text.Replace(",", "")),
                            Bodega = lbBodega.Text,
                            Localizacion = lbLocalizacion.Text,
                            RequisicionArmado = lkRequisicionArmado.Text,
                            Descripcion = lbDescripcion.Text,
                            Comentario = lbComentario.Text,
                            Estado = lbEstado.Text,
                            NumeroPedido = lbNumeroPedido.Text,
                            Oferta = lbOferta.Text,
                            FechaOfertaDesde = mFechaOfertaDesde,
                            PrecioOriginal = mPrecioOriginal,
                            TipoOferta = lbTipoOferta.Text,
                            EsDetalleKit = lbEsDetalleKit.Text,
                            Vale = Convert.ToInt32(lbVale.Text == "" ? "0" : lbVale.Text),
                            Autorizacion = Convert.ToInt64(lbAutorizacion.Text == "" ? "0" : lbAutorizacion.Text),
                            PrecioUnitarioDebioFacturar = Convert.ToDecimal(lbPrecioUnitarioDebioFacturar.Text.Replace(",", "")),
                            PrecioSugerido = Convert.ToDecimal(lbPrecioSugerido.Text.Replace(",", "")),
                            PrecioBaseLocal = Convert.ToDouble(lbPrecioBaseLocal.Text.Replace(",", "")),
                            Gel = lbGel.Text,
                            Linea = Convert.ToInt32(lbLinea.Text),
                            PorcentajeDescuento = Convert.ToDecimal(lbPorcentajeDescuento.Text) - (montoVale2 > 0 && Convert.ToDecimal(txtFacturar.Text.Replace(",", "")) > 0 ? Math.Round(montoVale2 / Convert.ToDecimal(txtFacturar.Text.Replace(",", "")) * 100,2,MidpointRounding.AwayFromZero) : 0),
                            Beneficiario = lbBeneficiario.Text,
                            Descuento = Convert.ToDecimal(lbDescuento.Text)
                        };
                        qqLinea.Add(linea);
                        articulosPL.Add(plinea);




                    }

                    string msgError = string.Empty;
                    txtDescuentos.Text = string.Format("{0:0,0.00}", decimal.Parse(txtDescuentos.Text.Replace("Q", "").Replace(",", "")) -montoVale2>=0? decimal.Parse(txtDescuentos.Text.Replace("Q", "").Replace(",", "")) - montoVale2:0);
                  
                    

                    MF_Clases.Comun.PromocionRegistro pedido = new MF_Clases.Comun.PromocionRegistro
                    {
                        articulos = articulosPL,
                        Descuento = decimal.Parse(txtDescuentos.Text.Replace("Q", "").Replace(",", "")),
                        DsctoVales = decimal.Parse(txtDescuentosVales.Text.Replace("Q", "").Replace(",", "")),
                        Financiera = int.Parse(cbFinanciera.SelectedValue.ToString()),
                        TotalFacturar = decimal.Parse(txtFacturar.Text.Replace("Q", "").Replace(",", "")),
                        SaldoFinanciar = decimal.Parse(txtSaldoFinanciar.Text.Replace("Q", "").Replace(",", "")),
                        Monto = decimal.Parse(txtMonto.Text.Replace("Q", "").Replace(",", "")),
                        nivelPrecio = cbNivelPrecio.SelectedValue

                    };
                    var ValeCalculado = Facturacion.CalcularDescuentoVales(pedido, ref msgError);
                    if (msgError != string.Empty)
                        lbError2.Text = msgError;
                    else
                    {
                        foreach (var item in ValeCalculado.articulos)
                        {

                            qqLinea.Find(x => x.Linea == item.Linea).PrecioTotal = item.PrecioTotal;
                            qqLinea.Find(x => x.Linea == item.Linea).Descuento = item.Descuento;
                            qqLinea.Find(x => x.Linea == item.Linea).PorcentajeDescuento = item.PorcentajeDescuento;
                            qqLinea.Find(x => x.Linea == item.Linea).NetoFacturar = item.NetoFacturar;
                            qqLinea.Find(x => x.Linea == item.Linea).PrecioTotal = item.PrecioTotal;
                            qqLinea.Find(x => x.Linea == item.Linea).PrecioFacturar = item.PrecioFacturar;

                        };

                        gridArticulos.DataSource = qqLinea;
                        gridArticulos.DataBind();

                        ViewState["qArticulos"] = qqLinea;
                        if (!ValeCalculado.EsBanco)
                        {
                            txtMonto.Text = String.Format("{0:0,0.00}", ValeCalculado.Monto.ToString());
                            txtDescuentos.Text = String.Format("{0:0,0.00}", ValeCalculado.Descuento);
                            txtDescuentosVales2.Text = txtDescuentosVales.Text;

                        }
                        else
                        {
                            wsPuntoVenta.wsPuntoVenta ws = new wsPuntoVenta.wsPuntoVenta();
                            if (General.Ambiente == "PRO" || General.Ambiente == "PRU")
                                ws.Url = Convert.ToString(ViewState["url"]);
                            double mFactor = ws.DevuelveFactorNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);
                            decimal mEnganche = Convert.ToDecimal(txtEnganche.Text.Trim().Replace(",", ""));
                            decimal mSaldoFinanciar = ValeCalculado.TotalFacturar - mEnganche - ValeCalculado.Descuento ;
                            decimal mMontoTotal = Convert.ToDecimal(Math.Round((double)mSaldoFinanciar * mFactor, 2, MidpointRounding.AwayFromZero));
                            decimal mRecargos = mMontoTotal - mSaldoFinanciar;

                            
                            txtSaldoFinanciar.Text = String.Format("{0:0,0.00}", mSaldoFinanciar);
                            txtIntereses.Text = String.Format("{0:0,0.00}", mRecargos);
                            txtMonto.Text = String.Format("{0:0,0.00}", mMontoTotal);
                            txtDescuentos.Text = String.Format("{0:0,0.00}", ValeCalculado.Descuento);
                            txtDescuentosVales2.Text = txtDescuentosVales.Text;

                            Int32 mPagos;
                            string mPagos1;
                            string mPagosTexto = ws.DevuelvePagosNivelPrecio(cbTipo.SelectedValue, cbFinanciera.SelectedValue, cbNivelPrecio.SelectedValue);

                            try
                            {
                                mPagos = Convert.ToInt32(mPagosTexto.Substring(0, 2).Replace(" ", ""));
                            }
                            catch
                            {
                                mPagos = 1;
                            }

                            decimal mCuotas = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) / mPagos;

                            if (mPagos <= 10)
                            {
                                mPagos1 = string.Format("0{0}", mPagos - 1);
                            }
                            else
                            {
                                mPagos1 = (mPagos - 1).ToString();
                            }

                            cbPagos1.SelectedValue = mPagos1;
                            cbPagos2.SelectedValue = "01";

                            if (mCuotas.ToString().Substring(mCuotas.ToString().Length - 2, 2) == "00")
                            {
                                txtPagos1.Text = String.Format("{0:0,0.00}", mCuotas);
                                txtPagos2.Text = String.Format("{0:0,0.00}", mCuotas);
                            }
                            else
                            {
                                #region "aproximaciones para imprimir documentos de financieras"
                                if (cbFinanciera.SelectedValue == Const.FINANCIERA.BANCREDIT)
                                {
                                    txtPagos1.Text = String.Format("{0:0,0.00}", Math.Round(mCuotas, 0, MidpointRounding.AwayFromZero));
                                    decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (Math.Round(mCuotas, 0, MidpointRounding.AwayFromZero) * (mPagos - 1));
                                    txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
                                }
                                else
                                {
                                    string[] mCuotasString = mCuotas.ToString().Split(new string[] { "." }, StringSplitOptions.None);

                                    Int32 mCuota = 1 + Convert.ToInt32(mCuotasString[0]);
                                    decimal mSaldo = Convert.ToDecimal(txtMonto.Text.Replace(",", "")) - (mCuota * (mPagos - 1));

                                    txtPagos1.Text = String.Format("{0:0,0.00}", mCuota);
                                    txtPagos2.Text = String.Format("{0:0,0.00}", mSaldo);
                                }
                                #endregion
                            }
                        }
                    }
                }
                

            } catch (Exception ex)
            {
                lbError2.Text = ex.Message;
            }

        }
    }
}

