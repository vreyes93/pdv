﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="perfil.aspx.cs" Inherits="PuntoDeVenta.perfil" %>
<%@ Register Src="ToolbarExport.ascx" TagName="ToolbarExport" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v16.2, Version=16.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1000
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>&nbsp;&nbsp;Perfil de Vendedor</h3>
    <div class="content2">
        <div class="clientes">
            <ul>
                <li>
                    
                    <table class="style1000">
                        <tr>
                            <td colspan="3">
                                <a>Datos Generales</a></td>
                            <td colspan="3" style="text-align: right">
                                <asp:Label ID="lbInfoGenerales" runat="server"></asp:Label>
                                <asp:Label ID="lbErrorGenerales" runat="server" Font-Bold="True" 
                                    ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                Número de
                                Celular:</td>
                            <td>
                                <asp:TextBox ID="txtCelular" runat="server" Width="150px" TabIndex="10"></asp:TextBox>
                            </td>
                            <td>
                                Correo:</td>
                            <td>
                                <asp:TextBox ID="txtCorreo" runat="server" Width="295px" TabIndex="20" style="text-transform: lowercase;"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Código Interconsumo:</td>
                            <td>
                                <asp:TextBox ID="txtCodigoInterconsumo" runat="server" Width="150px" 
                                    TabIndex="25" ToolTip="Este es su código en Interconsumo"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Texto en cotizaciones:</td>
                            <td colspan="3" rowspan="2">
                                <asp:TextBox ID="txtTextoCotizaciones" runat="server" Width="501px" style="resize:none;" 
                                    MaxLength="600" TabIndex="30" TextMode="MultiLine"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td colspan="4" style="text-align: center">
                                <asp:LinkButton ID="lkGrabarGenerales" runat="server" onclick="lkGrabarGenerales_Click" TabIndex="40" 
                                    ToolTip="Haga clic aquí para grabar sus datos generales">Grabar</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                </li>
            </ul>

            <ul>
                <li>
                    <table>
                        <tr>
                            <td colspan="3">
                                <a>Comisiones</a></td>
                            <td colspan="3" style="text-align: right">
                                <asp:Label ID="lbInfoComisiones" runat="server"></asp:Label>
                                <asp:Label ID="lbErrorComisiones" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>
                                Vendedor:
                            </td>
                            <td>
                                <dx:ASPxComboBox ID="tiendaComisiones" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle" 
                                    ValueType="System.String" Width="60px" DropDownStyle="DropDownList" 
                                    DropDownWidth="40" DropDownRows="25" TabIndex="80" Visible="true" 
                                    AutoPostBack="True" 
                                    onselectedindexchanged="tiendaComisiones_SelectedIndexChanged">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <dx:ASPxComboBox ID="vendedorComisiones" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle" 
                                    ValueType="System.String" Width="180px" DropDownStyle="DropDownList" 
                                    DropDownWidth="40" DropDownRows="25" TabIndex="90" Visible="true">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkComisiones" runat="server" ToolTip="Haga clic aquí para consultar las comisiones" TabIndex="100" onclick="lkComisiones_Click">Consultar</asp:LinkButton>
                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                <asp:LinkButton ID="lkComisionesTienda" runat="server" ToolTip="Haga clic aquí para consultar las comisiones de la tienda" TabIndex="102" onclick="lkComisionesTienda_Click" Visible="false">Tienda</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridComisiones" EnableTheming="True" Theme="SoftOrange" 
                                    runat="server" KeyFieldName="Vendedor" AutoGenerateColumns="False" 
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="280" 
                                    Visible="False" onhtmlrowprepared="gridComisiones_HtmlRowPrepared" 
                                    onhtmldatacellprepared="gridComisiones_HtmlDataCellPrepared" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                    CommandBatchEditUpdate="Aplicar cambios" 
                                    ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                    CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                    SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="true" Width="75px" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Vendedor" ReadOnly="true" Caption="Vendedor" Visible="true" Width="50px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreVendedor" ReadOnly="true" Caption="Nombre del vendedor" Visible="true" Width="260px" >
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" Caption="Valor" Visible="true" Width="110px" ToolTip="Valor total de las facturas" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MontoComisionable" ReadOnly="true" Caption="Comisionable" Visible="true" Width="110px" ToolTip="Monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ValorNeto" ReadOnly="true" Caption="Valor Neto" Visible="true" Width="110px" ToolTip="Valor neto del monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="true" Width="110px" ToolTip="Comisión a recibir" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Porcentaje" ReadOnly="true" Caption="Porcentaje" Visible="true" Width="75px" ToolTip="Porcentaje" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Titulo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Tooltip" Visible="false" Caption="Información">
                                            <CellStyle Wrap="False"></CellStyle>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="500" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
<%--                                    <TotalSummary>
                                        <dx:ASPxSummaryItem FieldName="NombreVendedor" SummaryType="Count" DisplayFormat="Total general: {0}" />
                                        <dx:ASPxSummaryItem FieldName="Valor" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="MontoComisionable" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ValorNeto" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </TotalSummary>--%>
                                    <GroupSummary>
                                        <dx:ASPxSummaryItem FieldName="NombreVendedor" ShowInGroupFooterColumn="NombreVendedor" SummaryType="Count" DisplayFormat="Cantidad vendedores: {0}" />
<%--                                        <dx:ASPxSummaryItem FieldName="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="MontoComisionable" ShowInGroupFooterColumn="MontoComisionable" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ValorNeto" ShowInGroupFooterColumn="ValorNeto" SummaryType="Sum" DisplayFormat="{0:n2}" />--%>
                                        <dx:ASPxSummaryItem FieldName="Comision" ShowInGroupFooterColumn="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </GroupSummary>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridTiendas" EnableTheming="True" Theme="SoftOrange" 
                                    runat="server" KeyFieldName="Tienda" AutoGenerateColumns="False" 
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="285" 
                                    Visible="False" onhtmlrowprepared="gridTiendas_HtmlRowPrepared" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                    CommandBatchEditUpdate="Aplicar cambios" 
                                    ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                    CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                    SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="true" Width="100px" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" Caption="Valor" Visible="true" Width="200px" ToolTip="Valor total de las facturas" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MontoComisionable" ReadOnly="true" Caption="Comisionable" Visible="true" Width="200px" ToolTip="Monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ValorNeto" ReadOnly="true" Caption="Valor Neto" Visible="true" Width="200px" ToolTip="Valor neto del monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="true" Width="200px" ToolTip="Comisión a recibir" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Titulo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="500" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
<%--                                    <TotalSummary>
                                        <dx:ASPxSummaryItem FieldName="NombreVendedor" SummaryType="Count" DisplayFormat="Total general: {0}" />
                                        <dx:ASPxSummaryItem FieldName="Valor" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="MontoComisionable" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ValorNeto" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </TotalSummary>--%>
                                    <GroupSummary>
                                        <dx:ASPxSummaryItem FieldName="Tienda" ShowInGroupFooterColumn="Tienda" SummaryType="Count" DisplayFormat="Cantidad tiendas: {0}" />
                                        <dx:ASPxSummaryItem FieldName="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="MontoComisionable" ShowInGroupFooterColumn="MontoComisionable" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ValorNeto" ShowInGroupFooterColumn="ValorNeto" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="Comision" ShowInGroupFooterColumn="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </GroupSummary>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridFacturas" EnableTheming="True" Theme="SoftOrange" 
                                    runat="server" KeyFieldName="Factura" AutoGenerateColumns="False" 
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="290" 
                                    Visible="False" onhtmlrowprepared="gridFacturas_HtmlRowPrepared" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                    CommandBatchEditUpdate="Aplicar cambios" 
                                    ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                    CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                    SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Vendedor" ReadOnly="true" Caption="Vendedor" Visible="false" Width="50px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreVendedor" ReadOnly="true" Caption="Nombre del vendedor" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura" Visible="true" Width="240px" >
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" Caption="Valor" Visible="false" Width="10px" ToolTip="Valor total de las facturas" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MontoComisionable" ReadOnly="true" Caption="Comisionable" Visible="true" Width="100px" ToolTip="Monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ValorNeto" ReadOnly="true" Caption="Valor Neto" Visible="true" Width="100px" ToolTip="Valor neto del monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="true" Width="100px" ToolTip="Comisión a recibir" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Porcentaje" ReadOnly="true" Caption="Porcentaje" Visible="true" Width="60px" ToolTip="Porcentaje" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaFactura" ReadOnly="true" Caption="Fecha Factura" Visible="true" Width="70px">
                                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaEntrega" ReadOnly="true" Caption="Fecha Entrega" Visible="true" Width="70px">
                                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn FieldName="Titulo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="NivelPrecio" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PorcentajeTarjeta" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="15000" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                    <GroupSummary>
                                        <dx:ASPxSummaryItem FieldName="Factura" ShowInGroupFooterColumn="Factura" SummaryType="Count" DisplayFormat="Facturas: {0}" />
                                        <dx:ASPxSummaryItem FieldName="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="MontoComisionable" ShowInGroupFooterColumn="MontoComisionable" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ValorNeto" ShowInGroupFooterColumn="ValorNeto" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="Comision" ShowInGroupFooterColumn="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </GroupSummary>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>    
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridExcepciones" EnableTheming="True" Theme="SoftOrange" 
                                    runat="server" KeyFieldName="Factura" AutoGenerateColumns="False" 
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="300" 
                                    Visible="False" onhtmlrowprepared="gridExcepciones_HtmlRowPrepared" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                    CommandBatchEditUpdate="Aplicar cambios" 
                                    ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                    CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                    SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Vendedor" ReadOnly="true" Caption="Vendedor" Visible="true" Width="50px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreVendedor" ReadOnly="true" Caption="Nombre del vendedor" Visible="true" Width="140px" >
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura" Visible="true" Width="50px" >
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" Caption="Valor" Visible="true" Width="90px" ToolTip="Valor total de las facturas" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MontoComisionable" ReadOnly="true" Caption="Comisionable" Visible="true" Width="90px" ToolTip="Monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ValorNeto" ReadOnly="true" Caption="Valor Neto" Visible="true" Width="90px" ToolTip="Valor neto del monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="false" ToolTip="Comisión a recibir" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Porcentaje" ReadOnly="true" Caption="Porcentaje" Visible="false" ToolTip="Porcentaje" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaFactura" ReadOnly="true" Caption="Fecha Factura" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaEntrega" ReadOnly="true" Caption="Fecha Entrega" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn FieldName="Tipo" Visible="true" Width="390px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle Wrap="False"></CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Titulo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="15000" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                    <GroupSummary>
                                        <dx:ASPxSummaryItem FieldName="Factura" ShowInGroupFooterColumn="Factura" SummaryType="Count" DisplayFormat="Cantidad facturas: {0}" />
                                        <dx:ASPxSummaryItem FieldName="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="MontoComisionable" ShowInGroupFooterColumn="MontoComisionable" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ValorNeto" ShowInGroupFooterColumn="ValorNeto" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="Comision" ShowInGroupFooterColumn="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </GroupSummary>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>    
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridAnuladas" EnableTheming="True" Theme="SoftOrange" 
                                    runat="server" KeyFieldName="Factura" AutoGenerateColumns="False" 
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="310" 
                                    Visible="False" onhtmlrowprepared="gridAnuladas_HtmlRowPrepared" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                    CommandBatchEditUpdate="Aplicar cambios" 
                                    ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                    CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                    SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="Hidden" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Vendedor" ReadOnly="true" Caption="Vendedor" Visible="true" Width="50px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreVendedor" ReadOnly="true" Caption="Nombre del vendedor" Visible="true" Width="430px" >
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura" Visible="true" Width="270px" >
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" Caption="Valor" Visible="true" Width="100px" ToolTip="Valor total de las facturas" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MontoComisionable" ReadOnly="true" Caption="Comisionable" Visible="false" ToolTip="Monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ValorNeto" ReadOnly="true" Caption="Valor Neto" Visible="false" ToolTip="Valor neto del monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="false" ToolTip="Comisión a recibir" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Porcentaje" ReadOnly="true" Caption="Porcentaje" Visible="false" ToolTip="Porcentaje" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaFactura" ReadOnly="true" Caption="Fecha Factura" Visible="true" Width="70px">
                                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaEntrega" ReadOnly="true" Caption="Fecha Entrega" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn FieldName="Titulo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="15000" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>    
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridPendientes" EnableTheming="True" Theme="SoftOrange" 
                                    runat="server" KeyFieldName="Factura" AutoGenerateColumns="False" 
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="320" 
                                    Visible="False" onhtmlrowprepared="gridPendientes_HtmlRowPrepared" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                    CommandBatchEditUpdate="Aplicar cambios" 
                                    ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                    CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                    SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Vendedor" ReadOnly="true" Caption="Vendedor" Visible="false" Width="50px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreVendedor" ReadOnly="true" Caption="Nombre del vendedor" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura" Visible="true" Width="160px" >
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Tipo" ReadOnly="true" Caption="Tipo" Visible="true">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Estado" ReadOnly="true" Caption="Estado" Visible="true">
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" Caption="Valor" Visible="false" Width="100px" ToolTip="Valor total de las facturas" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MontoComisionable" ReadOnly="true" Caption="Comisionable" Visible="true" Width="80px" ToolTip="Monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ValorNeto" ReadOnly="true" Caption="Valor Neto" Visible="true" Width="80px" ToolTip="Valor neto del monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="false" ToolTip="Comisión a recibir" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Porcentaje" ReadOnly="true" Caption="Porcentaje" Visible="false" ToolTip="Porcentaje" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaFactura" ReadOnly="true" Caption="Fecha Factura" Visible="true" Width="70px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaEntrega" ReadOnly="true" Caption="Fecha Entrega" Visible="true" Width="70px" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn FieldName="Titulo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="15000" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                    <GroupSummary>
                                        <dx:ASPxSummaryItem FieldName="Factura" ShowInGroupFooterColumn="Factura" SummaryType="Count" DisplayFormat="Cantidad facturas: {0}" />
                                        <dx:ASPxSummaryItem FieldName="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="MontoComisionable" ShowInGroupFooterColumn="MontoComisionable" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ValorNeto" ShowInGroupFooterColumn="ValorNeto" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="Comision" ShowInGroupFooterColumn="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </GroupSummary>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>    
                            </td>
                        </tr>

                    </table>
                </li>
            </ul>

        </div>
        <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server" TabIndex="620"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" TabIndex="630"></asp:Label>
                </td>
            </tr>
        </table>
    </div>

</asp:Content>
