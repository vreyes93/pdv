﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.IO;
using System.Net;
using DevExpress.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MF_Clases;

namespace PuntoDeVenta
{
    public partial class perfil : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                ViewState["url"] = Convert.ToString(Session["Url"]);
                CargarGenerales();
                CargarBodegas();
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                Clases.RetornaComisiones mRetorna = new Clases.RetornaComisiones();
                mRetorna = (Clases.RetornaComisiones)Session["Comisiones"];

                gridComisiones.DataSource = mRetorna.Comisiones.AsEnumerable();
                gridComisiones.DataMember = "comisiones";
                gridComisiones.DataBind();
                gridComisiones.FocusedRowIndex = -1;
                gridComisiones.SettingsPager.PageSize = 500;

                gridTiendas.DataSource = mRetorna.Tiendas.AsEnumerable();
                gridTiendas.DataMember = "tiendas";
                gridTiendas.DataBind();
                gridTiendas.FocusedRowIndex = -1;
                gridTiendas.SettingsPager.PageSize = 500;

                gridFacturas.DataSource = mRetorna.Facturas.AsEnumerable();
                gridFacturas.DataMember = "facturas";
                gridFacturas.DataBind();
                gridFacturas.FocusedRowIndex = -1;
                gridFacturas.SettingsPager.PageSize = 15000;

                gridExcepciones.DataSource = mRetorna.Excepciones.AsEnumerable();
                gridExcepciones.DataMember = "excepciones";
                gridExcepciones.DataBind();
                gridExcepciones.FocusedRowIndex = -1;
                gridExcepciones.SettingsPager.PageSize = 500;

                gridAnuladas.DataSource = mRetorna.Anuladas.AsEnumerable();
                gridAnuladas.DataMember = "anuladas";
                gridAnuladas.DataBind();
                gridAnuladas.FocusedRowIndex = -1;
                gridAnuladas.SettingsPager.PageSize = 500;

                gridPendientes.DataSource = mRetorna.Pendientes.AsEnumerable();
                gridPendientes.DataMember = "pendientes";
                gridPendientes.DataBind();
                gridPendientes.FocusedRowIndex = -1;
                gridPendientes.SettingsPager.PageSize = 15000;
            }
            catch{}
        }

        void CargarGenerales()
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                lbInfoGenerales.Text = "";
                lbErrorGenerales.Text = "";

                txtCodigoInterconsumo.Text = "";

                var q = ws.DevuelveVendedorGenerales(Convert.ToString(Session["Vendedor"])).ToList();

                txtCelular.Text = q[0].Celular;
                txtCorreo.Text = q[0].Correo;
                txtTextoCotizaciones.Text = q[0].TextoCotizacion;
                txtCodigoInterconsumo.Text = q[0].CodigoInterconsumo;

                txtCelular.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    //Nothing
                }
                lbErrorGenerales.Text = string.Format("Error al cargar sus datos generales {0} {1}", ex.Message, m);
            }
        }

        void CargarBodegas()
        {
            try
            {
                lbError.Text = "";
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                var q = ws.DevuelveTiendas();

                tiendaComisiones.DataSource = q;
                tiendaComisiones.ValueField = "Tienda";
                tiendaComisiones.ValueType = typeof(System.String);
                tiendaComisiones.TextField = "Tienda";
                tiendaComisiones.DataBindItems();

                tiendaComisiones.Value = Convert.ToString(Session["Tienda"]);
                CargaVendedores();

                WebRequest request = WebRequest.Create(string.Format("{0}/issupervisor/?usuario={1}", Convert.ToString(Session["UrlRestServices"]), Convert.ToString(Session["Usuario"])));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                dynamic mValida = JObject.Parse(responseString);

                lkComisionesTienda.Visible = false;
                tiendaComisiones.ReadOnly = true;
                vendedorComisiones.ReadOnly = true;

                if (Convert.ToBoolean(mValida.isSupervisor))
                {
                    lkComisionesTienda.Visible = true;
                    tiendaComisiones.ReadOnly = false;
                    vendedorComisiones.ReadOnly = false;                    
                }

                request = WebRequest.Create(string.Format("{0}/isJefe/?usuario={1}", Convert.ToString(Session["UrlRestServices"]), Convert.ToString(Session["Usuario"])));

                request.Method = "GET";
                request.ContentType = "application/json";

                response = (HttpWebResponse)request.GetResponse();
                responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                dynamic mValidaJefe = JObject.Parse(responseString);

                if (Convert.ToBoolean(mValidaJefe.isJefe))
                {
                    lkComisionesTienda.Visible = true;
                    vendedorComisiones.ReadOnly = false;
                }
            }
            catch (Exception ex)
            {
                lbError.Text = CatchClass.ExMessage(ex, "perfil", "CargarBodegas");
            }
        }

        void CargaVendedores()
        {
            try
            {
                lbError.Text = "";
                vendedorComisiones.Value = null;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var q = ws.DevuelveVendedoresTienda(Convert.ToString(tiendaComisiones.Value)).ToList();

                if (q.Count() == 0)
                {
                    lbError.Text = "Tienda inválida.";
                }

                vendedorComisiones.DataSource = q;
                vendedorComisiones.ValueField = "Vendedor";
                vendedorComisiones.ValueType = typeof(System.String);
                vendedorComisiones.TextField = "Nombre";
                vendedorComisiones.DataBindItems();

                bool mEncontro = false;
                foreach (var item in q)
                {
                    if (item.Vendedor == Convert.ToString(Session["Vendedor"]))
                    {
                        mEncontro = true;
                        vendedorComisiones.Value = Session["Vendedor"];
                    }
                }

                if (!mEncontro)
                {
                    vendedorComisiones.Value = q[0].Vendedor;
                }
            }
            catch (Exception ex)
            {
                lbError.Text = CatchClass.ExMessage(ex, "perfil", "CargaVendedores");
            }
        }


        protected void lkGrabarGenerales_Click(object sender, EventArgs e)
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                lbInfoGenerales.Text = "";
                lbErrorGenerales.Text = "";

                if (txtCorreo.Text.Trim().Length == 0)
                {
                    lbErrorGenerales.Text = "Debe ingresar su correo electrónico";
                    txtCorreo.Focus();
                    return;
                }
                if (!txtCorreo.Text.Contains("@mueblesfiesta.com"))
                {
                    lbErrorGenerales.Text = "El correo electrónico ingresado es inválido";
                    txtCorreo.Focus();
                    return;
                }

                string mMensaje = "";
                wsPuntoVenta.VendedorGenerales[] q = new wsPuntoVenta.VendedorGenerales[1];

                wsPuntoVenta.VendedorGenerales item = new wsPuntoVenta.VendedorGenerales();
                item.Celular = txtCelular.Text.Trim();
                item.Correo = txtCorreo.Text.Trim().ToLower();
                item.TextoCotizacion = txtTextoCotizaciones.Text.Trim();
                item.Vendedor = Convert.ToString(Session["Vendedor"]);
                item.CodigoInterconsumo = txtCodigoInterconsumo.Text.Trim();
                q[0] = item;

                if (!ws.GrabarVendedorGenerales(q, ref mMensaje))
                {
                    lbErrorGenerales.Text = mMensaje;
                    return;
                }

                CargarGenerales();
                lbInfoGenerales.Text = mMensaje;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    //Nothing
                }
                lbErrorGenerales.Text = string.Format("Error al grabar sus datos generales {0} {1}", ex.Message, m);
            }
        }

        protected void lkFacturas_Click(object sender, EventArgs e)
        {

        }

        protected void gridFacturas_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gridFacturas_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        void CargarComisiones(string tienda, string vendedor)
        {
            try
            {
                lbInfoComisiones.Text = "";
                lbErrorComisiones.Text = "";

                gridComisiones.Visible = false;
                gridTiendas.Visible = false;
                gridFacturas.Visible = false;
                gridExcepciones.Visible = false;
                gridAnuladas.Visible = false;
                gridPendientes.Visible = false;

                WebRequest request = WebRequest.Create(string.Format("{0}/fechascomisiones", Convert.ToString(Session["UrlRestServices"])));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                FechaComisionesRespuesta mFechas = JsonConvert.DeserializeObject<FechaComisionesRespuesta>(responseString);

                if (!Convert.ToBoolean(mFechas.exito))
                {
                    lbError.Text = mFechas.mensaje;
                    return;
                }

                DateTime mFechaInicial = Convert.ToDateTime(mFechas.FechaInicial);
                DateTime mFechaFinal = Convert.ToDateTime(mFechas.FechaFinal);

                request = WebRequest.Create(string.Format("{0}/comisiones/?anio1={1}&mes1={2}&dia1={3}&anio2={4}&mes2={5}&dia2={6}&vendedor={7}&tienda={8}", Convert.ToString(Session["UrlRestServices"]), mFechaInicial.Year.ToString(), mFechaInicial.Month.ToString(), mFechaInicial.Day.ToString(), mFechaFinal.Year.ToString(), mFechaFinal.Month.ToString(), mFechaFinal.Day.ToString(), vendedor, tienda));

                request.Method = "GET";
                request.ContentType = "application/json";

                response = (HttpWebResponse)request.GetResponse();
                responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                Clases.RetornaComisiones mRetorna = new Clases.RetornaComisiones();
                mRetorna = JsonConvert.DeserializeObject<Clases.RetornaComisiones>(responseString);

                if (!mRetorna.Info[0].Exito)
                {
                    Session["Comisiones"] = null;
                    lbErrorComisiones.Text = mRetorna.Info[0].Mensaje;
                    return;
                }

                lbInfoComisiones.Text = mRetorna.Info[0].Mensaje;

                if (tienda.Trim().Length > 0)
                {
                    gridComisiones.Visible = true;
                    gridComisiones.DataSource = mRetorna.Comisiones.AsEnumerable();
                    gridComisiones.DataMember = "comisiones";
                    gridComisiones.DataBind();
                    gridComisiones.FocusedRowIndex = -1;
                    gridComisiones.SettingsPager.PageSize = 500;

                    gridTiendas.Visible = true;
                    gridTiendas.DataSource = mRetorna.Tiendas.AsEnumerable();
                    gridTiendas.DataMember = "tiendas";
                    gridTiendas.DataBind();
                    gridTiendas.FocusedRowIndex = -1;
                    gridTiendas.SettingsPager.PageSize = 500;
                }

                gridFacturas.Visible = true;
                gridFacturas.DataSource = mRetorna.Facturas.AsEnumerable();
                gridFacturas.DataMember = "facturas";
                gridFacturas.DataBind();
                gridFacturas.FocusedRowIndex = -1;
                gridFacturas.SettingsPager.PageSize = 15000;

                gridExcepciones.Visible = true;
                gridExcepciones.DataSource = mRetorna.Excepciones.AsEnumerable();
                gridExcepciones.DataMember = "excepciones";
                gridExcepciones.DataBind();
                gridExcepciones.FocusedRowIndex = -1;
                gridExcepciones.SettingsPager.PageSize = 500;

                gridAnuladas.Visible = true;
                gridAnuladas.DataSource = mRetorna.Anuladas.AsEnumerable();
                gridAnuladas.DataMember = "anuladas";
                gridAnuladas.DataBind();
                gridAnuladas.FocusedRowIndex = -1;
                gridAnuladas.SettingsPager.PageSize = 500;

                gridPendientes.Visible = true;
                gridPendientes.DataSource = mRetorna.Pendientes.AsEnumerable();
                gridPendientes.DataMember = "pendientes";
                gridPendientes.DataBind();
                gridPendientes.FocusedRowIndex = -1;
                gridPendientes.SettingsPager.PageSize = 15000;

                foreach (GridViewDataColumn item in gridComisiones.GetGroupedColumns())
                    item.UnGroup();

                foreach (GridViewDataColumn item in gridTiendas.GetGroupedColumns())
                    item.UnGroup();

                foreach (GridViewDataColumn item in gridFacturas.GetGroupedColumns())
                    item.UnGroup();

                foreach (GridViewDataColumn item in gridExcepciones.GetGroupedColumns())
                    item.UnGroup();

                foreach (GridViewDataColumn item in gridAnuladas.GetGroupedColumns())
                    item.UnGroup();

                foreach (GridViewDataColumn item in gridPendientes.GetGroupedColumns())
                    item.UnGroup();

                ((GridViewDataColumn)gridComisiones.Columns["Tienda"]).SortIndex = 0;
                ((GridViewDataColumn)gridComisiones.Columns["Vendedor"]).SortIndex = 1;

                ((GridViewDataColumn)gridComisiones.Columns["Tienda"]).SortAscending();
                ((GridViewDataColumn)gridComisiones.Columns["Vendedor"]).SortAscending();

                gridComisiones.GroupBy(gridComisiones.Columns["Titulo"], 0);
                gridComisiones.GroupBy(gridComisiones.Columns["Tienda"], 1);

                gridTiendas.GroupBy(gridTiendas.Columns["Titulo"], 0);

                gridFacturas.GroupBy(gridFacturas.Columns["Titulo"], 0);
                gridFacturas.GroupBy(gridFacturas.Columns["Tienda"], 1);
                gridFacturas.GroupBy(gridFacturas.Columns["NombreVendedor"], 2);

                gridExcepciones.GroupBy(gridExcepciones.Columns["Titulo"], 0);
                gridExcepciones.GroupBy(gridExcepciones.Columns["Tienda"], 1);

                gridAnuladas.GroupBy(gridAnuladas.Columns["Titulo"], 0);
                gridAnuladas.GroupBy(gridAnuladas.Columns["Tienda"], 1);

                gridPendientes.GroupBy(gridPendientes.Columns["Titulo"], 0);
                gridPendientes.GroupBy(gridPendientes.Columns["Tienda"], 1);
                gridPendientes.GroupBy(gridPendientes.Columns["NombreVendedor"], 2);

                if (mRetorna.Excepciones.Count() == 0) gridExcepciones.Visible = false;
                if (mRetorna.Anuladas.Count() == 0) gridAnuladas.Visible = false;
                if (mRetorna.Pendientes.Count() == 0) gridPendientes.Visible = false;

                lbInfoComisiones.Text = mFechas.mensaje;
            }
            catch (Exception ex)
            {
                lbErrorComisiones.Text = CatchClass.ExMessage(ex, "perfil", "CargarComisiones");
            }
        }

        protected void lkComisiones_Click(object sender, EventArgs e)
        {
            CargarComisiones("", Convert.ToString(vendedorComisiones.Value));
        }

        protected void lkComisionesTienda_Click(object sender, EventArgs e)
        {
            CargarComisiones(tiendaComisiones.SelectedItem.ToString(), "");
        }

        protected void gridComisiones_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.VisibleIndex >= 0 && e.GetValue("Tooltip").ToString().Trim().Length > 0) e.Cell.ToolTip = e.GetValue("Tooltip").ToString();
        }

        protected void gridComisiones_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
            if (Convert.ToString(e.GetValue("Tooltip")).Trim().Length > 0) e.Row.ForeColor = System.Drawing.Color.FromArgb(227, 89, 0);
        }

        protected void gridTiendas_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void gridFacturas_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void gridExcepciones_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void gridAnuladas_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void gridPendientes_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void tiendaComisiones_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaVendedores();
        }


    }
}