﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="pruebas.aspx.cs" Inherits="PuntoDeVenta.pruebas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1000
        {
            color: #FF0000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

                <table>
                    <tr>
                        <td>
                            <asp:LinkButton ID="lkCargarInventario" runat="server" 
                                onclick="lkCargarInventario_Click">Cargar inventario F05 - Marzo 2015</asp:LinkButton>
                        </td>
                        <td style="text-align: right">
                            <span color="red">prueba</span><asp:Label ID="lbErrorInventarioGrabar" runat="server" Font-Bold="True" 
                                ForeColor="Red" Visible="False"></asp:Label>
                            <asp:Label ID="lbInfoInventarioGrabar" runat="server" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gridInventarioGrabar" runat="server" AutoGenerateColumns="False" 
                                        BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                                        CellPadding="3" CellSpacing="2"
                                        onrowdatabound="gridInventarioGrabar_RowDataBound" Visible="False" 
                                        Width="900px" onrowcancelingedit="gridInventarioGrabar_RowCancelingEdit" 
                                        onrowediting="gridInventarioGrabar_RowEditing" 
                                        onrowupdating="gridInventarioGrabar_RowUpdating">
                                        <Columns>
                                            <asp:TemplateField ShowHeader="False" Visible="false">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lkSeleccionarArticuloInventario" runat="server" 
                                                        CausesValidation="False" CommandName="Select" Text="Seleccionar" 
                                                        ToolTip="Haga clic aquí para seleccionar el artículo e ingresar o modificar la toma de inventario físico"></asp:LinkButton>
                                                </ItemTemplate>
                                                <HeaderStyle Width="80px" />
                                                <ItemStyle Width="80px" />
                                            </asp:TemplateField>
                                            <asp:CommandField ShowEditButton="True" CausesValidation="False" EditText="Editar" UpdateText="Actualizar" CancelText = "Cancelar" />
                                            <asp:TemplateField HeaderText="Artículo">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtArticulo" runat="server" ReadOnly="true" Width="90px" Text='<%# Bind("Articulo") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbArticulo" runat="server" Text='<%# Bind("Articulo") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="90px" />
                                                <ItemStyle Width="90px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Descripción" Visible="true">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtDescripcion" runat="server" ReadOnly="true" Width="560px" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <HeaderStyle Width="620px" />
                                                <ItemStyle Font-Size="X-Small" Width="620px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Localización" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbLocalizacion" runat="server" Text='<%# Bind("Localizacion") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtLocalizacion" runat="server" Text='<%# Bind("Localizacion") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <HeaderStyle Font-Size="Small" Width="100px" />
                                                <ItemStyle Font-Size="Small" Width="100px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Ultima Compra" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbUltimaCompra" runat="server" Text='<%# Bind("UltimaCompra", "{0:d}") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtUltimaCompra" runat="server" Text='<%# Bind("UltimaCompra") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <HeaderStyle Font-Size="Small" HorizontalAlign="Center" />
                                                <ItemStyle Font-Size="Small" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="No Escribir" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbBlanco1" runat="server" Text='<%# Bind("Blanco1") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtBlanco1" runat="server" Text='<%# Bind("Blanco1") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <HeaderStyle Font-Size="Small" />
                                                <ItemStyle Font-Size="Small" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Existencia Total" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbExistencias" runat="server" Text='<%# Bind("Existencias") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtExistencias" runat="server" Text='<%# Bind("Existencias") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <HeaderStyle Font-Size="Small" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Existencia Física" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbBlanco2" runat="server" Text='<%# Bind("Blanco2", "{0:####,###,###,###,##0}") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtBlanco2" runat="server" Text='<%# Bind("Blanco2", "{0:####,###,###,###,##0}") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <HeaderStyle Font-Size="Small" HorizontalAlign="Center" Width="110px" />
                                                <ItemStyle HorizontalAlign="Center" Width="110px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Existencia Física">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbExistenciaFisica" runat="server" Text='<%# Bind("ExistenciaFisica", "{0:####,###,###,###,##0}") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtExistenciaFisica" runat="server" Width="110px" Text='<%# Bind("ExistenciaFisica", "{0:####,###,###,###,##0}") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <HeaderStyle Font-Size="Small" HorizontalAlign="Center" Width="110px" />
                                                <ItemStyle HorizontalAlign="Center" Width="110px" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                                        <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                                        <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#FFF1D4" />
                                        <SortedAscendingHeaderStyle BackColor="#B95C30" />
                                        <SortedDescendingCellStyle BackColor="#F1E5CE" />
                                        <SortedDescendingHeaderStyle BackColor="#93451F" />
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td style="text-align: right">
                            <asp:Label ID="lbErrorInventarioGrabar2" runat="server" Font-Bold="True" 
                                ForeColor="Red" Visible="False"></asp:Label>
                            <asp:Label ID="lbInfoInventarioGrabar2" runat="server" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            &nbsp;
                            <asp:LinkButton ID="lkOcultarInventarioGrabar" runat="server" 
                                ToolTip="Haga clic aquí para ocultar el listado de inventario" TabIndex="590" 
                                onclick="lkOcultarInventarioGrabar_Click">Ocultar</asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="lkGrabarInventario" runat="server" 
                                ToolTip="Haga clic aquí para grabar la toma física de inventario." 
                                TabIndex="600" onclick="lkGrabarInventario_Click">Grabar</asp:LinkButton>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" align='right' bgcolor='white' class="style1000">
                            Rop</td>
                    </tr>
                    </table>

</asp:Content>
