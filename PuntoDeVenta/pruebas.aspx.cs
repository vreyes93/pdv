﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PuntoDeVenta
{
    public partial class pruebas : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();

        }

        protected void gridInventarioGrabar_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        void CargarInventario(string accion)
        {
            string mMensaje = "";
            try
            {
                bool mMostrarDetalle = false;
                string mBodega = Convert.ToString(Session["Tienda"]);
                if (mBodega.Length > 4) mBodega = "F01";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                ws.Timeout = 999999999;
                var q = ws.DevuelveArticulosInventario(DateTime.Now.Date, mBodega, "2015", "2", "ROLAPINE", ref mMostrarDetalle);

                if (q.Count() > 0)
                {
                    if (accion == "I")
                    {
                    }

                    if (accion == "T")
                    {
                        lbInfoInventarioGrabar.Visible = true;
                        lbInfoInventarioGrabar.Text = string.Format("Inventario - {0} (Se listan {1} artículos)", mBodega, q.Count().ToString());

                        lbInfoInventarioGrabar2.Visible = true;
                        lbInfoInventarioGrabar2.Text = string.Format("Inventario - {0} (Se listan {1} artículos)", mBodega, q.Count().ToString());
                    }
                }
                else
                {
                    if (accion == "I")
                    {
                    }

                    if (accion == "T")
                    {
                        lbErrorInventarioGrabar.Visible = true;
                        lbErrorInventarioGrabar.Text = "No se encontraron artículos para listar";

                        lbErrorInventarioGrabar2.Visible = true;
                        lbErrorInventarioGrabar2.Text = "No se encontraron artículos para listar";
                    }

                    return;
                }

                dsPuntoVenta ds = new dsPuntoVenta();

                for (int ii = 0; ii < q.Count(); ii++)
                {
                    DataRow row = ds.ArticulosInventario.NewRow();
                    row["Articulo"] = q[ii].Articulo;
                    row["Descripcion"] = q[ii].Descripcion;
                    row["Localizacion"] = q[ii].Localizacion;
                    row["UltimaCompra"] = q[ii].UltimaCompra;
                    row["Blanco1"] = q[ii].Blanco1;
                    row["Existencias"] = q[ii].Existencias;
                    row["Blanco2"] = q[ii].Blanco2;
                    row["ExistenciaFisica"] = q[ii].ExistenciaFisica;
                    ds.ArticulosInventario.Rows.Add(row);
                }

                ViewState["ds"] = ds;

                if (accion == "I")
                {
                }

                if (accion == "T")
                {
                    gridInventarioGrabar.DataSource = ds;
                    gridInventarioGrabar.DataMember = "ArticulosInventario";
                    gridInventarioGrabar.DataBind();
                    gridInventarioGrabar.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                if (accion == "I")
                {
                }

                if (accion == "I")
                {
                    lbErrorInventarioGrabar.Visible = true;
                    lbErrorInventarioGrabar.Text = string.Format("Error al generar inventario - {0} {1} {2}", ex.Message, mMensaje, m);

                    lbErrorInventarioGrabar2.Visible = true;
                    lbErrorInventarioGrabar2.Text = string.Format("Error al generar inventario - {0} {1} {2}", ex.Message, mMensaje, m);
                }
            }
        }

        protected void lkCargarInventario_Click(object sender, EventArgs e)
        {
            try
            {
                CargarInventario("T");
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventarioGrabar.Visible = true;
                lbErrorInventarioGrabar.Text = string.Format("Error al cargar el inventario {0} {1}", ex.Message, m);
            }
        }

        protected void lkOcultarInventarioGrabar_Click(object sender, EventArgs e)
        {
            gridInventarioGrabar.Visible = false;
        }

        protected void lkGrabarInventario_Click(object sender, EventArgs e)
        {

        }

        protected void gridInventarioGrabar_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                gridInventarioGrabar.EditIndex = e.NewEditIndex;

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventarioGrabar.Visible = true;
                lbErrorInventarioGrabar.Text = string.Format("Error al editar las existencias físicas {0} {1}", ex.Message, m);
            }
        }

        protected void gridInventarioGrabar_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gridInventarioGrabar.EditIndex = -1;
        }

        protected void gridInventarioGrabar_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                lbErrorInventarioGrabar.Text = "";
                lbErrorInventarioGrabar2.Text = "";

                GridViewRow row = gridInventarioGrabar.Rows[e.RowIndex];
                //Label lbArticulo = (Label)row.FindControl("lbArticulo");
                //Label lbExistenciaFisica = (Label)row.FindControl("lbExistenciaFisica");

                TextBox txtArticulo = (TextBox)row.FindControl("txtArticulo");
                TextBox txtExistenciaFisica = (TextBox)row.FindControl("txtExistenciaFisica");

                int mExistenciaFisica = 0;

                try
                {
                    mExistenciaFisica = Convert.ToInt32(txtExistenciaFisica.Text);
                }
                catch
                {
                    lbErrorInventarioGrabar.Visible = true;
                    lbErrorInventarioGrabar2.Visible = true;

                    lbErrorInventarioGrabar.Text = string.Format("La existencia ingresada es inválida");
                    lbErrorInventarioGrabar2.Text = string.Format("La existencia ingresada es inválida");

                    e.Cancel = true;
                    return;
                }

                if (mExistenciaFisica < 0)
                {
                    lbErrorInventarioGrabar.Visible = true;
                    lbErrorInventarioGrabar2.Visible = true;

                    lbErrorInventarioGrabar.Text = string.Format("La existencia ingresada es inválida");
                    lbErrorInventarioGrabar2.Text = string.Format("La existencia ingresada es inválida");

                    e.Cancel = true;
                    return;
                }

                dsPuntoVenta ds = new dsPuntoVenta();
                ds = (dsPuntoVenta)ViewState["ds"];

                for (int ii = 0; ii < ds.ArticulosInventario.Rows.Count; ii++)
                {
                    if (ds.ArticulosInventario.Rows[ii]["Articulo"].ToString() == txtArticulo.Text) ds.ArticulosInventario.Rows[ii]["ExistenciaFisica"] = mExistenciaFisica;
                }

                ViewState["ds"] = ds;

                gridInventarioGrabar.DataSource = ds;
                gridInventarioGrabar.DataMember = "ArticulosInventario";
                gridInventarioGrabar.DataBind();
                gridInventarioGrabar.Visible = true;

                gridInventarioGrabar.EditIndex = -1;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventarioGrabar.Visible = true;
                lbErrorInventarioGrabar.Text = string.Format("Error al actualizar las existencias físicas {0} {1}", ex.Message, m);
            }
        }


    }
}