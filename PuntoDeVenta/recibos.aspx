<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="recibos.aspx.cs" Inherits="PuntoDeVenta.recibos" MaintainScrollPositionOnPostback="true" %>
<%@ Import Namespace="System.Activities.Statements" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <style type="text/css">
        .style1000
        {
            height: 22px;
            text-align: center;
        }
        .style1001
        {
            width: 100%;
        }
        .exito {
            color: dimgrey;
            background-color:lightgreen;
        }
        .none {
            background-color:none;
        }
        .auto-style1 {
            margin-left: 0px;
        }
        .auto-style2 {
            width: 70px;
        }
        .auto-style3 {
            font-family: "Arial Narrow";
            font-size: medium;
            color: #000000;
            font-weight: bold;
        }
        .auto-style4 {
            padding: 15px;
            width: 383px;
        }
        </style>
    <script type='text/javascript'>
        $.noConflict();
        function Forzar() {
            __doPostBack('', '');
        }
        function limpiar() {
            document.getElementById("txtValor").value = "";
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>&nbsp;&nbsp;Recibos y Dep�sitos</h3>
    <div class="content2">
        <div class="clientes2">
            <ul>
                <li>
                    <table align="center">
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td style="text-align: left">
                            <span class="style1">Cliente:</span>&nbsp; </td>
                        <td style="text-align: right">
                            <asp:TextBox ID="txtCodigo" runat="server" BackColor="White" BorderStyle="None" 
                                Enabled="False" Font-Bold="True" Font-Size="Large" Width="85px" 
                                TabIndex="640"></asp:TextBox>
                        </td>
                        <td style="text-align: left">
                            <asp:LinkButton ID="lbBuscarCliente" runat="server" 
                                ToolTip="Haga clic aqu� para buscar un cliente" TabIndex="2" 
                                onclick="lbBuscarCliente_Click">Buscar cliente</asp:LinkButton>
                        </td>
                        <td style="text-align: right">
                            Nombre:</td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtNombre" runat="server" BackColor="White" BorderStyle="None" 
                                Enabled="False" Font-Bold="True" Width="420px" 
                                TabIndex="640"></asp:TextBox>
                        </td>
                        <td style="text-align: right" class="style1">
                            Nit:
                            <asp:TextBox ID="txtNit" runat="server" BackColor="White" BorderStyle="None" 
                                Enabled="False" Font-Bold="True" Width="115px" 
                                TabIndex="640"></asp:TextBox>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                    </table>
                </li>
            </ul>
            <ul>
                <li>
                    <table align="center" runat="server" visible="false" id="tblBuscar">
                        <tr>
                            <td colspan="6">
                                <a>B�squeda de Clientes</a>&nbsp;&nbsp;</td>
                            <td colspan="6" style="text-align: right">
                                <asp:Label ID="lbInfoCliente" runat="server"></asp:Label>
                                <asp:Label ID="lbErrorCliente" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>
                                C�digo:</td>
                            <td>
                                <asp:TextBox ID="txtCodigoBuscar" runat="server" AutoPostBack="True" 
                                    ontextchanged="txtCodigoBuscar_TextChanged" TabIndex="14" 
                            
                                    ToolTip="Ingrese aqu� el c�digo o parte del c�digo del cliente que desea buscar y luego presione ENTER.  No es necesario digitar los ceros a la izquierda del c�digo." 
                                    Width="80px"></asp:TextBox>
                            </td>
                            <td>
                                Nombre:</td>
                            <td>
                                <asp:TextBox ID="txtNombreBuscar" runat="server" Width="300px" AutoPostBack="True" 
                                    ontextchanged="txtNombreBuscar_TextChanged" TabIndex="16" style="text-transform: uppercase;" 
                                    ToolTip="Ingrese aqu� el nombre o parte del nombre del cliente que desea buscar y luego presione ENTER."></asp:TextBox>
                            </td>
                            <td colspan="2">
                                NIT:</td>
                            <td>
                                <asp:TextBox ID="txtNitBuscar" runat="server" AutoPostBack="True" 
                                    ontextchanged="txtNitBuscar_TextChanged" TabIndex="18" style="text-transform: uppercase;" 
                            
                                    ToolTip="Ingrese aqu� el NIT o parte del NIT que desea buscar y luego presione ENTER." 
                                    Width="80px"></asp:TextBox>
                            </td>
                            <td>
                                Tel.:</td>
                            <td>
                                <asp:TextBox ID="txtTelefono" runat="server" AutoPostBack="True" 
                                    ontextchanged="txtTelefono_TextChanged" TabIndex="20" Width="90px" 
                            
                            
                                    ToolTip="Ingrese aqu� el tel�fono que desea buscar y luego presione ENTER, se buscar� en el tel�fono de casa, trabajo y celular."></asp:TextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkOcultarBusqueda" runat="server" 
                                    ToolTip="Haga clic aqu� para ocultar la b�squeda de clientes." 
                                    onclick="lkOcultarBusqueda_Click" TabIndex="22">Ocultar</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                </li>
            </ul>
            <ul>
                <li>
                    <asp:GridView ID="gridClientes" runat="server" Visible="False" 
                        AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" 
                        BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" 
                        onselectedindexchanged="gridClientes_SelectedIndexChanged" 
                        onrowdatabound="gridClientes_RowDataBound" TabIndex="30">
                        <Columns>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lkSeleccionar" runat="server" CausesValidation="False" 
                                        CommandName="Select" Text="Seleccionar" 
                                        ToolTip="Haga clic aqu� para generarle recibo a este cliente"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Cliente" HeaderText="Cliente">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Nombre" HeaderText="Nombre" >
                            <ItemStyle Font-Size="X-Small" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Nit" HeaderText="Nit" />
                            <asp:BoundField DataField="TelCasa" HeaderText="Tel Casa" />
                            <asp:BoundField DataField="TelTrabajo" HeaderText="Tel Trabajo" />
                            <asp:BoundField DataField="TelCelular" HeaderText="Celular" />
                            <asp:BoundField DataField="Tienda" HeaderText="Tienda">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Vendedor" HeaderText="Vendedor" />
                            <asp:BoundField DataField="FechaIngreso" HeaderText="Ingreso" Visible="False">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                        <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                        <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                        <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                        <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#FFF1D4" />
                        <SortedAscendingHeaderStyle BackColor="#B95C30" />
                        <SortedDescendingCellStyle BackColor="#F1E5CE" />
                        <SortedDescendingHeaderStyle BackColor="#93451F" />
                    </asp:GridView>
                </li>
            </ul>
            <ul>
                <li>
                    <table align="center">
                        <tr>
                            <td colspan="3">
                                <a>Recibos</a></td>
                            <td colspan="5" style="text-align: right">
                                <asp:Label ID="lbInfoTasa" runat="server"></asp:Label>
                                </td>
                        </tr>
                        <tr>
                            <td >
                                Aplicar a:</td>
                            <td colspan="3">
                                <asp:DropDownList ID="cbAplicar" runat="server" Width="397px" TabIndex="40" 
                                    ToolTip="Seleccione la factura a la que se aplicar� el pago o bien indique que se trata de un anticipo" CssClass="auto-style1" Height="16px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                Moneda:</td>
                            <td>
                                <asp:DropDownList ID="cbMoneda" runat="server" AutoPostBack="True" 
                                    Font-Bold="True" onselectedindexchanged="cbMoneda_SelectedIndexChanged" TabIndex="50" Height="16px" Width="97px">
                                    <asp:ListItem Value="Q">Quetzales (Q)</asp:ListItem>
                                    <asp:ListItem Value="$">D�lares ($)</asp:ListItem>
                                </asp:DropDownList>
                                &nbsp;Monto a recibir:
                                <asp:TextBox ID="txtMontoRecibir" runat="server" ReadOnly="True" TabIndex="900"     
                                    ToolTip="Este es el monto a recibir, es decir, es el valor por el que se emitir� el recibo" 
                                    Width="129px" Font-Bold="True"></asp:TextBox>
                            </td>
                            <td>
                                </td>
                        </tr>
                        <tr>
                            <td class="auto-style2">
                                Efectivo:</td>
                            <td>
                                <asp:TextBox ID="txtEfectivo" runat="server" TabIndex="60" 
                                    ToolTip="Ingrese aqu� el valor que entrega el cliente en efectivo, no se aceptan letras, solamente n�meros" 
                                    AutoPostBack="True" ontextchanged="txtEfectivo_TextChanged" Width="116px"></asp:TextBox>
                                <asp:TextBox ID="txtTasaCambio" runat="server" TabIndex="999" 
                                    AutoPostBack="True" ontextchanged="txtEfectivo_TextChanged" Width="1px" 
                                    Visible="False"></asp:TextBox>
                            </td>
                            <td>
                                Tarjeta:</td>
                            <td colspan="2">
                                <asp:TextBox ID="txtTarjeta" runat="server" TabIndex="70" 
                                    
                                    ToolTip="Ingrese aqu� el valor que paga el cliente en tarjeta de cr�dito o d�bito, no se aceptan letras, solamente n�meros" 
                                    AutoPostBack="True" ontextchanged="txtTarjeta_TextChanged" Width="110px"></asp:TextBox>
                            </td>
                            <td>
                                POS:</td>
                            <td>
                                <asp:DropDownList ID="cbPos" runat="server" Width="345px" TabIndex="75" 
                                    ToolTip="Seleccione el POS en el que pasar� la tarjeta">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style2">
                                Cheque:</td>
                            <td>
                                <asp:TextBox ID="txtCheque" runat="server" TabIndex="80" 
                                    ToolTip="Ingrese aqu� el valor que entrega el cliente en cheque, se le pedir� el n�mero de cheque y el banco del mismo, no se aceptan letras, solamente n�meros" 
                                    AutoPostBack="True" ontextchanged="txtCheque_TextChanged" Width="116px"></asp:TextBox>
                            </td>
                            <td>
                                Cheque N�mero:</td>
                            <td colspan="2">
                                <asp:TextBox ID="txtChequeNumero" runat="server" TabIndex="90" 
                                    ToolTip="Ingrese aqu� el n�mero del cheque, no se aceptan letras, solamente n�meros" 
                                    Width="110px"></asp:TextBox>
                            </td>
                            <td>
                                Banco emisor:</td>
                            <td>
                                <asp:DropDownList ID="cbChequeBanco" runat="server" Width="345px" TabIndex="100" 
                                    ToolTip="Seleccione el banco al que pertenece el cheque que entrega el cliente o bien el banco emisor de la tarjeta de cr�dito � d�bito">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                                                
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style2">
                                Observaciones:</td>
                            <td colspan="6">
                                <asp:TextBox ID="txtObservacionesRecibo" runat="server" MaxLength="1000" 
                                    Width="779px" TabIndex="110" style="text-transform: uppercase;"
                                    
                                    ToolTip="Ingrese aqu� algunas observaciones que considere pertienentes en el recibo, no es necesario que mencione la factura que cancela, esto el sistema lo har� autom�ticamente"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="6" style="text-align: center">
                                <asp:Label ID="lbInfoRecibo" runat="server"></asp:Label>

                                <asp:Label ID="lbErrorRecibo" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style1000" colspan="6">

                                <asp:LinkButton ID="lkLimpiarRecibo" runat="server" TabIndex="120" 
                                    
                                    ToolTip="Haga clic aqu� para limpiar los campos e ingresar un recibo nuevo" 
                                    onclick="lkLimpiarRecibo_Click">Limpiar</asp:LinkButton>
                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                <asp:LinkButton ID="lkGrabarRecibo" runat="server" TabIndex="130" 
                                    ToolTip="Haga clic aqu� para grabar el recibo" 
                                    onclick="lkGrabarRecibo_Click">Grabar</asp:LinkButton>
                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                <asp:LinkButton ID="lkImprimirRecibo" runat="server" TabIndex="140" 
                                    ToolTip="Haga clic aqu� para imprimir el recibo" visible="false"
                                    onclick="lkImprimirRecibo_Click">Imprimir</asp:LinkButton>
                            </td>
                            <td class="style1000">
                                </td>
                        </tr>
                    </table>
                </li>
            </ul>
            <ul>
                <li>
                    
                    <table align="center">
                        <tr>
                            <td colspan="5"><a>Dep�sitos</a>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td colspan="3" style="text-align: center">
                                <asp:GridView ID="gridRecibos" runat="server" AutoGenerateColumns="False" 
                                    Width="757px" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" 
                                    BorderWidth="1px" CellPadding="3" CellSpacing="2" 
                                    onrowdatabound="gridRecibos_RowDataBound" 
                                    onselectedindexchanged="gridRecibos_SelectedIndexChanged" TabIndex="210">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbRecibo" runat="server" CommandName="Select" 
                                                    CausesValidation="true" AutoPostBack="True" 
                                                    oncheckedchanged="cbRecibo_CheckedChanged" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="30px" HorizontalAlign="Center" />
                                            <ItemStyle Width="30px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Recibo">
                                            <ItemTemplate>
                                                <asp:Label ID="lbRecibo" runat="server" Text='<%# Bind("Recibo") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtRecibo" runat="server" Text='<%# Bind("Recibo") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="160px" />
                                            <ItemStyle Width="160px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha">
                                            <ItemTemplate>
                                                <asp:Label ID="lbFecha" runat="server" Text='<%# Bind("Fecha", "{0:d}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtFecha" runat="server" Text='<%# Bind("Fecha", "{0:d}") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="120px" HorizontalAlign="Center" />
                                            <ItemStyle Width="120px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Efectivo">
                                            <ItemTemplate>
                                                <asp:Label ID="lbEfectivo" runat="server" Text='<%# Bind("Efectivo", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtEfectivo" runat="server" Text='<%# Bind("Efectivo", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="120px" HorizontalAlign="Right" />
                                            <ItemStyle Width="120px" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cheque">
                                            <ItemTemplate>
                                                <asp:Label ID="lbCheque" runat="server" 
                                                    Text='<%# Bind("Cheque", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtCheque" runat="server" 
                                                    Text='<%# Bind("Cheque", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="120px" HorizontalAlign="Right" />
                                            <ItemStyle Width="120px" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tarjeta">
                                            <ItemTemplate>
                                                <asp:Label ID="lbTarjeta" runat="server" 
                                                    Text='<%# Bind("Tarjeta", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTarjeta" runat="server" 
                                                    Text='<%# Bind("Tarjeta", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="120px" HorizontalAlign="Right" />
                                            <ItemStyle Width="120px" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pos" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lbPos" runat="server" Text='<%# Bind("Pos") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtPos" runat="server" Text='<%# Bind("Pos") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pos" Visible="True" >
                                            <ItemTemplate>
                                                <asp:Label ID="lbPosNombre" runat="server" Width="80px" Text='<%# Bind("PosNombre") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtPosNombre" runat="server"  Width="80px" Text='<%# Bind("PosNombre") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                                </asp:GridView>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td style="text-align: right">
                                Valor Dep�sito:</td>
                            <td>
                                <asp:TextBox ID="txtDeposito" runat="server" TabIndex="220" 
                                    ToolTip="Ingrese aqu� el valor que entrega el cliente en cheque, se le pedir� el n�mero de cheque y el banco del mismo, no se aceptan letras, solamente n�meros" 
                                    Width="100px"></asp:TextBox>
                            <asp:CheckBox ID="cbDeposito" runat="server" TabIndex="230" Text="Acepto el monto del dep�sito" />
                            </td>
                            <td>
                                <asp:Label ID="lbComision" runat="server" Text="Comisi�n:" Visible="False"></asp:Label>
                                &nbsp;<asp:TextBox ID="txtComision" runat="server" TabIndex="235" Visible="False" Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td style="text-align: right">
                                Cuenta No.:</td>
                            <td>
                                <asp:DropDownList ID="cbCuenta" runat="server" Width="300px" 
                                    TabIndex="240" ToolTip="Seleccione la cuenta bancaria">
                                </asp:DropDownList>
                                <asp:CheckBox ID="cbAceptaCuenta" runat="server" TabIndex="245" Text="Acepto" 
                                    ToolTip="Marque esta casilla si esta seguro de registrar el dep�sito en la cuenta seleccionada" />
                            </td>
                            <td style="text-align: left">
                                Boleta No.:<asp:TextBox ID="txtBoleta" 
                                    runat="server" TabIndex="250" 
                                    Width="100px"></asp:TextBox>
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td style="text-align: center">
                                &nbsp;</td>
                            <td style="text-align: center">
                                <asp:Label ID="lbInfoDeposito" runat="server"></asp:Label>
                                <asp:Label ID="lbErrorDeposito" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lbFecha" runat="server" Text="Fecha:" Visible="False"></asp:Label>
                                <asp:TextBox ID="txtFecha" runat="server" TabIndex="255" TextMode="Date" 
                                    Visible="False" Width="124px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td colspan="3" style="text-align: center">
                                <asp:LinkButton ID="lkLimpiarDeposito" runat="server" TabIndex="260" 
                                ToolTip="Haga clic aqu� para limpiar los campos e ingresar un dep�sito nuevo" 
                                onclick="lkLimpiarDeposito_Click">Limpiar</asp:LinkButton>
                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                <asp:LinkButton ID="lkGrabarDeposito" runat="server" TabIndex="270" 
                                ToolTip="Haga clic aqu� para grabar el dep�sito" 
                                onclick="lkGrabarDeposito_Click">Grabar</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                    
                </li>
            </ul>
            <ul>
                <li>    
                    <table align="center">
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                N�mero de recibo a re-imprimir:</td>
                            <td>
                                <asp:TextBox ID="txtReciboReimprimir" runat="server" TabIndex="310" style="text-transform: uppercase;"
                                    ToolTip="Ingrese aqu� el n�mero de recibo a Re-imprimir, ejemplo: PVF09-000011"></asp:TextBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkReimprimirRecibo" runat="server" TabIndex="315" 
                                    onclick="lkReimprimirRecibo_Click" 
                                    ToolTip="Haga clic aqu� para re-imprimir el recibo">Re-imprimir recibo</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                    </table>
                    
                </li>
            </ul>
            <ul>
            <li>
                <table align="center">
                    <tr>
                        <td colspan="3">
                            <a>Anulaci�n de Recibos</a></td>
                        <td colspan="3" style="text-align: right">
                            <asp:Label ID="lbErrorAnularRecibo" runat="server" Font-Bold="True" ForeColor="Red" 
                                Visible="False"></asp:Label>
                            <asp:Label ID="lbInfoAnularRecibo" runat="server" Visible="False" CssClass="exito"></asp:Label>
                            </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            Recibo a anular:</td>
                        <td colspan="2">
                            <asp:TextBox ID="txtReciboAnular" runat="server" 
                                style="text-transform: uppercase;" 
                                ToolTip="Ingrese aqu� el n�mero de recibo con serie y n�mero completo separado con un gu�on, por ejemplo: PVF02-000002" 
                                TabIndex="320" Width="120px"></asp:TextBox>
                        &nbsp;Observaciones:
                            <asp:TextBox ID="txtReciboAnularObservaciones" runat="server" 
                                style="text-transform: uppercase;" 
                                ToolTip="Ingrese aqu� la raz�n por la que est� anulando el recibo" 
                                TabIndex="330" Width="460px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:LinkButton ID="lkAnularRecibo" runat="server" TabIndex="340" 
                                ToolTip="Haga clic aqu� para anular el recibo" onclientclick="return confirm('Desea anular el recibo?');"
                                onclick="lkAnularRecibo_Click">Anular Recibo</asp:LinkButton>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
                
            </li>
            </ul>
            <ul>
            <li>
                <table align="center" >
                    <tr>
                        <td colspan="4">
                            <a>Corte de Caja</a></td>
                        <td colspan="4" style="text-align: right">
                            <asp:Label ID="lbErrorCorteCaja" runat="server" Font-Bold="True" 
                                ForeColor="Red"></asp:Label>
                            <asp:Label ID="lbInfoCorteCaja" runat="server" CssClass="exito"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td colspan="6" style="text-align: center">
                            <asp:LinkButton ID="lkDocuementosCierre" runat="server" 
                                onclick="lkDocuementosCierre_Click" TabIndex="350">Haga clic aqu� para cargar los documentos a cerrar</asp:LinkButton>
                            </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="6">
                            <asp:GridView ID="gridRecibosCierre" runat="server" AutoGenerateColumns="False" CssClass="FixBootstrapFailGrid"
                                    Width="800px" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" 
                                    BorderWidth="1px" CellPadding="3" CellSpacing="2" 
                                    onrowdatabound="gridRecibosCierre_RowDataBound" 
                                    onselectedindexchanged="gridRecibosCierre_SelectedIndexChanged" TabIndex="210" OnRowCommand="gridRecibosCierre_RowCommand">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbReciboCierre" runat="server" CommandName="Select" 
                                                    CausesValidation="true" AutoPostBack="True" 
                                                    oncheckedchanged="cbReciboCierre_CheckedChanged" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="30px" HorizontalAlign="Center" />
                                            <ItemStyle Width="30px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Recibo">
                                            <ItemTemplate>
                                                <asp:Label ID="lbRecibo" runat="server" Text='<%# Bind("Recibo") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtRecibo" runat="server" Text='<%# Bind("Recibo") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="160px" />
                                            <ItemStyle Width="160px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha">
                                            <ItemTemplate>
                                                <asp:Label ID="lbFecha" runat="server" Text='<%# Bind("Fecha", "{0:d}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtFecha" runat="server" Text='<%# Bind("Fecha", "{0:d}") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="120px" HorizontalAlign="Center" />
                                            <ItemStyle Width="120px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Efectivo">
                                            <ItemTemplate>
                                                <asp:Label ID="lbEfectivo" runat="server" Text='<%# Bind("Efectivo", "{0:####,###,###,###,###.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtEfectivo" runat="server" Text='<%# Bind("Efectivo", "{0:####,###,###,###,###.00}") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="120px" HorizontalAlign="Right" />
                                            <ItemStyle Width="120px" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cheque">
                                            <ItemTemplate>
                                                <asp:Label ID="lbCheque" runat="server" 
                                                    Text='<%# Bind("Cheque", "{0:####,###,###,###,###.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtCheque" runat="server" 
                                                    Text='<%# Bind("Cheque", "{0:####,###,###,###,###.00}") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="120px" HorizontalAlign="Right" />
                                            <ItemStyle Width="120px" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tarjeta">
                                            <ItemTemplate>
                                                <asp:Label ID="lbTarjeta" runat="server" 
                                                    Text='<%# Bind("Tarjeta", "{0:####,###,###,###,###.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTarjeta" runat="server" 
                                                    Text='<%# Bind("Tarjeta", "{0:####,###,###,###,###.00}") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="120px" HorizontalAlign="Right" />
                                            <ItemStyle Width="120px" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pos" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lbPos" runat="server" Text='<%# Bind("Pos") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtPos" runat="server" Text='<%# Bind("Pos") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pos" Visible="True">
                                            <ItemTemplate>
                                                <asp:Label ID="lbPosNombre" runat="server" Text='<%# Bind("PosNombre") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtPosNombre" runat="server" Text='<%# Bind("PosNombre") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Archivos">
                                            <ItemTemplate>
                                                <asp:Label ID="lbArchivos" runat="server" Text='<%# Bind("Archivos") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Seleccionar">
                                            <ItemTemplate>
                                                <asp:FileUpload runat="server" AllowMultiple="True" ID="fileUploader" accept=".png,.jpg,.jpeg,.pdf"
                                                                Visible='<%# ((decimal)DataBinder.Eval(Container.DataItem, "Tarjeta")) != 0  %>'/>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button runat="server" Text="Subir" CommandName="UploadFiles" CommandArgument="<%# Container.DataItemIndex %>"
                                                            Visible='<%# ((decimal)DataBinder.Eval(Container.DataItem, "Tarjeta")) != 0  %>'/>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                                </asp:GridView>                        
                            </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td colspan="6">
                            <br/>
                            <asp:GridView ID="gridDepositosCierre" runat="server" AutoGenerateColumns="False" CssClass="FixBootstrapFailGrid"
                                    Width="705px" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" 
                                    BorderWidth="1px" CellPadding="3" CellSpacing="2" 
                                    onrowdatabound="gridDepositosCierre_RowDataBound" 
                                    onselectedindexchanged="gridDepositosCierre_SelectedIndexChanged" TabIndex="210" OnRowCommand="gridDepositosCierre_RowCommand">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbDepositoCierre" runat="server" CommandName="Select" 
                                                    CausesValidation="true" AutoPostBack="True" 
                                                    oncheckedchanged="cbDepositoCierre_CheckedChanged" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="30px" HorizontalAlign="Center" />
                                            <ItemStyle Width="30px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cuenta">
                                            <ItemTemplate>
                                                <asp:Label ID="lbCuenta" runat="server" Text='<%# Bind("Cuenta") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtCuenta" runat="server" Text='<%# Bind("Cuenta") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="160px" />
                                            <ItemStyle Width="160px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TipoDocumento" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lbTipoDocumento" runat="server" Text='<%# Bind("TipoDocumento") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTipoDocumento" runat="server" Text='<%# Bind("TipoDocumento") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="160px" />
                                            <ItemStyle Width="160px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Dep�sito">
                                            <ItemTemplate>
                                                <asp:Label ID="lbNumero" runat="server" Text='<%# Bind("Numero", "{0:#}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtNumero" runat="server" Text='<%# Bind("Numero", "{0:#}") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="160px" />
                                            <ItemStyle Width="160px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Banco">
                                            <ItemTemplate>
                                                <asp:Label ID="lbBanco" runat="server" Text='<%# Bind("Banco") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtBanco" runat="server" Text='<%# Bind("Banco") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="300px" />
                                            <ItemStyle Width="300px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha">
                                            <ItemTemplate>
                                                <asp:Label ID="lbFecha" runat="server" Text='<%# Bind("Fecha", "{0:d}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtFecha" runat="server" Text='<%# Bind("Fecha", "{0:d}") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="120px" HorizontalAlign="Center" />
                                            <ItemStyle Width="120px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Monto">
                                            <ItemTemplate>
                                                <asp:Label ID="lbMonto" runat="server" Text='<%# Bind("Monto", "{0:####,###,###,###,###.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtMonto" runat="server" Text='<%# Bind("Monto", "{0:####,###,###,###,###.00}") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="120px" HorizontalAlign="Right" />
                                            <ItemStyle Width="120px" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Archivos">
                                            <ItemTemplate>
                                                <asp:Label ID="lbArchivos" runat="server" Text='<%# Bind("Archivos") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Seleccionar">
                                            <ItemTemplate>
                                                <asp:FileUpload runat="server" AllowMultiple="True" ID="fileUploader" accept=".png,.jpg,.jpeg,.pdf"/>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button runat="server" Text="Subir" CommandName="UploadFiles" CommandArgument="<%# Container.DataItemIndex %>"/>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                                </asp:GridView>                        
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td colspan="6" style="text-align: center">
                            <asp:GridView ID="gridDepositosCierrePendientes" runat="server" AutoGenerateColumns="False" 
                                    Width="705px" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" 
                                    BorderWidth="1px" CellPadding="3" CellSpacing="2" 
                                    onrowdatabound="gridDepositosCierrePendientes_RowDataBound" 
                                    onselectedindexchanged="gridDepositosCierrePendientes_SelectedIndexChanged" TabIndex="210">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbDepositoCierrePendiente" runat="server" CommandName="Select" 
                                                    CausesValidation="true" AutoPostBack="True" 
                                                    oncheckedchanged="cbDepositoCierrePendiente_CheckedChanged" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="30px" HorizontalAlign="Center" />
                                            <ItemStyle Width="30px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cuenta">
                                            <ItemTemplate>
                                                <asp:Label ID="lbCuenta" runat="server" Text='<%# Bind("Cuenta") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtCuenta" runat="server" Text='<%# Bind("Cuenta") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="160px" />
                                            <ItemStyle Width="160px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TipoDocumento" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lbTipoDocumento" runat="server" Text='<%# Bind("TipoDocumento") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTipoDocumento" runat="server" Text='<%# Bind("TipoDocumento") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="160px" />
                                            <ItemStyle Width="160px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Dep�sito">
                                            <ItemTemplate>
                                                <asp:Label ID="lbNumero" runat="server" Text='<%# Bind("Numero", "{0:#}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtNumero" runat="server" Text='<%# Bind("Numero", "{0:#}") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="160px" />
                                            <ItemStyle Width="160px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Banco">
                                            <ItemTemplate>
                                                <asp:Label ID="lbBanco" runat="server" Text='<%# Bind("Banco") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtBanco" runat="server" Text='<%# Bind("Banco") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="300px" />
                                            <ItemStyle Width="300px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha">
                                            <ItemTemplate>
                                                <asp:Label ID="lbFecha" runat="server" Text='<%# Bind("Fecha", "{0:d}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtFecha" runat="server" Text='<%# Bind("Fecha", "{0:d}") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="120px" HorizontalAlign="Center" />
                                            <ItemStyle Width="120px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Monto">
                                            <ItemTemplate>
                                                <asp:Label ID="lbMonto" runat="server" Text='<%# Bind("Monto", "{0:####,###,###,###,###.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtMonto" runat="server" Text='<%# Bind("Monto", "{0:####,###,###,###,###.00}") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="120px" HorizontalAlign="Right" />
                                            <ItemStyle Width="120px" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                                </asp:GridView>                                                    
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td style="text-align: left">
                            Efectivo/Cheques:</td>
                        <td>
                            <asp:TextBox ID="txtRecibosCierre" runat="server" ReadOnly="True" Width="127px"></asp:TextBox>
                        </td>
                        <td style="text-align: right">
                            Dep�sitos:</td>
                        <td>
                            <asp:TextBox ID="txtDepositosCierre" runat="server" ReadOnly="True" 
                                Width="120px"></asp:TextBox>
                        </td>
                        <td style="text-align: right">
                            Diferencia:</td>
                        <td>
                            <asp:TextBox ID="txtDiferenciaCierre" runat="server" ReadOnly="True" Width="127px" 
                                Font-Bold="True" ForeColor="Red"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td style="text-align: left">
                            Archivos:</td>
                        <td colspan="5">
                            <asp:FileUpload runat="server" ID="fuCierre" accept=".png,.jpg,.jpeg,.pdf" AllowMultiple="True"/>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td style="text-align: left">
                            Observaciones:</td>
                        <td colspan="5">
                            <asp:TextBox ID="txtObservacionesCierre" runat="server" Width="577px" 
                                TabIndex="360"></asp:TextBox>
                            <asp:TextBox ID="txtCierre" runat="server" ReadOnly="True" TabIndex="9000" 
                                Visible="False" Width="5px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td style="text-align: left">
                            Lote Visanet:</td>
                        <td>
                            <asp:TextBox ID="txtLoteVisanet" runat="server" Width="37px" TabIndex="360"></asp:TextBox>
                        &nbsp;&nbsp;Q&nbsp;<asp:TextBox ID="txtMontoVisanet" runat="server" Width="60px" 
                                TabIndex="370"></asp:TextBox>
                        &nbsp;<asp:LinkButton ID="lkAgregarVisanet" runat="server" 
                                ToolTip="Agregar lote de Visanet" onclick="lkAgregarVisanet_Click" 
                                TabIndex="380">+</asp:LinkButton>
                        </td>
                        <td colspan="2" style="text-align: center">
                            <asp:LinkButton ID="lkGrabarCierre" runat="server" 
                                onclick="lkGrabarCierre_Click" TabIndex="390" 
                                ToolTip="Haga clic aqu� para grabar el corte de caja">Grabar</asp:LinkButton>
                                &nbsp;&nbsp;|&nbsp;&nbsp;
                            <asp:LinkButton ID="lkImprimirCierre" runat="server" onclick="lkImprimirCierre_Click" TabIndex="395" ToolTip="Haga clic aqu� para imprimir el corte de caja">Imprimir</asp:LinkButton>
                        </td>
                        <td style="text-align: right">
                            Lote Credomatic:</td>
                        <td>
                            <asp:TextBox ID="txtLoteCredomatic" runat="server" Width="37px" TabIndex="400"></asp:TextBox>
                        &nbsp;&nbsp;Q&nbsp;<asp:TextBox ID="txtMontoCredomatic" runat="server" Width="60px" 
                                TabIndex="410"></asp:TextBox>
                        &nbsp;<asp:LinkButton ID="lkAgregarCredomatic" runat="server" 
                                ToolTip="Agregar lote de Credomatic" onclick="lkAgregarCredomatic_Click" 
                                TabIndex="420">+</asp:LinkButton>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td colspan="2">
                            <asp:GridView ID="gridVisanet" runat="server" AutoGenerateColumns="False"
                                BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                                CellPadding="3" CellSpacing="2" Width="240px" 
                                onrowdatabound="gridVisanet_RowDataBound" 
                                onselectedindexchanged="gridVisanet_SelectedIndexChanged" TabIndex="430">
                                <Columns>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkEliminarVisanet" runat="server" CausesValidation="False" 
                                                CommandName="Select" Text="Eliminar" 
                                                onclientclick="return confirm('Desea eliminar el lote?');" 
                                                ToolTip="Haga clic aqu� para eliminar este lote de Visanet"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lote">
                                        <ItemTemplate>
                                            <asp:Label ID="lbLote" runat="server" Text='<%# Bind("Lote") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtLote" runat="server" Text='<%# Bind("Lote") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderStyle Width="60px" />
                                        <ItemStyle Width="60px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Monto">
                                        <ItemTemplate>
                                            <asp:Label ID="lbMonto" runat="server" Text='<%# Bind("Monto", "{0:####,###,###,###,###.00}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtMonto" runat="server" Text='<%# Bind("Monto", "{0:####,###,###,###,###.00}") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderStyle Width="80px" HorizontalAlign="Right" />
                                        <ItemStyle Width="80px" HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#FFF1D4" />
                                <SortedAscendingHeaderStyle BackColor="#B95C30" />
                                <SortedDescendingCellStyle BackColor="#F1E5CE" />
                                <SortedDescendingHeaderStyle BackColor="#93451F" />
                            </asp:GridView>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td colspan="2">
                            <asp:GridView ID="gridCredomatic" runat="server" AutoGenerateColumns="False" 
                                BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                                CellPadding="3" CellSpacing="2" Width="240px" 
                                onrowdatabound="gridCredomatic_RowDataBound" 
                                onselectedindexchanged="gridCredomatic_SelectedIndexChanged" 
                                TabIndex="440">
                                <Columns>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkEliminarCredomatic" runat="server" 
                                                CausesValidation="False" CommandName="Select" Text="Eliminar" 
                                                onclientclick="return confirm('Desea eliminar el lote?');" 
                                                ToolTip="Haga clic aqu� para eliminar este lote de Credomatic"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lote">
                                        <ItemTemplate>
                                            <asp:Label ID="lbLote" runat="server" Text='<%# Bind("Lote") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtLote" runat="server" Text='<%# Bind("Lote") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderStyle Width="60px" />
                                        <ItemStyle Width="60px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Monto">
                                        <ItemTemplate>
                                            <asp:Label ID="lbMonto" runat="server" Text='<%# Bind("Monto", "{0:####,###,###,###,###.00}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtMonto" runat="server" Text='<%# Bind("Monto", "{0:####,###,###,###,###.00}") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderStyle Width="80px" HorizontalAlign="Right" />
                                        <ItemStyle Width="80px" HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#FFF1D4" />
                                <SortedAscendingHeaderStyle BackColor="#B95C30" />
                                <SortedDescendingCellStyle BackColor="#F1E5CE" />
                                <SortedDescendingHeaderStyle BackColor="#93451F" />
                            </asp:GridView>
                            </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    </table>
            </li>
            </ul>
            <ul>
            <li>
                
                <table align="center">
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            Tienda:</td>
                        <td>
                            <asp:DropDownList ID="cbTiendaCierre" runat="server" Width="70px" 
                                TabIndex="450" AutoPostBack="True" Enabled="False" 
                                onselectedindexchanged="cbTiendaCierre_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>
                            A�o:</td>
                        <td>
                            <asp:TextBox ID="txtAnioCierre" runat="server" Width="40px" 
                                TabIndex="455" ontextchanged="txtAnioCierre_TextChanged" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            Fecha:</td>
                        <td>
                            <asp:DropDownList ID="cbCierre" runat="server" Width="100px" 
                                TabIndex="460">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:LinkButton ID="lkReimprimirCierre" runat="server" 
                                onclick="lkReimprimirCierre_Click" 
                                ToolTip="Haga clic aqu� para reimprimir el cierre seleccionado" 
                                TabIndex="470">Reimprimir Cierre</asp:LinkButton>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                </table>
                
            </li>
            </ul>

            <ul>
                <li>
                    <table  align="center">
                        <tr>
                            <td colspan="3">
                                <a>Confirmaci�n de boleta de dep�sito</a></td>
                            <td colspan="3" style="text-align: right">
                                <asp:Label ID="lbInfoBoleta" runat="server" CssClass="exito" />
                                <asp:Label ID="lbErrorBoleta" runat="server" Font-Bold="True" ForeColor="Red" />
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>Boleta:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtBoletaConfirmar" runat="server" Width="130px" style="text-transform: uppercase;"
                                    CssClass="textBoxStyle" TabIndex="510" ToolTip="Ingrese el n�mero de boleta a confirmar" />
                            </td>
                            <td>Fecha:</td>
                            <td>
                                <dx:ASPxDateEdit ID="cbFechaBoletaConfirmar" runat="server" Theme="SoftOrange" DisplayFormatString="dd/MM/yyyy"
                                    CssClass="DateEditStyle" Width="140px" TabIndex="515">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Cuenta:</td>
                            <td>
                                <dx:ASPxComboBox ID="cbBancoBoletaConfirmar" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle" 
                                    ValueType="System.String" Width="300px" DropDownStyle="DropDownList" 
                                    DropDownWidth="40" DropDownRows="25" TabIndex="520">
                                </dx:ASPxComboBox>
                            </td>
                            <td>Monto:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtMontoBoletaConfirmar" runat="server" Width="140px" style="text-transform: uppercase;"
                                    CssClass="textBoxStyle" TabIndex="525" ToolTip="Ingrese el monto de boleta a confirmar" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td colspan="2" style="text-align: center">
                                <dx:ASPxButton ID="btnGrabarBoletaConfirmar" runat="server" Width="8px" Height="8px"
                                    ToolTip="Grabar la confirmaci�n de boleta" onclick="btnGrabarBoletaConfirmar_Click" TabIndex="530" >
                                    <Image IconID="actions_save_16x16devav" />
                                </dx:ASPxButton>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>

                </li>
            </ul>

        </div>
        <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server" TabIndex="620"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" 
                        TabIndex="630"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <asp:ScriptManager runat="server" ID="scM1">

       </asp:ScriptManager>
    <asp:ModalPopupExtender ID="mdlRecibos" runat="server" targetcontrolid="lblID" popupcontrolid="pnlConfirmation"></asp:ModalPopupExtender>
    
     <asp:ModalPopupExtender ID="mdlRecibosConf" runat="server" targetcontrolid="lblID2" popupcontrolid="pnlConfirmation2"></asp:ModalPopupExtender>
     <asp:ModalPopupExtender ID="mdlRecibosAnticipo" runat="server" targetcontrolid="LblId3" popupcontrolid="pnlConfirmation3"></asp:ModalPopupExtender>
    
   






   <asp:Label ID="lblID" runat="server" Text="" Font-Bold="True" Font-Names="Arial Narrow" Font-Size="Medium" ForeColor="Black"></asp:Label>
    <asp:Label ID="lblID2" runat="server" Text="" Font-Bold="True" Font-Names="Arial Narrow" Font-Size="Medium" ForeColor="Black"></asp:Label>
    <asp:Label ID="LblId3" runat="server" Text="" Font-Bold="True" Font-Names="Arial Narrow" Font-Size="Medium" ForeColor="Black"></asp:Label>

<asp:Panel ID="pnlConfirmation" runat="server"   Width="309px" Height="245px">
    <div class="panel panel-warning">
      <div class="panel-heading" style="text-align: right"><asp:Label ID="lblTexto" runat="server" Text="Asociar recibo a una factura existente" Font-Bold="True" Font-Names="Arial Narrow" Font-Size="Medium" ForeColor="#666666"></asp:Label>
          &nbsp;<asp:Image ID="Image1" runat="server" ImageUrl="~/images/Alerta1.png" />
      </div>
      <div class="panel-body">
       <table >
        <tr>
            <td rowspan="1" style="color: #000000; font-size: 14px; align-content:center;">
               <asp:Label ID="lblmsgRecibo" runat="server" Text="La factura ## tiene saldo pendiente, desea asociar este recibo a esa factura?" Font-Bold="True" Font-Names="Arial Narrow" Font-Size="Medium" ForeColor="Black"></asp:Label>
            </td>
        </tr>
        <tr>
            <td rowspan="1" >
               
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="btnOK1" runat="server" Text="Si" OnClick="btnOK1_Click" CssClass="btn btn-danger" ToolTip="Al aceptar este recibo es asociado a la factura"  /> 
                &nbsp;
                <asp:Button ID="btncancel"
                    runat="server" Text="No"  OnClick="btncancel_Click" CssClass="btn btn-danger" ToolTip="Al cancelar, este recibo se registrar� como anticipo a una factura posterior" />
            </td>
        </tr>
    </table>
       
     </div>
    </div>

</asp:Panel>
<asp:Panel ID="pnlConfirmation2" runat="server"   Width="416px" Height="217px">
    <div class="panel panel-warning">
      <div class="panel-heading" style="align-items:end; text-align: right; font-family: 'Arial Narrow'; color: #666666; font-size: medium; font-weight: bold;">Confirmaci�n 
          <asp:Image ID="Image2" runat="server" ImageUrl="~/images/Confimacion1.png" />
      </div>
      <div class="auto-style4">
       <table >
        <tr>
            <td rowspan="1" style="align-content:center;" class="auto-style3">
                <asp:Label ID="lblConfAsociar" runat="server" Text="" Font-Bold="True" Font-Names="Arial Narrow" Font-Size="Medium" ForeColor="Black"></asp:Label></td>
        </tr>
        <tr>
            <td rowspan="1" >
               
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="btnOk2" runat="server" Text="Si, es correcto"  CssClass="btn btn-danger" ToolTip="Asociar" OnClick="btnOk2_Click"  /> 
                &nbsp;
                <asp:Button ID="btnCancel2"
                    runat="server" Text="No, revisar� primero"   CssClass="btn btn-danger" ToolTip="Prefiero verificar" OnClick="btnCancel2_Click" />
            </td>
        </tr>
    </table>
       
     </div>
    </div>

</asp:Panel>
    <asp:Panel ID="pnlConfirmation3" runat="server"   Width="415px" Height="163px">
    <div class="panel panel-warning">
      <div class="panel-heading" style="align-items: end; text-align: right; font-family: 'Arial Narrow'; color: #666666; font-size: medium; font-weight: bold;">Confirmaci�n ingreso de anticipo
          <asp:Image ID="Image3" runat="server" ImageUrl="~/images/Confimacion1.png" />
      </div>
      <div class="panel-body">
       <table >
        <tr>
            <td rowspan="1" style="align-content:center;" class="auto-style3">
                <asp:Label ID="lblIngresoAnticipo" runat="server" Font-Bold="True" Font-Names="Arial Narrow" Font-Size="Medium" ForeColor="Black"></asp:Label></td>
        </tr>
        <tr>
            <td rowspan="1" >
               
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="btnAceptarAnticipo" runat="server" Text="Si, es correcto"  CssClass="btn btn-danger" ToolTip="Asociar" OnClick="btnAceptarAnticipo_Click"  /> 
                &nbsp;
                <asp:Button ID="btnCancelarAnticipo"
                    runat="server" Text="No, revisar� primero"   CssClass="btn btn-danger" ToolTip="Prefiero verificar" OnClick="btnCancelarAnticipo_Click" />
            </td>
        </tr>
    </table>
       
     </div>
    </div>

</asp:Panel>
</asp:Content>
