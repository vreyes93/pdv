﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using MF_Clases;
using System.Net;
using System.Text;
using System.Web.UI;
using MF_Clases.Restful;
using Newtonsoft.Json;
using RestSharp;

namespace PuntoDeVenta
{
    public partial class recibos : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                ViewState["url"] = Convert.ToString(Session["Url"]);
                CargarBancos();
                CargarPoss();
                CargarRecibos();
                CargarCuentas();
                LimpiarCierre();
                LimpiarControles();

                txtObservacionesCierre.ToolTip = string.Format("Para justificar el descuadre de Visanet o Credomatic debe escribir el siguiente texto seguido de su justificación:{0}{0}Visanet no cuadra porque {0}Credomatic no cuadra porque {0}{0}Por ejemplo:{0}Visanet no cuadra porque digité mal los montos en el POS y Credomatic no cuadra porque redondié el precio", System.Environment.NewLine);

                List<wsPuntoVenta.LoteCierre> qVisanet = new List<wsPuntoVenta.LoteCierre>();
                List<wsPuntoVenta.LoteCierre> qCredomatic = new List<wsPuntoVenta.LoteCierre>();

                ViewState["qVisanet"] = qVisanet;
                ViewState["qCredomatic"] = qCredomatic;

                if (Session["Cliente"] == null)
                {
                    //mostrarBusquedaCliente();
                }
                else
                {
                    CargarCliente("C", Convert.ToString(Session["Cliente"]), "I");
                }
            }
        }

        void LimpiarControles()
        {
            cbFechaBoletaConfirmar.Value = DateTime.Now.Date;
        }

        void CargarBancos()
        {
            try
            {
                
                lbInfoRecibo.Text = "";
                lbErrorRecibo.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var q = ws.DevuelveBancos().ToList();

                cbChequeBanco.DataSource = q;
                cbChequeBanco.DataTextField = "Descripcion";
                cbChequeBanco.DataValueField = "Banco";
                cbChequeBanco.DataBind();

                cbChequeBanco.SelectedValue = "ND";
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorRecibo.Text = string.Format("Error al cargar los bancos {0} {1}", ex.Message, m);
            }
        }

        void CargarPoss()
        {
            try
            {
               
                lbInfoRecibo.Text = "";
                lbErrorRecibo.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var q = ws.DevuelvePoss().ToList();

                cbPos.DataSource = q;
                cbPos.DataTextField = "Nombre";
                cbPos.DataValueField = "Pos";
                cbPos.DataBind();

                cbPos.SelectedValue = "N";
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorRecibo.Text = string.Format("Error al cargar los Pos'''s {0} {1}", ex.Message, m);
            }
        }

        protected void txtCodigoBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtCodigoBuscar.Text.Trim().Length == 0) return;
            BuscarCliente("C");
        }

        protected void txtNombreBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtNombreBuscar.Text.Trim().Length == 0) return;

            string mTipo = "N";
            try
            {
                Int32 mNumero = Convert.ToInt32(txtNombreBuscar.Text.Substring(0, 1));
                mTipo = "C";
            }
            catch
            {
                mTipo = "N";
            }

            BuscarCliente(mTipo);
        }

        protected void txtNitBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtNitBuscar.Text.Trim().Length == 0) return;
            BuscarCliente("T");
        }

        protected void txtTelefono_TextChanged(object sender, EventArgs e)
        {
            if (txtTelefono.Text.Trim().Length == 0) return;
            BuscarCliente("E");
        }

        void ocultarBusqueda()
        {
            lbErrorCliente.Text = "";
            lbInfoCliente.Text = "";
            tblBuscar.Visible = false;
            gridClientes.Visible = false;
        }

        protected void lkOcultarBusqueda_Click(object sender, EventArgs e)
        {
            ocultarBusqueda();
        }

        protected void lbBuscarCliente_Click(object sender, EventArgs e)
        {
            mostrarBusquedaCliente();
        }

        void mostrarBusquedaCliente()
        {
            tblBuscar.Visible = true;

            lbError.Text = "";
            lbErrorCliente.Text = "";
            lbInfoCliente.Text = "";
            txtCodigoBuscar.Text = "";
            txtNombreBuscar.Text = "";
            txtNitBuscar.Text = "";

            txtNombreBuscar.Focus();
        }

        protected void gridClientes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            seleccionarCliente();
        }

        void BuscarCliente(string tipo)
        {
            lbErrorCliente.Text = "";
            lbInfoCliente.Text = "";

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            string mTipo = tipo;
            string mCodigo = txtCodigoBuscar.Text.Trim();
            string mNombre = txtNombreBuscar.Text.Trim().ToUpper();

            if (mTipo == "C" && mNombre.Trim().Length > 0)
            {
                mCodigo = mNombre;
                mNombre = "";
            }

            var q = ws.DevuelveClientes(tipo, mCodigo, mNombre, txtNitBuscar.Text.Trim().ToUpper(), txtTelefono.Text.Trim());

            gridClientes.DataSource = q;
            gridClientes.DataBind();
            gridClientes.Visible = true;
            gridClientes.SelectedIndex = -1;

            if (q.Length == 0 && tipo != "1" && tipo != "2" && tipo != "3") lbErrorCliente.Text = "No existen clientes con el criterio de búsqueda ingresado.";

            if (q.Count() == 1 && tipo != "1" && tipo != "2" && tipo != "3")
            {
                gridClientes.SelectedIndex = 0;
                seleccionarCliente();
            }
            if (q.Count() > 0)
            {
                gridClientes.Focus();
                gridClientes.SelectedIndex = 0;
                LinkButton lkSeleccionar = (LinkButton)gridClientes.SelectedRow.FindControl("lkSeleccionar");

                lkSeleccionar.Focus();
            }
        }

        void seleccionarCliente()
        {
            GridViewRow gvr = gridClientes.SelectedRow;
            CargarCliente("C", gvr.Cells[1].Text, "I");
        }

        void CargarCliente(string tipo, string cliente, string nit)
        {
            try
            {
                limpiarRecibo();

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var q = ws.DevuelveCliente(tipo, cliente, nit);

                txtCodigo.Text = q[0].Cliente;
                txtNombre.Text = q[0].Nombre;
                txtNit.Text = q[0].Nit;
                Session["Cliente"] = q[0].Cliente;

                ocultarBusqueda();
                CargarFacturasCliente(q[0].Cliente);

                txtEfectivo.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al cargar el cliente {0} {1}", ex.Message, m);
            }
        }

        void CargarFacturasCliente(string cliente)
        {
            //var ws = new wsPuntoVenta.wsPuntoVenta();
            //if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            //var qFacturas = ws.DevuelveFacturasCliente(cliente).ToList();
            List<Clases.DocumentoConSaldo> mRespuesta = new List<Clases.DocumentoConSaldo>();
            List<string> lstResult = new List<string>();
            try
            {
                WebRequest request = WebRequest.Create(string.Format("{0}/Saldos/{1}", General.FiestaNetRestService, cliente));
                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();


                mRespuesta = JsonConvert.DeserializeObject<List<Clases.DocumentoConSaldo>>(responseString);


            }
            catch
            {
            }

            Clases.DocumentoConSaldo itemFactura = new Clases.DocumentoConSaldo();


            itemFactura.Documento = "Z";
            itemFactura.Saldo = 0;
            itemFactura.Descripcion = "Recibo para registrar un anticipo";
            mRespuesta.Add(itemFactura);

            cbAplicar.DataSource = mRespuesta;
            cbAplicar.DataTextField = "Descripcion";
            cbAplicar.DataValueField = "Documento";
            cbAplicar.DataBind();
        }

        void limpiarRecibo()
        {
            try
            {
                
                lbInfoTasa.Text = "";
                lbInfo.Text = "";
                lbError.Text = "";
                lbInfoRecibo.Text = "";
                lbErrorRecibo.Text = "";
                lkImprimirRecibo.Visible = false;
                ViewState["Recibo"] = null;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                ocultarBusqueda();

                cbAplicar.Items.Clear();
                LimpiarCamposRecibo();
                cbMoneda.SelectedValue = "Q";

                CargarRecibos();
                txtEfectivo.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorRecibo.Text = string.Format("Error al inicializar la página {0} {1}", ex.Message, m);
            }
        }

        protected void lkLimpiarRecibo_Click(object sender, EventArgs e)
        {
            limpiarRecibo();
            CargarFacturasCliente(txtCodigo.Text);
        }

        bool PasaValidacionesRecibo()
        {
            try
            {
                
                lbInfoRecibo.Text = "";
                lbErrorRecibo.Text = "";

                if (txtCodigo.Text.Trim().Length == 0)
                {
                    lbErrorRecibo.Text = "Debe seleccionar un cliente";
                    mostrarBusquedaCliente();
                    return false;
                }

                if (txtMontoRecibir.Text.Trim().Length == 0) txtMontoRecibir.Text = "0.00";

                decimal mMonto = 0;
                try
                {
                    mMonto = Convert.ToDecimal(txtMontoRecibir.Text);
                }
                catch
                {
                    mMonto = 0;
                }

                if (mMonto == 0)
                {
                    lbErrorRecibo.Text = "Debe ingresar al menos una forma de pago";
                    txtEfectivo.Focus();
                    return false;
                }

                if (Convert.ToDecimal(txtTarjeta.Text) > 0)
                {
                    if (cbPos.SelectedValue == "N")
                    {
                        lbErrorRecibo.Text = "Debe indicar en que POS pasará la tarjeta";
                        cbPos.Focus();
                        return false;
                    }
                    if (cbChequeBanco.SelectedValue == "ND")
                    {
                        lbErrorRecibo.Text = "Debe indicar el banco emisor de la tarjeta de crédito o débito";
                        cbChequeBanco.Focus();
                        return false;
                    }
                }

                if (Convert.ToDecimal(txtCheque.Text) > 0)
                {
                    if (txtChequeNumero.Text.Trim().Length == 0)
                    {
                        lbErrorRecibo.Text = "Debe ingresar el número de cheque";
                        txtChequeNumero.Focus();
                        return false;
                    }
                    if (cbChequeBanco.SelectedValue == "ND")
                    {
                        lbErrorRecibo.Text = "Debe indicar el banco emisor del cheque";
                        cbChequeBanco.Focus();
                        return false;
                    }
                }

                decimal mEfectivo = Convert.ToDecimal(txtEfectivo.Text);
                decimal mCheque = Convert.ToDecimal(txtCheque.Text);
                decimal mTarjeta = Convert.ToDecimal(txtTarjeta.Text);

                if (mTarjeta > 0)
                {
                    if (mEfectivo > 0 || mCheque > 0)
                    {
                        lbErrorRecibo.Text = "Los pagos de tarjeta no se pueden mezclar con efectivo y/o cheque, debe utilizar un recibo aparte para tarjeta";
                        txtTarjeta.Focus();
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorRecibo.Text = string.Format("Error al validar los datos {0} {1}", ex.Message, m);
                return false;
            }
        }

        private void GrabarRecibo(string pFactura = "")
        {
            try

            {

                string mRecibo = "";
                string mMensaje = "";

                wsPuntoVenta.Recibos[] q = new wsPuntoVenta.Recibos[1];


                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (General.Ambiente == "PRO" || General.Ambiente == "PRU")
                    ws.Url = Convert.ToString(ViewState["url"]);

                decimal mTasa = 1;
                if (cbMoneda.SelectedValue == "$") mTasa = Convert.ToDecimal(txtTasaCambio.Text);

                wsPuntoVenta.Recibos item = new wsPuntoVenta.Recibos();
                item.Documento = "";
                item.Tipo = "";
                item.Cliente = txtCodigo.Text;
                item.Fecha = DateTime.Now.Date;
                item.Cobrador = "";
                item.Vendedor = "";
                item.Factura = pFactura != string.Empty ? pFactura : cbAplicar.SelectedValue;
                item.Efectivo = Convert.ToDecimal(txtEfectivo.Text.Replace(" ", "").Replace(",", "").Replace("Q.", "").Replace("Q", "").Replace("q.", "").Replace("q", ""));
                item.Cheque = Convert.ToDecimal(txtCheque.Text.Replace(" ", "").Replace(",", "").Replace("Q.", "").Replace("Q", "").Replace("q.", "").Replace("q", ""));
                item.Tarjeta = Convert.ToDecimal(txtTarjeta.Text.Replace(" ", "").Replace(",", "").Replace("Q.", "").Replace("Q", "").Replace("q.", "").Replace("q", ""));
              
                item.Monto = Convert.ToDecimal(txtMontoRecibir.Text.Replace(" ", "").Replace(",", "").Replace("Q.", "").Replace("Q", "").Replace("q.", "").Replace("q", ""));
                item.NumeroCheque = txtChequeNumero.Text;
                item.EntidadFinancieraCheque = cbChequeBanco.SelectedValue;
                item.EsAnticipo = cbAplicar.SelectedValue == "Z" && pFactura == "" ? "S" : "N";
                item.Deposito = 0;
                item.EntidadFinancieraDeposito = "ND";
                item.EntidadFinancieraTarjeta = "ND";
                item.EmisorTarjeta = 0;
                item.Observaciones = txtObservacionesRecibo.Text.Trim().ToUpper();
                item.Pos = cbPos.SelectedValue;
                item.Moneda = cbMoneda.SelectedValue;
                item.Tasa = mTasa;
                q[0] = item;




                if (!ws.GrabarRecibo(ref mRecibo, ref mMensaje, q, Convert.ToString(Session["Usuario"]), Convert.ToString(Session["Tienda"]), Convert.ToString(Session["Vendedor"])))
                {

                    lbErrorRecibo.Text = mMensaje;
                    return;
                }
               

                    limpiarRecibo();
                CargarFacturasCliente(txtCodigo.Text);

                lkImprimirRecibo.Visible = true;
                
                lbInfoRecibo.Text = mMensaje;
                ViewState["Recibo"] = mRecibo;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorRecibo.Text = string.Format("Error al inicializar la página {0} {1}", ex.Message, m);
            }

        }

        protected void lkGrabarRecibo_Click(object sender, EventArgs e)
        {

            lbInfoRecibo.Text = "";
            lbErrorRecibo.Text = "";

            if (PasaValidacionesRecibo())
            {
                if (cbAplicar.Items.Count == 1 || cbAplicar.SelectedValue != "Z")
                    GrabarRecibo();
                else
                {
                    if (cbAplicar.Items.Count > 1 && cbAplicar.SelectedValue == "Z")
                        lblmsgRecibo.Text = $"El Cliente {txtNombre.Text} posee la {cbAplicar.Items[0].Text} <br/>¿desea asociar el recibo a esta factura? ";
                    mdlRecibos.Show();
                }
            }


        }
        

        void ImprimirRecibo(string recibo)
        {
            try
            {
               
                lbInfoRecibo.Text = "";
                lbErrorRecibo.Text = "";

                string mMensaje = "";
                var ws = new wsPuntoVenta.wsPuntoVenta();

                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var q = ws.DevuelveImpresionRecibo(recibo, ref mMensaje);

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorRecibo.Text = mMensaje;
                    return;
                }

                dsPuntoVenta ds = new dsPuntoVenta();

                DataRow row = ds.Recibos.NewRow();
                row["Documento"] = q[0].Documento;
                row["Tipo"] = q[0].Tipo;
                row["Cliente"] = q[0].Cliente;
                row["NombreCliente"] = q[0].NombreCliente;
                row["Fecha"] = q[0].Fecha;
                row["Cobrador"] = q[0].Cobrador;
                row["Vendedor"] = q[0].Vendedor;
                row["Factura"] = q[0].Factura;
                row["Efectivo"] = q[0].Efectivo;
                row["Cheque"] = q[0].Cheque;
                row["Tarjeta"] = q[0].Tarjeta;
                row["Monto"] = q[0].Monto;
                row["NumeroCheque"] = q[0].NumeroCheque;
                row["EntidadFinancieraCheque"] = q[0].EntidadFinancieraCheque;
                row["NombreBancoCheque"] = q[0].NombreBancoCheque;
                row["EmisorTarjeta"] = q[0].EmisorTarjeta;
                row["EntidadFinancieraTarjeta"] = q[0].EntidadFinancieraTarjeta;
                row["EsAnticipo"] = q[0].EsAnticipo;
                row["Deposito"] = q[0].Deposito;
                row["EntidadFinancieraDeposito"] = q[0].EntidadFinancieraDeposito;
                row["Observaciones"] = q[0].Observaciones;
                row["MontoLetras"] = q[0].MontoLetras;
                row["Direccion"] = q[0].Direccion;
                row["Telefono"] = q[0].Telefono;
                row["DireccionEmpresa"] = q[0].DireccionEmpresa;
                row["Registro"] = 1;
                row["Usuario"] = q[0].Usuario;
                row["FechaRegistro"] = q[0].FechaRegistro;
                row["TasaCambio"] = q[0].Tasa;
                row["Moneda"] = q[0].Moneda;
                row["SaldoFactura"] = q[0].SaldoFactura;
                ds.Recibos.Rows.Add(row);

                DataRow row2 = ds.Recibos.NewRow();
                row2["Documento"] = q[0].Documento;
                row2["Tipo"] = q[0].Tipo;
                row2["Cliente"] = q[0].Cliente;
                row2["NombreCliente"] = q[0].NombreCliente;
                row2["Fecha"] = q[0].Fecha;
                row2["Cobrador"] = q[0].Cobrador;
                row2["Vendedor"] = q[0].Vendedor;
                row2["Factura"] = q[0].Factura;
                row2["Efectivo"] = q[0].Efectivo;
                row2["Cheque"] = q[0].Cheque;
                row2["Tarjeta"] = q[0].Tarjeta;
                row2["Monto"] = q[0].Monto;
                row2["NumeroCheque"] = q[0].NumeroCheque;
                row2["EntidadFinancieraCheque"] = q[0].EntidadFinancieraCheque;
                row2["NombreBancoCheque"] = q[0].NombreBancoCheque;
                row2["EmisorTarjeta"] = q[0].EmisorTarjeta;
                row2["EntidadFinancieraTarjeta"] = q[0].EntidadFinancieraTarjeta;
                row2["EsAnticipo"] = q[0].EsAnticipo;
                row2["Deposito"] = q[0].Deposito;
                row2["EntidadFinancieraDeposito"] = q[0].EntidadFinancieraDeposito;
                row2["Observaciones"] = q[0].Observaciones;
                row2["MontoLetras"] = q[0].MontoLetras;
                row2["Direccion"] = q[0].Direccion;
                row2["Telefono"] = q[0].Telefono;
                row2["DireccionEmpresa"] = q[0].DireccionEmpresa;
                row2["Registro"] = 2;
                row2["Usuario"] = q[0].Usuario;
                row2["FechaRegistro"] = q[0].FechaRegistro;
                row2["TasaCambio"] = q[0].Tasa;
                row2["Moneda"] = q[0].Moneda;
                row2["SaldoFactura"] = q[0].SaldoFactura;
                ds.Recibos.Rows.Add(row2);

                using (ReportDocument reporte = new ReportDocument())
                {
                    string mFormatoRecibo = "rptReciboPV.rpt";

                    string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mFormatoRecibo);
                    reporte.Load(p);

                    string mNombreDocumento = string.Format("{0}.pdf", recibo);
                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                    reporte.SetDataSource(ds);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorRecibo.Text = string.Format("Error al imprimir el {0} {1} {2}", ex.Message, m, recibo);
            }
        }

        protected void txtEfectivo_TextChanged(object sender, EventArgs e)
        {
            CalcularMonto();
        }

        protected void txtTarjeta_TextChanged(object sender, EventArgs e)
        {
            CalcularMonto();
        }

        protected void txtCheque_TextChanged(object sender, EventArgs e)
        {
            CalcularMonto();
        }

        void CalcularMonto()
        {
            try
            {
               
                lbInfoRecibo.Text = "";
                lbErrorRecibo.Text = "";

                decimal mEfectivo = 0;
                decimal mCheque = 0;
                decimal mTarjeta = 0;
                decimal mvalorVale = 0;

                txtMontoRecibir.Text = "0.00";
                txtEfectivo.Text = txtEfectivo.Text.Replace(" ", "").Replace(",", "").Replace("Q.", "").Replace("Q", "").Replace("q.", "").Replace("q", "").Replace("$", "").Trim();
                txtTarjeta.Text = txtTarjeta.Text.Replace(" ", "").Replace(",", "").Replace("Q.", "").Replace("Q", "").Replace("q.", "").Replace("q", "").Replace("$", "").Trim();
                txtCheque.Text = txtCheque.Text.Replace(" ", "").Replace(",", "").Replace("Q.", "").Replace("Q", "").Replace("q.", "").Replace("q", "").Replace("$", "").Trim();
               

                if (txtEfectivo.Text.Trim().Length == 0) txtEfectivo.Text = "0.00";
                if (txtTarjeta.Text.Trim().Length == 0) txtTarjeta.Text = "0.00";
                if (txtCheque.Text.Trim().Length == 0) txtCheque.Text = "0.00";
               

                try
                {
                    mEfectivo = Convert.ToDecimal(txtEfectivo.Text);
                }
                catch
                {
                    lbErrorRecibo.Text = "El valor en efectivo es inválido";
                    txtEfectivo.Focus();
                    return;
                }
                try
                {
                    mTarjeta = Convert.ToDecimal(txtTarjeta.Text);
                }
                catch
                {
                    lbErrorRecibo.Text = "El valor en tarjeta es inválido";
                    cbPos.SelectedValue = "N";
                    txtTarjeta.Focus();
                    return;
                }
                try
                {
                    mCheque = Convert.ToDecimal(txtCheque.Text);
                }
                catch
                {
                    lbErrorRecibo.Text = "El valor en cheque es inválido";
                    cbChequeBanco.SelectedValue = "ND";
                    txtCheque.Focus();
                    return;
                }

                if (mTarjeta == 0) cbPos.SelectedValue = "N";
                if (mCheque == 0) cbChequeBanco.SelectedValue = "ND";


                decimal mTasa = 1;
                if (cbMoneda.SelectedValue == "$")
                    mTasa = Convert.ToDecimal(txtTasaCambio.Text);

                decimal mMonto = Math.Round((mEfectivo + mTarjeta + mCheque+ mvalorVale) * mTasa, 2);
                txtMontoRecibir.Text = String.Format("{0:0,0.00}", mMonto);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorRecibo.Text = string.Format("Error al calcular el pago {0} {1}", ex.Message, m);
            }
        }

        protected void lkImprimirRecibo_Click(object sender, EventArgs e)
        {
            try
            {
                
                lbInfoRecibo.Text = "";
                lbErrorRecibo.Text = "";

                string mRecibo = "";

                try
                {
                    mRecibo = ViewState["Recibo"].ToString().ToUpper();
                }
                catch
                {
                    lbErrorRecibo.Text = "No hay ningún recibo para imprimir";
                    return;
                }

                if (mRecibo.Length == 0)
                {
                    lbErrorRecibo.Text = "No hay ningún recibo para imprimir";
                    return;
                }

                ImprimirRecibo(mRecibo);
               
                lbInfoRecibo.Text = "El recibo se imprimió exitosamente";
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    //Nothing
                }
                lbErrorRecibo.Text = string.Format("Error al imprimir el recibo {0} {1}", ex.Message, m);
            }
        }

        void CargarRecibos()
        {
            try
            {
                lbInfoDeposito.Text = "";
                lbErrorDeposito.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var q = ws.DevuelveRecibosDepositar(Convert.ToString(Session["Tienda"]), Convert.ToString(Session["usuario"])).ToList();

                gridRecibos.DataSource = q;
                gridRecibos.DataBind();
                gridRecibos.Visible = true;
                gridRecibos.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorDeposito.Text = string.Format("Error al cargar los recibos {0} {1}", ex.Message, m);
            }
        }

        void CargarCuentas()
        {
            try
            {
                lbInfoDeposito.Text = "";
                lbErrorDeposito.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var q = ws.DevuelveCuentasBancarias().ToList();

                cbCuenta.DataSource = q;
                cbCuenta.DataTextField = "Nombre";
                cbCuenta.DataValueField = "Cuenta";
                cbCuenta.DataBind();

                cbChequeBanco.SelectedValue = "ND";

                cbBancoBoletaConfirmar.DataSource = q;
                cbBancoBoletaConfirmar.ValueField = "Cuenta";
                cbBancoBoletaConfirmar.ValueType = typeof(System.String);
                cbBancoBoletaConfirmar.TextField = "Nombre";
                cbBancoBoletaConfirmar.DataBindItems();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorDeposito.Text = string.Format("Error al cargar las cuentas bancarias {0} {1}", ex.Message, m);
            }
        }

        protected void gridRecibos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridRecibos_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lbInfoDeposito.Text = "";
                lbErrorDeposito.Text = "";


            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorDeposito.Text = string.Format("Error {0} {1}", ex.Message, m);
            }
        }

        void limpiarDeposito()
        {
            try
            {
                lbInfoDeposito.Text = "";
                lbErrorDeposito.Text = "";
                txtComision.Text = "";

                lbComision.Visible = false;
                txtComision.Visible = false;

                CargarRecibos();

                txtDeposito.Text = "";
                txtBoleta.Text = "";
                cbDeposito.Checked = false;
                cbAceptaCuenta.Checked = false;
                cbCuenta.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorDeposito.Text = string.Format("Error al inicializar los datos del depósito {0} {1}", ex.Message, m);
            }
        }

        protected void lkLimpiarDeposito_Click(object sender, EventArgs e)
        {
            limpiarDeposito();
        }

        bool PasaValidacionesDeposito()
        {
            try
            {
                lbInfoDeposito.Text = "";
                lbErrorDeposito.Text = "";

                decimal mDeposito = 0;
                decimal mValor = ValorDeposito(true);

                if (mValor == 0)
                {
                    lbErrorDeposito.Text = "Debe seleccionar al menos un recibo";
                    txtDeposito.Focus();
                    return false;
                }

                try
                {
                    mDeposito = Convert.ToDecimal(txtDeposito.Text.Replace(",", "").Replace("Q.", "").Replace("Q", "").Replace("q.", "").Replace("q", "").Replace(" ", ""));
                }
                catch
                {
                    lbErrorDeposito.Text = "El valor a depositar es inválido";
                    txtDeposito.Focus();
                    return false;
                }

                if (mDeposito < mValor)
                {
                    lbErrorDeposito.Text = "El valor a depositar debe ser mayor o igual al total de los recibos";
                    txtDeposito.Focus();
                    return false;
                }
                if (txtBoleta.Text.Trim().Length == 0)
                {
                    lbErrorDeposito.Text = "Debe ingresar el número de boleta";
                    txtBoleta.Focus();
                    return false;
                }

                if (!cbDeposito.Checked)
                {
                    lbErrorDeposito.Text = "Debe aceptar el valor a depositar";
                    cbDeposito.Focus();
                    return false;
                }
                if (!cbAceptaCuenta.Checked)
                {
                    lbErrorDeposito.Text = "Debe aceptar la cuenta a depositar";
                    cbAceptaCuenta.Focus();
                    return false;
                }

                if (txtComision.Visible)
                {
                    if (txtComision.Text.Trim().Length == 0)
                    {
                        lbErrorDeposito.Text = "Debe ingresar el valor de la comisión";
                        txtComision.Focus();
                        return false;
                    }

                    decimal mComision = 0;
                    try
                    {
                        mComision = Convert.ToDecimal(txtComision.Text.Replace(",", "").Replace("Q.", "").Replace("Q", "").Replace("q.", "").Replace("q", "").Replace(" ", ""));
                    }
                    catch
                    {
                        lbErrorDeposito.Text = "El valor de comisión ingresado es inválido";
                        txtComision.Focus();
                        return false;
                    }

                    if (mComision >= mDeposito)
                    {
                        lbErrorDeposito.Text = "El valor de comisión ingresado es inválido";
                        txtComision.Focus();
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorDeposito.Text = string.Format("Error al validar los datos del depósito {0} {1}", ex.Message, m);
                return false;
            }

            return true;
        }

        protected void lkGrabarDeposito_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoDeposito.Text = "";
                lbErrorDeposito.Text = "";

                if (!PasaValidacionesDeposito()) return;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mMensaje = "";
                wsPuntoVenta.RecibosDepositar[] q = new wsPuntoVenta.RecibosDepositar[1];

                int jj = 0;
                for (int ii = 0; ii < gridRecibos.Rows.Count; ii++)
                {
                    CheckBox cbRecibo = (CheckBox)gridRecibos.Rows[ii].FindControl("cbRecibo");
                    Label lbRecibo = (Label)gridRecibos.Rows[ii].FindControl("lbRecibo");

                    if (cbRecibo.Checked)
                    {
                        jj++;
                        wsPuntoVenta.RecibosDepositar item = new wsPuntoVenta.RecibosDepositar();
                        item.Recibo = lbRecibo.Text;
                        item.Fecha = DateTime.Now.Date;
                        item.Efectivo = 0;
                        item.Cheque = 0;
                        item.Tarjeta = 0;
                        item.Pos = "";

                        if (jj > 1) Array.Resize(ref q, jj);
                        q[jj - 1] = item;
                    }
                }

                if (!ws.GrabarDeposito(txtBoleta.Text, txtDeposito.Text, cbCuenta.SelectedValue, ref mMensaje, q, Session["usuario"].ToString(), Session["Tienda"].ToString(), txtComision.Text, txtFecha.Text))
                {
                    lbErrorDeposito.Text = mMensaje;
                    return;
                }

                limpiarDeposito();
                lbInfoDeposito.Text = mMensaje;
                CargarFacturasCliente(txtCodigo.Text);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorDeposito.Text = string.Format("Error al grabar el depósito depósito {0} {1}", ex.Message, m);
            }
        }

        protected void cbRecibo_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                lbInfoDeposito.Text = "";
                lbErrorDeposito.Text = "";

                decimal mValor = ValorDeposito(false);
                txtDeposito.Text = String.Format("{0:0,0.00}", mValor);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorDeposito.Text = string.Format("Error al sumar los valores de los recibos {0} {1}", ex.Message, m);
            }
        }

        decimal ValorDeposito(bool grabando)
        {
            lbFecha.Visible = false;
            lbComision.Visible = false;

            txtFecha.Visible = false;
            txtComision.Visible = false;

            decimal mValor = 0;
            for (int ii = 0; ii < gridRecibos.Rows.Count; ii++)
            {
                CheckBox cbRecibo = (CheckBox)gridRecibos.Rows[ii].FindControl("cbRecibo");
                Label lbEfectivo = (Label)gridRecibos.Rows[ii].FindControl("lbEfectivo");
                Label lbCheque = (Label)gridRecibos.Rows[ii].FindControl("lbCheque");
                Label lbTarjeta = (Label)gridRecibos.Rows[ii].FindControl("lbTarjeta");

                if (cbRecibo.Checked)
                {
                    decimal mEfectivo = Convert.ToDecimal(lbEfectivo.Text.Replace(",", ""));
                    decimal mCheque = Convert.ToDecimal(lbCheque.Text.Replace(",", ""));
                    decimal mTarjeta = Convert.ToDecimal(lbTarjeta.Text.Replace(",", ""));

                    if (mTarjeta > 0)
                    {
                        lbFecha.Visible = true;
                        lbComision.Visible = true;

                        txtFecha.Visible = true;
                        txtComision.Visible = true;

                        if (!grabando) txtComision.Text = "";
                    }

                    mValor = mValor + mEfectivo + mCheque + mTarjeta;
                }
            }

            return mValor;
        }

        protected void lkReimprimirRecibo_Click(object sender, EventArgs e)
        {
            try
            {
                lbError.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.ExisteRecibo(txtReciboReimprimir.Text.Trim().ToUpper()))
                {
                    lbError.Text = string.Format("El Recibo No. {0} no existe", txtReciboReimprimir.Text.Trim().ToUpper());
                    return;
                }

                ImprimirRecibo(txtReciboReimprimir.Text.ToUpper());
                txtReciboReimprimir.Text = "";
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    //Nothing
                }
                lbError.Text = string.Format("Error al reimprimir el recibo {0} {1}", ex.Message, m);
            }
        }

        protected void lkAnularRecibo_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoAnularRecibo.Text = "";
                lbErrorAnularRecibo.Text = "";
                lbInfoAnularRecibo.Visible = false;
                lbErrorAnularRecibo.Visible = false;

                if (txtReciboAnularObservaciones.Text.Trim().Length == 0)
                {
                    lbErrorAnularRecibo.Visible = true;
                    lbErrorAnularRecibo.Text = "Debe ingresar el motivo de la anulación del recibo";
                    txtReciboAnularObservaciones.Focus();
                    return;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (General.Ambiente != "DES")
                    ws.Url = General.FiestaNETService;

                string mMensaje = "";
                if (!ws.AnularRecibo(txtReciboAnular.Text, ref mMensaje, Convert.ToString(Session["Usuario"]), txtReciboAnularObservaciones.Text.ToUpper(), Convert.ToString(Session["Vendedor"]), Convert.ToString(Session["Tienda"])))
                {
                    lbErrorAnularRecibo.Visible = true;
                    lbErrorAnularRecibo.Text = mMensaje;
                    return;
                }

                lbInfoAnularRecibo.Visible = true;
                lbInfoAnularRecibo.Text = mMensaje;

                txtReciboAnular.Text = "";
                txtReciboAnularObservaciones.Text = "";
                CargarRecibos();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorAnularRecibo.Visible = true;
                lbErrorAnularRecibo.Text = string.Format("Error al anular el recibo. {0} {1}", ex.Message, m);
            }

        }

        protected void gridRecibosCierre_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridRecibosCierre_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lbInfoCorteCaja.Text = "";
                lbErrorCorteCaja.Text = "";
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorCorteCaja.Text = string.Format("Error {0} {1}", ex.Message, m);
            }
        }

        protected void gridDepositosCierre_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridDepositosCierre_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lbInfoCorteCaja.Text = "";
                lbErrorCorteCaja.Text = "";


            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorCorteCaja.Text = string.Format("Error {0} {1}", ex.Message, m);
            }
        }

        protected void gridDepositosCierrePendientes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridDepositosCierrePendientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lbInfoCorteCaja.Text = "";
                lbErrorCorteCaja.Text = "";
            }
            catch (Exception ex)
            {
                string m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));

                lbErrorCorteCaja.Text = string.Format("Error {0} {1}", ex.Message, m);
            }
        }

        decimal ValorRecibosCierre()
        {
            decimal mValor = 0;
            for (int ii = 0; ii < gridRecibosCierre.Rows.Count; ii++)
            {
                CheckBox cbRecibo = (CheckBox)gridRecibosCierre.Rows[ii].FindControl("cbReciboCierre");
                Label lbEfectivo = (Label)gridRecibosCierre.Rows[ii].FindControl("lbEfectivo");
                Label lbCheque = (Label)gridRecibosCierre.Rows[ii].FindControl("lbCheque");
                Label lbTarjeta = (Label)gridRecibosCierre.Rows[ii].FindControl("lbTarjeta");

                if (cbRecibo.Checked)
                {
                    decimal mEfectivo = Convert.ToDecimal(lbEfectivo.Text.Replace(",", ""));
                    decimal mCheque = Convert.ToDecimal(lbCheque.Text.Replace(",", ""));
                    decimal mTarjeta = Convert.ToDecimal(lbTarjeta.Text.Replace(",", ""));

                    mValor = mValor + mEfectivo + mCheque;
                }
            }

            return mValor;
        }

        decimal ValorDepositosCierre()
        {
            decimal mValor = 0;
            for (int ii = 0; ii < gridDepositosCierre.Rows.Count; ii++)
            {
                CheckBox cbDeposito = (CheckBox)gridDepositosCierre.Rows[ii].FindControl("cbDepositoCierre");
                Label lbMonto = (Label)gridDepositosCierre.Rows[ii].FindControl("lbMonto");

                if (cbDeposito.Checked)
                {
                    decimal mMonto = Convert.ToDecimal(lbMonto.Text.Replace(",", ""));

                    mValor = mValor + mMonto;
                }
            }

            return mValor;
        }

        void ValorDiferencia()
        {
            decimal mRecibos = 0; decimal mDepositos = 0;

            if (txtRecibosCierre.Text.Trim().Length == 0)
            {
                mRecibos = 0;
            }
            else
            {
                mRecibos = Convert.ToDecimal(txtRecibosCierre.Text.Replace(",", ""));
            }
            if (txtDepositosCierre.Text.Trim().Length == 0)
            {
                mDepositos = 0;
            }
            else
            {
                mDepositos = Convert.ToDecimal(txtDepositosCierre.Text.Replace(",", ""));
            }

            txtDiferenciaCierre.Text = String.Format("{0:0,0.00}", mRecibos - mDepositos);
        }

        protected void cbReciboCierre_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                lbInfoCorteCaja.Text = "";
                lbErrorCorteCaja.Text = "";

                decimal mValor = ValorRecibosCierre();
                txtRecibosCierre.Text = String.Format("{0:0,0.00}", mValor);

                ValorDiferencia();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorCorteCaja.Text = string.Format("Error {0} {1}", ex.Message, m);
            }
        }

        protected void cbDepositoCierre_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                lbInfoCorteCaja.Text = "";
                lbErrorCorteCaja.Text = "";

                decimal mValor = ValorDepositosCierre();
                txtDepositosCierre.Text = String.Format("{0:0,0.00}", mValor);

                ValorDiferencia();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorCorteCaja.Text = string.Format("Error {0} {1}", ex.Message, m);
            }
        }

        protected void cbDepositoCierrePendiente_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                lbInfoCorteCaja.Text = "";
                lbErrorCorteCaja.Text = "";

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorCorteCaja.Text = string.Format("Error {0} {1}", ex.Message, m);
            }
        }

        protected void lkDocuementosCierre_Click(object sender, EventArgs e)
        {
            LimpiarCierre();
        }

        void LimpiarCierre()
        {
            try
            {
                lbInfoCorteCaja.Text = "";
                lbErrorCorteCaja.Text = "";

                bool mCajeroF01 = false;
                string mTienda = Convert.ToString(Session["Tienda"]);

                var ws = new wsPuntoVenta.wsPuntoVenta();
                ws.Url = General.FiestaNETService;

                var q = ws.DevuelveRecibosCerrar(Convert.ToString(Session["Tienda"]), Convert.ToString(Session["usuario"]), ref mCajeroF01).ToList();
                var qDep = ws.DevuelveDepositosCerrar(Convert.ToString(Session["Tienda"]), Convert.ToString(Session["usuario"])).ToList();
                var qBodegas = ws.DevuelveTiendas();

                gridRecibosCierre.DataSource = q;
                gridRecibosCierre.DataBind();
                gridRecibosCierre.Visible = true;
                gridRecibosCierre.SelectedIndex = -1;

                gridDepositosCierre.DataSource = qDep;
                gridDepositosCierre.DataBind();
                gridDepositosCierre.Visible = true;
                gridDepositosCierre.SelectedIndex = -1;

                cbTiendaCierre.DataSource = qBodegas;
                cbTiendaCierre.DataTextField = "Tienda";
                cbTiendaCierre.DataValueField = "Tienda";
                cbTiendaCierre.DataBind();

                txtAnioCierre.Text = DateTime.Now.Year.ToString();

                try
                {
                    cbTiendaCierre.SelectedValue = mTienda;
                }
                catch (Exception ex)
                {
                    log.Error("Error al seleccionar tienda en recibos, para el cierre de caja, la tienda requerida era: " + mTienda + ", error: " + ex.Message);
                    cbTiendaCierre.SelectedValue = "F01";
                }

                txtAnioCierre.ReadOnly = false;
                txtAnioCierre.AutoPostBack = true;

                cbTiendaCierre.Enabled = false;
                cbTiendaCierre.AutoPostBack = false;

                if (mCajeroF01)
                {
                    cbTiendaCierre.Enabled = true;
                    cbTiendaCierre.AutoPostBack = true;
                }

                List<wsPuntoVenta.LoteCierre> qVisanet = new List<wsPuntoVenta.LoteCierre>();
                List<wsPuntoVenta.LoteCierre> qCredomatic = new List<wsPuntoVenta.LoteCierre>();

                gridVisanet.DataSource = qVisanet;
                gridVisanet.DataBind();

                gridCredomatic.DataSource = qCredomatic;
                gridCredomatic.DataBind();

                ViewState["qVisanet"] = qVisanet;
                ViewState["qCredomatic"] = qCredomatic;

                txtCierre.Text = "";
                txtRecibosCierre.Text = "";
                txtDepositosCierre.Text = "";
                txtDiferenciaCierre.Text = "";
                txtObservacionesCierre.Text = "";
                txtLoteVisanet.Text = "";
                txtMontoVisanet.Text = "";
                txtLoteCredomatic.Text = "";
                txtMontoCredomatic.Text = "";

                if (gridRecibosCierre.Rows.Count == 0 && gridDepositosCierre.Rows.Count == 0) lbErrorCorteCaja.Text = "No hay documentos pendientes de cerrar";
                CargarCierres();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorCorteCaja.Text = string.Format("Error al cargar los documentos a cerrar {0} {1}", ex.Message, m);
            }
        }

        protected void lkAgregarVisanet_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoCorteCaja.Text = "";
                lbErrorCorteCaja.Text = "";

                decimal mMonto = 0; int mLote = 0;

                if (txtLoteVisanet.Text.Trim().Length == 0)
                {
                    lbErrorCorteCaja.Text = "Debe ingresar un lote válido";
                    txtLoteVisanet.Focus();
                    return;
                }

                try
                {
                    mLote = Convert.ToInt32(txtLoteVisanet.Text.Replace(",", ""));
                }
                catch
                {
                    lbErrorCorteCaja.Text = "Debe ingresar un lote válido";
                    txtLoteVisanet.Focus();
                    return;
                }

                if (mLote <= 0)
                {
                    lbErrorCorteCaja.Text = "Debe ingresar un lote válido";
                    txtLoteVisanet.Focus();
                    return;
                }

                foreach (GridViewRow gvr in gridVisanet.Rows)
                {
                    Label lbLote = (Label)gvr.FindControl("lbLote");

                    if (lbLote.Text == mLote.ToString())
                    {
                        lbErrorCorteCaja.Text = "El lote ya existe en este cierre";
                        txtLoteVisanet.Focus();
                        return;
                    }
                }

                if (txtMontoVisanet.Text.Trim().Length == 0)
                {
                    lbErrorCorteCaja.Text = "Debe ingresar un monto válido";
                    txtMontoVisanet.Focus();
                    return;
                }

                try
                {
                    mMonto = Convert.ToDecimal(txtMontoVisanet.Text.Replace(",", ""));
                }
                catch
                {
                    lbErrorCorteCaja.Text = "Debe ingresar un monto válido";
                    txtMontoVisanet.Focus();
                    return;
                }

                if (mMonto <= 0)
                {
                    lbErrorCorteCaja.Text = "Debe ingresar un monto válido";
                    txtMontoVisanet.Focus();
                    return;
                }

                List<wsPuntoVenta.LoteCierre> qVisanet = new List<wsPuntoVenta.LoteCierre>();

                qVisanet = (List<wsPuntoVenta.LoteCierre>)ViewState["qVisanet"];
                wsPuntoVenta.LoteCierre item = new wsPuntoVenta.LoteCierre();

                item.Lote = mLote;
                item.Monto = mMonto;
                qVisanet.Add(item);

                gridVisanet.DataSource = qVisanet;
                gridVisanet.DataBind();
                ViewState["qVisanet"] = qVisanet;

                txtLoteVisanet.Text = "";
                txtMontoVisanet.Text = "";
                txtLoteVisanet.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorCorteCaja.Text = string.Format("Error al agregar el lote de Visanet {0} {1}", ex.Message, m);
            }
        }

        protected void lkAgregarCredomatic_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoCorteCaja.Text = "";
                lbErrorCorteCaja.Text = "";

                decimal mMonto = 0; int mLote = 0;

                if (txtLoteCredomatic.Text.Trim().Length == 0)
                {
                    lbErrorCorteCaja.Text = "Debe ingresar un lote válido";
                    txtLoteCredomatic.Focus();
                    return;
                }

                try
                {
                    mLote = Convert.ToInt32(txtLoteCredomatic.Text.Replace(",", ""));
                }
                catch
                {
                    lbErrorCorteCaja.Text = "Debe ingresar un lote válido";
                    txtLoteCredomatic.Focus();
                    return;
                }

                if (mLote <= 0)
                {
                    lbErrorCorteCaja.Text = "Debe ingresar un lote válido";
                    txtLoteCredomatic.Focus();
                    return;
                }

                if (txtMontoCredomatic.Text.Trim().Length == 0)
                {
                    lbErrorCorteCaja.Text = "Debe ingresar un monto válido";
                    txtMontoCredomatic.Focus();
                    return;
                }

                foreach (GridViewRow gvr in gridCredomatic.Rows)
                {
                    Label lbLote = (Label)gvr.FindControl("lbLote");

                    if (lbLote.Text == mLote.ToString())
                    {
                        lbErrorCorteCaja.Text = "El lote ya existe en este cierre";
                        txtLoteCredomatic.Focus();
                        return;
                    }
                }

                try
                {
                    mMonto = Convert.ToDecimal(txtMontoCredomatic.Text.Replace(",", ""));
                }
                catch
                {
                    lbErrorCorteCaja.Text = "Debe ingresar un monto válido";
                    txtMontoCredomatic.Focus();
                    return;
                }

                if (mMonto <= 0)
                {
                    lbErrorCorteCaja.Text = "Debe ingresar un monto válido";
                    txtMontoCredomatic.Focus();
                    return;
                }

                List<wsPuntoVenta.LoteCierre> qCredomatic = new List<wsPuntoVenta.LoteCierre>();

                qCredomatic = (List<wsPuntoVenta.LoteCierre>)ViewState["qCredomatic"];
                wsPuntoVenta.LoteCierre item = new wsPuntoVenta.LoteCierre();

                item.Lote = mLote;
                item.Monto = mMonto;
                qCredomatic.Add(item);

                gridCredomatic.DataSource = qCredomatic;
                gridCredomatic.DataBind();
                ViewState["qCredomatic"] = qCredomatic;

                txtLoteCredomatic.Text = "";
                txtMontoCredomatic.Text = "";
                txtLoteCredomatic.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorCorteCaja.Text = string.Format("Error al agregar el lote de Credomatic {0} {1}", ex.Message, m);
            }
        }

        protected void gridVisanet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridVisanet_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lbInfoCorteCaja.Text = "";
                lbErrorCorteCaja.Text = "";

                List<wsPuntoVenta.LoteCierre> qVisanet = new List<wsPuntoVenta.LoteCierre>();

                foreach (GridViewRow gvr in gridVisanet.Rows)
                {
                    Label lbLote = (Label)gvr.FindControl("lbLote");
                    Label lbMonto = (Label)gvr.FindControl("lbMonto");
                    Label lbLoteSeleccionado = (Label)gridVisanet.SelectedRow.FindControl("lbLote");

                    if (lbLote.Text != lbLoteSeleccionado.Text)
                    {
                        wsPuntoVenta.LoteCierre item = new wsPuntoVenta.LoteCierre();
                        item.Lote = Convert.ToInt32(lbLote.Text.Replace(",", ""));
                        item.Monto = Convert.ToDecimal(lbMonto.Text.Replace(",", ""));
                        qVisanet.Add(item);
                    }
                }

                gridVisanet.DataSource = qVisanet;
                gridVisanet.DataBind();
                ViewState["qVisanet"] = qVisanet;

                txtLoteVisanet.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorCorteCaja.Text = string.Format("Error al eliminar el lote de Visanet {0} {1}", ex.Message, m);
            }
        }

        protected void gridCredomatic_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridCredomatic_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lbInfoCorteCaja.Text = "";
                lbErrorCorteCaja.Text = "";

                List<wsPuntoVenta.LoteCierre> qCredomatic = new List<wsPuntoVenta.LoteCierre>();

                foreach (GridViewRow gvr in gridCredomatic.Rows)
                {
                    Label lbLote = (Label)gvr.FindControl("lbLote");
                    Label lbMonto = (Label)gvr.FindControl("lbMonto");
                    Label lbLoteSeleccionado = (Label)gridCredomatic.SelectedRow.FindControl("lbLote");

                    if (lbLote.Text != lbLoteSeleccionado.Text)
                    {
                        wsPuntoVenta.LoteCierre item = new wsPuntoVenta.LoteCierre();
                        item.Lote = Convert.ToInt32(lbLote.Text.Replace(",", ""));
                        item.Monto = Convert.ToDecimal(lbMonto.Text.Replace(",", ""));
                        qCredomatic.Add(item);
                    }
                }

                gridCredomatic.DataSource = qCredomatic;
                gridCredomatic.DataBind();
                ViewState["qCredomatic"] = qCredomatic;

                txtLoteCredomatic.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorCorteCaja.Text = string.Format("Error al eliminar el lote de Credomatic {0} {1}", ex.Message, m);
            }
        }

        bool PasaValidacionesCierre()
        {
            try
            {
                lbInfoCorteCaja.Text = "";
                lbErrorCorteCaja.Text = "";

                if (gridRecibosCierre.Rows.Count > 0 && txtRecibosCierre.Text.Trim().Length == 0)
                {
                    lbErrorCorteCaja.Text = "Debe seleccionar los recibos";
                    return false;
                }
                if (gridDepositosCierre.Rows.Count > 0 && txtDepositosCierre.Text.Trim().Length == 0)
                {
                    lbErrorCorteCaja.Text = "Debe seleccionar los depósitos";
                    return false;
                }

                bool mRecibosTodos = true;
                for (int ii = 0; ii < gridRecibosCierre.Rows.Count; ii++)
                {
                    CheckBox cbRecibo = (CheckBox)gridRecibosCierre.Rows[ii].FindControl("cbReciboCierre");
                    if (!cbRecibo.Checked) mRecibosTodos = false;
                }

                if (!mRecibosTodos)
                {
                    lbErrorCorteCaja.Text = "Debe seleccionar todos los recibos";
                    return false;
                }

                bool mDepositosTodos = true;
                for (int ii = 0; ii < gridDepositosCierre.Rows.Count; ii++)
                {
                    CheckBox cbDeposito = (CheckBox)gridDepositosCierre.Rows[ii].FindControl("cbDepositoCierre");
                    if (!cbDeposito.Checked) mDepositosTodos = false;
                }

                if (!mDepositosTodos)
                {
                    lbErrorCorteCaja.Text = "Debe seleccionar todos los depósitos";
                    return false;
                }

                bool mHayVisanet = false; decimal mRecVisanet = 0; decimal mLoteVisanet = 0;
                bool mHayCredomatic = false; decimal mRecCredomatic = 0; decimal mLoteCredomatic = 0;

                for (int ii = 0; ii < gridRecibosCierre.Rows.Count; ii++)
                {
                    CheckBox cbRecibo = (CheckBox)gridRecibosCierre.Rows[ii].FindControl("cbReciboCierre");
                    Label lbPos = (Label)gridRecibosCierre.Rows[ii].FindControl("lbPos");
                    Label lbTarjeta = (Label)gridRecibosCierre.Rows[ii].FindControl("lbTarjeta");

                    decimal mTarjeta = Convert.ToDecimal(lbTarjeta.Text.Replace(",", ""));

                    if (cbRecibo.Checked && mTarjeta > 0)
                    {
                        if (lbPos.Text == "V" || lbPos.Text == "L")
                        {
                            mHayVisanet = true;
                            mRecVisanet = mRecVisanet + mTarjeta;
                        }
                        if (lbPos.Text == "C" || lbPos.Text == "B")
                        {
                            mHayCredomatic = true;
                            mRecCredomatic = mRecCredomatic + mTarjeta;
                        }
                    }
                }

                if (mHayVisanet)
                {
                    if (gridVisanet.Rows.Count == 0)
                    {
                        lbErrorCorteCaja.Text = "Debe ingresar lote de Visanet";
                        txtLoteVisanet.Focus();
                        return false;
                    }
                }

                if (mHayCredomatic)
                {
                    if (gridCredomatic.Rows.Count == 0)
                    {
                        lbErrorCorteCaja.Text = "Debe ingresar lote de Credomatic";
                        txtLoteCredomatic.Focus();
                        return false;
                    }
                }

                for (int ii = 0; ii < gridVisanet.Rows.Count; ii++)
                {
                    Label lbMonto = (Label)gridVisanet.Rows[ii].FindControl("lbMonto");
                    mLoteVisanet = mLoteVisanet + Convert.ToDecimal(lbMonto.Text.Replace("'", ""));
                }
                for (int ii = 0; ii < gridCredomatic.Rows.Count; ii++)
                {
                    Label lbMonto = (Label)gridCredomatic.Rows[ii].FindControl("lbMonto");
                    mLoteCredomatic = mLoteCredomatic + Convert.ToDecimal(lbMonto.Text.Replace("'", ""));
                }

                if (mRecVisanet != mLoteVisanet)
                {
                    if (!txtObservacionesCierre.Text.Contains("Visanet no cuadra porque"))
                    {
                        lbErrorCorteCaja.Text = "No cuadran los recibos con los lotes de Visanet, explique porqué";
                        txtObservacionesCierre.Focus();
                        return false;
                    }

                    //lbErrorCorteCaja.Text = "No cuadran los recibos con los lotes de Visanet";
                    //txtLoteVisanet.Focus();
                    //return false;
                }

                if (mRecCredomatic != mLoteCredomatic)
                {
                    if (!txtObservacionesCierre.Text.Contains("Credomatic no cuadra porque"))
                    {
                        lbErrorCorteCaja.Text = "No cuadran los recibos con los lotes de Credomatic, explique porqué";
                        txtObservacionesCierre.Focus();
                        return false;
                    }

                    //lbErrorCorteCaja.Text = "No cuadran los recibos con los lotes de Credomatic";
                    //txtLoteCredomatic.Focus();
                    //return false;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorCorteCaja.Text = string.Format("Error al validar los datos del cierre {0} {1}", ex.Message, m);
                return false;
            }

            return true;
        }

        protected void lkGrabarCierre_Click(object sender, EventArgs e)
        {
            try
            {
                int jj = 0;

                lbInfoCorteCaja.Text = "";
                lbErrorCorteCaja.Text = "";

                if (!PasaValidacionesCierre()) return;

                wsPuntoVenta.LoteCierre[] qVisanet = new wsPuntoVenta.LoteCierre[1];
                wsPuntoVenta.LoteCierre[] qCredomatic = new wsPuntoVenta.LoteCierre[1];

                wsPuntoVenta.RecibosDepositar[] qRecibos = new wsPuntoVenta.RecibosDepositar[1];
                wsPuntoVenta.DepositosCerrar[] qDepositos = new wsPuntoVenta.DepositosCerrar[1];

                jj = 0;
                for (int ii = 0; ii < gridVisanet.Rows.Count; ii++)
                {
                    jj++;
                    Label lbLote = (Label)gridVisanet.Rows[ii].FindControl("lbLote");
                    Label lbMonto = (Label)gridVisanet.Rows[ii].FindControl("lbMonto");

                    wsPuntoVenta.LoteCierre item = new wsPuntoVenta.LoteCierre();
                    item.Lote = Convert.ToInt32(lbLote.Text.Replace("'", ""));
                    item.Monto = Convert.ToDecimal(lbMonto.Text.Replace("'", ""));

                    if (jj > 1) Array.Resize(ref qVisanet, jj);
                    qVisanet[jj - 1] = item;
                }

                jj = 0;
                for (int ii = 0; ii < gridCredomatic.Rows.Count; ii++)
                {
                    jj++;
                    Label lbLote = (Label)gridCredomatic.Rows[ii].FindControl("lbLote");
                    Label lbMonto = (Label)gridCredomatic.Rows[ii].FindControl("lbMonto");

                    wsPuntoVenta.LoteCierre item = new wsPuntoVenta.LoteCierre();
                    item.Lote = Convert.ToInt32(lbLote.Text.Replace("'", ""));
                    item.Monto = Convert.ToDecimal(lbMonto.Text.Replace("'", ""));

                    if (jj > 1) Array.Resize(ref qCredomatic, jj);
                    qCredomatic[jj - 1] = item;
                }

                jj = 0;
                for (int ii = 0; ii < gridRecibosCierre.Rows.Count; ii++)
                {
                    CheckBox cbRecibo = (CheckBox)gridRecibosCierre.Rows[ii].FindControl("cbReciboCierre");
                    Label lbRecibo = (Label)gridRecibosCierre.Rows[ii].FindControl("lbRecibo");

                    if (cbRecibo.Checked)
                    {
                        jj++;
                        wsPuntoVenta.RecibosDepositar item = new wsPuntoVenta.RecibosDepositar();
                        item.Recibo = lbRecibo.Text;
                        item.Fecha = DateTime.Now.Date;
                        item.Efectivo = 0;
                        item.Cheque = 0;
                        item.Tarjeta = 0;
                        item.Pos = "";

                        if (jj > 1) Array.Resize(ref qRecibos, jj);
                        qRecibos[jj - 1] = item;
                    }
                }

                jj = 0;
                for (int ii = 0; ii < gridDepositosCierre.Rows.Count; ii++)
                {
                    CheckBox cbDeposito = (CheckBox)gridDepositosCierre.Rows[ii].FindControl("cbDepositoCierre");
                    Label lbCuenta = (Label)gridDepositosCierre.Rows[ii].FindControl("lbCuenta");
                    Label lbTipoDocumento = (Label)gridDepositosCierre.Rows[ii].FindControl("lbTipoDocumento");
                    Label lbNumero = (Label)gridDepositosCierre.Rows[ii].FindControl("lbNumero");

                    if (cbDeposito.Checked)
                    {
                        jj++;
                        wsPuntoVenta.DepositosCerrar item = new wsPuntoVenta.DepositosCerrar();
                        item.Cuenta = lbCuenta.Text;
                        item.TipoDocumento = lbTipoDocumento.Text;
                        item.Numero = Convert.ToDecimal(lbNumero.Text);
                        item.Fecha = DateTime.Now.Date;
                        item.Banco = "";
                        item.Monto = 0;

                        if (jj > 1) Array.Resize(ref qDepositos, jj);
                        qDepositos[jj - 1] = item;
                    }
                }

                string mMensaje = ""; int mCierre = 0;
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.GrabarCierreCaja(qVisanet, qCredomatic, qRecibos, qDepositos, Convert.ToString(Session["Tienda"]), txtRecibosCierre.Text, txtDepositosCierre.Text, txtObservacionesCierre.Text, Convert.ToString(Session["usuario"]), ref mMensaje, ref mCierre))
                {
                    lbErrorCorteCaja.Text = mMensaje;
                    return;
                }

                LimpiarCierre();
                CargarCierres();

                if (fuCierre != null && fuCierre.HasFile)
                {
                    List<string> files = new List<string>();

                    try
                    {
                        string folder = Path.Combine(Server.MapPath("~"), "images", "temp");
                        if (!Directory.Exists(folder))
                            Directory.CreateDirectory(folder);

                        fuCierre.PostedFiles.ToList().ForEach(x =>
                        {
                            string fileName = Path.Combine(folder, x.FileName);
                            x.SaveAs(fileName);
                            files.Add(fileName);
                        });

                        Dictionary<string, string> header = new Dictionary<string, string>();
                        header.Add("AppKey", General.AppkeyAutenticacion.ToString());
                        header.Add("Token", HttpContext.Current.Request.Cookies.Get(General.CookieName)?.Value);

                        Api api = new Api(General.NetunimService);
                        string endpoint = General.S3Cierres.Replace("{0}", mCierre.ToString()).Replace("{1}", Session["Tienda"].ToString());
                        IRestResponse response = api.Process(Method.POST, endpoint, null, header, files);
                        files.ForEach(File.Delete);

                        if (response.StatusCode != HttpStatusCode.OK)
                            throw new Exception(response.ErrorMessage);
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine(exception);
                    }
                }

                txtCierre.Text = mCierre.ToString();
                lbInfoCorteCaja.Text = mMensaje;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorCorteCaja.Text = string.Format("Error al grabar el cierre {0} {1}", ex.Message, m);
            }
        }

        void ImprimirCierre()
        {
            Int32 mCierre = 0;

            try
            {
                lbInfoCorteCaja.Text = "";
                lbErrorCorteCaja.Text = "";

                try
                {
                    mCierre = Convert.ToInt32(cbCierre.SelectedValue);
                }
                catch
                {
                    lbErrorCorteCaja.Text = "No hay cierre para imprirmir";
                    return;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mMensaje = "";
                DataSet ds = new DataSet();

                ds = ws.DevuelveCierre(mCierre, ref mMensaje);

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorCorteCaja.Text = string.Format("Error al imprimir {0}", mMensaje);
                    return;
                }

                using (ReportDocument reporte = new ReportDocument())
                {
                    string mFormato = "rptCorteCaja.rpt";
                    string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mFormato);

                    reporte.Load(p);
                    reporte.OpenSubreport("rptCorteCajaDepositos.rpt");
                    reporte.OpenSubreport("rptCorteCajaVisanet.rpt");
                    reporte.OpenSubreport("rptCorteCajaCredomatic.rpt");
                    reporte.OpenSubreport("rptCorteCajaVisaEnLink.rpt");

                    string mNombreDocumento = string.Format("Cierre{0}_{1}.pdf", cbTiendaCierre.Text, cbCierre.SelectedItem.Text.Replace("/", ""));
                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                    reporte.SetDataSource(ds);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorCorteCaja.Text = string.Format("Error al imprimir el cierre {0} {1} {2}", ex.Message, m, mCierre);
            }
        }

        protected void lkImprimirCierre_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoCorteCaja.Text = "";
                lbErrorCorteCaja.Text = "";

                ImprimirCierre();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorCorteCaja.Text = string.Format("Error al imprimir el cierre generado {0} {1}", ex.Message, m);
            }
        }

        protected void lkReimprimirCierre_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoCorteCaja.Text = "";
                lbErrorCorteCaja.Text = "";

                ImprimirCierre();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorCorteCaja.Text = string.Format("Error al re-imprimir el cierre {0} {1}", ex.Message, m);
            }

        }

        void CargarCierres()
        {
            try
            {
                lbInfoCorteCaja.Text = "";
                lbErrorCorteCaja.Text = "";

                int mAnio = 0;

                try
                {
                    mAnio = Convert.ToInt32(txtAnioCierre.Text);
                }
                catch
                {
                    lbErrorCorteCaja.Text = "Año inválido";
                    txtAnioCierre.Focus();
                    return;
                }

                if (mAnio <= 0)
                {
                    lbErrorCorteCaja.Text = "Año inválido";
                    txtAnioCierre.Focus();
                    return;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var q = ws.DevuelveCierres(cbTiendaCierre.Text, mAnio.ToString()).ToList();

                cbCierre.DataSource = q;
                cbCierre.DataTextField = "Fecha";
                cbCierre.DataValueField = "Cierre";
                cbCierre.DataBind();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorCorteCaja.Text = string.Format("Error al cargar los cierres de la tienda {0} {1}", ex.Message, m);
            }
        }

        protected void cbTiendaCierre_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lbInfoCorteCaja.Text = "";
                lbErrorCorteCaja.Text = "";

                CargarCierres();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorCorteCaja.Text = string.Format("Error al buscar los cierres de la tienda {0} {1}", ex.Message, m);
            }
        }

        protected void txtAnioCierre_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lbInfoCorteCaja.Text = "";
                lbErrorCorteCaja.Text = "";

                CargarCierres();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorCorteCaja.Text = string.Format("Error en el año de los cierres de la tienda {0} {1}", ex.Message, m);
            }
        }

        void LimpiarCamposRecibo()
        {
            
            lbInfoTasa.Text = "";
            lbInfoRecibo.Text = "";
            lbErrorRecibo.Text = "";

            txtMontoRecibir.Text = "";
            txtEfectivo.Text = "";
            txtTarjeta.Text = "";
            txtCheque.Text = "";
            txtChequeNumero.Text = "";
            txtTasaCambio.Text = "";
            cbPos.SelectedValue = "N";
            cbChequeBanco.SelectedValue = "ND";

            txtTarjeta.ReadOnly = false;
            txtCheque.ReadOnly = false;
            txtChequeNumero.ReadOnly = false;

            cbPos.Enabled = true;
            cbChequeBanco.Enabled = true;
        }

        protected void cbMoneda_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LimpiarCamposRecibo();

                if (cbMoneda.SelectedValue == "$")
                {
                    txtTarjeta.ReadOnly = true;
                    txtCheque.ReadOnly = true;
                    txtChequeNumero.ReadOnly = true;

                    cbPos.Enabled = false;
                    cbChequeBanco.Enabled = false;

                    var ws = new wsPuntoVenta.wsPuntoVenta();
                    if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                    string mMensaje = "";
                    decimal mTasa = ws.TasaCambio(ref mMensaje);

                    if (mTasa <= 0)
                    {
                        lbErrorRecibo.Text = mMensaje;
                        return;
                    }

                    txtTasaCambio.Text = mTasa.ToString();
                    lbInfoTasa.Text = mMensaje;
                }

                txtEfectivo.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorRecibo.Text = string.Format("Error al cambiar la moneda {0} {1}", ex.Message, m);
            }
        }

        protected void btnGrabarBoletaConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoBoleta.Text = "";
                lbErrorBoleta.Text = "";

                Int64 mBoleta = 0;
                try
                {
                    mBoleta = Convert.ToInt64(txtBoletaConfirmar.Text.Trim());
                }
                catch
                {
                    mBoleta = 0;
                }
                if (mBoleta <= 0)
                {
                    lbErrorBoleta.Text = "Debe ingresar un número de boleta válido";
                    txtBoletaConfirmar.Focus();
                    return;
                }

                DateTime mFecha;
                try
                {
                    mFecha = Convert.ToDateTime(cbFechaBoletaConfirmar.Value);
                }
                catch
                {
                    mFecha = new DateTime(1980, 1, 1);
                }
                if (mFecha.Year == 1980)
                {
                    lbErrorBoleta.Text = "Debe ingresar una fecha válida";
                    cbFechaBoletaConfirmar.Focus();
                    return;
                }

                string mCuenta;
                try
                {
                    mCuenta = cbBancoBoletaConfirmar.Text;
                }
                catch
                {
                    mCuenta = "";
                }
                if (mCuenta == "")
                {
                    lbErrorBoleta.Text = "Debe seleccionar la cuenta";
                    cbBancoBoletaConfirmar.Focus();
                    return;
                }

                decimal mMonto = 0;
                try
                {
                    mMonto = Convert.ToDecimal(txtMontoBoletaConfirmar.Text.Trim().Replace(",", "").Replace("Q", "").Replace("$", "").Replace(" ", ""));
                }
                catch
                {
                    mMonto = 0;
                }
                if (mMonto <= 0)
                {
                    lbErrorBoleta.Text = "Debe ingresar un monto válido";
                    txtMontoBoletaConfirmar.Focus();
                    return;
                }

                Clases.BoletaConfirmar mBoletaConfirmar = new Clases.BoletaConfirmar();
                mBoletaConfirmar.Boleta = mBoleta;
                mBoletaConfirmar.Fecha = mFecha;
                mBoletaConfirmar.Banco = mCuenta;
                mBoletaConfirmar.Monto = mMonto;
                mBoletaConfirmar.Vendedor = Convert.ToString(Session["Vendedor"]);

                string json = JsonConvert.SerializeObject(mBoletaConfirmar);
                byte[] data = Encoding.ASCII.GetBytes(json);

                //para enviar solicitudes de confirmación solo en PRO.
                if (Convert.ToString(Session["Ambiente"]) == "PRO")
                {
                    WebRequest request = WebRequest.Create(string.Format("{0}/ConfirmaBoleta/", Convert.ToString(Session["UrlRestServices"])));

                    request.Method = "POST";
                    request.ContentType = "application/json";
                    request.ContentLength = data.Length;

                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }

                    var response = (HttpWebResponse)request.GetResponse();
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    string mRetorna = responseString.ToString().Replace("\"", "");
                    if (mRetorna.ToLower().Contains("error"))
                    {
                        lbErrorBoleta.Text = mRetorna;
                        return;
                    }

                    txtBoletaConfirmar.Text = "";
                    cbBancoBoletaConfirmar.Text = "";
                    cbBancoBoletaConfirmar.Value = null;
                    txtMontoBoletaConfirmar.Text = "";

                    lbInfoBoleta.Text = mRetorna;
                }
            }
            catch (Exception ex)
            {
                lbErrorBoleta.Text = CatchClass.ExMessage(ex, "recibos", "btnGrabarBoletaConfirmar_Click");
            }
        }


        protected void btnOK1_Click(object sender, EventArgs e)
        {
            lblConfAsociar.Text = "Se asociará el recibo a la factura " + cbAplicar.Items[0].Text + " ¿es correcto?";
            mdlRecibosConf.Show();
            mdlRecibos.Hide();
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            lblIngresoAnticipo.Text = "¿Esta seguro de ingresar un recibo como anticipo sin asociar?";
            mdlRecibosAnticipo.Show();

            mdlRecibos.Hide();
        }

        protected void btnOk2_Click(object sender, EventArgs e)
        {
            GrabarRecibo(cbAplicar.Items[0].Value);
            mdlRecibosConf.Hide();
        }

        protected void btnCancel2_Click(object sender, EventArgs e)
        {
            mdlRecibosConf.Hide();
        }

        protected void btnAceptarAnticipo_Click(object sender, EventArgs e)
        {
            mdlRecibosAnticipo.Hide();
            GrabarRecibo();
        }

        protected void btnCancelarAnticipo_Click(object sender, EventArgs e)
        {
            mdlRecibosConf.Hide();
            cbAplicar.Focus();
        }

        protected void gridRecibosCierre_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "UploadFiles")
            {
                if (gridRecibosCierre.Rows.Count > 0)
                {
                    int index = int.Parse(e.CommandArgument.ToString());
                    FileUpload postedFiles = (FileUpload)gridRecibosCierre.Rows[index].FindControl("fileUploader");
                    string recibo = ((Label)gridRecibosCierre.Rows[index].FindControl("lbRecibo")).Text;
                    List<string> files = new List<string>();

                    if (postedFiles != null && postedFiles.HasFile)
                    {
                        string folder = Path.Combine(Server.MapPath("~"), "images", "temp");
                        if (!Directory.Exists(folder))
                            Directory.CreateDirectory(folder);

                        postedFiles.PostedFiles.ToList().ForEach(x =>
                        {
                            string fileName = Path.Combine(folder, x.FileName);
                            x.SaveAs(fileName);
                            files.Add(fileName);
                        });

                        Dictionary<string, string> header = new Dictionary<string, string>();
                        header.Add("AppKey", General.AppkeyAutenticacion.ToString());
                        header.Add("Token", HttpContext.Current.Request.Cookies.Get(General.CookieName)?.Value);

                        Api api = new Api(General.NetunimService);
                        string endpoint = General.S3Recibo.Replace("{0}", recibo);
                        IRestResponse response = api.Process(Method.POST, endpoint, null, header, files);
                        files.ForEach(File.Delete);

                        string script = "";
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            Label lbFiles = (Label)gridRecibosCierre.Rows[index].FindControl("lbArchivos");
                            int uploads = int.Parse(response.Content);
                            int current = int.Parse(lbFiles.Text);
                            lbFiles.Text = (uploads + current).ToString();

                            string msg = $"{uploads} archivos nuevos han sido agregados al recibo {recibo}";
                            script = "setTimeout(function(){$('#dialogMessage').html('" + msg +
                                     "');$('#dialog').dialog('open');}, 1500)";
                        }
                        else
                            script = "setTimeout(function(){$('#dialogMessage').html('" + response.ErrorMessage +
                                     "');$('#dialog').dialog('open');}, 1500)";

                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "mv", script, true);

                    }
                    else
                    {
                        string msg = "No se seleccionaron archivos";
                        string script = "setTimeout(function(){$('#dialogMessage').html('" + msg + "');$('#dialog').dialog('open');}, 1500)";
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "mv", script, true);
                    }
                }
            }
        }

        protected void gridDepositosCierre_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "UploadFiles")
            {
                if (gridDepositosCierre.Rows.Count > 0)
                {
                    int index = int.Parse(e.CommandArgument.ToString());
                    FileUpload postedFiles = (FileUpload)gridDepositosCierre.Rows[index].FindControl("fileUploader");
                    string cuenta = ((Label)gridDepositosCierre.Rows[index].FindControl("lbCuenta")).Text;
                    string deposito = ((Label)gridDepositosCierre.Rows[index].FindControl("lbNumero")).Text;
                    List<string> files = new List<string>();

                    if (postedFiles != null && postedFiles.HasFile)
                    {
                        string folder = Path.Combine(Server.MapPath("~"), "images", "temp");
                        if (!Directory.Exists(folder))
                            Directory.CreateDirectory(folder);

                        postedFiles.PostedFiles.ToList().ForEach(x =>
                        {
                            string fileName = Path.Combine(folder, x.FileName);
                            x.SaveAs(fileName);
                            files.Add(fileName);
                        });

                        Dictionary<string, string> header = new Dictionary<string, string>();
                        header.Add("AppKey", General.AppkeyAutenticacion.ToString());
                        header.Add("Token", HttpContext.Current.Request.Cookies.Get(General.CookieName)?.Value);

                        Api api = new Api(General.NetunimService);
                        string endpoint = General.S3Depositos.Replace("{0}", deposito).Replace("{1}", cuenta);
                        IRestResponse response = api.Process(Method.POST, endpoint, null, header, files);
                        files.ForEach(File.Delete);

                        string script = "";
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            Label lbFiles = (Label)gridDepositosCierre.Rows[index].FindControl("lbArchivos");
                            int uploads = int.Parse(response.Content);
                            int current = int.Parse(lbFiles.Text);
                            lbFiles.Text = (uploads + current).ToString();

                            string msg = $"{uploads} archivos nuevos han sido agregados al depósito No. {deposito}, en la cuenta No. {cuenta}";
                            script = "setTimeout(function(){$('#dialogMessage').html('" + msg +
                                     "');$('#dialog').dialog('open');}, 1500)";
                        }
                        else
                            script = "setTimeout(function(){$('#dialogMessage').html('" + response.ErrorMessage +
                                     "');$('#dialog').dialog('open');}, 1500)";

                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "mv", script, true);

                    }
                    else
                    {
                        string msg = "No se seleccionaron archivos";
                        string script = "setTimeout(function(){$('#dialogMessage').html('" + msg + "');$('#dialog').dialog('open');}, 1500)";
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "mv", script, true);
                    }
                }
            }
        }

    }
}