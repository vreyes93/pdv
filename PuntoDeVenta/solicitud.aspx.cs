﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace PuntoDeVenta
{
    public partial class solicitud : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                ViewState["url"] = Convert.ToString(Session["Url"]);
                //ViewState["url"] = string.Format("http://itdesarrollo/services/wsPuntoVenta.asmx", Request.Url.Host);
                CargarSolicitud();
            }
        }
        
        void CargarSolicitud()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);
            
            string mSolicitud = Request.QueryString["proc"];
            var q = ws.DevuelveAutorizacion(mSolicitud);

            lbSolicitud.Text = q[0].Autorizacion.ToString();
            lbTienda.Text = q[0].Tienda;
            lbVendedor.Text = q[0].VendedorNombre;
            lbCliente.Text = q[0].Cliente;
            lbClienteNombre.Text = q[0].ClienteNombre;
            lbArticuloNombre.Text = q[0].ArticuloNombre;
            lbCantidad.Text = q[0].Cantidad.ToString();
            lbArticulo.Text = q[0].Articulo;
            lbNotas.Text = q[0].Observaciones;
              
            lbCosto.Text = String.Format("Q{0}", String.Format("{0:0,0.00}", q[0].Costo));
            lbPrecioLista.Text = String.Format("Q{0}", String.Format("{0:0,0.00}", q[0].PrecioLista));
            lbContado.Text = String.Format("Q{0}", String.Format("{0:0,0.00}", q[0].PrecioContado));
            lbDescuento.Text = String.Format("Q{0}  ({1}%)", String.Format("{0:0,0.00}", q[0].PrecioSolicitado), String.Format("{0:0,0.00}", q[0].PctjDescuento));
            lbMargen.Text = String.Format("{0}", String.Format("{0:0.00}", q[0].Margen));
            lbMargenAutorizar.Text = String.Format("{0}", String.Format("{0:0.00}", q[0].MargenAutorizar));
            lbDiferencia.Text = String.Format("{0}", String.Format("{0:0.00}", q[0].Diferencia));
            lbPromocion.Text = q[0].TipoVentaNombre;
            lbFinanciera.Text = q[0].FinancieraNombre;
            lbNivelPrecio.Text = string.Format("{0} ({1})", q[0].NivelPrecio, string.Format("{0:0.0000}", q[0].Factor));

            txtUsuario.Text = ws.DevuelveUsuarioGerente(Request.QueryString["g"].ToString());
            txtPrecioAutorizado.Text = String.Format("Q{0}", String.Format("{0:0,0.00}", q[0].PrecioSolicitado));

            if (q[0].Estatus == "P")
            {
                if (Request.QueryString["sol"].ToString() == "T")
                {
                    lkAutorizar.Visible = true;
                    lbAutorizar.Visible = true;
                }
                else
                {
                    lkRechazar.Visible = true;
                    lbRechazar.Visible = true;

                    txtPrecioAutorizado.Text = "No aplica";
                    txtPrecioAutorizado.ReadOnly = true;
                }


                txtPassword.Focus();
            }
            else
            {
                txtUsuario.Text = q[0].Gerente;
                txtObservaciones.Text = q[0].Comentario;

                lbEstado.Visible = true;
                txtUsuario.ReadOnly = true;
                txtPassword.ReadOnly = true;
                txtPrecioAutorizado.ReadOnly = true;
                txtObservaciones.ReadOnly = true;
                lbAutorizar.Visible = false;
                lbRechazar.Visible = false;

                if (q[0].Estatus == "A")
                {
                    lbEstado.Text = "La solicitud fue autorizada por:";
                    txtPrecioAutorizado.Text = String.Format("Q{0}", String.Format("{0:0,0.00}", q[0].PrecioAutorizado));
                }
                else
                {
                    txtPrecioAutorizado.Text = "No Aplica";
                    if (q[0].Estatus == "R")
                    {
                        lbEstado.Text = "La solicitud fue rechazada por:";
                    }
                    else
                    {
                        lbEstado.Text = "El vendedor se retractó de esta solicitud:";
                    }
                }
            }
        }

        bool LoginExitoso()
        {
            if (txtUsuario.Text.Trim().Length == 0)
            {
                lbError.Text = "Debe ingresar su usuario";
                txtUsuario.Focus();
                return false;
            }
            if (txtPassword.Text.Trim().Length == 0)
            {
                lbError.Text = "Debe ingresar su contraseña";
                txtPassword.Focus();
                return false;
            }

            string mAmbiente = Request.QueryString["amb"];

            string mServidor = "(local)";
            string mBase = "EXACTUSERP";

            if (Convert.ToString(Session["Ambiente"]) == "PRO")
            {
                mBase = "EXACTUSERP";
                mServidor = "sql.fiesta.local";
            }

            if (Convert.ToString(Session["Ambiente"]) == "PRU")
            {
                mBase = "PRUEBAS";
                mServidor = "sql.fiesta.local";
            }

            bool mLoginExitoso = false;
            string mStringConexion = string.Format("Data Source = {0}; Initial Catalog = {1};User id = {2};password = {3}; Max Pool Size=600;Pooling=false;Application Name=PDV", mServidor, mBase, txtUsuario.Text, txtPassword.Text);

            using (SqlConnection conexion = new SqlConnection(mStringConexion))
            {
                try
                {
                    conexion.Open();
                    conexion.Close();

                    mLoginExitoso = true;
                }
                catch
                {
                    lbError.Text = "Usuario o contraseña inválidos.";
                    txtPassword.Text = "";
                    txtPassword.Focus();
                }
                finally
                {
                    if (conexion.State == System.Data.ConnectionState.Open) conexion.Close();
                }
            }

            return mLoginExitoso;
        }

        protected void lkAutorizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!LoginExitoso()) return;
                
                string mMensaje = ""; decimal mPrecio = 0;
                string mSolicitud = Request.QueryString["proc"];

                try
                {
                    mPrecio = Convert.ToDecimal(txtPrecioAutorizado.Text.Trim().Replace("Q", "").Replace(",", "").Replace(" ", ""));
                }
                catch
                {
                    lbError.Text = "El precio a autorizar es inválido";
                    txtPrecioAutorizado.Focus();
                    return;
                }

                if (mPrecio <= 0)
                {
                    lbError.Text = "El precio a autorizar es inválido.";
                    txtPrecioAutorizado.Focus();
                    return;
                }
                
                //decimal mMargenValidar = 0;
                //decimal mMargen = Convert.ToDecimal(lbMargenAutorizar.Text.Replace("Q", "").Replace(",", "").Replace(" ", ""));

                //if (mMargen <= mMargenValidar)
                //{
                //    lbError.Text = "El margen es inválido";
                //    txtPrecioAutorizado.Focus();
                //    return;
                //}

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.ProcesarSolicitud(ref mMensaje, mSolicitud, "A", mPrecio.ToString(), txtObservaciones.Text.Trim(), txtUsuario.Text.Trim().ToUpper()))
                {
                    lbError.Text = mMensaje;
                    lkAutorizar.Focus();
                    if (mMensaje.Contains("procesada")) CargarSolicitud();
                    return;
                }

                lbError.Text = "";
                lbInfo.Text = mMensaje;
                lkAutorizar.Visible = false;
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al autorizar la solicitud. {0}", ex.Message);
                return;
            }
        }

        protected void lkRechazar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!LoginExitoso()) return;

                string mMensaje = "";
                string mSolicitud = Request.QueryString["proc"];

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.ProcesarSolicitud(ref mMensaje, mSolicitud, "R", "0", txtObservaciones.Text.Trim(), txtUsuario.Text.Trim().ToUpper()))
                {
                    lbError.Text = mMensaje;
                    lkRechazar.Focus();
                    if (mMensaje.Contains("procesada")) CargarSolicitud();
                    return;
                }

                lbError.Text = "";
                lbInfo.Text = mMensaje;
                lkRechazar.Visible = false;
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al rechazar la solicitud. {0}", ex.Message);
                return;
            }
        }

        protected void txtPrecioAutorizado_TextChanged(object sender, EventArgs e)
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                decimal mIVA = ws.iva(); decimal mMargenValidar = Convert.ToDecimal(1.9);
                decimal mCosto = Convert.ToDecimal(lbCosto.Text.Replace("Q", "").Replace(",", "").Replace(" ", ""));
                decimal mPrecioLista = Convert.ToDecimal(lbPrecioLista.Text.Replace("Q", "").Replace(",", "").Replace(" ", ""));
                decimal mDescuento = Convert.ToDecimal(txtPrecioAutorizado.Text.Replace("Q", "").Replace(",", "").Replace(" ", ""));
                decimal mNuevoPrecio = mPrecioLista - mDescuento;
                decimal mNuevoMargen = ((mNuevoPrecio * Convert.ToDecimal(.95)) / mIVA) / mCosto;

                lbMargenAutorizar.Text = String.Format("{0}", String.Format("{0:0.00}", mNuevoMargen));
                if (mNuevoMargen <= mMargenValidar) lbError.Text = "El margen es inválido";
            }
            catch (Exception ex)
            {
                lbError.Text = string.Format("Error al calcular margen. {0}", ex.Message);
            }            
        }

    }
}