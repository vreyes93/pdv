﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="solicitudCoti.aspx.cs" Inherits="PuntoDeVenta.solicitudCoti" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Muebles Fiesta</title>
    <style type="text/css">
        .table {
            border-collapse: collapse !important;
            -moz-box-shadow: 1px 1px 8px #dddddd;
            -webkit-box-shadow: 1px 1px 8px #dddddd;
            box-shadow: 1px 1px 8px #dddddd;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            -moz-border-radius-bottomleft: 5px;
            -moz-border-radius-bottomright: 5px;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -webkit-border-bottom-left-radius: 5px;
            -webkit-border-bottom-right-radius: 5px;
        }

        .table-bordered th,
        .table-bordered td {
            border: 1px solid #ddd !important;
        }

        .table-bordered {
            border: 1px solid #dddddd;
        }

            .table-bordered > thead > tr > th,
            .table-bordered > tbody > tr > th,
            .table-bordered > tfoot > tr > th,
            .table-bordered > thead > tr > td,
            .table-bordered > tbody > tr > td,
            .table-bordered > tfoot > tr > td {
                border: 1px solid #dddddd;
            }

            .table-bordered > thead > tr > th,
            .table-bordered > thead > tr > td {
                border-bottom-width: 2px;
            }
    </style>
    <link href='https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:300' rel='stylesheet' type='text/css'>
</head>
<body style="font-family: 'Alegreya Sans SC', sans-serif; text-shadow: 1px 1px 1px #ffffff; font-size: xx-large;">
    <form id="form1" runat="server">
        <table class="table table-bordered" runat="server" id="tblProcesar" visible="true">
            <tr>
                <td style="background-color: #dddddd">Cotización:</td>
                <td>
                    <asp:Label ID="lbCotizacion" runat="server" Font-Bold="True"></asp:Label>
                </td>
                <td style="text-align: right; background-color: #dddddd">Vendedor:</td>
                <td>
                    <asp:Label ID="lbVendedor" runat="server"></asp:Label>
                    &nbsp;-
                <asp:Label ID="lbTienda" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Cliente:</td>
                <td colspan="3">
                    <asp:Label ID="lbClienteNombre" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Notas:</td>
                <td colspan="3">
                    <asp:Label ID="lbNotas" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr runat="server" id="trCosto">
                <td style="background-color: #dddddd">Costo:</td>
                <td colspan="3">
                    <asp:Label ID="lbCosto" runat="server"></asp:Label>
                    &nbsp;-&nbsp;(Costo total de la cotización)
                </td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Fac Actual:</td>
                <td>
                    <asp:Label ID="lbFacturaActual" runat="server"></asp:Label>
                </td>
                <td style="text-align: right; background-color: #dddddd">A Facturar:</td>
                <td>
                    <asp:Label ID="lbFacturar" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Diferencia:</td>
                <td colspan="3">
                    <asp:Label ID="lbDiferencia" runat="server"></asp:Label>
                </td>
            </tr>
            <tr runat="server" id="trMargen">
                <td style="background-color: #dddddd">Margen:</td>
                <td>
                    <asp:Label ID="lbMargen" runat="server"></asp:Label>
                </td>
                <td style="text-align: right; background-color: #dddddd">Nuevo Margen:</td>
                <td>
                    <asp:Label ID="lbMargenAutorizar" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Promoción:</td>
                <td colspan="3">
                    <asp:Label ID="lbPromocion" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Financiera:</td>
                <td>
                    <asp:Label ID="lbFinanciera" runat="server"></asp:Label>
                </td>
                <td style="text-align: right; background-color: #dddddd">Nivel de precio:</td>
                <td>
                    <asp:Label ID="lbNivelPrecio" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:Label ID="lbAutorizar" runat="server" Font-Bold="True" ForeColor="#00CC00"
                        Text="Por favor confirme autorización:" Visible="False"></asp:Label>
                    <asp:Label ID="lbRechazar" runat="server" Font-Bold="True" ForeColor="Red"
                        Text="Por favor confirme rechazo:" Visible="False"></asp:Label>
                    <asp:Label ID="lbEstado" runat="server" Font-Bold="True" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Autoriza:</td>
                <td colspan="3">
                    <asp:TextBox ID="txtPrecioAutorizado" runat="server"
                        Width="220px" Font-Size="X-Large" AutoPostBack="True"
                        OnTextChanged="txtPrecioAutorizado_TextChanged"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Usuario:</td>
                <td colspan="3">
                    <asp:TextBox ID="txtUsuario" runat="server" Width="220px"
                        Style="text-transform: uppercase;" TabIndex="10" Font-Size="X-Large"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Password:</td>
                <td colspan="3">
                    <asp:TextBox ID="txtPassword" runat="server" TabIndex="20" TextMode="Password"
                        Width="220px" Font-Size="X-Large"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Obs.:</td>
                <td colspan="3">
                    <asp:TextBox ID="txtObservaciones" runat="server" TabIndex="30" Width="760px"
                        Font-Size="X-Large" MaxLength="800"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center">
                    <asp:LinkButton ID="lkAutorizar" runat="server"
                        ToolTip="Haga clic aquí para autorizar la solicitud"
                        OnClick="lkAutorizar_Click" TabIndex="40" Visible="False">Autorizar</asp:LinkButton>
                    <asp:LinkButton ID="lkRechazar" runat="server" OnClick="lkRechazar_Click"
                        TabIndex="50" Visible="False">Rechazar</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server"></asp:Label>
                    <asp:Label ID="lbError" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>

        </table>
    </form>
</body>
</html>
