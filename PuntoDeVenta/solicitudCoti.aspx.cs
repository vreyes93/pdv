﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace PuntoDeVenta
{
    public partial class solicitudCoti : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                string mAmbiente = Request.QueryString["amb"];

                Session["Ambiente"] = mAmbiente.ToUpper();
                Session["Url"] = string.Format("http://sql.fiesta.local/services/wsPuntoVenta.asmx", Request.Url.Host);
                Session["UrlPrecios"] = string.Format("http://sql.fiesta.local/services/wsCambioPrecios.asmx", Request.Url.Host);

                if (Convert.ToString(Session["Ambiente"]) == "PRU") Session["Url"] = string.Format("http://sql.fiesta.local/servicesPruebas/wsPuntoVenta.asmx", Request.Url.Host);
                if (Convert.ToString(Session["Ambiente"]) == "PRU") Session["UrlPrecios"] = string.Format("http://sql.fiesta.local/servicesPruebas/wsCambioPrecios.asmx", Request.Url.Host);

                ViewState["url"] = Convert.ToString(Session["Url"]);
                CargarCotizacion();
            }
        }

        void CargarCotizacion()
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);
                
                string mMensaje = "";
                string mCotizacion = Request.QueryString["proc"];
                string mGerente = Request.QueryString["g"];
                var q = ws.DevuelveAutorizacionCotizacion(mCotizacion, mGerente, ref mMensaje);

                if (mMensaje.Trim().Length > 0)
                {
                    lbError.Text = mMensaje;
                    return;
                }

                txtObservaciones.Text = "";
                lbCotizacion.Text = mCotizacion;
                lbTienda.Text = q[0].Tienda;
                lbVendedor.Text = q[0].Vendedor;
                lbClienteNombre.Text = q[0].Cliente;
                lbNotas.Text = q[0].Notas;

                lbCosto.Text = String.Format("Q{0}", String.Format("{0:0,0.00}", q[0].Costo));
                lbFacturaActual.Text = String.Format("Q{0}", String.Format("{0:0,0.00}", q[0].FacturaActual));
                lbFacturar.Text = String.Format("Q{0}", String.Format("{0:0,0.00}", q[0].Facturar));
                lbDiferencia.Text = String.Format("Q{0}", String.Format("{0:0,0.00}", q[0].FacturaActual - q[0].Facturar));
                lbMargen.Text = String.Format("{0}", String.Format("{0:0.00}", q[0].Margen));
                lbMargenAutorizar.Text = String.Format("{0}", String.Format("{0:0.00}", q[0].MargenAutorizar));
                lbPromocion.Text = q[0].Promocion;
                lbFinanciera.Text = q[0].Financiera;
                lbNivelPrecio.Text = q[0].NivelPrecio;

                txtUsuario.Text = ws.DevuelveUsuarioGerente(Request.QueryString["g"].ToString());
                txtPrecioAutorizado.Text = String.Format("Q{0}", String.Format("{0:0,0.00}", q[0].Facturar));

                if (ws.DevuelveAutorizaSinCosto(Request.QueryString["g"].ToString()) == "S")
                {
                    trCosto.Visible = false;
                    trMargen.Visible = false;
                }
                

                if (q[0].Estatus == "P")
                {
                    if (Request.QueryString["sol"].ToString() == "T")
                    {
                        lkAutorizar.Visible = true;
                        lbAutorizar.Visible = true;
                    }
                    else
                    {
                        lkRechazar.Visible = true;
                        lbRechazar.Visible = true;

                        txtPrecioAutorizado.Text = "No aplica";
                        txtPrecioAutorizado.ReadOnly = true;
                    }


                    txtPassword.Focus();
                }
                else
                {
                    txtUsuario.Text = q[0].Gerente;
                    txtObservaciones.Text = q[0].Observaciones;

                    lbEstado.Visible = true;
                    txtUsuario.ReadOnly = true;
                    txtPassword.ReadOnly = true;
                    txtPrecioAutorizado.ReadOnly = true;
                    txtObservaciones.ReadOnly = true;
                    lbAutorizar.Visible = false;
                    lbRechazar.Visible = false;

                    if (q[0].Estatus == "A")
                    {
                        lbEstado.Text = "La cotización fue autorizada por:";
                        txtPrecioAutorizado.Text = String.Format("Q{0}", String.Format("{0:0,0.00}", q[0].Facturar));
                    }
                    else
                    {
                        txtPrecioAutorizado.Text = "No Aplica";
                        if (q[0].Estatus == "R")
                        {
                            lbEstado.Text = "La cotización fue rechazada por:";
                        }
                        else
                        {
                            lbEstado.Text = "El vendedor se retractó de esta solicitud:";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al cargar los datos. {0} {1}", ex.Message, m);
            }
        }

        bool LoginExitoso()
        {
            if (txtUsuario.Text.Trim().Length == 0)
            {
                lbError.Text = "Debe ingresar su usuario";
                txtUsuario.Focus();
                return false;
            }
            if (txtPassword.Text.Trim().Length == 0)
            {
                lbError.Text = "Debe ingresar su contraseña";
                txtPassword.Focus();
                return false;
            }

            string mAmbiente = Request.QueryString["amb"];

            string mServidor = "(local)";
            string mBase = "EXACTUSERP";

            if (mAmbiente == "PRO")
            {
                mBase = "EXACTUSERP";
                mServidor = "sql.fiesta.local";
            }

            if (mAmbiente == "PRU")
            {
                mBase = "PRUEBAS";
                mServidor = "sql.fiesta.local";
            }

            bool mLoginExitoso = false;
            string mStringConexion = string.Format("Data Source = {0}; Initial Catalog = {1};User id = {2};password = {3}; Max Pool Size=600;Pooling=false;Application Name=PDV", mServidor, mBase, txtUsuario.Text, txtPassword.Text);

            using (SqlConnection conexion = new SqlConnection(mStringConexion))
            {
                try
                {
                    conexion.Open();
                    conexion.Close();

                    mLoginExitoso = true;
                }
                catch
                {
                    lbError.Text = "Usuario o contraseña inválidos.";
                    txtPassword.Text = "";
                    txtPassword.Focus();
                }
                finally
                {
                    if (conexion.State == System.Data.ConnectionState.Open) conexion.Close();
                }
            }

            return mLoginExitoso;
        }

        protected void lkAutorizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!LoginExitoso()) return;

                lbInfo.Text = "";
                lbError.Text = "";

                string mMensaje = "";
                string mCotizacion = lbCotizacion.Text;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                decimal mPrecioAutorizado = 0;

                try
                {
                    mPrecioAutorizado = Convert.ToDecimal(txtPrecioAutorizado.Text.Trim().Replace(",", "").Replace("Q", "").Replace(" ", ""));
                }
                catch
                {
                    lbError.Text = "El precio autorizado es inválido";
                    txtPrecioAutorizado.Focus();
                    return;
                }

                if (mPrecioAutorizado <= 0)
                {
                    lbError.Text = "El precio autorizado es inválido";
                    txtPrecioAutorizado.Focus();
                    return;
                }

                if (!ws.ProcesarSolicitudCotizacion(ref mMensaje, mCotizacion, "A", mPrecioAutorizado.ToString(), txtObservaciones.Text.Trim(), txtUsuario.Text.Trim().ToUpper(),lbNivelPrecio.Text,lbFinanciera.Text))
                {
                    lkAutorizar.Focus();
                    CargarCotizacion();
                    lbError.Text = mMensaje;
                    return;
                }

                lbError.Text = "";
                lbInfo.Text = mMensaje;
                lkAutorizar.Visible = false;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al autorizar la cotización. {0} {1}", ex.Message, m);
                return;
            }
        }
        
        protected void lkRechazar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!LoginExitoso()) return;

                lbInfo.Text = "";
                lbError.Text = "";

                string mMensaje = "";
                string mCotizacion = lbCotizacion.Text;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.ProcesarSolicitudCotizacion(ref mMensaje, mCotizacion, "R", "0", txtObservaciones.Text.Trim(), txtUsuario.Text.Trim().ToUpper(),lbNivelPrecio.Text,lbFinanciera.Text))
                {
                    lkRechazar.Focus();
                    CargarCotizacion();
                    lbError.Text = mMensaje;
                    return;
                }

                lbError.Text = "";
                lbInfo.Text = mMensaje;
                lkRechazar.Visible = false;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al rechazar la cotización. {0} {1}", ex.Message, m);
                return;
            }
        }

        protected void txtPrecioAutorizado_TextChanged(object sender, EventArgs e)
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                decimal mIVA = ws.iva(); 
                decimal mCosto = Convert.ToDecimal(lbCosto.Text.Replace("Q", "").Replace(",", "").Replace(" ", ""));
                decimal mPrecioAutorizado = Convert.ToDecimal(txtPrecioAutorizado.Text.Replace("Q", "").Replace(",", "").Replace(" ", ""));
                decimal mNuevoMargen = Math.Round((mPrecioAutorizado / mIVA) / mCosto, 2, MidpointRounding.AwayFromZero);

                lbMargenAutorizar.Text = String.Format("{0}", String.Format("{0:0.00}", mNuevoMargen));
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al calcular margen. {0} {1}", ex.Message, m);
            }            
        }

    }
}