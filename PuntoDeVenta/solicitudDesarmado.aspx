﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="solicitudDesarmado.aspx.cs" Inherits="PuntoDeVenta.solicitudDesarmado" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Muebles Fiesta</title>
    <style type="text/css">
        .table {
            border-collapse: collapse !important;
            -moz-box-shadow: 1px 1px 8px #dddddd;
            -webkit-box-shadow: 1px 1px 8px #dddddd;
            box-shadow: 1px 1px 8px #dddddd;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            -moz-border-radius-bottomleft: 5px;
            -moz-border-radius-bottomright: 5px;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -webkit-border-bottom-left-radius: 5px;
            -webkit-border-bottom-right-radius: 5px;
        }

        .table-bordered th,
        .table-bordered td {
            border: 1px solid #ddd !important;
        }

        .table-bordered {
            border: 1px solid #dddddd;
        }

            .table-bordered > thead > tr > th,
            .table-bordered > tbody > tr > th,
            .table-bordered > tfoot > tr > th,
            .table-bordered > thead > tr > td,
            .table-bordered > tbody > tr > td,
            .table-bordered > tfoot > tr > td {
                border: 1px solid #dddddd;
            }

            .table-bordered > thead > tr > th,
            .table-bordered > thead > tr > td {
                border-bottom-width: 2px;
            }
    </style>
    <link href='https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:300' rel='stylesheet' type='text/css'>
</head>
<body>
    <form id="form1" runat="server">
        <table class="table table-bordered" runat="server" id="tblProcesar" visible="true">
            <tr>
                <td style="background-color: #dddddd">Desarmado No.:</td>
                <td>
                    <asp:Label ID="lbDesarmado" runat="server" Font-Bold="True"></asp:Label>
                </td>
                <td style="text-align: right; background-color: #dddddd">Vendedor:</td>
                <td>
                    <asp:Label ID="lbVendedor" runat="server"></asp:Label>
                    &nbsp;-
                <asp:Label ID="lbTienda" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Notas:</td>
                <td colspan="3">
                    <asp:Label ID="lbNotas" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Fecha:</td>
                <td colspan="3">
                    <asp:Label ID="lbFecha" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Factura:</td>
                <td colspan="3">
                    <asp:Label ID="lbFactura" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Cliente:</td>
                <td colspan="3">
                    <asp:Label ID="lbCliente" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Artículo:</td>
                <td colspan="3">
                    <asp:Label ID="lbArticulo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Descripción:</td>
                <td colspan="3">
                    <asp:Label ID="lbDescripcion" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Existencias:</td>
                <td colspan="3">
                    <asp:Label ID="lbExistencias" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Ubicaciones:</td>
                <td colspan="3">
                    <asp:Label ID="lbUbicaciones" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:Label ID="lbAutorizar" runat="server" Font-Bold="True" ForeColor="#00CC00"
                        Text="Por favor confirme autorización:" Visible="False"></asp:Label>
                    <asp:Label ID="lbRechazar" runat="server" Font-Bold="True" ForeColor="Red"
                        Text="Por favor confirme rechazo:" Visible="False"></asp:Label>
                    <asp:Label ID="lbEstado" runat="server" Font-Bold="True" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Usuario:</td>
                <td colspan="3">
                    <asp:TextBox ID="txtUsuario" runat="server" Width="220px"
                        Style="text-transform: uppercase;" TabIndex="10" Font-Size="X-Large"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Password:</td>
                <td colspan="3">
                    <asp:TextBox ID="txtPassword" runat="server" TabIndex="20" TextMode="Password"
                        Width="220px" Font-Size="X-Large"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="background-color: #dddddd">Obs.:</td>
                <td colspan="3">
                    <asp:TextBox ID="txtObservaciones" runat="server" TabIndex="30" Width="760px"
                        Font-Size="X-Large" MaxLength="800"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center">
                    <asp:LinkButton ID="lkAutorizar" runat="server"
                        ToolTip="Haga clic aquí para autorizar la solicitud"
                        OnClick="lkAutorizar_Click" TabIndex="40" Visible="False">Autorizar</asp:LinkButton>
                    <asp:LinkButton ID="lkRechazar" runat="server" OnClick="lkRechazar_Click" ToolTip="Haga clic aquí para rechazar la solicitud"
                        TabIndex="50" Visible="False">Rechazar</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server"></asp:Label>
                    <asp:Label ID="lbError" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>

        </table>
    </form>
</body>
</html>
