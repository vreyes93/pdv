﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Net;
using Newtonsoft.Json;
using MF_Clases;
using static MF_Clases.Clases;

namespace PuntoDeVenta
{
    public partial class solicitudDesarmado : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                string mAmbiente = Request.QueryString["amb"];
                Session["Ambiente"] = mAmbiente;

                Session["UrlRestServices"] = "http://localhost:53874/api";
                if (Convert.ToString(Session["Ambiente"]) == "PRO") Session["UrlRestServices"] = string.Format("http://sql.fiesta.local/RestServices/api", Request.Url.Host);
                if (Convert.ToString(Session["Ambiente"]) == "PRU") Session["UrlRestServices"] = string.Format("http://sql.fiesta.local/RestServicesPruebas/api", Request.Url.Host);

                CargarDesarmado();
            }
        }

        void CargarDesarmado()
        {
            try
            {
                WebRequest request = WebRequest.Create(string.Format("{0}/solicituddesarmado/{1}", Convert.ToString(Session["UrlRestServices"]), Request.QueryString["proc"]));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                Desarmado mDesarmado = new Desarmado();
                mDesarmado = JsonConvert.DeserializeObject<Desarmado>(responseString);

                if (mDesarmado.Mensaje.ToLower().Contains("error"))
                {
                    lbError.Text = mDesarmado.Mensaje;
                    return;
                }

                lbDesarmado.Text = mDesarmado.NoDesarmado.ToString();
                lbVendedor.Text = mDesarmado.NombreVendedor;
                lbTienda.Text = mDesarmado.Tienda;
                lbNotas.Text = mDesarmado.Observaciones;
                lbFecha.Text = string.Format("{0}{1}/{2}{3}/{4}", mDesarmado.FechaDesarmado.Day < 10 ? "0" : "", mDesarmado.FechaDesarmado.Day.ToString(), mDesarmado.FechaDesarmado.Month < 10 ? "0" : "", mDesarmado.FechaDesarmado.Month.ToString(), mDesarmado.FechaDesarmado.Year.ToString());
                lbArticulo.Text = mDesarmado.Articulo[0].Articulo;
                lbDescripcion.Text = mDesarmado.Articulo[0].Descripcion;
                lbExistencias.Text = mDesarmado.Articulo[0].Existencias.ToString();
                lbUbicaciones.Text = mDesarmado.Articulo[0].Ubicaciones;
                lbFactura.Text = mDesarmado.Factura;
                lbCliente.Text = string.Format("{0} - {1}", mDesarmado.Cliente, mDesarmado.NombreCliente);

                Session["Desarmado"] = mDesarmado;
                txtObservaciones.Text = "";
                txtUsuario.Text = Request.QueryString["g"];

                if (mDesarmado.Estado == "P")
                {
                    if (Request.QueryString["sol"].ToString() == "T")
                    {
                        lkAutorizar.Visible = true;
                        lbAutorizar.Visible = true;
                    }
                    else
                    {
                        lkRechazar.Visible = true;
                        lbRechazar.Visible = true;
                    }

                    txtPassword.Focus();
                }
                else
                {
                    txtUsuario.Text = mDesarmado.UsuarioAutoriza;
                    txtObservaciones.Text = mDesarmado.ObservacionesAutoriza;

                    lbEstado.Visible = true;
                    txtUsuario.ReadOnly = true;
                    txtPassword.ReadOnly = true;
                    txtObservaciones.ReadOnly = true;
                    lbAutorizar.Visible = false;
                    lbRechazar.Visible = false;

                    if (mDesarmado.Estado == "A")
                    {
                        lbAutorizar.Visible = true;
                        lbAutorizar.Text = "La requisición fue autorizada por:";
                    }
                    else
                    {
                        if (mDesarmado.Estado == "R")
                        {
                            lbRechazar.Visible = true;
                            lbRechazar.Text = "La requisición fue rechazada por:";
                        }
                        else
                        {
                            lbEstado.Text = "El vendedor se retractó de esta solicitud:";
                        }
                    }

                    txtObservaciones.Text = mDesarmado.ObservacionesAutoriza;
                }

                txtPassword.Focus();
            }
            catch (Exception ex)
            {
                lbError.Text = CatchClass.ExMessage(ex, "solicitudDesarmado", "CargarDesarmado");
            }
        }

        protected void lkAutorizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!LoginExitoso()) return;

                lbInfo.Text = "";
                lbError.Text = "";

                Desarmado mDesarmado = new Desarmado();
                mDesarmado = (Desarmado)Session["Desarmado"];

                mDesarmado.UsuarioAutoriza = txtUsuario.Text;
                mDesarmado.ObservacionesAutoriza = txtObservaciones.Text;

                string json = JsonConvert.SerializeObject(mDesarmado);
                byte[] data = Encoding.ASCII.GetBytes(json);

                WebRequest request = WebRequest.Create(string.Format("{0}/SolicitudDesarmado/", Convert.ToString(Session["UrlRestServices"])));

                request.Method = "PUT";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string mRetorna = responseString.ToString().Replace("\"", "");
                if (mRetorna.ToLower().Contains("error"))
                {
                    lbError.Text = mRetorna;
                    return;
                }

                lbInfo.Text = mRetorna;
                lkAutorizar.Visible = false;
            }
            catch (Exception ex)
            {
                lbError.Text = CatchClass.ExMessage(ex, "solicitudDesarmado", "lkAutorizar");
            }
        }

        protected void lkRechazar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!LoginExitoso()) return;

                lbInfo.Text = "";
                lbError.Text = "";

                Desarmado mDesarmado = new Desarmado();
                mDesarmado = (Desarmado)Session["Desarmado"];

                mDesarmado.UsuarioAutoriza = txtUsuario.Text;
                mDesarmado.ObservacionesAutoriza = txtObservaciones.Text;

                string json = JsonConvert.SerializeObject(mDesarmado);
                byte[] data = Encoding.ASCII.GetBytes(json);

                WebRequest request = WebRequest.Create(string.Format("{0}/SolicitudDesarmado/", Convert.ToString(Session["UrlRestServices"])));

                request.Method = "DELETE";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string mRetorna = responseString.ToString().Replace("\"", "");
                if (mRetorna.ToLower().Contains("error"))
                {
                    lbError.Text = mRetorna;
                    return;
                }

                lbInfo.Text = mRetorna;
                lkRechazar.Visible = false;
            }
            catch (Exception ex)
            {
                lbError.Text = CatchClass.ExMessage(ex, "solicitudDesarmado", "lkRechazar");
            }
        }

        bool LoginExitoso()
        {
            if (txtUsuario.Text.Trim().Length == 0)
            {
                lbError.Text = "Debe ingresar su usuario";
                txtUsuario.Focus();
                return false;
            }
            if (txtPassword.Text.Trim().Length == 0)
            {
                lbError.Text = "Debe ingresar su contraseña";
                txtPassword.Focus();
                return false;
            }

            string mAmbiente = Request.QueryString["amb"];

            string mServidor = "(local)";
            string mBase = "EXACTUSERP";

            if (mAmbiente == "PRO")
            {
                mBase = "EXACTUSERP";
                mServidor = "sql.fiesta.local";
            }

            if (mAmbiente == "PRU")
            {
                mBase = "PRUEBAS";
                mServidor = "sql.fiesta.local";
            }

            bool mLoginExitoso = false;
            string mStringConexion = string.Format("Data Source = {0}; Initial Catalog = {1};User id = {2};password = {3}; Max Pool Size=600;Pooling=false;Application Name=PDV", mServidor, mBase, txtUsuario.Text, txtPassword.Text);

            using (SqlConnection conexion = new SqlConnection(mStringConexion))
            {
                try
                {
                    conexion.Open();
                    conexion.Close();

                    mLoginExitoso = true;
                }
                catch
                {
                    lbError.Text = "Usuario o contraseña inválidos.";
                    txtPassword.Text = "";
                    txtPassword.Focus();
                }
                finally
                {
                    if (conexion.State == System.Data.ConnectionState.Open) conexion.Close();
                }
            }

            return mLoginExitoso;
        }

    }
}