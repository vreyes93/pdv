﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="tiposventa.aspx.cs" Inherits="PuntoDeVenta.tiposventa" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            height: 32px;
        }
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>&nbsp;&nbsp;Promociones</h3>
    <div class="content2">
    <div class="clientes">
    <ul>
        <li>
            <table align="center">
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp</td>
                    <td>
                        Código de la Promoción:&nbsp;&nbsp;<asp:TextBox 
                            ID="txtCodigo" runat="server" 
                            Font-Bold="True" Font-Size="Large" Width="60px" 
                            ToolTip="Este es el código del tipo de venta" MaxLength="4" 
                            style="text-transform: uppercase;" AutoPostBack="True" 
                            ontextchanged="txtCodigo_TextChanged"></asp:TextBox>&nbsp;&nbsp;
                    </td>
                    <td>
                <asp:LinkButton ID="lbBuscar" runat="server" 
                            ToolTip="Haga clic aquí para buscar un tipo de venta para consultarlo y/o modificar sus datos" 
                            TabIndex="10" onclick="lbBuscar_Click">Buscar</asp:LinkButton>
                    </td>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
            </table>
        </li>
        <li>
       <asp:GridView ID="gridTiposVenta" runat="server" AutoGenerateColumns="False" 
                            BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                            CellPadding="3" CellSpacing="2" Visible="False" 
                onrowdatabound="gridTiposVenta_RowDataBound" 
                onselectedindexchanged="gridTiposVenta_SelectedIndexChanged" 
                TabIndex="600">
                            <Columns>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkSeleccionarTipoVenta" runat="server" 
                                            CausesValidation="False" CommandName="Select" Text="Seleccionar" 
                                            ToolTip="Haga clic aquí para seleccionar un tipo de venta para modificarlo"></asp:LinkButton>
                                    </ItemTemplate>
                                    <HeaderStyle Width="40px" />
                                    <ItemStyle Width="40px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="CodigoTipoVenta" HeaderText="Código" >
                                <HeaderStyle Width="50px" />
                                <ItemStyle Width="50px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Descripcion" HeaderText="Descripción" >
                                <HeaderStyle Width="100px" />
                                <ItemStyle Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Estatus" HeaderText="Estatus" >
                                <HeaderStyle Width="60px" />
                                <ItemStyle Width="60px" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Observaciones" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lbObservaciones" runat="server" 
                                            Text='<%# Bind("Observaciones") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtObservaciones" runat="server" Text='<%# Bind("Observaciones") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <HeaderStyle Width="50px" />
                                    <ItemStyle Width="50px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="TodosLosNiveles" Visible="False">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtTodosLosNiveles" runat="server" Text='<%# Bind("TodosLosNiveles") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbTodosLosNiveles" runat="server" Text='<%# Bind("TodosLosNiveles") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vence" Visible="False">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtVence" runat="server" Text='<%# Bind("Vence") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbVence" runat="server" Text='<%# Bind("Vence") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="FechaVence" Visible="False">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtFechaVence" runat="server" Text='<%# Bind("FechaVence") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbFechaVence" runat="server" Text='<%# Bind("FechaVence", "{0:d}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                            <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                            <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                            <SelectedRowStyle BackColor="#F8AA55" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#FFF1D4" />
                            <SortedAscendingHeaderStyle BackColor="#B95C30" />
                            <SortedDescendingCellStyle BackColor="#F1E5CE" />
                            <SortedDescendingHeaderStyle BackColor="#93451F" />
                        </asp:GridView>                 
        </li>
    </ul>
    <ul>
        <li>
            <table align="center">
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        Descripción:</td>
                    <td>
                        <asp:TextBox ID="txtDescripcion" runat="server" MaxLength="40" Width="352px" 
                            TabIndex="20"></asp:TextBox>
                    </td>
                    <td>
                        Estatus:</td>
                    <td>
                        <asp:DropDownList ID="cbEstatus" runat="server" TabIndex="30">
                            <asp:ListItem Value="Activo">Activo</asp:ListItem>
                            <asp:ListItem Value="Inactivo">Inactivo</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        Observaciones:</td>
                    <td colspan="3">
                        <asp:TextBox ID="txtObservaciones" runat="server" MaxLength="400" Width="480px" 
                            TabIndex="40"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style1">
                        </td>
                    <td class="style1">
                        Fecha Vence:</td>
                    <td class="style1">
                        <asp:TextBox ID="txtFechaVence" runat="server" 
                            ToolTip="Si la promoción tiene fecha de vencimiento por favor ingresela aquí." 
                            Width="140px" MaxLength="10" TextMode="Date" TabIndex="42"></asp:TextBox>
                        <asp:TextBox ID="txtFechaVenceOculto" runat="server" TabIndex="43" 
                            Visible="False"></asp:TextBox>
                        <asp:CheckBox ID="cbIndefinida" runat="server" Checked="True" 
                            Text="La promoción es indefinida" 
                            ToolTip="Marque esta casilla si la promoción es por tiempo indefinido.  Si desmarca esta casilla deberá indicar la fecha de vencimiento de la promoción." 
                            AutoPostBack="True" oncheckedchanged="cbIndefinida_CheckedChanged" 
                            TabIndex="44" />
                    </td>
                    <td class="style1">
                    </td>
                    <td class="style1">
                    </td>
                    <td class="style1">
                        </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        Niveles Precio:</td>
                    <td>
                        <asp:CheckBox ID="cbTodosLosNiveles" runat="server" Checked="True" 
                            Text="La promoción aplicará en todos los niveles" 
                            
                            ToolTip="Marque esta casilla si la promoción se aplicará en todos los niveles, al desmarcarla se le pedirá en que niveles se debe aplicar." 
                            AutoPostBack="True" oncheckedchanged="cbTodosLosNiveles_CheckedChanged" 
                            TabIndex="46" />
                    </td>
                    <td>
                        <asp:LinkButton ID="lkMarcarTodosLosNiveles" runat="server" 
                            onclick="lkMarcarTodosLosNiveles_Click" 
                            ToolTip="Haga clic aquí para marcar todos los niveles de precio" 
                            Visible="False" TabIndex="47">Marcar</asp:LinkButton>
                    </td>
                    <td>
                        <asp:LinkButton ID="lkDesmarcarTodosLosNiveles" runat="server" 
                            onclick="lkDesmarcarTodosLosNiveles_Click" 
                            ToolTip="Haga clic aquí para desmarcar todos los niveles de precio" 
                            Visible="False" TabIndex="48">Desmarcar</asp:LinkButton>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </li>
        <li>
            <asp:GridView ID="gvNivelesPrecio" runat="server" AutoGenerateColumns="False" 
                    BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="3" CellSpacing="2" 
                    onrowdatabound="gvNivelesPrecio_RowDataBound" TabIndex="49" 
                    Visible="False">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="cbNivelPrecio" runat="server" Checked="True" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" />
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="NivelPrecio" HeaderText="Nivel de Precio" />
                        <asp:BoundField DataField="Factor" HeaderText="Factor" />
                        <asp:BoundField DataField="TipoVenta" HeaderText="TipoVenta" Visible="False" />
                        <asp:BoundField DataField="TipoDeVenta" HeaderText="Tipo de Venta" 
                            Visible="False" />
                        <asp:BoundField DataField="Financiera" HeaderText="Financiera" 
                            Visible="False" />
                        <asp:BoundField DataField="NombreFinanciera" HeaderText="Financiera" />
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" Visible="False" />
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" Visible="False" />
                        <asp:BoundField DataField="Pagos" HeaderText="Pagos" />
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#F8AA55" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
            <table align="center">
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        Tipo:</td>
                    <td>
                        <asp:DropDownList ID="cbTipo" runat="server" TabIndex="50" AutoPostBack="True" 
                            onselectedindexchanged="cbTipo_SelectedIndexChanged">
                            <asp:ListItem Value="Porcentaje">Porcentaje</asp:ListItem>
                            <asp:ListItem Value="Valor">Valor</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Label ID="lbTipo" runat="server" Text="Porcentaje:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPorcentaje" runat="server" Width="90px" TabIndex="60"></asp:TextBox>&nbsp;<asp:LinkButton ID="lbBuscarArticulo" runat="server" 
                            ToolTip="Haga clic aquí para buscar un artículo y agregarlo a este tipo de venta" 
                            TabIndex="70" onclick="lbBuscarArticulo_Click">Agregar artículos</asp:LinkButton>
                    </td>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                </table>
    <table id="tablaBusquedaDet" runat="server" visible="false" >
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="4">
                <asp:Label ID="Label6" runat="server" ForeColor="Red"></asp:Label>
                <asp:Label ID="Label7" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>
                Artículo:</td>
            <td>
                <asp:TextBox ID="codigoArticuloDet" runat="server" TabIndex="80" 
                    AutoPostBack="True" ontextchanged="codigoArticuloDet_TextChanged"></asp:TextBox>
            </td>
            <td>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                Nombre:</td>
            <td>
                <asp:TextBox ID="descripcionArticuloDet" runat="server" TabIndex="90" 
                    MaxLength="100" AutoPostBack="True" 
                    ontextchanged="descripcionArticuloDet_TextChanged"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="4" style="text-align: center">
                <asp:LinkButton ID="lbBuscarArticuloDet" runat="server" 
                    
                    
                    
                    ToolTip="Haga clic aquí para buscar artículos con los criterios de búsqueda ingresados" onclick="lbBuscarArticuloDet_Click" 
                    TabIndex="100">Buscar artículo</asp:LinkButton>&nbsp;&nbsp;&nbsp;|&nbsp;
                <asp:LinkButton ID="lbCerrarBusquedaDet" runat="server" 
                    ToolTip="Cierra la ventana de búsqueda" onclick="lbCerrarBusquedaDet_Click" 
                    TabIndex="110">Cerrar Búsqueda</asp:LinkButton>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="4" style="text-align: left">
                <asp:LinkButton 
                            ID="lbAgregarArtículo" runat="server" 
                            ToolTip="Haga clic aquí para agregar el o los artículos seleccionados." 
                            TabIndex="120" onclick="lbAgregarArtículo_Click">Agregar artículos seleccionados</asp:LinkButton>
                            &nbsp;&nbsp;
                            <asp:LinkButton ID="lkMarcarTodosArticulos" runat="server" 
                                ToolTip="Haga clic aquí para marcar todos los artículos" 
                                onclick="lkMarcarTodosArticulos_Click" TabIndex="122">Marcar todos</asp:LinkButton>
                            &nbsp;&nbsp;
                            <asp:LinkButton ID="lkDesMarcarTodosArticulos" runat="server" 
                                ToolTip="Haga clic aquí para desmarcar todos los artículos" 
                                onclick="lkDesMarcarTodosArticulos_Click" TabIndex="124">Desmarcar todos</asp:LinkButton>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        </table>
    <asp:GridView ID="gridArticulosDet" runat="server" CellPadding="3" TabIndex="130" 
                    AutoGenerateColumns="False" onrowdatabound="gridArticulosDet_RowDataBound" 
                    style="text-align: left" BackColor="#DEBA84" BorderColor="#DEBA84" 
                    BorderStyle="None" BorderWidth="1px" CellSpacing="2" Visible="False">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="cbArticulo" runat="server" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" />
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False" Visible="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1Det" runat="server" CausesValidation="False" 
                                    CommandName="Select" 
                                    Text="Seleccionar"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Articulo" HeaderText="Artículo" />
                        <asp:BoundField DataField="Descripción" HeaderText="Nombre del Artículo" />
                        <asp:BoundField DataField="PrecioLista" HeaderText="PrecioLista" 
                            Visible="False" />
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#F8AA55" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
        <asp:GridView ID="gridArticulos" runat="server" AutoGenerateColumns="False" 
                            BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                            CellPadding="3" CellSpacing="2" Font-Size="Small" 
                TabIndex="140" Visible="False" DataKeyNames="Articulo" 
                onrowdeleted="gridArticulos_RowDeleted" 
                onrowdeleting="gridArticulos_RowDeleting" 
                onselectedindexchanged="gridArticulos_SelectedIndexChanged" 
                onrowdatabound="gridArticulos_RowDataBound">
                            <Columns>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkEliminarArticulo" runat="server" CausesValidation="False" 
                                            CommandName="Select" onclick="lkEliminarArticulo_Click" 
                                            onclientclick="return confirm('Desea eliminar el artículo?');" Text="Eliminar"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Articulo" HeaderText="Artículo" >
                                <HeaderStyle Width="80px" />
                                <ItemStyle Width="80px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Nombre" HeaderText="Nombre del Artículo" >
                                <ItemStyle Font-Size="X-Small" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Tipo" HeaderText="Tipo" />
                                <asp:BoundField DataField="Valor" HeaderText="Valor ó %" 
                                    HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" 
                                    DataFormatString="{0:####,###,###,###,##.00}" >
<HeaderStyle HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Observaciones" HeaderText="Observaciones" Visible="False" />
                                <asp:TemplateField HeaderText="Costo">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Costo", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Costo", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Costo+IVA">
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("CostoIVA", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("CostoIVA", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Precio Lista">
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("PrecioLista", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("PrecioLista", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                            <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                            <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                            <SelectedRowStyle BackColor="#F8AA55" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#FFF1D4" />
                            <SortedAscendingHeaderStyle BackColor="#B95C30" />
                            <SortedDescendingCellStyle BackColor="#F1E5CE" />
                            <SortedDescendingHeaderStyle BackColor="#93451F" />
                        </asp:GridView>        </li>
        <li>
            <table align="center">
                <tr>
                    <td style="text-align: center">
                        &nbsp;
                        <asp:LinkButton ID="lkGrabar" runat="server" 
                            ToolTip="Haga clic aquí para grabar los datos del tipo de venta" TabIndex="260" 
                            onclick="lkGrabar_Click">Grabar</asp:LinkButton>
                        &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton ID="lkLimpiar" runat="server" 
                            ToolTip="Haga clic aquí para limpiar los datos de la página e ingresar un tipo de venta nuevo" 
                            TabIndex="270" onclick="lkLimpiar_Click">Limpiar</asp:LinkButton>
                    </td>
                </tr>
            </table>

        </li>
    </ul>
    </div> 
        <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server" TabIndex="520"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" TabIndex="530"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
