﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace PuntoDeVenta
{
    public partial class tiposventa : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                HtmlGenericControl submenu = new HtmlGenericControl();
                submenu = (HtmlGenericControl)Master.FindControl("submenu");
                submenu.Visible = true;

                HtmlGenericControl lkTiposVenta = new HtmlGenericControl();
                lkTiposVenta = (HtmlGenericControl)Master.FindControl("lkTiposVenta");
                lkTiposVenta.Visible = true;

                HtmlGenericControl lkFinancieras = new HtmlGenericControl();
                lkFinancieras = (HtmlGenericControl)Master.FindControl("lkFinancieras");
                lkFinancieras.Visible = true;

                HtmlGenericControl lkNivelesPrecio = new HtmlGenericControl();
                lkNivelesPrecio = (HtmlGenericControl)Master.FindControl("lkNivelesPrecio");
                lkNivelesPrecio.Visible = true;

                HtmlGenericControl lkCrearOfertas = new HtmlGenericControl();
                lkCrearOfertas = (HtmlGenericControl)Master.FindControl("lkCrearOfertas");
                lkCrearOfertas.Visible = true;

                HtmlGenericControl lkEnviarMails = new HtmlGenericControl();
                lkEnviarMails = (HtmlGenericControl)Master.FindControl("lkEnviarMails");
                lkEnviarMails.Visible = true;

                ViewState["url"] = Convert.ToString(Session["Url"]);
                ViewState["urlPrecios"] = Convert.ToString(Session["UrlPrecios"]);

                List<wsPuntoVenta.TipoVentaDet> q = new List<wsPuntoVenta.TipoVentaDet>();
                ViewState["qArticulos"] = q;

                asignaCodigoTipoVenta();
                txtDescripcion.Focus();
                ViewState["nuevo"] = true;

                lbAgregarArtículo.ToolTip = string.Format("Haga clic aquí para agregar el artículo seleccionado.{0}{0}NOTA: Puede marcar varios artículos y luego ingresar el tipo y el valor de modo que al hacer clic aquí se agregar todos los artículos seleccionados con las mismas característica definidas en el tipo y el valor", System.Environment.NewLine);
            }
        }

        void asignaCodigoTipoVenta()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            string mCeros = "";
            var qTiposVenta = ws.DevuelveTiposVenta();

            int mCodigo = qTiposVenta.Count() + 1;

            if (mCodigo < 10) mCeros = "00";
            if (mCodigo >= 10 && mCodigo <= 99) mCeros = "0";

            txtCodigo.Text = string.Format("P{0}{1}", mCeros, mCodigo.ToString());

            var q = ws.DevuelveNivelesPrecioContadoCredito("CC");

            gvNivelesPrecio.DataSource = q;
            gvNivelesPrecio.DataBind();
        }

        protected void lbBuscar_Click(object sender, EventArgs e)
        {
            if (lbBuscar.Text == "Buscar")
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var q = ws.DevuelveTiposVenta();

                gridTiposVenta.DataSource = q;
                gridTiposVenta.DataBind();
                gridTiposVenta.Visible = true;
                gridTiposVenta.SelectedIndex = -1;

                lbBuscar.Text = "Ocultar búsqueda";
                lbBuscar.ToolTip = "Haga clic aquí para ocultar la búsqueda de los tipos de venta";
            }
            else
            {
                ocultarBusquedaTiposVenta();
            }
        }

        void ocultarBusquedaTiposVenta()
        {
            lbBuscar.Text = "Buscar";
            lbBuscar.ToolTip = "Haga clic aquí para buscar una promoción, consultarla y/o modificar sus datos";
            gridTiposVenta.Visible = false;
        }

        protected void gridTiposVenta_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridTiposVenta_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = gridTiposVenta.SelectedRow;
            Label lbObservaciones = (Label)gvr.FindControl("lbObservaciones");
            Label lbTodosLosNiveles = (Label)gvr.FindControl("lbTodosLosNiveles");
            Label lbVence = (Label)gvr.FindControl("lbVence");
            Label lbFechaVence = (Label)gvr.FindControl("lbFechaVence");

            string mDescripcion = gvr.Cells[2].Text;
            string mObservaciones = lbObservaciones.Text;

            mDescripcion = mDescripcion.Replace("&#39;", "'").Replace("&#225;", "á").Replace("&#233;", "é").Replace("&#237;", "í").Replace("&#243;", "ó").Replace("&#243;", "ú").Replace("&quot;", "\"");
            mObservaciones = mObservaciones.Replace("&#39;", "'").Replace("&#225;", "á").Replace("&#233;", "é").Replace("&#237;", "í").Replace("&#243;", "ó").Replace("&#243;", "ú").Replace("&quot;", "\"");

            txtCodigo.Text = gvr.Cells[1].Text;
            txtDescripcion.Text = mDescripcion;
            cbEstatus.SelectedValue = gvr.Cells[3].Text;
            txtObservaciones.Text = mObservaciones;

            if (lbVence.Text == "S")
            {
                txtFechaVence.Text = lbFechaVence.Text;
                txtFechaVence.Attributes.Add("value", lbFechaVence.Text);
                txtFechaVenceOculto.Text = lbFechaVence.Text;
                
                cbIndefinida.Checked = false;
                txtFechaVence.Visible = false;
                txtFechaVenceOculto.Visible = true;
            }
            else
            {
                txtFechaVence.Text = "";
                cbIndefinida.Checked = true;
            }

            if (lbTodosLosNiveles.Text == "S")
            {
                cbTodosLosNiveles.Checked = true;
                gvNivelesPrecio.Visible = false;
                lkMarcarTodosLosNiveles.Visible = false;
                lkDesmarcarTodosLosNiveles.Visible = false;
            }
            else
            {
                cbTodosLosNiveles.Checked = false;
                gvNivelesPrecio.Visible = true;
                lkMarcarTodosLosNiveles.Visible = true;
                lkDesmarcarTodosLosNiveles.Visible = true;
            }

            txtCodigo.Enabled = false;
            txtCodigo.BorderStyle = BorderStyle.None;
            txtCodigo.BackColor = System.Drawing.Color.White;

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var qq = ws.DevuelveTiposVentaDet(txtCodigo.Text);
            List<wsPuntoVenta.TipoVentaDet> qqq = new List<wsPuntoVenta.TipoVentaDet>();

            for (int ii = 0; ii < qq.Length; ii++)
            {
                wsPuntoVenta.TipoVentaDet item = new wsPuntoVenta.TipoVentaDet();
                item.Articulo = qq[ii].Articulo;
                item.Nombre = qq[ii].Nombre;
                item.Observaciones = "";
                item.Tipo = qq[ii].Tipo;
                item.Valor = qq[ii].Valor;
                item.Costo = ws.CostoArticulo(qq[ii].Articulo);
                item.CostoIVA = ws.CostoArticulo(qq[ii].Articulo) * ws.iva();
                item.PrecioLista = ws.PrecioLista(qq[ii].Articulo, Convert.ToString(Session["Tienda"]));
                qqq.Add(item);
            }

            gridArticulos.DataSource = qqq;
            gridArticulos.DataBind();
            gridArticulos.Visible = true;

            CargarNiveles();

            ViewState["nuevo"] = false;
            ViewState["qArticulos"] = qqq;

            ocultarBusquedaTiposVenta();

            //var q = ws.DevuelveTiposVentaDet(txtCodigo.Text);

            //gridArticulos.DataSource = q;
            //gridArticulos.DataBind();
            //gridArticulos.Visible = true;
            //gridTiposVenta.SelectedIndex = -1;

            //ViewState["qArticulos"] = q;
            //ocultarBusquedaTiposVenta();
        }

        protected void lkLimpiar_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        void limpiar()
        {
            lbInfo.Text = "";
            lbError.Text = "";
            txtCodigo.Text = "";
            txtDescripcion.Text = "";
            cbEstatus.SelectedValue = "Activo";
            txtObservaciones.Text = "";
            txtPorcentaje.Text = "";

            txtCodigo.Enabled = true;
            txtCodigo.BorderStyle = BorderStyle.NotSet;

            List<wsPuntoVenta.TipoVentaDet> qEnBlanco = new List<wsPuntoVenta.TipoVentaDet>();
            ViewState["qArticulos"] = qEnBlanco;

            gridArticulos.DataSource = qEnBlanco;
            gridArticulos.DataBind();

            ocultarBusquedaTiposVenta();
            gridArticulos.Visible = false;
            tablaBusquedaDet.Visible = false;
            gridArticulosDet.Visible = false;
            gvNivelesPrecio.Visible = false;
            lkMarcarTodosLosNiveles.Visible = false;
            lkDesmarcarTodosLosNiveles.Visible = false;

            txtFechaVence.Text = "";
            txtFechaVenceOculto.Text = "";

            cbIndefinida.Checked = true;
            cbTodosLosNiveles.Checked = true;

            ViewState["nuevo"] = true;
            txtFechaVence.Visible = true;
            txtFechaVenceOculto.Visible = false;

            asignaCodigoTipoVenta();
            txtDescripcion.Focus();
        }

        protected void lbBuscarArticulo_Click(object sender, EventArgs e)
        {
            tablaBusquedaDet.Visible = true;
            gridArticulosDet.Visible = true;

            List<wsCambioPrecios.Articulos> q = new List<wsCambioPrecios.Articulos>();

            gridArticulosDet.DataSource = q;
            gridArticulosDet.DataBind();

            Label6.Text = "";
            Label7.Text = "";
            codigoArticuloDet.Text = "";
            descripcionArticuloDet.Text = "";
            descripcionArticuloDet.Focus();
        }

        protected void lbBuscarArticuloDet_Click(object sender, EventArgs e)
        {
            BuscarArticulo();
        }

        void BuscarArticulo()
        {
            string mTipo = "";

            Label6.Text = "";
            Label7.Text = "";

            if (codigoArticuloDet.Text.Trim().Length == 0 && descripcionArticuloDet.Text.Trim().Length == 0)
            {
                Label6.Text = "Debe ingresar un criterio de búsqueda ya sea el código, la descripción o ambos.";
                codigoArticuloDet.Focus();
                return;
            }

            if (codigoArticuloDet.Text.Trim().Length > 0) mTipo = "C";
            if (descripcionArticuloDet.Text.Trim().Length > 0) mTipo = "D";
            if (codigoArticuloDet.Text.Trim().Length > 0 && descripcionArticuloDet.Text.Trim().Length > 0) mTipo = "A";

            var ws = new wsCambioPrecios.wsCambioPrecios();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["urlPrecios"]);

            var q = ws.ListarArticulosCriterioConPrecio(mTipo, codigoArticuloDet.Text, descripcionArticuloDet.Text);
            var articulos = q.ToArray();

            gridArticulosDet.DataSource = articulos;
            gridArticulosDet.DataBind();

            descripcionArticuloDet.Focus();
            if (q.Length == 0)
            {
                Label6.Text = "No se encontraron artículos con el criterio de búsqueda ingresado.";
            }
            else
            {
                Label7.Text = string.Format("Se encontraron {0} artículos con el criterio de búsqueda ingresado.", q.Length);
            }
        }

        protected void lbCerrarBusquedaDet_Click(object sender, EventArgs e)
        {
            tablaBusquedaDet.Visible = false;
            gridArticulosDet.Visible = false;
        }

        protected void gridArticulosDet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void descripcionArticuloDet_TextChanged(object sender, EventArgs e)
        {
            BuscarArticulo();
        }

        protected void codigoArticuloDet_TextChanged(object sender, EventArgs e)
        {
            BuscarArticulo();
        }

        protected void lbAgregarArtículo_Click(object sender, EventArgs e)
        {
            if (txtPorcentaje.Text.Trim().Length == 0)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('Debe ingresar un valor o porcentaje válido.');", true);
                txtPorcentaje.Focus();
                return;
            }

            try
            {
                decimal mPctjValor = Convert.ToDecimal(txtPorcentaje.Text);

                if (mPctjValor <= 0)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('Debe ingresar un valor o porcentaje válido.');", true);
                    txtPorcentaje.Focus();
                    return;
                }
            }
            catch
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", "alert('Debe ingresar un valor o porcentaje válido.');", true);
                txtPorcentaje.Focus();
                return;
            }

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            List<wsPuntoVenta.TipoVentaDet> q = new List<wsPuntoVenta.TipoVentaDet>();
            q = (List<wsPuntoVenta.TipoVentaDet>)ViewState["qArticulos"];

            foreach (GridViewRow gvr in gridArticulosDet.Rows)
            {
                CheckBox cb = (CheckBox)gvr.FindControl("cbArticulo");

                if (cb.Checked)
                {
                    foreach (GridViewRow gvrArticulo in gridArticulos.Rows)
                    {
                        if (gvr.Cells[2].Text == gvrArticulo.Cells[1].Text)
                        {
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('El artículo {0} ya se encuentra dentro de esta promoción.');", gvr.Cells[2].Text), true);
                            return;
                        }
                    }
                       
                    wsPuntoVenta.TipoVentaDet item = new wsPuntoVenta.TipoVentaDet();
                    item.Articulo = gvr.Cells[2].Text;
                    item.Nombre = gvr.Cells[3].Text;
                    item.Observaciones = "";
                    item.Tipo = cbTipo.SelectedValue;
                    item.Valor = Convert.ToDecimal(txtPorcentaje.Text);
                    item.Costo = ws.CostoArticulo(gvr.Cells[2].Text);
                    item.CostoIVA = ws.CostoArticulo(gvr.Cells[2].Text) * ws.iva();
                    item.PrecioLista = ws.PrecioLista(gvr.Cells[2].Text, Convert.ToString(Session["Tienda"]));
                    q.Add(item);

                    gridArticulos.DataSource = q;
                    gridArticulos.DataBind();
                    gridArticulos.Visible = true;
                }

            }

            ViewState["qArticulos"] = q;
            tablaBusquedaDet.Visible = false;
            gridArticulosDet.Visible = false;
        }

        protected void cbTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbTipo.Text = string.Format("{0}:", cbTipo.Text);
        }

        protected void txtCodigo_TextChanged(object sender, EventArgs e)
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            ocultarBusquedaTiposVenta();
            gridArticulos.Visible = false;
            tablaBusquedaDet.Visible = false;
            gridArticulosDet.Visible = false;

            if (ws.ExisteTipoVenta(txtCodigo.Text))
            {
                var q = ws.DevuelveTipoVenta(txtCodigo.Text);

                txtDescripcion.Text = q[0].Descripcion;
                cbEstatus.SelectedValue = q[0].Estatus;
                txtObservaciones.Text = q[0].Observaciones;

                if (q[0].Vence == "S")
                {
                    txtFechaVence.Text = q[0].FechaVence.ToShortDateString();
                    txtFechaVence.Attributes.Add("value", q[0].FechaVence.ToShortDateString());
                    txtFechaVenceOculto.Text = q[0].FechaVence.ToShortDateString();

                    cbIndefinida.Checked = false;
                    txtFechaVence.Visible = false;
                    txtFechaVenceOculto.Visible = true;
                }
                else
                {
                    txtFechaVence.Text = "";
                    cbIndefinida.Checked = true;
                }

                if (q[0].TodosLosNiveles == "S")
                {
                    cbTodosLosNiveles.Checked = true;
                    gvNivelesPrecio.Visible = false;
                    lkMarcarTodosLosNiveles.Visible = false;
                    lkDesmarcarTodosLosNiveles.Visible = false;
                }
                else
                {
                    cbTodosLosNiveles.Checked = false;
                    gvNivelesPrecio.Visible = true;
                    lkMarcarTodosLosNiveles.Visible = true;
                    lkDesmarcarTodosLosNiveles.Visible = true;
                }

                txtCodigo.Enabled = false;
                txtCodigo.BorderStyle = BorderStyle.None;
                txtCodigo.BackColor = System.Drawing.Color.White;

                var qq = ws.DevuelveTiposVentaDet(txtCodigo.Text);
                List<wsPuntoVenta.TipoVentaDet> qqq = new List<wsPuntoVenta.TipoVentaDet>();

                for (int ii = 0; ii < qq.Length; ii++)
                {
                    wsPuntoVenta.TipoVentaDet item = new wsPuntoVenta.TipoVentaDet();
                    item.Articulo = qq[ii].Articulo;
                    item.Nombre = qq[ii].Nombre;
                    item.Observaciones = "";
                    item.Tipo = qq[ii].Tipo;
                    item.Valor = qq[ii].Valor;
                    item.Costo = ws.CostoArticulo(qq[ii].Articulo);
                    item.CostoIVA = ws.CostoArticulo(qq[ii].Articulo) * ws.iva();
                    item.PrecioLista = ws.PrecioLista(qq[ii].Articulo, Convert.ToString(Session["Tienda"]));
                    qqq.Add(item);
                }

                gridArticulos.DataSource = qqq;
                gridArticulos.DataBind();
                gridArticulos.Visible = true;

                CargarNiveles();

                ViewState["nuevo"] = false;
                ViewState["qArticulos"] = qqq;

                ocultarBusquedaTiposVenta();
                txtPorcentaje.Focus();
            }
            else
            {
                txtDescripcion.Focus();
            }
        }

        void CargarNiveles()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveTipoVentaNiveles(txtCodigo.Text);

            for (int ii = 0; ii < gvNivelesPrecio.Rows.Count; ii++)
            {
                CheckBox cbNivelPrecio = (CheckBox)gvNivelesPrecio.Rows[ii].FindControl("cbNivelPrecio");
                cbNivelPrecio.Checked = false;
                
                for (int jj = 0; jj < q.Length; jj++)
                {
                    string mNivel = q[jj].Nivel;
                    string mNivelGrid = gvNivelesPrecio.Rows[ii].Cells[1].Text;
                    if (q[jj].Nivel == gvNivelesPrecio.Rows[ii].Cells[1].Text) cbNivelPrecio.Checked = true;
                }
            }
        }

        bool pasaValidaciones() 
        {
            if (txtCodigo.Text.Trim().Length == 0)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('Debe ingresar el código de la promoción.');"), true);
                txtCodigo.Focus();
                return false;
            }
            if (txtDescripcion.Text.Trim().Length == 0)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('Debe ingresar la descripción de la promoción.');"), true);
                txtDescripcion.Focus();
                return false;
            }
            if (txtObservaciones.Text.Trim().Length == 0)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('Debe ingresar las observaciones de la promoción.');"), true);
                txtObservaciones.Focus();
                return false;
            }
            if (gridArticulos.Rows.Count == 0)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('Debe ingresar los artículos que se venderán con esta promoción.');"), true);
                txtPorcentaje.Focus();
                return false;
            }
            if (!cbIndefinida.Checked)
            {
                if (txtFechaVence.Text.Trim().Length == 0)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('Desmarcó que la promoción sea indefinida, debe ingresar la fecha de vencimiento de la misma.');"), true);
                    txtFechaVence.Focus();
                    return false;
                }

                DateTime mFechaTemp;

                try
                {
                    mFechaTemp = Convert.ToDateTime(txtFechaVence.Text);
                }
                catch
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('La fecha ingresada es inválida, el formato esperado es dd/MM/yyyy.');"), true);
                    txtFechaVence.Focus();
                    return false;
                }
            }

            bool mMarcoNivel = false;
            for (int ii = 0; ii < gvNivelesPrecio.Rows.Count; ii++)
            {
                CheckBox cbNivelPrecio = (CheckBox)gvNivelesPrecio.Rows[ii].FindControl("cbNivelPrecio");
                if (cbNivelPrecio.Checked) mMarcoNivel = true;
            }
            if (!mMarcoNivel)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('Debe marcar al menos un nivel para esta promoción.');"), true);
                cbTodosLosNiveles.Checked = false;
                gvNivelesPrecio.Visible = true;
                lkMarcarTodosLosNiveles.Visible = true;
                lkDesmarcarTodosLosNiveles.Visible = true;
                return false;
            }

            return true;
        }

        protected void lkGrabar_Click(object sender, EventArgs e)
        {
            if (!pasaValidaciones()) return;

            string mensaje = "";
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            lbError.Text = "";
            lbInfo.Text = "";

            wsPuntoVenta.TipoVentaDet[] q = new wsPuntoVenta.TipoVentaDet[1];

            int jj = 0;
            for (int ii = 0; ii < gridArticulos.Rows.Count; ii++)
            {
                jj++;

                wsPuntoVenta.TipoVentaDet item = new wsPuntoVenta.TipoVentaDet();
                item.Articulo = gridArticulos.Rows[ii].Cells[1].Text;
                item.Nombre = gridArticulos.Rows[ii].Cells[2].Text;
                item.Observaciones = gridArticulos.Rows[ii].Cells[5].Text;
                item.Tipo = gridArticulos.Rows[ii].Cells[3].Text;
                item.Valor = Convert.ToDecimal(gridArticulos.Rows[ii].Cells[4].Text);
                item.Costo = 0; //ws.CostoArticulo(gridArticulos.Rows[ii].Cells[1].Text);
                item.CostoIVA = 0; // ws.CostoArticulo(gridArticulos.Rows[ii].Cells[1].Text) * ws.iva();
                item.PrecioLista = 0; // ws.PrecioLista(gridArticulos.Rows[ii].Cells[1].Text);

                if (jj > 1) Array.Resize(ref q, jj);
                q[jj - 1] = item;
            }

            string[] niveles = new string[1];

            jj = 0;
            for (int ii = 0; ii < gvNivelesPrecio.Rows.Count; ii++)
            {
                CheckBox cbNivelPrecio = (CheckBox)gvNivelesPrecio.Rows[ii].FindControl("cbNivelPrecio");
                if (cbNivelPrecio.Checked)
                {
                    if (jj > 0) Array.Resize(ref niveles, jj + 1);
                    niveles[jj] = gvNivelesPrecio.Rows[ii].Cells[1].Text;

                    jj++;
                }
            }

            DateTime mFechaVence = new DateTime(1980, 1, 1); string mTodos = "S"; string mVence = "N";

            if (!cbTodosLosNiveles.Checked) mTodos = "N";
            if (!cbIndefinida.Checked)
            {
                mVence = "S";
                mFechaVence = Convert.ToDateTime(txtFechaVence.Text);

                if (!Convert.ToBoolean(ViewState["nuevo"]))
                {
                    DateTime mFechaTemp;

                    try
                    {
                        mFechaTemp = Convert.ToDateTime(txtFechaVenceOculto.Text);
                    }
                    catch
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('La fecha ingresada es inválida, el formato esperado es dd/MM/yyyy.');"), true);
                        txtFechaVenceOculto.Focus();
                        return;
                    }

                    mFechaVence = mFechaTemp;
                }
            }

            ws.Timeout = 99999999;
            if (!ws.GrabarTipoVenta(txtCodigo.Text, txtDescripcion.Text, cbEstatus.SelectedValue, txtObservaciones.Text, q, ref mensaje, Convert.ToString(Session["usuario"]), niveles, mTodos, mVence, mFechaVence))
            {
                lbError.Text = mensaje;
                txtPorcentaje.Focus();
                return;
            }

            //Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('{0}');", mensaje), true);
            lbInfo.Text = "";
            limpiar();
        }

        protected void lkEliminarArticulo_Click(object sender, EventArgs e)
        {
            //Me.GridView1.Rows(Convert.ToInt32(Me.GridView1.SelectedValue.ToString())).Cells(1).Text.ToString()
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('Pasé');"));
            //lbTipo.Text = gridArticulos.SelectedRow.Cells[1].Text;
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('Linea:  {0}.');", gridArticulos.Rows[Convert.ToInt32(gridArticulos.SelectedValue.ToString())].Cells[1].Text, true));
            //return confirm('Desea eliminar el artículo?');
            //List<wsPuntoVenta.TipoVentaDet> q = new List<wsPuntoVenta.TipoVentaDet>();
            //q = (List<wsPuntoVenta.TipoVentaDet>)ViewState["qArticulos"];

            //foreach (GridViewRow gvr in gridArticulos.Rows)
            //{
            //    if (gvr.Cells[1].Text != gridArticulos.SelectedRow.Cells[1].Text)
            //    {
            //        wsPuntoVenta.TipoVentaDet item = new wsPuntoVenta.TipoVentaDet();
            //        item.Articulo = gvr.Cells[2].Text;
            //        item.Nombre = gvr.Cells[3].Text;
            //        item.Observaciones = "";
            //        item.Tipo = cbTipo.SelectedValue;
            //        item.Valor = Convert.ToDecimal(txtPorcentaje.Text);
            //        q.Add(item);
            //    }
            //}

            //gridArticulos.DataSource = q;
            //gridArticulos.DataBind();

            //ViewState["qArticulos"] = q;
            //tablaBusquedaDet.Visible = false;
            //gridArticulosDet.Visible = false;
        }

        protected void gridArticulos_SelectedIndexChanged(object sender, EventArgs e)
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            List<wsPuntoVenta.TipoVentaDet> q = new List<wsPuntoVenta.TipoVentaDet>();

            foreach (GridViewRow gvr in gridArticulos.Rows)
            {
                if (gvr.Cells[1].Text != gridArticulos.SelectedRow.Cells[1].Text)
                {
                    string mNombre = gvr.Cells[2].Text;
                    string mObservaciones = gvr.Cells[5].Text;

                    mNombre = mNombre.Replace("&#39;", "'").Replace("&#225;", "á").Replace("&#233;", "é").Replace("&#237;", "í").Replace("&#243;", "ó").Replace("&#243;", "ú").Replace("&quot;", "\"");
                    mObservaciones = mObservaciones.Replace("&#39;", "'").Replace("&#225;", "á").Replace("&#233;", "é").Replace("&#237;", "í").Replace("&#243;", "ó").Replace("&#243;", "ú").Replace("&quot;", "\"");

                    wsPuntoVenta.TipoVentaDet item = new wsPuntoVenta.TipoVentaDet();
                    item.Articulo = gvr.Cells[1].Text; //Tarjeta Libre&#39;s a&#225; e&#233; i&#237; o&#243; u&#243; comillas&quot;
                    item.Nombre = mNombre;
                    item.Observaciones = mObservaciones;
                    item.Tipo = gvr.Cells[3].Text;
                    item.Valor = Convert.ToDecimal(gvr.Cells[4].Text);
                    item.Costo = ws.CostoArticulo(gvr.Cells[1].Text);
                    item.CostoIVA = ws.CostoArticulo(gvr.Cells[1].Text) * ws.iva();
                    item.PrecioLista = ws.PrecioLista(gvr.Cells[1].Text, Convert.ToString(Session["Tienda"]));
                    q.Add(item);
                }
            }

            gridArticulos.DataSource = q;
            gridArticulos.DataBind();
            gridArticulos.SelectedIndex = -1;

            ViewState["qArticulos"] = q;
            tablaBusquedaDet.Visible = false;
            gridArticulosDet.Visible = false;
        }

        protected void gridArticulos_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void gridArticulos_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {

        }

        protected void gridArticulos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void lkMarcarTodosArticulos_Click(object sender, EventArgs e)
        {
            for (int ii = 0; ii < gridArticulosDet.Rows.Count; ii++)
            {
                CheckBox cbArticulo = (CheckBox)gridArticulosDet.Rows[ii].FindControl("cbArticulo");
                cbArticulo.Checked = true;
            }
        }

        protected void lkDesMarcarTodosArticulos_Click(object sender, EventArgs e)
        {
            for (int ii = 0; ii < gridArticulosDet.Rows.Count; ii++)
            {
                CheckBox cbArticulo = (CheckBox)gridArticulosDet.Rows[ii].FindControl("cbArticulo");
                cbArticulo.Checked = false;
            }
        }

        protected void gvNivelesPrecio_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void cbTodosLosNiveles_CheckedChanged(object sender, EventArgs e)
        {
            gvNivelesPrecio.Visible = !cbTodosLosNiveles.Checked;
            lkMarcarTodosLosNiveles.Visible = !cbTodosLosNiveles.Checked;
            lkDesmarcarTodosLosNiveles.Visible = !cbTodosLosNiveles.Checked;

            if (cbTodosLosNiveles.Checked)
            {
                for (int ii = 0; ii < gvNivelesPrecio.Rows.Count; ii++)
                {
                    CheckBox cbNivelPrecio = (CheckBox)gvNivelesPrecio.Rows[ii].FindControl("cbNivelPrecio");
                    cbNivelPrecio.Checked = true;
                }
            }
        }

        protected void cbIndefinida_CheckedChanged(object sender, EventArgs e)
        {
            if (cbIndefinida.Checked) txtFechaVence.Text = "";
        }

        protected void lkMarcarTodosLosNiveles_Click(object sender, EventArgs e)
        {
            for (int ii = 0; ii < gvNivelesPrecio.Rows.Count; ii++)
            {
                CheckBox cbNivelPrecio = (CheckBox)gvNivelesPrecio.Rows[ii].FindControl("cbNivelPrecio");
                cbNivelPrecio.Checked = true;
            }
        }

        protected void lkDesmarcarTodosLosNiveles_Click(object sender, EventArgs e)
        {
            for (int ii = 0; ii < gvNivelesPrecio.Rows.Count; ii++)
            {
                CheckBox cbNivelPrecio = (CheckBox)gvNivelesPrecio.Rows[ii].FindControl("cbNivelPrecio");
                cbNivelPrecio.Checked = false;
            }
        }

    }
}