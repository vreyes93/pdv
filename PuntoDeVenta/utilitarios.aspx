﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="utilitarios.aspx.cs" Inherits="PuntoDeVenta.utilitarios" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .auto-style1 {
            height: 26px;
        }
        .auto-style2 {
            height: 27px;
        }
        </style>
    <script type="text/javascript">
        function ScrollTop() {
            window.scrollTo(0, 0);
        }

        function OcultarTablas() {
            $("#tblInventarioBases").hide();
            $("#tblInventarioCamas").hide();
            $("#tblInventarioClosets").hide();
            $("#tblInventarioComedores").hide();
            $("#tblInventarioMateriaPrima").hide();
            $("#tblInventarioPromociones").hide();
            $("#tblInventarioSalas").hide();
            $("#tblInventarioVarios").hide();
        }

        function pageLoad() {
            $(document).ready(function () {
                OcultarTablas();
                $("#tblInventarioBotones").hide();

                $("#lkInventarioBotones").click(function () {
                    if ($("#lkInventarioBotones").text() == "-") {
                        $("#tblInventarioBotones").hide();
                        $("#lkInventarioBotones").text("+");
                        OcultarTablas();
                    }
                    else {
                        $("#tblInventarioBotones").show();
                        $("#lkInventarioBotones").text("-");
                    }
                });

                $("#lkInventarioBases").click(function () {
                    OcultarTablas();
                    $("#tblInventarioBases").show();
                });

                $("#lkInventarioCamas").click(function () {
                    OcultarTablas();
                    $("#tblInventarioCamas").show();
                });

                $("#lkInventarioClosets").click(function () {
                    OcultarTablas();
                    $("#tblInventarioClosets").show();
                });

                $("#lkInventarioComedores").click(function () {
                    OcultarTablas();
                    $("#tblInventarioComedores").show();
                });

                $("#lkInventarioMateriaPrima").click(function () {
                    OcultarTablas();
                    $("#tblInventarioMateriaPrima").show();
                });

                $("#lkInventarioPromocionales").click(function () {
                    OcultarTablas();
                    $("#tblInventarioPromociones").show();
                });

                $("#lkInventarioSalas").click(function () {
                    OcultarTablas();
                    $("#tblInventarioSalas").show();
                });

                $("#lkInventarioVarios").click(function () {
                    OcultarTablas();
                    $("#tblInventarioVarios").show();
                });
            })
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>&nbsp;&nbsp;Utilitarios </h3>
<div class="content2">
    <div class="clientes">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <ul>
            <li>
                <table align="center">
                    <tr>
                        <td>
                            <asp:LinkButton ID="lkInventario" runat="server" onclick="lkInventario_Click" ToolTip="Haga clic aquí para generar el inventario de su tienda">Generar listado para toma de inventario físico</asp:LinkButton>
                            &nbsp;&nbsp;
                            <asp:LinkButton ID="lkInventarioExcel" runat="server" 
                                onclick="lkInventarioExcel_Click" 
                                ToolTip="Haga clic aqui para descargar el inventario de su tienda en EXCEL" 
                                Visible="False">Excel</asp:LinkButton>                            
                            <asp:LinkButton ID="lkImprimirInventario" runat="server" 
                                onclick="lkImprimirInventario_Click" 
                                ToolTip="Haga clic aqui para imprimir el inventario de su tienda" 
                                Visible="False">Imprimir</asp:LinkButton>
                            <asp:LinkButton ID="lkOcultarInventario" runat="server" 
                                onclick="lkOcultarInventario_Click" 
                                ToolTip="Haga clic aquí para ocultar el inventario de su tienda" 
                                Visible="False">Ocultar</asp:LinkButton>
                        </td>
                        <td style="text-align: right">
                            <asp:FileUpload ID="ArchivoInventario" runat="server" ToolTip="Haga clic aquí para buscar el archivo PDF del inventario" />
                            <asp:LinkButton ID="lkGrabarPDF" runat="server" 
                                ToolTip="Haga clic aquí para cargar el archivo PDF del inventario" 
                                onclick="lkGrabarPDF_Click">Cargar</asp:LinkButton>
                            <asp:Label ID="lbErrorInventario" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                            <asp:Label ID="lbInfoInventario" runat="server"></asp:Label>
                        </td>
                    </tr>
                    </table>
            </li>
            <li>
                <asp:GridView ID="gridInventario" runat="server" AutoGenerateColumns="False" 
                    BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="3" CellSpacing="2" 
                    onrowdatabound="gridInventario_RowDataBound" Visible="False">
                    <Columns>
                        <asp:TemplateField HeaderText="Artículo">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulo" runat="server" Text='<%# Bind("Articulo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulo" runat="server" Text='<%# Bind("Articulo") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="90px" />
                            <ItemStyle Width="90px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Descripción">
                            <ItemTemplate>
                                <asp:Label ID="lbDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="640px" />
                            <ItemStyle Font-Size="Small" Width="640px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Localización">
                            <ItemTemplate>
                                <asp:Label ID="lbLocalizacion" runat="server" Text='<%# Bind("Localizacion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtLocalizacion" runat="server" Text='<%# Bind("Localizacion") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Font-Size="Small" />
                            <ItemStyle Font-Size="Small" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ultima Compra">
                            <ItemTemplate>
                                <asp:Label ID="lbUltimaCompra" runat="server" Text='<%# Bind("UltimaCompra", "{0:d}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtUltimaCompra" runat="server" Text='<%# Bind("UltimaCompra") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Font-Size="Small" HorizontalAlign="Center" />
                            <ItemStyle Font-Size="Small" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="No Escribir">
                            <ItemTemplate>
                                <asp:Label ID="lbBlanco1" runat="server" Text='<%# Bind("Blanco1") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtBlanco1" runat="server" Text='<%# Bind("Blanco1") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Font-Size="Small" />
                            <ItemStyle Font-Size="Small" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Existencia Total">
                            <ItemTemplate>
                                <asp:Label ID="lbExistencias" runat="server" Text='<%# Bind("Existencias") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtExistencias" runat="server" Text='<%# Bind("Existencias") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Font-Size="Small" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Existencia Física">
                            <ItemTemplate>
                                <asp:Label ID="lbBlanco2" runat="server" Text='<%# Bind("Blanco2") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtBlanco2" runat="server" Text='<%# Bind("Blanco2") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Font-Size="Small" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
            </li>
            <li>

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                
                <table>
                    <tr>
                        <td colspan="2">
                            <a>Toma de Inventario:</a></td>
                        <td style="text-align: center">
                            <asp:LinkButton ID="lkInventarioIniciar" runat="server" 
                                ToolTip="Iniciar el ingreso de la toma de inventario" TabIndex="22" 
                                onclick="lkInventarioIniciar_Click">Iniciar</asp:LinkButton>
                            &nbsp;&nbsp;
                            <asp:LinkButton ID="lkInventarioAnterior" runat="server" TabIndex="24" 
                                ToolTip="Regresar 10 items" onclick="lkInventarioAnterior_Click">&lt;&lt; Anterior</asp:LinkButton>
                            &nbsp;&nbsp;
                            <asp:LinkButton ID="lkInventarioSiguiente" runat="server" TabIndex="26" 
                                ToolTip="Avanzar 10 items" onclick="lkInventarioSiguiente_Click">Siguiente &gt;&gt;</asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;Página:
                            <asp:TextBox ID="txtInventarioPagina" runat="server" TabIndex="28" 
                                ToolTip="Ingrese la página a la que desea ir en el inventario" 
                                Width="40px" 
                                ontextchanged="txtInventarioPagina_TextChanged"></asp:TextBox>
                            <asp:LinkButton ID="lkInventarioPaginaIr" runat="server" TabIndex="30" 
                                ToolTip="Ir a la página ingresada" onclick="lkInventarioPaginaIr_Click">Ir</asp:LinkButton>
                        </td>
                        <td>
                            Buscar:
                            <asp:TextBox ID="txtBuscarArticuloInventario" runat="server"  
                                ontextchanged="txtBuscarArticuloInventario_TextChanged" 
                                style="text-transform: uppercase;" 
                                ToolTip="Ingrese el artículo a buscar y presione ENTER.  Si ingresó números, el sistema buscará códigos, si ingresa texto, el sistema buscará dentro de la descripción de los artículos" 
                                Width="130px" TabIndex="32"></asp:TextBox>
                            <asp:LinkButton ID="lkBuscarArticuloInventario" runat="server" 
                                onclick="lkBuscarArticuloInventario_Click" 
                                
                                ToolTip="Buscar artículos según el criterio de búsqueda ingresado.  Si ingresó números, el sistema buscará códigos, si ingresa texto, el sistema buscará dentro de la descripción de los artículos" 
                                TabIndex="34">Ir</asp:LinkButton>
                                &nbsp;&nbsp;
                            <asp:LinkButton ID="lkVerInventario" runat="server" 
                                onclick="lkVerInventario_Click" ToolTip="Listar el inventario digitado" 
                                TabIndex="36">Ver</asp:LinkButton>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="javascript:void();" id="lkInventarioBotones" >+</a>
<%--                            <asp:LinkButton ID="lkInventarioBotones" runat="server" 
                                ToolTip="Muestra u oculta las demás opciones de filtros del inventario" 
                                TabIndex="36" onclick="lkInventarioBotones_Click">+</asp:LinkButton>--%>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div class="interno">
                                <table id="tblInventarioBotones" align="center">
                                    <tr>
                                        <td style="text-align: center">
                                            <a href="javascript:void();" id="lkInventarioBases" >Bases</a>
                                            &nbsp;&nbsp;&nbsp;
                                            <a href="javascript:void();" id="lkInventarioCamas" >Colchones</a>
                                            &nbsp;&nbsp;&nbsp;
                                            <a href="javascript:void();" id="lkInventarioClosets" >Closets</a>
                                            &nbsp;&nbsp;&nbsp;
                                            <a href="javascript:void();" id="lkInventarioComedores" >Comedores</a>
                                            &nbsp;&nbsp;&nbsp;
                                            <a href="javascript:void();" id="lkInventarioMateriaPrima" >Materia Prima</a>
                                            &nbsp;&nbsp;&nbsp;
                                            <a href="javascript:void();" id="lkInventarioPromocionales" >Promocionales</a>
                                            &nbsp;&nbsp;&nbsp;
                                            <a href="javascript:void();" id="lkInventarioSalas" >Salas</a>
                                            &nbsp;&nbsp;&nbsp;
                                            <a href="javascript:void();" id="lkInventarioVarios" >Varios</a>
                                        </td>
                                    </tr>
                                </table>
<%--                                <table ID="tblInventarioBotones" runat="server" align="center" visible="false">
                                    <tr>
                                        <td style="text-align: center">
                                            <asp:LinkButton ID="lkInventarioBases" runat="server" 
                                                onclick="lkInventarioBases_Click" ToolTip="Filtros de bases">Bases</asp:LinkButton>
                                                &nbsp;&nbsp;&nbsp;
                                            <asp:LinkButton ID="lkInventarioCamas" runat="server" 
                                                onclick="lkInventarioCamas_Click" ToolTip="Filtros de colchones">Colchones</asp:LinkButton>
                                                &nbsp;&nbsp;&nbsp;
                                            <asp:LinkButton ID="lkInventarioClosets" runat="server" 
                                                onclick="lkInventarioClosets_Click" ToolTip="Filtros de Closets">Closets</asp:LinkButton>
                                                &nbsp;&nbsp;&nbsp;
                                                <asp:LinkButton ID="lkInventarioComedores" runat="server" 
                                                ToolTip="Comedores" onclick="lkInventarioComedores_Click">Comedores</asp:LinkButton>
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:LinkButton ID="lkInventarioMateriaPrima" runat="server" 
                                                onclick="lkInventarioMateriaPrima_Click" 
                                                ToolTip="Filtros de Materia Prima">Materia Prima</asp:LinkButton>
                                                &nbsp;&nbsp;&nbsp;
                                            <asp:LinkButton ID="lkInventarioPromocionales" runat="server" 
                                                onclick="lkInventarioPromocionales_Click" 
                                                ToolTip="Filtros de artículos promocionales">Promocionales</asp:LinkButton>
                                                &nbsp;&nbsp;&nbsp;
                                            <asp:LinkButton ID="lkInventarioSalas" runat="server" 
                                                onclick="lkInventarioSalas_Click" ToolTip="Filtros de salas">Salas</asp:LinkButton>
                                                &nbsp;&nbsp;&nbsp;
                                            <asp:LinkButton ID="lkInventarioVarios" runat="server" 
                                                onclick="lkInventarioVarios_Click" ToolTip="Filtros de productos varios">Varios</asp:LinkButton>
                                            </td>
                                    </tr>
                                </table>
--%>

                                <table id="tblInventarioBases" align="center">
                                    <tr>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>
                                            Fiesta:</td>
                                        <td>
                                            <asp:Button ID="btnBasesBackcare" runat="server" CssClass="botonredondo" 
                                                Text="Backcare" ToolTip="Bases Backcare Fiesta" 
                                                onclick="btnBasesBackcare_Click" />
                                            <asp:Button ID="btnBasesDeLujo" runat="server" CssClass="botonredondo" 
                                                Text="De Lujo" ToolTip="Bases de Lujo Fiesta" 
                                                onclick="btnBasesDeLujo_Click" />
                                            <asp:Button ID="btnBasesDeLuxe" runat="server" CssClass="botonredondo" 
                                                Text="Deluxe" ToolTip="Bases Deluxe Fiesta" 
                                                onclick="btnBasesDeLuxe_Click" />
                                            <asp:Button ID="btnBasesDreamSleeper" runat="server" CssClass="botonredondo" 
                                                Text="Dream Sleeper" ToolTip="Bases DreamSleeper Fiesta" 
                                                onclick="btnBasesDreamSleeper_Click" />
                                            <asp:Button ID="btnBasesErgonomic" runat="server" CssClass="botonredondo" 
                                                Text="Ergonomic" ToolTip="Bases Ergonomic Fiesta" 
                                                onclick="btnBasesErgonomic_Click" />
                                            <asp:Button ID="btnBasesEstelar" runat="server" CssClass="botonredondo" 
                                                Text="Estelar" ToolTip="Bases Estelar Fiesta" 
                                                onclick="btnBasesEstelar_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnBasesEvolution" runat="server" CssClass="botonredondo" 
                                                Text="Evolution" ToolTip="Bases Evolution Fiesta" 
                                                onclick="btnBasesEvolution_Click" />
                                            <asp:Button ID="btnBasesFrescoFoam" runat="server" CssClass="botonredondo" 
                                                Text="Fresco Foam" ToolTip="Bases FrescoFoam Fiesta" 
                                                onclick="btnBasesFrescoFoam_Click" />
                                            <asp:Button ID="btnBasesLuxurious" runat="server" CssClass="botonredondo" 
                                                Text="Luxurious" ToolTip="Bases Luxurious Fiesta" 
                                                onclick="btnBasesLuxurious_Click" />
                                        </td>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            <asp:Button ID="btnBasesPlush" runat="server" CssClass="botonredondo" 
                                                Text="Plush" ToolTip="Bases Plush Fiesta" onclick="btnBasesPlush_Click" />
                                            <asp:Button ID="btnBasesPremier" runat="server" CssClass="botonredondo" 
                                                Text="Premier" ToolTip="Bases Premier Fiesta" 
                                                onclick="btnBasesPremier_Click" />
                                            <asp:Button ID="btnBasesStandard" runat="server" CssClass="botonredondo" 
                                                Text="Standard" ToolTip="Bases de Standard Fiesta" 
                                                onclick="btnBasesStandard_Click" />
                                            <asp:Button ID="btnBasesTripleCrown" runat="server" CssClass="botonredondo" 
                                                Text="Triple Crown" ToolTip="Bases Triple Crown Fiesta" 
                                                onclick="btnBasesTripleCrown_Click" />
                                            <asp:Button ID="btnBasesTrueEnergy" runat="server" CssClass="botonredondo" 
                                                Text="True Energy" ToolTip="Bases True Energy Fiesta" 
                                                onclick="btnBasesTrueEnergy_Click" />
                                            <asp:Button ID="btnBasesWonder" runat="server" CssClass="botonredondo" 
                                                Text="Wonder" ToolTip="Bases Wonder Fiesta" 
                                                onclick="btnBasesWonder_Click" />
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            Mayoreo:</td>
                                        <td>
                                            <asp:Button ID="btnBasesBlackM" runat="server" CssClass="botonredondo" 
                                                Text="Black" ToolTip="Bases Black Mayoreo" 
                                                onclick="btnBasesBlackM_Click" />
                                            <asp:Button ID="btnBasesBackcareM" runat="server" CssClass="botonredondo" 
                                                Text="Backcare" ToolTip="Bases Backcare Mayoreo" 
                                                onclick="btnBasesBackcareM_Click" />
                                            <asp:Button ID="btnBasesDeLujoM" runat="server" CssClass="botonredondo" 
                                                Text="De Lujo" ToolTip="Bases de Lujo Mayoreo" 
                                                onclick="btnBasesDeLujoM_Click" />
                                            <asp:Button ID="btnBasesStandardM" runat="server" CssClass="botonredondo" 
                                                Text="Standard" ToolTip="Bases Standard Mayoreo" 
                                                onclick="btnBasesStandardM_Click" />
                                            <asp:Button ID="btnBasesTrueEnergyM" runat="server" CssClass="botonredondo" 
                                                Text="True Energy" ToolTip="Bases True Energy Mayoreo" 
                                                onclick="btnBasesTrueEnergyM_Click" />
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>

                                <table id="tblInventarioCamas" align="center">
                                    <tr>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>
                                            Fiesta:</td>
                                        <td>
                                            <asp:Button ID="btnCamasAdvanced" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasAdvanced_Click" Text="Advanced" 
                                                ToolTip="Colchones Advanced Fiesta" />
                                            <asp:Button ID="btnCamasBackcare" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasBackcare_Click" Text="Backcare" 
                                                ToolTip="Colchones Backcare Fiesta" />
                                            <asp:Button ID="btnCamasDeluxe" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasDeluxe_Click" Text="Deluxe" 
                                                ToolTip="Colchones Deluxe Fiesta" />
                                            <asp:Button ID="btnCamasDreamSleeper" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasDreamSleeper_Click" Text="Dream Sleeper" 
                                                ToolTip="Colchones Dream Sleeper Fiesta" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCamasEvolution" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasEvolution_Click" Text="Evolution" 
                                                ToolTip="Colchones Evolution Fiesta" />
                                            <asp:Button ID="btnCamasFirm" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasFirm_Click" Text="Firm" 
                                                ToolTip="Colchones Beautysleep Firm Fiesta" />
                                            <asp:Button ID="btnCamasFrescoFoam" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasFrescoFoam_Click" Text="Fresco Foam" 
                                                ToolTip="Colchones Fresco Foam Fiesta" />
                                            <asp:Button ID="btnCamasLuxurious" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasLuxurious_Click" Text="Luxurious" 
                                                ToolTip="Colchones Luxurious Fiesta" />
                                        </td>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            <asp:Button ID="btnCamasPlush" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasPlush_Click" Text="Plush" 
                                                ToolTip="Colchones Beautysleep Plush Fiesta" />
                                            <asp:Button ID="btnCamasPureEssence" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasPureEssence_Click" Text="Pure Essence" 
                                                ToolTip="Colchones True Energy Pure Essence Fiesta" />
                                            <asp:Button ID="btnCamasRecharge" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasRecharge_Click" Text="Recharge" 
                                                ToolTip="Colchones Bty Rest Recharge Fiesta" />
                                            <asp:Button ID="btnCamasSimmons" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasSimmons_Click" Text="Simmons" 
                                                ToolTip="Colchones Simmons Fiesta" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCamasTripleCrown" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasTripleCrown_Click" Text="Triple Crown" 
                                                ToolTip="Colchones Triple Crown Fiesta" />
                                            <asp:Button ID="btnCamasWonder" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasWonder_Click" Text="Wonder" 
                                                ToolTip="Colchones Wonder Fiesta" />
                                            <asp:Button ID="btnCamasWorldClass" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasWorldClass_Click" Text="World Class" 
                                                ToolTip="Colchones World Class Fiesta" />
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            Mayoreo:</td>
                                        <td>
                                            <asp:Button ID="btnCamasAdvancedM" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasAdvancedM_Click" Text="Advanced" 
                                                ToolTip="Colchones Advanced Mayoreo" />
                                            <asp:Button ID="btnCamasBackcareM" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasBackcareM_Click" Text="Backcare" 
                                                ToolTip="Colchones Backcare Mayoreo" />
                                            <asp:Button ID="btnCamasDeluxeM" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasDeluxeM_Click" Text="Deluxe" 
                                                ToolTip="Colchones Deluxe Mayoreo" />
                                            <asp:Button ID="btnCamasDreamSleeperM" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasDreamSleeperM_Click" Text="Dream Sleeper" 
                                                ToolTip="Colchones Dream Sleeper Mayoreo" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCamasEvolutionM" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasEvolutionM_Click" Text="Evolution" 
                                                ToolTip="Colchones Evolution Mayoreo" />
                                            <asp:Button ID="btnCamasFirmM" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasFirmM_Click" Text="Firm" 
                                                ToolTip="Colchones Beautysleep Firm Mayoreo" />
                                            <asp:Button ID="btnCamasFrescoFoamM" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasFrescoFoamM_Click" Text="Fresco Foam" 
                                                ToolTip="Colchones Fresco Foam Mayoreo" />
                                            <asp:Button ID="btnCamasLuxuriousM" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasLuxuriousM_Click" Text="Luxurious" 
                                                ToolTip="Colchones Luxurious Mayoreo" />
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            <asp:Button ID="btnCamasPlushM" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasPlushM_Click" Text="Plush" 
                                                ToolTip="Colchones Beautysleep Plush Mayoreo" />
                                            <asp:Button ID="btnCamasPureEssenceM" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasPureEssenceM_Click" Text="Pure Essence" 
                                                ToolTip="Colchones True Energy Pure Essence Mayoreo" />
                                            <asp:Button ID="btnCamasRechargeM" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasRechargeM_Click" Text="Recharge" 
                                                ToolTip="Colchones Bty Rest Recharge Mayoreo" />
                                            <asp:Button ID="btnCamasSimmonsM" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasSimmonsM_Click" Text="Simmons" 
                                                ToolTip="Colchones Simmons Mayoreo" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCamasTripleCrownM" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasTripleCrownM_Click" Text="Triple Crown" 
                                                ToolTip="Colchones Triple Crown Mayoreo" />
                                            <asp:Button ID="btnCamasWonderM" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasWonderM_Click" Text="Wonder" 
                                                ToolTip="Colchones Wonder Mayoreo" />
                                            <asp:Button ID="btnCamasWorldClassM" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasWorldClassM_Click" Text="World Class" 
                                                ToolTip="Colchones World Class Mayoreo" />
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                                <table id="tblInventarioClosets" align="center">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnClosetsAtitude" runat="server" CssClass="botonredondo" 
                                                onclick="btnClosetsAtitude_Click" Text="Atitude" ToolTip="Closets Atitude" />
                                            <asp:Button ID="btnClosetsAvai" runat="server" CssClass="botonredondo" 
                                                onclick="btnClosetsAvai_Click" Text="Avai" ToolTip="Closets Avai" />
                                            <asp:Button ID="btnClosetsB548" runat="server" CssClass="botonredondo" 
                                                onclick="btnClosetsB548_Click" Text="B548" ToolTip="Closets B548" />
                                            <asp:Button ID="btnClosetsBetim" runat="server" CssClass="botonredondo" 
                                                onclick="btnClosetsBetim_Click" Text="Betim" ToolTip="Closets Betim" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnClosetsCaribe" runat="server" CssClass="botonredondo" 
                                                onclick="btnClosetsCaribe_Click" Text="Caribe" ToolTip="Closets Caribe" />
                                            <asp:Button ID="btnClosetsCriative" runat="server" CssClass="botonredondo" 
                                                onclick="btnClosetsCriative_Click" Text="Criative" ToolTip="Closets Criative" />
                                            <asp:Button ID="btnClosetsCrystal" runat="server" CssClass="botonredondo" 
                                                onclick="btnClosetsCrystal_Click" Text="Crystal" ToolTip="Closets Crystal" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnClosetsEsquina" runat="server" CssClass="botonredondo" 
                                                onclick="btnClosetsEsquina_Click" Text="Esquina" ToolTip="Closets Esquina" />
                                            <asp:Button ID="btnClosetsEU502" runat="server" CssClass="botonredondo" 
                                                onclick="btnClosetsEU502_Click" Text="EU502" ToolTip="Closets EU502" />
                                            <asp:Button ID="btnClosetsFelicce" runat="server" CssClass="botonredondo" 
                                                onclick="btnClosetsFelicce_Click" Text="Felicce" ToolTip="Closets Felicce" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnClosetsHavana" runat="server" CssClass="botonredondo" 
                                                onclick="btnClosetsHavana_Click" Text="Havana" ToolTip="Closets Havana" />
                                            <asp:Button ID="btnClosetsImola" runat="server" CssClass="botonredondo" 
                                                onclick="btnClosetsImola_Click" Text="Imola" ToolTip="Closets Imola" />
                                            <asp:Button ID="btnClosetsItatiba" runat="server" CssClass="botonredondo" 
                                                onclick="btnClosetsItatiba_Click" Text="Itatiba" ToolTip="Closets Itatiba" />
                                            <asp:Button ID="btnClosetsLena" runat="server" CssClass="botonredondo" 
                                                onclick="btnClosetsLena_Click" Text="Lena" ToolTip="Closets Lena" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnClosetsLaguna" runat="server" CssClass="botonredondo" 
                                                onclick="btnClosetsLaguna_Click" Text="Laguna" ToolTip="Closets Laguna" />
                                            <asp:Button ID="btnClosetsNewBolivia" runat="server" CssClass="botonredondo" 
                                                onclick="btnClosetsNewBolivia_Click" Text="New Bolivia" 
                                                ToolTip="Closets New Bolivia" />
                                            <asp:Button ID="btnClosetsReno" runat="server" CssClass="botonredondo" 
                                                onclick="btnClosetsReno_Click" Text="Reno" ToolTip="Closets Reno" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnClosetsSino" runat="server" CssClass="botonredondo" 
                                                onclick="btnClosetsSino_Click" Text="Sino" ToolTip="Closets Sino" />
                                            <asp:Button ID="btnClosetsSanDiego" runat="server" CssClass="botonredondo" 
                                                onclick="btnClosetsSanDiego_Click" Text="San Diego" 
                                                ToolTip="Closets San Diego" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCamasCarro" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasCarro_Click" Text="Carro" ToolTip="Cama Carro Fiesta" 
                                                Visible="False" />
                                            <asp:Button ID="btnCamasCarroM" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasCarroM_Click" Text="Carro" ToolTip="Cama Carro Mayoreo" 
                                                Visible="False" />
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                                <table id="tblInventarioPromociones" align="center">
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td style="text-align: center">
                                            <asp:Button ID="btnInventarioPromoCamas" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioPromoCamas_Click" Text="Camas" 
                                                ToolTip="Promoción / Camas (021)" />
                                            <asp:Button ID="btnInventarioPromoComedorCocina" runat="server" 
                                                CssClass="botonredondo" onclick="btnInventarioPromoComedorCocina_Click" 
                                                Text="Comedor-Cocina" ToolTip="Promoción / Comedor - Cocina (023)" />
                                            <asp:Button ID="btnInventarioPromoElectronicos" runat="server" 
                                                CssClass="botonredondo" onclick="btnInventarioPromoElectronicos_Click" 
                                                Text="Electrónicos" ToolTip="Promoción / Productos Electrónicos (024)" />
                                            <asp:Button ID="btnInventarioPromoGenericos" runat="server" 
                                                CssClass="botonredondo" onclick="btnInventarioPromoGenericos_Click" 
                                                Text="Genéricos" ToolTip="Promoción / Genéricos (020)" />
                                            <asp:Button ID="btnInventarioPromoInfantil" runat="server" 
                                                CssClass="botonredondo" onclick="btnInventarioPromoInfantil_Click" 
                                                Text="Infantil" ToolTip="Promoción / Infantil (022)" />
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                                <table id="tblInventarioMateriaPrima" align="center">
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td style="text-align: center">
                                            <asp:Button ID="btnInventarioBolsas" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioBolsas_Click" Text="Bolsas" 
                                                ToolTip="Materia Prima / Bolsa (094)" />
                                            <asp:Button ID="btnInventarioCarton" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioCarton_Click" Text="Cartón" 
                                                ToolTip="Materia Prima / Cartón (092)" />
                                            <asp:Button ID="btnInventarioEmpaque" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioEmpaque_Click" Text="Empaque" 
                                                ToolTip="Material de Empaque (080)" />
                                            <asp:Button ID="btnInventarioEntretela" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioEntretela_Click" Text="Entretela" 
                                                ToolTip="Materia Prima / Entretela (095)" />
                                            <asp:Button ID="btnInventarioEsponja" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioEsponja_Click" Text="Esponja" 
                                                ToolTip="Materia Prima / Esponja (093)" />
                                            <asp:Button ID="btnInventarioMarcos" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioMarcos_Click" Text="Marcos" 
                                                ToolTip="Materia Prima / Marcos (091)" />
                                            <asp:Button ID="btnInventarioVarios" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioVarios_Click" Text="Varios" 
                                                ToolTip="Materia Prima / Varios (096)" />
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                                <table id="tblInventarioVarios" align="center">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnInventarioAlmohadas" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioAlmohadas_Click" Text="Almohadas" ToolTip="Almohadas" />
                                            <asp:Button ID="btnInventarioBar" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioBar_Click" Text="Bares" ToolTip="Bares" Width="50px" />
                                            <asp:Button ID="btnInventarioBaules" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioBaules_Click" Text="Baúles" ToolTip="Baúles" />
                                            <asp:Button ID="btnInventarioBasesLujo" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioBasesLujo_Click" Text="Bases de Lujo" 
                                                ToolTip="Bases de Lujo" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioBufeteras" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioBufeteras_Click" Text="Bufeteras" ToolTip="Bufeteras" />
                                            <asp:Button ID="btnInventarioCocinas" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioCocinas_Click" Text="Cocinas" ToolTip="Cocinas" />
                                            <asp:Button ID="btnInventarioColchonetas" runat="server" 
                                                CssClass="botonredondo" onclick="btnInventarioColchonetas_Click" 
                                                Text="Colchonetas" ToolTip="Colchonetas" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioComodas" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioComodas_Click" Text="Cómodas" ToolTip="Cómodas (115)" />
                                            <asp:Button ID="btnInventarioCubrecamas" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioCubrecamas_Click" Text="Cubrecamas" 
                                                ToolTip="Cubrecamas" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioCuna" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioCuna_Click" Text="Cunas" ToolTip="Colchón cuna" />
                                            <asp:Button ID="btnInventarioDecoracion" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioDecoracion_Click" Text="Decoración" 
                                                ToolTip="Decoración / Uso de Tienda (010 y 011)" />
                                            <asp:Button ID="btnInventarioDormitorios" runat="server" 
                                                CssClass="botonredondo" onclick="btnInventarioDormitorios_Click" 
                                                Text="Dormitorios" 
                                                ToolTip="Cabeceras, mesas de noche, dormitorios y camas que estén en la categoría de dormitorios (111, 112, 113 y 119)" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnInventarioEscritorios" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioEscritorios_Click" Text="Escritorios" 
                                                ToolTip="Escritorios" />
                                            <asp:Button ID="btnInventarioFundas" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioFundas_Click" Text="Fundas" ToolTip="Fundas" />
                                            <asp:Button ID="btnInventarioGabinetes" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioGabinetes_Click" Text="Gabinetes" ToolTip="Gabinetes" />
                                            <asp:Button ID="btnInventarioGavetero" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioGavetero_Click" Text="Gaveteros" 
                                                ToolTip="Gaveteros (114)" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioLibreras" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioLibreras_Click" Text="Libreras" ToolTip="Libreras" />
                                            <asp:Button ID="btnInventarioLiteras" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioLiteras_Click" Text="Literas" ToolTip="Literas (135)" />
                                            <asp:Button ID="btnCamasLuisFelipe" runat="server" CssClass="botonredondo" 
                                                onclick="btnCamasLuisFelipe_Click" Text="Luis Felipe" 
                                                ToolTip="Colchones y Bases / Luis Felipe" />
                                            <asp:Button ID="btnInventarioMesas" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioMesas_Click" Text="Mesas" ToolTip="Mesas" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioModulos" runat="server" 
                                                CssClass="botonredondo" onclick="btnInventarioModulos_Click" 
                                                Text="Módulos" ToolTip="Módulos" />
                                            <asp:Button ID="btnInventarioRacks" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioRacks_Click" Text="Racks" ToolTip="Racks" />
                                            <asp:Button ID="btnInventarioRoperos" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioRoperos_Click" Text="Roperos" ToolTip="Roperos" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioReclinables" runat="server" 
                                                CssClass="botonredondo" onclick="btnInventarioReclinables_Click" 
                                                Text="Reclinables" ToolTip="Reclinables" />
                                            <asp:Button ID="btnInventarioSabanas" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioSabanas_Click" Text="Sábanas" ToolTip="Sábanas" />
                                            <asp:Button ID="btnInventarioSillas" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioSillas_Click" Text="Sillas" ToolTip="Sillas" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnInventarioSofaCama" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioSofaCama_Click" Text="Sofás Cama" ToolTip="Sofás Cama" />
                                            <asp:Button ID="btnInventarioTrinchantes" runat="server" 
                                                CssClass="botonredondo" onclick="btnInventarioTrinchantes_Click" 
                                                Text="Trinchantes" ToolTip="Trinchantes" />
                                            <asp:Button ID="btnInventarioZapateras" runat="server" CssClass="botonredondo" 
                                                onclick="btnInventarioZapateras_Click" Text="Zapateras" ToolTip="Zapateras" />
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                                <table id="tblInventarioSalas" align="center">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnInventario2109" runat="server" CssClass="botonredondo" 
                                                Text="2109" ToolTip="2109" onclick="btnInventario2109_Click" />
                                            <asp:Button ID="btnInventario2309" runat="server" CssClass="botonredondo" 
                                                Text="2309" ToolTip="2309" onclick="btnInventario2309_Click" />
                                            <asp:Button ID="btnInventario2409" runat="server" CssClass="botonredondo" 
                                                Text="2409" ToolTip="2409" onclick="btnInventario2409_Click" />
                                            <asp:Button ID="btnInventarioAris" runat="server" CssClass="botonredondo" 
                                                Text="Aris" ToolTip="Aris" onclick="btnInventarioAris_Click" />
                                            <asp:Button ID="btnInventarioArles" runat="server" CssClass="botonredondo" 
                                                Text="Arles" ToolTip="Arles" onclick="btnInventarioArles_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioBari" runat="server" CssClass="botonredondo" 
                                                Text="Bari" ToolTip="Bari" onclick="btnInventarioBari_Click" />
                                            <asp:Button ID="btnInventarioBasilea" runat="server" CssClass="botonredondo" 
                                                Text="Basilea" ToolTip="Basilea" onclick="btnInventarioBasilea_Click" />
                                            <asp:Button ID="btnInventarioCarolina" runat="server" CssClass="botonredondo" 
                                                Text="Carolina" ToolTip="Carolina" onclick="btnInventarioCarolina_Click" />
                                            <asp:Button ID="btnInventarioChelsea" runat="server" CssClass="botonredondo" 
                                                Text="Chelsea" ToolTip="Chelsea" onclick="btnInventarioChelsea_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioChenille" runat="server" CssClass="botonredondo" 
                                                Text="Chenille" ToolTip="Chenille" onclick="btnInventarioChenille_Click" />
                                            <asp:Button ID="btnInventarioCiry" runat="server" CssClass="botonredondo" 
                                                Text="Ciry" ToolTip="Ciry" onclick="btnInventarioCiry_Click" />
                                            <asp:Button ID="btnInventarioCristy" runat="server" CssClass="botonredondo" 
                                                Text="Cristy" ToolTip="Cristy" onclick="btnInventarioCristy_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioCorduroy" runat="server" CssClass="botonredondo" 
                                                Text="Corduroy" ToolTip="Corduroy" onclick="btnInventarioCorduroy_Click" />
                                            <asp:Button ID="btnInventarioCuerina" runat="server" CssClass="botonredondo" 
                                                Text="Cuerina" ToolTip="Cuerina" onclick="btnInventarioCuerina_Click" />
                                            <asp:Button ID="btnInventarioCuero" runat="server" CssClass="botonredondo" 
                                                Text="Cuero" ToolTip="Cuero" onclick="btnInventarioCuero_Click" />
                                            <asp:Button ID="btnInventarioDaVinci" runat="server" CssClass="botonredondo" 
                                                Text="Da Vinci" ToolTip="Da Vinci" onclick="btnInventarioDaVinci_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnInventarioDiva" runat="server" CssClass="botonredondo" 
                                                Text="Diva" ToolTip="Diva" onclick="btnInventarioDiva_Click" />
                                            <asp:Button ID="btnInventarioDouglas" runat="server" CssClass="botonredondo" 
                                                Text="Douglas" ToolTip="Douglas" onclick="btnInventarioDouglas_Click" />
                                            <asp:Button ID="btnInventarioElizabeth" runat="server" CssClass="botonredondo" 
                                                Text="Elizabeth" ToolTip="Elizabeth" 
                                                onclick="btnInventarioElizabeth_Click" />
                                            <asp:Button ID="btnInventarioEnnia" runat="server" CssClass="botonredondo" 
                                                Text="Ennia" ToolTip="Ennia" onclick="btnInventarioEnnia_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioEscalona" runat="server" CssClass="botonredondo" 
                                                Text="Escalona" ToolTip="Escalona" onclick="btnInventarioEscalona_Click" />
                                            <asp:Button ID="btnInventarioEternity" runat="server" CssClass="botonredondo" 
                                                Text="Eternity" ToolTip="Eternity" onclick="btnInventarioEternity_Click" />
                                            <asp:Button ID="btnInventarioFashion" runat="server" CssClass="botonredondo" 
                                                Text="Fashion" ToolTip="Fashion" onclick="btnInventarioFashion_Click" />
                                            <asp:Button ID="btnInventarioGreta" runat="server" CssClass="botonredondo" 
                                                Text="Greta" ToolTip="Greta" onclick="btnInventarioGreta_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioHazel" runat="server" CssClass="botonredondo" 
                                                Text="Hazel" ToolTip="Hazel" onclick="btnInventarioHazel_Click" />
                                            <asp:Button ID="btnInventarioImperio" runat="server" CssClass="botonredondo" 
                                                Text="Imperio" ToolTip="Imperio" onclick="btnInventarioImperio_Click" />
                                            <asp:Button ID="btnInventarioIsabel" runat="server" CssClass="botonredondo" 
                                                Text="Isabel" ToolTip="Isabel" onclick="btnInventarioIsabel_Click" />
                                            <asp:Button ID="btnInventarioKelly" runat="server" CssClass="botonredondo" 
                                                Text="Kelly" ToolTip="Kelly" onclick="btnInventarioKelly_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioLexington" runat="server" CssClass="botonredondo" 
                                                Text="Lexington" ToolTip="Lexington" 
                                                onclick="btnInventarioLexington_Click" />
                                            <asp:Button ID="btnInventarioLiverpool" runat="server" CssClass="botonredondo" 
                                                Text="Liverpool" ToolTip="Liverpool" 
                                                onclick="btnInventarioLiverpool_Click" />
                                            <asp:Button ID="btnInventarioLoal" runat="server" CssClass="botonredondo" 
                                                Text="Loal" ToolTip="Loal" onclick="btnInventarioLoal_Click" />
                                            <asp:Button ID="btnInventarioLondres" runat="server" CssClass="botonredondo" 
                                                Text="Londres" ToolTip="Londres" onclick="btnInventarioLondres_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnInventarioLucas" runat="server" CssClass="botonredondo" 
                                                Text="Lucas" ToolTip="Lucas" onclick="btnInventarioLucas_Click" />
                                            <asp:Button ID="btnInventarioMallorca" runat="server" CssClass="botonredondo" 
                                                Text="Mallorca" ToolTip="Mallorca" onclick="btnInventarioMallorca_Click" />
                                            <asp:Button ID="btnInventarioMichelle" runat="server" CssClass="botonredondo" 
                                                Text="Michelle" ToolTip="Michelle" onclick="btnInventarioMichelle_Click" />
                                            <asp:Button ID="btnInventarioMilan" runat="server" CssClass="botonredondo" 
                                                Text="Milan" ToolTip="Milan" onclick="btnInventarioMilan_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioMicrofibra" runat="server" CssClass="botonredondo" 
                                                Text="Microfibra" ToolTip="Microfibra" 
                                                onclick="btnInventarioMicrofibra_Click" />
                                            <asp:Button ID="btnInventarioMicrotex" runat="server" CssClass="botonredondo" 
                                                Text="Microtex" ToolTip="Microtex" onclick="btnInventarioMicrotex_Click" />
                                            <asp:Button ID="btnInventarioMirka" runat="server" CssClass="botonredondo" 
                                                Text="Mirka" ToolTip="Mirka" onclick="btnInventarioMirka_Click" />
                                            <asp:Button ID="btnInventarioMisisipi" runat="server" CssClass="botonredondo" 
                                                Text="Misisipi" ToolTip="Misisipi" onclick="btnInventarioMisisipi_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioModena" runat="server" CssClass="botonredondo" 
                                                Text="Modena" ToolTip="Modena" onclick="btnInventarioModena_Click" />
                                            <asp:Button ID="btnInventarioModular" runat="server" CssClass="botonredondo" 
                                                Text="Modular" ToolTip="Modular" onclick="btnInventarioModular_Click" />
                                            <asp:Button ID="btnInventarioMonet" runat="server" CssClass="botonredondo" 
                                                Text="Monet" ToolTip="Monet" onclick="btnInventarioMonet_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioMontblanc" runat="server" CssClass="botonredondo" 
                                                Text="Montblanc" ToolTip="Montblanc" 
                                                onclick="btnInventarioMontblanc_Click" />
                                            <asp:Button ID="btnInventarioMontecarlo" runat="server" CssClass="botonredondo" 
                                                Text="Montecarlo" ToolTip="Montecarlo" 
                                                onclick="btnInventarioMontecarlo_Click" />
                                            <asp:Button ID="btnInventarioPatricia" runat="server" CssClass="botonredondo" 
                                                Text="Patricia" ToolTip="Patricia" onclick="btnInventarioPatricia_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnInventarioPicasso" runat="server" CssClass="botonredondo" 
                                                Text="Picasso" ToolTip="Picasso" onclick="btnInventarioPicasso_Click" />
                                            <asp:Button ID="btnInventarioPontevedra" runat="server" CssClass="botonredondo" 
                                                Text="Pontevedra" ToolTip="Pontevedra" 
                                                onclick="btnInventarioPontevedra_Click" />
                                            <asp:Button ID="btnInventarioRio" runat="server" CssClass="botonredondo" 
                                                Text="Rio" ToolTip="Rio" onclick="btnInventarioRio_Click" />
                                            <asp:Button ID="btnInventarioScalia" runat="server" CssClass="botonredondo" 
                                                Text="Scalia" ToolTip="Scalia" onclick="btnInventarioScalia_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioSeattle" runat="server" CssClass="botonredondo" 
                                                Text="Seattle" ToolTip="Seattle" onclick="btnInventarioSeattle_Click" />
                                            <asp:Button ID="btnInventarioSerena" runat="server" CssClass="botonredondo" 
                                                Text="Serena" ToolTip="Serena" onclick="btnInventarioSerena_Click" />
                                            <asp:Button ID="btnInventarioSuecia" runat="server" CssClass="botonredondo" 
                                                Text="Suecia" ToolTip="Suecia" onclick="btnInventarioSuecia_Click" />
                                            <asp:Button ID="btnInventarioTriunfo" runat="server" CssClass="botonredondo" 
                                                Text="Triunfo" ToolTip="Triunfo" onclick="btnInventarioTriunfo_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioVelvet" runat="server" CssClass="botonredondo" 
                                                Text="Velvet" ToolTip="Velvet" onclick="btnInventarioVelvet_Click" />
                                            <asp:Button ID="btnInventarioVenecia" runat="server" CssClass="botonredondo" 
                                                Text="Venecia" ToolTip="Venecia" onclick="btnInventarioVenecia_Click" />
                                            <asp:Button ID="btnInventarioVerona" runat="server" CssClass="botonredondo" 
                                                Text="Verona" ToolTip="Verona" onclick="btnInventarioVerona_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioVeronica" runat="server" CssClass="botonredondo" 
                                                Text="Veronica" ToolTip="Veronica" onclick="btnInventarioVeronica_Click" />
                                            <asp:Button ID="btnInventarioViscaya" runat="server" CssClass="botonredondo" 
                                                Text="Viscaya" ToolTip="Viscaya" onclick="btnInventarioViscaya_Click" />
                                            <asp:Button ID="btnInventarioWestin" runat="server" CssClass="botonredondo" 
                                                Text="Westin" ToolTip="Westin" onclick="btnInventarioWestin_Click" />
                                        </td>
                                    </tr>
                                </table>

                                <table id="tblInventarioComedores" align="center">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnInventarioAvion" runat="server" CssClass="botonredondo" 
                                                Text="Avion" ToolTip="Avion" onclick="btnInventarioAvion_Click" />
                                            <asp:Button ID="btnInventarioBarcelona" runat="server" CssClass="botonredondo" 
                                                Text="Barcelona" ToolTip="Barcelona" 
                                                onclick="btnInventarioBarcelona_Click" />
                                            <asp:Button ID="btnInventarioBianca" runat="server" CssClass="botonredondo" 
                                                Text="Bianca" ToolTip="Bianca" onclick="btnInventarioBianca_Click" />
                                            <asp:Button ID="btnInventarioBoleado" runat="server" CssClass="botonredondo" 
                                                Text="Boleado" ToolTip="Boleado" onclick="btnInventarioBoleado_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioBuda" runat="server" CssClass="botonredondo" 
                                                Text="Buda" ToolTip="Buda" onclick="btnInventarioBuda_Click" />
                                            <asp:Button ID="btnInventarioCuadrado" runat="server" CssClass="botonredondo" 
                                                Text="Cuadrado" ToolTip="Cuadrado" onclick="btnInventarioCuadrado_Click" />
                                            <asp:Button ID="btnInventarioDubai" runat="server" CssClass="botonredondo" 
                                                Text="Dubai" ToolTip="Dubai" onclick="btnInventarioDubai_Click" />
                                            <asp:Button ID="btnInventarioEspania" runat="server" CssClass="botonredondo" 
                                                Text="España" ToolTip="España" onclick="btnInventarioEspania_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioFlorencia" runat="server" CssClass="botonredondo" 
                                                Text="Florencia" ToolTip="Florencia" 
                                                onclick="btnInventarioFlorencia_Click" />
                                            <asp:Button ID="btnInventarioFrances" runat="server" CssClass="botonredondo" 
                                                Text="Frances" ToolTip="Frances" onclick="btnInventarioFrances_Click" />
                                            <asp:Button ID="btnInventarioHamburgo" runat="server" CssClass="botonredondo" 
                                                Text="Hamburgo" ToolTip="Hamburgo" onclick="btnInventarioHamburgo_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioKeenan" runat="server" CssClass="botonredondo" 
                                                Text="Keenan" ToolTip="Keenan" onclick="btnInventarioKeenan_Click" />
                                            <asp:Button ID="btnInventarioLisboa" runat="server" CssClass="botonredondo" 
                                                Text="Lisboa" ToolTip="Lisboa" onclick="btnInventarioLisboa_Click" />
                                            <asp:Button ID="btnInventarioMajestic" runat="server" CssClass="botonredondo" 
                                                Text="Majestic" ToolTip="Majestic" onclick="btnInventarioMajestic_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnInventarioMalibu" runat="server" CssClass="botonredondo" 
                                                Text="Malibu" ToolTip="Malibu" onclick="btnInventarioMalibu_Click" />
                                            <asp:Button ID="btnInventarioMarmol" runat="server" CssClass="botonredondo" 
                                                Text="Marmol" ToolTip="Marmol" onclick="btnInventarioMarmol_Click" />
                                            <asp:Button ID="btnInventarioMartel" runat="server" CssClass="botonredondo" 
                                                Text="Martel" ToolTip="Martel" onclick="btnInventarioMartel_Click" />
                                            <asp:Button ID="btnInventarioMontana" runat="server" CssClass="botonredondo" 
                                                Text="Montana" ToolTip="Montana" onclick="btnInventarioMontana_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioOmega" runat="server" CssClass="botonredondo" 
                                                Text="Omega" ToolTip="Omega" onclick="btnInventarioOmega_Click" />
                                            <asp:Button ID="btnInventarioOriental" runat="server" CssClass="botonredondo" 
                                                Text="Oriental" ToolTip="Oriental" onclick="btnInventarioOriental_Click" />
                                            <asp:Button ID="btnInventarioOval" runat="server" CssClass="botonredondo" 
                                                Text="Oval" ToolTip="Oval" onclick="btnInventarioOval_Click" />
                                            <asp:Button ID="btnInventarioPekin" runat="server" CssClass="botonredondo" 
                                                Text="Pekin" ToolTip="Pekin" onclick="btnInventarioPekin_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioPedestales" runat="server" CssClass="botonredondo" 
                                                Text="Pedestales" ToolTip="Pedestales" 
                                                onclick="btnInventarioPedestales_Click" />
                                            <asp:Button ID="btnInventarioRectangular" runat="server" 
                                                CssClass="botonredondo" Text="Rectangular" ToolTip="Rectangular" 
                                                onclick="btnInventarioRectangular_Click" />
                                            <asp:Button ID="btnInventarioRecto" runat="server" CssClass="botonredondo" 
                                                Text="Recto" ToolTip="Recto(a)" onclick="btnInventarioRecto_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioRed" runat="server" CssClass="botonredondo" 
                                                Text="Red" ToolTip="Red" onclick="btnInventarioRed_Click" />
                                            <asp:Button ID="btnInventarioRedondo" runat="server" CssClass="botonredondo" 
                                                Text="Redondo" ToolTip="Redondo" onclick="btnInventarioRedondo_Click" />
                                            <asp:Button ID="btnInventarioRegina" runat="server" CssClass="botonredondo" 
                                                Text="Regina" ToolTip="Regina" onclick="btnInventarioRegina_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnInventarioRegency" runat="server" CssClass="botonredondo" 
                                                Text="Regency" ToolTip="Regency" onclick="btnInventarioRegency_Click" />
                                            <asp:Button ID="btnInventarioReina" runat="server" CssClass="botonredondo" 
                                                Text="Reina" ToolTip="Reina" onclick="btnInventarioReina_Click" />
                                            <asp:Button ID="btnInventarioRimo" runat="server" CssClass="botonredondo" 
                                                Text="Rimo" ToolTip="Rimo" onclick="btnInventarioRimo_Click" />
                                            <asp:Button ID="btnInventarioSiena" runat="server" CssClass="botonredondo" 
                                                Text="Siena" ToolTip="Siena" onclick="btnInventarioSiena_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioSillaCurva" runat="server" CssClass="botonredondo" 
                                                Text="Silla Curva" ToolTip="Silla Curva" 
                                                onclick="btnInventarioSillaCurva_Click" />
                                            <asp:Button ID="btnInventarioSillaForrada" runat="server" 
                                                CssClass="botonredondo" Text="Silla Forrada" ToolTip="Silla Forrada" 
                                                onclick="btnInventarioSillaForrada_Click" />
                                            <asp:Button ID="btnInventarioTripode" runat="server" CssClass="botonredondo" 
                                                Text="Tripode" ToolTip="Tripode" onclick="btnInventarioTripode_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioVancouver" runat="server" CssClass="botonredondo" 
                                                Text="Vancouver" ToolTip="Vancouver" 
                                                onclick="btnInventarioVancouver_Click" />
                                            <asp:Button ID="btnInventarioValeria" runat="server" CssClass="botonredondo" 
                                                Text="Valeria" ToolTip="Valeria" onclick="btnInventarioValeria_Click" />
                                            <asp:Button ID="btnInventarioVictoria" runat="server" CssClass="botonredondo" 
                                                Text="Victoria" ToolTip="Victoria" onclick="btnInventarioVictoria_Click" />
                                            <asp:Button ID="btnInventarioViena" runat="server" CssClass="botonredondo" 
                                                Text="Viena" ToolTip="Viena" onclick="btnInventarioViena_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnInventarioVirginia" runat="server" CssClass="botonredondo" 
                                                Text="Virginia" ToolTip="Virginia" onclick="btnInventarioVirginia_Click" />
                                            <asp:Button ID="btnInventarioWestlake" runat="server" CssClass="botonredondo" 
                                                Text="Westlake" ToolTip="Westlake" onclick="btnInventarioWestlake_Click" />
                                            <asp:Button ID="btnInventarioWoodmont" runat="server" CssClass="botonredondo" 
                                                Text="Woodmont" ToolTip="Woodmont" onclick="btnInventarioWoodmont_Click" />
                                        </td>
                                    </tr>
                                </table>

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                        &nbsp;</td>
                        <td colspan="2" style="text-align: right">
                            <asp:LinkButton ID="lkCargarInventario" runat="server" 
                                onclick="lkCargarInventario_Click" Visible="False">Cargar inventario F05 - Marzo 2015</asp:LinkButton>
                            <asp:Label ID="lbErrorInventarioGrabar" runat="server" Font-Bold="True" 
                                ForeColor="Red" Visible="False"></asp:Label>
                            <asp:Label ID="lbInfoInventarioGrabar" runat="server" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                                    <asp:GridView ID="gridInventarioGrabar" runat="server" AutoGenerateColumns="False" 
                                        BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                                        CellPadding="3" CellSpacing="2"
                                        onrowdatabound="gridInventarioGrabar_RowDataBound" Visible="False" 
                                        Width="900px" onrowcancelingedit="gridInventarioGrabar_RowCancelingEdit" 
                                        onrowediting="gridInventarioGrabar_RowEditing" 
                                        onrowupdating="gridInventarioGrabar_RowUpdating">
                                        <Columns>
                                            <asp:TemplateField ShowHeader="False" Visible="false">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lkSeleccionarArticuloInventario" runat="server" 
                                                        CausesValidation="False" CommandName="Select" Text="Seleccionar" 
                                                        ToolTip="Haga clic aquí para seleccionar el artículo e ingresar o modificar la toma de inventario físico"></asp:LinkButton>
                                                </ItemTemplate>
                                                <HeaderStyle Width="80px" />
                                                <ItemStyle Width="80px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Artículo">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtArticulo" runat="server" ReadOnly="true" Width="90px" Text='<%# Bind("Articulo") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbArticulo" runat="server" Text='<%# Bind("Articulo") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="90px" />
                                                <ItemStyle Width="90px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Descripción" Visible="true">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtDescripcion" runat="server" ReadOnly="true" Width="475px" Font-Size="X-Small" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                                                    Observaciones:
                                                    <asp:TextBox ID="txtObservacionesInventario" runat="server" 
                                                        Text='<%# Bind("Observaciones")%>' Width="475px" Font-Size="X-Small" 
                                                        TabIndex="12" ontextchanged="txtObservacionesInventario_TextChanged"
                                                        ToolTip="Ingrese aquí alguna observación respecto al conteo físico de este artículo" 
                                                        AutoPostBack="True"></asp:TextBox>
                                                </EditItemTemplate>
                                                <HeaderStyle Width="500px" />
                                                <ItemStyle Font-Size="X-Small" Width="500px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Localización" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbLocalizacion" runat="server" Text='<%# Bind("Localizacion") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtLocalizacion" runat="server" Text='<%# Bind("Localizacion") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <HeaderStyle Font-Size="Small" Width="100px" />
                                                <ItemStyle Font-Size="Small" Width="100px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Ultima Compra" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbUltimaCompra" runat="server" Text='<%# Bind("UltimaCompra", "{0:d}") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtUltimaCompra" runat="server" Text='<%# Bind("UltimaCompra") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <HeaderStyle Font-Size="Small" HorizontalAlign="Center" />
                                                <ItemStyle Font-Size="Small" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="No Escribir" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbBlanco1" runat="server" Text='<%# Bind("Blanco1") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtBlanco1" runat="server" Text='<%# Bind("Blanco1") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <HeaderStyle Font-Size="Small" />
                                                <ItemStyle Font-Size="Small" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Existencia Total" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbExistencias" runat="server" Text='<%# Bind("Existencias") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtExistencias" runat="server" Text='<%# Bind("Existencias") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <HeaderStyle Font-Size="Small" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Existencia Física" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbBlanco2" runat="server" Text='<%# Bind("Blanco2", "{0:####,###,###,###,##0}") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtBlanco2" runat="server" Text='<%# Bind("Blanco2", "{0:####,###,###,###,##0}") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <HeaderStyle Font-Size="Small" HorizontalAlign="Center" Width="110px" />
                                                <ItemStyle HorizontalAlign="Center" Width="110px" />
                                            </asp:TemplateField>
                                            <asp:CommandField ShowEditButton="True" CausesValidation="False" EditText="Editar" UpdateText="Actualizar" CancelText = "Cancelar" >
                                                <ItemStyle ForeColor="#EA7D33" Width="125px" />
                                            </asp:CommandField>
                                            <asp:TemplateField HeaderText="Conteo">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbConteo" runat="server" Text='<%# Bind("Conteo") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtConteo" runat="server" Text='<%# Bind("Conteo")%>' 
                                                        Width="90px" TabIndex="11" ToolTip="Ejemplo: 8+3+7" AutoPostBack="True" 
                                                        ontextchanged="txtConteo_TextChanged"></asp:TextBox>
                                                </EditItemTemplate>
                                                <HeaderStyle Width="90px" HorizontalAlign="Center" />
                                                <ItemStyle Width="90px" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Exist. Física">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbExistenciaFisica" runat="server" Text='<%# Bind("ExistenciaFisica", "{0:####,###,###,###,###}") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtExistenciaFisica" runat="server" Width="80px" Text='<%# Bind("ExistenciaFisica")%>' ReadOnly="true" ></asp:TextBox>
                                                </EditItemTemplate>
                                                <HeaderStyle Font-Size="Small" HorizontalAlign="Center" Width="80px" />
                                                <ItemStyle HorizontalAlign="Center" Width="80px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Observaciones" Visible="false" AccessibleHeaderText="Observaciones">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbObservaciones" runat="server" Text='<%# Bind("Observaciones") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="90px" HorizontalAlign="Center" />
                                                <ItemStyle Width="90px" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                                        <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                                        <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" Height="35px" />
                                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#FFF1D4" />
                                        <SortedAscendingHeaderStyle BackColor="#B95C30" />
                                        <SortedDescendingCellStyle BackColor="#F1E5CE" />
                                        <SortedDescendingHeaderStyle BackColor="#93451F" />
                                    </asp:GridView>
                        </td>
                    </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>

            </li>
            <li>
                
                <table runat="server" id="tblCierreInventario" visible="false">
                    <tr>
                        <td colspan="2">
                            <asp:LinkButton ID="lkCerrarInventario" runat="server" 
                                onclick="lkCerrarInventario_Click" Visible="False">Revisión Docs Inventario Febrero 2015</asp:LinkButton>
                        </td>
                        <td colspan="2" style="text-align: right">
                            <asp:Label ID="lbErrorInventarioCerrar" runat="server" Font-Bold="True" 
                                ForeColor="Red"></asp:Label>
                            <asp:Label ID="lbInfoInventarioCerrar" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td colspan="2" style="text-align: center">
                            <asp:GridView ID="gridCerrarInventario" runat="server" 
                                AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" 
                                BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" 
                                Width="250px" onrowdatabound="gridCerrarInventario_RowDataBound" 
                                onselectedindexchanged="gridCerrarInventario_SelectedIndexChanged">
                                <Columns>
                                    <asp:TemplateField HeaderText="Tienda">
                                        <ItemTemplate>
                                            <asp:Label ID="lbTienda" runat="server" Text='<%# Bind("Tienda") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtTienda" runat="server" Text='<%# Bind("Tienda") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="125px"/>
                                        <ItemStyle HorizontalAlign="Center" Width="125px"/>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cerrado" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lbCerrado" runat="server" Text='<%# Bind("Cerrado") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtCerrado" runat="server" Text='<%# Bind("Cerrado") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cerrado" >
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbCerrado" runat="server" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="cbCerrado" runat="server" />
                                        </EditItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="125px"/>
                                        <ItemStyle HorizontalAlign="Center" Width="125px"/>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkImprimirCierreInventario" runat="server" 
                                                CausesValidation="False" CommandName="Select" Text="Imprimir" 
                                                ToolTip="Haga clic aquí para imprimir el inventario cerrado" 
                                                onclick="lkImprimirCierreInventario_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkInventarioExistencias" runat="server" 
                                                CausesValidation="False" CommandName="Select" Text="Existencias" 
                                                ToolTip="Haga clic aquí para actulizar las existencias de este cierre al mes seleccionado" 
                                                onclick="lkInventarioExistencias_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkInventarioPreliminar" runat="server" 
                                                CausesValidation="False" CommandName="Select" Text="Preliminar" 
                                                ToolTip="Haga clic aquí para enviarle a usted el inventario preliminar" 
                                                onclick="lkInventarioPreliminar_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#FFF1D4" />
                                <SortedAscendingHeaderStyle BackColor="#B95C30" />
                                <SortedDescendingCellStyle BackColor="#F1E5CE" />
                                <SortedDescendingHeaderStyle BackColor="#93451F" />
                            </asp:GridView>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td colspan="2" style="text-align: center">
                            <asp:LinkButton ID="lkGrabarInventarioCierre" runat="server" 
                                
                                ToolTip="Haga clic aquí para grabar el cierre de inventario de las tiendas marcadas" 
                                onclick="lkGrabarInventarioCierre_Click" TabIndex="415">Grabar</asp:LinkButton>
                        </td>
                        <td style="text-align: right">
                            <asp:DropDownList ID="cbMesCerrarInventario" runat="server" TabIndex="420" 
                                Width="100px">
                                <asp:ListItem Value="1">Enero</asp:ListItem>
                                <asp:ListItem Value="2">Febrero</asp:ListItem>
                                <asp:ListItem Value="3">Marzo</asp:ListItem>
                                <asp:ListItem Value="4">Abril</asp:ListItem>
                                <asp:ListItem Value="5">Mayo</asp:ListItem>
                                <asp:ListItem Value="6">Junio</asp:ListItem>
                                <asp:ListItem Value="7">Julio</asp:ListItem>
                                <asp:ListItem Value="8">Agosto</asp:ListItem>
                                <asp:ListItem Value="9">Septiembre</asp:ListItem>
                                <asp:ListItem Value="10">Octubre</asp:ListItem>
                                <asp:ListItem Value="11">Noviembre</asp:ListItem>
                                <asp:ListItem Value="12">Diciembre</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtAnioCerrarInventario" runat="server" TabIndex="425" 
                                Width="50px"></asp:TextBox>
                            <asp:LinkButton ID="lkCargarInventarioCerrar" runat="server" 
                                onclick="lkCargarInventarioCerrar_Click" TabIndex="430" 
                                ToolTip="Haga clic aquí para cargar los inventarios del mes y año seleccionados">Cargar inventarios</asp:LinkButton>
                            </td>
                    </tr>
                    </table>
                
            </li>

            <li>
                <table align="center">
                    <tr>
                        <td colspan="7">
                            <a>Utilice esta sección para buscar existencias de artículos en todas las tiendas</a></td></tr>
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            Artículo:</td>
                        <td>
                        <asp:TextBox ID="codigoArticuloDet" runat="server" TabIndex="290" 
                            AutoPostBack="True" ontextchanged="codigoArticuloDet_TextChanged" style="text-transform: uppercase;" 
                                ToolTip="Ingrese el código o parte del código del artículo que desea buscar, o bien puede ingresar alguna palabra clave a buscar en la descripción de los artículos y presione ENTER." 
                                Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:TextBox ID="descripcionArticuloDet" runat="server" TabIndex="300" 
                                MaxLength="100" AutoPostBack="True" 
                                ontextchanged="descripcionArticuloDet_TextChanged" style="text-transform: uppercase;" 
                                ToolTip="Ingrese aquí el nombre o parte del nombre del artículo que desea buscar y presione ENTER" 
                                Visible="False" Width="10px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:LinkButton ID="lkInventarioGeneral" runat="server" 
                                onclick="lkInventarioGeneral_Click" 
                                ToolTip="Haga clic aquí para generar el inventario general en base al criterio ingresado">Generar inventario general</asp:LinkButton>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align: center" colspan="7">
                            <asp:Label ID="lbInfoInventarioGeneral" runat="server" Visible="False"></asp:Label>
                            <asp:Label ID="lbErrorInventarioGeneral" runat="server" Font-Bold="True" ForeColor="Red" Visible="False"></asp:Label>
                            &nbsp;&nbsp;
                            <asp:LinkButton ID="lkOcultarInventarioGeneral" runat="server" 
                                onclick="lkOcultarInventarioGeneral_Click" 
                                ToolTip="Haga clic aquí para ocultar el inventario general" 
                                Visible="False">Ocultar</asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </li>
            <li>
                <asp:GridView ID="gridInventarioGeneral" runat="server" AutoGenerateColumns="False" 
                    BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="3" CellSpacing="2" 
                    onrowdatabound="gridInventarioGeneral_RowDataBound" Visible="False" 
                    onselectedindexchanged="gridInventarioGeneral_SelectedIndexChanged">
                    <Columns>
                        <asp:TemplateField HeaderText="Artículo">
                            <ItemTemplate>
                                <asp:LinkButton ID="lkArticuloInv" runat="server" 
                                    Text='<%# Bind("Articulo") %>' CausesValidation="true" CommandName="Select"
                                    ToolTip="Haga clic aquí para ver las existencias de este artículo en todas las tiendas"></asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Articulo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle Width="140px" />
                            <ItemStyle Font-Size="X-Small" Width="140px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripción" >
                        <HeaderStyle Width="360px" />
                        <ItemStyle Font-Size="XX-Small" Width="360px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Localizacion" HeaderText="Locali." >
                        <HeaderStyle Font-Size="Small" />
                        <ItemStyle Font-Size="XX-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="B05" HeaderText="B05" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="B19" HeaderText="B19" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F01" HeaderText="F01" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F02" HeaderText="F02" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F03" HeaderText="F03" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F04" HeaderText="F04" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F05" HeaderText="F05" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F06" HeaderText="F06" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F07" HeaderText="F07" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F08" HeaderText="F08" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F09" HeaderText="F09" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F10" HeaderText="F10" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F11" HeaderText="F11" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F12" HeaderText="F12" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F13" HeaderText="F13" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F14" HeaderText="F14" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F15" HeaderText="F15" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F16" HeaderText="F16" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F17" HeaderText="F17" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F18" HeaderText="F18" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F19" HeaderText="F19" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F20" HeaderText="F20" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F21" HeaderText="F21" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F22" HeaderText="F22" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F23" HeaderText="F23" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F24" HeaderText="F24" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F25" HeaderText="F25" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F26" HeaderText="F26" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F27" HeaderText="F27" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F28" HeaderText="F28" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F29" HeaderText="F29" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F30" HeaderText="F30" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F31" HeaderText="F31" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F32" HeaderText="F32" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F33" HeaderText="F33" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F34" HeaderText="F34" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F35" HeaderText="F35" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F36" HeaderText="F36" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F37" HeaderText="F37" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F38" HeaderText="F38" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F39" HeaderText="F39" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F40" HeaderText="F40" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F41" HeaderText="F41" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F42" HeaderText="F42" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F43" HeaderText="F43" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F44" HeaderText="F44" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F45" HeaderText="F45" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F46" HeaderText="F46" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F47" HeaderText="F47" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F48" HeaderText="F48" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F49" HeaderText="F49" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F50" HeaderText="F50" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F51" HeaderText="F51" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F52" HeaderText="F52" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F53" HeaderText="F53" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F54" HeaderText="F54" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F55" HeaderText="F55" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F56" HeaderText="F56" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F57" HeaderText="F57" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F58" HeaderText="F58" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F59" HeaderText="F59" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="F60" HeaderText="F60" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
            </li>
            <li>
                <table align="center">
                    <tr>
                        <td>
                            <asp:LinkButton ID="lkArticulosReemplazo" runat="server" 
                                
                                ToolTip="Haciendo clic aquí podrá ver los artículos que reemplaza el sistema automáticamente al realizar despachos" 
                                onclick="lkArticulosReemplazo_Click">Haga clic aquí para listar lor artículos que el sistema reemplaza automáticamente en los despachos</asp:LinkButton>
                        </td>
                        <td style="text-align: right">
                            <asp:LinkButton ID="lkOcultarArticulosReemplazo" runat="server" 
                                onclick="lkOcultarArticulosReemplazo_Click" 
                                ToolTip="Haga clic aquí para ocultar el listado de los artículos de reemplazo">Ocultar</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbInfoArticulosReemplazo" runat="server" Visible="False"></asp:Label>
                            <asp:Label ID="lbErrorArticulosReemplazo" runat="server" Font-Bold="True" ForeColor="Red" 
                                Visible="False"></asp:Label>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </li>
            <li>
                <asp:GridView ID="gridArticulosReemplazo" runat="server" visible="False"
                    AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" 
                    BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" 
                    onrowdatabound="gridArticulosReemplazo_RowDataBound" 
                    onselectedindexchanged="gridArticulosReemplazo_SelectedIndexChanged">
                    <Columns>
                        <asp:TemplateField HeaderText="Artículo">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Articulo1") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lkArticulo1" runat="server" Text='<%# Bind("Articulo1") %>' 
                                    onclick="lkArticulo1_Click" CausesValidation="true"
                                    ToolTip="Haga clic aquí para ver las existencias de este artículo" 
                                    CommandName="Select"></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle Width="80px" />
                            <ItemStyle Width="80px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Descripcion1" HeaderText="Este es el nuevo artículo que reemplaza" >
                            <ItemStyle Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Artículo">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Articulo2") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lkArticulo2" runat="server" Text='<%# Bind("Articulo2") %>' 
                                    onclick="lkArticulo2_Click" CausesValidation="true"
                                    ToolTip="Haga clic aquí para ver las existencias de este artículo" 
                                    CommandName="Select"></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle Width="80px" />
                            <ItemStyle Width="80px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Descripcion2" HeaderText="Este es el artículo que se reemplazará" >
                            <ItemStyle Font-Size="X-Small" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
            </li>
            <li runat="server" visible="true">
                
                <table class="style1" >
                    <tr>
                        <td colspan="4">
                            <a>Búsqueda de COMBOS o SETS</a></td>
                        <td colspan="4" style="text-align: right">
                            <asp:Label ID="lbErrorKits" runat="server" Font-Bold="True" ForeColor="Red" 
                                Visible="False"></asp:Label>
                            <asp:Label ID="lbInfoKits" runat="server" Visible="False"></asp:Label>
                            </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            Artículo:</td>
                        <td>
                        <asp:TextBox ID="codigoArticuloKit" runat="server" TabIndex="290" 
                            AutoPostBack="True" ontextchanged="codigoArticuloKit_TextChanged" style="text-transform: uppercase;"                            
                                ToolTip="Ingrese el código o parte del código del artículo que desea buscar, o bien puede ingresar alguna palabra clave a buscar en la descripción de los artículos y presione ENTER." 
                                Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:LinkButton ID="lkBuscarKit" runat="server" TabIndex="292" 
                                
                                ToolTip="Haga clic aquí para buscar los Combos o SETs del criterio de búsqueda ingresado" 
                                onclick="lkBuscarKit_Click">Buscar</asp:LinkButton>
                                &nbsp;&nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="lkListarTodosKits" runat="server" 
                                onclick="lkListarTodosKits_Click" 
                                ToolTip="Haga clic aquí para listar todos los Combos o SETs que existen en el sistema" TabIndex="294">Listar todos los Combos o SETs</asp:LinkButton>
                        </td>
                        <td>
                            </td>
                        <td>
                            </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                    </table>
            </li>
            <li>
                <asp:GridView ID="gridCombos"  runat="server" visible="False"
                    AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" 
                    BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" 
                    onrowdatabound="gridCombos_RowDataBound" 
                    onselectedindexchanged="gridCombos_SelectedIndexChanged" >
                    <Columns>
                        <asp:TemplateField HeaderText="Artículo">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Articulo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lkArticuloKit" runat="server" Text='<%# Bind("Articulo") %>' CausesValidation="true"
                                    ToolTip="Haga clic aquí para ver las existencias de este artículo, se mostrarán en la parte superior de la página, en la sección del inventario general" 
                                    CommandName="Select"></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle Width="100px" />
                            <ItemStyle Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nombre del Artículo">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cantidad">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Cantidad") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("Cantidad") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tipo" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTipo" runat="server" Text='<%# Bind("Tipo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbTipo" runat="server" Text='<%# Bind("Tipo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
            </li>
            <li runat="server" id="liReImprimir">
                <table align="center">
                    <tr>
                        <td style="text-align: left">
                            <a>En esta sección podrá re-imprimir facturas y/o despachos</a></td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
                            Factura re-imprimir:
                            <asp:TextBox ID="txtFacturaReImprimir" runat="server" 
                                style="text-transform: uppercase;" 
                                ToolTip="Ingrese aquí el número de factura con serie y número completo separado con un guíon incluyendo los ceros a la izquierda, por ejemplo: F09C-005946" 
                                TabIndex="307" Width="170px"></asp:TextBox>&nbsp;
                            <asp:LinkButton ID="lkReImprimirFactura" runat="server" ToolTip="Haga clic aquí para re-imprimir la factura ingresada"
                                onclick="lkReImprimirFactura_Click" TabIndex="308">Imprimir factura</asp:LinkButton>&nbsp;&nbsp;
                            <asp:LinkButton ID="lkReImprimirGarantia" runat="server" ToolTip="Haga clic aquí para re-imprimir la garantía de la factura ingresada"
                                onclick="lkReImprimirGarantia_Click" TabIndex="309">Imprimir garantía</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
                            Despacho re-imprimir:
                            <asp:TextBox ID="txtDespachoReImprimir" runat="server" 
                                style="text-transform: uppercase;" 
                                ToolTip="Ingrese aquí el número de despacho a re-imprimir por ejemplo F09-0000346" 
                                TabIndex="310" Width="100px"></asp:TextBox>&nbsp;
                            <asp:LinkButton ID="lkReImprimirDespacho" runat="server" 
                                onclick="lkReImprimirDespacho_Click" TabIndex="310">Imprimir despacho</asp:LinkButton>&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
                            <asp:Label ID="lbErrorReImprimir" runat="server" Font-Bold="True" ForeColor="Red" Visible="False"></asp:Label>
                        </td>
                    </tr>
                </table>            
            </li>
            <li>
                
                <table align="center">
                    <tr>
                        <td colspan="2">
                            <a>Anulación de Facturas</a>
                        </td>
                        <td colspan="3" style="text-align: right">
                            <asp:Label ID="lbErrorAnularFactura" runat="server" Font-Bold="True" ForeColor="Red" 
                                Visible="False"></asp:Label>
                            <asp:Label ID="lbInfoAnularFactura" runat="server" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                        </td>
                        <td>
                            Documento:
                        </td>
                        <td colspan="2">
                            <asp:FileUpload ID="AdjuntoFactura" runat="server" ToolTip="Haga clic aquí para buscar el archivo para adjuntar a la anulación de factura" TabIndex="902" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Factura:</td>
                        <td>
                            <asp:TextBox ID="txtFacturaAnular" runat="server" 
                                style="text-transform: uppercase;" 
                                
                                ToolTip="Ingrese aquí el número de factura con serie y número completo separado con un guíon incluyendo los ceros a la izquierda, por ejemplo: F09C-005946" 
                                TabIndex="317" Width="170px"></asp:TextBox>
                        </td>
                        <td>
                            Observaciones:</td>
                        <td>
                            <asp:TextBox ID="txtFacturaAnularObservaciones" runat="server" 
                                style="text-transform: uppercase;" 
                                
                                ToolTip="Ingrese aquí la razón por la que está anulando la factura" 
                                TabIndex="318" Width="470px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:LinkButton ID="lkAnularFactura" runat="server" TabIndex="319" 
                                ToolTip="Haga clic aquí para anular la factura" onclientclick="return confirm('Desea anular la factura?');"
                                onclick="lkAnularFactura_Click">Anular Factura</asp:LinkButton>
                        </td>
                    </tr>
                </table>
                
            </li>

            <li>
                
                <table align="center" class="style1">
                    <tr>
                        <td colspan="3">
                            <a>Asignación de transportista a envío</a></td>
                        <td colspan="3" style="text-align: right">
                            <asp:Label ID="lbErrorAsignarEnvio" runat="server" Font-Bold="True" 
                                ForeColor="Red"></asp:Label>
                            <asp:Label ID="lbInfoAsignarEnvio" runat="server"></asp:Label>
                            </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            No. de Envío:</td>
                        <td>
                            <asp:TextBox ID="txtEnvioAsignar" runat="server" 
                                style="text-transform: uppercase;" 
                                ToolTip="Ingrese aquí el número de envío para asignarle transportista" 
                                TabIndex="322" Width="100px"></asp:TextBox>
                        </td>
                        <td>
                            Transportista:</td>
                        <td>
                        <asp:DropDownList ID="cbTransportista" runat="server" Width="200px" 
                            TabIndex="324">
                        </asp:DropDownList>
                        &nbsp;
                        <asp:LinkButton ID="lkAsignarTransportista" runat="server" 
                                ToolTip="Haga clic aquí para asignar el transportista al número de envío ingresado" 
                                onclick="lkAsignarTransportista_Click" TabIndex="326">Asignar transportista</asp:LinkButton>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                </table>
                
            </li>

            <%--<li visible="false">
                <table  align="center" visible="false">
                    <tr>
                        <td class="style1000">
                            <asp:LinkButton ID="lkConsultarSolicitudes" runat="server" 
                                
                                ToolTip="Haga clic aquí para consultar las solicitudes de autorización de precio especial que haya realizado" 
                                onclick="lkConsultarSolicitudes_Click">Haga clic aquí para consultar solicitudes de autorización</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
                            <asp:Label ID="lbInfoSolicitudes" runat="server" Visible="False"></asp:Label>
                            <asp:Label ID="lbErrorSolicitudes" runat="server" Font-Bold="True" ForeColor="Red" Visible="False"></asp:Label>
                            &nbsp;&nbsp;
                            <asp:LinkButton ID="lkOcultarSolicitudes" runat="server" 
                                onclick="lkOcultarSolicitudes_Click" 
                                ToolTip="Haga clic aquí para ocultar las solicitudes de autorización de precio especial" 
                                Visible="False">Ocultar</asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </li>--%>
            <li>
                <asp:GridView ID="gridSolicitudes" runat="server" AutoGenerateColumns="False" 
                    BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="3" CellSpacing="2" onrowdatabound="gridSolicitudes_RowDataBound" 
                    Visible="False" 
                    onselectedindexchanged="gridSolicitudes_SelectedIndexChanged">
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lkRetractarse" runat="server" CausesValidation="False" 
                                    CommandName="Select" onclick="lkRetractarse_Click" Text="Retractarse" onclientclick="return confirm('Desea retractarse de esta solicitud?');"
                                    ToolTip="Haga clic aquí para retractarse de esta solicitud"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Font-Size="Small" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lkAplicar" runat="server" CausesValidation="False" 
                                    CommandName="Select" Text="Aplicar" 
                                    ToolTip="Haga clic aquí para aplicar esta autorización en un pedido" 
                                    onclick="lkAplicar_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Autorizacion" HeaderText="No." >
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" Visible="false" />
                        <asp:BoundField DataField="EstatusDescripcion" HeaderText="Estatus" >
                        <ItemStyle Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Fecha">
                            <ItemTemplate>
                                <asp:Label ID="lbFecha" runat="server" Text='<%# Bind("Fecha", "{0:d}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFecha" runat="server" Text='<%# Bind("Fecha", "{0:d}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Cliente" HeaderText="Cliente" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" Visible="false"/>
                        <asp:TemplateField HeaderText="Nombre" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbClienteNombre" runat="server" Text='<%# Bind("ClienteNombre") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtClienteNombre" runat="server" Text='<%# Bind("ClienteNombre") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemStyle Font-Size="X-Small" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Articulo" HeaderText="Artículo" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Nombre del Artículo" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbArticuloNombre" runat="server" Text='<%# Bind("ArticuloNombre") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticuloNombre" runat="server" Text='<%# Bind("ArticuloNombre") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="TipoVenta" HeaderText="TipoVenta" Visible="false" />
                        <asp:BoundField DataField="Financiera" HeaderText="Financiera" Visible="false" />
                        <asp:BoundField DataField="NivelPrecio" HeaderText="NivelPrecio" Visible="false" />
                        <asp:BoundField DataField="TipoVentaNombre" HeaderText="Promoción" Visible="false" />
                        <asp:BoundField DataField="FinancieraNombre" HeaderText="Financiera" Visible="false" />
                        <asp:BoundField DataField="Precio" HeaderText="Precio" Visible="false" />
                        <asp:BoundField DataField="PrecioLista" HeaderText="Precio Lista" Visible="false" />
                        <asp:TemplateField HeaderText="Solicitado" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecioSolicitado" runat="server" 
                                    Text='<%# Bind("PrecioSolicitado", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecioSolicitado" runat="server" 
                                    Text='<%# Bind("PrecioSolicitado", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" Visible="false" />
                        <asp:BoundField DataField="Diferencia" HeaderText="Diferencia" Visible="false" />
                        <asp:TemplateField HeaderText="Observaciones" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbObservaciones" runat="server" Text='<%# Bind("Observaciones") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtObservaciones" runat="server" Text='<%# Bind("Observaciones") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Tienda" HeaderText="Tienda" Visible="false" />
                        <asp:BoundField DataField="Vendedor" HeaderText="Vendedor" Visible="false" />
                        <asp:BoundField DataField="VendedorNombre" HeaderText="VendedorNombre" Visible="false" />
                        <asp:BoundField DataField="Gerente" HeaderText="Usuario" >
                        <ItemStyle Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Comentario" HeaderText="Comentario" >
                        <HeaderStyle Width="160px" />
                        <ItemStyle Font-Size="X-Small" Width="160px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Autorizado">
                            <ItemTemplate>
                                <asp:Label ID="lbPrecioAutorizado" runat="server" Text='<%# Bind("PrecioAutorizado", "{0:####,###,###,###,##.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPrecioAutorizado" runat="server" 
                                    Text='<%# Bind("PrecioAutorizado", "{0:####,###,###,###,##.00}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" Font-Size="X-Small" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="PrecioAutorizadoNivel" HeaderText="PrecioAutorizadoNivel" Visible="false" />
                        <asp:TemplateField HeaderText="Vence">
                            <ItemTemplate>
                                <asp:Label ID="lbFechaVence" runat="server" Text='<%# Bind("FechaVence", "{0:d}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFechaVence" runat="server" Text='<%# Bind("FechaVence", "{0:d}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Estado" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbEstado" runat="server" Text='<%# Bind("Estado") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEstado" runat="server" Text='<%# Bind("Estado") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="EstadoDescripcion" HeaderText="Estado" >
                        <ItemStyle Font-Size="X-Small" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
            </li>
            <li>
                
                <table align="center">
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td colspan="5">
                            <a>Calculo de artículos en liquidación</a></td>
                        <td colspan="5" style="text-align: right">
                            <asp:Label ID="lbErrorArticulosLiquidacion" runat="server" Font-Bold="True" ForeColor="Red" 
                                Visible="False"></asp:Label>
                            <asp:Label ID="lbInfoArticulosLiquidacion" runat="server" Visible="False"></asp:Label>
                            </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            Set:</td>
                        <td>
                            <asp:TextBox ID="txtSetLiquidacion" runat="server" TabIndex="703" Width="105px"></asp:TextBox>
                        </td>
                        <td>
                            Porcentaje:</td>
                        <td>
                            <asp:TextBox ID="txtPorcentajeLiquidacion" runat="server" Width="60px" 
                                TabIndex="704"></asp:TextBox>
                        </td>
                        <td>
                            Colchón:</td>
                        <td>
                            <asp:TextBox ID="txtColchonLiquidacion" runat="server" TabIndex="705" 
                                Width="105px"></asp:TextBox>
                        </td>
                        <td>
                            Base:</td>
                        <td>
                            <asp:TextBox ID="txtCantidadBaseLiquidacion" runat="server" TabIndex="706" 
                                ToolTip="Ingrese aquí la cantidad de bases que desea manejar" Width="30px">1</asp:TextBox>
                            <asp:TextBox ID="txtBaseLiquidacion" runat="server" TabIndex="707" 
                                Width="105px"></asp:TextBox>
                            <asp:LinkButton ID="lkArticulosLiquidacion" runat="server" 
                                onclick="lkArticulosLiquidacion_Click" TabIndex="708" 
                                ToolTip="Haga clic aquí para calcular el descuento que debe solicitar">Calcular</asp:LinkButton>
                        </td>
                        <td>
                            Desc. a solicitar:</td>
                        <td>
                            <asp:TextBox ID="txtDescuentoLiquidacion" runat="server" BackColor="AliceBlue" 
                                TabIndex="709" 
                                ToolTip="Este es el descuento que le debe solicitar al código del colchón" 
                                Width="80px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    </table>
                
            </li>
            <li>
                
                <table align="center">
                    <tr>
                        <td colspan="10">
                            <asp:LinkButton ID="lkPendienteDespachar" runat="server" 
                                ToolTip="Haga clic aquí para listar su mercadería pendiente de despacho, con esta opción también podrá cambiar la bodega asignada" 
                                onclick="lkPendienteDespachar_Click">Haga clic aquí para listar mercadería pendiente de despacho</asp:LinkButton>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                            <asp:LinkButton ID="lkPendienteDespacharTodos" runat="server" 
                                ToolTip="Haga clic aquí para listar TODA su mercadería pendiente de despacho, con esta opción también podrá cambiar la bodega asignada" 
                                onclick="lkPendienteDespacharTodos_Click">Ver todos</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            Artículo:</td>
                        <td>
                            <asp:TextBox ID="txtArticuloPendienteDespachar" runat="server" 
                                BackColor="AliceBlue" ReadOnly="True" 
                                Width="95px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtNombreArticuloPendienteDespachar" runat="server" BackColor="AliceBlue" 
                                ReadOnly="True" Width="362px"></asp:TextBox>
                        </td>
                        <td>
                            Bodega:</td>
                        <td>
                            <asp:DropDownList ID="cbBodegaPendienteDespachar" runat="server" Width="55px" TabIndex="26" />
                        </td>
                        <td>
                            <asp:LinkButton ID="lbLocalizacion" runat="server"                     
                            ToolTip="Haga clic aquí para ver las existencias del artículo por bodega y localización, para ocultar las localizaciones haga clic aquí de nuevo." 
                            onclick="lbLocalizacion_Click" TabIndex="29">Localización:</asp:LinkButton>
                        </td>
                        <td>
                        <asp:DropDownList ID="cbLocalizacionPendienteDespachar" runat="server" Width="98px" TabIndex="30" 
                            ToolTip="Seleccione la localización que desea cambiar para el artículo">
                            <asp:ListItem Value="ARMADO">ARMADO</asp:ListItem>
                            <asp:ListItem Value="CAJA">CAJA</asp:ListItem>
                            <asp:ListItem Value="DESARMDO">DESARMDO</asp:ListItem>
                        </asp:DropDownList>
                        </td>
                        <td>
                            <asp:LinkButton ID="lkCambiarBodega" runat="server" 
                                ToolTip="Haga clic aquí para cambiar la bodega asiganda al artículo seleccionado" 
                                onclick="lkCambiarBodega_Click" 
                                onclientclick="return confirm('Confirma que desea cambiar los datos de despacho?');" 
                                TabIndex="32">Cambiar</asp:LinkButton>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align: center" colspan="10">
                            <asp:Label ID="lbInfoPendienteDespachar" runat="server" Visible="False"></asp:Label>
                            <asp:Label ID="lbErrorPendienteDespachar" runat="server" Font-Bold="True" 
                                ForeColor="Red" Visible="False"></asp:Label>
                            &nbsp;&nbsp;
                            <asp:LinkButton ID="lkOcultarPendienteDespachar" runat="server" 
                                ToolTip="Haga clic aquí para ocultar su mercadería pendiente de despachar" 
                                onclick="lkOcultarPendienteDespachar_Click" Visible="False">Ocultar</asp:LinkButton>
                        </td>
                    </tr>
                </table>   
            </li>
            <li>
                <asp:GridView ID="gridLocalizaciones" runat="server" 
                    AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" 
                    BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" 
                    onrowdatabound="gridLocalizaciones_RowDataBound" 
                    onselectedindexchanged="gridLocalizaciones_SelectedIndexChanged" 
                    Visible="False" TabIndex="66">
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lkSeleccionarLocalizacion" runat="server" CausesValidation="False" 
                                    CommandName="Select" Text="Seleccionar" 
                                    ToolTip="Haga clic aquí para seleccionar esta localización para el artículo"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Articulo" HeaderText="Artículo" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre del Artículo" 
                            Visible="False" />
                        <asp:BoundField DataField="Bodega" HeaderText="Bodega" />
                        <asp:BoundField DataField="Localizacion" HeaderText="Localización" />
                        <asp:TemplateField HeaderText="Disponible Almacén">
                            <ItemTemplate>
                                <asp:Label ID="lbDisponibleAlmacen" runat="server" Text='<%# Bind("DisponibleAlmacen", "{0:####,###,###,###,##0}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDisponibleAlmacen" runat="server" Text='<%# Bind("DisponibleAlmacen", "{0:####,###,###,###,##0}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reservada Almacén">
                            <ItemTemplate>
                                <asp:Label ID="lbReservadaAlmacen" runat="server" Text='<%# Bind("ReservadaAlmacen", "{0:####,###,###,###,##0}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtReservadaAlmacen" runat="server" Text='<%# Bind("ReservadaAlmacen", "{0:####,###,###,###,##0}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remitida Almacén">
                            <ItemTemplate>
                                <asp:Label ID="lbRemitidaAlmacen" runat="server" Text='<%# Bind("RemitidaAlmacen", "{0:####,###,###,###,##0}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtRemitidaAlmacen" runat="server" Text='<%# Bind("RemitidaAlmacen", "{0:####,###,###,###,##0}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Almacén">
                            <ItemTemplate>
                                <asp:Label ID="lbTotalAlmacen" runat="server" Text='<%# Bind("TotalAlmacen", "{0:####,###,###,###,##0}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTotalAlmacen" runat="server" Text='<%# Bind("TotalAlmacen", "{0:####,###,###,###,##0}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
            </li>
            <li>
                <asp:GridView ID="gridArticulosPendienteDespachar" runat="server" 
                    CellPadding="3" TabIndex="280" 
                        AutoGenerateColumns="False" style="text-align: left" 
                    BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                    CellSpacing="2" 
                    onrowdatabound="gridArticulosPendienteDespachar_RowDataBound" 
                    onselectedindexchanged="gridArticulosPendienteDespachar_SelectedIndexChanged" 
                    Visible="False">
                        <Columns>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lkSeleccionarPendienteDespachar" runat="server" CausesValidation="False" 
                                        CommandName="Select" Text="Seleccionar" 
                                        ToolTip="Haga clic aquí para seleccionar este artículo y cambiar su localización" 
                                        onclick="lkSeleccionarPendienteDespachar_Click" Font-Size="Small"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Factura" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="lbEstado" runat="server" Text='<%# Bind("Estado") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtEstado" runat="server" Text='<%# Bind("Estado") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Artículo">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lkArticulo" runat="server" Text='<%# Bind("Articulo") %>' CausesValidation="true"
                                    ToolTip="Haga clic aquí para ver las existencias de este artículo, se mostrarán en la parte superior de la página, en la sección del inventario general" 
                                    CommandName="Select" onclick="lkArticuloPendienteDespachar_Click"></asp:LinkButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtArticulo" runat="server" Text='<%# Bind("Articulo") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <HeaderStyle Width="80px"></HeaderStyle>
                                <ItemStyle Width="80px"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lkMPendienteDespachar" runat="server" CausesValidation="False" 
                                        CommandName="Select" Text="M"
                                        ToolTip="Haga clic aquí para cambiar el código de Fiesta a 'M' o viceversa" 
                                        onclick="lkMPendienteDespachar_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lkCPendienteDespachar" runat="server" CausesValidation="False" 
                                        CommandName="Select" Text="C" 
                                        ToolTip="Haga clic aquí para cambiar el color del artículo" 
                                        onclick="lkCPendienteDespachar_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lkRPendienteDespachar" runat="server" CausesValidation="False" 
                                        CommandName="Select" Text="R" 
                                        ToolTip="Haga clic aquí para cambiar el regalo dentro de los disponibles para cada código según lo establecido en el sistema" 
                                        onclick="lkRPendienteDespachar_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lkSPendienteDespachar" runat="server" CausesValidation="False" 
                                        CommandName="Select" Text="S" 
                                        ToolTip="Haga clic aquí para sacar este código del COMBO o SET con el fin de poderlo despachar posteriormente" 
                                        onclick="lkSPendienteDespachar_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lkDPendienteDespachar" runat="server" CausesValidation="False" 
                                        CommandName="Select" Text="D" 
                                        ToolTip="Haga clic aquí para desmembrar en unidades la cantidad de este artículo" 
                                        onclick="lkDPendienteDespachar_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nombre del Artículo" Visible="true">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtArticuloNombre" runat="server" Text='<%# Bind("Nombre") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbArticuloNombre" runat="server" Text='<%# Bind("Nombre") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="235px" />
                                <ItemStyle Font-Size="X-Small" Width="235px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Precio" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lbPrecio" runat="server" Text='<%# Bind("PrecioUnitario", "{0:####,###,###,###,###.00}") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtPrecio" runat="server" Text='<%# Bind("PrecioUnitario", "{0:####,###,###,###,###.00}") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <HeaderStyle HorizontalAlign="Right" />
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cant">
                                <ItemTemplate>
                                    <asp:Label ID="lbCantidad" runat="server" Text='<%# Bind("CantidadPedida", "{0:####,###,###,###,##0}") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtCantidad" runat="server" Text='<%# Bind("CantidadPedida", "{0:####,###,###,###,##0}") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total">
                                <ItemTemplate>
                                    <asp:Label ID="lbTotal" runat="server" Text='<%# Bind("PrecioTotal", "{0:####,###,###,###,###.00}") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtTotal" runat="server" Text='<%# Bind("PrecioTotal", "{0:####,###,###,###,###.00}") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <HeaderStyle HorizontalAlign="Right" />
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Bodega" Visible="true">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtBodega" runat="server" Text='<%# Bind("Bodega") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbBodega" runat="server" Text='<%# Bind("Bodega") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Localización" Visible="true">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtLocalizacion" runat="server" Text='<%# Bind("Localizacion") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbLocalizacion" runat="server" Text='<%# Bind("Localizacion") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="RequisicionArmado" HeaderText="Armado?" Visible="false"/>
                            <asp:TemplateField HeaderText="Cliente" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="lbDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comentario" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lbComentario" runat="server" Text='<%# Bind("Comentario") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtComentario" runat="server" Text='<%# Bind("Comentario") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pedido" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="lbNumeroPedido" runat="server" Text='<%# Bind("NumeroPedido") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtNumeroPedido" runat="server" Text='<%# Bind("NumeroPedido") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Oferta" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lbOferta" runat="server" Text='<%# Bind("Oferta") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtOferta" runat="server" Text='<%# Bind("Oferta") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="FechaOfertaDesde" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lbFechaOfertaDesde" runat="server" Text='<%# Bind("FechaOfertaDesde") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtFechaOfertaDesde" runat="server" Text='<%# Bind("FechaOfertaDesde") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PrecioOriginal" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lbPrecioOriginal" runat="server" Text='<%# Bind("PrecioOriginal") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtPrecioOriginal" runat="server" Text='<%# Bind("PrecioOriginal") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="EsDetalleKit" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lbEsDetalleKit" runat="server" Text='<%# Bind("EsDetalleKit") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtEsDetalleKit" runat="server" Text='<%# Bind("EsDetalleKit") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PrecioFacturar" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lbPrecioFacturar" runat="server" Text='<%# Bind("PrecioFacturar") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtPrecioFacturar" runat="server" Text='<%# Bind("PrecioFacturar") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TipoOferta" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lbTipoOferta" runat="server" Text='<%# Bind("TipoOferta") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtTipoOferta" runat="server" Text='<%# Bind("TipoOferta") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vale" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lbVale" runat="server" Text='<%# Bind("Vale") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtVale" runat="server" Text='<%# Bind("Vale") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Autorizacion" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lbAutorizacion" runat="server" Text='<%# Bind("Autorizacion") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtAutorizacion" runat="server" Text='<%# Bind("Autorizacion") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Condicional" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lbCondicional" runat="server" Text='<%# Bind("Condicional") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtCondicional" runat="server" Text='<%# Bind("Condicional") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ArticuloCondicion" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lbArticuloCondicion" runat="server" Text='<%# Bind("ArticuloCondicion") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtArticuloCondicion" runat="server" Text='<%# Bind("ArticuloCondicion") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                        <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                        <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                        <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                        <SelectedRowStyle BackColor="#F8AA55" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#FFF1D4" />
                        <SortedAscendingHeaderStyle BackColor="#B95C30" />
                        <SortedDescendingCellStyle BackColor="#F1E5CE" />
                        <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>                
            </li>
            <li>
                
                <table align="center" class="style1">
                    <tr>
                        <td colspan="5"><a>Kardex</a></td>
                        <td colspan="7" style="text-align: right">
                            <asp:Label ID="lbErrorKardex" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                            <asp:Label ID="lbInfoKardex" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            Fecha inicial:
                        </td>
                        <td>
                <asp:DropDownList ID="cbDiaIni" runat="server" TabIndex="42">
                    <asp:ListItem>--</asp:ListItem>
                    <asp:ListItem Value="1">01</asp:ListItem>
                    <asp:ListItem Value="2">02</asp:ListItem>
                    <asp:ListItem Value="3">03</asp:ListItem>
                    <asp:ListItem Value="4">04</asp:ListItem>
                    <asp:ListItem Value="5">05</asp:ListItem>
                    <asp:ListItem Value="6">06</asp:ListItem>
                    <asp:ListItem Value="7">07</asp:ListItem>
                    <asp:ListItem Value="8">08</asp:ListItem>
                    <asp:ListItem Value="9">09</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="cbMesIni" runat="server" TabIndex="44">
                    <asp:ListItem>--</asp:ListItem>
                    <asp:ListItem Value="1">Ene</asp:ListItem>
                    <asp:ListItem Value="2">Feb</asp:ListItem>
                    <asp:ListItem Value="3">Mar</asp:ListItem>
                    <asp:ListItem Value="4">Abr</asp:ListItem>
                    <asp:ListItem Value="5">May</asp:ListItem>
                    <asp:ListItem Value="6">Jun</asp:ListItem>
                    <asp:ListItem Value="7">Jul</asp:ListItem>
                    <asp:ListItem Value="8">Ago</asp:ListItem>
                    <asp:ListItem Value="9">Sep</asp:ListItem>
                    <asp:ListItem Value="10">Oct</asp:ListItem>
                    <asp:ListItem Value="11">Nov</asp:ListItem>
                    <asp:ListItem Value="12">Dic</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtAnioIni" runat="server" TabIndex="46" 
                    Width="39px">2025</asp:TextBox>
                        </td>
                        <td>
                            Fecha final:</td>
                        <td>
                <asp:DropDownList ID="cbDiaFin" runat="server" TabIndex="48">
                    <asp:ListItem>--</asp:ListItem>
                    <asp:ListItem Value="1">01</asp:ListItem>
                    <asp:ListItem Value="2">02</asp:ListItem>
                    <asp:ListItem Value="3">03</asp:ListItem>
                    <asp:ListItem Value="4">04</asp:ListItem>
                    <asp:ListItem Value="5">05</asp:ListItem>
                    <asp:ListItem Value="6">06</asp:ListItem>
                    <asp:ListItem Value="7">07</asp:ListItem>
                    <asp:ListItem Value="8">08</asp:ListItem>
                    <asp:ListItem Value="9">09</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="cbMesFin" runat="server" TabIndex="50">
                    <asp:ListItem>--</asp:ListItem>
                    <asp:ListItem Value="1">Ene</asp:ListItem>
                    <asp:ListItem Value="2">Feb</asp:ListItem>
                    <asp:ListItem Value="3">Mar</asp:ListItem>
                    <asp:ListItem Value="4">Abr</asp:ListItem>
                    <asp:ListItem Value="5">May</asp:ListItem>
                    <asp:ListItem Value="6">Jun</asp:ListItem>
                    <asp:ListItem Value="7">Jul</asp:ListItem>
                    <asp:ListItem Value="8">Ago</asp:ListItem>
                    <asp:ListItem Value="9">Sep</asp:ListItem>
                    <asp:ListItem Value="10">Oct</asp:ListItem>
                    <asp:ListItem Value="11">Nov</asp:ListItem>
                    <asp:ListItem Value="12">Dic</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtAnioFin" runat="server" TabIndex="52" 
                    Width="39px">2025</asp:TextBox>
                        </td>
                        <td>
                            Bodega:</td>
                        <td>
                            <asp:DropDownList ID="cbBodegaKardex" runat="server" Width="55px" 
                                TabIndex="53">
                            </asp:DropDownList>
                        </td>
                        <td>
                            Artículo:</td>
                        <td>
                            <asp:TextBox ID="txtArticuloKardex" runat="server" 
                                ToolTip="Ingrese aquí el código o parte del código del artículo a buscar.  También puede escribir parte de la descripción." 
                                Width="160px" TabIndex="54" AutoPostBack="True" style="text-transform: uppercase;"
                                ontextchanged="txtArticuloKardex_TextChanged"></asp:TextBox>
                        </td>
                        <td>
                            <asp:LinkButton ID="lkKardex" runat="server" 
                                ToolTip="Haga clic aquí para generar el kardex del artículo seleccionado" 
                                TabIndex="56" onclick="lkKardex_Click">Kardex</asp:LinkButton>
                        </td>
                        <td>
                            <asp:LinkButton ID="lkOcultarKardex" runat="server" 
                                ToolTip="Haga clic aquí para imprimir el Kardex" 
                                TabIndex="58" onclick="lkOcultarKardex_Click">Imprimir</asp:LinkButton>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    </table>
            </li>
            <li>
                <asp:GridView ID="gridArticulosKardex" runat="server" AutoGenerateColumns="False" 
                    BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="3" CellSpacing="2" 
                    onrowdatabound="gridArticulosKardex_RowDataBound" 
                    onselectedindexchanged="gridArticulosKardex_SelectedIndexChanged">
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                                    CommandName="Select" Text="Seleccionar"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Artículo">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulo" runat="server" Text='<%# Bind("Articulo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulo" runat="server" Text='<%# Bind("Articulo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Descripción">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
            </li>
            <li>
                <asp:GridView ID="gridKardex" runat="server" BackColor="#DEBA84" 
                    BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
                    CellSpacing="2" AutoGenerateColumns="False" 
                    onrowdatabound="gridKardex_RowDataBound" 
                    onselectedindexchanged="gridKardex_SelectedIndexChanged">
                    <Columns>
                        <asp:BoundField DataField="Descripcion" HeaderText="Artículo" >
                        <ItemStyle Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Localizacion" HeaderText="Localización" />
                        <asp:TemplateField HeaderText="Fecha">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Fecha", "{0:d}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("Fecha", "{0:d}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Documento" HeaderText="Documento" />
                        <asp:BoundField DataField="Bodega" HeaderText="Bodega">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" />
                        <asp:TemplateField HeaderText="Cantidad">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Cantidad", "{0:####,###,###,###,###}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Cantidad", "{0:####,###,###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Saldo">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Saldo", "{0:####,###,###,###,##0}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("Saldo", "{0:####,###,###,###,##0}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
            </li>

            <li>
                <table align="center">
                    <tr>
                        <td style="text-align: left">
                            <a>Impresión de vales</a></td>
                        <td style="text-align: right">
                            <asp:Label ID="lbErrorValeFactura" runat="server" Font-Bold="True" ForeColor="Red" Visible="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center" colspan="2">
                            Factura:
                            <asp:TextBox ID="txtFacturaVale" runat="server" 
                                style="text-transform: uppercase;" 
                                ToolTip="Ingrese aquí el número de factura con serie y número completo separado con un guíon incluyendo los ceros a la izquierda, por ejemplo: F09C-005946" 
                                TabIndex="311" Width="100px"></asp:TextBox>&nbsp;
                            <asp:LinkButton ID="lkValesFactura" runat="server" ToolTip="Haga clic aquí para re-imprimir los vales de la factura ingresada"
                                onclick="lkValesFactura_Click" TabIndex="312">Imprimir vale</asp:LinkButton>&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>            
            </li>

            <li visible="false">
                <table align="center" runat="server" visible="false" id="tblBuscar">
                    <tr>
                        <td colspan="11">
                            <a>Vales de Q200 de descuento</a>&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td>
                            Código:</td>
                        <td>
                            <asp:TextBox ID="txtCodigoBuscar" runat="server" AutoPostBack="True" 
                                ontextchanged="txtCodigoBuscar_TextChanged" TabIndex="14" 
                            
                                ToolTip="Ingrese aquí el código o parte del código del cliente que desea buscar y luego presione ENTER.  No es necesario digitar los ceros a la izquierda del código." 
                                Width="80px"></asp:TextBox>
                        </td>
                        <td>
                            Nombre:</td>
                        <td>
                            <asp:TextBox ID="txtNombreBuscar" runat="server" Width="300px" AutoPostBack="True" 
                                ontextchanged="txtNombreBuscar_TextChanged" TabIndex="16" style="text-transform: uppercase;" 
                                ToolTip="Ingrese aquí el nombre o parte del nombre del cliente que desea buscar y luego presione ENTER."></asp:TextBox>
                        </td>
                        <td>
                            NIT:</td>
                        <td>
                            <asp:TextBox ID="txtNitBuscar" runat="server" AutoPostBack="True" 
                                ontextchanged="txtNitBuscar_TextChanged" TabIndex="18" style="text-transform: uppercase;" 
                            
                                ToolTip="Ingrese aquí el NIT o parte del NIT que desea buscar y luego presione ENTER." 
                                Width="80px"></asp:TextBox>
                        </td>
                        <td>
                            Tel.:</td>
                        <td>
                            <asp:TextBox ID="txtTelefono" runat="server" AutoPostBack="True" 
                                ontextchanged="txtTelefono_TextChanged" TabIndex="20" Width="90px" 
                            
                            
                                ToolTip="Ingrese aquí el teléfono que desea buscar y luego presione ENTER, se buscará en el teléfono de casa, trabajo y celular."></asp:TextBox>
                        </td>
                        <td>
                            <asp:LinkButton ID="lkOcultarBusqueda" runat="server" 
                                ToolTip="Haga clic aquí para limpiar la búsqueda de clientes." 
                                onclick="lkOcultarBusqueda_Click" TabIndex="22">Limpiar</asp:LinkButton>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="11" style="text-align: center">
                            <asp:Label ID="lbInfoVales" runat="server"></asp:Label>
                            <asp:Label ID="lbErrorVales" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                </table>
            </li>

            <li>
                <asp:GridView ID="gridClientes" runat="server" Visible="False" 
                    AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" 
                    BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" 
                    onselectedindexchanged="gridClientes_SelectedIndexChanged" 
                    onrowdatabound="gridClientes_RowDataBound" TabIndex="14">
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lkSeleccionar" runat="server" CausesValidation="False" 
                                    CommandName="Select" Text="Imprimir vale"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Cliente" HeaderText="Cliente">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" >
                        <ItemStyle Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Nit" HeaderText="Nit" />
                        <asp:BoundField DataField="TelCasa" HeaderText="Tel Casa" />
                        <asp:BoundField DataField="TelTrabajo" HeaderText="Tel Trabajo" />
                        <asp:BoundField DataField="TelCelular" HeaderText="Celular" />
                        <asp:BoundField DataField="Tienda" HeaderText="Tienda" Visible="false">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Vendedor" HeaderText="No. Vale" >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Fecha Vence">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFechaIngreso" runat="server" Text='<%# Bind("FechaIngreso", "{0:d}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbFechaIngreso" runat="server" Text='<%# Bind("FechaIngreso", "{0:d}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
            </li>
            <li>
                
                <table class="style1">
                    <tr>
                        <td colspan="3">
                            <a>Cambiar de Tienda</a></td>
                        <td colspan="3" style="text-align: right">
                            <asp:Label ID="lbInfoCambiarTienda" runat="server"></asp:Label>
                            <asp:Label ID="lbErrorCambiarTienda" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            Seleccione la tienda en la que desea trabajar:</td>
                        <td colspan="2">
                            <asp:DropDownList ID="cbTiendaCambiar" runat="server" Width="70px" 
                                TabIndex="23">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:LinkButton ID="lkCambiarTienda" runat="server" 
                                ToolTip="Haga clic aquí para cambiar la tienda de trabajo" 
                                onclick="lkCambiarTienda_Click" TabIndex="24">Aplicar cambio de tienda</asp:LinkButton>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                </table>
            </li>
            <li>
                
                <table>
                    <tr>
                        <td colspan="4">
                            <a>Cambiar vendedor a una factura</a></td>
                        <td colspan="4" style="text-align: right">
                            <asp:Label ID="lbInfoCambiarVendedor" runat="server"></asp:Label>
                            <asp:Label ID="lbErrorCambiarVendedor" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            No. de Factura:</td>
                        <td>
                            <asp:TextBox ID="txtFacturaCambiar" runat="server" Width="120px" 
                                style="text-transform: uppercase;" TabIndex="121"></asp:TextBox>
                        </td>
                        <td>
                            Trasladarla a:</td>
                        <td>
                            <asp:DropDownList ID="cbTiendaCambiarVendedor" runat="server" Width="70px" 
                                TabIndex="123" AutoPostBack="True" 
                                onselectedindexchanged="cbTiendaCambiarVendedor_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>
                    <asp:DropDownList ID="cbVendedorCambiar" runat="server" Width="170px" 
                        TabIndex="125" 
                        ToolTip="Seleccione el vendedor a utilizar en el pedido" Visible="true">
                    </asp:DropDownList>
                        </td>
                        <td>
                            <asp:LinkButton ID="lkCambiarVendedor" runat="server" 
                                
                                ToolTip="Haga clic aquí para cambiar el vendedor de la factura ingresada, solo podrá cambiar las facturas que haya grabado usted mismo" onclientclick="return confirm('Desea cambiar el vendedor?');"
                                onclick="lkCambiarVendedor_Click" TabIndex="127">Cambiar Vendedor</asp:LinkButton>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                    </table>
                
            </li>
            <li>
                
                <table>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td colspan="5">
                            <a>Requisición de armado/desarmado en tienda o servicio a cliente</a></td>
                        <td style="text-align: right">
                            <asp:Label ID="lbInfoRequisicionArmado" runat="server"></asp:Label>
                            <asp:Label ID="lbErrorRequisicionArmado" runat="server" Font-Bold="True" 
                                ForeColor="Red"></asp:Label>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            Tipo:</td>
                        <td colspan="2">
                        <asp:DropDownList ID="cbTipoArmado" runat="server" TabIndex="619" 
                            ToolTip="Seleccione el tipo de Armado a enviar en esta requisición" 
                            Width="170px" OnSelectedIndexChanged="cbTipoArmado_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem>--</asp:ListItem>
                        </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            Fecha:</td>
                        <td>
                <asp:DropDownList ID="cbDiaArmado" runat="server" TabIndex="620" 
                    ToolTip="Seleccione el día para el armado">
                    <asp:ListItem>--</asp:ListItem>
                    <asp:ListItem Value="1">01</asp:ListItem>
                    <asp:ListItem Value="2">02</asp:ListItem>
                    <asp:ListItem Value="3">03</asp:ListItem>
                    <asp:ListItem Value="4">04</asp:ListItem>
                    <asp:ListItem Value="5">05</asp:ListItem>
                    <asp:ListItem Value="6">06</asp:ListItem>
                    <asp:ListItem Value="7">07</asp:ListItem>
                    <asp:ListItem Value="8">08</asp:ListItem>
                    <asp:ListItem Value="9">09</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="cbMesArmado" runat="server" TabIndex="621" 
                    ToolTip="Seleccione el mes para el armado">
                    <asp:ListItem>--</asp:ListItem>
                    <asp:ListItem Value="1">Ene</asp:ListItem>
                    <asp:ListItem Value="2">Feb</asp:ListItem>
                    <asp:ListItem Value="3">Mar</asp:ListItem>
                    <asp:ListItem Value="4">Abr</asp:ListItem>
                    <asp:ListItem Value="5">May</asp:ListItem>
                    <asp:ListItem Value="6">Jun</asp:ListItem>
                    <asp:ListItem Value="7">Jul</asp:ListItem>
                    <asp:ListItem Value="8">Ago</asp:ListItem>
                    <asp:ListItem Value="9">Sep</asp:ListItem>
                    <asp:ListItem Value="10">Oct</asp:ListItem>
                    <asp:ListItem Value="11">Nov</asp:ListItem>
                    <asp:ListItem Value="12">Dic</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtAnioArmado" runat="server" TabIndex="622" 
                    ToolTip="Ingrese aquí el año para el armado en formato de 4 dígitos, ejemplo: 2014." 
                    Width="39px">2025</asp:TextBox>
                        
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            Obs.:</td>
                        <td>
                            <asp:TextBox ID="txtObsArmado" runat="server" TabIndex="624" Width="440px" MaxLength="2000" 
                                ToolTip="Ingrese las observaciones que desea sean transmitidas al armador" 
                                Font-Size="X-Small" Height="17px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style2">
                            </td>
                        <td class="auto-style2">
                            Cliente:</td>
                        <td class="auto-style2">
                            <asp:TextBox ID="txtClienteArmado" runat="server" AutoPostBack="True" TabIndex="625" style="text-transform: uppercase;" 
                                ToolTip="Ingrese aquí el código o parte del código del cliente que desea buscar y luego presione ENTER.  No es necesario digitar los ceros a la izquierda del código." 
                                Width="143px" ontextchanged="txtClienteArmado_TextChanged"></asp:TextBox>
                        </td>
                        <td colspan="2" class="auto-style2">
                            <asp:TextBox ID="txtNombreClienteArmado" runat="server" TabIndex="14" 
                                ToolTip="Nombre del cliente" 
                                Width="184px" BackColor="AliceBlue" ReadOnly="True" Font-Size="XX-Small" 
                                Height="17px"></asp:TextBox>
                        </td>
                        <td class="auto-style2">
                            Artículo:</td>
                        <td class="auto-style2">
                            <asp:TextBox ID="txtArticuloArmado" runat="server" AutoPostBack="True" ontextchanged="txtArticuloArmado_TextChanged" TabIndex="626" style="text-transform: uppercase;" 
                                ToolTip="Ingrese el código o parte del código del artículo que desea buscar, o bien puede ingresar alguna palabra clave a buscar en la descripción de los artículos y presione ENTER." 
                                Width="95px"></asp:TextBox>
                            <asp:TextBox ID="txtDescripcionArticuloArmado" runat="server" TabIndex="14" 
                            
                                ToolTip="Descripción del Artículo" 
                                Width="337px" BackColor="AliceBlue" ReadOnly="True" Font-Size="XX-Small" 
                                Height="17px"></asp:TextBox>
                        </td>
                        <td class="auto-style2">
                            </td>
                    </tr>
                    <tr>
                        <td class="auto-style1">
                            </td>
                        <td class="auto-style1">
                            Factura:</td>
                        <td class="auto-style1" colspan="3">
                            <asp:TextBox ID="txtFacturaArmado" runat="server" TabIndex="627" style="text-transform: uppercase;" 
                                ToolTip="Ingrese aquí el número de factura que respaldará el desarmado solicitado." 
                                Width="170px" ontextchanged="txtClienteArmado_TextChanged"></asp:TextBox>
                        </td>
                        <td class="auto-style1">
                            Tienda:
                            </td>
                        <td class="auto-style1">
                            <asp:DropDownList ID="cbTiendaArmado" runat="server" Width="60px" TabIndex="628" />
                        </td>
                        <td class="auto-style1">
                            </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td colspan="6" style="text-align: center">
                            <asp:GridView ID="gridArticulosRequisicion" runat="server" 
                                AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" 
                                BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" 
                                onrowdatabound="gridArticulosRequisicion_RowDataBound" 
                                onselectedindexchanged="gridArticulosRequisicion_SelectedIndexChanged" 
                                Visible="False" Width="888px">
                                <Columns>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkSeleccionarArticuloArmado" runat="server" 
                                                CausesValidation="False" CommandName="Select" Text="Eliminar" 
                                                ToolTip="Haga clic aquí para eliminar este artículo para la requisición de armado"></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle Width="80px" />
                                        <ItemStyle Width="80px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tienda">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtTienda" runat="server" Text='<%# Bind("Tienda") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTienda" runat="server" Text='<%# Bind("Tienda") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="120px" />
                                        <ItemStyle Width="120px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Artículo">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtArticulo" runat="server" Text='<%# Bind("Articulo") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lbArticulo" runat="server" Text='<%# Bind("Articulo") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="120px" />
                                        <ItemStyle Width="120px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Descripción">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lbDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="688px" HorizontalAlign="Left" />
                                        <ItemStyle Width="688px" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#FFF1D4" />
                                <SortedAscendingHeaderStyle BackColor="#B95C30" />
                                <SortedDescendingCellStyle BackColor="#F1E5CE" />
                                <SortedDescendingHeaderStyle BackColor="#93451F" />
                            </asp:GridView>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td colspan="6" style="text-align: center">
                            <asp:LinkButton ID="lkEnviarRequiscionArmado" runat="server" TabIndex="629" 
                                ToolTip="Haga clic aquí para enviar la requisición de armado" 
                                onclick="lkEnviarRequiscionArmado_Click">Enviar requisición</asp:LinkButton>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
                
            </li>

            <li>
                            </li>

            <li>
                <asp:GridView ID="gridClientesArmado" runat="server" Visible="False" 
                AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" 
                BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" 
                onselectedindexchanged="gridClientesArmado_SelectedIndexChanged" 
                onrowdatabound="gridClientesArmado_RowDataBound" TabIndex="24">
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkSeleccionarClienteArmado" runat="server" CausesValidation="False" 
                                CommandName="Select" Text="Seleccionar" ToolTip="Haga clic aquí para seleccionar el cliente de la requisición"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cliente">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCliente" runat="server" Text='<%# Bind("Cliente") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbCliente" runat="server" Text='<%# Bind("Cliente") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nombre">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNombre" runat="server" Text='<%# Bind("Nombre") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbNombre" runat="server" Text='<%# Bind("Nombre") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Font-Size="X-Small" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nit">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNit" runat="server" Text='<%# Bind("Nit") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbNit" runat="server" Text='<%# Bind("Nit") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tel Casa">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTelefonoCasa" runat="server" Text='<%# Bind("TelCasa") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbTelefonoCasa" runat="server" Text='<%# Bind("TelCasa") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tel Trabajo">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTelefonoTrabajo" runat="server" Text='<%# Bind("TelTrabajo") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbTelefonoTrabajo" runat="server" Text='<%# Bind("TelTrabajo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Celular">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCelular" runat="server" Text='<%# Bind("TelCelular") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbCelular" runat="server" Text='<%# Bind("TelCelular") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tienda">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTienda" runat="server" Text='<%# Bind("Tienda") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbTienda" runat="server" Text='<%# Bind("Tienda") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vendedor">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbVendedor" runat="server" Text='<%# Bind("Vendedor") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#FFF1D4" />
                <SortedAscendingHeaderStyle BackColor="#B95C30" />
                <SortedDescendingCellStyle BackColor="#F1E5CE" />
                <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
            </li>

            <li>
                <asp:GridView ID="gridArticulosArmado" runat="server" 
                    AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" 
                    BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" 
                    onrowdatabound="gridArticulosArmado_RowDataBound" 
                    onselectedindexchanged="gridArticulosArmado_SelectedIndexChanged" 
                    Visible="False">
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lkSeleccionarArticuloArmado" runat="server" 
                                    CausesValidation="False" CommandName="Select" Text="Agregar" 
                                    ToolTip="Haga clic aquí para seleccionar este artículo para la requisición de armado"></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle Width="100px" />
                            <ItemStyle Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Artículo">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtArticulo" runat="server" Text='<%# Bind("Articulo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbArticulo" runat="server" Text='<%# Bind("Articulo") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="120px" />
                            <ItemStyle Width="120px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Descripción">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
            </li>

            <li>
                <table class="style1">
                    <tr>
                        <td colspan="5"><a>Cambiar contraseña</a></td>
                        <td colspan="5" style="text-align: right">
                            <asp:Label ID="lbInfoPassword" runat="server"></asp:Label>
                            <asp:Label ID="lbErrorPassword" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            Contraseña actual:</td>
                        <td>
                            <asp:TextBox ID="txtActual" runat="server" 
                                ToolTip="Ingrese aquí su contraseña actual" TabIndex="131" 
                                TextMode="Password" Width="140px"></asp:TextBox>
                        </td>
                        <td>
                            Contraseña Nueva:</td>
                        <td colspan="2">
                            <asp:TextBox ID="txtNueva" runat="server" 
                                ToolTip="Ingrese aquí su contraseña nueva" TabIndex="132" 
                                TextMode="Password" Width="140px"></asp:TextBox>
                        </td>
                        <td>
                            Confirmar Nueva:</td>
                        <td>
                            <asp:TextBox ID="txtNueva2" runat="server" 
                                ToolTip="Confirme aquí su contraseña nueva" TabIndex="133" 
                                TextMode="Password" Width="140px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:LinkButton ID="lkCambiarPassword" runat="server" 
                                ToolTip="Haga clic aquí para cambiar su contraseña" 
                                onclick="lkCambiarPassword_Click" TabIndex="134">Cambiar</asp:LinkButton>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    </table>
                
            </li>
            <li>
                <table  align="center">
                    <tr>
                        <td colspan="3">
                            <a>Solicitud de Soporte</a></td>
                        <td colspan="3" style="text-align: right">
                            <asp:Label ID="lbInfoSolicitud" runat="server"></asp:Label>
                            <asp:Label ID="lbErrorSolicitud" runat="server" Font-Bold="True" 
                                ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            Solicito soporte en:</td>
                        <td colspan="2">
                            <asp:DropDownList ID="cbOpcion" runat="server" TabIndex="901">
                                <asp:ListItem>CLIENTES</asp:ListItem>
                                <asp:ListItem>COTIZACIONES</asp:ListItem>
                                <asp:ListItem>DESPACHOS</asp:ListItem>
                                <asp:ListItem>OTRA</asp:ListItem>
                                <asp:ListItem>PEDIDOS</asp:ListItem>
                                <asp:ListItem>PROBLEMAS COMPUTADORA</asp:ListItem>
                                <asp:ListItem>PROBLEMAS IMPRESORA</asp:ListItem>
                                <asp:ListItem>PROBLEMAS INTERNET</asp:ListItem>
                                <asp:ListItem>PROBLEMAS TELEFONOS</asp:ListItem>
                                <asp:ListItem>TONER BAJO</asp:ListItem>
                            </asp:DropDownList>
                                &nbsp;<asp:FileUpload ID="AdjuntoSoporte" runat="server" 
                                ToolTip="Haga clic aquí para buscar el archivo para adjuntar al soporte" 
                                TabIndex="902" />&nbsp;
                            <asp:LinkButton ID="lkLimpiarSolicitud" runat="server" onclick="lkLimpiarSolicitud_Click" 
                                TabIndex="906" 
                                ToolTip="Haga clic aquí para limpiar los datos de la solicitud de soporte">Limpiar</asp:LinkButton>
                        </td>
                        <td style="text-align: right">
                            <asp:LinkButton ID="lkSolicitud" runat="server" onclick="lkSolicitud_Click" 
                                TabIndex="905" 
                                ToolTip="Haga clic aquí para enviar la solicitud de soporte">Enviar solicitud</asp:LinkButton>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            Comentario:</td>
                        <td colspan="3">
                            <asp:TextBox ID="txtComentario" runat="server" MaxLength="1000" TabIndex="903" 
                                TextMode="MultiLine" style="resize:none;"
                                ToolTip="Ingrese aquí la descripción de su solicitud de soporte" 
                                Width="742px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            Se presentó este error:</td>
                        <td colspan="3">
                            <asp:TextBox ID="txtError" runat="server" MaxLength="1000" TabIndex="904" 
                                TextMode="MultiLine" style="resize:none;" 
                                ToolTip="Copie y puegue aqui el error que haya recibido en el sistema para informarlo al departemento de informática" 
                                Width="742px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
                
            </li>
        </ul>
    </div>
           <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" 
                        TabIndex="530"></asp:Label>
                </td>
            </tr>
        </table>
</div>
</asp:Content>
