﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Net.Mail;
using System.Net;
using Newtonsoft.Json;
using MF_Clases;
using static MF_Clases.Clases;
using System.Web.Configuration;
using MF.Seguridad;

namespace PuntoDeVenta
{
    public partial class utilitarios : BasePage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack)
            {
                ViewState["url"] = Convert.ToString(Session["Url"]);
                HookOnFocus(this.Page as Control);
                
                cbDiaIni.SelectedValue = "1";
                cbMesIni.SelectedValue = DateTime.Now.Date.Month.ToString();
                txtAnioIni.Text = DateTime.Now.Date.Year.ToString();

                cbDiaFin.SelectedValue = DateTime.Now.Day.ToString();
                cbMesFin.SelectedValue = DateTime.Now.Month.ToString();
                txtAnioFin.Text = DateTime.Now.Year.ToString();

                dsPuntoVenta ds = new dsPuntoVenta();
                ViewState["ds"] = ds;

                CargarBodegas();
                TextosInventario();
                CargarTransportistas();

                codigoArticuloDet.Text = "";
                descripcionArticuloDet.Text = "";

                DateTime mFechaArmadoValidar = DateTime.Now.Date.AddDays(2);
                if (mFechaArmadoValidar.DayOfWeek == DayOfWeek.Sunday) mFechaArmadoValidar = mFechaArmadoValidar.AddDays(1);

                txtObsArmado.Text = "";
                txtClienteArmado.Text = "";
                txtFacturaArmado.Text = "";

                CargarTiposDeServicio();
                cbTipoArmado.SelectedValue = "D";
                cbDiaArmado.SelectedValue = mFechaArmadoValidar.Day.ToString();
                cbMesArmado.SelectedValue = mFechaArmadoValidar.Month.ToString();
                txtAnioArmado.Text = mFechaArmadoValidar.Year.ToString();


            }

            Page.ClientScript.RegisterStartupScript(
                typeof(clientes),
                "ScriptDoFocus",
                SCRIPT_DOFOCUS.Replace("REQUEST_LASTFOCUS", Request["__LASTFOCUS"]),
                true);

            try
            {
                int mLinea = Convert.ToInt32(Session["LineaInventario"]);
                TextBox txtConteo = (TextBox)gridInventarioGrabar.Rows[mLinea].FindControl("txtConteo");
                txtConteo.Focus();
            }
            catch
            {
                //Nothing
            }
        }

        private const string SCRIPT_DOFOCUS = @"window.setTimeout('DoFocus()', 1); function DoFocus() {
            try {
                document.getElementById('REQUEST_LASTFOCUS').focus();
            } catch (ex) {}
        }";

        private void CargarTiposDeServicio()
        {

            try
            {
                WebRequest request = WebRequest.Create(string.Format("{0}/TipoServicio/{1}", Convert.ToString(Session["UrlRestServices"]), Const.CATALOGO.TIPO_DE_SERVICIO_ARMADO_TIENDA));
                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    lbErrorRequisicionArmado.Text = string.Format("Enviar soporte a Informática indicando sobre el error: {0}", "Error al cargar los tipos de servicio del catálgo.");
                    return;
                }
                else
                {
                    List<Catalogo> mListadoServicio = new List<Catalogo>();
                    mListadoServicio = JsonConvert.DeserializeObject<List<Catalogo>>(responseString);

                    foreach (var item in mListadoServicio)
                    {
                        ListItem oItem = new ListItem(item.Descripcion, item.Codigo);
                        cbTipoArmado.Items.Add(oItem);
                    }
                }
            }
            catch
            {
                lbErrorRequisicionArmado.Text = string.Format("Enviar soporte a Informática indicando sobre el error: {0}", "Error al cargar los tipos de servicio del catálgo.");
            }
        }

        private void HookOnFocus(Control CurrentControl)
        {
            //checks if control is one of TextBox, DropDownList, ListBox or Button
            if ((CurrentControl is TextBox) ||
                (CurrentControl is DropDownList) ||
                (CurrentControl is ListBox) ||
                (CurrentControl is Button))
                //adds a script which saves active control on receiving focus 
                //in the hidden field __LASTFOCUS.
                (CurrentControl as WebControl).Attributes.Add(
                   "onfocus",
                   "try{document.getElementById('__LASTFOCUS').value=this.id} catch(e) {}");
            //checks if the control has children
            if (CurrentControl.HasControls())
                //if yes do them all recursively
                foreach (Control CurrentChildControl in CurrentControl.Controls)
                    HookOnFocus(CurrentChildControl);
        }

        void CargarTransportistas()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var qTransportistas = ws.DevuelveTransportistas(Convert.ToString(Session["Tienda"]), "");

            DataTable dt = new DataTable("transportistas");
            DataTable dt2 = new DataTable("transportistas");

            dt.Columns.Add(new DataColumn("Nombre", typeof(System.String)));
            dt.Columns.Add(new DataColumn("Transportista", typeof(System.String)));
            dt2.Columns.Add(new DataColumn("Nombre", typeof(System.String)));
            dt2.Columns.Add(new DataColumn("Transportista", typeof(System.String)));

            foreach (var item in qTransportistas)
            {
                DataRow row = dt.NewRow();
                row["Nombre"] = item.Nombre;
                row["Transportista"] = item.Transportista;
                dt.Rows.Add(row);
            }

            DataRow[] rows = dt.Select("", "Nombre");

            for (int ii = 0; ii < rows.Length; ii++)
            {
                DataRow row = dt2.NewRow();
                row["Nombre"] = rows[ii]["Nombre"];
                row["Transportista"] = rows[ii]["Transportista"];
                dt2.Rows.Add(row);
            }

            cbTransportista.DataSource = dt2;
            cbTransportista.DataTextField = "Nombre";
            cbTransportista.DataValueField = "Transportista";
            cbTransportista.DataBind();

            cbTransportista.SelectedValue = "0";
        }

        void TextosInventario()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            txtAnioCerrarInventario.Text = DateTime.Now.Year.ToString();
            cbMesCerrarInventario.SelectedValue = DateTime.Now.Month.ToString();

            int mMesInventario = 0; int mAnioInventario = 0; string mTextoCierre = "";
            lkCargarInventario.Text = ws.FechaInventario(Convert.ToString(Session["Tienda"]), ref mMesInventario, ref mAnioInventario, ref mTextoCierre);
            lkCerrarInventario.Text = mTextoCierre;

            ViewState["MesInventario"] = mMesInventario;
            ViewState["AnioInventario"] = mAnioInventario;
        }

        void CargarBodegas()
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveTiendas();
            var qTodas = ws.DevuelveTiendasTodas();

            cbBodegaPendienteDespachar.DataSource = q;
            cbBodegaPendienteDespachar.DataTextField = "Tienda";
            cbBodegaPendienteDespachar.DataValueField = "Tienda";
            cbBodegaPendienteDespachar.DataBind();

            cbTiendaArmado.DataSource = q;
            cbTiendaArmado.DataTextField = "Tienda";
            cbTiendaArmado.DataValueField = "Tienda";
            cbTiendaArmado.DataBind();

            cbTiendaCambiar.DataSource = qTodas;
            cbTiendaCambiar.DataTextField = "Tienda";
            cbTiendaCambiar.DataValueField = "Tienda";
            cbTiendaCambiar.DataBind();

            cbTiendaCambiarVendedor.DataSource = q;
            cbTiendaCambiarVendedor.DataTextField = "Tienda";
            cbTiendaCambiarVendedor.DataValueField = "Tienda";
            cbTiendaCambiarVendedor.DataBind();

            cbBodegaKardex.DataSource = qTodas;
            cbBodegaKardex.DataTextField = "Tienda";
            cbBodegaKardex.DataValueField = "Tienda";
            cbBodegaKardex.DataBind();

            string mTienda = Convert.ToString(Session["Tienda"]);
            if (mTienda.Length <= 3)
            {
                cbBodegaPendienteDespachar.SelectedValue = mTienda;
                cbTiendaArmado.SelectedValue = mTienda;
                cbTiendaCambiar.SelectedValue = mTienda;
                cbTiendaCambiarVendedor.SelectedValue = mTienda;
                cbBodegaKardex.SelectedValue = mTienda;
            }
            else
            {
                cbBodegaPendienteDespachar.SelectedValue = "F01";
                cbTiendaCambiar.SelectedValue = "F01";
                cbTiendaCambiarVendedor.SelectedValue = "F01";
                cbBodegaKardex.SelectedValue = "F01";
            }

            CargaVendedores();

            for (int ii = 0; ii < gridInventarioGeneral.Columns.Count; ii++)
            {
                for (int jj = 0; jj < q.Count(); jj++)
                {
                    if (q[jj].Tienda == gridInventarioGeneral.Columns[ii].HeaderText) gridInventarioGeneral.Columns[ii].Visible = true;
                }
            }
        }

        void OcultarInventario()
        {
            lbInfoInventario.Text = "";
            lbErrorInventario.Text = "";
            lbInfoInventario.Visible = false;
            lbErrorInventario.Visible = false;
            gridInventario.Visible = false;
            lkOcultarInventario.Visible = false;
            lkImprimirInventario.Visible = false;

            lbInfoInventarioGrabar.Text = "";
            lbErrorInventarioGrabar.Text = "";
            lbInfoInventarioGrabar.Visible = false;
            lbErrorInventarioGrabar.Visible = false;
        }

        void CargarInventario(string accion, string boton, bool enviarCorreo, DataSet dsBodegas, string asunto, string body, string bodega, string tipo)
        {
            string mMensaje = "";
            try
            {
                OcultarInventario();

                string mBodega = Convert.ToString(Session["Tienda"]);
                if (mBodega.Length > 4) mBodega = "F01";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                DateTime mFecha = DateTime.Now.Date; bool mMostrarDetalle = false;
                string mAnio = Convert.ToString(ViewState["AnioInventario"]); string mMes = Convert.ToString(ViewState["MesInventario"]);

                if (DateTime.Now.Date.Day < 7)
                {
                    mFecha = new DateTime(DateTime.Now.Date.Year, DateTime.Now.Date.Month, 1);
                    mFecha = mFecha.AddDays(-1);
                }

                if (accion == "A")
                {
                    mMostrarDetalle = true;
                    mMes = cbMesCerrarInventario.SelectedValue;
                    mAnio = txtAnioCerrarInventario.Text.Trim();

                    mFecha = new DateTime(Convert.ToInt32(mAnio), Convert.ToInt32(mMes), 1);
                    mFecha = mFecha.AddMonths(1);
                    mFecha = new DateTime(mFecha.Year, mFecha.Month, 1);
                    mFecha = mFecha.AddDays(-1);

                    if (enviarCorreo)
                    {
                        mBodega = bodega;
                    }
                    else
                    {
                        Label lbTienda = (Label)gridCerrarInventario.SelectedRow.FindControl("lbTienda");
                        mBodega = lbTienda.Text;
                    }
                }

                ws.Timeout = 999999999;
                var q = ws.DevuelveArticulosInventarioNada();

                if (boton == "LIS")
                {
                    dsPuntoVenta dsArticulos = new dsPuntoVenta();
                    dsArticulos = (dsPuntoVenta)ViewState["ds"];

                    if (accion == "T") q = ws.DevuelveArticulosInventarioTomado(mBodega, mAnio, mMes);
                    if (accion == "I" || accion == "A") q = ws.DevuelveArticulosInventario(mFecha, mBodega, mAnio, mMes, Convert.ToString(Session["usuario"]), ref mMostrarDetalle);
                    if (accion == "INICIAR") q = ws.DevuelveArticulosInventarioIniciar(DateTime.Now.Date, mBodega, mAnio, mMes);
                    if (accion == "ANTERIOR" && dsArticulos.ArticulosInventario.Rows.Count > 0) q = ws.DevuelveArticulosInventarioMover(DateTime.Now.Date, mBodega, mAnio, mMes, dsArticulos.ArticulosInventario.Rows[0]["Articulo"].ToString(), "<");
                    if (accion == "SIGUIENTE" && dsArticulos.ArticulosInventario.Rows.Count > 0) q = ws.DevuelveArticulosInventarioMover(DateTime.Now.Date, mBodega, mAnio, mMes, dsArticulos.ArticulosInventario.Rows[dsArticulos.ArticulosInventario.Rows.Count - 1]["Articulo"].ToString(), ">");

                    if (accion == "PAGINA")
                    {
                        if (txtInventarioPagina.Text.Trim().Length == 0)
                        {
                            lbErrorInventarioGrabar.Visible = true;
                            lbErrorInventarioGrabar.Text = "Debe ingresar una página";
                            txtInventarioPagina.Focus();
                            return;
                        }

                        int mPagina = 0;

                        try
                        {
                            mPagina = Convert.ToInt32(txtInventarioPagina.Text.Trim());
                        }
                        catch
                        {
                            lbErrorInventarioGrabar.Visible = true;
                            lbErrorInventarioGrabar.Text = "Debe ingresar una página válida";
                            txtInventarioPagina.Focus();
                            return;
                        }

                        q = ws.DevuelveArticulosInventarioMover(DateTime.Now.Date, mBodega, mAnio, mMes, "", txtInventarioPagina.Text.Trim());
                    }
                }
                else
                {
                    q = ws.DevuelveArticulosInventarioCriterio(DateTime.Now.Date, mBodega, mAnio, mMes, Convert.ToString(Session["usuario"]), boton, txtBuscarArticuloInventario.Text.Trim());
                }

                if (q.Count() > 0)
                {
                    if (accion == "I")
                    {
                        lbInfoInventario.Visible = true;
                        lbInfoInventario.Text = string.Format("Inventario - {0} (Se listan {1} artículos)", mBodega, q.Count().ToString());
                    }

                    if (accion == "T" || accion == "INICIAR" || accion == "ANTERIOR" || accion == "SIGUIENTE" || accion == "PAGINA")
                    {
                        lbInfoInventarioGrabar.Visible = true;
                        lbInfoInventarioGrabar.Text = string.Format("Inventario - {0} (Se listan {1} artículos)", mBodega, q.Count().ToString());
                    }
                }
                else
                {
                    if (accion == "I")
                    {
                        lbErrorInventario.Visible = true;
                        lbErrorInventario.Text = "No se encontraron artículos para listar";
                    }

                    if (accion == "T" || accion == "INICIAR" || accion == "ANTERIOR" || accion == "SIGUIENTE" || accion == "PAGINA")
                    {
                        lbErrorInventarioGrabar.Visible = true;
                        lbErrorInventarioGrabar.Text = "No se encontraron artículos para listar";
                    }

                    return;
                }

                DataTable dtCodigosM = new DataTable();
                dsPuntoVenta ds = new dsPuntoVenta();

                for (int ii = 0; ii < q.Count(); ii++)
                {
                    bool mInsertar = true;

                    if (q[ii].Articulo == "141083-084" || q[ii].Articulo == "M141083-084")
                    {
                        mInsertar = true;
                    }

                    if (mBodega == "F01")
                    {
                        if (q[ii].Articulo.Substring(0, 1) == "M")
                        {
                            if (Convert.ToInt32(ds.ArticulosInventario.Compute("COUNT(Articulo)", string.Format("Articulo = '{0}'", q[ii].Articulo.Replace("M", "")))) > 0)
                            {
                                mInsertar = false;
                                ds.ArticulosInventario.FindByArticulo(q[ii].Articulo.Replace("M", "")).Existencias = ds.ArticulosInventario.FindByArticulo(q[ii].Articulo.Replace("M", "")).Existencias + q[ii].Existencias;
                                ds.ArticulosInventario.FindByArticulo(q[ii].Articulo.Replace("M", "")).ExistenciaFisica = ds.ArticulosInventario.FindByArticulo(q[ii].Articulo.Replace("M", "")).ExistenciaFisica + q[ii].ExistenciaFisica;
                            }
                        }
                    }

                    if (mInsertar)
                    {
                        DataRow row = ds.ArticulosInventario.NewRow();
                        row["Articulo"] = q[ii].Articulo;
                        row["Descripcion"] = q[ii].Descripcion;
                        row["Localizacion"] = q[ii].Localizacion;
                        row["UltimaCompra"] = q[ii].UltimaCompra;
                        row["Blanco1"] = q[ii].Blanco1;
                        row["Existencias"] = q[ii].Existencias;
                        row["Blanco2"] = q[ii].Blanco2;
                        row["ExistenciaFisica"] = q[ii].ExistenciaFisica;
                        row["Conteo"] = q[ii].Conteo;
                        row["Observaciones"] = q[ii].Observaciones;

                        if (mBodega == "F01")
                        {
                            if (q[ii].Articulo.Substring(0, 3) == "021" || q[ii].Articulo.Substring(0, 3) == "081" || q[ii].Articulo.Substring(0, 2) == "09" || q[ii].Articulo.Substring(0, 3) == "110" || q[ii].Articulo.Substring(0, 3) == "140" || q[ii].Articulo.Substring(0, 3) == "141" || q[ii].Articulo.Substring(0, 3) == "142" || q[ii].Articulo.Substring(0, 3) == "143" || q[ii].Articulo.Substring(0, 3) == "144" || q[ii].Articulo.Substring(0, 3) == "145" || q[ii].Articulo.Substring(0, 4) == "M141" || q[ii].Articulo.Substring(0, 4) == "M142")
                            {
                                row["Tipo"] = "CAMAS";
                            }
                            else
                            {
                                if (q[ii].Articulo.Substring(0, 1) == "0")
                                {
                                    row["Tipo"] = "DECORACIÓN";
                                }
                                else
                                {
                                    row["Tipo"] = "RESTO DE ARTÍCULOS";
                                }
                            }
                        }
                        else
                        {
                            row["Tipo"] = "";
                        }

                        ds.ArticulosInventario.Rows.Add(row);
                    }
                }

                ViewState["ds"] = ds;
                if (accion == "I" || accion == "A") ImprimirInventarioDS(ds, mFecha, mMostrarDetalle, enviarCorreo, dsBodegas, asunto, body, bodega, tipo);

                if (accion == "T" || accion == "INICIAR" || accion == "ANTERIOR" || accion == "SIGUIENTE" || accion == "PAGINA")
                {
                    gridInventarioGrabar.DataSource = ds;
                    gridInventarioGrabar.DataMember = "ArticulosInventario";
                    gridInventarioGrabar.DataBind();
                    gridInventarioGrabar.Visible = true;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                if (accion == "A")
                {
                    lbInfoInventarioCerrar.Text = "";
                    lbErrorInventarioCerrar.Visible = true;
                    lbErrorInventarioCerrar.Text = string.Format("Error inventario - {0} {1} {2}", ex.Message, mMensaje, m);
                }

                if (accion == "I")
                {
                    lbErrorInventario.Visible = true;
                    lbInfoInventarioGeneral.Visible = true;
                    lbInfoInventarioGeneral.Text = mMensaje;
                    lbErrorInventario.Text = string.Format("Error al generar inventario - {0} {1} {2}", ex.Message, mMensaje, m);
                }

                if (accion == "I")
                {
                    lbErrorInventarioGrabar.Visible = true;
                    lbErrorInventarioGrabar.Text = string.Format("Error al generar inventario - {0} {1} {2}", ex.Message, mMensaje, m);
                }
            }
        }

        protected void gridInventario_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void lkInventarioExcel_Click(object sender, EventArgs e)
        {
            TomaInventario(true);
        }

        protected void lkInventario_Click(object sender, EventArgs e)
        {
            TomaInventario(false);
        }

        void TomaInventario(bool excel)
        {
            bool agrupa = false;
            try
            {
                WebRequest request = WebRequest.Create(string.Format("{0}/TomaInventario/?bodega={1}&usuario={2}", Convert.ToString(Session["UrlRestServices"]), Convert.ToString(Session["Tienda"]), Convert.ToString(Session["Usuario"])));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                List<Clases.ArticuloInventario> mArticulos = new List<Clases.ArticuloInventario>();
                mArticulos = JsonConvert.DeserializeObject<List<Clases.ArticuloInventario>>(responseString);
                if (mArticulos.Count(x => x.AgrupacionEspecial != null) > 0)
                {
                    agrupa = true;
                    mArticulos.ForEach(x =>
                    {
                        x.Clasificacion = x.AgrupacionEspecial != null ? x.AgrupacionEspecial : "";
                    });
                }

                dsPuntoVenta ds = new dsPuntoVenta();
                foreach (var item in mArticulos)
                {
                    DataRow mRow = ds.ArticulosInventario.NewRow();
                    mRow["Articulo"] = item.Articulo;
                    mRow["Descripcion"] = item.Descripcion;
                    mRow["Localizacion"] = "";
                    mRow["Clasificacion"] = item.Clasificacion;
                    mRow["UltimaCompra"] = item.UltimaCompra;
                    mRow["Blanco1"] = item.Blanco1;
                    mRow["Existencias"] = item.Existencias;
                    mRow["Blanco2"] = item.Blanco2;
                    mRow["ExistenciaFisica"] = item.ExistenciaFisica;
                    mRow["Conteo"] = item.Conteo;
                    mRow["Observaciones"] = item.Observaciones;
                    mRow["Tipo"] = item.Tipo;
                    ds.ArticulosInventario.Rows.Add(mRow);
                }

                string mBodega = Convert.ToString(Session["Tienda"]);
                if (mBodega.Length > 4) mBodega = "F01";

                DateTime mFecha = DateTime.Now.Date;
                using (ReportDocument reporte = new ReportDocument())
                {
                    string mReporte = "rptTomaInventario.rpt";
                    if (excel) mReporte = "rptInventarioExcel.rpt";

                    string p = (Request.PhysicalApplicationPath + "reportes/" + mReporte);

                    reporte.Load(p);
                    //if (mMostrarDetalle) reporte.OpenSubreport("reportes/rptInventarioDet.rpt");

                    string mNombreDocumento = string.Format("Inventario{0}_{1}-{2}-{3}.pdf", mBodega, mFecha.Day, mFecha.Month, mFecha.Year);

                    try
                    {
                        if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                    }
                    catch
                    {
                        try
                        {
                            mNombreDocumento = mNombreDocumento.Replace(".pdf", "_1.pdf");
                            if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                        }
                        catch
                        {
                            try
                            {
                                mNombreDocumento = mNombreDocumento.Replace(".pdf", "_1.pdf");
                                if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                            }
                            catch
                            {
                                mNombreDocumento = mNombreDocumento.Replace(".pdf", "_1.pdf");
                                if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                            }
                        }
                    }

                    reporte.SetDataSource(ds);
                    reporte.SetParameterValue("Bodega", string.Format("{0} - {1}", mBodega, Convert.ToString(Session["Ubicacion"])));
                    reporte.SetParameterValue("Titulo", string.Format("Inventario al {0}", mFecha.ToShortDateString()));
                    reporte.SetParameterValue("Agrupar", agrupa);

                    if (excel)
                    {
                        mNombreDocumento = mNombreDocumento.Replace(".pdf", ".xls");
                        reporte.ExportToDisk(ExportFormatType.ExcelRecord, @"C:\reportes\" + mNombreDocumento);
                    }
                    else
                    {
                        reporte.ExportToDisk(ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);
                    }

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventario.Visible = true;
                lbErrorInventario.Text = string.Format("Error al imprimir el inventario {0} {1}", ex.Message, m);
            }
        }

        protected void lkOcultarInventario_Click(object sender, EventArgs e)
        {
            OcultarInventario();
        }

        protected void lkImprimirInventario_Click(object sender, EventArgs e)
        {
            ImprimirInventario();
        }

        void ImprimirInventarioDS(dsPuntoVenta ds, DateTime fecha, bool mostrarExistenciaFisica, bool enviarCorreo, DataSet dsBodegas, string asunto, string body, string bodega, string tipo)
        {
            try
            {
                bool mMostrarDetalle = false;
                string mBodega = Convert.ToString(Session["Tienda"]);
                if (mBodega.Length > 4) mBodega = "F01";

                using (ReportDocument reporte = new ReportDocument())
                {
                    if (mostrarExistenciaFisica)
                    {
                        DataRow[] rows = ds.ArticulosInventario.Select("LEN(Observaciones) > 0", "Articulo");

                        if (rows.Length > 0) mMostrarDetalle = true;

                        if (enviarCorreo)
                        {
                            mBodega = bodega;
                        }
                        else
                        {
                            try
                            {
                                Label lbTienda = (Label)gridCerrarInventario.SelectedRow.FindControl("lbTienda");
                                mBodega = lbTienda.Text;
                            }
                            catch
                            {
                                mBodega = Convert.ToString(Session["Tienda"]);
                            }
                        }
                    }

                    string mReporte = "rptInventario.rpt";
                    if (tipo == "EXCEL") mReporte = "rptInventarioExcel.rpt";

                    string p = (Request.PhysicalApplicationPath + "reportes/" + mReporte);

                    reporte.Load(p);
                    if (mMostrarDetalle) reporte.OpenSubreport("reportes/rptInventarioDet.rpt");

                    string mNombreDocumento = string.Format("Inventario{0}_{1}-{2}-{3}.pdf", mBodega, fecha.Day, fecha.Month, fecha.Year);

                    try
                    {
                        if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                    }
                    catch
                    {
                        try
                        {
                            mNombreDocumento = mNombreDocumento.Replace(".pdf", "_1.pdf");
                            if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                        }
                        catch
                        {
                            try
                            {
                                mNombreDocumento = mNombreDocumento.Replace(".pdf", "_1.pdf");
                                if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                            }
                            catch
                            {
                                mNombreDocumento = mNombreDocumento.Replace(".pdf", "_1.pdf");
                                if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);
                            }
                        }
                    }


                    var ws = new wsPuntoVenta.wsPuntoVenta();
                    if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                    string mEnviado = ""; string mDigitado = "";
                    if (tipo == "P") mostrarExistenciaFisica = true;
                    ws.UsuariosInventario(fecha.Year.ToString(), fecha.Month.ToString(), ref mBodega, ref mEnviado, ref mDigitado);

                    reporte.SetDataSource(ds);
                    reporte.SetParameterValue("Bodega", mBodega);
                    reporte.SetParameterValue("MostrarDetalle", mMostrarDetalle);
                    reporte.SetParameterValue("MostrarExistenciaTotal", mostrarExistenciaFisica);
                    reporte.SetParameterValue("EnviadoPor", mEnviado);
                    reporte.SetParameterValue("DigitadoPor", mDigitado);
                    reporte.SetParameterValue("Titulo", string.Format("Inventario al {0}", fecha.ToShortDateString()));

                    if (tipo == "EXCEL")
                    {
                        mNombreDocumento = mNombreDocumento.Replace(".pdf", ".xls");
                        reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, @"C:\reportes\" + mNombreDocumento);
                    }
                    else
                    {
                        reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);
                    }


                    if (enviarCorreo)
                    {
                        string mPreliminar = "";
                        if (tipo == "P") mPreliminar = "   *** PRELIMINAR ***";
                        List<string> mCuentaCorreo = new List<string>();
                        string mAsunto = string.Empty;
                        List<Attachment> attachments = new List<Attachment>();
                        StringBuilder mBody = new StringBuilder();
                        mBody.Append(body);

                        for (int jj = 0; jj < dsBodegas.Tables["destinatarios"].Rows.Count; jj++)
                        {
                            mCuentaCorreo.Add(dsBodegas.Tables["destinatarios"].Rows[jj]["destinatario"].ToString());
                        }

                        mAsunto = string.Format("{0}{1}", asunto, mPreliminar);

                        attachments.Add(new Attachment(@"C:\reportes\" + mNombreDocumento));
                        Mail.EnviarEmail(WebConfigurationManager.AppSettings["UsrMail"].ToString(), mCuentaCorreo, mAsunto, mBody, "Punto de Venta", null, attachments);
                    }
                    else
                    {
                        Response.Clear();
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                        Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                        Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                string m = "";

                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    //Nothing
                }

                lbErrorInventario.Visible = true;
                lbErrorInventario.Text = string.Format("Error al imprimir el inventario DS {0} {1}", ex.Message, m);
            }
        }

        void ImprimirInventario()
        {
            try
            {
                dsPuntoVenta ds = new dsPuntoVenta();

                DateTime mFecha = DateTime.Now.Date;

                for (int ii = 0; ii < gridInventario.Rows.Count; ii++)
                {
                    Label lbArticulo = (Label)gridInventario.Rows[ii].FindControl("lbArticulo");
                    Label lbDescripcion = (Label)gridInventario.Rows[ii].FindControl("lbDescripcion");
                    Label lbLocalizacion = (Label)gridInventario.Rows[ii].FindControl("lbLocalizacion");
                    Label lbUltimaCompra = (Label)gridInventario.Rows[ii].FindControl("lbUltimaCompra");
                    Label lbBlanco1 = (Label)gridInventario.Rows[ii].FindControl("lbBlanco1");
                    Label lbExistencias = (Label)gridInventario.Rows[ii].FindControl("lbExistencias");
                    Label lbBlanco2 = (Label)gridInventario.Rows[ii].FindControl("lbBlanco2");

                    string mFechaSTR = lbUltimaCompra.Text.Replace("-", "/");

                    char[] Separador = { '/' };
                    string[] stringFecha = mFechaSTR.Split(Separador);

                    if (stringFecha[0].Length == 4)
                    {
                        mFecha = new DateTime(Convert.ToInt32(stringFecha[0]), Convert.ToInt32(stringFecha[1]), Convert.ToInt32(stringFecha[2]));
                    }
                    else
                    {
                        mFecha = new DateTime(Convert.ToInt32(stringFecha[2]), Convert.ToInt32(stringFecha[1]), Convert.ToInt32(stringFecha[0]));
                    }

                    DataRow mRow = ds.ArticulosInventario.NewRow();
                    mRow["Articulo"] = lbArticulo.Text;
                    mRow["Descripcion"] = lbDescripcion.Text;
                    mRow["Localizacion"] = lbLocalizacion.Text;
                    mRow["UltimaCompra"] = mFecha;
                    mRow["Blanco1"] = lbBlanco1.Text;
                    mRow["Existencias"] = Convert.ToInt32(lbExistencias.Text);
                    mRow["Blanco2"] = lbBlanco2.Text;
                    ds.ArticulosInventario.Rows.Add(mRow);
                }

                ImprimirInventarioDS(ds, mFecha, false, false, ds, "", "", "", "N");
            }
            catch (Exception ex)
            {
                string m = "";

                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    //Nothing
                }

                lbErrorInventario.Visible = true;
                lbErrorInventario.Text = string.Format("Error al imprimir el inventario {0} {1}", ex.Message, m);
            }
        }

        protected void lkInventarioGeneral_Click(object sender, EventArgs e)
        {
            try
            {
                string mTipo = "A";

                lbInfoInventarioGeneral.Text = "";
                lbErrorInventarioGeneral.Text = "";

                if (codigoArticuloDet.Text.Trim().Length == 0)
                {
                    lbErrorInventarioGeneral.Visible = true;
                    lbErrorInventarioGeneral.Text = "Debe ingresar un criterio de búsqueda";
                    codigoArticuloDet.Focus();
                    return;
                }

                if (codigoArticuloDet.Text.Trim().Length > 0) mTipo = "C";
                if (descripcionArticuloDet.Text.Trim().Length > 0) mTipo = "D";
                if (codigoArticuloDet.Text.Trim().Length > 0 && descripcionArticuloDet.Text.Trim().Length > 0) mTipo = "A";

                CargarInventarioGeneral(mTipo);
            }
            catch (Exception ex)
            {
                string m = "";

                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    //Nothing
                }

                lbErrorInventarioGeneral.Visible = true;
                lbErrorInventarioGeneral.Text = string.Format("Error al llamar al inventario {0} {1}", ex.Message, m);
            }
        }

        protected void lkOcultarInventarioGeneral_Click(object sender, EventArgs e)
        {
            OcultarInventarioGeneral();
        }

        protected void gridInventarioGeneral_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void codigoArticuloDet_TextChanged(object sender, EventArgs e)
        {
            CargarInventarioGeneral("C");
        }

        protected void descripcionArticuloDet_TextChanged(object sender, EventArgs e)
        {
            CargarInventarioGeneral("D");
        }

        void OcultarInventarioGeneral()
        {
            lbInfoInventarioGeneral.Text = "";
            lbErrorInventarioGeneral.Text = "";
            lbInfoInventarioGeneral.Visible = false;
            lbErrorInventarioGeneral.Visible = false;
            gridInventarioGeneral.Visible = false;
            lkOcultarInventarioGeneral.Visible = false;
        }

        void CargarInventarioGeneral(string tipo)
        {
            try
            {
                OcultarInventarioGeneral();

                string mCaracter = codigoArticuloDet.Text.Trim().Substring(0, 1);

                try
                {
                    Int32 mNumero = Convert.ToInt32(mCaracter);
                    tipo = "C";
                }
                catch
                {
                    tipo = "D";
                    if (mCaracter == "M") tipo = "C";
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mMensaje = "";

                ws.Timeout = 999999999;
                DataTable dt = new DataTable();

                dt = ws.DevuelveArticulosInventarioGeneral(DateTime.Now.Date, tipo, codigoArticuloDet.Text.Trim(), codigoArticuloDet.Text.Trim(), ref mMensaje, Convert.ToString(Session["Tienda"]));

                lbInfoInventarioGeneral.Visible = true;
                lbInfoInventarioGeneral.Text = mMensaje;

                gridInventarioGeneral.DataSource = dt;
                gridInventarioGeneral.DataBind();
                gridInventarioGeneral.Visible = true;

                gridInventarioGeneral.DataSource = dt;
                gridInventarioGeneral.DataBind();

                lkOcultarInventarioGeneral.Visible = true;

                //for (int ii = 0; ii < gridInventarioGeneral.Columns.Count; ii++)
                //{
                //    for (int jj = 0; jj < dt.Columns.Count; jj++)
                //    {
                //        if (dt.Columns[jj].ColumnName == gridInventarioGeneral.Columns[ii].HeaderText) gridInventarioGeneral.Columns[ii].Visible = true;
                //    }
                //}

            }
            catch (Exception ex)
            {
                string m = "";

                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    //Nothing
                }

                lbErrorInventarioGeneral.Visible = true;
                lbErrorInventarioGeneral.Text = string.Format("Error al generar inventario general - {0} {1}", ex.Message, m);
            }
        }

        void OcultarSolicitudes()
        {
            //ValidarSesion();
            //lbInfoSolicitudes.Text = "";
            //lbErrorSolicitudes.Text = "";
            //lbInfoSolicitudes.Visible = false;
            //lbErrorSolicitudes.Visible = false;
            //gridSolicitudes.Visible = false;
            //lkOcultarSolicitudes.Visible = false;
        }

        void CargarSolicitudes()
        {
            //try
            //{
            //    ValidarSesion();
            //    OcultarSolicitudes();

            //    var ws = new wsPuntoVenta.wsPuntoVenta();
            //    if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            //    var q = ws.DevuelveAutorizacionesVendedor(Convert.ToString(Session["Vendedor"]));

            //    if (q.Count() > 0)
            //    {
            //        lbInfoSolicitudes.Visible = true;
            //        lbInfoSolicitudes.Text = string.Format("Solicitudes de autorización - Se listan {0} registros", q.Count().ToString());
            //    }
            //    else
            //    {
            //        lbErrorSolicitudes.Visible = true;
            //        lbErrorSolicitudes.Text = "No se encontraron solicitudes para listar";
            //        return;
            //    }

            //    gridSolicitudes.DataSource = q;
            //    gridSolicitudes.DataBind();
            //    gridSolicitudes.Visible = true;

            //    lkOcultarSolicitudes.Visible = true;
            //}
            //catch (Exception ex)
            //{
            //    lbErrorSolicitudes.Visible = true;
            //    lbErrorSolicitudes.Text = string.Format("Error al consultar las solicitudes - {0}", ex.Message);
            //}
        }

        protected void lkConsultarSolicitudes_Click(object sender, EventArgs e)
        {
            CargarSolicitudes();
        }

        protected void lkOcultarSolicitudes_Click(object sender, EventArgs e)
        {
            OcultarSolicitudes();
        }

        protected void gridSolicitudes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");

                Label lbArticuloNombre = (Label)e.Row.FindControl("lbArticuloNombre");
                Label lbObservaciones = (Label)e.Row.FindControl("lbObservaciones");
                Label lbPrecioSolicitado = (Label)e.Row.FindControl("lbPrecioSolicitado");
                Label lbClienteNombre = (Label)e.Row.FindControl("lbClienteNombre");

                e.Row.Cells[6].ToolTip = lbClienteNombre.Text;
                e.Row.Cells[9].ToolTip = lbArticuloNombre.Text;
                e.Row.Cells[26].ToolTip = string.Format("Usted escribió:{0}{1}", System.Environment.NewLine, lbObservaciones.Text);
                e.Row.Cells[27].ToolTip = string.Format("Usted solicitó:{0}{1}", System.Environment.NewLine, lbPrecioSolicitado.Text);
            }
        }

        protected void gridSolicitudes_SelectedIndexChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    ValidarSesion();

            //    lbInfoSolicitudes.Text = "";
            //    lbErrorSolicitudes.Text = "";
            //    lbInfoSolicitudes.Visible = false;
            //    lbErrorSolicitudes.Visible = false;

            //    Label lbEstado = (Label)gridSolicitudes.SelectedRow.FindControl("lbEstado");

            //    if (lbEstado.Text == "P")
            //    {
            //        lbErrorSolicitudes.Visible = true;
            //        lbErrorSolicitudes.Text = "No es posible continuar debido a que esta solicitud ya fué procesada";
            //        return;
            //    }

            //    var ws = new wsPuntoVenta.wsPuntoVenta();
            //    if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            //    string mMensaje = "";

            //    switch (ViewState["AccionSolicitud"].ToString())
            //    {
            //        case "Retractarse":
            //            if (!ws.ProcesarSolicitud(ref mMensaje, gridSolicitudes.SelectedRow.Cells[2].Text, "E", "0", "El vendedor se retractó de esta solicitud", Convert.ToString(Session["Usuario"])))
            //            {
            //                lbErrorSolicitudes.Visible = true;
            //                lbErrorSolicitudes.Text = mMensaje;
            //                return;
            //            }

            //            lbInfoSolicitudes.Visible = true;
            //            lbInfoSolicitudes.Text = mMensaje;

            //            CargarSolicitudes();
            //            break;
            //        case "Aplicar":
            //            if (gridSolicitudes.SelectedRow.Cells[4].Text == "Autorizada")
            //            {
            //                Session["Cliente"] = gridSolicitudes.SelectedRow.Cells[6].Text;
            //                Response.Redirect("pedidos.aspx?accion=sol");
            //            }
            //            else
            //            {
            //                lbErrorSolicitudes.Visible = true;
            //                lbErrorSolicitudes.Text = "Para poder aplicar la solicitud esta debe estar autorizada";
            //                return;
            //            }
            //            break;
            //        default:
            //            break;
            //    }

            //}
            //catch (Exception ex)
            //{
            //    lbErrorSolicitudes.Visible = true;
            //    lbErrorSolicitudes.Text = string.Format("Error al retractarse de la solicitud - {0}", ex.Message);
            //}
        }

        protected void lkAplicar_Click(object sender, EventArgs e)
        {
            ViewState["AccionSolicitud"] = "Aplicar";
        }

        protected void lkRetractarse_Click(object sender, EventArgs e)
        {
            ViewState["AccionSolicitud"] = "Retractarse";
        }

        protected void lkPendienteDespachar_Click(object sender, EventArgs e)
        {
            CargarPendienteDespachar(true);
        }

        protected void lkPendienteDespacharTodos_Click(object sender, EventArgs e)
        {
            CargarPendienteDespachar(false);
        }

        void CargarPendienteDespachar(bool excluirAlmohadas)
        {
            try
            {
                lbInfoPendienteDespachar.Text = "";
                lbErrorPendienteDespachar.Text = "";
                lbInfoPendienteDespachar.Visible = false;
                lbErrorPendienteDespachar.Visible = false;

                txtArticuloPendienteDespachar.Text = "";
                txtNombreArticuloPendienteDespachar.Text = "";
                cbBodegaPendienteDespachar.SelectedValue = "F01";
                cbLocalizacionPendienteDespachar.SelectedValue = "ARMADO";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                ws.Timeout = 999999999;
                var q = ws.DevuelvePendienteDespachar(Convert.ToString(Session["Vendedor"]), excluirAlmohadas);
                

                gridArticulosPendienteDespachar.DataSource = q;
                gridArticulosPendienteDespachar.DataBind();
                gridArticulosPendienteDespachar.Visible = true;
                gridArticulosPendienteDespachar.SelectedIndex = -1;

                if (q.Count() == 0)
                {
                    lbErrorPendienteDespachar.Visible = true;
                    lbErrorPendienteDespachar.Text = "No se encontraron artículos pendientes de despacho";
                }
                else
                {
                    lbInfoPendienteDespachar.Visible = true;
                    lbInfoPendienteDespachar.Text = string.Format("Se encontraron {0} artículos pendientes de despacho", q.Count());
                }

                lkOcultarPendienteDespachar.Visible = true;
            }
            catch (Exception ex)
            {
                lbErrorPendienteDespachar.Visible = true;
                lbErrorPendienteDespachar.Text = string.Format("Error al retractarse de la solicitud - {0}", ex.Message);
            }
        }

        void OcultarPendienteDespachar()
        {
            lbInfoPendienteDespachar.Text = "";
            lbErrorPendienteDespachar.Text = "";
            lbInfoPendienteDespachar.Visible = false;
            lbErrorPendienteDespachar.Visible = false;
            gridArticulosPendienteDespachar.Visible = false;
            lkOcultarPendienteDespachar.Visible = false;
        }

        protected void lkOcultarPendienteDespachar_Click(object sender, EventArgs e)
        {
            OcultarPendienteDespachar();
        }

        protected void gridArticulosPendienteDespachar_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");

                Label lbClienteNombre = (Label)e.Row.FindControl("lbComentario");
                e.Row.Cells[9].ToolTip = lbClienteNombre.Text;
            }
        }

        protected void gridArticulosPendienteDespachar_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string mMensaje = "";
                string mAccion = ViewState["AccionPendienteDespachar"].ToString();

                lbInfoPendienteDespachar.Text = "";
                lbErrorPendienteDespachar.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                Label lbLinea = (Label)gridArticulosPendienteDespachar.SelectedRow.FindControl("lbVale");
                Label lbFactura = (Label)gridArticulosPendienteDespachar.SelectedRow.FindControl("lbEstado");
                Label lbPedido = (Label)gridArticulosPendienteDespachar.SelectedRow.FindControl("lbNumeroPedido");
                Label lbCliente = (Label)gridArticulosPendienteDespachar.SelectedRow.FindControl("lbDescripcion");
                LinkButton lkArticulo = (LinkButton)gridArticulosPendienteDespachar.SelectedRow.FindControl("lkArticulo");

                Session["Cliente"] = lbCliente.Text;

                switch (mAccion)
                {
                    case "Seleccionar":
                        Label lbArticuloNombre = (Label)gridArticulosPendienteDespachar.SelectedRow.FindControl("lbArticuloNombre");
                        Label lbBodega = (Label)gridArticulosPendienteDespachar.SelectedRow.FindControl("lbBodega");
                        Label lbLocalizacion = (Label)gridArticulosPendienteDespachar.SelectedRow.FindControl("lbLocalizacion");

                        txtArticuloPendienteDespachar.Text = lkArticulo.Text;
                        txtNombreArticuloPendienteDespachar.Text = lbArticuloNombre.Text;
                        cbBodegaPendienteDespachar.SelectedValue = lbBodega.Text;
                        cbLocalizacionPendienteDespachar.SelectedValue = lbLocalizacion.Text;

                        gridLocalizaciones.Visible = false;
                        this.MaintainScrollPositionOnPostBack = true;
                        break;
                    case "Articulo":
                        codigoArticuloDet.Text = lkArticulo.Text.Replace(" ", "").Replace("&nbsp;", "");
                        CargarInventarioGeneral("C");
                        this.MaintainScrollPositionOnPostBack = false;
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "jsKeys", "javascript:ScrollTop();", true);

                        break;
                    case "M":
                        AccionArticulos(ref mMensaje, lbPedido.Text, lbFactura.Text, lbLinea.Text, lkArticulo.Text, mAccion);
                        break;
                    case "C":
                        AccionArticulos(ref mMensaje, lbPedido.Text, lbFactura.Text, lbLinea.Text, lkArticulo.Text, mAccion);
                        break;
                    case "R":
                        AccionArticulos(ref mMensaje, lbPedido.Text, lbFactura.Text, lbLinea.Text, lkArticulo.Text, mAccion);
                        break;
                    case "S":
                        AccionArticulos(ref mMensaje, lbPedido.Text, lbFactura.Text, lbLinea.Text, lkArticulo.Text, mAccion);
                        break;
                    case "D":
                        AccionArticulos(ref mMensaje, lbPedido.Text, lbFactura.Text, lbLinea.Text, lkArticulo.Text, mAccion);
                        break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorPendienteDespachar.Visible = true;
                lbErrorPendienteDespachar.Text = string.Format("Error al seleccionar el artículo {0} {1}.", ex.Message, m);
            }
        }

        void AccionArticulos(ref string mensaje, string pedido, string factura, string linea, string articulo, string accion)
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            ws.Url = General.FiestaNETService;

            if (!ws.AccionArticuloPendienteDespachar(ref mensaje, pedido, factura, linea, articulo, accion))
            {
                lbErrorPendienteDespachar.Visible = true;
                lbErrorPendienteDespachar.Text = mensaje;
                return;
            }

            CargarPendienteDespachar(true);
            lbInfoPendienteDespachar.Visible = true;
            lbInfoPendienteDespachar.Text = mensaje;

            this.MaintainScrollPositionOnPostBack = true;
        }

        protected void lbLocalizacion_Click(object sender, EventArgs e)
        {
            if (gridLocalizaciones.Visible)
            {
                gridLocalizaciones.Visible = false;
                return;
            }
            if (txtArticuloPendienteDespachar.Text.Trim().Length == 0) return;

            cargarLocalizaciones(txtArticuloPendienteDespachar.Text.Trim());
        }

        void cargarLocalizaciones(string articulo)
        {
            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveBodegaLocalizacion(articulo);

            gridLocalizaciones.DataSource = q;
            gridLocalizaciones.DataBind();
            gridLocalizaciones.Visible = true;

            if (gridLocalizaciones.Rows.Count > 0)
            {
                gridLocalizaciones.Focus();
                gridLocalizaciones.SelectedIndex = 0;
                LinkButton lkSeleccionar = (LinkButton)gridLocalizaciones.SelectedRow.FindControl("lkSeleccionarLocalizacion");

                lkSeleccionar.Focus();
            }
        }

        protected void lkCambiarBodega_Click(object sender, EventArgs e)
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                lbInfoPendienteDespachar.Text = "";
                lbErrorPendienteDespachar.Text = "";

                Label lbPedido = (Label)gridArticulosPendienteDespachar.SelectedRow.FindControl("lbNumeroPedido");
                Label lbFactura = (Label)gridArticulosPendienteDespachar.SelectedRow.FindControl("lbEstado");
                Label lbLinea = (Label)gridArticulosPendienteDespachar.SelectedRow.FindControl("lbVale");

                lbInfoPendienteDespachar.Text = "";
                lbErrorPendienteDespachar.Text = "";
                lbInfoPendienteDespachar.Visible = false;
                lbErrorPendienteDespachar.Visible = false;

                string mMensaje = "";
                Label lbBodega = (Label)gridArticulosPendienteDespachar.SelectedRow.FindControl("lbBodega");
                LinkButton lkArticulo = (LinkButton)gridArticulosPendienteDespachar.SelectedRow.FindControl("lkArticulo");
                Label lbLocalizacion = (Label)gridArticulosPendienteDespachar.SelectedRow.FindControl("lbLocalizacion");

                if (!ws.CambiaBodegaDespacho(ref mMensaje, lbPedido.Text, lbFactura.Text, lbLinea.Text, lkArticulo.Text, lbBodega.Text, lbLocalizacion.Text, cbBodegaPendienteDespachar.SelectedValue, cbLocalizacionPendienteDespachar.SelectedValue))
                {
                    lbErrorPendienteDespachar.Visible = true;
                    lbErrorPendienteDespachar.Text = mMensaje;
                    return;
                }

                CargarPendienteDespachar(true);
                lbInfoPendienteDespachar.Visible = true;
                lbInfoPendienteDespachar.Text = mMensaje;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorPendienteDespachar.Visible = true;
                lbErrorPendienteDespachar.Text = string.Format("Error al seleccionar el artículo {0} {1}.", ex.Message, m);
            }
        }

        protected void gridLocalizaciones_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridLocalizaciones_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbBodegaPendienteDespachar.SelectedValue = gridLocalizaciones.SelectedRow.Cells[3].Text;
            cbLocalizacionPendienteDespachar.SelectedValue = gridLocalizaciones.SelectedRow.Cells[4].Text;

            gridLocalizaciones.Visible = false;
        }

        protected void txtCodigoBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtCodigoBuscar.Text.Trim().Length == 0) return;
            BuscarCliente("C");
        }

        protected void txtNombreBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtNombreBuscar.Text.Trim().Length == 0) return;
            BuscarCliente("N");
        }

        protected void txtNitBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtNitBuscar.Text.Trim().Length == 0) return;
            BuscarCliente("T");
        }

        protected void txtTelefono_TextChanged(object sender, EventArgs e)
        {
            if (txtTelefono.Text.Trim().Length == 0) return;
            BuscarCliente("E");
        }

        protected void lkOcultarBusqueda_Click(object sender, EventArgs e)
        {
            lbInfoVales.Text = "";
            lbErrorVales.Text = "";
            txtCodigoBuscar.Text = "";
            txtNombreBuscar.Text = "";
            txtNitBuscar.Text = "";
            txtTelefono.Text = "";
            gridClientes.Visible = false;
        }

        protected void gridClientes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mVale = gridClientes.SelectedRow.Cells[8].Text;
                var q = ws.DevuelveValeFechaVenceNombre(mVale);

                if (DateTime.Now.Date > q[0].FechaVence)
                {
                    lbErrorVales.Text = "Este vale ya venció, no es posible imprimirlo";
                    return;
                }

                //Response.Write(string.Format("<script>window.open('vale.aspx?vale={0}', '_blank');</script>", gridClientes.SelectedRow.Cells[8].Text));
                //ScriptManager.RegisterStartupScript(this, typeof(Page), "jsKeys", "javascript:Forzar();", true);

                using (ReportDocument reporte = new ReportDocument())
                {
                    string p = (Request.PhysicalApplicationPath + "reportes/rptVale.rpt");
                    reporte.Load(p);

                    reporte.SetParameterValue("Vale", mVale);
                    reporte.SetParameterValue("FechaVence", q[0].FechaVence);
                    reporte.SetParameterValue("ClienteNombre", q[0].Nombre);

                    string mNombreDocumento = string.Format("Vale_{0}", mVale);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                    lbErrorVales.Text = "";
                }
            }
            catch (Exception ex)
            {
                lbErrorVales.Text = string.Format("Error al imprimir el vale {0}", ex.Message);
            }
        }

        void BuscarCliente(string tipo)
        {
            lbInfoVales.Text = "";
            lbErrorVales.Text = "";

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveClientesVales(tipo, txtCodigoBuscar.Text.Trim(), txtNombreBuscar.Text.Trim().ToUpper(), txtNitBuscar.Text.Trim().ToUpper(), txtTelefono.Text.Trim());

            gridClientes.DataSource = q;
            gridClientes.DataBind();
            gridClientes.Visible = true;
            gridClientes.SelectedIndex = -1;

            if (q.Length == 0) lbErrorVales.Text = "No existen clientes que tengan vale de Q200 con el criterio de búsqueda ingresado.";

            if (q.Count() > 0)
            {
                gridClientes.Focus();
                gridClientes.SelectedIndex = 0;
                LinkButton lkSeleccionar = (LinkButton)gridClientes.SelectedRow.FindControl("lkSeleccionar");

                lkSeleccionar.Focus();
                lbInfoVales.Text = string.Format("Se encontraron {0} clientes con vale generado", q.Count());
            }
        }

        protected void lkLimpiarSolicitud_Click(object sender, EventArgs e)
        {
            LimpiarSolicitud();
        }

        void LimpiarSolicitud()
        {
            cbOpcion.SelectedValue = "CLIENTES";
            txtComentario.Text = "";
            txtError.Text = "";

            lbInfoSolicitud.Text = "";
            lbErrorSolicitud.Text = "";
        }

        protected void lkSolicitud_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoSolicitud.Text = "";
                lbErrorSolicitud.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mMensaje = ""; string mEmail = ""; int mSolicitud = 0; DataTable dtUsuarios = new DataTable("Usuarios"); string mBody = "";
                if (!ws.EnviarSolicitudSoporte(ref mMensaje, Session["Tienda"].ToString(), Session["Vendedor"].ToString(), Session["NombreVendedor"].ToString(), cbOpcion.SelectedValue, txtComentario.Text, txtError.Text, ref mEmail, ref mSolicitud, ref dtUsuarios, ref mBody))
                {
                    lbErrorSolicitud.Text = mMensaje;
                    return;
                }

                List<string> mCuentaCorreo = new List<string>();
                List<string> mResponder = new List<string>();
                string mAsunto = string.Empty;
                List<Attachment> attachments = null;
                StringBuilder Body = new StringBuilder();
                Body.Append(mBody);

                for (int ii = 0; ii < dtUsuarios.Rows.Count; ii++)
                {
                    mCuentaCorreo.Add(dtUsuarios.Rows[ii]["Email"].ToString());
                }


                mResponder.Add(mEmail);
                // mail.From = new MailAddress(mEmail, Session["NombreVendedor"].ToString());
                //mail.From = new MailAddress("puntodeventa@mueblesfiesta.com", Session["NombreVendedor"].ToString());
                mAsunto = string.Format("Soporte para {0} - {1}", Session["NombreVendedor"].ToString(), Session["Tienda"].ToString());

                if (AdjuntoSoporte.HasFile)
                {
                    string mNombreDocumento = @"C:\reportes\" + AdjuntoSoporte.FileName;
                    if (File.Exists(mNombreDocumento)) File.Delete(mNombreDocumento);
                    AdjuntoSoporte.PostedFile.SaveAs(mNombreDocumento);
                    attachments = new List<Attachment>();
                    attachments.Add(new Attachment(mNombreDocumento));
                }
                Mail.EnviarEmail(mEmail, mCuentaCorreo, mAsunto, Body, Session["NombreVendedor"].ToString(), null, attachments, mResponder);
                #region "Código Anterior"
                //mail.Body = mBody;

                //smtp.Host = WebConfigurationManager.AppSettings["MailHost"];
                //smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort"]);

                //smtp.EnableSsl = true;
                //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                //try
                //{
                //    smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail"], WebConfigurationManager.AppSettings["PwdMail"]);
                //    //smtp.Credentials = new System.Net.NetworkCredential("puntodeventa", "jh$%Pjma");
                //    smtp.Send(mail);
                //}
                //catch (Exception ex)
                //{
                //    lbErrorSolicitud.Text = string.Format("Error al enviar el correo {0}", ex.Message);
                //    return;
                //}
                #endregion


                //Ticket
                GrabarTicket();


                lbInfoSolicitud.Text = mMensaje;
                cbOpcion.SelectedValue = "CLIENTES";
                txtComentario.Text = "";
                txtError.Text = "";
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorSolicitud.Text = string.Format("Error {0} {1}", ex.Message, m);
            }
        }

        string LimpiarString(string cadena)
        {
            return cadena.Trim().Replace("&#39;", "'").Replace("&#225;", "á").Replace("&#233;", "é").Replace("&#237;", "í").Replace("&#243;", "ó").Replace("&#250;", "ú").Replace("&quot;", "\"").Replace("&#209;", "Ñ").Replace("&#241;", "Ñ").Replace("&#252;", "ü").Replace("&#193;", "Á").Replace("&#201;", "É").Replace("&#205;", "Í").Replace("&#211;", "Ó").Replace("&#218;", "Ú").Replace("&#220;", "Ü").Replace("&#191;", "¿").Replace("&#161;", "¡");
        }

        private void GrabarTicket()
        {
            string mCategoria = "128"; //Clientes
            if (cbOpcion.SelectedValue == "COTIZACIONES") mCategoria = "129";
            if (cbOpcion.SelectedValue == "DESPACHOS") mCategoria = "130";
            if (cbOpcion.SelectedValue == "OTRA") mCategoria = "131";
            if (cbOpcion.SelectedValue == "PEDIDOS") mCategoria = "132";
            if (cbOpcion.SelectedValue == "PROBLEMAS COMPUTADORA") mCategoria = "133";
            if (cbOpcion.SelectedValue == "PROBLEMAS IMPRESORA") mCategoria = "134";
            if (cbOpcion.SelectedValue == "PROBLEMAS INTERNET") mCategoria = "135";
            if (cbOpcion.SelectedValue == "PROBLEMAS TELEFONOS") mCategoria = "136";
            if (cbOpcion.SelectedValue == "TONER BAJO") mCategoria = "137";

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            string mUsuario = ws.UsuarioWindows(Convert.ToString(Session["Vendedor"]));

            string mError = LimpiarString(txtError.Text.Replace('"', ' '));
            string mComentario = LimpiarString(txtComentario.Text.Replace('"', ' '));

            //Attachment
            string mAttachment = "";
            string attachmentResponse;
            TicketAttachment attach;
            if (AdjuntoSoporte.HasFile)
            {
                attachmentResponse = TicketRequest(AdjuntoSoporte.FileBytes, "POST", "uploads.json", "application/octet-stream");
                attach = JsonConvert.DeserializeObject<TicketAttachment>(attachmentResponse);

                TicketUploads mTicketUploads = new TicketUploads { Token = attach.upload.Token, Filename = AdjuntoSoporte.FileName, ContentType = AdjuntoSoporte.PostedFile.ContentType };
                mAttachment = JsonConvert.SerializeObject(mTicketUploads, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new SnakeCaseContractResolver() });
                mAttachment = string.Format("|uploads|: [ {0} ], ", mAttachment);
            }

            string mData = "{|issue|: { " +
                "|project_id|: 39, " +
                "|tracker_id|: 10, " +
                "|priority_id|: 2, " +
                "|subject|: |Problema en " + Convert.ToString(Session["Tienda"]) + "|, " +
                mAttachment +
                //"|watcher_user_ids|: [ |" + mUsuario + "| ], " +
                "|description|: |" + mComentario + "|, " +
                "|category_id|: " + mCategoria + ", " +
                "|custom_fields|: [{|id|: 4, " +
                    "|value|: [|" + Convert.ToString(Session["Tienda"]).Replace("F", "F-") + "|]}, " +
                    "{|id|: 9, " +
                    "|value|: |" + LimpiarString(Convert.ToString(Session["NombreVendedor"])) + "|}, " +
                    "{|id|: 14, " +
                    "|value|: |" + mError + "|}] " +
                "}}";

            var postData = mData.Replace('|', '"');
            byte[] data = Encoding.ASCII.GetBytes(postData);


            string response = TicketRequest(data, "POST", "issues.json");
        }

        private string TicketRequest(byte[] data, string metodo, string urlParcial, string contentType = "application/json")
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(string.Format("https://soporte.mueblesfiesta.com/{0}", urlParcial));
                request.Headers["X-Redmine-API-Key"] = "6db4f3be0c1170fcbea933b05da85e0c3fbb52f3";

                request.Method = metodo;
                request.ContentType = contentType;
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                return responseString;
            }
            catch (Exception ex)
            {
                log.Error(string.Format("Error: {0}", CatchClass.ExMessage(ex, "utilitarios", "TicketRequest")));
                return "";
            }
        }


        bool LoginExitoso()
        {
            string mServidor = "10.10.5.14";
            string mBase = "EXACTUSERP";

            if (Convert.ToString(Session["Ambiente"]) == "PRO")
            {
                mBase = "EXACTUSERP";
                mServidor = "sql.fiesta.local";
            }

            if (Convert.ToString(Session["Ambiente"]) == "PRU")
            {
                mBase = "PRUEBAS";
                mServidor = "sql.fiesta.local";
            }

            bool mLoginExitoso = false;
            string mStringConexion = string.Format("Data Source = {0}; Initial Catalog = {1};User id = {2};password = {3}; Max Pool Size=600;Pooling=false;Application Name=PDV", mServidor, mBase, Convert.ToString(Session["Usuario"]), txtActual.Text);

            using (System.Data.SqlClient.SqlConnection conexion = new System.Data.SqlClient.SqlConnection(mStringConexion))
            {
                try
                {
                    conexion.Open();
                    conexion.Close();

                    mLoginExitoso = true;
                }
                catch
                {
                    //Nothing
                }
                finally
                {
                    if (conexion.State == System.Data.ConnectionState.Open) conexion.Close();
                }
            }

            return mLoginExitoso;
        }

        protected void lkCambiarPassword_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoPassword.Text = "";
                lbErrorPassword.Text = "";

                if (!LoginExitoso())
                {
                    txtActual.Text = "";
                    lbErrorPassword.Text = "La contraseña actual es inválida";
                    txtActual.Focus();
                    return;
                }

                if (txtNueva.Text.Trim() == txtActual.Text.Trim())
                {
                    txtNueva.Text = "";
                    txtNueva2.Text = "";
                    lbErrorPassword.Text = "La contraseña nueva no puede ser igual a la actual";
                    txtNueva.Focus();
                    return;
                }

                if (txtNueva.Text.Trim() != txtNueva2.Text.Trim())
                {
                    txtNueva.Text = "";
                    txtNueva2.Text = "";
                    lbErrorPassword.Text = "Las contraseñas nuevas no coinciden";
                    txtNueva.Focus();
                    return;
                }

                string mServidor = "10.10.5.14";
                string mBase = "EXACTUSERP";

                if (Convert.ToString(Session["Ambiente"]) == "PRO")
                {
                    mBase = "EXACTUSERP";
                    mServidor = "sql.fiesta.local";
                }

                if (Convert.ToString(Session["Ambiente"]) == "PRU")
                {
                    mBase = "PRUEBAS";
                    mServidor = "sql.fiesta.local";
                }

                System.Data.SqlClient.SqlCommand mCommand = new System.Data.SqlClient.SqlCommand();
                string mStringConexion = string.Format("Data Source = {0}; Initial Catalog = {1};User id = {2};password = {3}; Max Pool Size=600", mServidor, mBase, "mf.fiestanet", "54321;Pooling=false;Application Name=PDV");

                using (System.Data.SqlClient.SqlConnection mConexion = new System.Data.SqlClient.SqlConnection(mStringConexion))
                {
                    mConexion.Open();
                    mCommand.Connection = mConexion;

                    mCommand.CommandText = string.Format("EXEC sp_password null, '{0}', '{1}'", txtNueva.Text.Trim(), Convert.ToString(Session["Usuario"]));
                    mCommand.ExecuteNonQuery();

                    mConexion.Close();
                }

                lbInfoPassword.Text = "La constraseña fue cambiada exitosamente";

                txtActual.Text = "";
                txtNueva.Text = "";
                txtNueva2.Text = "";

                Session["InfoCambio"] = "S";
                Session["Usuario"] = null;
                Session["Tienda"] = null;
                Session["Vendedor"] = null;
                Session["NombreVendedor"] = null;
                Session["Cliente"] = null;
                Session["Pedido"] = null;
                Session["TipoVenta"] = null;
                Session["Financiera"] = null;
                Session["NivelPrecio"] = null;

                Response.Redirect("default.aspx");
            }
            catch (Exception ex)
            {
                lbErrorPassword.Text = string.Format("Error al cambiar la contraseña {0}", ex.Message);
            }
        }

        protected void lkCambiarTienda_Click(object sender, EventArgs e)
        {
            try
            {
                string mMensaje = ""; string mUbicacion = "";

                lbInfoCambiarTienda.Text = "";
                lbErrorCambiarTienda.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.CambiarTienda(Session["Usuario"].ToString(), cbTiendaCambiar.SelectedValue, ref mMensaje, ref mUbicacion))
                {
                    lbErrorCambiarTienda.Text = mMensaje;
                    return;
                }

                Session["Ubicacion"] = mUbicacion;
                Session["Tienda"] = cbTiendaCambiar.SelectedValue;
                Autenticacion.CookieTienda(false);

                TextosInventario();
                lbInfoCambiarTienda.Text = mMensaje;

                Tools.IconHandler(this.Master, Session["Tienda"].ToString());
            }
            catch (Exception ex)
            {
                lbErrorCambiarTienda.Text = string.Format("Error al cambiar la tienda {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }

        protected void codigoArticuloKit_TextChanged(object sender, EventArgs e)
        {
            BuscarKits(codigoArticuloKit.Text.Trim());
        }

        protected void lkListarTodosKits_Click(object sender, EventArgs e)
        {
            BuscarKits("T");
        }

        protected void lkBuscarKit_Click(object sender, EventArgs e)
        {
            BuscarKits(codigoArticuloKit.Text.Trim());
        }

        void BuscarKits(string criterio)
        {
            try
            {
                string mMensaje = "";

                lbInfoKits.Text = "";
                lbErrorKits.Text = "";

                lbInfoKits.Visible = false;
                lbErrorKits.Visible = false;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                wsPuntoVenta.Kits[] q = ws.DevuelveKits(criterio, ref mMensaje);

                if (q.Length == 0)
                {
                    gridCombos.Visible = false;
                    lbErrorKits.Visible = true;
                    lbErrorKits.Text = "No se encontraron combos";
                }
                else
                {
                    gridCombos.DataSource = q;
                    gridCombos.DataBind();
                    gridCombos.Visible = true;
                    gridCombos.SelectedIndex = -1;

                    lbInfoKits.Visible = true;
                    lbInfoKits.Text = string.Format("Se encontraron {0} artículos", q.Length);
                }
            }
            catch (Exception ex)
            {
                string m = "";

                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    //Nothing
                }

                lbErrorKits.Visible = true;
                lbErrorKits.Text = string.Format("Error al consultar los Combos. {0} {1}", ex.Message, m);
            }
        }

        protected void lkArticulosLiquidacion_Click(object sender, EventArgs e)
        {
            try
            {
                string mMensaje = "";

                txtDescuentoLiquidacion.Text = "";
                lbInfoArticulosLiquidacion.Text = "";
                lbErrorArticulosLiquidacion.Text = "";

                lbInfoArticulosLiquidacion.Visible = false;
                lbErrorArticulosLiquidacion.Visible = false;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                decimal mDescuento = ws.DescuentoSolicitarLiquidacion(ref mMensaje, txtSetLiquidacion.Text, txtPorcentajeLiquidacion.Text, txtColchonLiquidacion.Text, txtBaseLiquidacion.Text, txtCantidadBaseLiquidacion.Text, Convert.ToString(Session["Tienda"]));
                if (mDescuento < 0)
                {
                    lbErrorArticulosLiquidacion.Visible = true;
                    lbErrorArticulosLiquidacion.Text = mMensaje;
                    return;
                }

                lbInfoArticulosLiquidacion.Visible = true;
                txtDescuentoLiquidacion.Text = mDescuento.ToString();
                lbInfoArticulosLiquidacion.Text = mMensaje;

                txtDescuentoLiquidacion.Focus();
            }
            catch (Exception ex)
            {
                lbErrorArticulosLiquidacion.Text = string.Format("Error al consultar el descuento {0} {1}", ex.Message, ex.StackTrace.IndexOf("línea") > 0 ? ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")) : "");
            }
        }

        protected void lkReImprimirFactura_Click(object sender, EventArgs e)
        {
            lbErrorReImprimir.Text = "";
            lbErrorReImprimir.Visible = false;
            int i = 20;
            for (int x = 0; x < i; x++)
            {
                Console.WriteLine(x);
            }
            string mMensaje = "";

            try
            {
                WebRequest request = WebRequest.Create(string.Format("{0}/felactivado/", Convert.ToString(Session["UrlRestServices"])));

                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string mFelActivado = Convert.ToString(responseString).Replace("\"", "");
                if (mFelActivado == "S" && txtFacturaReImprimir.Text.Trim().Length > 15)
                {
                    string mReportServer = WebConfigurationManager.AppSettings["ReportServer"].ToString();
                    if (Convert.ToString(Session["Ambiente"]) == "DES" || Convert.ToString(Session["Ambiente"]) == "PRU") mReportServer = WebConfigurationManager.AppSettings["ReportServerPruebas"].ToString();
                    Response.Redirect(string.Format("{0}/Report/FacturaMF_FEL/{1}", mReportServer, txtFacturaReImprimir.Text.Trim().ToUpper()), false);
                }
                else
                {
                    var ws = new wsPuntoVenta.wsPuntoVenta();
                    if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                    wsPuntoVenta.Factura[] q = new wsPuntoVenta.Factura[1];
                    wsPuntoVenta.FacturaLinea[] qLinea = new wsPuntoVenta.FacturaLinea[1];

                    mMensaje = "";
                    if (!ws.DevuelveImpresionFactura(txtFacturaReImprimir.Text.Trim().ToUpper(), ref mMensaje, ref q, ref qLinea))
                    {
                        lbErrorReImprimir.Text = mMensaje;
                        lbErrorReImprimir.Visible = true;
                        return;
                    }

                    dsPuntoVenta ds = new dsPuntoVenta();

                    DataRow row = ds.Factura.NewRow();
                    row["NoFactura"] = q[0].NoFactura;
                    row["Fecha"] = q[0].Fecha;
                    row["Cliente"] = q[0].Cliente;
                    row["Nombre"] = q[0].Nombre;
                    row["Nit"] = q[0].Nit;
                    row["Direccion"] = q[0].Direccion;
                    row["Vendedor"] = q[0].Vendedor;
                    row["Monto"] = q[0].Monto;
                    row["MontoLetras"] = q[0].MontoLetras;
                    ds.Factura.Rows.Add(row);

                    for (int ii = 0; ii < qLinea.Count(); ii++)
                    {
                        DataRow rowDet = ds.FacturaLinea.NewRow();
                        rowDet["NoFactura"] = qLinea[ii].NoFactura;
                        rowDet["Articulo"] = qLinea[ii].Articulo;
                        rowDet["Cantidad"] = qLinea[ii].Cantidad;
                        rowDet["Descripcion"] = qLinea[ii].Descripcion;
                        rowDet["PrecioUnitario"] = qLinea[ii].PrecioUnitario;
                        rowDet["PrecioTotal"] = qLinea[ii].PrecioTotal;
                        ds.FacturaLinea.Rows.Add(rowDet);
                    }

                    using (ReportDocument reporte = new ReportDocument())
                    {
                        string mFormatoFactura = "rptFacturaPV.rpt";
                        if (Convert.ToString(Session["Tienda"]).Length == 3) mFormatoFactura = ws.DevuelveFormatoFactura(Convert.ToString(Session["Tienda"]), txtFacturaReImprimir.Text);

                        string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mFormatoFactura);
                        reporte.Load(p);

                        string mNombreDocumento = string.Format("{0}.pdf", txtFacturaReImprimir.Text);
                        if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                        if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                        txtFacturaReImprimir.Text = "";
                        txtDespachoReImprimir.Text = "";

                        reporte.SetDataSource(ds);
                        reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                        Response.Clear();
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                        Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                        Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorReImprimir.Visible = true;
                lbErrorReImprimir.Text = string.Format("Error al re-imprimir la factura {0} {1}", ex.Message, ex.InnerException);
            }
        }

        protected void lkReImprimirGarantia_Click(object sender, EventArgs e)
        {
            string mMensaje = "";

            try
            {
                if (txtFacturaReImprimir.Text.Length > 0)
                {
                    var ws = new wsPuntoVenta.wsPuntoVenta();
                    if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);
                    wsPuntoVenta.Factura[] q = new wsPuntoVenta.Factura[1];
                    wsPuntoVenta.FacturaLinea[] qLinea = new wsPuntoVenta.FacturaLinea[1];

                    mMensaje = "";
                    if (!ws.DevuelveImpresionFactura(txtFacturaReImprimir.Text, ref mMensaje, ref q, ref qLinea))
                    {
                        lbErrorReImprimir.Text = mMensaje;
                        lbErrorReImprimir.Visible = true;
                        return;
                    }

                    if (q[0].Fecha < Convert.ToDateTime("2019-09-11")) /*se deja hardcode la fecha ya que es la fecha en la que sse implementa el nuevo certificado de garantia*/
                    {
                        string mNombreDocumento = string.Format("Garantia_{0}.pdf", q[0].NoFactura);

                        using (ReportDocument reporte = new ReportDocument())
                        {
                            string mFormato = "rptGarantia.rpt";

                            string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mFormato);
                            reporte.Load(p);

                            reporte.SetParameterValue("factura", q[0].NoFactura);
                            reporte.SetParameterValue("fecha", q[0].Fecha);
                            reporte.SetParameterValue("cliente", string.Format("{0}  -  {1}", q[0].Cliente, q[0].Nombre));

                            if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                            if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                            reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);
                        }

                        Response.Clear();
                        Response.ContentType = "application/pdf";

                        Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                        Response.WriteFile(@"C:\reportes\" + mNombreDocumento);

                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }
                    else
                    {
                        Response.Redirect(string.Format("{0}/Report/CertificadoGarantia/{1}/PDF", General.PortalService, txtFacturaReImprimir.Text));
                    }
                }
            }
            catch (Exception ex)
            {
                lbErrorReImprimir.Visible = true;
                lbErrorReImprimir.Text = string.Format("Error al imprimir garantía {0}", ex.Message);
            }
        }

        protected void lkReImprimirDespacho_Click(object sender, EventArgs e)
        {
            lbErrorReImprimir.Text = "";
            lbErrorReImprimir.Visible = false;

            string mMensaje = "";

            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                DataSet dsInfo = new DataSet();
                wsPuntoVenta.Despacho[] q = new wsPuntoVenta.Despacho[1];
                wsPuntoVenta.DespachoLinea[] qLinea = new wsPuntoVenta.DespachoLinea[1];

                mMensaje = "";
                if (!ws.DevuelveImpresionDespacho(txtDespachoReImprimir.Text, txtFacturaReImprimir.Text, ref mMensaje, ref q, ref qLinea, ref dsInfo))
                {
                    lbErrorReImprimir.Text = mMensaje;
                    lbErrorReImprimir.Visible = true;
                    return;
                }

                dsPuntoVenta ds = new dsPuntoVenta();

                DataRow row = ds.Despacho.NewRow();
                row["NoDespacho"] = q[0].NoDespacho;
                row["Fecha"] = q[0].Fecha;
                row["Cliente"] = q[0].Cliente;
                row["Nombre"] = q[0].Nombre;
                row["Direccion"] = q[0].Direccion;
                row["Indicaciones"] = q[0].Indicaciones;
                row["Telefonos"] = q[0].Telefonos;
                row["FormaPago"] = q[0].FormaPago;
                row["Tienda"] = q[0].Tienda;
                row["Vendedor"] = q[0].Vendedor;
                row["NombreVendedor"] = q[0].NombreVendedor;
                row["Factura"] = q[0].Factura;
                row["Observaciones"] = q[0].Observaciones;
                row["Notas"] = q[0].Notas;
                row["Transportista"] = q[0].Transportista;
                row["NombreTransportista"] = q[0].NombreTransportista;
                ds.Despacho.Rows.Add(row);

                for (int ii = 0; ii < qLinea.Count(); ii++)
                {
                    DataRow rowDet = ds.DespachoLinea.NewRow();
                    rowDet["NoDespacho"] = qLinea[ii].NoDespacho;
                    rowDet["Articulo"] = qLinea[ii].Articulo;
                    rowDet["Cantidad"] = qLinea[ii].Cantidad;
                    rowDet["Descripcion"] = qLinea[ii].Descripcion;
                    rowDet["Bodega"] = qLinea[ii].Bodega;
                    rowDet["Localizacion"] = qLinea[ii].Localizacion;
                    ds.DespachoLinea.Rows.Add(rowDet);
                }

                using (ReportDocument reporte = new ReportDocument())
                {
                    string p = (Request.PhysicalApplicationPath + "reportes/rptDespachoPV.rpt");
                    reporte.Load(p);

                    string mNombreDocumento = string.Format("{0}.pdf", txtDespachoReImprimir.Text);

                    txtFacturaReImprimir.Text = "";
                    txtDespachoReImprimir.Text = "";

                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                    reporte.SetDataSource(ds);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorReImprimir.Visible = true;
                lbErrorReImprimir.Text = string.Format("Error al imprimir el envío. {0} {1} {2}", ex.Message, mMensaje, m);
                return;
            }
        }

        protected void lkAnularFactura_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoAnularFactura.Text = "";
                lbErrorAnularFactura.Text = "";
                lbInfoAnularFactura.Visible = false;
                lbErrorAnularFactura.Visible = false;

                if (txtFacturaAnular.Text.Trim().Length == 0) return;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mArchivo = "";
                if (AdjuntoFactura.HasFile)
                {
                    mArchivo = AdjuntoFactura.FileName;
                    string mNombreDocumento = @"C:\reportes\" + AdjuntoFactura.FileName;
                    if (File.Exists(mNombreDocumento)) File.Delete(mNombreDocumento);
                    AdjuntoFactura.PostedFile.SaveAs(mNombreDocumento);
                }
                else
                {
                    DateTime mFechaFactura = ws.FechaFactura(txtFacturaAnular.Text.Trim());

                    if (mFechaFactura < DateTime.Now.Date && mFechaFactura.Year > 1980)
                    {
                        lbErrorAnularFactura.Visible = true;
                        lbErrorAnularFactura.Text = "La factura es de días anteriores, se requiere que la adjunte escaneada.";
                        return;
                    }
                }

                string mMensaje = "";
                if (!ws.AnularFactura(txtFacturaAnular.Text, ref mMensaje, Convert.ToString(Session["Tienda"]), Convert.ToString(Session["Usuario"]), txtFacturaAnularObservaciones.Text, mArchivo))
                {
                    lbErrorAnularFactura.Visible = true;
                    lbErrorAnularFactura.Text = mMensaje;
                    return;
                }

                lbInfoAnularFactura.Visible = true;
                lbInfoAnularFactura.Text = mMensaje;

                txtFacturaAnular.Text = "";
                txtFacturaAnularObservaciones.Text = "";
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorAnularFactura.Visible = true;
                lbErrorAnularFactura.Text = string.Format("Error al anular la factura. {0} {1}", ex.Message, m);
            }
        }

        protected void lkArticulosReemplazo_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoArticulosReemplazo.Text = "";
                lbErrorArticulosReemplazo.Text = "";
                lbInfoArticulosReemplazo.Visible = false;
                lbErrorArticulosReemplazo.Visible = false;

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                var q = ws.DevuelveArticulosReemplazo();

                gridArticulosReemplazo.DataSource = q;
                gridArticulosReemplazo.DataBind();
                gridArticulosReemplazo.Visible = true;
                gridArticulosReemplazo.SelectedIndex = -1;

                if (q.Length == 0)
                {
                    lbErrorArticulosReemplazo.Visible = true;
                    lbErrorArticulosReemplazo.Text = "No se encontraron artículos";
                }
                else
                {
                    lbInfoArticulosReemplazo.Visible = true;
                    lbInfoArticulosReemplazo.Text = string.Format("Se encontraron {0} artículos", q.Length);
                }
            }
            catch (Exception ex)
            {
                string m = "";

                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    //Nothing
                }

                lbErrorArticulosReemplazo.Visible = true;
                lbErrorArticulosReemplazo.Text = string.Format("Error al consultar los artículos de reemplazo. {0} {1}", ex.Message, m);
            }
        }

        protected void lkArticulo1_Click(object sender, EventArgs e)
        {
            ViewState["AccionArticulo"] = "lkArticulo1";
        }

        protected void lkArticulo2_Click(object sender, EventArgs e)
        {
            ViewState["AccionArticulo"] = "lkArticulo2";
        }

        protected void gridArticulosReemplazo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridArticulosReemplazo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LinkButton lkArticulo = (LinkButton)gridArticulosReemplazo.SelectedRow.FindControl(ViewState["AccionArticulo"].ToString());
                codigoArticuloDet.Text = lkArticulo.Text;
                CargarInventarioGeneral("C");
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = string.Format(" - {0}", ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")));
                }
                catch
                {
                    m = "";
                }

                lbErrorArticulosReemplazo.Visible = true;
                lbErrorArticulosReemplazo.Text = string.Format("Error en los artículos de reemplazo. ** {0}{1}", ex.Message, m);
            }
        }

        protected void lkOcultarArticulosReemplazo_Click(object sender, EventArgs e)
        {
            gridArticulosReemplazo.Visible = false;
            lbErrorArticulosReemplazo.Text = "";
            lbInfoArticulosReemplazo.Text = "";
            lbErrorArticulosReemplazo.Visible = false;
            lbInfoArticulosReemplazo.Visible = false;
        }

        protected void gridCombos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }

            try
            {
                Label lbTipo = (Label)e.Row.FindControl("lbTipo");

                if (lbTipo.Text == "K")
                {
                    e.Row.ForeColor = System.Drawing.Color.FromArgb(227, 89, 4);
                    e.Row.Font.Bold = true;
                }
            }
            catch
            {
                //Nothing
            }
        }

        protected void gridCombos_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Label lbTipo = (Label)gridCombos.SelectedRow.FindControl("lbTipo");
                if (lbTipo.Text == "K")
                {
                    gridCombos.SelectedIndex = -1;
                    return;
                }

                LinkButton lkArticulo = (LinkButton)gridCombos.SelectedRow.FindControl("lkArticuloKit");
                codigoArticuloDet.Text = lkArticulo.Text.Replace(" ", "").Replace("&nbsp;", "");
                CargarInventarioGeneral("C");
                this.MaintainScrollPositionOnPostBack = false;
                ScriptManager.RegisterStartupScript(this, typeof(Page), "jsKeys", "javascript:ScrollTop();", true);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = string.Format(" - {0}", ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")));
                }
                catch
                {
                    m = "";
                }

                lbErrorKits.Visible = true;
                lbErrorKits.Text = string.Format("Error en al consultar las existencias del Kit. ** {0}{1}", ex.Message, m);
            }
        }

        protected void lkSeleccionarPendienteDespachar_Click(object sender, EventArgs e)
        {
            ViewState["AccionPendienteDespachar"] = "Seleccionar";
        }

        protected void lkArticuloPendienteDespachar_Click(object sender, EventArgs e)
        {
            ViewState["AccionPendienteDespachar"] = "Articulo";
        }

        protected void lkMPendienteDespachar_Click(object sender, EventArgs e)
        {
            ViewState["AccionPendienteDespachar"] = "M";
        }

        protected void lkCPendienteDespachar_Click(object sender, EventArgs e)
        {
            ViewState["AccionPendienteDespachar"] = "C";
        }

        protected void lkRPendienteDespachar_Click(object sender, EventArgs e)
        {
            ViewState["AccionPendienteDespachar"] = "R";
        }

        protected void lkSPendienteDespachar_Click(object sender, EventArgs e)
        {
            ViewState["AccionPendienteDespachar"] = "S";
        }

        protected void lkDPendienteDespachar_Click(object sender, EventArgs e)
        {
            ViewState["AccionPendienteDespachar"] = "D";
        }

        protected void lkCambiarVendedor_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoCambiarVendedor.Text = "";
                lbErrorCambiarVendedor.Text = "";
                lbInfoCambiarVendedor.Visible = false;
                lbErrorCambiarVendedor.Visible = false;

                if (txtFacturaCambiar.Text.Trim().Length == 0)
                {
                    lbErrorCambiarVendedor.Visible = true;
                    lbErrorCambiarVendedor.Text = "Debe ingresar el número de factura";
                    txtFacturaCambiar.Focus();
                    return;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mMensaje = "";
                if (!ws.CambiarVendedorFactura(txtFacturaCambiar.Text.Trim().ToUpper(), Convert.ToString(Session["Vendedor"]), Convert.ToString(Session["Usuario"]), ref mMensaje, cbVendedorCambiar.SelectedValue))
                {
                    lbErrorCambiarVendedor.Visible = true;
                    lbErrorCambiarVendedor.Text = mMensaje;
                    return;
                }

                lbInfoCambiarVendedor.Visible = true;
                lbInfoCambiarVendedor.Text = mMensaje;

                txtFacturaCambiar.Text = "";
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = string.Format(" - {0}", ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")));
                }
                catch
                {
                    m = "";
                }

                lbErrorKits.Visible = true;
                lbErrorKits.Text = string.Format("Error en al consultar las existencias del Kit. ** {0}{1}", ex.Message, m);
            }
        }

        protected void cbTiendaCambiarVendedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaVendedores();
        }

        void CargaVendedores()
        {
            cbVendedorCambiar.SelectedValue = null;

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveVendedoresTienda(cbTiendaCambiarVendedor.SelectedValue).ToList();

            cbVendedorCambiar.DataSource = q;
            cbVendedorCambiar.DataTextField = "Nombre";
            cbVendedorCambiar.DataValueField = "Vendedor";
            cbVendedorCambiar.DataBind();
        }

        protected void lkKardex_Click(object sender, EventArgs e)
        {
            CargarKardex();
        }

        protected void lkOcultarKardex_Click(object sender, EventArgs e)
        {
            try
            {
                lbErrorKardex.Text = "";

                dsPuntoVenta ds = new dsPuntoVenta();
                ds = (dsPuntoVenta)ViewState["ds"];

                using (ReportDocument reporte = new ReportDocument())
                {
                    string mFormato = "rptkardex.rpt";

                    string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mFormato);
                    reporte.Load(p);

                    string mNombreDocumento = string.Format("Kardex_{0}.pdf", txtArticuloKardex.Text.Trim());
                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                    txtFacturaReImprimir.Text = "";
                    txtDespachoReImprimir.Text = "";

                    reporte.SetDataSource(ds);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = string.Format(" - {0}", ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")));
                }
                catch
                {
                    m = "";
                }

                lbErrorKardex.Visible = true;
                lbErrorKardex.Text = string.Format("Error al imprimir el Kardex {0}{1}", ex.Message, m);
            }
        }

        protected void gridArticulosKardex_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridArticulosKardex_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lbErrorKardex.Text = "";
                lbInfoKardex.Text = "";

                Label lbArticulo = (Label)gridArticulosKardex.SelectedRow.FindControl("lbArticulo");

                txtArticuloKardex.Text = lbArticulo.Text;
                CargarKardex();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = string.Format(" - {0}", ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")));
                }
                catch
                {
                    m = "";
                }

                lbErrorKardex.Visible = true;
                lbErrorKardex.Text = string.Format("Error al seleccionar el artículo {0}{1}", ex.Message, m);
            }
        }

        protected void gridKardex_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridKardex_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void txtArticuloKardex_TextChanged(object sender, EventArgs e)
        {
            CargarKardex();
        }

        void CargarKardex()
        {
            try
            {
                lbErrorKardex.Text = "";
                lbInfoKardex.Text = "";

                DateTime mFechaIni = new DateTime(Convert.ToInt32(txtAnioIni.Text), Convert.ToInt32(cbMesIni.SelectedValue), Convert.ToInt32(cbDiaIni.SelectedValue));
                DateTime mFechaFin = new DateTime(Convert.ToInt32(txtAnioFin.Text), Convert.ToInt32(cbMesFin.SelectedValue), Convert.ToInt32(cbDiaFin.SelectedValue));

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                gridKardex.Visible = false;
                gridArticulosKardex.Visible = false;

                string mMensaje = ""; string mArticulo = ""; string mDescripcion = ""; int mInicial = 0; int mFinal = 0;

                wsPuntoVenta.Kardex[] q = new wsPuntoVenta.Kardex[1];
                wsPuntoVenta.ArticuloDescripcionPrecio[] qArticulos = new wsPuntoVenta.ArticuloDescripcionPrecio[1];

                if (!ws.DevuelveKardex(ref mMensaje, ref q, ref qArticulos, txtArticuloKardex.Text, mFechaIni, mFechaFin, cbBodegaKardex.SelectedValue, ref mArticulo, ref mDescripcion, ref mInicial, ref mFinal))
                {
                    lbErrorKardex.Text = mMensaje;

                    if (qArticulos.Length > 0)
                    {
                        gridArticulosKardex.DataSource = qArticulos;
                        gridArticulosKardex.DataBind();
                        gridArticulosKardex.Visible = true;
                        gridArticulosKardex.SelectedIndex = -1;
                    }

                    return;
                }

                if (q.Length > 0)
                {
                    gridKardex.DataSource = q;
                    gridKardex.DataBind();
                    gridKardex.Visible = true;
                    gridKardex.SelectedIndex = -1;

                    dsPuntoVenta ds = new dsPuntoVenta();
                    ds = (dsPuntoVenta)ViewState["ds"];

                    ds.Kardex.Clear();
                    for (int ii = 0; ii < q.Count(); ii++)
                    {
                        DataRow mRow = ds.Kardex.NewRow();
                        mRow["Articulo"] = q[ii].Articulo;
                        mRow["Descripcion"] = q[ii].Descripcion;
                        mRow["FechaInicial"] = q[ii].FechaInicial;
                        mRow["FechaFinal"] = q[ii].FechaFinal;
                        mRow["Localizacion"] = q[ii].Localizacion;
                        mRow["Fecha"] = q[ii].Fecha;
                        mRow["Documento"] = q[ii].Documento;
                        mRow["Bodega"] = q[ii].Bodega;
                        mRow["Tipo"] = q[ii].Tipo;
                        mRow["Cantidad"] = q[ii].Cantidad;
                        mRow["Saldo"] = q[ii].Saldo;
                        ds.Kardex.Rows.Add(mRow);
                    }

                    ViewState["ds"] = ds;
                }
                else
                {
                    gridKardex.Visible = false;
                    lbInfoKardex.Text = "No se encontraron movimientos";
                }

                txtArticuloKardex.Text = mArticulo;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = string.Format(" - {0}", ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea")));
                }
                catch
                {
                    m = "";
                }

                lbErrorKardex.Text = string.Format("Error al consultar el Kardex {0}{1}", ex.Message, m);
            }
        }

        protected void gridInventarioGrabar_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void lkCargarInventario_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                CargarInventario("T", "LIS", false, ds, "", "", "", "");
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventarioGrabar.Visible = true;
                lbErrorInventarioGrabar.Text = string.Format("Error al cargar el inventario {0} {1}", ex.Message, m);
            }
        }

        protected void lkOcultarInventarioGrabar_Click(object sender, EventArgs e)
        {
            OcultarInventario();
            gridInventarioGrabar.Visible = false;
        }

        protected void lkGrabarInventario_Click(object sender, EventArgs e)
        {

        }

        void inventarioIniciaEdit(int index)
        {
            try
            {
                gridInventarioGrabar.EditIndex = index;
                BindGridInventarioGrabar();

                GridViewRow row = gridInventarioGrabar.Rows[index];
                TextBox txtConteo = (TextBox)row.FindControl("txtConteo");

                txtConteo.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventarioGrabar.Visible = true;
                lbErrorInventarioGrabar.Text = string.Format("Error al editar las existencias físicas {0} {1}", ex.Message, m);
            }
        }

        protected void gridInventarioGrabar_RowEditing(object sender, GridViewEditEventArgs e)
        {
            inventarioIniciaEdit(e.NewEditIndex);
        }

        protected void gridInventarioGrabar_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            InventarioCancelarEdicion();
        }

        void InventarioCancelarEdicion()
        {
            gridInventarioGrabar.EditIndex = -1;
            BindGridInventarioGrabar();
        }

        void BindGridInventarioGrabar()
        {
            dsPuntoVenta ds = new dsPuntoVenta();
            ds = (dsPuntoVenta)ViewState["ds"];

            gridInventarioGrabar.DataSource = ds;
            gridInventarioGrabar.DataMember = "ArticulosInventario";
            gridInventarioGrabar.DataBind();
            gridInventarioGrabar.Visible = true;
        }

        void inventarioGrabaLinea(int index)
        {
            try
            {
                lbErrorInventarioGrabar.Text = "";
                lbErrorInventarioGrabar.Visible = false;

                lbInfoInventarioGrabar.Text = "";
                lbInfoInventarioGrabar.Visible = false;

                GridViewRow row = gridInventarioGrabar.Rows[index];

                TextBox txtConteo = (TextBox)row.FindControl("txtConteo");
                TextBox txtArticulo = (TextBox)row.FindControl("txtArticulo");
                TextBox txtExistenciaFisica = (TextBox)row.FindControl("txtExistenciaFisica");
                TextBox txtObservaciones = (TextBox)row.FindControl("txtObservacionesInventario");

                int mExistenciaFisica = 0; string mConteo = "";

                try
                {
                    mConteo = txtConteo.Text.Trim().Replace(" ", "").Replace("&", "+").Replace("q", "+").Replace("Q", "+").Replace("w", "+").Replace("W", "+").Replace("e", "+").Replace("E", "+").Replace("r", "+").Replace("R", "+").Replace("t", "+").Replace("T", "+").Replace("y", "+").Replace("Y", "+").Replace("u", "+").Replace("U", "+").Replace("i", "+").Replace("I", "+").Replace("o", "+").Replace("O", "+").Replace("p", "+").Replace("P", "+");
                }
                catch
                {
                    mConteo = "";
                }

                if (mConteo.Length > 0)
                {
                    char[] Suma = { '+' };
                    string[] mSumando = mConteo.Split(Suma);

                    for (int ii = 0; ii < mSumando.Length; ii++)
                    {
                        int mNumero = 0;

                        try
                        {
                            mNumero = Convert.ToInt32(mSumando[ii]);
                        }
                        catch
                        {
                            gridInventarioGrabar.EditIndex = -1;
                            BindGridInventarioGrabar();

                            lbErrorInventarioGrabar.Visible = true;
                            lbErrorInventarioGrabar.Text = string.Format("Caracter inválido");

                            return;
                        }

                        mExistenciaFisica += mNumero;
                    }
                }

                if (mExistenciaFisica < 0)
                {
                    gridInventarioGrabar.EditIndex = -1;
                    BindGridInventarioGrabar();

                    lbErrorInventarioGrabar.Visible = true;
                    lbErrorInventarioGrabar.Text = string.Format("La existencia ingresada es inválida");

                    return;
                }

                txtExistenciaFisica.Text = mExistenciaFisica.ToString();

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mMensaje = "";
                string mBodega = Convert.ToString(Session["Tienda"]);
                if (mBodega.Length > 4) mBodega = "F01";

                ws.Timeout = 999999999;
                if (!ws.ActualizaExistenciaFisica(mBodega, Convert.ToString(ViewState["AnioInventario"]), Convert.ToString(ViewState["MesInventario"]), txtArticulo.Text, mConteo, mExistenciaFisica.ToString(), Convert.ToString(Session["usuario"]), ref mMensaje, txtObservaciones.Text.Trim()))
                {
                    lbErrorInventarioGrabar.Visible = true;
                    lbErrorInventarioGrabar.Text = mMensaje;
                    return;
                }

                lbInfoInventarioGrabar.Visible = true;
                lbInfoInventarioGrabar.Text = mMensaje;

                dsPuntoVenta ds = new dsPuntoVenta();
                ds = (dsPuntoVenta)ViewState["ds"];

                ds.ArticulosInventario.FindByArticulo(txtArticulo.Text).Conteo = mConteo;
                ds.ArticulosInventario.FindByArticulo(txtArticulo.Text).ExistenciaFisica = mExistenciaFisica;
                ds.ArticulosInventario.FindByArticulo(txtArticulo.Text).Observaciones = txtObservaciones.Text.Trim();

                if (txtObservaciones.Text.Trim().Length == 0)
                {
                    ds.ArticulosInventario.FindByArticulo(txtArticulo.Text).Descripcion = ds.ArticulosInventario.FindByArticulo(txtArticulo.Text).Descripcion.Replace("* ", "");
                }
                else
                {
                    if (ds.ArticulosInventario.FindByArticulo(txtArticulo.Text).ToString().Substring(0, 1) != "*") ds.ArticulosInventario.FindByArticulo(txtArticulo.Text).Descripcion = string.Format("* {0}", ds.ArticulosInventario.FindByArticulo(txtArticulo.Text).Descripcion);
                }

                ds.ArticulosInventario.FindByArticulo(txtArticulo.Text).Blanco1 = "";
                ds.ArticulosInventario.FindByArticulo(txtArticulo.Text).Blanco2 = "";

                if (mConteo.Trim().Length > 0) ds.ArticulosInventario.FindByArticulo(txtArticulo.Text).Blanco1 = mConteo;
                if (mExistenciaFisica > 0) ds.ArticulosInventario.FindByArticulo(txtArticulo.Text).Blanco2 = mExistenciaFisica.ToString();

                ViewState["ds"] = ds;

                gridInventarioGrabar.EditIndex = -1;
                BindGridInventarioGrabar();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventarioGrabar.Visible = true;
                lbErrorInventarioGrabar.Text = string.Format("Error al actualizar las existencias físicas {0} {1}", ex.Message, m);
            }
        }

        protected void gridInventarioGrabar_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            inventarioGrabaLinea(e.RowIndex);
        }

        protected void lkCerrarInventario_Click(object sender, EventArgs e)
        {
            CargarBodegasInventario(Convert.ToString(ViewState["AnioInventario"]), Convert.ToString(ViewState["MesInventario"]));
        }

        void CargarBodegasInventario(string anio, string mes)
        {
            try
            {
                lbInfoInventarioCerrar.Text = "";
                lbErrorInventarioCerrar.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                ws.Timeout = 999999999;
                var q = ws.DevuelveBodegasInventario(anio, mes);

                gridCerrarInventario.DataSource = q;
                gridCerrarInventario.DataBind();
                gridCerrarInventario.SelectedIndex = -1;

                for (int ii = 0; ii < gridCerrarInventario.Rows.Count; ii++)
                {
                    GridViewRow row = gridCerrarInventario.Rows[ii];

                    Label lbCerrado = (Label)row.FindControl("lbCerrado");
                    CheckBox cbCerrado = (CheckBox)row.FindControl("cbCerrado");

                    if (lbCerrado.Text == "S")
                    {
                        cbCerrado.Checked = true;
                    }
                    else
                    {
                        cbCerrado.Checked = false;
                    }
                }

                if (gridCerrarInventario.Rows.Count == 0) lbErrorInventarioCerrar.Text = string.Format("No hay información de bodegas en ese año y mes");
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventarioCerrar.Text = string.Format("Error al cargar las tiendas para cerrar {0} {1}", ex.Message, m);
            }
        }

        protected void gridCerrarInventario_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        void GrabarInventarioCierre(string tipo)
        {
            try
            {
                lbInfoInventarioCerrar.Text = "";
                lbErrorInventarioCerrar.Text = "";

                if (gridCerrarInventario.Rows.Count == 0)
                {
                    lbErrorInventarioCerrar.Text = "No hay tiendas para cerrar";
                    return;
                }

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                DataTable dt = new DataTable("Cierre");

                dt.Columns.Add(new DataColumn("Bodega", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Anio", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("Mes", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("Cerrado", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Cerrar", typeof(System.String)));
                dt.Columns.Add(new DataColumn("html", typeof(System.String)));
                dt.Columns.Add(new DataColumn("asunto", typeof(System.String)));

                string mMensaje = "";
                string mMes = cbMesCerrarInventario.SelectedValue;
                string mAnio = txtAnioCerrarInventario.Text.Trim();

                if (tipo == "P")
                {
                    GridViewRow row = gridCerrarInventario.SelectedRow;
                    Label lbTienda = (Label)row.FindControl("lbTienda");

                    DataRow dtRow = dt.NewRow();
                    dtRow["Bodega"] = lbTienda.Text;
                    dtRow["Anio"] = Convert.ToInt32(mAnio);
                    dtRow["Mes"] = Convert.ToInt32(mMes);
                    dtRow["Cerrado"] = "S";
                    dtRow["Cerrar"] = "S";
                    dtRow["html"] = "";
                    dtRow["asunto"] = "";
                    dt.Rows.Add(dtRow);
                }
                else
                {
                    for (int ii = 0; ii < gridCerrarInventario.Rows.Count; ii++)
                    {
                        string mCerrado = "N";
                        GridViewRow row = gridCerrarInventario.Rows[ii];

                        Label lbTienda = (Label)row.FindControl("lbTienda");
                        Label lbCerrado = (Label)row.FindControl("lbCerrado");
                        CheckBox cbCerrado = (CheckBox)row.FindControl("cbCerrado");

                        if (cbCerrado.Checked) mCerrado = "S";

                        DataRow dtRow = dt.NewRow();
                        dtRow["Bodega"] = lbTienda.Text;
                        dtRow["Anio"] = Convert.ToInt32(mAnio);
                        dtRow["Mes"] = Convert.ToInt32(mMes);
                        dtRow["Cerrado"] = mCerrado;
                        dtRow["Cerrar"] = "N";
                        dtRow["html"] = "";
                        dtRow["asunto"] = "";
                        dt.Rows.Add(dtRow);
                    }
                }

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);

                ws.Timeout = 999999999;
                if (!ws.GrabarBodegasInventarioCierre(ref ds, Convert.ToString(Session["Usuario"]), ref mMensaje, tipo))
                {
                    lbErrorInventarioCerrar.Text = mMensaje;
                    return;
                }

                for (int ii = 0; ii < ds.Tables["Cierre"].Rows.Count; ii++)
                {
                    if (ds.Tables["Cierre"].Rows[ii]["Cerrar"].ToString() == "S") CargarInventario("A", "LIS", true, ds, ds.Tables["Cierre"].Rows[ii]["asunto"].ToString(), ds.Tables["Cierre"].Rows[ii]["html"].ToString(), ds.Tables["Cierre"].Rows[ii]["Bodega"].ToString(), tipo);
                }

                lbInfoInventarioCerrar.Text = mMensaje;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventarioCerrar.Text = string.Format("Error al grabar las tiendas para cerrar {0} {1}", ex.Message, m);
            }
        }

        protected void lkGrabarInventarioCierre_Click(object sender, EventArgs e)
        {
            GrabarInventarioCierre("D");
        }

        protected void lkCargarInventarioCerrar_Click(object sender, EventArgs e)
        {
            try
            {
                string mMes = ""; string mAnio = "";

                mMes = cbMesCerrarInventario.SelectedValue;

                if (txtAnioCerrarInventario.Text.Trim().Length == 0)
                {
                    lbErrorInventarioCerrar.Text = string.Format("Debe ingresar el año");
                    txtAnioCerrarInventario.Focus();
                    return;
                }

                int mAnioValidar = 0;

                try
                {
                    mAnioValidar = Convert.ToInt32(txtAnioCerrarInventario.Text.Trim());
                }
                catch
                {
                    lbErrorInventarioCerrar.Text = string.Format("Año inválido");
                    txtAnioCerrarInventario.Focus();
                    return;
                }

                if (mAnioValidar < 2015)
                {
                    lbErrorInventarioCerrar.Text = string.Format("Año inválido");
                    txtAnioCerrarInventario.Focus();
                    return;
                }

                mAnio = mAnioValidar.ToString();
                CargarBodegasInventario(mAnio, mMes);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventarioCerrar.Text = string.Format("Error al cargar los inventarios {0} {1}", ex.Message, m);
            }
        }

        protected void gridCerrarInventario_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string mAccion = ViewState["AccionInventario"].ToString();

                switch (mAccion)
                {
                    case "I":
                        DataSet ds = new DataSet();
                        CargarInventario("A", "LIS", false, ds, "", "", "", "");
                        break;
                    case "P":
                        GrabarInventarioCierre("P");
                        break;
                    case "E":
                        var ws = new wsPuntoVenta.wsPuntoVenta();
                        if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                        string mMensaje = "";
                        string mMes = cbMesCerrarInventario.SelectedValue;
                        string mAnio = txtAnioCerrarInventario.Text.Trim();

                        GridViewRow row = gridCerrarInventario.SelectedRow;
                        Label lbTienda = (Label)row.FindControl("lbTienda");

                        ws.Timeout = 999999999;
                        if (!ws.ActualizarExistenciasCierreInventario(lbTienda.Text, mAnio, mMes, ref mMensaje, Convert.ToString(Session["Usuario"])))
                        {
                            lbErrorInventarioCerrar.Text = mMensaje;
                            return;
                        }

                        lbInfoInventarioCerrar.Text = mMensaje;

                        break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventarioCerrar.Text = string.Format("Error al imprimir los inventarios {0} {1}", ex.Message, m);
            }
        }

        void LlamarInventario(string boton)
        {
            try
            {
                DataSet ds = new DataSet();
                CargarInventario("T", boton, false, ds, "", "", "", "");
                OcultarTablasInventario();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventarioGrabar.Visible = true;
                lbErrorInventarioGrabar.Text = string.Format("Error al cargar el inventario {0} {1}", ex.Message, m);
            }
        }

        protected void btnCamasAdvanced_Click(object sender, EventArgs e)
        {
            LlamarInventario("Advanced");
        }

        protected void btnCamasBackcare_Click(object sender, EventArgs e)
        {
            LlamarInventario("Backcare");
        }

        protected void btnCamasBeautyRest_Click(object sender, EventArgs e)
        {
            LlamarInventario("Beauty Rest");
        }

        protected void btnCamasBeautysleep_Click(object sender, EventArgs e)
        {
            LlamarInventario("Beautysleep");
        }

        protected void btnCamasCarro_Click(object sender, EventArgs e)
        {
            LlamarInventario("Carro");
        }

        protected void btnCamasDeluxe_Click(object sender, EventArgs e)
        {
            LlamarInventario("Deluxe");
        }

        protected void btnCamasDreamSleeper_Click(object sender, EventArgs e)
        {
            LlamarInventario("Dream Sleeper");
        }

        protected void btnCamasEvolution_Click(object sender, EventArgs e)
        {
            LlamarInventario("Evolution");
        }

        protected void btnCamasFrescoFoam_Click(object sender, EventArgs e)
        {
            LlamarInventario("Fresco Foam");
        }

        protected void btnCamasLuisFelipe_Click(object sender, EventArgs e)
        {
            LlamarInventario("Luis Felipe");
        }

        protected void btnCamasLuxurious_Click(object sender, EventArgs e)
        {
            LlamarInventario("Luxurious");
        }

        protected void btnCamasSimmons_Click(object sender, EventArgs e)
        {
            LlamarInventario("Simmons");
        }

        protected void btnCamasTripleCrown_Click(object sender, EventArgs e)
        {
            LlamarInventario("Triple Crown");
        }

        protected void btnCamasTrueEnergy_Click(object sender, EventArgs e)
        {
            LlamarInventario("True Energy");
        }

        protected void btnCamasWonder_Click(object sender, EventArgs e)
        {
            LlamarInventario("Wonder");
        }

        protected void btnCamasWorldClass_Click(object sender, EventArgs e)
        {
            LlamarInventario("World Class");
        }

        protected void btnClosetsAtitude_Click(object sender, EventArgs e)
        {
            LlamarInventario("Atitude");
        }

        protected void btnClosetsAvai_Click(object sender, EventArgs e)
        {
            LlamarInventario("Avai");
        }

        protected void btnClosetsB548_Click(object sender, EventArgs e)
        {
            LlamarInventario("B548");
        }

        protected void btnClosetsBetim_Click(object sender, EventArgs e)
        {
            LlamarInventario("Betim");
        }

        protected void btnClosetsCaribe_Click(object sender, EventArgs e)
        {
            LlamarInventario("Caribe");
        }

        protected void btnClosetsCriative_Click(object sender, EventArgs e)
        {
            LlamarInventario("Criative");
        }

        protected void btnClosetsCrystal_Click(object sender, EventArgs e)
        {
            LlamarInventario("Crystal");
        }

        protected void btnClosetsEsquina_Click(object sender, EventArgs e)
        {
            LlamarInventario("Esquina");
        }

        protected void btnClosetsEU502_Click(object sender, EventArgs e)
        {
            LlamarInventario("EU502");
        }

        protected void btnClosetsFelicce_Click(object sender, EventArgs e)
        {
            LlamarInventario("Felicce");
        }

        protected void btnClosetsHavana_Click(object sender, EventArgs e)
        {
            LlamarInventario("Havana");
        }

        protected void btnClosetsImola_Click(object sender, EventArgs e)
        {
            LlamarInventario("Imola");
        }

        protected void btnClosetsItatiba_Click(object sender, EventArgs e)
        {
            LlamarInventario("Itatiba");
        }

        protected void btnClosetsLena_Click(object sender, EventArgs e)
        {
            LlamarInventario("Lena");
        }

        protected void btnClosetsLaguna_Click(object sender, EventArgs e)
        {
            LlamarInventario("Laguna");
        }

        protected void btnClosetsNewBolivia_Click(object sender, EventArgs e)
        {
            LlamarInventario("New Bolivia");
        }

        protected void btnClosetsReno_Click(object sender, EventArgs e)
        {
            LlamarInventario("Reno");
        }

        protected void btnClosetsSino_Click(object sender, EventArgs e)
        {
            LlamarInventario("Sino");
        }

        protected void btnClosetsSanDiego_Click(object sender, EventArgs e)
        {
            LlamarInventario("San Diego");
        }

        protected void btnInventarioPromoCamas_Click(object sender, EventArgs e)
        {
            LlamarInventario("Camas");
        }

        protected void btnInventarioPromoComedorCocina_Click(object sender, EventArgs e)
        {
            LlamarInventario("Comedor-Cocina");
        }

        protected void btnInventarioPromoGenericos_Click(object sender, EventArgs e)
        {
            LlamarInventario("Genéricos");
        }

        protected void btnInventarioPromoInfantil_Click(object sender, EventArgs e)
        {
            LlamarInventario("Infantil");
        }

        protected void btnInventarioPromoElectronicos_Click(object sender, EventArgs e)
        {
            LlamarInventario("Electrónicos");
        }

        protected void btnInventarioBolsas_Click(object sender, EventArgs e)
        {
            LlamarInventario("Bolsas");
        }

        protected void btnInventarioCarton_Click(object sender, EventArgs e)
        {
            LlamarInventario("Cartón");
        }

        protected void btnInventarioEmpaque_Click(object sender, EventArgs e)
        {
            LlamarInventario("Empaque");
        }

        protected void btnInventarioEntretela_Click(object sender, EventArgs e)
        {
            LlamarInventario("Entretela");
        }

        protected void btnInventarioEsponja_Click(object sender, EventArgs e)
        {
            LlamarInventario("Esponja");
        }

        protected void btnInventarioMarcos_Click(object sender, EventArgs e)
        {
            LlamarInventario("Marcos");
        }

        protected void btnInventarioVarios_Click(object sender, EventArgs e)
        {
            LlamarInventario("Varios");
        }

        protected void btnInventarioAlmohadas_Click(object sender, EventArgs e)
        {
            LlamarInventario("Almohadas");
        }

        protected void btnInventarioBar_Click(object sender, EventArgs e)
        {
            LlamarInventario("Bares");
        }

        protected void btnInventarioBasesLujo_Click(object sender, EventArgs e)
        {
            LlamarInventario("Bases de Lujo");
        }

        protected void btnInventarioBufeteras_Click(object sender, EventArgs e)
        {
            LlamarInventario("Bufeteras");
        }

        protected void btnInventarioCocinas_Click(object sender, EventArgs e)
        {
            LlamarInventario("Cocinas");
        }

        protected void btnInventarioColchonetas_Click(object sender, EventArgs e)
        {
            LlamarInventario("Colchonetas");
        }

        protected void btnInventarioComedores_Click(object sender, EventArgs e)
        {
            LlamarInventario("Comedores");
        }

        protected void btnInventarioComodas_Click(object sender, EventArgs e)
        {
            LlamarInventario("Cómodas");
        }

        protected void btnInventarioCubrecamas_Click(object sender, EventArgs e)
        {
            LlamarInventario("Cubrecamas");
        }

        protected void btnInventarioCuna_Click(object sender, EventArgs e)
        {
            LlamarInventario("Cunas");
        }

        protected void btnInventarioDecoracion_Click(object sender, EventArgs e)
        {
            LlamarInventario("Decoración");
        }

        protected void btnInventarioDormitorios_Click(object sender, EventArgs e)
        {
            LlamarInventario("Dormitorios");
        }

        protected void btnInventarioEscritorios_Click(object sender, EventArgs e)
        {
            LlamarInventario("Escritorios");
        }

        protected void btnInventarioFundas_Click(object sender, EventArgs e)
        {
            LlamarInventario("Fundas");
        }

        protected void btnInventarioGabinetes_Click(object sender, EventArgs e)
        {
            LlamarInventario("Gabinetes");
        }

        protected void btnInventarioGavetero_Click(object sender, EventArgs e)
        {
            LlamarInventario("Gaveteros");
        }

        protected void btnInventarioLibreras_Click(object sender, EventArgs e)
        {
            LlamarInventario("Libreras");
        }

        protected void btnInventarioLiteras_Click(object sender, EventArgs e)
        {
            LlamarInventario("Literas");
        }

        protected void btnInventarioModulos_Click(object sender, EventArgs e)
        {
            LlamarInventario("Módulos");
        }

        protected void btnInventarioRacks_Click(object sender, EventArgs e)
        {
            LlamarInventario("Racks");
        }

        protected void btnInventarioRoperos_Click(object sender, EventArgs e)
        {
            LlamarInventario("Roperos");
        }

        protected void btnInventarioReclinables_Click(object sender, EventArgs e)
        {
            LlamarInventario("Reclinables");
        }

        protected void btnInventarioSabanas_Click(object sender, EventArgs e)
        {
            LlamarInventario("Sábanas");
        }

        protected void btnInventarioSalas_Click(object sender, EventArgs e)
        {
            LlamarInventario("Salas");
        }

        protected void btnInventarioSillas_Click(object sender, EventArgs e)
        {
            LlamarInventario("Sillas");
        }

        protected void btnInventarioSofaCama_Click(object sender, EventArgs e)
        {
            LlamarInventario("Sofás Cama");
        }

        protected void btnInventarioTrinchantes_Click(object sender, EventArgs e)
        {
            LlamarInventario("Trinchantes");
        }

        protected void btnInventarioZapateras_Click(object sender, EventArgs e)
        {
            LlamarInventario("Zapateras");
        }

        protected void txtBuscarArticuloInventario_TextChanged(object sender, EventArgs e)
        {
            LlamarInventario("Buscar");
        }

        protected void lkBuscarArticuloInventario_Click(object sender, EventArgs e)
        {
            LlamarInventario("Buscar");
        }

        protected void lkVerInventario_Click(object sender, EventArgs e)
        {
            try
            {
                InventarioCancelarEdicion();

                DataSet ds = new DataSet();
                CargarInventario("T", "LIS", false, ds, "", "", "", "");
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventarioGrabar.Visible = true;
                lbErrorInventarioGrabar.Text = string.Format("Error al cargar el inventario {0} {1}", ex.Message, m);
            }
        }

        protected void btnCamasRecharge_Click(object sender, EventArgs e)
        {
            LlamarInventario("Recharge");
        }

        protected void btnCamasFirm_Click(object sender, EventArgs e)
        {
            LlamarInventario("Firm");
        }

        protected void btnCamasPlush_Click(object sender, EventArgs e)
        {
            LlamarInventario("Plush");
        }

        protected void btnCamasPureEssence_Click(object sender, EventArgs e)
        {
            LlamarInventario("Pure Essence");
        }

        protected void btnInventarioBaules_Click(object sender, EventArgs e)
        {
            LlamarInventario("Baúles");
        }

        protected void lkInventarioCamas_Click(object sender, EventArgs e)
        {
            OcultarTablasInventario();
            //tblInventarioCamas.Visible = true;
        }

        protected void lkInventarioClosets_Click(object sender, EventArgs e)
        {
            OcultarTablasInventario();
            //tblInventarioClosets.Visible = true;
        }

        protected void lkInventarioMateriaPrima_Click(object sender, EventArgs e)
        {
            OcultarTablasInventario();
            //tblInventarioMateriaPrima.Visible = true;
        }

        protected void lkInventarioPromocionales_Click(object sender, EventArgs e)
        {
            OcultarTablasInventario();
            //tblInventarioPromociones.Visible = true;
        }

        protected void lkInventarioVarios_Click(object sender, EventArgs e)
        {
            OcultarTablasInventario();
            //tblInventarioVarios.Visible = true;
        }

        void OcultarTablasInventario()
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "jsKeys", "javascript:OcultarTablas();", true);
        }

        protected void btnInventarioMesas_Click(object sender, EventArgs e)
        {
            LlamarInventario("Mesas");
        }

        protected void lkInventarioSalas_Click(object sender, EventArgs e)
        {
            OcultarTablasInventario();
            //tblInventarioSalas.Visible = true;
        }

        protected void btnInventario2109_Click(object sender, EventArgs e)
        {
            LlamarInventario("2109");
        }

        protected void btnInventario2309_Click(object sender, EventArgs e)
        {
            LlamarInventario("2309");
        }

        protected void btnInventario2409_Click(object sender, EventArgs e)
        {
            LlamarInventario("Mesa2409");
        }

        protected void btnInventarioAris_Click(object sender, EventArgs e)
        {
            LlamarInventario("Aris");
        }

        protected void btnInventarioArles_Click(object sender, EventArgs e)
        {
            LlamarInventario("Arles");
        }

        protected void btnInventarioBari_Click(object sender, EventArgs e)
        {
            LlamarInventario("Bari");
        }

        protected void btnInventarioBasilea_Click(object sender, EventArgs e)
        {
            LlamarInventario("Basilea");
        }

        protected void btnInventarioCarolina_Click(object sender, EventArgs e)
        {
            LlamarInventario("Carolina");
        }

        protected void btnInventarioChelsea_Click(object sender, EventArgs e)
        {
            LlamarInventario("Chelsea");
        }

        protected void btnInventarioChenille_Click(object sender, EventArgs e)
        {
            LlamarInventario("Chenille");
        }

        protected void btnInventarioCiry_Click(object sender, EventArgs e)
        {
            LlamarInventario("Ciry");
        }

        protected void btnInventarioCristy_Click(object sender, EventArgs e)
        {
            LlamarInventario("Cristy");
        }

        protected void btnInventarioCorduroy_Click(object sender, EventArgs e)
        {
            LlamarInventario("Corduroy");
        }

        protected void btnInventarioCuerina_Click(object sender, EventArgs e)
        {
            LlamarInventario("Cuerina");
        }

        protected void btnInventarioCuero_Click(object sender, EventArgs e)
        {
            LlamarInventario("Cuero");
        }

        protected void btnInventarioDaVinci_Click(object sender, EventArgs e)
        {
            LlamarInventario("Da Vinci");
        }

        protected void btnInventarioDiva_Click(object sender, EventArgs e)
        {
            LlamarInventario("Diva");
        }

        protected void btnInventarioDouglas_Click(object sender, EventArgs e)
        {
            LlamarInventario("Douglas");
        }

        protected void btnInventarioElizabeth_Click(object sender, EventArgs e)
        {
            LlamarInventario("Elizabeth");
        }

        protected void btnInventarioEnnia_Click(object sender, EventArgs e)
        {
            LlamarInventario("Ennia");
        }

        protected void btnInventarioEscalona_Click(object sender, EventArgs e)
        {
            LlamarInventario("Escalona");
        }

        protected void btnInventarioEternity_Click(object sender, EventArgs e)
        {
            LlamarInventario("Eternity");
        }

        protected void btnInventarioFashion_Click(object sender, EventArgs e)
        {
            LlamarInventario("Fashion");
        }

        protected void btnInventarioGreta_Click(object sender, EventArgs e)
        {
            LlamarInventario("Greta");
        }

        protected void btnInventarioHazel_Click(object sender, EventArgs e)
        {
            LlamarInventario("Hazel");
        }

        protected void btnInventarioImperio_Click(object sender, EventArgs e)
        {
            LlamarInventario("Imperio");
        }

        protected void btnInventarioIsabel_Click(object sender, EventArgs e)
        {
            LlamarInventario("Isabel");
        }

        protected void btnInventarioKelly_Click(object sender, EventArgs e)
        {
            LlamarInventario("Kelly");
        }

        protected void btnInventarioLexington_Click(object sender, EventArgs e)
        {
            LlamarInventario("Lexington");
        }

        protected void btnInventarioLiverpool_Click(object sender, EventArgs e)
        {
            LlamarInventario("Liverpool");
        }

        protected void btnInventarioLoal_Click(object sender, EventArgs e)
        {
            LlamarInventario("Loal");
        }

        protected void btnInventarioLondres_Click(object sender, EventArgs e)
        {
            LlamarInventario("Londres");
        }

        protected void btnInventarioLucas_Click(object sender, EventArgs e)
        {
            LlamarInventario("Lucas");
        }

        protected void btnInventarioMallorca_Click(object sender, EventArgs e)
        {
            LlamarInventario("Mallorca");
        }

        protected void btnInventarioMichelle_Click(object sender, EventArgs e)
        {
            LlamarInventario("Michelle");
        }

        protected void btnInventarioMilan_Click(object sender, EventArgs e)
        {
            LlamarInventario("Milan");
        }

        protected void btnInventarioMicrofibra_Click(object sender, EventArgs e)
        {
            LlamarInventario("Microfibra");
        }

        protected void btnInventarioMicrotex_Click(object sender, EventArgs e)
        {
            LlamarInventario("Microtex");
        }

        protected void btnInventarioMirka_Click(object sender, EventArgs e)
        {
            LlamarInventario("Mirka");
        }

        protected void btnInventarioMisisipi_Click(object sender, EventArgs e)
        {
            LlamarInventario("Misisipi");
        }

        protected void btnInventarioModena_Click(object sender, EventArgs e)
        {
            LlamarInventario("Modena");
        }

        protected void btnInventarioModular_Click(object sender, EventArgs e)
        {
            LlamarInventario("Modular");
        }

        protected void btnInventarioMonet_Click(object sender, EventArgs e)
        {
            LlamarInventario("Monet");
        }

        protected void btnInventarioMontblanc_Click(object sender, EventArgs e)
        {
            LlamarInventario("Montblanc");
        }

        protected void btnInventarioMontecarlo_Click(object sender, EventArgs e)
        {
            LlamarInventario("Montecarlo");
        }

        protected void btnInventarioPatricia_Click(object sender, EventArgs e)
        {
            LlamarInventario("Patricia");
        }

        protected void btnInventarioPicasso_Click(object sender, EventArgs e)
        {
            LlamarInventario("Picasso");
        }

        protected void btnInventarioPontevedra_Click(object sender, EventArgs e)
        {
            LlamarInventario("Pontevedra");
        }

        protected void btnInventarioRio_Click(object sender, EventArgs e)
        {
            LlamarInventario("Rio");
        }

        protected void btnInventarioScalia_Click(object sender, EventArgs e)
        {
            LlamarInventario("Scalia");
        }

        protected void btnInventarioSeattle_Click(object sender, EventArgs e)
        {
            LlamarInventario("Seattle");
        }

        protected void btnInventarioSerena_Click(object sender, EventArgs e)
        {
            LlamarInventario("Serena");
        }

        protected void btnInventarioSuecia_Click(object sender, EventArgs e)
        {
            LlamarInventario("Suecia");
        }

        protected void btnInventarioTriunfo_Click(object sender, EventArgs e)
        {
            LlamarInventario("Triunfo");
        }

        protected void btnInventarioVelvet_Click(object sender, EventArgs e)
        {
            LlamarInventario("Velvet");
        }

        protected void btnInventarioVenecia_Click(object sender, EventArgs e)
        {
            LlamarInventario("Venecia");
        }

        protected void btnInventarioVerona_Click(object sender, EventArgs e)
        {
            LlamarInventario("Verona");
        }

        protected void btnInventarioVeronica_Click(object sender, EventArgs e)
        {
            LlamarInventario("Veronica");
        }

        protected void btnInventarioViscaya_Click(object sender, EventArgs e)
        {
            LlamarInventario("Viscaya");
        }

        protected void btnInventarioWestin_Click(object sender, EventArgs e)
        {
            LlamarInventario("Westin");
        }

        protected void lkInventarioComedores_Click(object sender, EventArgs e)
        {
            OcultarTablasInventario();
            //tblInventarioComedores.Visible = true;
        }

        protected void btnInventarioAvion_Click(object sender, EventArgs e)
        {
            LlamarInventario("Avion");
        }

        protected void btnInventarioBarcelona_Click(object sender, EventArgs e)
        {
            LlamarInventario("Barcelona");
        }

        protected void btnInventarioBianca_Click(object sender, EventArgs e)
        {
            LlamarInventario("Bianca");
        }

        protected void btnInventarioBoleado_Click(object sender, EventArgs e)
        {
            LlamarInventario("Boleado");
        }

        protected void btnInventarioBuda_Click(object sender, EventArgs e)
        {
            LlamarInventario("Buda");
        }

        protected void btnInventarioCuadrado_Click(object sender, EventArgs e)
        {
            LlamarInventario("Cuadrado");
        }

        protected void btnInventarioDubai_Click(object sender, EventArgs e)
        {
            LlamarInventario("Dubai");
        }

        protected void btnInventarioEspania_Click(object sender, EventArgs e)
        {
            LlamarInventario("Espania");
        }

        protected void btnInventarioFlorencia_Click(object sender, EventArgs e)
        {
            LlamarInventario("Florencia");
        }

        protected void btnInventarioFrances_Click(object sender, EventArgs e)
        {
            LlamarInventario("Frances");
        }

        protected void btnInventarioHamburgo_Click(object sender, EventArgs e)
        {
            LlamarInventario("Hamburgo");
        }

        protected void btnInventarioKeenan_Click(object sender, EventArgs e)
        {
            LlamarInventario("Keenan");
        }

        protected void btnInventarioLisboa_Click(object sender, EventArgs e)
        {
            LlamarInventario("Lisboa");
        }

        protected void btnInventarioMajestic_Click(object sender, EventArgs e)
        {
            LlamarInventario("Majestic");
        }

        protected void btnInventarioMalibu_Click(object sender, EventArgs e)
        {
            LlamarInventario("Malibu");
        }

        protected void btnInventarioMarmol_Click(object sender, EventArgs e)
        {
            LlamarInventario("Marmol");
        }

        protected void btnInventarioMartel_Click(object sender, EventArgs e)
        {
            LlamarInventario("Martel");
        }

        protected void btnInventarioMontana_Click(object sender, EventArgs e)
        {
            LlamarInventario("Montana");
        }

        protected void btnInventarioOmega_Click(object sender, EventArgs e)
        {
            LlamarInventario("Omega");
        }

        protected void btnInventarioOriental_Click(object sender, EventArgs e)
        {
            LlamarInventario("Oriental");
        }

        protected void btnInventarioOval_Click(object sender, EventArgs e)
        {
            LlamarInventario("Oval");
        }

        protected void btnInventarioPekin_Click(object sender, EventArgs e)
        {
            LlamarInventario("Pekin");
        }

        protected void btnInventarioPedestales_Click(object sender, EventArgs e)
        {
            LlamarInventario("Pedestales");
        }

        protected void btnInventarioRectangular_Click(object sender, EventArgs e)
        {
            LlamarInventario("Rectangular");
        }

        protected void btnInventarioRecto_Click(object sender, EventArgs e)
        {
            LlamarInventario("Recto");
        }

        protected void btnInventarioRed_Click(object sender, EventArgs e)
        {
            LlamarInventario("Red");
        }

        protected void btnInventarioRedondo_Click(object sender, EventArgs e)
        {
            LlamarInventario("Redondo");
        }

        protected void btnInventarioRegina_Click(object sender, EventArgs e)
        {
            LlamarInventario("Regina");
        }

        protected void btnInventarioRegency_Click(object sender, EventArgs e)
        {
            LlamarInventario("Regency");
        }

        protected void btnInventarioReina_Click(object sender, EventArgs e)
        {
            LlamarInventario("Reina");
        }

        protected void btnInventarioRimo_Click(object sender, EventArgs e)
        {
            LlamarInventario("Rimo");
        }

        protected void btnInventarioSiena_Click(object sender, EventArgs e)
        {
            LlamarInventario("Siena");
        }

        protected void btnInventarioSillaCurva_Click(object sender, EventArgs e)
        {
            LlamarInventario("SillaCurva");
        }

        protected void btnInventarioSillaForrada_Click(object sender, EventArgs e)
        {
            LlamarInventario("SillaForrada");
        }

        protected void btnInventarioTripode_Click(object sender, EventArgs e)
        {
            LlamarInventario("Tripode");
        }

        protected void btnInventarioVancouver_Click(object sender, EventArgs e)
        {
            LlamarInventario("Vancouver");
        }

        protected void btnInventarioValeria_Click(object sender, EventArgs e)
        {
            LlamarInventario("Valeria");
        }

        protected void btnInventarioVictoria_Click(object sender, EventArgs e)
        {
            LlamarInventario("Victoria");
        }

        protected void btnInventarioViena_Click(object sender, EventArgs e)
        {
            LlamarInventario("Viena");
        }

        protected void btnInventarioVirginia_Click(object sender, EventArgs e)
        {
            LlamarInventario("Virginia");
        }

        protected void btnInventarioWestlake_Click(object sender, EventArgs e)
        {
            LlamarInventario("Westlake");
        }

        protected void btnInventarioWoodmont_Click(object sender, EventArgs e)
        {
            LlamarInventario("Woodmont");
        }

        protected void lkInventarioIniciar_Click(object sender, EventArgs e)
        {
            try
            {
                InventarioCancelarEdicion();

                DataSet ds = new DataSet();
                CargarInventario("INICIAR", "LIS", false, ds, "", "", "", "");
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventarioGrabar.Visible = true;
                lbErrorInventarioGrabar.Text = string.Format("Error al cargar el inventario {0} {1}", ex.Message, m);
            }
        }

        protected void lkInventarioAnterior_Click(object sender, EventArgs e)
        {
            try
            {
                InventarioCancelarEdicion();

                DataSet ds = new DataSet();
                CargarInventario("ANTERIOR", "LIS", false, ds, "", "", "", "");
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventarioGrabar.Visible = true;
                lbErrorInventarioGrabar.Text = string.Format("Error al cargar el inventario {0} {1}", ex.Message, m);
            }
        }

        protected void lkInventarioSiguiente_Click(object sender, EventArgs e)
        {
            try
            {
                InventarioCancelarEdicion();

                DataSet ds = new DataSet();
                CargarInventario("SIGUIENTE", "LIS", false, ds, "", "", "", "");
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventarioGrabar.Visible = true;
                lbErrorInventarioGrabar.Text = string.Format("Error al cargar el inventario {0} {1}", ex.Message, m);
            }
        }

        protected void lkInventarioPaginaIr_Click(object sender, EventArgs e)
        {
            try
            {
                InventarioCancelarEdicion();

                DataSet ds = new DataSet();
                CargarInventario("PAGINA", "LIS", false, ds, "", "", "", "");
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventarioGrabar.Visible = true;
                lbErrorInventarioGrabar.Text = string.Format("Error al cargar el inventario {0} {1}", ex.Message, m);
            }
        }

        protected void txtInventarioPagina_TextChanged(object sender, EventArgs e)
        {
            try
            {
                InventarioCancelarEdicion();

                DataSet ds = new DataSet();
                CargarInventario("PAGINA", "LIS", false, ds, "", "", "", "");
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventarioGrabar.Visible = true;
                lbErrorInventarioGrabar.Text = string.Format("Error al cargar el inventario {0} {1}", ex.Message, m);
            }
        }

        protected void txtConteo_TextChanged(object sender, EventArgs e)
        {
            GrabarLineaInventario();
        }

        protected void lkInventarioBotones_Click(object sender, EventArgs e)
        {
            //if (tblInventarioBotones.Visible)
            //{
            //    lkInventarioBotones.Text = "+";
            //    tblInventarioBotones.Visible = false;
            //}
            //else
            //{
            //    lkInventarioBotones.Text = "-";
            //    tblInventarioBotones.Visible = true;
            //}
        }

        protected void txtObservacionesInventario_TextChanged(object sender, EventArgs e)
        {
            GrabarLineaInventario();
        }

        void GrabarLineaInventario()
        {
            int mRow = gridInventarioGrabar.EditIndex;
            int mNewRow = mRow + 1;

            inventarioGrabaLinea(mRow);

            if (mNewRow >= gridInventarioGrabar.Rows.Count)
            {
                DataSet ds = new DataSet();
                CargarInventario("SIGUIENTE", "LIS", false, ds, "", "", "", "");

                mNewRow = -1;
                if (gridInventarioGrabar.Rows.Count > 0) mNewRow = 0;
            }

            if (mNewRow >= 0)
            {
                inventarioIniciaEdit(mNewRow);
                Session["LineaInventario"] = mNewRow;

                TextBox txtConteo = (TextBox)gridInventarioGrabar.Rows[mNewRow].FindControl("txtConteo");
                txtConteo.Focus();
            }
        }

        protected void btnCamasAdvancedM_Click(object sender, EventArgs e)
        {
            LlamarInventario("Advanced_M");
        }

        protected void btnCamasBackcareM_Click(object sender, EventArgs e)
        {
            LlamarInventario("Backcare_M");
        }

        protected void btnCamasCarroM_Click(object sender, EventArgs e)
        {
            LlamarInventario("Carro_M");
        }

        protected void btnCamasDeluxeM_Click(object sender, EventArgs e)
        {
            LlamarInventario("Deluxe_M");
        }

        protected void btnCamasDreamSleeperM_Click(object sender, EventArgs e)
        {
            LlamarInventario("DreamSleeper_M");
        }

        protected void btnCamasEvolutionM_Click(object sender, EventArgs e)
        {
            LlamarInventario("Evolution_M");
        }

        protected void btnCamasFirmM_Click(object sender, EventArgs e)
        {
            LlamarInventario("Firm_M");
        }

        protected void btnCamasFrescoFoamM_Click(object sender, EventArgs e)
        {
            LlamarInventario("FrescoFoam_M");
        }

        protected void btnCamasLuxuriousM_Click(object sender, EventArgs e)
        {
            LlamarInventario("Luxurious_M");
        }

        protected void btnCamasPlushM_Click(object sender, EventArgs e)
        {
            LlamarInventario("Plush_M");
        }

        protected void btnCamasPureEssenceM_Click(object sender, EventArgs e)
        {
            LlamarInventario("PureEssence_M");
        }

        protected void btnCamasRechargeM_Click(object sender, EventArgs e)
        {
            LlamarInventario("Recharge_M");
        }

        protected void btnCamasSimmonsM_Click(object sender, EventArgs e)
        {
            LlamarInventario("Simmons_M");
        }

        protected void btnCamasTripleCrownM_Click(object sender, EventArgs e)
        {
            LlamarInventario("TripleCrown_M");
        }

        protected void btnCamasWonderM_Click(object sender, EventArgs e)
        {
            LlamarInventario("Wonder_M");
        }

        protected void btnCamasWorldClassM_Click(object sender, EventArgs e)
        {
            LlamarInventario("WorldClass_M");
        }

        protected void lkInventarioBases_Click(object sender, EventArgs e)
        {
            OcultarTablasInventario();
            //tblInventarioBases.Visible = true;
        }

        protected void btnBasesBackcare_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesBackcare");
        }

        protected void btnBasesDeLujo_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesDe Lujo");
        }

        protected void btnBasesDeLuxe_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesDeluxe");
        }

        protected void btnBasesDreamSleeper_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesDream Sleeper");
        }

        protected void btnBasesErgonomic_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesErgonomic");
        }

        protected void btnBasesEstelar_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesEstelar");
        }

        protected void btnBasesEvolution_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesEvolution");
        }

        protected void btnBasesFrescoFoam_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesFresco Foam");
        }

        protected void btnBasesLuxurious_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesLuxurious");
        }

        protected void btnBasesPlush_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesPlush");
        }

        protected void btnBasesPremier_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesPremier");
        }

        protected void btnBasesStandard_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesStandard");
        }

        protected void btnBasesTripleCrown_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesTriple Crown");
        }

        protected void btnBasesTrueEnergy_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesTrue Energy");
        }

        protected void btnBasesWonder_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesWonder");
        }

        protected void btnBasesBlackM_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesBlack_M");
        }

        protected void btnBasesBackcareM_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesBackcare_M");
        }

        protected void btnBasesDeLujoM_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesDe Lujo_M");
        }

        protected void btnBasesStandardM_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesStandard_M");
        }

        protected void btnBasesTrueEnergyM_Click(object sender, EventArgs e)
        {
            LlamarInventario("BasesTrue Energy_M");
        }

        protected void lkGrabarPDF_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoInventario.Text = "";
                lbErrorInventario.Text = "";

                bool fileOK = false;

                if (ArchivoInventario.HasFile)
                {
                    string fileExtension = System.IO.Path.GetExtension(ArchivoInventario.FileName).ToLower();
                    string[] allowedExtensions = { ".pdf", ".xls", ".xlsx" };
                    for (int i = 0; i < allowedExtensions.Length; i++)
                    {
                        if (fileExtension == allowedExtensions[i]) fileOK = true;
                    }
                }

                if (fileOK)
                {
                    try
                    {
                        string mBodega = Convert.ToString(Session["Tienda"]);
                        int mMesInventario = Convert.ToInt32(ViewState["MesInventario"]);
                        int mAnioInventario = Convert.ToInt32(ViewState["AnioInventario"]);

                        if (ArchivoInventario.PostedFile.FileName.Contains("xls"))
                        {
                            lbErrorInventario.Text = "Favor usar la carga de inventarios en contabilidad";
                            return;

                            //string[] mValores = ArchivoInventario.PostedFile.FileName.Split(new string[] { "_" }, StringSplitOptions.None);
                            //mBodega = mValores[0].Replace("Inventario", "");

                            //string[] mValoresFecha = mValores[1].Split(new string[] { "-" }, StringSplitOptions.None);
                            //mMesInventario = Convert.ToInt32(mValoresFecha[1]);
                            //mAnioInventario = Convert.ToInt32(mValoresFecha[2].Substring(0, 4));
                        }

                        string mMes = "Enero";

                        switch (mMesInventario)
                        {
                            case 2:
                                mMes = "Febrero";
                                break;
                            case 3:
                                mMes = "Marzo";
                                break;
                            case 4:
                                mMes = "Abril";
                                break;
                            case 5:
                                mMes = "Mayo";
                                break;
                            case 6:
                                mMes = "Junio";
                                break;
                            case 7:
                                mMes = "Julio";
                                break;
                            case 8:
                                mMes = "Agosto";
                                break;
                            case 9:
                                mMes = "Septiembre";
                                break;
                            case 10:
                                mMes = "Octubre";
                                break;
                            case 11:
                                mMes = "Noviembre";
                                break;
                            case 12:
                                mMes = "Diciembre";
                                break;
                            default:
                                break;
                        }

                        string mNombreDocumento = @"C:\reportes\Inventario" + mMes + mAnioInventario.ToString() + "_" + mBodega + ".pdf";

                        //SmtpClient smtp = new SmtpClient();
                        //MailMessage mail = new MailMessage();
                        List<string> mCuentaCorreo = new List<string>();

                        var ws = new wsPuntoVenta.wsPuntoVenta();
                        if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                        if (ArchivoInventario.PostedFile.FileName.Contains("pdf"))
                        {
                            var q = ws.DevuelveGerentes("IT");
                            for (int ii = 0; ii < q.Length; ii++)
                            {
                                mCuentaCorreo.Add(q[ii].mail);
                            }
                        }
                        else
                        {
                            lbErrorInventario.Text = "Archivo inválido";
                            return;
                        }

                        try
                        {
                            if (File.Exists(mNombreDocumento)) File.Delete(mNombreDocumento);
                            ArchivoInventario.PostedFile.SaveAs(mNombreDocumento);
                        }
                        catch
                        {
                            try
                            {
                                mNombreDocumento = mNombreDocumento.Replace(".pdf", "_1.pdf").Replace(".xls", "_1.xls");
                                if (File.Exists(mNombreDocumento)) File.Delete(mNombreDocumento);
                                ArchivoInventario.PostedFile.SaveAs(mNombreDocumento);
                            }
                            catch
                            {
                                mNombreDocumento = mNombreDocumento.Replace(".pdf", "_1.pdf").Replace(".xls", "_1.xls");
                                if (File.Exists(mNombreDocumento)) File.Delete(mNombreDocumento);
                                ArchivoInventario.PostedFile.SaveAs(mNombreDocumento);
                            }
                        }



                        StringBuilder mBody = new StringBuilder();

                        mBody.Append("<!DOCTYPE html><html lang='en-us'>");
                        mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } .style1 { color: #8C4510; background-color: #FFF7E7; font-size: xx-small; text-align: left; height:35px; } .style2 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; height:35px; } .style3 { color: #8C4510; background-color: #FFF7E7; font-size: x-small; text-align: center; height:35px; } .style4 { border: 1px solid #8C4510; } .style5 { font-weight: bold; color: #FFFFFF; background-color: #A55129; font-size: x-small; text-align: center; height:35px; } </style></head>");
                        mBody.Append("<body>");

                        mBody.Append(string.Format("<p>A continuación podrán encontrar el inventario correspondiente a {0} {1} de {2}.</p>", mMes, mAnioInventario.ToString(), mBodega));
                        mBody.Append(string.Format("<p>{0} - {1}</p>", Convert.ToString(Session["NombreVendedor"]), mBodega));
                        mBody.Append("</body>");
                        mBody.Append("</html>");

                        List<Attachment> attachments = new List<Attachment>();
                        attachments.Add(new Attachment(mNombreDocumento));
                        Mail.EnviarEmail(WebConfigurationManager.AppSettings["UsrMail"].ToString(), mCuentaCorreo, string.Format("Inventario {0} {1} - {2}", mMes, mAnioInventario.ToString(), mBodega), mBody, "Punto de Venta", null, attachments);
                        #region "Código Anterior"
                        //mail.Body = mBody.ToString();
                        //mail.Subject = string.Format("Inventario {0} {1} - {2}", mMes, mAnioInventario.ToString(), mBodega);
                        //mail.From = new MailAddress("puntodeventa@productosmultiples.com", "Punto de Venta");

                        //mail.Attachments.Add(new Attachment(mNombreDocumento));

                        //smtp.Host = WebConfigurationManager.AppSettings["MailHost"];
                        //smtp.Port = Int32.Parse(WebConfigurationManager.AppSettings["MailPort"]);

                        //smtp.EnableSsl = true;
                        //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                        //try
                        //{
                        //    smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["UsrMail"], WebConfigurationManager.AppSettings["PwdMail"]);
                        //    //smtp.Credentials = new System.Net.NetworkCredential("puntodeventa", "jh$%Pjma");
                        //    smtp.Send(mail);
                        //}
                        //catch
                        //{
                        //    //Nothing
                        //}

                        #endregion
                        if (!ArchivoInventario.PostedFile.FileName.Contains("pdf"))
                        {
                            lbErrorInventario.Text = "Archivo inválido";
                            return;

                            //DataSet ds = new DataSet();
                            //System.Data.OleDb.OleDbConnection mConexion = new System.Data.OleDb.OleDbConnection(string.Format("Provider=Microsoft.ACE.OLEDB.12.0; data source={0}; Extended Properties=Excel 8.0;", mNombreDocumento));
                            //System.Data.OleDb.OleDbDataAdapter da = new System.Data.OleDb.OleDbDataAdapter("SELECT Articulo, Descripcion, Conteo, Observaciones FROM [Sheet1$]", mConexion);

                            //da.Fill(ds);

                            //if (!ws.GrabarCargaInventarioXLS(Session["Usuario"].ToString(), mBodega, mAnioInventario.ToString(), mMesInventario.ToString(), ref mMensaje, ds))
                            //{
                            //    lbErrorInventario.Visible = true;
                            //    lbErrorInventario.Text = mMensaje;
                            //    //lbErrorInventario.Text = "Carga fallida";
                            //    return;
                            //}
                        }
                        else
                        {
                            Clases.TomaInventario mInventario = new Clases.TomaInventario();

                            mInventario.Mes = mMesInventario;
                            mInventario.Anio = mAnioInventario;
                            mInventario.Tipo = "PDF";
                            mInventario.Bodega = mBodega;
                            mInventario.Usuario = Convert.ToString(Session["Usuario"]);

                            string json = JsonConvert.SerializeObject(mInventario);
                            byte[] data = Encoding.ASCII.GetBytes(json);

                            WebRequest request = WebRequest.Create(string.Format("{0}/TomaInventario/", Convert.ToString(Session["UrlRestServices"])));

                            request.Method = "POST";
                            request.ContentType = "application/json";
                            request.ContentLength = data.Length;

                            using (var stream = request.GetRequestStream())
                            {
                                stream.Write(data, 0, data.Length);
                            }

                            var response = (HttpWebResponse)request.GetResponse();
                            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                            string mRetorna = responseString.ToString().Replace("\"", "");
                            if (mRetorna.ToLower().Contains("error"))
                            {
                                lbErrorInventario.Text = mRetorna;
                                return;
                            }

                            lbInfoInventario.Text = mRetorna;

                            //if (!ws.GrabarCargaInventarioPDF(Session["Usuario"].ToString(), mBodega, mAnioInventario.ToString(), mMesInventario.ToString(), ref mMensaje))
                            //{
                            //    lbErrorInventario.Visible = true;
                            //    lbErrorInventario.Text = mMensaje;
                            //    //lbErrorInventario.Text = "Carga fallida";
                            //    return;
                            //}
                        }
                    }
                    catch (Exception ex1)
                    {
                        lbErrorInventario.Text = ex1.Message;
                        return;
                    }
                }
                else
                {
                    lbErrorInventario.Text = "Archivo inválido";
                    return;
                }
            }
            catch (Exception ex2)
            {
                string m = "";
                try
                {
                    m = ex2.StackTrace.Substring(ex2.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex2.StackTrace.Substring(ex2.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventario.Text = string.Format("Error al cargar archivo de inventario {0} {1}", ex2.Message, m);
            }
        }

        protected void lkImprimirCierreInventario_Click(object sender, EventArgs e)
        {
            ViewState["AccionInventario"] = "I";
        }

        protected void lkInventarioPreliminar_Click(object sender, EventArgs e)
        {
            ViewState["AccionInventario"] = "P";
        }

        protected void lkInventarioExistencias_Click(object sender, EventArgs e)
        {
            ViewState["AccionInventario"] = "E";
        }

        protected void lkAsignarTransportista_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoAsignarEnvio.Text = "";
                lbErrorAsignarEnvio.Text = "";

                if (txtEnvioAsignar.Text.Trim().Length == 0)
                {
                    lbErrorAsignarEnvio.Text = "Debe ingresar el número de envío.";
                    txtEnvioAsignar.Focus();
                    return;
                }
                if (cbTransportista.SelectedValue == "0")
                {
                    lbErrorAsignarEnvio.Text = "Debe seleccionar el transportista.";
                    cbTransportista.Focus();
                    return;
                }

                string mMensaje = "";
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.GrabarTransportista(txtEnvioAsignar.Text.Trim(), cbTransportista.SelectedValue, Convert.ToString(Session["Usuario"]), ref mMensaje))
                {
                    lbErrorAsignarEnvio.Text = mMensaje;
                    return;
                }

                lbInfoAsignarEnvio.Text = mMensaje;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventario.Visible = true;
                lbErrorInventario.Text = string.Format("Error al descargar el inventario en EXCEL {0} {1}", ex.Message, m);
            }
        }

        protected void gridInventarioGeneral_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LinkButton lkArticulo = (LinkButton)gridInventarioGeneral.SelectedRow.FindControl("lkArticuloInv");
                codigoArticuloDet.Text = lkArticulo.Text;
                CargarInventarioGeneral("C");

                gridInventarioGeneral.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorInventarioGeneral.Visible = true;
                lbErrorInventarioGeneral.Text = string.Format("Error al generar el inventario {0} {1}", ex.Message, m);
            }
        }

        protected void txtClienteArmado_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtClienteArmado.Text.Trim().Length == 0)
                {
                    txtNombreClienteArmado.Text = "";
                    return;
                }

                gridClientesArmado.Visible = false;
                lbInfoRequisicionArmado.Text = "";
                lbErrorRequisicionArmado.Text = "";

                DataSet ds = new DataSet();

                string mMensaje = "";
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.ConsultaClientes(txtClienteArmado.Text, ref ds, ref mMensaje))
                {
                    lbErrorRequisicionArmado.Text = mMensaje;
                    return;
                }

                if (ds.Tables["clientes"].Rows.Count == 1)
                {
                    txtClienteArmado.Text = Convert.ToString(ds.Tables["clientes"].Rows[0]["Cliente"]);
                    txtNombreClienteArmado.Text = Convert.ToString(ds.Tables["clientes"].Rows[0]["Nombre"]);
                }
                else
                {
                    gridClientesArmado.Visible = true;
                    gridClientesArmado.DataSource = ds.Tables["clientes"];
                    gridClientesArmado.DataBind();
                    gridClientesArmado.SelectedIndex = -1;

                    lbInfoRequisicionArmado.Text = mMensaje;
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorRequisicionArmado.Text = string.Format("Error al buscar clientes {0} {1}", ex.Message, m);
            }
        }

        protected void txtArticuloArmado_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtArticuloArmado.Text.Trim().Length == 0)
                {
                    txtDescripcionArticuloArmado.Text = "";
                    return;
                }

                gridArticulosArmado.Visible = false;
                lbInfoRequisicionArmado.Text = "";
                lbErrorRequisicionArmado.Text = "";

                DataSet ds = new DataSet();

                string mMensaje = "";
                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                if (!ws.ConsultaArticulos(txtArticuloArmado.Text, ref ds, ref mMensaje))
                {
                    lbErrorRequisicionArmado.Text = mMensaje;
                    return;
                }

                if (ds.Tables["articulos"].Rows.Count == 1)
                {
                    txtArticuloArmado.Text = Convert.ToString(ds.Tables["articulos"].Rows[0]["Articulo"]);
                    txtDescripcionArticuloArmado.Text = Convert.ToString(ds.Tables["articulos"].Rows[0]["Descripcion"]);

                    DataSet dsArticulosArmado = new DataSet();
                    if (ViewState["ArticulosArmado"] == null)
                    {
                        DataTable dt = new DataTable("articulos");
                        dt = ds.Tables["articulos"].Copy();

                        dsArticulosArmado.Tables.Add(dt);
                        dsArticulosArmado.Tables["articulos"].Clear();
                        dsArticulosArmado.Tables["articulos"].Columns.Add("Tienda");
                    }
                    else
                    {
                        dsArticulosArmado = (DataSet)ViewState["ArticulosArmado"];
                    }

                    DataRow row = dsArticulosArmado.Tables["articulos"].NewRow();
                    row["Articulo"] = Convert.ToString(ds.Tables["articulos"].Rows[0]["Articulo"]);
                    row["Descripcion"] = Convert.ToString(ds.Tables["articulos"].Rows[0]["Descripcion"]);
                    row["Tienda"] = cbTiendaArmado.SelectedValue.ToString();
                    dsArticulosArmado.Tables["articulos"].Rows.Add(row);

                    ViewState["ArticulosArmado"] = dsArticulosArmado;

                    gridArticulosRequisicion.Visible = true;
                    gridArticulosRequisicion.DataSource = dsArticulosArmado.Tables["articulos"];
                    gridArticulosRequisicion.DataBind();
                    gridArticulosRequisicion.SelectedIndex = -1;

                    txtArticuloArmado.Text = "";
                    txtDescripcionArticuloArmado.Text = "";
                }
                else
                {
                    gridArticulosArmado.Visible = true;
                    gridArticulosArmado.DataSource = ds.Tables["articulos"];
                    gridArticulosArmado.DataBind();
                    gridArticulosArmado.SelectedIndex = -1;

                    lbInfoRequisicionArmado.Text = mMensaje;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorRequisicionArmado.Text = string.Format("Error al buscar clientes {0} {1}", ex.Message, m);
            }
        }

        protected void gridClientesArmado_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridClientesArmado_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lbInfoRequisicionArmado.Text = "";
                lbErrorRequisicionArmado.Text = "";

                Label lbCliente = (Label)gridClientesArmado.SelectedRow.FindControl("lbCliente");
                Label lbNombre = (Label)gridClientesArmado.SelectedRow.FindControl("lbNombre");

                txtClienteArmado.Text = lbCliente.Text;
                txtNombreClienteArmado.Text = lbNombre.Text;

                gridClientesArmado.Visible = false;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorRequisicionArmado.Text = string.Format("Error al seleccionar el cliente {0} {1}", ex.Message, m);
            }
        }

        protected void gridArticulosRequisicion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridArticulosRequisicion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lbInfoRequisicionArmado.Text = "";
                lbErrorRequisicionArmado.Text = "";
               
                DataSet dsArticulosArmado = new DataSet();
                DataSet dsArticulosArmado2 = new DataSet();

                dsArticulosArmado = (DataSet)ViewState["ArticulosArmado"];

                DataTable dt = new DataTable("articulos");
                dt = dsArticulosArmado.Tables["articulos"].Copy();
                dsArticulosArmado2.Tables.Add(dt);

                dsArticulosArmado.Tables["articulos"].Clear();

                for (int ii = 0; ii < dsArticulosArmado2.Tables["articulos"].Rows.Count; ii++)
                {
                    if (ii != gridArticulosRequisicion.SelectedRow.RowIndex)
                    {
                        DataRow row = dsArticulosArmado.Tables["articulos"].NewRow();
                        row["Articulo"] = Convert.ToString(dsArticulosArmado2.Tables["articulos"].Rows[ii]["Articulo"]);
                        row["Descripcion"] = Convert.ToString(dsArticulosArmado2.Tables["articulos"].Rows[ii]["Descripcion"]);
                        row["Tienda"] = Convert.ToString(dsArticulosArmado2.Tables["articulos"].Rows[ii]["Tienda"]);
                        dsArticulosArmado.Tables["articulos"].Rows.Add(row);
                    }
                }

                ViewState["ArticulosArmado"] = dsArticulosArmado;

                gridArticulosRequisicion.Visible = true;
                gridArticulosRequisicion.DataSource = dsArticulosArmado.Tables["articulos"];
                gridArticulosRequisicion.DataBind();
                gridArticulosRequisicion.SelectedIndex = -1;

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorRequisicionArmado.Text = string.Format("Error al eliminar el artículo {0} {1}", ex.Message, m);
            }

        }

        protected void gridArticulosArmado_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void gridArticulosArmado_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lbInfoRequisicionArmado.Text = "";
                lbErrorRequisicionArmado.Text = "";

                Label lbArticulo = (Label)gridArticulosArmado.SelectedRow.FindControl("lbArticulo");
                Label lbDescripcion = (Label)gridArticulosArmado.SelectedRow.FindControl("lbDescripcion");

                DataSet dsArticulosArmado = new DataSet();
                if (ViewState["ArticulosArmado"] == null)
                {
                    string mMensaje = "";
                    var ws = new wsPuntoVenta.wsPuntoVenta();
                    if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                    ws.ConsultaArticulos(lbArticulo.Text, ref dsArticulosArmado, ref mMensaje);
                    dsArticulosArmado.Tables["articulos"].Columns.Add("Tienda");
                    dsArticulosArmado.Tables["articulos"].Clear();
                }
                else
                {
                    dsArticulosArmado = (DataSet)ViewState["ArticulosArmado"];
                }

                DataRow row = dsArticulosArmado.Tables["articulos"].NewRow();
                row["Articulo"] = lbArticulo.Text;
                row["Descripcion"] = lbDescripcion.Text;
                row["Tienda"] = cbTiendaArmado.SelectedValue.ToString();
                dsArticulosArmado.Tables["articulos"].Rows.Add(row);

                ViewState["ArticulosArmado"] = dsArticulosArmado;

                gridArticulosRequisicion.Visible = true;
                gridArticulosRequisicion.DataSource = dsArticulosArmado.Tables["articulos"];
                gridArticulosRequisicion.DataBind();
                gridArticulosRequisicion.SelectedIndex = -1;

                txtArticuloArmado.Text = "";
                txtDescripcionArticuloArmado.Text = "";
                gridArticulosArmado.Visible = false;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorRequisicionArmado.Text = string.Format("Error al seleccionar el artículo {0} {1}", ex.Message, m);
            }
        }

        protected void lkEnviarRequiscionArmado_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoRequisicionArmado.Text = "";
                lbErrorRequisicionArmado.Text = "";

                if (cbTipoArmado.SelectedValue == "--")
                {
                    lbErrorRequisicionArmado.Text = "Debe seleccionar el tipo de armado";
                    cbTipoArmado.Focus();
                    return;
                }

                DateTime mFechaArmado = DateTime.Now.Date;

                DateTime mFechaArmadoValidar = DateTime.Now.Date.AddDays(2);
                if (mFechaArmadoValidar.DayOfWeek == DayOfWeek.Sunday) mFechaArmadoValidar = mFechaArmadoValidar.AddDays(1);

                try
                {
                    if (txtAnioArmado.Text.Trim().Length == 2) txtAnioArmado.Text = string.Format("20{0}", txtAnioArmado.Text);
                    mFechaArmado = new DateTime(Convert.ToInt32(txtAnioArmado.Text), Convert.ToInt32(cbMesArmado.SelectedValue), Convert.ToInt32(cbDiaArmado.SelectedValue));
                }
                catch
                {
                    lbErrorRequisicionArmado.Text = "Debe ingresar una fecha de armado válida";
                    cbDiaArmado.Focus();
                    return;
                }

                if (mFechaArmado < mFechaArmadoValidar)
                {
                    lbErrorRequisicionArmado.Text = "La fecha de armado ingresada es inválida";
                    cbDiaArmado.Focus();
                    return;
                }

                txtObsArmado.Text = txtObsArmado.Text.Replace(">", "").Replace("<", "").Replace("&", "");

                if (txtObsArmado.Text.Trim().Length == 0)
                {
                    lbErrorRequisicionArmado.Text = "Debe ingresar las observaciones del armado/servicio";
                    txtObsArmado.Focus();
                    return;
                }

                if (cbTipoArmado.SelectedValue == "S" || cbTipoArmado.SelectedValue == "T" || cbTipoArmado.SelectedValue == "J")
                {
                    if (txtObsArmado.Text.Trim().Length <= 30)
                    {
                        lbErrorRequisicionArmado.Text = "Debe ser explícito al ingresar el servicio que necesita el artículo.";
                        txtObsArmado.Focus();
                        return;
                    }
                }

                if (ViewState["ArticulosArmado"] == null)
                {
                    lbErrorRequisicionArmado.Text = "Debe ingresar al menos un artículo";
                    txtArticuloArmado.Focus();
                    return;
                }

                DataSet dsArticulosArmado = new DataSet();
                dsArticulosArmado = (DataSet)ViewState["ArticulosArmado"];

                if (dsArticulosArmado.Tables["articulos"].Rows.Count == 0)
                {
                    lbErrorRequisicionArmado.Text = "Debe ingresar al menos un artículo";
                    txtArticuloArmado.Focus();
                    return;
                }

                string mRetorna = "";
                foreach (DataRow drArmado in dsArticulosArmado.Tables["articulos"].DefaultView.ToTable(true, "Tienda").Rows)
                {
                    string tipo = cbTipoArmado.SelectedValue;
                    string codigoTienda = tipo == "D" ? drArmado["Tienda"].ToString() : cbTiendaArmado.SelectedValue;
                    Desarmado mDesarmado = new Desarmado();

                    mDesarmado.Tienda = codigoTienda;
                    mDesarmado.Vendedor = Convert.ToString(Session["Vendedor"]);
                    mDesarmado.Usuario = Convert.ToString(Session["usuario"]);
                    mDesarmado.FechaDesarmado = mFechaArmado;
                    mDesarmado.Observaciones = txtObsArmado.Text.Trim();
                    mDesarmado.Cliente = txtClienteArmado.Text;
                    mDesarmado.Tipo = tipo;
                    mDesarmado.Factura = txtFacturaArmado.Text;
                    mDesarmado.Pedido = "";

                    DataRow[] draArticulos = dsArticulosArmado.Tables["articulos"].Select("Tienda='" + codigoTienda  + "'");
                    foreach (DataRow drArticulo in draArticulos)
                    {
                        Articulos mArticulos = new Articulos();
                        mArticulos.Articulo = drArticulo["Articulo"].ToString();
                        mArticulos.Descripcion = drArticulo["Descripcion"].ToString();
                        mDesarmado.Articulo.Add(mArticulos);
                    }
                    
                    string json = JsonConvert.SerializeObject(mDesarmado);
                    byte[] data = Encoding.ASCII.GetBytes(json);

                    //Si es un desarmado en tienda debe pasar por autorización
                    if (mDesarmado.Tipo == "D")
                    {
                        WebRequest request2 = WebRequest.Create(string.Format("{0}/SolicitudDesarmado/", Convert.ToString(Session["UrlRestServices"])));

                        request2.Method = "POST";
                        request2.ContentType = "application/json";
                        request2.ContentLength = data.Length;

                        using (var stream = request2.GetRequestStream())
                        {
                            stream.Write(data, 0, data.Length);
                        }

                        var response2 = (HttpWebResponse)request2.GetResponse();
                        var responseString2 = new StreamReader(response2.GetResponseStream()).ReadToEnd();

                        string mMensaje = responseString2.ToString().Replace("\"", "");
                        if (mMensaje.ToLower().Contains("error"))
                        {
                            lbErrorRequisicionArmado.Text = mMensaje;
                            return;
                        }
                    }
                    else {
                        WebRequest request = WebRequest.Create(string.Format("{0}/SolicitudDesarmado/", Convert.ToString(Session["UrlRestServices"])));

                        request.Method = "PUT";
                        request.ContentType = "application/json";
                        request.ContentLength = data.Length;

                        using (var stream = request.GetRequestStream())
                        {
                            stream.Write(data, 0, data.Length);
                        }

                        var response = (HttpWebResponse)request.GetResponse();
                        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                        mRetorna = responseString.ToString().Replace("\"", "");
                        if (mRetorna.ToLower().Contains("error"))
                        {
                            lbErrorRequisicionArmado.Text = mRetorna;
                            return;
                        }
                    }
                }

                txtObsArmado.Text = "";
                txtClienteArmado.Text = "";
                txtFacturaArmado.Text = "";

                dsArticulosArmado.Tables["articulos"].Clear();
                ViewState["ArticulosArmado"] = dsArticulosArmado;

                limpiarRequisicion(mFechaArmadoValidar);
                lbInfoRequisicionArmado.Text = mRetorna;
            }
            catch (Exception ex)
            {
                lbErrorRequisicionArmado.Text = CatchClass.ExMessage(ex, "utilitarios", "lkEnviarRequisicionArmado");
            }
        }

        void limpiarRequisicion(DateTime fechaArmado)
        {
            txtObsArmado.Text = "";
            txtClienteArmado.Text = "";
            txtNombreClienteArmado.Text = "";
            txtArticuloArmado.Text = "";
            txtDescripcionArticuloArmado.Text = "";

            cbTipoArmado.SelectedValue = "--";
            cbDiaArmado.SelectedValue = fechaArmado.Day.ToString();
            cbMesArmado.SelectedValue = fechaArmado.Month.ToString();
            txtAnioArmado.Text = fechaArmado.Year.ToString();

            gridArticulosRequisicion.Visible = false;
            gridArticulosArmado.Visible = false;
            gridClientesArmado.Visible = false;
        }

        protected void lkValesFactura_Click(object sender, EventArgs e)
        {
            try
            {
                lbErrorValeFactura.Text = "";

                var ws = new wsPuntoVenta.wsPuntoVenta();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                string mMensaje = "";
                string mFactura = txtFacturaVale.Text.Trim().ToUpper();

                var q = ws.DevuelveCertificados(mFactura, ref mMensaje);

                if (mMensaje.Trim().Length > 0)
                {
                    lbErrorValeFactura.Text = mMensaje;
                    return;
                }

                dsPuntoVenta ds = new dsPuntoVenta();

                foreach (var item in q)
                {
                    DataRow mRow = ds.Certificados.NewRow();
                    mRow["Vale"] = item.Vale;
                    mRow["Beneficiario"] = item.Beneficiario;
                    mRow["Comprador"] = item.Comprador;
                    mRow["FechaCompra"] = item.FechaCompra;
                    mRow["FechaVence"] = item.FechaVence;
                    mRow["Valor"] = item.Valor;
                    ds.Certificados.Rows.Add(mRow);
                }

                string mNombreDocumento = string.Format("ValeFactura_{0}.pdf", mFactura);

                using (ReportDocument reporte = new ReportDocument())
                {
                    string mFormato = "rptCertificado.rpt";

                    string p = string.Format("{0}reportes/{1}", Request.PhysicalApplicationPath, mFormato);
                    reporte.Load(p);

                    if (!Directory.Exists(@"C:\reportes")) Directory.CreateDirectory(@"C:\reportes");
                    if (File.Exists(@"C:\reportes\" + mNombreDocumento)) File.Delete(@"C:\reportes\" + mNombreDocumento);

                    reporte.SetDataSource(ds);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);
                }

                Response.Clear();
                Response.ContentType = "application/pdf";

                Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                Response.WriteFile(@"C:\reportes\" + mNombreDocumento);

                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                lbErrorValeFactura.Text = CatchClass.ExMessage(ex, "utilitarios", "lkValesFactura_Click");
            }
        }

        protected void cbTipoArmado_SelectedIndexChanged(object sender, EventArgs e)
        {
            gridArticulosRequisicion.Columns[1].Visible = true;

            if (gridArticulosRequisicion.Rows.Count > 0)
            {
                DataSet ds = (DataSet)ViewState["ArticulosArmado"];
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row["Tienda"] = cbTiendaArmado.SelectedValue;
                }

                if (((DropDownList)sender).SelectedValue == "D")
                {
                    foreach (GridViewRow row in gridArticulosRequisicion.Rows)
                    {
                        row.Cells[1].Text = cbTiendaArmado.SelectedValue;
                    }
                }
            }

            if (((DropDownList)sender).SelectedValue != "D")
            {
                gridArticulosRequisicion.Columns[1].Visible = false;
            }
        }
    }
}
