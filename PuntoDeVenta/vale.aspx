﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="vale.aspx.cs" Inherits="PuntoDeVenta.vale" MaintainScrollPositionOnPostback="true" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:LinkButton ID="lkImprimir" runat="server" onclick="lkImprimir_Click" 
            ToolTip="Haga clic aquí para exportar a PDF el vale y luego imprimirlo">Imprimir</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;<asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
    
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
            AutoDataBind="true" DisplayStatusbar="False" DisplayToolbar="False" 
            EnableDatabaseLogonPrompt="False" EnableDrillDown="False" 
            EnableParameterPrompt="False" EnableTheming="False" EnableToolTips="False" 
            HasCrystalLogo="False" HasDrilldownTabs="False" HasDrillUpButton="False" 
            HasExportButton="False" HasGotoPageButton="False" 
            HasPageNavigationButtons="False" HasPrintButton="False" HasRefreshButton="True" 
            HasSearchButton="False" HasToggleGroupTreeButton="False" 
            HasToggleParameterPanelButton="False" HasZoomFactorList="False" 
            ToolPanelView="None" />    
    </div>
    </form>
</body>
</html>
