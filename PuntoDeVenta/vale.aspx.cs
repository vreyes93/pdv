﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;

namespace PuntoDeVenta
{
    public partial class vale : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.LogActivity();
            if (!IsPostBack) CargarVale(false);
        }

        void CargarVale(bool exportar)
        {
            string mVale = Request.QueryString["vale"];

            var ws = new wsPuntoVenta.wsPuntoVenta();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.DevuelveValeFechaVenceNombre(mVale);

            using (ReportDocument reporte = new ReportDocument())
            {
                string p = (Request.PhysicalApplicationPath + "reportes/rptVale.rpt");
                reporte.Load(p);

                reporte.SetParameterValue("Vale", mVale);
                reporte.SetParameterValue("FechaVence", q[0].FechaVence);
                reporte.SetParameterValue("ClienteNombre", q[0].Nombre);
                this.CrystalReportViewer1.ReportSource = reporte;

                if (exportar)
                {
                    if (DateTime.Now.Date > q[0].FechaVence)
                    {
                        lbError.Text = "Este vale está vencido, no es posible imprimirlo.";
                        return;
                    }

                    string mNombreDocumento = string.Format("Vale_{0}", mVale);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                    Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                    Response.End();
                }
            }
        }

        protected void lkImprimir_Click(object sender, EventArgs e)
        {
            CargarVale(true);
        }

    }

}