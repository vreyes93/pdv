﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="administracion.aspx.cs" Inherits="PuntoDeVenta.administracion" MaintainScrollPositionOnPostback="true" %>
<%@ Register assembly="DevExpress.Web.v16.1, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<%@ Register Src="ToolbarExport.ascx" TagName="ToolbarExport" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #middle {
            vertical-align: middle;
        }
        a:-webkit-any-link{
            text-decoration: none;
        }        
    </style>

    <script type="text/javascript">
        function OnBatchEditEndEditing(s, e) {
            setTimeout(function () {
                s.UpdateEdit();
            }, 0);
        }
        function HidePopup() {
            popupArticulos.Hide();
        }
        function onDescriptionClick(s, e, key) {
            txtLinea.SetText(key);
            popupArticulos.Show();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>&nbsp;&nbsp;Opciones de Administración</h3>
    <div class="content2">
        <div class="clientes2">

            <ul runat="server" visible="false">
                <li>
                    
                    <table >
                        <tr>
                            <td colspan="3">
                                <a>Actualizar despachos</a></td>
                            <td colspan="3" style="text-align: right">
                                <asp:Label ID="lbErrorTransportista" runat="server" Font-Bold="True" ForeColor="Red" ></asp:Label>
                                <asp:Label ID="lbInfoTransportista" runat="server" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                Rango de fechas de entrega:</td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaInicialT" runat="server" Theme="SoftOrange" 
                                    CssClass="DateEditStyle" Width="140px" TabIndex="10">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaFinalT" runat="server" Theme="SoftOrange"
                                    CssClass="DateEditStyle" Width="140px" TabIndex="20">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkFechasTransporte" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar envíos según la fecha de entrega de los mismos" 
                                    onclick="lkSeleccionarFechasT_Click" TabIndex="30">Por fechas de entrega</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Tienda que desea consultar:</td>
                            <td>
                                <dx:ASPxComboBox ID="tiendaTransporte" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle" 
                                    ValueType="System.String" Width="75px" DropDownStyle="DropDownList" 
                                    DropDownWidth="40" DropDownRows="25" TabIndex="40">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkSeleccionarTiendaT" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar documentos según la tienda seleccionada" 
                                    onclick="lkSeleccionarTiendaT_Click" TabIndex="50">Seleccionar tienda</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Transportista que lo llevaba:</td>
                            <td colspan="2">
                                <dx:ASPxComboBox ID="transportistaT" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle" 
                                    ValueType="System.String" Width="295px" DropDownStyle="DropDownList" 
                                    DropDownWidth="40" DropDownRows="25" TabIndex="60">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkTransportistaT" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar envíos según el transportista y las fechas seleccionadas" 
                                    onclick="lkTransportistaT_Click" TabIndex="70">Por transportista</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                </td>
                            <td colspan="2" style="text-align: center">
                                <asp:LinkButton ID="lkActualizarTransportista" runat="server" 
                                    ToolTip="Haga clic aquí para grabar los cambios en los transportistas y aceptación de clientes" 
                                    onclick="lkActualizarTransportista_Click" TabIndex="80">Grabar</asp:LinkButton>
                            </td>
                            <td>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="6">

                                <dx:ASPxGridView ID="gridDespachos" EnableTheming="True" Theme="SoftOrange" visible="false"
                                    runat="server" KeyFieldName="Despacho" AutoGenerateColumns="False"
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" 
                                    onbatchupdate="gridDespachos_BatchUpdate" 
                                    onrowupdating="gridDespachos_RowUpdating" 
                                    oncommandbuttoninitialize="gridDespachos_CommandButtonInitialize" 
                                    oncelleditorinitialize="gridDespachos_CellEditorInitialize" 
                                    onhtmldatacellprepared="gridDespachos_HtmlDataCellPrepared" 
                                    onhtmlrowprepared="gridDespachos_HtmlRowPrepared" TabIndex="90" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                        <GroupRow Font-Bold="true" />
                                    </Styles>
                                        <SettingsBehavior EnableRowHotTrack="True" AllowSort="false"></SettingsBehavior>
                                        <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                        CommandBatchEditUpdate="Aplicar cambios"
                                        ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                        CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                        SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Despacho" ReadOnly="true" Caption="Despacho" Visible="true" Width="105px">
                                            <EditFormSettings Visible="false"/>
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataDateColumn FieldName="Fecha" ReadOnly="true" Caption="Fecha" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaEntrega" ReadOnly="false" Caption="Fecha Entrega" Visible="true" Width="80px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="Transportista" ReadOnly="false" Caption="Transportista" Visible="true" Width="270px" >
                                            <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="300" DropDownRows="25" ValueField="Transportista" ValueType="System.Int32" TextField="Nombre"/>
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Left" >
                                                <Border BorderStyle="None"></Border>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Articulos" ReadOnly="false" Caption="Artículos" Visible="true" Width="40px" ToolTip="Ingrese aquí el número total de artículos válidos de cada envío">
                                            <PropertiesSpinEdit MinValue="0" MaxValue="100"></PropertiesSpinEdit>
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataCheckColumn FieldName="Cliente" ReadOnly="false" Caption="Cliente" Visible="true" Width="40px" ToolTip="Marque esta casilla si el cliente confirmó de recibida su mercadería">
                                            <PropertiesCheckEdit ValueType="System.String" ValueChecked="S" ValueUnchecked="N"></PropertiesCheckEdit>
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataTextColumn FieldName="NombreCliente" ReadOnly="true" Caption="Nombre Cliente" Visible="true" Width="365px">
                                            <EditFormSettings Visible="false"/>
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                            <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="false" Width="40px">
                                            <EditFormSettings Visible="false"/>
                                            <HeaderStyle BackColor="#ff8a3f"  />
                                        </dx:GridViewDataColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsEditing Mode="Batch" />
                                    <ClientSideEvents BatchEditEndEditing="OnBatchEditEndEditing" />
                                    <SettingsBehavior EnableRowHotTrack="true" />
                                    <SettingsPager PageSize="20" Visible="False">
                                    </SettingsPager>
                                    <SettingsText SearchPanelEditorNullText="Ingrese aquí su criterio de búsqueda" CommandBatchEditUpdate="Aplicar cambios" CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"/>
                                    <Border BorderStyle="None"></Border>
                                </dx:ASPxGridView>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                </td>
                            <td>
                            </td>
                            <td>
                                </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        </table>
                    
                </li>
            </ul>

            <ul runat="server" visible="false">
                <li>
                    
                    <table class="dxflInternalEditorTable_SoftOrange">
                        <tr>
                            <td colspan="3">
                                <a>Modificar facturas para comisiones</a></td>
                            <td colspan="3" style="text-align: right">
                                <asp:Label ID="lbErrorModificaFactura" runat="server" Font-Bold="True" 
                                    ForeColor="Red" ></asp:Label>
                                <asp:Label ID="lbInfoModificaFactura" runat="server" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                No. de factura:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtFacturaModifica" runat="server" Width="110px" 
                                    CssClass="textBoxStyle" TabIndex="110" 
                                    ToolTip="Ingrese el número de factura" AutoPostBack="True" 
                                    ontextchanged="txtFacturaModifica_TextChanged" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                Tipo de excepción:</td>
                            <td>
                                <dx:ASPxComboBox ID="tipoExcepcion" runat="server" Theme="SoftOrange" 
                                    CssClass="ComboBoxStyle" Width="200px" 
                                    DropDownWidth="40px" DropDownRows="25" TabIndex="120">
                                    <Items>
                                        <dx:ListEditItem Text="Modificar monto a comisionar" Value="M" />
                                        <dx:ListEditItem Text="No pagar comisión" Value="N" />
                                        <dx:ListEditItem Text="Eliminar excepción" Value="E" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Monto factura:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtMontoFactura" runat="server" Width="110px" 
                                    CssClass="textBoxStyle" TabIndex="9999" 
                                    ToolTip="Este es el monto de la factura con IVA incluído" ReadOnly="True" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                Monto a comisionar:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtMontoComision" runat="server" Width="200px" 
                                    CssClass="textBoxStyle" TabIndex="140" 
                                    ToolTip="Ingrese el a comisionar con IVA incluído" >
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td style="text-align: center">
                                <asp:LinkButton ID="lkGrabarModificaFactura" runat="server" 
                                    ToolTip="Haga clic aquí para grabar los cambios en la factura" 
                                    onclick="lkGrabarModificaFactura_Click" TabIndex="150">Grabar</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                    
                </li>
            </ul>

            <ul>
                <li>
                    
                    <table>
                        <tr>
                            <td colspan="2">
                                <a>Comisiones de vendedores</a></td>
                            <td colspan="4" style="text-align: right">
                                <asp:Label ID="lbErrorComisiones" runat="server" Font-Bold="True" 
                                    ForeColor="Red" ></asp:Label>
                                <asp:Label ID="lbInfoComisiones" runat="server" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>
                                Rango de fechas:</td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaInicialComisiones" runat="server" Theme="SoftOrange" 
                                    CssClass="DateEditStyle" Width="140px" TabIndex="210">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="fechaFinalComisiones" runat="server" Theme="SoftOrange"
                                    CssClass="DateEditStyle" Width="140px" TabIndex="220">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                        <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkFechasComisiones" runat="server" 
                                    
                                    ToolTip="Haga clic aquí para seleccionar envíos según la fecha de entrega de los mismos" 
                                    TabIndex="230" onclick="lkFechasComisiones_Click">Por fechas de entrega</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Tienda consultar:</td>
                            <td>
                                <dx:ASPxComboBox ID="tiendaComisiones" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle" 
                                    ValueType="System.String" Width="75px" DropDownStyle="DropDownList" 
                                    DropDownWidth="40" DropDownRows="25" TabIndex="240" AutoPostBack="True" 
                                    onselectedindexchanged="tiendaComisiones_SelectedIndexChanged">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkSeleccionarTiendaComisiones" runat="server" 
                                    ToolTip="Haga clic aquí para seleccionar documentos según la tienda seleccionada" 
                                    onclick="lkSeleccionarTiendaComisiones_Click" TabIndex="250">Seleccionar tienda</asp:LinkButton>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkCalcularComisiones" runat="server" 
                                    ToolTip="Haga clic aquí para calcular nuevamente las comisiones previamente grabadas" 
                                    onclick="lkCalcularComisiones_Click" TabIndex="255">Calcular</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Vendedor:</td>
                            <td colspan="2">
                                <dx:ASPxComboBox ID="vendedorComisiones" runat="server" Theme="SoftOrange" CssClass="ComboBoxStyle" 
                                    ValueType="System.String" Width="295px" DropDownStyle="DropDownList" 
                                    DropDownWidth="40" DropDownRows="25" TabIndex="260">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <asp:LinkButton ID="lkVendedorComisiones" runat="server" 
                                    
                                    ToolTip="Haga clic aquí para seleccionar las comisiones del vendedor seleccionado" 
                                    TabIndex="270" onclick="lkVendedorComisiones_Click">Por vendedor</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td colspan="2" style="text-align: center">
                                <asp:LinkButton ID="lkGrabarComisiones" runat="server" 
                                    ToolTip="Haga clic aquí para grabar el cierre de comisiones" 
                                    onclick="lkGrabarComisiones_Click" TabIndex="275">Grabar</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td colspan="2" style="text-align: center">
                                <dx:ToolbarExport runat="server" ID="ToolbarExport" ExportItemTypes="Com,Fac,Exc,Anu,Pend,All" OnItemClick="ToolbarExport_ItemClick" />
                                <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridComisiones" onrenderbrick="gridExport_RenderBrick"></dx:ASPxGridViewExporter>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridComisiones" EnableTheming="True" Theme="SoftOrange" 
                                    runat="server" KeyFieldName="Vendedor" AutoGenerateColumns="False" 
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="280" 
                                    Visible="False" onhtmlrowprepared="gridComisiones_HtmlRowPrepared" 
                                    onhtmldatacellprepared="gridComisiones_HtmlDataCellPrepared" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="true"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                    CommandBatchEditUpdate="Aplicar cambios" 
                                    ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                    CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                    SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="true" Width="75px" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreVendedor" ReadOnly="true" Caption="Nombre del vendedor" Visible="true" Width="260px" >
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" Caption="Valor" Visible="true" Width="110px" ToolTip="Valor total de las facturas" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MontoComisionable" ReadOnly="true" Caption="Comisionable" Visible="true" Width="110px" ToolTip="Monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ValorNeto" ReadOnly="true" Caption="Valor Neto" Visible="true" Width="110px" ToolTip="Valor neto del monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="true" Width="110px" ToolTip="Comisión a recibir" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Porcentaje" ReadOnly="true" Caption="Porcentaje" Visible="true" Width="75px" ToolTip="Porcentaje" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="Vendedor" ReadOnly="true" Caption="Vendedor" Visible="true" Width="50px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Titulo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Tooltip" Visible="false" Caption="Información">
                                            <CellStyle Wrap="False"></CellStyle>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="500" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
<%--                                    <TotalSummary>
                                        <dx:ASPxSummaryItem FieldName="NombreVendedor" SummaryType="Count" DisplayFormat="Total general: {0}" />
                                        <dx:ASPxSummaryItem FieldName="Valor" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="MontoComisionable" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ValorNeto" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </TotalSummary>--%>
                                    <GroupSummary>
                                        <dx:ASPxSummaryItem FieldName="NombreVendedor" ShowInGroupFooterColumn="NombreVendedor" SummaryType="Count" DisplayFormat="Cantidad vendedores: {0}" />
                                        <dx:ASPxSummaryItem FieldName="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="MontoComisionable" ShowInGroupFooterColumn="MontoComisionable" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ValorNeto" ShowInGroupFooterColumn="ValorNeto" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="Comision" ShowInGroupFooterColumn="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </GroupSummary>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridFacturas" EnableTheming="True" Theme="SoftOrange" 
                                    runat="server" KeyFieldName="Factura" AutoGenerateColumns="False" 
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="290" 
                                    Visible="False" onhtmlrowprepared="gridFacturas_HtmlRowPrepared" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="false"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                    CommandBatchEditUpdate="Aplicar cambios" 
                                    ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                    CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                    SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreVendedor" ReadOnly="true" Caption="Nombre del vendedor" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura" Visible="true" Width="270px" >
                                            <DataItemTemplate>
                                                <dx:ASPxButton ID="lkFacturaComision" EncodeHtml="false" runat="server" AutoPostBack="false" Text='<%# Bind("Factura") %>' RenderMode="Link" OnClick="lkFacturaComision_Click" ToolTip="Haga clic aquí para seleccionar la factura y modificarle la comisión"></dx:ASPxButton>
                                            </DataItemTemplate>
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" Caption="Valor" Visible="true" Width="100px" ToolTip="Valor total de las facturas" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MontoComisionable" ReadOnly="true" Caption="Comisionable" Visible="true" Width="100px" ToolTip="Monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ValorNeto" ReadOnly="true" Caption="Valor Neto" Visible="true" Width="100px" ToolTip="Valor neto del monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="true" Width="100px" ToolTip="Comisión a recibir" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Porcentaje" ReadOnly="true" Caption="Porcentaje" Visible="true" Width="60px" ToolTip="Porcentaje" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaFactura" ReadOnly="true" Caption="Fecha Factura" Visible="true" Width="70px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaEntrega" ReadOnly="true" Caption="Fecha Entrega" Visible="true" Width="70px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataColumn FieldName="Vendedor" ReadOnly="true" Caption="Vendedor" Visible="true" Width="50px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Titulo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="15000" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                    <GroupSummary>
                                        <dx:ASPxSummaryItem FieldName="Factura" ShowInGroupFooterColumn="Factura" SummaryType="Count" DisplayFormat="Cantidad facturas: {0}" />
                                        <dx:ASPxSummaryItem FieldName="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="MontoComisionable" ShowInGroupFooterColumn="MontoComisionable" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ValorNeto" ShowInGroupFooterColumn="ValorNeto" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="Comision" ShowInGroupFooterColumn="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </GroupSummary>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>    
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridExcepciones" EnableTheming="True" Theme="SoftOrange" 
                                    runat="server" KeyFieldName="Factura" AutoGenerateColumns="False" 
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="300" 
                                    Visible="False" onhtmlrowprepared="gridExcepciones_HtmlRowPrepared" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="false"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                    CommandBatchEditUpdate="Aplicar cambios" 
                                    ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                    CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                    SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreVendedor" ReadOnly="true" Caption="Nombre del vendedor" Visible="true" Width="140px" >
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura" Visible="true" Width="50px" >
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" Caption="Valor" Visible="true" Width="90px" ToolTip="Valor total de las facturas" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MontoComisionable" ReadOnly="true" Caption="Comisionable" Visible="true" Width="90px" ToolTip="Monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ValorNeto" ReadOnly="true" Caption="Valor Neto" Visible="true" Width="90px" ToolTip="Valor neto del monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="false" ToolTip="Comisión a recibir" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Porcentaje" ReadOnly="true" Caption="Porcentaje" Visible="false" ToolTip="Porcentaje" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaFactura" ReadOnly="true" Caption="Fecha Factura" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaEntrega" ReadOnly="true" Caption="Fecha Entrega" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataColumn FieldName="Vendedor" ReadOnly="true" Caption="Vendedor" Visible="true" Width="50px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Tipo" Visible="true" Width="390px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle Wrap="False"></CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Titulo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="15000" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                    <GroupSummary>
                                        <dx:ASPxSummaryItem FieldName="Factura" ShowInGroupFooterColumn="Factura" SummaryType="Count" DisplayFormat="Cantidad facturas: {0}" />
                                        <dx:ASPxSummaryItem FieldName="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="MontoComisionable" ShowInGroupFooterColumn="MontoComisionable" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ValorNeto" ShowInGroupFooterColumn="ValorNeto" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="Comision" ShowInGroupFooterColumn="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </GroupSummary>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>    
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridAnuladas" EnableTheming="True" Theme="SoftOrange" 
                                    runat="server" KeyFieldName="Factura" AutoGenerateColumns="False" 
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="310" 
                                    Visible="False" onhtmlrowprepared="gridAnuladas_HtmlRowPrepared" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="false"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                    CommandBatchEditUpdate="Aplicar cambios" 
                                    ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                    CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                    SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="Hidden" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura" Visible="true" Width="270px" >
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreVendedor" ReadOnly="true" Caption="Nombre del vendedor" Visible="true" Width="430px" >
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" Caption="Valor" Visible="true" Width="100px" ToolTip="Valor total de las facturas" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MontoComisionable" ReadOnly="true" Caption="Comisionable" Visible="false" ToolTip="Monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ValorNeto" ReadOnly="true" Caption="Valor Neto" Visible="false" ToolTip="Valor neto del monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="false" ToolTip="Comisión a recibir" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Porcentaje" ReadOnly="true" Caption="Porcentaje" Visible="false" ToolTip="Porcentaje" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaFactura" ReadOnly="true" Caption="Fecha Factura" Visible="true" Width="70px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaEntrega" ReadOnly="true" Caption="Fecha Entrega" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataColumn FieldName="Vendedor" ReadOnly="true" Caption="Vendedor" Visible="true" Width="50px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Titulo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="15000" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>    
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <dx:ASPxGridView ID="gridPendientes" EnableTheming="True" Theme="SoftOrange" 
                                    runat="server" KeyFieldName="Factura" AutoGenerateColumns="False" 
                                    Border-BorderStyle="None" Width="900px" CssClass="dxGrid" TabIndex="320" 
                                    Visible="False" onhtmlrowprepared="gridPendientes_HtmlRowPrepared" >
                                    <Styles>
                                        <StatusBar><Border BorderStyle="None" /></StatusBar>
                                        <RowHotTrack ForeColor="#e35904" BackColor="#fbd5ab" />
                                        <FocusedRow BackColor="#fcd4a9" ForeColor="#e35e04" />
                                        <AlternatingRow BackColor="#fde4cf" />
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="True" AutoExpandAllGroups="false"></SettingsBehavior>
                                    <SettingsText CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" 
                                    CommandBatchEditUpdate="Aplicar cambios" 
                                    ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?" 
                                    CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" 
                                    SearchPanelEditorNullText="Ingrese aqu&#237; su criterio de b&#250;squeda"></SettingsText>
                                    <Settings ShowStatusBar="Hidden" ShowGroupPanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowGroupFooter="VisibleIfExpanded" />
                                    <Columns>
                                        <dx:GridViewDataColumn FieldName="Tienda" ReadOnly="true" Caption="Tienda" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="NombreVendedor" ReadOnly="true" Caption="Nombre del vendedor" Visible="false" >
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="Factura" ReadOnly="true" Caption="Factura" Visible="true" Width="410px" >
                                            <DataItemTemplate>
                                                <dx:ASPxButton ID="lkFacturaPendiente" EncodeHtml="false" runat="server" AutoPostBack="false" Text='<%# Bind("Factura") %>' RenderMode="Link" OnClick="lkFacturaPendiente_Click" ToolTip="Haga clic aquí para excluir la factura de las pendientes"></dx:ASPxButton>
                                            </DataItemTemplate>
                                            <HeaderStyle BackColor="#ff8a3f" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Valor" ReadOnly="true" Caption="Valor" Visible="true" Width="100px" ToolTip="Valor total de las facturas" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MontoComisionable" ReadOnly="true" Caption="Comisionable" Visible="true" Width="100px" ToolTip="Monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ValorNeto" ReadOnly="true" Caption="Valor Neto" Visible="true" Width="100px" ToolTip="Valor neto del monto comisionable" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Comision" ReadOnly="true" Caption="Comisión" Visible="false" ToolTip="Comisión a recibir" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Porcentaje" ReadOnly="true" Caption="Porcentaje" Visible="false" ToolTip="Porcentaje" >
                                            <PropertiesTextEdit DisplayFormatString="n2" />
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Right" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaFactura" ReadOnly="true" Caption="Fecha Factura" Visible="true" Width="70px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaEntrega" ReadOnly="true" Caption="Fecha Entrega" Visible="true" Width="70px" >
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataColumn FieldName="Vendedor" ReadOnly="true" Caption="Vendedor" Visible="true" Width="50px">
                                            <HeaderStyle BackColor="#ff8a3f" HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataTextColumn FieldName="Titulo" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Styles>
                                        <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                                    </Styles>
                                    <SettingsBehavior EnableRowHotTrack="true" AllowFocusedRow="true" />
                                        <SettingsPager PageSize="15000" Visible="False">
                                    </SettingsPager>
                                    <Border BorderStyle="None"></Border>
                                    <GroupSummary>
                                        <dx:ASPxSummaryItem FieldName="Factura" ShowInGroupFooterColumn="Factura" SummaryType="Count" DisplayFormat="Cantidad facturas: {0}" />
                                        <dx:ASPxSummaryItem FieldName="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="MontoComisionable" ShowInGroupFooterColumn="MontoComisionable" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="ValorNeto" ShowInGroupFooterColumn="ValorNeto" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                        <dx:ASPxSummaryItem FieldName="Comision" ShowInGroupFooterColumn="Comision" SummaryType="Sum" DisplayFormat="{0:n2}" />
                                    </GroupSummary>
                                    <Templates>
                                        <GroupRowContent>
                                            <%# Container.GroupText%>
                                        </GroupRowContent>
                                    </Templates>
                                </dx:ASPxGridView>    
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                    
                </li>
            </ul>

        </div>
        <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server" TabIndex="8888"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" 
                        TabIndex="8888"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
