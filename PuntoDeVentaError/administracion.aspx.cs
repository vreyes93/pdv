﻿using System;
using System.Data;
using System.Text;
using System.IO;
using System.Configuration;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using System.Net.Mail;
using System.Net;
using DevExpress.Web;
using DevExpress.Web.Data;
using DevExpress.Export;
using DevExpress.XtraPrinting;
using DevExpress.Data.Filtering;

namespace PuntoDeVenta
{
    public partial class administracion : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                //var ws = new wsPuntoVenta.wsPuntoVenta();
                //if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                //var q = ws.DevuelveTransportistasTodos();
                //(gridDespachos.Columns["Transportista"] as GridViewDataComboBoxColumn).PropertiesComboBox.DataSource = q;

                DataSet dsDespachos = new DataSet();
                dsDespachos = (DataSet)Session["dsDespachos"];

                gridDespachos.DataSource = dsDespachos;
                gridDespachos.DataMember = "documentos";
                gridDespachos.DataBind();
                gridDespachos.SettingsPager.PageSize = 500;
                gridDespachos.DataColumns["NombreCliente"].Settings.AutoFilterCondition = AutoFilterCondition.Contains;

                DataSet dsComisiones = new DataSet();
                dsComisiones = (DataSet)Session["dsComisiones"];

                gridComisiones.DataSource = dsComisiones;
                gridComisiones.DataMember = "comisiones";
                gridComisiones.DataBind();
                gridComisiones.FocusedRowIndex = -1;
                gridComisiones.SettingsPager.PageSize = 500;

                gridFacturas.DataSource = dsComisiones;
                gridFacturas.DataMember = "facturas";
                gridFacturas.DataBind();
                gridFacturas.FocusedRowIndex = -1;
                gridFacturas.SettingsPager.PageSize = 15000;

                gridExcepciones.DataSource = dsComisiones;
                gridExcepciones.DataMember = "excepciones";
                gridExcepciones.DataBind();
                gridExcepciones.FocusedRowIndex = -1;
                gridExcepciones.SettingsPager.PageSize = 500;

                gridAnuladas.DataSource = dsComisiones;
                gridAnuladas.DataMember = "anuladas";
                gridAnuladas.DataBind();
                gridAnuladas.FocusedRowIndex = -1;
                gridAnuladas.SettingsPager.PageSize = 500;

                gridPendientes.DataSource = dsComisiones;
                gridPendientes.DataMember = "pendientes";
                gridPendientes.DataBind();
                gridPendientes.FocusedRowIndex = -1;
                gridPendientes.SettingsPager.PageSize = 15000;
            }
            catch
            {
                ValidarSesion();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ValidarSesion();
                ValidarPruebas();
                HookOnFocus(this.Page as Control);

                Label lbUsuario = (Label)this.Master.FindControl("lblUsuario");
                if (Convert.ToString(Session["NombreVendedor"]) == "Alerta") Session["Vendedor"] = null;
                lbUsuario.Text = string.Format("{0}  -  {1}", Convert.ToString(Session["NombreVendedor"]), Convert.ToString(Session["Tienda"]));

                HtmlGenericControl submenu = new HtmlGenericControl();
                submenu = (HtmlGenericControl)Master.FindControl("submenu");
                submenu.Visible = true;

                ValidarOpciones();

                Session["dsArticulos"] = null;
                Session["dsDespachos"] = null;
                Session["dsComisiones"] = null;

                CargarBodegas();
                CargarTransportistas();
                LimpiarControlesDespacho();
                LimpiarControlesModificaFactura();
                LimpiarControlesComisiones();

                CargarComisiones("F");
            }

            Page.ClientScript.RegisterStartupScript(
            typeof(administracion),
            "ScriptDoFocus",
            SCRIPT_DOFOCUS.Replace("REQUEST_LASTFOCUS", Request["__LASTFOCUS"]),
            true);

        }

        private const string SCRIPT_DOFOCUS = @"window.setTimeout('DoFocus()', 1); function DoFocus() {
            try {
                document.getElementById('REQUEST_LASTFOCUS').focus();
            } catch (ex) {}
        }";

        private void HookOnFocus(Control CurrentControl)
        {
            //checks if control is one of TextBox, DropDownList, ListBox or Button
            if ((CurrentControl is TextBox) ||
                (CurrentControl is DropDownList) ||
                (CurrentControl is ListBox) ||
                (CurrentControl is Button))
                //adds a script which saves active control on receiving focus 
                //in the hidden field __LASTFOCUS.
                (CurrentControl as WebControl).Attributes.Add(
                   "onfocus",
                   "try{document.getElementById('__LASTFOCUS').value=this.id} catch(e) {}");
            //checks if the control has children
            if (CurrentControl.HasControls())
                //if yes do them all recursively
                foreach (Control CurrentChildControl in CurrentControl.Controls)
                    HookOnFocus(CurrentChildControl);
        }

        void ValidarPruebas()
        {
            if (Convert.ToString(Session["Ambiente"]) == "PRU")
            {
                HtmlImage logo = new HtmlImage();
                logo = (HtmlImage)Master.FindControl("logo");
                logo.Visible = false;

                HtmlImage logoPruebas = new HtmlImage();
                logoPruebas = (HtmlImage)Master.FindControl("logoPruebas");
                logoPruebas.Visible = true;

                HtmlAnchor piePagina = new HtmlAnchor();
                piePagina = (HtmlAnchor)Master.FindControl("piePagina");
                piePagina.Visible = true;
            }
        }

        void ValidarSesion()
        {
            
        }

        void ValidarOpciones()
        {
        }

        void CargarBodegas()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("bodegas");

            ds.Tables.Add(dt);
            ds.Tables["bodegas"].Columns.Add(new DataColumn("Bodega", typeof(System.String)));

            DataRow row = ds.Tables["bodegas"].NewRow();
            row["Bodega"] = "F01";
            ds.Tables["bodegas"].Rows.Add(row);

            tiendaTransporte.DataSource = ds.Tables["bodegas"];
            tiendaTransporte.ValueField = "Bodega";
            tiendaTransporte.ValueType = typeof(System.String);
            tiendaTransporte.TextField = "Bodega";
            tiendaTransporte.DataBindItems();

            tiendaComisiones.DataSource = ds.Tables["bodegas"];
            tiendaComisiones.ValueField = "Bodega";
            tiendaComisiones.ValueType = typeof(System.String);
            tiendaComisiones.TextField = "Bodega";
            tiendaComisiones.DataBindItems();

        }

        void CargarTransportistas()
        {
        }

        void LimpiarControlesDespacho()
        {
            try
            {
                lbInfoTransportista.Text = "";
                lbErrorTransportista.Text = "";

                DateTime mFechaFinal = DateTime.Now.Date.AddDays(-1);
                DateTime mFechaInicial = mFechaFinal.AddDays(-1);

                fechaInicialT.Value = mFechaInicial;
                fechaFinalT.Value = mFechaFinal;

                tiendaTransporte.Value = "F05";
                fechaInicialT.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorTransportista.Text = string.Format("Error al inicializar los despachos. {0} {1}", ex.Message, m);
            }
        }

        void UpdateDespacho(string despacho, DateTime fechaEntrega, Int32 transportista, string cliente, Int32 articulos)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsDespachos"];

                ds.Tables["documentos"].Rows.Find(despacho)["FechaEntrega"] = fechaEntrega;
                ds.Tables["documentos"].Rows.Find(despacho)["Transportista"] = transportista;
                ds.Tables["documentos"].Rows.Find(despacho)["Cliente"] = cliente;
                ds.Tables["documentos"].Rows.Find(despacho)["Articulos"] = articulos;
                Session["dsArticulos"] = ds;

                gridDespachos.DataSource = ds;
                gridDespachos.DataMember = "documentos";
                gridDespachos.DataBind();
                gridDespachos.FocusedRowIndex = -1;
                gridDespachos.SettingsPager.PageSize = 500;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorTransportista.Text = string.Format("Error al actualizar los datos UPDATE {0} {1}", ex.Message, m);
            }
        }

        protected void gridDespachos_BatchUpdate(object sender, ASPxDataBatchUpdateEventArgs e)
        {
            foreach (var args in e.UpdateValues)
                UpdateDespacho(args.Keys["Despacho"].ToString(), Convert.ToDateTime(args.NewValues["FechaEntrega"]), Convert.ToInt32(args.NewValues["Transportista"]), args.NewValues["Cliente"].ToString(), Convert.ToInt32(args.NewValues["Articulos"]));

            e.Handled = true;
        }

        protected void gridDespachos_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {

        }

        protected void gridDespachos_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            UpdateDespacho(e.Keys["Despacho"].ToString(), Convert.ToDateTime(e.NewValues["FechaEntrega"]), Convert.ToInt32(e.NewValues["Cantidad"]), e.NewValues["Cliente"].ToString(), Convert.ToInt32(e.NewValues["Articulos"]));
            CancelEditingDespacho(e);
        }

        protected void gridDespachos_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == ColumnCommandButtonType.Update || e.ButtonType == ColumnCommandButtonType.Cancel) e.Visible = false;
        }

        protected void gridDespachos_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.Name == "Cliente") e.Cell.ToolTip = "Marque esta casilla si el cliente confirmó de recibida su mercadería";
        }

        protected void gridDespachos_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {

        }

        protected void CancelEditingDespacho(CancelEventArgs e)
        {
            ValidarSesion();
            e.Cancel = true;
            gridDespachos.CancelEdit();
        }

        protected void lkSeleccionarFechasT_Click(object sender, EventArgs e)
        {
            CargarDespachados("F");
        }

        protected void lkSeleccionarTiendaT_Click(object sender, EventArgs e)
        {
            CargarDespachados("T");
        }

        protected void lkTransportistaT_Click(object sender, EventArgs e)
        {
            CargarDespachados("R");
        }

        void CargarDespachados(string tipo)
        {
        }

        protected void lkActualizarTransportista_Click(object sender, EventArgs e)
        {
        }

        void LimpiarControlesModificaFactura()
        {
            lbErrorModificaFactura.Text = "";
            lbInfoModificaFactura.Text = "";

            txtFacturaModifica.Text = "";
            tipoExcepcion.Value = "M";
            txtMontoFactura.Text = "";
            txtMontoComision.Text = "";
        }

        protected void lkGrabarModificaFactura_Click(object sender, EventArgs e)
        {
        }

        protected void txtFacturaModifica_TextChanged(object sender, EventArgs e)
        {
        }

        void LimpiarControlesComisiones()
        {
            lbInfoComisiones.Text = "";
            lbErrorTransportista.Text = "";

            Int16 mRestarInicial = -1;
            Int16 mRestarFinal = 0;


            if (DateTime.Now.Date.Day < 12)
            {
                mRestarInicial = -2;
                mRestarFinal = -1;
            }

            DateTime mFechaInicial = DateTime.Now.Date.AddMonths(mRestarInicial);
            DateTime mFechaFinal = DateTime.Now.Date.AddMonths(mRestarFinal);

            fechaInicialComisiones.Value = new DateTime(mFechaInicial.Year, mFechaInicial.Month, 26);
            fechaFinalComisiones.Value = new DateTime(mFechaFinal.Year, mFechaFinal.Month, 25);

            //fechaInicialComisiones.Value = new DateTime(2016, 2, 19);
            //fechaFinalComisiones.Value = new DateTime(2016, 2, 19);

            tiendaComisiones.Value = "F02";
            CargarVendedoresComisiones();
        }

        protected void tiendaComisiones_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarVendedoresComisiones();
        }

        void CargarVendedoresComisiones()
        {
        }

        protected void lkFechasComisiones_Click(object sender, EventArgs e)
        {
            CargarComisiones("F");
        }

        protected void lkSeleccionarTiendaComisiones_Click(object sender, EventArgs e)
        {
            CargarComisiones("T");
        }

        protected void lkVendedorComisiones_Click(object sender, EventArgs e)
        {
            CargarComisiones("V");
        }

        void CargarComisiones(string tipo)
        {
            try
            {
                ValidarSesion();

                lbInfoComisiones.Text = "";
                lbErrorComisiones.Text = "";

                gridComisiones.Visible = false;
                gridFacturas.Visible = false;
                gridExcepciones.Visible = false;
                gridAnuladas.Visible = false;
                gridPendientes.Visible = false;

                DateTime mFechaInicial = Convert.ToDateTime(fechaInicialComisiones.Value);
                DateTime mFechaFinal = Convert.ToDateTime(fechaFinalComisiones.Value);

                DataSet ds = new DataSet(); string mMensaje = "";
                ds = LoadData(ref mMensaje);

                if (mMensaje.Trim().Length > 20)
                {
                    lbErrorComisiones.Text = mMensaje;
                    return;
                }

                if (ds.Tables["comisiones"].Rows.Count > 0)
                {
                    gridComisiones.Visible = true;
                    gridComisiones.DataSource = ds;
                    gridComisiones.DataMember = "comisiones";
                    gridComisiones.DataBind();
                    gridComisiones.FocusedRowIndex = -1;
                    gridComisiones.SettingsPager.PageSize = 500;

                    gridFacturas.Visible = true;
                    gridFacturas.DataSource = ds;
                    gridFacturas.DataMember = "facturas";
                    gridFacturas.DataBind();
                    gridFacturas.FocusedRowIndex = -1;
                    gridFacturas.SettingsPager.PageSize = 15000;

                    gridExcepciones.Visible = true;
                    gridExcepciones.DataSource = ds;
                    gridExcepciones.DataMember = "excepciones";
                    gridExcepciones.DataBind();
                    gridExcepciones.FocusedRowIndex = -1;
                    gridExcepciones.SettingsPager.PageSize = 500;

                    gridAnuladas.Visible = true;
                    gridAnuladas.DataSource = ds;
                    gridAnuladas.DataMember = "anuladas";
                    gridAnuladas.DataBind();
                    gridAnuladas.FocusedRowIndex = -1;
                    gridAnuladas.SettingsPager.PageSize = 500;

                    gridPendientes.Visible = true;
                    gridPendientes.DataSource = ds;
                    gridPendientes.DataMember = "pendientes";
                    gridPendientes.DataBind();
                    gridPendientes.FocusedRowIndex = -1;
                    gridPendientes.SettingsPager.PageSize = 15000;

                    foreach (GridViewDataColumn item in gridComisiones.GetGroupedColumns())
                        item.UnGroup();

                    foreach (GridViewDataColumn item in gridFacturas.GetGroupedColumns())
                        item.UnGroup();

                    foreach (GridViewDataColumn item in gridExcepciones.GetGroupedColumns())
                        item.UnGroup();

                    foreach (GridViewDataColumn item in gridAnuladas.GetGroupedColumns())
                        item.UnGroup();

                    foreach (GridViewDataColumn item in gridPendientes.GetGroupedColumns())
                        item.UnGroup();

                    gridComisiones.GroupBy(gridComisiones.Columns["Titulo"], 0);
                    gridComisiones.GroupBy(gridComisiones.Columns["Tienda"], 1);

                    gridFacturas.GroupBy(gridFacturas.Columns["Titulo"], 0);
                    gridFacturas.GroupBy(gridFacturas.Columns["Tienda"], 1);
                    gridFacturas.GroupBy(gridFacturas.Columns["NombreVendedor"], 2);

                    gridExcepciones.GroupBy(gridExcepciones.Columns["Titulo"], 0);
                    gridExcepciones.GroupBy(gridExcepciones.Columns["Tienda"], 1);

                    gridAnuladas.GroupBy(gridAnuladas.Columns["Titulo"], 0);
                    gridAnuladas.GroupBy(gridAnuladas.Columns["Tienda"], 1);

                    gridPendientes.GroupBy(gridPendientes.Columns["Titulo"], 0);
                    gridPendientes.GroupBy(gridPendientes.Columns["Tienda"], 1);
                    gridPendientes.GroupBy(gridPendientes.Columns["NombreVendedor"], 2);

                    if (ds.Tables["excepciones"].Rows.Count == 0) gridExcepciones.Visible = false;
                    if (ds.Tables["anuladas"].Rows.Count == 0) gridAnuladas.Visible = false;
                    if (ds.Tables["pendientes"].Rows.Count == 0) gridPendientes.Visible = false;

                    Session["dsComisiones"] = ds;
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorComisiones.Text = string.Format("Error al mostrar las comisiones. {0} {1}", ex.Message, m);
            }
        }

        DataSet LoadData(ref string mensaje)
        {
            DataSet ds = new DataSet();
            DataTable dtComisiones = new DataTable("comisiones");
            DataTable dtFacturas = new DataTable("facturas");
            DataTable dtPendientes = new DataTable("pendientes");
            DataTable dtExcepciones = new DataTable("excepciones");
            DataTable dtAnuladas = new DataTable("anuladas");       

            try
            {
                ds.Tables.Add(dtComisiones);
                ds.Tables.Add(dtFacturas);
                ds.Tables.Add(dtPendientes);
                ds.Tables.Add(dtExcepciones);
                ds.Tables.Add(dtAnuladas);

                ds.Tables["comisiones"].Columns.Add(new DataColumn("NombreVendedor", typeof(System.String)));
                ds.Tables["comisiones"].Columns.Add(new DataColumn("Valor", typeof(System.Decimal)));
                ds.Tables["comisiones"].Columns.Add(new DataColumn("MontoComisionable", typeof(System.Decimal)));
                ds.Tables["comisiones"].Columns.Add(new DataColumn("ValorNeto", typeof(System.Decimal)));
                ds.Tables["comisiones"].Columns.Add(new DataColumn("Comision", typeof(System.Decimal)));
                ds.Tables["comisiones"].Columns.Add(new DataColumn("Porcentaje", typeof(System.Decimal)));
                ds.Tables["comisiones"].Columns.Add(new DataColumn("Vendedor", typeof(System.String)));
                ds.Tables["comisiones"].Columns.Add(new DataColumn("Tienda", typeof(System.String)));
                ds.Tables["comisiones"].Columns.Add(new DataColumn("Titulo", typeof(System.String)));
                ds.Tables["comisiones"].Columns.Add(new DataColumn("Tooltip", typeof(System.String)));

                DataColumn[] dcComisiones = new DataColumn[1];
                dcComisiones[0] = ds.Tables["comisiones"].Columns["Vendedor"];
                ds.Tables["comisiones"].PrimaryKey = dcComisiones;

                ds.Tables["facturas"].Columns.Add(new DataColumn("Factura", typeof(System.String)));
                ds.Tables["facturas"].Columns.Add(new DataColumn("Valor", typeof(System.Decimal)));
                ds.Tables["facturas"].Columns.Add(new DataColumn("MontoComisionable", typeof(System.Decimal)));
                ds.Tables["facturas"].Columns.Add(new DataColumn("ValorNeto", typeof(System.Decimal)));
                ds.Tables["facturas"].Columns.Add(new DataColumn("Comision", typeof(System.Decimal)));
                ds.Tables["facturas"].Columns.Add(new DataColumn("Porcentaje", typeof(System.Decimal)));
                ds.Tables["facturas"].Columns.Add(new DataColumn("FechaFactura", typeof(System.DateTime)));
                ds.Tables["facturas"].Columns.Add(new DataColumn("FechaEntrega", typeof(System.DateTime)));
                ds.Tables["facturas"].Columns.Add(new DataColumn("Vendedor", typeof(System.String)));
                ds.Tables["facturas"].Columns.Add(new DataColumn("NombreVendedor", typeof(System.String)));
                ds.Tables["facturas"].Columns.Add(new DataColumn("Tienda", typeof(System.String)));
                ds.Tables["facturas"].Columns.Add(new DataColumn("Titulo", typeof(System.String)));

                DataColumn[] dcFacturas = new DataColumn[1];
                dcFacturas[0] = ds.Tables["facturas"].Columns["Factura"];
                ds.Tables["facturas"].PrimaryKey = dcFacturas;

                ds.Tables["pendientes"].Columns.Add(new DataColumn("Factura", typeof(System.String)));
                ds.Tables["pendientes"].Columns.Add(new DataColumn("Valor", typeof(System.Decimal)));
                ds.Tables["pendientes"].Columns.Add(new DataColumn("MontoComisionable", typeof(System.Decimal)));
                ds.Tables["pendientes"].Columns.Add(new DataColumn("ValorNeto", typeof(System.Decimal)));
                ds.Tables["pendientes"].Columns.Add(new DataColumn("Comision", typeof(System.Decimal)));
                ds.Tables["pendientes"].Columns.Add(new DataColumn("Porcentaje", typeof(System.Decimal)));
                ds.Tables["pendientes"].Columns.Add(new DataColumn("FechaFactura", typeof(System.DateTime)));
                ds.Tables["pendientes"].Columns.Add(new DataColumn("FechaEntrega", typeof(System.DateTime)));
                ds.Tables["pendientes"].Columns.Add(new DataColumn("Vendedor", typeof(System.String)));
                ds.Tables["pendientes"].Columns.Add(new DataColumn("NombreVendedor", typeof(System.String)));
                ds.Tables["pendientes"].Columns.Add(new DataColumn("Tienda", typeof(System.String)));
                ds.Tables["pendientes"].Columns.Add(new DataColumn("Titulo", typeof(System.String)));

                DataColumn[] dcPendientes = new DataColumn[1];
                dcPendientes[0] = ds.Tables["pendientes"].Columns["Factura"];
                ds.Tables["pendientes"].PrimaryKey = dcPendientes;

                ds.Tables["excepciones"].Columns.Add(new DataColumn("Factura", typeof(System.String)));
                ds.Tables["excepciones"].Columns.Add(new DataColumn("Valor", typeof(System.Decimal)));
                ds.Tables["excepciones"].Columns.Add(new DataColumn("MontoComisionable", typeof(System.Decimal)));
                ds.Tables["excepciones"].Columns.Add(new DataColumn("ValorNeto", typeof(System.Decimal)));
                ds.Tables["excepciones"].Columns.Add(new DataColumn("Comision", typeof(System.Decimal)));
                ds.Tables["excepciones"].Columns.Add(new DataColumn("Porcentaje", typeof(System.Decimal)));
                ds.Tables["excepciones"].Columns.Add(new DataColumn("FechaFactura", typeof(System.DateTime)));
                ds.Tables["excepciones"].Columns.Add(new DataColumn("FechaEntrega", typeof(System.DateTime)));
                ds.Tables["excepciones"].Columns.Add(new DataColumn("Vendedor", typeof(System.String)));
                ds.Tables["excepciones"].Columns.Add(new DataColumn("NombreVendedor", typeof(System.String)));
                ds.Tables["excepciones"].Columns.Add(new DataColumn("Tienda", typeof(System.String)));
                ds.Tables["excepciones"].Columns.Add(new DataColumn("Tipo", typeof(System.String)));
                ds.Tables["excepciones"].Columns.Add(new DataColumn("Titulo", typeof(System.String)));

                DataColumn[] dcExcepciones = new DataColumn[1];
                dcExcepciones[0] = ds.Tables["excepciones"].Columns["Factura"];
                ds.Tables["excepciones"].PrimaryKey = dcExcepciones;

                ds.Tables["anuladas"].Columns.Add(new DataColumn("Factura", typeof(System.String)));
                ds.Tables["anuladas"].Columns.Add(new DataColumn("Valor", typeof(System.Decimal)));
                ds.Tables["anuladas"].Columns.Add(new DataColumn("MontoComisionable", typeof(System.Decimal)));
                ds.Tables["anuladas"].Columns.Add(new DataColumn("ValorNeto", typeof(System.Decimal)));
                ds.Tables["anuladas"].Columns.Add(new DataColumn("Comision", typeof(System.Decimal)));
                ds.Tables["anuladas"].Columns.Add(new DataColumn("Porcentaje", typeof(System.Decimal)));
                ds.Tables["anuladas"].Columns.Add(new DataColumn("FechaFactura", typeof(System.DateTime)));
                ds.Tables["anuladas"].Columns.Add(new DataColumn("FechaEntrega", typeof(System.DateTime)));
                ds.Tables["anuladas"].Columns.Add(new DataColumn("Vendedor", typeof(System.String)));
                ds.Tables["anuladas"].Columns.Add(new DataColumn("NombreVendedor", typeof(System.String)));
                ds.Tables["anuladas"].Columns.Add(new DataColumn("Tienda", typeof(System.String)));
                ds.Tables["anuladas"].Columns.Add(new DataColumn("Titulo", typeof(System.String)));

                DataColumn[] dcAnuladas = new DataColumn[1];
                dcAnuladas[0] = ds.Tables["anuladas"].Columns["Factura"];
                ds.Tables["anuladas"].PrimaryKey = dcAnuladas;


                for (int ii = 0; ii < 50; ii++)
                {
                    DataRow rowComision = ds.Tables["comisiones"].NewRow();
                    rowComision["NombreVendedor"] = string.Format("Salesman 0{0}", ii.ToString()); ;
                    rowComision["Valor"] = ii * 100;
                    rowComision["MontoComisionable"] = ii * 75;
                    rowComision["ValorNeto"] = ii *50;
                    rowComision["Comision"] = 0;
                    rowComision["Porcentaje"] = 0;
                    rowComision["Vendedor"] = string.Format("0{0}", ii.ToString());
                    rowComision["Tienda"] = "F01";
                    rowComision["Titulo"] = "Comisiones";
                    rowComision["Tooltip"] = "";
                    ds.Tables["comisiones"].Rows.Add(rowComision);
                }

                for (int ii = 0; ii < 100; ii++)
                {
                    DataRow rowFactura = ds.Tables["facturas"].NewRow();
                    rowFactura["Factura"] = string.Format("Invoice {0}", ii.ToString());
                    rowFactura["Valor"] = ii * 100;
                    rowFactura["MontoComisionable"] = ii * 75;
                    rowFactura["ValorNeto"] = ii * 50;
                    rowFactura["Comision"] = 0;
                    rowFactura["Porcentaje"] = 0;
                    rowFactura["FechaFactura"] = DateTime.Now.Date;
                    rowFactura["FechaEntrega"] = DateTime.Now.Date;
                    rowFactura["Vendedor"] = string.Format("0{0}", ii.ToString());
                    rowFactura["NombreVendedor"] = string.Format("Salesman 0{0}", ii.ToString());
                    rowFactura["Tienda"] = "F01";
                    rowFactura["Titulo"] = "Facturas";
                    ds.Tables["facturas"].Rows.Add(rowFactura);
                }

                for (int ii = 0; ii < 40; ii++)
                {
                    DataRow rowPendiente = ds.Tables["pendientes"].NewRow();
                    rowPendiente["Factura"] = string.Format("Invoice {0}", ii.ToString());
                    rowPendiente["Valor"] = ii * 100;
                    rowPendiente["MontoComisionable"] = ii * 75;
                    rowPendiente["ValorNeto"] = ii * 50;
                    rowPendiente["Comision"] = 0;
                    rowPendiente["Porcentaje"] = 0;
                    rowPendiente["FechaFactura"] = DateTime.Now.Date;
                    rowPendiente["FechaEntrega"] = DateTime.Now.Date;
                    rowPendiente["Vendedor"] = string.Format("0{0}", ii.ToString());
                    rowPendiente["NombreVendedor"] = string.Format("Salesman 0{0}", ii.ToString());
                    rowPendiente["Tienda"] = "F01";
                    rowPendiente["Titulo"] = "Pendientes";
                    ds.Tables["pendientes"].Rows.Add(rowPendiente);
                }

                for (int ii = 0; ii < 30; ii++)
                {
                    DataRow rowExcepcion = ds.Tables["excepciones"].NewRow();
                    rowExcepcion["Factura"] = string.Format("Invoice {0}", ii.ToString());
                    rowExcepcion["Valor"] = ii * 100;
                    rowExcepcion["MontoComisionable"] = ii * 50;
                    rowExcepcion["ValorNeto"] = ii * 25;
                    rowExcepcion["Comision"] = 0;
                    rowExcepcion["Porcentaje"] = 0;
                    rowExcepcion["FechaFactura"] = DateTime.Now.Date;
                    rowExcepcion["Vendedor"] = string.Format("0{0}", ii.ToString());
                    rowExcepcion["NombreVendedor"] = string.Format("Salesman 0{0}", ii.ToString());
                    rowExcepcion["Tienda"] = "F01";
                    rowExcepcion["Tipo"] = "";
                    rowExcepcion["Titulo"] = "Excepciones";
                    ds.Tables["excepciones"].Rows.Add(rowExcepcion);
                }

                for (int ii = 0; ii < 20; ii++)
                {
                    DataRow rowAnulada = ds.Tables["anuladas"].NewRow();
                    rowAnulada["Factura"] = string.Format("Invoice {0}", ii.ToString());
                    rowAnulada["Valor"] = 0;
                    rowAnulada["MontoComisionable"] = 0;
                    rowAnulada["ValorNeto"] = 0;
                    rowAnulada["Comision"] = 0;
                    rowAnulada["Porcentaje"] = 0;
                    rowAnulada["FechaFactura"] = DateTime.Now.Date;
                    rowAnulada["Vendedor"] = string.Format("0{0}", ii.ToString());
                    rowAnulada["NombreVendedor"] = string.Format("Salesman 0{0}", ii.ToString());
                    rowAnulada["Tienda"] = "F01";
                    rowAnulada["Titulo"] = "Anuladas";
                    ds.Tables["anuladas"].Rows.Add(rowAnulada);
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                mensaje = string.Format("Error loading data. {0} {1}", ex.Message, m);
            }

            return ds;
        }

        protected void gridComisiones_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.VisibleIndex >= 0 && e.GetValue("Tooltip").ToString().Trim().Length > 0) e.Cell.ToolTip = e.GetValue("Tooltip").ToString();
        }

        protected void gridComisiones_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
            if (Convert.ToString(e.GetValue("Tooltip")).Trim().Length > 0) e.Row.ForeColor = System.Drawing.Color.FromArgb(227, 89, 0);
        }

        protected void gridFacturas_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void gridExcepciones_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void gridAnuladas_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void gridPendientes_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Group) e.Row.Font.Bold = true;
            if (e.RowType == GridViewRowType.GroupFooter) e.Row.Font.Bold = true;
        }

        protected void ToolbarExport_ItemClick(object source, ExportItemClickEventArgs e)
        {
            try
            {
                ValidarSesion();

                lbInfoComisiones.Text = "";
                lbErrorComisiones.Text = "";

                if (Session["dsComisiones"] == null)
                {
                    lbErrorComisiones.Text = "Debe cargar documentos de comisiones.";
                    return;
                }

                DataSet dsComisiones = new DataSet();
                dsComisiones = (DataSet)Session["dsComisiones"];

                if (dsComisiones.Tables["comisiones"].Rows.Count == 0)
                {
                    lbErrorComisiones.Text = "Debe seleccionar un rango de fechas.";
                    return;
                }

                switch (e.ExportType)
                {
                    case DemoExportFormat.Com:
                        foreach (GridViewDataColumn item in gridComisiones.GetGroupedColumns())
                            item.UnGroup();

                        gridComisiones.Columns["Tooltip"].Width = 300;
                        gridComisiones.Columns["Tooltip"].Visible = true;

                        //DevExpressSupport: Here I want to set a SheetName = "Info"
                        gridExport.FileName = "Comisiones";
                        gridExport.GridViewID = "gridComisiones";
                        gridExport.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });

                        gridComisiones.Columns["Tooltip"].Width = 0;
                        gridComisiones.Columns["Tooltip"].Visible = false;

                        gridComisiones.GroupBy(gridComisiones.Columns["Titulo"], 0);
                        gridComisiones.GroupBy(gridComisiones.Columns["Tienda"], 1);

                        break;
                    case DemoExportFormat.Fac:
                        if (dsComisiones.Tables["facturas"].Rows.Count == 0)
                        {
                            lbErrorComisiones.Text = "No hay facturas para exportar.";
                            return;
                        }

                        foreach (GridViewDataColumn item in gridFacturas.GetGroupedColumns())
                            item.UnGroup();

                        gridFacturas.Columns["Tienda"].Visible = true;
                        gridFacturas.Columns["NombreVendedor"].Visible = true;

                        //DevExpressSupport: Here I want to set a SheetName = "Info"
                        gridExport.FileName = "Facturas";
                        gridExport.GridViewID = "gridFacturas";
                        gridExport.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });

                        gridFacturas.Columns["Tienda"].Visible = false;
                        gridFacturas.Columns["NombreVendedor"].Visible = false;

                        gridFacturas.GroupBy(gridFacturas.Columns["Titulo"], 0);
                        gridFacturas.GroupBy(gridFacturas.Columns["Tienda"], 1);
                        gridFacturas.GroupBy(gridFacturas.Columns["NombreVendedor"], 2);

                        break;
                    case DemoExportFormat.Exc:
                        if (dsComisiones.Tables["excepciones"].Rows.Count == 0)
                        {
                            lbErrorComisiones.Text = "No hay excepciones para exportar.";
                            return;
                        }

                        foreach (GridViewDataColumn item in gridExcepciones.GetGroupedColumns())
                            item.UnGroup();

                        gridExcepciones.Columns["Tienda"].Visible = true;
                        gridExcepciones.Columns["FechaFactura"].Visible = true;

                        //DevExpressSupport: Here I want to set a SheetName = "Info"
                        gridExport.FileName = "Excepciones";
                        gridExport.GridViewID = "gridExcepciones";
                        gridExport.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });

                        gridExcepciones.Columns["Tienda"].Visible = false;
                        gridExcepciones.Columns["FechaFactura"].Visible = false;
                        gridExcepciones.GroupBy(gridExcepciones.Columns["Titulo"], 0);
                        gridExcepciones.GroupBy(gridExcepciones.Columns["Tienda"], 1);

                       break;
                    case DemoExportFormat.Anu:
                        if (dsComisiones.Tables["anuladas"].Rows.Count == 0)
                        {
                            lbErrorComisiones.Text = "No hay facturas anuladas para exportar.";
                            return;
                        }

                        foreach (GridViewDataColumn item in gridAnuladas.GetGroupedColumns())
                            item.UnGroup();

                        gridAnuladas.Columns["Tienda"].Visible = true;
                        gridAnuladas.Columns["NombreVendedor"].Visible = true;

                        //DevExpressSupport: Here I want to set a SheetName = "Info"
                        gridExport.FileName = "Anuladas";
                        gridExport.GridViewID = "gridAnuladas";
                        gridExport.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });

                        gridAnuladas.Columns["Tienda"].Visible = false;
                        gridAnuladas.Columns["NombreVendedor"].Visible = false;

                        gridAnuladas.GroupBy(gridAnuladas.Columns["Titulo"], 0);
                        gridAnuladas.GroupBy(gridAnuladas.Columns["Tienda"], 1);

                        break;
                    case DemoExportFormat.Pend:
                        if (dsComisiones.Tables["pendientes"].Rows.Count == 0)
                        {
                            lbErrorComisiones.Text = "No hay facturas pendientes para exportar.";
                            return;
                        }

                        foreach (GridViewDataColumn item in gridPendientes.GetGroupedColumns())
                            item.UnGroup();

                        gridPendientes.Columns["Tienda"].Visible = true;
                        gridPendientes.Columns["NombreVendedor"].Visible = true;

                        //DevExpressSupport: Here I want to set a SheetName = "Info"
                        gridExport.FileName = "Pendientes";
                        gridExport.GridViewID = "gridPendientes";
                        gridExport.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });

                        gridPendientes.Columns["Tienda"].Visible = false;
                        gridPendientes.Columns["NombreVendedor"].Visible = false;

                        gridPendientes.GroupBy(gridPendientes.Columns["Titulo"], 0);
                        gridPendientes.GroupBy(gridPendientes.Columns["Tienda"], 1);
                        gridPendientes.GroupBy(gridPendientes.Columns["NombreVendedor"], 2);

                        break;
                    case DemoExportFormat.All:
                        //DevExpressSupport: Here I want to export grids: "gridComisiones", "gridFacturas", "gridExcepciones", "gridAnuladas" and "gridPendientes" in a single ExcelFile and its SheetName as the GridName without the word "grid"

                        break;
                }

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorComisiones.Text = string.Format("Error al exportar las comisiones. {0} {1}", ex.Message, m);
            }

        }

        //void PrintingSystem_XlsxDocumentCreated(object sender, DevExpress.XtraPrinting.XlsxDocumentCreatedEventArgs e)
        //{
        //    e.SheetNames[0] = "Name1";
        //    e.SheetNames[1] = "Name2";
        //    //...
        //}

        protected void gridExport_RenderBrick(object sender, ASPxGridViewExportRenderingEventArgs e)
        {
            if (e.RowType != GridViewRowType.Header) e.BrickStyle.BorderWidth = 0;
        }

        protected void lkFacturaComision_Click(object sender, EventArgs e)
        {
        }

        protected void lkFacturaPendiente_Click(object sender, EventArgs e)
        {
        }

        protected void lkGrabarComisiones_Click(object sender, EventArgs e)
        {

        }

        protected void lkCalcularComisiones_Click(object sender, EventArgs e)
        {

        }


    }
}