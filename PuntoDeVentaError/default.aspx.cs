﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace PuntoDeVenta
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["Usuario"] = "DevExpress";
                Session["Tienda"] = "F01";
                Session["Vendedor"] = "1";
                Session["NombreVendedor"] = "Vendedor1";
                Session["Cliente"] = "1";
                Session["Pedido"] = "Cliente1";
                Session["TipoVenta"] = "NR";
                Session["Financiera"] = "1";
                Session["NivelPrecio"] = "ContadoEfect";

                Response.Redirect("gerencia.aspx");
            }
        }

    }
}