﻿using System;
using System.Data;
using System.Text;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using System.Net.Mail;
using System.Net;
using DevExpress.Web.Data;
using DevExpress.Web;

namespace PuntoDeVenta
{
    public partial class despachos : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                DataSet datas = new DataSet();
                DataTable dt = new DataTable("Transportistas");

                datas.Tables.Add(dt);
                datas.Tables["Transportistas"].Columns.Add(new DataColumn("Transportista", typeof(System.Int32)));
                datas.Tables["Transportistas"].Columns.Add(new DataColumn("Nombre", typeof(System.String)));

                for (int ii = 1; ii < 20; ii++)
                {
                    DataRow row = datas.Tables["Transportistas"].NewRow();
                    row["Transportista"] = ii;
                    row["Nombre"] = string.Format("Transport - {0}", ii.ToString());
                    datas.Tables["Transportistas"].Rows.Add(row);
                }

                (gridDespachos.Columns["Transportista"] as GridViewDataComboBoxColumn).PropertiesComboBox.DataSource = datas.Tables["Transportistas"];

                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsFacturas"];

                gridFacturas.DataSource = ds;
                gridFacturas.DataMember = "documentos";
                gridFacturas.DataBind();

                DataSet dsDespachos = new DataSet();
                dsDespachos = (DataSet)Session["dsDespachos"];

                gridDespachos.DataSource = dsDespachos;
                gridDespachos.DataMember = "documentos";
                gridDespachos.DataBind();
                gridDespachos.SettingsPager.PageSize = 500;
                gridDespachos.DataColumns["NombreCliente"].Settings.AutoFilterCondition = AutoFilterCondition.Contains;
            }
            catch (Exception ex)
            {
                lbErrorDespachos.Text = string.Format("Sesión expirada, por favor salga del sistema y entre de nuevo {0}.", ex.Message);
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HookOnFocus(this.Page as Control);

                Session["dsArticulos"] = null;
                ViewState["Accion"] = null;
                Session["dsFacturas"] = null;

                CargarBodegas();
                CargarArticulosBusqueda();
                LimpiarControlesDespacho();
                CargarTransportistas(); //Here I fill the ComboBox transportistaT
                //CargarFactura();
                CargarDespachados("F");
            }
            else
            {
                try
                {
                    DataSet dsTodos = new DataSet();
                    dsTodos = (DataSet)Session["dsTodos"];

                    gridArticulosBuscar.DataSource = dsTodos;
                    gridArticulosBuscar.DataMember = "articulos";
                    gridArticulosBuscar.DataBind();
                    gridArticulosBuscar.FocusedRowIndex = -1;
                    gridArticulosBuscar.SettingsPager.PageSize = 35;
                }
                catch
                {
                    lbErrorDespachos.Text = "Sesión expirada, por favor salga del sistema y vuelva a ingresar.";
                }
            }

            Page.ClientScript.RegisterStartupScript(
                typeof(despachos),
                "ScriptDoFocus",
                SCRIPT_DOFOCUS.Replace("REQUEST_LASTFOCUS", Request["__LASTFOCUS"]),
                true);
        }

        private const string SCRIPT_DOFOCUS = @"window.setTimeout('DoFocus()', 1); function DoFocus() {
            try {
                document.getElementById('REQUEST_LASTFOCUS').focus();
            } catch (ex) {}
        }";

        private void HookOnFocus(Control CurrentControl)
        {
            //checks if control is one of TextBox, DropDownList, ListBox or Button
            if ((CurrentControl is TextBox) ||
                (CurrentControl is DropDownList) ||
                (CurrentControl is ListBox) ||
                (CurrentControl is Button))
                //adds a script which saves active control on receiving focus 
                //in the hidden field __LASTFOCUS.
                (CurrentControl as WebControl).Attributes.Add(
                   "onfocus",
                   "try{document.getElementById('__LASTFOCUS').value=this.id} catch(e) {}");
            //checks if the control has children
            if (CurrentControl.HasControls())
                //if yes do them all recursively
                foreach (Control CurrentChildControl in CurrentControl.Controls)
                    HookOnFocus(CurrentChildControl);
        }
        
        void CargarBodegas()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("bodegas");

            ds.Tables.Add(dt);
            ds.Tables["bodegas"].Columns.Add(new DataColumn("Bodega", typeof(System.String)));

            DataRow row = ds.Tables["bodegas"].NewRow();
            row["Bodega"] = "F01";
            ds.Tables["bodegas"].Rows.Add(row);

            tienda.DataSource = ds.Tables["bodegas"];
            tienda.ValueField = "Bodega";
            tienda.ValueType = typeof(System.String);
            tienda.TextField = "Bodega";
            tienda.DataBindItems();
        }

        void CargarArticulosBusqueda()
        {
            try
            {
                DataSet dsTodos = new DataSet();
                DataTable dt = new DataTable("articulos");

                dsTodos.Tables.Add(dt);
                dsTodos.Tables["articulos"].Columns.Add(new DataColumn("Articulo", typeof(System.String)));
                dsTodos.Tables["articulos"].Columns.Add(new DataColumn("Descripcion", typeof(System.String)));
                dsTodos.Tables["articulos"].Columns.Add(new DataColumn("Tooltip", typeof(System.String)));

                for (int ii = 1; ii < 100000; ii++)
                {
                    DataRow row = dsTodos.Tables["articulos"].NewRow();
                    row["Articulo"] = string.Format("000-{0}", ii.ToString());
                    row["Descripcion"] = string.Format("Artículo - {0}", ii.ToString());
                    row["Tooltip"] = string.Format("Tooltip - {0}", ii.ToString());
                    dsTodos.Tables["articulos"].Rows.Add(row);
                }

                gridArticulosBuscar.DataSource = dsTodos;
                gridArticulosBuscar.DataMember = "articulos";
                gridArticulosBuscar.DataBind();
                gridArticulosBuscar.FocusedRowIndex = -1;
                gridArticulosBuscar.SettingsPager.PageSize = 35;

                Session["dsTodos"] = dsTodos;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al cargar los articulos. {0} {1}", ex.Message, m);
            }
        }

        //This is my original method, how can I do to call my webservice as asynchronous?
        //void CargarArticulosBusqueda()
        //{
        //    var ws = new wsPuntoVenta.wsPuntoVenta();
        //    if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

        //    DataSet dsTodos = new DataSet();
        //    dsTodos = ws.DevuelveArticulosTodos();

        //    gridArticulosBuscar.DataSource = dsTodos;
        //    gridArticulosBuscar.DataMember = "articulos";
        //    gridArticulosBuscar.DataBind();
        //    gridArticulosBuscar.FocusedRowIndex = -1;
        //    gridArticulosBuscar.SettingsPager.PageSize = 35;

        //    Session["dsTodos"] = dsTodos;
        //}


        void LimpiarControlesDespacho()
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                DateTime mFechaFinal = DateTime.Now.Date;
                DateTime mFechaInicial = mFechaFinal.AddDays(-7);

                fechaInicial.Value = mFechaInicial;
                fechaFinal.Value = mFechaFinal;

                fechaInicialE.Value = DateTime.Now.Date;
                fechaFinalE.Value = DateTime.Now.Date;

                txtCliente.Text = "";
                tienda.Value = "F01";
                txtFactura.Text = "";
                txtFactura.ToolTip = "Ingrese el número de factura a buscar";
                lbPedido.Text = "";

                fechaInicial.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al inicializar los despachos. {0} {1}", ex.Message, m);
            }
        }

        protected void gridFacturas_BatchUpdate(object sender, ASPxDataBatchUpdateEventArgs e)
        {

        }

        protected void gridFacturas_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {

        }

        protected void gridFacturas_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {

        }

        protected void gridFacturas_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {

        }

        protected void lkSeleccionarFechas_Click(object sender, EventArgs e)
        {
            CargarFacturas("F");
        }

        protected void lkSeleccionarFechasE_Click(object sender, EventArgs e)
        {
            CargarFacturas("E");
        }

        protected void lkSeleccionarCliente_Click(object sender, EventArgs e)
        {
            CargarFacturas("C");
        }

        protected void lkSeleccionarTienda_Click(object sender, EventArgs e)
        {
            CargarFacturas("T");
        }

        protected void lkSeleccionarAprobadas_Click(object sender, EventArgs e)
        {
            CargarFacturas("A");
        }

        void CargarFacturas(string tipo)
        {
            try
            {                

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al mostrar los documentos. {0} {1}", ex.Message, m);
            }

        }

        void CargarFactura()
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable("documento");

                ds.Tables.Add(dt);

                ds.Tables["documento"].Columns.Add(new DataColumn("Factura", typeof(System.String)));
                ds.Tables["documento"].Columns.Add(new DataColumn("Linea", typeof(System.Int32)));
                ds.Tables["documento"].Columns.Add(new DataColumn("Articulo", typeof(System.String)));
                ds.Tables["documento"].Columns.Add(new DataColumn("Articulo2", typeof(System.String)));
                ds.Tables["documento"].Columns.Add(new DataColumn("Descripcion", typeof(System.String)));
                ds.Tables["documento"].Columns.Add(new DataColumn("Cantidad", typeof(System.Int32)));
                ds.Tables["documento"].Columns.Add(new DataColumn("Bodega", typeof(System.String)));
                ds.Tables["documento"].Columns.Add(new DataColumn("Localizacion", typeof(System.String)));
                ds.Tables["documento"].Columns.Add(new DataColumn("Tooltip", typeof(System.String)));
                ds.Tables["documento"].Columns.Add(new DataColumn("Existencias", typeof(System.Int32)));
                ds.Tables["documento"].Columns.Add(new DataColumn("Tipo", typeof(System.String)));
                ds.Tables["documento"].Columns.Add(new DataColumn("Cliente", typeof(System.String)));
                ds.Tables["documento"].Columns.Add(new DataColumn("Consecutivo", typeof(System.String)));
                ds.Tables["documento"].Columns.Add(new DataColumn("Despacho", typeof(System.String)));
                ds.Tables["documento"].Columns.Add(new DataColumn("Cobrador", typeof(System.String)));
                ds.Tables["documento"].Columns.Add(new DataColumn("NombreCliente", typeof(System.String)));
                ds.Tables["documento"].Columns.Add(new DataColumn("Direccion", typeof(System.String)));
                ds.Tables["documento"].Columns.Add(new DataColumn("CostoTotal", typeof(System.Decimal)));
                ds.Tables["documento"].Columns.Add(new DataColumn("CostoTotalDolar", typeof(System.Decimal)));
                ds.Tables["documento"].Columns.Add(new DataColumn("LineaOrigen", typeof(System.Int32)));
                ds.Tables["documento"].Columns.Add(new DataColumn("PrecioTotal", typeof(System.Decimal)));
                ds.Tables["documento"].Columns.Add(new DataColumn("Telefonos", typeof(System.String)));
                ds.Tables["documento"].Columns.Add(new DataColumn("Pedido", typeof(System.String)));
                ds.Tables["documento"].Columns.Add(new DataColumn("NombreRecibe", typeof(System.String)));
                ds.Tables["documento"].Columns.Add(new DataColumn("Vendedor", typeof(System.String)));
                ds.Tables["documento"].Columns.Add(new DataColumn("ObservacionesVendedor", typeof(System.String)));
                ds.Tables["documento"].Columns.Add(new DataColumn("FechaEntrega", typeof(System.DateTime)));
                ds.Tables["documento"].Columns.Add(new DataColumn("Observaciones", typeof(System.String)));

                DataColumn[] dc = new DataColumn[1];
                dc[0] = ds.Tables["documento"].Columns["Linea"];
                ds.Tables["documento"].PrimaryKey = dc;

                for (int ii = 1; ii < 6; ii++)
                {
                    DataRow row = ds.Tables["documento"].NewRow();
                    row["Factura"] = string.Format("Factura-{0}", ii.ToString());
                    row["Linea"] = ii;
                    row["Articulo"] = string.Format("000-{0}", ii.ToString());
                    row["Articulo2"] = string.Format("000-{0}", ii.ToString());
                    row["Descripcion"] = string.Format("Artículo - {0}", ii.ToString());
                    row["Cantidad"] = 1;
                    row["Bodega"] = "F01";
                    row["Localizacion"] = "ARMADO";
                    row["Tooltip"] = "Tip";
                    row["Existencias"] = 25;
                    row["Tipo"] = "N";
                    row["Cliente"] = "1";
                    row["Consecutivo"] = "C";
                    row["Despacho"] = "1";
                    row["Cobrador"] = "F01";
                    row["NombreCliente"] = "Nombre del cliente";
                    row["Direccion"] = "--";
                    row["CostoTotal"] = 1;
                    row["CostoTotalDolar"] = 1;
                    row["LineaOrigen"] = 1;
                    row["PrecioTotal"] = 1;
                    row["Telefonos"] = "T";
                    row["Pedido"] = "P";
                    row["NombreRecibe"] = "Recibe";
                    row["Vendedor"] = "1";
                    row["ObservacionesVendedor"] = "";
                    row["FechaEntrega"] = DateTime.Now.Date;
                    row["Observaciones"] = "";
                    ds.Tables["documento"].Rows.Add(row);
                }

                gridFacturas.Visible = false;
                gridFacturaDet.Visible = true;
                gridFacturaDet.DataSource = ds;
                gridFacturaDet.DataMember = "documento";
                gridFacturaDet.DataBind();
                gridFacturaDet.FocusedRowIndex = -1;

                Session["Cliente"] = Convert.ToString(ds.Tables["documento"].Rows[0]["Cliente"]);
                txtDespacho.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Despacho"]);
                txtNombreCliente.Text = string.Format("{0} - {1}", Convert.ToString(ds.Tables["documento"].Rows[0]["Cliente"]), Convert.ToString(ds.Tables["documento"].Rows[0]["NombreCliente"]));
                txtTelefonos.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Telefonos"]);
                txtDireccion.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Direccion"]);
                txtTienda.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Cobrador"]);
                txtPedido.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Pedido"]);
                txtPersonaRecibe.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["NombreRecibe"]);
                txtVendedor.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Vendedor"]);
                txtObservacionesVendedor.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["ObservacionesVendedor"]);
                fechaEntrega.Value = Convert.ToDateTime(ds.Tables["documento"].Rows[0]["FechaEntrega"]);
                txtObservaciones.Text = Convert.ToString(ds.Tables["documento"].Rows[0]["Observaciones"]);
                txtFactura.ToolTip = string.Format("Esta factura tiene el Pedido No. {0}", Convert.ToString(ds.Tables["documento"].Rows[0]["Pedido"]));
                lbPedido.Text = string.Format("Pedido No. {0}", Convert.ToString(ds.Tables["documento"].Rows[0]["Pedido"]));

                txtTelefonos.ReadOnly = true;
                txtDireccion.ReadOnly = true;
                txtNombreCliente.ReadOnly = true;

                string mSerie = "O";
                if (Convert.ToString(ds.Tables["documento"].Rows[0]["Consecutivo"]) == "DESPA") mSerie = "A";
                if (Convert.ToString(ds.Tables["documento"].Rows[0]["Consecutivo"]) == "DES SUENIA") mSerie = "S";

                cbSerie.Value = mSerie;

                if (Convert.ToString(ds.Tables["documento"].Rows[0]["Consecutivo"]) == "DES SUENIA")
                {
                    txtTelefonos.ReadOnly = false;
                    txtDireccion.ReadOnly = false;
                    txtNombreCliente.ReadOnly = false;

                    txtNombreCliente.Focus();
                }
                else
                {
                    txtObservaciones.Focus();
                }

                Session["dsArticulos"] = ds;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al mostrar el documento. {0} {1}", ex.Message, m);
            }
        }

        protected void CancelEditing(CancelEventArgs e)
        {
            e.Cancel = true;
            gridFacturaDet.CancelEdit();
        }

        void UpdateFactura(string linea, string bodega, string cantidad, string localizacion)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsArticulos"];

                ds.Tables["documento"].Rows.Find(linea)["Bodega"] = bodega;
                ds.Tables["documento"].Rows.Find(linea)["Cantidad"] = Convert.ToInt32(cantidad);
                ds.Tables["documento"].Rows.Find(linea)["Localizacion"] = localizacion;
                Session["dsArticulos"] = ds;

                gridFacturaDet.DataSource = ds;
                gridFacturaDet.DataMember = "documento";
                gridFacturaDet.DataBind();
                gridFacturaDet.FocusedRowIndex = -1;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbError.Text = string.Format("Error al actualizar los datos UPDATE {0} {1}", ex.Message, m);
            }
        }


        protected void gridFacturaDet_BatchUpdate(object sender, ASPxDataBatchUpdateEventArgs e)
        {
            foreach (var args in e.UpdateValues)
                UpdateFactura(args.Keys["Linea"].ToString(), args.NewValues["Bodega"].ToString(), args.NewValues["Cantidad"].ToString(), args.NewValues["Localizacion"].ToString());

            e.Handled = true;
        }

        protected void gridFacturaDet_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            UpdateFactura(e.Keys["Linea"].ToString(), e.NewValues["Bodega"].ToString(), e.NewValues["Cantidad"].ToString(), e.NewValues["Localizacion"].ToString());
            CancelEditing(e);
        }

        protected void gridFacturaDet_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            if (e.Column.FieldName == "Bodega")
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable("bodegas");

                ds.Tables.Add(dt);
                ds.Tables["bodegas"].Columns.Add(new DataColumn("Bodega", typeof(System.String)));

                DataRow row = ds.Tables["bodegas"].NewRow();
                row["Bodega"] = "F01";
                ds.Tables["bodegas"].Rows.Add(row);

                DataRow row2 = ds.Tables["bodegas"].NewRow();
                row2["Bodega"] = "F02";
                ds.Tables["bodegas"].Rows.Add(row2);

                DataRow row3 = ds.Tables["bodegas"].NewRow();
                row3["Bodega"] = "F03";
                ds.Tables["bodegas"].Rows.Add(row3);

                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                cmb.DataSource = ds.Tables["Bodegas"];
                cmb.ValueField = "Bodega";
                cmb.ValueType = typeof(System.String);
                cmb.TextField = "Bodega";
                cmb.DataBindItems();
            }
            
            if (e.Column.FieldName == "Localizacion")
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable("Localizaciones");

                ds.Tables.Add(dt);
                ds.Tables["Localizaciones"].Columns.Add(new DataColumn("Localizacion", typeof(System.String)));

                DataRow row = ds.Tables["Localizaciones"].NewRow();
                row["Localizacion"] = "ARMADO";
                ds.Tables["Localizaciones"].Rows.Add(row);

                DataRow row2 = ds.Tables["Localizaciones"].NewRow();
                row2["Localizacion"] = "CAJA";
                ds.Tables["Localizaciones"].Rows.Add(row2);

                DataRow row3 = ds.Tables["Localizaciones"].NewRow();
                row3["Localizacion"] = "DESRMADO";
                ds.Tables["Localizaciones"].Rows.Add(row3);

                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                cmb.DataSource = ds.Tables["Localizaciones"];
                cmb.ValueField = "Localizacion";
                cmb.ValueType = typeof(System.String);
                cmb.TextField = "Localizacion";
                cmb.DataBindItems();
            }
        }

        protected void gridFacturaDet_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == ColumnCommandButtonType.Update || e.ButtonType == ColumnCommandButtonType.Cancel) e.Visible = false;
        }

        protected void lkCargarFactura_Click(object sender, EventArgs e)
        {
            CargarFactura();
        }

        protected void txtCliente_TextChanged(object sender, EventArgs e)
        {
            CargarFacturas("C");
        }

        protected void lkGrabarDespacho_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al grabar el despacho. {0} {1}", ex.Message, m);
            }
        }

        protected void gridFacturaDet_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.VisibleIndex >= 0) e.Cell.ToolTip = e.GetValue("Tooltip").ToString();
        }

        protected void gridFacturas_FocusedRowChanged(object sender, EventArgs e)
        {
            if (!gridFacturas.Visible) return;
            if (gridFacturas.FocusedRowIndex == -1) return;
            if (txtFactura.Text.Trim().Length > 0) return;
            if (Convert.ToString(gridFacturas.GetRowValues(gridFacturas.FocusedRowIndex, "Factura")).Trim().Length == 0) return;
            if (txtFactura.Text == Convert.ToString(gridFacturas.GetRowValues(gridFacturas.FocusedRowIndex, "Factura"))) return;

            txtFactura.Text = Convert.ToString(gridFacturas.GetRowValues(gridFacturas.FocusedRowIndex, "Factura"));
            CargarFactura();
        }

        protected void gridFacturas_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (Convert.ToString(e.GetValue("Aprobada")) == "S") e.Row.BackColor = System.Drawing.Color.LightGreen;
        }

        protected void gridFacturaDet_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (Convert.ToInt32(e.GetValue("Existencias")) == 0 && Convert.ToString(e.GetValue("Tipo")) != "K") e.Row.ForeColor = System.Drawing.Color.Red;
        }

        void ImprimirExpediente(string pedido)
        {
            try
            {
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorDespachos.Text = string.Format("Error al imprimir el expediente {0} {1}", ex.Message, m);
            }
        }


        protected void cbSerie_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al seleccionar el consecutivo. {0} {1}", ex.Message, m);
            }
        }

        protected void btnImprimirDespacho_Click(object sender, EventArgs e)
        {
            string mMensaje = "";

            try
            {
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al imprimir el envío. {0} {1} {2}", ex.Message, mMensaje, m);
                return;
            }

        }

        protected void lkExpediente_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al imprimir el expediente. {0} {1}", ex.Message, m);
                return;
            }
        }

        protected void lkVerFactura_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al re-imprimir la factura {0} {1}", ex.Message, m);
            }
        }

        protected void lkAprobarFactura_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al aprobar la factura. {0} {1}", ex.Message, m);
            }
        }

        protected void lkDesAprobarFactura_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al desaprobar la factura. {0} {1}", ex.Message, m);
            }
        }

        protected void lkCliente_Click(object sender, EventArgs e)
        {
        }

        protected void lkPedidos_Click(object sender, EventArgs e)
        {
        }

        protected void btnArticulo2_Click(object sender, EventArgs e)
        {
            try
            {
                var btn = (sender as ASPxButton);
                var nc = btn.NamingContainer as GridViewDataItemTemplateContainer;

                string mArticulo = DataBinder.Eval(nc.DataItem, "Articulo").ToString();
                string mDescripcion = DataBinder.Eval(nc.DataItem, "Descripcion").ToString();

                txtObservacionesVendedor.Text = string.Format("{0} - {1}", mArticulo, mDescripcion);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al desaprobar la factura. {0} {1}", ex.Message, m);
            }
        }

        protected void btnArticuloBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoDespachos.Text = "";
                lbErrorDespachos.Text = "";

                var btn = (sender as ASPxButton);
                var nc = btn.NamingContainer as GridViewDataItemTemplateContainer;

                string mArticulo = DataBinder.Eval(nc.DataItem, "Articulo").ToString();
                string mDescripcion = DataBinder.Eval(nc.DataItem, "Descripcion").ToString();

                txtNombreCliente.Text = mArticulo;
                txtObservaciones.Text = mDescripcion;

                string startUpScript = "window.parent.HidePopup();";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "ANY_KEY", startUpScript, true);
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al seleccionar el artículo del listado para cambiar. {0} {1}", ex.Message, m);
            }
        }

        protected void btnDescripcion_Init(object sender, EventArgs e)
        {
            var btn = sender as ASPxButton;
            var container = btn.NamingContainer as GridViewDataItemTemplateContainer;
            btn.ClientSideEvents.Click = string.Format("function(s,e){{ onDescriptionClick(s,e,{0}); }}", container.KeyValue ?? -1);
        }

        protected void btnArticuloBuscar_Init(object sender, EventArgs e)
        {
            var btn = sender as ASPxButton;
            var nc = btn.NamingContainer as GridViewDataItemTemplateContainer;

            string mArticulo = DataBinder.Eval(nc.DataItem, "Articulo").ToString();
            string mDescripcion = DataBinder.Eval(nc.DataItem, "Descripcion").ToString();

            txtNombreCliente.Text = mArticulo;
            txtObservaciones.Text = mDescripcion;

            btn.ClientSideEvents.Click = "function(s,e){{ HidePopup(); }}";
        }

        protected void gridArticulosBuscar_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.VisibleIndex >= 0) e.Cell.ToolTip = e.GetValue("Tooltip").ToString();
        }

        protected void lkSeleccionarFechasT_Click(object sender, EventArgs e)
        {
            CargarDespachados("F");
        }

        protected void lkSeleccionarTiendaT_Click(object sender, EventArgs e)
        {
            CargarDespachados("T");
        }

        protected void lkTransportistaT_Click(object sender, EventArgs e)
        {
            CargarDespachados("R");
        }

        void CargarDespachados(string tipo)
        {
            try
            {
                //This code is for demonstration purpose
                gridDespachos.Visible = true;

                DataSet ds = new DataSet();
                DataTable dt = new DataTable("documentos");

                ds.Tables.Add(dt);
                ds.Tables["documentos"].Columns.Add(new DataColumn("Despacho", typeof(System.String)));
                ds.Tables["documentos"].Columns.Add(new DataColumn("Fecha", typeof(System.DateTime)));
                ds.Tables["documentos"].Columns.Add(new DataColumn("FechaEntrega", typeof(System.DateTime)));
                ds.Tables["documentos"].Columns.Add(new DataColumn("Transportista", typeof(System.Int32)));
                ds.Tables["documentos"].Columns.Add(new DataColumn("Cliente", typeof(System.String)));
                ds.Tables["documentos"].Columns.Add(new DataColumn("NombreCliente", typeof(System.String)));
                ds.Tables["documentos"].Columns.Add(new DataColumn("Tienda", typeof(System.String)));

                DataColumn[] dc = new DataColumn[1];
                dc[0] = ds.Tables["documentos"].Columns["Despacho"];
                ds.Tables["documentos"].PrimaryKey = dc;

                Random rnd = new Random();
                for (int ii = 1; ii < 30; ii++)
                {
                    DataRow row = ds.Tables["documentos"].NewRow();
                    row["Despacho"] = string.Format("Order No. {0}", ii.ToString());
                    row["Fecha"] = DateTime.Now.Date;
                    row["FechaEntrega"] = DateTime.Now.Date;
                    row["Transportista"] = rnd.Next(1, 20);
                    row["Cliente"] = "N";
                    row["NombreCliente"] = string.Format("Customer {0}", ii.ToString());
                    row["Tienda"] = "F01";
                    ds.Tables["documentos"].Rows.Add(row);
                }

                gridDespachos.Visible = true;
                gridDespachos.DataSource = ds;
                gridDespachos.DataMember = "documentos";
                gridDespachos.DataBind();
                gridDespachos.FocusedRowIndex = -1;
                gridDespachos.SettingsPager.PageSize = 500;
                gridDespachos.DataColumns["NombreCliente"].Settings.AutoFilterCondition = AutoFilterCondition.Contains;

                Session["dsDespachos"] = ds;


                //*******This is my original code
                //ValidarSesion();

                //lbInfoTransportista.Text = "";
                //lbErrorTransportista.Text = "";

                //gridDespachos.Visible = false;

                //var ws = new wsPuntoVenta.wsPuntoVenta();
                //if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                //DateTime mFechaInicial = Convert.ToDateTime(fechaInicialT.Value);
                //DateTime mFechaFinal = Convert.ToDateTime(fechaFinalT.Value);

                //DataSet ds = new DataSet(); string mMensaje = "";
                //ds = ws.DocumentosDespachadosModificar(ref mMensaje, mFechaInicial, mFechaFinal, Convert.ToString(tiendaTransporte.Value), tipo, Convert.ToString(transportistaT.Value));

                //if (mMensaje.Trim().Length > 0)
                //{
                //    lbErrorTransportista.Text = mMensaje;
                //    return;
                //}

                //if (ds.Tables["documentos"].Rows.Count > 0)
                //{
                //    gridDespachos.Visible = true;
                //    gridDespachos.DataSource = ds;
                //    gridDespachos.DataMember = "documentos";
                //    gridDespachos.DataBind();
                //    gridDespachos.FocusedRowIndex = -1;
                //    gridDespachos.SettingsPager.PageSize = 500;
                //    gridDespachos.DataColumns["NombreCliente"].Settings.AutoFilterCondition = AutoFilterCondition.Contains;

                //    if (tipo == "P")
                //    {
                //        gridDespachos.DataColumns["Despacho"].Caption = "Traspaso";
                //        gridDespachos.DataColumns["Cliente"].ReadOnly = true;
                //    }
                //    else
                //    {
                //        gridDespachos.Columns["Despacho"].Caption = "Despacho";
                //        gridDespachos.DataColumns["Cliente"].ReadOnly = false;
                //    }

                //    Session["dsDespachos"] = ds;
                //}

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al mostrar los documentos despachados. {0} {1}", ex.Message, m);
            }

        }

        protected void lkTraspasos_Click(object sender, EventArgs e)
        {
            CargarDespachados("P");
        }

        void UpdateDespacho(string despacho, DateTime fechaEntrega, Int32 transportista, string cliente)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = (DataSet)Session["dsDespachos"];

                ds.Tables["documentos"].Rows.Find(despacho)["FechaEntrega"] = fechaEntrega;
                ds.Tables["documentos"].Rows.Find(despacho)["Transportista"] = transportista;
                ds.Tables["documentos"].Rows.Find(despacho)["Cliente"] = cliente;
                Session["dsArticulos"] = ds;

                gridDespachos.DataSource = ds;
                gridDespachos.DataMember = "documentos";
                gridDespachos.DataBind();
                gridDespachos.FocusedRowIndex = -1;
                gridDespachos.SettingsPager.PageSize = 500;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                lbErrorTransportista.Text = string.Format("Error al actualizar los datos UPDATE {0} {1}", ex.Message, m);
            }
        }

        protected void gridDespachos_BatchUpdate(object sender, ASPxDataBatchUpdateEventArgs e)
        {
            foreach (var args in e.UpdateValues)
                UpdateDespacho(args.Keys["Despacho"].ToString(), Convert.ToDateTime(args.NewValues["FechaEntrega"]), Convert.ToInt32(args.NewValues["Transportista"]), args.NewValues["Cliente"].ToString());

            e.Handled = true;
        }

        protected void gridDespachos_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            //This code is for demonstration purpose
            //if (e.Column.FieldName == "Transportista")
            //{
            //    DataSet ds = new DataSet();
            //    DataTable dt = new DataTable("Transportistas");

            //    ds.Tables.Add(dt);
            //    ds.Tables["Transportistas"].Columns.Add(new DataColumn("Transportista", typeof(System.Int32)));
            //    ds.Tables["Transportistas"].Columns.Add(new DataColumn("Nombre", typeof(System.String)));

            //    for (int ii = 1; ii < 20; ii++)
            //    {
            //        DataRow row = ds.Tables["Transportistas"].NewRow();
            //        row["Transportista"] = ii;
            //        row["Nombre"] = string.Format("Transport - {0}", ii.ToString());
            //        ds.Tables["Transportistas"].Rows.Add(row);
            //    }

            //    ASPxComboBox cmb = e.Editor as ASPxComboBox;
            //    cmb.DataSource = ds.Tables["Transportistas"];
            //    cmb.ValueField = "Transportista";
            //    cmb.ValueType = typeof(System.Int32);
            //    cmb.TextField = "Nombre";
            //    cmb.DataBindItems();
            //}



            //This is my original code
            //var ws = new wsPuntoVenta.wsPuntoVenta();
            //if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            //if (e.Column.FieldName == "Transportista")
            //{
            //    var q = ws.DevuelveTransportistas("F01").OrderBy(t => t.Nombre);

            //    ASPxComboBox cmb = e.Editor as ASPxComboBox;
            //    cmb.DataSource = q;
            //    cmb.ValueField = "Transportista";
            //    cmb.ValueType = typeof(System.Int32);
            //    cmb.TextField = "Nombre";
            //    cmb.DataBindItems();
            //}
        }

        protected void gridDespachos_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            UpdateDespacho(e.Keys["Despacho"].ToString(), Convert.ToDateTime(e.NewValues["FechaEntrega"]), Convert.ToInt32(e.NewValues["Cantidad"]), e.NewValues["Cliente"].ToString());
            CancelEditingDespacho(e);
        }

        protected void gridDespachos_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            if (e.ButtonType == ColumnCommandButtonType.Update || e.ButtonType == ColumnCommandButtonType.Cancel) e.Visible = false;
        }

        protected void gridDespachos_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.Name == "Cliente") e.Cell.ToolTip = "Marque esta casilla si el cliente confirmó de recibida su mercadería";
        }

        protected void gridDespachos_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {

        }

        protected void CancelEditingDespacho(CancelEventArgs e)
        {
            ValidarSesion();
            e.Cancel = true;
            gridDespachos.CancelEdit();
        }

        void CargarTransportistas()
        {
            try
            {
                //This code is for demonstration purpose
                DataSet ds = new DataSet();
                DataTable dt = new DataTable("Transportistas");

                ds.Tables.Add(dt);
                ds.Tables["Transportistas"].Columns.Add(new DataColumn("Transportista", typeof(System.Int32)));
                ds.Tables["Transportistas"].Columns.Add(new DataColumn("Nombre", typeof(System.String)));

                for (int ii = 1; ii < 20; ii++)
                {
                    DataRow row = ds.Tables["Transportistas"].NewRow();
                    row["Transportista"] = ii;
                    row["Nombre"] = string.Format("Transport - {0}", ii.ToString());
                    ds.Tables["Transportistas"].Rows.Add(row);
                }

                transportistaT.DataSource = ds.Tables["Transportistas"];
                transportistaT.ValueField = "Transportista";
                transportistaT.ValueType = typeof(System.Int32);
                transportistaT.TextField = "Nombre";
                transportistaT.DataBindItems();

                transportistaT.Value = 1;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al cargar los transportistas. {0} {1}", ex.Message, m);
            }


            


            //This is my original code
            //var ws = new wsPuntoVenta.wsPuntoVenta();
            //if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            //var q = ws.DevuelveTransportistas("F01").OrderBy(t => t.Nombre);

            //transportistaT.DataSource = q;
            //transportistaT.ValueField = "Transportista";
            //transportistaT.ValueType = typeof(System.Int32);
            //transportistaT.TextField = "Nombre";
            //transportistaT.DataBindItems();

            //transportistaT.Value = 1;
        }

        void ValidarSesion()
        {
            //Nothing
        }










        protected void lkActualizarTransportista_Click(object sender, EventArgs e)
        {
            try
            {
                //ValidarSesion();

                //lbInfoTransportista.Text = "";
                //lbErrorTransportista.Text = "";

                //var ws = new wsPuntoVenta.wsPuntoVenta(); string mMensaje = "";
                //if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

                //DataSet ds = new DataSet();
                //ds = (DataSet)Session["dsDespachos"];

                //if (!ws.ActualizarTransportista(ref mMensaje, Convert.ToString(Session["Usuario"]), ds))
                //{
                //    lbErrorTransportista.Text = mMensaje;
                //    return;
                //}

                //gridDespachos.Visible = false;
                //lbInfoTransportista.Text = mMensaje;
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                lbErrorDespachos.Text = string.Format("Error al actualizar los transportistas y aceptación de clientes. {0} {1}", ex.Message, m);
            }
        }



    }
}
