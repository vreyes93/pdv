﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="gerencia.aspx.cs" Inherits="PuntoDeVenta.gerencia" %>
<%@ Register assembly="DevExpress.Web.v16.1, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1000
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>&nbsp;&nbsp;Opciones de Gerencia</h3>
    <div class="content2">
        <div class="clientes">
            <ul>
                <li>
                    <table align="center" >
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td colspan="2">
                                <a>Tasa de cambio</a></td>
                            <td colspan="2" style="text-align: right">
                                <asp:Label ID="lbErrorTasa" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                <asp:Label ID="lbInfoTasa" runat="server"></asp:Label>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td colspan="4" style="text-align: center">
                                Tasa de cambio: 
                                <asp:TextBox ID="txtTasaCambio" runat="server" TabIndex="10" 
                                    AutoPostBack="True" ontextchanged="txtTasaCambio_TextChanged"></asp:TextBox>
                            &nbsp;<asp:LinkButton ID="lkGrabarTasaCambio" runat="server" TabIndex="20" 
                                    ToolTip="Grabar tasa de cambio" onclick="lkGrabarTasaCambio_Click">Grabar</asp:LinkButton>
                                <asp:LinkButton ID="lkEnviarCorreoTasa" runat="server" 
                                    onclick="lkEnviarCorreoTasa_Click" Visible="False">Enviar correo tasa</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td style="text-align: center">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td colspan="2" style="text-align: center">
                                <asp:GridView ID="gridTasaCambio" runat="server" AutoGenerateColumns="False" 
                                    BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                                    CellPadding="3" CellSpacing="2" TabIndex="30" Width="570px">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Tasa de Cambio">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTasaCambio" runat="server" Text='<%# Bind("TasaCambio") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbTasaCambio" runat="server" Text='<%# Bind("TasaCambio") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="150px" />
                                            <ItemStyle Width="150px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtFecha" runat="server" Text='<%# Bind("Fecha") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbFecha" runat="server" Text='<%# Bind("Fecha") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="170px" />
                                            <ItemStyle Width="170px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Activa">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtActiva" runat="server" Text='<%# Bind("Activa") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbActiva" runat="server" Text='<%# Bind("Activa") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="70px" />
                                            <ItemStyle Width="70px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Usuario Ingresó">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtUsuario" runat="server" Text='<%# Bind("Usuario") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbUsuario" runat="server" Text='<%# Bind("Usuario") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="150px" />
                                            <ItemStyle Width="150px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                                </asp:GridView>
                            </td>
                            <td style="text-align: center">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                </li>
            </ul>

            <ul>
                <li>
                    
                    <table class="style1000">
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Cargar precios base:</td>
                            <td>
                                <dx:ASPxUploadControl ID="upPreciosBase" runat="server" Theme="SoftOrange" CssClass="textBoxStyle"
                                    UploadMode="Auto" Width="280px" ToolTip="Cargar archivo de precios base">
                                    <Border BorderStyle="None" />
                                </dx:ASPxUploadControl>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="6">

                                <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" 
                                    EnableTheming="True" Theme="SoftOrange">
                                    <SettingsCommandButton>
                                    <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>

                                    <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                    </SettingsCommandButton>
                                    <Columns>
                                        <dx:GridViewDataTextColumn VisibleIndex="0" Caption="One">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn VisibleIndex="1" Caption="Two">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Three">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn VisibleIndex="3" Caption="Four">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:ASPxGridView>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                    
                </li>
            </ul>
        </div>
        <table align="center">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbInfo" runat="server" TabIndex="620"></asp:Label>
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red" TabIndex="630"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
