﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using System.Net.Mail;
using System.Net;

namespace PuntoDeVenta
{
    public partial class gerencia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ValidarSesion();
                ValidarPruebas();

                Label lbUsuario = (Label)this.Master.FindControl("lblUsuario");
                if (Convert.ToString(Session["NombreVendedor"]) == "Alerta") Session["Vendedor"] = null;
                lbUsuario.Text = string.Format("{0}  -  {1}", Convert.ToString(Session["NombreVendedor"]), Convert.ToString(Session["Tienda"]));

                ViewState["url"] = Convert.ToString(Session["Url"]);
                ViewState["urlPrecios"] = Convert.ToString(Session["UrlPrecios"]);

                HtmlGenericControl submenu = new HtmlGenericControl();
                submenu = (HtmlGenericControl)Master.FindControl("submenu");
                submenu.Visible = true;

                ValidarOpciones();
            }
        }

        void ValidarPruebas()
        {
        }

        void ValidarSesion()
        {
        }

        void ValidarOpciones()
        {
        }

        void CargarTasas()
        {
        }

        void GrabarTasaCambio()
        {
        }

        protected void lkGrabarTasaCambio_Click(object sender, EventArgs e)
        {
            GrabarTasaCambio();
        }

        protected void txtTasaCambio_TextChanged(object sender, EventArgs e)
        {
            GrabarTasaCambio();
        }

        protected void lkEnviarCorreoTasa_Click(object sender, EventArgs e)
        {
        }

    }
}
