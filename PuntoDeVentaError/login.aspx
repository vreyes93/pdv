<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="PuntoDeVenta.login" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="es-ES">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />

<title>Muebles Fiesta | Guatemala</title>
<meta http-equiv="content" content="Guatemala: Muebles Fiesta, Camas Guatemala, Camas, Camas Indufoam, Closets, Cocinas, Comedores, Dormitorios, Escritorios, Infantil, Salas, Muebles, Muebles en Guatemala, Módulos, Centros de Entretenimiento " />
<meta http-equiv="description" content="Guatemala: Muebles Fiesta, Camas Guatemala, Camas, Camas Indufoam, Closets, Cocinas, Comedores, Dormitorios, Escritorios, Infantil, Salas, Muebles, Muebles en Guatemala, Módulos, Centros de Entretenimiento" />
<meta http-equiv="keywords" content="Guatemala: Muebles Fiesta, Camas Guatemala, Camas, Camas Indufoam, Closets, Cocinas, Comedores, Dormitorios, Escritorios, Infantil, Salas, Muebles, Muebles en Guatemala, Módulos, Centros de Entretenimiento" />

<script type="text/javascript" src="Scripts/nn_jquery.js"></script> 
<script type="text/javascript" src="Scripts/mbScrollable.js"></script> 
<script type="text/javascript"> 
	jQuery.noConflict();
	jQuery(document).ready(function() {
        jQuery("#dsbs_rotator").mbScrollable({
            width: 985,
            elementsInPage: 1,
            elementMargin: 0,
            height: 188,
            controls: "#dsbs_rotatorcontrols",
            slideTimer: 600,
            autoscroll: true,
            scrollTimer: 10000,  });
    });
</script>
<style type="text/css"> 
	<!--
	#dsbs_rotatorhorizontal{height: autopx; width: 985px;}
	#dsbs_rotator span.description {width: 985px;}
	#dsbs_rotator img {border:0px none;}
	#dsbs_rotator .description {display:none!important; position: absolute;font-family:sans-serif;font-size:180%; color:#FFFFFF;padding:60px 60px 60px 60px;text-align:left;width:100%;}
	#dsbs_rotator .scrollEl {}
	#dsbs_rotatorcontrols {position:relative; padding:0 10px; margin:-42px 20px 0 0; text-align:right;}
	#dsbs_rotatorcontrols div {font-family:sans-serif;font-size:12px;background:#000;display:inline-block;padding:5px;cursor:pointer; filter:alpha(opacity=50); -moz-opacity:0.5;-khtml-opacity: 0.5;opacity: 0.5;}
	#dsbs_rotatorcontrols .sel {color:white !important;font-weight:bold;}
	#dsbs_rotatorcontrols .disabled {color:gray;}
	#dsbs_rotatorcontrols .page {padding:0 5px;color:gray; border:1px solid; margin:0 3px;float:left;}
	-->
    </style>

<style type="text/css">@import url(style.css);</style>
<style type="text/css"></style>
</head>

<body>

    <form id="form1" runat="server">

<div class="fullpage">

<div class="social">Vis�tanos en: <a href="http://www.facebook.com/home.php#!/pages/Muebles-Fiesta/175200984104" target="_blank"><img src="images/facebook.png" height="25" align="absmiddle" alt="FB" /></a>&nbsp;<a href="http://twitter.com/mueblesfiesta" target="_blank"><img src="images/twitter.png" height="25" align="absmiddle" alt="TW" /></a></div>

	<div class="header">
	
		<div class="header1"><a href="http://www.mueblesfiesta.com" target="_blank">
          <img runat="server" id="logo" src="images/logoMF.png" alt="Muebles Fiesta" title="Muebles Fiesta" width="250" height="96" visible="true" />
          <img runat="server" id="logoPruebas" src="images/logoMF_Pruebas.png" alt="Muebles Fiesta" title="Muebles Fiesta" width="336" height="96" visible="false" />
          <asp:Label ID="Label1" runat="server" ForeColor="Black" TabIndex="1090" Font-Bold="True" Font-Size="Large" Visible="false">PRUEBAS</asp:Label></a></div>
		
		<div class="header2">
                    <ul>
                        <li style="text-align: center;">&nbsp;<span style="font-size: 20px; font-weight: bold; text-shadow: 2px 2px 2px #FFFFFF;">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Punto de Venta</span><br />
                        </li>

                    </ul>
                    <div class="clear"></div>

                    <p><a href="http://www.mueblesfiesta.com" target="_blank">Sitio Web mueblesfiesta.com</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>

                    <div class="submenu">
                        <img src="images/mail.png" align="absmiddle" height="30" alt="Mail" />
                        <a href="https://merkava.mueblesfiesta.com/owa/auth/logon.aspx?url=https://merkava.mueblesfiesta.com/owa/&reason=0" target="_blank">Web Mail</a>&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label 
                            ID="lblUsuario" runat="server" Text="Bienvenido" ForeColor="White"></asp:Label>
                    </div>
		</div>
			
	</div>
<div class="contenthome">
    <h3>&nbsp;&nbsp;Ingreso al Sistema</h3>
    <p></p>
		<table align="center" runat="server" id="tblLogin">
            <tr>
                <td>
                    Usuario:</td>
                <td>
                    <asp:TextBox ID="txtUsuario" runat="server" style="text-transform: uppercase;" 
                        TabIndex="1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Contrase�a:</td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" TabIndex="10"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <asp:Button ID="btnLogin" runat="server" BackColor="#E35904" ForeColor="White" 
                        Text="Ingresar" onclick="btnLogin_Click" TabIndex="20" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Label ID="lbInfo" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        
		<table align="center" runat="server" id="tblCambiar" visible="false">
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2"><b>Cambiar Contrase�a</b></td>
            </tr>
            <tr>
                <td>
                    Nueva:</td>
                <td>
                    <asp:TextBox ID="txtNueva" runat="server" TextMode="Password" TabIndex="30" 
                        ToolTip="Ingrese aqu� su contrase�a nueva"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Contrase�a:</td>
                <td>
                    <asp:TextBox ID="txtNueva2" runat="server" TextMode="Password" TabIndex="40" 
                        ToolTip="Confirme aqu� su contrase�a nueva"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <asp:Button ID="btnCambiar" runat="server" BackColor="#E35904" ForeColor="White" 
                        Text="Cambiar Contrase�a" onclick="btnCambiar_Click" TabIndex="50" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <asp:Label ID="lbInfoCambiar" runat="server" Font-Bold="True" ForeColor="Black"></asp:Label>
                    <asp:Label ID="lbErrorCambiar" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
        

</div>
	<div class="footer">
	
	<ul>
		<li><a href="https://merkava.mueblesfiesta.com/owa/auth/logon.aspx?url=https://merkava.mueblesfiesta.com/owa/&reason=0" 
                target="_blank" tabindex="500">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Web Mail</a></li>
		<li><a href="http://www.mueblesfiesta.com" target="_blank" tabindex="510">Muebles Fiesta</a></li>
	</ul>
	
	</div>

</div>


<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
    _uacct = "UA-3401323-1";
    urchinTracker();
</script>
    
    </form>
    
</body></html>
