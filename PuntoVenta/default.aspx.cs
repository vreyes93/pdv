﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PuntoDeVenta
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Usuario"] == null) Response.Redirect("login.aspx");

                Label lbUsuario = (Label)this.Master.FindControl("lblUsuario");
                if (Convert.ToString(Session["NombreVendedor"]) == "Alerta") Session["Vendedor"] = null;
                lbUsuario.Text = string.Format("{0}  -  {1}", Convert.ToString(Session["NombreVendedor"]), Convert.ToString(Session["Tienda"]));
            }
        }
    }
}