﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace PuntoDeVenta
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["Usuario"] = null;
                Session["Tienda"] = null;
                Session["Vendedor"] = null;
                Session["NombreVendedor"] = null;
                Session["Cliente"] = null;
                Session["Pedido"] = null;
                Session["TipoVenta"] = null;
                Session["Financiera"] = null;
                Session["NivelPrecio"] = null;
                
                lbInfo.Text = "";
                lbError.Text = "";
                tblCambiar.Visible = false;
                ViewState["url"] = string.Format("http://sql.fiesta.local/services/wsPuntoVenta.asmx", Request.Url.Host);

                if (Convert.ToString(Session["InfoCambio"]) == "S") lbInfo.Text = "Debe ingresar de nuevo al sistema";

                txtUsuario.Focus();
            }
        }
        
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "";
                lbError.Text = "";
                
                SqlConnection conexion = new SqlConnection(string.Format("Data Source = (local); Initial Catalog = EXACTUSERP;User id = {0};password = {1}", txtUsuario.Text, txtPassword.Text));
                conexion.Open();
                conexion.Close();
            }
            catch 
            {
                lbError.Text = "Usuario o contraseña inválidos.";
                
                Session["Usuario"] = null;
                Session["Tienda"] = null;
                Session["Vendedor"] = null;
                Session["NombreVendedor"] = null;
                Session["Cliente"] = null;
                Session["Pedido"] = null;
                
                txtPassword.Text = "";
                txtPassword.Focus();

                return;
            }
            
            if (txtPassword.Text == "Estrella01")
            {
                lbInfoCambiar.Text = "";
                lbErrorCambiar.Text = "";
                txtNueva.Text = "";
                txtNueva2.Text = "";

                tblLogin.Visible = false;
                tblCambiar.Visible = true;
                lbInfoCambiar.Text = "Debe cambiar su contraseña";
                txtNueva.Focus();
            }
            else
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                //ws.Url = Convert.ToString(ViewState["url"]);

                lbError.Text = "";
                char[] mSeparador = { '|' };
                string[] mDatos = ws.DevuelveDatos(txtUsuario.Text.Trim().ToUpper()).Split(mSeparador);

                Session["Usuario"] = txtUsuario.Text.Trim().ToUpper();
                Session["Tienda"] = mDatos[0];
                Session["Vendedor"] = mDatos[1];
                Session["NombreVendedor"] = mDatos[2];
                Session["Cliente"] = null;
                Session["Pedido"] = null;
                Session["InfoCambio"] = null;

                Response.Redirect("default.aspx");
            }
        }

        protected void btnCambiar_Click(object sender, EventArgs e)
        {
            try
            {
                lbInfoCambiar.Text = "";
                lbErrorCambiar.Text = "";

                if (txtNueva.Text == "Estrella01")
                {
                    txtNueva.Text = "";
                    txtNueva2.Text = "";
                    lbErrorCambiar.Text = "La contraseña ingresada es inválida";
                    txtNueva.Focus();
                    return;
                }
                if (txtNueva.Text.Trim() != txtNueva2.Text.Trim())
                {
                    txtNueva.Text = "";
                    txtNueva2.Text = "";
                    lbErrorCambiar.Text = "Las contraseñas nuevas no coinciden";
                    txtNueva.Focus();
                    return;
                }

                System.Data.SqlClient.SqlCommand mCommand = new System.Data.SqlClient.SqlCommand();
                System.Data.SqlClient.SqlConnection mConexion = new System.Data.SqlClient.SqlConnection(string.Format("Data Source = (local); Initial Catalog = EXACTUSERP;User id = {0};password = {1}", "sa", "RestNET2015"));

                mConexion.Open();
                mCommand.Connection = mConexion;

                mCommand.CommandText = string.Format("EXEC sp_password null, '{0}', '{1}'", txtNueva.Text.Trim(), txtUsuario.Text);
                mCommand.ExecuteNonQuery();

                mConexion.Close();

                lbInfo.Text = "";
                lbError.Text = "";
                tblLogin.Visible = true;
                tblCambiar.Visible = false;

                Session["InfoCambio"] = "S";
                txtPassword.Text = "";
                txtPassword.Focus();
                lbInfo.Text = "Debe ingresar de nuevo al sistema";
            }
            catch (Exception ex)
            {
                lbErrorCambiar.Text = string.Format("Error al cambiar la contraseña {0}", ex.Message);
            }
        }

    }
}