﻿<%@ Page Title="Productos Múltiples" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">

        <a href="pedidos.aspx" title="Haga clic aquí para crear un pedido nuevo">Crear un pedido nuevo</a>
    </div>

    <div>
            <table align="left" id="tblCliente" runat="server" visible="false" >
            <tr>
                <td>Trabajar en cliente:&nbsp;</td>
                <td>
                    <asp:DropDownList ID="cbCliente" runat="server"></asp:DropDownList>
                </td>
                <td>
                    &nbsp;
                    <asp:LinkButton ID="lkCambiarCliente" runat="server" ToolTip="Haga clic aquí para cambiar de cliente" OnClick="lkCambiarCliente_Click">Cambiar</asp:LinkButton>
                    &nbsp;
                    <asp:Label ID="lbInfoCambio" runat="server"></asp:Label>
                </td>
            </tr>
        </table>

    </div>
</asp:Content>
