﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ValidarSesion();

            //Label lbUsuario = (Label)this.Master.FindControl("lblUsuario");
            //if (Convert.ToString(Session["NombreVendedor"]) == "Alerta") Session["Vendedor"] = null;
            //lbUsuario.Text = string.Format("{0}  -  {1}", Convert.ToString(Session["NombreVendedor"]), Convert.ToString(Session["Tienda"]));

            if (!Convert.ToBoolean(Session["UsuarioPM"])) Response.Redirect("pedidos.aspx");
            CargarClientes();
        }
    }

    void ValidarSesion()
    {
        if (Session["Usuario"] == null) Response.Redirect("login.aspx");
    }

    void CargarClientes()
    {
        bool mMostrar = false;
        var ws = new wsPuntoVentaMayoreo.wsPuntoVentaMayoreo();
        if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

        var q = ws.DevuelveClientesMayoreo(Convert.ToString(Session["Usuario"]), ref mMostrar);

        if (mMostrar)
        {
            tblCliente.Visible = true;
            cbCliente.DataSource = q;
            cbCliente.DataTextField = "Nombre";
            cbCliente.DataValueField = "Cliente";
            cbCliente.DataBind();
            
            cbCliente.SelectedValue = Convert.ToString(Session["Cliente"]);
            cbCliente.Focus();
        }
    }

    protected void lkCambiarCliente_Click(object sender, EventArgs e)
    {
        try
        {
            var ws = new wsPuntoVentaMayoreo.wsPuntoVentaMayoreo();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            string mMensaje = ""; string mNombre = "";
            lbInfoCambio.Text = "";

            if (!ws.CambiarCliente(Convert.ToString(Session["Usuario"]), cbCliente.SelectedValue, ref mMensaje, ref mNombre))
            {
                lbInfoCambio.Text = mMensaje;
                return;
            }

            Session["NombreCliente"] = mNombre;
            Session["Cliente"] = cbCliente.SelectedValue;

            lbInfoCambio.Text = mMensaje;
        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }
            lbInfoCambio.Text = string.Format("Error {0} {1}", ex.Message, m);
        }
    }
}