﻿<%@ Page Language="C#" Title="Productos Múltiples" AutoEventWireup="true" CodeFile="cambiapass.aspx.cs" Inherits="cambiapass" %>

<%@ Register assembly="DevExpress.Web.v18.1, Version=18.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Productos Múltiples</title>
    <webopt:bundlereference runat="server" path="~/Content/css" />

</head>
<body>
    <form runat="server">

        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" runat="server" href="login.aspx">Productos Múltiples, S.A.</a>
                </div>
            </div>
        </div>


        <div class="container body-content">

            <section id="loginForm">
                <div class="form-horizontal">
                    <h4>Utilice sus credenciales de acceso.</h4>
                    <hr />
                    <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                        <p class="text-danger">
                            <asp:Literal runat="server" ID="FailureText" />
                        </p>
                    </asp:PlaceHolder>
                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="txtPassword" CssClass="col-md-2 control-label">Nueva Contraseña:</asp:Label>
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="txtPassword" CssClass="form-control" TextMode="Password" Width="308px" />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="txtPassword2" CssClass="col-md-2 control-label">Confirmar contraseña:</asp:Label>
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="txtPassword2" TextMode="Password" CssClass="form-control" Width="308px" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <asp:Button runat="server" Text="Cambiar contraseña" CssClass="btn btn-primary btn-lg" ID="btnCambiar" OnClick="btnCambiar_Click" />
                            <asp:Button runat="server" Text="Regresar" CssClass="btn btn-primary btn-lg" ID="btnRegresar" OnClick="btnRegresar_Click" />
                            <p>
                                <asp:Label ID="lbInfo" runat="server"></asp:Label>
                                <asp:Label ID="lbError" runat="server" ForeColor="Red"></asp:Label>
                            </p>
                        </div>
                    </div>

                </div>
            </section>

            
    <div class="row">
        <div class="col-md-4">
            <img src="ïmg/logoIndufoam.png" style="align-content:center; text-align:center"/>
        </div>
        <div class="col-md-4">
            <img src="ïmg/logoSimmons.png" style="align-content:center; text-align:center" />
        </div>
    </div>

            <hr />
            <footer>
                <p>&copy; <%--To learn more about bundling scripts in ScriptManager see http://go.microsoft.com/fwlink/?LinkID=301884 --%>- Productos Múltiples, S.A.</p>
            </footer>
        </div>
    </form>
</body>
</html>
