﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            Session["Ambiente"] = WebConfigurationManager.AppSettings["Ambiente"].ToString();
            //Session["Ambiente"] = "PRO"; //PRODUCCION
            // Session["Ambiente"] = "PRU"; //PRUEBAS

            Session["Usuario"] = null;
            Session["NombreUsuario"] = null;
            Session["NombreCliente"] = null;
            Session["Cliente"] = null;
            Session["url"] = null;
            Session["EntroLogin"] = "S";

            txtUsuario.Focus();

        }
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        try
        {
            lbInfo.Text = "";

            Session["url"] = string.Format("http://sql.fiesta.local/services/wsPuntoVentaMayoreo.asmx", Request.Url.Host);

            if (Convert.ToString(Session["Ambiente"]) == "PRU") Session["Url"] = string.Format("http://sql.fiesta.local/servicesPruebas/wsPuntoVentaMayoreo.asmx", Request.Url.Host);
            else
            if (Convert.ToString(Session["Ambiente"]) == "DES") Session["Url"] = string.Format("http://localhost:58145/wsPuntoVentaMayoreo.asmx", Request.Url.Host);

            if (txtUsuario.Text.Trim().Length == 0)
            {
                lbInfo.Text = "Debe ingresar el usuario";
                return;
            }
            if (txtPassword.Text.Trim().Length == 0)
            {
                lbInfo.Text = "Debe ingresar la contraseña";
                return;
            }
            
            var ws = new wsPuntoVentaMayoreo.wsPuntoVentaMayoreo();
            ws.Url = Convert.ToString(Session["url"]);
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            string mNombre = ""; string mCliente = ""; string mNombreCliente = ""; string mMensaje = ""; string mCambiaPassword = ""; bool mUsuarioPM = false;

            if (!ws.LoginExitoso(txtUsuario.Text.Trim(), txtPassword.Text.Trim(), ref mNombre, ref mCliente, ref mNombreCliente, ref mMensaje, ref mCambiaPassword, ref mUsuarioPM))
            {
                lbInfo.Text = mMensaje;
                return;
            }

            Session["Usuario"] = txtUsuario.Text.Trim();

            if (mCambiaPassword == "S")
            {
                Response.Redirect("cambiapass.aspx");
            }
            else
            {
                Session["NombreUsuario"] = mNombre;
                Session["NombreCliente"] = mNombreCliente;
                Session["Cliente"] = mCliente;
                Session["Mensaje"] = "";

                if (Request.QueryString["pe"] == null)
                {
                    if (mUsuarioPM)
                    {
                        Session["UsuarioPM"] = true;
                        Response.Redirect("default.aspx", false);
                    }
                    else
                    {
                        Session["UsuarioPM"] = false;
                        Response.Redirect("pedidos.aspx", false);
                    }
                }
                else
                {
                    Session["UsuarioPM"] = true;
                    if (!mUsuarioPM) Session["UsuarioPM"] = false;
                    Response.Redirect(string.Format("pedidos.aspx?pe={0}", Convert.ToString(Request.QueryString["pe"])), false);
                }
            }

        }
        catch (Exception ex)
        {
            lbInfo.Text = string.Format("Error {0}", ex.Message);
        }
    }

    protected void lkOlvidoPassword_Click(object sender, EventArgs e)
    {
        Response.Redirect("olvidopass.aspx");
    }
}