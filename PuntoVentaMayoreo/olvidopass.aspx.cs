﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class olvidopass : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["EntroLogin"] == null) Response.Redirect("login.aspx");
            HookOnFocus(this.Page as Control);
            txtUsuario.Focus();
        }

        Page.ClientScript.RegisterStartupScript(
            typeof(olvidopass),
            "ScriptDoFocus",
            SCRIPT_DOFOCUS.Replace("REQUEST_LASTFOCUS", Request["__LASTFOCUS"]),
            true);
    }

    private const string SCRIPT_DOFOCUS = @"window.setTimeout('DoFocus()', 1); function DoFocus() {
            try {
                document.getElementById('REQUEST_LASTFOCUS').focus();
            } catch (ex) {}
        }";

    private void HookOnFocus(Control CurrentControl)
    {
        //checks if control is one of TextBox, DropDownList, ListBox or Button
        if ((CurrentControl is TextBox) ||
            (CurrentControl is DropDownList) ||
            (CurrentControl is ListBox) ||
            (CurrentControl is Button))
            //adds a script which saves active control on receiving focus 
            //in the hidden field __LASTFOCUS.
            (CurrentControl as WebControl).Attributes.Add(
               "onfocus",
               "try{document.getElementById('__LASTFOCUS').value=this.id} catch(e) {}");
        //checks if the control has children
        if (CurrentControl.HasControls())
            //if yes do them all recursively
            foreach (Control CurrentChildControl in CurrentControl.Controls)
                HookOnFocus(CurrentChildControl);
    }

    protected void btnRestablecer_Click(object sender, EventArgs e)
    {
        try
        {
            lbError.Text = "";
            lbInfo.Text = "";

            string mMensaje = "";
            var ws = new wsPuntoVentaMayoreo.wsPuntoVentaMayoreo();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            if (!ws.ResetPassword(txtUsuario.Text.Trim(), ref mMensaje))
            {
                lbError.Text = mMensaje;
                return;
            }

            btnRestablecer.Visible = false;

            lbInfo.Text = mMensaje;
            btnRegresar.Focus();
        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }
            lbInfo.Text = string.Format("{0} {1}", ex.Message, m);
        }
    }

    protected void btnRegresar_Click(object sender, EventArgs e)
    {
        Response.Redirect("login.aspx");
    }
    protected void txtUsuario_TextChanged(object sender, EventArgs e)
    {
        try
        {
            lbInfo.Text = "";
            lbError.Text = "";

            var ws = new wsPuntoVentaMayoreo.wsPuntoVentaMayoreo();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            txtEmail.Text = "";

            string mMail = ws.DevuelveCorreoUsuario(txtUsuario.Text.ToLower());

            if (mMail.Contains("*"))
            {
                txtEmail.Text = mMail;
            }
            else
            {
                lbError.Text = mMail;
            }

            btnRestablecer.Focus();
        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }
            lbError.Text = string.Format("{0} {1}", ex.Message, m);
        }
    }
}