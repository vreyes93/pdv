﻿<%@ Page Title="Pedidos" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="pedidos.aspx.cs" Inherits="pedidos" MaintainScrollPositionOnPostback="true"%>
<%@ Register assembly="DevExpress.Web.v18.1, Version=18.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type='text/javascript'>
        
        function OnBatchEditEndEditing(s, e) {
            setTimeout(function () {

                //Si el resultado del PageMethod es "N" quiero que se vaya a login.aspx

                s.UpdateEdit();
            }, 0);
        }

        $.ajax({
            type: "POST",
            url: "./pedidos.aspx/SesionActiva",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                ('input[type=text],select,input[type=checkbox],input[type=radio]').attr('disabled', result.d);
            }
        });
    </script>
    <style type="text/css">
        .auto-style1 {
            width: 140px;
            font-size:small;
        }
        .auto-style2 {
            width: 8px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="jumbotron">
        <asp:LinkButton ID="lbPedido" runat="server" Text="Pedidos - La Colchonería" style="font-size: x-large;" EnableTheming="True" ForeColor="#0072C6" OnClick="lbPedido_Click"></asp:LinkButton>
        <%--<dx:ASPxLabel ID="lbPedido" runat="server" Text="Pedidos - La Colchonería" style="font-size: x-large;" EnableTheming="True" ForeColor="#0072C6" Theme="MetropolisBlue" ></dx:ASPxLabel>--%>
        
        <table align="center">
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td colspan="2">

                <table align="left">
                    <tr runat="server" id="TrVendedorCemaco" visible="false">
                        <td colspan="2" style="width:100px;" class="auto-style1">
                            Vendedor:
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="revVendedor" Text="* Seleccione al vendedor que realizó la venta"  ControlToValidate="cbVendedor"
                                runat="server" CssClass="alert-danger" Display="Dynamic" Font-Size="Small" Enabled="false"
                                ErrorMessage="*Seleccione el vendedor para la comisión de este pedido."></asp:RequiredFieldValidator>
                            <dx:ASPxComboBox ID="cbVendedor"  runat="server" ValueType="System.String" Theme="MetropolisBlue" Width="250px" DropDownStyle="DropDown" OnSelectedIndexChanged="cbVendedor_SelectedIndexChanged">
                                <HelpTextSettings DisplayMode="Popup" Position="Top">
                                </HelpTextSettings>
                                <HelpTextStyle Font-Size="8pt">
                                </HelpTextStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td colspan="1" style="width: 70px;">
                            Tipo:</td><td>
                            <asp:RequiredFieldValidator ID="revTipoVenta" runat="server" Enabled="false"
                                ControlToValidate="rdlTipoVenta" ErrorMessage="* Elija una opción" CssClass="alert-danger" Display="Dynamic" Font-Size="Small"></asp:RequiredFieldValidator>
                                <asp:RadioButtonList ID="rdlTipoVenta" runat="server" BorderStyle="None" BorderWidth="0px" CellPadding="5" CellSpacing="0" CssClass="list-inline" Font-Size="Small" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="rdlTipoVenta_SelectedIndexChanged1">
                                <asp:ListItem Value="1">Venta normal</asp:ListItem>
                                <asp:ListItem Value="2">Venta a Colaborador</asp:ListItem>
                            </asp:RadioButtonList>
                                </td>
                    </tr>
                </table>
                           
                            
                           
            </td></tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td colspan="2">
                    <dx:ASPxGridView ID="gridArticulos" runat="server" KeyFieldName="Articulo" OnBatchUpdate="gridArticulos_BatchUpdate" OnRowUpdating="gridArticulos_RowUpdating" EnableTheming="True" Theme="MetropolisBlue" Width="550px" AutoGenerateColumns="False"  OnCellEditorInitialize="gridArticulos_CellEditorInitialize" OnStartRowEditing="gridArticulos_StartRowEditing" OnDataBinding="gridArticulos_DataBinding" OnCustomColumnDisplayText="gridArticulos_CustomColumnDisplayText" >
                        <SettingsSearchPanel Visible="true" ShowApplyButton="true" ShowClearButton="true" />
                        <EditFormLayoutProperties ColCount="1"></EditFormLayoutProperties>
                        <Columns>
                            <dx:GridViewDataColumn FieldName="Articulo" ReadOnly="true" Caption="Artículo" visible="false" Width="0px" />
                            <dx:GridViewDataColumn FieldName="Descripcion" ReadOnly="true" Caption="Descripción" Width="185px" VisibleIndex="5" >
                                
                            </dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="PrecioBase" ReadOnly="true" Caption="PrecioBase" Visible="false" Width="0px" VisibleIndex="0" />
                            <dx:GridViewDataColumn FieldName="PrecioLista" ReadOnly="true" Caption="PrecioLista" Visible="false" Width="0px" VisibleIndex="1" />
                            <dx:GridViewDataColumn FieldName="TieneRegalo" Width="0px" Caption="TieneRegalo" ReadOnly="True" Visible="False" VisibleIndex="2" >
                            </dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="Regalo" ReadOnly="true" Caption="Regalo" Visible="false" Width="0px" VisibleIndex="3" />
                            <dx:GridViewDataColumn FieldName="CantidadRegalo" ReadOnly="true" Caption="CantidadRegalo" Visible="false" Width="0px" VisibleIndex="4" />
                            <dx:GridViewDataSpinEditColumn FieldName="Cantidad" VisibleIndex="6" Width="30px">
                                <PropertiesSpinEdit MinValue="0" MaxValue="999999" NumberType="Float">
                                    <ValidationSettings ErrorText="La cantidad debe estar entre 0....999999" ValidateOnLeave="true"></ValidationSettings>
                                </PropertiesSpinEdit>
                                <HeaderStyle BackColor="#0072C6" ForeColor="White" HorizontalAlign="Right" />
                            </dx:GridViewDataSpinEditColumn>
                            <dx:GridViewDataColumn FieldName="IdDescuento" ReadOnly="true" Caption="IdDescuento" Visible="false" Width="0px" VisibleIndex="4" />
                             <dx:GridViewDataComboBoxColumn FieldName="Descuento"  ReadOnly="false" Caption="Descuento" Visible="true" Width="50px" >
                            <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="60" DropDownRows="25" ValueField="IdDescuento" TextField="Descuento"/>
                            <HeaderStyle HorizontalAlign="Left" >
                                <Border BorderStyle="None"></Border>
                            </HeaderStyle>
                            <CellStyle HorizontalAlign="Left" Font-Size="Small" />
                        </dx:GridViewDataComboBoxColumn>
                            
                        </Columns>
                        <Styles>
                            <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                            <Header BackColor="#0072C6"  ForeColor="White"></Header>
                        </Styles>
                        <SettingsBehavior EnableRowHotTrack="true" />

<SettingsAdaptivity>
<AdaptiveDetailLayoutProperties ColCount="1"></AdaptiveDetailLayoutProperties>
</SettingsAdaptivity>

                        <SettingsPager NumericButtonCount="3">
                            <Summary Visible="False" />
                            <PageSizeItemSettings Items="10, 20">
                            </PageSizeItemSettings>
                        </SettingsPager>
                        <SettingsEditing Mode="Batch" />
                        <ClientSideEvents BatchEditEndEditing="OnBatchEditEndEditing" />
                        <SettingsText SearchPanelEditorNullText="Ingrese aquí su criterio de búsqueda" CommandBatchEditUpdate="Agregar artículos" CommandCancel="Cancelar cambios" CommandApplySearchPanelFilter="Buscar" CommandClearFilter="Limpiar filtro" CommandClearSearchPanelFilter="Limpiar" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"/>
                    </dx:ASPxGridView>
                    <dx:ASPxGridView ID="gridPedido" runat="server" KeyFieldName="Articulo" OnBatchUpdate="gridPedido_BatchUpdate" OnRowUpdating="gridPedido_RowUpdating" Visible="False" SettingsPager-PageSize="10" EnableTheming="True" Theme="MetropolisBlue" Width="550px" SettingsPager-ShowNumericButtons="true" AutoGenerateColumns="False" OnCellEditorInitialize="gridPedido_CellEditorInitialize" >
                        <SettingsSearchPanel Visible="true" ShowApplyButton="true" ShowClearButton="true" />
                        <Columns>
                            <dx:GridViewDataColumn FieldName="Articulo" ReadOnly="true" Caption="Artículo" Visible="false" Width="0px" />
                            <dx:GridViewDataColumn FieldName="Descripcion" ReadOnly="true" Caption="Descripción" Width="385px" VisibleIndex="5" >
                                <HeaderStyle BackColor="#0072C6" ForeColor="White" />
                            </dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="PrecioBase" ReadOnly="true" Caption="PrecioBase" Visible="false" Width="0px" VisibleIndex="0" />
                            <dx:GridViewDataColumn FieldName="PrecioLista" ReadOnly="true" Caption="PrecioLista" Visible="false" Width="0px" VisibleIndex="1" />
                            <dx:GridViewDataSpinEditColumn FieldName="Cantidad" VisibleIndex="6" Width="30px">
                                <PropertiesSpinEdit MinValue="0" MaxValue="999999" NumberType="Float">
                                    <ValidationSettings ErrorText="La cantidad debe estar entre 0....999999" ValidateOnLeave="true"></ValidationSettings>
                                </PropertiesSpinEdit>
                                <HeaderStyle BackColor="#0072C6" ForeColor="White" HorizontalAlign="Right" />
                            </dx:GridViewDataSpinEditColumn>
                            <dx:GridViewDataColumn FieldName="TieneRegalo" ReadOnly="true" Caption="TieneRegalo" Visible="false" Width="0px" VisibleIndex="2" />
                            <dx:GridViewDataColumn FieldName="Regalo" ReadOnly="true" Caption="Regalo" Visible="false" Width="0px" VisibleIndex="3" />
                            <dx:GridViewDataColumn FieldName="CantidadRegalo" ReadOnly="true" Caption="CantidadRegalo" Visible="false" Width="0px" VisibleIndex="4" />
                            <dx:GridViewDataColumn FieldName="IdDescuento" ReadOnly="true" Caption="IdDescuento" Visible="false" Width="0px" VisibleIndex="4" />
                             <dx:GridViewDataComboBoxColumn FieldName="Descuento"  ReadOnly="false" Caption="Descuento" Visible="true" Width="50px" >
                            <PropertiesComboBox DropDownStyle="DropDownList" DropDownWidth="60" DropDownRows="25" ValueField="IdDescuento" TextField="Descuento"/>
                            <HeaderStyle HorizontalAlign="Left" >
                                <Border BorderStyle="None"></Border>
                            </HeaderStyle>
                            <CellStyle HorizontalAlign="Left" Font-Size="Small" />
                        </dx:GridViewDataComboBoxColumn>
                        </Columns>
                        <Styles>
                            <RowHotTrack BackColor="#d9ecff"></RowHotTrack>
                             <Header BackColor="#0072C6"  ForeColor="White"></Header>
                        </Styles>
                        <SettingsBehavior EnableRowHotTrack="true" />
                        <SettingsEditing Mode="Batch" />
                        <SettingsText SearchPanelEditorNullText="Ingrese aquí su criterio de búsqueda" CommandBatchEditUpdate="Aplicar cambios" CommandCancel="Cancelar cambios" CommandClearFilter="Limpiar filtro" CommandApplySearchPanelFilter="Buscar" CommandClearSearchPanelFilter="Limpiar" ConfirmOnLosingBatchChanges="Desea salir sin guardar los cambios realizados?"/>
                    </dx:ASPxGridView>
                </td>
                <td>&nbsp;</td>
            </tr>
            
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td colspan="2" style="font-size: large;">
                    Orden de compra:
                    <asp:TextBox ID="txtOrdenCompra" runat="server" TextMode="MultiLine" Width="400px" TabIndex="30" Font-Size="X-Small" MaxLength="399" Height="72px"></asp:TextBox>
                  <%--  <dx:ASPxTextBox ID="txtOrdenCompra" runat="server" EnableTheming="True" Theme="MetropolisBlue" Width="550px" TabIndex="30" ></dx:ASPxTextBox>--%>
                    
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td colspan="2">
                    Observaciones:
                    <asp:TextBox ID="txtObservaciones" runat="server" TextMode="MultiLine" Width="400px" TabIndex="30" Font-Size="X-Small" MaxLength="399" Height="72px"></asp:TextBox>
                    <%--<dx:ASPxTextBox ID="txtObservaciones" runat="server" EnableTheming="True" Theme="MetropolisBlue" Width="550px" TabIndex="40" ></dx:ASPxTextBox>--%>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td>
                    <table id="tblCemaco" runat="server" visible="false">
                        <tr>
                            <td class="auto-style1">
                                Tienda:
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txtCemacoTienda" runat="server" EnableTheming="True" Theme="MetropolisBlue" Width="140px" TabIndex="50" ></dx:ASPxTextBox>
                            </td>
                            <td colspan="2" class="auto-style1">
                                Fecha venta:
                            </td>
                            <td colspan="2">
                                <dx:ASPxDateEdit ID="CemacoFechaVenta" runat="server" Theme="MetropolisBlue" Width="120px" TabIndex="60" DisplayFormatString="dd/MM/yyyy">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                            <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">
                                Caja:
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txtCemacoCaja" runat="server" EnableTheming="True" Theme="MetropolisBlue" Width="140px" TabIndex="70" ></dx:ASPxTextBox>
                            </td>
                            <td colspan="2">
                                Transacción:
                            </td>
                            <td colspan="2">
                                <dx:ASPxTextBox ID="txtCemacoTransaccion" runat="server" EnableTheming="True" Theme="MetropolisBlue" Width="120px" TabIndex="80" ></dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">
                                Teléfonos:
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txtCemacoClienteTelefono" runat="server" EnableTheming="True" Theme="MetropolisBlue" Width="140px" TabIndex="84" ></dx:ASPxTextBox>
                            </td>
                            <td colspan="2">
                                Fecha entrega:
                            </td>
                            <td colspan="2">
                                <dx:ASPxCheckBox ID="cbPendiente" runat="server" Theme="MetropolisBlue" TabIndex="85" Text="Entrega pendiente" ToolTip="Indique si dejará pendiente la fecha de entrega de este pedido"></dx:ASPxCheckBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">
                                Nombre:
                            </td>
                            <td colspan="3">
                                <dx:ASPxTextBox ID="txtCemacoClienteNombre" runat="server" EnableTheming="True" Theme="MetropolisBlue" Width="280px" TabIndex="90" ></dx:ASPxTextBox>
                            </td>
                            <td colspan="2">
                                <dx:ASPxDateEdit ID="CemacoFechaEntrega" runat="server" Theme="MetropolisBlue" Width="120px" TabIndex="92" ToolTip="Ingrese la fecha de entrega de este pedido" DisplayFormatString="dd/MM/yyyy">
                                    <CalendarProperties EnableMonthNavigation="true" EnableYearNavigation="true" 
                                        ShowClearButton="False" ShowDayHeaders="true" ShowTodayButton="true" 
                                        ShowWeekNumbers="False" >
                                        <Style Wrap="True"></Style>
                                        <MonthGridPaddings Padding="3px" />
                                        <DayStyle Font-Size="11px">
                                            <Paddings Padding="3px" />
                                        </DayStyle>
                                    </CalendarProperties>
                                </dx:ASPxDateEdit>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">
                                Dirección:
                            </td>
                            <td colspan="5">
                                <dx:ASPxTextBox ID="txtCemacoClienteDireccion" runat="server" EnableTheming="True" Theme="MetropolisBlue" Width="405px" TabIndex="120" ></dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">
                                Zona:</td>
                            <td>
                                <dx:ASPxComboBox ID="cbZona" runat="server" Theme="MetropolisBlue" Width="140px" 
                                    DropDownWidth="40px" DropDownRows="25" TabIndex="121" SelectedIndex="0" >
                                    <Items>
                                        <dx:ListEditItem Selected="True" Text="Seleccione zona ..." Value="" />
                                        <dx:ListEditItem Text="00" Value="00" />
                                        <dx:ListEditItem Text="01" Value="01" />
                                        <dx:ListEditItem Text="02" Value="02" />
                                        <dx:ListEditItem Text="03" Value="03" />
                                        <dx:ListEditItem Text="04" Value="04" />
                                        <dx:ListEditItem Text="05" Value="05" />
                                        <dx:ListEditItem Text="06" Value="06" />
                                        <dx:ListEditItem Text="07" Value="07" />
                                        <dx:ListEditItem Text="08" Value="08" />
                                        <dx:ListEditItem Text="09" Value="09" />
                                        <dx:ListEditItem Text="10" Value="10" />
                                        <dx:ListEditItem Text="11" Value="11" />
                                        <dx:ListEditItem Text="12" Value="12" />
                                        <dx:ListEditItem Text="13" Value="13" />
                                        <dx:ListEditItem Text="14" Value="14" />
                                        <dx:ListEditItem Text="15" Value="15" />
                                        <dx:ListEditItem Text="16" Value="16" />
                                        <dx:ListEditItem Text="17" Value="17" />
                                        <dx:ListEditItem Text="18" Value="18" />
                                        <dx:ListEditItem Text="19" Value="19" />
                                        <dx:ListEditItem Text="20" Value="20" />
                                        <dx:ListEditItem Text="21" Value="21" />
                                        <dx:ListEditItem Text="22" Value="22" />
                                        <dx:ListEditItem Text="23" Value="23" />
                                        <dx:ListEditItem Text="24" Value="24" />
                                        <dx:ListEditItem Text="25" Value="25" />
                                    </Items>
                                </dx:ASPxComboBox>                            
                            </td>
                            <td>
                                <dx:ASPxCheckBox ID="cbSegundoPiso" runat="server" Theme="MetropolisBlue" TabIndex="122" Text="Segundo piso" ToolTip="Indique si el artículo se entregará en un segundo piso"></dx:ASPxCheckBox>
                            </td>
                            <td colspan="2">
                                <dx:ASPxCheckBox ID="cbEntraCamion" runat="server" Theme="MetropolisBlue" TabIndex="123" Text="Camión" ToolTip="Indique si en la dirección del cliente entra camión"></dx:ASPxCheckBox>
                            </td>
                            <td>
                                <dx:ASPxCheckBox ID="cbEntraPickup" runat="server" Theme="MetropolisBlue" TabIndex="124" Text="Pickup" ToolTip="Indique si en la dirección del cliente entra un pickup"></dx:ASPxCheckBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">
                                Departamento:
                            </td>
                            <td>
                                <dx:ASPxComboBox ID="cbDepartamento" runat="server" Theme="MetropolisBlue" ValueType="System.String" Width="140px" DropDownStyle="DropDownList" 
                                    DropDownWidth="40" DropDownRows="25" TabIndex="125" OnValueChanged="cbDepartamento_ValueChanged" AutoPostBack="True">
                                </dx:ASPxComboBox>
                            </td>
                            <td colspan="2">
                                Municipio:
                            </td>
                            <td colspan="2">
                                <dx:ASPxComboBox ID="cbMunicipio" runat="server" Theme="MetropolisBlue" ValueType="System.String" Width="120px" DropDownStyle="DropDownList" 
                                    DropDownWidth="40" DropDownRows="25" TabIndex="126">
                                </dx:ASPxComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">
                                Indicaciones:
                            </td>
                            <td colspan="5">
                                <dx:ASPxTextBox ID="txtIndicaciones" runat="server" EnableTheming="True" Theme="MetropolisBlue" Width="405px" TabIndex="127" ToolTip="Ingrese algunas indicaciones que le ayuden al transportista a encontrar la dirección del cliente" ></dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">
                                Archivo:
                            </td>
                            <td colspan="5">
                                <dx:ASPxUploadControl runat="server" NullText="Clic aqu&#237; para examinar…" Width="405px" Theme="MetropolisBlue" TabIndex="128" ToolTip="Cargar archivo" ID="uploadFile">
                                    <ValidationSettings AllowedFileExtensions=".jpg, .jpeg, .bmp, .gif">
                                    </ValidationSettings>
                                    <Border BorderStyle="None">
                                    </Border>
                                </dx:ASPxUploadControl>
                            </td>
                        </tr>
                        
                    </table>
                    <br />
                    <table id="tblModificar" runat="server">
                        <tr>
                            <td class="auto-style1">
                                Pedido a modificar:
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txtPedido" runat="server" EnableTheming="True" Theme="MetropolisBlue" Width="140px" TabIndex="129" ></dx:ASPxTextBox>
                            </td>
                            <td colspan="2">                                
                                <asp:Button ID="btnPedido" runat="server" CssClass="btn btn-primary btn-lg" Text="Seleccionar pedido" Font-Size="Small" ToolTip="Selecciona el pedido para modificarlo" OnClick="btnPedido_Click" BackColor="#0072C6" TabIndex="130" />
                            </td>
                            <td colspan="2">
                            </td>
                        </tr>
                    </table>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert-danger" DisplayMode="List" Font-Size="10pt"  />
                </td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td>
                    <asp:Button ID="btnPreliminar" runat="server" CssClass="btn btn-primary btn-lg" Text="Preliminar" Font-Size="Small" ToolTip="Muestra un preliminar de su pedido" OnClick="btnPreliminar_Click" BackColor="#0072C6" TabIndex="135" />
                    <asp:Button ID="btnGrabar" runat="server" CssClass="btn btn-primary btn-lg" Text="Enviar" Font-Size="Small" ToolTip="Graba y envía a Productos Múltiples su pedido" Visible="false" OnClick="btnGrabar_Click" BackColor="#0072C6" TabIndex="140" />
                    <asp:Button ID="btnAgregar" runat="server" CssClass="btn btn-primary btn-lg" Text="Agregar más" Font-Size="Small" ToolTip="Agregar más artículos a su pedido" Visible="false" OnClick="btnAgregar_Click" BackColor="#0072C6" TabIndex="150" />
                    <asp:Label ID="lbInfo" runat="server" Font-Size="Small"></asp:Label>
                    <asp:Label ID="lbError" runat="server" ForeColor="Red" Font-Size="Small"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            </table>
        

    </div>

</asp:Content>

