﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Web.UI.WebControls;
using DevExpress.Web.Data;
using DevExpress.Web;
using System.Web.Services;

public partial class pedidos : System.Web.UI.Page
{
    protected List<ItemArticulo> DatosArticulos
    {
        get
        {
            var key = "34FAA431-CF79-4869-9488-93F6AAE81263";
            if (!IsPostBack || Session[key] == null)
            {
                var ws = new wsPuntoVentaMayoreo.wsPuntoVentaMayoreo();
                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

                var wsPDV = new wsPuntoVenta.wsPuntoVenta();
                wsPDV.Url = wsPDV.Url.Replace("Mayoreo", "");

                if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") wsPDV.Url = Convert.ToString(Session["url"]).Replace("Mayoreo", "");
                var qq = wsPDV.DevuelvePedidoLinea(txtPedido.Text.Trim());

                var q = ws.DevuelveArticulosMayoreo(Convert.ToString(Session["Cliente"])).ToList();

                List<ItemArticulo> lista = new List<ItemArticulo>();

                for (int ii = 0; ii < q.Count(); ii++)
                {
                    ItemArticulo item = new ItemArticulo();
                    item.Articulo = q[ii].Articulo;
                    item.Descripcion = q[ii].Descripcion;
                    item.PrecioBase = q[ii].PrecioBase;
                    item.PrecioLista = q[ii].PrecioLista;
                    item.TieneRegalo = q[ii].TieneRegalo;
                    item.Cantidad = q[ii].Cantidad;
                    item.Regalo = q[ii].Regalo;
                    item.CantidadRegalo = q[ii].CantidadRegalo;
                    item.Tipo = q[ii].Tipo;
                    lista.Add(item);
                }

                Session[key] = lista;
            }

            return (List<ItemArticulo>)Session[key];
        }
    }

    protected List<ItemPedido> DatosPedido
    {
        get
        {
            var key = "34FAA431-CF79-4869-9488-93F6AAE81264";
            if (!IsPostBack || Session[key] == null)
            {
                List<ItemPedido> lista = new List<ItemPedido>();
                Session[key] = lista;
            }

            return (List<ItemPedido>)Session[key];
        }
    }
    private void CargarUsuarios(string cliente)
    {
        try
        {
            var ws = new wsPuntoVentaMayoreo.wsPuntoVentaMayoreo();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.ObtenerUsuariosMayoreo((Session["Cliente"].ToString())).ToList();

            cbVendedor.DataSource = q;
            cbVendedor.TextField = "Nombre";
            cbVendedor.ValueField = "UsuarioId";
            cbVendedor.DataBind();
            cbVendedor.HelpText = "--Seleccione--";
        }
        catch
        { }
    }
    private void CargarTiposDescuento(string cliente, ref DataSet dsDescuento)
    {
        dsDescuento = new DataSet();
        DataTable dtDescuento = new DataTable("Descuento");


        dtDescuento.Columns.Add("IdDescuento", typeof(System.Int32));
        dtDescuento.Columns.Add("Descuento", typeof(System.String));


        try
        {
            var ws = new wsPuntoVentaMayoreo.wsPuntoVentaMayoreo();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            var q = ws.ObtenerTipoDescuentoMayoreo(cliente).ToList();
            foreach (var item in q)
            {
                if (rdlTipoVenta.SelectedIndex < 1)
                {
                    if (!item.Descuento.Contains("Colaborador")) //todos los descuentos menos el de colaborador
                        dtDescuento.Rows.Add(item.IdDescuento, item.Descuento);
                }
                else
                    if (item.Descuento.Contains("Colaborador"))//agregar solo el descuento del colaborador
                {
                    dtDescuento.Rows.Add(item.IdDescuento, item.Descuento);
                }
                else
                if (item.Descuento.Contains("Exhi"))//agregar solo el descuento del colaborador
                {
                    dtDescuento.Rows.Add(item.IdDescuento, item.Descuento);
                }
            }
            dsDescuento.Tables.Add(dtDescuento);
            Session["Descuento"] = dsDescuento;
        }
        catch (Exception ex)
        {
            var i = ex.Message;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            ValidarSesion();
            lbPedido.Text = string.Format("{0}{1}  -  {2}, por favor ingrese su pedido:", Session["NombreCliente"].ToString(), Session["NombreCliente"].ToString().Substring(Session["NombreCliente"].ToString().Length - 1, 1) == "." ? "" : ",", Session["NombreUsuario"].ToString());

            gridArticulos.DataSource = DatosArticulos;
            gridArticulos.DataBind();

            gridPedido.DataSource = DatosPedido;
            gridPedido.DataBind();

            if (!IsPostBack)
            {
                CargarDepartamentos();
                CargarUsuarios(Session["Cliente"].ToString());



                lbInfo.Text = Convert.ToString(Session["Mensaje"]);
                Session["Mensaje"] = "";

                CemacoFechaVenta.Value = DateTime.Now.Date;
                CemacoFechaEntrega.Value = DateTime.Now.Date.AddDays(2);
                if (validarCliente(Session["Cliente"].ToString()))
                    tblCemaco.Visible = true;

                if (Request.QueryString["pe"] != null)
                {
                    txtPedido.Text = Convert.ToString(Request.QueryString["pe"]);
                    SeleccionarPedido();

                    gridArticulos.DataSource = DatosArticulos;
                    gridArticulos.DataBind();

                    gridPedido.DataSource = DatosPedido;
                    gridPedido.DataBind();
                    tblModificar.Visible = true;
                }
                else
                    tblModificar.Visible = false;


                if (Session["Cliente"].ToString().Equals("MA0017"))
                {
                    TrVendedorCemaco.Visible = true;
                    revTipoVenta.Enabled = true;
                    revVendedor.Enabled = true;
                    gridArticulos.Columns["Descuento"].Visible = true;
                    gridArticulos.Columns["Descuento"].Visible = true;
                }
                else
                {
                    TrVendedorCemaco.Visible = false;
                    revTipoVenta.Enabled = false;
                    revVendedor.Enabled = false;
                    gridArticulos.Columns["Descuento"].Visible = false;
                    gridPedido.Columns["Descuento"].Visible = false;
                }

            }
            CargarDescuentos();
            CargarDescuentosPedido();
        }
        catch (Exception ex)
        {
            btnGrabar.Visible = false;
            btnPreliminar.Visible = false;
            lbError.Text = string.Format("Sesión expirada, por favor salga del sistema e ingrese de nuevo. {0}", ex.Message);
            //Response.Redirect("login.aspx");
        }
    }
    public void CargarDescuentos()
    {
        DataSet dsDescuento = new DataSet();
        CargarTiposDescuento("", ref dsDescuento);
        (gridArticulos.Columns["Descuento"] as GridViewDataComboBoxColumn).PropertiesComboBox.DataSource = dsDescuento;

    }
    public void CargarDescuentosPedido()
    {
        try
        {
            DataSet dsDescuento = new DataSet();
            if (Session["Cliente"].ToString().Equals("MA0017"))
                CargarTiposDescuento(Session["Cliente"].ToString(), ref dsDescuento);
            else
                CargarTiposDescuento("", ref dsDescuento);

            (gridPedido.Columns["Descuento"] as GridViewDataComboBoxColumn).PropertiesComboBox.DataSource = dsDescuento;
        }
        catch
        { }

    }
    void ValidarSesion()
    {
        if (Session["Usuario"] == null) Response.Redirect("login.aspx");
    }

    [WebMethod]
    public string SesionActiva()
    {
        string mSesionActiva = "S";
        if (Session["Usuario"] == null) mSesionActiva = "N";
        return mSesionActiva;
    }
    public bool validarCliente(string cliente)
    {
        var ws = new wsPuntoVentaMayoreo.wsPuntoVentaMayoreo();
        if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

        var lstClientes = ws.DevuelveClientesEspeciales().ToList();
        foreach (var item in lstClientes)
        {
            if (item.Equals(Session["Cliente"].ToString()))
            {
                return true;
            }
        }
        return false;
    }
    void CargarDepartamentos()
    {
        var ws = new wsPuntoVentaMayoreo.wsPuntoVentaMayoreo();
        if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

        var q = ws.DevuelveDepartamentos().ToList();

        cbDepartamento.DataSource = q;
        cbDepartamento.TextField = "Departamento";
        cbDepartamento.ValueField = "Departamento";
        cbDepartamento.DataBind();

        cbDepartamento.Value = "GUATEMALA";

        CargaMunicipios();
        cbMunicipio.Value = "GUATEMALA";
    }

    void CargaMunicipios()
    {
        var ws = new wsPuntoVentaMayoreo.wsPuntoVentaMayoreo();
        if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

        var q = ws.DevuelveMunicipios(cbDepartamento.Value.ToString()).ToList();

        cbMunicipio.DataSource = q;
        cbMunicipio.TextField = "Municipio";
        cbMunicipio.ValueField = "Municipio";
        cbMunicipio.DataBind();

        cbMunicipio.Value = q[0].Municipio;
    }

    protected void gridArticulos_BatchUpdate(object sender, DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs e)
    {
        try
        {
            lbInfo.Text = "";
            lbError.Text = "";

            foreach (var args in e.UpdateValues)
                UpdateArticulo(args.Keys, args.NewValues);

            e.Handled = true;
        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }
            lbError.Text = string.Format("Error al actualizar los datos BATCH {0} {1}", ex.Message, m);
        }

    }
    protected void gridArticulos_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        try
        {
            UpdateArticulo(e.Keys, e.NewValues);
            CancelEditing(e);
        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }
            lbError.Text = string.Format("Error al actualizar los datos UPDATE {0} {1}", ex.Message, m);
        }
    }
    protected ItemArticulo UpdateArticulo(OrderedDictionary keys, OrderedDictionary newValues)
    {
        ValidarSesion();
        var id = Convert.ToString(keys["Articulo"]);

        var item = DatosArticulos.First(i => i.Articulo == id);
        CargarNuevosValores(item, newValues);
        return item;
    }

    private void SetDescuentosPedido()
    {
        try
        {
            foreach (var item in DatosPedido)
            {
                if (rdlTipoVenta.SelectedIndex > 0)
                {
                    item.IdDescuento = 3;
                    item.Descuento = "Colaborador";
                }
                else
                {
                    //item.IdDescuento = 1;
                    //item.Descuento = "---";
                }
            }
        }
        catch
        { }
        gridPedido.DataSource = DatosPedido;
        gridPedido.DataBind();

        CargarDescuentosPedido();
    }
    private void SetDescuentos()
    {
        foreach (var item in DatosArticulos)
        {
            if (rdlTipoVenta.SelectedIndex > 0)
            {
                item.IdDescuento = 3;
                item.Descuento = "Colaborador";
            }
            else
            {
                item.IdDescuento = 1;
                item.Descuento = "---";
            }
        }
        gridArticulos.DataSource = DatosArticulos;
        gridArticulos.DataBind();
        foreach (var item in DatosPedido)
        {
            if (rdlTipoVenta.SelectedIndex > 0)
            {
                item.IdDescuento = 3;
                item.Descuento = "Colaborador";
            }
            else
            {
                item.IdDescuento = 1;
                item.Descuento = "---";
            }
        }
        gridPedido.DataSource = DatosPedido;
        gridPedido.DataBind();
    }

    protected void CargarNuevosValores(ItemArticulo item, OrderedDictionary values)
    {
        int n;
        //si solo cambia cantidad
        if (values["Cantidad"] != null)
        {
            if (item.orden == null)
            {
                List<ItemArticulo> data = (List<ItemArticulo>)gridArticulos.DataSource;
                int orden = data.Count(x => x.orden != null);
                item.orden = orden;
            }

            item.Cantidad = Convert.ToInt32(values["Cantidad"]);
        }

        #region "cemaco"
        //si solo cambia descuento
        if (Session["Cliente"].Equals("MA0017"))
            if (values[2] != null && int.TryParse(values[2].ToString(), out n))
            {
                item.IdDescuento = Convert.ToInt32(values[2]);
                var dsDescuento = new DataSet();
                if (Session["Descuento"] != null)
                {
                    dsDescuento = ((DataSet)(Session["Descuento"]));
                }
                else
                    CargarTiposDescuento(Session["Cliente"].ToString(), ref dsDescuento);

                DropDownList ddlDescuento = new DropDownList();
                ddlDescuento.DataTextField = "Descuento";
                ddlDescuento.DataValueField = "IdDescuento";
                ddlDescuento.DataSource = dsDescuento;
                ddlDescuento.DataBind();
                item.Descuento = ddlDescuento.Items.FindByValue(item.IdDescuento.ToString()).Text;

            }
        #endregion
    }

    protected void CancelEditing(CancelEventArgs e)
    {
        ValidarSesion();
        e.Cancel = true;
        gridArticulos.CancelEdit();
    }

    //GridPedido
    protected void gridPedido_BatchUpdate(object sender, DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs e)
    {
        try
        {
            lbInfo.Text = "";
            lbError.Text = "";

            foreach (var args in e.UpdateValues)
                UpdatePedido(args.Keys, args.NewValues);

            e.Handled = true;
        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }
            lbError.Text = string.Format("Error al actualizar los datos BATCH {0} {1}", ex.Message, m);
        }
    }
    protected void gridPedido_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        try
        {
            UpdatePedido(e.Keys, e.NewValues);
            CancelEditing(e);
        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }
            lbError.Text = string.Format("Error al actualizar los datos UPDATE {0} {1}", ex.Message, m);
        }
    }
    protected ItemPedido UpdatePedido(OrderedDictionary keys, OrderedDictionary newValues)
    {
        ValidarSesion();
        var id = Convert.ToString(keys["Articulo"]);
        var item = DatosPedido.First(i => i.Articulo == id);
        CargarNuevosValoresPedido(item, newValues);
        return item;
    }
    protected void CargarNuevosValoresPedido(ItemPedido item, OrderedDictionary values)
    {
        int n;
        if (values["Cantidad"] != null)
            item.Cantidad = Convert.ToInt32(values["Cantidad"]);
        //si solo cambia descuento
        if (values["Descuento"] != null && int.TryParse(values["Descuento"].ToString(), out n))
        {
            item.IdDescuento = Convert.ToInt32(values[2]);

            if (Session["Descuento"] != null)
            {
                DataSet dsDescuento = ((DataSet)(Session["Descuento"]));
                DropDownList ddlDescuento = new DropDownList();
                ddlDescuento.DataTextField = "Descuento";
                ddlDescuento.DataValueField = "IdDescuento";
                ddlDescuento.DataSource = dsDescuento;
                ddlDescuento.DataBind();
                item.Descuento = ddlDescuento.Items.FindByValue(item.IdDescuento.ToString()).Text;
            }
        }
    }
    //GridPedido

    public class ItemArticulo
    {
        public int? orden { get; set; }
        public string Articulo { get; set; }
        public string Descripcion { get; set; }
        public int Cantidad { get; set; }
        public double PrecioBase { get; set; }
        public decimal PrecioLista { get; set; }
        public string TieneRegalo { get; set; }
        public string Regalo { get; set; }
        public Int32 CantidadRegalo { get; set; }
        public string Tipo { get; set; }
        public int IdDescuento { get; set; }
        public string Descuento { get; set; }
        public ItemArticulo()
        {
            IdDescuento = 1;//sin descuento
            Descuento = "---";
        }
    }

    public class ItemPedido
    {
        public int? orden { get; set; }
        public string Articulo { get; set; }
        public string Descripcion { get; set; }
        public int Cantidad { get; set; }
        public double PrecioBase { get; set; }
        public decimal PrecioLista { get; set; }
        public string TieneRegalo { get; set; }
        public string Regalo { get; set; }
        public Int32 CantidadRegalo { get; set; }
        public string Tipo { get; set; }
        public int IdDescuento { get; set; }
        public string Descuento { get; set; }
    }

    protected void btnPreliminar_Click(object sender, EventArgs e)
    {
        try
        {
            ValidarSesion();
            Session["Descuento"] = null;
            lbInfo.Text = "";
            lbError.Text = "";

            bool mHay = false;
            DatosPedido.Clear();

            IEnumerable<ItemArticulo> query = DatosArticulos.Where(x => x.Cantidad > 0);
            if (Session["Cliente"].ToString() == "MA0018")
                query = query.OrderBy(x => x.orden);

            query.ToList().ForEach(x =>
            {
                DatosPedido.Add(new ItemPedido()
                {
                    Articulo = x.Articulo,
                    Descripcion = x.Descripcion,
                    PrecioBase = x.PrecioBase,
                    PrecioLista = x.PrecioLista,
                    Cantidad = x.Cantidad,
                    TieneRegalo = x.TieneRegalo,
                    Regalo = x.Regalo,
                    CantidadRegalo = x.CantidadRegalo,
                    Tipo = x.Tipo,
                    IdDescuento = x.IdDescuento,
                    Descuento = x.Descuento,
                    orden = x.orden
                });
                mHay = true;
            });

            if (!mHay)
            {
                lbError.Text = "Debe ingresar cantidad en al menos un artículo";
                return;
            }

            List<string> current = DatosPedido.Select(x => x.Descripcion).ToList(),
                ordered = DatosPedido.OrderBy(x => x.orden).Select(x => x.Descripcion).ToList();


            gridPedido.DataSource = DatosPedido;
            gridPedido.DataBind();
            //if (rdlTipoVenta.SelectedIndex > 0)
            SetDescuentosPedido();
            btnPreliminar.Visible = false;
            btnGrabar.Visible = true;
            btnAgregar.Visible = true;

            gridPedido.Visible = true;
            gridArticulos.Visible = false;

            lbInfo.Text = "Listo, por favor revise su pedido";
            btnGrabar.Focus();
        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }
            lbError.Text = string.Format("Error en el preliminar {0} {1}", ex.Message, m);
        }

    }
    protected void btnGrabar_Click(object sender, EventArgs e)
    {
        try
        {
            ValidarSesion();

            lbInfo.Text = "";
            lbError.Text = "";

            string mPedido = txtPedido.Text.Trim().ToUpper(); string mMensaje = "";
            var ws = new wsPuntoVentaMayoreo.wsPuntoVentaMayoreo();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);



            DataSet dsInfo = new DataSet();
            DataTable dtPedido = new DataTable("Pedido");
            DataTable dtPedidoLinea = new DataTable("PedidoLinea");

            dtPedido.Columns.Add(new DataColumn("CemacoTienda", typeof(System.String)));
            dtPedido.Columns.Add(new DataColumn("CemacoFechaVenta", typeof(System.DateTime)));
            dtPedido.Columns.Add(new DataColumn("CemacoCaja", typeof(System.String)));
            dtPedido.Columns.Add(new DataColumn("CemacoTransaccion", typeof(System.String)));
            dtPedido.Columns.Add(new DataColumn("CemacoClienteNombre", typeof(System.String)));
            dtPedido.Columns.Add(new DataColumn("CemacoClienteTelefono", typeof(System.String)));
            dtPedido.Columns.Add(new DataColumn("CemacoClienteDireccion", typeof(System.String)));
            dtPedido.Columns.Add(new DataColumn("CemacoFechaEntrega", typeof(System.DateTime)));
            dtPedido.Columns.Add(new DataColumn("CemacoImagen", typeof(System.Byte[])));
            dtPedido.Columns.Add(new DataColumn("CemacoClienteDepartamento", typeof(System.String)));
            dtPedido.Columns.Add(new DataColumn("CemacoClienteMunicipio", typeof(System.String)));
            dtPedido.Columns.Add(new DataColumn("CemacoClienteZona", typeof(System.String)));
            dtPedido.Columns.Add(new DataColumn("CemacoClienteIndicaciones", typeof(System.String)));
            dtPedido.Columns.Add(new DataColumn("CemacoClienteSegundoPiso", typeof(System.String)));
            dtPedido.Columns.Add(new DataColumn("CemacoClienteEntraCamion", typeof(System.String)));
            dtPedido.Columns.Add(new DataColumn("CemacoClienteEntraPickup", typeof(System.String)));
            dtPedido.Columns.Add(new DataColumn("HayImagen", typeof(System.String)));
            dtPedido.Columns.Add(new DataColumn("VendedorComision", typeof(System.String)));


            dtPedidoLinea.Columns.Add(new DataColumn("Articulo", typeof(System.String)));
            dtPedidoLinea.Columns.Add(new DataColumn("Descripcion", typeof(System.String)));
            dtPedidoLinea.Columns.Add(new DataColumn("Cantidad", typeof(System.Int32)));
            dtPedidoLinea.Columns.Add(new DataColumn("PrecioBase", typeof(System.Decimal)));
            dtPedidoLinea.Columns.Add(new DataColumn("PrecioLista", typeof(System.Decimal)));
            dtPedidoLinea.Columns.Add(new DataColumn("TieneRegalo", typeof(System.String)));
            dtPedidoLinea.Columns.Add(new DataColumn("Regalo", typeof(System.String)));
            dtPedidoLinea.Columns.Add(new DataColumn("CantidadRegalo", typeof(System.Int32)));
            dtPedidoLinea.Columns.Add(new DataColumn("Tipo", typeof(System.String)));
            dtPedidoLinea.Columns.Add(new DataColumn("TipoDescuento", typeof(System.Int32)));

            DataRow row = dtPedido.NewRow();
            string mCemacoIndicaciones = "";
            string mCemacoSegundoPiso = "N";
            string mCemacoEntraCamion = "N";
            string mCemacoEntraPickup = "N";
            DateTime mCemacoFechaVenta = new DateTime(1980, 1, 1);
            DateTime mCemacoFechaEntrega = DateTime.Now.Date;
            string mCemacoDepartamento = "GUATEMALA";
            string mCemacoMunicipio = "GUATEMALA";
            string mCemacoTienda = "";
            string mCemacoCaja = "";
            string mCemacoTransaccion = "";
            string mCemacoClienteNombre = "";
            string mCemacoClienteTelefono = "";
            string mCemacoClienteDireccion = "";
            string mCemacoZona = "00";
            string mVendedorComision = string.Empty;
            string mTipoVenta = string.Empty;
            row["HayImagen"] = "N";

            #region "Detalle"
            List<string> lstArticulos = new List<string>();

            for (int ii = 0; ii < DatosPedido.Count; ii++)
            {
                if ((Int32)DatosPedido[ii].Cantidad > 0)
                {
                    DataRow mRow = dtPedidoLinea.NewRow();
                    mRow["Articulo"] = DatosPedido[ii].Articulo;
                    mRow["Descripcion"] = DatosPedido[ii].Descripcion;
                    mRow["Cantidad"] = (Int32)DatosPedido[ii].Cantidad;
                    mRow["PrecioBase"] = DatosPedido[ii].PrecioBase;
                    mRow["PrecioLista"] = DatosPedido[ii].PrecioLista;
                    mRow["TieneRegalo"] = DatosPedido[ii].TieneRegalo;
                    mRow["Regalo"] = DatosPedido[ii].Regalo;
                    mRow["CantidadRegalo"] = DatosPedido[ii].CantidadRegalo;
                    mRow["Tipo"] = DatosPedido[ii].Tipo;
                    mRow["TipoDescuento"] = DatosPedido[ii].IdDescuento;
                    dtPedidoLinea.Rows.Add(mRow);

                    //para la validación de los artículos dentro del pedido
                    //si son para despaharse directamente al cliente final o a las 
                    //bodegas de nuestro cliente

                    lstArticulos.Add(DatosPedido[ii].Articulo);
                }
            }
            dsInfo.Tables.Add(dtPedidoLinea);
            #endregion

            var blnEnvioF01 = ws.HayArticulosDespachoCentral(Session["Cliente"].ToString(), lstArticulos.ToArray());

            if (tblCemaco.Visible && blnEnvioF01)
            {
                if (txtCemacoTienda.Text.Trim().Length == 0)
                {
                    lbError.Text = "Debe ingresar la tienda";
                    txtCemacoTienda.Focus();
                    return;
                }

                try
                {
                    mCemacoFechaVenta = Convert.ToDateTime(CemacoFechaVenta.Value);
                }
                catch
                {
                    lbError.Text = "Debe ingresar la fecha de venta";
                    CemacoFechaVenta.Focus();
                    return;
                }

                if (mCemacoFechaVenta.Year == 1)
                {
                    lbError.Text = "Debe ingresar la fecha de venta";
                    CemacoFechaVenta.Focus();
                    return;
                }

                if (txtCemacoCaja.Text.Trim().Length == 0)
                {
                    lbError.Text = "Debe ingresar la caja";
                    txtCemacoCaja.Focus();
                    return;
                }
                if (txtCemacoTransaccion.Text.Trim().Length == 0)
                {
                    lbError.Text = "Debe ingresar la transaccion";
                    txtCemacoTransaccion.Focus();
                    return;
                }
                if (txtCemacoClienteTelefono.Text.Trim().Length == 0)
                {
                    lbError.Text = "Debe ingresar el teléfono del cliente";
                    txtCemacoClienteTelefono.Focus();
                    return;
                }

                if (cbPendiente.Checked) CemacoFechaEntrega.Value = new DateTime(2040, 1, 1);

                try
                {
                    mCemacoFechaEntrega = Convert.ToDateTime(CemacoFechaEntrega.Value);
                }
                catch
                {
                    lbError.Text = "Debe ingresar la fecha de entrega";
                    CemacoFechaEntrega.Focus();
                    return;
                }

                if (mCemacoFechaEntrega.Year == 1)
                {
                    lbError.Text = "Debe ingresar la fecha de entrega";
                    CemacoFechaEntrega.Focus();
                    return;
                }

                if (!cbPendiente.Checked && mCemacoFechaEntrega == new DateTime(2040, 1, 1))
                {
                    lbError.Text = "Debe ingresar una fecha de entrega válida";
                    CemacoFechaEntrega.Focus();
                    return;
                }

                DateTime mFechaMinima = DateTime.Now.Date.AddDays(2);
                if (mCemacoFechaEntrega < mFechaMinima)
                {
                    lbError.Text = string.Format("La fecha de entrega mínima admitida es el {0}{1}/{2}{3}/{4}.", mFechaMinima.Day < 10 ? "0" : "", mFechaMinima.Day.ToString(), mFechaMinima.Month < 10 ? "0" : "", mFechaMinima.Month.ToString(), mFechaMinima.Year.ToString());
                    CemacoFechaEntrega.Focus();
                    return;
                }

                if (txtCemacoClienteNombre.Text.Trim().Length == 0)
                {
                    lbError.Text = "Debe ingresar el nombre del cliente";
                    txtCemacoClienteNombre.Focus();
                    return;
                }
                if (txtCemacoClienteDireccion.Text.Trim().Length == 0)
                {
                    lbError.Text = "Debe ingresar la dirección del cliente";
                    txtCemacoClienteDireccion.Focus();
                    return;
                }
                if (cbZona.Value == null)
                {
                    lbError.Text = "Debe seleccionar la zona";
                    cbZona.Focus();
                    return;
                }
                if (cbZona.Value.ToString().Length == 0)
                {
                    lbError.Text = "Debe seleccionar la zona";
                    cbZona.Focus();
                    return;
                }

                string mFile = "";
                try
                {
                    mFile = uploadFile.UploadedFiles[0].FileName;
                }
                catch
                {
                    mFile = "";
                }

                //if (mFile.Trim().Length == 0)
                //{
                //    lbError.Text = "No se ha cargado ningún archivo";
                //    return;
                //}

                txtCemacoTienda.Text = txtCemacoTienda.Text.ToUpper();
                txtCemacoCaja.Text = txtCemacoCaja.Text.ToUpper();
                txtCemacoTransaccion.Text = txtCemacoTransaccion.Text.ToUpper();
                txtCemacoClienteNombre.Text = txtCemacoClienteNombre.Text.ToUpper();
                txtCemacoClienteTelefono.Text = txtCemacoClienteTelefono.Text.ToUpper();
                txtCemacoClienteDireccion.Text = txtCemacoClienteDireccion.Text.ToUpper();

                mCemacoTienda = txtCemacoTienda.Text.ToUpper();
                mCemacoCaja = txtCemacoCaja.Text.ToUpper();
                mCemacoTransaccion = txtCemacoTransaccion.Text.ToUpper();
                mCemacoClienteNombre = txtCemacoClienteNombre.Text.ToUpper();
                mCemacoClienteTelefono = txtCemacoClienteTelefono.Text.ToUpper();
                mCemacoClienteDireccion = txtCemacoClienteDireccion.Text.ToUpper();
                mCemacoDepartamento = cbDepartamento.Value.ToString();
                mCemacoMunicipio = cbMunicipio.Value.ToString();
                mCemacoZona = cbZona.Value.ToString();
                mCemacoIndicaciones = txtIndicaciones.Text.Trim().ToUpper();
                mCemacoSegundoPiso = cbSegundoPiso.Checked ? "S" : "N";
                mCemacoEntraCamion = cbEntraCamion.Checked ? "S" : "N";
                mCemacoEntraPickup = cbEntraPickup.Checked ? "S" : "N";
                try
                {
                    mVendedorComision = cbVendedor.SelectedItem.Value.ToString();
                }
                catch
                { }

                if (mFile.Trim().Length > 0)
                {
                    row["HayImagen"] = "S";
                    row["CemacoImagen"] = uploadFile.UploadedFiles[0].FileBytes;
                }
            }

            #region "Pedido"
            row["CemacoTienda"] = mCemacoTienda;
            row["CemacoFechaVenta"] = mCemacoFechaVenta;
            row["CemacoCaja"] = mCemacoCaja;
            row["CemacoTransaccion"] = mCemacoTransaccion;
            row["CemacoClienteNombre"] = mCemacoClienteNombre;
            row["CemacoClienteTelefono"] = mCemacoClienteTelefono;
            row["CemacoClienteDireccion"] = mCemacoClienteDireccion;
            row["CemacoFechaEntrega"] = mCemacoFechaEntrega;
            row["CemacoClienteDepartamento"] = mCemacoDepartamento;
            row["CemacoClienteMunicipio"] = mCemacoMunicipio;
            row["CemacoClienteZona"] = mCemacoZona;
            row["CemacoClienteIndicaciones"] = mCemacoIndicaciones;
            row["CemacoClienteSegundoPiso"] = mCemacoSegundoPiso;
            row["CemacoClienteEntraCamion"] = mCemacoEntraCamion;
            row["CemacoClienteEntraPickup"] = mCemacoEntraPickup;
            row["VendedorComision"] = mVendedorComision;

            dtPedido.Rows.Add(row);



            dsInfo.Tables.Add(dtPedido);
            #endregion

            if (!ws.GrabarPedido(ref mPedido, dsInfo, ref mMensaje, Convert.ToString(Session["Usuario"]).ToUpper(), Convert.ToString(Session["Cliente"]), txtObservaciones.Text.Trim(), txtOrdenCompra.Text.Trim()))
            {
                lbError.Text = mMensaje;
                return;
            }

            lbInfo.Text = mMensaje;

            DatosPedido.Clear();

            btnGrabar.Visible = false;
            btnAgregar.Visible = false;
            btnPreliminar.Visible = true;

            gridPedido.Visible = false;
            gridArticulos.Visible = true;

            for (int ii = 0; ii < DatosArticulos.Count; ii++)
            {
                DatosArticulos[ii].Cantidad = 0;
            }

            txtObservaciones.Text = "";
            gridArticulos.SearchPanelFilter = "";

            Session["Mensaje"] = mMensaje;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "PopupScript", string.Format("alert('{0}.');", mMensaje), true);

            Response.Redirect("default.aspx");
        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }
            lbError.Text = string.Format("Error al enviar el pedidos {0} {1}", ex.Message, m);
        }
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        try
        {
            ValidarSesion();

            lbInfo.Text = "";
            lbError.Text = "";
            btnPreliminar.Visible = true;
            btnGrabar.Visible = false;
            btnAgregar.Visible = false;

            gridPedido.Visible = false;
            gridArticulos.Visible = true;
        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }
            lbError.Text = string.Format("Error en el preliminar {0} {1}", ex.Message, m);
        }
    }

    protected void cbDepartamento_ValueChanged(object sender, EventArgs e)
    {
        CargaMunicipios();
        cbMunicipio.Focus();
    }
    protected void lbPedido_Click(object sender, EventArgs e)
    {
        //if (tblCemaco.Visible)
        //{
        //    tblCemaco.Visible = false;
        //}
        //else
        //{
        //    tblCemaco.Visible = true;
        //}
    }

    void SeleccionarPedido()
    {
        try
        {
            lbError.Text = "";
            if (txtPedido.Text.Trim().Length == 0) return;

            var ws = new wsPuntoVentaMayoreo.wsPuntoVentaMayoreo();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(Session["url"]);

            string mMensaje = "";
            if (!ws.ExistePedido(txtPedido.Text.Trim(), ref mMensaje))
            {
                //lbError.Text = mMensaje;
                //return;
            }

            var q = ws.DevuelvePedido(txtPedido.Text);
            if (q.Count() == 0) return;

            txtOrdenCompra.Text = q[0].OrdenCompra;
            txtObservaciones.Text = q[0].Observaciones;
            txtCemacoTienda.Text = q[0].CemacoTienda;
            CemacoFechaVenta.Value = q[0].CemacoFechaVenta;
            txtCemacoCaja.Text = q[0].CemacoCaja;
            txtCemacoTransaccion.Text = q[0].CemacoTransaccion;
            txtCemacoClienteTelefono.Text = q[0].CemacoClienteTelefono;
            CemacoFechaEntrega.Value = q[0].FechaEntrega;
            txtCemacoClienteNombre.Text = q[0].CemacoClienteNombre;
            txtCemacoClienteDireccion.Text = q[0].CemacoClienteDireccion;
            cbZona.Value = q[0].CemacoClienteZona;
            cbDepartamento.Value = q[0].CemacoClienteDepartamento;
            txtIndicaciones.Text = q[0].CemacoClienteIndicaciones;
            try
            {
                //solo para cemaco
                cbVendedor.SelectedIndex = cbVendedor.Items.FindByValue(q[0].Vendedor).Index;
            }
            catch
            { }
            CargaMunicipios();
            cbMunicipio.Value = q[0].CemacoClienteMunicipio;

            if (q[0].FechaEntrega.Year == 2040 && q[0].FechaEntrega.Month == 1 && q[0].FechaEntrega.Day == 1) cbPendiente.Checked = true;

            cbSegundoPiso.Checked = false;
            cbEntraCamion.Checked = false;
            cbEntraPickup.Checked = false;

            if (q[0].CemacoClienteSegundoPiso == "S") cbSegundoPiso.Checked = true;
            if (q[0].CemacoClienteEntraCamion == "S") cbEntraCamion.Checked = true;
            if (q[0].CemacoClienteEntraPickup == "S") cbEntraPickup.Checked = true;

            var wsPDV = new wsPuntoVenta.wsPuntoVenta();
            wsPDV.Url = wsPDV.Url.Replace("Mayoreo", "");

            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") wsPDV.Url = Convert.ToString(Session["url"]).Replace("Mayoreo", "");

            var qq = wsPDV.DevuelvePedidoLinea(txtPedido.Text.Trim());

            if (qq.Count() == 0) return;

            foreach (var item in qq)
            {
                foreach (var articulo in DatosArticulos)
                {
                    if (articulo.Articulo == "142090-102" || articulo.Articulo == "142096-102" || articulo.Articulo == "142098-102" || articulo.Articulo == "142099-102")
                    {
                        int ii = qq.Count();
                    }
                    if (item.Articulo == articulo.Articulo)
                    {
                        articulo.Cantidad = Convert.ToInt32(item.CantidadPedida);
                        articulo.IdDescuento = item.IdDescuento;
                        articulo.Descuento = item.DescuentoNombre;
                    }
                }
                try
                {
                    //Procedemos a ubicar un tipo de venta a colaborador en pedido_linea (columna TIPO_DESCUENTO)
                    if (rdlTipoVenta.SelectedIndex == -1)
                    {
                        if (item.IdDescuento == 3)
                        {
                            rdlTipoVenta.SelectedIndex = 1;
                            SetDescuentos();
                        }
                    }
                }
                catch
                { }
            }
            //no encontro descuentos de colaborador en del detalle (por default cuando es tipo venta a colaborador)
            //cemaco solamente
            try
            {
                if (rdlTipoVenta.SelectedIndex == -1)
                    rdlTipoVenta.SelectedIndex = 0;
            }
            catch
            { }
            gridArticulos.DataSource = DatosArticulos;
            gridArticulos.DataBind();

            gridPedido.DataSource = DatosPedido;
            gridPedido.DataBind();
        }
        catch (Exception ex)
        {
            string m = "";
            try
            {
                m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
            }
            catch
            {
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                }
                catch
                {
                    //Nothing
                }
            }
            lbError.Text = string.Format("Error al seleccionar el pedido {0} {1}", ex.Message, m);
        }
    }

    protected void btnPedido_Click(object sender, EventArgs e)
    {
        SeleccionarPedido();
    }


    protected void cbVendedor_SelectedIndexChanged(object sender, EventArgs e)
    {

    }





    protected void gridArticulos_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName != "Descuento") return;
        if (gridArticulos.VisibleRowCount > 0 && Session["Descuento"] == null)
        {
            for (int i = 0; i < gridArticulos.VisibleRowCount; i++)
            {
                try
                {
                    string Nombre = string.Empty;
                    int val = int.Parse(gridArticulos.GetRowValues(i, "IdDescuento").ToString());
                    ASPxComboBox combo = e.Editor as ASPxComboBox;
                    FillCityCombo(combo, val, ref Nombre);

                }
                catch
                { }

            }
        }

    }

    protected void FillCityCombo(ASPxComboBox cmb, int idDescuento, ref string NomDescuento)
    {
        //if(string.IsNullOrEmpty(idDescuento)) return;

        DataSet dsDescuento = new DataSet();
        CargarTiposDescuento("", ref dsDescuento);
        cmb.Items.Clear();
        if (dsDescuento != null)
        {
            cmb.DataSource = dsDescuento;
            cmb.TextField = "Descuento";
            cmb.ValueField = "IdDescuento";
            cmb.DataBind();
            Session["Descuento"] = dsDescuento;
        }
        try
        {
            if (cmb.Items.Count > 0)
            {
                if (rdlTipoVenta.SelectedIndex > 0)
                {
                    cmb.SelectedIndex = cmb.Items.FindByText("Colaborador").Index;
                    NomDescuento = cmb.SelectedItem.Text;
                }
                else
                {
                    cmb.SelectedIndex = cmb.Items.FindByValue(idDescuento.ToString()).Index;
                    NomDescuento = cmb.SelectedItem.Text;
                }
            }
        }
        catch
        { }
    }

    protected void gridArticulos_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {

    }



    protected void gridArticulos_DataBinding(object sender, EventArgs e)
    {

        //if (gridArticulos.VisibleRowCount > 0)
        //{
        //    for (int i = 0; i < gridArticulos.VisibleRowCount; i++)
        //    {
        //        string val = gridArticulos.GetRowValues(i, "IdDescuento").ToString();


        //    }
        //}
    }

    protected void gridArticulos_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {

    }



    protected void gridPedido_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName != "Descuento") return;
        if (gridPedido.VisibleRowCount > 0 && Session["Descuento"] == null)
        {
            for (int i = 0; i < gridPedido.VisibleRowCount; i++)
            {
                try
                {
                    string Nombre = string.Empty;
                    int val = int.Parse(gridPedido.GetRowValues(i, "IdDescuento").ToString());
                    ASPxComboBox combo = e.Editor as ASPxComboBox;
                    FillCityCombo(combo, val, ref Nombre);

                }
                catch
                { }

            }
        }
    }



    protected void rdlTipoVenta_SelectedIndexChanged1(object sender, EventArgs e)
    {
        SetDescuentos();

    }
}
