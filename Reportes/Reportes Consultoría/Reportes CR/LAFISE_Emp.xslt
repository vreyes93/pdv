<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:ex="http://www.softland.cr">

<xsl:output method="text"/>

<!-- Archivo de Transferencia para Banco de Costa Rica--> 


<!-- Encabezado del archivo de tranferencia --> 

<xsl:template match="parametros">
	<!-- NO HAY ENCABEZADO -->
</xsl:template>

<!-- Datos de los empleados para la tranferencia -->
					 			
<xsl:template match="empleado">

	<!-- Detalle empleado --> 	
	
	<!-- 1. C�digo de Beneficiario -->  
	<xsl:value-of select="ex:Rpad(nombre, ' ', 20)"/>       
        
	<!-- 2. Monto, con punto decimal, con la cantidad de decimales delimitados y validando que cumpla una longitud definida y alineado a la izquierda-->
	<xsl:value-of select="ex:Lpad(ex:Decimales(monto*100, 2), '0', 12)"/>
        
    <!-- IMPORTANTE: Este campo debe ser configurado para el cliente -->	
	<!--  		 puede ser un valor directo en la plantilla, un par�metro o un adicional de n�mina -->	
	<!-- 3. C�digo de la moneda: COL o DOL-->
	<xsl:text>COL</xsl:text>
	
    <!-- 4. Descripci�n -->
	<xsl:value-of select="ex:Rpad(referencia, ' ', 20)"/>

	<!-- 5. Servicio m�ximo 20 caracteres -->
	<xsl:value-of select="ex:Rpad(nombre, ' ', 20)"/>

	<!-- Siguiente l�nea --> 
<xsl:text>
</xsl:text> 


</xsl:template>
</xsl:stylesheet>
