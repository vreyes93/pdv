﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ReportesMF
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        //static void Main()
        //{
        //    Application.EnableVisualStyles();
        //    Application.SetCompatibleTextRenderingDefault(false);
        //    Application.Run(new frmVentaMayoreo());
        //}
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (args.Length == 0)
            {
                Application.Run(new Form1());
            }
            else
            {
                switch (args[0])
                {
                    case "VentaMayoreo":
                        Application.Run(new frmVentaMayoreo());
                        break;
                    default:
                        Application.Run(new Form1());
                        break;
                }
            }
        }
    }
}
