﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Diagnostics;

namespace ReportesMF
{
    public partial class frmVentaMayoreo : Form
    {
        public frmVentaMayoreo()
        {
            InitializeComponent();
        }

        private void frmVentaMayoreo_Load(object sender, EventArgs e)
        {
            rbResumida.Checked = true;
            rbDetallada.Checked = false;
            txtAnio1.Text = Convert.ToString(DateTime.Now.Year - 1);
            txtAnio2.Text = Convert.ToString(DateTime.Now.Year);

            txtAnio1.Focus();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            string mCaption = "Venta Mayoreo";

            int mAnio1, mAnio2;

            try
            {
                mAnio1 = Convert.ToInt32(txtAnio1.Text);
            }
            catch
            {
                MessageBox.Show("El año inicial es inválido.", mCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtAnio1.Focus();
                return;
            }
            try
            {
                mAnio2 = Convert.ToInt32(txtAnio2.Text);
            }
            catch
            {
                MessageBox.Show("El año final es inválido.", mCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtAnio2.Focus();
                return;
            }

            if (mAnio1 > mAnio2)
            {
                MessageBox.Show("El año inicial no puede ser mayor al año final.", mCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtAnio1.Focus();
                return;
            }
                
            try
            {
                string mReporte = "rptVentaMayoreoResumidaDS.rpt";
                ReportDocument reporte = new ReportDocument();

                if (rbDetallada.Checked) mReporte = "rptVentaMayoreoDetalladaDS.rpt";

                dsFiesta ds = new dsFiesta();
                SqlConnection mConexion = new SqlConnection("Data Source = INTEGRA; Initial Catalog = EXACTUSERP;User id = REPORTEADOR;password = RPTcry98");

                string mPathReporte = string.Format("{0}\\{1}", Application.StartupPath, mReporte);

                SqlDataAdapter daComando = new SqlDataAdapter(string.Format("DECLARE @Cliente AS VARCHAR(20) " +
                    "DECLARE @NombreCliente AS VARCHAR(80) " +
                    "DECLARE @EneroMonto1 AS DECIMAL(12,2) " +
                    "DECLARE @EneroUtilidad1 AS DECIMAL(12,2) " +
                    "DECLARE @FebreroMonto1 AS DECIMAL(12,2) " +
                    "DECLARE @FebreroUtilidad1 AS DECIMAL(12,2) " +
                    "DECLARE @MarzoMonto1 AS DECIMAL(12,2) " +
                    "DECLARE @MarzoUtilidad1 AS DECIMAL(12,2) " +
                    "DECLARE @AbrilMonto1 AS DECIMAL(12,2) " +
                    "DECLARE @AbrilUtilidad1 AS DECIMAL(12,2) " +
                    "DECLARE @MayoMonto1 AS DECIMAL(12,2) " +
                    "DECLARE @MayorUtilidad1 AS DECIMAL(12,2) " +
                    "DECLARE @JunioMonto1 AS DECIMAL(12,2) " +
                    "DECLARE @JunioUtilidad1 AS DECIMAL(12,2) " +
                    "DECLARE @JulioMonto1 AS DECIMAL(12,2) " +
                    "DECLARE @JulioUtilidad1 AS DECIMAL(12,2) " +
                    "DECLARE @AgostoMonto1 AS DECIMAL(12,2)  " +
                    "DECLARE @AgostoUtilidad1 AS DECIMAL(12,2) " +
                    "DECLARE @SeptiembreMonto1 AS DECIMAL(12,2) " +
                    "DECLARE @SeptiembreUtilidad1 AS DECIMAL(12,2) " +
                    "DECLARE @OctubreMonto1 AS DECIMAL(12,2) " +
                    "DECLARE @OctubreUtilidad1 AS DECIMAL(12,2) " +
                    "DECLARE @NoviembreMonto1 AS DECIMAL(12,2) " +
                    "DECLARE @NoviembreUtilidad1 AS DECIMAL(12,2) " +
                    "DECLARE @DiciembreMonto1 AS DECIMAL(12,2) " +
                    "DECLARE @DiciembreUtilidad1 AS DECIMAL(12,2) " +
                    "DECLARE @EneroMonto2 AS DECIMAL(12,2) " +
                    "DECLARE @EneroUtilidad2 AS DECIMAL(12,2) " +
                    "DECLARE @FebreroMonto2 AS DECIMAL(12,2) " +
                    "DECLARE @FebreroUtilidad2 AS DECIMAL(12,2) " +
                    "DECLARE @MarzoMonto2 AS DECIMAL(12,2) " +
                    "DECLARE @MarzoUtilidad2 AS DECIMAL(12,2) " +
                    "DECLARE @AbrilMonto2 AS DECIMAL(12,2) " +
                    "DECLARE @AbrilUtilidad2 AS DECIMAL(12,2) " +
                    "DECLARE @MayoMonto2 AS DECIMAL(12,2) " +
                    "DECLARE @MayorUtilidad2 AS DECIMAL(12,2)  " +
                    "DECLARE @JunioMonto2 AS DECIMAL(12,2) " +
                    "DECLARE @JunioUtilidad2 AS DECIMAL(12,2) " +
                    "DECLARE @JulioMonto2 AS DECIMAL(12,2) " +
                    "DECLARE @JulioUtilidad2 AS DECIMAL(12,2) " +
                    "DECLARE @AgostoMonto2 AS DECIMAL(12,2) " +
                    "DECLARE @AgostoUtilidad2 AS DECIMAL(12,2) " +
                    "DECLARE @SeptiembreMonto2 AS DECIMAL(12,2) " +
                    "DECLARE @SeptiembreUtilidad2 AS DECIMAL(12,2) " +
                    "DECLARE @OctubreMonto2 AS DECIMAL(12,2) " +
                    "DECLARE @OctubreUtilidad2 AS DECIMAL(12,2) " +
                    "DECLARE @NoviembreMonto2 AS DECIMAL(12,2) " +
                    "DECLARE @NoviembreUtilidad2 AS DECIMAL(12,2) " +
                    "DECLARE @DiciembreMonto2 AS DECIMAL(12,2) " +
                    "DECLARE @DiciembreUtilidad2 AS DECIMAL(12,2) " +
                    "DECLARE @anio1 AS INT " +
                    "DECLARE @anio2 AS INT " +
                    "SET @anio1 = {0} " +
                    "SET @anio2 = {1} " +
                    "BEGIN TRY	DROP TABLE #temp END TRY BEGIN CATCH SET @anio1 = @anio1 END CATCH  " +
                    "BEGIN TRY " +
                    "CREATE TABLE #temp( Cliente VARCHAR(20), Nombre VARCHAR(80), Articulo VARCHAR(20), NombreArticulo VARCHAR(254), " +
                    "EneroMonto1 DECIMAL(12,2), EneroMargen1 DECIMAL(12,2), EneroMargenTotal1 DECIMAL(12,2), EneroUtilidad1 DECIMAL(12,2), " +
                    "FebreroMonto1 DECIMAL(12,2), FebreroMargen1 DECIMAL(12,2), FebreroMargenTotal1 DECIMAL(12,2), FebreroUtilidad1 DECIMAL(12,2), " +
                    "MarzoMonto1 DECIMAL(12,2), MarzoMargen1 DECIMAL(12,2), MarzoMargenTotal1 DECIMAL(12,2), MarzoUtilidad1 DECIMAL(12,2), " +
                    "AbrilMonto1 DECIMAL(12,2), AbrilMargen1 DECIMAL(12,2), AbrilMargenTotal1 DECIMAL(12,2), AbrilUtilidad1 DECIMAL(12,2), " +
                    "MayoMonto1 DECIMAL(12,2), MayoMargen1 DECIMAL(12,2), MayoMargenTotal1 DECIMAL(12,2), MayorUtilidad1 DECIMAL(12,2), " +
                    "JunioMonto1 DECIMAL(12,2), JunioMargen1 DECIMAL(12,2), JunioMargenTotal1 DECIMAL(12,2), JunioUtilidad1 DECIMAL(12,2), " +
                    "JulioMonto1 DECIMAL(12,2), JulioMargen1 DECIMAL(12,2), JulioMargenTotal1 DECIMAL(12,2), JulioUtilidad1 DECIMAL(12,2), " +
                    "AgostoMonto1 DECIMAL(12,2), AgostoMargen1 DECIMAL(12,2), AgostoMargenTotal1 DECIMAL(12,2), AgostoUtilidad1 DECIMAL(12,2), " +
                    "SeptiembreMonto1 DECIMAL(12,2), SeptiembreMargen1 DECIMAL(12,2), SeptiembreMargenTotal1 DECIMAL(12,2), SeptiembreUtilidad1 DECIMAL(12,2), " +
                    "OctubreMonto1 DECIMAL(12,2), OctubreMargen1 DECIMAL(12,2), OctubreMargenTotal1 DECIMAL(12,2), OctubreUtilidad1 DECIMAL(12,2), " +
                    "NoviembreMonto1 DECIMAL(12,2), NoviembreMargen1 DECIMAL(12,2), NoviembreMargenTotal1 DECIMAL(12,2), NoviembreUtilidad1 DECIMAL(12,2), " +
                    "DiciembreMonto1 DECIMAL(12,2), DiciembreMargen1 DECIMAL(12,2), DiciembreMargenTotal1 DECIMAL(12,2), DiciembreUtilidad1 DECIMAL(12,2), " +
                    "EneroMonto2 DECIMAL(12,2), EneroMargen2 DECIMAL(12,2), EneroMargenTotal2 DECIMAL(12,2), EneroUtilidad2 DECIMAL(12,2), " +
                    "FebreroMonto2 DECIMAL(12,2), FebreroMargen2 DECIMAL(12,2), FebreroMargenTotal2 DECIMAL(12,2), FebreroUtilidad2 DECIMAL(12,2), " +
                    "MarzoMonto2 DECIMAL(12,2), MarzoMargen2 DECIMAL(12,2), MarzoMargenTotal2 DECIMAL(12,2), MarzoUtilidad2 DECIMAL(12,2), " +
                    "AbrilMonto2 DECIMAL(12,2), AbrilMargen2 DECIMAL(12,2), AbrilMargenTotal2 DECIMAL(12,2), AbrilUtilidad2 DECIMAL(12,2), " +
                    "MayoMonto2 DECIMAL(12,2), MayoMargen2 DECIMAL(12,2), MayoMargenTotal2 DECIMAL(12,2), MayorUtilidad2 DECIMAL(12,2), " +
                    "JunioMonto2 DECIMAL(12,2), JunioMargen2 DECIMAL(12,2), JunioMargenTotal2 DECIMAL(12,2), JunioUtilidad2 DECIMAL(12,2), " +
                    "JulioMonto2 DECIMAL(12,2), JulioMargen2 DECIMAL(12,2), JulioMargenTotal2 DECIMAL(12,2), JulioUtilidad2 DECIMAL(12,2), " +
                    "AgostoMonto2 DECIMAL(12,2), AgostoMargen2 DECIMAL(12,2), AgostoMargenTotal2 DECIMAL(12,2), AgostoUtilidad2 DECIMAL(12,2), " +
                    "SeptiembreMonto2 DECIMAL(12,2), SeptiembreMargen2 DECIMAL(12,2), SeptiembreMargenTotal2 DECIMAL(12,2), SeptiembreUtilidad2 DECIMAL(12,2), " +
                    "OctubreMonto2 DECIMAL(12,2), OctubreMargen2 DECIMAL(12,2), OctubreMargenTotal2 DECIMAL(12,2), OctubreUtilidad2 DECIMAL(12,2), " +
                    "NoviembreMonto2 DECIMAL(12,2), NoviembreMargen2 DECIMAL(12,2), NoviembreMargenTotal2 DECIMAL(12,2), NoviembreUtilidad2 DECIMAL(12,2), " +
                    "DiciembreMonto2 DECIMAL(12,2), DiciembreMargen2 DECIMAL(12,2), DiciembreMargenTotal2 DECIMAL(12,2), DiciembreUtilidad2 DECIMAL(12,2), " +
                    "TotalAnio1 DECIMAL(12,2), TotalAnio2 DECIMAL(12,2), MargenAnio1 DECIMAL(12,2), MargenTotalAnio1 DECIMAL(12,2), MargenAnio2 DECIMAL(12,2), " +
                    "MargenTotalAnio2 DECIMAL(12,2), TotalArticulo1 DECIMAL(12,2), TotalArticulo2 DECIMAL(12,2) ) " +
                    "END TRY " +
                    "BEGIN CATCH " +
                    "SET @anio1 = @anio1 " +
                    "END CATCH " +
                    "DELETE FROM #temp " +
                    "DECLARE #CursorClientes CURSOR LOCAL FOR " +
                    "SELECT DISTINCT a.CLIENTE, c.NOMBRE FROM prodmult.FACTURA a JOIN prodmult.FACTURA_LINEA b ON a.FACTURA = b.FACTURA " +
                    "JOIN prodmult.CLIENTE c ON a.CLIENTE = c.CLIENTE " +
                    "WHERE a.CLIENTE LIKE 'M%' AND (YEAR(a.FECHA) = @anio1 OR YEAR(a.FECHA) = @anio2) " +
                    "ORDER BY a.CLIENTE " +
                    "OPEN #CursorClientes " +
                    "FETCH NEXT FROM #CursorClientes INTO @Cliente, @NombreCliente " +
                    "WHILE @@FETCH_STATUS = 0 " +
                    "BEGIN " +
                    "INSERT INTO #temp " +
                    "SELECT DISTINCT @Cliente, @NombreCliente, b.ARTICULO, c.DESCRIPCION, " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 1 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 1 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 1 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 1 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 2 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 2 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 2 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 2 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 3 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 3 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 3 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 3 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 4 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 4 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 4 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 4 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 5 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 5 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 5 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 5 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 6 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 6 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 6 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 6 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 7 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 7 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 7 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 7 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 8 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 8 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 8 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 8 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 9 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 9 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 9 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 9 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 10 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 10 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 10 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 10 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 11 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 11 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 11 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 11 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 12 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 12 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 12 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND MONTH(d.FECHA) = 12 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 1 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 1 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 1 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 1 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 2 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 2 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 2 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 2 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 3 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 3 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 3 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 3 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 4 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 4 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 4 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 4 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 5 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 5 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 5 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 5 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 6 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 6 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 6 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 6 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 7 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 7 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 7 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 7 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 8 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 8 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 8 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 8 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 9 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 9 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 9 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 9 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 10 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 10 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 10 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 10 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 11 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 11 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 11 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 11 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 12 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 12 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 12 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND MONTH(d.FECHA) = 12 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    "(SELECT SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio1 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    " " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE LIKE 'M%' AND YEAR(d.FECHA) = @anio2 AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F'), " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio1 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO), " +
                    " " +
                    "(SELECT (SUM(e.PRECIO_TOTAL) - SUM(e.COSTO_TOTAL)) / SUM(e.PRECIO_TOTAL) FROM prodmult.FACTURA d JOIN prodmult.FACTURA_LINEA e ON d.FACTURA = e.FACTURA " +
                    "WHERE d.CLIENTE = a.CLIENTE AND YEAR(d.FECHA) = @anio2 AND e.PRECIO_TOTAL > 0 AND e.ARTICULO = b.ARTICULO AND e.ANULADA = 'N' AND e.TIPO_DOCUMENTO = 'F' GROUP BY e.ARTICULO) " +
                    "FROM prodmult.FACTURA a JOIN prodmult.FACTURA_LINEA b ON a.FACTURA = b.FACTURA JOIN prodmult.ARTICULO c ON b.ARTICULO = c.ARTICULO " +
                    "WHERE a.CLIENTE = @Cliente AND (YEAR(a.FECHA) = @anio1 OR YEAR(a.FECHA) = @anio2) " +
                    "ORDER BY b.ARTICULO " +
                    " " +
                    "FETCH NEXT FROM #CursorClientes INTO @Cliente, @NombreCliente " +
                    "END " +
                    "CLOSE #CursorClientes " +
                    "DEALLOCATE #CursorClientes " +
                    " " +
                    "SELECT * FROM #temp ORDER BY Cliente, Articulo", mAnio1, mAnio2), mConexion);

                daComando.SelectCommand.CommandTimeout = 999999999;
                daComando.Fill(ds, "Comando");
                
                reporte.Load(mPathReporte);
                reporte.SetDataSource(ds);
                
                //reporte.SetDatabaseLogon("INTEGRA", "EXACTUSERP", "REPORTEADOR", "RPTcry98");
                //reporte.DataSourceConnections[0].SetConnection("INTEGRA", "EXACTUSERP", "REPORTEADOR", "RPTcry98");r
                reporte.SetParameterValue("anio1", mAnio1);
                reporte.SetParameterValue("anio2", mAnio2);
                  
                saveFileDialog1.Title = "Exportar Reporte de Venta de Mayoreo";
                saveFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                saveFileDialog1.Filter = "xls files (*.xls)|*.xls";
                saveFileDialog1.FilterIndex = 0;
                saveFileDialog1.RestoreDirectory = true;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string mArchivo = saveFileDialog1.FileName;
                    if (File.Exists(mArchivo)) File.Delete(mArchivo);
                    reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, mArchivo);
                    
                    Process prExcel = new Process();
                    prExcel.StartInfo.FileName = "excel.exe";
                    prExcel.StartInfo.Arguments = string.Format("\"{0}\"", mArchivo);
                    prExcel.Start();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Se produjo el siguiente error al generar el reporte.{0}{1}", Environment.NewLine, ex.Message), mCaption, MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
