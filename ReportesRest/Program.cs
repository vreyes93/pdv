﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

namespace ReportesRest
{
    static class Program
    {
        /// <summary>
        /// Se debe ejectutar con NombreServicio identifica el nombre del servicio, [valor] parámetro a enviar al servicio, [DES ó PRU] para especificar desarrollo o pruebas, al no especificar este parámetro, se conectará a PRODUCCION
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            string mServicio = "";

            try
            {
                mServicio = args[0];
                mServicio = mServicio.Trim().ToLower();
            }
            catch
            {
                MessageBox.Show("Debe especificar el servicio a consumir.", "ReportesRest");
                Application.Exit();
            }

            string mParametro = "";
            try
            {
                mParametro = args[1];
            }
            catch
            {
                mParametro = "";
            }

            if (mParametro == "-") mParametro = "";

            string mAmbiente = "PRO";
            string mURL = "http://sql.fiesta.local/RestServices/api";

            try
            {
                mAmbiente = args[2];
                mAmbiente = mAmbiente.Trim().ToUpper();
            }
            catch
            {
                mAmbiente = "PRO";
            }

            if (mAmbiente == "-") mAmbiente = "PRO";

            string mMensaje = "";
            try
            {
                mMensaje = args[3];
            }
            catch
            {
                mMensaje = "";
            }

            if (mAmbiente == "DES") mURL = "http://localhost:53874/api";
            if (mAmbiente == "PRU") mURL = "http://sql.fiesta.local/RestServicesPruebas/api";

            string mLlamada = string.Format("{0}/{1}/{2}", mURL, mServicio, mParametro);
            WebRequest request = WebRequest.Create(mLlamada);
            
            request.Method = "GET";
            request.ContentType = "application/json";

            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            if (mMensaje.Trim().ToUpper() == "S") MessageBox.Show(responseString.ToString() + "\n\n" + mLlamada, "ReportesRest");
        }
    }
}
