﻿using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace Requisiciones
{
    public class Catalogo
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
    }

    partial class frmRequisiciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRequisiciones));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem1 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.exportarExcel = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.actualizar = new DevExpress.XtraBars.BarButtonItem();
            this.fechaFinal = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.tipo = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.fechaInicial = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.verLista = new DevExpress.XtraBars.BarButtonItem();
            this.verTarjetas = new DevExpress.XtraBars.BarButtonItem();
            this.cbTipo = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.btnCambiar = new DevExpress.XtraBars.BarButtonItem();
            this.cbFecha = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.trasladar = new DevExpress.XtraBars.BarButtonItem();
            this.sistema = new DevExpress.XtraBars.BarEditItem();
            this.repositorySistema = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.copiar = new DevExpress.XtraBars.BarButtonItem();
            this.fechaArmado = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.tipoArmado = new DevExpress.XtraBars.BarEditItem();
            this.repositoryTipoArmado = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.icbTipoServicio = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup9 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup8 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridRequisiciones = new DevExpress.XtraGrid.GridControl();
            this.gridViewRequisiciones = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridViewRequisicionesCard = new DevExpress.XtraGrid.Views.Card.CardView();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositorySistema)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTipoArmado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRequisiciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRequisiciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRequisicionesCard)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Appearance.Options.UseFont = true;
            this.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnSalir.Location = new System.Drawing.Point(164, 206);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(151, 40);
            this.btnSalir.TabIndex = 6;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Images = this.imageList1;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.exportarExcel,
            this.barButtonItem1,
            this.actualizar,
            this.fechaFinal,
            this.tipo,
            this.fechaInicial,
            this.verLista,
            this.verTarjetas,
            this.cbTipo,
            this.btnCambiar,
            this.cbFecha,
            this.trasladar,
            this.sistema,
            this.copiar,
            this.fechaArmado,
            this.tipoArmado,
            this.icbTipoServicio});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 33;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1,
            this.repositoryItemDateEdit2,
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2,
            this.repositoryItemDateEdit3,
            this.repositoryItemComboBox3,
            this.repositoryItemComboBox4,
            this.repositorySistema,
            this.repositoryItemDateEdit4,
            this.repositoryTipoArmado,
            this.repositoryItemLookUpEdit1,
            this.repositoryItemImageComboBox1});
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbonControl1.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.ShowOnMultiplePages;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(1259, 95);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Download.ico");
            this.imageList1.Images.SetKeyName(1, "Compose Email.ico");
            this.imageList1.Images.SetKeyName(2, "tratamiento.ico");
            this.imageList1.Images.SetKeyName(3, "menos.ico");
            this.imageList1.Images.SetKeyName(4, "excel3.ICO");
            this.imageList1.Images.SetKeyName(5, "excel.ICO");
            this.imageList1.Images.SetKeyName(6, "Document.ico");
            this.imageList1.Images.SetKeyName(7, "door.png");
            this.imageList1.Images.SetKeyName(8, "refresh.ico");
            this.imageList1.Images.SetKeyName(9, "archivero.ico");
            this.imageList1.Images.SetKeyName(10, "disco_a2.ico");
            this.imageList1.Images.SetKeyName(11, "notas.ico");
            this.imageList1.Images.SetKeyName(12, "Email 3.ico");
            this.imageList1.Images.SetKeyName(13, "trasladada.ico");
            // 
            // exportarExcel
            // 
            this.exportarExcel.Caption = "Exportar Reqs. a Excel";
            this.exportarExcel.Id = 10;
            this.exportarExcel.ImageOptions.ImageIndex = 5;
            this.exportarExcel.ImageOptions.LargeImageIndex = 5;
            this.exportarExcel.Name = "exportarExcel";
            this.exportarExcel.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            toolTipTitleItem1.Text = "Exportar ";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Exportar a Excel las requisiciones";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.exportarExcel.SuperTip = superToolTip1;
            this.exportarExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.exportarExcel_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Salir del sistema";
            this.barButtonItem1.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItem1.Id = 12;
            this.barButtonItem1.ImageOptions.ImageIndex = 7;
            this.barButtonItem1.ImageOptions.LargeImageIndex = 7;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            toolTipTitleItem2.Text = "Salir";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Haga clic aquí para salir del sistema";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.barButtonItem1.SuperTip = superToolTip2;
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // actualizar
            // 
            this.actualizar.Caption = "Actualizar Requisiciones";
            this.actualizar.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.actualizar.Id = 14;
            this.actualizar.ImageOptions.ImageIndex = 8;
            this.actualizar.Name = "actualizar";
            this.actualizar.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            toolTipTitleItem3.Text = "Actualizar";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Haga clic aquí para actualizar las requisiciones";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.actualizar.SuperTip = superToolTip3;
            this.actualizar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.actualizar_ItemClick);
            // 
            // fechaFinal
            // 
            this.fechaFinal.Caption = "Fecha final:  ";
            this.fechaFinal.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.fechaFinal.Edit = this.repositoryItemDateEdit2;
            this.fechaFinal.EditWidth = 110;
            this.fechaFinal.Id = 16;
            this.fechaFinal.Name = "fechaFinal";
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // tipo
            // 
            this.tipo.Caption = "Filtar por:     ";
            this.tipo.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.tipo.Edit = this.repositoryItemComboBox1;
            this.tipo.EditWidth = 110;
            this.tipo.Id = 17;
            this.tipo.Name = "tipo";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // fechaInicial
            // 
            this.fechaInicial.Caption = "Fecha inicial:";
            this.fechaInicial.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.fechaInicial.Edit = this.repositoryItemDateEdit3;
            this.fechaInicial.EditWidth = 110;
            this.fechaInicial.Id = 19;
            this.fechaInicial.Name = "fechaInicial";
            // 
            // repositoryItemDateEdit3
            // 
            this.repositoryItemDateEdit3.AutoHeight = false;
            this.repositoryItemDateEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit3.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit3.Name = "repositoryItemDateEdit3";
            // 
            // verLista
            // 
            this.verLista.Caption = "Ver en lista";
            this.verLista.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.verLista.Id = 20;
            this.verLista.ImageOptions.ImageIndex = 1;
            this.verLista.Name = "verLista";
            this.verLista.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            toolTipItem4.Text = "Ver las requisiciones en lista";
            superToolTip4.Items.Add(toolTipItem4);
            this.verLista.SuperTip = superToolTip4;
            this.verLista.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.verLista_ItemClick);
            // 
            // verTarjetas
            // 
            this.verTarjetas.Caption = "Ver en tarjetas";
            this.verTarjetas.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.verTarjetas.Id = 21;
            this.verTarjetas.ImageOptions.ImageIndex = 6;
            this.verTarjetas.Name = "verTarjetas";
            this.verTarjetas.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            toolTipItem5.Text = "Ver las requisiciones en tarjetas";
            superToolTip5.Items.Add(toolTipItem5);
            this.verTarjetas.SuperTip = superToolTip5;
            this.verTarjetas.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.verTarjetas_ItemClick);
            // 
            // cbTipo
            // 
            this.cbTipo.Caption = "Tipo:";
            this.cbTipo.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.cbTipo.Edit = this.repositoryItemComboBox3;
            this.cbTipo.EditWidth = 165;
            this.cbTipo.Id = 23;
            this.cbTipo.Name = "cbTipo";
            this.cbTipo.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.AutoHeight = false;
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // btnCambiar
            // 
            this.btnCambiar.Caption = "Cambiar la requisición seleccionada";
            this.btnCambiar.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btnCambiar.Id = 24;
            this.btnCambiar.ImageOptions.ImageIndex = 9;
            this.btnCambiar.Name = "btnCambiar";
            toolTipTitleItem4.Text = "Cambiar requisición";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Cambia de tipo la requisición seleccionada";
            superToolTip6.Items.Add(toolTipTitleItem4);
            superToolTip6.Items.Add(toolTipItem6);
            this.btnCambiar.SuperTip = superToolTip6;
            this.btnCambiar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCambiar_ItemClick);
            // 
            // cbFecha
            // 
            this.cbFecha.Caption = "Fecha validar:";
            this.cbFecha.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.cbFecha.Edit = this.repositoryItemComboBox4;
            this.cbFecha.EditWidth = 121;
            this.cbFecha.Id = 25;
            this.cbFecha.Name = "cbFecha";
            // 
            // repositoryItemComboBox4
            // 
            this.repositoryItemComboBox4.AutoHeight = false;
            this.repositoryItemComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox4.Items.AddRange(new object[] {
            "Fecha Armado",
            "Fecha Transporte"});
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            // 
            // trasladar
            // 
            this.trasladar.Caption = "Trasladar al Sistema de Armados";
            this.trasladar.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.trasladar.Id = 26;
            this.trasladar.ImageOptions.ImageIndex = 13;
            this.trasladar.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T));
            this.trasladar.Name = "trasladar";
            this.trasladar.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.trasladar.ShortcutKeyDisplayString = "Ctrl+T";
            this.trasladar.ShowItemShortcut = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Text = "Trasladar información";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Esta opción toma la requisición seleccionada y la graba en el sistema de armados";
            toolTipTitleItem6.LeftIndent = 6;
            toolTipTitleItem6.Text = "Para activar esta opción presione Ctrl+T";
            superToolTip7.Items.Add(toolTipTitleItem5);
            superToolTip7.Items.Add(toolTipItem7);
            superToolTip7.Items.Add(toolTipSeparatorItem1);
            superToolTip7.Items.Add(toolTipTitleItem6);
            this.trasladar.SuperTip = superToolTip7;
            this.trasladar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.trasladar_ItemClick);
            // 
            // sistema
            // 
            this.sistema.Caption = "Sistema:";
            this.sistema.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.sistema.Edit = this.repositorySistema;
            this.sistema.EditWidth = 140;
            this.sistema.Id = 27;
            this.sistema.Name = "sistema";
            toolTipTitleItem7.Text = "Sistema";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Seleccione el sistema al que desee grabar el envío";
            superToolTip8.Items.Add(toolTipTitleItem7);
            superToolTip8.Items.Add(toolTipItem8);
            this.sistema.SuperTip = superToolTip8;
            this.sistema.EditValueChanged += new System.EventHandler(this.sistema_EditValueChanged);
            // 
            // repositorySistema
            // 
            this.repositorySistema.AutoHeight = false;
            this.repositorySistema.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositorySistema.DropDownRows = 20;
            this.repositorySistema.Name = "repositorySistema";
            // 
            // copiar
            // 
            this.copiar.Caption = "Enviar correo al vendedor";
            this.copiar.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.copiar.Id = 28;
            this.copiar.ImageOptions.ImageIndex = 12;
            this.copiar.Name = "copiar";
            this.copiar.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            toolTipTitleItem8.Text = "Enviar correo";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Enviar correo al vendedor con la información de la requisición o envío";
            superToolTip9.Items.Add(toolTipTitleItem8);
            superToolTip9.Items.Add(toolTipItem9);
            this.copiar.SuperTip = superToolTip9;
            this.copiar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.copiar_ItemClick);
            // 
            // fechaArmado
            // 
            this.fechaArmado.Caption = "Fecha:   ";
            this.fechaArmado.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.fechaArmado.Edit = this.repositoryItemDateEdit4;
            this.fechaArmado.EditWidth = 140;
            this.fechaArmado.Id = 29;
            this.fechaArmado.Name = "fechaArmado";
            toolTipTitleItem9.Text = "Fecha de Armado";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Esta es la fecha de armado que será traslada al Sistema de Armados";
            superToolTip10.Items.Add(toolTipTitleItem9);
            superToolTip10.Items.Add(toolTipItem10);
            this.fechaArmado.SuperTip = superToolTip10;
            // 
            // repositoryItemDateEdit4
            // 
            this.repositoryItemDateEdit4.AutoHeight = false;
            this.repositoryItemDateEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit4.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit4.Name = "repositoryItemDateEdit4";
            // 
            // tipoArmado
            // 
            this.tipoArmado.Caption = "Tipo:      ";
            this.tipoArmado.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.tipoArmado.Edit = this.repositoryTipoArmado;
            this.tipoArmado.EditWidth = 140;
            this.tipoArmado.Id = 30;
            this.tipoArmado.Name = "tipoArmado";
            this.tipoArmado.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            toolTipTitleItem10.Text = "Tipo de Armado";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Seleccione aquí el tipo de armado que desea grabar en el envío al Sistema de Arma" +
    "dos";
            superToolTip11.Items.Add(toolTipTitleItem10);
            superToolTip11.Items.Add(toolTipItem11);
            this.tipoArmado.SuperTip = superToolTip11;
            // 
            // repositoryTipoArmado
            // 
            this.repositoryTipoArmado.AutoHeight = false;
            this.repositoryTipoArmado.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryTipoArmado.DropDownRows = 40;
            this.repositoryTipoArmado.Name = "repositoryTipoArmado";
            // 
            // icbTipoServicio
            // 
            this.icbTipoServicio.Caption = "Tipo:";
            this.icbTipoServicio.Edit = this.repositoryItemImageComboBox1;
            this.icbTipoServicio.EditWidth = 165;
            this.icbTipoServicio.Id = 32;
            this.icbTipoServicio.Name = "icbTipoServicio";
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup3,
            this.ribbonPageGroup7,
            this.ribbonPageGroup2,
            this.ribbonPageGroup4,
            this.ribbonPageGroup5,
            this.ribbonPageGroup9,
            this.ribbonPageGroup8,
            this.ribbonPageGroup6});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Opciones";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.fechaInicial);
            this.ribbonPageGroup1.ItemLinks.Add(this.fechaFinal);
            this.ribbonPageGroup1.ItemLinks.Add(this.tipo);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Fechas";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.cbFecha);
            this.ribbonPageGroup3.ItemLinks.Add(this.icbTipoServicio);
            this.ribbonPageGroup3.ItemLinks.Add(this.btnCambiar);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Tipo";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.ItemLinks.Add(this.actualizar);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.Text = "Actualizar";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.verLista);
            this.ribbonPageGroup2.ItemLinks.Add(this.verTarjetas);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Vistas";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.exportarExcel);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "Exportar";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.sistema);
            this.ribbonPageGroup5.ItemLinks.Add(this.fechaArmado);
            this.ribbonPageGroup5.ItemLinks.Add(this.tipoArmado);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "Datos";
            // 
            // ribbonPageGroup9
            // 
            this.ribbonPageGroup9.ItemLinks.Add(this.trasladar);
            this.ribbonPageGroup9.Name = "ribbonPageGroup9";
            this.ribbonPageGroup9.Text = "Grabar";
            // 
            // ribbonPageGroup8
            // 
            this.ribbonPageGroup8.ItemLinks.Add(this.copiar);
            this.ribbonPageGroup8.Name = "ribbonPageGroup8";
            this.ribbonPageGroup8.Text = "Correo";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.Text = "Salir";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            // 
            // gridRequisiciones
            // 
            this.gridRequisiciones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridRequisiciones.Location = new System.Drawing.Point(0, 95);
            this.gridRequisiciones.MainView = this.gridViewRequisiciones;
            this.gridRequisiciones.Name = "gridRequisiciones";
            this.gridRequisiciones.Size = new System.Drawing.Size(1259, 606);
            this.gridRequisiciones.TabIndex = 9;
            this.gridRequisiciones.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewRequisiciones,
            this.gridViewRequisicionesCard});
            this.gridRequisiciones.ViewRegistered += new DevExpress.XtraGrid.ViewOperationEventHandler(this.gridRequisiciones_ViewRegistered);
            // 
            // gridViewRequisiciones
            // 
            this.gridViewRequisiciones.GridControl = this.gridRequisiciones;
            this.gridViewRequisiciones.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridViewRequisiciones.Name = "gridViewRequisiciones";
            this.gridViewRequisiciones.OptionsFind.SearchInPreview = true;
            this.gridViewRequisiciones.PaintStyleName = "Web";
            this.gridViewRequisiciones.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridViewRequisiciones_RowStyle);
            this.gridViewRequisiciones.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewRequisiciones_KeyDown);
            this.gridViewRequisiciones.DoubleClick += new System.EventHandler(this.gridViewRequisiciones_DoubleClick);
            // 
            // gridViewRequisicionesCard
            // 
            this.gridViewRequisicionesCard.FocusedCardTopFieldIndex = 0;
            this.gridViewRequisicionesCard.GridControl = this.gridRequisiciones;
            this.gridViewRequisicionesCard.Name = "gridViewRequisicionesCard";
            this.gridViewRequisicionesCard.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto;
            this.gridViewRequisicionesCard.CustomDrawCardCaption += new DevExpress.XtraGrid.Views.Card.CardCaptionCustomDrawEventHandler(this.gridViewRequisicionesCard_CustomDrawCardCaption);
            // 
            // frmRequisiciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnSalir;
            this.ClientSize = new System.Drawing.Size(1259, 701);
            this.Controls.Add(this.gridRequisiciones);
            this.Controls.Add(this.ribbonControl1);
            this.Controls.Add(this.btnSalir);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmRequisiciones";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Requisiciones de Armados";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmRequisiciones_Load);
            this.Shown += new System.EventHandler(this.frmRequisiciones_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositorySistema)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTipoArmado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRequisiciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRequisiciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRequisicionesCard)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion




        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraBars.BarButtonItem exportarExcel;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.BarButtonItem actualizar;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraBars.BarEditItem fechaFinal;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraGrid.GridControl gridRequisiciones;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewRequisiciones;
        private DevExpress.XtraBars.BarEditItem tipo;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraBars.BarEditItem fechaInicial;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraBars.BarButtonItem verLista;
        private DevExpress.XtraBars.BarButtonItem verTarjetas;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraGrid.Views.Card.CardView gridViewRequisicionesCard;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private DevExpress.XtraBars.BarEditItem cbTipo;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraBars.BarButtonItem btnCambiar;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarEditItem cbFecha;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox4;
        private DevExpress.XtraBars.BarButtonItem trasladar;
        private DevExpress.XtraBars.BarEditItem sistema;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositorySistema;
        private DevExpress.XtraBars.BarButtonItem copiar;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.BarEditItem fechaArmado;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup8;
        private DevExpress.XtraBars.BarEditItem tipoArmado;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryTipoArmado;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup9;
        private RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraBars.BarEditItem icbTipoServicio;
        private RepositoryItemImageComboBox repositoryItemImageComboBox1;
    }


}