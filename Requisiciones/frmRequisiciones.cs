﻿using System.Configuration;

using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Card;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Data;
using DevExpress.XtraGrid;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using System.Net.Mail;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraBars;
using System.Net;
using Newtonsoft.Json;
using DevExpress.XtraEditors.Controls;
using FiestaNETRestServices.Models;

namespace Requisiciones
{
    public partial class frmRequisiciones : Form
    {
        bool mYa = false;
        DataSet ds = new DataSet();
        public frmRequisiciones()
        {
            InitializeComponent();
        }

        private void frmRequisiciones_Load(object sender, EventArgs e)
        {
            //CargarTiposArmado();
            CargarTiposDeServicio();
            CargaRegiones();
        }
        
        private void frmRequisiciones_Shown(object sender, EventArgs e)
        {
            fechaInicial.EditValue = DateTime.Now.Date;
            fechaFinal.EditValue = DateTime.Now.Date;
            fechaArmado.EditValue = DateTime.Now.Date.AddDays(1);

            DataSet dsSistemas = new DataSet();

            wsArmados.wsArmados ws = new wsArmados.wsArmados();
            ws.Url = Globales.pUrl;
            ws.Timeout = 999999999;

            dsSistemas = ws.DevuelveSistemas();
            (sistema.Edit as RepositoryItemComboBox).Items.Clear();

            for (int ii = 0; ii < dsSistemas.Tables["sistemas"].Rows.Count; ii++)
            {
                (sistema.Edit as RepositoryItemComboBox).Items.Add(dsSistemas.Tables["sistemas"].Rows[ii]["sistema"]);
            }
            
            sistema.EditValue = "arma2_guate";
            //tipo.EditValue = "Ciudad";
            //cbTipo.EditValue = "Armado";
            icbTipoServicio.EditValue = "A";
            cbFecha.EditValue = "Fecha Armado";
        }
        
        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        
        private void actualizar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string mMensaje = "";
                DataSet dsBlanco = new DataSet();

                wsArmados.wsArmados ws = new wsArmados.wsArmados();
                ws.Url = Globales.pUrl;

                ds = dsBlanco;
                ws.Timeout = 999999999;

                //var val = tipo.ImageIndex;

                if (!ws.DevuelveRequisiciones(Globales.pUsuario, ref mMensaje, Convert.ToDateTime(fechaInicial.EditValue), Convert.ToDateTime(fechaFinal.EditValue), Convert.ToString(tipo.EditValue), ref ds, Convert.ToString(cbFecha.EditValue)))
                {
                    MessageBox.Show(mMensaje, Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                
                gridRequisiciones.DataSource = ds.Tables["requisiciones"];

                mYa = true;
                gridViewRequisiciones.OptionsView.ShowAutoFilterRow = true;

                foreach (GridColumn column in gridViewRequisiciones.Columns)
                {
                    if (column.Name == "colReq") column.Visible = false;
                    if (column.Name == "colCorreoUsuario") column.Visible = false;
                    if (column.Name == "colCorreoVendedor") column.Visible = false;
                    if (column.Name == "colCorreoSupervisor") column.Visible = false;
                    if (column.Name == "colNombreUsuario") column.Visible = false;

                    if (column.Name == "colMonto") column.DisplayFormat.FormatType = FormatType.Numeric;
                    if (column.Name == "colMonto") column.DisplayFormat.FormatString = "###,###,###,###,###,###.00";

                    if (column.Name == "colFechaGrabada") column.DisplayFormat.FormatType = FormatType.DateTime;
                    if (column.Name == "colFechaGrabada") column.DisplayFormat.FormatString = "g";
                    if (column.Name == "colFechaModificada") column.DisplayFormat.FormatType = FormatType.DateTime;
                    if (column.Name == "colFechaModificada") column.DisplayFormat.FormatString = "g";

                    if (column.Name == "colRequisicion")
                    {
                        column.Width = 100;
                        column.AppearanceHeader.Options.UseTextOptions = true;
                        column.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                        column.AppearanceCell.Options.UseTextOptions = true;
                        column.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
                    }

                    column.MinWidth = column.GetBestWidth();
                    column.OptionsColumn.ReadOnly = true;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al cargar las requisiciones. {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void verLista_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gridRequisiciones.MainView = gridViewRequisiciones;
        }

        private void verTarjetas_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gridRequisiciones.MainView = gridViewRequisicionesCard;
            gridViewRequisicionesCard.CardWidth = 400;
            
            foreach (GridColumn column in gridViewRequisicionesCard.Columns)
            {
                column.MinWidth = column.GetBestWidth();
                column.OptionsColumn.ReadOnly = true;
                
                if (column.Name == "colRequisicion")
                {
                    column.AppearanceCell.Options.UseTextOptions = true;
                    column.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Near;
                }
            }

        }

        private void exportarExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (gridViewRequisiciones.DataRowCount == 0)
                {
                    MessageBox.Show("No hay datos para exportar.", Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                saveFileDialog1.Title = "Exportar requisiciones";

                saveFileDialog1.InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString();
                saveFileDialog1.Filter = "xlsx files (*.xlsx)|*.xlsx";
                saveFileDialog1.FilterIndex = 1;
                saveFileDialog1.RestoreDirectory = true;

                if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string mArchivo = saveFileDialog1.FileName;
                    if (File.Exists(mArchivo)) File.Delete(mArchivo);

                    gridViewRequisiciones.ExportToXlsx(mArchivo);

                    Process prExcel = new Process();
                    prExcel.StartInfo.FileName = "excel.exe";
                    prExcel.StartInfo.Arguments = "\"" + mArchivo + "\"";
                    prExcel.Start();
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al exportar a excel. {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Application.Exit();
        }

        private void gridViewRequisicionesCard_CustomDrawCardCaption(object sender, DevExpress.XtraGrid.Views.Card.CardCaptionCustomDrawEventArgs e)
        {
            e.CardCaption = string.Format("Requsición No. {0}", gridViewRequisicionesCard.GetDataRow(e.RowHandle)["Requisicion"]);
        }
        
        private void gridViewRequisiciones_DoubleClick(object sender, EventArgs e)
        {
            GridView view = (GridView)sender;
            Point pt = view.GridControl.PointToClient(Control.MousePosition);
            DoRowDoubleClick(view, pt);
        }
        
        private static void DoRowDoubleClick(GridView view, Point pt)
        {
            try
            {
                GridHitInfo info = view.CalcHitInfo(pt);
                if (info.InRow || info.InRowCell)
                {
                    string mMensaje = ""; string mGrabada = "";
                    wsArmados.wsArmados ws = new wsArmados.wsArmados();
                    ws.Url = Globales.pUrl;

                    if (!ws.MarcaGrabadaRequisicion(ref mMensaje, ref mGrabada, Convert.ToString(view.GetDataRow(info.RowHandle)["Req"]), Convert.ToString(view.GetDataRow(info.RowHandle)["Envio"])))
                    {
                        MessageBox.Show(mMensaje, Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    view.SetRowCellValue(info.RowHandle, "Grabada", mGrabada);

                    //string colCaption = info.Column == null ? "N/A" : info.Column.GetCaption();
                    //MessageBox.Show(string.Format("DoubleClick on row: {0}, column: {1}.", info.RowHandle, colCaption));
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al marcar la requisición. {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void gridRequisiciones_ViewRegistered(object sender, ViewOperationEventArgs e)
        {
            if (e.View.LevelName == "Detalle")
            {                
                e.View.KeyDown += View_KeyDown;
                GridView view = e.View as GridView;

                view.OptionsView.ShowAutoFilterRow = false;
                view.Columns["Requisicion"].Visible = false;
                view.Columns["Cantidad"].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Near;
                view.Columns["Cantidad"].AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Near;
                view.Columns["Cantidad"].Width = 50;  
                
                for (int i = 0; i < view.Columns.Count; i++)
                {
                    view.Columns[i].OptionsColumn.ReadOnly = true;
                }
            }
        }

        void View_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                GridView view = (gridRequisiciones.FocusedView as GridView);
                var row = view.GetFocusedRow();
                DataRowView mRow = (DataRowView)row;

                string mSeleccionado = mRow["Armado"].ToString();
                string mNuevoSeleccionado = "Sí";

                if (mSeleccionado == "Sí") mNuevoSeleccionado = "No";
                view.SetRowCellValue(view.FocusedRowHandle, "Armado", mNuevoSeleccionado);                
            }

            //throw new NotImplementedException();            
        }

        private void gridViewRequisiciones_RowStyle(object sender, RowStyleEventArgs e)
        {
            if (!mYa) return;
            if (e.RowHandle < 0) return;
            GridView view = sender as GridView;

            if (Convert.ToString(view.GetDataRow(e.RowHandle)["Grabada"]) == "Sí")
            {
                e.Appearance.BackColor = Color.GreenYellow;
            }
        }

        private void btnCambiar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string mMensaje = "";
                wsArmados.wsArmados ws = new wsArmados.wsArmados();
                ws.Url = Globales.pUrl;

                //TODO:Validar nulo
                var mCodigo = icbTipoServicio.EditValue;
                string mDescripcion;
                if (icbTipoServicio.EditValue == null)
                {
                    return;
                }
                    mDescripcion = repositoryItemImageComboBox1.Items.GetItem(icbTipoServicio.EditValue).Description.ToString();
                
                

                if (!ws.CambiarTipoRequisicion(ref mMensaje, gridViewRequisiciones.GetFocusedRowCellValue("Req").ToString(), icbTipoServicio.EditValue.ToString()))
                {
                    MessageBox.Show(mMensaje, Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                gridViewRequisiciones.SetRowCellValue(gridViewRequisiciones.FocusedRowHandle, "Tipo", mDescripcion.ToString());
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al cambiar la requisición. {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void GrabarRequisicion()
        {
            MySqlCommand mCommand = new MySqlCommand(); MySqlTransaction mTransaction = null;
            MySqlConnection mConexion = new MySqlConnection(string.Format("server=whitehouse;uid=root;pwd=fred12345;database={0};", sistema.EditValue));

            try
            {
                if (gridViewRequisiciones.RowCount == 0) return;

                if (gridViewRequisiciones.GetFocusedRowCellValue("Grabada").ToString() == "Sí")
                {
                    MessageBox.Show("Este registro ya fue grabado.", Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                string mArticulos = ""; string mSeparador = "";
                string mEnvio = gridViewRequisiciones.GetFocusedRowCellValue("Envio").ToString();
                string mRequisicion = gridViewRequisiciones.GetFocusedRowCellValue("Req").ToString();
                string mNombreCliente = gridViewRequisiciones.GetFocusedRowCellValue("NombreCliente").ToString();

                DataRow[] mRows = ds.Tables["requisicionesDet"].Select(string.Format("Requisicion = {0} AND Armado = 'Sí'", mRequisicion), "Articulo");

                for (int ii = 0; ii < mRows.Length; ii++)
                {
                    mArticulos = string.Format("{0}{1}{2}  -  {3}", mArticulos, mSeparador, mRows[ii]["Articulo"], mRows[ii]["Descripcion"]);
                    mSeparador = ", ";
                }

                if (mArticulos.Trim().Length == 0)
                {
                    MessageBox.Show("Este registro no hay artículos que requieran Armado.", Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (mEnvio.Trim().Length == 0) mEnvio = mRequisicion;

                if (mEnvio.Trim().Length == 0 && mNombreCliente.Trim().Length > 0)
                {
                    MessageBox.Show("Este registro no tiene envío, no es posible grabarla en el sistema de Armados.", Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                string mTipo = tipoArmado.EditValue.ToString();
                if (mTipo.Trim().Length == 0)
                {
                    MessageBox.Show("Debe seleccionar el tipo que se grabará en el sistema de Armados.", Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                string[] mNombres = mNombreCliente.Split(new string[] { "," }, StringSplitOptions.None);

                string mNombre = "";
                string mApellido = "";

                mApellido = mNombres[0];
                if (mNombres.Length > 1) mNombre = mNombres[1];

                if (mNombreCliente.Trim().Length == 0)
                {
                    mNombre = "";
                    mApellido = gridViewRequisiciones.GetFocusedRowCellValue("Tienda").ToString();
                    mEnvio = mRequisicion; //string.Format("TIENDA {0}", gridViewRequisiciones.GetFocusedRowCellValue("Tienda").ToString());
                }

                mConexion.Open();
                mTransaction = mConexion.BeginTransaction();

                mCommand.Connection = mConexion;
                mCommand.Transaction = mTransaction;

                mCommand.Parameters.Add("@otherid", MySqlDbType.VarChar).Value = mEnvio;
                mCommand.Parameters.Add("@cliente_nombre", MySqlDbType.VarChar).Value = mNombre;
                mCommand.Parameters.Add("@cliente_apellido", MySqlDbType.VarChar).Value = mApellido; 
                mCommand.Parameters.Add("@cliente_zona", MySqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@cliente_direccion", MySqlDbType.VarChar).Value = string.Format("{0}, {1}, {2}", gridViewRequisiciones.GetFocusedRowCellValue("Direccion").ToString(), gridViewRequisiciones.GetFocusedRowCellValue("Municipio").ToString(), gridViewRequisiciones.GetFocusedRowCellValue("Departamento").ToString());
                mCommand.Parameters.Add("@cliente_ciudad", MySqlDbType.VarChar).Value = gridViewRequisiciones.GetFocusedRowCellValue("Municipio").ToString();
                mCommand.Parameters.Add("@cliente_tel1", MySqlDbType.VarChar).Value = gridViewRequisiciones.GetFocusedRowCellValue("Celular").ToString();
                mCommand.Parameters.Add("@cliente_tel2", MySqlDbType.VarChar).Value = gridViewRequisiciones.GetFocusedRowCellValue("TelefonoCasa").ToString();
                mCommand.Parameters.Add("@cliente_tel3", MySqlDbType.VarChar).Value = gridViewRequisiciones.GetFocusedRowCellValue("Factura").ToString();

                DateTime mFechaEnvio;
                try
                {
                    mFechaEnvio = Convert.ToDateTime(gridViewRequisiciones.GetFocusedRowCellValue("FechaEnvio"));
                }
                catch
                {
                    mFechaEnvio = DateTime.Now.Date;
                }
                mCommand.Parameters.Add("@deliver_date", MySqlDbType.Date).Value = mFechaEnvio;

                DateTime mFechaTransportista; string mTransportista = "";
                try
                {
                    mFechaTransportista = Convert.ToDateTime(gridViewRequisiciones.GetFocusedRowCellValue("FechaTransportista"));
                    mTransportista = string.Format("Transportista: {0} Fecha Transportista: {1}/{2}/{3}", gridViewRequisiciones.GetFocusedRowCellValue("Transportista").ToString(), Convert.ToDateTime(gridViewRequisiciones.GetFocusedRowCellValue("FechaTransportista").ToString()).Day, Convert.ToDateTime(gridViewRequisiciones.GetFocusedRowCellValue("FechaTransportista").ToString()).Month, Convert.ToDateTime(gridViewRequisiciones.GetFocusedRowCellValue("FechaTransportista").ToString()).Year);
                }
                catch
                {
                    mTransportista = "";
                }

                DateTime mFechaFactura;
                try
                {
                    mFechaFactura = Convert.ToDateTime(gridViewRequisiciones.GetFocusedRowCellValue("FechaFactura"));
                }
                catch
                {
                    mFechaFactura = DateTime.Now.Date;
                }
                mCommand.Parameters.Add("@sale_date", MySqlDbType.Date).Value = mFechaFactura;
               
                mCommand.Parameters.Add("@tienda", MySqlDbType.VarChar).Value = gridViewRequisiciones.GetFocusedRowCellValue("Tienda").ToString();
                mCommand.Parameters.Add("@vendedor", MySqlDbType.VarChar).Value = gridViewRequisiciones.GetFocusedRowCellValue("Vendedor").ToString();
                mCommand.Parameters.Add("@assemble_date", MySqlDbType.Date).Value = fechaArmado.EditValue;
                mCommand.Parameters.Add("@notes", MySqlDbType.VarChar).Value = string.Format("[{0}/{1}/{2} {3}] {4} {5} [.]", DateTime.Now.Date.Day, DateTime.Now.Date.Month, DateTime.Now.Date.Year, Globales.pUsuario, gridViewRequisiciones.GetFocusedRowCellValue("Observaciones").ToString(), mTransportista);
                mCommand.Parameters.Add("@complete", MySqlDbType.Int32).Value = 0;
                mCommand.Parameters.Add("@complete_date", MySqlDbType.Date).Value = new DateTime(3000, 1, 1);
                mCommand.Parameters.Add("@status", MySqlDbType.VarChar).Value = "N";
                mCommand.Parameters.Add("@cliente_descr", MySqlDbType.VarChar).Value = string.Format("{0} ({1}, {2}, {3})", mNombreCliente, gridViewRequisiciones.GetFocusedRowCellValue("Celular").ToString(), gridViewRequisiciones.GetFocusedRowCellValue("TelefonoCasa").ToString(), gridViewRequisiciones.GetFocusedRowCellValue("TelefonoOficina").ToString());
                mCommand.Parameters.Add("@prod_work", MySqlDbType.VarChar).Value = 0;
                mCommand.Parameters.Add("@prod_descr", MySqlDbType.VarChar).Value = mArticulos;
                mCommand.Parameters.Add("@prod_cnt", MySqlDbType.VarChar).Value = 1;
                mCommand.Parameters.Add("@prod_charge", MySqlDbType.VarChar).Value = 0;
                mCommand.Parameters.Add("@reg_work", MySqlDbType.Float).Value = 0;
                mCommand.Parameters.Add("@reg_charge", MySqlDbType.Float).Value = 0;

                mCommand.CommandText = string.Format("INSERT INTO {0}.gt_envio ( otherid, cliente_nombre, cliente_apellido, cliente_zona, cliente_direccion, cliente_ciudad, cliente_tel1, cliente_tel2, cliente_tel3, deliver_date, " +
                        "tienda, vendedor, assemble_date, notes, complete, complete_date, sale_date, status, cliente_descr, prod_work, prod_descr, prod_cnt, prod_charge, reg_work, reg_charge ) " +
                    "VALUES ( @otherid, @cliente_nombre, @cliente_apellido, @cliente_zona, @cliente_direccion, @cliente_ciudad, @cliente_tel1, @cliente_tel2, @cliente_tel3, @deliver_date, " +
                        "@tienda, @vendedor, @assemble_date, @notes, @complete, @complete_date, @sale_date, @status, @cliente_descr, @prod_work, @prod_descr, @prod_cnt, @prod_charge, @reg_work, @reg_charge ) ", sistema.EditValue);
                mCommand.ExecuteNonQuery();

                mCommand.CommandText = "SELECT @@IDENTITY";
                Int32 mEnvioID = Convert.ToInt32(mCommand.ExecuteScalar());

                mCommand.Parameters.Add("@articulo", MySqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@descripcion", MySqlDbType.VarChar).Value = "";
                mCommand.Parameters.Add("@articulodesc", MySqlDbType.VarChar).Value = "";

                for (int ii = 0; ii < mRows.Length; ii++)
                {
                    mCommand.Parameters["@articulo"].Value = mRows[ii]["Articulo"];
                    mCommand.Parameters["@descripcion"].Value = mRows[ii]["Descripcion"];
                    mCommand.Parameters["@articulodesc"].Value = string.Format("{0}  -  {1}", mRows[ii]["Articulo"], mRows[ii]["Descripcion"]);

                    Int32 mProductID = 0;
                    mCommand.CommandText = string.Format("SELECT COUNT(*) FROM {0}.gt_product WHERE otherid = @articulo", sistema.EditValue);
                    if (Convert.ToInt32(mCommand.ExecuteScalar()) == 0)
                    {
                        mCommand.CommandText = string.Format("INSERT INTO {0}.gt_product ( otherid, description, status ) VALUES ( @articulo, @articulodesc, 'N' )", sistema.EditValue);
                        mCommand.ExecuteNonQuery();

                        mCommand.CommandText = "SELECT @@IDENTITY";
                        mProductID = Convert.ToInt32(mCommand.ExecuteScalar());
                    }
                    else
                    {
                        mCommand.CommandText = string.Format("SELECT productid FROM {0}.gt_product WHERE otherid = @articulo", sistema.EditValue);
                        mProductID = Convert.ToInt32(mCommand.ExecuteScalar());
                    }

                    mCommand.CommandText = string.Format("SELECT tipoid FROM {0}.gt_tipoarmado WHERE code = '{1}'", sistema.EditValue, mTipo);
                    Int32 mTipoID = Convert.ToInt32(mCommand.ExecuteScalar());

                    mCommand.CommandText = string.Format("INSERT INTO {0}.gt_envio_product ( productid, envioid, tipoid ) VALUES ( {1}, {2}, {3} )", sistema.EditValue, mProductID, mEnvioID, mTipoID);
                    mCommand.ExecuteNonQuery();
                }


                mTransaction.Commit();
                mConexion.Close();

                MessageBox.Show("Envío grabado exitosamente.", Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Information);

                string mMensaje = ""; string mGrabada = "";
                wsArmados.wsArmados ws = new wsArmados.wsArmados();
                ws.Url = Globales.pUrl;

                if (!ws.MarcaGrabadaRequisicion(ref mMensaje, ref mGrabada, Convert.ToString(gridViewRequisiciones.GetFocusedRowCellValue("Req")), Convert.ToString(gridViewRequisiciones.GetFocusedRowCellValue("Envio"))))
                {
                    MessageBox.Show(mMensaje, Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                gridViewRequisiciones.SetRowCellValue(gridViewRequisiciones.FocusedRowHandle, "Grabada", mGrabada);

                if (!ws.GrabarSistemaRequisicion(ref mMensaje, gridViewRequisiciones.GetFocusedRowCellValue("Pedido").ToString(), gridViewRequisiciones.GetFocusedRowCellValue("Envio").ToString(), sistema.EditValue.ToString()))
                {
                    MessageBox.Show(mMensaje, Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                gridViewRequisiciones.SetRowCellValue(gridViewRequisiciones.FocusedRowHandle, "Sistema", sistema.EditValue.ToString());
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                mTransaction.Rollback();
                if (mConexion.State == ConnectionState.Open) mConexion.Close();
                MessageBox.Show(string.Format("Error al grabar la requisición. {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void trasladar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GrabarRequisicion();
        }

        private void copiar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (gridViewRequisiciones.RowCount == 0) return;

                string mEnvio = gridViewRequisiciones.GetFocusedRowCellValue("Envio").ToString();
                string mTienda = gridViewRequisiciones.GetFocusedRowCellValue("Tienda").ToString();
                string mReq = gridViewRequisiciones.GetFocusedRowCellValue("Req").ToString();
                string mRequisicion = gridViewRequisiciones.GetFocusedRowCellValue("Requisicion").ToString();
                string mFactura = gridViewRequisiciones.GetFocusedRowCellValue("Factura").ToString();
                string mPedido = gridViewRequisiciones.GetFocusedRowCellValue("Pedido").ToString();
                string mCliente = gridViewRequisiciones.GetFocusedRowCellValue("Cliente").ToString();
                string mNombreCliente = gridViewRequisiciones.GetFocusedRowCellValue("NombreCliente").ToString();
                string mVendedor = gridViewRequisiciones.GetFocusedRowCellValue("Vendedor").ToString();
                string mCorreoVendedor = gridViewRequisiciones.GetFocusedRowCellValue("CorreoVendedor").ToString();
                string mCorreoUsuario = gridViewRequisiciones.GetFocusedRowCellValue("CorreoUsuario").ToString();
                string mCorreoSupervisor = gridViewRequisiciones.GetFocusedRowCellValue("CorreoSupervisor").ToString();
                string mNombreUsuario = gridViewRequisiciones.GetFocusedRowCellValue("NombreUsuario").ToString();
                string mTipo = gridViewRequisiciones.GetFocusedRowCellValue("Tipo").ToString();
                string mObservaciones = gridViewRequisiciones.GetFocusedRowCellValue("Observaciones").ToString();

                if (mVendedor.Trim().Length == 0) return;

                string mCuerpo = "";
                Globales.InputBox("Cuerpo del correo", "Escriba el texto para el correo:", ref mCuerpo);

                if (mCuerpo.Trim().Length == 0) return;

                using (SmtpClient smtp = new SmtpClient())
                {
                    using (MailMessage mail = new MailMessage())
                    {
                        string mAsunto = string.Format("Requisición No. {0}", mRequisicion);
                        if (mRequisicion.Trim().Length == 0)
                        {
                            mRequisicion = "No existe requisición";
                            mAsunto = string.Format("Envío No. {0} no tiene requisición", mEnvio);
                        }

                        string mNombreTipo = "Armado";
                        string mFirma = "armados";

                        if (mTipo.ToLower().Contains("servicio"))
                        {
                            mNombreTipo = "Servicio";
                            mFirma = "servicios";
                        }

                        mail.Subject = mAsunto;
                        mCuerpo = string.Format("Estimado(a) {0}:<br><br>El departamento de {2} hace de su conocimiento lo siguiente:<br><br>{1}", mVendedor, mCuerpo, mFirma);
                        
                        mail.To.Add(mCorreoVendedor);
                        mail.To.Add("carlos.quezada@mueblesfiesta.com");
                        if (mCorreoSupervisor.Contains("@")) mail.CC.Add(mCorreoSupervisor);

                        mail.IsBodyHtml = true;
                        mail.ReplyToList.Add(mCorreoUsuario);
                        mail.From = new MailAddress(mCorreoUsuario, mNombreUsuario);
                        //mail.From = new MailAddress("puntodeventa@mueblesfiesta.com", mNombreUsuario);

                        StringBuilder mBody = new StringBuilder();

                        mBody.Append("<!DOCTYPE html><html lang='en-us'>");
                        mBody.Append("<head><style type='text/css'> table, td { border: thin solid #000000;} table { border-collapse: collapse; } </style></head>");
                        mBody.Append("<body>");
                        mBody.Append(string.Format("{0}", mCuerpo));
                        mBody.Append(string.Format("<br><br><b>Requisición No.: </b>{0}", mRequisicion));

                        if (!mTipo.ToLower().Contains("servicio"))
                        {
                            mBody.Append(string.Format("<br><b>Envío No.: </b>{0}", mEnvio));
                            mBody.Append(string.Format("<br><b>Factura No.: </b>{0}", mFactura));
                            mBody.Append(string.Format("<br><b>Pedido No.: </b>{0}", mPedido));
                        }

                        mBody.Append(string.Format("<br><b>Tienda: </b>{0}", mTienda));

                        if (mCliente.Trim().Length > 0)
                        {
                            mBody.Append(string.Format("<br><b>Cliente: </b>{0}", mCliente));
                            mBody.Append(string.Format("<br><b>Nombre Cliente: </b>{0}", mNombreCliente));
                        }

                        mBody.Append(string.Format("<br><b>Tipo de {1}: </b>{0}", mTipo, mNombreTipo));
                        mBody.Append(string.Format("<br><b>Observaciones: </b>{0}", mObservaciones));

                        mBody.Append(string.Format("<br><br><b>Artículos:</b><br>"));
                        DataRow[] mRows = ds.Tables["requisicionesDet"].Select(string.Format("Requisicion = {0} AND Armado = 'Sí'", mReq), "Articulo");

                        for (int ii = 0; ii < mRows.Length; ii++)
                        {
                            mBody.Append(string.Format("<br>{0} - {1}", mRows[ii]["Articulo"], mRows[ii]["Descripcion"]));
                        }

                        if (mRows.Length == 0) mBody.Append("<br>No se ingresó ningún artículo para armado.");

                        mBody.Append(string.Format("<br><br>Favor tomar nota de lo anterior.<br><br>"));
                        mBody.Append("<p>Atentamente,</p>");

                        mFirma = string.Format("Departamento de {0} - Muebles Fiesta", mFirma);
                        mBody.Append(string.Format("<p>{0}<BR><b>{1}</b></p>", mNombreUsuario, mFirma));
                        mBody.Append("</body>");
                        mBody.Append("</html>");

                        mail.Body = mBody.ToString();

                        smtp.Host = "smtp.googlemail.com";
                        //smtp.Host = "merkava.mueblesfiesta.com";
                        smtp.Port = 587;
                        smtp.EnableSsl = true;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                        try
                        {
                            smtp.Credentials = new System.Net.NetworkCredential("puntodeventa@productosmultiples.com", "Estrella01");
                            //smtp.Credentials = new System.Net.NetworkCredential("scanner", "gH7%#45");
                            //smtp.Credentials = new System.Net.NetworkCredential("puntodeventa", "jh$%Pjma");
                            smtp.Send(mail);
                        }
                        catch (Exception ex2)
                        {
                            string mm = "";
                            try
                            {
                                mm = ex2.StackTrace.Substring(ex2.StackTrace.IndexOf("línea"));
                            }
                            catch
                            {
                                try
                                {
                                    mm = ex2.StackTrace.Substring(ex2.StackTrace.IndexOf("line"));
                                }
                                catch
                                {
                                    //Nothing
                                }
                            }

                            MessageBox.Show(string.Format("Error al enviar el correo {0} {1}", ex2.Message, mm), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }

                        MessageBox.Show("El correo fue enviado exitosamente", Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }                      



                //var row = (gridRequisiciones.FocusedView as GridView).GetFocusedRow();
                //DataRowView mRow = (DataRowView)row;

                //MessageBox.Show(mRow[0].ToString(), "0");
                //MessageBox.Show(mRow[1].ToString(), "1");
                //MessageBox.Show(mRow[2].ToString(), "2");
                //MessageBox.Show(mRow[3].ToString(), "3");

                //MessageBox.Show(mRow["Armado"].ToString(), "Armado");
                //MessageBox.Show(mRow["Bodega"].ToString(), "Bodega");
                //MessageBox.Show(mRow["Articulo"].ToString(), "Articulo");
                //MessageBox.Show(mRow["Descripcion"].ToString(), "Descripcion");

            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                MessageBox.Show(string.Format("Error al enviar correo de la requisición. {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void CargarTiposArmado()
        {
            MySqlConnection mConexion = new MySqlConnection(string.Format("server=whitehouse;uid=root;pwd=fred12345;database={0};", sistema.EditValue));

            try
            {
                DataTable dt = new DataTable();
                MySqlDataAdapter da = new MySqlDataAdapter(string.Format("SELECT tipoid, code FROM {0}.gt_tipoarmado WHERE expiration_date >= @Fecha ORDER BY tipoid", sistema.EditValue), mConexion);

                da.SelectCommand.Parameters.Add("@Fecha", MySqlDbType.Date).Value = DateTime.Now.Date;

                da.Fill(dt);
                repositoryTipoArmado.Items.Clear();

                for (int ii = 0; ii < dt.Rows.Count; ii++)
                {
                    repositoryTipoArmado.Items.Add(dt.Rows[ii]["code"]);
                }

                tipoArmado.EditValue = dt.Rows[0]["code"];
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                
                MessageBox.Show(string.Format("Error al obtener los tipos de armado. {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CargarTiposDeServicio()
        {
            try
            {
                var mUrlRest = Globales.pUrlRest;
                WebRequest request = WebRequest.Create(string.Format("{0}/TipoServicio", mUrlRest));
                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    MessageBox.Show(string.Format("Error al obtener los tipos de servicios."), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    List<Catalogo> mListadoServicio = new List<Catalogo>();
                    mListadoServicio = JsonConvert.DeserializeObject<List<Catalogo>>(responseString);
                    ImageComboBoxItemCollection coll = repositoryItemImageComboBox1.Items;
                    coll.BeginUpdate();
                    try
                    {
                        foreach (var item in mListadoServicio)
                        {
                            coll.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem(item.Descripcion, item.Codigo, -1));
                        }
                        
                    }
                    finally
                    {
                        coll.EndUpdate();
                    }
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al obtener los tipos de armado. {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CargaRegiones()
        {
            const string EMPRESA_ARMADOS_INDUSTRIALES = "1";
            try
            {
                var mUrlRest = Globales.pUrlRest;
                WebRequest request = WebRequest.Create(string.Format("{0}/Region/{1}", mUrlRest, EMPRESA_ARMADOS_INDUSTRIALES));
                request.Method = "GET";
                request.ContentType = "application/json";

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    MessageBox.Show(string.Format("Error al obtener los tipos de servicios."), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    List<MF_Region> mListadoServicio = new List<MF_Region>();
                    mListadoServicio = JsonConvert.DeserializeObject<List<MF_Region>>(responseString);
                    ComboBoxItemCollection coll = repositoryItemComboBox1.Items;
                    coll.BeginUpdate();
                    try
                    {
                        foreach (var item in mListadoServicio)
                        {
                            coll.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem(item.NOMBRE, item.REGION));
                        }

                    }
                    finally
                    {
                        coll.EndUpdate();
                    }
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al obtener los tipos de armado. {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void sistema_EditValueChanged(object sender, EventArgs e)
        {
            CargarTiposArmado();
            //CargarTiposDeServicio();
            //CargaRegiones();

        }

        private void gridViewRequisiciones_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                gridViewRequisiciones.ExpandMasterRow(gridViewRequisiciones.FocusedRowHandle);
            }


        }


    }
}
