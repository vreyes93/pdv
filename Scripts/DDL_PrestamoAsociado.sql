

ALTER TABLE [prodmult].[MF_Pedido]
ADD MONTO_DESEMBOLSADO [decimal](28, 8) NULL CONSTRAINT MF_Pedido_Monto_desembolsado DEFAULT 0;

ALTER TABLE [prodmult].[MF_Pedido]
ADD PRESTAMO [varchar](25) NULL ;

UPDATE [prodmult].[MF_Pedido]
SET MONTO_DESEMBOLSADO = 0 ;