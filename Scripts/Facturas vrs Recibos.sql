DECLARE @ii AS INT
DECLARE @jj AS INT
DECLARE @FechaValidar AS DATETIME = '2017-01-01'

BEGIN TRY
	DROP TABLE #facturas
END TRY
BEGIN CATCH
	SET @ii = 0
END CATCH

BEGIN TRY
	CREATE TABLE #facturas (	Tienda VARCHAR(4), Vendedor VARCHAR(200), Fecha DATETIME, Factura VARCHAR(20), Monto DECIMAL(12,2), Saldo DECIMAL(12,2), Cliente VARCHAR(20), NombreCliente VARCHAR(100), FormaPago VARCHAR(20), 
								Recibo1 VARCHAR(20), MontoRecibo1 DECIMAL(12,2), TipoRecibo1 VARCHAR(20), FechaRecibo1 DATETIME, FechaDeposito1 DATETIME, DiasDeposito1 INT, Recibo2 VARCHAR(20), MontoRecibo2 DECIMAL(12,2), TipoRecibo2 VARCHAR(20), FechaRecibo2 DATETIME, FechaDeposito2 DATETIME, DiasDeposito2 INT, 
								Recibo3 VARCHAR(20), MontoRecibo3 DECIMAL(12,2), TipoRecibo3 VARCHAR(20), FechaRecibo3 DATETIME, FechaDeposito3 DATETIME, DiasDeposito3 INT, Recibo4 VARCHAR(20), MontoRecibo4 DECIMAL(12,2), TipoRecibo4 VARCHAR(20), FechaRecibo4 DATETIME, FechaDeposito4 DATETIME, DiasDeposito4 INT, 
								Recibo5 VARCHAR(20), MontoRecibo5 DECIMAL(12,2), TipoRecibo5 VARCHAR(20), FechaRecibo5 DATETIME, FechaDeposito5 DATETIME, DiasDeposito5 INT, Financiera VARCHAR(20) )
END TRY
BEGIN CATCH
	SET @ii = 0
END CATCH

DELETE FROM #facturas

DECLARE @Tienda AS VARCHAR(4)
DECLARE @Vendedor AS VARCHAR(200)
DECLARE @Fecha AS DATETIME 
DECLARE @Factura AS VARCHAR(20)
DECLARE @Monto AS DECIMAL(12,2)
DECLARE @FormaPago AS VARCHAR(20)
DECLARE @Cliente AS VARCHAR(20)
DECLARE @NombreCliente AS VARCHAR(200)
DECLARE @Recibo1 AS VARCHAR(20)
DECLARE @MontoRecibo1 AS DECIMAL(12,2)
DECLARE @TipoRecibo1 AS VARCHAR(20)
DECLARE @FechaRecibo1 AS DATETIME
DECLARE @FechaDeposito1 AS DATETIME
DECLARE @DiasDeposito1 AS INT
DECLARE @Recibo2 AS VARCHAR(20)
DECLARE @MontoRecibo2 AS DECIMAL(12,2)
DECLARE @TipoRecibo2 AS VARCHAR(20)
DECLARE @FechaRecibo2 AS DATETIME
DECLARE @FechaDeposito2 AS DATETIME
DECLARE @DiasDeposito2 AS INT
DECLARE @Recibo3 AS VARCHAR(20)
DECLARE @MontoRecibo3 AS DECIMAL(12,2)
DECLARE @TipoRecibo3 AS VARCHAR(20)
DECLARE @FechaRecibo3 AS DATETIME
DECLARE @FechaDeposito3 AS DATETIME
DECLARE @DiasDeposito3 AS INT
DECLARE @Recibo4 AS VARCHAR(20)
DECLARE @MontoRecibo4 AS DECIMAL(12,2)
DECLARE @TipoRecibo4 AS VARCHAR(20)
DECLARE @FechaRecibo4 AS DATETIME
DECLARE @FechaDeposito4 AS DATETIME
DECLARE @DiasDeposito4 AS INT
DECLARE @Recibo5 AS VARCHAR(20)
DECLARE @MontoRecibo5 AS DECIMAL(12,2)
DECLARE @TipoRecibo5 AS VARCHAR(20)
DECLARE @FechaRecibo5 AS DATETIME
DECLARE @FechaDeposito5 AS DATETIME
DECLARE @DiasDeposito5 AS INT
DECLARE @Recibos AS DECIMAL(12,2)
DECLARE @Insertar AS INT
DECLARE @Efectivo AS DECIMAL(12,2)
DECLARE @Cheque AS DECIMAL(12,2)
DECLARE @Tarjeta AS DECIMAL(12,2)
DECLARE @Recibo AS VARCHAR(20)
DECLARE @MontoRecibo AS DECIMAL(12,2)
DECLARE @FechaRecibo AS DATETIME
DECLARE @TipoRecibo AS VARCHAR(20)
DECLARE @Saldo AS DECIMAL(12,2)
DECLARE @Financiera AS INT
DECLARE @Enganche AS DECIMAL(12,2)
DECLARE @NombreFinanciera AS VARCHAR(20)
DECLARE @FechaDeposito AS DATETIME
DECLARE @DiasDeposito AS INT
DECLARE @CuentaBanco AS VARCHAR(50)
DECLARE @TipoDocumento AS VARCHAR(4)
DECLARE @Numero AS BIGINT

--Cursor de facturas
DECLARE #CursorFacturas CURSOR LOCAL FOR
SELECT a.COBRADOR, c.NOMBRE, a.FECHA, a.FACTURA, a.TOTAL_FACTURA, d.NIVEL_PRECIO, a.CLIENTE, b.NOMBRE, d.FINANCIERA, e.ENGANCHE, f.Nombre
FROM prodmult.FACTURA a JOIN prodmult.CLIENTE b ON a.CLIENTE = b.CLIENTE JOIN prodmult.VENDEDOR c ON a.VENDEDOR = c.VENDEDOR
JOIN prodmult.MF_Factura d ON a.FACTURA = d.FACTURA JOIN prodmult.MF_Pedido e ON a.FACTURA = e.FACTURA JOIN prodmult.MF_Financiera f ON d.FINANCIERA = f.Financiera
WHERE a.FECHA >= @FechaValidar AND a.ANULADA = 'N' AND a.COBRADOR <> 'F01' AND a.VENDEDOR NOT IN ( '0107' ) 
--AND a.COBRADOR = 'F25'
ORDER BY a.COBRADOR, c.NOMBRE, a.FECHA

OPEN #CursorFacturas
FETCH NEXT FROM #CursorFacturas INTO @Tienda, @Vendedor, @Fecha, @Factura, @Monto, @FormaPago, @Cliente, @NombreCliente, @Financiera, @Enganche, @NombreFinanciera

WHILE @@FETCH_STATUS = 0
BEGIN

	SET @jj = 0
	SET @Saldo = 0
	SET @Recibos = 0
	SET @Insertar = 1
	
	SET @Recibo1 = ''
	SET @MontoRecibo1 = 0
	SET @TipoRecibo1 = ''
	SET @FechaRecibo1 = '1980-01-01'
	SET @FechaDeposito1 = '1980-01-01'
	SET @DiasDeposito1 = 0
	
	SET @Recibo2 = ''
	SET @MontoRecibo2 = 0
	SET @TipoRecibo2 = ''
	SET @FechaRecibo2 = '1980-01-01'
	SET @FechaDeposito2 = '1980-01-01'
	SET @DiasDeposito2 = 0
	
	SET @Recibo3 = ''
	SET @MontoRecibo3 = 0
	SET @TipoRecibo3 = ''
	SET @FechaRecibo3 = '1980-01-01'
	SET @FechaDeposito3 = '1980-01-01'
	SET @DiasDeposito3 = 0
	
	SET @Recibo4 = ''
	SET @MontoRecibo4 = 0
	SET @TipoRecibo4 = ''
	SET @FechaRecibo4 = '1980-01-01'
	SET @FechaDeposito4 = '1980-01-01'
	SET @DiasDeposito4 = 0
	
	SET @Recibo5 = ''
	SET @MontoRecibo5 = 0
	SET @TipoRecibo5 = ''
	SET @FechaRecibo5 = '1980-01-01'
	SET @FechaDeposito5 = '1980-01-01'
	SET @DiasDeposito5 = 0
	
	--Cursor de recibos
	DECLARE #CursorRecibos CURSOR LOCAL FOR
	SELECT a.DOCUMENTO, a.MONTO, a.EFECTIVO, a.CHEQUE, a.TARJETA, a.FECHA, a.CUENTA_BANCO, a.TIPO_DOCUMENTO, a.NUMERO FROM prodmult.MF_Recibo a WHERE a.FACTURA = @Factura AND a.ANULADO = 'N' ORDER BY a.DOCUMENTO
	
	OPEN #CursorRecibos
	FETCH NEXT FROM #CursorRecibos INTO @Recibo, @MontoRecibo, @Efectivo, @Cheque, @Tarjeta, @FechaRecibo, @CuentaBanco, @TipoDocumento, @Numero
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @jj = @jj + 1
		SET @Recibos = @Recibos + @MontoRecibo
		
		SET @TipoRecibo = 'Efectivo'
		IF (@Cheque > 0) SET @TipoRecibo = 'Cheque'
		IF (@Tarjeta > 0) SET @TipoRecibo = 'Tarjeta'
		
		IF (@jj = 1)
		BEGIN
			SET @Recibo1 = @Recibo
			SET @MontoRecibo1 = @MontoRecibo
			SET @TipoRecibo1 = @TipoRecibo
			SET @FechaRecibo1 = @FechaRecibo
			SET @FechaDeposito1 = (SELECT FECHA FROM prodmult.MF_Deposito WHERE CUENTA_BANCO = @CuentaBanco AND TIPO_DOCUMENTO = @TipoDocumento AND NUMERO = @Numero)
			SET @DiasDeposito1 = DATEDIFF(DAY, @FechaRecibo1, @FechaDeposito1)
		END
		
		IF (@jj = 2)
		BEGIN
			SET @Recibo2 = @Recibo
			SET @MontoRecibo2 = @MontoRecibo
			SET @TipoRecibo2 = @TipoRecibo
			SET @FechaRecibo2 = @FechaRecibo
			SET @FechaDeposito2 = (SELECT FECHA FROM prodmult.MF_Deposito WHERE CUENTA_BANCO = @CuentaBanco AND TIPO_DOCUMENTO = @TipoDocumento AND NUMERO = @Numero)
			SET @DiasDeposito2 = DATEDIFF(DAY, @FechaRecibo2, @FechaDeposito2)
		END
		
		IF (@jj = 3)
		BEGIN
			SET @Recibo3 = @Recibo
			SET @MontoRecibo3 = @MontoRecibo
			SET @TipoRecibo3 = @TipoRecibo
			SET @FechaRecibo3 = @FechaRecibo
			SET @FechaDeposito3 = (SELECT FECHA FROM prodmult.MF_Deposito WHERE CUENTA_BANCO = @CuentaBanco AND TIPO_DOCUMENTO = @TipoDocumento AND NUMERO = @Numero)
			SET @DiasDeposito3 = DATEDIFF(DAY, @FechaRecibo3, @FechaDeposito3)
		END
		
		IF (@jj = 4)
		BEGIN
			SET @Recibo4 = @Recibo
			SET @MontoRecibo4 = @MontoRecibo
			SET @TipoRecibo4 = @TipoRecibo
			SET @FechaRecibo4 = @FechaRecibo
			SET @FechaDeposito4 = (SELECT FECHA FROM prodmult.MF_Deposito WHERE CUENTA_BANCO = @CuentaBanco AND TIPO_DOCUMENTO = @TipoDocumento AND NUMERO = @Numero)
			SET @DiasDeposito4 = DATEDIFF(DAY, @FechaRecibo4, @FechaDeposito4)
		END
		
		IF (@jj = 5)
		BEGIN
			SET @Recibo5 = @Recibo
			SET @MontoRecibo5 = @MontoRecibo
			SET @TipoRecibo5 = @TipoRecibo
			SET @FechaRecibo5 = @FechaRecibo
			SET @FechaDeposito5 = (SELECT FECHA FROM prodmult.MF_Deposito WHERE CUENTA_BANCO = @CuentaBanco AND TIPO_DOCUMENTO = @TipoDocumento AND NUMERO = @Numero)
			SET @DiasDeposito5 = DATEDIFF(DAY, @FechaRecibo5, @FechaDeposito5)
		END
		
		FETCH NEXT FROM #CursorRecibos INTO @Recibo, @MontoRecibo, @Efectivo, @Cheque, @Tarjeta, @FechaRecibo, @CuentaBanco, @TipoDocumento, @Numero
	END
	CLOSE #CursorRecibos
	DEALLOCATE #CursorRecibos

	SET @Saldo = @Monto - @Recibos		
	
	IF (@Financiera = 3 OR @Financiera = 7)
	BEGIN
		IF (@Enganche = 0) SET @Insertar = 0			
		IF (@Fecha <= '2017-04-30') SET @Saldo = 0
	END
	
	--IF (DATEDIFF(day, @Fecha, @FechaRecibo1) < 5) SET @Insertar = 0
	--IF (DATEDIFF(day, @Fecha, @FechaRecibo2) < 5) SET @Insertar = 0
	--IF (DATEDIFF(day, @Fecha, @FechaRecibo3) < 5) SET @Insertar = 0
	--IF (DATEDIFF(day, @Fecha, @FechaRecibo4) < 5) SET @Insertar = 0
	--IF (DATEDIFF(day, @Fecha, @FechaRecibo5) < 5) SET @Insertar = 0
	
	IF (@Saldo > 0 AND @Recibo2 = '')
	BEGIN
		IF ((SELECT COUNT(*) FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND ANULADO = 'N' AND FACTURA = '') > 0)
		BEGIN
			SET @TipoRecibo2 = 'Efectivo'
			SET @Recibo2 = (SELECT TOP 1 DOCUMENTO FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND ANULADO = 'N' AND FACTURA = '')
			SET @MontoRecibo2 = (SELECT TOP 1 MONTO FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND ANULADO = 'N' AND FACTURA = '')
			IF ((SELECT TOP 1 CHEQUE FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND ANULADO = 'N' AND FACTURA = '') > 0) SET @TipoRecibo2 = 'Cheque'
			IF ((SELECT TOP 1 TARJETA FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND ANULADO = 'N' AND FACTURA = '') > 0) SET @TipoRecibo2 = 'Tarjeta'
			SET @FechaRecibo2 = (SELECT TOP 1 FECHA FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND ANULADO = 'N' AND FACTURA = '')
			
			SET @Recibos = @Recibos + @MontoRecibo2
			SET @Saldo = @Monto - @Recibos		
		END
	END

	IF (@Saldo > 0 AND @Recibo3 = '')
	BEGIN
		IF ((SELECT COUNT(*) FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND DOCUMENTO <> @Recibo2 AND ANULADO = 'N' AND FACTURA = '') > 0)
		BEGIN
			SET @TipoRecibo3 = 'Efectivo'
			SET @Recibo3 = (SELECT TOP 1 DOCUMENTO FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND DOCUMENTO <> @Recibo2 AND ANULADO = 'N' AND FACTURA = '')
			SET @MontoRecibo3 = (SELECT TOP 1 MONTO FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND DOCUMENTO <> @Recibo2 AND ANULADO = 'N' AND FACTURA = '')
			IF ((SELECT TOP 1 CHEQUE FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND DOCUMENTO <> @Recibo2 AND ANULADO = 'N' AND FACTURA = '') > 0) SET @TipoRecibo3 = 'Cheque'
			IF ((SELECT TOP 1 TARJETA FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND DOCUMENTO <> @Recibo2 AND ANULADO = 'N' AND FACTURA = '') > 0) SET @TipoRecibo3 = 'Tarjeta'
			SET @FechaRecibo3 = (SELECT TOP 1 FECHA FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND DOCUMENTO <> @Recibo2 AND ANULADO = 'N' AND FACTURA = '')
			
			SET @Recibos = @Recibos + @MontoRecibo3
			SET @Saldo = @Monto - @Recibos		
		END
	END
		
	IF (@Saldo > 0 AND @Recibo4 = '')
	BEGIN
		IF ((SELECT COUNT(*) FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND DOCUMENTO <> @Recibo2 AND DOCUMENTO <> @Recibo3 AND ANULADO = 'N' AND FACTURA = '') > 0)
		BEGIN
			SET @TipoRecibo4 = 'Efectivo'
			SET @Recibo4 = (SELECT TOP 1 DOCUMENTO FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND DOCUMENTO <> @Recibo2 AND DOCUMENTO <> @Recibo3 AND ANULADO = 'N' AND FACTURA = '')
			SET @MontoRecibo4 = (SELECT TOP 1 MONTO FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND DOCUMENTO <> @Recibo2 AND DOCUMENTO <> @Recibo3 AND ANULADO = 'N' AND FACTURA = '')
			IF ((SELECT TOP 1 CHEQUE FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND DOCUMENTO <> @Recibo2 AND DOCUMENTO <> @Recibo3 AND ANULADO = 'N' AND FACTURA = '') > 0) SET @TipoRecibo4 = 'Cheque'
			IF ((SELECT TOP 1 TARJETA FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND DOCUMENTO <> @Recibo2 AND DOCUMENTO <> @Recibo3 AND ANULADO = 'N' AND FACTURA = '') > 0) SET @TipoRecibo4 = 'Tarjeta'
			SET @FechaRecibo4 = (SELECT TOP 1 FECHA FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND DOCUMENTO <> @Recibo2 AND DOCUMENTO <> @Recibo3 AND ANULADO = 'N' AND FACTURA = '')
			
			SET @Recibos = @Recibos + @MontoRecibo4
			SET @Saldo = @Monto - @Recibos		
		END
	END
		
	IF (@Saldo > 0 AND @Recibo5 = '')
	BEGIN
		IF ((SELECT COUNT(*) FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND DOCUMENTO <> @Recibo2 AND DOCUMENTO <> @Recibo3 AND DOCUMENTO <> @Recibo4 AND ANULADO = 'N' AND FACTURA = '') > 0)
		BEGIN
			SET @TipoRecibo5 = 'Efectivo'
			SET @Recibo5 = (SELECT TOP 1 DOCUMENTO FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND DOCUMENTO <> @Recibo2 AND DOCUMENTO <> @Recibo3 AND DOCUMENTO <> @Recibo4 AND ANULADO = 'N' AND FACTURA = '')
			SET @MontoRecibo5 = (SELECT TOP 1 MONTO FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND DOCUMENTO <> @Recibo2 AND DOCUMENTO <> @Recibo3 AND DOCUMENTO <> @Recibo4 AND ANULADO = 'N' AND FACTURA = '')
			IF ((SELECT TOP 1 CHEQUE FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND DOCUMENTO <> @Recibo2 AND DOCUMENTO <> @Recibo3 AND DOCUMENTO <> @Recibo4 AND ANULADO = 'N' AND FACTURA = '') > 0) SET @TipoRecibo5 = 'Cheque'
			IF ((SELECT TOP 1 TARJETA FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND DOCUMENTO <> @Recibo2 AND DOCUMENTO <> @Recibo3 AND DOCUMENTO <> @Recibo4 AND ANULADO = 'N' AND FACTURA = '') > 0) SET @TipoRecibo5 = 'Tarjeta'
			SET @FechaRecibo5 = (SELECT TOP 1 FECHA FROM prodmult.MF_Recibo WHERE CLIENTE = @Cliente AND DOCUMENTO <> @Recibo1 AND DOCUMENTO <> @Recibo2 AND DOCUMENTO <> @Recibo3 AND DOCUMENTO <> @Recibo4 AND ANULADO = 'N' AND FACTURA = '')
			
			SET @Recibos = @Recibos + @MontoRecibo5
			SET @Saldo = @Monto - @Recibos		
		END
	END
	
	IF (@Saldo > 0)
	BEGIN
		IF ((SELECT COUNT(*) FROM prodmult.DOCUMENTOS_CC WHERE CLIENTE = @Cliente AND TIPO = 'O/C' AND ANULADO = 'N') > 0)
		BEGIN
			SET @TipoRecibo5 = ''
			SET @Recibo5 = (SELECT TOP 1 DOCUMENTO FROM prodmult.DOCUMENTOS_CC WHERE CLIENTE = @Cliente AND TIPO = 'O/C' AND ANULADO = 'N')
			SET @MontoRecibo5 = (SELECT TOP 1 MONTO FROM prodmult.DOCUMENTOS_CC WHERE CLIENTE = @Cliente AND TIPO = 'O/C' AND ANULADO = 'N')
			SET @FechaRecibo5 = (SELECT TOP 1 FECHA FROM prodmult.DOCUMENTOS_CC WHERE CLIENTE = @Cliente AND TIPO = 'O/C' AND ANULADO = 'N')
			
			SET @Recibos = @Recibos + @MontoRecibo5
			SET @Saldo = @Monto - @Recibos		
		END
	END
		
	IF (@Insertar = 1)
	BEGIN
		INSERT INTO #facturas VALUES (	@Tienda, @Vendedor, @Fecha, @Factura, @Monto, @Saldo, @Cliente, @NombreCliente, @FormaPago, 
										@Recibo1, @MontoRecibo1, @TipoRecibo1, @FechaRecibo1, @FechaDeposito1, @DiasDeposito1, @Recibo2, @MontoRecibo2, @TipoRecibo2, @FechaRecibo2, @FechaDeposito2, @DiasDeposito2, 
										@Recibo3, @MontoRecibo3, @TipoRecibo3, @FechaRecibo3, @FechaDeposito3, @DiasDeposito3, @Recibo4, @MontoRecibo4, @TipoRecibo4, @FechaRecibo4, @FechaDeposito4, @DiasDeposito4, 
										@Recibo5, @MontoRecibo5, @TipoRecibo5, @FechaRecibo5, @FechaDeposito5, @DiasDeposito5, @NombreFinanciera )
	END
	
	FETCH NEXT FROM #CursorFacturas INTO @Tienda, @Vendedor, @Fecha, @Factura, @Monto, @FormaPago, @Cliente, @NombreCliente, @Financiera, @Enganche, @NombreFinanciera
END
CLOSE #CursorFacturas
DEALLOCATE #CursorFacturas

SELECT * , DATEDIFF(day, Fecha, FechaRecibo1) DiasRecibo1, (CASE WHEN DATEDIFF(day, Fecha, FechaRecibo2) < 1000 THEN 0 ELSE DATEDIFF(day, Fecha, FechaRecibo2) END) AS DiasRecibo2, (CASE WHEN DATEDIFF(day, Fecha, FechaRecibo3) < 1000 THEN 0 ELSE DATEDIFF(day, Fecha, FechaRecibo3) END) AS DiasRecibo3, (CASE WHEN DATEDIFF(day, Fecha, FechaRecibo4) < 1000 THEN 0 ELSE DATEDIFF(day, Fecha, FechaRecibo4) END) AS DiasRecibo4, (CASE WHEN DATEDIFF(day, Fecha, FechaRecibo5) < 1000 THEN 0 ELSE DATEDIFF(day, Fecha, FechaRecibo5) END) AS DiasRecibo5
FROM #facturas
ORDER BY Tienda, Vendedor, Fecha
