CREATE TABLE [prodmult].[MF_Factura_Documento_Soporte](
	[FACTURA] [varchar](50) NOT NULL,
	[TIPO_DOCUMENTO] [varchar](3) NOT NULL,
	[VALOR] [varchar](200) NULL,
    RecordDate              CurrentDateType         NOT NULL DEFAULT(GETDATE()),
	CreatedBy               UsernameType            NOT NULL,
	UpdatedBy               UsernameType            NULL,
	CreateDate              CurrentDateType         NOT NULL DEFAULT(GETDATE())
) ON [PRIMARY];