CREATE TABLE [prodmult].[MF_Factura_Estado](
	[FACTURA] [varchar](50) NOT NULL,
	[ESTADO] [varchar](3) NOT NULL,
	[FECHA] [datetime] NOT NULL,
    RecordDate              CurrentDateType         NOT NULL DEFAULT(GETDATE()),
	CreatedBy               UsernameType            NOT NULL,
	UpdatedBy               UsernameType            NULL,
	CreateDate              CurrentDateType         NOT NULL DEFAULT(GETDATE())
) ON [PRIMARY];