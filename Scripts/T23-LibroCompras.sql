

ALTER PROCEDURE [prodmult].[spLibroCompras] @FechaInicial AS DATETIME, @FechaFinal AS DATETIME, @Empresa AS VARCHAR(20)
AS
BEGIN

--DECLARE @Empresa AS VARCHAR(20) = 'prodmult'
--DECLARE @FechaInicial AS DATETIME = '2017-09-01'
--DECLARE @FechaFinal AS DATETIME = '2017-09-30'

SET @Empresa = LOWER(@Empresa)
DECLARE @Cuenta AS VARCHAR(20) = '1-1-2-004-0001'
DECLARE @FechaInicialString AS VARCHAR(20) = CONVERT(VARCHAR(4), YEAR(@FechaInicial)) + '-' + CONVERT(VARCHAR(2), MONTH(@FechaInicial)) + '-' + CONVERT(VARCHAR(2), DAY(@FechaInicial))
DECLARE @FechaFinalString AS VARCHAR(20) = CONVERT(VARCHAR(4), YEAR(@FechaFinal)) + '-' + CONVERT(VARCHAR(2), MONTH(@FechaFinal)) + '-' + CONVERT(VARCHAR(2), DAY(@FechaFinal))

EXEC (
'DECLARE @Cuenta AS VARCHAR(50) = ''' + @Cuenta + ''' ' +
'DECLARE @Empresa AS VARCHAR(50) = ''' + @Empresa + ''' ' +
'DECLARE @FechaInicial AS DATETIME = ''' + @FechaInicialString + ''' ' + 
'DECLARE @FechaFinal AS DATETIME = ''' + @FechaFinalString + ' 23:59:59 ''' + 
'DECLARE @FechaString AS VARCHAR(10) = CONVERT(VARCHAR(2), (CASE WHEN DAY(@FechaFinal) < 10 THEN ''0'' ELSE '''' END)) + CONVERT(VARCHAR(2), DAY(@FechaFinal)) + ''/'' + CONVERT(VARCHAR(2), (CASE WHEN MONTH(@FechaFinal) < 10 THEN ''0'' ELSE '''' END)) + CONVERT(VARCHAR(2), MONTH(@FechaFinal)) + ''/'' + CONVERT(VARCHAR(4), YEAR(@FechaFinal)) ' +
' ' +
'DECLARE @NitEmpresa AS VARCHAR(20) = (SELECT REPLACE(NIT, ''-'', '''') FROM ERPADMIN.CONJUNTO WHERE CONJUNTO = ''' + @Empresa + ''' ) ' +
'DECLARE @NitEmpresaGuion AS VARCHAR(20) = (SELECT NIT FROM ERPADMIN.CONJUNTO WHERE CONJUNTO = ''' + @Empresa + ''' ) ' +
'DECLARE @NombreEmpresa AS VARCHAR(200) = (SELECT NOMBRE FROM ERPADMIN.CONJUNTO WHERE CONJUNTO = ''' + @Empresa + ''' ) ' +
' ' +
'DECLARE @ii AS INT = 0 ' +
' ' +
'BEGIN TRY ' +
'	CREATE TABLE #dmg (	Fecha DATETIME, Asiento VARCHAR(50), CentroCosto VARCHAR(20), Referencia VARCHAR(800), Fuente VARCHAR(50), TipoAsiento VARCHAR(10), Origen VARCHAR(20), ' +
'						Debitos DECIMAL(12,2), Creditos DECIMAL(12,2), Cuenta VARCHAR(50), Descripcion VARCHAR(200), Libro VARCHAR(10), SaldoInicial DECIMAL(12,2), SaldoFinal DECIMAL(12,2) ) ' +
'END TRY ' +
'BEGIN CATCH ' +
'	SET @ii = 0 ' +
'END CATCH ' +
' ' +
'BEGIN TRY ' +
'	CREATE TABLE #dmgventas (	Fecha DATETIME, Asiento VARCHAR(50), CentroCosto VARCHAR(20), Referencia VARCHAR(800), Fuente VARCHAR(50), TipoAsiento VARCHAR(10), Origen VARCHAR(20), ' +
'								Debitos DECIMAL(12,2), Creditos DECIMAL(12,2), Cuenta VARCHAR(50), Descripcion VARCHAR(200), Libro VARCHAR(10), SaldoInicial DECIMAL(12,2), SaldoFinal DECIMAL(12,2) ) ' +
'END TRY ' +
'BEGIN CATCH ' +
'	SET @ii = 0 ' +
'END CATCH ' +
' ' +
'BEGIN TRY ' +
'	CREATE TABLE #resumen (	Columna1 VARCHAR(100) NULL, Columna2 VARCHAR(100) NULL, Columna3 VARCHAR(100) NULL, Columna4 VARCHAR(100) NULL, Columna5 VARCHAR(100) NULL, ' +
'							Columna6 VARCHAR(100) NULL, Columna7 VARCHAR(100) NULL, Columna8 VARCHAR(100) NULL, Columna9 VARCHAR(100) NULL, Referencia VARCHAR(100) NULL ) ' +
'END TRY ' +
'BEGIN CATCH ' +
'	SET @ii = 0 ' +
'END CATCH ' +
' ' +
'BEGIN TRY ' +
'	CREATE TABLE #ventas (	Tipo VARCHAR(10), SubTotal DECIMAL(12,2), Iva DECIMAL(12,2), Total DECIMAL(12,2), Nit VARCHAR(50), BienServicio VARCHAR(50), Documento VARCHAR(50), Fecha DATETIME ) ' +
'END TRY ' +
'BEGIN CATCH ' +
'	SET @ii = 0 ' +
'END CATCH ' +
' ' +
'DELETE FROM #dmg ' +
'DELETE FROM #dmgventas ' +
'DELETE FROM #ventas ' +
' ' +
'INSERT INTO #dmg ' +
'EXEC prodmult.spBalanceComprobacion @Cuenta, @FechaInicial, @FechaFinal, ''' + @Empresa + ''' ' +
' ' +
'INSERT INTO #dmgventas ' +
'EXEC prodmult.spBalanceComprobacion ''2-1-3-003-0008'', @FechaInicial, @FechaFinal, ''' + @Empresa + ''' ' +
' ' +
'INSERT INTO #ventas ' +
'EXEC prodmult.spGFace @FechaInicial, @FechaFinal, ''' + @Empresa + ''', 1 ' +
' ' +
'BEGIN TRY ' +
'	CREATE TABLE #temp (	Establecimiento INT, ComprasVentas CHAR(1), Documento VARCHAR(29), SerieDocumento VARCHAR(20), NumeroDocumento VARCHAR(20), FechaDocumento DATETIME, ' +
'							NitClienteProveedor VARCHAR(20), NombreClienteProveedor VARCHAR(400), TipoTransaccion CHAR(1), TipoOperacionBienServicio VARCHAR(20), ' +
'							EstadoDocumento VARCHAR(20), NoOrdenCedulaDpiPasaporte VARCHAR(20), NoRegistroCedulaDpiPasaporte VARCHAR(20), TipoDocumentoOperacion VARCHAR(20), ' +
'							NumeroDocumentoOperacion VARCHAR(20), BienesOperacionLocal DECIMAL(12,2), BienesOperacionExterior DECIMAL(12,2), ServiciosOperacionLocal DECIMAL(12,2),  ' +
'							ServiciosOperacionExterior DECIMAL(12,2), BienesOperacionLocalExento DECIMAL(12,2), BienesOperacionExteriorExento DECIMAL(12,2), ' +
'							ServiciosOperacionLocalExento DECIMAL(12,2), ServiciosOperacionExteriorExento DECIMAL(12,2), TipoConstanica VARCHAR(20), NumeroConstancia VARCHAR(20), ' +
'							ValorConstancia VARCHAR(20), BienesOperacionLocalPC DECIMAL(12,2), ServiciosOperacionLocalPC DECIMAL(12,2), BienesOperacionExteriorPC DECIMAL(12,2), ' +
'							ServiciosOperacionExteriorPC DECIMAL(12,2), IVA DECIMAL(12,2), TotalDocumento DECIMAL(12,2), Modulo VARCHAR(2), ValorIVA DECIMAL(12,2), ' +
'							BienServicio VARCHAR(50), Proveedor VARCHAR(20), CajaChica VARCHAR(20), Consecutivo VARCHAR(20), Vale VARCHAR(20) ) ' +
'END TRY ' +
'BEGIN CATCH ' +
'	SET @ii = 0 ' +
'END CATCH ' +
' ' +
'DELETE FROM #temp ' +
' ' +
'DECLARE #Compras CURSOR FOR ' +
'SELECT REPLACE(REPLACE(sub.TIPO, ''/'', ''''), ''A'', '''') AS Documento, ' +
'		prodmult.fcDevuelveSerie(doc.DOC_SOPORTE) AS SerieDocumento, prodmult.fcDevuelveNumero(doc.DOC_SOPORTE) AS NumeroDocumento, doc.FECHA AS FechaDocumento, ' +
'		REPLACE(n.NIT, ''-'', '''') AS NitClienteProveedor, n.RAZON_SOCIAL AS NombreClienteProveedor, ' +
'		doc.SUBTOTAL AS SubTotal, doc.IMPUESTO1 AS IVA, ''0'' AS Proveedor, sub.DESCRIPCION AS BienServicio, ''CH'' AS Modulo, v.CAJA_CHICA AS CajaChica, ' +
'		CONVERT(VARCHAR(20), doc.VALE) AS Consecutivo, v.VALE AS Vale, ''GTQ'' AS Moneda, 1 AS TipoCambio, doc.SUBTIPO AS SubTipo ' +
'FROM	' + @Empresa + '.DOCS_SOPORTE doc INNER JOIN ' + @Empresa + '.NIT n ON doc.NIT = n.NIT ' +
'		INNER JOIN ' + @Empresa + '.SUBTIPO_DOC_CP sub ON doc.TIPO = sub.TIPO AND doc.SUBTIPO = sub.SUBTIPO ' +
'		LEFT OUTER JOIN ' + @Empresa + '.VALE v ON doc.VALE = v.CONSECUTIVO ' +
'		LEFT OUTER JOIN ' + @Empresa + '.ASIENTO_MAYORIZADO m ON v.ASIENTO = m.ASIENTO ' +
'		LEFT OUTER JOIN ' + @Empresa + '.ASIENTO_DE_DIARIO d ON v.ASIENTO = d.ASIENTO ' +
'WHERE	(CASE WHEN m.FECHA IS NULL THEN d.FECHA ELSE m.FECHA END) BETWEEN @FechaInicial AND @FechaFinal ' +
'AND		(doc.TIPO = ''FAC'' OR doc.TIPO = ''N/D'' OR doc.TIPO = ''N/C'' OR doc.TIPO = ''O/D'') ' +
'UNION ' +
'SELECT	(CASE cp.PROVEEDOR WHEN ''11-0015'' THEN ''DA'' ELSE REPLACE(REPLACE(sub.TIPO, ''/'', ''''), ''A'', '''') END) AS Documento, ' +
'		prodmult.fcDevuelveSerie(cp.DOCUMENTO) AS SerieDocumento, prodmult.fcDevuelveNumero(cp.DOCUMENTO) AS NumeroDocumento, cp.FECHA AS FechaDocumento, ' +
'		(CASE cp.PROVEEDOR WHEN ''11-0015'' THEN @NitEmpresa ELSE REPLACE(p.CONTRIBUYENTE, ''-'', '''') END) AS NitClienteProveedor, ' +
'		(CASE cp.PROVEEDOR WHEN ''11-0015'' THEN @NombreEmpresa ELSE p.NOMBRE END) AS NombreClienteProveedor, ' +
'		cp.SUBTOTAL AS SubTotal, cp.IMPUESTO1 AS IVA, cp.PROVEEDOR AS Proveedor, sub.DESCRIPCION AS BienServicio, ''CP'' AS Modulo, '''' AS CajaChica, ' +
'		'''' AS Consecutivo, '''' AS Vale, cp.MONEDA AS Moneda, cp.TIPO_CAMBIO_DOLAR AS TipoCambio, cp.SUBTIPO AS SubTipo ' +
'FROM	' + @Empresa + '.PROVEEDOR p INNER JOIN ' + @Empresa + '.DOCUMENTOS_CP cp ON p.PROVEEDOR = cp.PROVEEDOR ' +
'		INNER JOIN ' + @Empresa + '.SUBTIPO_DOC_CP sub ON sub.TIPO = cp.TIPO AND sub.SUBTIPO = cp.SUBTIPO ' +
'		LEFT OUTER JOIN ' + @Empresa + '.ASIENTO_MAYORIZADO m ON cp.ASIENTO = m.ASIENTO ' +
'		LEFT OUTER JOIN ' + @Empresa + '.ASIENTO_DE_DIARIO d ON cp.ASIENTO = d.ASIENTO ' +
'WHERE  (CASE WHEN m.FECHA IS NULL THEN d.FECHA ELSE m.FECHA END) BETWEEN @FechaInicial AND @FechaFinal ' +
'AND		(cp.TIPO = ''FAC'' OR cp.TIPO = ''N/D'' OR cp.TIPO = ''N/C'' OR cp.TIPO = ''O/D'') ' +
'ORDER BY FechaDocumento, SerieDocumento, NumeroDocumento ' +
' ' +
'DECLARE @Documento AS VARCHAR(20) ' +
'DECLARE @SerieDocumento AS VARCHAR(20) ' +
'DECLARE @NumeroDocumento AS VARCHAR(20) ' +
'DECLARE @FechaDocumento AS DATETIME ' +
'DECLARE @NitClienteProveedor AS VARCHAR(20) ' +
'DECLARE @NombreClienteProveedor AS VARCHAR(400) ' +
'DECLARE @SubTotal AS DECIMAL(12,2) ' +
'DECLARE @IVA AS DECIMAL(12,2) ' +
'DECLARE @Proveedor AS VARCHAR(20) ' +
'DECLARE @BienServicio AS VARCHAR(20) ' +
'DECLARE @Vale AS VARCHAR(20) ' +
'DECLARE @Consecutivo AS VARCHAR(20) ' +
'DECLARE @Moneda AS VARCHAR(20) ' +
' ' +
'DECLARE @Establecimiento AS INT ' +
'DECLARE @ComprasVentas AS CHAR(1) ' +
'DECLARE @LocalImportacion AS CHAR(1) ' +
'DECLARE @TipoOperacionBienServicio AS VARCHAR(20) ' +
'DECLARE @Estado AS VARCHAR(20) ' +
'DECLARE @NoOrdenCedula AS VARCHAR(20) ' +
'DECLARE @NoRegistroCedula AS VARCHAR(20) ' +
'DECLARE @TipoDocumentoOperacion AS VARCHAR(20) ' +
'DECLARE @NumeroDocumentoOperacion AS VARCHAR(20) ' +
'DECLARE @BienesOperacionLocal AS DECIMAL(12,2) ' +
'DECLARE @BienesOperacionExterior AS DECIMAL(12,2) ' +
'DECLARE @ServiciosOperacionLocal AS DECIMAL(12,2) ' +
'DECLARE @ServiciosOperacionExterior AS DECIMAL(12,2) ' +
'DECLARE @BienesOperacionLocalExento AS DECIMAL(12,2) ' +
'DECLARE @BienesOperacionExteriorExento AS DECIMAL(12,2) ' +
'DECLARE @ServiciosOperacionLocalExento AS DECIMAL(12,2) ' +
'DECLARE @ServiciosOperacionExteriorExento AS DECIMAL(12,2) ' +
'DECLARE @TipoConstancia AS VARCHAR(20) ' +
'DECLARE @NumeroConstancia AS VARCHAR(20) ' +
'DECLARE @ValorConstancia AS VARCHAR(20) ' +
'DECLARE @BienesOperacionLocalPC AS DECIMAL(12,2) ' +
'DECLARE @BienesOperacionExteriorPC AS DECIMAL(12,2) ' +
'DECLARE @ServiciosOperacionLocalPC AS DECIMAL(12,2) ' +
'DECLARE @ServiciosOperacionExteriorPC AS DECIMAL(12,2) ' +
'DECLARE @ValorIVA AS DECIMAL(12,2) ' +
'DECLARE @Insertar AS INT ' +
'DECLARE @TotalDocumento AS DECIMAL(12,2) ' +
'DECLARE @PctjIVA AS DECIMAL(12,2) = 1 + ((SELECT IMPUESTO1 FROM prodmult.IMPUESTO WHERE IMPUESTO = ''IVA'') / 100) ' +
'DECLARE @Modulo AS VARCHAR(2) ' +
'DECLARE @CajaChica AS VARCHAR(20) ' +
'DECLARE @Numero AS BIGINT ' +
'DECLARE @TipoCambio AS DECIMAL(12,8) ' +
'DECLARE @SubTipo AS INT ' +
' ' +
'OPEN #Compras ' +
'FETCH NEXT FROM #Compras INTO @Documento, @SerieDocumento, @NumeroDocumento, @FechaDocumento, @NitClienteProveedor, @NombreClienteProveedor, @SubTotal, @IVA, @Proveedor, @BienServicio, @Modulo, @CajaChica, @Consecutivo, @Vale, @Moneda, @TipoCambio, @SubTipo ' +
' ' +
'WHILE @@FETCH_STATUS = 0 ' +
'BEGIN ' +
' ' +
'	SET @Establecimiento = 1 ' +
'	SET @ComprasVentas = ''C'' ' +
'	SET @LocalImportacion = ''L'' ' +
'	SET @Estado = '''' ' +
'	SET @NoOrdenCedula = '''' ' +
'	SET @NoRegistroCedula = '''' ' +
'	SET @TipoOperacionBienServicio = '''' ' +
'	SET @TipoDocumentoOperacion = '''' ' +
'	SET @NumeroDocumentoOperacion = '''' ' +
'	SET @BienesOperacionLocal = 0 ' +
'	SET @BienesOperacionExterior = 0 ' +
'	SET @ServiciosOperacionLocal = 0 ' +
'	SET @ServiciosOperacionExterior = 0 ' +
'	SET @BienesOperacionLocalExento = 0 ' +
'	SET @BienesOperacionExteriorExento = 0 ' +
'	SET @ServiciosOperacionLocalExento = 0 ' +
'	SET @ServiciosOperacionExteriorExento = 0 ' +
'	SET @TipoConstancia = '''' ' +
'	SET @TipoConstancia = '''' ' +
'	SET @NumeroConstancia = '''' ' +
'	SET @ValorConstancia = '''' ' +
'	SET @BienesOperacionLocalPC = 0 ' +
'	SET @BienesOperacionExteriorPC = 0 ' +
'	SET @ServiciosOperacionLocalPC = 0 ' +
'	SET @ServiciosOperacionExteriorPC = 0 ' +
'	SET @ValorIVA = 0 ' +
'	SET @TotalDocumento = @SubTotal + @IVA ' +
'	SET @Insertar = 1 ' +
'	SET @Numero = 0 ' +
' ' +	
'	IF (@Moneda = ''USD'') ' +
'	BEGIN ' +
'		SET @IVA = @IVA * @TipoCambio ' +
'		SET @TotalDocumento = @TotalDocumento * @TipoCambio ' +
'	END ' +
' ' +	
'	IF (@SerieDocumento = ''FSV'') ' + 
'	BEGIN ' +
'		SET @BienServicio = ''Bienes'' ' +
'		SET @BienesOperacionLocal = 0 ' +
'		SET @Documento = ''FA'' ' +
'	END ' +
'	IF (@SerieDocumento = ''FEA2'' OR @SerieDocumento = ''FA2'') SET @SerieDocumento = ''A2'' ' +
' ' +
'	IF (@Documento = ''DA'' AND @SerieDocumento = ''FSV'') SET @Insertar = 0 ' +
' ' +	
'	SET @NoOrdenCedula = '''' ' +
'	SET @NoRegistroCedula = '''' ' +
'	SET @TipoDocumentoOperacion = '''' ' +
'	SET @NumeroDocumentoOperacion = '''' ' +
' ' +
'	IF (@Documento = ''OD'' AND @IVA = 0) SET @Insertar = 0 ' +
' ' +
'	IF (@Proveedor = ''11-0022'' AND @IVA = 0) SET @Insertar = 0 ' +
'	IF (@Proveedor = ''11-0055'' AND @IVA = 0) SET @Insertar = 0 ' +
'	IF (SUBSTRING(@Proveedor, 1, 2) = ''02'' AND @IVA = 0) SET @Insertar = 0 ' +
' ' +
'	IF (@IVA = 0) SET @Documento = ''FPC'' ' +
' ' +
'	IF (@BienServicio LIKE ''%servicio%'') ' +
'	BEGIN ' +
'		IF (@Documento = ''FPC'') ' +
'		BEGIN ' +
'			SET @ServiciosOperacionLocalPC = @TotalDocumento ' +
'		END ' +
'		ELSE ' +
'		BEGIN ' +
'			SET @ServiciosOperacionLocal = @TotalDocumento ' +
'		END ' +
'	END ' +
'	ELSE ' +
'	BEGIN ' +
'		IF (@Documento = ''FPC'') ' +
'		BEGIN ' +
'			SET @BienesOperacionLocalPC = @TotalDocumento ' +
'		END ' +
'		ELSE ' +
'		BEGIN ' +
'			SET @BienesOperacionLocal = @TotalDocumento ' +
'		END ' +
'	END ' +
' ' +	
'	IF (@Documento = ''DA'' AND @SubTipo = 10) ' +
'	BEGIN ' +
'		SET @Documento = ''FO'' ' +
'		SET @BienesOperacionLocal = 0 ' +
'		SET @BienesOperacionExterior = 0 ' +
'		IF (SUBSTRING(@NumeroDocumento, 1, 1) = ''0'') SET @NumeroDocumento = SUBSTRING(@NumeroDocumento, 2, 100) ' +
'		IF (SUBSTRING(@NumeroDocumento, 1, 1) = ''0'') SET @NumeroDocumento = SUBSTRING(@NumeroDocumento, 2, 100) ' +
'		IF (SUBSTRING(@NumeroDocumento, 1, 1) = ''0'') SET @NumeroDocumento = SUBSTRING(@NumeroDocumento, 2, 100) ' +
'		IF (SUBSTRING(@NumeroDocumento, 1, 1) = ''0'') SET @NumeroDocumento = SUBSTRING(@NumeroDocumento, 2, 100) ' +
'		SET @NumeroDocumento = @SerieDocumento + @NumeroDocumento ' +
'		SET @SerieDocumento = '''' ' + 
'	END ' +
' ' +	
'	IF (@Documento = ''DA'' OR @SerieDocumento = ''FSV'') ' +
'	BEGIN ' +
'		SET @BienServicio = ''Bienes'' ' +
'		SET @BienesOperacionLocal = 0 ' +
'		SET @BienesOperacionExterior = (@IVA * @PctjIVA) / (@PctjIVA - 1) ' +
'		SET @TotalDocumento = @BienesOperacionExterior ' +
'	END ' +
' ' +	
'	IF (@SerieDocumento = ''A2'') ' +
'	BEGIN ' +
'		BEGIN TRY ' +
'			SET @Numero = CONVERT(BIGINT, @NumeroDocumento) ' +
'		END TRY ' +
'		BEGIN CATCH ' +
'			SET @Numero = 0 ' +
'		END CATCH ' +
' ' +		
'		IF (@Numero >= 2000 AND @Numero <= 10000) ' +
'		BEGIN ' +
'			SET @Documento = ''FE'' ' +
'			SET @BienesOperacionLocal = 0 ' +
'			SET @BienServicio = ''Servicio'' ' +
'			SET @ServiciosOperacionLocal = @TotalDocumento ' +
' ' +
'			IF (LEN(@NitClienteProveedor) > 10) ' +
'			BEGIN ' +
'				SET @NoOrdenCedula = ''DPI'' ' +
'				SET @NoRegistroCedula = @NitClienteProveedor ' +
'				SET @NitClienteProveedor = '''' ' +
'			END ' +
'		END ' +
'	END ' +
' ' +	
'	IF (@Modulo = ''CH'') ' +
'	BEGIN ' +
'		IF ((SELECT COUNT(*) FROM #dmg WHERE Creditos = @IVA AND @IVA > 1000) > 0) SET @Insertar = 0 ' +
'	END ' +
' ' +	
'	IF (@Documento = ''OD'') SET @Documento = ''FC'' ' +
'	IF (@Documento = ''FPC'' AND @TotalDocumento > 25000) SET @Insertar = 0 ' +
'	IF (@Documento = ''FE'' AND LEN(@NitClienteProveedor) > 0 AND @SerieDocumento <> ''A2'') SET @Documento = ''FC'' ' +
'	IF (@Documento = ''FA'' OR @Documento = ''DA'') SET @LocalImportacion = ''I'' ' +
' ' +
'	IF (@Insertar = 1) ' +
'	BEGIN ' +
'		INSERT INTO #temp VALUES (	@Establecimiento, @ComprasVentas, @Documento, @SerieDocumento, @NumeroDocumento, @FechaDocumento, @NitClienteProveedor, @NombreClienteProveedor, ' +
'									@LocalImportacion, @TipoOperacionBienServicio, @Estado, @NoOrdenCedula, @NoRegistroCedula, @TipoDocumentoOperacion, @NumeroDocumentoOperacion, ' +
'									@BienesOperacionLocal, @BienesOperacionExterior, @ServiciosOperacionLocal, @ServiciosOperacionExterior, @BienesOperacionLocalExento, ' +
'									@BienesOperacionExteriorExento, @ServiciosOperacionLocalExento, @ServiciosOperacionExteriorExento, @TipoConstancia, @NumeroConstancia, @ValorConstancia, ' +
'									@BienesOperacionLocalPC, @ServiciosOperacionLocalPC, @BienesOperacionExteriorPC, @ServiciosOperacionExteriorPC, @ValorIVA, @TotalDocumento, @Modulo, ' +
'									@IVA, @BienServicio, @Proveedor, @CajaChica, @Consecutivo, @Vale ) ' +
'	END ' +
' ' +
'	FETCH NEXT FROM #Compras INTO @Documento, @SerieDocumento, @NumeroDocumento, @FechaDocumento, @NitClienteProveedor, @NombreClienteProveedor, @SubTotal, @IVA, @Proveedor, @BienServicio, @Modulo, @CajaChica, @Consecutivo, @Vale, @Moneda, @TipoCambio, @SubTipo ' +
'END ' +
'CLOSE #Compras ' +
'DEALLOCATE #Compras ' +
' ' +
'SELECT	Establecimiento, ComprasVentas AS [Compras/Ventas], Documento, SerieDocumento AS [Serie del documento], NumeroDocumento AS [N�mero del documento], ' +
'		FechaDocumento AS [Fecha del documento], NitClienteProveedor AS [NIT del cliente/proveedor], NombreClienteProveedor AS [Nombre del cliente/proveedor], ' + 
'		TipoTransaccion AS [Tipo de transacci�n], TipoOperacionBienServicio AS [Tipo de Operaci�n (Bien o Servicio)], EstadoDocumento AS [Estado del documento], ' +
'		NoOrdenCedulaDpiPasaporte AS [No. de orden de la c�dula, DPI o Pasaporte], NoRegistroCedulaDpiPasaporte AS [No. de registro de la c�dula, DPI o Pasaporte], ' +
'		TipoDocumentoOperacion AS [Tipo Documento de Operaci�n], NumeroDocumentoOperacion AS [N�mero del documento de Operaci�n], ' +
'		(CASE Documento WHEN ''FPC'' THEN '''' ELSE CONVERT(VARCHAR(40), BienesOperacionLocal) END) AS [Total Valor Gravado del documento, Bienes operaci�n Local], ' +
'		(CASE Documento WHEN ''FPC'' THEN '''' ELSE CONVERT(VARCHAR(40), BienesOperacionExterior) END) AS [Total Valor Gravado del documento, Bienes operaci�n del Exterior], ' +
'		(CASE Documento WHEN ''FPC'' THEN '''' ELSE CONVERT(VARCHAR(40), ServiciosOperacionLocal) END) AS [Total Valor Gravado del documento Servicios operaci�n Local], ' +
'		(CASE Documento WHEN ''FPC'' THEN '''' ELSE CONVERT(VARCHAR(40), ServiciosOperacionExterior) END) AS [Total Valor Gravado del documento Servicios operaci�n del uso Exterior], ' +
'		(CASE Documento WHEN ''FPC'' THEN '''' ELSE CONVERT(VARCHAR(40), BienesOperacionLocalExento) END) AS [Total Valor Exento del documento, Bienes operaci�n Local], ' +
'		(CASE Documento WHEN ''FPC'' THEN '''' ELSE CONVERT(VARCHAR(40), BienesOperacionExteriorExento) END) AS [Total Valor Exento del documento, Bienes operaci�n del Exterior], ' +
'		(CASE Documento WHEN ''FPC'' THEN '''' ELSE CONVERT(VARCHAR(40), ServiciosOperacionLocalExento) END) AS [Total Valor Exento del documento, Servicios operaci�n Local], ' +
'		(CASE Documento WHEN ''FPC'' THEN '''' ELSE CONVERT(VARCHAR(40), ServiciosOperacionExteriorExento) END) AS [Total Valor Exento del documento, Servicios operaci�n del Exterior], ' +
'		TipoConstanica AS [Tipo de Constancia], NumeroConstancia AS [N�mero de la constancia de exenci�n/adquisici�n de insumos/reten-ci�n del IVA], ' +
'		ValorConstancia AS [Valor de la constancia de exenci�n/adquisici�n de insumos/reten-ci�n del IVA], ' +
'		(CASE Documento WHEN ''FPC'' THEN CONVERT(VARCHAR(40), BienesOperacionLocalPC) ELSE '''' END) AS [Peque�o Contribuyente Total Facturado Operaci�n Local Bienes], ' +
'		(CASE Documento WHEN ''FPC'' THEN CONVERT(VARCHAR(40), ServiciosOperacionLocalPC) ELSE '''' END) AS [Peque�o Contribuyente Total Facturado Operaci�n Local Servicios], ' +
'		(CASE Documento WHEN ''FPC'' THEN (CASE WHEN BienesOperacionExteriorPC > 0 THEN CONVERT(VARCHAR(40), BienesOperacionExteriorPC) ELSE '''' END) ELSE '''' END) AS [Peque�o Contribuyente Total Facturado Operaci�n al Exterior Bienes], ' +
'		(CASE Documento WHEN ''FPC'' THEN (CASE WHEN ServiciosOperacionExteriorPC > 0 THEN CONVERT(VARCHAR(40), ServiciosOperacionExteriorPC) ELSE '''' END) ELSE '''' END) AS [Peque�o Contribuyente Total Facturado Operaci�n al Exterior Servicios], ' +
'		IVA, TotalDocumento AS [Total Valor del Documento] ' +
'FROM	#temp ' +
'ORDER BY [Fecha del documento], [Nombre del cliente/proveedor], Documento, [Serie del documento], [N�mero del documento] ' +
' ' +
'SELECT	Documento, SerieDocumento AS Serie, NumeroDocumento AS Numero, ' +
'		FechaDocumento AS Fecha, NitClienteProveedor AS NIT, NombreClienteProveedor AS Nombre, ' + 
'		(CASE WHEN BienesOperacionLocal > 0 THEN ''BienesLocal'' ELSE (CASE WHEN BienesOperacionExterior > 0 THEN ''BienesExterior'' ELSE (CASE WHEN ServiciosOperacionLocal > 0 THEN ''ServiciosLocal'' ELSE (CASE WHEN ServiciosOperacionExterior > 0 THEN ''ServiciosExterior'' ELSE (CASE WHEN BienesOperacionLocalExento > 0 THEN ''BienesLocalExento'' ELSE (CASE WHEN BienesOperacionExteriorExento > 0 THEN ''BienesExteriorExento'' ELSE (CASE WHEN ServiciosOperacionLocalExento > 0 THEN ''ServiciosLocalExento'' ELSE (CASE WHEN ServiciosOperacionExteriorExento > 0 THEN ''ServiciosExteriorExento'' ELSE (CASE WHEN BienesOperacionLocalPC > 0 THEN ''BienesLocalPC'' ELSE (CASE WHEN ServiciosOperacionLocalPC > 0 THEN ''ServiciosLocalPC'' ELSE ''BienServicioExteriorPC'' END) END) END) END) END) END) END) END) END) END) AS Columna, ' +
'		ValorIVA AS IVA, TotalDocumento, Modulo, BienServicio, (CASE Proveedor WHEN ''0'' THEN '''' ELSE Proveedor END) AS Proveedor, CajaChica, Vale, Consecutivo ' +
'FROM	#temp ' +
'ORDER BY Fecha, Proveedor, Documento, Serie, numero ' +
' ' +
'SELECT * FROM #dmg ' +
'SELECT * FROM #dmgventas ' +
' ' +
'DECLARE @Cuadre AS DECIMAL(12,2) = 0 ' +
'DECLARE @TextoCuadre AS VARCHAR(30) = ''CUADRE OK'' ' +
'DECLARE @TotalDebitosDMG AS DECIMAL(12,2) = (SELECT COALESCE(SUM(Debitos), 0) FROM #dmg WHERE TipoAsiento IN ( ''CHI'', ''COM'', ''CXP'', ''NCR'', ''SER'', ''EGR'' ) ) ' +
'DECLARE @TotalCreditosDMG AS DECIMAL(12,2) = (SELECT COALESCE(SUM(Creditos), 0) FROM #dmg WHERE TipoAsiento IN ( ''CHI'', ''COM'', ''CXP'', ''NCR'', ''SER'', ''EGR'' ) ) ' +
'DECLARE @TotalDMG AS DECIMAL(12,2) = (SELECT COALESCE(SUM(Debitos), 0) FROM #dmg WHERE TipoAsiento IN ( ''CHI'', ''COM'', ''CXP'', ''NCR'', ''SER'', ''EGR'' ) ) - (SELECT COALESCE(SUM(Creditos), 0) FROM #dmg WHERE TipoAsiento IN ( ''CHI'', ''COM'', ''CXP'', ''NCR'', ''SER'', ''EGR'' ) ) ' +
' ' +
'DECLARE @DAIVA AS DECIMAL(12,2) = (SELECT COALESCE(SUM(ValorIVA), 0) FROM #temp WHERE Documento = ''DA'' ) ' +
'DECLARE @DA AS DECIMAL(12,2) = (SELECT COALESCE(SUM(TotalDocumento), 0) - COALESCE(SUM(ValorIVA), 0) FROM #temp WHERE BienServicio LIKE ''%Bienes%'' AND Documento = ''DA'' ) ' +
'DECLARE @DAServicios AS DECIMAL(12,2) = (SELECT COALESCE(SUM(TotalDocumento), 0) - COALESCE(SUM(ValorIVA), 0) FROM #temp WHERE BienServicio NOT LIKE ''%Bienes%'' AND Documento = ''DA'' ) ' +
' ' +
'DECLARE @FAIVA AS DECIMAL(12,2) = (SELECT COALESCE(SUM(ValorIVA), 0) FROM #temp WHERE Documento = ''FA'' ) ' +
'DECLARE @FA AS DECIMAL(12,2) = (SELECT COALESCE(SUM(TotalDocumento), 0) - COALESCE(SUM(ValorIVA), 0) FROM #temp WHERE BienServicio LIKE ''%Bienes%'' AND Documento = ''FA'' ) ' +
'DECLARE @FAServicios AS DECIMAL(12,2) = (SELECT COALESCE(SUM(TotalDocumento), 0) - COALESCE(SUM(ValorIVA), 0) FROM #temp WHERE BienServicio NOT LIKE ''%Bienes%'' AND Documento = ''FA'' ) ' +
' ' +
'DECLARE @FCIVA AS DECIMAL(12,2) = (SELECT COALESCE(SUM(ValorIVA), 0) FROM #temp WHERE Documento = ''FC'' ) ' +
'DECLARE @FCIVAServicios AS DECIMAL(12,2) = (SELECT COALESCE(SUM(ValorIVA), 0) FROM #temp WHERE BienServicio NOT LIKE ''%Bienes%'' AND Documento = ''FC'' ) ' +
'DECLARE @FC AS DECIMAL(12,2) = (SELECT COALESCE(SUM(TotalDocumento), 0) - COALESCE(SUM(ValorIVA), 0) FROM #temp WHERE BienServicio LIKE ''%Bienes%'' AND Documento = ''FC'' ) ' +
'DECLARE @FCServicios AS DECIMAL(12,2) = (SELECT COALESCE(SUM(TotalDocumento), 0) - COALESCE(SUM(ValorIVA), 0) FROM #temp WHERE BienServicio NOT LIKE ''%Bienes%'' AND Documento = ''FC'' ) ' +
' ' +
'DECLARE @FEIVA AS DECIMAL(12,2) = (SELECT COALESCE(SUM(ValorIVA), 0) FROM #temp WHERE Documento = ''FE'' ) ' +
'DECLARE @FE AS DECIMAL(12,2) = (SELECT COALESCE(SUM(TotalDocumento), 0) - COALESCE(SUM(ValorIVA), 0) FROM #temp WHERE BienServicio LIKE ''%Bienes%'' AND Documento = ''FE'' ) ' +
'DECLARE @FEServicios AS DECIMAL(12,2) = (SELECT COALESCE(SUM(TotalDocumento), 0) - COALESCE(SUM(ValorIVA), 0) FROM #temp WHERE BienServicio NOT LIKE ''%Bienes%'' AND Documento = ''FE'' ) ' +
' ' +
'DECLARE @NCIVA AS DECIMAL(12,2) = (SELECT COALESCE(SUM(ValorIVA), 0) * -1 FROM #temp WHERE Documento = ''NC'' ) ' +
'DECLARE @NCIVABienes AS DECIMAL(12,2) = (SELECT COALESCE(SUM(ValorIVA), 0) * -1 FROM #temp WHERE BienServicio LIKE ''%Bienes%'' AND Documento = ''NC'' ) ' +
'DECLARE @NCIVAServicios AS DECIMAL(12,2) = (SELECT COALESCE(SUM(ValorIVA), 0) * -1 FROM #temp WHERE BienServicio NOT LIKE ''%Bienes%'' AND Documento = ''NC'' ) ' +
'DECLARE @NC AS DECIMAL(12,2) = (SELECT (COALESCE(SUM(TotalDocumento), 0) - COALESCE(SUM(ValorIVA), 0)) * -1 FROM #temp WHERE BienServicio LIKE ''%Bienes%'' AND Documento = ''NCR'' ) ' +
'DECLARE @NCServicios AS DECIMAL(12,2) = (SELECT (COALESCE(SUM(TotalDocumento), 0) - COALESCE(SUM(ValorIVA), 0)) * -1 FROM #temp WHERE BienServicio NOT LIKE ''%Bienes%'' AND Documento = ''NC'' ) ' +
' ' +
'DECLARE @FO AS DECIMAL(12,2) = (SELECT COALESCE(SUM(TotalDocumento), 0) FROM #temp WHERE Documento = ''FO'' ) ' +
' ' +
'DECLARE @FPC AS DECIMAL(12,2) = (SELECT COALESCE(SUM(TotalDocumento), 0) FROM #temp WHERE Documento = ''FPC'' ) ' +
'DECLARE @TotalLC AS DECIMAL(12,2) = @DAIVA + @FAIVA + @FCIVA + @FEIVA + @NCIVA' +
' ' +
'DECLARE @TotalBienes AS DECIMAL(12,2) = (SELECT COALESCE(SUM(Debitos), 0) FROM #dmg WHERE TipoAsiento IN ( ''CHI'', ''COM'', ''CXP'', ''NCR'', ''SER'' ) ) ' +
'DECLARE @TotalServicios AS DECIMAL(12,2) =  (SELECT COALESCE(SUM(Creditos), 0) FROM #dmg WHERE TipoAsiento IN ( ''CHI'', ''COM'', ''CXP'', ''NCR'', ''SER'' ) ) ' +
' ' +
'IF (@TotalDMG > @TotalLC) ' +
'BEGIN ' +
'	SET @Cuadre = @TotalDMG - @TotalLC ' +
'	SET @TextoCuadre = ''MAS EN CONTABILIDAD'' ' +
'END ' +
' ' +
'IF (@TotalLC > @TotalDMG) ' +
'BEGIN ' +
'	SET @Cuadre = @TotalLC - @TotalDMG ' +
'	SET @TextoCuadre = ''COMPRAS CON MAS SALDO'' ' +
'END ' +
' ' +
'DELETE FROM #resumen ' +
'UPDATE #ventas SET BienServicio = ''BIEN'' WHERE Tipo = ''NC'' AND NIT <> ''EXPORT'' ' +
' ' +
'DECLARE @BienesLocal AS DECIMAL(12,2) = (SELECT COALESCE(SUM(SubTotal), 0) FROM #ventas WHERE Tipo = ''FC'' AND Nit <> ''EXPORT'' AND BienServicio = ''BIEN'') ' +
'DECLARE @BienesLocalNC AS DECIMAL(12,2) = (SELECT COALESCE(SUM(SubTotal), 0) FROM #ventas WHERE Tipo = ''NC'' AND Nit <> ''EXPORT'' AND BienServicio = ''BIEN'') ' +
'DECLARE @ServiciosLocal AS DECIMAL(12,2) = (SELECT COALESCE(SUM(SubTotal), 0) FROM #ventas WHERE Tipo = ''FC'' AND Nit <> ''EXPORT'' AND BienServicio = ''SERVICIO'') ' +
'DECLARE @ServiciosLocalNC AS DECIMAL(12,2) = (SELECT COALESCE(SUM(SubTotal), 0) FROM #ventas WHERE Tipo = ''NC'' AND Nit <> ''EXPORT'' AND BienServicio = ''SERVICIO'') ' +
'DECLARE @ServiciosExteriorExento AS DECIMAL(12,2) = (SELECT COALESCE(SUM(Total), 0) FROM #ventas WHERE Tipo = ''FC'' AND Nit = ''EXPORT'' AND BienServicio = ''SERVICIO'') ' +
'DECLARE @VentasGrabadasBase AS DECIMAL(12,2) = @BienesLocal + @BienesLocalNC ' +
'DECLARE @VentasGrabadasDebitos AS DECIMAL(12,2) = @VentasGrabadasBase * (@PctjIVA - 1) ' +
'DECLARE @ServiciosGrabadosBase AS DECIMAL(12,2) = @ServiciosLocal + @ServiciosLocalNC ' +
'DECLARE @ServiciosGrabadosDebitos AS DECIMAL(12,2) = @ServiciosGrabadosBase * (@PctjIVA - 1) ' +
'DECLARE @TotalVentasBase AS DECIMAL(12,2) = @VentasGrabadasBase + @ServiciosGrabadosBase ' +
'DECLARE @TotalVentasDebitos AS DECIMAL(12,2) = @VentasGrabadasDebitos + @ServiciosGrabadosDebitos ' +
'DECLARE @Exenciones AS DECIMAL(12,2) = (SELECT COALESCE(SUM(Debitos), 0) FROM #dmgventas WHERE Referencia LIKE ''%exenci%'') ' +
' ' +
'DECLARE @OtrasCompras AS DECIMAL(12,2) = @FC + @NC ' +
'DECLARE @OtrasComprasIVA AS DECIMAL(12,2) = (@FCIVA - @FCIVAServicios) + @NCIVABienes ' +
'DECLARE @ServiciosAdquiridos AS DECIMAL(12,2) = @FCServicios + @FEServicios + @NCServicios ' +
'DECLARE @ServiciosAdquiridosIVA AS DECIMAL(12,2) = @FCIVAServicios + @FEIVA + @NCIVAServicios ' +
'DECLARE @SumatoriaBase AS DECIMAL(12,2) = @FC + @NC + @ServiciosAdquiridos + @FA + @DA ' +
'DECLARE @SumatoriaCreditos AS DECIMAL(12,2) = (@FCIVA - @FCIVAServicios) + @ServiciosAdquiridosIVA + @FAIVA + @DAIVA + @Exenciones + @NCIVABienes + @FO ' +
'DECLARE @ImpuestoPreliminar AS DECIMAL(12,2) = @TotalVentasDebitos - @SumatoriaCreditos ' +
'DECLARE @Retenciones AS DECIMAL(12,2) = (SELECT COALESCE(SUM(VALOR), 0) FROM prodmult.MF_Exencion WHERE TIPO = ''CRIVA'' AND FECHA BETWEEN @FechaInicial AND @FechaFinal) ' +
'DECLARE @ImpuestoPagar AS DECIMAL(12,2) = @ImpuestoPreliminar - @Retenciones ' +
' ' +
'IF (@Empresa <> ''prodmult'') SET @Retenciones = 0 ' +
' ' +
'INSERT INTO #resumen ( Columna1, Referencia ) VALUES ( ''N_DMG 1-1-2-004-0001'', ''Black'' ) ' +
'INSERT INTO #resumen ( Columna1, Referencia, Columna7, Columna8 ) VALUES ( ''N_IVA CREDITO FISCAL'', ''Black'', ''N_'' + @TextoCuadre, ''N_'' + CONVERT(VARCHAR(40), @Cuadre) ) ' +
'INSERT INTO #resumen ( Columna1, Referencia ) VALUES ( '' '', '' '' ) ' +
'INSERT INTO #resumen ( Columna1, columna2, Columna3, Columna4, Referencia, Columna7 ) VALUES ( ''N_Documento'', ''N_D�bitos'', ''N_Cr�ditos'', ''N_IVA FINAL'', ''Black'', ''N_'' + @NitEmpresaGuion ) ' +
'INSERT INTO #resumen ( Columna1, columna2, Columna3, Columna4, Referencia, Columna7 ) VALUES ( ''CHI'', (SELECT COALESCE(SUM(Debitos), 0) FROM #dmg WHERE TipoAsiento = ''CHI''), (SELECT COALESCE(SUM(Creditos), 0) FROM #dmg WHERE TipoAsiento = ''CHI''), (SELECT COALESCE(SUM(Debitos), 0) FROM #dmg WHERE TipoAsiento = ''CHI'') - (SELECT COALESCE(SUM(Creditos), 0) FROM #dmg WHERE TipoAsiento = ''CHI''), '''', ''N_'' + @NombreEmpresa ) ' +
'INSERT INTO #resumen ( Columna1, columna2, Columna3, Columna4, Referencia, Columna7 ) VALUES ( ''COM'', (SELECT COALESCE(SUM(Debitos), 0) FROM #dmg WHERE TipoAsiento = ''COM''), (SELECT COALESCE(SUM(Creditos), 0) FROM #dmg WHERE TipoAsiento = ''COM''), (SELECT COALESCE(SUM(Debitos), 0) FROM #dmg WHERE TipoAsiento = ''COM'') - (SELECT COALESCE(SUM(Creditos), 0) FROM #dmg WHERE TipoAsiento = ''COM''), '''', ''N_'' + @FechaString ) ' +
'INSERT INTO #resumen ( Columna1, columna2, Columna3, Columna4, Referencia, Columna7 ) VALUES ( ''CXP'', (SELECT COALESCE(SUM(Debitos), 0) FROM #dmg WHERE TipoAsiento = ''CXP''), (SELECT COALESCE(SUM(Creditos), 0) FROM #dmg WHERE TipoAsiento = ''CXP''), (SELECT COALESCE(SUM(Debitos), 0) FROM #dmg WHERE TipoAsiento = ''CXP'') - (SELECT COALESCE(SUM(Creditos), 0) FROM #dmg WHERE TipoAsiento = ''CXP''), '''', '''' ) ' +
'INSERT INTO #resumen ( Columna1, columna2, Columna3, Columna4, Referencia, Columna7, Columna8, Columna9 ) VALUES ( ''NCR'', (SELECT COALESCE(SUM(Debitos), 0) FROM #dmg WHERE TipoAsiento IN ( ''NCR'', ''EGR'' )), (SELECT COALESCE(SUM(Creditos), 0) FROM #dmg WHERE TipoAsiento IN ( ''NCR'', ''EGR'' )), (SELECT COALESCE(SUM(Debitos), 0) FROM #dmg WHERE TipoAsiento IN ( ''NCR'', ''EGR'' )) - (SELECT COALESCE(SUM(Creditos), 0) FROM #dmg WHERE TipoAsiento IN ( ''NCR'', ''EGR'' )), '''', ''N_D�bito Fiscal por Operaciones Locales'', ''N_BASE'', ''N_D�BITOS'' ) ' +
'INSERT INTO #resumen ( Columna1, columna2, Columna3, Columna4, Referencia, Columna7, Columna8, Columna9 ) VALUES ( ''SER'', (SELECT COALESCE(SUM(Debitos), 0) FROM #dmg WHERE TipoAsiento = ''SER''), (SELECT COALESCE(SUM(Creditos), 0) FROM #dmg WHERE TipoAsiento = ''SER''), (SELECT COALESCE(SUM(Debitos), 0) FROM #dmg WHERE TipoAsiento = ''SER'') - (SELECT COALESCE(SUM(Creditos), 0) FROM #dmg WHERE TipoAsiento = ''SER''), '''', ''VENTAS GRABADAS'', @VentasGrabadasBase, @VentasGrabadasDebitos ) ' +
'INSERT INTO #resumen ( Columna1, columna2, Columna3, Columna4, Referencia, Columna7, Columna8, Columna9 ) VALUES ( ''N_Total general:'', ''N_'' + CONVERT(VARCHAR(40), @TotalDebitosDMG),'' N_ '' + CONVERT(VARCHAR(40), @TotalCreditosDMG), ''N_'' + CONVERT(VARCHAR(40), @TotalDMG), ''Black'', ''SERVICIOS GRABADOS'', @ServiciosGrabadosBase, @ServiciosGrabadosDebitos ) ' +
'INSERT INTO #resumen ( Columna1, Referencia ) VALUES ( '' '', '' '' ) ' +
'INSERT INTO #resumen ( Columna1, Referencia, Columna7 ) VALUES ( '' '', '' '', ''N_Exportaciones'' ) ' +
'INSERT INTO #resumen ( Columna1, Referencia, Columna7, Columna8 ) VALUES ( '' '', '' '', ''VENTAS EXPORTACIONES'', @ServiciosExteriorExento ) ' +
' ' +
'INSERT INTO #resumen ( Columna1, Referencia, Columna8, Columna9 ) VALUES ( ''N_LIBRO DE COMPRAS'', ''Black'', ''N_'' + CONVERT(VARCHAR(40), @TotalVentasBase), ''N_'' + CONVERT(VARCHAR(40), @TotalVentasDebitos) ) ' +
'INSERT INTO #resumen ( Columna1, Referencia ) VALUES ( '' '', '' '' ) ' +
'INSERT INTO #resumen ( Columna1, columna2, Columna3, Columna4, Referencia, Columna7 ) VALUES ( ''N_Documento'', ''N_Compras'', ''N_Servicios'', ''N_IVA'', ''Black'', ''N_CREDITO FISCAL POR OPERACIONES LOCALES'' ) ' +
'INSERT INTO #resumen ( Columna1, columna2, Columna3, Columna4, Referencia, Columna7, Columna8 ) VALUES ( ''DA'', @DA, @DAServicios, @DAIVA, '''', ''Compras y Servicios adquiridos de Peque�os Contribuyentes'', @FPC ) ' +
'INSERT INTO #resumen ( Columna1, columna2, Columna3, Columna4, Referencia ) VALUES ( ''FA'', @FA, @FAServicios, @FAIVA, '''' ) ' +
'INSERT INTO #resumen ( Columna1, columna2, Columna3, Columna4, Referencia, Columna7, Columna9 ) VALUES ( ''FC'', @FC, @FCServicios, @FCIVA, '''', ''Compras de veh�culos terrestres del modelo de dos a�os o m�s anteriores al del a�o en curso'', @FO ) ' +
'INSERT INTO #resumen ( Columna1, columna2, Columna3, Columna4, Referencia, Columna7, Columna8, Columna9 ) VALUES ( ''FE'', @FE, @FEServicios, @FEIVA, '''', ''Otras compras'', @OtrasCompras, @OtrasComprasIVA ) ' +
'INSERT INTO #resumen ( Columna1, columna2, Columna3, Columna4, Referencia, Columna7, Columna8, Columna9 ) VALUES ( ''NC'', @NC, @NCServicios, @NCIVA, '''', ''Servicios adquiridos'', @ServiciosAdquiridos, @ServiciosAdquiridosIVA ) ' +
'INSERT INTO #resumen ( Columna1, columna2, Columna3, Columna4, Referencia, Columna7, Columna8, Columna9 ) VALUES ( ''N_Total general:'', ''N_'' + CONVERT(VARCHAR(40), @DA + @FA + @FC + @FE + @NC), ''N_'' + CONVERT(VARCHAR(40), @DAServicios + @FAServicios + @FCServicios + @FEServicios + @NCServicios), ''N_'' + CONVERT(VARCHAR(40), @TotalLC), ''Black'', ''Importaciones de Centro Am�rica'', @FA, @FAIVA ) ' +
'INSERT INTO #resumen ( Columna7, Columna8, Columna9 ) VALUES ( ''Importaciones del Resto del Mundo'', @DA, @DAIVA ) ' +
'INSERT INTO #resumen ( Columna7 ) VALUES ( ''Compra de Veh�culos'' ) ' +
'INSERT INTO #resumen ( Columna7 ) VALUES ( ''Remanente cr�dito fiscal per�odo anterior'' ) ' +
'INSERT INTO #resumen ( Columna7, Columna9 ) VALUES ( ''IVA conforme a constancias de exenci�n recibidas'', @Exenciones ) ' +
'INSERT INTO #resumen ( Columna7, Columna8, Columna9 ) VALUES ( ''N_Sumatoria de columnas BASE Y CR�DITOS'', ''N_'' + CONVERT(VARCHAR(40), @SumatoriaBase), ''N_'' + CONVERT(VARCHAR(40), @SumatoriaCreditos) ) ' +
'INSERT INTO #resumen ( Columna1, Referencia ) VALUES ( '' '', '' '' ) ' +
'INSERT INTO #resumen ( Columna7, Columna9 ) VALUES ( ''N_DETERMINACION DEL CREDITO FISCAL O IMPUESTO A PAGAR'', ''N_'' + CONVERT(VARCHAR(40), @ImpuestoPreliminar) ) ' +
'INSERT INTO #resumen ( Columna1, Referencia ) VALUES ( '' '', '' '' ) ' +
'INSERT INTO #resumen ( Columna1, Referencia ) VALUES ( '' '', '' '' ) ' +
'INSERT INTO #resumen ( Columna7, Columna8 ) VALUES ( ''Remanente de Retenciones del IVA del Per�odo Anterior'', 0.00 ) ' +
'INSERT INTO #resumen ( Columna7, Columna8 ) VALUES ( ''Constancias de Retenciones del IVA del Per�odo a Declarar'', @Retenciones ) ' +
'INSERT INTO #resumen ( Columna1, Referencia ) VALUES ( '' '', '' '' ) ' +
'INSERT INTO #resumen ( Columna1, Referencia ) VALUES ( '' '', '' '' ) ' +
'INSERT INTO #resumen ( Columna7, Columna9 ) VALUES ( ''N_IMPUESTO A PAGAR'', ''N_'' + CONVERT(VARCHAR(40), @ImpuestoPagar) ) ' +
' ' +
'SELECT * FROM #resumen ' +
' ' 
)

END 


GO


