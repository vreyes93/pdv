DECLARE 

	@TipoFecha AS VARCHAR(1)		= 'A', 
	@ArticuloDesde  VARCHAR(20)		= '010000-000', 
	@ArticuloHasta  VARCHAR(20)		= 'S00013-000', 
	@BodegaDesde  VARCHAR(4)		= 'DO01', 
	@BodegaHasta VARCHAR(4)			= 'XF20', 
	@FechaFinal DATETIME			= '2017-05-31 23:59:59',
	@Orden SMALLINT					= 0, 
	@CalcularCostos VARCHAR(1)		= 'S', 
	@TipoCosto VARCHAR(1)			= 'F', 
	@MonedaCalculo VARCHAR(1)		= 'L', 
	@SoloConExistencia VARCHAR(1)	= 'S', 
	@ConDisponible VARCHAR(1)		= 'S', 
	@ConReservado VARCHAR(1)		= 'S', 
	@ConNoAprobado VARCHAR(1)		= 'S', 
	@ConVencido VARCHAR(1)			= 'S', 
	@ConRemitido VARCHAR(1)			= 'S', 
	@ConTransito VARCHAR(1)			= 'N', 
	@ConPorDespachar VARCHAR(1)		= 'S' 

BEGIN  
	BEGIN TRY 

		IF OBJECT_ID('tempdb..#TempPorDespachar' ) IS NOT NULL 
		BEGIN 
			DROP TABLE #TempPorDespachar 
		END 

		IF OBJECT_ID('tempdb..#TempPorDespacharDetalle' ) IS NOT NULL 
		BEGIN 
			DROP TABLE #TempPorDespacharDetalle 
		END 

		IF OBJECT_ID('tempdb..#TempInventario' ) IS NOT NULL 
		BEGIN 
			DROP TABLE #TempInventario 
		END 

		CREATE TABLE #TempPorDespachar (
		[CANTIDAD] numeric(38,8),
		[ARTICULO] varchar(20),
		[BODEGA] varchar(4)
		)

		
		CREATE TABLE #TempPorDespacharDetalle (
		[FACTURA] varchar(50),
		[LINEA] smallint,
		[ARTICULO] varchar(20),
		[CANTIDAD] numeric(38,8),		
		[CANTIDAD_PENDIENTE] numeric(38,8),
		[COSTO_PROMEDIO] numeric(38,6),
		[COSTO_TOTAL] numeric(38,6)
		)


		CREATE TABLE #TempInventario ( 
		[articulo] VARCHAR(20), 
		[descripcion] VARCHAR(254), 
		[tipo] VARCHAR(1), 
		[clasificacion_1] VARCHAR(12), 
		[clasificacion_2] VARCHAR(12), 
		[clasificacion_3] VARCHAR(12), 
		[clasificacion_4] VARCHAR(12), 
		[clasificacion_5] VARCHAR(12), 
		[clasificacion_6] VARCHAR(12), 
		[unidad_almacen] VARCHAR(6), 
		[factor_empaque] NUMERIC (28,8), 
		[unidad_empaque] VARCHAR(6), 
		[codigo_barras_invt] VARCHAR(20), 
		[codigo_barras_vent] VARCHAR(20), 
		[activo] VARCHAR(1), 
		[articulo_cuenta] VARCHAR(4), 
		[clase_abc] VARCHAR(1), 
		[proveedor] VARCHAR(20), 
		[tipo_cod_barra_alm] VARCHAR(1), 
		[tipo_cod_barra_det] VARCHAR(1), 
		[ultima_salida] DATETIME, 
		[ultimo_ingreso] DATETIME, 
		[ultimo_movimiento] DATETIME, 
		[usa_lotes] VARCHAR(1), 
		[utilizado_manufact] VARCHAR(1), 
		[articulo_exist_min] NUMERIC (28,8), 
		[bodega] VARCHAR(4), 
		[bodega_descripcion] VARCHAR(40), 
		[Localizacion] INT, 
		[bodega_exist_min] NUMERIC (28,8), 
		[disponible] NUMERIC (38,8), 
		[reservado] NUMERIC (38,8), 
		[vencido] NUMERIC (38,8), 
		[no_aprobado] NUMERIC (38,8), 
		[remitido] NUMERIC (38,8), 
		[transito] NUMERIC (28,8), 
		[por_despachar] NUMERIC (28,8), 
		[costo_unitario] NUMERIC (28,8) 
		) 
	END TRY 
	
	BEGIN CATCH 
	END CATCH 
	
		INSERT INTO #TempPorDespachar
		SELECT 	SUM (
					CASE POR_DESPACHAR_BODEGA.tipo_documento 
						WHEN 'F' THEN POR_DESPACHAR_BODEGA.cantidad
						WHEN 'D' THEN -POR_DESPACHAR_BODEGA.cantidad 
						WHEN 'E' THEN -POR_DESPACHAR_BODEGA.cantidad 
					END ) AS CANTIDAD
				, POR_DESPACHAR_BODEGA.ARTICULO
				, POR_DESPACHAR_BODEGA.BODEGA
		FROM	(                                             
				SELECT 	SUM ( cantidad ) cantidad, 
						fln.articulo,
						fln.bodega,
						'F' tipo_documento
				FROM   	PRODMULT.factura fac (NOLOCK),
						PRODMULT.factura_linea fln (NOLOCK)
				WHERE  	fac.usa_despachos = 'S' 
						AND fac.tipo_documento IN ( 'F','B' )
						AND fac.factura = fln.factura
						AND fac.tipo_documento = fln.tipo_documento 
						AND fac.fecha_hora <= @FechaFinal
						AND ( fac.anulada = 'N' OR ( fac.anulada = 'S' AND fac.fecha_hora_anula > @FechaFinal ) )
				GROUP BY fln.articulo,
						fln.bodega
				UNION
				SELECT 	SUM ( dpd.cantidad ), 
						dpd.articulo,
						fl.BODEGA,
						'E' tipo_documento
				FROM   	PRODMULT.despacho dsp (NOLOCK), 
						PRODMULT.despacho_detalle dpd (NOLOCK)
						, PRODMULT.FACTURA_LINEA fl (NOLOCK)
				WHERE   dsp.despacho = dpd.despacho 
						AND fl.FACTURA = dpd.DOCUM_ORIG
						AND fl.TIPO_DOCUMENTO = dpd.TIPO_DOCUM_ORIG
						AND fl.LINEA = dpd.LINEA_DOCUM_ORIG
						AND dsp.estado = 'D' 
						AND dsp.fch_hora_aplicacion <= @FechaFinal
				GROUP BY dpd.articulo,
						fl.BODEGA
				UNION
				SELECT 	SUM ( cantidad ), 
						fln.articulo,
						fln.bodega,
						'D' tipo_documento
				FROM   	PRODMULT.factura fac (NOLOCK),
						PRODMULT.factura_linea fln (NOLOCK)
				WHERE	fac.usa_despachos = 'S' 
						AND fac.tipo_documento = 'D'
						AND fac.factura = fln.factura
						AND fac.tipo_documento = fln.tipo_documento 
						AND fac.fecha_hora <= @FechaFinal
				GROUP BY fln.articulo,
						fln.bodega
				UNION
				SELECT 	SUM ( dpd.cantidad ), 
						dpd.articulo,
						fl.BODEGA,
						'E' tipo_documento
				FROM   	PRODMULT.despacho dsp (NOLOCK), 
						PRODMULT.despacho_detalle dpd (NOLOCK)
						, PRODMULT.FACTURA_LINEA fl (NOLOCK)
				WHERE   dsp.despacho = dpd.despacho 
						AND fl.FACTURA = dpd.DOCUM_ORIG
						AND fl.TIPO_DOCUMENTO = dpd.TIPO_DOCUM_ORIG
						AND fl.LINEA = dpd.LINEA_DOCUM_ORIG
						AND dsp.estado = 'A' 
						AND dsp.fch_hora_aplicacion <= @FechaFinal
						AND dsp.fch_hora_anulacion > @FechaFinal
				GROUP BY dpd.articulo,
				fl.BODEGA

				) POR_DESPACHAR_BODEGA

		GROUP BY POR_DESPACHAR_BODEGA.ARTICULO
				, POR_DESPACHAR_BODEGA.BODEGA
		HAVING SUM (
				CASE POR_DESPACHAR_BODEGA.tipo_documento 
					WHEN 'F' THEN POR_DESPACHAR_BODEGA.cantidad
					WHEN 'D' THEN -POR_DESPACHAR_BODEGA.cantidad 
					WHEN 'E' THEN -POR_DESPACHAR_BODEGA.cantidad 
				END )  <> 0

		INSERT INTO #TempInventario 
		SELECT 	ARTICULO.articulo,  
				ARTICULO.descripcion,  
				ARTICULO.tipo, 
				ARTICULO.clasificacion_1, 
				ARTICULO.clasificacion_2, 
				ARTICULO.clasificacion_3, 
				ARTICULO.clasificacion_4, 
				ARTICULO.clasificacion_5, 
				ARTICULO.clasificacion_6, 
				ARTICULO.unidad_almacen, 
				ARTICULO.factor_empaque, 
				ARTICULO.unidad_empaque, 
				ARTICULO.codigo_barras_invt, 
				ARTICULO.codigo_barras_vent, 
				ARTICULO.activo, 
				ARTICULO.articulo_cuenta, 
				ARTICULO.clase_abc, 
				ARTICULO.proveedor, 
				ARTICULO.tipo_cod_barra_alm, 
				ARTICULO.tipo_cod_barra_det, 
				ARTICULO.ultima_salida, 
				ARTICULO.ultimo_ingreso, 
				ARTICULO.ultimo_movimiento, 
				ARTICULO.usa_lotes, 
				ARTICULO.utilizado_manufact, 
				ARTICULO.existencia_minima AS articulo_exist_min, 
				EXISTENCIA_BODEGA.bodega, 
				BODEGA.nombre AS bodega_descripcion, 
				NULL AS Localizacion, 
				EXISTENCIA_BODEGA.existencia_minima AS bodega_exist_min, 
				(CASE WHEN @ConDisponible = 'S' THEN ISNULL (EXISTENCIA_BODEGA.cant_disponible,0) - ISNULL(EXISTENCIA.disponible,0) ELSE 0 END ) AS disponible,  
				(CASE WHEN @ConReservado = 'S' THEN ISNULL (EXISTENCIA_BODEGA.cant_reservada,0) - ISNULL(EXISTENCIA.reservado,0) ELSE 0 END ) AS reservado, 
				(CASE WHEN @ConVencido = 'S' THEN ISNULL ( EXISTENCIA_BODEGA.cant_vencida,0) - ISNULL(EXISTENCIA.vencido,0) ELSE 0 END ) AS vencido, 
				(CASE WHEN @ConNoAprobado = 'S' THEN ISNULL ( EXISTENCIA_BODEGA.cant_no_aprobada,0) - ISNULL(EXISTENCIA.no_aprobada,0) ELSE 0 END ) AS no_aprobado, 
				(CASE WHEN @ConRemitido = 'S' THEN ISNULL ( EXISTENCIA_BODEGA.cant_remitida,0) - ISNULL(EXISTENCIA.remitida,0) ELSE 0 END ) AS remitido, 
				(CASE WHEN @ConTransito = 'S' THEN ISNULL ( EXISTENCIA_BODEGA.cant_transito, 0 ) ELSE 0 END ) AS transito, 
				(CASE WHEN @ConPorDespachar = 'S' THEN  ISNULL (TEMP_POR_DESPACHAR.cantidad, 0 ) ELSE 0 END ) AS por_despachar, 
				(CASE WHEN @CalcularCostos = 'S'  
							AND (((CASE WHEN @ConDisponible = 'S' THEN ISNULL ( EXISTENCIA_BODEGA.cant_disponible - ISNULL(EXISTENCIA.disponible,0),0) ELSE 0 END ) 
							+ (CASE WHEN @ConReservado = 'S' THEN ISNULL ( EXISTENCIA_BODEGA.cant_reservada - ISNULL(EXISTENCIA.reservado,0),0) ELSE 0 END ) 
							+ (CASE WHEN @ConVencido = 'S' THEN ISNULL ( EXISTENCIA_BODEGA.cant_vencida - ISNULL(EXISTENCIA.vencido,0),0) ELSE 0 END ) 
							+ (CASE WHEN @ConNoAprobado = 'S' THEN ISNULL ( EXISTENCIA_BODEGA.cant_no_aprobada - ISNULL(EXISTENCIA.no_aprobada,0),0) ELSE 0 END ) 
							+ (CASE WHEN @ConRemitido = 'S' THEN ISNULL ( EXISTENCIA_BODEGA.cant_remitida - ISNULL(EXISTENCIA.remitida,0),0) ELSE 0 END ) ) <> 0) 
					THEN 
					PRODMULT.CostoUnitarioArticuloBodega( 
					@CalcularCostos, 
					@TipoFecha, 
					@FechaFinal, 
					'F', 
					@TipoCosto, 
					@MonedaCalculo, 
					GETDATE(), 
					ARTICULO.articulo, 
					ARTICULO.costo_fiscal, 
					ARTICULO.costo_comparativo, 
					ARTICULO.costo_std_loc, 
					ARTICULO.costo_std_dol, 
					ARTICULO.costo_prom_loc, 
					ARTICULO.costo_prom_dol, 
					ARTICULO.costo_ult_loc, 
					ARTICULO.costo_ult_dol, 
					EXISTENCIA_BODEGA.BODEGA )  
					ELSE 0 END )AS costo_unitario 
		FROM	PRODMULT.existencia_bodega EXISTENCIA_BODEGA (NOLOCK) 
				LEFT JOIN ( 
					SELECT 	articulo, 
							bodega,  
							NULL AS Localizacion, 
							SUM ( CASE 
								WHEN TRANSACCION_INV.naturaleza = 'E' AND ( TRANSACCION_INV.tipo = 'R' OR TRANSACCION_INV.tipo = 'A' OR TRANSACCION_INV.subtipo = 'D' ) THEN ABS(ISNULL(TRANSACCION_INV.cantidad,0))  
								WHEN TRANSACCION_INV.naturaleza = 'S' AND ( TRANSACCION_INV.tipo = 'R' OR TRANSACCION_INV.tipo = 'A' OR TRANSACCION_INV.subtipo = 'D' ) THEN -ABS(ISNULL(TRANSACCION_INV.cantidad,0))  
								ELSE 0 END 
								) AS disponible, 
							SUM ( CASE  
								WHEN TRANSACCION_INV.naturaleza = 'S' AND TRANSACCION_INV.tipo = 'R' THEN ABS(ISNULL(TRANSACCION_INV.cantidad,0))  
								WHEN TRANSACCION_INV.naturaleza = 'E' AND TRANSACCION_INV.tipo <> 'R' AND TRANSACCION_INV.subtipo = 'R' THEN ABS(ISNULL(TRANSACCION_INV.cantidad,0)) 
								WHEN TRANSACCION_INV.naturaleza = 'E' AND TRANSACCION_INV.tipo = 'R' THEN -ABS(ISNULL(TRANSACCION_INV.cantidad,0))  
								WHEN TRANSACCION_INV.naturaleza = 'S' AND TRANSACCION_INV.tipo <> 'R' AND TRANSACCION_INV.subtipo = 'R' THEN -ABS(ISNULL(TRANSACCION_INV.cantidad,0))  
								ELSE 0 END 
								) AS reservado, 
							SUM ( CASE 
								WHEN TRANSACCION_INV.naturaleza = 'S' AND TRANSACCION_INV.tipo = 'N' THEN ABS(ISNULL(TRANSACCION_INV.cantidad,0))  
								WHEN TRANSACCION_INV.naturaleza = 'E' AND TRANSACCION_INV.tipo <> 'N' AND TRANSACCION_INV.subtipo = 'V' THEN ABS(ISNULL(TRANSACCION_INV.cantidad,0)) 
								WHEN TRANSACCION_INV.naturaleza = 'E' AND TRANSACCION_INV.tipo = 'N' THEN -ABS(ISNULL(TRANSACCION_INV.cantidad,0)) 
								WHEN TRANSACCION_INV.naturaleza = 'S' AND TRANSACCION_INV.tipo <> 'N' AND TRANSACCION_INV.subtipo = 'V' THEN -ABS(ISNULL(TRANSACCION_INV.cantidad,0)) 
								ELSE 0 END 
								) AS vencido, 
							SUM ( CASE 
								WHEN TRANSACCION_INV.naturaleza = 'S' AND TRANSACCION_INV.tipo = 'A' THEN ABS(ISNULL(TRANSACCION_INV.cantidad,0))  
								WHEN TRANSACCION_INV.naturaleza = 'E' AND TRANSACCION_INV.tipo <> 'A' AND TRANSACCION_INV.subtipo = 'C' THEN ABS(ISNULL(TRANSACCION_INV.cantidad,0))  
								WHEN TRANSACCION_INV.naturaleza = 'E' AND TRANSACCION_INV.tipo = 'A' THEN -ABS(ISNULL(TRANSACCION_INV.cantidad,0)) 
								WHEN TRANSACCION_INV.naturaleza = 'S' AND TRANSACCION_INV.tipo <> 'A' AND TRANSACCION_INV.subtipo = 'C' THEN -ABS(ISNULL(TRANSACCION_INV.cantidad,0))  
								ELSE 0 END 
								) AS no_aprobada, 
							SUM ( CASE  
								WHEN TRANSACCION_INV.naturaleza = 'S' AND TRANSACCION_INV.tipo = 'I' THEN ABS(ISNULL(TRANSACCION_INV.cantidad,0))  
								WHEN TRANSACCION_INV.naturaleza = 'E' AND TRANSACCION_INV.tipo <> 'I' AND TRANSACCION_INV.subtipo = 'I' THEN ABS(ISNULL(TRANSACCION_INV.cantidad,0))  
								WHEN TRANSACCION_INV.naturaleza = 'E' AND TRANSACCION_INV.tipo = 'I' THEN -ABS(ISNULL(TRANSACCION_INV.cantidad,0))  
								WHEN TRANSACCION_INV.naturaleza = 'S' AND TRANSACCION_INV.tipo <> 'I' AND TRANSACCION_INV.subtipo = 'I' THEN -ABS(ISNULL(TRANSACCION_INV.cantidad,0))  
								ELSE 0 END 
								) AS remitida 
					FROM 	PRODMULT.transaccion_inv TRANSACCION_INV (NOLOCK) 
					WHERE	TRANSACCION_INV.tipo IN ( 'A', 'C', 'E', 'F', 'I', 'M', 'N', 'O', 'P', 'R', 'T', 'V' ) 
							AND TRANSACCION_INV.articulo BETWEEN ISNULL (@ArticuloDesde,'0') AND ISNULL (@ArticuloHasta,'ZZZZZZZZZZZZZZZZZZZZ') 
							AND TRANSACCION_INV.bodega BETWEEN ISNULL(@BodegaDesde,'0') AND ISNULL(@BodegaHasta,'ZZZZ') 
							AND ( ( @TipoFecha = 'D' AND CAST(SUBSTRING(CAST(TRANSACCION_INV.fecha AS CHAR),1,11) AS DATETIME) > ISNULL(@FechaFinal,CAST('2050-12-31 23:59:59.998' AS DATETIME)) ) 
							OR ( @TipoFecha = 'A' AND TRANSACCION_INV.fecha_hora_transac > ISNULL(@FechaFinal,CAST('2050-12-31 23:59:59.998' AS DATETIME)) ) 
							) 
					GROUP BY articulo, bodega 
				) EXISTENCIA 
					ON (EXISTENCIA_BODEGA.articulo = EXISTENCIA.articulo  
						AND EXISTENCIA_BODEGA.bodega = EXISTENCIA.bodega )
				INNER JOIN PRODMULT.bodega BODEGA (NOLOCK) 
					ON EXISTENCIA_BODEGA.bodega = BODEGA.bodega 
				INNER JOIN PRODMULT.articulo ARTICULO (NOLOCK) 
					ON EXISTENCIA_BODEGA.articulo = ARTICULO.articulo 
				LEFT JOIN #TempPorDespachar TEMP_POR_DESPACHAR
					ON (TEMP_POR_DESPACHAR.articulo = EXISTENCIA_BODEGA.articulo  collate SQL_Latin1_General_CP1_CI_AS
						AND TEMP_POR_DESPACHAR.bodega = EXISTENCIA_BODEGA.bodega collate SQL_Latin1_General_CP1_CI_AS)
				LEFT JOIN ( 
					SELECT	ARTICULO,BODEGA,CANT_DISPONIBLE,CANT_RESERVADA, CANT_NO_APROBADA,CANT_VENCIDA,CANT_REMITIDA,TIPO_COSTO,COSTO_FISC_UNT_LOC, COSTO_FISC_UNT_DOL,FECHA_CIERRE 
					FROM   	PRODMULT.EXISTENCIA_CIERRE A 
					WHERE	FECHA_CIERRE = ( SELECT MAX(fecha_cierre) 
								FROM 	PRODMULT.EXISTENCIA_CIERRE B 
								WHERE A.ARTICULO = B.ARTICULO 
								AND A.FECHA_CIERRE >= @FechaFinal 
								AND a.TIPO_FECHA = @TipoFecha  
								)  
				)CIERRE 
				ON (EXISTENCIA_BODEGA.articulo = CIERRE.articulo  
					AND EXISTENCIA_BODEGA.bodega = CIERRE.bodega )
		WHERE	( @SoloConExistencia = 'N' 
					OR ( @SoloConExistencia = 'S'  
						AND (((CASE WHEN @ConDisponible = 'S' THEN ISNULL ( EXISTENCIA_BODEGA.cant_disponible,0) - ISNULL(EXISTENCIA.disponible,0) ELSE 0 END ) 
								+ (CASE WHEN @ConReservado = 'S' THEN ISNULL ( EXISTENCIA_BODEGA.cant_reservada,0) - ISNULL(EXISTENCIA.reservado,0) ELSE 0 END ) 
								+ (CASE WHEN @ConVencido = 'S' THEN ISNULL ( EXISTENCIA_BODEGA.cant_vencida,0) - ISNULL(EXISTENCIA.vencido,0) ELSE 0 END ) 
								+ (CASE WHEN @ConNoAprobado = 'S' THEN ISNULL (EXISTENCIA_BODEGA.cant_no_aprobada,0) - ISNULL(EXISTENCIA.no_aprobada,0) ELSE 0 END ) 
								+ (CASE WHEN @ConRemitido = 'S' THEN ISNULL ( EXISTENCIA_BODEGA.cant_remitida,0) - ISNULL(EXISTENCIA.remitida,0) ELSE 0 END ) ) <> 0 
								) ) ) 
				AND ARTICULO.tipo IN ('T','M','E','R','U','C','B','D','Q') 
				AND ARTICULO.articulo BETWEEN ISNULL (@ArticuloDesde,'0') AND ISNULL (@ArticuloHasta,'ZZZZZZZZZZZZZZZZZZZZ') 
				AND EXISTENCIA_BODEGA.bodega BETWEEN ISNULL(@BodegaDesde,'0') AND ISNULL(@BodegaHasta,'ZZZZ') 
		ORDER BY 1, 27 
		
		INSERT INTO #TempPorDespacharDetalle
		SELECT	fln.FACTURA, 
				fln.LINEA, 
				fln.ARTICULO,
				Fln.CANTIDAD,
				( Fln.CANTIDAD 	- isnull(DESP.CANTIDAD,0) - isnull(DEV.CANTIDAD,0) - isnull(ANUL.CANTIDAD,0))  CANTIDAD_PENDIENTE, 
				(	select avg(I.costo_unitario) from #TempInventario  I where I.articulo = fln.articulo collate SQL_Latin1_General_CP1_CI_AS
				) COSTO_PROMEDIO,
				( Fln.CANTIDAD 	- isnull(DESP.CANTIDAD,0) - isnull(DEV.CANTIDAD,0) - isnull(ANUL.CANTIDAD,0))* 
				(	select avg(I.costo_unitario) from #TempInventario  I where I.articulo = fln.articulo collate SQL_Latin1_General_CP1_CI_AS
				) COSTO_TOTAL
		FROM	PRODMULT.factura fac (NOLOCK)
				inner join PRODMULT.factura_linea fln (NOLOCK)
				on (	fac.factura = fln.factura
						AND fac.tipo_documento = fln.tipo_documento )
				left join (
				SELECT	fl.FACTURA, fl.LINEA, fl.ARTICULO, dpd.cantidad 
						FROM	PRODMULT.despacho dsp (NOLOCK), 
								PRODMULT.despacho_detalle dpd (NOLOCK)
								, PRODMULT.FACTURA_LINEA fl (NOLOCK)
						WHERE   dsp.despacho = dpd.despacho 
								AND fl.FACTURA = dpd.DOCUM_ORIG
								AND fl.TIPO_DOCUMENTO = dpd.TIPO_DOCUM_ORIG
								AND fl.LINEA = dpd.LINEA_DOCUM_ORIG
								AND dsp.estado = 'D' 
								AND dsp.fch_hora_aplicacion <= @FechaFinal
				) DESP
				on (	DESP.ARTICULO = fln.ARTICULO
						and DESP.FACTURA = fln.FACTURA
						and DESP.LINEA = fln.LINEA)
				LEFT JOIN (
				SELECT	fl.FACTURA, fl.LINEA, fl.ARTICULO, cantidad 
						FROM	PRODMULT.factura fac (NOLOCK),
								PRODMULT.factura_linea fl (NOLOCK)
						WHERE   fac.usa_despachos = 'S' 
								AND fac.tipo_documento = 'D'
								AND fac.factura = fl.factura
								AND fac.tipo_documento = fl.tipo_documento 
								AND fac.fecha_hora <= @FechaFinal
					)DEV
					ON 	(DEV.ARTICULO = fln.ARTICULO
								and DEV.FACTURA = fln.FACTURA
								and DEV.LINEA = fln.LINEA)
				LEFT JOIN (
					SELECT	fl.FACTURA, fl.LINEA, fl.ARTICULO, dpd.cantidad 
							FROM	PRODMULT.despacho dsp (NOLOCK), 
									PRODMULT.despacho_detalle dpd (NOLOCK)
									, PRODMULT.FACTURA_LINEA fl (NOLOCK)
							WHERE	dsp.despacho = dpd.despacho 
									AND fl.FACTURA = dpd.DOCUM_ORIG
									AND fl.TIPO_DOCUMENTO = dpd.TIPO_DOCUM_ORIG
									AND fl.LINEA = dpd.LINEA_DOCUM_ORIG
									AND dsp.estado = 'A' 
									AND dsp.fch_hora_aplicacion <= @FechaFinal
									AND dsp.fch_hora_anulacion > @FechaFinal
				) ANUL
				ON 	(ANUL.ARTICULO = fln.ARTICULO
								and ANUL.FACTURA = fln.FACTURA
								and ANUL.LINEA = fln.LINEA)
				
		WHERE   fac.usa_despachos = 'S' 
				AND fac.tipo_documento IN ( 'F','B' )
				AND fac.fecha_hora <= @FechaFinal
				AND ( fac.anulada = 'N' OR ( fac.anulada = 'S' AND fac.fecha_hora_anula > @FechaFinal ) )
				and exists(
						select 1 from #TempPorDespachar PD
						where PD.articulo = fln.articulo collate SQL_Latin1_General_CP1_CI_AS
				)
				and exists(
						select 1 from #TempInventario I
						where I.articulo = fln.articulo collate SQL_Latin1_General_CP1_CI_AS

				)
				and ( Fln.CANTIDAD 	- isnull(DESP.CANTIDAD,0) - isnull(DEV.CANTIDAD,0) - isnull(ANUL.CANTIDAD,0)) > 0

		SELECT  articulo,
				bodega,
				cantidad
		FROM   	#TempPorDespachar
		ORDER BY articulo, bodega

		SELECT  PDD.articulo,
				A.DESCRIPCION ,
				F.FECHA,
				PDD.FACTURA,
				PDD.LINEA,
				F.CLIENTE,
				F.NOMBRE_CLIENTE,
				PDD.cantidad	FACTURADO,
				(PDD.cantidad - PDD.CANTIDAD_PENDIENTE)	DESPACHADO,
				PDD.CANTIDAD_PENDIENTE	PENDIENTE,
				PDD.COSTO_PROMEDIO ,
				PDD.COSTO_TOTAL,
				F.COBRADOR
		FROM	#TempPorDespacharDetalle PDD
				, prodmult.FACTURA F
				, prodmult.ARTICULO A
		WHERE	F.FACTURA = PDD.FACTURA collate SQL_Latin1_General_CP1_CI_AS
				AND A.ARTICULO = PDD.ARTICULO collate SQL_Latin1_General_CP1_CI_AS
		ORDER BY PDD.articulo, PDD.FACTURA

		SELECT  articulo,  
				descripcion,  
				isnull(clasificacion_1,'') as ambiente,  
				isnull(clasificacion_2,'') as linea, 
				isnull(clasificacion_3,'') as tipo, 
				avg(costo_unitario) as costo_promedio, 
				sum(disponible + reservado + vencido + no_aprobado + remitido - por_despachar) as cantidad_en_bodega, 
				sum(disponible + reservado + vencido + no_aprobado + remitido - por_despachar)*avg(costo_unitario) as costo_en_bodega 
		FROM   #TempInventario 
		group by articulo,  
				descripcion,  
				isnull(clasificacion_1,'') ,  
				isnull(clasificacion_2,'') , 
				isnull(clasificacion_3,'')  
		order by 1; 
		 
		SELECT  articulo,  
				descripcion,  
				bodega, 
				isnull(clasificacion_1,'') as ambiente,  
				isnull(clasificacion_2,'') as linea, 
				isnull(clasificacion_3,'') as tipo, 
				avg(costo_unitario) as costo_promedio, 
				sum(disponible + reservado + vencido + no_aprobado + remitido - por_despachar) as cantidad_en_bodega, 
				sum(disponible + reservado + vencido + no_aprobado + remitido - por_despachar)*avg(costo_unitario) as costo_en_bodega 
		FROM   #TempInventario 
		group by articulo,  
				descripcion,  
				bodega, 
				isnull(clasificacion_1,'') ,  
				isnull(clasificacion_2,'') , 
				isnull(clasificacion_3,'')  
		order by 1,2 
END
