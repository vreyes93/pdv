/****** Object:  StoredProcedure [prodmult].[spCargaContabilidad]    Script Date: 28/07/2017 9:43:48 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [prodmult].[spCargaContabilidad]
(
	@PFechaLimite DateTime = '2017-7-7 23:59:59:000' ,
	@PUsuario	varchar(50) = 'SA'
)AS 
BEGIN
	-------------------------------------------------------------------------------------
	--Almacena la cantidad de facturas a cargar a contabilidad
	DECLARE @CantidadFacturas INT;
	DECLARE @CantidadFacturasXDia INT;
	--Almacena la cantidad de despachos a cargar a contabilidad
	DECLARE @CantidadDespachos INT;
	--Almacena la fecha limite a cargar, segun el parametro indicado
	DECLARE @FechaLimite DateTime = @PFechaLimite;
	--Almacena el usuario de la transaccion
	DECLARE @Usuario	varchar(50) =@PUsuario;
	--Almacena acceso al paquete por usuario
	DECLARE @Paquete	varchar(50);
	--Almacena acceso al paquete por usuario
	DECLARE @ProcesoMayorizacion	varchar(50);
	--Almacena el rango de fechas a cargar, necesarios para realizar un asiento por dia
	DECLARE @AsientoDiario	varchar(50) ;
	DECLARE @UltimoAsiento	varchar(50) ;
	DECLARE @ConsecutivoAsientoDiario INT;

	DECLARE @ConsecutivoDiario INT;
	DECLARE @LetraAsientoDiario	varchar(2);

	--Almacena el rango de fechas a cargar, necesarios para realizar un asiento por dia
	DECLARE @FechaACargar DateTime;

	DECLARE @Fecha DateTime;
	

	DECLARE @TipoTransaccion varchar(1); 
	DECLARE @CuentaContable varchar(50); 

	----------------------------------------------------------------------------------
	---Cursor de Factura
	DECLARE @RowFactura_TipoDocumento varchar(1); 
	DECLARE @RowFactura_MonedaFactura varchar(1); 
	DECLARE @RowFactura_Factura varchar(50);
	DECLARE @RowFactura_Fuente varchar(50);
	DECLARE @RowFactura_Cliente varchar(20);
	DECLARE @RowFactura_Contribuyente varchar(20);
	DECLARE @RowFactura_TipoCambio decimal(28,8);
	DECLARE @RowFactura_RowPointer uniqueidentifier;
	DECLARE @RowFactura_CtrVentas varchar(25);
	DECLARE @RowFactura_CtrCostVent varchar(25);

	----------------------------------------------------------------------------------
	--Cursor de Factura Linea
	DECLARE @RowFacturaLinea_Linea smallint; 
	DECLARE @RowFacturaLinea_Articulo varchar(20); 
	DECLARE @RowFacturaLinea_TotalImpuesto decimal(28,8);
	DECLARE @RowFacturaLinea_PrecioTotal decimal(28,8);
	DECLARE @RowFacturaLinea_PrecioTotalDolar decimal(28,8);
	DECLARE @RowFacturaLinea_CostoTotalLocal decimal(28,8); 
	DECLARE @RowFacturaLinea_CostoTotalDolar decimal(28,8); 
	DECLARE @RowFacturaLinea_RowPointer uniqueidentifier; 
	DECLARE @RowFacturaLinea_CentroCosto varchar(25);

	--DECLARE @RowFactura_Observaciones text;
	DECLARE @CostoTotalFactura decimal(28,8); 
	DECLARE @CostoTotalFacturaDolar decimal(28,8); 
	DECLARE @PrecioTotalFactura decimal(28,8); 
	DECLARE @ImpuestoTotalFactura decimal(28,8); 

	----------------------------------------------------------------------------------
	--Cursor de Despacho
	DECLARE @RowDespacho_Despacho varchar(20);
	DECLARE @RowDespacho_Fuente varchar(50);
	DECLARE @RowDespacho_Moneda varchar(1);
	DECLARE @RowDespacho_Cliente  varchar(20);
	DECLARE @RowDespacho_TipoCambio decimal(28,8);
	DECLARE @RowDespacho_RowPointer uniqueidentifier;
	DECLARE @RowDespacho_CtrVentas varchar(25);
	DECLARE @RowDespacho_CtrCostVent varchar(25);
	DECLARE @RowDespacho_AuditTransInv int;

	--Cursor de DespachoDetalle
	DECLARE @RowDespachoDetalle_Articulo varchar(20);
	DECLARE @RowDespachoDetalle_DescuentoTotal decimal(28,8);
	DECLARE @RowDespachoDetalle_DescuentoVolumen decimal(28,8);
	DECLARE @RowDespachoDetalle_PrecioTotal decimal(28,8);
	DECLARE @RowDespachoDetalle_PrecioTotalDolar decimal(28,8);
	DECLARE @RowDespachoDetalle_CostoEstimLocal decimal(28,8);
	DECLARE @RowDespachoDetalle_CostoEstimDolar decimal(28,8);
	DECLARE @RowDespachoDetalle_CostoLocal decimal(28,8);
	DECLARE @RowDespachoDetalle_CostoLocalQtz decimal(28,8);
	DECLARE @RowDestpachoDetalle_CostoDolar decimal(28,8);
	DECLARE @RowDestpachoDetalle_CostoLocalComp decimal(28,8);
	DECLARE @RowDestpachoDetalle_CostoDolarComp decimal(28,8);
	DECLARE @RowDestpachoDetalle_TipoLinea varchar(1);

	----------------------------------------------------------------------------------

	DECLARE @TotalDebitoLocal decimal(28,8); 
	DECLARE @TotalDebitoDolar decimal(28,8); 
	DECLARE @TotalCreditoLocal decimal(28,8); 
	DECLARE @TotalCreditoDolar decimal(28,8); 

	DECLARE @TotalControlLocal decimal(28,8); 
	DECLARE @TotalControlDolar decimal(28,8); 

	DECLARE @TotalFacturaDebitoLocal decimal(28,8); 
	DECLARE @TotalFacturaDebitoDolar decimal(28,8); 
	DECLARE @TotalFacturaCreditoLocal decimal(28,8); 
	DECLARE @TotalFacturaCreditoDolar decimal(28,8); 


	DECLARE @PrecioVenta decimal(28,8)
	DECLARE @PrecioVentaDolar decimal(28,8)
	DECLARE @ImpuestoTotalFacturaDolar decimal(28,8)
	DECLARE @Diferencia decimal(28,8)

	------------------------------------------------------------------------------------

	BEGIN

		------------------------------------------------------------------------------------------------------------------------------------------
		-- Obtenermos la cantidad de facturas a cargar respecto a la fecha limite
		------------------------------------------------------------------------------------------------------------------------------------------
		SELECT  @CantidadFacturas = COUNT(*) 
		FROM 	PRODMULT.factura                         
		WHERE  	fecha <= @FechaLimite
				AND cargado_cg = 'N' 
				AND anulada = 'N' 
				AND tipo_documento <> 'R' 
				AND estado_remision = 'N';


		SELECT	@CantidadDespachos = COUNT( d.despacho )    
		FROM 	PRODMULT.despacho d, PRODMULT.factura f                              
		WHERE  	d.fecha <= @FechaLimite
				AND d.cargado_cg = 'N'  
				AND d.estado = 'D'  
				AND	0 < (	SELECT	COUNT( * )    		
							FROM 	PRODMULT.despacho_detalle dl   	
							WHERE	dl.despacho = d.despacho  
							AND		dl.docum_orig = f.factura  	
							AND		dl.tipo_docum_orig = f.tipo_documento 
				);

		IF(( @CantidadFacturas + @CantidadDespachos) = 0 )
		BEGIN 
			--RETURN 'No se pudo generar ninguna transacción contable. No existen registros a procesar';
			SELECT 'No se pudo generar ninguna transacción contable. No existen registros a procesar';
		END

		------------------------------------------------------------------------------------------------------------------------------------------
		-- Verificamos que el usuario tenga privilegios al modulo de Facturación.
		------------------------------------------------------------------------------------------------------------------------------------------
		SELECT 	@Paquete = paquete   		
		FROM 	PRODMULT.usuario_paquete  		
		WHERE 	usuario = @Usuario  		
				AND paquete = 'FA'  		
				AND  total = 'S'

		IF (ISNULL(@Paquete,'-') <> '-') 
		BEGIN
		
			--------------------------------------------------------------------------------------------------------------------------------------
			-- Verificamos que no se este ejecutando el proceso de mayorización
			--------------------------------------------------------------------------------------------------------------------------------------
			SELECT 	 @ProcesoMayorizacion = nombre --, fecha    
			FROM 	PRODMULT.locks lo(NOLOCK),
					ERPADMIN.usuario usr 
			WHERE 	llave LIKE 'MAYORIZACIONFA'     
					AND lo.usuario = usr.usuario     
					AND lo.usuario <> @Usuario  		

			IF ( ISNULL(@ProcesoMayorizacion,'-' ) = '-')
			BEGIN
			BEGIN TRY
			BEGIN TRANSACTION; 
				----------------------------------------------------------------------------------------------------------------------------------
				-- Colocamos un locks para ejectuar el proceso de carga a contabilidad.
				----------------------------------------------------------------------------------------------------------------------------------
				INSERT INTO PRODMULT.locks( llave, usuario, fecha, connection_id )
				VALUES( 'PAQUETEFASA', @Usuario , ERPADMIN.SF_GETDATE(), HOST_NAME());


				----------------------------------------------------------------------------------------------------------------------------------
				-- Verificamos que el periodo contable no esté cerrado.
				----------------------------------------------------------------------------------------------------------------------------------
				IF( (SELECT	COUNT (fecha_final)
					FROM	PRODMULT.periodo_contable  
					WHERE	estado = 'C'  
							AND	  fecha_final >= @FechaACargar
							) >0)
				BEGIN
					SELECT 'No se puede generar la transacción, ya que existe un cierre para el periodo contable.'
					ROLLBACK;
					RETURN;
				END


				/*****************************************************************************************************************************
				*
				* Recorremos el cursor con las fechas que tienen al menos una factura a cargar a contabilidad.
				*
				******************************************************************************************************************************/
				DECLARE db_fechas CURSOR LOCAL FOR 	
				SELECT  DISTINCT DATEADD(dd, 0, DATEDIFF(DD, 0, fecha))  as Fecha
				FROM 	PRODMULT.factura 
				WHERE 	fecha <= @FechaLimite
						AND cargado_cg = 'N' 
						AND anulada = 'N' 
						AND tipo_documento <> 'R' 
						AND estado_remision = 'N' 
				ORDER BY 1;

				OPEN db_fechas;
				FETCH NEXT FROM db_fechas INTO @FechaACargar;
				WHILE @@FETCH_STATUS = 0  
				BEGIN  


					------------------------------------------------------------------------------------------------------------------------------
					-- Obtenemos el ultimo asiento para crear la transacción.
					--TODO: descomentar este codigo
					------------------------------------------------------------------------------------------------------------------------------
					SELECT 	@UltimoAsiento = ultimo_asiento
					FROM 	PRODMULT.paquete (UPDLOCK)
					WHERE 	paquete = 'FA'

					------------------------------------------------------------------------------------------------------------------------------
					-- Incrementamos en uno el asiento del modulo de facturacion
					------------------------------------------------------------------------------------------------------------------------------
					SET	@LetraAsientoDiario = SUBSTRING(@UltimoAsiento,1,2)

					SET @ConsecutivoAsientoDiario = CAST(SUBSTRING(@UltimoAsiento,3,LEN(@UltimoAsiento)-2) AS INT)  +1

					SET @AsientoDiario = @LetraAsientoDiario + REPLICATE('0', (LEN(@UltimoAsiento)-2 - LEN(@ConsecutivoAsientoDiario))) 
										+ CAST(@ConsecutivoAsientoDiario AS VARCHAR(10))

					------------------------------------------------------------------------------------------------------------------------------
					-- Verificamos si el asiento ya existe en el diario
					------------------------------------------------------------------------------------------------------------------------------
					IF( (SELECT 	ISNULL(asiento,'-')
					FROM 	PRODMULT.asiento_de_diario(NOLOCK) 
					WHERE 	asiento = @AsientoDiario) <> '-')
					BEGIN
						SELECT 'El asiento esta en diario, favor verificar el utlimo asiento.'
						ROLLBACK;
						RETURN;
					END

					------------------------------------------------------------------------------------------------------------------------------
					-- Verificamos si el asiento ya esta mayorizado
					------------------------------------------------------------------------------------------------------------------------------
					IF( (SELECT 	ISNULL(asiento,'-')
					FROM 	PRODMULT.asiento_mayorizado(NOLOCK) 
					WHERE 	asiento = @AsientoDiario) <> '-')
					BEGIN
						SELECT 'El asiento esta mayorizado, favor verificar el utlimo asiento.'
						ROLLBACK;
						RETURN;
					END

					------------------------------------------------------------------------------------------------------------------------------
					-- Actualizamos el ultimo asiento del modulo de facturación
					-- TODO: descomentar este pedazo de codigo
					------------------------------------------------------------------------------------------------------------------------------
					UPDATE 	PRODMULT.paquete 
					SET 	ultimo_asiento = @AsientoDiario       
					WHERE	paquete = 'FA'


					INSERT INTO PRODMULT.asiento_de_diario 
					( paquete, tipo_asiento, fecha, contabilidad, origen, notas, marcado,       
					total_debito_loc, total_credito_loc, total_debito_dol, total_credito_dol,        
					total_control_loc, total_control_dol, ultimo_usuario, fecha_ult_modif,       
					usuario_creacion, fecha_creacion, RowPointer, dependencia, documento_global, 
					asiento, clase_asiento )
					VALUES('FA','FAC',@FechaACargar,'A','FA','','N',
					0.0,0.0,0.0,0.0,0.0,0.0,@Usuario, ERPADMIN.SF_GETDATE(),
					@Usuario, ERPADMIN.SF_GETDATE(),NEWID(),NULL,NULL,@AsientoDiario,'O')

					SET @ConsecutivoDiario = 0;

					/*****************************************************************************************************************************
					*
					* Recorremos el cursor con las facturas del dia, para cargarlas al modulo de contabilidad.
					*
					******************************************************************************************************************************/
					DECLARE db_facturas CURSOR LOCAL FOR 
					SELECT  F.tipo_documento, F.factura, F.cliente, F.tipo_cambio, F.RowPointer, CC.CTR_VENTAS, CC.CTR_COST_VENT, C.CONTRIBUYENTE
							, CASE	WHEN F.TIPO_DOCUMENTO = 'F' THEN 'FAC#' + F.FACTURA 
									WHEN F.TIPO_DOCUMENTO = 'D' THEN 'DEV#' + F.FACTURA 
									ELSE 'FAC#' + F.FACTURA 
							 END AS FUENTE
							 , F.MONEDA_FACTURA
					FROM 	PRODMULT.factura F
							, PRODMULT.cliente C
							, PRODMULT.categoria_cliente CC
					WHERE 	F.fecha >= @FechaACargar 
							AND F.fecha <= DATEADD(SECOND,59,DATEADD(MINUTE,59,DATEADD(HOUR,23,@FechaACargar)))
							AND C.cliente = F.cliente
							AND CC.categoria_cliente = C.categoria_cliente
							AND F.cargado_cg = 'N' 
							AND F.anulada = 'N' 
							AND F.tipo_documento <> 'R' 
							AND F.estado_remision = 'N' 
					ORDER BY 1;

					OPEN db_facturas;
					FETCH NEXT FROM db_facturas INTO  @RowFactura_TipoDocumento, @RowFactura_Factura, @RowFactura_Cliente, @RowFactura_TipoCambio
					, @RowFactura_RowPointer, @RowFactura_CtrVentas, @RowFactura_CtrCostVent, @RowFactura_Contribuyente, @RowFactura_Fuente, @RowFactura_MonedaFactura;
					WHILE @@FETCH_STATUS = 0  
					BEGIN  

						SET @CostoTotalFactura			= 0; 
						SET @CostoTotalFacturaDolar		= 0;
						SET @PrecioTotalFactura			= 0; 
						SET @ImpuestoTotalFactura		= 0; 


						SET	@TotalFacturaDebitoLocal	= 0; 
						SET	@TotalFacturaDebitoDolar	= 0; 
						SET	@TotalFacturaCreditoLocal	= 0; 
						SET	@TotalFacturaCreditoDolar	= 0; 


						/*************************************************************************************************************************
						*
						* Recorremos el cursor con las facturas del dia, para cargarlas al modulo de contabilidad.
						*
						**************************************************************************************************************************/
						
						DECLARE db_factura_linea CURSOR LOCAL FOR 
						SELECT  linea, 
								articulo, 
								ROUND(total_impuesto1,2), 
								CASE WHEN @RowFactura_MonedaFactura = 'L' THEN ROUND(precio_total,2) 
								ELSE ROUND(precio_total * @RowFactura_TipoCambio ,2)
								END, 
								ROUND(costo_total_local,2), 
								ROUND(costo_total_dolar,2), 
								RowPointer, 
								centro_costo,
								CASE WHEN @RowFactura_MonedaFactura = 'L' THEN ROUND(precio_total / @RowFactura_TipoCambio ,2) 
								ELSE ROUND(precio_total,2) 
								END
						FROM 	PRODMULT.factura_linea 
						WHERE 	tipo_documento = @RowFactura_TipoDocumento
								and factura =  @RowFactura_Factura
								AND TIPO_LINEA IN ('N','K')
						ORDER BY linea DESC

						OPEN db_factura_linea;
						FETCH NEXT FROM db_factura_linea INTO  	@RowFacturaLinea_Linea, @RowFacturaLinea_Articulo, @RowFacturaLinea_TotalImpuesto
						, @RowFacturaLinea_PrecioTotal, @RowFacturaLinea_CostoTotalLocal, @RowFacturaLinea_CostoTotalDolar, @RowFacturaLinea_RowPointer
						, @RowFacturaLinea_CentroCosto, @RowFacturaLinea_PrecioTotalDolar ;
						WHILE @@FETCH_STATUS = 0  
						BEGIN  

							
							IF( @RowFacturaLinea_CostoTotalLocal > 0)
							BEGIN


								SET @ConsecutivoDiario = @ConsecutivoDiario + 1;

								/*---COMENTARIO #01
								INSERT INTO #Temp_CG_AUX (   GUID_ORIGEN, TABLA_ORIGEN, ASIENTO, LINEA, COMENTARIO  ) 
								SELECT	@RowFactura_RowPointer GUID_ORIGEN,
										'FACTURA' TABLA_ORIGEN,
										@AsientoDiario ASIENTO,
										@ConsecutivoDiario LINEA,
										'GUID - Origen' COMENTARIO

								INSERT INTO #Temp_DIARIO ( asiento, consecutivo, cuenta_contable, centro_costo, referencia, fuente, debito_local
								, debito_dolar,	credito_local, credito_dolar, debito_unidades, credito_unidades, nit, tipo_cambio, RowPointer
								, base_local, base_dolar, fase, proyecto, documento_global ) 
								SELECT	@AsientoDiario ASIENTO,
										@ConsecutivoDiario consecutivo, 
										'1-1-3-050-0001' cuenta_contable, 
										'0-000' centro_costo,       
										'FAC#' + @RowFactura_Factura referencia, 
										'FAC#' + @RowFactura_Factura fuente,      
										NULL debito_local, 
										NULL debito_dolar,	
										ROUND(@RowFacturaLinea_CostoTotalLocal,2) credito_local,
										ROUND(@RowFacturaLinea_CostoTotalDolar,2) credito_dolar,       
										NULL debito_unidades, 
										NULL credito_unidades, 
										'ND' nit, 
										NULL tipo_cambio, 
										NEWID() RowPointer,      
										NULL base_local, 
										NULL base_dolar, 
										NULL fase, 
										NULL proyecto, 
										NULL documento_global
										*/
										--C
										SET @TipoTransaccion = CASE WHEN @RowFactura_TipoDocumento = 'F' THEN 'C' ELSE 'D' END;
										--1-1-3-050-0001
										SET @CuentaContable = CASE WHEN @RowFactura_TipoDocumento = 'F' THEN '1-1-3-050-0001' ELSE '1-1-3-010-0001' END;
										EXEC prodmult.spRegistroDiario
										@GUID_ORIGEN =  @RowFactura_RowPointer, 
										@TABLA_ORIGEN ='FACTURA', 
										@ASIENTO =@AsientoDiario, 
										@LINEA = @ConsecutivoDiario, 
										@COMENTARIO ='GUID - Origen'										, 
										@CUENTA_CONTABLE = @CuentaContable, 
										@CENTRO_COSTO = '0-000', 
										@FUENTE = @RowFactura_Fuente,  
										@REFERENCIA =@RowFactura_Fuente,
										@TIPO_TRANSACCION = @TipoTransaccion,
										@MONTO_LOCAL = @RowFacturaLinea_CostoTotalLocal, 
										@MONTO_DOLAR = @RowFacturaLinea_CostoTotalDolar, 
										@NIT ='ND', 
										@BASE_LOCAL =NULL, 
										@BASE_DOLAR = NULL

								IF @TipoTransaccion = 'C'  
								BEGIN
										SET @TotalFacturaCreditoLocal = @TotalFacturaCreditoLocal + @RowFacturaLinea_CostoTotalLocal;
										SET @TotalFacturaCreditoDolar = @TotalFacturaCreditoDolar + @RowFacturaLinea_CostoTotalDolar;
								END
								ELSE
								BEGIN
										SET @TotalFacturaDebitoLocal = @TotalFacturaDebitoLocal + @RowFacturaLinea_CostoTotalLocal;
										SET @TotalFacturaDebitoDolar = @TotalFacturaDebitoDolar + @RowFacturaLinea_CostoTotalDolar;
								END;

							END;

							IF( @RowFacturaLinea_PrecioTotal > 0)
							BEGIN

								SET @ConsecutivoDiario = @ConsecutivoDiario + 1;

								/*---COMENTARIO #02
								INSERT INTO #Temp_CG_AUX (   GUID_ORIGEN, TABLA_ORIGEN, ASIENTO, LINEA, COMENTARIO  ) 
								SELECT	@RowFactura_RowPointer GUID_ORIGEN,
										'FACTURA' TABLA_ORIGEN,
										@AsientoDiario ASIENTO,
										@ConsecutivoDiario LINEA,
										'GUID - Origen' COMENTARIO

								INSERT INTO #Temp_DIARIO ( asiento, consecutivo, cuenta_contable, centro_costo, referencia, fuente, debito_local
								, debito_dolar,	credito_local, credito_dolar, debito_unidades, credito_unidades, nit, tipo_cambio, RowPointer
								, base_local, base_dolar, fase, proyecto, documento_global ) 
								SELECT	@AsientoDiario ASIENTO,
										@ConsecutivoDiario consecutivo, 
										'4-1-1-001-0001' cuenta_contable, 
										@RowFactura_CtrCostVent centro_costo,       
										'FAC#' + @RowFactura_Factura referencia, 
										'FAC#' + @RowFactura_Factura fuente,      
										NULL debito_local, 
										NULL debito_dolar,	
										ROUND(@RowFacturaLinea_PrecioTotal,2) credito_local,
										ROUND(@RowFacturaLinea_PrecioTotal / @RowFactura_TipoCambio,2) credito_dolar,       
										NULL debito_unidades, 
										NULL credito_unidades, 
										'ND' nit, 
										NULL tipo_cambio, 
										NEWID() RowPointer,      
										NULL base_local, 
										NULL base_dolar, 
										NULL fase, 
										NULL proyecto, 
										NULL documento_global
										*/

										--C
										SET @TipoTransaccion = CASE WHEN @RowFactura_TipoDocumento = 'F' THEN 'C' ELSE 'D' END;

										EXEC prodmult.spRegistroDiario
										@GUID_ORIGEN =  @RowFactura_RowPointer, 
										@TABLA_ORIGEN ='FACTURA', 
										@ASIENTO =@AsientoDiario, 
										@LINEA = @ConsecutivoDiario, 
										@COMENTARIO ='GUID - Origen', 
										@CUENTA_CONTABLE = '4-1-1-001-0001', 
										@CENTRO_COSTO =  @RowFactura_CtrCostVent, 
										@FUENTE = @RowFactura_Fuente,  
										@REFERENCIA =@RowFactura_Fuente,
										@TIPO_TRANSACCION = @TipoTransaccion,
										@MONTO_LOCAL = @RowFacturaLinea_PrecioTotal, 
										@MONTO_DOLAR = @RowFacturaLinea_PrecioTotalDolar, 
										@NIT ='ND', 
										@BASE_LOCAL =NULL, 
										@BASE_DOLAR = NULL

								IF @TipoTransaccion = 'C'  
								BEGIN
										SET @TotalFacturaCreditoLocal = @TotalFacturaCreditoLocal + @RowFacturaLinea_PrecioTotal;
										SET @TotalFacturaCreditoDolar = @TotalFacturaCreditoDolar + @RowFacturaLinea_PrecioTotalDolar;
								END
								ELSE
								BEGIN
										SET @TotalFacturaDebitoLocal = @TotalFacturaDebitoLocal + @RowFacturaLinea_PrecioTotal;
										SET @TotalFacturaDebitoDolar = @TotalFacturaDebitoDolar + @RowFacturaLinea_PrecioTotalDolar;
								END;
							END;

							-----------------------------------------------------------------------------------------------------------------------------
							-- Se sumarizan todos los valores que serviran para registrar a cuentas contables, por factura.
							-----------------------------------------------------------------------------------------------------------------------------
							SET @CostoTotalFactura = @CostoTotalFactura + @RowFacturaLinea_CostoTotalLocal; 
							SET @CostoTotalFacturaDolar = @CostoTotalFacturaDolar + @RowFacturaLinea_CostoTotalDolar;
							SET @PrecioTotalFactura = @PrecioTotalFactura + @RowFacturaLinea_PrecioTotal; 
							SET @ImpuestoTotalFactura = @ImpuestoTotalFactura + @RowFacturaLinea_TotalImpuesto; 

				
						FETCH NEXT FROM db_factura_linea INTO  	@RowFacturaLinea_Linea, @RowFacturaLinea_Articulo, @RowFacturaLinea_TotalImpuesto
						, @RowFacturaLinea_PrecioTotal, @RowFacturaLinea_CostoTotalLocal, @RowFacturaLinea_CostoTotalDolar, @RowFacturaLinea_RowPointer
						, @RowFacturaLinea_CentroCosto, @RowFacturaLinea_PrecioTotalDolar;
						END;
						CLOSE db_factura_linea;
						DEALLOCATE db_factura_linea;

						IF( @CostoTotalFactura > 0)
							BEGIN

								SET @ConsecutivoDiario = @ConsecutivoDiario + 1;
								/*---COMENTARIO #03
								INSERT INTO #Temp_CG_AUX (   GUID_ORIGEN, TABLA_ORIGEN, ASIENTO, LINEA, COMENTARIO  ) 
								SELECT	@RowFactura_RowPointer GUID_ORIGEN,
										'FACTURA' TABLA_ORIGEN,
										@AsientoDiario ASIENTO,
										@ConsecutivoDiario LINEA,
										'GUID - Origen' COMENTARIO

								INSERT INTO #Temp_DIARIO ( asiento, consecutivo, cuenta_contable, centro_costo, referencia, fuente, debito_local
								, debito_dolar,	credito_local, credito_dolar, debito_unidades, credito_unidades, nit, tipo_cambio, RowPointer
								, base_local, base_dolar, fase, proyecto, documento_global ) 
								SELECT	@AsientoDiario ASIENTO,
										@ConsecutivoDiario consecutivo, 
										'5-1-1-001-0020' cuenta_contable, 
										@RowFactura_CtrCostVent centro_costo,       
										'FAC#' + @RowFactura_Factura referencia, 
										'FAC#' + @RowFactura_Factura fuente,      
										ROUND(@CostoTotalFactura,2) debito_local, 
										ROUND(@CostoTotalFacturaDolar,2) debito_dolar,	
										NULL credito_local,
										NULL credito_dolar,       
										NULL debito_unidades, 
										NULL credito_unidades, 
										'ND' nit, 
										NULL tipo_cambio, 
										NEWID() RowPointer,      
										NULL base_local, 
										NULL base_dolar, 
										NULL fase, 
										NULL proyecto, 
										NULL documento_global
										*/
										--D
										SET @TipoTransaccion = CASE WHEN @RowFactura_TipoDocumento = 'F' THEN 'D' ELSE 'C' END;

										EXEC prodmult.spRegistroDiario
										@GUID_ORIGEN =  @RowFactura_RowPointer, 
										@TABLA_ORIGEN ='FACTURA', 
										@ASIENTO = @AsientoDiario, 
										@LINEA = @ConsecutivoDiario, 
										@COMENTARIO ='GUID - Origen', 
										@CUENTA_CONTABLE = '5-1-1-001-0020', 
										@CENTRO_COSTO =  @RowFactura_CtrCostVent, 
										@FUENTE = @RowFactura_Fuente,  
										@REFERENCIA =@RowFactura_Fuente,
										@TIPO_TRANSACCION = @TipoTransaccion,
										@MONTO_LOCAL = @CostoTotalFactura, 
										@MONTO_DOLAR = @CostoTotalFacturaDolar, 
										@NIT ='ND', 
										@BASE_LOCAL =NULL, 
										@BASE_DOLAR = NULL


								IF @TipoTransaccion = 'C'  
								BEGIN
										SET @TotalFacturaCreditoLocal = @TotalFacturaCreditoLocal + @CostoTotalFactura;
										SET @TotalFacturaCreditoDolar = @TotalFacturaCreditoDolar + @CostoTotalFacturaDolar;
								END
								ELSE
								BEGIN
										SET @TotalFacturaDebitoLocal = @TotalFacturaDebitoLocal + @CostoTotalFactura;
										SET @TotalFacturaDebitoDolar = @TotalFacturaDebitoDolar + @CostoTotalFacturaDolar;
								END;

							END;

						IF( @PrecioTotalFactura > 0)
							BEGIN

								SET @ConsecutivoDiario = @ConsecutivoDiario + 1;
								/*---COMENTARIO #04
								INSERT INTO #Temp_CG_AUX (   GUID_ORIGEN, TABLA_ORIGEN, ASIENTO, LINEA, COMENTARIO  ) 
								SELECT	@RowFactura_RowPointer GUID_ORIGEN,
										'FACTURA' TABLA_ORIGEN,
										@AsientoDiario ASIENTO,
										@ConsecutivoDiario LINEA,
										'GUID - Origen' COMENTARIO

								INSERT INTO #Temp_DIARIO ( asiento, consecutivo, cuenta_contable, centro_costo, referencia, fuente, debito_local
								, debito_dolar,	credito_local, credito_dolar, debito_unidades, credito_unidades, nit, tipo_cambio, RowPointer
								, base_local, base_dolar, fase, proyecto, documento_global ) 
								SELECT	@AsientoDiario ASIENTO,
										@ConsecutivoDiario consecutivo, 
										'1-1-2-001-0001' cuenta_contable, 
										'0-000' centro_costo,       
										'FAC#' + @RowFactura_Factura referencia, 
										'FAC#' + @RowFactura_Factura fuente,      
										ROUND(@PrecioTotalFactura + @ImpuestoTotalFactura,2)  debito_local, 
										ROUND((@PrecioTotalFactura + @ImpuestoTotalFactura) / @RowFactura_TipoCambio,2)  debito_dolar,	
										NULL credito_local,
										NULL credito_dolar,       
										NULL debito_unidades, 
										NULL credito_unidades, 
										'ND' nit, 
										NULL tipo_cambio, 
										NEWID() RowPointer,      
										NULL base_local, 
										NULL base_dolar, 
										NULL fase, 
										NULL proyecto, 
										NULL documento_global
										*/
										--D
										SET @TipoTransaccion = CASE WHEN @RowFactura_TipoDocumento = 'F' THEN 'D' ELSE 'C' END;


										SET @PrecioVenta  = ROUND(@PrecioTotalFactura + @ImpuestoTotalFactura,2);
										SET @PrecioVentaDolar = ROUND((@PrecioTotalFactura + @ImpuestoTotalFactura)/ @RowFactura_TipoCambio,2);

										EXEC prodmult.spRegistroDiario
										@GUID_ORIGEN =  @RowFactura_RowPointer, 
										@TABLA_ORIGEN ='FACTURA', 
										@ASIENTO = @AsientoDiario, 
										@LINEA = @ConsecutivoDiario, 
										@COMENTARIO ='GUID - Origen', 
										@CUENTA_CONTABLE = '1-1-2-001-0001', 
										@CENTRO_COSTO =  '0-000', 
										@FUENTE = @RowFactura_Fuente,  
										@REFERENCIA =@RowFactura_Fuente,
										@TIPO_TRANSACCION = @TipoTransaccion,
										@MONTO_LOCAL = @PrecioVenta, 
										@MONTO_DOLAR = @PrecioVentaDolar, 
										@NIT ='ND', 
										@BASE_LOCAL =NULL, 
										@BASE_DOLAR = NULL

								--SET @TotalFacturaDebitoLocal = @TotalFacturaDebitoLocal + @PrecioVenta;
								--SET @TotalFacturaDebitoDolar = @TotalFacturaDebitoDolar + @PrecioVentaDolar;

								IF @TipoTransaccion = 'C'  
								BEGIN
										SET @TotalFacturaCreditoLocal = @TotalFacturaCreditoLocal + @PrecioVenta;
										SET @TotalFacturaCreditoDolar = @TotalFacturaCreditoDolar + @PrecioVentaDolar;
								END
								ELSE
								BEGIN
										SET @TotalFacturaDebitoLocal = @TotalFacturaDebitoLocal + @PrecioVenta;
										SET @TotalFacturaDebitoDolar = @TotalFacturaDebitoDolar + @PrecioVentaDolar;
								END;
							END;

							IF( @ImpuestoTotalFactura > 0)
							BEGIN

								SET @ConsecutivoDiario = @ConsecutivoDiario + 1;
								/*---COMENTARIO #05
								INSERT INTO #Temp_CG_AUX (   GUID_ORIGEN, TABLA_ORIGEN, ASIENTO, LINEA, COMENTARIO  ) 
								SELECT	@RowFactura_RowPointer GUID_ORIGEN,
										'FACTURA' TABLA_ORIGEN,
										@AsientoDiario ASIENTO,
										@ConsecutivoDiario LINEA,
										'GUID - Origen' COMENTARIO

								INSERT INTO #Temp_DIARIO ( asiento, consecutivo, cuenta_contable, centro_costo, referencia, fuente, debito_local
								, debito_dolar,	credito_local, credito_dolar, debito_unidades, credito_unidades, nit, tipo_cambio, RowPointer
								, base_local, base_dolar, fase, proyecto, documento_global ) 
								SELECT	@AsientoDiario ASIENTO,
										@ConsecutivoDiario consecutivo, 
										'2-1-3-003-0008' cuenta_contable, 
										'0-000' centro_costo,       
										'FAC#' + @RowFactura_Factura referencia, 
										'FAC#' + @RowFactura_Factura fuente,      
										NULL debito_local, 
										NULL debito_dolar,	
										ROUND(@ImpuestoTotalFactura,2)  credito_local,
										ROUND(@ImpuestoTotalFactura  / @RowFactura_TipoCambio,2) credito_dolar,       
										NULL debito_unidades, 
										NULL credito_unidades, 
										'ND' nit, 
										NULL tipo_cambio, 
										NEWID() RowPointer,      
										NULL base_local, 
										NULL base_dolar, 
										NULL fase, 
										NULL proyecto, 
										NULL documento_global
										*/
										--C
										SET @TipoTransaccion = CASE WHEN @RowFactura_TipoDocumento = 'F' THEN 'C' ELSE 'D' END;

										SET @ImpuestoTotalFacturaDolar  = ROUND(@ImpuestoTotalFactura  / @RowFactura_TipoCambio,2);

										EXEC prodmult.spRegistroDiario
										@GUID_ORIGEN =  @RowFactura_RowPointer, 
										@TABLA_ORIGEN ='FACTURA', 
										@ASIENTO = @AsientoDiario, 
										@LINEA = @ConsecutivoDiario, 
										@COMENTARIO ='GUID - Origen', 
										@CUENTA_CONTABLE = '2-1-3-003-0008', 
										@CENTRO_COSTO =  '0-000', 
										@FUENTE = @RowFactura_Fuente,  
										@REFERENCIA =@RowFactura_Fuente,
										@TIPO_TRANSACCION = @TipoTransaccion,
										@MONTO_LOCAL = @ImpuestoTotalFactura, 
										@MONTO_DOLAR = @ImpuestoTotalFacturaDolar, 
										@NIT ='ND', 
										@BASE_LOCAL =NULL, 
										@BASE_DOLAR = NULL

								--SET @TotalFacturaCreditoLocal = @TotalFacturaCreditoLocal + @ImpuestoTotalFactura;
								--SET @TotalFacturaCreditoDolar = @TotalFacturaCreditoDolar + @ImpuestoTotalFacturaDolar;

								IF @TipoTransaccion = 'C'  
								BEGIN
										SET @TotalFacturaCreditoLocal = @TotalFacturaCreditoLocal + @ImpuestoTotalFactura;
										SET @TotalFacturaCreditoDolar = @TotalFacturaCreditoDolar + @ImpuestoTotalFacturaDolar;
								END
								ELSE
								BEGIN
										SET @TotalFacturaDebitoLocal = @TotalFacturaDebitoLocal + @ImpuestoTotalFactura;
										SET @TotalFacturaDebitoDolar = @TotalFacturaDebitoDolar + @ImpuestoTotalFacturaDolar;
								END;


							END;
					
							
							IF(@TotalFacturaCreditoDolar> @TotalFacturaDebitoDolar)
							BEGIN
								SET @ConsecutivoDiario = @ConsecutivoDiario + 1;
								/*---COMENTARIO #06
								INSERT INTO #Temp_CG_AUX (   GUID_ORIGEN, TABLA_ORIGEN, ASIENTO, LINEA, COMENTARIO  ) 
								SELECT	@RowFactura_RowPointer GUID_ORIGEN,
										'FACTURA' TABLA_ORIGEN,
										@AsientoDiario ASIENTO,
										@ConsecutivoDiario LINEA,
										'GUID - Origen' COMENTARIO

								INSERT INTO #Temp_DIARIO ( asiento, consecutivo, cuenta_contable, centro_costo, referencia, fuente, debito_local
								, debito_dolar,	credito_local, credito_dolar, debito_unidades, credito_unidades, nit, tipo_cambio, RowPointer
								, base_local, base_dolar, fase, proyecto, documento_global ) 
								SELECT	@AsientoDiario ASIENTO,
										@ConsecutivoDiario consecutivo, 
										'4-2-1-001-0003' cuenta_contable, 
										'1-001' centro_costo,       
										'FAC#' + @RowFactura_Factura referencia, 
										'FAC#' + @RowFactura_Factura fuente,      
										NULL debito_local, 
										NULL debito_dolar,	
										0 credito_local,
										(@TotalFacturaCreditoDolar - @TotalFacturaDebitoDolar) credito_dolar,       
										NULL debito_unidades, 
										NULL credito_unidades, 
										@RowFactura_Contribuyente nit, 
										NULL tipo_cambio, 
										NEWID() RowPointer,      
										NULL base_local, 
										NULL base_dolar, 
										NULL fase, 
										NULL proyecto, 
										NULL documento_global
										*/

										--C
										SET @TipoTransaccion = CASE WHEN @RowFactura_TipoDocumento = 'F' THEN 'C' ELSE 'D' END;


										SET @Diferencia  = (@TotalFacturaCreditoDolar - @TotalFacturaDebitoDolar);

										EXEC prodmult.spRegistroDiario
										@GUID_ORIGEN =  @RowFactura_RowPointer, 
										@TABLA_ORIGEN ='FACTURA', 
										@ASIENTO = @AsientoDiario, 
										@LINEA = @ConsecutivoDiario, 
										@COMENTARIO ='GUID - Origen', 
										@CUENTA_CONTABLE = '4-2-1-001-0003', 
										@CENTRO_COSTO =  '1-001', 
										@FUENTE = @RowFactura_Fuente,  
										@REFERENCIA =@RowFactura_Fuente,
										@TIPO_TRANSACCION = @TipoTransaccion,
										@MONTO_LOCAL = 0, 
										@MONTO_DOLAR = @Diferencia, 
										@NIT =@RowFactura_Contribuyente, 
										@BASE_LOCAL =NULL, 
										@BASE_DOLAR = NULL


								SET @ConsecutivoDiario = @ConsecutivoDiario + 1;
								/*---COMENTARIO #07
								INSERT INTO #Temp_CG_AUX (   GUID_ORIGEN, TABLA_ORIGEN, ASIENTO, LINEA, COMENTARIO  ) 
								SELECT	@RowFactura_RowPointer GUID_ORIGEN,
										'FACTURA' TABLA_ORIGEN,
										@AsientoDiario ASIENTO,
										@ConsecutivoDiario LINEA,
										'GUID - Origen' COMENTARIO

								INSERT INTO #Temp_DIARIO ( asiento, consecutivo, cuenta_contable, centro_costo, referencia, fuente, debito_local
								, debito_dolar,	credito_local, credito_dolar, debito_unidades, credito_unidades, nit, tipo_cambio, RowPointer
								, base_local, base_dolar, fase, proyecto, documento_global ) 
								SELECT	@AsientoDiario ASIENTO,
										@ConsecutivoDiario consecutivo, 
										'5-3-1-001-0008' cuenta_contable, 
										'1-001' centro_costo,       
										'Diferencia de cambio' referencia, 
										'FAC#' + @RowFactura_Factura fuente,      
										0 debito_local, 
										(@TotalFacturaCreditoDolar - @TotalFacturaDebitoDolar)*2 debito_dolar,	
										NULL credito_local,
										NULL credito_dolar,       
										NULL debito_unidades, 
										NULL credito_unidades, 
										'ND' nit, 
										NULL tipo_cambio, 
										NEWID() RowPointer,      
										NULL base_local, 
										NULL base_dolar, 
										NULL fase, 
										NULL proyecto, 
										NULL documento_global
										*/

										--D
										SET @TipoTransaccion = CASE WHEN @RowFactura_TipoDocumento = 'F' THEN 'D' ELSE 'C' END;
										SET @Diferencia = (@TotalFacturaCreditoDolar - @TotalFacturaDebitoDolar)*2;

										EXEC prodmult.spRegistroDiario
										@GUID_ORIGEN =  @RowFactura_RowPointer, 
										@TABLA_ORIGEN ='FACTURA', 
										@ASIENTO = @AsientoDiario, 
										@LINEA = @ConsecutivoDiario, 
										@COMENTARIO ='GUID - Origen', 
										@CUENTA_CONTABLE = '5-3-1-001-0008', 
										@CENTRO_COSTO =  '1-001', 
										@FUENTE = @RowFactura_Fuente,  
										@REFERENCIA ='Diferencia de cambio',
										@TIPO_TRANSACCION = @TipoTransaccion,
										@MONTO_LOCAL = 0, 
										@MONTO_DOLAR = @Diferencia, 
										@NIT ='ND', 
										@BASE_LOCAL =NULL, 
										@BASE_DOLAR = NULL

							END;

						
							IF(@TotalFacturaCreditoLocal> @TotalFacturaDebitoLocal)
							BEGIN
								SET @ConsecutivoDiario = @ConsecutivoDiario + 1;
							/*---COMENTARIO #08
								INSERT INTO #Temp_CG_AUX (   GUID_ORIGEN, TABLA_ORIGEN, ASIENTO, LINEA, COMENTARIO  ) 
								SELECT	@RowFactura_RowPointer GUID_ORIGEN,
										'FACTURA' TABLA_ORIGEN,
										@AsientoDiario ASIENTO,
										@ConsecutivoDiario LINEA,
										'GUID - Origen' COMENTARIO

								INSERT INTO #Temp_DIARIO ( asiento, consecutivo, cuenta_contable, centro_costo, referencia, fuente, debito_local
								, debito_dolar,	credito_local, credito_dolar, debito_unidades, credito_unidades, nit, tipo_cambio, RowPointer
								, base_local, base_dolar, fase, proyecto, documento_global ) 
								SELECT	@AsientoDiario ASIENTO,
										@ConsecutivoDiario consecutivo, 
										'5-3-1-001-0008' cuenta_contable, 
										'1-001' centro_costo,       
										'Diferencia de cambio' referencia, 
										'FAC#' + @RowFactura_Factura fuente,      
										(@TotalFacturaCreditoLocal - @TotalFacturaDebitoLocal) debito_local, 
										0 debito_dolar,	
										NULL credito_local,
										NULL credito_dolar,       
										NULL debito_unidades, 
										NULL credito_unidades, 
										'ND' nit, 
										NULL tipo_cambio, 
										NEWID() RowPointer,      
										NULL base_local, 
										NULL base_dolar, 
										NULL fase, 
										NULL proyecto, 
										NULL documento_global
										*/

										--D
										SET @TipoTransaccion = CASE WHEN @RowFactura_TipoDocumento = 'F' THEN 'D' ELSE 'C' END;
										SET @Diferencia = (@TotalFacturaCreditoLocal - @TotalFacturaDebitoLocal);

										EXEC prodmult.spRegistroDiario
										@GUID_ORIGEN =  @RowFactura_RowPointer, 
										@TABLA_ORIGEN ='FACTURA', 
										@ASIENTO = @AsientoDiario, 
										@LINEA = @ConsecutivoDiario, 
										@COMENTARIO ='GUID - Origen', 
										@CUENTA_CONTABLE = '5-3-1-001-0008', 
										@CENTRO_COSTO =  '1-001', 
										@FUENTE = @RowFactura_Fuente,  
										@REFERENCIA ='Diferencia de cambio',
										@TIPO_TRANSACCION = @TipoTransaccion,
										@MONTO_LOCAL = @Diferencia, 
										@MONTO_DOLAR = 0, 
										@NIT ='ND', 
										@BASE_LOCAL =NULL, 
										@BASE_DOLAR = NULL


							END;



							IF(@TotalFacturaDebitoLocal > @TotalFacturaCreditoLocal)
							BEGIN
								SET @ConsecutivoDiario = @ConsecutivoDiario + 1;
								/*---COMENTARIO #09
								INSERT INTO #Temp_CG_AUX (   GUID_ORIGEN, TABLA_ORIGEN, ASIENTO, LINEA, COMENTARIO  ) 
								SELECT	@RowFactura_RowPointer GUID_ORIGEN,
										'FACTURA' TABLA_ORIGEN,
										@AsientoDiario ASIENTO,
										@ConsecutivoDiario LINEA,
										'GUID - Origen' COMENTARIO

								INSERT INTO #Temp_DIARIO ( asiento, consecutivo, cuenta_contable, centro_costo, referencia, fuente, debito_local
								, debito_dolar,	credito_local, credito_dolar, debito_unidades, credito_unidades, nit, tipo_cambio, RowPointer
								, base_local, base_dolar, fase, proyecto, documento_global ) 
								SELECT	@AsientoDiario ASIENTO,
										@ConsecutivoDiario consecutivo, 
										'4-2-1-001-0003' cuenta_contable, 
										'1-001' centro_costo,       
										'Diferencia de cambio' referencia, 
										'FAC#' + @RowFactura_Factura fuente,      
										NULL debito_local, 
										NULL debito_dolar,	
										(@TotalFacturaCreditoLocal - @TotalFacturaDebitoLocal) credito_local,
										0 credito_dolar,       
										NULL debito_unidades, 
										NULL credito_unidades, 
										'ND' nit, 
										NULL tipo_cambio, 
										NEWID() RowPointer,      
										NULL base_local, 
										NULL base_dolar, 
										NULL fase, 
										NULL proyecto, 
										NULL documento_global
										*/

										--C
										SET @TipoTransaccion = CASE WHEN @RowFactura_TipoDocumento = 'F' THEN 'C' ELSE 'D' END;
										SET @Diferencia = (@TotalFacturaCreditoLocal - @TotalFacturaDebitoLocal);

										EXEC prodmult.spRegistroDiario
										@GUID_ORIGEN =  @RowFactura_RowPointer, 
										@TABLA_ORIGEN ='FACTURA', 
										@ASIENTO = @AsientoDiario, 
										@LINEA = @ConsecutivoDiario, 
										@COMENTARIO ='GUID - Origen', 
										@CUENTA_CONTABLE = '4-2-1-001-0003', 
										@CENTRO_COSTO =  '1-001', 
										@FUENTE = @RowFactura_Fuente,  
										@REFERENCIA ='Diferencia de cambio',
										@TIPO_TRANSACCION = @TipoTransaccion,
										@MONTO_LOCAL = @Diferencia, 
										@MONTO_DOLAR = 0, 
										@NIT ='ND', 
										@BASE_LOCAL =NULL, 
										@BASE_DOLAR = NULL

							END;



							IF(@TotalFacturaDebitoDolar > @TotalFacturaCreditoDolar)
							BEGIN

									

								SET @ConsecutivoDiario = @ConsecutivoDiario + 1;
								/*---COMENTARIO #10
								INSERT INTO #Temp_CG_AUX (   GUID_ORIGEN, TABLA_ORIGEN, ASIENTO, LINEA, COMENTARIO  ) 
								SELECT	@RowFactura_RowPointer GUID_ORIGEN,
										'FACTURA' TABLA_ORIGEN,
										@AsientoDiario ASIENTO,
										@ConsecutivoDiario LINEA,
										'GUID - Origen' COMENTARIO

								INSERT INTO #Temp_DIARIO ( asiento, consecutivo, cuenta_contable, centro_costo, referencia, fuente, debito_local
								, debito_dolar,	credito_local, credito_dolar, debito_unidades, credito_unidades, nit, tipo_cambio, RowPointer
								, base_local, base_dolar, fase, proyecto, documento_global ) 
								SELECT	@AsientoDiario ASIENTO,
										@ConsecutivoDiario consecutivo, 
										'5-3-1-001-0008' cuenta_contable, 
										'1-001' centro_costo,       
										'FAC#' + @RowFactura_Factura referencia, 
										'FAC#' + @RowFactura_Factura fuente,      
										0 debito_local, 
										(@TotalFacturaDebitoDolar - @TotalFacturaCreditoDolar) debito_dolar,	
										NULL credito_local,
										NULL credito_dolar,       
										NULL debito_unidades, 
										NULL credito_unidades, 
										@RowFactura_Contribuyente nit, 
										NULL tipo_cambio, 
										NEWID() RowPointer,      
										NULL base_local, 
										NULL base_dolar, 
										NULL fase, 
										NULL proyecto, 
										NULL documento_global
										*/

										--D
										SET @TipoTransaccion = CASE WHEN @RowFactura_TipoDocumento = 'F' THEN 'D' ELSE 'C' END;

										SET @Diferencia = (@TotalFacturaDebitoDolar - @TotalFacturaCreditoDolar);

										EXEC prodmult.spRegistroDiario
										@GUID_ORIGEN =  @RowFactura_RowPointer, 
										@TABLA_ORIGEN ='FACTURA', 
										@ASIENTO = @AsientoDiario, 
										@LINEA = @ConsecutivoDiario, 
										@COMENTARIO ='GUID - Origen', 
										@CUENTA_CONTABLE = '5-3-1-001-0008', 
										@CENTRO_COSTO =  '1-001', 
										@FUENTE = @RowFactura_Fuente,  
										@REFERENCIA = @RowFactura_Fuente,
										@TIPO_TRANSACCION = @TipoTransaccion,
										@MONTO_LOCAL = 0, 
										@MONTO_DOLAR = @Diferencia, 
										@NIT =@RowFactura_Contribuyente, 
										@BASE_LOCAL =NULL, 
										@BASE_DOLAR = NULL

								SET @ConsecutivoDiario = @ConsecutivoDiario + 1;
								/*---COMENTARIO #11
								INSERT INTO #Temp_CG_AUX (   GUID_ORIGEN, TABLA_ORIGEN, ASIENTO, LINEA, COMENTARIO  ) 
								SELECT	@RowFactura_RowPointer GUID_ORIGEN,
										'FACTURA' TABLA_ORIGEN,
										@AsientoDiario ASIENTO,
										@ConsecutivoDiario LINEA,
										'GUID - Origen' COMENTARIO

								INSERT INTO #Temp_DIARIO ( asiento, consecutivo, cuenta_contable, centro_costo, referencia, fuente, debito_local
								, debito_dolar,	credito_local, credito_dolar, debito_unidades, credito_unidades, nit, tipo_cambio, RowPointer
								, base_local, base_dolar, fase, proyecto, documento_global ) 
								SELECT	@AsientoDiario ASIENTO,
										@ConsecutivoDiario consecutivo, 
										'4-2-1-001-0003' cuenta_contable, 
										'1-001' centro_costo,       
										'Diferencia de cambio' referencia, 
										'FAC#' + @RowFactura_Factura fuente,      
										NULL debito_local, 
										NULL debito_dolar,	
										0  credito_local,
										(@TotalFacturaDebitoDolar - @TotalFacturaCreditoDolar)*2 credito_dolar,       
										NULL debito_unidades, 
										NULL credito_unidades, 
										'ND' nit, 
										NULL tipo_cambio, 
										NEWID() RowPointer,      
										NULL base_local, 
										NULL base_dolar, 
										NULL fase, 
										NULL proyecto, 
										NULL documento_global
										*/
										--C
										SET @TipoTransaccion = CASE WHEN @RowFactura_TipoDocumento = 'F' THEN 'C' ELSE 'D' END;

										SET @Diferencia = (@TotalFacturaDebitoDolar - @TotalFacturaCreditoDolar)*2;

										EXEC prodmult.spRegistroDiario
										@GUID_ORIGEN =  @RowFactura_RowPointer, 
										@TABLA_ORIGEN ='FACTURA', 
										@ASIENTO = @AsientoDiario, 
										@LINEA = @ConsecutivoDiario, 
										@COMENTARIO ='GUID - Origen', 
										@CUENTA_CONTABLE = '4-2-1-001-0003', 
										@CENTRO_COSTO =  '1-001', 
										@FUENTE = @RowFactura_Fuente,  
										@REFERENCIA =  'Diferencia de cambio',
										@TIPO_TRANSACCION = 'C',
										@MONTO_LOCAL = 0, 
										@MONTO_DOLAR = @Diferencia, 
										@NIT = 'ND', 
										@BASE_LOCAL =NULL, 
										@BASE_DOLAR = NULL

							END;

							--TODO: DESCOMENTAR LAS LINEAS Y VERIFICAR LA FECHA DE APROBACION EN DOCUMENTOS_CC Y EL TIPO CUANDO NO SEA FACTURA
							UPDATE	PRODMULT.factura  	
							SET		cargado_cg = 'S',  		
									asiento_documento = @AsientoDiario 
							WHERE	tipo_documento = @RowFactura_TipoDocumento 
									AND	factura = @RowFactura_Factura 

							UPDATE	PRODMULT.transaccion_inv  
							SET 	contabilizada = 'S' 
							WHERE 	audit_trans_inv = NULL 


							UPDATE 	PRODMULT.audit_trans_inv 
							SET		asiento = @AsientoDiario 
							WHERE 	audit_trans_inv = NULL 

							UPDATE 	PRODMULT.documentos_cc 
							SET		asiento = @AsientoDiario, 
									asiento_pendiente = 'N', 
									aprobado = 'S', 
									usuario_aprobacion = @Usuario,  -->Parametro
									fecha_aprobacion = GETDATE()
							WHERE 	documento = @RowFactura_Factura
									AND tipo =  CASE WHEN @RowFactura_TipoDocumento = 'F' THEN 'FAC' ELSE 'N/C' END


					FETCH NEXT FROM db_facturas INTO  @RowFactura_TipoDocumento, @RowFactura_Factura, @RowFactura_Cliente, @RowFactura_TipoCambio
					, @RowFactura_RowPointer, @RowFactura_CtrVentas, @RowFactura_CtrCostVent, @RowFactura_Contribuyente, @RowFactura_Fuente, @RowFactura_MonedaFactura;
					END;
					CLOSE db_facturas;
					DEALLOCATE db_facturas;


					-------------------------------------------------------------------------------------------------------------------------------------
					-- Actualizamos el asiento de diario, con la sumatoria total
					-------------------------------------------------------------------------------------------------------------------------------------
					UPDATE	P
					SET		TOTAL_DEBITO_LOC = t.TOTAL_DEBITO_LOC
							, TOTAL_DEBITO_DOL = t.TOTAL_DEBITO_DOL
							, TOTAL_CREDITO_LOC = t.TOTAL_CREDITO_LOC
							, TOTAL_CREDITO_DOL = t.TOTAL_CREDITO_DOL
							, TOTAL_CONTROL_LOC = t.TOTAL_CONTROL_LOC
							, TOTAL_CONTROL_DOL = t.TOTAL_CONTROL_DOL
							, FECHA_ULT_MODIF = GETDATE()
					FROM PRODMULT.asiento_de_diario P
					INNER JOIN (
								SELECT	D.ASIENTO, SUM(D.DEBITO_LOCAL) AS TOTAL_DEBITO_LOC, SUM(D.DEBITO_DOLAR) AS TOTAL_DEBITO_DOL
										, SUM(D.CREDITO_LOCAL) AS TOTAL_CREDITO_LOC, SUM(D.CREDITO_DOLAR)  AS  TOTAL_CREDITO_DOL
										, SUM(D.DEBITO_LOCAL) AS TOTAL_CONTROL_LOC, SUM(D.DEBITO_DOLAR) AS TOTAL_CONTROL_DOL
								FROM	PRODMULT.DIARIO D
								WHERE	D.ASIENTO = @AsientoDiario
								GROUP BY D.ASIENTO
								) t
							ON (P.ASIENTO = t.ASIENTO)
					WHERE	P.ASIENTO = @AsientoDiario
				
				FETCH NEXT FROM db_fechas INTO @FechaACargar;
				END;
				CLOSE db_fechas;
				DEALLOCATE db_fechas;


				
				/*****************************************************************************************************************************
				*
				* Recorremos el cursor con las fechas que tienen al menos una factura a cargar a contabilidad.
				*
				******************************************************************************************************************************/
				DECLARE db_fechas_despachos CURSOR LOCAL FOR 	
				SELECT  DISTINCT d.fecha  
				FROM 	PRODMULT.despacho d, PRODMULT.factura f                                   
				WHERE  	d.fecha <= @FechaLimite
						AND 	d.cargado_cg = 'N'  
						AND 	d.estado = 'D'  
						AND	0 < (	SELECT	COUNT( 1 )    		
									FROM 	PRODMULT.despacho_detalle dl   		
									WHERE	dl.despacho = d.despacho  		
											AND	dl.docum_orig = f.factura  		
											AND	dl.tipo_docum_orig = f.tipo_documento 
								)
				ORDER BY 1;

				OPEN db_fechas_despachos;
				FETCH NEXT FROM db_fechas_despachos INTO @FechaACargar;
				WHILE @@FETCH_STATUS = 0  
				BEGIN  

				
					------------------------------------------------------------------------------------------------------------------------------
					-- Obtenemos el ultimo asiento para crear la transacción.
					--TODO: descomentar este codigo
					------------------------------------------------------------------------------------------------------------------------------
					SELECT 	@UltimoAsiento = ultimo_asiento
					FROM 	PRODMULT.paquete (UPDLOCK)
					WHERE 	paquete = 'FA'

					------------------------------------------------------------------------------------------------------------------------------
					-- Incrementamos en uno el asiento del modulo de facturacion
					------------------------------------------------------------------------------------------------------------------------------
					SET	@LetraAsientoDiario = SUBSTRING(@UltimoAsiento,1,2)

					SET @ConsecutivoAsientoDiario = CAST(SUBSTRING(@UltimoAsiento,3,LEN(@UltimoAsiento)-2) AS INT)  +1

					SET @AsientoDiario = @LetraAsientoDiario + REPLICATE('0', (LEN(@UltimoAsiento)-2 - LEN(@ConsecutivoAsientoDiario))) 
										+ CAST(@ConsecutivoAsientoDiario AS VARCHAR(10))

					------------------------------------------------------------------------------------------------------------------------------
					-- Actualizamos el ultimo asiento del modulo de facturación
					-- TODO: descomentar este pedazo de codigo
					------------------------------------------------------------------------------------------------------------------------------
					UPDATE 	PRODMULT.paquete 
					SET 	ultimo_asiento = @AsientoDiario       
					WHERE	paquete = 'FA'

					INSERT INTO PRODMULT.asiento_de_diario 
					( paquete, tipo_asiento, fecha, contabilidad, origen, notas, marcado,       
					total_debito_loc, total_credito_loc, total_debito_dol, total_credito_dol,        
					total_control_loc, total_control_dol, ultimo_usuario, fecha_ult_modif,       
					usuario_creacion, fecha_creacion, RowPointer, dependencia, documento_global, 
					asiento, clase_asiento )
					VALUES('FA','FAC',@FechaACargar,'A','FA','','N',
					0.0,0.0,0.0,0.0,0.0,0.0,@Usuario, ERPADMIN.SF_GETDATE(),
					@Usuario, ERPADMIN.SF_GETDATE(),NEWID(),NULL,NULL,@AsientoDiario,'O')

					SET @ConsecutivoDiario = 0;
					/************************************************************************************************************************************
					*
					* CARGA DE DESPACHOS
					*
					*************************************************************************************************************************************/
					DECLARE db_despachos CURSOR LOCAL FOR 
					SELECT 	d.despacho,	f.moneda_factura, f.cliente, f.tipo_cambio , D.RowPointer , CC.CTR_VENTAS, CC.CTR_COST_VENT, d.audit_trans_inv
							, 'DESP#'+d.despacho
					FROM 	PRODMULT.despacho d
							, PRODMULT.factura f     
							, PRODMULT.cliente C
							, PRODMULT.categoria_cliente CC
					WHERE  	d.fecha <= DATEADD(SECOND,59,DATEADD(MINUTE,59,DATEADD(HOUR,23,@FechaACargar)))  --> PARAMETRO
							AND d.fecha >= @FechaACargar   --> PARAMETRO
							AND d.cargado_cg = 'N'
							AND d.estado = 'D'  
							AND C.cliente = F.cliente
							AND CC.categoria_cliente = C.categoria_cliente
							AND	0 < (	SELECT	COUNT( 1 )    		
										FROM 	PRODMULT.despacho_detalle dl   		
										WHERE	dl.despacho = d.despacho  		
												AND	dl.docum_orig = f.factura  		
												AND	dl.tipo_docum_orig = f.tipo_documento )

					OPEN db_despachos;
					FETCH NEXT FROM db_despachos INTO  @RowDespacho_Despacho, @RowDespacho_Moneda, @RowDespacho_Cliente, @RowDespacho_TipoCambio
					, @RowDespacho_RowPointer, @RowDespacho_CtrVentas, @RowDespacho_CtrCostVent, @RowDespacho_AuditTransInv, @RowDespacho_Fuente;
					WHILE @@FETCH_STATUS = 0  
					BEGIN 
					
						SET	@TotalFacturaDebitoLocal = 0; 
						SET	@TotalFacturaDebitoDolar = 0; 
						SET	@TotalFacturaCreditoLocal = 0; 
						SET	@TotalFacturaCreditoDolar = 0; 


						DECLARE db_despacho_detalle CURSOR LOCAL FOR
						SELECT	d.articulo, 
								( f.desc_tot_linea * d.cantidad ) / f.cantidad AS DESCUENTO_TOTAL, 
								( f.descuento_volumen * d.cantidad ) / f.cantidad AS DESCUENTO_VOLUMEN, 
								( f.precio_total * d.cantidad ) / f.cantidad AS PRECIO_TOTAL, 
								( ( d.costo_local / d.cantidad ) - ( f.costo_estim_local / f.cantidad ) ) * d.cantidad AS COSTO_ESTIM_LOCAL, 
								( ( d.costo_dolar / d.cantidad ) - ( f.costo_estim_dolar / f.cantidad ) ) * d.cantidad AS COSTO_ESTIM_DOLAR, 
								d.costo_local, 
								d.costo_dolar, 
								d.costo_local_comp, 
								d.costo_dolar_comp, 
								d.tipo_linea ,
								case when @RowDespacho_Moneda = 'L' then d.costo_local else f.costo_total_local end
						FROM	PRODMULT.despacho_detalle d, PRODMULT.factura_linea f 
						WHERE	d.despacho = @RowDespacho_Despacho ---> parametro
								AND	d.tipo_docum_orig = f.tipo_documento  
								AND	d.docum_orig = f.factura  
								AND	d.linea_docum_orig = f.linea  
								AND d.tipo_linea in ('K','N')
						ORDER BY	d.linea desc

						OPEN db_despacho_detalle;
						FETCH NEXT FROM db_despacho_detalle INTO  @RowDespachoDetalle_Articulo, @RowDespachoDetalle_DescuentoTotal, @RowDespachoDetalle_DescuentoVolumen
						, @RowDespachoDetalle_PrecioTotal, @RowDespachoDetalle_CostoEstimLocal, @RowDespachoDetalle_CostoEstimDolar, @RowDespachoDetalle_CostoLocal
						, @RowDestpachoDetalle_CostoDolar, @RowDestpachoDetalle_CostoLocalComp, @RowDestpachoDetalle_CostoDolarComp, @RowDestpachoDetalle_TipoLinea, @RowDespachoDetalle_CostoLocalQtz;
						WHILE @@FETCH_STATUS = 0  
						BEGIN 

							SET @ConsecutivoDiario = @ConsecutivoDiario + 1;

							--SET @TotalDebitoLocal = @RowDespachoDetalle_CostoLocalQtz - @RowDespachoDetalle_CostoEstimLocal;
							--case when @RowDespacho_Moneda = 'L' then d.costo_local else f.costo_total_local end

							SET @TotalDebitoLocal =  @RowDespachoDetalle_CostoLocalQtz - 
									case when @RowDespacho_Moneda = 'L' then @RowDespachoDetalle_CostoEstimLocal else @RowDespachoDetalle_CostoEstimDolar end ;

							SET @TotalDebitoDolar = @RowDestpachoDetalle_CostoDolar - @RowDespachoDetalle_CostoEstimDolar;
							

							/*---COMENTARIO #12
							INSERT INTO #Temp_CG_AUX (   GUID_ORIGEN, TABLA_ORIGEN, ASIENTO, LINEA, COMENTARIO  ) 
									SELECT	@RowDespacho_RowPointer GUID_ORIGEN,
											'DESPACHO' TABLA_ORIGEN,
											@AsientoDiario ASIENTO,
											@ConsecutivoDiario LINEA,
											'GUID - Origen' COMENTARIO

								INSERT INTO #Temp_DIARIO ( asiento, consecutivo, cuenta_contable, centro_costo, referencia, fuente, debito_local
								, debito_dolar,	credito_local, credito_dolar, debito_unidades, credito_unidades, nit, tipo_cambio, RowPointer
								, base_local, base_dolar, fase, proyecto, documento_global ) 
								SELECT	@AsientoDiario ASIENTO,
										@ConsecutivoDiario consecutivo, 
										'1-1-3-050-0001' cuenta_contable, 
										'0-000' centro_costo,       
										'DESP#' + @RowDespacho_Despacho referencia, 
										'DESP#' + @RowDespacho_Despacho fuente,      
										@RowDespachoDetalle_CostoLocal debito_local, 
										@RowDestpachoDetalle_CostoDolar debito_dolar,	
										NULL credito_local,
										NULL credito_dolar,       
										NULL debito_unidades, 
										NULL credito_unidades, 
										'ND' nit, 
										NULL tipo_cambio, 
										NEWID() RowPointer,      
										NULL base_local, 
										NULL base_dolar, 
										NULL fase, 
										NULL proyecto, 
										NULL documento_global
										*/

										EXEC prodmult.spRegistroDiario
										@GUID_ORIGEN =  @RowDespacho_RowPointer, 
										@TABLA_ORIGEN ='DESPACHO', 
										@ASIENTO = @AsientoDiario, 
										@LINEA = @ConsecutivoDiario, 
										@COMENTARIO ='GUID - Origen', 
										@CUENTA_CONTABLE = '1-1-3-050-0001', 
										@CENTRO_COSTO =  '0-000', 
										@FUENTE = @RowDespacho_Fuente,  
										@REFERENCIA = @RowDespacho_Fuente,
										@TIPO_TRANSACCION = 'D',
										@MONTO_LOCAL = @TotalDebitoLocal, 
										@MONTO_DOLAR = @TotalDebitoDolar, 
										@NIT = 'ND', 
										@BASE_LOCAL =NULL, 
										@BASE_DOLAR = NULL

							SET	@TotalFacturaDebitoLocal = @TotalFacturaDebitoLocal + @TotalDebitoLocal; 
							SET	@TotalFacturaDebitoDolar = @TotalFacturaDebitoDolar + @TotalDebitoDolar; 


							SET @ConsecutivoDiario = @ConsecutivoDiario + 1;
							/*--- COMENTARIO #13
							INSERT INTO #Temp_CG_AUX (   GUID_ORIGEN, TABLA_ORIGEN, ASIENTO, LINEA, COMENTARIO  ) 
									SELECT	@RowDespacho_RowPointer GUID_ORIGEN,
											'DESPACHO' TABLA_ORIGEN,
											@AsientoDiario ASIENTO,
											@ConsecutivoDiario LINEA,
											'GUID - Origen' COMENTARIO

								INSERT INTO #Temp_DIARIO ( asiento, consecutivo, cuenta_contable, centro_costo, referencia, fuente, debito_local
								, debito_dolar,	credito_local, credito_dolar, debito_unidades, credito_unidades, nit, tipo_cambio, RowPointer
								, base_local, base_dolar, fase, proyecto, documento_global ) 
								SELECT	@AsientoDiario ASIENTO,
										@ConsecutivoDiario consecutivo, 
										'1-1-3-010-0001' cuenta_contable, 
										'0-000' centro_costo,       
										'DESP#' + @RowDespacho_Despacho referencia, 
										'DESP#' + @RowDespacho_Despacho fuente,      
										NULL debito_local, 
										NULL debito_dolar,	
										@RowDespachoDetalle_CostoLocal credito_local,
										@RowDestpachoDetalle_CostoDolar credito_dolar,       
										NULL debito_unidades, 
										NULL credito_unidades, 
										'ND' nit, 
										NULL tipo_cambio, 
										NEWID() RowPointer,      
										NULL base_local, 
										NULL base_dolar, 
										NULL fase, 
										NULL proyecto, 
										NULL documento_global
										*/


										EXEC prodmult.spRegistroDiario
										@GUID_ORIGEN =  @RowDespacho_RowPointer, 
										@TABLA_ORIGEN ='DESPACHO', 
										@ASIENTO = @AsientoDiario, 
										@LINEA = @ConsecutivoDiario, 
										@COMENTARIO ='GUID - Origen', 
										@CUENTA_CONTABLE = '1-1-3-010-0001', 
										@CENTRO_COSTO =  '0-000', 
										@FUENTE = @RowDespacho_Fuente,  
										@REFERENCIA = @RowDespacho_Fuente,
										@TIPO_TRANSACCION = 'C',
										@MONTO_LOCAL = @RowDespachoDetalle_CostoLocal, 
										@MONTO_DOLAR = @RowDestpachoDetalle_CostoDolar, 
										@NIT = 'ND', 
										@BASE_LOCAL =NULL, 
										@BASE_DOLAR = NULL


							SET	@TotalFacturaCreditoLocal = @TotalFacturaCreditoLocal + @RowDespachoDetalle_CostoLocal; 
							SET	@TotalFacturaCreditoDolar = @TotalFacturaCreditoDolar + @RowDestpachoDetalle_CostoDolar; 


						FETCH NEXT FROM db_despacho_detalle INTO  @RowDespachoDetalle_Articulo, @RowDespachoDetalle_DescuentoTotal, @RowDespachoDetalle_DescuentoVolumen
						, @RowDespachoDetalle_PrecioTotal, @RowDespachoDetalle_CostoEstimLocal, @RowDespachoDetalle_CostoEstimDolar, @RowDespachoDetalle_CostoLocal
						, @RowDestpachoDetalle_CostoDolar, @RowDestpachoDetalle_CostoLocalComp, @RowDestpachoDetalle_CostoDolarComp, @RowDestpachoDetalle_TipoLinea, @RowDespachoDetalle_CostoLocalQtz;
						END;
						CLOSE db_despacho_detalle;
						DEALLOCATE db_despacho_detalle;


							IF(( @TotalFacturaCreditoLocal > @TotalFacturaDebitoLocal) or ( @TotalFacturaCreditoDolar > @TotalFacturaDebitoDolar))
							BEGIN

								SET @ConsecutivoDiario = @ConsecutivoDiario + 1;
								/*---COMENTARIO #03
								INSERT INTO #Temp_CG_AUX (   GUID_ORIGEN, TABLA_ORIGEN, ASIENTO, LINEA, COMENTARIO  ) 
								SELECT	@RowFactura_RowPointer GUID_ORIGEN,
										'FACTURA' TABLA_ORIGEN,
										@AsientoDiario ASIENTO,
										@ConsecutivoDiario LINEA,
										'GUID - Origen' COMENTARIO

								INSERT INTO #Temp_DIARIO ( asiento, consecutivo, cuenta_contable, centro_costo, referencia, fuente, debito_local
								, debito_dolar,	credito_local, credito_dolar, debito_unidades, credito_unidades, nit, tipo_cambio, RowPointer
								, base_local, base_dolar, fase, proyecto, documento_global ) 
								SELECT	@AsientoDiario ASIENTO,
										@ConsecutivoDiario consecutivo, 
										'5-1-1-001-0020' cuenta_contable, 
										@RowFactura_CtrCostVent centro_costo,       
										'FAC#' + @RowFactura_Factura referencia, 
										'FAC#' + @RowFactura_Factura fuente,      
										ROUND(@CostoTotalFactura,2) debito_local, 
										ROUND(@CostoTotalFacturaDolar,2) debito_dolar,	
										NULL credito_local,
										NULL credito_dolar,       
										NULL debito_unidades, 
										NULL credito_unidades, 
										'ND' nit, 
										NULL tipo_cambio, 
										NEWID() RowPointer,      
										NULL base_local, 
										NULL base_dolar, 
										NULL fase, 
										NULL proyecto, 
										NULL documento_global
										*/

										SET @TotalDebitoLocal = @TotalFacturaCreditoLocal - @TotalFacturaDebitoLocal; 
										SET @TotalDebitoDolar = @TotalFacturaCreditoDolar - @TotalFacturaDebitoDolar; 
										--DECLARE @TotalCreditoLocal decimal(28,8); 
										--DECLARE @TotalCreditoDolar 

										--D
										EXEC prodmult.spRegistroDiario
										@GUID_ORIGEN =  @RowDespacho_RowPointer, 
										@TABLA_ORIGEN ='DESPACHO', 
										@ASIENTO = @AsientoDiario, 
										@LINEA = @ConsecutivoDiario, 
										@COMENTARIO ='GUID - Origen', 
										@CUENTA_CONTABLE = '5-1-1-001-0020', 
										@CENTRO_COSTO =  @RowDespacho_CtrCostVent, 
										@FUENTE = @RowDespacho_Fuente,  
										@REFERENCIA =@RowDespacho_Fuente,
										@TIPO_TRANSACCION = 'D',
										@MONTO_LOCAL = @TotalDebitoLocal, 
										@MONTO_DOLAR = @TotalDebitoDolar, 
										@NIT ='ND', 
										@BASE_LOCAL =NULL, 
										@BASE_DOLAR = NULL
							END;


							IF(( @TotalFacturaDebitoLocal > @TotalFacturaCreditoLocal) or ( @TotalFacturaDebitoDolar > @TotalFacturaCreditoDolar))
							BEGIN

								SET @ConsecutivoDiario = @ConsecutivoDiario + 1;
								/*---COMENTARIO #03
								INSERT INTO #Temp_CG_AUX (   GUID_ORIGEN, TABLA_ORIGEN, ASIENTO, LINEA, COMENTARIO  ) 
								SELECT	@RowFactura_RowPointer GUID_ORIGEN,
										'FACTURA' TABLA_ORIGEN,
										@AsientoDiario ASIENTO,
										@ConsecutivoDiario LINEA,
										'GUID - Origen' COMENTARIO

								INSERT INTO #Temp_DIARIO ( asiento, consecutivo, cuenta_contable, centro_costo, referencia, fuente, debito_local
								, debito_dolar,	credito_local, credito_dolar, debito_unidades, credito_unidades, nit, tipo_cambio, RowPointer
								, base_local, base_dolar, fase, proyecto, documento_global ) 
								SELECT	@AsientoDiario ASIENTO,
										@ConsecutivoDiario consecutivo, 
										'5-1-1-001-0020' cuenta_contable, 
										@RowFactura_CtrCostVent centro_costo,       
										'FAC#' + @RowFactura_Factura referencia, 
										'FAC#' + @RowFactura_Factura fuente,      
										ROUND(@CostoTotalFactura,2) debito_local, 
										ROUND(@CostoTotalFacturaDolar,2) debito_dolar,	
										NULL credito_local,
										NULL credito_dolar,       
										NULL debito_unidades, 
										NULL credito_unidades, 
										'ND' nit, 
										NULL tipo_cambio, 
										NEWID() RowPointer,      
										NULL base_local, 
										NULL base_dolar, 
										NULL fase, 
										NULL proyecto, 
										NULL documento_global
										*/

										--SET @TotalDebitoLocal = @TotalFacturaCreditoLocal - @TotalFacturaDebitoLocal; 
										--SET @TotalDebitoDolar = @TotalFacturaCreditoDolar - @TotalFacturaDebitoDolar; 
										SET @TotalCreditoLocal = @TotalFacturaDebitoLocal - @TotalFacturaCreditoLocal; 
										SET @TotalCreditoDolar = @TotalFacturaDebitoDolar - @TotalFacturaCreditoDolar; 

										--C
										EXEC prodmult.spRegistroDiario
										@GUID_ORIGEN =  @RowDespacho_RowPointer, 
										@TABLA_ORIGEN ='DESPACHO', 
										@ASIENTO = @AsientoDiario, 
										@LINEA = @ConsecutivoDiario, 
										@COMENTARIO ='GUID - Origen', 
										@CUENTA_CONTABLE = '5-1-1-001-0020', 
										@CENTRO_COSTO =  @RowDespacho_CtrCostVent, 
										@FUENTE = @RowDespacho_Fuente,  
										@REFERENCIA =@RowDespacho_Fuente,
										@TIPO_TRANSACCION = 'C',
										@MONTO_LOCAL = @TotalCreditoLocal, 
										@MONTO_DOLAR = @TotalCreditoDolar, 
										@NIT ='ND', 
										@BASE_LOCAL =NULL, 
										@BASE_DOLAR = NULL
							END;

						
						UPDATE	PRODMULT.transaccion_inv  
						SET 	contabilizada = 'S'  
						WHERE 	audit_trans_inv = @RowDespacho_AuditTransInv

						UPDATE 	PRODMULT.audit_trans_inv  
						SET		asiento			= @AsientoDiario --> Parametro
						WHERE 	audit_trans_inv = @RowDespacho_AuditTransInv --> parametro

						UPDATE	PRODMULT.despacho    
						SET 	cargado_cg			= 'S',  	
								asiento_despacho	= @AsientoDiario --> parametro
						WHERE 	despacho			= @RowDespacho_Despacho --> parametro 

					FETCH NEXT FROM db_despachos INTO  @RowDespacho_Despacho, @RowDespacho_Moneda, @RowDespacho_Cliente, @RowDespacho_TipoCambio
					, @RowDespacho_RowPointer, @RowDespacho_CtrVentas, @RowDespacho_CtrCostVent, @RowDespacho_AuditTransInv, @RowDespacho_Fuente;
					END;
					CLOSE db_despachos;
					DEALLOCATE db_despachos;


					-------------------------------------------------------------------------------------------------------------------------------------
					-- Actualizamos el asiento de diario, con la sumatoria total
					-------------------------------------------------------------------------------------------------------------------------------------
					UPDATE	P
					SET		TOTAL_DEBITO_LOC = t.TOTAL_DEBITO_LOC
							, TOTAL_DEBITO_DOL = t.TOTAL_DEBITO_DOL
							, TOTAL_CREDITO_LOC = t.TOTAL_CREDITO_LOC
							, TOTAL_CREDITO_DOL = t.TOTAL_CREDITO_DOL
							, TOTAL_CONTROL_LOC = t.TOTAL_CONTROL_LOC
							, TOTAL_CONTROL_DOL = t.TOTAL_CONTROL_DOL
							, FECHA_ULT_MODIF = GETDATE()
					FROM PRODMULT.asiento_de_diario P
					INNER JOIN (
								SELECT	D.ASIENTO, SUM(D.DEBITO_LOCAL) AS TOTAL_DEBITO_LOC, SUM(D.DEBITO_DOLAR) AS TOTAL_DEBITO_DOL
										, SUM(D.CREDITO_LOCAL) AS TOTAL_CREDITO_LOC, SUM(D.CREDITO_DOLAR)  AS  TOTAL_CREDITO_DOL
										, SUM(D.DEBITO_LOCAL) AS TOTAL_CONTROL_LOC, SUM(D.DEBITO_DOLAR) AS TOTAL_CONTROL_DOL
								FROM	PRODMULT.DIARIO D
								WHERE	D.ASIENTO = @AsientoDiario
								GROUP BY D.ASIENTO
								) t
							ON (P.ASIENTO = t.ASIENTO)
					WHERE	P.ASIENTO = @AsientoDiario
								
				FETCH NEXT FROM db_fechas_despachos INTO @FechaACargar;
				END;
				CLOSE db_fechas_despachos;
				DEALLOCATE db_fechas_despachos;


				----------------------------------------------------------------------------------------------------------------------------------
				-- Quitamos el lock que se puso para ejectuar el proceso
				----------------------------------------------------------------------------------------------------------------------------------
				DELETE   
				FROM 	PRODMULT.locks 
				WHERE 	llave = 'PAQUETEFASA';

			COMMIT TRANSACTION;
			END TRY
			BEGIN CATCH
				ROLLBACK;
				SELECT 'ROLLBACK' 
				SELECT  ERROR_NUMBER() AS ErrorNumber  
						,ERROR_SEVERITY() AS ErrorSeverity  
						,ERROR_STATE() AS ErrorState  
						,ERROR_PROCEDURE() AS ErrorProcedure  
						,ERROR_LINE() AS ErrorLine  
						,ERROR_MESSAGE() AS ErrorMessage;  
			END CATCH
			END
			ELSE
			BEGIN
				SELECT 'No se puede ejecutar el proceso, debido a que el usuario: ' + isnull(@ProcesoMayorizacion,'') + ' tiene ejecutando el proceso de marorizacion';
				RETURN;
				--RETURN 'No se puede ejecutar el proceso, debido a que el usuario: ' + isnull(@ProcesoMayorizacion,'') + ' tiene ejecutando el proceso de marorizacion';
			END
		END;
		ELSE
		BEGIN
				SELECT 'Usuario sin privilegios';
				--ROLLBACK;
				RETURN;
				--RETURN  'Usuario sin privilegios';
		END
	SELECT 'PROCESO EJECUTADO CON EXISTO';
	--ROLLBACK;
	RETURN;
	--RETURN 'PROCESO EJECUTADO CON EXISTO';
	END;
END;

GO
