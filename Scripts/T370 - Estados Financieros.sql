DROP PROCEDURE prodmult.spEstadosFinancieros
GO

CREATE PROCEDURE prodmult.spEstadosFinancieros @FechaInicial AS DATETIME, @FechaFinal AS DATETIME, @Empresa AS VARCHAR(20), @Ayer AS CHAR(1) = 'N', @Usuario AS VARCHAR(20) = '', @Mostrar AS CHAR(1) = 'N'
AS
BEGIN

--DECLARE @Empresa AS VARCHAR(20) = 'prodmult'
--DECLARE @FechaInicial AS DATETIME = '2017-07-01'
--DECLARE @FechaFinal AS DATETIME = '2017-07-31'
--DECLARE @Usuario AS VARCHAR(20) = 'ROLAPINE'
--DECLARE @Mostrar AS CHAR(1) = 'S'
--DECLARE @Ayer AS CHAR(1) = 'N'

DECLARE @ii AS INT
SET @Empresa = LOWER(@Empresa)

DECLARE @FechaFinalStringMsg AS VARCHAR(20) = (CASE WHEN DAY(@FechaFinal) < 10 THEN '0' ELSE '' END) + CONVERT(VARCHAR(2), DAY(@FechaFinal)) + '/' + (CASE WHEN MONTH(@FechaFinal) < 10 THEN '0' ELSE '' END) + CONVERT(VARCHAR(2), MONTH(@FechaFinal)) + '/' + CONVERT(VARCHAR(4), YEAR(@FechaFinal))

IF (@Ayer = 'S')
BEGIN
	IF ((SELECT COUNT(*) FROM prodmult.MF_EstadosFinancieros WHERE FECHA_INICIAL = @FechaInicial AND FECHA_FINAL = @FechaFinal AND EMPRESA = @Empresa) > 0)
	BEGIN
	
		SELECT ('EF al ' + @FechaFinalStringMsg + ' generados el ' + (CASE WHEN DAY(a.FECHA_HORA_FIN) < 10 THEN '0' ELSE '' END) + CONVERT(VARCHAR(2), DAY(a.FECHA_HORA_FIN)) + '/' + (CASE WHEN MONTH(a.FECHA_HORA_FIN) < 10 THEN '0' ELSE '' END) + CONVERT(VARCHAR(2), MONTH(a.FECHA_HORA_FIN)) + '/' + CONVERT(VARCHAR(4), YEAR(@FechaFinal)) + ' a las ' + CONVERT(VARCHAR(40), a.FECHA_HORA_FIN, 108) + ' por ' + b.NOMBRE) AS mensaje, 'N' AS error
		FROM prodmult.MF_EstadosFinancieros a JOIN erpadmin.USUARIO b ON a.USUARIO = b.USUARIO WHERE a.FECHA_INICIAL = @FechaInicial AND a.FECHA_FINAL = @FechaFinal AND a.EMPRESA = @Empresa
	
		SELECT Cuenta, Descripcion, AnioActual, PctjAnterior, AnioAnterior, PctjAnterior, Diferencias 
		FROM prodmult.MF_EstadosFinancieros_ER_Mes WHERE FECHA_INICIAL = @FechaInicial AND FECHA_FINAL = @FechaFinal AND EMPRESA = @Empresa ORDER BY SECUENCIAL
		
		SELECT Cuenta, Descripcion, AnioActual, PctjAnterior, AnioAnterior, PctjAnterior, Diferencias 
		FROM prodmult.MF_EstadosFinancieros_ER_MesAcum WHERE FECHA_INICIAL = @FechaInicial AND FECHA_FINAL = @FechaFinal AND EMPRESA = @Empresa ORDER BY SECUENCIAL

		SELECT Cuenta, Descripcion, AnioActual, PctjAnterior, AnioAnterior, PctjAnterior, Diferencias 
		FROM prodmult.MF_EstadosFinancieros_ER_MesDet WHERE FECHA_INICIAL = @FechaInicial AND FECHA_FINAL = @FechaFinal AND EMPRESA = @Empresa ORDER BY SECUENCIAL
		
		SELECT Cuenta, Descripcion, AnioActual, PctjAnterior, AnioAnterior, PctjAnterior, Diferencias 
		FROM prodmult.MF_EstadosFinancieros_ER_MesAcumDet WHERE FECHA_INICIAL = @FechaInicial AND FECHA_FINAL = @FechaFinal AND EMPRESA = @Empresa ORDER BY SECUENCIAL

		SELECT Cuenta, Descripcion, AnioActual, PctjAnterior, AnioAnterior, PctjAnterior, Diferencias 
		FROM prodmult.MF_EstadosFinancieros_Balance WHERE FECHA_INICIAL = @FechaInicial AND FECHA_FINAL = @FechaFinal AND EMPRESA = @Empresa ORDER BY SECUENCIAL
		
		SELECT Cuenta, Descripcion, AnioActual, PctjAnterior, AnioAnterior, PctjAnterior, Diferencias 
		FROM prodmult.MF_EstadosFinancieros_BalanceDet WHERE FECHA_INICIAL = @FechaInicial AND FECHA_FINAL = @FechaFinal AND EMPRESA = @Empresa ORDER BY SECUENCIAL

		SELECT	CentroCosto, NombreCCosto, VentasNetas, PctjVentasNetas, CostoVentas, PctjCostoVentas, MargenBruto, PctjMargenBruto,   
				GastosVenta, PctjGastosVenta, ResultPreDist, PctjResultPreDist, GastosPorDist, ResultPosDist, PctjGastosPorDist, PctjTRFinal 
		FROM prodmult.MF_EstadosFinancieros_CentroCosto ORDER BY Secuencial

		SELECT	CentroCosto, NombreCCosto, VentasNetas, PctjVentasNetas, CostoVentas, PctjCostoVentas, MargenBruto, PctjMargenBruto,   
				GastosVenta, PctjGastosVenta, ResultPreDist, PctjResultPreDist, GastosPorDist, ResultPosDist, PctjGastosPorDist, PctjTRFinal 
		FROM prodmult.MF_EstadosFinancieros_CentroCostoAcum ORDER BY Secuencial

		SELECT	CentroCosto, NombreCCosto, VentasNetas, PctjVentasNetas, CostoVentas, PctjCostoVentas, MargenBruto, PctjMargenBruto,   
				GastosVenta, PctjGastosVenta, ResultPreDist, PctjResultPreDist, GastosPorDist, ResultPosDist, PctjGastosPorDist, PctjTRFinal 
		FROM prodmult.MF_EstadosFinancieros_CentroCostoAnt ORDER BY Secuencial

		SELECT	CentroCosto, NombreCCosto, VentasNetas, PctjVentasNetas, CostoVentas, PctjCostoVentas, MargenBruto, PctjMargenBruto,   
				GastosVenta, PctjGastosVenta, ResultPreDist, PctjResultPreDist, GastosPorDist, ResultPosDist, PctjGastosPorDist, PctjTRFinal 
		FROM prodmult.MF_EstadosFinancieros_CentroCostoAntAcum ORDER BY Secuencial
		
	END
	ELSE
	BEGIN
	
		SELECT ('No hay estados financieros generados al ' + CONVERT(VARCHAR(40), @FechaFinal) + '.') AS mensaje, 'S' AS error
	
	END
END
ELSE
BEGIN

DELETE FROM prodmult.MF_EstadosFinancieros WHERE FECHA_INICIAL = @FechaInicial AND FECHA_FINAL = @FechaFinal AND EMPRESA = @Empresa

INSERT INTO prodmult.MF_EstadosFinancieros ( FECHA_INICIAL, FECHA_FINAL, EMPRESA, FECHA_HORA_INICIO, ESTATUS, USUARIO )
VALUES ( @FechaInicial, @FechaFinal, @Empresa, GETDATE(), 'G', @Usuario )

DELETE FROM prodmult.MF_EstadosFinancieros_ER_Mes WHERE FECHA_INICIAL = @FechaInicial AND FECHA_FINAL = @FechaFinal AND EMPRESA = @Empresa
DELETE FROM prodmult.MF_EstadosFinancieros_ER_MesAcum WHERE FECHA_INICIAL = @FechaInicial AND FECHA_FINAL = @FechaFinal AND EMPRESA = @Empresa
DELETE FROM prodmult.MF_EstadosFinancieros_ER_MesDet WHERE FECHA_INICIAL = @FechaInicial AND FECHA_FINAL = @FechaFinal AND EMPRESA = @Empresa
DELETE FROM prodmult.MF_EstadosFinancieros_ER_MesAcumDet WHERE FECHA_INICIAL = @FechaInicial AND FECHA_FINAL = @FechaFinal AND EMPRESA = @Empresa
DELETE FROM prodmult.MF_EstadosFinancieros_Balance WHERE FECHA_INICIAL = @FechaInicial AND FECHA_FINAL = @FechaFinal AND EMPRESA = @Empresa
DELETE FROM prodmult.MF_EstadosFinancieros_BalanceDet WHERE FECHA_INICIAL = @FechaInicial AND FECHA_FINAL = @FechaFinal AND EMPRESA = @Empresa
DELETE FROM prodmult.MF_EstadosFinancieros_CentroCosto WHERE FECHA_INICIAL = @FechaInicial AND FECHA_FINAL = @FechaFinal AND EMPRESA = @Empresa
DELETE FROM prodmult.MF_EstadosFinancieros_CentroCostoAcum WHERE FECHA_INICIAL = @FechaInicial AND FECHA_FINAL = @FechaFinal AND EMPRESA = @Empresa
DELETE FROM prodmult.MF_EstadosFinancieros_CentroCostoAnt WHERE FECHA_INICIAL = @FechaInicial AND FECHA_FINAL = @FechaFinal AND EMPRESA = @Empresa
DELETE FROM prodmult.MF_EstadosFinancieros_CentroCostoAntAcum WHERE FECHA_INICIAL = @FechaInicial AND FECHA_FINAL = @FechaFinal AND EMPRESA = @Empresa

DECLARE @FechaInicialAnterior AS DATETIME = DATEADD(YEAR, -1, @FechaInicial)
DECLARE @FechaFinalAnterior AS DATETIME = DATEADD(YEAR, -1, @FechaFinal)

DECLARE @FechaInicialString AS VARCHAR(20) = CONVERT(VARCHAR(4), YEAR(@FechaInicial)) + '-' + CONVERT(VARCHAR(2), MONTH(@FechaInicial)) + '-' + CONVERT(VARCHAR(2), DAY(@FechaInicial))
DECLARE @FechaFinalString AS VARCHAR(20) = CONVERT(VARCHAR(4), YEAR(@FechaFinal)) + '-' + CONVERT(VARCHAR(2), MONTH(@FechaFinal)) + '-' + CONVERT(VARCHAR(2), DAY(@FechaFinal))

DECLARE @FechaInicialAnteriorString AS VARCHAR(20) = CONVERT(VARCHAR(4), YEAR(@FechaInicialAnterior)) + '-' + CONVERT(VARCHAR(2), MONTH(@FechaInicialAnterior)) + '-' + CONVERT(VARCHAR(2), DAY(@FechaInicialAnterior))
DECLARE @FechaFinalAnteriorString AS VARCHAR(20) = CONVERT(VARCHAR(4), YEAR(@FechaFinalAnterior)) + '-' + CONVERT(VARCHAR(2), MONTH(@FechaFinalAnterior)) + '-' + CONVERT(VARCHAR(2), DAY(@FechaFinalAnterior))

BEGIN TRY
	CREATE TABLE #mes ( SECUENCIAL INT, Cuenta VARCHAR(20), Descripcion VARCHAR(200), AnioActual DECIMAL(12,2), PctjActual DECIMAL(12,2), AnioAnterior DECIMAL(12,2), PctjAnterior DECIMAL(12,2), Diferencias DECIMAL(12,2) ) 
END TRY 
BEGIN CATCH 
	SET @ii = 0 
END CATCH 

BEGIN TRY
	CREATE TABLE #mesDetallado ( SECUENCIAL INT, Cuenta VARCHAR(20), Descripcion VARCHAR(200), AnioActual DECIMAL(12,2), PctjActual DECIMAL(12,2), AnioAnterior DECIMAL(12,2), PctjAnterior DECIMAL(12,2), Diferencias DECIMAL(12,2) ) 
END TRY 
BEGIN CATCH 
	SET @ii = 0 
END CATCH 

BEGIN TRY
	CREATE TABLE #balance ( SECUENCIAL INT, Cuenta VARCHAR(20), Descripcion VARCHAR(200), AnioActual DECIMAL(12,2), PctjActual DECIMAL(12,2), AnioAnterior DECIMAL(12,2), PctjAnterior DECIMAL(12,2), Diferencias DECIMAL(12,2) ) 
END TRY 
BEGIN CATCH 
	SET @ii = 0 
END CATCH 

BEGIN TRY
	CREATE TABLE #balanceDetallado ( SECUENCIAL INT, Cuenta VARCHAR(20), Descripcion VARCHAR(200), AnioActual DECIMAL(12,2), PctjActual DECIMAL(12,2), AnioAnterior DECIMAL(12,2), PctjAnterior DECIMAL(12,2), Diferencias DECIMAL(12,2) ) 
END TRY 
BEGIN CATCH 
	SET @ii = 0 
END CATCH 

BEGIN TRY
	CREATE TABLE #acumulado ( SECUENCIAL INT, Cuenta VARCHAR(20), Descripcion VARCHAR(200), AnioActual DECIMAL(12,2), PctjActual DECIMAL(12,2), AnioAnterior DECIMAL(12,2), PctjAnterior DECIMAL(12,2), Diferencias DECIMAL(12,2) ) 
END TRY 
BEGIN CATCH 
	SET @ii = 0 
END CATCH 

BEGIN TRY
	CREATE TABLE #acumuladoDetallado ( SECUENCIAL INT, Cuenta VARCHAR(20), Descripcion VARCHAR(200), AnioActual DECIMAL(12,2), PctjActual DECIMAL(12,2), AnioAnterior DECIMAL(12,2), PctjAnterior DECIMAL(12,2), Diferencias DECIMAL(12,2) ) 
END TRY 
BEGIN CATCH 
	SET @ii = 0 
END CATCH 

BEGIN TRY
	CREATE TABLE #actual (	SECUENCIAL INT, FECHA DATETIME, CUENTA_CONTABLE VARCHAR(20), CUENTA_NOMBRE VARCHAR(200), SALDO_FINAL DECIMAL(12,2), VENTAS DECIMAL(12,2), 
							SALDO_ACUMULADO DECIMAL(12,2), ACUMULADO DECIMAL(12,2), CUENTA_MADRE VARCHAR(20), TIPO CHAR(1) ) 
END TRY 
BEGIN CATCH 
	SET @ii = 0 
END CATCH 

BEGIN TRY
	CREATE TABLE #anterior (	SECUENCIAL INT, FECHA DATETIME, CUENTA_CONTABLE VARCHAR(20), CUENTA_NOMBRE VARCHAR(200), SALDO_FINAL DECIMAL(12,2), VENTAS DECIMAL(12,2), 
								SALDO_ACUMULADO DECIMAL(12,2), ACUMULADO DECIMAL(12,2), CUENTA_MADRE VARCHAR(20), TIPO CHAR(1) ) 
END TRY 
BEGIN CATCH 
	SET @ii = 0 
END CATCH 

BEGIN TRY
	CREATE TABLE #balanceActual (	SECUENCIAL INT, FECHA DATETIME, CUENTA_CONTABLE VARCHAR(20), CUENTA_NOMBRE VARCHAR(200), SALDO_FINAL DECIMAL(12,2), VENTAS DECIMAL(12,2), 
									SALDO_ANTERIOR DECIMAL(12,2), VENTAS_ANTERIOR DECIMAL(12,2), TIPO CHAR(1) ) 
END TRY 
BEGIN CATCH 
	SET @ii = 0 
END CATCH 

BEGIN TRY
	CREATE TABLE #balanceAnterior (	SECUENCIAL INT, FECHA DATETIME, CUENTA_CONTABLE VARCHAR(20), CUENTA_NOMBRE VARCHAR(200), SALDO_FINAL DECIMAL(12,2), VENTAS DECIMAL(12,2), 
										SALDO_ANTERIOR DECIMAL(12,2), VENTAS_ANTERIOR DECIMAL(12,2), TIPO CHAR(1) ) 
END TRY 
BEGIN CATCH 
	SET @ii = 0 
END CATCH 

BEGIN TRY
	CREATE TABLE #centroCosto (	Empresa VARCHAR(20), Secuencial INT, Fecha DATETIME, CentroCosto VARCHAR(20), Acumulado CHAR(1), NombreCCosto VARCHAR(200), VentasNetas DECIMAL(12,2), 
							PctjVentasNetas DECIMAL(12,2), CostoVentas DECIMAL(12,2), PctjCostoVentas DECIMAL(12,2), MargenBruto DECIMAL(12,2), PctjMargenBruto DECIMAL(12,2),   
							GastosVenta DECIMAL(12,2), PctjGastosVenta DECIMAL(12,2), ResultPreDist DECIMAL(12,2), PctjResultPreDist DECIMAL(12,2), GastosPorDist DECIMAL(12,2), 
							ResultPosDist DECIMAL(12,2), PctjGastosPorDist DECIMAL(12,2), PctjTRFinal DECIMAL(12,2) )					
END TRY 
BEGIN CATCH 
	SET @ii = 0 
END CATCH 

BEGIN TRY
	CREATE TABLE #centroCostoAcum (	Empresa VARCHAR(20), Secuencial INT, Fecha DATETIME, CentroCosto VARCHAR(20), Acumulado CHAR(1), NombreCCosto VARCHAR(200), VentasNetas DECIMAL(12,2), 
							PctjVentasNetas DECIMAL(12,2), CostoVentas DECIMAL(12,2), PctjCostoVentas DECIMAL(12,2), MargenBruto DECIMAL(12,2), PctjMargenBruto DECIMAL(12,2),   
							GastosVenta DECIMAL(12,2), PctjGastosVenta DECIMAL(12,2), ResultPreDist DECIMAL(12,2), PctjResultPreDist DECIMAL(12,2), GastosPorDist DECIMAL(12,2), 
							ResultPosDist DECIMAL(12,2), PctjGastosPorDist DECIMAL(12,2), PctjTRFinal DECIMAL(12,2) )					
END TRY 
BEGIN CATCH 
	SET @ii = 0 
END CATCH 

BEGIN TRY
	CREATE TABLE #centroCostoAnt (	Empresa VARCHAR(20), Secuencial INT, Fecha DATETIME, CentroCosto VARCHAR(20), Acumulado CHAR(1), NombreCCosto VARCHAR(200), VentasNetas DECIMAL(12,2), 
							PctjVentasNetas DECIMAL(12,2), CostoVentas DECIMAL(12,2), PctjCostoVentas DECIMAL(12,2), MargenBruto DECIMAL(12,2), PctjMargenBruto DECIMAL(12,2),   
							GastosVenta DECIMAL(12,2), PctjGastosVenta DECIMAL(12,2), ResultPreDist DECIMAL(12,2), PctjResultPreDist DECIMAL(12,2), GastosPorDist DECIMAL(12,2), 
							ResultPosDist DECIMAL(12,2), PctjGastosPorDist DECIMAL(12,2), PctjTRFinal DECIMAL(12,2) )					
END TRY 
BEGIN CATCH 
	SET @ii = 0 
END CATCH 

BEGIN TRY
	CREATE TABLE #centroCostoAntAcum (	Empresa VARCHAR(20), Secuencial INT, Fecha DATETIME, CentroCosto VARCHAR(20), Acumulado CHAR(1), NombreCCosto VARCHAR(200), VentasNetas DECIMAL(12,2), 
							PctjVentasNetas DECIMAL(12,2), CostoVentas DECIMAL(12,2), PctjCostoVentas DECIMAL(12,2), MargenBruto DECIMAL(12,2), PctjMargenBruto DECIMAL(12,2),   
							GastosVenta DECIMAL(12,2), PctjGastosVenta DECIMAL(12,2), ResultPreDist DECIMAL(12,2), PctjResultPreDist DECIMAL(12,2), GastosPorDist DECIMAL(12,2), 
							ResultPosDist DECIMAL(12,2), PctjGastosPorDist DECIMAL(12,2), PctjTRFinal DECIMAL(12,2) )					
END TRY 
BEGIN CATCH 
	SET @ii = 0 
END CATCH 


DELETE FROM #actual
DELETE FROM #anterior
DELETE FROM #mes
DELETE FROM #mesDetallado
DELETE FROM #acumulado
DELETE FROM #acumuladoDetallado

DELETE FROM #balanceActual
DELETE FROM #balanceAnterior
DELETE FROM #balance
DELETE FROM #balanceDetallado

DELETE FROM #centroCosto
DELETE FROM #centroCostoAcum
DELETE FROM #centroCostoAnt
DELETE FROM #centroCostoAntAcum

INSERT INTO #actual
EXEC prodmult.spEstadoResultados @FechaInicial, @FechaFinal, 'S', 'S'

INSERT INTO #anterior
EXEC prodmult.spEstadoResultados @FechaInicialAnterior, @FechaFinalAnterior, 'S', 'S'

INSERT INTO #mes
SELECT a.SECUENCIAL, a.CUENTA_CONTABLE, a.CUENTA_NOMBRE, a.SALDO_FINAL, a.VENTAS, b.SALDO_FINAL, b.VENTAS, a.SALDO_FINAL - b.SALDO_FINAL
FROM #actual a JOIN #anterior b ON a.SECUENCIAL = b.SECUENCIAL WHERE a.TIPO = 'R' ORDER BY a.SECUENCIAL

INSERT INTO #mesDetallado
SELECT a.SECUENCIAL, a.CUENTA_CONTABLE, a.CUENTA_NOMBRE, a.SALDO_FINAL, a.VENTAS, b.SALDO_FINAL, b.VENTAS, a.SALDO_FINAL - b.SALDO_FINAL 
FROM #actual a JOIN #anterior b ON a.SECUENCIAL = b.SECUENCIAL ORDER BY a.SECUENCIAL

INSERT INTO #acumulado
SELECT a.SECUENCIAL, a.CUENTA_CONTABLE, a.CUENTA_NOMBRE, a.SALDO_ACUMULADO, a.ACUMULADO, b.SALDO_ACUMULADO, b.ACUMULADO, a.SALDO_ACUMULADO - b.SALDO_ACUMULADO
FROM #actual a JOIN #anterior b ON a.SECUENCIAL = b.SECUENCIAL WHERE a.TIPO = 'R' ORDER BY a.SECUENCIAL

INSERT INTO #acumuladoDetallado
SELECT a.SECUENCIAL, a.CUENTA_CONTABLE, a.CUENTA_NOMBRE, a.SALDO_ACUMULADO, a.ACUMULADO, b.SALDO_ACUMULADO, b.ACUMULADO, a.SALDO_ACUMULADO - b.SALDO_ACUMULADO 
FROM #actual a JOIN #anterior b ON a.SECUENCIAL = b.SECUENCIAL ORDER BY a.SECUENCIAL

INSERT INTO #balanceActual
EXEC prodmult.spBalanceGeneral @FechaInicial, @FechaFinal, 'S', 'S'

INSERT INTO #balanceAnterior
EXEC prodmult.spBalanceGeneral @FechaInicialAnterior, @FechaFinalAnterior, 'S', 'S'

INSERT INTO #balance
SELECT a.SECUENCIAL, a.CUENTA_CONTABLE, a.CUENTA_NOMBRE, a.SALDO_FINAL, a.VENTAS, b.SALDO_FINAL, b.VENTAS, a.SALDO_FINAL - b.SALDO_FINAL
FROM #balanceActual a JOIN #balanceAnterior b ON a.SECUENCIAL = b.SECUENCIAL WHERE a.TIPO = 'R' ORDER BY a.SECUENCIAL

INSERT INTO #balanceDetallado
SELECT a.SECUENCIAL, a.CUENTA_CONTABLE, a.CUENTA_NOMBRE, a.SALDO_FINAL, a.VENTAS, b.SALDO_FINAL, b.VENTAS, a.SALDO_FINAL - b.SALDO_FINAL 
FROM #actual a JOIN #anterior b ON a.SECUENCIAL = b.SECUENCIAL ORDER BY a.SECUENCIAL

INSERT INTO #centroCosto
EXEC prodmult.sp_ERTienda @FechaInicial, @FechaFinal, 'N', 'S', @Empresa

INSERT INTO #centroCostoAcum
EXEC prodmult.sp_ERTienda @FechaInicial, @FechaFinal, 'S', 'S', @Empresa

INSERT INTO #centroCostoAnt
EXEC prodmult.sp_ERTienda @FechaInicialAnterior, @FechaFinalAnterior, 'N', 'S', @Empresa

INSERT INTO #centroCostoAntAcum
EXEC prodmult.sp_ERTienda @FechaInicialAnterior, @FechaFinalAnterior, 'S', 'S', @Empresa

INSERT INTO prodmult.MF_EstadosFinancieros_ER_Mes
SELECT @FechaInicial, @FechaFinal, @Empresa, SECUENCIAL, Cuenta, Descripcion, AnioActual, PctjActual, AnioAnterior, PctjAnterior, Diferencias FROM #mes ORDER BY SECUENCIAL

INSERT INTO prodmult.MF_EstadosFinancieros_ER_MesAcum
SELECT @FechaInicial, @FechaFinal, @Empresa, SECUENCIAL, Cuenta, Descripcion, AnioActual, PctjActual, AnioAnterior, PctjAnterior, Diferencias FROM #acumulado ORDER BY SECUENCIAL

INSERT INTO prodmult.MF_EstadosFinancieros_ER_MesDet
SELECT @FechaInicial, @FechaFinal, @Empresa, SECUENCIAL, Cuenta, Descripcion, AnioActual, PctjActual, AnioAnterior, PctjAnterior, Diferencias FROM #mesDetallado ORDER BY SECUENCIAL

INSERT INTO prodmult.MF_EstadosFinancieros_ER_MesAcumDet
SELECT @FechaInicial, @FechaFinal, @Empresa, SECUENCIAL, Cuenta, Descripcion, AnioActual, PctjActual, AnioAnterior, PctjAnterior, Diferencias FROM #acumuladoDetallado ORDER BY SECUENCIAL

INSERT INTO prodmult.MF_EstadosFinancieros_Balance
SELECT @FechaInicial, @FechaFinal, @Empresa, SECUENCIAL, Cuenta, Descripcion, AnioActual, PctjActual, AnioAnterior, PctjAnterior, Diferencias FROM #balance WHERE AnioActual > 0 OR AnioAnterior > 0 ORDER BY SECUENCIAL

INSERT INTO prodmult.MF_EstadosFinancieros_BalanceDet
SELECT @FechaInicial, @FechaFinal, @Empresa, SECUENCIAL, Cuenta, Descripcion, AnioActual, PctjActual, AnioAnterior, PctjAnterior, Diferencias FROM #balanceDetallado WHERE AnioActual > 0 OR AnioAnterior > 0 ORDER BY SECUENCIAL

INSERT INTO prodmult.MF_EstadosFinancieros_CentroCosto
SELECT	@FechaInicial, @FechaFinal, @Empresa, SECUENCIAL, CentroCosto, NombreCCosto, VentasNetas, PctjVentasNetas, CostoVentas, PctjCostoVentas, MargenBruto, 
		PctjMargenBruto, GastosVenta, PctjGastosVenta, ResultPreDist, PctjResultPreDist, GastosPorDist, ResultPosDist, PctjGastosPorDist, PctjTRFinal 
FROM #centroCosto ORDER BY Secuencial

INSERT INTO prodmult.MF_EstadosFinancieros_CentroCostoAcum
SELECT	@FechaInicial, @FechaFinal, @Empresa, SECUENCIAL, CentroCosto, NombreCCosto, VentasNetas, PctjVentasNetas, CostoVentas, PctjCostoVentas, MargenBruto, 
		PctjMargenBruto, GastosVenta, PctjGastosVenta, ResultPreDist, PctjResultPreDist, GastosPorDist, ResultPosDist, PctjGastosPorDist, PctjTRFinal 
FROM #centroCostoAcum ORDER BY Secuencial

INSERT INTO prodmult.MF_EstadosFinancieros_CentroCostoAnt
SELECT	@FechaInicial, @FechaFinal, @Empresa, SECUENCIAL, CentroCosto, NombreCCosto, VentasNetas, PctjVentasNetas, CostoVentas, PctjCostoVentas, MargenBruto, 
		PctjMargenBruto, GastosVenta, PctjGastosVenta, ResultPreDist, PctjResultPreDist, GastosPorDist, ResultPosDist, PctjGastosPorDist, PctjTRFinal 
FROM #centroCostoAnt ORDER BY Secuencial

INSERT INTO prodmult.MF_EstadosFinancieros_CentroCostoAntAcum
SELECT	@FechaInicial, @FechaFinal, @Empresa, SECUENCIAL, CentroCosto, NombreCCosto, VentasNetas, PctjVentasNetas, CostoVentas, PctjCostoVentas, MargenBruto, 
		PctjMargenBruto, GastosVenta, PctjGastosVenta, ResultPreDist, PctjResultPreDist, GastosPorDist, ResultPosDist, PctjGastosPorDist, PctjTRFinal 
FROM #centroCostoAntAcum ORDER BY Secuencial

IF (@Mostrar = 'S')
BEGIN
	SELECT Cuenta, Descripcion, AnioActual, PctjAnterior, AnioAnterior, PctjAnterior, Diferencias FROM #mes ORDER BY SECUENCIAL
	SELECT Cuenta, Descripcion, AnioActual, PctjAnterior, AnioAnterior, PctjAnterior, Diferencias FROM #acumulado ORDER BY SECUENCIAL

	SELECT Cuenta, Descripcion, AnioActual, PctjAnterior, AnioAnterior, PctjAnterior, Diferencias FROM #mesDetallado ORDER BY SECUENCIAL
	SELECT Cuenta, Descripcion, AnioActual, PctjAnterior, AnioAnterior, PctjAnterior, Diferencias FROM #acumuladoDetallado ORDER BY SECUENCIAL

	SELECT Cuenta, Descripcion, AnioActual, PctjAnterior, AnioAnterior, PctjAnterior, Diferencias FROM #balance WHERE AnioActual > 0 OR AnioAnterior > 0
	SELECT Cuenta, Descripcion, AnioActual, PctjAnterior, AnioAnterior, PctjAnterior, Diferencias FROM #balanceDetallado WHERE AnioActual > 0 OR AnioAnterior > 0

	SELECT	CentroCosto, NombreCCosto, VentasNetas, PctjVentasNetas, CostoVentas, PctjCostoVentas, MargenBruto, PctjMargenBruto,   
			GastosVenta, PctjGastosVenta, ResultPreDist, PctjResultPreDist, GastosPorDist, ResultPosDist, PctjGastosPorDist, PctjTRFinal 
	FROM #centroCosto
	ORDER BY Secuencial

	SELECT	CentroCosto, NombreCCosto, VentasNetas, PctjVentasNetas, CostoVentas, PctjCostoVentas, MargenBruto, PctjMargenBruto,   
			GastosVenta, PctjGastosVenta, ResultPreDist, PctjResultPreDist, GastosPorDist, ResultPosDist, PctjGastosPorDist, PctjTRFinal 
	FROM #centroCostoAcum
	ORDER BY Secuencial

	SELECT	CentroCosto, NombreCCosto, VentasNetas, PctjVentasNetas, CostoVentas, PctjCostoVentas, MargenBruto, PctjMargenBruto,   
			GastosVenta, PctjGastosVenta, ResultPreDist, PctjResultPreDist, GastosPorDist, ResultPosDist, PctjGastosPorDist, PctjTRFinal 
	FROM #centroCostoAnt
	ORDER BY Secuencial

	SELECT	CentroCosto, NombreCCosto, VentasNetas, PctjVentasNetas, CostoVentas, PctjCostoVentas, MargenBruto, PctjMargenBruto,   
			GastosVenta, PctjGastosVenta, ResultPreDist, PctjResultPreDist, GastosPorDist, ResultPosDist, PctjGastosPorDist, PctjTRFinal 
	FROM #centroCostoAntAcum
	ORDER BY Secuencial
END

UPDATE prodmult.MF_EstadosFinancieros SET FECHA_HORA_FIN = GETDATE(), ESTATUS = 'F' WHERE FECHA_INICIAL = @FechaInicial AND FECHA_FINAL = @FechaFinal AND EMPRESA = @Empresa

IF (LEN(@Usuario) > 0)
BEGIN
	SET @Usuario = REPLACE(@Usuario, 'FA/', '')
	
	DECLARE @Asunto AS VARCHAR(200) = 'EF al ' + @FechaFinalStringMsg
	DECLARE @Body AS VARCHAR(200) = 'Por este medio se le informa que los Estados Financieros al ' + @FechaFinalStringMsg + ' ya se encuentran listos para ser descargados en el PDV.'
	DECLARE @Email AS VARCHAR(100) = (SELECT b.EMAIL FROM ERPADMIN.USUARIO a JOIN prodmult.MF_Vendedor b ON a.CELULAR = b.VENDEDOR WHERE a.USUARIO = @Usuario)
	
	EXEC msdb.dbo.sp_send_dbmail @profile_name = 'PDV',
			@recipients = @Email,
			@subject = @Asunto,
			@body = @Body
END

END

END 

GO

GRANT EXECUTE ON prodmult.spEstadosFinancieros TO [REPORTEADOR]
GO
GRANT EXECUTE ON prodmult.spEstadosFinancieros TO [FIESTA\rolando.pineda]
GO
GRANT EXECUTE ON prodmult.spEstadosFinancieros TO [FIESTA\william.quiacain]
GO
GRANT EXECUTE ON prodmult.spEstadosFinancieros TO [FIESTA\mike]
GO
GRANT EXECUTE ON prodmult.spEstadosFinancieros TO [mf.fiestanet]
GO
GRANT EXECUTE ON prodmult.spEstadosFinancieros TO [FIESTA\eder.hulse]
GO


--EXEC prodmult.spEstadosFinancieros '2017-07-01', '2017-07-31', 'prodmult', 'N', 'ROLAPINE', 'N'


