-- - -----------------------------------------
-- Servidor: sql.fiesta.local
-- Esquema:  exatuserp
-- - -----------------------------------------


insert into [prodmult].[MF_Tabla_Catalogo]
(CODIGO_TABLA, NOMBRE_TABLA)
VALUES(7,'Lista de correo - Cemaco - Pedidos del dia');

INSERT INTO [prodmult].[MF_Catalogo]
(CODIGO_TABLA, CODIGO_CAT,NOMBRE_CAT,ORDEN)
VALUES('7','William Quiacain', 'william.quiacain@mueblesfiesta.com',1);
INSERT INTO [prodmult].[MF_Catalogo]
(CODIGO_TABLA, CODIGO_CAT,NOMBRE_CAT,ORDEN)
VALUES('7','Juan Manuel Carrillo', 'manuel.carrillo@productosmultiples.com',2);
INSERT INTO [prodmult].[MF_Catalogo]
(CODIGO_TABLA, CODIGO_CAT,NOMBRE_CAT,ORDEN)
VALUES('7','Valeska Ochoa', 'valeska.ochoa@productosmultiples.com',3);
INSERT INTO [prodmult].[MF_Catalogo]
(CODIGO_TABLA, CODIGO_CAT,NOMBRE_CAT,ORDEN)
VALUES('7','Rosaura Gutiérrez', 'rosaura.gutierrez@productosmultiples.com',4);
INSERT INTO [prodmult].[MF_Catalogo]
(CODIGO_TABLA, CODIGO_CAT,NOMBRE_CAT,ORDEN)
VALUES('7','Jorge Mata', 'jorge.mata@productosmultiples.com',5);
INSERT INTO [prodmult].[MF_Catalogo]
(CODIGO_TABLA, CODIGO_CAT,NOMBRE_CAT,ORDEN)
VALUES('7','James Altalef', 'james.altalef@productosmultiples.comstef',6);
INSERT INTO [prodmult].[MF_Catalogo]
(CODIGO_TABLA, CODIGO_CAT,NOMBRE_CAT,ORDEN)
VALUES('7','Stefan Landsberger', 'stefan.landsberger@mueblesfiesta.com',7);