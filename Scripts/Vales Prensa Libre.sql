CREATE TABLE prodmult.MF_Vale (
	VALE				VARCHAR(20)			NOT NULL,
	VALOR				DECIMAL(12,2)		NOT NULL,
	PEDIDO				VARCHAR(20)			NULL,
	FACTURADO			CHAR(1)				NOT NULL DEFAULT('N'),
	FACTURA				VARCHAR(20)			NULL,
	FECHA_FACTURA		DATETIME			NULL,
	RecordDate			CurrentDateType		NOT NULL DEFAULT(GETDATE()),
	CreatedBy			UsernameType		NOT NULL,
	UpdatedBy			UsernameType		NULL,
	CreateDate			CurrentDateType		NOT NULL DEFAULT(GETDATE())
)
GO

ALTER TABLE prodmult.MF_Vale ADD PRIMARY KEY ( VALE )
GO

CREATE TABLE prodmult.MF_ValeFacturaAnulada (
	VALE				VARCHAR(20)			NOT NULL,
	FACTURA				VARCHAR(20)			NULL,
	FECHA_FACTURA		DATETIME			NULL,
	RecordDate			CurrentDateType		NOT NULL DEFAULT(GETDATE()),
	CreatedBy			UsernameType		NOT NULL,
	UpdatedBy			UsernameType		NULL,
	CreateDate			CurrentDateType		NOT NULL DEFAULT(GETDATE())
)
GO

ALTER TABLE prodmult.MF_ValeFacturaAnulada ADD PRIMARY KEY ( VALE )
GO
