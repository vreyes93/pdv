
CREATE procedure [prodmult].[spBalanceGeneral]
	@FechaInicial AS DATETIME, 
	@FechaFinal AS DATETIME,
	@Detalle  as char(1) = 'N',
	@Borrar as char(1) = 'S',
	@Empresa	as varchar(25) = 'prodmult'

as
begin
declare @SaldoInicial as DECIMAL(12,2) 
declare @SaldoFinal as DECIMAL(12,2) 
declare @SaldoFinalCalc as DECIMAL(12,2) 
declare @Creditos as DECIMAL(12,2) 
declare @Debitos as DECIMAL(12,2) 
declare @TipoCta	as varchar(1)
declare	@CtaDescripcion as VARCHAR(200) 
declare	@CtaMadre as varchar(50)
declare	@AceptaDatos as varchar(1)
declare	@Cta as varchar(50)
declare	@EstadoF as varchar(1)
declare	@Secuencial	as int
declare	@SaldoUtilidad DECIMAL(12,2) 	
declare	@SaldoUtilidadAnt DECIMAL(12,2) 	
declare	@SaldoActivo	DECIMAL(12,2) 	
declare	@SaldoActivoAnt	DECIMAL(12,2) 	
declare	@SaldoActivoCorriente	DECIMAL(12,2) 
declare	@SaldoActivoCorrienteAnt	DECIMAL(12,2) 
declare	@SaldoActivoNoCorriente	DECIMAL(12,2) 
declare	@SaldoActivoNoCorrienteAnt	DECIMAL(12,2) 
declare	@SaldoPasivo	DECIMAL(12,2) 	
declare	@SaldoPasivoAnt	DECIMAL(12,2) 	
declare	@SaldoPasivoCorriente	DECIMAL(12,2) 
declare	@SaldoPasivoCorrienteAnt	DECIMAL(12,2) 
declare	@SaldoPatrimonio	DECIMAL(12,2) 
declare	@SaldoPatrimonioAnt DECIMAL(12,2) 
declare	@SaldoIngresos DECIMAL(12,2) 
declare	@SaldoIngresosAnt DECIMAL(12,2) 
declare	@SaldoEgresos DECIMAL(12,2) 
declare	@SaldoEgresosAnt DECIMAL(12,2) 

DECLARE	@FechaIniAnt	datetime
DECLARE	@FechaFinAnt	datetime
	


create table #balance_general (
secuencial	int null,
fecha	datetime null,
cuenta_contable	varchar(25) null,
cuenta_descripcion varchar(50) null,
cuenta_padre	varchar(25) null,
saldo_actual	DECIMAL(12,2) null,
porcentaje_ventas_actual	DECIMAL(12,4) null,
saldo_anterior	DECIMAL(12,2) null,
porcentaje_ventas_anterior	DECIMAL(12,4) null,
variacion	DECIMAL(12,4) null,
Tipo	CHAR(1)
)
set	@Secuencial	 = 0
set	@SaldoActivo = 0
set	@SaldoActivoAnt = 0
SET @SaldoActivoCorriente= 0
SET @SaldoActivoCorrienteAnt= 0
SET @SaldoActivoNoCorriente= 0
SET @SaldoActivoNoCorrienteAnt= 0
set	@SaldoPasivo = 0
set	@SaldoPasivoAnt = 0
SET @SaldoPasivoCorriente= 0
SET @SaldoPasivoCorrienteAnt= 0
set	@SaldoPatrimonio = 0
set	@SaldoPatrimonioAnt = 0
SET @SaldoUtilidad  = 0
set @SaldoFinalCalc  = 0
SET @SaldoUtilidadAnt  = 0
set	@SaldoIngresos = 0
set	@SaldoIngresosAnt = 0
set	@SaldoEgresos = 0
set	@SaldoEgresosAnt = 0

set @FechaIniAnt = dateadd(yy,-1,@FechaInicial)
set @FechaFinAnt = dateadd(yy,-1,@FechaFinal)

if @Borrar = 'S'
begin
	delete	prodmult.MF_Balance_General 
	where	fecha = @FechaFinal

	-- ACTIVO CORRIENTE
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, '', 'ACTIVO', '', 0, 0, 0, 0, 0, 'R')
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, '', 'ACTIVO CORRIENTE', '', 0, 0, 0, 0, 0, 'R')

	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-1-1-001-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
	set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-1-1-002-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
	set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')
	
	-- 1-1-1-003-0000	Bancos Moneda Nacional
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-1-1-003-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
	--set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	SET @CtaMadre = @Cta 
	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	
	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre AND TIPO = 'B'
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set	@SaldoFinalCalc = 0

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

		set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
		set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
		set	@Secuencial	 = @Secuencial	 + 1
		insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'D')
			
		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	--1-1-2-001-0000	Cuentas por cobrar clientes
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-1-2-001-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
	set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	--1-1-2-002-0000	Anticipo y préstamos a empleados
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-1-2-002-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
	--set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	SET @CtaMadre = @Cta 
	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	
	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre AND TIPO = 'B'
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set	@SaldoFinalCalc = 0
		
		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		--select @Cta, @SaldoFinal , @SaldoFinalCalc
		
		set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
		set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
		set	@Secuencial	 = @Secuencial	 + 1
		insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'D')
			
		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	--1-1-2-003-0000	Otras Cuentas por Cobrar
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-1-2-003-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
	--set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	SET @CtaMadre = @Cta 
	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	
	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre AND TIPO = 'B'
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set	@SaldoFinalCalc = 0

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
--		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

		set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
		set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
		set	@Secuencial	 = @Secuencial	 + 1
		insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'D')
			
		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	
	--1-1-2-004-0000	Impuestos por cobrar
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-1-2-004-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
	--set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	SET @CtaMadre = @Cta 
	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	
	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre AND TIPO = 'B'
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set	@SaldoFinalCalc = 0

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

		set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
		set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
		set	@Secuencial	 = @Secuencial	 + 1
		insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'D')

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta


	--1-1-2-006-0000	Anticipos 
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-1-2-006-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
	--set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	SET @CtaMadre = @Cta 
	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	
	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre AND TIPO = 'B'
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set	@SaldoFinalCalc = 0

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

		set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
		set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
		set	@Secuencial	 = @Secuencial	 + 1
		insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'D')

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	--1-1-3-000-0000	Inventarios de Mercaderías
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-1-3-000-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
	--set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')
	
	SET @CtaMadre = @Cta 
	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	
	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre AND TIPO = 'B'
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set	@SaldoFinalCalc = 0

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

		set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
		set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
		set	@Secuencial	 = @Secuencial	 + 1
		insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'D')

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta


	-- GASTOS PAGADOS POR ANTICIPADO (1-1-4-001-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-1-4-001-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
	--set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	SET @CtaMadre = @Cta 
	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	
	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre AND TIPO = 'B'
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set	@SaldoFinalCalc = 0

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

		set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
		set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
		set	@Secuencial	 = @Secuencial	 + 1
		insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'D')

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	--1-1-4-004-0000	Gastos de Instalación
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-1-4-004-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
	--set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	SET @CtaMadre = @Cta 
	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	
	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre AND TIPO = 'B'
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set	@SaldoFinalCalc = 0

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

		set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
		set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
		set	@Secuencial	 = @Secuencial	 + 1
		insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'D')

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	--1-3-1-002-0002	Amortización Ac. Gastos de Instalación
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-3-1-002-0002'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set	@SaldoActivoCorriente = @SaldoActivoCorriente  + (@SaldoFinal*-1)
	set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc*-1
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal*-1, 0, @SaldoFinalCalc*-1, 0, 0, 'R')
	
	update	#balance_general
	set	saldo_actual = saldo_actual + (@SaldoFinal*-1)
	where cuenta_contable = '1-1-4-004-0000'
	and	FECHA = @FechaFinal

	-- 1-3-1-002-0003	Amortización Ac. Mejoras Edificios
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-3-1-002-0003'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set	@SaldoActivoCorriente = @SaldoActivoCorriente  + (@SaldoFinal *-1)
	set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + (@SaldoFinalCalc*-1)
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal*-1, 0, @SaldoFinalCalc*-1, 0, 0, 'R')
	
	update	#balance_general
	set	saldo_actual = saldo_actual + (@SaldoFinal*-1)
	where cuenta_contable = '1-1-4-004-0000'
	and	FECHA = @FechaFinal

	-- Total Activo Corriente
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, '', 'TOTAL ACTIVO CORRIENTE', '', @SaldoActivoCorriente , 0, @SaldoActivoCorrienteAnt, 0,  0, 'R')
	
	-- ACTIVO NO CORRIENTE
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, '', 'ACTIVO NO CORRIENTE', '', 0, 0, 0, 0,  0, 'R')
	set @SaldoActivoNoCorriente  = 0
	--1-2-1-001-0000	Mobiliario y Equipo de Oficina
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-2-1-001-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--set	@SaldoActivoCorriente = @SaldoActivoCorriente  + @SaldoFinal
	--set	@SaldoActivoCorrienteAnt = @SaldoActivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	SET @CtaMadre = @Cta 
	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	
	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre AND TIPO = 'B'
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set	@SaldoFinalCalc = 0

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

		set	@SaldoActivoNoCorriente = @SaldoActivoNoCorriente  + @SaldoFinal
		set	@SaldoActivoNoCorrienteAnt = @SaldoActivoNoCorrienteAnt  + @SaldoFinalCalc
		set	@Secuencial	 = @Secuencial	 + 1
		insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'D')

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta
	
	--1-2-1-002-0000	Mobiliario y Equipo de Computación
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-2-1-002-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set	@SaldoActivoNoCorriente = @SaldoActivoNoCorriente  + @SaldoFinal
	set	@SaldoActivoNoCorrienteAnt = @SaldoActivoNoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	--1-2-1-003-0000	Herramientas
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-2-1-003-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set	@SaldoActivoNoCorriente = @SaldoActivoNoCorriente  + @SaldoFinal
	set	@SaldoActivoNoCorrienteAnt = @SaldoActivoNoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	--1-2-1-004-0000	Maquinaria y Equipo
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-2-1-004-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set	@SaldoActivoNoCorriente = @SaldoActivoNoCorriente  + @SaldoFinal
	set	@SaldoActivoNoCorrienteAnt = @SaldoActivoNoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')
	
	-- 1-2-1-005-0000	Vehículos
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-2-1-005-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set	@SaldoActivoNoCorriente = @SaldoActivoNoCorriente  + @SaldoFinal
	set	@SaldoActivoNoCorrienteAnt = @SaldoActivoNoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	--1-2-1-009-0000	Depreciaciones Acumualdas
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-2-1-009-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--set	@SaldoActivoNoCorriente = @SaldoActivoNoCorriente  + @SaldoFinal
	--set	@SaldoActivoNoCorrienteAnt = @SaldoActivoNoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal*-1, 0, @SaldoFinalCalc*-1, 0, 0, 'R')

	SET @CtaMadre = @Cta 
	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	
	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre AND TIPO = 'B'
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set	@SaldoFinalCalc = 0

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

		set	@SaldoActivoNoCorriente = @SaldoActivoNoCorriente  + (@SaldoFinal*-1)
		set	@SaldoActivoNoCorrienteAnt = @SaldoActivoNoCorrienteAnt  + (@SaldoFinalCalc*-1)
		set	@Secuencial	 = @Secuencial	 + 1
		insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'D')
			
		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	--1-3-1-001-0000	Programas Licencias de Computación
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-3-1-001-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set	@SaldoActivoNoCorriente = @SaldoActivoNoCorriente  + (@SaldoFinal)
	set	@SaldoActivoNoCorrienteAnt = @SaldoActivoNoCorrienteAnt  + (@SaldoFinalCalc)
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')
	
	-- 1-3-1-002-0000	Amortizaciones
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-3-1-002-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--set	@SaldoActivoNoCorriente = @SaldoActivoNoCorriente  + @SaldoFinal
	--set	@SaldoActivoNoCorrienteAnt = @SaldoActivoNoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal*-1, 0, @SaldoFinalCalc*-1, 0, 0, 'R')

	-- 1-3-1-002-0000	Amortizaciones
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-3-1-002-0001'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set	@SaldoActivoNoCorriente = @SaldoActivoNoCorriente  + (@SaldoFinal*-1)
	set	@SaldoActivoNoCorrienteAnt = @SaldoActivoNoCorrienteAnt  + (@SaldoFinalCalc*-1)
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal*-1, 0, @SaldoFinalCalc*-1, 0, 0, 'R')

	update	#balance_general
	set	saldo_actual = (@SaldoFinal*-1)
	where cuenta_contable = '1-3-1-002-0000'
	and	FECHA = @FechaFinal

	--1-3-1-005-0000	Gastos por liquidar
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-3-1-005-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set	@SaldoActivoNoCorriente = @SaldoActivoNoCorriente  + @SaldoFinal
	set	@SaldoActivoNoCorrienteAnt = @SaldoActivoNoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')
	
	--1-4-1-001-0000	Depósitos en Garantía
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '1-4-1-001-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--set	@SaldoActivoNoCorriente = @SaldoActivoNoCorriente  + @SaldoFinal
	--set	@SaldoActivoNoCorrienteAnt = @SaldoActivoNoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	SET @CtaMadre = @Cta 
	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	
	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre AND TIPO = 'B'
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set	@SaldoFinalCalc = 0

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

		set	@SaldoActivoNoCorriente = @SaldoActivoNoCorriente  + @SaldoFinal
		set	@SaldoActivoNoCorrienteAnt = @SaldoActivoNoCorrienteAnt  + @SaldoFinalCalc
		set	@Secuencial	 = @Secuencial	 + 1
		insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'D')

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	-- Total Activo No Corriente
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, '', 'TOTAL ACTIVO NO CORRIENTE', '', @SaldoActivoNoCorriente , 0, @SaldoActivoNoCorrienteAnt , 0,  0, 'R')
	set	@SaldoActivo = @SaldoActivoNoCorriente + @SaldoActivoCorriente
	set	@SaldoActivoAnt = @SaldoActivoNoCorrienteAnt + @SaldoActivoCorrienteAnt
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, '', 'TOTAL ACTIVO ', '', @SaldoActivo , 0, @SaldoActivoAnt , 0,  0, 'R')

	UPDATE	#balance_general 
	SET		porcentaje_ventas_actual = saldo_actual/@SaldoActivo/*,
			porcentaje_ventas_anterior = saldo_anterior/@SaldoActivoAnt*/
	where	cuenta_contable like '1-%'
	and	FECHA = @FechaFinal
	
	-- PASIVO CORRIENTE
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, '', 'PASIVO CORRIENTE', '', 0, 0, 0, 0,  0, 'R')
	
	--2-1-1-001-0000	Proveedores
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '2-1-1-001-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--set	@SaldoPasivoCorriente = @SaldoPasivoCorriente  + @SaldoFinal
	--set	@SaldoPasivoCorrienteAnt = @SaldoPasivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	SET @CtaMadre = @Cta 
	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	
	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre AND TIPO = 'B'
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set	@SaldoFinalCalc = 0

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

		set	@SaldoPasivoCorriente = @SaldoPasivoCorriente  + @SaldoFinal
		set	@SaldoPasivoCorrienteAnt = @SaldoPasivoCorrienteAnt  + @SaldoFinalCalc
		set	@Secuencial	 = @Secuencial	 + 1
		insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'D')

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta


	--2-1-1-003-0000	Acreedore Varios
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '2-1-1-003-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--set	@SaldoPasivoCorriente = @SaldoPasivoCorriente  + @SaldoFinal
	--set	@SaldoPasivoCorrienteAnt = @SaldoPasivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	SET @CtaMadre = @Cta 
	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	
	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre AND TIPO = 'B'
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set	@SaldoFinalCalc = 0

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

		set	@SaldoPasivoCorriente = @SaldoPasivoCorriente  + @SaldoFinal
		set	@SaldoPasivoCorrienteAnt = @SaldoPasivoCorrienteAnt  + @SaldoFinalCalc
		set	@Secuencial	 = @Secuencial	 + 1
		insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'D')

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	--2-1-1-004-0000	Anticipos Recibidos
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '2-1-1-004-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--set	@SaldoPasivoCorriente = @SaldoPasivoCorriente  + @SaldoFinal
	--set	@SaldoPasivoCorrienteAnt = @SaldoPasivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	SET @CtaMadre = @Cta 
	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	
	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre AND TIPO = 'B'
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set	@SaldoFinalCalc = 0

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

		set	@SaldoPasivoCorriente = @SaldoPasivoCorriente  + @SaldoFinal
		set	@SaldoPasivoCorrienteAnt = @SaldoPasivoCorrienteAnt  + @SaldoFinalCalc
		set	@Secuencial	 = @Secuencial	 + 1
		insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'D')

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	--2-1-1-005-0000	Otras Cuentas y Documentos por pagar
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '2-1-1-005-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--set	@SaldoPasivoCorriente = @SaldoPasivoCorriente  + @SaldoFinal
	--set	@SaldoPasivoCorrienteAnt = @SaldoPasivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	SET @CtaMadre = @Cta 
	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	
	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre AND TIPO = 'B'
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set	@SaldoFinalCalc = 0

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

		set	@SaldoPasivoCorriente = @SaldoPasivoCorriente  + @SaldoFinal
		set	@SaldoPasivoCorrienteAnt = @SaldoPasivoCorrienteAnt  + @SaldoFinalCalc
		set	@Secuencial	 = @Secuencial	 + 1
		insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'D')

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	--2-1-2-000-0000	PRESTAMOS BANCARIOS C/P
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '2-1-2-000-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set	@SaldoPasivoCorriente = @SaldoPasivoCorriente  + @SaldoFinal
	set	@SaldoPasivoCorrienteAnt = @SaldoPasivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	--2-1-3-001-0000	Sueldos y Prestaciones por Pagar
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '2-1-3-001-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set	@SaldoPasivoCorriente = @SaldoPasivoCorriente  + @SaldoFinal
	set	@SaldoPasivoCorrienteAnt = @SaldoPasivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	--2-1-3-002-0000	Otras Cuentas por Pagar
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '2-1-3-002-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--set	@SaldoPasivoCorriente = @SaldoPasivoCorriente  + @SaldoFinal
	--set	@SaldoPasivoCorrienteAnt = @SaldoPasivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	SET @CtaMadre = @Cta 
	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	
	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre AND TIPO = 'B'
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set	@SaldoFinalCalc = 0

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

		set	@SaldoPasivoCorriente = @SaldoPasivoCorriente  + @SaldoFinal
		set	@SaldoPasivoCorrienteAnt = @SaldoPasivoCorrienteAnt  + @SaldoFinalCalc
		set	@Secuencial	 = @Secuencial	 + 1
		insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'D')

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	--2-1-3-003-0000	Impuestos y contribuciones por pagar
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '2-1-3-003-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set	@SaldoPasivoCorriente = @SaldoPasivoCorriente  + @SaldoFinal
	set	@SaldoPasivoCorrienteAnt = @SaldoPasivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	--2-1-3-004-0000	Impuesto Sobre La Renta
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '2-1-3-004-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--set	@SaldoPasivoCorriente = @SaldoPasivoCorriente  + @SaldoFinal
	--set	@SaldoPasivoCorrienteAnt = @SaldoPasivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')

	SET @CtaMadre = @Cta 
	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	
	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre AND TIPO = 'B'
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set	@SaldoFinalCalc = 0

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
--		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

		set	@SaldoPasivoCorriente = @SaldoPasivoCorriente  + @SaldoFinal
		set	@SaldoPasivoCorrienteAnt = @SaldoPasivoCorrienteAnt  + @SaldoFinalCalc
		set	@Secuencial	 = @Secuencial	 + 1
		insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'D')

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	--2-1-3-005-0000	Comisiones por pagar / aplicar (TC)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '2-1-3-005-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	--exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set	@SaldoPasivoCorriente = @SaldoPasivoCorriente  + @SaldoFinal
	set	@SaldoPasivoCorrienteAnt = @SaldoPasivoCorrienteAnt  + @SaldoFinalCalc
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')
	-- Total Pasivo No Corriente
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, '', 'TOTAL PASIVO CORRIENTE', '', @SaldoPasivoCorriente , 0, @SaldoPasivoCorrienteAnt , 0,  0, 'R')
	
	set @SaldoPasivo = @SaldoPasivoCorriente
	set @SaldoPasivoAnt = @SaldoPasivoCorrienteAnt

	-- PATRIMONIO
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, '', 'PATRIMONIO', '', 0, 0, 0, 0,  0, 'R')
	-- APORTE DE SOCIOS(3-1-1-001-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '3-1-1-001-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')
	set	@SaldoPatrimonio = @SaldoPatrimonio + @SaldoFinal
	set	@SaldoPatrimonioAnt = @SaldoPatrimonioAnt  + @SaldoFinalCalc
	
	-- RESERVA LEGAL (3-1-2-001-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '3-1-2-001-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')
	set	@SaldoPatrimonio = @SaldoPatrimonio + @SaldoFinal
	set	@SaldoPatrimonioAnt = @SaldoPatrimonioAnt  + @SaldoFinalCalc

	-- RESULTADOS DE EJERCICIOS ANTERIORES (3-1-2-002-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '3-1-2-002-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinal, 0, @SaldoFinalCalc, 0, 0, 'R')
	set	@SaldoPatrimonio = @SaldoPatrimonio + @SaldoFinal
	set	@SaldoPatrimonioAnt = @SaldoPatrimonioAnt  + @SaldoFinalCalc

	-- RESULTADOS DEL  EJERCICIO 
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '4-0-0-000-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set @SaldoIngresos = @SaldoFinal

	set @SaldoFinal = 0
	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set @SaldoIngresosAnt = @SaldoFinal

	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-0-0-000-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set @SaldoEgresos  = @SaldoFinal


	set @SaldoFinal = 0
	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIniAnt, @FechaFinal = @FechaFinAnt, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinalCalc output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set @SaldoEgresosAnt = @SaldoFinal

	set @SaldoUtilidad = @SaldoIngresos - @SaldoEgresos
	
	set @SaldoUtilidadAnt = @SaldoIngresosAnt - @SaldoEgresosAnt

	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, '', 'RESULTADO DEL EJERCICIO', '', @SaldoUtilidad, 0, @SaldoUtilidadAnt, 0,  0, 'R')
	
	set	@SaldoPatrimonio = @SaldoPatrimonio + @SaldoUtilidad
	set	@SaldoPatrimonioAnt = @SaldoPatrimonioAnt  + @SaldoUtilidadAnt 
	
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, '', 'TOTAL PATRIMONIO', '', @SaldoPatrimonio, 0, @SaldoPatrimonioAnt , 0,  0, 'R')
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #balance_general values (@Secuencial	, @FechaFinal, '', 'TOTAL PASIVO MÁS PATRIMONIO', '', @SaldoPasivo + @SaldoPatrimonio , 0, @SaldoPasivoAnt + @SaldoPatrimonioAnt, 0,  0, 'R')
	
	
	UPDATE	#balance_general 
	SET		porcentaje_ventas_actual = saldo_actual/(@SaldoPasivo+@SaldoPatrimonio)/*,
			porcentaje_ventas_anterior = saldo_anterior/(@SaldoPasivoAnt+@SaldoPatrimonioAnt)*/
	where	(cuenta_contable like '2-%' or cuenta_contable like '3-%')
	and	FECHA = @FechaFinal
	
	
	UPDATE	#balance_general 
	SET		variacion = saldo_actual - saldo_anterior
	where	FECHA = @FechaFinal
	
	insert into prodmult.MF_Balance_General 
	select @Empresa, * from #balance_general 
end

select	SECUENCIAL = secuencial, FECHA = fecha, CUENTA_CONTABLE =  cuenta_contable, CUENTA_NOMBRE = cuenta_descripcion, SALDO_FINAL = saldo_actual, '% VENTAS' = porcentaje_ventas_actual * 100, SALDO_ANTERIOR = saldo_anterior, '% VENTAS ANTERIOR' = porcentaje_ventas_anterior * 100, Tipo 
from	prodmult.MF_Balance_General 
where	fecha = @FechaFinal
AND	EMPRESA = @Empresa
order	by SECUENCIAL
end

GO



GRANT EXECUTE ON prodmult.spBalanceGeneral TO [REPORTEADOR]
GO
GRANT EXECUTE ON prodmult.spBalanceGeneral TO [FIESTA\rolando.pineda]
GO
GRANT EXECUTE ON prodmult.spBalanceGeneral TO [FIESTA\william.quiacain]
GO
GRANT EXECUTE ON prodmult.spBalanceGeneral TO [FIESTA\mike]
GO
GRANT EXECUTE ON prodmult.spBalanceGeneral TO [mf.fiestanet]
GO
GRANT EXECUTE ON prodmult.spBalanceGeneral TO [FIESTA\eder.hulse]
GO

