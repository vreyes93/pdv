
CREATE procedure [prodmult].[spEstadoResultados]
	@FechaInicial AS DATETIME, 
	@FechaFinal AS DATETIME,
	@Detalle  as char(1) = 'N',
	@Borrar as char(1) = 'N',
	@Empresa	as varchar(25) = 'prodmult'
as
begin
declare @SaldoInicial as DECIMAL(12,2) 
declare @SaldoFinal as DECIMAL(12,2) 
declare @SaldoFinalCalc as DECIMAL(12,2) 
declare @Creditos as DECIMAL(12,2) 
declare @Debitos as DECIMAL(12,2) 
declare @TipoCta	as varchar(1)
declare	@CtaDescripcion as VARCHAR(200) 
declare	@CtaMadre as varchar(50)
declare	@AceptaDatos as varchar(1)
declare	@Cta as varchar(50)
declare	@EstadoF as varchar(1)
declare	@Secuencial	as int
declare	@SaldoVentas as DECIMAL(12,2) 	
declare	@SaldoCostoVentas as DECIMAL(12,2) 	
declare	@SaldoSubTotal as DECIMAL(12,2) 	
declare	@SaldoMargenBruto as DECIMAL(12,2) 	
declare	@SaldoIngVentas as DECIMAL(12,2) 	
declare	@SaldoGastosVentas as DECIMAL(12,2) 	
declare	@SaldoGastosAdmon DECIMAL(12,2) 	
declare	@SaldoGastosFin DECIMAL(12,2) 	
declare	@SaldoUtiOperacion DECIMAL(12,2) 	
declare	@SaldoGastosNoDeduc DECIMAL(12,2) 	
declare	@SaldoUtilidad DECIMAL(12,2) 	

declare	@SaldoVentasAcum as DECIMAL(12,2) 	
declare	@SaldoCostoVentasAcum as DECIMAL(12,2) 	
declare	@SaldoSubTotalAcum as DECIMAL(12,2) 	
declare	@SaldoMargenBrutoAcum as DECIMAL(12,2) 	
declare	@SaldoIngVentasAcum as DECIMAL(12,2) 	
declare	@SaldoGastosVentasAcum as DECIMAL(12,2) 	
declare	@SaldoGastosAdmonAcum DECIMAL(12,2) 	
declare	@SaldoGastosFinAcum DECIMAL(12,2) 	
declare	@SaldoUtiOperacionAcum DECIMAL(12,2) 	
declare	@SaldoGastosNoDeducAcum DECIMAL(12,2) 	
declare	@SaldoUtilidadAcum DECIMAL(12,2) 	


create table #estado_resultados (
secuencial	int,
fecha	datetime,
cuenta_contable	varchar(25),
cuenta_descripcion varchar(50),
cuenta_padre	varchar(25),
saldo_actual	DECIMAL(12,2) ,
porcentaje_ventas_actual	DECIMAL(12,4) ,
saldo_acumulado	DECIMAL(12,2) ,
porcentaje_ventas_acumulado DECIMAL(12,4) ,
variacion	DECIMAL(12,4),
Tipo	CHAR(1)
)
set	@Secuencial	 = 0
set @SaldoMargenBruto = 0
set	@SaldoIngVentas = 0
SET @SaldoGastosFin = 0
SET @SaldoUtiOperacion = 0
SET @SaldoUtilidad  = 0
set @SaldoGastosNoDeduc  = 0
set @SaldoFinalCalc  = 0
set @SaldoMargenBrutoAcum = 0
set	@SaldoIngVentasAcum = 0
SET @SaldoGastosFinAcum = 0
SET @SaldoUtiOperacionAcum = 0
SET @SaldoUtilidadAcum  = 0
set @SaldoGastosNoDeducAcum  = 0
set	@SaldoVentasAcum   = 0

if @Borrar = 'S'
begin
	delete	prodmult.MF_Estado_Resultado
	where	FECHA = @FechaFinal
		
-- Ingreso Por Ventas (4-1-1-001-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '4-1-1-001-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set @SaldoFinalCalc  = @Creditos - @Debitos
	set @SaldoVentas  = @SaldoFinalCalc  
	set @SaldoVentasAcum  = @SaldoFinal
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, 0, @SaldoFinal, 0, 0, 'R')

-- DEVOLUCIONES REBAJAS DESC.S/V (4-1-1-003-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '4-1-1-003-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set @SaldoFinalCalc  = @Creditos - @Debitos
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc*-1, 0, @SaldoFinal*-1, 0, 0, 'R')

-- Ventas Netas
	set @SaldoVentas  = @SaldoVentas  - @SaldoFinalCalc  
	set @SaldoVentasAcum  = @SaldoVentasAcum  - @SaldoFinal
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, '', 'Ventas Netas', '', @SaldoVentas , 1, @SaldoVentasAcum,  1, 0, 'R')
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, '', 'Costo de Ventas ', '', 0, 0, 0, 0, 0, 'R')


-- Compras (5-1-1-001-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-1-1-001-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set @SaldoFinalCalc  = @Creditos - @Debitos
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, 0, @SaldoFinal, 0, 0, 'R')
	set @SaldoCostoVentas = @SaldoFinalCalc
	set @SaldoCostoVentasAcum = @SaldoFinal


-- DEV. Y REBAJAS SOBRE COMPRAS (5-1-1-002-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-1-1-002-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set @SaldoFinalCalc  = @Creditos - @Debitos
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc*-1, 0, @SaldoFinal*-1, 0, 0, 'R')
	set @SaldoCostoVentas = @SaldoCostoVentas - @SaldoFinalCalc
	set @SaldoCostoVentasAcum = @SaldoCostoVentasAcum - @SaldoFinal
	
	set @SaldoMargenBruto = @SaldoVentas - @SaldoCostoVentas
	set @SaldoMargenBrutoAcum = @SaldoVentasAcum - @SaldoCostoVentasAcum

	-- Costo Ventas Neto
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, '', 'Costo de Ventas Neto', '', @SaldoCostoVentas , @SaldoCostoVentas/@SaldoVentas, @SaldoCostoVentasAcum, @SaldoCostoVentasAcum/@SaldoVentasAcum,  0, 'R')
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, '', 'Margen Bruto ', '', @SaldoMargenBruto , @SaldoMargenBruto/@SaldoVentas, @SaldoMargenBrutoAcum, @SaldoMargenBrutoAcum/@SaldoVentasAcum, 0, 'R')

	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, '', 'Ventas de Servicios y Otros Ingrsos', '', 0, 0, 0, 0, 0, 'R')


	-- SERVICIOS (4-1-3-001-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '4-1-3-001-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set @SaldoFinalCalc  = @Creditos - @Debitos
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, 0, @SaldoFinal, 0, 0, 'R')
	set @SaldoSubTotal = @SaldoFinalCalc
	set @SaldoSubTotalAcum = @SaldoFinal
	
	
	-- INTERESES Y COMISIONES DEVENGADAS (4-2-1-001-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '4-2-1-001-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set @SaldoFinalCalc  = @Creditos - @Debitos
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, 0, @SaldoFinal, 0, 0, 'R')
	set @SaldoSubTotal = @SaldoSubTotal  + @SaldoFinalCalc
	set @SaldoSubTotalAcum = @SaldoSubTotalAcum + @SaldoFinal

	-- OTROS INGRESOS (4-5-1-001-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '4-5-1-001-0000'
	
	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set @SaldoFinalCalc  = @Creditos - @Debitos
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, 0, @SaldoFinal, 0, 0, 'R')
	set @SaldoSubTotal = @SaldoSubTotal  + @SaldoFinalCalc
	set @SaldoSubTotalAcum = @SaldoSubTotalAcum + @SaldoFinal

	set @SaldoIngVentas = @SaldoMargenBruto+@SaldoSubTotal 
	set @SaldoIngVentasAcum = @SaldoMargenBrutoAcum+@SaldoSubTotalAcum 
	
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, '', 'Total Ingresos Deducido Costo de Ventas', '', @SaldoMargenBruto+@SaldoSubTotal , (@SaldoMargenBruto+@SaldoSubTotal)/@SaldoVentas, @SaldoMargenBrutoAcum+@SaldoSubTotalAcum, (@SaldoMargenBrutoAcum+@SaldoSubTotalAcum)/@SaldoVentasAcum, 0, 'R')
	
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, '', 'Costo de Operacion', '', 0, 0, 0, 0, 0, 'R')
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, '', 'Gastos de Ventas', '', 0, 0, 0, 0, 0, 'R')

	set	@SaldoGastosVentas= 0
	set @SaldoGastosVentasAcum = 0

	-- SUELDOS Y PRESTACIONES(5-2-1-001-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-2-1-001-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	set @SaldoGastosVentas = @SaldoGastosVentas+ @SaldoFinalCalc
	set @SaldoGastosVentasAcum = @SaldoGastosVentasAcum+ @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos

		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	-- VIATICOS Y GASTOS DE VIAJE(5-2-1-002-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-2-1-002-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	set @SaldoGastosVentas = @SaldoGastosVentas+ @SaldoFinalCalc
	set @SaldoGastosVentasAcum = @SaldoGastosVentasAcum+ @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta


	-- SERVICIOS RECIBIDOS(5-2-1-003-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-2-1-003-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	set @SaldoGastosVentas = @SaldoGastosVentas+ @SaldoFinalCalc
	set @SaldoGastosVentasAcum = @SaldoGastosVentasAcum+ @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta


	-- MATERIALES Y SUMINISTROS(5-2-1-004-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-2-1-004-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	set @SaldoGastosVentas = @SaldoGastosVentas+ @SaldoFinalCalc
	set @SaldoGastosVentasAcum = @SaldoGastosVentasAcum+ @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	-- HONORARIOS Y SERVICIOS(5-2-1-006-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-2-1-006-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	set @SaldoGastosVentas = @SaldoGastosVentas+ @SaldoFinalCalc
	set @SaldoGastosVentasAcum = @SaldoGastosVentasAcum+ @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	-- IMPUESTOS FISCALES Y MUNICIPALES (5-2-1-007-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-2-1-007-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	set @SaldoGastosVentas = @SaldoGastosVentas+ @SaldoFinalCalc
	set @SaldoGastosVentasAcum = @SaldoGastosVentasAcum+ @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	-- REPARACIONES Y MANTENIMIENTO(5-2-1-008-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-2-1-008-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	set @SaldoGastosVentas = @SaldoGastosVentas+ @SaldoFinalCalc
	set @SaldoGastosVentasAcum = @SaldoGastosVentasAcum+ @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	-- DEPRECIACIONES Y AMORTIZACIONES(5-2-1-009-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-2-1-009-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	set @SaldoGastosVentas = @SaldoGastosVentas+ @SaldoFinalCalc
	set @SaldoGastosVentasAcum = @SaldoGastosVentasAcum+ @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	-- COMPRA EQUIPO COMPUTO y MOBILIARIO(5-2-1-010-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-2-1-010-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	set @SaldoGastosVentas = @SaldoGastosVentas+ @SaldoFinalCalc
	set @SaldoGastosVentasAcum = @SaldoGastosVentasAcum+ @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta


	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, '', 'Subtotal Gastos de Ventas', '', @SaldoGastosVentas  , @SaldoGastosVentas/@SaldoVentas, @SaldoGastosVentasAcum, @SaldoGastosVentasAcum/@SaldoVentasAcum, 0, 'R')
	
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, '', 'Resultado en Gastos de Ventas', '', (@SaldoIngVentas-@SaldoGastosVentas), (@SaldoIngVentas-@SaldoGastosVentas )/@SaldoVentas, (@SaldoIngVentasAcum-@SaldoGastosVentasAcum), (@SaldoIngVentasAcum-@SaldoGastosVentasAcum)/@SaldoVentasAcum, 0, 'R')
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, '', 'Gastos de Administración', '', 0, 0, 0, 0, 0, 'R')

	set	@SaldoGastosAdmon = 0
	set	@SaldoGastosAdmonAcum = 0

	-- SUELDOS Y PRESTACIONES(5-2-3-001-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-2-3-001-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	set @SaldoGastosAdmon = @SaldoGastosAdmon + @SaldoFinalCalc
	set @SaldoGastosAdmonAcum = @SaldoGastosAdmonAcum + @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	-- VIATICOS Y GASTOS DE VIAJE(5-2-3-002-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-2-3-002-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	set @SaldoGastosAdmon = @SaldoGastosAdmon + @SaldoFinalCalc
	set @SaldoGastosAdmonAcum = @SaldoGastosAdmonAcum + @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	-- SERVICIOS RECIBIDOS(5-2-3-003-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-2-3-003-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	set @SaldoGastosAdmon = @SaldoGastosAdmon + @SaldoFinalCalc
	set @SaldoGastosAdmonAcum = @SaldoGastosAdmonAcum + @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	-- MATERIALES Y SUMINISTROS(5-2-3-004-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-2-3-004-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	set @SaldoGastosAdmon = @SaldoGastosAdmon + @SaldoFinalCalc
	set @SaldoGastosAdmonAcum = @SaldoGastosAdmonAcum + @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	-- HONORARIOS Y SERVICIOS(5-2-3-006-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-2-3-006-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	set @SaldoGastosAdmon = @SaldoGastosAdmon + @SaldoFinalCalc
	set @SaldoGastosAdmonAcum = @SaldoGastosAdmonAcum + @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	-- IMPUESTOS FISCALES Y MUNICIPALES(5-2-3-007-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-2-3-007-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	set @SaldoGastosAdmon = @SaldoGastosAdmon + @SaldoFinalCalc
	set @SaldoGastosAdmonAcum = @SaldoGastosAdmonAcum + @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	-- REPARACIONES Y MANTENIMIENTO(5-2-3-008-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-2-3-008-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	set @SaldoGastosAdmon = @SaldoGastosAdmon + @SaldoFinalCalc
	set @SaldoGastosAdmonAcum = @SaldoGastosAdmonAcum + @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	-- DEPRECIACIONES Y AMORTIZACIONES(5-2-3-009-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-2-3-009-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	set @SaldoGastosAdmon = @SaldoGastosAdmon + @SaldoFinalCalc
	set @SaldoGastosAdmonAcum = @SaldoGastosAdmonAcum + @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	-- COMPRA EQUIPO Y HERRAMIENTA GASTO(5-2-3-010-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-2-3-010-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	set @SaldoGastosAdmon = @SaldoGastosAdmon + @SaldoFinalCalc
	set @SaldoGastosAdmonAcum = @SaldoGastosAdmonAcum + @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, '', 'Subtotal Gastos de Administración', '', @SaldoGastosAdmon  , @SaldoGastosAdmon/@SaldoVentas, @SaldoGastosAdmonAcum, @SaldoGastosAdmonAcum/@SaldoVentasAcum, 0, 'R')
	
	--select	@SaldoIngVentasAcum, @SaldoGastosVentasAcum, @SaldoGastosAdmonAcum

	SET @SaldoUtiOperacion = @SaldoIngVentas-@SaldoGastosVentas-@SaldoGastosAdmon
	SET @SaldoUtiOperacionAcum = @SaldoIngVentasAcum-@SaldoGastosVentasAcum-@SaldoGastosAdmonAcum

	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, '', 'Resultado en Operación', '', @SaldoUtiOperacion , @SaldoUtiOperacion/@SaldoVentas, @SaldoUtiOperacionAcum, @SaldoUtiOperacionAcum/@SaldoVentasAcum, 0, 'R')
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, '', 'Gastos Financieros', '', 0, 0, 0, 0, 0, 'R')

	

	SET @SaldoGastosFin = 0
	SET @SaldoUtilidad  = 0
	SET @SaldoGastosFinAcum = 0
	SET @SaldoUtilidadAcum  = 0

	-- INTERESES Y GASTOS BANCARIOS(5-3-1-001-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-3-1-001-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	SET @SaldoGastosFin = @SaldoGastosFin + @SaldoFinalCalc
	SET @SaldoGastosFinAcum = @SaldoGastosFinAcum + @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	-- GASTOS DE SEGUROS (5-3-1-002-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-3-1-002-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	SET @SaldoGastosFin = @SaldoGastosFin + @SaldoFinalCalc
	SET @SaldoGastosFinAcum = @SaldoGastosFinAcum + @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta


	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, '', 'Subtotal Gastos Financieros', '', @SaldoGastosFin , @SaldoGastosFin/@SaldoVentas, @SaldoGastosFinAcum, @SaldoGastosFinAcum/@SaldoVentasAcum, 0, 'R')
	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, '', 'Gastos No Deducibles', '', 0, 0, 0, 0, 0, 'R')

	set @SaldoGastosNoDeduc  = 0
	set @SaldoGastosNoDeducAcum  = 0

	-- OTROS EGRESOS(5-5-1-001-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-5-1-001-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	SET @SaldoGastosNoDeduc   = @SaldoGastosNoDeduc  + @SaldoFinalCalc
	SET @SaldoGastosNoDeducAcum   = @SaldoGastosNoDeducAcum  + @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta

	-- MULTAS Y RECARGOS (5-5-1-002-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '5-5-1-002-0000'

	exec prodmult.spDatosCuenta @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

	set	@Secuencial	 = @Secuencial	 + 1
	set @SaldoFinalCalc  = @Creditos - @Debitos
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'R')
	SET @SaldoGastosNoDeduc   = @SaldoGastosNoDeduc  + @SaldoFinalCalc
	SET @SaldoGastosNoDeducAcum   = @SaldoGastosNoDeducAcum  + @SaldoFinal

	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @Cta, @CtaDescripcion

	WHILE @@FETCH_STATUS = 0 and @Detalle   = 'S'
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
	
		exec prodmult.spDatosCuenta  @Cuenta = @Cta, @FechaInicial  = @FechaInicial , @FechaFinal = @FechaFinal, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output

		set	@Secuencial	 = @Secuencial	 + 1
		set @SaldoFinalCalc  = @Creditos - @Debitos
		if (@CtaDescripcion != '.' and  @CtaDescripcion != '')
			insert into #estado_resultados values (@Secuencial	, @FechaFinal, @Cta , @CtaDescripcion , @CtaMadre , @SaldoFinalCalc, @SaldoFinalCalc/@SaldoVentas, @SaldoFinal, @SaldoFinal/@SaldoVentasAcum, 0, 'D')

		SET @CtaDescripcion = ''

		FETCH NEXT FROM #ctas_conta INTO @Cta , @CtaDescripcion
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta


	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, '', 'Subtotal Gastos No Deducibles', '', @SaldoGastosNoDeduc, @SaldoGastosNoDeduc/@SaldoVentas, @SaldoGastosNoDeducAcum, @SaldoGastosNoDeducAcum/@SaldoVentasAcum, 0, 'R')

	SET @SaldoUtilidad  = @SaldoUtiOperacion - @SaldoGastosFin - @SaldoGastosNoDeduc
	SET @SaldoUtilidadAcum  = @SaldoUtiOperacionAcum - @SaldoGastosFinAcum - @SaldoGastosNoDeducAcum

	set	@Secuencial	 = @Secuencial	 + 1
	insert into #estado_resultados values (@Secuencial	, @FechaFinal, '', 'Resultado del Ejercicio', '', @SaldoUtilidad, @SaldoUtilidad/@SaldoVentas, @SaldoUtilidadAcum, @SaldoUtilidadAcum/@SaldoVentasAcum, 0, 'R')	

	update	#estado_resultados 
	set	variacion = saldo_actual - saldo_acumulado

	insert into prodmult.MF_Estado_Resultado
	select @Empresa, * from #estado_resultados 

	select	SECUENCIAL = secuencial, FECHA = fecha, CUENTA_CONTABLE =  cuenta_contable, CUENTA_NOMBRE = cuenta_descripcion, SALDO_FINAL = saldo_actual, '% VENTAS' = porcentaje_ventas_actual * 100, SALDO_ACUMULADO = saldo_acumulado, '% VENTAS ACUMULADO' = porcentaje_ventas_acumulado * 100, Cuenta_Padre AS CuentaMadre, Tipo
	from	prodmult.MF_Estado_Resultado
	where	fecha = @FechaFinal
	AND		EMPRESA = @Empresa
	order	by SECUENCIAL
	/*select	SECUENCIAL = secuencial, FECHA = CONVERT(VARCHAR, fecha,102), CUENTA_CONTABLE =  cuenta_contable, CUENTA_NOMBRE = cuenta_descripcion, SALDO_FINAL = saldo_actual, '% VENTAS' = porcentaje_ventas_actual, SALDO_ACUMULADO = saldo_acumulado, '% VENTAS ACUMULADO' = porcentaje_ventas_acumulado
	from #estado_resultados */
end
else
begin

select	SECUENCIAL = secuencial, FECHA = fecha, CUENTA_CONTABLE =  cuenta_contable, CUENTA_NOMBRE = cuenta_descripcion, SALDO_FINAL = saldo_actual, '% VENTAS' = porcentaje_ventas_actual * 100, SALDO_ACUMULADO = saldo_acumulado, '% VENTAS ACUMULADO' = porcentaje_ventas_acumulado * 100, Cuenta_Padre AS CuentaMadre, Tipo
from	prodmult.MF_Estado_Resultado
where	fecha = @FechaFinal
AND		EMPRESA = @Empresa
order	by SECUENCIAL
end
end

GO


GRANT EXECUTE ON prodmult.spEstadoResultados TO [REPORTEADOR]
GO
GRANT EXECUTE ON prodmult.spEstadoResultados TO [FIESTA\rolando.pineda]
GO
GRANT EXECUTE ON prodmult.spEstadoResultados TO [FIESTA\william.quiacain]
GO
GRANT EXECUTE ON prodmult.spEstadoResultados TO [FIESTA\mike]
GO
GRANT EXECUTE ON prodmult.spEstadoResultados TO [mf.fiestanet]
GO
GRANT EXECUTE ON prodmult.spEstadoResultados TO [FIESTA\eder.hulse]
GO

