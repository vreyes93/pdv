DROP PROCEDURE prodmult.spFormasPago
GO

CREATE PROCEDURE prodmult.spFormasPago
AS
BEGIN

DECLARE @FechaString AS VARCHAR(20) = CONVERT(VARCHAR(4), YEAR(GETDATE())) + '-' + CONVERT(VARCHAR(2), MONTH(GETDATE())) + '-' + CONVERT(VARCHAR(2), DAY(GETDATE()))
DECLARE @FechaFinal AS DATETIME = '' + @FechaString + ''

DECLARE @FechaInicial AS DATETIME = '' + CONVERT(VARCHAR(4), YEAR(GETDATE())) + '-01-01'

IF (MONTH(@FechaFinal) = 1 AND DAY(@FechaFinal) <= 5)
BEGIN
	SET @FechaInicial = '' + CONVERT(VARCHAR(4), YEAR(GETDATE()) - 1) + '-01-01'
	SET @FechaFinal = '' + CONVERT(VARCHAR(4), YEAR(GETDATE()) - 1) + '-12-31'
END

SELECT a.COBRADOR AS Tienda, c.NOMBRE AS Vendedor, YEAR(a.FECHA) AS Anio, MONTH(a.FECHA) AS Mes, DAY(a.FECHA) AS Dia, -- CONVERT(VARCHAR(4), YEAR(a.FECHA)) + '-' + CONVERT(VARCHAR(2), MONTH(a.FECHA)) AS Mes, 
b.NIVEL_PRECIO AS FormaPago, COUNT(a.FACTURA) AS Ventas, SUM(a.TOTAL_FACTURA) AS Monto, 
(CASE b.NIVEL_PRECIO WHEN 'ContadoCheq' THEN 'Contado' WHEN 'ContadoEfect' THEN 'Contado' WHEN 'TarjetaVisa' THEN 'Contado' WHEN 'TarjetaCredo' THEN 'Contado' ELSE d.Nombre END) AS Financiera 
FROM prodmult.FACTURA a JOIN prodmult.MF_Factura b ON a.FACTURA = b.FACTURA
JOIN prodmult.VENDEDOR c ON a.VENDEDOR = c.VENDEDOR JOIN prodmult.MF_Financiera d ON b.FINANCIERA = d.Financiera
WHERE a.COBRADOR <> 'F01' AND a.ANULADA = 'N' AND a.VENDEDOR NOT IN ( '0107', '0002' )
AND a.FECHA BETWEEN @FechaInicial AND @FechaFinal
GROUP BY a.COBRADOR, c.NOMBRE, b.NIVEL_PRECIO, YEAR(a.FECHA), MONTH(a.FECHA), DAY(a.FECHA), d.Nombre --CONVERT(VARCHAR(4), YEAR(a.FECHA)) + '-' + CONVERT(VARCHAR(2), MONTH(a.FECHA))
ORDER BY a.COBRADOR, c.NOMBRE, Mes, FormaPago

END

GO


GRANT EXECUTE ON prodmult.spFormasPago TO [REPORTEADOR]
GO
GRANT EXECUTE ON prodmult.spFormasPago TO [FIESTA\rolando.pineda]
GO
GRANT EXECUTE ON prodmult.spFormasPago TO [FIESTA\william.quiacain]
GO
GRANT EXECUTE ON prodmult.spFormasPago TO [FIESTA\mike]
GO
GRANT EXECUTE ON prodmult.spFormasPago TO [mf.fiestanet]
GO
GRANT EXECUTE ON prodmult.spFormasPago TO [FIESTA\eder.hulse]
GO
