--DROP PROCEDURE prodmult.sp_ERTienda
--GO

CREATE PROCEDURE prodmult.sp_ERTienda
	@FechaInicial AS DATETIME, 
	@FechaFinal AS DATETIME,
	@Acumulado as char(1) = 'N',
	@Borrar as char(1) = 'N',
	@Empresa	as varchar(25) = 'prodmult'
as
begin

	--DECLARE @FechaInicial AS DATETIME = '2017-07-01'
	--DECLARE @FechaFinal AS DATETIME = '2017-07-31'
	--DECLARE @Acumulado AS CHAR(1) = 'N'
	--DECLARE @Borrar AS CHAR(1) = 'S'
	--DECLARE @Empresa AS VARCHAR(20) = 'prodmult'

	declare	@Secuencial	as int
	declare	@SaldoVentas as DECIMAL(12,2) 	
	declare	@SaldoVentasTiendas as DECIMAL(12,2) 	
	declare	@SaldoVentasTiendasAcum as DECIMAL(12,2) 	
	declare	@SaldoVentasTiendasMay as DECIMAL(12,2) 	
	declare	@SaldoVentasTiendasMayAcum as DECIMAL(12,2) 	
	declare	@SaldoVentasMay as DECIMAL(12,2) 	
	declare	@SaldoVentasMayAcum as DECIMAL(12,2) 	
	declare	@SaldoVentasAdmon as DECIMAL(12,2) 	
	declare	@SaldoVentasAdmonAcum as DECIMAL(12,2) 	
	declare	@SaldoVentasTotal as DECIMAL(12,2) 	
	declare	@SaldoVentasTotalAcum as DECIMAL(12,2) 	
	declare	@SaldoCostoVentas as DECIMAL(12,2) 	
	declare	@SaldoCostoVentasAdmon as DECIMAL(12,2) 	
	declare	@SaldoCostoVentasGer as DECIMAL(12,2) 	
	declare	@SaldoCostoVentasAdmonAcum as DECIMAL(12,2) 	
	declare	@SaldoCostoVentasGerAcum as DECIMAL(12,2) 	
	declare	@SaldoSubTotal as DECIMAL(12,2) 	
	declare	@SaldoMargenBruto as DECIMAL(12,2) 	
	declare	@SaldoIngVentas as DECIMAL(12,2) 	
	declare	@SaldoGastosVentas as DECIMAL(12,2) 	
	declare	@SaldoGastosVentasAdmon as DECIMAL(12,2) 	
	declare	@SaldoGastosVentasAdmonAcum as DECIMAL(12,2) 	
	declare	@SaldoGastosVentasGer as DECIMAL(12,2) 	
	declare	@SaldoGastosVentasGerAcum as DECIMAL(12,2) 	

	declare	@SaldoGastosaDist as DECIMAL(12,2) 	
	declare	@SaldoGastosaDistAcum as DECIMAL(12,2) 	

	declare	@SaldoGastosAdmon DECIMAL(12,2) 	
	declare	@SaldoGastosFin DECIMAL(12,2) 	
	declare	@SaldoUtiOperacion DECIMAL(12,2) 	
	declare	@SaldoGastosNoDeduc DECIMAL(12,2) 	
	declare	@SaldoUtilidad DECIMAL(12,2) 	
	declare	@SaldoPorcRPosDist as DECIMAL(12,2) 	
	declare	@SaldoVentasAcum as DECIMAL(12,2) 	
	declare	@SaldoCostoVentasAcum as DECIMAL(12,2) 	
	declare	@SaldoSubTotalAcum as DECIMAL(12,2) 	
	declare	@SaldoMargenBrutoAcum as DECIMAL(12,2) 	
	declare	@SaldoIngVentasAcum as DECIMAL(12,2) 	
	declare	@SaldoGastosVentasAcum as DECIMAL(12,2)
	declare	@SaldoResultPreDist	DECIMAL(12,2) 
	declare	@SaldoResultPosDist DECIMAL(12,2)
	declare	@SaldoResultPreDistAcum	DECIMAL(12,2) 
	declare	@SaldoResultPosDistAcum DECIMAL(12,2)
	declare	@SaldoResultPosDistTotal DECIMAL(12,2)
 	
	declare	@SaldoGastosAdmonAcum DECIMAL(12,2) 	
	declare	@SaldoGastosFinAcum DECIMAL(12,2) 	
	declare	@SaldoUtiOperacionAcum DECIMAL(12,2) 	
	declare	@SaldoGastosNoDeducAcum DECIMAL(12,2) 	
	declare	@SaldoUtilidadAcum DECIMAL(12,2) 	

	declare	@PorcVentasTiendas as DECIMAL(12,4) 	
	declare	@PorcVentasTiendasMay as DECIMAL(12,4) 	


	declare @SaldoInicial as DECIMAL(12,2) 
	declare @SaldoFinal as DECIMAL(12,2) 
	declare @SaldoFinalCalc as DECIMAL(12,2) 
	declare @Creditos as DECIMAL(12,2) 
	declare @Debitos as DECIMAL(12,2) 
	declare @TipoCta	as varchar(1)
	declare @SignoCta	as varchar(1)
	declare	@CtaDescripcion as VARCHAR(200) 
	declare	@CtaMadre as varchar(50)
	declare	@CtaHija as varchar(50)
	declare	@AceptaDatos as varchar(1)
	declare	@SubCta1 as varchar(5)
	declare	@SubCta2 as varchar(3)
	declare	@SubCta3 as varchar(4)
	declare	@Cta as varchar(50)
	declare	@EstadoF as varchar(1)
	declare	@Tienda	as varchar(5)
	declare	@NombreTienda as varchar(100)
	declare	@FechaIni AS DATETIME
	DECLARE	@FechaFin AS DATETIME

	set	@Debitos = 0
	set	@Creditos = 0
	set	@SaldoInicial = 0
	set	@TipoCta = 'X'
	set	@CtaDescripcion = ''
	set	@SaldoFinal = 0
	set	@Tienda = ''
	set	@CtaHija = ''
	set	@NombreTienda = ''
	SET	@FechaIni = @FechaInicial 
	set	@FechaFin = @FechaFinal 
	set	@SaldoResultPreDist	= 0
	set	@SaldoResultPosDist = 0
	set	@SaldoGastosVentas = 0
	set	@SaldoVentasTotal = 0
	set	@SaldoVentasTotalAcum = 0
	set	@Secuencial	 = 0
	set	@SaldoPorcRPosDist  = 0
	set	@SaldoResultPosDistTotal = 0
	set @SaldoVentasTiendas = 0
	set	@SaldoVentasTiendasAcum = 0
	set	@SaldoVentasMay = 0
	set	@SaldoVentasAdmon = 0
	set	@SaldoVentasTiendasMay  = 0
	set	@SaldoCostoVentasAdmon = 0
	set	@SaldoCostoVentasGer = 0
	set	@SaldoCostoVentasAdmonAcum = 0
	set	@SaldoCostoVentasGerAcum = 0
	set	@SaldoGastosVentasAdmon = 0
	set	@SaldoGastosVentasAdmonAcum = 0
	set	@SaldoGastosVentasGer = 0
	set	@SaldoGastosVentasGerAcum = 0

	--drop table #temp_saldos 

	create table #temp_ventas (
	Fecha	datetime,
	CCosto	varchar(10),
	VentasNetasTienda	DECIMAL(12,2) ,
	PorcVNetasTotal	DECIMAL(12,4),
	PorcVentasTiendas DECIMAL(12,4))


	create table #temp_saldos (
	Secuencial	integer,
	Fecha	datetime,
	CentroCosto	varchar(10),
	NombreCCosto	varchar(100),
	VentasNetas	DECIMAL(12,2) ,
	PorcVNetas	DECIMAL(12,4) ,
	CostoVentas	DECIMAL(12,2) ,
	PorcCVentas	DECIMAL(12,4) ,
	MargenBruto	DECIMAL(12,2) ,
	PorcMBruto	DECIMAL(12,4) ,
	GastosVenta	DECIMAL(12,2) ,
	PorcGVenta	DECIMAL(12,4) ,
	ResultPreDist	DECIMAL(12,2) ,
	PorcRPreDist	DECIMAL(12,4) ,
	GastosPorDist	DECIMAL(12,2) ,
	ResultPosDist DECIMAL(12,2),
	PorcRPosDist	DECIMAL(12,4),
	PorcTRFinal		DECIMAL(12,4),
	Acumulado		varchar(1)
	)

if @Borrar = 'S'
begin
	delete	prodmult.MF_EstResult_CCosto
	where	fecha = @FechaFinal
	and		Acumulado = @Acumulado
	and		EMPRESA = @Empresa

	-- Ingreso Por Ventas Total (4-1-1-001-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @Cta = '4-1-1-001-0000'
	set	@SaldoGastosaDist  = 0
	set	@SaldoGastosNoDeduc = 0  
	set	@SaldoGastosFin = 0
	set	@SaldoGastosAdmon = 0
	set	@SaldoGastosNoDeducAcum = 0  
	set	@SaldoGastosFinAcum = 0
	set	@SaldoGastosAdmonAcum = 0

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set @SaldoFinalCalc  = @Creditos - @Debitos
	set @SaldoVentasTiendas  = @SaldoFinalCalc  
	set @SaldoVentasTiendasAcum  = @SaldoFinal


	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @CentroCosto = '2-001',  @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set @SaldoFinalCalc  = @Creditos - @Debitos
	set	@SaldoVentasMay = @SaldoFinalCalc 
	set	@SaldoVentasMayAcum = @SaldoFinal

	
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @CentroCosto = '2-099',  @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set @SaldoFinalCalc  = @Creditos - @Debitos
	set	@SaldoVentasAdmon = @SaldoFinalCalc 
	set	@SaldoVentasAdmonAcum = @SaldoFinal

	set @SaldoVentasTiendas  = @SaldoVentasTiendas  - @SaldoVentasMay - @SaldoVentasAdmon 
	set @SaldoVentasTiendasAcum  = @SaldoVentasTiendasAcum  - @SaldoVentasMayAcum - @SaldoVentasAdmonAcum 
	set	@SaldoVentasTiendasMay   = @SaldoVentasTiendas   + @SaldoVentasMay 
	set	@SaldoVentasTiendasMayAcum   = @SaldoVentasTiendasAcum   + @SaldoVentasMayAcum 

	--- Calculo de Gastos a Distribuir
	SET @CtaMadre = ''
	set	@SaldoGastosVentasAdmon = 0
	set	@SaldoGastosVentasAdmonAcum = 0
	set	@SaldoGastosVentasGer = 0
	set	@SaldoGastosVentasGerAcum = 0
	-- Gastos de Ventas (5-1-1-002-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @SaldoFinalCalc  = 0
	set @Cta = '5-2-1-000-0000'

		
	SET @CtaMadre = @Cta

	declare  #ctas_conta CURSOR LOCAL FOR
	select	cc.CUENTA_CONTABLE, cc.descripcion, mcc.signo	
	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	
	--and		mcc.cuenta_padre = @CtaMadre 
	and		cc.CUENTA_CONTABLE > '5-2-1-001-0000'
	and		cc.CUENTA_CONTABLE < '5-2-1-011-0000'
	and		substring(cc.cuenta_contable,11,4) != '0000'
	order by cc.CUENTA_CONTABLE

	OPEN #ctas_conta
	FETCH NEXT FROM #ctas_conta INTO @CtaHija, @CtaDescripcion, @SignoCta

	WHILE @@FETCH_STATUS = 0 
	BEGIN
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set @SaldoFinalCalc  = 0
	
		exec prodmult.spDatosCuentaAcum @Cuenta = @CtaHija, @CentroCosto = '2-998',  @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		set @SaldoFinalCalc  = @Creditos - @Debitos
		set	@SaldoGastosVentasAdmon = @SaldoGastosVentasAdmon + @SaldoFinalCalc
		set	@SaldoGastosVentasAdmonAcum = @SaldoGastosVentasAdmonAcum + @SaldoFinal

		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set @SaldoFinalCalc  = 0

		exec prodmult.spDatosCuentaAcum @Cuenta = @CtaHija, @CentroCosto = '2-999',  @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		set @SaldoFinalCalc  = @Creditos - @Debitos
		set	@SaldoGastosVentasGer= @SaldoGastosVentasGer + @SaldoFinalCalc
		set	@SaldoGastosVentasGerAcum = @SaldoGastosVentasGerAcum + @SaldoFinal

		
		FETCH NEXT FROM #ctas_conta INTO @CtaHija, @CtaDescripcion, @SignoCta
	END
	CLOSE #ctas_conta 
	DEALLOCATE #ctas_conta
--	select	@SaldoGastosVentasGer , @SaldoGastosVentasAdmon 

	-- Gastos de Administracion
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '5-2-3-000-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set @SaldoFinalCalc  = @Creditos - @Debitos
	set @SaldoGastosAdmon = @SaldoGastosAdmon + @SaldoFinalCalc
	set @SaldoGastosAdmonAcum = @SaldoGastosAdmonAcum + @SaldoFinal

	
	-- Gastos Financieros
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '5-3-1-000-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set @SaldoFinalCalc  = @Creditos - @Debitos
	set @SaldoGastosFin = @SaldoGastosFin + @SaldoFinalCalc
	set @SaldoGastosFinAcum = @SaldoGastosFinAcum + @SaldoFinal
	
	-- Gastos No Deducibles
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set	@SaldoFinalCalc = 0
	set @Cta = '5-5-1-000-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set @SaldoFinalCalc  = @Creditos - @Debitos
	set @SaldoGastosNoDeduc= @SaldoGastosNoDeduc + @SaldoFinalCalc
	set @SaldoGastosNoDeducAcum = @SaldoGastosNoDeducAcum + @SaldoFinal
	

	declare  #ctas_tienda CURSOR LOCAL FOR
	select	CENTRO_COSTO, DESCRIPCION
	from	prodmult.CENTRO_COSTO
	where	CENTRO_COSTO not in ('2-000', '2-998')
	order by CENTRO_COSTO
	OPEN #ctas_tienda
	FETCH NEXT FROM #ctas_tienda INTO @Tienda, @NombreTienda

	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- Ingreso Por Ventas (4-1-1-001-0000)
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set @Cta = '4-1-1-001-0000'

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @CentroCosto = @Tienda,  @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		set @SaldoFinalCalc  = @Creditos - @Debitos
		set @SaldoVentas  = @SaldoFinalCalc  
		set @SaldoVentasAcum  = @SaldoFinal


		-- DEVOLUCIONES REBAJAS DESC.S/V (4-1-1-003-0000)
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set @Cta = '4-1-1-003-0000'

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @CentroCosto = @Tienda,  @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		set @SaldoFinalCalc  = @Creditos - @Debitos

		-- Ventas Netas
		set @SaldoVentas  = @SaldoVentas  - @SaldoFinalCalc  
		set @SaldoVentasAcum  = @SaldoVentasAcum  - @SaldoFinal


		if @Acumulado = 'N'
			insert into #temp_ventas values (@FechaFin, @Tienda, @SaldoVentas,  ROUND(@SaldoVentas/@SaldoVentasTiendasMay, 4), ROUND(@SaldoVentas/@SaldoVentasTiendas, 4))
		else
			insert into #temp_ventas values (@FechaFin, @Tienda, @SaldoVentasAcum, ROUND(@SaldoVentasAcum/@SaldoVentasTiendasMayAcum,4),  ROUND(@SaldoVentasAcum/@SaldoVentasTiendasAcum,4))


		FETCH NEXT FROM #ctas_tienda INTO @Tienda, @NombreTienda
	END
	CLOSE #ctas_tienda 
	DEALLOCATE #ctas_tienda 

	-- Costo Por Ventas que se distribuyen(4-1-1-001-0000)
	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @SaldoFinalCalc  = 0
	set @SaldoFinal = 0
	set @Cta = '5-1-1-002-0000'

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @CentroCosto = '2-998',  @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set @SaldoFinalCalc  = @Creditos - @Debitos
	set @SaldoCostoVentasAdmon = @SaldoFinalCalc  
	set @SaldoCostoVentasAdmonAcum = @SaldoFinal


	--select	@SaldoCostoVentasAdmon, @SaldoCostoVentasGer, @SaldoFinalCalc, @SaldoFinal

	Set	@Debitos = 0
	set @Creditos = 0
	set	@SaldoInicial = 0
	set @SaldoFinal = 0
	set @SaldoFinalCalc  = 0
	set @SaldoFinal = 0

	exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @CentroCosto = '2-999',  @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
	set @SaldoFinalCalc  = @Creditos - @Debitos
	set @SaldoCostoVentasGer = @SaldoFinalCalc  
	set @SaldoCostoVentasGerAcum = @SaldoFinal

	--select	@SaldoCostoVentasAdmon, @SaldoCostoVentasGer, @SaldoFinalCalc, @SaldoFinal

	declare  #ctas_tienda CURSOR LOCAL FOR
	select	CENTRO_COSTO, DESCRIPCION
	from	prodmult.CENTRO_COSTO
	where	CENTRO_COSTO not in ('2-000', '2-001', '2-099', '2-998', '2-999')
	--and		CENTRO_COSTO in ('2-002', '2-014')
	order by CENTRO_COSTO


	OPEN #ctas_tienda
	FETCH NEXT FROM #ctas_tienda INTO @Tienda, @NombreTienda

	WHILE @@FETCH_STATUS = 0
	BEGIN
		set @SaldoGastosVentas = 0
		set	@SaldoGastosVentasAcum = 0
		set	@SaldoVentas = 0
		set	@SaldoCostoVentas  = 0
		set	@SaldoMargenBruto = 0
		set	@SaldoResultPreDist = 0
		set	@SaldoResultPosDist = 0
		set	@SaldoResultPreDistAcum = 0
		set	@SaldoResultPosDistAcum = 0
		set	@SaldoGastosaDist  = 0


		select	@PorcVentasTiendas = PorcVentasTiendas,
				@PorcVentasTiendasMay = PorcVNetasTotal
		from	#temp_ventas
		where	fecha = @FechaFin
		and		CCosto = @Tienda	

		-- Ingreso Por Ventas (4-1-1-001-0000)
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set @Cta = '4-1-1-001-0000'

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @CentroCosto = @Tienda,  @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		set @SaldoFinalCalc  = @Creditos - @Debitos
		set @SaldoVentas  = @SaldoFinalCalc  
		set @SaldoVentasAcum  = @SaldoFinal

		-- DEVOLUCIONES REBAJAS DESC.S/V (4-1-1-003-0000)
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set @Cta = '4-1-1-003-0000'

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @CentroCosto = @Tienda,  @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		set @SaldoFinalCalc  = @Creditos - @Debitos

		-- Ventas Netas
		set @SaldoVentas  = @SaldoVentas  - @SaldoFinalCalc  
		set @SaldoVentasAcum  = @SaldoVentasAcum  - @SaldoFinal
	
		-- Compras (5-1-1-001-0000)
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set @Cta = '5-1-1-001-0000'

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @CentroCosto = @Tienda,  @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

		set @SaldoFinalCalc  = @Creditos - @Debitos
		set @SaldoCostoVentas = @SaldoFinalCalc
		set @SaldoCostoVentasAcum = @SaldoFinal

		-- DEV. Y REBAJAS SOBRE COMPRAS (5-1-1-002-0000)
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set @Cta = '5-1-1-002-0000'

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @CentroCosto = @Tienda,  @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		set @SaldoFinalCalc  = @Creditos - @Debitos
		set @SaldoCostoVentas = @SaldoCostoVentas - (@SaldoFinalCalc*-1)
		set @SaldoCostoVentasAcum = @SaldoCostoVentasAcum - (@SaldoFinal*-1)

		--select	@Cta, @SaldoCostoVentas, @SaldoFinal, @SaldoFinalCalc, @SignoCta 

	
		-- Se distribuyen Costos de Ventas de Gerencia y Admon.
		set @SaldoCostoVentas = @SaldoCostoVentas + ((@PorcVentasTiendas*@SaldoCostoVentasAdmon)*-1) + ((@PorcVentasTiendasMay*@SaldoCostoVentasGer)*-1)
		set @SaldoCostoVentasAcum = @SaldoCostoVentasAcum + ((@PorcVentasTiendas*@SaldoCostoVentasAdmonAcum)*-1) + ((@PorcVentasTiendasMay*@SaldoCostoVentasGerAcum)*-1)

		set @SaldoCostoVentas = @SaldoCostoVentas *-1
		set @SaldoCostoVentasAcum = @SaldoCostoVentasAcum *-1

		--select	@Cta, @SaldoCostoVentas, @SignoCta , @SaldoCostoVentasAdmon, @SaldoCostoVentasGer, @PorcVentasTiendas, @PorcVentasTiendasMay

		
		-- Gastos de Ventas (5-1-1-002-0000)
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set @Cta = '5-2-1-000-0000'
		set	@SaldoGastosVentas = 0
		set	@SaldoGastosVentasAcum = 0

		
		SET @CtaMadre = @Cta

		declare  #ctas_conta CURSOR LOCAL FOR
		select	cc.CUENTA_CONTABLE, cc.descripcion, mcc.signo	
		from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
		order by cc.CUENTA_CONTABLE

		OPEN #ctas_conta
		FETCH NEXT FROM #ctas_conta INTO @CtaHija, @CtaDescripcion, @SignoCta

		WHILE @@FETCH_STATUS = 0 
		BEGIN
			Set	@Debitos = 0
			set @Creditos = 0
			set	@SaldoInicial = 0
			set @SaldoFinal = 0
	
			exec prodmult.spDatosCuentaAcum @Cuenta = @CtaHija, @CentroCosto = @Tienda,  @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
			set @SaldoFinalCalc  = @Creditos - @Debitos
			--select @CtaHija, @SaldoGastosVentas, @SaldoFinalCalc 
			if @SignoCta = '+'
			begin
				set	@SaldoGastosVentas = @SaldoGastosVentas + @SaldoFinalCalc  
				set	@SaldoGastosVentasAcum = @SaldoGastosVentasAcum + @SaldoFinal
			end
			else
			begin
				set	@SaldoGastosVentas = @SaldoGastosVentas + (@SaldoFinalCalc * ( -1))
				set	@SaldoGastosVentasAcum = @SaldoGastosVentasAcum + (@SaldoFinal *  ( -1))
			end
		
			FETCH NEXT FROM #ctas_conta INTO @CtaHija, @CtaDescripcion, @SignoCta
		END
		CLOSE #ctas_conta 
		DEALLOCATE #ctas_conta
		--select	@SaldoGastosVentas , @SaldoGastosVentasGer , @SaldoGastosVentasAdmon , @PorcVentasTiendas

		set	@SaldoGastosVentas = @SaldoGastosVentas + (@SaldoGastosVentasAdmon * (@PorcVentasTiendas)) + (@SaldoGastosVentasGer * (@PorcVentasTiendasMay))
		set	@SaldoGastosVentasAcum = @SaldoGastosVentasAcum + (@SaldoGastosAdmonAcum * (@PorcVentasTiendas)) + (@SaldoGastosVentasGerAcum * (@PorcVentasTiendasMay))

		--select	@SaldoGastosVentas , @SaldoGastosVentasGer , @SaldoGastosVentasAdmon 

	
		set	@SaldoVentasTotal  = @SaldoVentasTotal  + @SaldoVentas
		set	@SaldoVentasTotalAcum  = @SaldoVentasTotalAcum  + @SaldoVentasAcum
		
	
		if @SaldoVentas != 0
		begin
			set	@Secuencial	 = @Secuencial	 + 1
			if @Acumulado  = 'N'
				insert into #temp_saldos values (@Secuencial, @FechaFin, @Tienda, @NombreTienda, @SaldoVentas, @PorcVentasTiendasMay, @SaldoCostoVentas, @SaldoCostoVentas/@SaldoVentas, @SaldoVentas+@SaldoCostoVentas , (@SaldoVentas+@SaldoCostoVentas)/@SaldoVentas, @SaldoGastosVentas*-1, @SaldoGastosVentas/@SaldoVentas,0, 0, 0, 0, 0, 0, @Acumulado)
			else
				INSERT INTO #temp_saldos VALUES (@Secuencial, @FechaFin, @Tienda, @NombreTienda, @SaldoVentasAcum, @PorcVentasTiendasMay, @SaldoCostoVentasAcum, @SaldoCostoVentasAcum/@SaldoVentasAcum, @SaldoVentasAcum+@SaldoCostoVentasAcum, (@SaldoVentasAcum+@SaldoCostoVentasAcum)/@SaldoVentasAcum, @SaldoGastosVentasAcum*-1, @SaldoGastosVentasAcum/@SaldoVentasAcum,0, 0, 0, 0, 0, 0, @Acumulado)
		end
		FETCH NEXT FROM #ctas_tienda INTO @Tienda, @NombreTienda
	END
	CLOSE #ctas_tienda 
	DEALLOCATE #ctas_tienda 


	set @SaldoGastosaDist  = @SaldoGastosNoDeduc  + @SaldoGastosFin + @SaldoGastosAdmon  - 90000
	set @SaldoGastosaDistAcum  = @SaldoGastosNoDeducAcum  + @SaldoGastosFinAcum + @SaldoGastosAdmonAcum - (90000*MONTH(@FechaFinal))
	
	set	@SaldoResultPosDist = @SaldoResultPreDist - @SaldoGastosaDist  
	set	@SaldoResultPosDistAcum = @SaldoResultPreDistAcum - @SaldoGastosaDistAcum  

	--select	@SaldoGastosaDist  , @SaldoGastosNoDeduc  , @SaldoGastosFin , @SaldoGastosAdmon
	--select	@Tienda, @SaldoGastosaDist * @PorcVentasTiendas, @PorcVentasTiendas

	if @Acumulado  = 'N'
	begin
		update	#temp_saldos 
		set		ResultPreDist = MargenBruto + GastosVenta,
				GastosPorDist = (@SaldoGastosaDist * PorcVentasTiendas) * -1
		from	#temp_ventas 
		where	CentroCosto = CCosto
	end
	else
	begin
		update	#temp_saldos 
		set		ResultPreDist = MargenBruto + GastosVenta,
				GastosPorDist = (@SaldoGastosaDistAcum * PorcVentasTiendas) * -1 
		from	#temp_ventas 
		where	CentroCosto = CCosto
	end
	update	#temp_saldos 
	set		PorcRPreDist = ResultPreDist / VentasNetas,
			ResultPosDist = ResultPreDist + GastosPorDist 

	update	#temp_saldos 
	set		PorcRPosDist= ResultPosDist / VentasNetas
			
			


	set	@Secuencial	 = @Secuencial	 + 1
	insert into #temp_saldos
	select	@Secuencial	, @FechaFin, '', 'TOTAL TIENDAS', sum(VentasNetas), sum(PorcVNetas), sum(CostoVentas), sum(CostoVentas)/sum(VentasNetas), sum(MargenBruto), sum(MargenBruto)/sum(VentasNetas), sum(GastosVenta), sum(GastosVenta)/sum(VentasNetas), sum(ResultPreDist), sum(ResultPreDist)/sum(VentasNetas), sum(GastosPorDist), sum(ResultPosDist), sum(ResultPosDist)/sum(VentasNetas), sum(PorcTRFinal), @Acumulado
	from	#temp_saldos 
	where	VentasNetas != 0


	--- MAYOREO Y ADMINISTRACION
	declare  #ctas_tienda CURSOR LOCAL FOR
	select	CENTRO_COSTO, DESCRIPCION
	from	prodmult.CENTRO_COSTO
	where	CENTRO_COSTO in ('2-001', '2-099')
	order by CENTRO_COSTO desc


	OPEN #ctas_tienda
	FETCH NEXT FROM #ctas_tienda INTO @Tienda, @NombreTienda

	WHILE @@FETCH_STATUS = 0
	BEGIN
		set @SaldoGastosVentas = 0
		set	@SaldoGastosVentasAcum = 0
		set	@SaldoVentas = 0
		set	@SaldoCostoVentas  = 0
		set	@SaldoMargenBruto = 0
		
		select	@PorcVentasTiendas = PorcVentasTiendas,
				@PorcVentasTiendasMay = PorcVNetasTotal
		from	#temp_ventas
		where	fecha = @FechaFin
		and		CCosto = @Tienda	

		-- Ingreso Por Ventas (4-1-1-001-0000)
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set @Cta = '4-1-1-001-0000'

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @CentroCosto = @Tienda,  @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		set @SaldoFinalCalc  = @Creditos - @Debitos
		set @SaldoVentas  = @SaldoFinalCalc  
		set @SaldoVentasAcum  = @SaldoFinal

		-- DEVOLUCIONES REBAJAS DESC.S/V (4-1-1-003-0000)
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set @Cta = '4-1-1-003-0000'

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @CentroCosto = @Tienda,  @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		set @SaldoFinalCalc  = @Creditos - @Debitos

		-- Ventas Netas
		set @SaldoVentas  = @SaldoVentas  - @SaldoFinalCalc  
		set @SaldoVentasAcum  = @SaldoVentasAcum  - @SaldoFinal

		-- Compras (5-1-1-001-0000)
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set @Cta = '5-1-1-001-0000'

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @CentroCosto = @Tienda,  @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output

		set @SaldoFinalCalc  = @Creditos - @Debitos
		set @SaldoCostoVentas = @SaldoFinalCalc
		set @SaldoCostoVentasAcum = @SaldoFinal


		-- DEV. Y REBAJAS SOBRE COMPRAS (5-1-1-002-0000)
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set @Cta = '5-1-1-002-0000'

		exec prodmult.spDatosCuentaAcum @Cuenta = @Cta, @CentroCosto = @Tienda,  @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
		set @SaldoFinalCalc  = @Creditos - @Debitos
		set @SaldoCostoVentas = (@SaldoCostoVentas - @SaldoFinalCalc) * -1
		set @SaldoCostoVentasAcum = (@SaldoCostoVentasAcum - @SaldoFinal) * -1
	
		--set @SaldoMargenBruto = @SaldoVentas - @SaldoCostoVentas
		--set @SaldoMargenBrutoAcum = @SaldoVentasAcum - @SaldoCostoVentasAcum


		-- Gastos de Ventas (5-1-1-002-0000)
		Set	@Debitos = 0
		set @Creditos = 0
		set	@SaldoInicial = 0
		set @SaldoFinal = 0
		set @Cta = '5-2-1-000-0000'

		SET @CtaMadre = @Cta

		declare  #ctas_conta CURSOR LOCAL FOR
		select	cc.CUENTA_CONTABLE, cc.descripcion	from	prodmult.CUENTA_CONTABLE  cc, prodmult.MF_CuentaContable mcc 	WHERE	cc.cuenta_contable = mcc.cuenta_contable	and		mcc.cuenta_padre = @CtaMadre 
		order by cc.CUENTA_CONTABLE

		OPEN #ctas_conta
		FETCH NEXT FROM #ctas_conta INTO @CtaHija, @CtaDescripcion

		WHILE @@FETCH_STATUS = 0 
		BEGIN
			Set	@Debitos = 0
			set @Creditos = 0
			set	@SaldoInicial = 0
			set @SaldoFinal = 0
	
			exec prodmult.spDatosCuentaAcum @Cuenta = @CtaHija, @CentroCosto = @Tienda,  @FechaInicial  = @FechaIni , @FechaFinal = @FechaFin, @SInicial = @SaldoInicial output,  @SFinal = @SaldoFinal output,  @Cred = @Creditos output,  @Deb = @Debitos output, @CtaNombre = @CtaDescripcion output, @CtaSup = @CtaMadre output
			set @SaldoFinalCalc  = @Creditos - @Debitos
			set	@SaldoGastosVentas = @SaldoGastosVentas + @SaldoFinalCalc  
			set	@SaldoGastosVentasAcum = @SaldoGastosVentasAcum + @SaldoFinal
		
			FETCH NEXT FROM #ctas_conta INTO @CtaHija, @CtaDescripcion
		END
		CLOSE #ctas_conta 
		DEALLOCATE #ctas_conta


		set @SaldoResultPreDist	 = @SaldoMargenBruto - @SaldoGastosVentas
		set @SaldoResultPreDistAcum = @SaldoMargenBrutoAcum - @SaldoGastosVentasAcum

	
		set	@SaldoVentasTotal  = @SaldoVentasTotal  + @SaldoVentas
		set	@SaldoVentasTotalAcum  = @SaldoVentasTotalAcum  + @SaldoVentasAcum
		
		set	@SaldoMargenBruto = @SaldoVentas + @SaldoCostoVentas
		set	@SaldoMargenBrutoAcum = @SaldoVentasAcum + @SaldoCostoVentasAcum
	
		if @SaldoVentas != 0
		begin
			set	@Secuencial	 = @Secuencial	 + 1
			if @Acumulado  = 'N'
				insert into #temp_saldos values (@Secuencial, @FechaFin, @Tienda, @NombreTienda, @SaldoVentas, @PorcVentasTiendasMay, @SaldoCostoVentas, @SaldoCostoVentas/@SaldoVentas, @SaldoMargenBruto, @SaldoMargenBruto/@SaldoVentas, @SaldoGastosVentas*-1, @SaldoGastosVentas/@SaldoVentas,@SaldoMargenBruto-@SaldoGastosVentas, (@SaldoMargenBruto-@SaldoGastosVentas)/@SaldoVentas, 0, 0, 0, 0, @Acumulado)
			else
				INSERT INTO #temp_saldos VALUES (@Secuencial, @FechaFin, @Tienda, @NombreTienda, @SaldoVentasAcum, @PorcVentasTiendasMay, @SaldoCostoVentasAcum, @SaldoCostoVentasAcum/@SaldoVentasAcum, @SaldoMargenBrutoAcum, @SaldoMargenBrutoAcum/@SaldoVentasAcum, @SaldoGastosVentasAcum*-1, @SaldoGastosVentasAcum/@SaldoVentasAcum,@SaldoMargenBrutoAcum-@SaldoGastosVentasAcum, (@SaldoMargenBrutoAcum-@SaldoGastosVentasAcum)/@SaldoVentasAcum, 0, 0, 0, 0, @Acumulado)
		end
		FETCH NEXT FROM #ctas_tienda INTO @Tienda, @NombreTienda
	END
	CLOSE #ctas_tienda 
	DEALLOCATE #ctas_tienda 
	update	#temp_saldos 
	set		GastosPorDist = 0
	where	CentroCosto = '2-099'
	if @Acumulado  = 'N'
	begin
		update	#temp_saldos 
		set		GastosPorDist = -90000
		where	CentroCosto = '2-001'
	end
	else
	begin
		update	#temp_saldos 
		set		GastosPorDist = (-90000) * MONTH(@FechaFinal)
		where	CentroCosto = '2-001'
	end
	/*
	if @Acumulado  = 'N'
	begin
		update	#temp_saldos 
		set		PorcTRFinal = ResultPosDist/@SaldoVentasTotal 			
		where	VentasNetas	!= 0
		and		CentroCosto != ''
	end
	else
	begin
		update	#temp_saldos 
		set		PorcTRFinal = ResultPosDist/@SaldoVentasTotalAcum
		where	VentasNetas	!= 0
		and		CentroCosto != ''
	end
	*/	
	
	--select	@SaldoResultPosDistTotal
	update	#temp_saldos 
	set		ResultPosDist = ResultPreDist + GastosPorDist			
	where	CentroCosto in ('2-001', '2-099')

	update	#temp_saldos 
	set		PorcRPosDist= ResultPosDist/VentasNetas
	where	CentroCosto in ('2-001', '2-099')

	select	@SaldoResultPosDistTotal= sum(ResultPosDist)
	from	#temp_saldos 
	where	CentroCosto != ''

	--select	@SaldoResultPosDistTotal

	update	#temp_saldos 
	set		PorcTRFinal = ResultPosDist/@SaldoResultPosDistTotal
	where	VentasNetas	!= 0
	and		CentroCosto != ''

	set	@Secuencial	 = @Secuencial	 + 1

	insert into #temp_saldos
	select	@Secuencial	, @FechaFin, '', 'TOTAL TIENDAS + MAYOREO', sum(VentasNetas), sum(PorcVNetas), sum(CostoVentas), sum(CostoVentas)/sum(VentasNetas), sum(MargenBruto), sum(MargenBruto)/sum(VentasNetas), sum(GastosVenta), sum(GastosVenta)/sum(VentasNetas), 
			sum(ResultPreDist), sum(ResultPreDist)/sum(VentasNetas), sum(GastosPorDist), sum(ResultPosDist), sum(ResultPosDist)/sum(VentasNetas), sum(PorcTRFinal), @Acumulado
	from	#temp_saldos 
	where	CentroCosto != ''


	insert into prodmult.MF_EstResult_CCosto
	select	@Empresa, Secuencial, Fecha, CentroCosto, Acumulado, NombreCCosto, VentasNetas, PorcVNetas, CostoVentas, PorcCVentas, MargenBruto, PorcMBruto, GastosVenta, PorcGVenta, ResultPreDist, PorcRPreDist, GastosPorDist, ResultPosDist, PorcRPosDist, PorcTRFinal
	from	#temp_saldos 
	where	fecha = @FechaFinal
	and		Acumulado = @Acumulado
end

select	*
from	prodmult.MF_EstResult_CCosto
where	FECHA = @FechaFinal
and		Acumulado = @Acumulado
and		EMPRESA = @Empresa

end

GO


GRANT EXECUTE ON prodmult.sp_ERTienda TO [REPORTEADOR]
GO
GRANT EXECUTE ON prodmult.sp_ERTienda TO [FIESTA\rolando.pineda]
GO
GRANT EXECUTE ON prodmult.sp_ERTienda TO [FIESTA\william.quiacain]
GO
GRANT EXECUTE ON prodmult.sp_ERTienda TO [FIESTA\mike]
GO
GRANT EXECUTE ON prodmult.sp_ERTienda TO [mf.fiestanet]
GO
GRANT EXECUTE ON prodmult.sp_ERTienda TO [FIESTA\eder.hulse]
GO
