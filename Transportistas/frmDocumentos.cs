﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Diagnostics;
using DevExpress.XtraPrinting;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Layout;


namespace Transportistas
{
    public partial class frmDocumentos : Form
    {
        bool vEditando = false;
        DataTable dt = new DataTable("transportistas");
        DataTable dt2 = new DataTable("transportistas");
        
        public frmDocumentos()
        {
            InitializeComponent();
        }

        private void frmDocumentos_Load(object sender, EventArgs e)
        {
            this.CancelButton = btnCancelar;
            lbUsuario.Text = string.Format("Bienvenido(a) {0}", this.Tag.ToString());
        }

        private void frmDocumentos_Shown(object sender, EventArgs e)
        {
            CargarTransportistas();
            tipo.Focus();
        }

        void CargarTransportistas()
        {
            try
            {
                var ws = new wsPuntoVenta.wsPuntoVenta();
                ws.Url = Globales.pUrlPV;

                var qTransportistas = ws.DevuelveTransportistas("F01");

                DataTable dt = new DataTable("transportistas");

                dt.Columns.Add(new DataColumn("Nombre", typeof(System.String)));
                dt.Columns.Add(new DataColumn("Transportista", typeof(System.String)));
                dt2.Columns.Add(new DataColumn("Nombre", typeof(System.String)));
                dt2.Columns.Add(new DataColumn("Transportista", typeof(System.String)));

                foreach (var item in qTransportistas)
                {
                    DataRow row = dt.NewRow();
                    row["Nombre"] = item.Nombre;
                    row["Transportista"] = item.Transportista;
                    dt.Rows.Add(row);
                }

                DataRow[] rows = dt.Select("", "Nombre");

                for (int ii = 0; ii < rows.Length; ii++)
                {
                    DataRow row = dt2.NewRow();
                    row["Nombre"] = rows[ii]["Nombre"];
                    row["Transportista"] = rows[ii]["Transportista"];
                    dt2.Rows.Add(row);
                }

                transportista.Properties.DataSource = dt2;
                transportista.Properties.ValueMember = "Transportista";
                transportista.Properties.DisplayMember = "Nombre";
                transportista.Properties.PopulateColumns();
                transportista.Properties.Columns["Transportista"].Visible = false;

                tipo.EditValue = "Envío";
                transportista.EditValue = "0";
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al cargar los transportistas {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                if (documento.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Debe ingresar un documento.", Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    documento.Focus();
                    return;
                }
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("El documento no tiene líneas.", Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    documento.Focus();
                    return;
                }

                string mMensaje = "";
                wsTransportistas.wsTransportistas ws = new wsTransportistas.wsTransportistas();
                ws.Url = Globales.pUrl;

                if (!ws.GrabarTransportistaDocumento(this.AccessibleName, ref mMensaje, dt))
                {
                    if (mMensaje == null) mMensaje = "Error al grabar.";
                    MessageBox.Show(mMensaje, Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    documento.Focus();

                    return;
                }
                
                DataTable dtBlanco = new DataTable("transportistas");
                MessageBox.Show(mMensaje, Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Information);
                
                documento.Text = "";
                transportista.EditValue = "0";
                //fecha.EditValue = new DateTime(2015, 1, 1);
                gridArticulos.DataSource = dtBlanco;
                documento.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al grabar el documento {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void fecha_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                for (int ii = 0; ii < dt.Rows.Count; ii++)
                {
                    dt.Rows[ii]["Fecha"] = fecha.EditValue;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al asignar la fecha {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void transportista_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                for (int ii = 0; ii < dt.Rows.Count; ii++)
                {
                    dt.Rows[ii]["Transportista"] = transportista.EditValue;
                }
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al asignar el transportista {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void documento_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) validarDocumento();
        }

        private void documento_Leave(object sender, EventArgs e)
        {
            validarDocumento();
        }

        private void documento_TextChanged(object sender, EventArgs e)
        {
            vEditando = true;
        }

        void validarDocumento()
        {
            try
            {
                if (!vEditando) return;
                if (documento.Text.Trim().Length == 0) return;

                string mMensaje = ""; 
                string mDocumento = documento.Text.Trim().ToUpper();

                wsTransportistas.wsTransportistas ws = new wsTransportistas.wsTransportistas();
                ws.Url = Globales.pUrl;

                if (!ws.DocumentoValido(Convert.ToString(tipo.EditValue), mDocumento, ref dt, ref mMensaje))
                {
                    if (mMensaje == null) mMensaje = "El documento es inválido.";
                    MessageBox.Show(mMensaje, Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    DataTable dtBlanco = new DataTable("transportistas");

                    documento.Text = "";
                    gridArticulos.DataSource = dtBlanco;
                    documento.Focus();

                    return;
                }

                gridArticulos.DataSource = dt;

                viewArticulos.Columns["Tipo"].Visible = false;
                viewArticulos.Columns["Linea"].Visible = false;
                viewArticulos.Columns["Descripcion"].Caption = "Descripción";
                viewArticulos.Columns["Documento"].Width = 90;
                viewArticulos.Columns["Articulo"].Width = 90;
                viewArticulos.Columns["Descripcion"].Width = 320;
                viewArticulos.Columns["Transportista"].Width = 190;
                viewArticulos.Columns["Documento"].OptionsColumn.ReadOnly = true;
                viewArticulos.Columns["Articulo"].OptionsColumn.ReadOnly = true;
                viewArticulos.Columns["Descripcion"].OptionsColumn.ReadOnly = true;
                viewArticulos.OptionsView.ShowGroupPanel = false;


                RepositoryItemLookUpEdit edit = new RepositoryItemLookUpEdit();
                edit.DataSource = dt2;
                edit.ValueMember = "Transportista";
                edit.DisplayMember = "Nombre";
                edit.PopulateColumns();
                edit.Columns["Transportista"].Visible = false;
                edit.DropDownRows = 12;
                edit.ShowHeader = false;
                viewArticulos.Columns["Transportista"].ColumnEdit = edit;

                vEditando = false;
                fecha.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error al validar el documento {0} {1}", ex.Message, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
