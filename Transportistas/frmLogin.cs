﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Transportistas
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            this.AcceptButton = btnAceptar;
            this.CancelButton = btnCancelar;
        }

        private void frmLogin_Shown(object sender, EventArgs e)
        {
            try
            {
                
                string mSPS = string.Format("{0}\\SPS.ini", Environment.GetFolderPath(Environment.SpecialFolder.UserProfile));
                IniFile ini = new IniFile(mSPS);

                txtUsuario.Text = ini.IniReadValue("EXACTUS", "Usuario");
                txtPassword.Focus();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }
                //MessageBox.Show(string.Format("Error {0}{1}{2}", ex.Message, System.Environment.NewLine, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);

                txtUsuario.Focus();
            }
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtUsuario.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Debe ingresar el usuario.", Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtUsuario.Focus();
                    return;
                }
                if (txtPassword.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Debe ingresar la contraseña.", Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPassword.Focus();
                    return;
                }
                
                string mMensaje = ""; string mNombre = "";
                wsTransportistas.wsTransportistas ws = new wsTransportistas.wsTransportistas();
                ws.Url = Globales.pUrl;

                if (!ws.LoginExitoso(txtUsuario.Text.Trim().ToUpper(), txtPassword.Text, ref mNombre, ref mMensaje))
                {
                    MessageBox.Show(mMensaje, Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPassword.Text = "";
                    txtPassword.Focus();
                    return;
                }

                frmDocumentos frmDocs = new frmDocumentos();
                frmDocs.AccessibleName = txtUsuario.Text.Trim().ToUpper();
                frmDocs.Tag = mNombre;
                frmDocs.ShowDialog();
                frmDocs.Dispose();
                Application.Exit();
            }
            catch (Exception ex)
            {
                string m = "";
                try
                {
                    m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("línea"));
                }
                catch
                {
                    try
                    {
                        m = ex.StackTrace.Substring(ex.StackTrace.IndexOf("line"));
                    }
                    catch
                    {
                        //Nothing
                    }
                }

                MessageBox.Show(string.Format("Error {0}{1}{2}", ex.Message, System.Environment.NewLine, m), Globales.pSistema, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
