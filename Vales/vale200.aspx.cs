﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;

public partial class vale200 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string mVale = Request.QueryString["vale"];
        string mDia = Request.QueryString["dia"];
        string mMes = Request.QueryString["mes"];
        string mAnio = Request.QueryString["anio"];

        SqlCommand mCommand = new SqlCommand(); DateTime mFechaVence; string mNombreCliente = "";
        SqlConnection mConexion = new SqlConnection("Data Source = (local); Initial catalog = EXACTUSERP;User id = mf.fiestanet;password = 54321");

        mConexion.Open();
        mCommand.Connection = mConexion;
        mCommand.CommandText = string.Format("SELECT b.NOMBRE FROM prodmult.MF_ClienteVale200 a JOIN prodmult.CLIENTE b ON a.CLIENTE = b.CLIENTE WHERE a.VALE = {0}", mVale);
        mNombreCliente = mCommand.ExecuteScalar().ToString();
        mConexion.Close();

        mFechaVence = new DateTime(Convert.ToInt32(mAnio), Convert.ToInt32(mMes), Convert.ToInt32(mDia));

        ReportDocument reporte = new ReportDocument();
        string p = (Request.PhysicalApplicationPath + "reportes/rptVale.rpt");
        reporte.Load(p);

        string mNombreDocumento = string.Format("Vale_{0}.pdf", mVale);

        reporte.SetParameterValue("Vale", mVale);
        reporte.SetParameterValue("FechaVence", mFechaVence);
        reporte.SetParameterValue("ClienteNombre", mNombreCliente);
        report.ReportSource = reporte;
        reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

        Response.Clear();
        Response.ContentType = "application/pdf";
        Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
        Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
        Response.End();
    }
}