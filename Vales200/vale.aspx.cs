﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;

namespace Vales200
{
    public partial class vale : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) CargarVale(false);
        }

        void CargarVale(bool exportar)
        {
            string mVale = Request.QueryString["vale"];

            SqlCommand mCommand = new SqlCommand(); DateTime mFechaVence; string mNombreCliente = "";
            SqlConnection mConexion = new SqlConnection("Data Source = INTEGRA; Initial Catalog = EXACTUSERP;User id = mf.fiestanet;password = 54321");
            
            mConexion.Open();
            mCommand.Connection = mConexion;

            mCommand.CommandText = string.Format("SELECT b.NOMBRE FROM prodmult.MF_ClienteVale200 a JOIN prodmult.CLIENTE b ON a.CLIENTE = b.CLIENTE WHERE a.VALE = {0}", mVale);
            mNombreCliente = mCommand.ExecuteScalar().ToString();

            mCommand.CommandText = string.Format("SELECT a.FECHA_VENCE FROM prodmult.MF_ClienteVale200 a JOIN prodmult.CLIENTE b ON a.CLIENTE = b.CLIENTE WHERE a.VALE = {0}", mVale);
            mFechaVence = (DateTime)mCommand.ExecuteScalar();

            mConexion.Close();
            lbError.Text = "";
            
            ReportDocument reporte = new ReportDocument();
            string p = (Request.PhysicalApplicationPath + "reportes/rptVale.rpt");
            reporte.Load(p);

            reporte.SetParameterValue("Vale", mVale);
            reporte.SetParameterValue("FechaVence", mFechaVence);
            reporte.SetParameterValue("ClienteNombre", mNombreCliente);
            this.CrystalReportViewer1.ReportSource = reporte;

            if (exportar)
            {
                if (DateTime.Now.Date > mFechaVence)
                {
                    lbError.Text = "Este vale está vencido, no es posible imprimirlo.";
                    return;
                }

                string mNombreDocumento = string.Format("Vale_{0}", mVale);
                reporte.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\reportes\" + mNombreDocumento);

                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-disposition", "attachment; filename=" + @"" + mNombreDocumento);
                Response.WriteFile(@"C:\reportes\" + mNombreDocumento);
                Response.End();
            }
        }

        protected void lkImprimir_Click(object sender, EventArgs e)
        {
            CargarVale(true);
        }
    }
}