﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ValidacionNIT
{
    public partial class frmValidacionNIT : Form
    {
        public frmValidacionNIT()
        {
            InitializeComponent();
        }

        private void frmValidacionNIT_Load(object sender, EventArgs e)
        {

        }

        private void frmValidacionNIT_Shown(object sender, EventArgs e)
        {
            txtNit.Focus();
            lbResultado.Text = "";
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txtNit_TextChanged(object sender, EventArgs e)
        {
            lbResultado.Text = "";
        }

        private void btnValidar_Click(object sender, EventArgs e)
        {
            if (ValidarNIT(txtNit.Text))
            {
                lbResultado.Text = "NIT VALIDO";
                lbResultado.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                lbResultado.Text = "NIT INVALIDO";
                lbResultado.ForeColor = System.Drawing.Color.Red;
            }
        }

        public bool ValidarNIT(string Nit)
        {
            try
            {
                Nit = Nit.ToUpper();

                int pos = Nit.IndexOf("-");
                string Correlativo = Nit.Substring(0, pos);
                string DigitoVerificador = Nit.Substring(pos + 1);
                int Factor = Correlativo.Length + 1;
                int Suma = 0;
                int Valor = 0;

                for (int x = 0; x <= Nit.IndexOf("-") - 1; x++)
                {
                    Valor = Convert.ToInt32(Nit.Substring(x, 1));
                    Suma = Suma + (Valor * Factor);
                    Factor = Factor - 1;
                }

                double xMOd11 = 0;
                xMOd11 = (11 - (Suma % 11)) % 11;
                string s = Convert.ToString(xMOd11);
                if ((xMOd11 == 10 & DigitoVerificador == "K") | (s == DigitoVerificador))
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            lbResultado.Text = "";
        }

    }
}
