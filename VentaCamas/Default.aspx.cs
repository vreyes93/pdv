﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VentaCamas
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnVentaCamas_Click(object sender, EventArgs e)
        {
            Response.Redirect("VentaCamas.aspx");
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("http://{0}/FiestaNet", Request.Url.Host));
        }
    }
}
