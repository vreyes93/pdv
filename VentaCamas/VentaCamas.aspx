﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="VentaCamas.aspx.cs" Inherits="VentaCamas.VentaCamas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style4
        {
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table align="center" id="tablaTitulos" runat="server">
        <tr>
            <td class="style4">
                <strong style="text-align: center; font-weight: bold;">REPORTE DE VENTA DE CAMAS</strong></td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:LinkButton ID="lbInfo" runat="server" Font-Underline="True" 
                    ForeColor="#696984" Font-Bold="True" 
                    Font-Size="Small" ToolTip="Regresa la página a su vista normal"></asp:LinkButton>
            </td>
        </tr>
    </table>
    <table align="center" id="tablaFechas" runat="server">
        <tr>
            <td>
                <asp:Label ID="lbFechaInicial" runat="server" Text="Fecha Inicial:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="FechaInicial" runat="server" Width="110px" TabIndex="10"></asp:TextBox>
            </td>
            <td>
                <asp:LinkButton ID="lbCalendarioInicio" runat="server" 
                    onclick="lbCalendarioInicio_Click" TabIndex="15" Width="107px">Abrir Calendario</asp:LinkButton>
            </td>
            <td>
                <asp:Calendar ID="CalendarioInicial" runat="server" BackColor="White" 
                    BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" 
                    DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="6pt" 
                    ForeColor="#003399" Height="150px" Visible="False" Width="160px" 
                    onselectionchanged="CalendarioInicial_SelectionChanged" TabIndex="20">
                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                    <OtherMonthDayStyle Font-Bold="False" ForeColor="#999999" />
                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" 
                        Font-Bold="True" Font-Size="8pt" ForeColor="#CCCCFF" Height="22px" />
                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                    <WeekendDayStyle BackColor="#CCCCFF" />
                </asp:Calendar>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbFechaFinal" runat="server" Text="Fecha Final:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="FechaFinal" runat="server" Width="110px" TabIndex="30" 
                    MaxLength="10"></asp:TextBox>
            </td>
            <td>
                <asp:LinkButton ID="lbCalendarioFinal" runat="server" 
                    onclick="lbCalendarioFinal_Click" TabIndex="40" Width="107px">Abrir Calendario</asp:LinkButton>
            </td>
            <td>
                <asp:Calendar ID="CalendarioFinal" runat="server" BackColor="White" 
                    BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" 
                    DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="6pt" 
                    ForeColor="#003399" Height="150px" Visible="False" Width="160px" 
                    onselectionchanged="CalendarioFinal_SelectionChanged" TabIndex="50">
                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                    <OtherMonthDayStyle Font-Bold="False" ForeColor="#999999" />
                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" 
                        Font-Bold="True" Font-Size="8pt" ForeColor="#CCCCFF" Height="22px" />
                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                    <WeekendDayStyle BackColor="#CCCCFF" />
                </asp:Calendar>
            </td>
        </tr>
        <tr>
            <td>
                Tipo:</td>
            <td colspan="3">
                <asp:DropDownList ID="tipo" runat="server" Width="300px" TabIndex="60">
                </asp:DropDownList>
            &nbsp;<asp:CheckBox ID="cbBases" runat="server" Text="Listar Bases" TabIndex="65" />
            </td>
        </tr>
        <tr>
            <td>
                Tienda:</td>
            <td colspan="3">
                <asp:DropDownList ID="bodega" runat="server" Width="300px" TabIndex="70">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <table align="center" id="tablaBotones" runat="server">
        <tr>
            <td style="text-align: center">
                <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Button ID="btnGenerar" runat="server" Text="Generar Reporte" 
                    onclick="btnGenerar_Click" TabIndex="80" />&nbsp;<asp:Button ID="btnSalir" 
                    runat="server" Text="Salir" 
                    TabIndex="90" onclick="btnSalir_Click" />
            &nbsp;<asp:Button ID="btnImprimir" runat="server" 
                    Text="Imprimir" 
                    ToolTip="Prepara la página para impresión y para regresar a la vista normal haga clic sobre las fechas." 
                    TabIndex="100" Visible="False" />
            &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Label ID="Label2" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <table align="center">
        <tr>
            <td>
                <asp:GridView ID="gridVentas" runat="server" CellPadding="4" 
                    ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" 
                    onrowdatabound="gridVentas_RowDataBound" TabIndex="110" 
                    onsorting="gridVentas_Sorting" AllowSorting="True">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField AccessibleHeaderText="Tienda" HeaderText="Tienda">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Tienda") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Tienda") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="Tipo" HeaderText="Tipo" 
                            Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label23" runat="server" Text='<%# Bind("Tipo") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox23" runat="server" Text='<%# Bind("Tipo") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField AccessibleHeaderText="Tamaño" DataField="Tamaño" 
                            HeaderText="Tamaño" />
                        <asp:TemplateField AccessibleHeaderText="Varios" HeaderText="Varios">
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("Varios", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Varios") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="FrescoFoam" HeaderText="Fresco Foam" 
                            Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("FrescoFoam", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("FrescoFoam") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="Wonder" HeaderText="Wonder">
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("Wonder", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Wonder") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="Deluxe" HeaderText="Deluxe">
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Bind("Deluxe", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Deluxe") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="Advanced" HeaderText="Advanced">
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Bind("Advanced", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("Advanced") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="DreamSleeper" 
                            HeaderText="Dream Sleeper">
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Text='<%# Bind("DreamSleeper", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("DreamSleeper") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="KidzSleeper" HeaderText="Kidz Sleeper" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text='<%# Bind("KidzSleeper", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("KidzSleeper") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="TripleCrown" HeaderText="Triple Crown">
                            <ItemTemplate>
                                <asp:Label ID="Label9" runat="server" Text='<%# Bind("TripleCrown", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("TripleCrown") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="Luxurious" HeaderText="Luxurious">
                            <ItemTemplate>
                                <asp:Label ID="Label10" runat="server" Text='<%# Bind("Luxurious", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("Luxurious") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="WorldClass" HeaderText="World Class">
                            <ItemTemplate>
                                <asp:Label ID="Label11" runat="server" Text='<%# Bind("WorldClass", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("WorldClass") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="BtyRestExceptionale" HeaderText="Bty Rest Exceptionale" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="Label12" runat="server" Text='<%# Bind("BtyRestExceptionale", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox12" runat="server" Text='<%# Bind("BtyRestExceptionale") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="DeepSleepPlush" HeaderText="Deep Sleep Plush" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label13" runat="server" Text='<%# Bind("DeepSleepPlush", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox13" runat="server" Text='<%# Bind("DeepSleepPlush") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="BackCare" HeaderText="BackCare">
                            <ItemTemplate>
                                <asp:Label ID="Label14" runat="server" Text='<%# Bind("BackCare", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox14" runat="server" Text='<%# Bind("BackCare") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="NaturalTouch" HeaderText="Natural Touch" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="Label15" runat="server" Text='<%# Bind("NaturalTouch", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox15" runat="server" Text='<%# Bind("NaturalTouch") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="BtyRestBlack" HeaderText="Bty Rest Black">
                            <ItemTemplate>
                                <asp:Label ID="Label16" runat="server" Text='<%# Bind("BtyRestBlack", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox16" runat="server" Text='<%# Bind("BtyRestBlack") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="BtyRestExceptionalePlush" HeaderText="Bty Rest Exceptionale Plush" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label17" runat="server" Text='<%# Bind("BtyRestExceptionalePlush", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox17" runat="server" Text='<%# Bind("BtyRestExceptionalePlush") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="BeautySleepFirm" HeaderText="Bty Sleep Firm">
                            <ItemTemplate>
                                <asp:Label ID="Label18" runat="server" Text='<%# Bind("BeautySleepFirm", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox18" runat="server" Text='<%# Bind("BeautySleepFirm") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="BeautySleepPlush" HeaderText="Bty Sleep Plush">
                            <ItemTemplate>
                                <asp:Label ID="Label19" runat="server" Text='<%# Bind("BeautySleepPlush", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox19" runat="server" Text='<%# Bind("BeautySleepPlush") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="BeautyRestNXG" HeaderText="Bty Rest NXG" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="Label20" runat="server" Text='<%# Bind("BeautyRestNXG", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox20" runat="server" Text='<%# Bind("BeautyRestNXG") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="SlumberTime" HeaderText="Slumber Time">
                            <ItemTemplate>
                                <asp:Label ID="Label21" runat="server" Text='<%# Bind("SlumberTime", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox21" runat="server" Text='<%# Bind("SlumberTime") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="T.E. Pure Essence" AccessibleHeaderText="TrueEnergyPureEssence">
                            <ItemTemplate>
                                <asp:Label ID="Label24" runat="server" Text='<%# Bind("TrueEnergyPureEssence", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox24" runat="server" Text='<%# Bind("TrueEnergyPureEssence") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="T.E. Advanced" AccessibleHeaderText="TrueEnergyAdvanced">
                            <ItemTemplate>
                                <asp:Label ID="Label25" runat="server" Text='<%# Bind("TrueEnergyAdvanced", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox25" runat="server" Text='<%# Bind("TrueEnergyAdvanced") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="T.E. Elilte" AccessibleHeaderText="TrueEnergyElite">
                            <ItemTemplate>
                                <asp:Label ID="Label26" runat="server" Text='<%# Bind("TrueEnergyElite", "{0:###,###,###}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox26" runat="server" Text='<%# Bind("TrueEnergyElite") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField AccessibleHeaderText="TotalTamanio" HeaderText="Total Tamaño">
                            <ItemTemplate>
                                <asp:Label ID="Label22" runat="server" Text='<%# Bind("TotalTamanio") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox22" runat="server" Text='<%# Bind("TotalTamanio") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
        </tr>
    </table>

</asp:Content>
