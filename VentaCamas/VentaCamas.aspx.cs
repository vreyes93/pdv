﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VentaCamas
{
    public partial class VentaCamas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lbInfo.Text = "";
                Label1.Text = "";
                Label2.Text = "";

                FechaInicial.Text = "";
                FechaFinal.Text = "";
                
                ViewState["url"] = string.Format("http://sql.fiesta.local/services/wsVentaCamas.asmx", Request.Url.Host);

                CargarBodegas();
                FechaInicial.Focus();
            }
        }
        
        void CargarBodegas()
        {
            var ws = new wsVentaCamas.wsVentaCamas();
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            var q = ws.ListarBodegas().ToList();
            var qq = ws.ListarClientesMayoreo().ToList();

            wsVentaCamas.BodegasVentaCamas item = new wsVentaCamas.BodegasVentaCamas();
            item.Bodega = "MF";
            item.Nombre = "Ver Todas las Tiendas";
            q.Add(item);

            wsVentaCamas.ClientesMayoreo itemClientesMA = new wsVentaCamas.ClientesMayoreo();
            itemClientesMA.Cliente = "MA";
            itemClientesMA.Nombre = "LISTAR TODOS LOS CLIENTES DE MAYOREO";
            qq.Add(itemClientesMA);

            wsVentaCamas.ClientesMayoreo itemClientesMF = new wsVentaCamas.ClientesMayoreo();
            itemClientesMF.Cliente = "F";
            itemClientesMF.Nombre = "LISTAR CLIENTES DE MUEBLES FIESTA";
            qq.Add(itemClientesMF);

            tipo.DataSource = qq;
            tipo.DataTextField = "Nombre";
            tipo.DataValueField = "Cliente";
            tipo.DataBind();

            tipo.SelectedValue = "F";

            bodega.DataSource = q;
            bodega.DataTextField = "Nombre";
            bodega.DataValueField = "Bodega";
            bodega.DataBind();

            if (Session["bodega"] == null) bodega.SelectedValue = "F01"; else bodega.SelectedValue = (string)Session["bodega"];
        }

        protected void btnGenerar_Click(object sender, EventArgs e)
        {
            DateTime mFechaInicial, mFechaFinal;

            if (FechaInicial.Text.Trim().Length == 0)
            {
                Label2.Text = "";
                Label1.Text = "Debe ingresar la fecha inicial.";
                FechaInicial.Focus();
                return;
            }

            if (FechaFinal.Text.Trim().Length == 0)
            {
                Label2.Text = "";
                Label1.Text = "Debe ingresar la fecha final.";
                FechaFinal.Focus();
                return;
            }

            FechaInicial.Text = FechaInicial.Text.Replace("-", "/");
            FechaFinal.Text = FechaFinal.Text.Replace("-", "/");

            try
            {
                mFechaInicial = Convert.ToDateTime(FechaInicial.Text);
            }
            catch
            {
                Label2.Text = "";
                Label1.Text = "La fecha inicial ingresada es inválida.";
                FechaInicial.Focus();
                return;
            }

            try
            {
                mFechaFinal = Convert.ToDateTime(FechaFinal.Text);
            }
            catch
            {
                Label2.Text = "";
                Label1.Text = "La fecha final ingresada es inválida.";
                FechaFinal.Focus();
                return;
            }

            if (mFechaInicial > mFechaFinal)
            {
                Label2.Text = "";
                Label1.Text = "La fecha inicial no puede ser mayor a la fecha final.";
                FechaInicial.Focus();
                return;
            }

            GenerarVentas();
        }

        private void GenerarVentas()
        {
            char[] Separador = { '/' };
            string[] stringFechaInicial = FechaInicial.Text.Split(Separador);
            string[] stringFechaFinal = FechaFinal.Text.Split(Separador);

            DateTime mInicial = new DateTime(Convert.ToInt32(stringFechaInicial[2]), Convert.ToInt32(stringFechaInicial[1]), Convert.ToInt32(stringFechaInicial[0]));
            DateTime mFinal = new DateTime(Convert.ToInt32(stringFechaFinal[2]), Convert.ToInt32(stringFechaFinal[1]), Convert.ToInt32(stringFechaFinal[0]));

            var ws = new wsVentaCamas.wsVentaCamas();

            ws.Timeout = 999999;
            if (Convert.ToString(Session["Ambiente"]) == "PRO" || Convert.ToString(Session["Ambiente"]) == "PRU") ws.Url = Convert.ToString(ViewState["url"]);

            gridVentas.Visible = false;
            var q = ws.ListarVentaCamas(mInicial, mFinal, bodega.SelectedValue, tipo.SelectedValue, cbBases.Checked);

            if (tipo.SelectedValue.StartsWith("M"))
            {
                var qq = ws.ListarVentaCamasMayoreo(mInicial, mFinal, bodega.SelectedValue, tipo.SelectedValue, cbBases.Checked);

                for (int ii = 0; ii < q.Length; ii++)
                {
                    q[ii].Varios = q[ii].Varios + qq[ii].Varios;
                    q[ii].FrescoFoam = q[ii].FrescoFoam + qq[ii].FrescoFoam;
                    q[ii].Wonder = q[ii].Wonder + qq[ii].Wonder;
                    q[ii].Deluxe = q[ii].Deluxe + qq[ii].Deluxe;
                    q[ii].Advanced = q[ii].Advanced + qq[ii].Advanced;
                    q[ii].DreamSleeper = q[ii].DreamSleeper + qq[ii].DreamSleeper;
                    q[ii].KidzSleeper = q[ii].KidzSleeper + qq[ii].KidzSleeper;
                    q[ii].TripleCrown = q[ii].TripleCrown + qq[ii].TripleCrown;
                    q[ii].Luxurious = q[ii].Luxurious + qq[ii].Luxurious;
                    q[ii].WorldClass = q[ii].WorldClass + qq[ii].WorldClass;
                    q[ii].BtyRestExceptionale = q[ii].BtyRestExceptionale + qq[ii].BtyRestExceptionale;
                    q[ii].DeepSleepPlush = q[ii].DeepSleepPlush + qq[ii].DeepSleepPlush;
                    q[ii].BackCare = q[ii].BackCare + qq[ii].BackCare;
                    q[ii].NaturalTouch = q[ii].NaturalTouch + qq[ii].NaturalTouch;
                    q[ii].BtyRestBlack = q[ii].BtyRestBlack + qq[ii].BtyRestBlack;
                    q[ii].BtyRestExceptionalePlush = q[ii].BtyRestExceptionalePlush + qq[ii].BtyRestExceptionalePlush;
                    q[ii].BeautySleepFirm = q[ii].BeautySleepFirm + qq[ii].BeautySleepFirm;
                    q[ii].BeautySleepPlush = q[ii].BeautySleepPlush + qq[ii].BeautySleepPlush;
                    q[ii].BeautyRestNXG = q[ii].BeautyRestNXG + qq[ii].BeautyRestNXG;
                    q[ii].SlumberTime = q[ii].SlumberTime + qq[ii].SlumberTime;
                    q[ii].TrueEnergyPureEssence = q[ii].TrueEnergyPureEssence + qq[ii].TrueEnergyPureEssence;
                    q[ii].TrueEnergyAdvanced = q[ii].TrueEnergyAdvanced + qq[ii].TrueEnergyAdvanced;
                    q[ii].TrueEnergyElite = q[ii].TrueEnergyElite + qq[ii].TrueEnergyElite;
                }
            }

            List<wsVentaCamas.Camas> qqq = new List<wsVentaCamas.Camas>();

            for (int ii = 0; ii < q.Length; ii++)
            {
                if (q[ii].Varios > 0 || q[ii].FrescoFoam > 0 || q[ii].Wonder > 0 || q[ii].Deluxe > 0 || q[ii].Advanced > 0 || q[ii].DreamSleeper > 0 || q[ii].KidzSleeper > 0 || q[ii].TripleCrown > 0 || q[ii].Luxurious > 0 || q[ii].WorldClass > 0 || q[ii].BtyRestExceptionale > 0 || q[ii].DeepSleepPlush > 0 || q[ii].BackCare > 0 || q[ii].NaturalTouch > 0 || q[ii].BtyRestBlack > 0 || q[ii].BtyRestExceptionalePlush > 0 || q[ii].BeautySleepFirm > 0 || q[ii].BeautySleepPlush > 0 || q[ii].BeautyRestNXG > 0 || q[ii].SlumberTime > 0 || q[ii].TrueEnergyPureEssence > 0 || q[ii].TrueEnergyAdvanced > 0 || q[ii].TrueEnergyElite > 0)
                {
                    wsVentaCamas.Camas itemCamas = new wsVentaCamas.Camas();
                    itemCamas.Tienda = q[ii].Tienda;
                    itemCamas.Tipo = q[ii].Tipo;
                    itemCamas.Tamaño = q[ii].Tamaño;
                    itemCamas.Varios = q[ii].Varios;
                    itemCamas.FrescoFoam = q[ii].FrescoFoam;
                    itemCamas.Wonder = q[ii].Wonder;
                    itemCamas.Deluxe = q[ii].Deluxe;
                    itemCamas.Advanced = q[ii].Advanced;
                    itemCamas.DreamSleeper = q[ii].DreamSleeper;
                    itemCamas.KidzSleeper = q[ii].KidzSleeper;
                    itemCamas.TripleCrown = q[ii].TripleCrown;
                    itemCamas.Luxurious = q[ii].Luxurious;
                    itemCamas.WorldClass = q[ii].WorldClass;
                    itemCamas.BtyRestExceptionale = q[ii].BtyRestExceptionale;
                    itemCamas.DeepSleepPlush = q[ii].DeepSleepPlush;
                    itemCamas.BackCare = q[ii].BackCare;
                    itemCamas.NaturalTouch = q[ii].NaturalTouch;
                    itemCamas.BtyRestBlack = q[ii].BtyRestBlack;
                    itemCamas.BtyRestExceptionalePlush = q[ii].BtyRestExceptionalePlush;
                    itemCamas.BeautySleepFirm = q[ii].BeautySleepFirm;
                    itemCamas.BeautySleepPlush = q[ii].BeautySleepPlush;
                    itemCamas.BeautyRestNXG = q[ii].BeautyRestNXG;
                    itemCamas.SlumberTime = q[ii].SlumberTime;
                    itemCamas.TrueEnergyPureEssence = q[ii].TrueEnergyPureEssence;
                    itemCamas.TrueEnergyAdvanced = q[ii].TrueEnergyAdvanced;
                    itemCamas.TrueEnergyElite = q[ii].TrueEnergyElite;
                    itemCamas.TotalTamanio = q[ii].Varios + q[ii].FrescoFoam + q[ii].Wonder + q[ii].Deluxe + q[ii].Advanced + q[ii].DreamSleeper + q[ii].KidzSleeper + q[ii].TripleCrown + q[ii].Luxurious + q[ii].WorldClass + q[ii].BtyRestExceptionale + q[ii].DeepSleepPlush + q[ii].BackCare + q[ii].NaturalTouch + q[ii].BtyRestBlack + q[ii].BtyRestExceptionalePlush + q[ii].BeautySleepFirm + q[ii].BeautySleepPlush + q[ii].BeautyRestNXG + q[ii].SlumberTime + q[ii].TrueEnergyPureEssence + q[ii].TrueEnergyAdvanced + q[ii].TrueEnergyElite;
                    qqq.Add(itemCamas);
                }
            }            

            gridVentas.DataSource = qqq;
            gridVentas.DataBind();
            gridVentas.Visible = true;

            GridViewHelper helper = new GridViewHelper(this.gridVentas);

            helper.RegisterGroup("Tienda", true, true);
            helper.GroupHeader += new GroupEvent(helper_GroupHeader);

            helper.RegisterSummary("Varios", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            //helper.RegisterSummary("FrescoFoam", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            helper.RegisterSummary("Wonder", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            helper.RegisterSummary("Deluxe", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            helper.RegisterSummary("Advanced", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            helper.RegisterSummary("DreamSleeper", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            //helper.RegisterSummary("KidzSleeper", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            helper.RegisterSummary("TripleCrown", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            helper.RegisterSummary("Luxurious", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            helper.RegisterSummary("WorldClass", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            //helper.RegisterSummary("BtyRestExceptionale", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            //helper.RegisterSummary("DeepSleepPlush", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            helper.RegisterSummary("BackCare", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            //helper.RegisterSummary("NaturalTouch", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            helper.RegisterSummary("BtyRestBlack", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            //helper.RegisterSummary("BtyRestExceptionalePlush", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            helper.RegisterSummary("BeautySleepFirm", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            helper.RegisterSummary("BeautySleepPlush", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            //helper.RegisterSummary("BeautyRestNXG", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            helper.RegisterSummary("SlumberTime", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            helper.RegisterSummary("TrueEnergyPureEssence", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            helper.RegisterSummary("TrueEnergyAdvanced", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            helper.RegisterSummary("TrueEnergyElite", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");
            helper.RegisterSummary("TotalTamanio", "{0:###,###,###,###,###}", SummaryOperation.Sum, "Tienda");

            helper.GroupSummary += new GroupEvent(helper_Bug);


            helper.RegisterSummary("Varios", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            //helper.RegisterSummary("FrescoFoam", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            helper.RegisterSummary("Wonder", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            helper.RegisterSummary("Deluxe", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            helper.RegisterSummary("Advanced", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            helper.RegisterSummary("DreamSleeper", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            //helper.RegisterSummary("KidzSleeper", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            helper.RegisterSummary("TripleCrown", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            helper.RegisterSummary("Luxurious", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            helper.RegisterSummary("WorldClass", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            //helper.RegisterSummary("BtyRestExceptionale", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            //helper.RegisterSummary("DeepSleepPlush", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            helper.RegisterSummary("BackCare", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            //helper.RegisterSummary("NaturalTouch", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            helper.RegisterSummary("BtyRestBlack", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            //helper.RegisterSummary("BtyRestExceptionalePlush", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            helper.RegisterSummary("BeautySleepFirm", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            helper.RegisterSummary("BeautySleepPlush", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            //helper.RegisterSummary("BeautyRestNXG", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            helper.RegisterSummary("SlumberTime", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            helper.RegisterSummary("TrueEnergyPureEssence", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            helper.RegisterSummary("TrueEnergyAdvanced", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            helper.RegisterSummary("TrueEnergyElite", "{0:###,###,###,###,###}", SummaryOperation.Sum);
            helper.RegisterSummary("TotalTamanio", "{0:###,###,###,###,###}", SummaryOperation.Sum);

            helper.GeneralSummary += new FooterEvent(helper_GeneralSummary);


            helper.ApplyGroupSort();
            gridVentas.DataBind();
  
            gridVentas.Sort("Tienda", SortDirection.Ascending);

            for (int ii = 0; ii < gridVentas.Rows.Count; ii++)
            {
                for (int jj = 0; jj < gridVentas.Columns.Count; jj++)
                {
                    gridVentas.Rows[ii].Cells[jj].ToolTip = gridVentas.Columns[jj].HeaderText;
                }
            }

            gridVentas.GridLines = GridLines.Both;
        }

        private void helper_Bug(string groupName, object[] values, GridViewRow row)
        {
            if (groupName == null) return;

            if (groupName == "Tienda")
            {
                row.Cells[0].Text = "TOTAL TIENDA:";
                row.Cells[0].HorizontalAlign = HorizontalAlign.Right;

                row.Font.Bold = true;
                row.BackColor = System.Drawing.ColorTranslator.FromHtml("#F5F6CE");
            }

            if (groupName == "Tipo")
            {
                row.Cells[0].Text = "TOTAL TIPO:";
                row.Cells[0].HorizontalAlign = HorizontalAlign.Right;

                row.Font.Bold = true;
                row.BackColor = System.Drawing.ColorTranslator.FromHtml("#F5F6CE");
            }
        }

        private void helper_GeneralSummary(GridViewRow row)
        {
            row.Cells[0].Text = "TOTAL GENERAL:";
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;

            row.Font.Bold = true;
            row.ForeColor = System.Drawing.Color.White;
            row.BackColor = System.Drawing.ColorTranslator.FromHtml("#5D7B9D");
        }

        private void helper_GroupHeader(string groupName, object[] values, GridViewRow row)
        {
            if (groupName == "Tienda")
            {
                row.Cells[0].Font.Bold = true;
                row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                row.BackColor = System.Drawing.ColorTranslator.FromHtml("#B3D5F7");
                row.Cells[0].Text = string.Format("Tienda:   {0}", values[0].ToString());
            }

            if (groupName == "Tipo")
            {
                row.Cells[0].Font.Bold = true;
                row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                row.BackColor = System.Drawing.ColorTranslator.FromHtml("#B3D5F7");
                row.Cells[0].Text = string.Format("Tipo:   {0}", values[0].ToString());
            }
        }

        protected void gridVentas_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;this.style.backgroundColor='Khaki';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
            }
        }

        protected void lbCalendarioInicio_Click(object sender, EventArgs e)
        {
            if (CalendarioInicial.Visible)
            {
                CalendarioInicial.Visible = false;
                lbCalendarioInicio.Text = "Abrir Calendario";
            }
            else
            {
                CalendarioInicial.Visible = true;
                lbCalendarioInicio.Text = "Cerrar Calendario";
            }
        }

        protected void lbCalendarioFinal_Click(object sender, EventArgs e)
        {
            if (CalendarioFinal.Visible)
            {
                CalendarioFinal.Visible = false;
                lbCalendarioFinal.Text = "Abrir Calendario";
            }
            else
            {
                CalendarioFinal.Visible = true;
                lbCalendarioFinal.Text = "Cerrar Calendario";
            }
        }

        protected void CalendarioInicial_SelectionChanged(object sender, EventArgs e)
        {
            CalendarioInicial.Visible = false;
            lbCalendarioInicio.Text = "Abrir Calendario";
            FechaInicial.Text = CalendarioInicial.SelectedDate.ToShortDateString();
        }

        protected void CalendarioFinal_SelectionChanged(object sender, EventArgs e)
        {
            CalendarioFinal.Visible = false;
            lbCalendarioFinal.Text = "Abrir Calendario";
            FechaFinal.Text = CalendarioFinal.SelectedDate.ToShortDateString();
        }

        protected void gridVentas_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void bodega_SelectedIndexChanged(object sender, EventArgs e)
        {
            GenerarVentas();
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("http://{0}/VentaCamas/Default.aspx", Request.Url.Host));
        }

    }
}